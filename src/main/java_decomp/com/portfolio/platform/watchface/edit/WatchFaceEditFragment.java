package com.portfolio.platform.watchface.edit;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Ab7;
import com.fossil.Ao7;
import com.fossil.B77;
import com.fossil.Bb7;
import com.fossil.Cb7;
import com.fossil.D87;
import com.fossil.Ds0;
import com.fossil.Ea7;
import com.fossil.Eb7;
import com.fossil.El7;
import com.fossil.Fb7;
import com.fossil.Fc7;
import com.fossil.G67;
import com.fossil.Gc7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hb7;
import com.fossil.Hc7;
import com.fossil.Hq4;
import com.fossil.Hr7;
import com.fossil.Ja7;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Mo5;
import com.fossil.N97;
import com.fossil.O87;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.Rb7;
import com.fossil.Rg5;
import com.fossil.S37;
import com.fossil.S87;
import com.fossil.S97;
import com.fossil.Ts0;
import com.fossil.U37;
import com.fossil.Ub7;
import com.fossil.Um5;
import com.fossil.Vs0;
import com.fossil.W67;
import com.fossil.Y77;
import com.fossil.Yn7;
import com.fossil.Z97;
import com.fossil.Za7;
import com.fossil.Zb7;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;
import com.portfolio.platform.watchface.edit.WatchFaceEditViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceEditFragment extends Qv5 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ Ai D; // = new Ai(null);
    @DexIgnore
    public WatchFaceEditorView A;
    @DexIgnore
    public HashMap B;
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public /* final */ ArrayList<Fragment> i; // = new ArrayList<>();
    @DexIgnore
    public WatchFaceEditViewModel j;
    @DexIgnore
    public Rg5 k;
    @DexIgnore
    public int l; // = 2;
    @DexIgnore
    public View m;
    @DexIgnore
    public Ea7 s;
    @DexIgnore
    public S97 t;
    @DexIgnore
    public Z97 u;
    @DexIgnore
    public D87 v;
    @DexIgnore
    public N97 w;
    @DexIgnore
    public String x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchFaceEditFragment.C;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements BottomNavigationView.d {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 b;

        @DexIgnore
        public Bi(WatchFaceEditFragment watchFaceEditFragment, Hh6 hh6) {
            this.a = watchFaceEditFragment;
            this.b = hh6;
        }

        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            Drawable newDrawable;
            Drawable newDrawable2;
            Drawable newDrawable3;
            Drawable newDrawable4;
            Drawable newDrawable5;
            Drawable newDrawable6;
            Drawable drawable = null;
            Wg6.c(menuItem, "item");
            BottomNavigationView bottomNavigationView = WatchFaceEditFragment.M6(this.a).z;
            Wg6.b(bottomNavigationView, "mBinding.tlIndicator");
            Menu menu = bottomNavigationView.getMenu();
            Wg6.b(menu, "mBinding.tlIndicator.menu");
            menu.findItem(2131363192).setIcon(2131231208);
            menu.findItem(2131363149).setIcon(2131231198);
            menu.findItem(2131363186).setIcon(2131231207);
            menu.findItem(2131361907).setIcon(2131231203);
            menu.findItem(2131362148).setIcon(2131231199);
            int i = 2;
            switch (menuItem.getItemId()) {
                case 2131361907:
                    Drawable f = W6.f(this.a.requireContext(), 2131231203);
                    if (f != null) {
                        Drawable.ConstantState constantState = f.getConstantState();
                        if (!(constantState == null || (newDrawable = constantState.newDrawable()) == null)) {
                            drawable = newDrawable.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment = this.a;
                    View view = WatchFaceEditFragment.M6(watchFaceEditFragment).E;
                    Wg6.b(view, "mBinding.viewIndicatorPhoto");
                    watchFaceEditFragment.c7(view);
                    i = 3;
                    break;
                case 2131362148:
                    Drawable f2 = W6.f(this.a.requireContext(), 2131231199);
                    if (f2 != null) {
                        Drawable.ConstantState constantState2 = f2.getConstantState();
                        if (!(constantState2 == null || (newDrawable2 = constantState2.newDrawable()) == null)) {
                            drawable = newDrawable2.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment2 = this.a;
                    View view2 = WatchFaceEditFragment.M6(watchFaceEditFragment2).D;
                    Wg6.b(view2, "mBinding.viewIndicatorComplication");
                    watchFaceEditFragment2.c7(view2);
                    i = 4;
                    break;
                case 2131363149:
                    Drawable f3 = W6.f(this.a.requireContext(), 2131231198);
                    if (f3 != null) {
                        Drawable.ConstantState constantState3 = f3.getConstantState();
                        if (!(constantState3 == null || (newDrawable3 = constantState3.newDrawable()) == null)) {
                            drawable = newDrawable3.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment3 = this.a;
                    View view3 = WatchFaceEditFragment.M6(watchFaceEditFragment3).F;
                    Wg6.b(view3, "mBinding.viewIndicatorSticker");
                    watchFaceEditFragment3.c7(view3);
                    i = 1;
                    break;
                case 2131363186:
                    Drawable f4 = W6.f(this.a.requireContext(), 2131231207);
                    if (f4 != null) {
                        Drawable.ConstantState constantState4 = f4.getConstantState();
                        if (!(constantState4 == null || (newDrawable4 = constantState4.newDrawable()) == null)) {
                            drawable = newDrawable4.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment4 = this.a;
                    View view4 = WatchFaceEditFragment.M6(watchFaceEditFragment4).G;
                    Wg6.b(view4, "mBinding.viewIndicatorTemplate");
                    watchFaceEditFragment4.c7(view4);
                    break;
                case 2131363192:
                    Drawable f5 = W6.f(this.a.requireContext(), 2131231208);
                    if (f5 != null) {
                        Drawable.ConstantState constantState5 = f5.getConstantState();
                        if (!(constantState5 == null || (newDrawable5 = constantState5.newDrawable()) == null)) {
                            drawable = newDrawable5.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment5 = this.a;
                    View view5 = WatchFaceEditFragment.M6(watchFaceEditFragment5).H;
                    Wg6.b(view5, "mBinding.viewIndicatorText");
                    watchFaceEditFragment5.c7(view5);
                    i = 0;
                    break;
                default:
                    Drawable f6 = W6.f(this.a.requireContext(), 2131231207);
                    if (f6 != null) {
                        Drawable.ConstantState constantState6 = f6.getConstantState();
                        if (!(constantState6 == null || (newDrawable6 = constantState6.newDrawable()) == null)) {
                            drawable = newDrawable6.mutate();
                        }
                        if (drawable != null) {
                            drawable.setColorFilter(this.b.element, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(drawable);
                    }
                    WatchFaceEditFragment watchFaceEditFragment6 = this.a;
                    View view6 = WatchFaceEditFragment.M6(watchFaceEditFragment6).G;
                    Wg6.b(view6, "mBinding.viewIndicatorTemplate");
                    watchFaceEditFragment6.c7(view6);
                    break;
            }
            this.a.l = i;
            WatchFaceEditFragment.M6(this.a).I.j(i, false);
            B77.a.b(this.a.l, this.a.getActivity());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment b;

        @DexIgnore
        public Ci(WatchFaceEditFragment watchFaceEditFragment) {
            this.b = watchFaceEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.l7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment b;

        @DexIgnore
        public Di(WatchFaceEditFragment watchFaceEditFragment) {
            this.b = watchFaceEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(WatchFaceEditFragment.D.a(), "onclick preview elementConfigs");
            if (Jn5.c(Jn5.b, this.b.requireContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
                WatchFaceEditFragment.Q6(this.b).l0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment b;

        @DexIgnore
        public Ei(WatchFaceEditFragment watchFaceEditFragment) {
            this.b = watchFaceEditFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnDragListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Fi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final boolean onDrag(View view, DragEvent dragEvent) {
            Gc7 f;
            Wg6.b(dragEvent, Constants.EVENT);
            if (dragEvent.getAction() == 3) {
                ClipData clipData = dragEvent.getClipData();
                if (clipData == null || clipData.getItemCount() == 0) {
                    return false;
                }
                ClipData.Item itemAt = clipData.getItemAt(0);
                Wg6.b(itemAt, "clipData.getItemAt(0)");
                Intent intent = itemAt.getIntent();
                String stringExtra = intent.getStringExtra("complication_id");
                if (stringExtra != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = WatchFaceEditFragment.D.a();
                    local.d(a2, "clipData[EXTRA_COMPLICATION_ID] = " + stringExtra);
                    ClipDescription description = clipData.getDescription();
                    if (Wg6.a(description != null ? description.getLabel() : null, "complication_list")) {
                        int intExtra = intent.getIntExtra("complication_radius", 0);
                        Point point = (Point) intent.getParcelableExtra("complication_point");
                        if (point == null) {
                            point = new Point();
                        }
                        Za7 za7 = new Za7(dragEvent.getX() - ((((float) point.x) + WatchFaceEditFragment.R6(this.a).getOffset()) + WatchFaceEditFragment.R6(this.a).getX()), dragEvent.getY() - ((((float) point.y) + WatchFaceEditFragment.R6(this.a).getOffset()) + WatchFaceEditFragment.R6(this.a).getY()), intExtra);
                        if (!WatchFaceEditFragment.R6(this.a).p0(za7)) {
                            return false;
                        }
                        if (WatchFaceEditFragment.R6(this.a).o0(stringExtra)) {
                            return false;
                        }
                        Bb7 bb7 = new Bb7(stringExtra, "", null, za7);
                        WatchFaceEditFragment.Q6(this.a).V(Ab7.ADDED, bb7.a(), bb7.c());
                    }
                }
            } else if (dragEvent.getAction() == 4 && (f = Hc7.c.f(this.a)) != null) {
                f.s();
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements WatchFaceEditorView.b {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditFragment$initWatchFaceEditor$editorEventListener$1$initEditorDone$1", f = "WatchFaceEditFragment.kt", l = {188}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchFaceEditViewModel Q6 = WatchFaceEditFragment.Q6(this.this$0.a);
                    String str = this.this$0.a.x;
                    this.L$0 = il6;
                    this.label = 1;
                    if (Q6.i0(str, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (WatchFaceEditFragment.Q6(this.this$0.a).a0() != null) {
                    FlexibleTextView flexibleTextView = WatchFaceEditFragment.M6(this.this$0.a).C;
                    Wg6.b(flexibleTextView, "mBinding.tvPresetName");
                    Mo5 a0 = WatchFaceEditFragment.Q6(this.this$0.a).a0();
                    if (a0 != null) {
                        flexibleTextView.setText(a0.f());
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void a() {
            this.a.n7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void b() {
            this.a.d7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public boolean c(Zb7 zb7) {
            Wg6.c(zb7, "component");
            boolean i7 = this.a.i7(zb7);
            if (i7) {
                WatchFaceEditFragment.R6(this.a).r0(zb7);
            }
            this.a.d7();
            return i7;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void d(Zb7 zb7) {
            Wg6.c(zb7, "component");
            this.a.n7();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void e(W67.Bi bi, S87 s87) {
            Wg6.c(bi, "eventType");
            Wg6.c(s87, "config");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "onConfigChanged eventType=" + bi + " config=" + s87);
            WatchFaceEditFragment.Q6(this.a).k0(s87, bi);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void f(S87.Ci ci) {
            Wg6.c(ci, "textConfig");
            this.a.m7(ci);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void g(Eb7 eb7) {
            Wg6.c(eb7, Constants.EVENT);
            Gc7 f = Hc7.c.f(this.a);
            if (f != null) {
                f.E(eb7);
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void h(S87 s87) {
            Gc7 f = Hc7.c.f(this.a);
            if (f != null) {
                f.v(s87);
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.watchface.WatchFaceEditorView.b
        public void i(Rect rect) {
            Wg6.c(rect, "rect");
            Rm6 unused = Gu7.d(Ds0.a(this.a), null, null, new Aii(this, null), 3, null);
            Gc7 f = Hc7.c.f(this.a);
            if (f != null) {
                f.u(rect);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Hi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            WatchFaceEditFragment.R6(this.a).j0(t);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ii(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            S87.Ci ci = (S87.Ci) t.a();
            if (ci != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchFaceEditFragment.D.a();
                local.d(a2, "Received text: " + ci);
                if (!WatchFaceEditFragment.R6(this.a).Y(ci)) {
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886595);
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    Wg6.b(c, "errorDescription");
                    s37.K(childFragmentManager, c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ji(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            S87.Bi bi = (S87.Bi) t.a();
            if (bi != null && !WatchFaceEditFragment.R6(this.a).X(bi)) {
                Hr7 hr7 = Hr7.a;
                String string = PortfolioApp.get.instance().getString(2131886583);
                Wg6.b(string, "PortfolioApp.instance.ge\u2026erStickersAppliedToWatch)");
                String format = String.format(string, Arrays.copyOf(new Object[]{"5"}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.K(childFragmentManager, format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ki(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            WatchFaceEditorView.u0(WatchFaceEditFragment.R6(this.a), null, t, null, 5, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Li(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            WatchFaceEditorView.u0(WatchFaceEditFragment.R6(this.a), null, null, t, 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Mi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            boolean booleanValue = t.booleanValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "goalRingLive, value = " + booleanValue);
            WatchFaceEditFragment.R6(this.a).v0(booleanValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ni(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "listComplicationLive, value = " + ((String) t2));
            if (WatchFaceEditFragment.R6(this.a).getCurrentComplication() != null) {
                WatchFaceEditFragment.Q6(this.a).V(Ab7.SELECTED, t2, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Oi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "ringLive, value = " + ((Object) t2));
            WatchFaceEditFragment.R6(this.a).w0(t2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Pi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "dianaAppSetingLive, value = " + ((Object) t2));
            WatchFaceEditFragment.Q6(this.a).q0(t2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Qi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                bool.booleanValue();
                FLogger.INSTANCE.getLocal().d(WatchFaceEditFragment.D.a(), "refresh watch face editor");
                WatchFaceEditFragment.R6(this.a).q0();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri<T> implements Ls0<WatchFaceEditViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ri(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(WatchFaceEditViewModel.Bi bi) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchFaceEditFragment.D.a();
            local.d(a2, "complicationDataLive, value = " + bi);
            int i = Y77.a[bi.b().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    WatchFaceEditFragment.R6(this.a).x0(bi.a());
                }
            } else if (!WatchFaceEditFragment.R6(this.a).W(bi.a())) {
                Hr7 hr7 = Hr7.a;
                String string = PortfolioApp.get.instance().getString(2131886572);
                Wg6.b(string, "PortfolioApp.instance.ge\u2026erFeaturesAppliedToWatch)");
                String format = String.format(string, Arrays.copyOf(new Object[]{"4"}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.K(childFragmentManager, format);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchFaceEditViewModel.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si<T> implements Ls0<Ub7> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Si(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Ub7 ub7) {
            Hb7 a2;
            String str = null;
            if (ub7 == null) {
                Gc7 f = Hc7.c.f(this.a);
                if (f != null) {
                    f.x("-metadata.priority");
                    return;
                }
                return;
            }
            WatchFaceEditorView R6 = WatchFaceEditFragment.R6(this.a);
            Rb7 a3 = ub7.a();
            String c = a3 != null ? a3.c() : null;
            Rb7 a4 = ub7.a();
            R6.n0(c, a4 != null ? a4.b() : null);
            Gc7 f2 = Hc7.c.f(this.a);
            if (f2 != null) {
                Rb7 a5 = ub7.a();
                if (!(a5 == null || (a2 = a5.a()) == null)) {
                    str = a2.b();
                }
                f2.x(str);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ub7 ub7) {
            a(ub7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti<T> implements Ls0<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ti(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.z(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ui<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Ui(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "isChanged");
            if (bool.booleanValue()) {
                FlexibleButton flexibleButton = WatchFaceEditFragment.M6(this.a).r;
                Wg6.b(flexibleButton, "mBinding.fbApply");
                flexibleButton.setEnabled(true);
                WatchFaceEditFragment.M6(this.a).r.setBackgroundResource(2131231291);
                return;
            }
            FlexibleButton flexibleButton2 = WatchFaceEditFragment.M6(this.a).r;
            Wg6.b(flexibleButton2, "mBinding.fbApply");
            flexibleButton2.setEnabled(false);
            WatchFaceEditFragment.M6(this.a).r.setBackgroundResource(2131231292);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Vi<T> implements Ls0<Hq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Vi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Hq4.Ai ai) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, ai.a(), ai.b());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Wi<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Wi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi.a()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Xi<T> implements Ls0<Gl7<? extends Boolean, ? extends Boolean, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Xi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Gl7<Boolean, Boolean, Integer> gl7) {
            boolean booleanValue = gl7.getFirst().booleanValue();
            if (gl7.getSecond().booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = WatchFaceEditFragment.D.a();
                StringBuilder sb = new StringBuilder();
                sb.append("isStartFromMyFaces ");
                sb.append(this.a.y);
                sb.append(" presetId ");
                Mo5 a0 = WatchFaceEditFragment.Q6(this.a).a0();
                sb.append(a0 != null ? a0.e() : null);
                local.d(a2, sb.toString());
                if (this.a.y) {
                    Mo5 a02 = WatchFaceEditFragment.Q6(this.a).a0();
                    if (!TextUtils.isEmpty(a02 != null ? a02.e() : null)) {
                        HomeActivity.a aVar = HomeActivity.B;
                        FragmentActivity requireActivity = this.a.requireActivity();
                        Wg6.b(requireActivity, "requireActivity()");
                        Mo5 a03 = WatchFaceEditFragment.Q6(this.a).a0();
                        if (a03 != null) {
                            aVar.c(requireActivity, a03.e(), this.a.z);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
                this.a.requireActivity().finish();
            } else if (booleanValue) {
            } else {
                if (gl7.getThird().intValue() == 600 || gl7.getThird().intValue() == 601) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    s37.O(childFragmentManager, gl7.getThird().intValue(), "");
                    return;
                }
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.B;
                FragmentActivity requireActivity2 = this.a.requireActivity();
                Wg6.b(requireActivity2, "requireActivity()");
                aVar2.a(requireActivity2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends Boolean, ? extends Boolean, ? extends Integer> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Yi<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Yi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            WatchFaceEditFragment.Q6(this.a).j0(fb7);
            this.a.b7(fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Zi<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        public Zi(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            WatchFaceEditFragment.Q6(this.a).j0(fb7);
            this.a.b7(fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditFragment$setPresetToWatch$1", f = "WatchFaceEditFragment.kt", l = {464}, m = "invokeSuspend")
    public static final class a0 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a0(WatchFaceEditFragment watchFaceEditFragment, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a0 a0Var = new a0(this.this$0, xe6);
            a0Var.p$ = (Il6) obj;
            throw null;
            //return a0Var;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a0) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object h0;
            Boolean a2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WatchFaceEditViewModel Q6 = WatchFaceEditFragment.Q6(this.this$0);
                this.L$0 = il6;
                this.label = 1;
                h0 = Q6.h0(this);
                if (h0 == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                h0 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) h0).booleanValue()) {
                return Cd6.a;
            }
            Mo5 a0 = WatchFaceEditFragment.Q6(this.this$0).a0();
            boolean booleanValue = (a0 == null || (a2 = Ao7.a(a0.m())) == null) ? false : a2.booleanValue();
            List<S87> g0 = WatchFaceEditFragment.R6(this.this$0).g0();
            if (booleanValue && !Jn5.c(Jn5.b, this.this$0.requireActivity(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 44, null)) {
                return Cd6.a;
            }
            if (!Jn5.f(Jn5.b, this.this$0.requireActivity(), WatchFaceEditFragment.Q6(this.this$0).c0(g0), WatchFaceEditFragment.Q6(this.this$0).Z(g0), false, true, true, null, 72, null)) {
                return Cd6.a;
            }
            if (booleanValue) {
                WatchFaceEditFragment.Q6(this.this$0).P();
            } else {
                WatchFaceEditFragment.Q6(this.this$0).T();
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 implements Ja7.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b0(WatchFaceEditFragment watchFaceEditFragment) {
            this.a = watchFaceEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ja7.Bi
        public void a(String str) {
            Wg6.c(str, "text");
            WatchFaceEditorView.u0(WatchFaceEditFragment.R6(this.a), str, null, null, 6, null);
        }

        @DexIgnore
        @Override // com.fossil.Ja7.Bi
        public void onCancel() {
        }
    }

    /*
    static {
        String simpleName = WatchFaceEditFragment.class.getSimpleName();
        Wg6.b(simpleName, "WatchFaceEditFragment::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Rg5 M6(WatchFaceEditFragment watchFaceEditFragment) {
        Rg5 rg5 = watchFaceEditFragment.k;
        if (rg5 != null) {
            return rg5;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceEditViewModel Q6(WatchFaceEditFragment watchFaceEditFragment) {
        WatchFaceEditViewModel watchFaceEditViewModel = watchFaceEditFragment.j;
        if (watchFaceEditViewModel != null) {
            return watchFaceEditViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceEditorView R6(WatchFaceEditFragment watchFaceEditFragment) {
        WatchFaceEditorView watchFaceEditorView = watchFaceEditFragment.A;
        if (watchFaceEditorView != null) {
            return watchFaceEditorView;
        }
        Wg6.n("watchFaceEditor");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return C;
    }

    @DexIgnore
    public final void L() {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886536));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886534));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886535));
        fi.b(2131363291);
        fi.b(2131363373);
        fi.k(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -523101473) {
            if (hashCode == 1810891314 && str.equals("FAILED_LOADING_WATCH_FACE") && i2 == 2131363373 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
        } else {
            if (i2 != 2131363291) {
                if (i2 == 2131363373 && isActive()) {
                    l7();
                }
            } else if (isActive()) {
                requireActivity().finish();
            }
        }
    }

    @DexIgnore
    public final void b7(Fb7 fb7) {
        if (this.k == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (fb7 != null) {
            WatchFaceEditorView watchFaceEditorView = this.A;
            if (watchFaceEditorView != null) {
                watchFaceEditorView.s0(fb7.f(), fb7.e());
            } else {
                Wg6.n("watchFaceEditor");
                throw null;
            }
        } else {
            WatchFaceEditorView watchFaceEditorView2 = this.A;
            if (watchFaceEditorView2 != null) {
                watchFaceEditorView2.setWatchFaceBackground(null);
            } else {
                Wg6.n("watchFaceEditor");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c7(View view) {
        View view2;
        Drawable f = W6.f(PortfolioApp.get.instance(), 2131230957);
        Drawable f2 = W6.f(PortfolioApp.get.instance(), 2131230956);
        if (!(f == null || (view2 = this.m) == null)) {
            view2.setBackground(f);
        }
        if (f2 != null) {
            view.setBackground(f2);
        }
        this.m = view;
    }

    @DexIgnore
    public final void d7() {
        Rg5 rg5 = this.k;
        if (rg5 != null) {
            LinearLayout linearLayout = rg5.w;
            Wg6.b(linearLayout, "mBinding.llTab");
            linearLayout.setVisibility(0);
            Rg5 rg52 = this.k;
            if (rg52 != null) {
                ConstraintLayout constraintLayout = rg52.q;
                Wg6.b(constraintLayout, "mBinding.clTrashBin");
                constraintLayout.setVisibility(8);
                Rg5 rg53 = this.k;
                if (rg53 != null) {
                    FrameLayout frameLayout = rg53.t;
                    Wg6.b(frameLayout, "mBinding.flBottom");
                    frameLayout.setVisibility(0);
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e7() {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d)) {
            Rg5 rg5 = this.k;
            if (rg5 != null) {
                rg5.x.setBackgroundColor(Color.parseColor(d));
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        Hh6 hh6 = new Hh6();
        hh6.element = W6.d(requireContext(), 2131099967);
        String d2 = ThemeManager.l.a().d("primaryIconColor");
        if (d2 != null) {
            hh6.element = Color.parseColor(d2);
        }
        Rg5 rg52 = this.k;
        if (rg52 != null) {
            BottomNavigationView bottomNavigationView = rg52.z;
            Wg6.b(bottomNavigationView, "mBinding.tlIndicator");
            bottomNavigationView.setItemIconTintList(null);
            Rg5 rg53 = this.k;
            if (rg53 != null) {
                rg53.z.setOnNavigationItemSelectedListener(new Bi(this, hh6));
                Rg5 rg54 = this.k;
                if (rg54 != null) {
                    BottomNavigationView bottomNavigationView2 = rg54.z;
                    Wg6.b(bottomNavigationView2, "mBinding.tlIndicator");
                    bottomNavigationView2.setSelectedItemId(2131363186);
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void f7() {
        this.i.clear();
        this.s = (Ea7) getChildFragmentManager().Z("WatchFaceTextFragment");
        this.t = (S97) getChildFragmentManager().Z("WatchFaceStickerFragment");
        this.u = (Z97) getChildFragmentManager().Z("WatchFaceTemplateFragment");
        this.w = (N97) getChildFragmentManager().Z("WatchFacePhotoBackgroundFragment");
        this.v = (D87) getChildFragmentManager().Z("WatchFaceComplicationContainerFragment");
        if (this.s == null) {
            Fc7 fc7 = Fc7.a;
            String str = this.x;
            if (str == null) {
                str = "";
            }
            this.s = (Ea7) Fc7.b(fc7, Ea7.class, str, 0, null, false, false, 60, null);
        }
        if (this.t == null) {
            Fc7 fc72 = Fc7.a;
            String str2 = this.x;
            if (str2 == null) {
                str2 = "";
            }
            this.t = (S97) Fc7.b(fc72, S97.class, str2, 0, null, false, false, 60, null);
        }
        if (this.u == null) {
            Fc7 fc73 = Fc7.a;
            String str3 = this.x;
            if (str3 == null) {
                str3 = "";
            }
            this.u = (Z97) Fc7.b(fc73, Z97.class, str3, 0, null, false, false, 60, null);
        }
        if (this.w == null) {
            Fc7 fc74 = Fc7.a;
            String str4 = this.x;
            if (str4 == null) {
                str4 = "";
            }
            this.w = (N97) Fc7.b(fc74, N97.class, str4, 0, null, false, false, 60, null);
        }
        if (this.v == null) {
            Fc7 fc75 = Fc7.a;
            String str5 = this.x;
            if (str5 == null) {
                str5 = "";
            }
            this.v = (D87) Fc7.b(fc75, D87.class, str5, 0, null, false, false, 60, null);
        }
        Ea7 ea7 = this.s;
        if (ea7 != null) {
            this.i.add(ea7);
        }
        S97 s97 = this.t;
        if (s97 != null) {
            this.i.add(s97);
        }
        Z97 z97 = this.u;
        if (z97 != null) {
            this.i.add(z97);
        }
        N97 n97 = this.w;
        if (n97 != null) {
            this.i.add(n97);
        }
        D87 d87 = this.v;
        if (d87 != null) {
            this.i.add(d87);
        }
        Hc7 hc7 = Hc7.c;
        Ea7 ea72 = this.s;
        if (ea72 != null) {
            hc7.b(this, ea72);
            Hc7 hc72 = Hc7.c;
            S97 s972 = this.t;
            if (s972 != null) {
                hc72.b(this, s972);
                Hc7 hc73 = Hc7.c;
                Z97 z972 = this.u;
                if (z972 != null) {
                    hc73.b(this, z972);
                    Hc7 hc74 = Hc7.c;
                    N97 n972 = this.w;
                    if (n972 != null) {
                        hc74.b(this, n972);
                        Hc7 hc75 = Hc7.c;
                        D87 d872 = this.v;
                        if (d872 != null) {
                            hc75.b(this, d872);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void g7() {
        Rg5 rg5 = this.k;
        if (rg5 != null) {
            rg5.r.setOnClickListener(new Ci(this));
            Rg5 rg52 = this.k;
            if (rg52 != null) {
                rg52.s.setOnClickListener(new Di(this));
                Rg5 rg53 = this.k;
                if (rg53 != null) {
                    rg53.B.setOnClickListener(new Ei(this));
                    Rg5 rg54 = this.k;
                    if (rg54 != null) {
                        rg54.n().setOnDragListener(new Fi(this));
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void h7() {
        Rg5 rg5 = this.k;
        if (rg5 != null) {
            WatchFaceEditorView watchFaceEditorView = rg5.L;
            Wg6.b(watchFaceEditorView, "mBinding.watchFaceEditorView");
            this.A = watchFaceEditorView;
            Gi gi = new Gi(this);
            WatchFaceEditorView watchFaceEditorView2 = this.A;
            if (watchFaceEditorView2 != null) {
                Rg5 rg52 = this.k;
                if (rg52 != null) {
                    ConstraintLayout constraintLayout = rg52.q;
                    Wg6.b(constraintLayout, "mBinding.clTrashBin");
                    watchFaceEditorView2.m0(constraintLayout, gi);
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("watchFaceEditor");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final boolean i7(Zb7 zb7) {
        View view = zb7.getView();
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        view.getLocationOnScreen(iArr);
        Rg5 rg5 = this.k;
        if (rg5 != null) {
            rg5.q.getLocationOnScreen(iArr2);
            return view.getHeight() + iArr[1] > iArr2[1];
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void j7() {
        LiveData<U37<Boolean>> m2;
        LiveData<List<DianaAppSetting>> g;
        LiveData<Cb7> n;
        LiveData<String> k2;
        LiveData<Boolean> j2;
        LiveData<O87> o;
        LiveData<Typeface> p;
        MutableLiveData<U37<S87.Bi>> c;
        LiveData<U37<S87.Ci>> d;
        LiveData<Fb7> l2;
        LiveData<Fb7> e;
        WatchFaceEditViewModel watchFaceEditViewModel = this.j;
        if (watchFaceEditViewModel != null) {
            watchFaceEditViewModel.e0().h(getViewLifecycleOwner(), new Ui(this));
            WatchFaceEditViewModel watchFaceEditViewModel2 = this.j;
            if (watchFaceEditViewModel2 != null) {
                watchFaceEditViewModel2.h().h(getViewLifecycleOwner(), new Vi(this));
                WatchFaceEditViewModel watchFaceEditViewModel3 = this.j;
                if (watchFaceEditViewModel3 != null) {
                    watchFaceEditViewModel3.j().h(getViewLifecycleOwner(), new Wi(this));
                    WatchFaceEditViewModel watchFaceEditViewModel4 = this.j;
                    if (watchFaceEditViewModel4 != null) {
                        watchFaceEditViewModel4.U().h(getViewLifecycleOwner(), new Xi(this));
                        Gc7 f = Hc7.c.f(this);
                        if (!(f == null || (e = f.e()) == null)) {
                            e.h(getViewLifecycleOwner(), new Yi(this));
                        }
                        Gc7 f2 = Hc7.c.f(this);
                        if (!(f2 == null || (l2 = f2.l()) == null)) {
                            l2.h(getViewLifecycleOwner(), new Zi(this));
                        }
                        Gc7 f3 = Hc7.c.f(this);
                        if (!(f3 == null || (d = f3.d()) == null)) {
                            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
                            d.h(viewLifecycleOwner, new Ii(this));
                        }
                        Gc7 f4 = Hc7.c.f(this);
                        if (!(f4 == null || (c = f4.c()) == null)) {
                            LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner2, "viewLifecycleOwner");
                            c.h(viewLifecycleOwner2, new Ji(this));
                        }
                        Gc7 f5 = Hc7.c.f(this);
                        if (!(f5 == null || (p = f5.p()) == null)) {
                            LifecycleOwner viewLifecycleOwner3 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner3, "viewLifecycleOwner");
                            p.h(viewLifecycleOwner3, new Ki(this));
                        }
                        Gc7 f6 = Hc7.c.f(this);
                        if (!(f6 == null || (o = f6.o()) == null)) {
                            LifecycleOwner viewLifecycleOwner4 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner4, "viewLifecycleOwner");
                            o.h(viewLifecycleOwner4, new Li(this));
                        }
                        Gc7 f7 = Hc7.c.f(this);
                        if (!(f7 == null || (j2 = f7.j()) == null)) {
                            LifecycleOwner viewLifecycleOwner5 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner5, "viewLifecycleOwner");
                            j2.h(viewLifecycleOwner5, new Mi(this));
                        }
                        Gc7 f8 = Hc7.c.f(this);
                        if (!(f8 == null || (k2 = f8.k()) == null)) {
                            LifecycleOwner viewLifecycleOwner6 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner6, "viewLifecycleOwner");
                            k2.h(viewLifecycleOwner6, new Ni(this));
                        }
                        Gc7 f9 = Hc7.c.f(this);
                        if (!(f9 == null || (n = f9.n()) == null)) {
                            LifecycleOwner viewLifecycleOwner7 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner7, "viewLifecycleOwner");
                            n.h(viewLifecycleOwner7, new Oi(this));
                        }
                        Gc7 f10 = Hc7.c.f(this);
                        if (!(f10 == null || (g = f10.g()) == null)) {
                            LifecycleOwner viewLifecycleOwner8 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner8, "viewLifecycleOwner");
                            g.h(viewLifecycleOwner8, new Pi(this));
                        }
                        Gc7 f11 = Hc7.c.f(this);
                        if (!(f11 == null || (m2 = f11.m()) == null)) {
                            LifecycleOwner viewLifecycleOwner9 = getViewLifecycleOwner();
                            Wg6.b(viewLifecycleOwner9, "viewLifecycleOwner");
                            m2.h(viewLifecycleOwner9, new Qi(this));
                        }
                        WatchFaceEditViewModel watchFaceEditViewModel5 = this.j;
                        if (watchFaceEditViewModel5 != null) {
                            watchFaceEditViewModel5.W().h(getViewLifecycleOwner(), new Ri(this));
                            WatchFaceEditViewModel watchFaceEditViewModel6 = this.j;
                            if (watchFaceEditViewModel6 != null) {
                                watchFaceEditViewModel6.d0().h(getViewLifecycleOwner(), new Si(this));
                                WatchFaceEditViewModel watchFaceEditViewModel7 = this.j;
                                if (watchFaceEditViewModel7 != null) {
                                    LiveData<List<S87>> Y = watchFaceEditViewModel7.Y();
                                    LifecycleOwner viewLifecycleOwner10 = getViewLifecycleOwner();
                                    Wg6.b(viewLifecycleOwner10, "viewLifecycleOwner");
                                    Y.h(viewLifecycleOwner10, new Hi(this));
                                    WatchFaceEditViewModel watchFaceEditViewModel8 = this.j;
                                    if (watchFaceEditViewModel8 != null) {
                                        watchFaceEditViewModel8.b0().h(getViewLifecycleOwner(), new Ti(this));
                                    } else {
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void k7() {
        WatchFaceEditViewModel watchFaceEditViewModel = this.j;
        if (watchFaceEditViewModel != null) {
            Boolean e = watchFaceEditViewModel.e0().e();
            if (e == null) {
                e = Boolean.FALSE;
            }
            Wg6.b(e, "viewModel.wfChangedLive.value ?: false");
            if (e.booleanValue()) {
                L();
                return;
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void l7() {
        FLogger.INSTANCE.getLocal().d(C, "setPresetToWatch");
        Rm6 unused = Gu7.d(Ds0.a(this), null, null, new a0(this, null), 3, null);
    }

    @DexIgnore
    public final void m7(S87.Ci ci) {
        if (getChildFragmentManager().Z(Ja7.h.a()) == null) {
            Ja7 b = Ja7.h.b(ci.i(), new b0(this));
            if (isActive()) {
                b.show(getChildFragmentManager(), Ja7.h.a());
            }
        }
    }

    @DexIgnore
    public final void n7() {
        Rg5 rg5 = this.k;
        if (rg5 != null) {
            LinearLayout linearLayout = rg5.w;
            Wg6.b(linearLayout, "mBinding.llTab");
            linearLayout.setVisibility(8);
            Rg5 rg52 = this.k;
            if (rg52 != null) {
                ConstraintLayout constraintLayout = rg52.q;
                Wg6.b(constraintLayout, "mBinding.clTrashBin");
                constraintLayout.setVisibility(0);
                Rg5 rg53 = this.k;
                if (rg53 != null) {
                    FrameLayout frameLayout = rg53.t;
                    Wg6.b(frameLayout, "mBinding.flBottom");
                    frameLayout.setVisibility(8);
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        int i2;
        super.onActivityCreated(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.x = arguments.getString("preset_id");
            arguments.getString("order_id");
            this.y = arguments.getBoolean("from_faces");
            this.z = arguments.getBoolean("sharing_flow_extra");
            i2 = arguments.getInt("TAB_EXTRA");
        } else {
            i2 = 2;
        }
        FLogger.INSTANCE.getLocal().d(C, "onActivityCreated isStartFromMyFace " + this.y + " isSharingFlow " + this.z);
        PortfolioApp.get.instance().getIface().q1().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            Po4 po4 = this.h;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(watchFaceEditActivity, po4).a(WatchFaceEditViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026ditViewModel::class.java)");
                this.j = (WatchFaceEditViewModel) a2;
                Hc7.c.a(this);
                h7();
                f7();
                e7();
                Rg5 rg5 = this.k;
                if (rg5 != null) {
                    ViewPager2 viewPager2 = rg5.I;
                    Wg6.b(viewPager2, "mBinding.vpTab");
                    viewPager2.setAdapter(new G67(getChildFragmentManager(), this.i));
                    Rg5 rg52 = this.k;
                    if (rg52 != null) {
                        ViewPager2 viewPager22 = rg52.I;
                        Wg6.b(viewPager22, "mBinding.vpTab");
                        viewPager22.setUserInputEnabled(false);
                        Rg5 rg53 = this.k;
                        if (rg53 != null) {
                            ViewPager2 viewPager23 = rg53.I;
                            Wg6.b(viewPager23, "mBinding.vpTab");
                            viewPager23.setOffscreenPageLimit(4);
                            Rg5 rg54 = this.k;
                            if (rg54 != null) {
                                rg54.I.j(i2, false);
                                this.l = i2;
                                Rg5 rg55 = this.k;
                                if (rg55 != null) {
                                    BottomNavigationView bottomNavigationView = rg55.z;
                                    Wg6.b(bottomNavigationView, "mBinding.tlIndicator");
                                    bottomNavigationView.setSelectedItemId(this.l != 4 ? 2131363186 : 2131362148);
                                    g7();
                                    j7();
                                    return;
                                }
                                Wg6.n("mBinding");
                                throw null;
                            }
                            Wg6.n("mBinding");
                            throw null;
                        }
                        Wg6.n("mBinding");
                        throw null;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("viewModelFactory");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && Jn5.b.m(getContext(), Jn5.Ci.BLUETOOTH_CONNECTION)) {
            FLogger.INSTANCE.getLocal().d(C, "onclick apply elements elementConfigs, user skip setting & permission");
            WatchFaceEditViewModel watchFaceEditViewModel = this.j;
            if (watchFaceEditViewModel != null) {
                Mo5 a02 = watchFaceEditViewModel.a0();
                if (a02 != null ? a02.m() : false) {
                    WatchFaceEditViewModel watchFaceEditViewModel2 = this.j;
                    if (watchFaceEditViewModel2 != null) {
                        watchFaceEditViewModel2.P();
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    WatchFaceEditViewModel watchFaceEditViewModel3 = this.j;
                    if (watchFaceEditViewModel3 != null) {
                        watchFaceEditViewModel3.T();
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean onBackPressed() {
        k7();
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Rg5 z2 = Rg5.z(layoutInflater);
        Wg6.b(z2, "WatchFaceCustomizeFragme\u2026Binding.inflate(inflater)");
        this.k = z2;
        String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (d != null) {
            Rg5 rg5 = this.k;
            if (rg5 != null) {
                rg5.n().setBackgroundColor(Color.parseColor(d));
                Rg5 rg52 = this.k;
                if (rg52 != null) {
                    rg52.J.setBackgroundColor(Color.parseColor(d));
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        Rg5 rg53 = this.k;
        if (rg53 != null) {
            this.m = rg53.E;
            if (rg53 != null) {
                View n = rg53.n();
                Wg6.b(n, "mBinding.root");
                return n;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        Hc7.c.c(this);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.B;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
