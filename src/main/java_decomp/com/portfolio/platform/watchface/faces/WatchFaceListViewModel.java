package com.portfolio.platform.watchface.faces;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.B77;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Hq5;
import com.fossil.Hs0;
import com.fossil.J37;
import com.fossil.Jo5;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Kz4;
import com.fossil.Mo5;
import com.fossil.Oo5;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Us0;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceListViewModel extends Hq4 {
    @DexIgnore
    public /* final */ LiveData<List<DianaWatchFaceUser>> h;
    @DexIgnore
    public /* final */ LiveData<Integer> i;
    @DexIgnore
    public /* final */ MutableLiveData<String> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ DianaWatchFaceRepository l;
    @DexIgnore
    public /* final */ FileRepository m;
    @DexIgnore
    public /* final */ DianaPresetRepository n;
    @DexIgnore
    public /* final */ PreviewWatchFaceUseCase o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class WatchFaceDownloadResponse {
        @DexIgnore
        public String checksumFace;
        @DexIgnore
        public String downloadFaceUrl;
        @DexIgnore
        public String name;
        @DexIgnore
        public String previewFaceUrl;

        @DexIgnore
        public WatchFaceDownloadResponse(String str, String str2, String str3, String str4) {
            this.downloadFaceUrl = str;
            this.checksumFace = str2;
            this.name = str3;
            this.previewFaceUrl = str4;
        }

        @DexIgnore
        public static /* synthetic */ WatchFaceDownloadResponse copy$default(WatchFaceDownloadResponse watchFaceDownloadResponse, String str, String str2, String str3, String str4, int i, Object obj) {
            if ((i & 1) != 0) {
                str = watchFaceDownloadResponse.downloadFaceUrl;
            }
            if ((i & 2) != 0) {
                str2 = watchFaceDownloadResponse.checksumFace;
            }
            if ((i & 4) != 0) {
                str3 = watchFaceDownloadResponse.name;
            }
            if ((i & 8) != 0) {
                str4 = watchFaceDownloadResponse.previewFaceUrl;
            }
            return watchFaceDownloadResponse.copy(str, str2, str3, str4);
        }

        @DexIgnore
        public final String component1() {
            return this.downloadFaceUrl;
        }

        @DexIgnore
        public final String component2() {
            return this.checksumFace;
        }

        @DexIgnore
        public final String component3() {
            return this.name;
        }

        @DexIgnore
        public final String component4() {
            return this.previewFaceUrl;
        }

        @DexIgnore
        public final WatchFaceDownloadResponse copy(String str, String str2, String str3, String str4) {
            return new WatchFaceDownloadResponse(str, str2, str3, str4);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof WatchFaceDownloadResponse) {
                    WatchFaceDownloadResponse watchFaceDownloadResponse = (WatchFaceDownloadResponse) obj;
                    if (!Wg6.a(this.downloadFaceUrl, watchFaceDownloadResponse.downloadFaceUrl) || !Wg6.a(this.checksumFace, watchFaceDownloadResponse.checksumFace) || !Wg6.a(this.name, watchFaceDownloadResponse.name) || !Wg6.a(this.previewFaceUrl, watchFaceDownloadResponse.previewFaceUrl)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getChecksumFace() {
            return this.checksumFace;
        }

        @DexIgnore
        public final String getDownloadFaceUrl() {
            return this.downloadFaceUrl;
        }

        @DexIgnore
        public final String getName() {
            return this.name;
        }

        @DexIgnore
        public final String getPreviewFaceUrl() {
            return this.previewFaceUrl;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.downloadFaceUrl;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.checksumFace;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.name;
            int hashCode3 = str3 != null ? str3.hashCode() : 0;
            String str4 = this.previewFaceUrl;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public final void setChecksumFace(String str) {
            this.checksumFace = str;
        }

        @DexIgnore
        public final void setDownloadFaceUrl(String str) {
            this.downloadFaceUrl = str;
        }

        @DexIgnore
        public final void setName(String str) {
            this.name = str;
        }

        @DexIgnore
        public final void setPreviewFaceUrl(String str) {
            this.previewFaceUrl = str;
        }

        @DexIgnore
        public String toString() {
            return "WatchFaceDownloadResponse(downloadFaceUrl=" + this.downloadFaceUrl + ", checksumFace=" + this.checksumFace + ", name=" + this.name + ", previewFaceUrl=" + this.previewFaceUrl + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements V3<List<? extends DianaWatchFaceUser>, Integer> {
        @DexIgnore
        @Override // com.mapped.V3
        public final Integer apply(List<? extends DianaWatchFaceUser> list) {
            return Integer.valueOf(list.size());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$_watchFaceListLiveData$1", f = "WatchFaceListViewModel.kt", l = {59, 59}, m = "invokeSuspend")
    public static final class b extends Ko7 implements Coroutine<Hs0<List<? extends DianaWatchFaceUser>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFaceListViewModel watchFaceListViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (Hs0) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Hs0<List<? extends DianaWatchFaceUser>> hs0, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b) create(hs0, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x003c
                if (r0 == r2) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                com.fossil.El7.b(r7)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$1
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                com.fossil.El7.b(r7)
                r3 = r0
                r2 = r7
            L_0x002d:
                r0 = r2
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.a(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x0017
            L_0x003c:
                com.fossil.El7.b(r7)
                com.fossil.Hs0 r0 = r6.p$
                com.portfolio.platform.watchface.faces.WatchFaceListViewModel r1 = r6.this$0
                com.portfolio.platform.data.source.DianaWatchFaceRepository r1 = com.portfolio.platform.watchface.faces.WatchFaceListViewModel.r(r1)
                r6.L$0 = r0
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r1.getAllDianaWatchFaceUserLiveData(r6)
                if (r2 != r4) goto L_0x0055
                r0 = r4
                goto L_0x0017
            L_0x0055:
                r1 = r0
                r3 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1", f = "WatchFaceListViewModel.kt", l = {88, 90, 97, 112, 123, 134}, m = "invokeSuspend")
    public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchFaceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$1", f = "WatchFaceListViewModel.kt", l = {124}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $presetId;
            @DexIgnore
            public /* final */ /* synthetic */ Kz4 $response;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Kz4 kz4, Jh6 jh6, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$response = kz4;
                this.$presetId = jh6;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, this.$response, this.$presetId, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FileType fileType = FileType.WATCH_FACE;
                    String b = ((Mo5) this.$response.c()).b();
                    this.L$0 = il6;
                    this.label = 1;
                    Object downloadAndSaveWithFileName = this.this$0.this$0.m.downloadAndSaveWithFileName(((Mo5) this.$response.c()).d(), this.$presetId.element, fileType, b, this);
                    return downloadAndSaveWithFileName == d ? d : downloadAndSaveWithFileName;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$2", f = "WatchFaceListViewModel.kt", l = {135}, m = "invokeSuspend")
        public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Mo5 $activePreset;
            @DexIgnore
            public /* final */ /* synthetic */ Mo5 $preset;
            @DexIgnore
            public /* final */ /* synthetic */ byte[] $watchFaceByteArrayData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, Mo5 mo5, Mo5 mo52, byte[] bArr, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$preset = mo5;
                this.$activePreset = mo52;
                this.$watchFaceByteArrayData = bArr;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                b bVar = new b(this.this$0, this.$preset, this.$activePreset, this.$watchFaceByteArrayData, xe6);
                bVar.p$ = (Il6) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object v;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
                    this.L$0 = il6;
                    this.label = 1;
                    v = encryptedDatabaseManager.v(this);
                    if (v == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    v = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ((DianaCustomizeDatabase) v).getDianaPresetDao().l(this.$preset);
                B77.a.d(this.$activePreset.e());
                FileRepository fileRepository = this.this$0.this$0.m;
                String e = this.$preset.e();
                FileType fileType = FileType.WATCH_FACE;
                byte[] bArr = this.$watchFaceByteArrayData;
                Wg6.b(bArr, "watchFaceByteArrayData");
                String d2 = J37.d(bArr);
                byte[] bArr2 = this.$watchFaceByteArrayData;
                Wg6.b(bArr2, "watchFaceByteArrayData");
                FileRepository.writeFileWithDir$default(fileRepository, e, fileType, d2, bArr2, null, 16, null);
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$activePreset$1", f = "WatchFaceListViewModel.kt", l = {91}, m = "invokeSuspend")
        public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Mo5>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(c cVar, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$activeSerial = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                c cVar = new c(this.this$0, this.$activeSerial, xe6);
                cVar.p$ = (Il6) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Mo5> xe6) {
                throw null;
                //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DianaPresetRepository dianaPresetRepository = this.this$0.this$0.n;
                    String str = this.$activeSerial;
                    this.L$0 = il6;
                    this.label = 1;
                    Object b = dianaPresetRepository.b(str, this);
                    return b == d ? d : b;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$response$1", f = "WatchFaceListViewModel.kt", l = {117}, m = "invokeSuspend")
        public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Mo5>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Mo5 $activePreset;
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerial;
            @DexIgnore
            public /* final */ /* synthetic */ String $todayDate;
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser $watchFace;
            @DexIgnore
            public /* final */ /* synthetic */ String $watchFaceThemeData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, Mo5 mo5, DianaWatchFaceUser dianaWatchFaceUser, String str, String str2, String str3, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$activePreset = mo5;
                this.$watchFace = dianaWatchFaceUser;
                this.$watchFaceThemeData = str;
                this.$activeSerial = str2;
                this.$todayDate = str3;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                d dVar = new d(this.this$0, this.$activePreset, this.$watchFace, this.$watchFaceThemeData, this.$activeSerial, this.$todayDate, xe6);
                dVar.p$ = (Il6) obj;
                throw null;
                //return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Mo5>> xe6) {
                throw null;
                //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    String e = Ym5.e(24);
                    Wg6.b(e, "StringHelper.randomUUID(24)");
                    List<Oo5> a2 = this.$activePreset.a();
                    String name = this.$watchFace.getName();
                    String previewURL = this.$watchFace.getPreviewURL();
                    String str2 = this.$watchFaceThemeData;
                    Wg6.b(str2, "watchFaceThemeData");
                    String str3 = this.$activeSerial;
                    DianaWatchFaceOrder order = this.$watchFace.getOrder();
                    if (order == null || (str = order.getWatchFaceId()) == null) {
                        str = "";
                    }
                    String str4 = this.$todayDate;
                    Jo5 jo5 = new Jo5(e, a2, name, previewURL, str2, str3, false, str, str4, str4);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceListViewModel", "createWatchFaceAsPresetById presetDraft name " + this.$watchFace.getName() + " orderId " + this.$watchFace.getOrder());
                    DianaPresetRepository dianaPresetRepository = this.this$0.this$0.n;
                    this.L$0 = il6;
                    this.L$1 = jo5;
                    this.label = 1;
                    Object q = dianaPresetRepository.q(jo5, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    Jo5 jo52 = (Jo5) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$createWatchFaceAsPresetById$1$watchFaceDataFile$1", f = "WatchFaceListViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class e extends Ko7 implements Coroutine<Il6, Xe6<? super File>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DianaWatchFaceUser $watchFace;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(c cVar, DianaWatchFaceUser dianaWatchFaceUser, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$watchFace = dianaWatchFaceUser;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                e eVar = new e(this.this$0, this.$watchFace, xe6);
                eVar.p$ = (Il6) obj;
                throw null;
                //return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super File> xe6) {
                throw null;
                //return ((e) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.m.getFileByFileName(this.$watchFace.getId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFaceListViewModel watchFaceListViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceListViewModel;
            this.$watchFaceId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c cVar = new c(this.this$0, this.$watchFaceId, xe6);
            cVar.p$ = (Il6) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x00cf  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0169  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x01a9  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0252  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x02b8  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x02c1  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x0348  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r25) {
            /*
            // Method dump skipped, instructions count: 1104
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Xe6 xe6, WatchFaceListViewModel watchFaceListViewModel, String str) {
            super(2, xe6);
            this.this$0 = watchFaceListViewModel;
            this.$id$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            d dVar = new d(xe6, this.this$0, this.$id$inlined);
            dVar.p$ = (Il6) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hq4.d(this.this$0, true, false, null, 6, null);
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.l;
                String str = this.$id$inlined;
                this.L$0 = il6;
                this.label = 1;
                if (dianaWatchFaceRepository.deleteDianaWatchFaceUserById(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Hq4.d(this.this$0, false, true, null, 5, null);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$downloadShareWatchFace$1", f = "WatchFaceListViewModel.kt", l = {212}, m = "invokeSuspend")
    public static final class e extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$downloadShareWatchFace$1$1", f = "WatchFaceListViewModel.kt", l = {222, 229, 238}, m = "invokeSuspend")
        public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WatchFaceDownloadResponse $wfResponse;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, WatchFaceDownloadResponse watchFaceDownloadResponse, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
                this.$wfResponse = watchFaceDownloadResponse;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                a aVar = new a(this.this$0, this.$wfResponse, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0129  */
            /* JADX WARNING: Removed duplicated region for block: B:53:0x01ce  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 517
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WatchFaceListViewModel watchFaceListViewModel, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceListViewModel;
            this.$id = str;
            this.$token = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            e eVar = new e(this.this$0, this.$id, this.$token, xe6);
            eVar.p$ = (Il6) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((e) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object j;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hq4.d(this.this$0, true, false, null, 6, null);
                DianaPresetRepository dianaPresetRepository = this.this$0.n;
                String str = this.$id;
                String str2 = this.$token;
                this.L$0 = il6;
                this.label = 1;
                j = dianaPresetRepository.j(str, str2, this);
                if (j == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                j = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ap4 ap4 = (Ap4) j;
            if (ap4 instanceof Kq5) {
                WatchFaceDownloadResponse watchFaceDownloadResponse = (WatchFaceDownloadResponse) new Gson().k(String.valueOf(((Kq5) ap4).a()), WatchFaceDownloadResponse.class);
                if (watchFaceDownloadResponse != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchFaceListViewModel", "downloadShareWatchFace wfResponse " + watchFaceDownloadResponse);
                    if (!(watchFaceDownloadResponse.getDownloadFaceUrl() == null || watchFaceDownloadResponse.getChecksumFace() == null || watchFaceDownloadResponse.getName() == null)) {
                        Rm6 unused = Gu7.d(Us0.a(this.this$0), Bw7.b(), null, new a(this, watchFaceDownloadResponse, null), 2, null);
                    }
                } else {
                    Hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel = this.this$0;
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    Hq4.b(watchFaceListViewModel, 0, c, 1, null);
                }
            } else if (ap4 instanceof Hq5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("downloadShareWatchFace get link failed ");
                Hq5 hq5 = (Hq5) ap4;
                sb.append(hq5.a());
                local2.d("WatchFaceListViewModel", sb.toString());
                if (hq5.a() == 404 || hq5.a() == 403) {
                    Hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel2 = this.this$0;
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886580);
                    Wg6.b(c2, "LanguageHelper.getString\u2026yThisWatchFaceWasRemoved)");
                    Hq4.b(watchFaceListViewModel2, 0, c2, 1, null);
                } else {
                    Hq4.d(this.this$0, false, true, null, 5, null);
                    WatchFaceListViewModel watchFaceListViewModel3 = this.this$0;
                    String c3 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    Hq4.b(watchFaceListViewModel3, 0, c3, 1, null);
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel", f = "WatchFaceListViewModel.kt", l = {72}, m = "isReachedMaxPreset")
    public static final class f extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(WatchFaceListViewModel watchFaceListViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.WatchFaceListViewModel$isReachedMaxPreset$totalPreset$1", f = "WatchFaceListViewModel.kt", l = {72}, m = "invokeSuspend")
    public static final class g extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(WatchFaceListViewModel watchFaceListViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            g gVar = new g(this.this$0, xe6);
            gVar.p$ = (Il6) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
            throw null;
            //return ((g) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaPresetRepository dianaPresetRepository = this.this$0.n;
                String J = PortfolioApp.get.instance().J();
                this.L$0 = il6;
                this.label = 1;
                Object i2 = dianaPresetRepository.i(J, this);
                return i2 == d ? d : i2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceListViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CoroutineUseCase.Ei<PreviewWatchFaceUseCase.Ci, PreviewWatchFaceUseCase.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            public a(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(PreviewWatchFaceUseCase.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(PreviewWatchFaceUseCase.Ai ai) {
                Wg6.c(ai, "errorValue");
                this.a.this$0.o.s();
                Hq4.d(this.a.this$0, false, true, null, 5, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceListViewModel", "preview watch face failed! Error code " + ai.b());
                int b = ai.b();
                if (b == 1101 || b == 1112 || b == 1113) {
                    List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ai.a());
                    Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                    WatchFaceListViewModel watchFaceListViewModel = this.a.this$0;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                    if (array != null) {
                        Uh5[] uh5Arr = (Uh5[]) array;
                        watchFaceListViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
                this.a.this$0.f();
            }

            @DexIgnore
            public void c(PreviewWatchFaceUseCase.Ci ci) {
                Wg6.c(ci, "responseValue");
                FLogger.INSTANCE.getLocal().d("WatchFaceListViewModel", "preview watch face success");
                this.a.this$0.o.s();
                Hq4.d(this.a.this$0, false, true, null, 5, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(PreviewWatchFaceUseCase.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(String str, Xe6 xe6, WatchFaceListViewModel watchFaceListViewModel) {
            super(2, xe6);
            this.$it = str;
            this.this$0 = watchFaceListViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            h hVar = new h(this.$it, xe6, this.this$0);
            hVar.p$ = (Il6) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((h) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Hq4.d(this.this$0, true, false, null, 6, null);
                this.this$0.o.p();
                this.this$0.o.e(new PreviewWatchFaceUseCase.Bi(this.$it, null, null, 6, null), new a(this));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public WatchFaceListViewModel(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, PreviewWatchFaceUseCase previewWatchFaceUseCase) {
        Wg6.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(previewWatchFaceUseCase, "mPreviewWatchFaceUseCase");
        this.l = dianaWatchFaceRepository;
        this.m = fileRepository;
        this.n = dianaPresetRepository;
        this.o = previewWatchFaceUseCase;
        LiveData<List<DianaWatchFaceUser>> c2 = Or0.c(Bw7.b(), 0, new b(this, null), 2, null);
        this.h = c2;
        LiveData<Integer> b2 = Ss0.b(c2, new a());
        Wg6.b(b2, "Transformations.map(this) { transform(it) }");
        this.i = b2;
    }

    @DexIgnore
    public final MutableLiveData<String> A() {
        return this.j;
    }

    @DexIgnore
    public final MutableLiveData<String> B() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Integer> C() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.mapped.Xe6<? super java.lang.Boolean> r7) {
        /*
            r6 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.faces.WatchFaceListViewModel.f
            if (r0 == 0) goto L_0x0054
            r0 = r7
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f r0 = (com.portfolio.platform.watchface.faces.WatchFaceListViewModel.f) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0054
            int r1 = r1 + r4
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0062
            if (r4 != r2) goto L_0x005a
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel r0 = (com.portfolio.platform.watchface.faces.WatchFaceListViewModel) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isReachedMaxPreset total "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "WatchFaceListViewModel"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            r1 = 20
            if (r0 >= r1) goto L_0x007b
            r0 = 0
        L_0x004f:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
        L_0x0053:
            return r0
        L_0x0054:
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f r0 = new com.portfolio.platform.watchface.faces.WatchFaceListViewModel$f
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x005a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0062:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.watchface.faces.WatchFaceListViewModel$g r4 = new com.portfolio.platform.watchface.faces.WatchFaceListViewModel$g
            r5 = 0
            r4.<init>(r6, r5)
            r0.L$0 = r6
            r0.label = r2
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0027
            r0 = r3
            goto L_0x0053
        L_0x007b:
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            r1 = 2131886573(0x7f1201ed, float:1.9407729E38)
            java.lang.String r0 = com.fossil.Um5.c(r0, r1)
            java.lang.String r1 = "LanguageHelper.getString\u2026t__MaximumPresetsCreated)"
            com.mapped.Wg6.b(r0, r1)
            r1 = -1
            r6.a(r1, r0)
            r0 = r2
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.WatchFaceListViewModel.D(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void E(String str) {
        if (str == null || Gu7.d(Us0.a(this), null, null, new h(str, null, this), 3, null) == null) {
            a(-1, "Can not find this watch face");
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void x(String str) {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void y(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceListViewModel", "deleteWatchFaceById id " + str);
        if (str != null) {
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new d(null, this, str), 3, null);
        }
    }

    @DexIgnore
    public final void z(String str, String str2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "token");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceListViewModel", "downloadShareWatchFace - id " + str + " token " + str2);
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                if (DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                    Rm6 unused = Gu7.d(Us0.a(this), null, null, new e(this, str, str2, null), 3, null);
                    return;
                }
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886579);
                Wg6.b(c2, "LanguageHelper.getString\u2026keSureYourHybridHrDevice)");
                Hq4.b(this, 0, c2, 1, null);
            }
        }
    }
}
