package com.portfolio.platform.watchface.faces.page;

import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dc7;
import com.fossil.El7;
import com.fossil.Fb7;
import com.fossil.Hq4;
import com.fossil.Hs0;
import com.fossil.Ka7;
import com.fossil.Ko7;
import com.fossil.Or0;
import com.fossil.S87;
import com.fossil.Ss0;
import com.fossil.Ub7;
import com.fossil.Yn7;
import com.fossil.Z58;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.model.ElementConfigHelper;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MyFaceViewModel extends Hq4 {
    @DexIgnore
    public /* final */ LiveData<List<DianaWatchFaceUser>> h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public List<Fb7> j;
    @DexIgnore
    public List<Fb7> k;
    @DexIgnore
    public List<Fb7> l;
    @DexIgnore
    public /* final */ LiveData<List<Ka7.Bi>> m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository o;
    @DexIgnore
    public /* final */ FileRepository p;
    @DexIgnore
    public /* final */ WFAssetRepository q;
    @DexIgnore
    public /* final */ UserRepository r;
    @DexIgnore
    public /* final */ CustomizeRealDataManager s;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<I, O> implements V3<List<? extends DianaWatchFaceUser>, LiveData<List<Ka7.Bi>>> {
        @DexIgnore
        public /* final */ /* synthetic */ MyFaceViewModel a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Hs0<List<Ka7.Bi>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Fb7>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object b;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        WFAssetRepository wFAssetRepository = this.this$0.this$0.a.q;
                        this.L$0 = il6;
                        this.label = 1;
                        b = wFAssetRepository.b(this);
                        if (b == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        b = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Dc7.c((List) b, null, 1, null);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Fb7>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        WFAssetRepository wFAssetRepository = this.this$0.this$0.a.q;
                        this.L$0 = il6;
                        this.label = 1;
                        g = wFAssetRepository.g(this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Dc7.g((List) g);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Fb7>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Fb7>> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object f;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        WFAssetRepository wFAssetRepository = this.this$0.this$0.a.q;
                        this.L$0 = il6;
                        this.label = 1;
                        f = wFAssetRepository.f(this);
                        if (f == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        f = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Dc7.e((List) f);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Diii extends Ko7 implements Coroutine<Il6, Xe6<? super byte[]>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ DianaWatchFaceUser $it;
                @DexIgnore
                public /* final */ /* synthetic */ List $uiModelList$inlined;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Diii(DianaWatchFaceUser dianaWatchFaceUser, Xe6 xe6, Aii aii, List list) {
                    super(2, xe6);
                    this.$it = dianaWatchFaceUser;
                    this.this$0 = aii;
                    this.$uiModelList$inlined = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Diii diii = new Diii(this.$it, xe6, this.this$0, this.$uiModelList$inlined);
                    diii.p$ = (Il6) obj;
                    throw null;
                    //return diii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super byte[]> xe6) {
                    throw null;
                    //return ((Diii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        File fileByFileName = this.this$0.this$0.a.p.getFileByFileName(this.$it.getId());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("MyFaceViewModel", "process face " + this.$it.getName() + " wfFile " + fileByFileName);
                        if (fileByFileName != null) {
                            return Z58.c(fileByFileName);
                        }
                        return null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Eiii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends List<? extends S87>, ? extends Bitmap>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $uiModelList$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ byte[] $wfByteArray;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public Object L$4;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Eiii(byte[] bArr, Xe6 xe6, Aii aii, List list) {
                    super(2, xe6);
                    this.$wfByteArray = bArr;
                    this.this$0 = aii;
                    this.$uiModelList$inlined = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Eiii eiii = new Eiii(this.$wfByteArray, xe6, this.this$0, this.$uiModelList$inlined);
                    eiii.p$ = (Il6) obj;
                    throw null;
                    //return eiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends List<? extends S87>, ? extends Bitmap>> xe6) {
                    throw null;
                    //return ((Eiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:12:0x0048  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                    /*
                        r8 = this;
                        r7 = 2
                        r3 = 1
                        r4 = 0
                        java.lang.Object r5 = com.fossil.Yn7.d()
                        int r0 = r8.label
                        if (r0 == 0) goto L_0x009b
                        if (r0 == r3) goto L_0x003a
                        if (r0 != r7) goto L_0x0032
                        java.lang.Object r0 = r8.L$4
                        android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
                        java.lang.Object r1 = r8.L$3
                        com.fossil.Ub7 r1 = (com.fossil.Ub7) r1
                        java.lang.Object r1 = r8.L$2
                        com.fossil.Ob7 r1 = (com.fossil.Ob7) r1
                        java.lang.Object r1 = r8.L$1
                        com.fossil.Ob7 r1 = (com.fossil.Ob7) r1
                        java.lang.Object r1 = r8.L$0
                        com.mapped.Il6 r1 = (com.mapped.Il6) r1
                        com.fossil.El7.b(r9)
                        r1 = r9
                        r3 = r0
                    L_0x0028:
                        r0 = r1
                        java.util.List r0 = (java.util.List) r0
                        com.mapped.Lc6 r0 = com.fossil.Hl7.a(r0, r3)
                        if (r0 == 0) goto L_0x00c7
                    L_0x0031:
                        return r0
                    L_0x0032:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x003a:
                        java.lang.Object r0 = r8.L$0
                        com.mapped.Il6 r0 = (com.mapped.Il6) r0
                        com.fossil.El7.b(r9)
                        r2 = r0
                        r1 = r9
                    L_0x0043:
                        r0 = r1
                        com.fossil.Ob7 r0 = (com.fossil.Ob7) r0
                        if (r0 == 0) goto L_0x00c7
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai$Aii r1 = r8.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai r1 = r1.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel r1 = r1.a
                        java.util.List r1 = com.portfolio.platform.watchface.faces.page.MyFaceViewModel.n(r1)
                        if (r1 == 0) goto L_0x00c3
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai$Aii r3 = r8.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai r3 = r3.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel r3 = r3.a
                        java.util.List r3 = com.portfolio.platform.watchface.faces.page.MyFaceViewModel.s(r3)
                        if (r3 == 0) goto L_0x00bf
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai$Aii r6 = r8.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai r6 = r6.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel r6 = r6.a
                        java.util.List r6 = com.portfolio.platform.watchface.faces.page.MyFaceViewModel.o(r6)
                        if (r6 == 0) goto L_0x00bb
                        com.fossil.Ub7 r6 = com.fossil.Cc7.j(r0, r1, r3, r6)
                        com.fossil.Hb7 r1 = r0.b()
                        if (r1 == 0) goto L_0x00b9
                        byte[] r1 = r1.a()
                        if (r1 == 0) goto L_0x00b9
                        android.graphics.Bitmap r1 = com.fossil.J37.c(r1)
                        r3 = r1
                    L_0x0081:
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai$Aii r1 = r8.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai r1 = r1.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel r1 = r1.a
                        r8.L$0 = r2
                        r8.L$1 = r0
                        r8.L$2 = r0
                        r8.L$3 = r6
                        r8.L$4 = r3
                        r8.label = r7
                        java.lang.Object r1 = r1.z(r6, r8)
                        if (r1 != r5) goto L_0x0028
                        r0 = r5
                        goto L_0x0031
                    L_0x009b:
                        com.fossil.El7.b(r9)
                        com.mapped.Il6 r0 = r8.p$
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai$Aii r1 = r8.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel$Ai r1 = r1.this$0
                        com.portfolio.platform.watchface.faces.page.MyFaceViewModel r1 = r1.a
                        com.portfolio.platform.PortfolioApp r1 = com.portfolio.platform.watchface.faces.page.MyFaceViewModel.r(r1)
                        byte[] r2 = r8.$wfByteArray
                        r8.L$0 = r0
                        r8.label = r3
                        java.lang.Object r1 = r1.N0(r2, r8)
                        if (r1 != r5) goto L_0x00d1
                        r0 = r5
                        goto L_0x0031
                    L_0x00b9:
                        r3 = r4
                        goto L_0x0081
                    L_0x00bb:
                        com.mapped.Wg6.i()
                        throw r4
                    L_0x00bf:
                        com.mapped.Wg6.i()
                        throw r4
                    L_0x00c3:
                        com.mapped.Wg6.i()
                        throw r4
                    L_0x00c7:
                        java.util.List r0 = com.fossil.Hm7.e()
                        com.mapped.Lc6 r0 = com.fossil.Hl7.a(r0, r4)
                        goto L_0x0031
                    L_0x00d1:
                        r2 = r0
                        goto L_0x0043
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.page.MyFaceViewModel.Ai.Aii.Eiii.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(List list, Xe6 xe6, Ai ai) {
                super(2, xe6);
                this.$it = list;
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<List<Ka7.Bi>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v18, types: [java.lang.Iterable] */
            /* JADX WARN: Type inference failed for: r3v21, types: [java.lang.Iterable] */
            /* JADX WARN: Type inference failed for: r2v32, types: [java.lang.Iterable] */
            /* JADX WARNING: Removed duplicated region for block: B:11:0x005d  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00b7  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0105  */
            /* JADX WARNING: Removed duplicated region for block: B:33:0x0191  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01d4  */
            /* JADX WARNING: Removed duplicated region for block: B:47:0x0215  */
            /* JADX WARNING: Removed duplicated region for block: B:57:0x0278  */
            /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Unknown variable types count: 3 */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r16) {
                /*
                // Method dump skipped, instructions count: 670
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.page.MyFaceViewModel.Ai.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ai(MyFaceViewModel myFaceViewModel) {
            this.a = myFaceViewModel;
        }

        @DexIgnore
        public final LiveData<List<Ka7.Bi>> a(List<? extends DianaWatchFaceUser> list) {
            FLogger.INSTANCE.getLocal().d("MyFaceViewModel", "face changes");
            return Or0.c(null, 0, new Aii(list, null, this), 3, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ LiveData<List<Ka7.Bi>> apply(List<? extends DianaWatchFaceUser> list) {
            return a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.faces.page.MyFaceViewModel$_watchFaceListLiveData$1", f = "MyFaceViewModel.kt", l = {41, 41}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Hs0<List<? extends DianaWatchFaceUser>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MyFaceViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(MyFaceViewModel myFaceViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = myFaceViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Hs0) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Hs0<List<? extends DianaWatchFaceUser>> hs0, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(hs0, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x003c
                if (r0 == r2) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                com.fossil.El7.b(r7)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$1
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                com.fossil.El7.b(r7)
                r3 = r0
                r2 = r7
            L_0x002d:
                r0 = r2
                androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.a(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x0017
            L_0x003c:
                com.fossil.El7.b(r7)
                com.fossil.Hs0 r0 = r6.p$
                com.portfolio.platform.watchface.faces.page.MyFaceViewModel r1 = r6.this$0
                com.portfolio.platform.data.source.DianaWatchFaceRepository r1 = com.portfolio.platform.watchface.faces.page.MyFaceViewModel.p(r1)
                r6.L$0 = r0
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r1.getAllDianaWatchFaceUserLiveData(r6)
                if (r2 != r4) goto L_0x0055
                r0 = r4
                goto L_0x0017
            L_0x0055:
                r1 = r0
                r3 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.faces.page.MyFaceViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public MyFaceViewModel(PortfolioApp portfolioApp, DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository, WFAssetRepository wFAssetRepository, UserRepository userRepository, CustomizeRealDataManager customizeRealDataManager, CustomizeRealDataRepository customizeRealDataRepository) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(customizeRealDataManager, "customizeRealDataManager");
        Wg6.c(customizeRealDataRepository, "customizeRealDataRepository");
        this.n = portfolioApp;
        this.o = dianaWatchFaceRepository;
        this.p = fileRepository;
        this.q = wFAssetRepository;
        this.r = userRepository;
        this.s = customizeRealDataManager;
        this.t = customizeRealDataRepository;
        LiveData<List<DianaWatchFaceUser>> c = Or0.c(Bw7.b(), 0, new Bi(this, null), 2, null);
        this.h = c;
        LiveData<List<Ka7.Bi>> c2 = Ss0.c(c, new Ai(this));
        Wg6.b(c2, "Transformations.switchMap(this) { transform(it) }");
        this.m = c2;
    }

    @DexIgnore
    public final LiveData<List<Ka7.Bi>> A() {
        return this.m;
    }

    @DexIgnore
    public final /* synthetic */ Object z(Ub7 ub7, Xe6<? super List<? extends S87>> xe6) {
        return ElementConfigHelper.a.b(ub7, this.q, this.t, this.r, this.s, xe6);
    }
}
