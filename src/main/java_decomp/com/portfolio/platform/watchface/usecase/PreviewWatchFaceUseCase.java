package com.portfolio.platform.watchface.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.io.File;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreviewWatchFaceUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Di e; // = new Di();
    @DexIgnore
    public /* final */ DianaWatchFaceRepository f;
    @DexIgnore
    public /* final */ FileRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public Ai(int i, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Bi(String str, String str2, String str3) {
            Wg6.c(str, "watchFaceId");
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Bi(String str, String str2, String str3, int i, Qg6 qg6) {
            this(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PreviewWatchFaceUseCase", "onReceive - isExecuted " + PreviewWatchFaceUseCase.this.o() + " communicateMode=" + communicateMode);
            if (PreviewWatchFaceUseCase.this.o() && communicateMode == CommunicateMode.PREVIEW_THEME_SESSION) {
                PreviewWatchFaceUseCase.this.r(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d("PreviewWatchFaceUseCase", "onReceive - success");
                    PreviewWatchFaceUseCase.this.j(new Ci());
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("PreviewWatchFaceUseCase", "onReceive - failed erorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra);
                PreviewWatchFaceUseCase.this.i(new Ai(intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Xe6 xe6, PreviewWatchFaceUseCase previewWatchFaceUseCase, Bi bi, Xe6 xe62) {
            super(2, xe6);
            this.this$0 = previewWatchFaceUseCase;
            this.$requestValues$inlined = bi;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(xe6, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super File> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.g.getFileByRemoteUrl(this.$requestValues$inlined.b());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super DianaWatchFaceUser>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Xe6 xe6, PreviewWatchFaceUseCase previewWatchFaceUseCase, Bi bi, Xe6 xe62) {
            super(2, xe6);
            this.this$0 = previewWatchFaceUseCase;
            this.$requestValues$inlined = bi;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(xe6, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super DianaWatchFaceUser> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.f;
                String c = this.$requestValues$inlined.c();
                this.L$0 = il6;
                this.label = 1;
                Object dianaWatchFaceUserById = dianaWatchFaceRepository.getDianaWatchFaceUserById(c, this);
                return dianaWatchFaceUserById == d ? d : dianaWatchFaceUserById;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $watchFace;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(Jh6 jh6, Xe6 xe6, PreviewWatchFaceUseCase previewWatchFaceUseCase, Bi bi, Xe6 xe62) {
            super(2, xe6);
            this.$watchFace = jh6;
            this.this$0 = previewWatchFaceUseCase;
            this.$requestValues$inlined = bi;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.$watchFace, xe6, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super File> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.g.getFileByFileName(this.$watchFace.element.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $watchFace;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(Jh6 jh6, Xe6 xe6, PreviewWatchFaceUseCase previewWatchFaceUseCase, Bi bi, Xe6 xe62) {
            super(2, xe6);
            this.$watchFace = jh6;
            this.this$0 = previewWatchFaceUseCase;
            this.$requestValues$inlined = bi;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.$watchFace, xe6, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FileRepository fileRepository = this.this$0.g;
                String downloadURL = this.$watchFace.element.getDownloadURL();
                String id = this.$watchFace.element.getId();
                FileType fileType = FileType.WATCH_FACE;
                String checksum = this.$watchFace.element.getChecksum();
                this.L$0 = il6;
                this.label = 1;
                Object downloadAndSaveWithFileName = fileRepository.downloadAndSaveWithFileName(downloadURL, id, fileType, checksum, this);
                return downloadAndSaveWithFileName == d ? d : downloadAndSaveWithFileName;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $watchFace;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(Jh6 jh6, Xe6 xe6, PreviewWatchFaceUseCase previewWatchFaceUseCase, Bi bi, Xe6 xe62) {
            super(2, xe6);
            this.$watchFace = jh6;
            this.this$0 = previewWatchFaceUseCase;
            this.$requestValues$inlined = bi;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.$watchFace, xe6, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super File> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.g.getFileByFileName(this.$watchFace.element.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase", f = "PreviewWatchFaceUseCase.kt", l = {54, 55, 58, 63, 66, 69, 72, 81}, m = "run")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PreviewWatchFaceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(PreviewWatchFaceUseCase previewWatchFaceUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = previewWatchFaceUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore
    public PreviewWatchFaceUseCase(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository) {
        Wg6.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        Wg6.c(fileRepository, "mFileRepository");
        this.f = dianaWatchFaceRepository;
        this.g = fileRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "PreviewWatchFaceUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return q(bi, xe6);
    }

    @DexIgnore
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    public final void p() {
        BleCommandResultManager.d.e(this.e, CommunicateMode.PREVIEW_THEME_SESSION);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        if (r1 != null) goto L_0x0049;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0224  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object q(com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase.Bi r13, com.mapped.Xe6<java.lang.Object> r14) {
        /*
        // Method dump skipped, instructions count: 796
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase.q(com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void r(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void s() {
        BleCommandResultManager.d.j(this.e, CommunicateMode.PREVIEW_THEME_SESSION);
    }
}
