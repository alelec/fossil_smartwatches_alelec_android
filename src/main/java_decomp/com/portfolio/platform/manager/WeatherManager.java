package com.portfolio.platform.manager;

import android.location.Location;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.U08;
import com.fossil.Ul5;
import com.fossil.W08;
import com.fossil.Yn7;
import com.fossil.Zq7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherInfoWatchAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.helper.AnalyticsHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherManager {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static WeatherManager l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public CustomizeRealDataRepository e;
    @DexIgnore
    public DianaAppSettingRepository f;
    @DexIgnore
    public GoogleApiService g;
    @DexIgnore
    public String h;
    @DexIgnore
    public Weather i;
    @DexIgnore
    public U08 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WeatherManager a() {
            WeatherManager weatherManager;
            synchronized (this) {
                if (WeatherManager.l == null) {
                    WeatherManager.l = new WeatherManager(null);
                }
                weatherManager = WeatherManager.l;
                if (weatherManager == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return weatherManager;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {259}, m = "getAddressBaseOnLocation")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WeatherManager weatherManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(0.0d, 0.0d, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1", f = "WeatherManager.kt", l = {259}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WeatherManager weatherManager, double d, double d2, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = weatherManager;
            this.$lat = d;
            this.$lng = d2;
            this.$type = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ci(this.this$0, this.$lat, this.$lng, this.$type, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ci) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: double : 0x0025: IGET  (r4v0 double) = (r7v0 'this' com.portfolio.platform.manager.WeatherManager$Ci A[IMMUTABLE_TYPE, THIS]) com.portfolio.platform.manager.WeatherManager.Ci.$lat double), (',' char), (wrap: double : 0x002f: IGET  (r4v1 double) = (r7v0 'this' com.portfolio.platform.manager.WeatherManager$Ci A[IMMUTABLE_TYPE, THIS]) com.portfolio.platform.manager.WeatherManager.Ci.$lng double)] */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                GoogleApiService m = this.this$0.m();
                StringBuilder sb = new StringBuilder();
                sb.append(this.$lat);
                sb.append(',');
                sb.append(this.$lng);
                String sb2 = sb.toString();
                String str = this.$type;
                this.label = 1;
                Object addressWithType = m.getAddressWithType(sb2, str, this);
                return addressWithType == d ? d : addressWithType;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {287}, m = "getWeather")
    public static final class Di extends Jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WeatherManager weatherManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(0.0d, 0.0d, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeather$repoResponse$1", f = "WeatherManager.kt", l = {287}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WeatherManager weatherManager, double d, double d2, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = weatherManager;
            this.$lat = d;
            this.$lng = d2;
            this.$tempUnit = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ei(this.this$0, this.$lat, this.$lng, this.$tempUnit, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ei) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 j = this.this$0.j();
                double d2 = this.$lat;
                double d3 = this.$lng;
                String str = this.$tempUnit;
                this.label = 1;
                Object weather = j.getWeather(String.valueOf(d2), String.valueOf(d3), str, this);
                return weather == d ? d : weather;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $currentLocation;
        @DexIgnore
        public /* final */ /* synthetic */ Zq7 $isFromCaches;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit$inlined;
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, double d, double d2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$currentLat, this.$currentLong, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Fi fi = this.this$0;
                    WeatherManager weatherManager = fi.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    String str = fi.$tempUnit$inlined;
                    this.L$0 = il6;
                    this.label = 1;
                    Object p = weatherManager.p(d2, d3, str, this);
                    return p == d ? d : p;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Fi fi, double d, double d2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$currentLat, this.$currentLong, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WeatherManager weatherManager = this.this$0.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    this.L$0 = il6;
                    this.label = 1;
                    Object i2 = weatherManager.i(d2, d3, this);
                    return i2 == d ? d : i2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Fi fi, double d, double d2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$currentLat, this.$currentLong, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WeatherManager weatherManager = this.this$0.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    this.L$0 = il6;
                    this.label = 1;
                    Object i2 = weatherManager.i(d2, d3, this);
                    return i2 == d ? d : i2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Location location, Zq7 zq7, Xe6 xe6, WeatherManager weatherManager, Xe6 xe62, String str) {
            super(2, xe6);
            this.$currentLocation = location;
            this.$isFromCaches = zq7;
            this.this$0 = weatherManager;
            this.$continuation$inlined = xe62;
            this.$tempUnit$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.$currentLocation, this.$isFromCaches, xe6, this.this$0, this.$continuation$inlined, this.$tempUnit$inlined);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x006a  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x019b  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 457
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {374, 196, 201}, m = "getWeatherBaseOnLocation")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(WeatherManager weatherManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForChanceOfRain$2", f = "WeatherManager.kt", l = {156, 162, 166}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(String str, Xe6 xe6, Hi hi) {
                super(2, xe6);
                this.$tempUnit = str;
                this.this$0 = hi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$tempUnit, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WeatherManager weatherManager = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = il6;
                    this.label = 1;
                    Object q = weatherManager.q("chance-of-rain", str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(WeatherManager weatherManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$serial, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 264
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.Hi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$2", f = "WeatherManager.kt", l = {175, 181, 185}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(String str, Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.$tempUnit = str;
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$tempUnit, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WeatherManager weatherManager = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = il6;
                    this.label = 1;
                    Object q = weatherManager.q("weather", str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(WeatherManager weatherManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$serial, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 264
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.Ii.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$2", f = "WeatherManager.kt", l = {91, 92, 129, 137, 141}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WeatherLocationWrapper $location;
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public /* final */ /* synthetic */ Il6 $this_withContext$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ji this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(WeatherLocationWrapper weatherLocationWrapper, String str, Xe6 xe6, MFUser mFUser, Ji ji, Il6 il6) {
                super(2, xe6);
                this.$location = weatherLocationWrapper;
                this.$tempUnit = str;
                this.$user$inlined = mFUser;
                this.this$0 = ji;
                this.$this_withContext$inlined = il6;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$location, this.$tempUnit, xe6, this.$user$inlined, this.this$0, this.$this_withContext$inlined);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Weather, ? extends Boolean>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object p;
                Object q;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WeatherLocationWrapper weatherLocationWrapper = this.$location;
                    Boolean a2 = weatherLocationWrapper != null ? Ao7.a(weatherLocationWrapper.isUseCurrentLocation()) : null;
                    if (a2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (a2.booleanValue()) {
                        WeatherManager weatherManager = this.this$0.this$0;
                        String str = this.$tempUnit;
                        this.L$0 = il6;
                        this.label = 1;
                        q = weatherManager.q("weather", str, this);
                        if (q == d) {
                            return d;
                        }
                        return (Lc6) q;
                    } else {
                        WeatherManager weatherManager2 = this.this$0.this$0;
                        double lat = this.$location.getLat();
                        double lng = this.$location.getLng();
                        String str2 = this.$tempUnit;
                        this.L$0 = il6;
                        this.label = 2;
                        p = weatherManager2.p(lat, lng, str2, this);
                        if (p == d) {
                            return d;
                        }
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    q = obj;
                    return (Lc6) q;
                } else if (i == 2) {
                    Il6 il63 = (Il6) this.L$0;
                    El7.b(obj);
                    p = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (Lc6) p;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(WeatherManager weatherManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, this.$serial, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v47, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r4v49, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r4v51, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:112:0x0530  */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x0533  */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x054e  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00bd  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0145  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01e6  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x01f0  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0206  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x025c  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x0341  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r30) {
            /*
            // Method dump skipped, instructions count: 1368
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.Ji.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {330}, m = "showChanceOfRain")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(WeatherManager weatherManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$showChanceOfRain$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(WeatherManager weatherManager, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Li li = new Li(this.this$0, this.$rainProbabilityPercent, xe6);
            li.p$ = (Il6) obj;
            throw null;
            //return li;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Li) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.k().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {316}, m = "showTemperature")
    public static final class Mi extends Jf6 {
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(WeatherManager weatherManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.WeatherManager$showTemperature$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class Ni extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $tempInCelsius;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(WeatherManager weatherManager, float f, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$tempInCelsius = f;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ni ni = new Ni(this.this$0, this.$tempInCelsius, xe6);
            ni.p$ = (Il6) obj;
            throw null;
            //return ni;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ni) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.k().upsertCustomizeRealData(new CustomizeRealData("temperature", String.valueOf(this.$tempInCelsius)));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = WeatherManager.class.getSimpleName();
        Wg6.b(simpleName, "WeatherManager::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public WeatherManager() {
        this.j = W08.b(false, 1, null);
        PortfolioApp.get.instance().getIface().I1(this);
    }

    @DexIgnore
    public /* synthetic */ WeatherManager(Qg6 qg6) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(double r14, double r16, com.mapped.Xe6<? super java.lang.String> r18) {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.i(double, double, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final ApiServiceV2 j() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        Wg6.n("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository k() {
        CustomizeRealDataRepository customizeRealDataRepository = this.e;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        Wg6.n("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final DianaAppSettingRepository l() {
        DianaAppSettingRepository dianaAppSettingRepository = this.f;
        if (dianaAppSettingRepository != null) {
            return dianaAppSettingRepository;
        }
        Wg6.n("mDianaAppSettingRepository");
        throw null;
    }

    @DexIgnore
    public final GoogleApiService m() {
        GoogleApiService googleApiService = this.g;
        if (googleApiService != null) {
            return googleApiService;
        }
        Wg6.n("mGoogleApiService");
        throw null;
    }

    @DexIgnore
    public final LocationSource n() {
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            return locationSource;
        }
        Wg6.n("mLocationSource");
        throw null;
    }

    @DexIgnore
    public final UserRepository o() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(double r14, double r16, java.lang.String r18, com.mapped.Xe6<? super com.mapped.Lc6<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r19) {
        /*
            r13 = this;
            r0 = r19
            boolean r2 = r0 instanceof com.portfolio.platform.manager.WeatherManager.Di
            if (r2 == 0) goto L_0x007b
            r2 = r19
            com.portfolio.platform.manager.WeatherManager$Di r2 = (com.portfolio.platform.manager.WeatherManager.Di) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r4 & r3
            if (r4 == 0) goto L_0x007b
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = r3 + r4
            r2.label = r3
            r10 = r2
        L_0x0017:
            java.lang.Object r3 = r10.result
            java.lang.Object r11 = com.fossil.Yn7.d()
            int r2 = r10.label
            if (r2 == 0) goto L_0x008c
            r4 = 1
            if (r2 != r4) goto L_0x0084
            java.lang.Object r2 = r10.L$1
            java.lang.String r2 = (java.lang.String) r2
            double r4 = r10.D$1
            double r4 = r10.D$0
            java.lang.Object r2 = r10.L$0
            com.portfolio.platform.manager.WeatherManager r2 = (com.portfolio.platform.manager.WeatherManager) r2
            com.fossil.El7.b(r3)
            r2 = r3
        L_0x0034:
            com.mapped.Ap4 r2 = (com.mapped.Ap4) r2
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.manager.WeatherManager.k
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "getWeather onResponse: response = "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            boolean r3 = r2 instanceof com.fossil.Kq5
            if (r3 == 0) goto L_0x00be
            com.fossil.Kq5 r2 = (com.fossil.Kq5) r2
            java.lang.Object r3 = r2.a()
            if (r3 == 0) goto L_0x00b2
            com.fossil.Oq5 r3 = new com.fossil.Oq5
            r3.<init>()
            java.lang.Object r2 = r2.a()
            com.mapped.Ku3 r2 = (com.mapped.Ku3) r2
            r3.b(r2)
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            com.portfolio.platform.data.model.microapp.weather.Weather r3 = r3.a()
            r4 = 0
            java.lang.Boolean r4 = com.fossil.Ao7.a(r4)
            r2.<init>(r3, r4)
        L_0x007a:
            return r2
        L_0x007b:
            com.portfolio.platform.manager.WeatherManager$Di r2 = new com.portfolio.platform.manager.WeatherManager$Di
            r0 = r19
            r2.<init>(r13, r0)
            r10 = r2
            goto L_0x0017
        L_0x0084:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "call to 'resume' before 'invoke' with coroutine"
            r2.<init>(r3)
            throw r2
        L_0x008c:
            com.fossil.El7.b(r3)
            com.portfolio.platform.manager.WeatherManager$Ei r2 = new com.portfolio.platform.manager.WeatherManager$Ei
            r9 = 0
            r3 = r13
            r4 = r14
            r6 = r16
            r8 = r18
            r2.<init>(r3, r4, r6, r8, r9)
            r10.L$0 = r13
            r10.D$0 = r14
            r0 = r16
            r10.D$1 = r0
            r0 = r18
            r10.L$1 = r0
            r3 = 1
            r10.label = r3
            java.lang.Object r2 = com.portfolio.platform.response.ResponseKt.d(r2, r10)
            if (r2 != r11) goto L_0x0034
            r2 = r11
            goto L_0x007a
        L_0x00b2:
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            r3 = 0
            r4 = 0
            java.lang.Boolean r4 = com.fossil.Ao7.a(r4)
            r2.<init>(r3, r4)
            goto L_0x007a
        L_0x00be:
            boolean r2 = r2 instanceof com.fossil.Hq5
            if (r2 == 0) goto L_0x00ce
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            r3 = 0
            r4 = 0
            java.lang.Boolean r4 = com.fossil.Ao7.a(r4)
            r2.<init>(r3, r4)
            goto L_0x007a
        L_0x00ce:
            com.mapped.Kc6 r2 = new com.mapped.Kc6
            r2.<init>()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.p(double, double, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[Catch:{ all -> 0x01ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f2 A[SYNTHETIC, Splitter:B:44:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0158 A[Catch:{ all -> 0x01d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(java.lang.String r17, java.lang.String r18, com.mapped.Xe6<? super com.mapped.Lc6<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r19) {
        /*
        // Method dump skipped, instructions count: 501
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.q(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object r(String str, Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.a(), new Hi(this, str, null), xe6);
    }

    @DexIgnore
    public final Object s(String str, Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.a(), new Ii(this, str, null), xe6);
    }

    @DexIgnore
    public final Object t(String str, Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.b(), new Ji(this, str, null), xe6);
    }

    @DexIgnore
    public final boolean u(AddressOfWeather addressOfWeather, double d2, double d3) {
        Location location = new Location("LocationA");
        location.setLatitude(addressOfWeather.getLat());
        location.setLongitude(addressOfWeather.getLng());
        Location location2 = new Location("LocationB");
        location2.setLatitude(d2);
        location2.setLongitude(d3);
        float distanceTo = location.distanceTo(location2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "distance between two address=" + distanceTo);
        return distanceTo < ((float) VideoUploader.RETRY_DELAY_UNIT_MS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object v(com.portfolio.platform.data.model.microapp.weather.Weather r9, boolean r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
        /*
            r8 = this;
            r7 = 0
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.portfolio.platform.manager.WeatherManager.Ki
            if (r0 == 0) goto L_0x0051
            r0 = r11
            com.portfolio.platform.manager.WeatherManager$Ki r0 = (com.portfolio.platform.manager.WeatherManager.Ki) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0051
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0060
            if (r3 != r6) goto L_0x0058
            java.lang.Object r0 = r1.L$2
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r0 = (com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo) r0
            int r0 = r1.I$0
            boolean r10 = r1.Z$0
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.manager.WeatherManager r0 = (com.portfolio.platform.manager.WeatherManager) r0
            com.fossil.El7.b(r2)
        L_0x0034:
            com.portfolio.platform.helper.AnalyticsHelper$Ai r1 = com.portfolio.platform.helper.AnalyticsHelper.f
            java.lang.String r2 = "chance-of-rain"
            com.fossil.Ul5 r1 = r1.f(r2)
            if (r1 == 0) goto L_0x0047
            java.lang.String r0 = r0.h
            if (r0 == 0) goto L_0x00c3
            java.lang.String r2 = ""
            r1.d(r0, r10, r2)
        L_0x0047:
            com.portfolio.platform.helper.AnalyticsHelper$Ai r0 = com.portfolio.platform.helper.AnalyticsHelper.f
            java.lang.String r1 = "chance-of-rain"
            r0.k(r1)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0050:
            return r0
        L_0x0051:
            com.portfolio.platform.manager.WeatherManager$Ki r0 = new com.portfolio.platform.manager.WeatherManager$Ki
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0060:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.manager.WeatherManager.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "showChanceOfRain - probability="
            r4.append(r5)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r5 = r9.getCurrently()
            float r5 = r5.getRainProbability()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r2 = r9.getCurrently()
            float r2 = r2.getRainProbability()
            r3 = 100
            float r3 = (float) r3
            float r2 = r2 * r3
            int r2 = (int) r2
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r3 = r9.toChanceOfRainComplicationAppInfo()
            java.lang.String r4 = r8.h
            if (r4 == 0) goto L_0x00a5
            com.portfolio.platform.PortfolioApp$inner r5 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r5 = r5.instance()
            r5.g1(r3, r4)
        L_0x00a5:
            com.fossil.Dv7 r4 = com.fossil.Bw7.a()
            com.portfolio.platform.manager.WeatherManager$Li r5 = new com.portfolio.platform.manager.WeatherManager$Li
            r5.<init>(r8, r2, r7)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.Z$0 = r10
            r1.I$0 = r2
            r1.L$2 = r3
            r1.label = r6
            java.lang.Object r1 = com.fossil.Eu7.g(r4, r5, r1)
            if (r1 == r0) goto L_0x0050
            r0 = r8
            goto L_0x0034
        L_0x00c3:
            com.mapped.Wg6.i()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.v(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(com.portfolio.platform.data.model.microapp.weather.Weather r9, boolean r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.WeatherManager.w(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void x(Lc6<Weather, Boolean> lc6, Lc6<Weather, Boolean> lc62, Lc6<Weather, Boolean> lc63) {
        Weather first;
        Weather first2;
        Weather first3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "showWeatherWatchApp - firstWeather=" + lc6 + ", secondWeather=" + lc62 + ", thirdWeather=" + lc63);
        WeatherWatchAppInfo weatherWatchAppInfo = (lc6 == null || (first3 = lc6.getFirst()) == null) ? null : first3.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo2 = (lc62 == null || (first2 = lc62.getFirst()) == null) ? null : first2.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo3 = (lc63 == null || (first = lc63.getFirst()) == null) ? null : first.toWeatherWatchAppInfo();
        if (weatherWatchAppInfo == null) {
            return;
        }
        if (weatherWatchAppInfo != null) {
            PortfolioApp.get.instance().g1(new WeatherInfoWatchAppResponse(weatherWatchAppInfo, weatherWatchAppInfo2, weatherWatchAppInfo3), PortfolioApp.get.instance().J());
            Ul5 f2 = AnalyticsHelper.f.f("weather");
            if (f2 != null) {
                String str2 = this.h;
                if (str2 != null) {
                    Boolean second = lc6 != null ? lc6.getSecond() : null;
                    if (second != null) {
                        f2.d(str2, second.booleanValue(), "");
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            AnalyticsHelper.f.k("weather");
            return;
        }
        Wg6.i();
        throw null;
    }
}
