package com.portfolio.platform.manager;

import android.graphics.Typeface;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Ej0;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeManager {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static ThemeManager k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public /* final */ String a; // = "theme/fonts/";
    @DexIgnore
    public /* final */ String b; // = "theme/fonts/ginger/";
    @DexIgnore
    public /* final */ String c; // = "theme/fonts/sf/";
    @DexIgnore
    public /* final */ String d; // = ".otf";
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Style> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ Ej0<String, String> f; // = new Ej0<>(10);
    @DexIgnore
    public /* final */ Ej0<String, Typeface> g; // = new Ej0<>(10);
    @DexIgnore
    public /* final */ ArrayList<String> h; // = Hm7.c(this.a, this.b, this.c);
    @DexIgnore
    public ThemeRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ThemeManager a() {
            ThemeManager themeManager;
            synchronized (this) {
                if (ThemeManager.k == null) {
                    ThemeManager.k = new ThemeManager();
                }
                themeManager = ThemeManager.k;
                if (themeManager == null) {
                    Wg6.i();
                    throw null;
                }
            }
            return themeManager;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.ThemeManager$addNewTheme$1", f = "ThemeManager.kt", l = {128, 132}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ThemeManager themeManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = themeManager;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x006e  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0055
                if (r0 == r2) goto L_0x0024
                if (r0 != r4) goto L_0x001c
                java.lang.Object r0 = r5.L$1
                com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                java.lang.Object r0 = r5.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r6)
            L_0x0019:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r5.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r6)
                r2 = r0
                r1 = r6
            L_0x002d:
                r0 = r1
                com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                if (r0 == 0) goto L_0x006e
                java.lang.String r1 = r5.$id
                r0.setId(r1)
                java.lang.String r1 = "New theme"
                r0.setName(r1)
                java.lang.String r1 = "modified"
                r0.setType(r1)
                com.portfolio.platform.manager.ThemeManager r1 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.h()
                r5.L$0 = r2
                r5.L$1 = r0
                r5.label = r4
                java.lang.Object r0 = r1.upsertUserTheme(r0, r5)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0055:
                com.fossil.El7.b(r6)
                com.mapped.Il6 r0 = r5.p$
                com.portfolio.platform.manager.ThemeManager r1 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.h()
                r5.L$0 = r0
                r5.label = r2
                java.lang.String r2 = "default"
                java.lang.Object r1 = r1.getThemeById(r2, r5)
                if (r1 != r3) goto L_0x0073
                r0 = r3
                goto L_0x001b
            L_0x006e:
                com.mapped.Wg6.i()
                r0 = 0
                throw r0
            L_0x0073:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.ThemeManager.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.ThemeManager", f = "ThemeManager.kt", l = {38}, m = "reloadStyleCache")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ThemeManager themeManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = themeManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    /*
    static {
        String simpleName = ThemeManager.class.getSimpleName();
        Wg6.b(simpleName, "ThemeManager::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public ThemeManager() {
        PortfolioApp.get.instance().getIface().O1(this);
    }

    @DexIgnore
    public final void c(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "addNewTheme id=" + str);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, null), 3, null);
    }

    @DexIgnore
    public final String d(String str) {
        T t;
        Wg6.c(str, "themeStyle");
        String d2 = this.f.d(str);
        if (d2 == null) {
            Iterator<T> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.getKey(), str)) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            d2 = t2 != null ? t2.getValue() : null;
            if (!TextUtils.isEmpty(d2)) {
                Ej0<String, String> ej0 = this.f;
                if (d2 != null) {
                    ej0.f(str, d2);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        return d2;
    }

    @DexIgnore
    public final String e(String str, List<Style> list) {
        T t;
        Wg6.c(str, "themeStyle");
        Wg6.c(list, "predefineStyles");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return t2.getValue();
        }
        return null;
    }

    @DexIgnore
    public final Typeface f(String str) {
        T t;
        Typeface typeface;
        Wg6.c(str, "themeStyle");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        Ej0<String, Typeface> ej0 = this.g;
        if (value != null) {
            Typeface d2 = ej0.d(value);
            if (d2 == null) {
                Iterator<T> it2 = this.h.iterator();
                while (true) {
                    typeface = d2;
                    if (!it2.hasNext()) {
                        break;
                    }
                    try {
                        Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.get.instance().getAssets(), ((String) it2.next()) + value + this.d);
                        Ej0<String, Typeface> ej02 = this.g;
                        if (createFromAsset != null) {
                            ej02.f(value, createFromAsset);
                            return createFromAsset;
                        }
                        Wg6.i();
                        throw null;
                    } catch (Exception e2) {
                        d2 = typeface;
                        FLogger.INSTANCE.getLocal().d(j, "font file is not exists");
                    }
                }
            } else {
                typeface = d2;
            }
            return typeface;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Typeface g(String str, List<Style> list) {
        T t;
        Wg6.c(str, "themeStyle");
        Wg6.c(list, "predefineStyles");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        Iterator<T> it2 = this.h.iterator();
        Typeface typeface = null;
        while (it2.hasNext()) {
            try {
                Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.get.instance().getAssets(), ((String) it2.next()) + value + this.d);
                Ej0<String, Typeface> ej0 = this.g;
                if (createFromAsset != null) {
                    ej0.f(str, createFromAsset);
                    return createFromAsset;
                }
                Wg6.i();
                throw null;
            } catch (Exception e2) {
                typeface = typeface;
            }
        }
        return typeface;
    }

    @DexIgnore
    public final ThemeRepository h() {
        ThemeRepository themeRepository = this.i;
        if (themeRepository != null) {
            return themeRepository;
        }
        Wg6.n("mThemeRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r2 = 0
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.ThemeManager.Ci
            if (r0 == 0) goto L_0x005d
            r0 = r7
            com.portfolio.platform.manager.ThemeManager$Ci r0 = (com.portfolio.platform.manager.ThemeManager.Ci) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x005d
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x006b
            if (r4 != r5) goto L_0x0063
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.ThemeManager r0 = (com.portfolio.platform.manager.ThemeManager) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.manager.ThemeManager.j
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "reload style cache currentId "
            r5.append(r1)
            if (r0 == 0) goto L_0x0099
            java.lang.String r1 = r0.getId()
        L_0x0043:
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r3.d(r4, r1)
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r1 = r6.e
            if (r0 == 0) goto L_0x008d
            java.util.ArrayList r0 = r0.getStyles()
            if (r0 == 0) goto L_0x008d
        L_0x0057:
            r1.addAll(r0)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x005c:
            return r0
        L_0x005d:
            com.portfolio.platform.manager.ThemeManager$Ci r0 = new com.portfolio.platform.manager.ThemeManager$Ci
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0063:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006b:
            com.fossil.El7.b(r1)
            com.fossil.Ej0<java.lang.String, java.lang.String> r1 = r6.f
            r1.c()
            com.fossil.Ej0<java.lang.String, android.graphics.Typeface> r1 = r6.g
            r1.c()
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r1 = r6.e
            r1.clear()
            com.portfolio.platform.data.source.ThemeRepository r1 = r6.i
            if (r1 == 0) goto L_0x0093
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.getCurrentTheme(r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x005c
        L_0x008d:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x0057
        L_0x0093:
            java.lang.String r0 = "mThemeRepository"
            com.mapped.Wg6.n(r0)
            throw r2
        L_0x0099:
            r1 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.ThemeManager.i(com.mapped.Xe6):java.lang.Object");
    }
}
