package com.portfolio.platform.manager;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataManager {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ PortfolioApp b;
    @DexIgnore
    public /* final */ DeviceRepository c;
    @DexIgnore
    public /* final */ DianaAppSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {93, 121}, m = "getComplicationRealData")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRealDataManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(CustomizeRealDataManager customizeRealDataManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = customizeRealDataManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {154}, m = "getComplicationRealData")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRealDataManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeRealDataManager customizeRealDataManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = customizeRealDataManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.CustomizeRealDataManager$getComplicationRealData$deviceInDb$1", f = "CustomizeRealDataManager.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRealDataManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(CustomizeRealDataManager customizeRealDataManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeRealDataManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.c.getDeviceBySerial(this.this$0.b.J());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.CustomizeRealDataManager$getComplicationRealData$secondTzComplication$2", f = "CustomizeRealDataManager.kt", l = {94}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super DianaAppSetting>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRealDataManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(CustomizeRealDataManager customizeRealDataManager, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeRealDataManager;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$complicationId, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super DianaAppSetting> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaAppSettingRepository dianaAppSettingRepository = this.this$0.d;
                String str = this.$complicationId;
                this.L$0 = il6;
                this.label = 1;
                Object dianaAppSetting = dianaAppSettingRepository.getDianaAppSetting("complication", str, this);
                return dianaAppSetting == d ? d : dianaAppSetting;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public CustomizeRealDataManager(PortfolioApp portfolioApp, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.b = portfolioApp;
        this.c = deviceRepository;
        this.d = dianaAppSettingRepository;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v11 double), ('h' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (r2v11 double), ('h' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084 A[Catch:{ Exception -> 0x0121 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.portfolio.platform.data.model.MFUser r9, java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r10, java.lang.String r11, com.mapped.Xe6<? super java.lang.String> r12) {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.CustomizeRealDataManager.d(com.portfolio.platform.data.model.MFUser, java.util.List, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r0v92 int), ('%' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0103: INVOKE  (r0v69 int) = (r0v68 float) type: STATIC call: com.fossil.Lr7.b(float):int), (8451 char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x014d: INVOKE  (r1v22 int) = (wrap: float : 0x0149: INVOKE  (r1v21 float) = (r4v8 float) type: STATIC call: com.fossil.Jk5.a(float):float) type: STATIC call: com.fossil.Lr7.b(float):int), (8457 char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0270: INVOKE  (r1v9 int) = (r1v7 com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile.getBatteryLevel():int), ('%' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object e(java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r10, java.lang.String r11, com.portfolio.platform.data.model.MFUser r12, com.mapped.Xe6<? super java.lang.String> r13) {
        /*
        // Method dump skipped, instructions count: 728
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.CustomizeRealDataManager.e(java.util.List, java.lang.String, com.portfolio.platform.data.model.MFUser, com.mapped.Xe6):java.lang.Object");
    }
}
