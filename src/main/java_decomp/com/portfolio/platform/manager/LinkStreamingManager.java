package com.portfolio.platform.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.Ao1;
import com.fossil.Ao7;
import com.fossil.Bj5;
import com.fossil.Bo1;
import com.fossil.Bw7;
import com.fossil.Dj5;
import com.fossil.El7;
import com.fossil.Eo1;
import com.fossil.Fo1;
import com.fossil.Go1;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kr5;
import com.fossil.Ku1;
import com.fossil.Ln5;
import com.fossil.Mr5;
import com.fossil.Op1;
import com.fossil.Pn5;
import com.fossil.Qp1;
import com.fossil.Tc7;
import com.fossil.Ux7;
import com.fossil.Vh5;
import com.fossil.Vi5;
import com.fossil.Vp1;
import com.fossil.Wi5;
import com.fossil.Yn7;
import com.fossil.Zi5;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.E90;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.RingPhoneComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.model.microapp.RingMyPhoneMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LinkStreamingManager implements Mr5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Kr5> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ Il6 b; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));
    @DexIgnore
    public /* final */ HybridPresetRepository c;
    @DexIgnore
    public /* final */ AlarmsRepository d;
    @DexIgnore
    public /* final */ BuddyChallengeManager e;
    @DexIgnore
    public /* final */ WorkoutTetherGpsManager f;
    @DexIgnore
    public /* final */ QuickResponseRepository g;
    @DexIgnore
    public /* final */ MusicControlComponent h;
    @DexIgnore
    public /* final */ SetNotificationUseCase i;
    @DexIgnore
    public /* final */ An4 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return LinkStreamingManager.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Runnable {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager d;

        @DexIgnore
        public Bi(LinkStreamingManager linkStreamingManager, String str, int i) {
            Wg6.c(str, "serial");
            this.d = linkStreamingManager;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LinkStreamingManager.l.a();
            local.d(a2, "Inside SteamingAction .run - eventId=" + this.c + ", serial=" + this.b);
            if (TextUtils.isEmpty(this.b)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = LinkStreamingManager.l.a();
                local2.e(a3, "Inside .onStreamingEvent of " + this.b + " no active device");
                return;
            }
            HybridPreset activePresetBySerial = this.d.c.getActivePresetBySerial(this.b);
            if (activePresetBySerial != null) {
                Iterator<HybridPresetAppSetting> it = activePresetBySerial.getButtons().iterator();
                while (it.hasNext()) {
                    HybridPresetAppSetting next = it.next();
                    if (Wg6.a(next.getAppId(), MicroAppInstruction.MicroAppID.Companion.getMicroAppIdFromDeviceEventId(this.c).getValue())) {
                        int i = this.c;
                        if (i == E90.RING_MY_PHONE_MICRO_APP.ordinal()) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = LinkStreamingManager.l.a();
                            local3.d(a4, "MicroAppAction.run UAPP_RING_PHONE microAppSetting " + next.getSettings());
                            LinkStreamingManager linkStreamingManager = this.d;
                            String str = this.b;
                            String settings = next.getSettings();
                            if (settings != null) {
                                linkStreamingManager.y(str, settings);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else if (i == E90.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == E90.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                            Bundle bundle = new Bundle();
                            bundle.putString(Constants.EXTRA_INFO, next.getSettings());
                            CommuteTimeService.C.b(PortfolioApp.get.instance(), bundle, this.d);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$onDeviceAppEvent$1", f = "LinkStreamingManager.kt", l = {109, 116, 123, 219}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Vi5 $event;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ EnumMap $map$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, EnumMap enumMap, Ci ci) {
                super(2, xe6);
                this.$map$inlined = enumMap;
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.$map$inlined, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    MusicControlComponent musicControlComponent = this.this$0.this$0.h;
                    this.L$0 = il6;
                    this.label = 1;
                    if (musicControlComponent.n(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(LinkStreamingManager linkStreamingManager, Vi5 vi5, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkStreamingManager;
            this.$event = vi5;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$event, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x03df  */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x03e2  */
        /* JADX WARNING: Removed duplicated region for block: B:106:0x040e  */
        /* JADX WARNING: Removed duplicated region for block: B:139:0x035f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:141:0x035f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0399  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x039c  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x03bd  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 1473
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.LinkStreamingManager.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processAlarmSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {419}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(LinkStreamingManager linkStreamingManager, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkStreamingManager;
            this.$action = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$action, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object activeAlarms;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$action == Op1.SET.ordinal()) {
                    AlarmsRepository alarmsRepository = this.this$0.d;
                    this.L$0 = il6;
                    this.label = 1;
                    activeAlarms = alarmsRepository.getActiveAlarms(this);
                    if (activeAlarms == d) {
                        return d;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                activeAlarms = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) activeAlarms;
            if (list == null) {
                list = new ArrayList();
            }
            PortfolioApp.get.instance().q1(Dj5.a(list));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Vp1 $notificationEvent$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Bo1 $subEvent;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends BroadcastReceiver {
            @DexIgnore
            public /* final */ /* synthetic */ Ei a;

            @DexIgnore
            public Aii(Ei ei, byte b, QuickResponseMessage quickResponseMessage) {
                this.a = ei;
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus;
                boolean z = false;
                if (intent != null) {
                    z = intent.getBooleanExtra("SEND_SMS_PERMISSION_REQUIRED", false);
                }
                if (getResultCode() == -1) {
                    FLogger.INSTANCE.getLocal().d(LinkStreamingManager.l.a(), "SMS delivered");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.SUCCESS;
                } else if (z) {
                    FLogger.INSTANCE.getLocal().d(LinkStreamingManager.l.a(), "SMS permission required");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.NOT_FOUND;
                } else {
                    FLogger.INSTANCE.getLocal().d(LinkStreamingManager.l.a(), "SMS failed");
                    notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.FAILED;
                }
                PortfolioApp.get.instance().E0(PortfolioApp.get.instance().J(), new DianaNotificationObj(((Qp1) this.a.$notificationEvent$inlined).getNotificationUid(), NotificationBaseObj.ANotificationType.TEXT, DianaNotificationObj.AApplicationName.Companion.getMESSAGES(), "", "", 1, "", new ArrayList(), Long.valueOf(System.currentTimeMillis()), notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType.REPLY_MESSAGE));
                PortfolioApp.get.instance().unregisterReceiver(this);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Bo1 bo1, Xe6 xe6, LinkStreamingManager linkStreamingManager, Vp1 vp1) {
            super(2, xe6);
            this.$subEvent = bo1;
            this.this$0 = linkStreamingManager;
            this.$notificationEvent$inlined = vp1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$subEvent, xe6, this.this$0, this.$notificationEvent$inlined);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            QuickResponseSender quickResponseSender;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                byte messageId = ((Go1) this.$subEvent).getMessageId();
                Iterator<T> it = this.this$0.g.getAllQuickResponse().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Ao7.a(next.getId() == messageId).booleanValue()) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (!(t2 == null || (quickResponseSender = this.this$0.g.getQuickResponseSender(((Go1) this.$subEvent).getSenderId())) == null)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = LinkStreamingManager.l.a();
                    local.d(a2, "process to send sms to " + quickResponseSender.getContent() + " with id " + ((int) messageId) + " senderId " + ((Go1) this.$subEvent).getSenderId());
                    Ln5.c.a().e(quickResponseSender.getContent(), t2.getResponse(), new Aii(this, messageId, t2));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processDeviceConfigSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {431}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(int i, String str, Xe6 xe6) {
            super(2, xe6);
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.$action, this.$deviceId, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$action == Op1.SET.ordinal()) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String str = this.$deviceId;
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.x1(str, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$processNotificationFilterSynchronizeEvent$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $action;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LinkStreamingManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(LinkStreamingManager linkStreamingManager, int i, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = linkStreamingManager;
            this.$action = i;
            this.$deviceId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$action, this.$deviceId, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (this.$action == Op1.SET.ordinal()) {
                    this.this$0.i.e(new SetNotificationUseCase.Ai(this.$deviceId), null);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LinkStreamingManager$startRingMyPhone$1", f = "LinkStreamingManager.kt", l = {}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $ringtoneName;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(String str, Xe6 xe6) {
            super(2, xe6);
            this.$ringtoneName = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.$ringtoneName, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                List<Ringtone> e = AppHelper.g.e();
                if (!e.isEmpty()) {
                    for (Ringtone ringtone : e) {
                        if (Wg6.a(ringtone.getRingtoneName(), this.$ringtoneName)) {
                            Pn5.o.a().k(ringtone);
                            return Cd6.a;
                        }
                    }
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String canonicalName = LinkStreamingManager.class.getCanonicalName();
        if (canonicalName != null) {
            Wg6.b(canonicalName, "LinkStreamingManager::class.java.canonicalName!!");
            k = canonicalName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public LinkStreamingManager(HybridPresetRepository hybridPresetRepository, AlarmsRepository alarmsRepository, BuddyChallengeManager buddyChallengeManager, WorkoutTetherGpsManager workoutTetherGpsManager, QuickResponseRepository quickResponseRepository, MusicControlComponent musicControlComponent, SetNotificationUseCase setNotificationUseCase, An4 an4) {
        Wg6.c(hybridPresetRepository, "mPresetRepository");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        Wg6.c(buddyChallengeManager, "mBuddyChallengeManager");
        Wg6.c(workoutTetherGpsManager, "mWorkoutTetherGpsManager");
        Wg6.c(quickResponseRepository, "mQuickResponseRepository");
        Wg6.c(musicControlComponent, "mMusicControlComponent");
        Wg6.c(setNotificationUseCase, "mSetNotificationUseCase");
        Wg6.c(an4, "sharedPreferencesManager");
        this.c = hybridPresetRepository;
        this.d = alarmsRepository;
        this.e = buddyChallengeManager;
        this.f = workoutTetherGpsManager;
        this.g = quickResponseRepository;
        this.h = musicControlComponent;
        this.i = setNotificationUseCase;
        this.j = an4;
        PortfolioApp.get.h(this);
    }

    @DexIgnore
    public final void A(List<? extends Kr5> list) {
        if (!(list == null || this.a.isEmpty())) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "stopServices serviceListSize=" + this.a.size());
            for (Kr5 kr5 : list) {
                kr5.b();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Mr5
    public void a(Kr5 kr5) {
        Wg6.c(kr5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "addObject");
        if (!this.a.contains(kr5)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "addObject service actionId " + kr5.c());
            this.a.add(kr5);
        }
    }

    @DexIgnore
    @Override // com.fossil.Mr5
    public void b(int i2, Vh5 vh5) {
        Wg6.c(vh5, "status");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onStatusChanged action " + i2);
    }

    @DexIgnore
    @Override // com.fossil.Mr5
    public void c(Kr5 kr5) {
        Wg6.c(kr5, Constants.SERVICE);
        FLogger.INSTANCE.getLocal().d(k, "removeObject");
        if (!this.a.isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "removeObject mServiceListSize=" + this.a.size());
            Iterator<Kr5> it = this.a.iterator();
            while (it.hasNext()) {
                Kr5 next = it.next();
                Wg6.b(next, "autoStopBaseService");
                if (next.c() == kr5.c()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = k;
                    local2.d(str2, "removeObject actionId " + kr5.c());
                    this.a.remove(next);
                }
            }
        }
    }

    @DexIgnore
    @Tc7
    public final void onDeviceAppEvent(Vi5 vi5) {
        Wg6.c(vi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onDeviceAppEvent - appId=" + vi5.a() + ", serial=" + vi5.c());
        Rm6 unused = Gu7.d(this.b, null, null, new Ci(this, vi5, null), 3, null);
    }

    @DexIgnore
    @Tc7
    public final void onMicroAppCancelEvent(Wi5 wi5) {
        Wg6.c(wi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onMicroAppCancelEvent - event=" + wi5.a() + ", serial=" + wi5.b());
        A(this.a);
    }

    @DexIgnore
    @Tc7
    public final void onMusicActionEvent(Zi5 zi5) {
        Wg6.c(zi5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onMusicActionEvent - musicActionEvent=" + zi5.a() + ", serial=" + zi5.b());
        MusicControlComponent musicControlComponent = this.h;
        NotifyMusicEventResponse.MusicMediaAction a2 = zi5.a();
        Wg6.b(a2, "event.musicActionEvent");
        musicControlComponent.l(a2);
    }

    @DexIgnore
    @Tc7
    public final void onStreamingEvent(Bj5 bj5) {
        Wg6.c(bj5, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "Inside " + k + ".onStreamingEvent - event=" + bj5.a() + ", serial=" + bj5.b());
        String b2 = bj5.b();
        Wg6.b(b2, "event.serial");
        new Thread(new Bi(this, b2, bj5.a())).start();
    }

    @DexIgnore
    public final String t(String str) {
        Hr7 hr7 = Hr7.a;
        String format = String.format("update_%s", Arrays.copyOf(new Object[]{str}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final Rm6 u(String str, int i2) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new Di(this, i2, null), 3, null);
    }

    @DexIgnore
    public final void v(Vp1 vp1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("processCallMessageNotificationEvent event ");
        sb.append(vp1);
        sb.append(" subEvent ");
        Qp1 qp1 = (Qp1) vp1;
        sb.append(qp1 != null ? qp1.getAction() : null);
        local.d(str, sb.toString());
        if (vp1 != null) {
            Bo1 action = qp1.getAction();
            if (action instanceof Fo1) {
                if (DeviceHelper.o.z()) {
                    Ln5.c.a().d();
                }
            } else if (action instanceof Ao1) {
                if (DeviceHelper.o.z()) {
                    Ln5.c.a().a();
                }
            } else if (action instanceof Go1) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ei(action, null, this, vp1), 3, null);
            } else {
                if (action instanceof Eo1) {
                }
            }
        }
    }

    @DexIgnore
    public final Rm6 w(String str, int i2) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Fi(i2, str, null), 3, null);
    }

    @DexIgnore
    public final Rm6 x(String str, int i2) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Gi(this, i2, str, null), 3, null);
    }

    @DexIgnore
    public final void y(String str, String str2) {
        Wg6.c(str, "deviceId");
        Wg6.c(str2, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = k;
        local.d(str3, "startRingMyPhone, setting=" + str2);
        try {
            if (TextUtils.isEmpty(str2)) {
                List<Ringtone> e2 = AppHelper.g.e();
                Ringtone ringtone = e2.get(0);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = k;
                local2.d(str4, "ringtones=" + e2 + ", defaultRingtone=" + ringtone);
                Pn5.o.a().k(ringtone);
                return;
            }
            String component1 = ((Ringtone) new Gson().k(str2, Ringtone.class)).component1();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = k;
            local3.d(str5, "MicroAppAction.run - ringtone " + str2);
            PortfolioApp.get.instance().g1(new RingMyPhoneMicroAppResponse(), str);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Hi(component1, null), 3, null);
        } catch (Exception e3) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("AppUtil", "Error when read asset file - ex=" + e3);
        }
    }

    @DexIgnore
    public final void z(String str, int i2) {
        Ku1 ku1;
        FLogger.INSTANCE.getLocal().d(k, "startRingMyPhone");
        if (i2 == Ku1.ON.ordinal()) {
            Pn5.o.a().n(new Ringtone(Constants.RINGTONE_DEFAULT, ""), 60000);
            ku1 = Ku1.ON;
        } else {
            Pn5.o.a().p();
            ku1 = Ku1.OFF;
        }
        PortfolioApp.get.instance().g1(new RingPhoneComplicationAppInfo(ku1), str);
    }
}
