package com.portfolio.platform.manager;

import android.os.Handler;
import android.os.Looper;
import android.provider.Telephony;
import android.text.TextUtils;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Et7;
import com.fossil.F47;
import com.fossil.Fn5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Mn5;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.HybridNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppFilterHistory;
import com.portfolio.platform.data.AppType;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.NotificationType;
import com.portfolio.platform.data.model.LightAndHaptics;
import com.portfolio.platform.data.model.MessageComparator;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.data.model.NotificationPriority;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.util.NotificationAppHelper;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.zip.CRC32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LightAndHapticsManager {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static LightAndHapticsManager h;
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public DeviceRepository a;
    @DexIgnore
    public An4 b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<LightAndHaptics> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ Runnable f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final LightAndHapticsManager a() {
            if (b() == null) {
                c(new LightAndHapticsManager(null));
            }
            LightAndHapticsManager b = b();
            if (b != null) {
                return b;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final LightAndHapticsManager b() {
            return LightAndHapticsManager.h;
        }

        @DexIgnore
        public final void c(LightAndHapticsManager lightAndHapticsManager) {
            LightAndHapticsManager.h = lightAndHapticsManager;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstAppFilter$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$info, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                LightAndHapticsManager lightAndHapticsManager = this.this$0;
                String packageName = this.$info.getPackageName();
                Wg6.b(packageName, "info.packageName");
                LightAndHaptics t = lightAndHapticsManager.t(packageName, this.$info.getBody());
                if (t != null) {
                    t.setNotificationType(NotificationType.APP_FILTER);
                    this.this$0.g(t);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstEmail$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$info, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = LightAndHapticsManager.g;
                local.e(str, "Check again email with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics t = this.this$0.t(AppType.ALL_EMAIL.name(), this.$info.getBody());
                if (t != null) {
                    this.this$0.g(t);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstPhone$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$info, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = LightAndHapticsManager.g;
                local.e(str, "Check again phone with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics t = this.this$0.t(AppType.ALL_CALLS.name(), this.$info.getSenderInfo());
                if (t != null) {
                    t.setNotificationType(NotificationType.CALL);
                    this.this$0.g(t);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.LightAndHapticsManager$checkAgainstSMS$1", f = "LightAndHapticsManager.kt", l = {}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationInfo $info;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(LightAndHapticsManager lightAndHapticsManager, NotificationInfo notificationInfo, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = lightAndHapticsManager;
            this.$info = notificationInfo;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$info, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = LightAndHapticsManager.g;
                local.e(str, "Check again sms with senderInfo " + this.$info.getSenderInfo() + " and body " + this.$info.getBody());
                LightAndHaptics t = this.this$0.t(AppType.ALL_SMS.name(), this.$info.getBody());
                if (t != null) {
                    t.setNotificationType(NotificationType.SMS);
                    this.this$0.g(t);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LightAndHapticsManager b;

        @DexIgnore
        public Fi(LightAndHapticsManager lightAndHapticsManager) {
            this.b = lightAndHapticsManager;
        }

        @DexIgnore
        public final void run() {
            this.b.u();
        }
    }

    /*
    static {
        String simpleName = LightAndHapticsManager.class.getSimpleName();
        Wg6.b(simpleName, "LightAndHapticsManager::class.java.simpleName");
        g = simpleName;
        Wg6.b(Arrays.asList(CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR), "Arrays.asList(Calibratio\u2026brationEnums.HandId.HOUR)");
        Wg6.b(Arrays.asList(CalibrationEnums.HandId.MINUTE, CalibrationEnums.HandId.HOUR, CalibrationEnums.HandId.SUB_EYE), "Arrays.asList(Calibratio\u2026tionEnums.HandId.SUB_EYE)");
    }
    */

    @DexIgnore
    public LightAndHapticsManager() {
        PortfolioApp.get.instance().getIface().R(this);
        this.c = new PriorityBlockingQueue<>(5, new MessageComparator());
        this.e = new Handler(Looper.getMainLooper());
        this.f = new Fi(this);
    }

    @DexIgnore
    public /* synthetic */ LightAndHapticsManager(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void g(LightAndHaptics lightAndHaptics) {
        if (this.c.size() < 5) {
            this.c.add(lightAndHaptics);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "addEventToQueue : " + lightAndHaptics.getType());
            if (!this.d) {
                this.d = true;
                v();
            }
        }
    }

    @DexIgnore
    public final void h(NotificationInfo notificationInfo) {
        Wg6.c(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "notification info : " + notificationInfo.getSource());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local2.d(str2, "notification body : " + notificationInfo.getBody());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local3.d(str3, "notification type : " + notificationInfo.getSource());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local4.e(str4, "notification - info=" + notificationInfo);
        String packageName = notificationInfo.getPackageName();
        Wg6.b(packageName, "info.packageName");
        if (!q(packageName)) {
            if (notificationInfo.getSource() == NotificationSource.CALL || notificationInfo.getSource() == NotificationSource.TEXT || notificationInfo.getSource() == NotificationSource.MAIL) {
                if (k(notificationInfo)) {
                    return;
                }
                if (notificationInfo.getSource() == NotificationSource.CALL) {
                    m(notificationInfo);
                } else if (notificationInfo.getSource() == NotificationSource.TEXT) {
                    n(notificationInfo);
                } else if (notificationInfo.getSource() == NotificationSource.MAIL) {
                    l(notificationInfo);
                }
            } else if (notificationInfo.getSource() == NotificationSource.OS) {
                j(notificationInfo);
            }
        }
    }

    @DexIgnore
    public final void i(NotificationInfo notificationInfo) {
        Wg6.c(notificationInfo, "info");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : pkg = " + notificationInfo);
        LightAndHaptics s = s(notificationInfo.getSenderInfo());
        if (s == null) {
            String packageName = notificationInfo.getPackageName();
            Wg6.b(packageName, "info.packageName");
            s = t(packageName, notificationInfo.getBody());
            if (s == null) {
                String packageName2 = notificationInfo.getPackageName();
                Wg6.b(packageName2, "info.packageName");
                if (q(packageName2)) {
                    List<ContactGroup> b2 = F47.d().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
                    NotificationSource source = notificationInfo.getSource();
                    Wg6.b(source, "info.source");
                    s = r("-5678", b2, source);
                }
            }
        }
        if (s != null) {
            g(s);
        }
    }

    @DexIgnore
    public final void j(NotificationInfo notificationInfo) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "PACKAGE CHECK : " + notificationInfo.getPackageName());
        if (!TextUtils.isEmpty(notificationInfo.getPackageName())) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Bi(this, notificationInfo, null), 3, null);
        }
    }

    @DexIgnore
    public final boolean k(NotificationInfo notificationInfo) {
        List<ContactGroup> list;
        List<ContactGroup> list2;
        LightAndHaptics r;
        if (notificationInfo.getSource() == NotificationSource.TEXT) {
            list = F47.d().b("-5678", MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                F47 d2 = F47.d();
                String senderInfo = notificationInfo.getSenderInfo();
                if (senderInfo != null) {
                    list2 = d2.b(senderInfo, MFDeviceFamily.DEVICE_FAMILY_SAM);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            list2 = null;
        } else if (notificationInfo.getSource() == NotificationSource.CALL) {
            list = F47.d().a("-1234", MFDeviceFamily.DEVICE_FAMILY_SAM);
            if (!TextUtils.isEmpty(notificationInfo.getSenderInfo())) {
                F47 d3 = F47.d();
                String senderInfo2 = notificationInfo.getSenderInfo();
                if (senderInfo2 != null) {
                    list2 = d3.a(senderInfo2, MFDeviceFamily.DEVICE_FAMILY_SAM);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            list2 = null;
        } else {
            list = null;
            list2 = null;
        }
        if (list != null && list2 != null && list2.isEmpty() && list.isEmpty()) {
            if (p()) {
                j(notificationInfo);
                FLogger.INSTANCE.getLocal().d(g, ".Inside checkAgainstContacts, NO assigned contact with this phone number, NO ALL-TEXT, let Message fire");
            }
            return false;
        } else if (list2 == null && list == null) {
            return false;
        } else {
            if (list2 == null || !(!list2.isEmpty())) {
                NotificationSource source = notificationInfo.getSource();
                if (source == null) {
                    r = null;
                } else {
                    int i2 = Fn5.a[source.ordinal()];
                    if (i2 == 1) {
                        NotificationSource source2 = notificationInfo.getSource();
                        Wg6.b(source2, "info.source");
                        r = r("-5678", list, source2);
                        FLogger.INSTANCE.getLocal().d(g, "ALL TEXT with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + r);
                    } else if (i2 != 2) {
                        r = null;
                    } else {
                        NotificationSource source3 = notificationInfo.getSource();
                        Wg6.b(source3, "info.source");
                        r = r("-1234", list, source3);
                        FLogger.INSTANCE.getLocal().d(g, "ALL CALL with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + r);
                    }
                }
            } else {
                String senderInfo3 = notificationInfo.getSenderInfo();
                NotificationSource source4 = notificationInfo.getSource();
                Wg6.b(source4, "info.source");
                r = r(senderInfo3, list2, source4);
            }
            FLogger.INSTANCE.getLocal().d(g, "checkAgainstContacts with " + notificationInfo.getSenderInfo() + " body " + notificationInfo.getBody() + " matchcontact is " + r);
            if (r == null) {
                return false;
            }
            g(r);
            return true;
        }
    }

    @DexIgnore
    public final Rm6 l(NotificationInfo notificationInfo) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ci(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final Rm6 m(NotificationInfo notificationInfo) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Di(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final Rm6 n(NotificationInfo notificationInfo) {
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ei(this, notificationInfo, null), 3, null);
    }

    @DexIgnore
    public final void o(LightAndHaptics lightAndHaptics) {
        FLogger.INSTANCE.getLocal().d(g, "doPlayHandsNotification");
        NotificationType type = lightAndHaptics.getType();
        if (type != null) {
            int i2 = Fn5.c[type.ordinal()];
            if (i2 == 1) {
                PortfolioApp instance = PortfolioApp.get.instance();
                String J = PortfolioApp.get.instance().J();
                NotificationBaseObj.ANotificationType aNotificationType = NotificationBaseObj.ANotificationType.INCOMING_CALL;
                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                String senderName = lightAndHaptics.getSenderName();
                Wg6.b(senderName, "item.senderName");
                String senderName2 = lightAndHaptics.getSenderName();
                Wg6.b(senderName2, "item.senderName");
                String senderName3 = lightAndHaptics.getSenderName();
                Wg6.b(senderName3, "item.senderName");
                instance.h1(J, new DianaNotificationObj(0, aNotificationType, phone_incoming_call, senderName, senderName2, -1, senderName3, Hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), null, null, null, 1792, null));
            } else if (i2 == 2) {
                PortfolioApp instance2 = PortfolioApp.get.instance();
                String J2 = PortfolioApp.get.instance().J();
                NotificationBaseObj.ANotificationType aNotificationType2 = NotificationBaseObj.ANotificationType.TEXT;
                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                String senderName4 = lightAndHaptics.getSenderName();
                Wg6.b(senderName4, "item.senderName");
                String senderName5 = lightAndHaptics.getSenderName();
                Wg6.b(senderName5, "item.senderName");
                String senderName6 = lightAndHaptics.getSenderName();
                Wg6.b(senderName6, "item.senderName");
                instance2.h1(J2, new DianaNotificationObj(0, aNotificationType2, messages, senderName4, senderName5, -1, senderName6, Hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), null, null, null, 1792, null));
            } else if (i2 == 3) {
                String senderName7 = lightAndHaptics.getSenderName();
                String packageName = senderName7 == null || Vt7.l(senderName7) ? lightAndHaptics.getPackageName() : lightAndHaptics.getSenderName();
                CRC32 crc32 = new CRC32();
                String packageName2 = lightAndHaptics.getPackageName();
                Wg6.b(packageName2, "item.packageName");
                Charset charset = Et7.a;
                if (packageName2 != null) {
                    byte[] bytes = packageName2.getBytes(charset);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    crc32.update(bytes);
                    crc32.getValue();
                    Wg6.b(packageName, "appName");
                    String packageName3 = lightAndHaptics.getPackageName();
                    Wg6.b(packageName3, "item.packageName");
                    FNotification fNotification = new FNotification(packageName, packageName3, "", NotificationBaseObj.ANotificationType.NOTIFICATION);
                    PortfolioApp instance3 = PortfolioApp.get.instance();
                    String J3 = PortfolioApp.get.instance().J();
                    NotificationBaseObj.ANotificationType aNotificationType3 = NotificationBaseObj.ANotificationType.NOTIFICATION;
                    String packageName4 = lightAndHaptics.getPackageName();
                    Wg6.b(packageName4, "item.packageName");
                    String packageName5 = lightAndHaptics.getPackageName();
                    Wg6.b(packageName5, "item.packageName");
                    String packageName6 = lightAndHaptics.getPackageName();
                    Wg6.b(packageName6, "item.packageName");
                    instance3.h1(J3, new HybridNotificationObj(0, aNotificationType3, fNotification, packageName4, packageName5, -1, packageName6, Hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
    }

    @DexIgnore
    public final boolean p() {
        for (AppFilter appFilter : Mn5.p.a().c().getAllAppFilters()) {
            Wg6.b(appFilter, "item");
            if (Vt7.j(appFilter.getType(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), true)) {
                FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return true");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(g, ".Inside hasMessageApp, return false");
        return false;
    }

    @DexIgnore
    public final boolean q(String str) {
        if (TextUtils.equals(Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), str)) {
            An4 an4 = this.b;
            if (an4 == null) {
                Wg6.n("mSharePref");
                throw null;
            } else if (an4.q0()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final LightAndHaptics r(String str, List<? extends ContactGroup> list, NotificationSource notificationSource) {
        ContactGroup contactGroup;
        Contact contactWithPhoneNumber;
        if (list == null || !(!list.isEmpty()) || (contactWithPhoneNumber = (contactGroup = (ContactGroup) list.get(0)).getContactWithPhoneNumber(str)) == null) {
            return null;
        }
        String firstName = contactWithPhoneNumber.getFirstName();
        if (!TextUtils.isEmpty(contactWithPhoneNumber.getLastName()) && !Vt7.j(contactWithPhoneNumber.getLastName(), "null", true)) {
            firstName = firstName + " " + contactWithPhoneNumber.getLastName();
        }
        int i2 = Fn5.b[notificationSource.ordinal()];
        if (i2 == 1) {
            return new LightAndHaptics("", NotificationType.CALL, firstName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_CALL);
        }
        if (i2 != 2) {
            return null;
        }
        return new LightAndHaptics("", NotificationType.SMS, firstName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public final LightAndHaptics s(String str) {
        Contact c2 = F47.d().c(str, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (c2 == null || !c2.isUseSms()) {
            return null;
        }
        NotificationType notificationType = NotificationType.SMS;
        String displayName = c2.getDisplayName();
        ContactGroup contactGroup = c2.getContactGroup();
        Wg6.b(contactGroup, "contact.contactGroup");
        return new LightAndHaptics("", notificationType, displayName, contactGroup.getHour(), NotificationPriority.ENTOURAGE_SMS);
    }

    @DexIgnore
    public final LightAndHaptics t(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "Attepting to match app " + str);
        AppFilter f2 = NotificationAppHelper.b.f(str, MFDeviceFamily.DEVICE_FAMILY_SAM);
        if (f2 == null) {
            return null;
        }
        AppFilterHistory appFilterHistory = new AppFilterHistory("");
        appFilterHistory.setColor(f2.getColor());
        appFilterHistory.setHaptic(f2.getHaptic());
        appFilterHistory.setTitle(f2.getName());
        appFilterHistory.setSubTitle(str2);
        appFilterHistory.setType(f2.getType());
        NotificationType notificationType = NotificationType.APP_FILTER;
        if (Wg6.a(str, AppType.ALL_CALLS.name())) {
            notificationType = NotificationType.CALL;
        } else if (Wg6.a(str, AppType.ALL_SMS.name())) {
            notificationType = NotificationType.SMS;
        } else if (Wg6.a(str, AppType.ALL_EMAIL.name())) {
            notificationType = NotificationType.EMAIL;
        }
        return new LightAndHaptics(str, notificationType, "", f2.getHour(), NotificationPriority.APP_FILTER);
    }

    @DexIgnore
    public final void u() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Check the  : " + this.c.size());
        if (!this.c.isEmpty()) {
            this.d = true;
            v();
            return;
        }
        this.d = false;
    }

    @DexIgnore
    public final void v() {
        if (!this.c.isEmpty()) {
            try {
                LightAndHaptics remove = this.c.remove();
                Wg6.b(remove, "nextItem");
                o(remove);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Total duration: 500");
                this.e.postDelayed(this.f, 500);
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "exception when startQueue " + e2);
            }
        } else {
            this.d = false;
        }
    }
}
