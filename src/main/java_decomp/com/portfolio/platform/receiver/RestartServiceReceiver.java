package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mapped.ServiceUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RestartServiceReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        String simpleName = RestartServiceReceiver.class.getSimpleName();
        Wg6.b(simpleName, "RestartServiceReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Wg6.c(context, "context");
        Wg6.c(intent, "intent");
        if (!(PortfolioApp.get.instance().J().length() == 0)) {
            FLogger.INSTANCE.getLocal().e(a, "Service Tracking - startForegroundService in RestartServiceReceiver");
            ServiceUtils.Ai.e(ServiceUtils.a, context, MFDeviceService.class, null, 4, null);
            ServiceUtils.Ai.e(ServiceUtils.a, context, ButtonService.class, null, 4, null);
        }
    }
}
