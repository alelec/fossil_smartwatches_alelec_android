package com.portfolio.platform.preset.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendedPresetRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ DianaRecommendedPresetRemote b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository", f = "DianaRecommendedPresetRepository.kt", l = {21, 31}, m = "fetchRecommendPreset")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaRecommendedPresetRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DianaRecommendedPresetRepository dianaRecommendedPresetRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaRecommendedPresetRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore
    public DianaRecommendedPresetRepository(DianaRecommendedPresetRemote dianaRecommendedPresetRemote) {
        Wg6.c(dianaRecommendedPresetRemote, "remote");
        this.b = dianaRecommendedPresetRemote;
        String simpleName = DianaRecommendedPresetRepository.class.getSimpleName();
        Wg6.b(simpleName, "DianaRecommendedPresetRe\u2026ry::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r11, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.No5>>> r12) {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository.a(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
