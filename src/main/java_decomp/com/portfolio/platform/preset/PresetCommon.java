package com.portfolio.platform.preset;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PresetCommon {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ PresetCommon b; // = new PresetCommon();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.PresetCommon", f = "PresetCommon.kt", l = {19, 21}, m = "fetchDianaPresetAndWF")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PresetCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(PresetCommon presetCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = presetCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.PresetCommon", f = "PresetCommon.kt", l = {31, 37}, m = "fetchRecommendedPresetAndWF")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PresetCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(PresetCommon presetCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = presetCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.PresetCommon", f = "PresetCommon.kt", l = {74, 78, 85, 92}, m = "syncAllPresets")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PresetCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(PresetCommon presetCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = presetCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.PresetCommon", f = "PresetCommon.kt", l = {48}, m = "upsertPresets")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PresetCommon this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(PresetCommon presetCommon, Xe6 xe6) {
            super(xe6);
            this.this$0 = presetCommon;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, null, null, this);
        }
    }

    /*
    static {
        String simpleName = PresetCommon.class.getSimpleName();
        Wg6.b(simpleName, "PresetCommon::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.portfolio.platform.preset.data.source.DianaPresetRepository r17, com.portfolio.platform.data.source.FileRepository r18, java.lang.String r19, com.mapped.Xe6<? super java.util.List<com.fossil.Mo5>> r20) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.preset.PresetCommon.a(com.portfolio.platform.preset.data.source.DianaPresetRepository, com.portfolio.platform.data.source.FileRepository, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository r18, com.portfolio.platform.data.source.FileRepository r19, java.lang.String r20, com.mapped.Xe6<? super com.mapped.Lc6<? extends java.util.List<com.fossil.No5>, java.lang.Boolean>> r21) {
        /*
        // Method dump skipped, instructions count: 367
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.preset.PresetCommon.b(com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository, com.portfolio.platform.data.source.FileRepository, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v43, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.portfolio.platform.preset.data.source.DianaPresetRepository r11, com.portfolio.platform.data.source.FileRepository r12, java.lang.String r13, com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 481
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.preset.PresetCommon.c(com.portfolio.platform.preset.data.source.DianaPresetRepository, com.portfolio.platform.data.source.FileRepository, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.util.List<com.fossil.Jo5> r10, com.portfolio.platform.data.source.FileRepository r11, com.portfolio.platform.preset.data.source.DianaPresetRepository r12, com.mapped.Xe6<? super java.lang.Boolean> r13) {
        /*
            r9 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 1
            boolean r0 = r13 instanceof com.portfolio.platform.preset.PresetCommon.Di
            if (r0 == 0) goto L_0x00ac
            r0 = r13
            com.portfolio.platform.preset.PresetCommon$Di r0 = (com.portfolio.platform.preset.PresetCommon.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x00ac
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x00bc
            if (r1 != r8) goto L_0x00b4
            java.lang.Object r0 = r2.L$3
            com.portfolio.platform.preset.data.source.DianaPresetRepository r0 = (com.portfolio.platform.preset.data.source.DianaPresetRepository) r0
            java.lang.Object r0 = r2.L$2
            com.portfolio.platform.data.source.FileRepository r0 = (com.portfolio.platform.data.source.FileRepository) r0
            java.lang.Object r1 = r2.L$1
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r2.L$0
            com.portfolio.platform.preset.PresetCommon r1 = (com.portfolio.platform.preset.PresetCommon) r1
            com.fossil.El7.b(r3)
            r1 = r3
        L_0x0034:
            com.fossil.Kz4 r1 = (com.fossil.Kz4) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r2.getLocal()
            java.lang.String r4 = com.portfolio.platform.preset.PresetCommon.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r2 = "upsertPresets - upsertResultWrapper: "
            r5.append(r2)
            java.lang.Object r2 = r1.c()
            java.util.List r2 = (java.util.List) r2
            r5.append(r2)
            java.lang.String r2 = r5.toString()
            r3.e(r4, r2)
            java.lang.Object r1 = r1.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x00d5
            java.util.Iterator r6 = r1.iterator()
        L_0x0064:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x00d0
            java.lang.Object r1 = r6.next()
            r5 = r1
            com.fossil.Mo5 r5 = (com.fossil.Mo5) r5
            java.lang.String r1 = r5.e()
            com.portfolio.platform.data.model.LocalFile r1 = r0.getLocalFileByFileName(r1)
            java.lang.String r2 = r5.e()
            java.io.File r2 = r0.getFileByFileName(r2)
            if (r2 == 0) goto L_0x0064
            byte[] r4 = com.fossil.J37.b(r2)
            java.lang.String r2 = r5.e()
            com.misfit.frameworks.buttonservice.model.FileType r3 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r0.deleteFileByName(r2, r3)
            if (r1 == 0) goto L_0x0095
            r0.delete(r1)
        L_0x0095:
            java.lang.String r1 = r5.d()
            com.misfit.frameworks.buttonservice.model.FileType r2 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.lang.String r3 = r5.b()
            java.lang.String r7 = "binaryData"
            com.mapped.Wg6.b(r4, r7)
            java.lang.String r5 = r5.e()
            r0.writeFileWithDir(r1, r2, r3, r4, r5)
            goto L_0x0064
        L_0x00ac:
            com.portfolio.platform.preset.PresetCommon$Di r0 = new com.portfolio.platform.preset.PresetCommon$Di
            r0.<init>(r9, r13)
            r2 = r0
            goto L_0x0014
        L_0x00b4:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x00bc:
            com.fossil.El7.b(r3)
            r2.L$0 = r9
            r2.L$1 = r10
            r2.L$2 = r11
            r2.L$3 = r12
            r2.label = r8
            java.lang.Object r1 = r12.r(r10, r2)
            if (r1 != r0) goto L_0x00db
        L_0x00cf:
            return r0
        L_0x00d0:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r8)
            goto L_0x00cf
        L_0x00d5:
            r0 = 0
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
            goto L_0x00cf
        L_0x00db:
            r0 = r11
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.preset.PresetCommon.d(java.util.List, com.portfolio.platform.data.source.FileRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository, com.mapped.Xe6):java.lang.Object");
    }
}
