package com.portfolio.platform.app_setting.flag.data;

import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Kr4;
import com.fossil.Kz4;
import com.fossil.Nr4;
import com.fossil.Rr4;
import com.fossil.Wt7;
import com.fossil.Yn7;
import com.fossil.Zi4;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Fu3;
import com.mapped.Il6;
import com.mapped.Kc6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlagRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Nr4 b;
    @DexIgnore
    public /* final */ FlagRemoteDataSource c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$fetchFlags$2", f = "FlagRepository.kt", l = {24}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Rr4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $agent;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $flags;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FlagRepository this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends TypeToken<List<? extends Rr4>> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FlagRepository flagRepository, String str, String str2, String[] strArr, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = flagRepository;
            this.$serialNumber = str;
            this.$agent = str2;
            this.$flags = strArr;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$serialNumber, this.$agent, this.$flags, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Rr4>>> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object b;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FlagRemoteDataSource flagRemoteDataSource = this.this$0.c;
                String str = this.$serialNumber;
                String str2 = this.$agent;
                String[] strArr = this.$flags;
                this.L$0 = il6;
                this.label = 1;
                b = flagRemoteDataSource.b(str, str2, strArr, this);
                if (b == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                b = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ap4 ap4 = (Ap4) b;
            if (ap4 instanceof Kq5) {
                Ku3 ku3 = (Ku3) ((Kq5) ap4).a();
                if (ku3 == null) {
                    return new Kz4(new ServerError(600, "success with empty result"));
                }
                try {
                    Fu3 q = ku3.q("flags");
                    Gson d2 = new Zi4().d();
                    Wg6.b(d2, "GsonBuilder().create()");
                    List<Rr4> list = (List) d2.h(q, new Aii().getType());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = this.this$0.a;
                    local.e(str3, "result: " + list);
                    Nr4 nr4 = this.this$0.b;
                    Wg6.b(list, "flagItems");
                    nr4.b(list);
                    return new Kz4(list, null, 2, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    return new Kz4(new ServerError(600, "success with empty result"));
                }
            } else if (ap4 instanceof Hq5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = this.this$0.a;
                StringBuilder sb = new StringBuilder();
                sb.append("fetchFlags - failed - ");
                Hq5 hq5 = (Hq5) ap4;
                sb.append(hq5.a());
                local2.e(str4, sb.toString());
                int a2 = hq5.a();
                ServerError c = hq5.c();
                return new Kz4(new ServerError(a2, c != null ? c.getMessage() : null));
            } else {
                throw new Kc6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$isBcOn$2", f = "FlagRepository.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FlagRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FlagRepository flagRepository, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = flagRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Rr4 f = this.this$0.f(Kr4.BUDDY_CHALLENGE.getStrType());
                if (f != null) {
                    String b = f.b();
                    if (b == null) {
                        b = "";
                    }
                    if (!Wt7.v(b, "on", false, 2, null)) {
                        z = false;
                        return Ao7.a(z);
                    }
                }
                z = true;
                return Ao7.a(z);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public FlagRepository(Nr4 nr4, FlagRemoteDataSource flagRemoteDataSource) {
        Wg6.c(nr4, "local");
        Wg6.c(flagRemoteDataSource, "remote");
        this.b = nr4;
        this.c = flagRemoteDataSource;
        String simpleName = FlagRepository.class.getSimpleName();
        Wg6.b(simpleName, "FlagRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final Object e(String str, String str2, String[] strArr, Xe6<? super Kz4<List<Rr4>>> xe6) {
        return Eu7.g(Bw7.b(), new Ai(this, str, str2, strArr, null), xe6);
    }

    @DexIgnore
    public final Rr4 f(String str) {
        return this.b.a(str);
    }

    @DexIgnore
    public final Object g(Xe6<? super Boolean> xe6) {
        return Eu7.g(Bw7.b(), new Bi(this, null), xe6);
    }
}
