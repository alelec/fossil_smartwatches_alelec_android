package com.portfolio.platform;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase.Ai;
import com.portfolio.platform.CoroutineUseCase.Bi;
import com.portfolio.platform.CoroutineUseCase.Di;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class CoroutineUseCase<Q extends Bi, R extends Di, E extends Ai> {
    @DexIgnore
    public Ei<? super R, ? super E> a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ Il6 c; // = Jv7.a(Bw7.a());

    @DexIgnore
    public interface Ai extends Ci {
    }

    @DexIgnore
    public interface Bi {
    }

    @DexIgnore
    public interface Ci {
    }

    @DexIgnore
    public interface Di extends Ci {
    }

    @DexIgnore
    public interface Ei<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ei $callBack;
        @DexIgnore
        public /* final */ /* synthetic */ Bi $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(CoroutineUseCase coroutineUseCase, Ei ei, Bi bi, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$callBack = ei;
            this.$requestValues = bi;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$callBack, this.$requestValues, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v4, resolved type: com.portfolio.platform.CoroutineUseCase */
        /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.portfolio.platform.CoroutineUseCase */
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.portfolio.platform.CoroutineUseCase */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object k;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.a = this.$callBack;
                CoroutineUseCase coroutineUseCase = this.this$0;
                coroutineUseCase.b = coroutineUseCase.h();
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Start UseCase");
                CoroutineUseCase coroutineUseCase2 = this.this$0;
                Bi bi = this.$requestValues;
                this.L$0 = il6;
                this.label = 1;
                k = coroutineUseCase2.k(bi, this);
                if (k == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                k = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (k instanceof Di) {
                this.this$0.j((Di) k);
            } else if (k instanceof Ai) {
                this.this$0.i((Ai) k);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.CoroutineUseCase$onError$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ai $errorValue;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(CoroutineUseCase coroutineUseCase, Ai ai, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$errorValue = ai;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$errorValue, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback failed");
                Ei ei = this.this$0.a;
                if (ei != null) {
                    ei.a(this.$errorValue);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Di $response;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(CoroutineUseCase coroutineUseCase, Di di, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = coroutineUseCase;
            this.$response = di;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$response, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback success");
                Ei ei = this.this$0.a;
                if (ei != null) {
                    ei.onSuccess(this.$response);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final Rm6 e(Q q, Ei<? super R, ? super E> ei) {
        return Gu7.d(this.c, null, null, new Fi(this, ei, q, null), 3, null);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.portfolio.platform.CoroutineUseCase$Ei<? super R extends com.portfolio.platform.CoroutineUseCase$Di, ? super E extends com.portfolio.platform.CoroutineUseCase$Ai>, com.portfolio.platform.CoroutineUseCase$Ei<R extends com.portfolio.platform.CoroutineUseCase$Di, E extends com.portfolio.platform.CoroutineUseCase$Ai> */
    public final Ei<R, E> f() {
        return (Ei<? super R, ? super E>) this.a;
    }

    @DexIgnore
    public final Il6 g() {
        return this.c;
    }

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public final Rm6 i(E e) {
        Wg6.c(e, "errorValue");
        return Gu7.d(this.c, Bw7.c(), null, new Gi(this, e, null), 2, null);
    }

    @DexIgnore
    public final Rm6 j(R r) {
        Wg6.c(r, "response");
        return Gu7.d(this.c, Bw7.c(), null, new Hi(this, r, null), 2, null);
    }

    @DexIgnore
    public abstract Object k(Q q, Xe6<Object> xe6);

    @DexIgnore
    public final void l(Ei<? super R, ? super E> ei) {
        Wg6.c(ei, Constants.CALLBACK);
        this.a = ei;
    }
}
