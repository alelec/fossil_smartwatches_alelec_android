package com.portfolio.platform.news.notifications;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.TaskStackBuilder;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Jx7;
import com.fossil.Ko7;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilNotificationBar {
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1", f = "FossilNotificationBar.kt", l = {42}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Context $context;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ FossilNotificationBar $fossilNotificationBar;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, FossilNotificationBar fossilNotificationBar, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$fossilNotificationBar = fossilNotificationBar;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$fossilNotificationBar, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        Ai.g(FossilNotificationBar.c, this.this$0.$context, this.$fossilNotificationBar, false, 4, null);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Context context, Xe6 xe6) {
                super(2, xe6);
                this.$context = context;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$context, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    String I = PortfolioApp.get.instance().I();
                    FossilNotificationBar fossilNotificationBar = new FossilNotificationBar(I, null, 2, null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("FossilNotificationBar", "content " + I);
                    Jx7 c = Bw7.c();
                    Aiii aiii = new Aiii(this, fossilNotificationBar, null);
                    this.L$0 = il6;
                    this.L$1 = I;
                    this.L$2 = fossilNotificationBar;
                    this.label = 1;
                    if (Eu7.g(c, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    FossilNotificationBar fossilNotificationBar2 = (FossilNotificationBar) this.L$2;
                    String str = (String) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void g(Ai ai, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            ai.f(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public static /* synthetic */ void i(Ai ai, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            ai.h(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public final PendingIntent a(Context context, String str, int i) {
            Intent intent = new Intent(context, NotificationReceiver.class);
            intent.setAction(str);
            intent.putExtra("ACTION_EVENT", i);
            return PendingIntent.getBroadcast(context, Action.DisplayMode.DATE, intent, 134217728);
        }

        @DexIgnore
        public final PendingIntent b(Context context, int i, int i2) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", i2);
            TaskStackBuilder f = TaskStackBuilder.f(context);
            f.b(intent);
            Wg6.b(f, "TaskStackBuilder.create(\u2026ntWithParentStack(intent)");
            return f.g(i, 134217728);
        }

        @DexIgnore
        public final void c(Context context, Service service, boolean z) {
            Wg6.c(context, "context");
            Wg6.c(service, Constants.SERVICE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "Service Trackinng - startForegroundNotification() - context=" + context + ", service=" + service + ", isStopForeground = " + z);
            try {
                NotificationUtils.Companion.getInstance().startForegroundNotification(context, service, "", "", z);
                d(context);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "startForegroundNotification() - ex=" + e);
            }
        }

        @DexIgnore
        public final void d(Context context) {
            Wg6.c(context, "context");
            try {
                boolean q0 = PortfolioApp.get.instance().q0();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FossilNotificationBar", "updateData() - context=" + context + " hasWorkout " + q0);
                if (q0) {
                    Lc6<String, String> k = PortfolioApp.get.instance().g0().k();
                    i(this, context, new FossilNotificationBar(k.getSecond(), k.getFirst()), false, 4, null);
                    return;
                }
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Aii(context, null), 3, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
            }
        }

        @DexIgnore
        public final void e(Context context, String str, String str2) {
            Wg6.c(context, "context");
            Wg6.c(str, "title");
            Wg6.c(str2, "content");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateData() - title=" + str + " - content=" + str2);
            try {
                i(this, context, Vt7.l(str) ^ true ? new FossilNotificationBar(str2, str) : new FossilNotificationBar(str2, null, 2, null), false, 4, null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public final void f(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotification content " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.get.instance().J())) {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, b(context, Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
            } else {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, null, null, z);
            }
        }

        @DexIgnore
        public final void h(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotificationWithRichContent " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.get.instance().J())) {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, z);
            } else {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, z);
            }
        }
    }

    @DexIgnore
    public FossilNotificationBar() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public FossilNotificationBar(String str, String str2) {
        Wg6.c(str, "mContent");
        Wg6.c(str2, "mTitle");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ FossilNotificationBar(java.lang.String r3, java.lang.String r4, int r5, com.mapped.Qg6 r6) {
        /*
            r2 = this;
            r0 = r5 & 1
            if (r0 == 0) goto L_0x0016
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            r1 = 2131887059(0x7f1203d3, float:1.9408714E38)
            java.lang.String r3 = com.fossil.Um5.c(r0, r1)
            java.lang.String r0 = "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)"
            com.mapped.Wg6.b(r3, r0)
        L_0x0016:
            r0 = r5 & 2
            if (r0 == 0) goto L_0x001c
            java.lang.String r4 = ""
        L_0x001c:
            r2.<init>(r3, r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.news.notifications.FossilNotificationBar.<init>(java.lang.String, java.lang.String, int, com.mapped.Qg6):void");
    }
}
