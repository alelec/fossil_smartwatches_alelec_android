package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$1", f = "CloudImageHelper.kt", l = {93, 98}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$download$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$1$1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$download$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageHelper$ItemImage$download$Anon1 cloudImageHelper$ItemImage$download$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageHelper$ItemImage$download$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = CloudImageHelper.Companion.getTAG();
                local.d(tag, "withContext, mSerialNumber=" + this.this$0.this$0.mSerialNumber);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = CloudImageHelper.Companion.getTAG();
                    local2.d(tag2, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    WeakReference weakReference = this.this$0.this$0.mWeakReferenceImageView;
                    if (weakReference != null) {
                        ImageView imageView = (ImageView) weakReference.get();
                        if (imageView != null) {
                            Integer num = this.this$0.this$0.mResourceId;
                            if (num != null) {
                                imageView.setImageResource(num.intValue());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File file = this.this$0.this$0.mFile;
                if (file != null) {
                    String str = this.this$0.this$0.mSerialNumber;
                    if (str != null) {
                        String str2 = this.this$0.this$0.mSerialPrefix;
                        if (str2 != null) {
                            CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(file, str, str2, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.DEVICE, this.this$0.this$0.mDeviceType.getType(), this.this$0.this$0.mListener));
                            return Cd6.a;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$download$Anon1(CloudImageHelper.ItemImage itemImage, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = itemImage;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        CloudImageHelper$ItemImage$download$Anon1 cloudImageHelper$ItemImage$download$Anon1 = new CloudImageHelper$ItemImage$download$Anon1(this.this$0, xe6);
        cloudImageHelper$ItemImage$download$Anon1.p$ = (Il6) obj;
        throw null;
        //return cloudImageHelper$ItemImage$download$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((CloudImageHelper$ItemImage$download$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b8  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
            r12 = this;
            r11 = 2
            r7 = 1
            r10 = 0
            java.lang.Object r9 = com.fossil.Yn7.d()
            int r0 = r12.label
            if (r0 == 0) goto L_0x005c
            if (r0 == r7) goto L_0x0021
            if (r0 != r11) goto L_0x0019
            java.lang.Object r0 = r12.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r13)
        L_0x0016:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0018:
            return r0
        L_0x0019:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r12.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r13)
            r2 = r0
            r1 = r13
        L_0x002a:
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x00b8
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.cloudimage.CloudImageHelper$Companion r1 = com.portfolio.platform.cloudimage.CloudImageHelper.Companion
            java.lang.String r1 = r1.getTAG()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "file is exist, mSerialNumber="
            r2.append(r3)
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r3 = r12.this$0
            java.lang.String r3 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMSerialNumber$p(r3)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0018
        L_0x005c:
            com.fossil.El7.b(r13)
            com.mapped.Il6 r8 = r12.p$
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r12.this$0
            java.io.File r0 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r0)
            if (r0 != 0) goto L_0x0078
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r12.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper r1 = com.portfolio.platform.cloudimage.CloudImageHelper.this
            com.portfolio.platform.PortfolioApp r1 = r1.getMApp()
            java.io.File r1 = r1.getFilesDir()
            com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$setMFile$p(r0, r1)
        L_0x0078:
            com.portfolio.platform.cloudimage.AssetUtil r0 = com.portfolio.platform.cloudimage.AssetUtil.INSTANCE
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r1 = r12.this$0
            java.io.File r1 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r1)
            if (r1 == 0) goto L_0x00d2
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r2 = r12.this$0
            java.lang.String r2 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMSerialNumber$p(r2)
            if (r2 == 0) goto L_0x00ce
            com.portfolio.platform.cloudimage.ResolutionHelper r3 = com.portfolio.platform.cloudimage.ResolutionHelper.INSTANCE
            com.portfolio.platform.cloudimage.Constants$Resolution r3 = r3.getResolutionFromDevice()
            java.lang.String r3 = r3.getResolution()
            com.portfolio.platform.cloudimage.Constants$Feature r4 = com.portfolio.platform.cloudimage.Constants.Feature.DEVICE
            java.lang.String r4 = r4.getFeature()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r5 = r12.this$0
            com.portfolio.platform.cloudimage.Constants$DeviceType r5 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMDeviceType$p(r5)
            java.lang.String r5 = r5.getType()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r6 = r12.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r6 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMListener$p(r6)
            r12.L$0 = r8
            r12.label = r7
            r7 = r12
            java.lang.Object r1 = r0.checkAssetExist(r1, r2, r3, r4, r5, r6, r7)
            if (r1 != r9) goto L_0x00d6
            r0 = r9
            goto L_0x0018
        L_0x00b8:
            com.fossil.Jx7 r0 = com.fossil.Bw7.c()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$Anon1$Anon1_Level2 r1 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$Anon1$Anon1_Level2
            r1.<init>(r12, r10)
            r12.L$0 = r2
            r12.label = r11
            java.lang.Object r0 = com.fossil.Eu7.g(r0, r1, r12)
            if (r0 != r9) goto L_0x0016
            r0 = r9
            goto L_0x0018
        L_0x00ce:
            com.mapped.Wg6.i()
            throw r10
        L_0x00d2:
            com.mapped.Wg6.i()
            throw r10
        L_0x00d6:
            r2 = r8
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$download$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
