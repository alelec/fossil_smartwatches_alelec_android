package com.portfolio.platform.ui.profile.usecase;

import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Hr7;
import com.fossil.Rt7;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.model.request.CustomField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetZendeskInformation extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ long h; // = (Wg6.a("release", "release") ? 25044663 : 360000146526L);
    @DexIgnore
    public static /* final */ long i; // = (Wg6.a("release", "release") ? 23451689 : 24435966);
    @DexIgnore
    public static /* final */ long j; // = (Wg6.a("release", "release") ? 360000024083L : 360000148503L);
    @DexIgnore
    public static /* final */ long k; // = (Wg6.a("release", "release") ? 24416029 : 24436006);
    @DexIgnore
    public static /* final */ long l; // = (Wg6.a("release", "release") ? 24504683 : 24881463);
    @DexIgnore
    public static /* final */ long m; // = (Wg6.a("release", "release") ? 23777683 : 24435986);
    @DexIgnore
    public static /* final */ long n; // = (Wg6.a("release", "release") ? 24545186 : 24881503);
    @DexIgnore
    public static /* final */ long o; // = (Wg6.a("release", "release") ? 24545246 : 24945726);
    @DexIgnore
    public static /* final */ long p; // = (Wg6.a("release", "release") ? 23780847 : 24436086);
    @DexIgnore
    public static /* final */ long q; // = (Wg6.a("release", "release") ? 24935086 : 24881543);
    @DexIgnore
    public static /* final */ long r; // = (Wg6.a("release", "release") ? 24506223 : 24881523);
    @DexIgnore
    public static /* final */ long s; // = (Wg6.a("release", "release") ? 360000156783L : 360000156723L);
    @DexIgnore
    public static /* final */ long t; // = (Wg6.a("release", "release") ? 360000156803L : 360000156743L);
    @DexIgnore
    public static /* final */ long u; // = (Wg6.a("release", "release") ? 360000156823L : 360000156763L);
    @DexIgnore
    public static /* final */ long v; // = (Wg6.a("release", "release") ? 60000157103L : 360000148563L);
    @DexIgnore
    public static /* final */ long w; // = (Wg6.a("release", "release") ? 360050695374L : 360053812213L);
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ An4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Bi(String str) {
            Wg6.c(str, "subject");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ List<CustomField> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public Ci(List<CustomField> list, List<String> list2, String str, String str2, String str3, String str4) {
            Wg6.c(list, "customFieldList");
            Wg6.c(list2, "tags");
            Wg6.c(str, Constants.EMAIL);
            Wg6.c(str2, "userName");
            Wg6.c(str3, "subject");
            Wg6.c(str4, "additionalInfo");
            this.a = list;
            this.b = list2;
            this.c = str;
            this.d = str2;
            this.e = str3;
            this.f = str4;
        }

        @DexIgnore
        public final String a() {
            return this.f;
        }

        @DexIgnore
        public final List<CustomField> b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final List<String> e() {
            return this.b;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.profile.usecase.GetZendeskInformation", f = "GetZendeskInformation.kt", l = {26}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetZendeskInformation this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(GetZendeskInformation getZendeskInformation, Xe6 xe6) {
            super(xe6);
            this.this$0 = getZendeskInformation;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    /*
    static {
        String simpleName = GetZendeskInformation.class.getSimpleName();
        Wg6.b(simpleName, "GetZendeskInformation::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public GetZendeskInformation(UserRepository userRepository, DeviceRepository deviceRepository, An4 an4) {
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = deviceRepository;
        this.f = an4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return n(bi, xe6);
    }

    @DexIgnore
    public final StringBuilder m(String str, String str2, String str3, String str4, String str5, String str6) {
        StringBuilder sb = new StringBuilder();
        String str7 = Build.DEVICE + " - " + Build.MODEL;
        String str8 = Build.VERSION.RELEASE;
        StringBuilder sb2 = new StringBuilder();
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        sb2.append(locale.getLanguage());
        sb2.append("_t");
        String sb3 = sb2.toString();
        String str9 = "";
        String str10 = "";
        String str11 = "";
        for (T t2 : this.e.getAllDevice()) {
            str10 = str10 + " " + t2.getDeviceId();
            if (t2.isActive()) {
                long C = this.f.C(t2.getDeviceId());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str9);
                Hr7 hr7 = Hr7.a;
                String format = String.format(" %d (%s)", Arrays.copyOf(new Object[]{Long.valueOf(C / ((long) 1000)), TimeUtils.g(new Date(C))}, 2));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                sb4.append(format);
                str9 = sb4.toString();
                if (!TextUtils.isEmpty(t2.getActivationDate())) {
                    String k2 = TimeUtils.k(TimeUtils.r0(t2.getActivationDate()));
                    Wg6.b(k2, "DateHelper.formatShortDa\u2026e(device.activationDate))");
                    str11 = k2;
                } else {
                    str11 = "";
                }
            }
        }
        sb.append("App Name: " + str);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("App Version: " + str2);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Build number: 27171-2020-11-21");
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Phone Info: " + str7);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("System version: " + str8);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Host Maker: " + str4);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Host Model: " + str5);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Carrier: " + str6);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Language code: " + sb3);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("_______________");
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Serial ");
        if (str3 == null) {
            str3 = "";
        }
        sb5.append(str3);
        sb.append(sb5.toString());
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Last successful sync: " + str9);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("List of paired device: " + str10);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        sb.append("Activation date: " + str11);
        Wg6.b(sb, "append(value)");
        Rt7.b(sb);
        return sb;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0301  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0328  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e9 A[SYNTHETIC, Splitter:B:37:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x02e6 A[SYNTHETIC, Splitter:B:94:0x02e6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object n(com.portfolio.platform.ui.profile.usecase.GetZendeskInformation.Bi r31, com.mapped.Xe6<? super com.mapped.Cd6> r32) {
        /*
        // Method dump skipped, instructions count: 820
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.profile.usecase.GetZendeskInformation.n(com.portfolio.platform.ui.profile.usecase.GetZendeskInformation$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
