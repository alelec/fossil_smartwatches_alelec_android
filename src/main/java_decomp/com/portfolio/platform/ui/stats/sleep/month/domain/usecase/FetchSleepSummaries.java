package com.portfolio.platform.ui.stats.sleep.month.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FetchSleepSummaries extends CoroutineUseCase<Ai, CoroutineUseCase.Di, CoroutineUseCase.Ai> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SleepSummariesRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;
    @DexIgnore
    public /* final */ FitnessDataRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public Ai(Date date) {
            Wg6.c(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries", f = "FetchSleepSummaries.kt", l = {29, 57, 58, 61}, m = "run")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FetchSleepSummaries this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FetchSleepSummaries fetchSleepSummaries, Xe6 xe6) {
            super(xe6);
            this.this$0 = fetchSleepSummaries;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    /*
    static {
        String simpleName = FetchSleepSummaries.class.getSimpleName();
        Wg6.b(simpleName, "FetchSleepSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public FetchSleepSummaries(SleepSummariesRepository sleepSummariesRepository, UserRepository userRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository) {
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        this.d = sleepSummariesRepository;
        this.e = userRepository;
        this.f = sleepSessionsRepository;
        this.g = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return h;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries.Ai r23, com.mapped.Xe6<? super com.mapped.Cd6> r24) {
        /*
        // Method dump skipped, instructions count: 627
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries.m(com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
