package com.portfolio.platform.ui.user.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResetPasswordUseCase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ AuthApiGuestService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ai(String str) {
            Wg6.c(str, Constants.EMAIL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Bi(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase", f = "ResetPasswordUseCase.kt", l = {27}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ResetPasswordUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ResetPasswordUseCase resetPasswordUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = resetPasswordUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$run$response$1", f = "ResetPasswordUseCase.kt", l = {27}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Hg6<Xe6<? super Q88<Void>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ResetPasswordUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ResetPasswordUseCase resetPasswordUseCase, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = resetPasswordUseCase;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ei(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Void>> xe6) {
            throw null;
            //return ((Ei) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                AuthApiGuestService authApiGuestService = this.this$0.d;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object passwordRequestReset = authApiGuestService.passwordRequestReset(ku3, this);
                return passwordRequestReset == d ? d : passwordRequestReset;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = ResetPasswordUseCase.class.getSimpleName();
        Wg6.b(simpleName, "ResetPasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ResetPasswordUseCase(AuthApiGuestService authApiGuestService) {
        Wg6.c(authApiGuestService, "mApiGuestService");
        this.d = authApiGuestService;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return n(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object n(com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase.Ai r10, com.mapped.Xe6<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 257
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase.n(com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
