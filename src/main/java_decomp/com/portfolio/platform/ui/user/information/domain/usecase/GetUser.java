package com.portfolio.platform.ui.user.information.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetUser extends CoroutineUseCase<CoroutineUseCase.Bi, Ai, CoroutineUseCase.Ai> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public Ai(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.information.domain.usecase.GetUser", f = "GetUser.kt", l = {13}, m = "run")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetUser this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(GetUser getUser, Xe6 xe6) {
            super(xe6);
            this.this$0 = getUser;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public GetUser(UserRepository userRepository) {
        Wg6.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "GetUser";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    @Override // com.portfolio.platform.CoroutineUseCase
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.portfolio.platform.CoroutineUseCase.Bi r6, com.mapped.Xe6<java.lang.Object> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.ui.user.information.domain.usecase.GetUser.Bi
            if (r0 == 0) goto L_0x0035
            r0 = r7
            com.portfolio.platform.ui.user.information.domain.usecase.GetUser$Bi r0 = (com.portfolio.platform.ui.user.information.domain.usecase.GetUser.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0035
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0044
            if (r0 != r4) goto L_0x003c
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.CoroutineUseCase$Bi r0 = (com.portfolio.platform.CoroutineUseCase.Bi) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.ui.user.information.domain.usecase.GetUser r0 = (com.portfolio.platform.ui.user.information.domain.usecase.GetUser) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002c:
            com.portfolio.platform.ui.user.information.domain.usecase.GetUser$Ai r1 = new com.portfolio.platform.ui.user.information.domain.usecase.GetUser$Ai
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            r1.<init>(r0)
            r0 = r1
        L_0x0034:
            return r0
        L_0x0035:
            com.portfolio.platform.ui.user.information.domain.usecase.GetUser$Bi r0 = new com.portfolio.platform.ui.user.information.domain.usecase.GetUser$Bi
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.UserRepository r0 = r5.d
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r0 = r0.getCurrentUser(r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.information.domain.usecase.GetUser.k(com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
