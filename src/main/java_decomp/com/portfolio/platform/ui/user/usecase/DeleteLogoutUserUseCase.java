package com.portfolio.platform.ui.user.usecase;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Ko7;
import com.fossil.Tt4;
import com.fossil.Um5;
import com.fossil.Vn5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Kk4;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.domain.NotificationRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteLogoutUserUseCase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ Ai T; // = new Ai(null);
    @DexIgnore
    public /* final */ HeartRateSummaryRepository A;
    @DexIgnore
    public /* final */ WorkoutSessionRepository B;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase C;
    @DexIgnore
    public /* final */ ProfileRepository D;
    @DexIgnore
    public /* final */ FriendRepository E;
    @DexIgnore
    public /* final */ Tt4 F;
    @DexIgnore
    public /* final */ NotificationRepository G;
    @DexIgnore
    public /* final */ FileRepository H;
    @DexIgnore
    public /* final */ QuickResponseRepository I;
    @DexIgnore
    public /* final */ WatchFaceRepository J;
    @DexIgnore
    public /* final */ RingStyleRepository K;
    @DexIgnore
    public /* final */ FCMRepository L;
    @DexIgnore
    public /* final */ WorkoutSettingRepository M;
    @DexIgnore
    public /* final */ DianaAppSettingRepository N;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository O;
    @DexIgnore
    public /* final */ WatchAppDataRepository P;
    @DexIgnore
    public /* final */ DianaPresetRepository Q;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository R;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;
    @DexIgnore
    public /* final */ An4 f;
    @DexIgnore
    public /* final */ HybridPresetRepository g;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ MicroAppSettingRepository j;
    @DexIgnore
    public /* final */ NotificationsRepository k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;
    @DexIgnore
    public /* final */ Vn5 o;
    @DexIgnore
    public /* final */ Kk4 p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ com.portfolio.platform.data.source.DianaPresetRepository r;
    @DexIgnore
    public /* final */ WatchAppRepository s;
    @DexIgnore
    public /* final */ ComplicationRepository t;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;
    @DexIgnore
    public /* final */ MicroAppRepository w;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository x;
    @DexIgnore
    public /* final */ FitnessDataRepository y;
    @DexIgnore
    public /* final */ HeartRateSampleRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return DeleteLogoutUserUseCase.S;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ WeakReference<Activity> b;

        @DexIgnore
        public Bi(int i, WeakReference<Activity> weakReference) {
            this.a = i;
            this.b = weakReference;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final WeakReference<Activity> b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Ci(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", f = "DeleteLogoutUserUseCase.kt", l = {180, 181, 182, 186, 188, 195, 196, 197, 198, Action.Music.MUSIC_END_ACTION, 200, Action.Selfie.TAKE_BURST, 209, 210, 211, 212, 213, 214, 216, 219, 220, 224, 225, 226}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(DeleteLogoutUserUseCase deleteLogoutUserUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a2  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00cb  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00f0  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0144  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0160  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x017c  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01da  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x01ff  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x021b  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0237  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0253  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x026f  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x028a  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x02c5  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x02e0  */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0304  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x031f  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x033a  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x003e  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 1014
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1", f = "DeleteLogoutUserUseCase.kt", l = {148}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1$1", f = "DeleteLogoutUserUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.I.removeAll();
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886131);
                    Wg6.b(c, "LanguageHelper.getString\u2026an_Text__CanICallYouBack)");
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(c);
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886132);
                    Wg6.b(c2, "LanguageHelper.getString\u2026_MessageOn_Text__OnMyWay)");
                    QuickResponseMessage quickResponseMessage2 = new QuickResponseMessage(c2);
                    String c3 = Um5.c(PortfolioApp.get.instance(), 2131886133);
                    Wg6.b(c3, "LanguageHelper.getString\u2026y_Text__SorryCantTalkNow)");
                    this.this$0.this$0.I.insertQRs(Hm7.h(quickResponseMessage, quickResponseMessage2, new QuickResponseMessage(c3)));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(DeleteLogoutUserUseCase deleteLogoutUserUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.K1(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {89, 98, 100, 119, 134}, m = "run")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteLogoutUserUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(DeleteLogoutUserUseCase deleteLogoutUserUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = deleteLogoutUserUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e0(null, this);
        }
    }

    /*
    static {
        String simpleName = DeleteLogoutUserUseCase.class.getSimpleName();
        Wg6.b(simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        S = simpleName;
    }
    */

    @DexIgnore
    public DeleteLogoutUserUseCase(UserRepository userRepository, AlarmsRepository alarmsRepository, An4 an4, HybridPresetRepository hybridPresetRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, MicroAppSettingRepository microAppSettingRepository, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, SleepSessionsRepository sleepSessionsRepository, GoalTrackingRepository goalTrackingRepository, Vn5 vn5, Kk4 kk4, SleepSummariesRepository sleepSummariesRepository, com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, NotificationSettingsDatabase notificationSettingsDatabase, DNDSettingsDatabase dNDSettingsDatabase, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository, FitnessDataRepository fitnessDataRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, RemindersSettingsDatabase remindersSettingsDatabase, ProfileRepository profileRepository, FriendRepository friendRepository, Tt4 tt4, NotificationRepository notificationRepository, FileRepository fileRepository, QuickResponseRepository quickResponseRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, FCMRepository fCMRepository, WorkoutSettingRepository workoutSettingRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository, WatchAppDataRepository watchAppDataRepository, DianaPresetRepository dianaPresetRepository2, WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(alarmsRepository, "mAlarmRepository");
        Wg6.c(an4, "mSharedPreferences");
        Wg6.c(hybridPresetRepository, "mPresetRepository");
        Wg6.c(activitiesRepository, "mActivitiesRepository");
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(microAppSettingRepository, "mMicroAppSettingRepository");
        Wg6.c(notificationsRepository, "mNotificationRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(vn5, "mLoginGoogleManager");
        Wg6.c(kk4, "mGoogleFitHelper");
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        Wg6.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        Wg6.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        Wg6.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        Wg6.c(profileRepository, "mSocialProfileRepository");
        Wg6.c(friendRepository, "mSocialFriendRepository");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(notificationRepository, "notificationRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(quickResponseRepository, "mQuickResponseRepository");
        Wg6.c(watchFaceRepository, "mWatchFaceRepository");
        Wg6.c(ringStyleRepository, "mRingStyleRepository");
        Wg6.c(fCMRepository, "fcmRepository");
        Wg6.c(workoutSettingRepository, "mWorkoutSettingRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        Wg6.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        Wg6.c(watchAppDataRepository, "mWatchAppDataRepository");
        Wg6.c(dianaPresetRepository2, "dianaPresetRepository");
        Wg6.c(wFBackgroundPhotoRepository, "photoWFBackgroundPhotoRepository");
        this.d = userRepository;
        this.e = alarmsRepository;
        this.f = an4;
        this.g = hybridPresetRepository;
        this.h = activitiesRepository;
        this.i = summariesRepository;
        this.j = microAppSettingRepository;
        this.k = notificationsRepository;
        this.l = deviceRepository;
        this.m = sleepSessionsRepository;
        this.n = goalTrackingRepository;
        this.o = vn5;
        this.p = kk4;
        this.q = sleepSummariesRepository;
        this.r = dianaPresetRepository;
        this.s = watchAppRepository;
        this.t = complicationRepository;
        this.u = notificationSettingsDatabase;
        this.v = dNDSettingsDatabase;
        this.w = microAppRepository;
        this.x = microAppLastSettingRepository;
        this.y = fitnessDataRepository;
        this.z = heartRateSampleRepository;
        this.A = heartRateSummaryRepository;
        this.B = workoutSessionRepository;
        this.C = remindersSettingsDatabase;
        this.D = profileRepository;
        this.E = friendRepository;
        this.F = tt4;
        this.G = notificationRepository;
        this.H = fileRepository;
        this.I = quickResponseRepository;
        this.J = watchFaceRepository;
        this.K = ringStyleRepository;
        this.L = fCMRepository;
        this.M = workoutSettingRepository;
        this.N = dianaAppSettingRepository;
        this.O = dianaWatchFaceRepository;
        this.P = watchAppDataRepository;
        this.Q = dianaPresetRepository2;
        this.R = wFBackgroundPhotoRepository;
    }

    @DexIgnore
    public final void b0(Bi bi) {
        this.o.f(bi.b());
        try {
            IButtonConnectivity b = PortfolioApp.get.b();
            if (b != null) {
                b.deviceUnlink(PortfolioApp.get.instance().J());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        Rm6 unused = Gu7.d(g(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final void c0() {
        if (this.p.e()) {
            this.p.j();
        }
    }

    @DexIgnore
    public final void d0() {
        Rm6 unused = Gu7.d(g(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0229  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object e0(com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase.Bi r11, com.mapped.Xe6<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 572
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase.e0(com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return S;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return e0(bi, xe6);
    }
}
