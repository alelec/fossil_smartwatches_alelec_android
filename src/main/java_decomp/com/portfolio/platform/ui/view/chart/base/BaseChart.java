package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseChart extends ViewGroup {
    @DexIgnore
    public static /* final */ b v; // = new b(null);
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public c l;
    @DexIgnore
    public e m;
    @DexIgnore
    public a s;
    @DexIgnore
    public d t;
    @DexIgnore
    public /* final */ String u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BaseChart baseChart, Context context) {
            super(context);
            Wg6.c(context, "context");
            this.b = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            Wg6.c(canvas, "canvas");
            super.onDraw(canvas);
            this.b.c(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.b.d(i, i2, i3, i4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final boolean a(PointF pointF, RectF rectF) {
            Wg6.c(pointF, "point");
            Wg6.c(rectF, "rect");
            float f = pointF.x;
            if (f <= rectF.right && f >= rectF.left) {
                float f2 = pointF.y;
                return f2 <= rectF.bottom && f2 >= rectF.top;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BaseChart baseChart, Context context) {
            super(context);
            Wg6.c(context, "context");
            this.b = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            Wg6.c(canvas, "canvas");
            super.onDraw(canvas);
            this.b.f(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.b.setMGraphWidth(i);
            this.b.setMGraphHeight(i2);
            this.b.j(i, i2, i3, i4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(BaseChart baseChart, Context context) {
            super(context);
            Wg6.c(context, "context");
            this.b = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            Wg6.c(canvas, "canvas");
            super.onDraw(canvas);
            this.b.g(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.b.h(i, i2, i3, i4);
        }

        @DexIgnore
        public boolean onTouchEvent(MotionEvent motionEvent) {
            Wg6.c(motionEvent, Constants.EVENT);
            return this.b.i(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends View {
        @DexIgnore
        public /* final */ /* synthetic */ BaseChart b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(BaseChart baseChart, Context context) {
            super(context);
            Wg6.c(context, "context");
            this.b = baseChart;
        }

        @DexIgnore
        public void onDraw(Canvas canvas) {
            Wg6.c(canvas, "canvas");
            super.onDraw(canvas);
            this.b.k(canvas);
        }

        @DexIgnore
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.b.setMLegendWidth(i);
            this.b.l(i, i2, i3, i4);
        }
    }

    @DexIgnore
    public BaseChart(Context context) {
        super(context);
        String simpleName = getClass().getSimpleName();
        Wg6.b(simpleName, "this::class.java.simpleName");
        this.u = simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String simpleName = getClass().getSimpleName();
        Wg6.b(simpleName, "this::class.java.simpleName");
        this.u = simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        String simpleName = getClass().getSimpleName();
        Wg6.b(simpleName, "this::class.java.simpleName");
        this.u = simpleName;
    }

    @DexIgnore
    public BaseChart(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        String simpleName = getClass().getSimpleName();
        Wg6.b(simpleName, "this::class.java.simpleName");
        this.u = simpleName;
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(this.u, "initGraph");
        Context context = getContext();
        Wg6.b(context, "context");
        c cVar = new c(this, context);
        this.l = cVar;
        if (cVar != null) {
            addView(cVar);
            Context context2 = getContext();
            Wg6.b(context2, "context");
            e eVar = new e(this, context2);
            this.m = eVar;
            if (eVar != null) {
                addView(eVar);
                Context context3 = getContext();
                Wg6.b(context3, "context");
                a aVar = new a(this, context3);
                this.s = aVar;
                if (aVar != null) {
                    addView(aVar);
                    Context context4 = getContext();
                    Wg6.b(context4, "context");
                    d dVar = new d(this, context4);
                    this.t = dVar;
                    if (dVar != null) {
                        addView(dVar);
                    } else {
                        Wg6.n("mGraphOverlay");
                        throw null;
                    }
                } else {
                    Wg6.n("mBackground");
                    throw null;
                }
            } else {
                Wg6.n("mLegend");
                throw null;
            }
        } else {
            Wg6.n("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public final void b() {
        c cVar = this.l;
        if (cVar != null) {
            cVar.invalidate();
            e eVar = this.m;
            if (eVar != null) {
                eVar.invalidate();
                a aVar = this.s;
                if (aVar != null) {
                    aVar.invalidate();
                    d dVar = this.t;
                    if (dVar != null) {
                        dVar.invalidate();
                    } else {
                        Wg6.n("mGraphOverlay");
                        throw null;
                    }
                } else {
                    Wg6.n("mBackground");
                    throw null;
                }
            } else {
                Wg6.n("mLegend");
                throw null;
            }
        } else {
            Wg6.n("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public void c(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }

    @DexIgnore
    public void d(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public final void e() {
        b();
    }

    @DexIgnore
    public void f(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }

    @DexIgnore
    public void g(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }

    @DexIgnore
    public final a getMBackground() {
        a aVar = this.s;
        if (aVar != null) {
            return aVar;
        }
        Wg6.n("mBackground");
        throw null;
    }

    @DexIgnore
    public final int getMBottomPadding() {
        return this.g;
    }

    @DexIgnore
    public final c getMGraph() {
        c cVar = this.l;
        if (cVar != null) {
            return cVar;
        }
        Wg6.n("mGraph");
        throw null;
    }

    @DexIgnore
    public final int getMGraphHeight() {
        return this.i;
    }

    @DexIgnore
    public final d getMGraphOverlay() {
        d dVar = this.t;
        if (dVar != null) {
            return dVar;
        }
        Wg6.n("mGraphOverlay");
        throw null;
    }

    @DexIgnore
    public final int getMGraphWidth() {
        return this.h;
    }

    @DexIgnore
    public final int getMHeight() {
        return this.b;
    }

    @DexIgnore
    public final int getMLeftPadding() {
        return this.d;
    }

    @DexIgnore
    public final e getMLegend() {
        e eVar = this.m;
        if (eVar != null) {
            return eVar;
        }
        Wg6.n("mLegend");
        throw null;
    }

    @DexIgnore
    public final int getMLegendHeight() {
        return this.k;
    }

    @DexIgnore
    public final int getMLegendWidth() {
        return this.j;
    }

    @DexIgnore
    public final int getMRightPadding() {
        return this.f;
    }

    @DexIgnore
    public final int getMTopPadding() {
        return this.e;
    }

    @DexIgnore
    public final int getMWidth() {
        return this.c;
    }

    @DexIgnore
    public final String getTAG() {
        return this.u;
    }

    @DexIgnore
    public void h(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public boolean i(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void j(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void k(Canvas canvas) {
        Wg6.c(canvas, "canvas");
    }

    @DexIgnore
    public void l(int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.c = i2;
        this.b = i3;
        this.d = getPaddingLeft();
        this.e = getPaddingTop();
        this.f = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        this.g = paddingBottom;
        c cVar = this.l;
        if (cVar != null) {
            cVar.layout(this.d, this.e, i2 - this.f, (i3 - this.k) - paddingBottom);
            e eVar = this.m;
            if (eVar != null) {
                int i6 = this.d;
                int i7 = this.k;
                int i8 = this.g;
                eVar.layout(i6, (i3 - i7) - i8, i2 - this.f, i3 - i8);
                a aVar = this.s;
                if (aVar != null) {
                    aVar.layout(this.d, this.e, i2 - this.f, i3 - this.g);
                    d dVar = this.t;
                    if (dVar != null) {
                        dVar.layout(this.d, this.e, i2 - this.f, (i3 - this.k) - this.g);
                    } else {
                        Wg6.n("mGraphOverlay");
                        throw null;
                    }
                } else {
                    Wg6.n("mBackground");
                    throw null;
                }
            } else {
                Wg6.n("mLegend");
                throw null;
            }
        } else {
            Wg6.n("mGraph");
            throw null;
        }
    }

    @DexIgnore
    public final void setMBackground(a aVar) {
        Wg6.c(aVar, "<set-?>");
        this.s = aVar;
    }

    @DexIgnore
    public final void setMBottomPadding(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public final void setMGraph(c cVar) {
        Wg6.c(cVar, "<set-?>");
        this.l = cVar;
    }

    @DexIgnore
    public final void setMGraphHeight(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public final void setMGraphOverlay(d dVar) {
        Wg6.c(dVar, "<set-?>");
        this.t = dVar;
    }

    @DexIgnore
    public final void setMGraphWidth(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void setMHeight(int i2) {
        this.b = i2;
    }

    @DexIgnore
    public final void setMLeftPadding(int i2) {
        this.d = i2;
    }

    @DexIgnore
    public final void setMLegend(e eVar) {
        Wg6.c(eVar, "<set-?>");
        this.m = eVar;
    }

    @DexIgnore
    public final void setMLegendHeight(int i2) {
        this.k = i2;
    }

    @DexIgnore
    public final void setMLegendWidth(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void setMRightPadding(int i2) {
        this.f = i2;
    }

    @DexIgnore
    public final void setMTopPadding(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final void setMWidth(int i2) {
        this.c = i2;
    }
}
