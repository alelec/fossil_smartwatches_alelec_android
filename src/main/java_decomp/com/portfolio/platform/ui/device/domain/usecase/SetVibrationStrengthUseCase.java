package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Tk5;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetVibrationStrengthUseCase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ Ei g; // = new Ei();
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SetVibrationStrengthUseCase.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Bi(String str, int i) {
            Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Ci(int i, int i2, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object updateDevice;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Device deviceBySerial = SetVibrationStrengthUseCase.this.h.getDeviceBySerial(SetVibrationStrengthUseCase.this.q());
                    if (deviceBySerial != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = SetVibrationStrengthUseCase.j.a();
                        local.d(a2, "Update vibration stregnth " + SetVibrationStrengthUseCase.this.p() + " to db");
                        deviceBySerial.setVibrationStrength(Ao7.e(SetVibrationStrengthUseCase.this.p()));
                        DeviceRepository deviceRepository = SetVibrationStrengthUseCase.this.h;
                        this.L$0 = il6;
                        this.L$1 = deviceBySerial;
                        this.L$2 = deviceBySerial;
                        this.label = 1;
                        updateDevice = deviceRepository.updateDevice(deviceBySerial, false, this);
                        if (updateDevice == d) {
                            return d;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
                    SetVibrationStrengthUseCase.this.j(new Di());
                    return Cd6.a;
                } else if (i == 1) {
                    Device device = (Device) this.L$2;
                    Device device2 = (Device) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    updateDevice = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Ap4 ap4 = (Ap4) updateDevice;
                FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
                SetVibrationStrengthUseCase.this.j(new Di());
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SetVibrationStrengthUseCase.j.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + SetVibrationStrengthUseCase.this.r() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.SET_VIBRATION_STRENGTH && SetVibrationStrengthUseCase.this.r()) {
                SetVibrationStrengthUseCase.this.u(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    Rm6 unused = Gu7.d(SetVibrationStrengthUseCase.this.g(), null, null, new Aii(this, null), 3, null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                SetVibrationStrengthUseCase.this.i(new Ci(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        String simpleName = SetVibrationStrengthUseCase.class.getSimpleName();
        Wg6.b(simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SetVibrationStrengthUseCase(DeviceRepository deviceRepository, An4 an4) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.h = deviceRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return i;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return t(bi, xe6);
    }

    @DexIgnore
    public final int p() {
        return this.e;
    }

    @DexIgnore
    public final String q() {
        return this.f;
    }

    @DexIgnore
    public final boolean r() {
        return this.d;
    }

    @DexIgnore
    public final void s() {
        BleCommandResultManager.d.e(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public Object t(Bi bi, Xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d(i, "running UseCase");
            this.d = true;
            Integer e2 = bi != null ? Ao7.e(bi.b()) : null;
            if (e2 != null) {
                this.e = e2.intValue();
                this.f = bi.a();
                PortfolioApp.get.instance().L1(bi.a(), new VibrationStrengthObj(Tk5.b(bi.b()), false, 2, null));
                return new Object();
            }
            Wg6.i();
            throw null;
        } catch (Exception e3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.e(str, "Error inside " + i + ".connectDevice - e=" + e3);
            return new Ci(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void v() {
        BleCommandResultManager.d.j(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }
}
