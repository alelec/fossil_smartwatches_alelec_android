package com.portfolio.platform.ui.device.domain.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Mn5;
import com.fossil.Vt7;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareUsecase extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ Ai g; // = new Ai(null);
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ An4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final FirmwareData a(An4 an4, String str) {
            Wg6.c(an4, "sharedPreferencesManager");
            Wg6.c(str, "deviceSKU");
            if (Vt7.j("release", "release", true) || !an4.Y()) {
                Firmware e = Mn5.p.a().g().e(str);
                if (e != null) {
                    return FirmwareFactory.getInstance().createFirmwareData(e.getVersionNumber(), e.getDeviceModel(), e.getChecksum());
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = b();
                local.e(b, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            Firmware k = an4.k(str);
            if (k != null) {
                return FirmwareFactory.getInstance().createFirmwareData(k.getVersionNumber(), k.getDeviceModel(), k.getChecksum());
            }
            Firmware e2 = Mn5.p.a().g().e(str);
            if (e2 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(e2.getVersionNumber(), e2.getDeviceModel(), e2.getChecksum());
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String b2 = b();
            local2.e(b2, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }

        @DexIgnore
        public final String b() {
            return UpdateFirmwareUsecase.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Bi(String str, boolean z) {
            Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Bi(String str, boolean z, int i, Qg6 qg6) {
            this(str, (i & 2) != 0 ? false : z);
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase", f = "UpdateFirmwareUsecase.kt", l = {45, 54}, m = "run")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareUsecase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(UpdateFirmwareUsecase updateFirmwareUsecase, Xe6 xe6) {
            super(xe6);
            this.this$0 = updateFirmwareUsecase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        Wg6.b(simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwareUsecase(DeviceRepository deviceRepository, An4 an4) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = an4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return f;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return n(bi, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f A[Catch:{ Exception -> 0x0168 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object n(com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.Bi r11, com.mapped.Xe6<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 514
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase.n(com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
