package com.portfolio.platform.usecase;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hq5;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Oq5;
import com.fossil.Q88;
import com.fossil.Tq4;
import com.fossil.Uj4;
import com.fossil.Yh5;
import com.fossil.Yn7;
import com.google.android.gms.maps.model.LatLng;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetWeather extends Tq4<Ci, Di, Bi> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return GetWeather.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Ai {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Bi(String str) {
            Wg6.c(str, "errorMessage");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Bi {
        @DexIgnore
        public /* final */ WeatherSettings.TEMP_UNIT a;
        @DexIgnore
        public /* final */ LatLng b;

        @DexIgnore
        public Ci(LatLng latLng, WeatherSettings.TEMP_UNIT temp_unit) {
            Wg6.c(latLng, "latLng");
            Wg6.c(temp_unit, "tempUnit");
            Uj4.b(temp_unit);
            Wg6.b(temp_unit, "checkNotNull(tempUnit)");
            this.a = temp_unit;
            Uj4.b(latLng);
            Wg6.b(latLng, "checkNotNull(latLng)");
            this.b = latLng;
        }

        @DexIgnore
        public final LatLng a() {
            return this.b;
        }

        @DexIgnore
        public final WeatherSettings.TEMP_UNIT b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Tq4.Ci {
        @DexIgnore
        public /* final */ Weather a;

        @DexIgnore
        public Di(Weather weather) {
            Wg6.c(weather, "weather");
            this.a = weather;
        }

        @DexIgnore
        public final Weather a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ci $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetWeather this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1", f = "GetWeather.kt", l = {30}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(1, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                return new Aii(this.this$0, xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
                throw null;
                //return ((Aii) create(xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    ApiServiceV2 apiServiceV2 = this.this$0.this$0.d;
                    double d2 = this.this$0.$requestValues.a().b;
                    double d3 = this.this$0.$requestValues.a().c;
                    String value = this.this$0.$requestValues.b().getValue();
                    this.label = 1;
                    Object weather = apiServiceV2.getWeather(String.valueOf(d2), String.valueOf(d3), value, this);
                    return weather == d ? d : weather;
                } else if (i == 1) {
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(GetWeather getWeather, Ci ci, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = getWeather;
            this.$requestValues = ci;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$requestValues, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d;
            Object d2 = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                d = ResponseKt.d(aii, this);
                if (d == d2) {
                    return d2;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                d = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Ap4 ap4 = (Ap4) d;
            if (ap4 instanceof Kq5) {
                Oq5 oq5 = new Oq5();
                oq5.b((Ku3) ((Kq5) ap4).a());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = GetWeather.f.a();
                local.d(a2, "onSuccess" + oq5.a());
                Tq4.Di b = this.this$0.b();
                Weather a3 = oq5.a();
                Wg6.b(a3, "mfWeatherResponse.weather");
                b.onSuccess(new Di(a3));
            } else if (ap4 instanceof Hq5) {
                this.this$0.b().a(new Bi(Yh5.NETWORK_ERROR.name()));
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = GetWeather.class.getSimpleName();
        Wg6.b(simpleName, "GetWeather::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public GetWeather(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.d = apiServiceV2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Ci ci) {
        i(ci);
    }

    @DexIgnore
    public void i(Ci ci) {
        Wg6.c(ci, "requestValues");
        FLogger.INSTANCE.getLocal().d(e, "executeUseCase");
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ei(this, ci, null), 3, null);
    }
}
