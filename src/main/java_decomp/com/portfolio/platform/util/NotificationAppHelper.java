package com.portfolio.platform.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.Bw7;
import com.fossil.D26;
import com.fossil.D47;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Jq4;
import com.fossil.Ko7;
import com.fossil.Ll5;
import com.fossil.Mn5;
import com.fossil.Pm7;
import com.fossil.V36;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.NotificationHandMovingConfig;
import com.mapped.NotificationVibePattern;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AppHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppHelper {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ NotificationAppHelper b; // = new NotificationAppHelper();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2", f = "NotificationAppHelper.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super List<AppNotificationFilter>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ D26 $getAllContactGroup;
        @DexIgnore
        public /* final */ /* synthetic */ V36 $getApps;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ An4 $sharedPrefs;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1", f = "NotificationAppHelper.kt", l = {99}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $callSettingsType;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $messageSettingsType;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Hh6 hh6, Hh6 hh62, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$callSettingsType = hh6;
                this.$messageSettingsType = hh62;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$callSettingsType, this.$messageSettingsType, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<AppNotificationFilter>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX DEBUG: Failed to insert an additional move for type inference into block B:35:0x007c */
            /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Object] */
            /* JADX WARN: Type inference failed for: r2v3, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r2v4 */
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object a2;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    D26 d26 = this.this$0.$getAllContactGroup;
                    this.L$0 = il6;
                    this.L$1 = arrayList;
                    this.label = 1;
                    a2 = Jq4.a(d26, null, this);
                    if (a2 != d) {
                        d = arrayList;
                    }
                    return d;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    d = (List) this.L$1;
                    a2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                CoroutineUseCase.Ci ci = (CoroutineUseCase.Ci) a2;
                if (ci instanceof D26.Ci) {
                    List j0 = Pm7.j0(((D26.Ci) ci).a());
                    int i2 = this.$callSettingsType.element;
                    if (i2 == 0) {
                        DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                        d.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                    } else if (i2 == 1) {
                        int size = j0.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            ContactGroup contactGroup = (ContactGroup) j0.get(i3);
                            DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                            List<Contact> contacts = contactGroup.getContacts();
                            Wg6.b(contacts, "item.contacts");
                            if (!contacts.isEmpty()) {
                                Contact contact = contactGroup.getContacts().get(0);
                                Wg6.b(contact, "item.contacts[0]");
                                appNotificationFilter.setSender(contact.getDisplayName());
                                d.add(appNotificationFilter);
                            }
                        }
                    }
                    int i4 = this.$messageSettingsType.element;
                    if (i4 == 0) {
                        DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                        d.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                    } else if (i4 == 1) {
                        int size2 = j0.size();
                        for (int i5 = 0; i5 < size2; i5++) {
                            ContactGroup contactGroup2 = (ContactGroup) j0.get(i5);
                            DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            FNotification fNotification = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                            List<Contact> contacts2 = contactGroup2.getContacts();
                            Wg6.b(contacts2, "item.contacts");
                            if (!contacts2.isEmpty()) {
                                AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification);
                                Contact contact2 = contactGroup2.getContacts().get(0);
                                Wg6.b(contact2, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact2.getDisplayName());
                                d.add(appNotificationFilter2);
                            }
                        }
                    }
                }
                return d;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1", f = "NotificationAppHelper.kt", l = {143}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationAllFilterList;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, List list, boolean z, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$notificationAllFilterList = list;
                this.$isAllAppToggleEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<AppNotificationFilter>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object a2;
                ArrayList d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    V36 v36 = this.this$0.$getApps;
                    this.L$0 = il6;
                    this.L$1 = arrayList;
                    this.label = 1;
                    a2 = Jq4.a(v36, null, this);
                    if (a2 != d) {
                        d = arrayList;
                    }
                    return d;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    d = (List) this.L$1;
                    a2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                CoroutineUseCase.Ci ci = (CoroutineUseCase.Ci) a2;
                if (ci instanceof V36.Ai) {
                    this.$notificationAllFilterList.addAll(D47.b(((V36.Ai) ci).a(), this.$isAllAppToggleEnabled));
                }
                return d;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(An4 an4, NotificationSettingsDatabase notificationSettingsDatabase, D26 d26, V36 v36, Xe6 xe6) {
            super(2, xe6);
            this.$sharedPrefs = an4;
            this.$notificationSettingsDatabase = notificationSettingsDatabase;
            this.$getAllContactGroup = d26;
            this.$getApps = v36;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super List<AppNotificationFilter>> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v8, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x020f  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r22) {
            /*
            // Method dump skipped, instructions count: 531
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.util.NotificationAppHelper.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = NotificationAppHelper.class.getSimpleName();
        Wg6.b(simpleName, "NotificationAppHelper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final AppNotificationFilterSettings b(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        ArrayList arrayList = new ArrayList();
        if (sparseArray != null) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            Wg6.b(clone, "it.clone()");
            short s = -1;
            int size = clone.size();
            for (int i = 0; i < size; i++) {
                List<BaseFeatureModel> valueAt = clone.valueAt(i);
                if (valueAt != null) {
                    for (BaseFeatureModel baseFeatureModel : valueAt) {
                        if (baseFeatureModel.isEnabled()) {
                            short e = (short) Ll5.e(baseFeatureModel.getHour());
                            if (baseFeatureModel instanceof ContactGroup) {
                                ContactGroup contactGroup = (ContactGroup) baseFeatureModel;
                                if (contactGroup.getContacts() != null) {
                                    for (Contact contact : contactGroup.getContacts()) {
                                        Wg6.b(contact, "contact");
                                        if (contact.isUseCall()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(e, e, s, 10000);
                                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                                            appNotificationFilter.setSender(contact.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                                            appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (contact.isUseSms()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(e, e, s, 10000);
                                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                                            appNotificationFilter2.setSender(contact.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                                            appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                    }
                                }
                            } else if (baseFeatureModel instanceof AppFilter) {
                                AppFilter appFilter = (AppFilter) baseFeatureModel;
                                String type = appFilter.getType();
                                if (!(type == null || Vt7.l(type))) {
                                    String name = appFilter.getName() == null ? "" : appFilter.getName();
                                    if (z) {
                                        s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                    }
                                    NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(e, e, s, 10000);
                                    Wg6.b(name, "appName");
                                    String type2 = appFilter.getType();
                                    Wg6.b(type2, "item.type");
                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(name, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                    appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
                                    appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                                    arrayList.add(appNotificationFilter3);
                                }
                            }
                        }
                    }
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(a, "buildAppNotificationFilterSettings size = " + arrayList.size());
        return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final Object c(V36 v36, D26 d26, NotificationSettingsDatabase notificationSettingsDatabase, An4 an4, Xe6<? super List<AppNotificationFilter>> xe6) {
        return Eu7.g(Bw7.a(), new Ai(an4, notificationSettingsDatabase, d26, v36, null), xe6);
    }

    @DexIgnore
    public final List<AppNotificationFilter> d(Context context, NotificationSettingsDatabase notificationSettingsDatabase, An4 an4) {
        int i;
        int i2 = 0;
        Wg6.c(context, "context");
        Wg6.c(notificationSettingsDatabase, "notificationSettingsDatabase");
        Wg6.c(an4, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean W = an4.W();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        FLogger.INSTANCE.getLocal().d(a, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + W);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> h = h(i, i2);
        List<AppWrapper> g = g(context);
        arrayList.addAll(h);
        arrayList.addAll(D47.b(g, W));
        return arrayList;
    }

    @DexIgnore
    public final List<AppFilter> e(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? Mn5.p.a().c().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter f(String str, MFDeviceFamily mFDeviceFamily) {
        Wg6.c(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return Mn5.p.a().c().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final List<AppWrapper> g(Context context) {
        List<AppFilter> allAppFilters = Mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.Bi> it = AppHelper.g.g().iterator();
        while (it.hasNext()) {
            AppHelper.Bi next = it.next();
            if (TextUtils.isEmpty(next.b()) || !Vt7.j(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), Boolean.FALSE);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    Wg6.b(next2, "appFilter");
                    if (Wg6.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        return linkedList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> h(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (i == 0) {
            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
            arrayList.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
        } else if (i == 1) {
            int size = allContactGroups.size();
            for (int i3 = 0; i3 < size; i3++) {
                ContactGroup contactGroup = allContactGroups.get(i3);
                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                Wg6.b(contactGroup, "item");
                List<Contact> contacts = contactGroup.getContacts();
                Wg6.b(contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                    Contact contact = contactGroup.getContacts().get(0);
                    Wg6.b(contact, "item.contacts[0]");
                    appNotificationFilter.setSender(contact.getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        if (i2 == 0) {
            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
            arrayList.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
        } else if (i2 == 1) {
            int size2 = allContactGroups.size();
            for (int i4 = 0; i4 < size2; i4++) {
                ContactGroup contactGroup2 = allContactGroups.get(i4);
                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                Wg6.b(contactGroup2, "item");
                List<Contact> contacts2 = contactGroup2.getContacts();
                Wg6.b(contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                    Contact contact2 = contactGroup2.getContacts().get(0);
                    Wg6.b(contact2, "item.contacts[0]");
                    appNotificationFilter2.setSender(contact2.getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean i(String str) {
        Wg6.c(str, "type");
        return f(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final boolean j(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku fromSerialNumber;
        Wg6.c(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            fromSerialNumber = companion.fromType(sku);
        } else {
            fromSerialNumber = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return fromSerialNumber == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }
}
