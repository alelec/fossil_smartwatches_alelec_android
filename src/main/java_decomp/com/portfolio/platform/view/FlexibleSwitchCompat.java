package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.SwitchCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Am0;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleSwitchCompat extends SwitchCompat {
    @DexIgnore
    public String V; // = "";
    @DexIgnore
    public String W; // = "";
    @DexIgnore
    public String a0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context) {
        super(context);
        Wg6.c(context, "context");
        m(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        m(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleSwitchCompat(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        m(attributeSet);
    }

    @DexIgnore
    public final void m(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleSwitchCompat);
            String string = obtainStyledAttributes.getString(2);
            if (string == null) {
                string = "";
            }
            this.V = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.W = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 == null) {
                string3 = "";
            }
            this.a0 = string3;
            obtainStyledAttributes.recycle();
        }
        n();
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void n() {
        if (!TextUtils.isEmpty(this.V) && !TextUtils.isEmpty(this.W) && !TextUtils.isEmpty(this.a0)) {
            String d = ThemeManager.l.a().d(this.V);
            String d2 = ThemeManager.l.a().d(this.W);
            if (!TextUtils.isEmpty(d) && !TextUtils.isEmpty(d2)) {
                Am0.o(getTrackDrawable(), new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(d2), Color.parseColor(d)}));
            }
        }
    }
}
