package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.WindowManager;
import com.mapped.X24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilPopupWindow {
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public WindowManager d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public View f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;

    @DexIgnore
    public FossilPopupWindow() {
        this((View) null, 0, 0);
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.h = 0;
        this.i = true;
        this.j = true;
        this.k = -1;
        this.l = true;
        this.m = true;
        this.o = false;
        this.p = -1;
        this.q = 0;
        this.c = context;
        this.d = (WindowManager) context.getSystemService("window");
        context.obtainStyledAttributes(attributeSet, X24.PopupWindow, i2, i3).recycle();
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3) {
        this(view, i2, i3, false);
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3, boolean z) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.h = 0;
        this.i = true;
        this.j = true;
        this.k = -1;
        this.l = true;
        this.m = true;
        this.o = false;
        this.p = -1;
        this.q = 0;
        if (view != null) {
            Context context = view.getContext();
            this.c = context;
            this.d = (WindowManager) context.getSystemService("window");
        }
        b(view);
        this.g = z;
    }

    @DexIgnore
    public void a(boolean z) {
        this.m = z;
        this.n = true;
    }

    @DexIgnore
    public void b(View view) {
        if (!this.e) {
            this.f = view;
            if (this.c == null && view != null) {
                this.c = view.getContext();
            }
            if (this.d == null && this.f != null) {
                this.d = (WindowManager) this.c.getSystemService("window");
            }
            Context context = this.c;
            if (context != null && !this.n) {
                a(context.getApplicationInfo().targetSdkVersion >= 22);
            }
        }
    }
}
