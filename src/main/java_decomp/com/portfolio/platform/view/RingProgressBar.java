package com.portfolio.platform.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingProgressBar extends View {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public float C;
    @DexIgnore
    public int D;
    @DexIgnore
    public ObjectAnimator E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public Paint m;
    @DexIgnore
    public Paint s;
    @DexIgnore
    public Paint t;
    @DexIgnore
    public Paint u;
    @DexIgnore
    public Paint v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum a {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        SLEEP,
        GOAL
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public b(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public c(RingProgressBar ringProgressBar, int i) {
            this.a = ringProgressBar;
            this.b = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Wg6.c(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        public d(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            RingProgressBar ringProgressBar = this.a;
            float f = (float) 2;
            ringProgressBar.invalidate((ringProgressBar.getWidth() / 2) - ((int) ((((float) this.a.z) * 1.15f) / f)), (int) ((this.a.g / 2.0f) + (this.a.g * 0.14999998f)), ((int) ((((float) this.a.z) * 1.15f) / f)) + (this.a.getWidth() / 2), (int) ((this.a.g / 2.0f) + (this.a.g * 0.14999998f) + (((float) this.a.z) * 1.15f)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(RingProgressBar ringProgressBar) {
            this.a = ringProgressBar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            Wg6.c(animator, "animator");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Wg6.c(animator, "animator");
            this.a.setMReachAnimateState$app_fossilRelease(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RingProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public f(RingProgressBar ringProgressBar, boolean z) {
            this.a = ringProgressBar;
            this.b = z;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (this.a.getMProgress$app_fossilRelease() >= ((float) 1) && this.a.getMReachAnimateState$app_fossilRelease() == 0) {
                if (this.b) {
                    RingProgressBar ringProgressBar = this.a;
                    ringProgressBar.h(ringProgressBar.getMProgressIndex$app_fossilRelease());
                    return;
                }
                this.a.h(0);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context) {
        this(context, null);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.b = "RingProgressBar";
        this.c = new RectF();
        this.d = new RectF();
        this.e = new RectF();
        this.h = true;
        this.m = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.RingProgressBar, i2, 0);
        if (obtainStyledAttributes != null) {
            try {
                setProgressColor(obtainStyledAttributes.getColor(7, W6.d(PortfolioApp.get.instance(), 2131099947)));
                setProgressBackgroundColor(obtainStyledAttributes.getColor(6, -16711936));
                setProgress(obtainStyledAttributes.getFloat(5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                setStrokeWidth((int) obtainStyledAttributes.getDimension(10, (float) 10));
                setSpaceWidth((int) obtainStyledAttributes.getDimension(9, (float) 2));
                setIconSize((int) obtainStyledAttributes.getDimension(2, (float) 60));
                setMaxProgress(obtainStyledAttributes.getFloat(4, 2.0f));
                setIconSource(obtainStyledAttributes.getResourceId(3, 0));
                i(W6.d(context, obtainStyledAttributes.getResourceId(1, 2131099811)), W6.d(context, obtainStyledAttributes.getResourceId(0, 2131099942)));
                this.D = obtainStyledAttributes.getInteger(8, 0);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        n();
        p();
        this.h = false;
    }

    @DexIgnore
    private final void setIconSize(int i2) {
        this.z = i2;
        invalidate();
    }

    @DexIgnore
    private final void setMaxProgress(float f2) {
        this.C = f2;
        invalidate();
    }

    @DexIgnore
    private final void setProgressBackgroundColor(int i2) {
        this.k = i2;
        n();
    }

    @DexIgnore
    private final void setSpaceWidth(int i2) {
        n();
        p();
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator c(int i2, int i3) {
        ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", 0);
        Wg6.b(ofInt, "gradientAnimator");
        ofInt.setDuration((long) i2);
        ofInt.setStartDelay((long) 33);
        ofInt.addUpdateListener(new b(this));
        ofInt.addListener(new c(this, i3));
        return ofInt;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final ObjectAnimator d(int i2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "", 1.0f);
        Wg6.b(ofFloat, "scaleAnimator");
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new d(this));
        ofFloat.addListener(new e(this));
        return ofFloat;
    }

    @DexIgnore
    public final void e(Canvas canvas, float f2) {
        int i2 = this.F;
        for (Paint paint : i2 != 2 ? i2 != 3 ? i2 != 4 ? new Paint[0] : new Paint[]{this.v, this.u, this.t} : new Paint[]{this.u, this.t} : new Paint[]{this.t}) {
            canvas.drawArc(this.c, 270.0f, this.i > 1.0f ? 360.0f : f2, false, paint);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2;
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), this.y);
        if (decodeResource != null) {
            int i2 = this.z;
            float f3 = this.g;
            float f4 = (((float) i2) * 0.14999998f) + (f3 / 2.0f);
            if (this.F == 1) {
                i2 = (int) (((float) i2) * 1.15f);
                f2 = (f3 / 2.0f) + (f3 * 0.14999998f);
            } else {
                f2 = f4;
            }
            Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, i2, i2, false);
            Paint paint = new Paint(1);
            paint.setColorFilter(new PorterDuffColorFilter(this.B, PorterDuff.Mode.SRC_OVER));
            int i3 = this.z;
            float f5 = this.g;
            canvas.drawRect((((float) (-i3)) * 1.15f) / 2.0f, (f5 * 0.14999998f) + (f5 / 2.0f) + 10.0f, (((float) i3) * 1.15f) / 2.0f, ((float) i3) * 2.0f, paint);
            paint.setColorFilter(new PorterDuffColorFilter(this.A, PorterDuff.Mode.SRC_ATOP));
            if (createScaledBitmap != null) {
                canvas.drawBitmap(createScaledBitmap, ((float) (-createScaledBitmap.getWidth())) / 2.0f, f2, paint);
                createScaledBitmap.recycle();
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final int g(int i2, int i3) {
        return Color.argb(i3, Color.red(i2), Color.green(i2), Color.blue(i2));
    }

    @DexIgnore
    public final float getMProgress$app_fossilRelease() {
        return this.i;
    }

    @DexIgnore
    public final int getMProgressIndex$app_fossilRelease() {
        return this.D;
    }

    @DexIgnore
    public final int getMReachAnimateState$app_fossilRelease() {
        return this.F;
    }

    @DexIgnore
    public final void h(int i2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(d(170), c(60, 2), c(60, 3), c(120, 4), c(60, 3), c(60, 2), c(60, 0));
        animatorSet.setStartDelay(((long) i2) * ((long) 170));
        animatorSet.start();
    }

    @DexIgnore
    public final void i(int i2, int i3) {
        this.A = i2;
        this.B = i3;
        invalidate();
    }

    @DexIgnore
    public final void j(String str, String str2) {
        Wg6.c(str, "progressColorValue");
        Wg6.c(str2, "unfilledColorValue");
        String d2 = ThemeManager.l.a().d(str);
        String d3 = ThemeManager.l.a().d(str2);
        if (d2 != null) {
            setProgressColor(Color.parseColor(d2));
        }
        if (d3 != null) {
            setProgressBackgroundColor(Color.parseColor(d3));
        }
    }

    @DexIgnore
    public final void k(float f2, int i2, boolean z2) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "progress", this.i, f2);
        this.E = ofFloat;
        if (ofFloat != null) {
            ofFloat.setDuration((long) i2);
            ObjectAnimator objectAnimator = this.E;
            if (objectAnimator != null) {
                objectAnimator.reverse();
                ObjectAnimator objectAnimator2 = this.E;
                if (objectAnimator2 != null) {
                    objectAnimator2.addListener(new f(this, z2));
                    ObjectAnimator objectAnimator3 = this.E;
                    if (objectAnimator3 != null) {
                        objectAnimator3.start();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void l(float f2, boolean z2) {
        if (f2 != this.j) {
            m();
            k(f2, 1000, z2);
            this.j = f2;
        }
    }

    @DexIgnore
    public final void m() {
        ObjectAnimator objectAnimator = this.E;
        if (objectAnimator == null) {
            return;
        }
        if (objectAnimator != null) {
            objectAnimator.cancel();
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        o(this.s, this.k, (float) this.f);
        invalidate();
    }

    @DexIgnore
    public final void o(Paint paint, int i2, float f2) {
        paint.setColor(i2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(f2);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        canvas.translate(this.w, this.x);
        float f2 = this.i * ((float) 360);
        canvas.drawArc(this.c, 270.0f, -(360.0f - f2), false, this.s);
        canvas.drawArc(this.c, 270.0f, this.i > 1.0f ? 360.0f : f2, false, this.m);
        float f3 = this.i;
        if (f3 > 1.0f) {
            canvas.drawArc(this.d, 270.0f, f3 > 2.0f ? 360.0f : f2 - 360.0f, false, this.m);
        }
        e(canvas, f2);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -this.x);
        f(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (i2 <= i3) {
            i2 = i3;
        }
        int defaultSize = View.getDefaultSize(getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight(), i2);
        setMeasuredDimension(defaultSize, defaultSize);
        float f2 = ((float) defaultSize) / 2.0f;
        int i4 = this.f;
        float f3 = (float) (i4 + 2);
        this.g = f3;
        float f4 = (f2 - ((float) i4)) - ((((float) this.z) * 1.15f) / ((float) 2));
        float f5 = f4 - f3;
        float f6 = f5 - f3;
        float f7 = -f4;
        this.c.set(f7, f7, f4, f4);
        float f8 = -f5;
        this.d.set(f8, f8, f5, f5);
        float f9 = -f6;
        this.e.set(f9, f9, f6, f6);
        this.w = f2;
        this.x = f2;
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        Wg6.c(parcelable, "state");
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            setProgress(bundle.getFloat("mProgress"));
            int i2 = bundle.getInt("progress_color");
            if (i2 != this.l) {
                this.l = i2;
                p();
            }
            int i3 = bundle.getInt("progress_background_color");
            if (i3 != this.k) {
                this.k = i3;
                n();
            }
            super.onRestoreInstanceState(bundle.getParcelable("saved_state"));
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("saved_state", super.onSaveInstanceState());
        bundle.putFloat("mProgress", this.i);
        bundle.putInt("progress_color", this.l);
        bundle.putInt("progress_background_color", this.k);
        return bundle;
    }

    @DexIgnore
    public final void p() {
        o(this.m, this.l, (float) this.f);
        o(this.t, g(this.l, 30), ((float) this.f) * 1.9f);
        o(this.u, g(this.l, 30), ((float) this.f) * 2.7f);
        o(this.v, g(this.l, 30), ((float) this.f) * 3.5f);
        invalidate();
    }

    @DexIgnore
    public final void setIconSource(int i2) {
        this.y = i2;
        invalidate();
    }

    @DexIgnore
    public final void setMProgress$app_fossilRelease(float f2) {
        this.i = f2;
    }

    @DexIgnore
    public final void setMProgressIndex$app_fossilRelease(int i2) {
        this.D = i2;
    }

    @DexIgnore
    public final void setMReachAnimateState$app_fossilRelease(int i2) {
        this.F = i2;
    }

    @DexIgnore
    @SuppressLint({"AnimatorKeep"})
    public final void setProgress(float f2) {
        if (f2 != this.i) {
            float f3 = this.C;
            if (f2 >= f3) {
                f2 = f3;
            }
            this.i = f2;
            if (!this.h) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public final void setProgressColor(int i2) {
        this.l = i2;
        p();
    }

    @DexIgnore
    public final void setProgressRingColorPreview(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.b;
        local.d(str, "setProgressRingColorPreview color=" + i2);
        setProgressColor(i2);
    }

    @DexIgnore
    public final void setStrokeWidth(int i2) {
        this.f = i2;
        n();
        p();
    }
}
