package com.portfolio.platform.view.watchface;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.A51;
import com.fossil.Hl7;
import com.fossil.Jo0;
import com.fossil.Qq7;
import com.fossil.S67;
import com.fossil.S87;
import com.fossil.T67;
import com.fossil.U71;
import com.fossil.V87;
import com.fossil.W87;
import com.fossil.X41;
import com.fossil.Z71;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFacePreviewView extends ConstraintLayout {
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public RelativeLayout x;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView c;
        @DexIgnore
        public /* final */ /* synthetic */ Gg6 d;

        @DexIgnore
        public a(View view, WatchFacePreviewView watchFacePreviewView, Gg6 gg6) {
            this.b = view;
            this.c = watchFacePreviewView;
            this.d = gg6;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d("WatchFacePreviewView", "preview task invoke doOnPreDraw");
            this.d.invoke();
            this.c.y = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $background;
        @DexIgnore
        public /* final */ /* synthetic */ List $elementConfigs;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchFacePreviewView watchFacePreviewView, Bitmap bitmap, List list) {
            super(0);
            this.this$0 = watchFacePreviewView;
            this.$background = bitmap;
            this.$elementConfigs = list;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            int width = this.this$0.getWidth();
            ImageView imageView = this.this$0.w;
            if (imageView != null) {
                Bitmap bitmap = this.$background;
                A51 b = X41.b();
                Context context = imageView.getContext();
                Wg6.b(context, "context");
                U71 u71 = new U71(context, b.a());
                u71.x(bitmap);
                u71.z(imageView);
                b.b(u71.w());
            }
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            for (T t : V87.a(this.$elementConfigs)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFacePreviewView", "preview " + ((Object) t));
                if (t instanceof S87.Ci) {
                    WatchFacePreviewView watchFacePreviewView = this.this$0;
                    W87 b2 = t.b();
                    if (b2 != null) {
                        t.d(watchFacePreviewView.N(b2));
                        T t2 = t;
                        t2.l(t2.h() * ((float) width));
                        this.this$0.T(t2);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (t instanceof S87.Bi) {
                    WatchFacePreviewView watchFacePreviewView2 = this.this$0;
                    W87 b3 = t.b();
                    if (b3 != null) {
                        t.d(watchFacePreviewView2.N(b3));
                        this.this$0.S(t);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (t instanceof S87.Ai) {
                    this.this$0.R(t);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Qq7 implements Gg6<Z71> {
        @DexIgnore
        public /* final */ /* synthetic */ String $previewURL;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchFacePreviewView watchFacePreviewView, String str) {
            super(0);
            this.this$0 = watchFacePreviewView;
            this.$previewURL = str;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Z71 invoke() {
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            ImageView imageView = this.this$0.w;
            if (imageView == null) {
                return null;
            }
            String str = this.$previewURL;
            A51 b = X41.b();
            Context context = imageView.getContext();
            Wg6.b(context, "context");
            U71 u71 = new U71(context, b.a());
            u71.x(str);
            u71.z(imageView);
            return b.b(u71.w());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Qq7 implements Gg6<Z71> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFacePreviewView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WatchFacePreviewView watchFacePreviewView) {
            super(0);
            this.this$0 = watchFacePreviewView;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Z71 invoke() {
            RelativeLayout relativeLayout = this.this$0.x;
            if (relativeLayout != null) {
                relativeLayout.removeAllViews();
            }
            ImageView imageView = this.this$0.w;
            if (imageView == null) {
                return null;
            }
            A51 b = X41.b();
            Context context = imageView.getContext();
            Wg6.b(context, "context");
            U71 u71 = new U71(context, b.a());
            u71.x(2131231268);
            u71.z(imageView);
            return b.b(u71.w());
        }
    }

    @DexIgnore
    public WatchFacePreviewView(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public WatchFacePreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFacePreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        this.y = true;
        LayoutInflater.from(context).inflate(2131558860, (ViewGroup) this, true);
        this.w = (ImageView) findViewById(2131361908);
        this.x = (RelativeLayout) findViewById(2131362962);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFacePreviewView(Context context, AttributeSet attributeSet, int i, int i2, Qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final W87 N(W87 w87) {
        float width = (float) getWidth();
        return new W87((float) Math.ceil((double) (w87.d() * width)), (float) Math.ceil((double) (w87.e() * width)), (float) Math.ceil((double) (w87.c() * width)), (float) Math.ceil((double) (width * w87.a())), 1.0f);
    }

    @DexIgnore
    public final void O(Gg6<? extends Object> gg6) {
        if (this.y) {
            RelativeLayout relativeLayout = this.x;
            if (relativeLayout != null) {
                Wg6.b(Jo0.a(relativeLayout, new a(relativeLayout, this, gg6)), "OneShotPreDrawListener.add(this) { action(this) }");
                return;
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("WatchFacePreviewView", "preview task invoke after initialized");
        gg6.invoke();
    }

    @DexIgnore
    public final void P(String str) {
        Wg6.c(str, "previewURL");
        O(new c(this, str));
    }

    @DexIgnore
    public final void Q(List<? extends S87> list, Bitmap bitmap) {
        Wg6.c(list, "elementConfigs");
        O(new b(this, bitmap, list));
    }

    @DexIgnore
    public final void R(S87.Ai ai) {
        Rect rect = new Rect();
        getDrawingRect(rect);
        W87 b2 = ai.b();
        if (b2 != null) {
            float d2 = b2.d();
            W87 b3 = ai.b();
            if (b3 != null) {
                CustomizeWidget customizeWidget = new CustomizeWidget(getContext(), rect, d2, b3.e(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 32, null);
                customizeWidget.setSetting(ai.l());
                customizeWidget.O(ai.i());
                customizeWidget.setContentTheme(ai.h());
                customizeWidget.setBottomContent(ai.j());
                customizeWidget.Z(ai.k(), ai.g());
                customizeWidget.Y(ai.m());
                customizeWidget.setRemoveMode(false);
                customizeWidget.N();
                customizeWidget.T();
                RelativeLayout relativeLayout = this.x;
                if (relativeLayout != null) {
                    relativeLayout.addView(customizeWidget);
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void S(S87.Bi bi) {
        S67.Ai ai = S67.s;
        Context context = getContext();
        Wg6.b(context, "this.context");
        Lc6<S67, ViewGroup.LayoutParams> b2 = ai.b(context, bi);
        S67 component1 = b2.component1();
        ViewGroup.LayoutParams component2 = b2.component2();
        RelativeLayout relativeLayout = this.x;
        if (relativeLayout != null) {
            relativeLayout.addView(component1, component2);
        }
        W87 b3 = bi.b();
        if (b3 != null) {
            Lc6 a2 = Hl7.a(Float.valueOf(b3.d()), Float.valueOf(b3.e()));
            component1.r(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void T(S87.Ci ci) {
        T67.Ai ai = T67.s;
        Context context = getContext();
        Wg6.b(context, "this.context");
        Lc6<T67, ViewGroup.LayoutParams> b2 = ai.b(context, ci);
        T67 component1 = b2.component1();
        ViewGroup.LayoutParams component2 = b2.component2();
        RelativeLayout relativeLayout = this.x;
        if (relativeLayout != null) {
            relativeLayout.addView(component1, component2);
        }
        W87 b3 = ci.b();
        if (b3 != null) {
            Lc6 a2 = Hl7.a(Float.valueOf(b3.d()), Float.valueOf(b3.e()));
            component1.r(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void U() {
        O(new d(this));
    }
}
