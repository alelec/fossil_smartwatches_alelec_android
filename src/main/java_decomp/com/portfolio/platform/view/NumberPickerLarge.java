package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.E57;
import com.fossil.Nl0;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;
import java.io.Serializable;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NumberPickerLarge extends LinearLayout {
    @DexIgnore
    public static /* final */ l q0; // = new l();
    @DexIgnore
    public f A;
    @DexIgnore
    public /* final */ SparseArray<String> B;
    @DexIgnore
    public /* final */ int[] C;
    @DexIgnore
    public /* final */ Paint D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Drawable F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public /* final */ E57 J;
    @DexIgnore
    public /* final */ E57 K;
    @DexIgnore
    public int L;
    @DexIgnore
    public j M;
    @DexIgnore
    public e N;
    @DexIgnore
    public d O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public VelocityTracker R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public /* final */ int W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ boolean b0;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ Drawable c0;
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ int d0;
    @DexIgnore
    public /* final */ ImageButton e;
    @DexIgnore
    public int e0;
    @DexIgnore
    public /* final */ ImageButton f;
    @DexIgnore
    public boolean f0;
    @DexIgnore
    public /* final */ AppCompatEditText g;
    @DexIgnore
    public boolean g0;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int h0;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int i0;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public int j0;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public boolean k0;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean l0;
    @DexIgnore
    public /* final */ boolean m;
    @DexIgnore
    public k m0;
    @DexIgnore
    public /* final */ i n0;
    @DexIgnore
    public int o0;
    @DexIgnore
    public Typeface p0;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public String[] u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public h y;
    @DexIgnore
    public g z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            NumberPickerLarge.this.i();
            NumberPickerLarge.this.g.clearFocus();
            if (view.getId() == 2131362888) {
                NumberPickerLarge.this.a(true);
            } else {
                NumberPickerLarge.this.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnLongClickListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            NumberPickerLarge.this.i();
            NumberPickerLarge.this.g.clearFocus();
            if (view.getId() == 2131362888) {
                NumberPickerLarge.this.t(true, 0);
            } else {
                NumberPickerLarge.this.t(false, 0);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends AccessibilityNodeProvider {
        @DexIgnore
        public /* final */ Rect a; // = new Rect();
        @DexIgnore
        public /* final */ int[] b; // = new int[2];
        @DexIgnore
        public int c; // = RecyclerView.UNDEFINED_DURATION;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final AccessibilityNodeInfo a(int i, int i2, int i3, int i4) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(NumberPickerLarge.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this);
            if (g()) {
                obtain.addChild(NumberPickerLarge.this, 3);
            }
            obtain.addChild(NumberPickerLarge.this, 2);
            if (h()) {
                obtain.addChild(NumberPickerLarge.this, 1);
            }
            obtain.setParent((View) NumberPickerLarge.this.getParentForAccessibility());
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            obtain.setScrollable(true);
            if (this.c != -1) {
                obtain.addAction(64);
            }
            if (this.c == -1) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue()) {
                    obtain.addAction(4096);
                }
                if (NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue()) {
                    obtain.addAction(8192);
                }
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo b(int i, String str, int i2, int i3, int i4, int i5) {
            AccessibilityNodeInfo obtain = AccessibilityNodeInfo.obtain();
            obtain.setClassName(Button.class.getName());
            obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
            obtain.setSource(NumberPickerLarge.this, i);
            obtain.setParent(NumberPickerLarge.this);
            obtain.setText(str);
            obtain.setClickable(true);
            obtain.setLongClickable(true);
            obtain.setEnabled(NumberPickerLarge.this.isEnabled());
            Rect rect = this.a;
            rect.set(i2, i3, i4, i5);
            obtain.setBoundsInParent(rect);
            int[] iArr = this.b;
            NumberPickerLarge.this.getLocationOnScreen(iArr);
            rect.offset(iArr[0], iArr[1]);
            obtain.setBoundsInScreen(rect);
            if (this.c != i) {
                obtain.addAction(64);
            }
            if (this.c == i) {
                obtain.addAction(128);
            }
            if (NumberPickerLarge.this.isEnabled()) {
                obtain.addAction(16);
            }
            return obtain;
        }

        @DexIgnore
        public final AccessibilityNodeInfo c() {
            AccessibilityNodeInfo createAccessibilityNodeInfo = NumberPickerLarge.this.g.createAccessibilityNodeInfo();
            createAccessibilityNodeInfo.setSource(NumberPickerLarge.this, 2);
            if (this.c != 2) {
                createAccessibilityNodeInfo.addAction(64);
            }
            if (this.c == 2) {
                createAccessibilityNodeInfo.addAction(128);
            }
            return createAccessibilityNodeInfo;
        }

        @DexIgnore
        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            if (i == -1) {
                return a(NumberPickerLarge.this.getScrollX(), NumberPickerLarge.this.getScrollY(), NumberPickerLarge.this.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            }
            if (i == 1) {
                String f = f();
                int scrollX = NumberPickerLarge.this.getScrollX();
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                return b(1, f, scrollX, numberPickerLarge.i0 - numberPickerLarge.d0, numberPickerLarge.getScrollX() + (NumberPickerLarge.this.getRight() - NumberPickerLarge.this.getLeft()), NumberPickerLarge.this.getScrollY() + (NumberPickerLarge.this.getBottom() - NumberPickerLarge.this.getTop()));
            } else if (i == 2) {
                return c();
            } else {
                if (i != 3) {
                    return super.createAccessibilityNodeInfo(i);
                }
                String e = e();
                int scrollX2 = NumberPickerLarge.this.getScrollX();
                int scrollY = NumberPickerLarge.this.getScrollY();
                int scrollX3 = NumberPickerLarge.this.getScrollX();
                int right = NumberPickerLarge.this.getRight();
                int left = NumberPickerLarge.this.getLeft();
                NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                return b(3, e, scrollX2, scrollY, (right - left) + scrollX3, numberPickerLarge2.d0 + numberPickerLarge2.h0);
            }
        }

        @DexIgnore
        public final void d(String str, int i, List<AccessibilityNodeInfo> list) {
            if (i == 1) {
                String f = f();
                if (!TextUtils.isEmpty(f) && f.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(1));
                }
            } else if (i == 2) {
                Editable text = NumberPickerLarge.this.g.getText();
                if (TextUtils.isEmpty(text) || !text.toString().toLowerCase().contains(str)) {
                    Editable text2 = NumberPickerLarge.this.g.getText();
                    if (!TextUtils.isEmpty(text2) && text2.toString().toLowerCase().contains(str)) {
                        list.add(createAccessibilityNodeInfo(2));
                        return;
                    }
                    return;
                }
                list.add(createAccessibilityNodeInfo(2));
            } else if (i == 3) {
                String e = e();
                if (!TextUtils.isEmpty(e) && e.toLowerCase().contains(str)) {
                    list.add(createAccessibilityNodeInfo(3));
                }
            }
        }

        @DexIgnore
        public final String e() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.x - 1;
            if (numberPickerLarge.V) {
                i = numberPickerLarge.h(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            int i2 = numberPickerLarge2.v;
            if (i < i2) {
                return null;
            }
            String[] strArr = numberPickerLarge2.u;
            return strArr == null ? numberPickerLarge2.f(i) : strArr[i - i2];
        }

        @DexIgnore
        public final String f() {
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            int i = numberPickerLarge.x + 1;
            if (numberPickerLarge.V) {
                i = numberPickerLarge.h(i);
            }
            NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
            if (i > numberPickerLarge2.w) {
                return null;
            }
            String[] strArr = numberPickerLarge2.u;
            return strArr == null ? numberPickerLarge2.f(i) : strArr[i - numberPickerLarge2.v];
        }

        @DexIgnore
        @Override // android.view.accessibility.AccessibilityNodeProvider
        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i) {
            if (TextUtils.isEmpty(str)) {
                return Collections.emptyList();
            }
            String lowerCase = str.toLowerCase();
            ArrayList arrayList = new ArrayList();
            if (i == -1) {
                d(lowerCase, 3, arrayList);
                d(lowerCase, 2, arrayList);
                d(lowerCase, 1, arrayList);
                return arrayList;
            } else if (i != 1 && i != 2 && i != 3) {
                return super.findAccessibilityNodeInfosByText(str, i);
            } else {
                d(lowerCase, i, arrayList);
                return arrayList;
            }
        }

        @DexIgnore
        public final boolean g() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() > NumberPickerLarge.this.getMinValue();
        }

        @DexIgnore
        public final boolean h() {
            return NumberPickerLarge.this.getWrapSelectorWheel() || NumberPickerLarge.this.getValue() < NumberPickerLarge.this.getMaxValue();
        }

        @DexIgnore
        public final void i(int i, int i2, String str) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
                obtain.setClassName(Button.class.getName());
                obtain.setPackageName(NumberPickerLarge.this.getContext().getPackageName());
                obtain.getText().add(str);
                obtain.setEnabled(NumberPickerLarge.this.isEnabled());
                obtain.setSource(NumberPickerLarge.this, i);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public final void j(int i) {
            if (((AccessibilityManager) NumberPickerLarge.this.getContext().getSystemService("accessibility")).isEnabled()) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
                NumberPickerLarge.this.g.onInitializeAccessibilityEvent(obtain);
                NumberPickerLarge.this.g.onPopulateAccessibilityEvent(obtain);
                obtain.setSource(NumberPickerLarge.this, 2);
                NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                numberPickerLarge.requestSendAccessibilityEvent(numberPickerLarge, obtain);
            }
        }

        @DexIgnore
        public void k(int i, int i2) {
            if (i != 1) {
                if (i == 2) {
                    j(i2);
                } else if (i == 3 && g()) {
                    i(i, i2, e());
                }
            } else if (h()) {
                i(i, i2, f());
            }
        }

        @DexIgnore
        public boolean performAction(int i, int i2, Bundle bundle) {
            if (i != -1) {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128 || this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    k(i, 65536);
                                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                                    numberPickerLarge.invalidate(0, 0, numberPickerLarge.getRight(), NumberPickerLarge.this.h0);
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    k(i, 32768);
                                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.h0);
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.a(false);
                                k(i, 1);
                                return true;
                            }
                        }
                    } else if (i2 != 1) {
                        if (i2 != 2) {
                            if (i2 != 16) {
                                if (i2 != 64) {
                                    if (i2 != 128) {
                                        return NumberPickerLarge.this.g.performAccessibilityAction(i2, bundle);
                                    }
                                    if (this.c != i) {
                                        return false;
                                    }
                                    this.c = RecyclerView.UNDEFINED_DURATION;
                                    k(i, 65536);
                                    NumberPickerLarge.this.g.invalidate();
                                    return true;
                                } else if (this.c == i) {
                                    return false;
                                } else {
                                    this.c = i;
                                    k(i, 32768);
                                    NumberPickerLarge.this.g.invalidate();
                                    return true;
                                }
                            } else if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            } else {
                                NumberPickerLarge.this.z();
                                return true;
                            }
                        } else if (!NumberPickerLarge.this.isEnabled() || !NumberPickerLarge.this.g.isFocused()) {
                            return false;
                        } else {
                            NumberPickerLarge.this.g.clearFocus();
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled() || NumberPickerLarge.this.g.isFocused()) {
                        return false;
                    } else {
                        return NumberPickerLarge.this.g.requestFocus();
                    }
                } else if (i2 != 16) {
                    if (i2 != 64) {
                        if (i2 != 128 || this.c != i) {
                            return false;
                        }
                        this.c = RecyclerView.UNDEFINED_DURATION;
                        k(i, 65536);
                        NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                        numberPickerLarge3.invalidate(0, numberPickerLarge3.i0, numberPickerLarge3.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    } else if (this.c == i) {
                        return false;
                    } else {
                        this.c = i;
                        k(i, 32768);
                        NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                        numberPickerLarge4.invalidate(0, numberPickerLarge4.i0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                        return true;
                    }
                } else if (!NumberPickerLarge.this.isEnabled()) {
                    return false;
                } else {
                    NumberPickerLarge.this.a(true);
                    k(i, 1);
                    return true;
                }
            } else if (i2 != 64) {
                if (i2 != 128) {
                    if (i2 != 4096) {
                        if (i2 == 8192) {
                            if (!NumberPickerLarge.this.isEnabled()) {
                                return false;
                            }
                            if (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() <= NumberPickerLarge.this.getMinValue()) {
                                return false;
                            }
                            NumberPickerLarge.this.a(false);
                            return true;
                        }
                    } else if (!NumberPickerLarge.this.isEnabled()) {
                        return false;
                    } else {
                        if (!NumberPickerLarge.this.getWrapSelectorWheel() && NumberPickerLarge.this.getValue() >= NumberPickerLarge.this.getMaxValue()) {
                            return false;
                        }
                        NumberPickerLarge.this.a(true);
                        return true;
                    }
                } else if (this.c != i) {
                    return false;
                } else {
                    this.c = RecyclerView.UNDEFINED_DURATION;
                    NumberPickerLarge.this.performAccessibilityAction(128, null);
                    return true;
                }
            } else if (this.c == i) {
                return false;
            } else {
                this.c = i;
                NumberPickerLarge.this.performAccessibilityAction(64, null);
                return true;
            }
            return super.performAction(i, i2, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.z();
            NumberPickerLarge.this.f0 = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public void run() {
            NumberPickerLarge.this.a(this.b);
            NumberPickerLarge.this.postDelayed(this, 300);
        }
    }

    @DexIgnore
    public interface f extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void a(NumberPickerLarge numberPickerLarge, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements Runnable {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public void a(int i) {
            c();
            this.c = 1;
            this.b = i;
            NumberPickerLarge.this.postDelayed(this, (long) ViewConfiguration.getTapTimeout());
        }

        @DexIgnore
        public void b(int i) {
            c();
            this.c = 2;
            this.b = i;
            NumberPickerLarge.this.post(this);
        }

        @DexIgnore
        public void c() {
            this.c = 0;
            this.b = 0;
            NumberPickerLarge.this.removeCallbacks(this);
            NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
            if (numberPickerLarge.k0) {
                numberPickerLarge.k0 = false;
                numberPickerLarge.invalidate(0, numberPickerLarge.i0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
            }
        }

        @DexIgnore
        public void run() {
            int i = this.c;
            if (i == 1) {
                int i2 = this.b;
                if (i2 == 1) {
                    NumberPickerLarge numberPickerLarge = NumberPickerLarge.this;
                    numberPickerLarge.k0 = true;
                    numberPickerLarge.invalidate(0, numberPickerLarge.i0, numberPickerLarge.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i2 == 2) {
                    NumberPickerLarge numberPickerLarge2 = NumberPickerLarge.this;
                    numberPickerLarge2.l0 = true;
                    numberPickerLarge2.invalidate(0, 0, numberPickerLarge2.getRight(), NumberPickerLarge.this.h0);
                }
            } else if (i == 2) {
                int i3 = this.b;
                if (i3 == 1) {
                    NumberPickerLarge numberPickerLarge3 = NumberPickerLarge.this;
                    if (!numberPickerLarge3.k0) {
                        numberPickerLarge3.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge4 = NumberPickerLarge.this;
                    numberPickerLarge4.k0 = !numberPickerLarge4.k0;
                    numberPickerLarge4.invalidate(0, numberPickerLarge4.i0, numberPickerLarge4.getRight(), NumberPickerLarge.this.getBottom());
                } else if (i3 == 2) {
                    NumberPickerLarge numberPickerLarge5 = NumberPickerLarge.this;
                    if (!numberPickerLarge5.l0) {
                        numberPickerLarge5.postDelayed(this, (long) ViewConfiguration.getPressedStateDuration());
                    }
                    NumberPickerLarge numberPickerLarge6 = NumberPickerLarge.this;
                    numberPickerLarge6.l0 = !numberPickerLarge6.l0;
                    numberPickerLarge6.invalidate(0, 0, numberPickerLarge6.getRight(), NumberPickerLarge.this.h0);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements Runnable {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k {
        @DexIgnore
        public c a;

        @DexIgnore
        public k() {
            this.a = new c();
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            c cVar = this.a;
            return cVar != null && cVar.performAction(i, i2, bundle);
        }

        @DexIgnore
        public void b(int i, int i2) {
            c cVar = this.a;
            if (cVar != null) {
                cVar.k(i, i2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class l implements f {
        @DexIgnore
        public /* final */ Object[] mArgs; // = new Object[1];
        @DexIgnore
        public /* final */ StringBuilder mBuilder; // = new StringBuilder();
        @DexIgnore
        public transient Formatter mFmt;
        @DexIgnore
        public char mZeroDigit;

        @DexIgnore
        public l() {
            c(Locale.getDefault());
        }

        @DexIgnore
        public static char b(Locale locale) {
            return new DecimalFormatSymbols(locale).getZeroDigit();
        }

        @DexIgnore
        public final Formatter a(Locale locale) {
            return new Formatter(this.mBuilder, locale);
        }

        @DexIgnore
        public final void c(Locale locale) {
            this.mFmt = a(locale);
            this.mZeroDigit = b(locale);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPickerLarge.f
        public String format(int i) {
            Locale locale = Locale.getDefault();
            if (this.mZeroDigit != b(locale)) {
                c(locale);
            }
            this.mArgs[0] = Integer.valueOf(i);
            StringBuilder sb = this.mBuilder;
            sb.delete(0, sb.length());
            this.mFmt.format("%02d", this.mArgs);
            return this.mFmt.toString();
        }
    }

    @DexIgnore
    public NumberPickerLarge(Context context) {
        this(context, null);
    }

    @DexIgnore
    public NumberPickerLarge(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969482);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NumberPickerLarge(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        boolean z2 = false;
        this.B = new SparseArray<>();
        this.C = new int[5];
        this.H = RecyclerView.UNDEFINED_DURATION;
        this.e0 = 0;
        this.o0 = -1;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.NumberPicker, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        this.b0 = resourceId != 0;
        this.W = obtainStyledAttributes.getColor(15, 0);
        this.a0 = obtainStyledAttributes.getColor(10, 0);
        obtainStyledAttributes.getColor(16, 0);
        this.c0 = obtainStyledAttributes.getDrawable(12);
        this.b = obtainStyledAttributes.getString(7);
        this.d = obtainStyledAttributes.getString(6);
        this.c = obtainStyledAttributes.getString(8);
        this.d0 = obtainStyledAttributes.getDimensionPixelSize(13, (int) TypedValue.applyDimension(1, 2.0f, getResources().getDisplayMetrics()));
        this.h = obtainStyledAttributes.getDimensionPixelSize(14, (int) TypedValue.applyDimension(1, 48.0f, getResources().getDisplayMetrics()));
        this.i = obtainStyledAttributes.getDimensionPixelSize(3, -1);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(1, -1);
        this.j = dimensionPixelSize;
        int i3 = this.i;
        if (i3 == -1 || dimensionPixelSize == -1 || i3 <= dimensionPixelSize) {
            this.k = obtainStyledAttributes.getDimensionPixelSize(4, -1);
            int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(2, -1);
            this.l = dimensionPixelSize2;
            int i4 = this.k;
            if (i4 == -1 || dimensionPixelSize2 == -1 || i4 <= dimensionPixelSize2) {
                this.m = this.l == -1 ? true : z2;
                this.F = obtainStyledAttributes.getDrawable(17);
                String string = obtainStyledAttributes.getString(9);
                if (obtainStyledAttributes.hasValue(5)) {
                    this.p0 = Nl0.b(getContext(), obtainStyledAttributes.getResourceId(5, -1));
                }
                obtainStyledAttributes.recycle();
                this.n0 = new i();
                setWillNotDraw(!this.b0);
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                if (layoutInflater != null) {
                    layoutInflater.inflate(resourceId, (ViewGroup) this, true);
                }
                a aVar = new a();
                b bVar = new b();
                if (!this.b0) {
                    ImageButton imageButton = (ImageButton) findViewById(2131362888);
                    this.e = imageButton;
                    imageButton.setOnClickListener(aVar);
                    this.e.setOnLongClickListener(bVar);
                } else {
                    this.e = null;
                }
                if (!this.b0) {
                    ImageButton imageButton2 = (ImageButton) findViewById(2131362887);
                    this.f = imageButton2;
                    imageButton2.setOnClickListener(aVar);
                    this.f.setOnLongClickListener(bVar);
                } else {
                    this.f = null;
                }
                this.g = (AppCompatEditText) findViewById(2131362889);
                if (!TextUtils.isEmpty(this.b)) {
                    int parseColor = Color.parseColor(ThemeManager.l.a().d(this.b));
                    this.g.setTextColor(parseColor);
                    this.a0 = parseColor;
                    this.c0.setColorFilter(parseColor, PorterDuff.Mode.SRC_ATOP);
                }
                if (!TextUtils.isEmpty(this.d)) {
                    this.a0 = Color.parseColor(ThemeManager.l.a().d(this.d));
                }
                if (!TextUtils.isEmpty(this.c)) {
                    Typeface f2 = ThemeManager.l.a().f(this.c);
                    this.g.setTypeface(f2);
                    this.p0 = f2;
                }
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                this.S = viewConfiguration.getScaledTouchSlop();
                this.T = viewConfiguration.getScaledMinimumFlingVelocity();
                this.U = viewConfiguration.getScaledMaximumFlingVelocity() / 8;
                this.s = (int) this.g.getTextSize();
                Paint paint = new Paint(1);
                paint.setAntiAlias(true);
                paint.setTextAlign(Paint.Align.CENTER);
                paint.setTextSize((float) this.s);
                Typeface typeface = this.p0;
                if (typeface != null) {
                    paint.setTypeface(typeface);
                } else {
                    paint.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
                }
                paint.setColor(this.g.getTextColors().getColorForState(LinearLayout.ENABLED_STATE_SET, -1));
                this.D = paint;
                Paint paint2 = new Paint(1);
                paint2.setAntiAlias(true);
                paint2.setTextAlign(Paint.Align.CENTER);
                paint2.setTextSize((float) this.s);
                Typeface typeface2 = this.p0;
                if (typeface2 != null) {
                    paint2.setTypeface(typeface2);
                } else {
                    paint2.setTypeface(Typeface.createFromAsset(getResources().getAssets(), string));
                }
                paint2.setColor(this.a0);
                this.E = paint2;
                this.J = new E57(getContext(), null, true);
                this.K = new E57(getContext(), new DecelerateInterpolator(2.5f));
                B();
                if (getImportantForAccessibility() == 0) {
                    setImportantForAccessibility(1);
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("minWidth > maxWidth");
        }
        throw new IllegalArgumentException("minHeight > maxHeight");
    }

    @DexIgnore
    public static String g(int i2) {
        return String.format(Locale.getDefault(), "%d", Integer.valueOf(i2));
    }

    @DexIgnore
    private k getSupportAccessibilityNodeProvider() {
        return new k();
    }

    @DexIgnore
    public static f getTwoDigitFormatter() {
        return q0;
    }

    @DexIgnore
    public static int resolveSizeAndState(int i2, int i3, int i4) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 1073741824) {
                i2 = size;
            }
        } else if (size < i2) {
            i2 = 16777216 | size;
        }
        return (-16777216 & i4) | i2;
    }

    @DexIgnore
    private void setWrapSelectorWheel(boolean z2) {
        boolean z3 = this.w - this.v >= this.C.length;
        if ((!z2 || z3) && z2 != this.V) {
            this.V = z2;
        }
    }

    @DexIgnore
    public final void A() {
        int i2;
        int i3 = 0;
        if (this.m) {
            String[] strArr = this.u;
            if (strArr == null) {
                float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                int i4 = 0;
                while (i4 <= 9) {
                    float measureText = this.D.measureText(g(i4));
                    if (measureText <= f2) {
                        measureText = f2;
                    }
                    i4++;
                    f2 = measureText;
                }
                for (int i5 = this.w; i5 > 0; i5 /= 10) {
                    i3++;
                }
                i2 = (int) (((float) i3) * f2);
            } else {
                i2 = 0;
                for (String str : strArr) {
                    float measureText2 = this.D.measureText(str);
                    if (measureText2 > ((float) i2)) {
                        i2 = (int) measureText2;
                    }
                }
            }
            int paddingLeft = i2 + this.g.getPaddingLeft() + this.g.getPaddingRight();
            if (this.l != paddingLeft) {
                int i6 = this.k;
                if (paddingLeft > i6) {
                    this.l = paddingLeft;
                } else {
                    this.l = i6;
                }
                invalidate();
            }
        }
    }

    @DexIgnore
    public final boolean B() {
        String[] strArr = this.u;
        String f2 = strArr == null ? f(this.x) : strArr[this.x - this.v];
        if (TextUtils.isEmpty(f2) || f2.equals(this.g.getText().toString())) {
            return false;
        }
        this.g.setText(f2);
        return true;
    }

    @DexIgnore
    public void a(boolean z2) {
        if (this.b0) {
            this.g.setVisibility(4);
            if (!o(this.J)) {
                o(this.K);
            }
            this.L = 0;
            if (z2) {
                this.J.j(0, 0, 0, -this.G, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            } else {
                this.J.j(0, 0, 0, this.G, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            }
            invalidate();
        } else if (z2) {
            y(this.x + 1, true);
        } else {
            y(this.x - 1, true);
        }
    }

    @DexIgnore
    public final void b(int[] iArr) {
        System.arraycopy(iArr, 0, iArr, 1, iArr.length - 1);
        int i2 = iArr[1] - 1;
        if (this.V && i2 < this.v) {
            i2 = this.w;
        }
        iArr[0] = i2;
        c(i2);
    }

    @DexIgnore
    public final void c(int i2) {
        String str;
        SparseArray<String> sparseArray = this.B;
        if (sparseArray.get(i2) == null) {
            int i3 = this.v;
            if (i2 < i3 || i2 > this.w) {
                str = "";
            } else {
                String[] strArr = this.u;
                str = strArr != null ? strArr[i2 - i3] : f(i2);
            }
            sparseArray.put(i2, str);
        }
    }

    @DexIgnore
    public void computeScroll() {
        E57 e57 = this.J;
        if (e57.i()) {
            e57 = this.K;
            if (e57.i()) {
                return;
            }
        }
        e57.b();
        int f2 = e57.f();
        if (this.L == 0) {
            this.L = e57.h();
        }
        scrollBy(0, f2 - this.L);
        this.L = f2;
        if (e57.i()) {
            r(e57);
        } else {
            invalidate();
        }
    }

    @DexIgnore
    public final boolean d() {
        int i2 = this.H - this.I;
        if (i2 == 0) {
            return false;
        }
        this.L = 0;
        int abs = Math.abs(i2);
        int i3 = this.G;
        if (abs > i3 / 2) {
            if (i2 > 0) {
                i3 = -i3;
            }
            i2 += i3;
        }
        this.K.j(0, 0, 0, i2, 800);
        invalidate();
        return true;
    }

    @DexIgnore
    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        if (!this.b0) {
            return super.dispatchHoverEvent(motionEvent);
        }
        if (((AccessibilityManager) getContext().getSystemService("accessibility")).isEnabled()) {
            int y2 = (int) motionEvent.getY();
            int i2 = y2 < this.h0 ? 3 : y2 > this.i0 ? 1 : 2;
            int action = motionEvent.getAction() & 255;
            k supportAccessibilityNodeProvider = getSupportAccessibilityNodeProvider();
            if (action == 7) {
                int i3 = this.j0;
                if (!(i3 == i2 || i3 == -1)) {
                    supportAccessibilityNodeProvider.b(i3, 256);
                    supportAccessibilityNodeProvider.b(i2, 128);
                    this.j0 = i2;
                    supportAccessibilityNodeProvider.a(i2, 64, null);
                }
            } else if (action == 9) {
                supportAccessibilityNodeProvider.b(i2, 128);
                this.j0 = i2;
                supportAccessibilityNodeProvider.a(i2, 64, null);
            } else if (action == 10) {
                supportAccessibilityNodeProvider.b(i2, 256);
                this.j0 = -1;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003e, code lost:
        requestFocus();
        r5.o0 = r0;
        u();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004c, code lost:
        if (r5.J.i() == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004e, code lost:
        if (r0 != 20) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0050, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0051, code lost:
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchKeyEvent(android.view.KeyEvent r6) {
        /*
            r5 = this;
            r4 = 20
            r1 = 1
            int r0 = r6.getKeyCode()
            r2 = 19
            if (r0 == r2) goto L_0x001e
            if (r0 == r4) goto L_0x001e
            r1 = 23
            if (r0 == r1) goto L_0x001a
            r1 = 66
            if (r0 == r1) goto L_0x001a
        L_0x0015:
            boolean r1 = super.dispatchKeyEvent(r6)
        L_0x0019:
            return r1
        L_0x001a:
            r5.u()
            goto L_0x0015
        L_0x001e:
            boolean r2 = r5.b0
            if (r2 == 0) goto L_0x0015
            int r2 = r6.getAction()
            if (r2 == 0) goto L_0x0032
            if (r2 != r1) goto L_0x0015
            int r2 = r5.o0
            if (r2 != r0) goto L_0x0015
            r0 = -1
            r5.o0 = r0
            goto L_0x0019
        L_0x0032:
            boolean r2 = r5.V
            if (r2 != 0) goto L_0x0038
            if (r0 != r4) goto L_0x0055
        L_0x0038:
            int r2 = r5.x
            int r3 = r5.w
            if (r2 >= r3) goto L_0x0015
        L_0x003e:
            r5.requestFocus()
            r5.o0 = r0
            r5.u()
            com.fossil.E57 r2 = r5.J
            boolean r2 = r2.i()
            if (r2 == 0) goto L_0x0019
            if (r0 != r4) goto L_0x005c
            r0 = r1
        L_0x0051:
            r5.a(r0)
            goto L_0x0019
        L_0x0055:
            int r2 = r5.x
            int r3 = r5.v
            if (r2 <= r3) goto L_0x0015
            goto L_0x003e
        L_0x005c:
            r0 = 0
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.NumberPickerLarge.dispatchKeyEvent(android.view.KeyEvent):boolean");
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            u();
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction() & 255;
        if (action == 1 || action == 3) {
            u();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public final void e(int i2) {
        this.L = 0;
        if (i2 > 0) {
            this.J.c(0, 0, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        } else {
            this.J.c(0, Integer.MAX_VALUE, 0, i2, 0, 0, 0, Integer.MAX_VALUE);
        }
        invalidate();
    }

    @DexIgnore
    public String f(int i2) {
        f fVar = this.A;
        return fVar != null ? fVar.format(i2) : g(i2);
    }

    @DexIgnore
    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (!this.b0) {
            return super.getAccessibilityNodeProvider();
        }
        if (this.m0 == null) {
            this.m0 = new k();
        }
        return this.m0.a;
    }

    @DexIgnore
    public float getBottomFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getMaxValue() {
        return this.w;
    }

    @DexIgnore
    public int getMinValue() {
        return this.v;
    }

    @DexIgnore
    public int getSolidColor() {
        return this.W;
    }

    @DexIgnore
    public float getTopFadingEdgeStrength() {
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public int getValue() {
        return this.x;
    }

    @DexIgnore
    public boolean getWrapSelectorWheel() {
        return this.V;
    }

    @DexIgnore
    public int h(int i2) {
        int i3 = this.w;
        if (i2 > i3) {
            int i4 = this.v;
            return (((i2 - i3) % (i3 - i4)) + i4) - 1;
        }
        int i5 = this.v;
        return i2 < i5 ? (i3 - ((i5 - i2) % (i3 - i5))) + 1 : i2;
    }

    @DexIgnore
    public void i() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive(this.g)) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
            if (this.b0) {
                this.g.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public final void j(int[] iArr) {
        System.arraycopy(iArr, 1, iArr, 0, iArr.length - 1);
        int i2 = iArr[iArr.length - 2] + 1;
        if (this.V && i2 > this.w) {
            i2 = this.v;
        }
        iArr[iArr.length - 1] = i2;
        c(i2);
    }

    @DexIgnore
    public final void k() {
        setVerticalFadingEdgeEnabled(true);
        setFadingEdgeLength(((getBottom() - getTop()) - this.s) / 2);
    }

    @DexIgnore
    public final void l() {
        m();
        int[] iArr = this.C;
        int bottom = (int) ((((float) ((getBottom() - getTop()) - (iArr.length * this.s))) / ((float) iArr.length)) + 0.5f);
        this.t = bottom;
        this.G = bottom + this.s;
        int baseline = (this.g.getBaseline() + this.g.getTop()) - (this.G * 2);
        this.H = baseline;
        this.I = baseline;
        B();
    }

    @DexIgnore
    public final void m() {
        this.B.clear();
        int[] iArr = this.C;
        int i2 = this.x;
        for (int i3 = 0; i3 < this.C.length; i3++) {
            int i4 = (i3 - 2) + i2;
            if (this.V) {
                i4 = h(i4);
            }
            iArr[i3] = i4;
            c(iArr[i3]);
        }
    }

    @DexIgnore
    public final int n(int i2, int i3) {
        if (i3 == -1) {
            return i2;
        }
        int size = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i2);
        if (mode == Integer.MIN_VALUE) {
            return View.MeasureSpec.makeMeasureSpec(Math.min(size, i3), 1073741824);
        }
        if (mode == 0) {
            return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        if (mode == 1073741824) {
            return i2;
        }
        throw new IllegalArgumentException("Unknown measure mode: " + mode);
    }

    @DexIgnore
    public final boolean o(E57 e57) {
        e57.d(true);
        int g2 = e57.g() - e57.f();
        int i2 = this.H - ((this.I + g2) % this.G);
        if (i2 == 0) {
            return false;
        }
        int abs = Math.abs(i2);
        int i3 = this.G;
        if (abs > i3 / 2) {
            i2 = i2 > 0 ? i2 - i3 : i2 + i3;
        }
        scrollBy(0, i2 + g2);
        return true;
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        u();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (!this.b0) {
            super.onDraw(canvas);
            return;
        }
        float right = (float) ((getRight() - getLeft()) / 2);
        float f2 = (float) this.I;
        Drawable drawable = this.F;
        if (drawable != null && this.e0 == 0) {
            if (this.l0) {
                drawable.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.F.setBounds(0, 0, getRight(), this.h0);
                this.F.draw(canvas);
            }
            if (this.k0) {
                this.F.setState(LinearLayout.PRESSED_ENABLED_STATE_SET);
                this.F.setBounds(0, this.i0, getRight(), getBottom());
                this.F.draw(canvas);
            }
        }
        int[] iArr = this.C;
        float f3 = f2;
        for (int i2 = 0; i2 < iArr.length; i2++) {
            String str = this.B.get(iArr[i2]);
            if (i2 != 2) {
                canvas.drawText(str, right, f3, this.D);
            } else if (this.g.getVisibility() != 0) {
                canvas.drawText(str, right, f3, this.E);
            }
            f3 += (float) this.G;
        }
        Drawable drawable2 = this.c0;
        if (drawable2 != null) {
            int i3 = this.h0;
            drawable2.setBounds(0, i3, getRight(), this.d0 + i3);
            this.c0.draw(canvas);
            int i4 = this.i0;
            this.c0.setBounds(0, i4 - this.d0, getRight(), i4);
            this.c0.draw(canvas);
        }
    }

    @DexIgnore
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(NumberPickerLarge.class.getName());
        accessibilityEvent.setScrollable(true);
        accessibilityEvent.setScrollY((this.v + this.x) * this.G);
        accessibilityEvent.setMaxScrollY((this.w - this.v) * this.G);
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.b0 || !isEnabled() || (motionEvent.getAction() & 255) != 0) {
            return false;
        }
        u();
        this.g.setVisibility(4);
        float y2 = motionEvent.getY();
        this.P = y2;
        this.Q = y2;
        motionEvent.getEventTime();
        this.f0 = false;
        this.g0 = false;
        float f2 = this.P;
        if (f2 < ((float) this.h0)) {
            if (this.e0 == 0) {
                this.n0.a(2);
            }
        } else if (f2 > ((float) this.i0) && this.e0 == 0) {
            this.n0.a(1);
        }
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!this.J.i()) {
            this.J.d(true);
            this.K.d(true);
            q(0);
            return true;
        } else if (!this.K.i()) {
            this.J.d(true);
            this.K.d(true);
            return true;
        } else {
            float f3 = this.P;
            if (f3 < ((float) this.h0)) {
                i();
                t(false, (long) ViewConfiguration.getLongPressTimeout());
                return true;
            } else if (f3 > ((float) this.i0)) {
                i();
                t(true, (long) ViewConfiguration.getLongPressTimeout());
                return true;
            } else {
                this.g0 = true;
                s();
                return true;
            }
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (!this.b0) {
            super.onLayout(z2, i2, i3, i4, i5);
            return;
        }
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredWidth2 = this.g.getMeasuredWidth();
        int measuredHeight2 = this.g.getMeasuredHeight();
        int i6 = (measuredWidth - measuredWidth2) / 2;
        int i7 = (measuredHeight - measuredHeight2) / 2;
        this.g.layout(i6, i7, measuredWidth2 + i6, measuredHeight2 + i7);
        if (z2) {
            l();
            k();
            int height = getHeight();
            int i8 = this.h;
            int i9 = this.d0;
            int i10 = ((height - i8) / 2) - i9;
            this.h0 = i10;
            this.i0 = i10 + (i9 * 2) + i8;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (!this.b0) {
            super.onMeasure(i2, i3);
            return;
        }
        super.onMeasure(n(i2, this.l), n(i3, this.j));
        setMeasuredDimension(x(this.k, getMeasuredWidth(), i2), x(this.i, getMeasuredHeight(), i3));
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled() || !this.b0) {
            return false;
        }
        if (this.R == null) {
            this.R = VelocityTracker.obtain();
        }
        this.R.addMovement(motionEvent);
        int action = motionEvent.getAction() & 255;
        if (action == 1) {
            v();
            w();
            this.n0.c();
            VelocityTracker velocityTracker = this.R;
            velocityTracker.computeCurrentVelocity(1000, (float) this.U);
            int yVelocity = (int) velocityTracker.getYVelocity();
            if (Math.abs(yVelocity) > this.T) {
                e(yVelocity);
                q(2);
            } else {
                int y2 = (int) motionEvent.getY();
                if (((int) Math.abs(((float) y2) - this.P)) > this.S) {
                    d();
                } else if (this.g0) {
                    this.g0 = false;
                    z();
                } else {
                    int i2 = (y2 / this.G) - 2;
                    if (i2 > 0) {
                        a(true);
                        this.n0.b(1);
                    } else if (i2 < 0) {
                        a(false);
                        this.n0.b(2);
                    }
                }
                q(0);
            }
            this.R.recycle();
            this.R = null;
            return true;
        } else if (action != 2 || this.f0) {
            return true;
        } else {
            float y3 = motionEvent.getY();
            if (this.e0 == 1) {
                scrollBy(0, (int) (y3 - this.Q));
                invalidate();
            } else if (((int) Math.abs(y3 - this.P)) > this.S) {
                u();
                q(1);
            }
            this.Q = y3;
            return true;
        }
    }

    @DexIgnore
    public final void p(int i2) {
        h hVar = this.y;
        if (hVar != null) {
            hVar.a(this, i2, this.x);
        }
    }

    @DexIgnore
    public final void q(int i2) {
        if (this.e0 != i2) {
            this.e0 = i2;
            g gVar = this.z;
            if (gVar != null) {
                gVar.a(this, i2);
            }
        }
    }

    @DexIgnore
    public final void r(E57 e57) {
        if (Objects.equals(e57, this.J)) {
            if (!d()) {
                B();
            }
            q(0);
        } else if (this.e0 != 1) {
            B();
        }
    }

    @DexIgnore
    public final void s() {
        d dVar = this.O;
        if (dVar == null) {
            this.O = new d();
        } else {
            removeCallbacks(dVar);
        }
        postDelayed(this.O, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public void scrollBy(int i2, int i3) {
        int[] iArr = this.C;
        if (!this.V && i3 > 0 && iArr[2] <= this.v) {
            this.I = this.H;
        } else if (this.V || i3 >= 0 || iArr[2] < this.w) {
            this.I += i3;
            while (true) {
                int i4 = this.I;
                if (i4 - this.H <= this.t) {
                    break;
                }
                this.I = i4 - this.G;
                b(iArr);
                y(iArr[2], true);
                if (!this.V && iArr[2] <= this.v) {
                    this.I = this.H;
                }
            }
            while (true) {
                int i5 = this.I;
                if (i5 - this.H < (-this.t)) {
                    this.I = i5 + this.G;
                    j(iArr);
                    y(iArr[2], true);
                    if (!this.V && iArr[2] >= this.w) {
                        this.I = this.H;
                    }
                } else {
                    return;
                }
            }
        } else {
            this.I = this.H;
        }
    }

    @DexIgnore
    public void setDisplayedValues(String[] strArr) {
        if (!Arrays.equals(this.u, strArr)) {
            this.u = strArr;
            if (strArr != null) {
                this.g.setRawInputType(524289);
            } else {
                this.g.setRawInputType(2);
            }
            B();
            m();
            A();
        }
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        ImageButton imageButton;
        ImageButton imageButton2;
        super.setEnabled(z2);
        if (!this.b0 && (imageButton2 = this.e) != null) {
            imageButton2.setEnabled(z2);
        }
        if (!this.b0 && (imageButton = this.f) != null) {
            imageButton.setEnabled(z2);
        }
        this.g.setEnabled(z2);
    }

    @DexIgnore
    public void setFormatter(f fVar) {
        if (!Objects.equals(fVar, this.A)) {
            this.A = fVar;
            m();
            B();
        }
    }

    @DexIgnore
    public void setMaxValue(int i2) {
        if (this.w != i2) {
            if (i2 >= 0) {
                this.w = i2;
                if (i2 < this.x) {
                    this.x = i2;
                }
                setWrapSelectorWheel(this.w - this.v > this.C.length);
                m();
                B();
                A();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("maxValue must be >= 0");
        }
    }

    @DexIgnore
    public void setMinValue(int i2) {
        if (this.v != i2) {
            if (i2 >= 0) {
                this.v = i2;
                if (i2 > this.x) {
                    this.x = i2;
                }
                setWrapSelectorWheel(this.w - this.v > this.C.length);
                m();
                B();
                A();
                invalidate();
                return;
            }
            throw new IllegalArgumentException("minValue must be >= 0");
        }
    }

    @DexIgnore
    public void setOnValueChangedListener(h hVar) {
        this.y = hVar;
    }

    @DexIgnore
    public void setValue(int i2) {
        y(i2, false);
    }

    @DexIgnore
    public void t(boolean z2, long j2) {
        e eVar = this.N;
        if (eVar == null) {
            this.N = new e();
        } else {
            removeCallbacks(eVar);
        }
        this.N.a(z2);
        postDelayed(this.N, j2);
    }

    @DexIgnore
    public final void u() {
        e eVar = this.N;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
        j jVar = this.M;
        if (jVar != null) {
            removeCallbacks(jVar);
        }
        d dVar = this.O;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
        this.n0.c();
    }

    @DexIgnore
    public final void v() {
        d dVar = this.O;
        if (dVar != null) {
            removeCallbacks(dVar);
        }
    }

    @DexIgnore
    public final void w() {
        e eVar = this.N;
        if (eVar != null) {
            removeCallbacks(eVar);
        }
    }

    @DexIgnore
    public final int x(int i2, int i3, int i4) {
        return i2 != -1 ? resolveSizeAndState(Math.max(i2, i3), i4, 0) : i3;
    }

    @DexIgnore
    public final void y(int i2, boolean z2) {
        if (this.x != i2) {
            int h2 = this.V ? h(i2) : Math.min(Math.max(i2, this.v), this.w);
            int i3 = this.x;
            this.x = h2;
            B();
            if (z2) {
                p(i3);
            }
            m();
            invalidate();
        }
    }

    @DexIgnore
    public void z() {
    }
}
