package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.Hm7;
import com.fossil.Jl5;
import com.fossil.Rh5;
import com.fossil.Y47;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomEditGoalView extends ConstraintLayout {
    @DexIgnore
    public static /* final */ ArrayList<String> C; // = Hm7.c("primaryText", "goalNumber", "goalNumberDescription");
    @DexIgnore
    public static /* final */ ArrayList<String> D; // = Hm7.c("nonBrandDisableCalendarDay", "goalNumber", "goalNumberDescription");
    @DexIgnore
    public int A;
    @DexIgnore
    public Rh5 B;
    @DexIgnore
    public AppCompatImageView w;
    @DexIgnore
    public AutoResizeTextView x;
    @DexIgnore
    public FlexibleTextView y;
    @DexIgnore
    public String z; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomEditGoalView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(2131558840, (ViewGroup) this, true) : view;
        if (view != null) {
            View findViewById = view.findViewById(2131361853);
            Wg6.b(findViewById, "view.findViewById(R.id.aciv_icon)");
            this.w = (AppCompatImageView) findViewById;
            View findViewById2 = view.findViewById(2131363418);
            Wg6.b(findViewById2, "view.findViewById(R.id.tv_value)");
            this.x = (AutoResizeTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362444);
            Wg6.b(findViewById3, "view.findViewById(R.id.ftv_goal_type)");
            this.y = (FlexibleTextView) findViewById3;
            I("custom_edit_goal_disable");
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    public final Drawable G(int i) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(i);
        return gradientDrawable;
    }

    @DexIgnore
    public final void H(String str, String str2, String str3, String str4, String str5) {
        String d = ThemeManager.l.a().d(str3);
        Typeface f = ThemeManager.l.a().f(str5);
        Typeface f2 = ThemeManager.l.a().f(str4);
        String d2 = ThemeManager.l.a().d(str2);
        if (d != null) {
            this.y.setTextColor(Color.parseColor(d));
        }
        if (f != null) {
            this.y.setTypeface(f);
        }
        if (f2 != null) {
            this.x.setTypeface(f2);
        }
        if (d2 != null) {
            this.x.setTextColor(Color.parseColor(d2));
        }
        String d3 = ThemeManager.l.a().d(str);
        if (d3 != null) {
            this.w.setBackground(G(Color.parseColor(d3)));
        }
    }

    @DexIgnore
    public final void I(String str) {
        int hashCode = str.hashCode();
        if (hashCode != -836633469) {
            if (hashCode == 1806838920 && str.equals("custom_edit_goal_enable")) {
                String str2 = this.z;
                String str3 = C.get(0);
                Wg6.b(str3, "STATE_ENABLED_STYLES[0]");
                String str4 = C.get(1);
                Wg6.b(str4, "STATE_ENABLED_STYLES[1]");
                String str5 = C.get(2);
                Wg6.b(str5, "STATE_ENABLED_STYLES[2]");
                H(str2, str2, str3, str4, str5);
            }
        } else if (str.equals("custom_edit_goal_disable")) {
            String str6 = D.get(0);
            Wg6.b(str6, "STATE_DISABLED_STYLES[0]");
            String str7 = D.get(1);
            Wg6.b(str7, "STATE_DISABLED_STYLES[1]");
            String str8 = D.get(2);
            Wg6.b(str8, "STATE_DISABLED_STYLES[2]");
            H("nonBrandSurface", "nonBrandDisableCalendarDay", str6, str7, str8);
        }
    }

    @DexIgnore
    public final Rh5 getMGoalType() {
        return this.B;
    }

    @DexIgnore
    public final int getMValue() {
        return this.A;
    }

    @DexIgnore
    public final int getValue() {
        return this.A;
    }

    @DexIgnore
    public final void setMGoalType(Rh5 rh5) {
        this.B = rh5;
    }

    @DexIgnore
    public final void setMValue(int i) {
        this.A = i;
    }

    @DexIgnore
    public void setSelected(boolean z2) {
        if (z2) {
            I("custom_edit_goal_enable");
        } else {
            I("custom_edit_goal_disable");
        }
        super.setSelected(z2);
    }

    @DexIgnore
    public final void setType(Rh5 rh5) {
        Wg6.c(rh5, "type");
        this.B = rh5;
        int i = Y47.a[rh5.ordinal()];
        if (i == 1) {
            this.w.setImageDrawable(getContext().getDrawable(2131231065));
            this.z = "dianaActiveCaloriesTab";
            this.y.setText(getContext().getString(2131886630));
        } else if (i == 2) {
            this.w.setImageDrawable(getContext().getDrawable(2131231070));
            this.z = "dianaActiveMinutesTab";
            this.y.setText(getContext().getString(2131886638));
        } else if (i == 3) {
            this.w.setImageDrawable(getContext().getDrawable(2131231069));
            this.z = "hybridStepsTab";
            this.y.setText(getContext().getString(2131887124));
        } else if (i == 4) {
            this.w.setImageDrawable(getContext().getDrawable(2131231178));
            this.z = "dianaSleepTab";
            this.y.setText(getContext().getString(2131886767));
        } else if (i == 5) {
            this.w.setImageDrawable(getContext().getDrawable(2131231084));
            this.z = "hybridGoalTrackingTab";
            this.y.setText(getContext().getString(2131886449));
        }
    }

    @DexIgnore
    public final void setValue(int i) {
        this.A = i;
        Rh5 rh5 = this.B;
        if (rh5 != null) {
            int i2 = Y47.b[rh5.ordinal()];
            if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
                this.x.setText(Jl5.b.m(this.A));
            } else if (i2 == 5) {
                this.x.setText(Jl5.b.o(this.A));
            }
        }
    }
}
