package com.portfolio.platform.view.ruler;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Hr7;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RulerView extends View {
    @DexIgnore
    public int b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public Paint d;
    @DexIgnore
    public int e; // = 14;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 100;
    @DexIgnore
    public float h; // = 0.6f;
    @DexIgnore
    public float i; // = 0.4f;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public int m;
    @DexIgnore
    public Typeface s;
    @DexIgnore
    public RulerValuePicker.a t;
    @DexIgnore
    public int u; // = -1;
    @DexIgnore
    public int v; // = 36;
    @DexIgnore
    public float w; // = 4.0f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context) {
        super(context);
        Wg6.c(context, "context");
        f(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        f(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        f(attributeSet);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2) {
        int i3 = this.e;
        float f2 = (float) (i3 * i2);
        int i4 = this.b;
        float f3 = (float) i4;
        float f4 = (float) (i3 * i2);
        float f5 = (float) (i4 - this.j);
        Paint paint = this.c;
        if (paint != null) {
            canvas.drawLine(f2, f3, f4, f5, paint);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void b(Canvas canvas, int i2) {
        int i3 = this.e;
        float f2 = (float) (i3 * i2);
        int i4 = this.b;
        float f3 = (float) i4;
        float f4 = (float) (i3 * i2);
        float f5 = (float) (i4 - this.k);
        Paint paint = this.c;
        if (paint != null) {
            canvas.drawLine(f2, f3, f4, f5, paint);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2) {
        String d2 = d(this.f + i2);
        float f2 = (float) (this.e * i2);
        Paint paint = this.d;
        if (paint != null) {
            float textSize = paint.getTextSize();
            Paint paint2 = this.d;
            if (paint2 != null) {
                canvas.drawText(d2, f2, textSize, paint2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final String d(int i2) {
        RulerValuePicker.a aVar = this.t;
        if (aVar == null) {
            return e(i2);
        }
        if (aVar != null) {
            return aVar.format(i2);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final String e(int i2) {
        Hr7 hr7 = Hr7.a;
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        String format = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
        Wg6.b(format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    @DexIgnore
    public final void f(AttributeSet attributeSet) {
        if (attributeSet != null) {
            Context context = getContext();
            Wg6.b(context, "context");
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, X24.RulerView, 0, 0);
            try {
                if (obtainStyledAttributes.hasValue(6)) {
                    this.l = obtainStyledAttributes.getColor(6, -1);
                }
                if (obtainStyledAttributes.hasValue(7)) {
                    this.v = obtainStyledAttributes.getDimensionPixelSize(7, 24);
                }
                if (obtainStyledAttributes.hasValue(0)) {
                    this.u = obtainStyledAttributes.getColor(0, -1);
                }
                if (obtainStyledAttributes.hasValue(2)) {
                    this.w = (float) obtainStyledAttributes.getDimensionPixelSize(2, 4);
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.e = obtainStyledAttributes.getDimensionPixelSize(1, 4);
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.h = obtainStyledAttributes.getFraction(3, 1, 1, 0.6f);
                }
                if (obtainStyledAttributes.hasValue(8)) {
                    this.i = obtainStyledAttributes.getFraction(8, 1, 1, 0.4f);
                }
                h(this.h, this.i);
                if (obtainStyledAttributes.hasValue(5)) {
                    this.f = obtainStyledAttributes.getInteger(5, 0);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.g = obtainStyledAttributes.getInteger(4, 100);
                }
                i(this.f, this.g);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        g();
    }

    @DexIgnore
    public final void g() {
        Paint paint = new Paint(1);
        this.c = paint;
        if (paint != null) {
            paint.setColor(this.u);
            Paint paint2 = this.c;
            if (paint2 != null) {
                paint2.setStrokeWidth(this.w);
                Paint paint3 = this.c;
                if (paint3 != null) {
                    paint3.setStyle(Paint.Style.STROKE);
                    Paint paint4 = new Paint(1);
                    this.d = paint4;
                    if (paint4 != null) {
                        paint4.setColor(this.l);
                        Paint paint5 = this.d;
                        if (paint5 != null) {
                            paint5.setTextSize((float) this.v);
                            Typeface typeface = this.s;
                            if (typeface == null) {
                                Paint paint6 = this.d;
                                if (paint6 == null) {
                                    Wg6.i();
                                    throw null;
                                } else if (paint6 != null) {
                                    paint6.setTypeface(Typeface.create(paint6.getTypeface(), this.m));
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Paint paint7 = this.d;
                                if (paint7 != null) {
                                    paint7.setTypeface(Typeface.create(typeface, this.m));
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                            Paint paint8 = this.d;
                            if (paint8 != null) {
                                paint8.setTextAlign(Paint.Align.CENTER);
                                invalidate();
                                requestLayout();
                                return;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final int getIndicatorColor() {
        return this.u;
    }

    @DexIgnore
    public final int getIndicatorIntervalWidth() {
        return this.e;
    }

    @DexIgnore
    public final float getIndicatorWidth() {
        return this.w;
    }

    @DexIgnore
    public final float getLongIndicatorHeightRatio() {
        return this.h;
    }

    @DexIgnore
    public final int getMaxValue() {
        return this.g;
    }

    @DexIgnore
    public final int getMinValue() {
        return this.f;
    }

    @DexIgnore
    public final float getShortIndicatorHeightRatio() {
        return this.i;
    }

    @DexIgnore
    public final int getTextColor() {
        return this.u;
    }

    @DexIgnore
    public final float getTextSize() {
        return (float) this.v;
    }

    @DexIgnore
    public final void h(float f2, float f3) {
        float f4 = (float) 0;
        if (f3 >= f4) {
            float f5 = (float) 1;
            if (f3 <= f5) {
                if (f2 < f4 || f2 > f5) {
                    throw new IllegalArgumentException("Long indicator height must be between 0 to 1.");
                } else if (f3 <= f2) {
                    this.h = f2;
                    this.i = f3;
                    j(f2, f3);
                    invalidate();
                    return;
                } else {
                    throw new IllegalArgumentException("Long indicator height cannot be less than sort indicator height.");
                }
            }
        }
        throw new IllegalArgumentException("Sort indicator height must be between 0 to 1.");
    }

    @DexIgnore
    public final void i(int i2, int i3) {
        this.f = i2;
        this.g = i3;
    }

    @DexIgnore
    public final void j(float f2, float f3) {
        int i2 = this.b;
        this.j = (int) (((float) i2) * f2);
        this.k = (int) (((float) i2) * f3);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        int i2 = this.g;
        int i3 = this.f;
        for (int i4 = 1; i4 < i2 - i3; i4++) {
            RulerValuePicker.a aVar = this.t;
            if (aVar != null) {
                if (aVar == null) {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.data.ProfileFormatter");
                } else if (((ProfileFormatter) aVar).getMUnit() == 3) {
                    if (i4 % 12 == 0) {
                        a(canvas, i4);
                        c(canvas, i4);
                    } else {
                        b(canvas, i4);
                    }
                }
            }
            if (i4 % 5 == 0) {
                a(canvas, i4);
                if (i4 % 10 == 0) {
                    c(canvas, i4);
                }
            } else {
                b(canvas, i4);
            }
        }
        b(canvas, 0);
        b(canvas, getWidth());
        float f2 = (float) this.b;
        float measuredWidth = (float) getMeasuredWidth();
        float f3 = (float) this.b;
        Paint paint = this.c;
        if (paint != null) {
            canvas.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, measuredWidth, f3, paint);
            super.onDraw(canvas);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        this.b = View.MeasureSpec.getSize(i3);
        int i4 = this.g;
        int i5 = this.f;
        int i6 = this.e;
        j(this.h, this.i);
        setMeasuredDimension(((i4 - i5) - 1) * i6, this.b);
    }

    @DexIgnore
    public final void setFormatter(RulerValuePicker.a aVar) {
        Wg6.c(aVar, "formatter");
        if (!Wg6.a(aVar, this.t)) {
            this.t = aVar;
            invalidate();
            requestLayout();
        }
    }

    @DexIgnore
    public final void setIndicatorColor(int i2) {
        this.u = i2;
        g();
    }

    @DexIgnore
    public final void setIndicatorIntervalDistance(int i2) {
        if (i2 > 0) {
            this.e = i2;
            invalidate();
            return;
        }
        throw new IllegalArgumentException("Interval cannot be negative or zero.");
    }

    @DexIgnore
    public final void setIndicatorWidth(int i2) {
        this.w = (float) i2;
        g();
    }

    @DexIgnore
    public final void setTextColor(int i2) {
        this.l = i2;
        g();
    }

    @DexIgnore
    public final void setTextSize(int i2) {
        this.v = i2;
        g();
    }

    @DexIgnore
    public final void setTextStyle(int i2) {
        this.m = i2;
        g();
    }

    @DexIgnore
    public final void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            this.s = typeface;
            g();
        }
    }
}
