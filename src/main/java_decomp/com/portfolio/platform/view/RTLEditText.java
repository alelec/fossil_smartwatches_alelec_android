package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.Il5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLEditText extends FlexibleEditText {
    @DexIgnore
    public RTLEditText(Context context) {
        super(context);
        h(context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        h(context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        h(context);
    }

    @DexIgnore
    public final void h(Context context) {
        Il5.b(this, context);
    }
}
