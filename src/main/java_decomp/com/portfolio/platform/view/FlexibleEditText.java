package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatEditText;
import com.fossil.H57;
import com.fossil.L47;
import com.fossil.Um5;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleEditText extends AppCompatEditText {
    @DexIgnore
    public /* final */ String e; // = "FlexibleEditText";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l; // = FlexibleTextView.u.a();
    @DexIgnore
    public String m; // = "";
    @DexIgnore
    public String s; // = "primaryColor";
    @DexIgnore
    public /* final */ LayerDrawable t;
    @DexIgnore
    public /* final */ GradientDrawable u;
    @DexIgnore
    public boolean v;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context) {
        super(context);
        Wg6.c(context, "context");
        LayerDrawable layerDrawable = (LayerDrawable) W6.f(getContext(), 2131230852);
        this.t = layerDrawable;
        this.u = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363115) : null);
        this.v = true;
        b(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        LayerDrawable layerDrawable = (LayerDrawable) W6.f(getContext(), 2131230852);
        this.t = layerDrawable;
        this.u = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363115) : null);
        this.v = true;
        b(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleEditText(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        LayerDrawable layerDrawable = (LayerDrawable) W6.f(getContext(), 2131230852);
        this.t = layerDrawable;
        this.u = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363115) : null);
        this.v = true;
        b(attributeSet);
    }

    @DexIgnore
    public final String a(int i2) {
        String c = Um5.c(PortfolioApp.get.instance(), i2);
        Wg6.b(c, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return c;
    }

    @DexIgnore
    public final void b(AttributeSet attributeSet) {
        CharSequence charSequence;
        CharSequence charSequence2;
        this.j = 0;
        Editable text = getText();
        if (text != null) {
            CharSequence hint = getHint();
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleEditText);
                this.j = obtainStyledAttributes.getInt(7, 0);
                this.k = obtainStyledAttributes.getInt(3, 0);
                this.l = obtainStyledAttributes.getColor(8, FlexibleTextView.u.a());
                String string = obtainStyledAttributes.getString(5);
                if (string == null) {
                    string = "";
                }
                this.f = string;
                String string2 = obtainStyledAttributes.getString(6);
                if (string2 == null) {
                    string2 = "";
                }
                this.g = string2;
                String string3 = obtainStyledAttributes.getString(4);
                if (string3 == null) {
                    string3 = "";
                }
                this.h = string3;
                String string4 = obtainStyledAttributes.getString(0);
                if (string4 == null) {
                    string4 = "";
                }
                this.m = string4;
                this.v = obtainStyledAttributes.getBoolean(1, true);
                String string5 = obtainStyledAttributes.getString(2);
                if (string5 == null) {
                    string5 = "";
                }
                this.i = string5;
                obtainStyledAttributes.recycle();
                TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
                int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
                charSequence2 = resourceId != -1 ? a(resourceId) : text;
                int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
                charSequence = resourceId2 != -1 ? a(resourceId2) : hint;
                obtainStyledAttributes2.recycle();
            } else {
                charSequence = hint;
                charSequence2 = text;
            }
            if (!TextUtils.isEmpty(charSequence2)) {
                setText(charSequence2);
            }
            if (!TextUtils.isEmpty(charSequence)) {
                Wg6.b(charSequence, "hint");
                setHint(g(charSequence, this.k));
            }
            if (this.l != FlexibleTextView.u.a()) {
                H57.b(this, this.l);
            }
            if (TextUtils.isEmpty(this.f)) {
                this.f = "primaryText";
            }
            if (TextUtils.isEmpty(this.g)) {
                this.g = "nonBrandTextStyle2";
            }
            d(this.f, this.g, this.h, this.i, this.m);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final CharSequence c(CharSequence charSequence) {
        return g(charSequence, this.j);
    }

    @DexIgnore
    public final void d(String str, String str2, String str3, String str4, String str5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = this.e;
        local.d(str6, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String d = ThemeManager.l.a().d(str);
        Typeface f2 = ThemeManager.l.a().f(str2);
        String d2 = ThemeManager.l.a().d(str3);
        String d3 = ThemeManager.l.a().d(str4);
        String d4 = ThemeManager.l.a().d(str5);
        if (d != null) {
            setTextColor(Color.parseColor(d));
        }
        if (f2 != null) {
            setTypeface(f2);
        }
        if (d2 != null) {
            setBackgroundColor(Color.parseColor(d2));
        }
        if (d3 != null) {
            setHintTextColor(Color.parseColor(d3));
        }
        if (d4 != null) {
            GradientDrawable gradientDrawable = this.u;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(d4));
            }
            setBackground(this.t);
        }
    }

    @DexIgnore
    public final void e() {
        Object systemService = PortfolioApp.get.instance().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).showSoftInput(this, 0);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public final void f(boolean z) {
        String d;
        String str = z ? this.s : this.m;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.e;
        local.d(str2, "switchBorderColor " + str);
        if (!TextUtils.isEmpty(str) && (d = ThemeManager.l.a().d(str)) != null) {
            GradientDrawable gradientDrawable = this.u;
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(d));
            }
            setBackground(this.t);
        }
    }

    @DexIgnore
    public final CharSequence g(CharSequence charSequence, int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? i2 != 4 ? i2 != 5 ? charSequence : L47.c(charSequence) : L47.e(charSequence) : L47.d(charSequence) : L47.b(charSequence) : L47.a(charSequence);
    }

    @DexIgnore
    public final LayerDrawable getShape() {
        return this.t;
    }

    @DexIgnore
    public boolean onKeyPreIme(int i2, KeyEvent keyEvent) {
        Wg6.c(keyEvent, Constants.EVENT);
        if (keyEvent.getKeyCode() != 4) {
            return false;
        }
        clearFocus();
        return false;
    }

    @DexIgnore
    public void onSelectionChanged(int i2, int i3) {
        Editable text;
        if (this.v || (text = getText()) == null || (i2 == text.length() && i3 == text.length())) {
            super.onSelectionChanged(i2, i3);
        } else {
            setSelection(text.length(), text.length());
        }
    }

    @DexIgnore
    @Override // android.widget.TextView, android.widget.EditText
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        Wg6.c(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        super.setText(c(charSequence), bufferType);
    }
}
