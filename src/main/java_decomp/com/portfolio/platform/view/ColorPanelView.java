package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;
import com.fossil.Mo0;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Py5;
import com.mapped.X24;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPanelView extends View {
    @DexIgnore
    public Paint b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public Paint d;
    @DexIgnore
    public Paint e;
    @DexIgnore
    public Rect f;
    @DexIgnore
    public Rect g;
    @DexIgnore
    public RectF h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;

    @DexIgnore
    public ColorPanelView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public ColorPanelView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ColorPanelView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.h = new RectF();
        this.k = -9539986;
        this.l = -16777216;
        a(context, attributeSet);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.ColorPanelView);
        this.m = obtainStyledAttributes.getInt(1, 1);
        boolean z = obtainStyledAttributes.getBoolean(2, false);
        this.i = z;
        if (!z || this.m == 1) {
            this.k = obtainStyledAttributes.getColor(0, -9539986);
            obtainStyledAttributes.recycle();
            if (this.k == -9539986) {
                TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
                this.k = obtainStyledAttributes2.getColor(0, this.k);
                obtainStyledAttributes2.recycle();
            }
            this.j = Py5.a(context, 1.0f);
            Paint paint = new Paint();
            this.b = paint;
            paint.setAntiAlias(true);
            Paint paint2 = new Paint();
            this.c = paint2;
            paint2.setAntiAlias(true);
            if (this.i) {
                this.e = new Paint();
            }
            if (this.m == 1) {
                Bitmap bitmap = ((BitmapDrawable) context.getResources().getDrawable(2131230942)).getBitmap();
                Paint paint3 = new Paint();
                this.d = paint3;
                paint3.setAntiAlias(true);
                Shader.TileMode tileMode = Shader.TileMode.REPEAT;
                this.d.setShader(new BitmapShader(bitmap, tileMode, tileMode));
                return;
            }
            return;
        }
        throw new IllegalStateException("Color preview is only available in circle mode");
    }

    @DexIgnore
    public final void b() {
        Rect rect = this.f;
        int i2 = rect.left;
        int i3 = this.j;
        this.h = new RectF((float) (i2 + i3), (float) (rect.top + i3), (float) (rect.right - i3), (float) (rect.bottom - i3));
    }

    @DexIgnore
    public final void c() {
        Rect rect = this.f;
        int i2 = rect.left;
        int i3 = this.j;
        this.g = new Rect(i2 + i3, rect.top + i3, rect.right - i3, rect.bottom - i3);
    }

    @DexIgnore
    public void d() {
        int[] iArr = new int[2];
        Rect rect = new Rect();
        getLocationOnScreen(iArr);
        getWindowVisibleDisplayFrame(rect);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i2 = iArr[1];
        int i3 = height / 2;
        int i4 = (width / 2) + iArr[0];
        if (Mo0.z(this) == 0) {
            i4 = context.getResources().getDisplayMetrics().widthPixels - i4;
        }
        StringBuilder sb = new StringBuilder("#");
        if (Color.alpha(this.l) != 255) {
            sb.append(Integer.toHexString(this.l).toUpperCase(Locale.ENGLISH));
        } else {
            sb.append(String.format("%06X", Integer.valueOf(16777215 & this.l)).toUpperCase(Locale.ENGLISH));
        }
        Toast makeText = Toast.makeText(context, sb.toString(), 0);
        if (i2 + i3 < rect.height()) {
            makeText.setGravity(8388661, i4, (iArr[1] + height) - rect.top);
        } else {
            makeText.setGravity(81, 0, height);
        }
        makeText.show();
    }

    @DexIgnore
    public int getBorderColor() {
        return this.k;
    }

    @DexIgnore
    public int getColor() {
        return this.l;
    }

    @DexIgnore
    public int getShape() {
        return this.m;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        this.b.setColor(this.k);
        this.c.setColor(this.l);
        int i2 = this.m;
        if (i2 == 0) {
            if (this.j > 0) {
                canvas.drawRect(this.f, this.b);
            }
            canvas.drawRect(this.g, this.c);
        } else if (i2 == 1) {
            int measuredWidth = getMeasuredWidth() / 2;
            if (this.j > 0) {
                canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) measuredWidth, this.b);
            }
            if (Color.alpha(this.l) < 255) {
                canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) (measuredWidth - this.j), this.d);
            }
            if (this.i) {
                canvas.drawArc(this.h, 90.0f, 180.0f, true, this.e);
                canvas.drawArc(this.h, 270.0f, 180.0f, true, this.c);
                return;
            }
            canvas.drawCircle((float) (getMeasuredWidth() / 2), (float) (getMeasuredHeight() / 2), (float) (measuredWidth - this.j), this.c);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4 = this.m;
        if (i4 == 0) {
            setMeasuredDimension(View.MeasureSpec.getSize(i2), View.MeasureSpec.getSize(i3));
        } else if (i4 == 1) {
            super.onMeasure(i2, i2);
            setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
        } else {
            super.onMeasure(i2, i3);
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.l = bundle.getInt(BaseFeatureModel.COLUMN_COLOR);
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt(BaseFeatureModel.COLUMN_COLOR, this.l);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.m == 0 || this.i) {
            Rect rect = new Rect();
            this.f = rect;
            rect.left = getPaddingLeft();
            this.f.right = i2 - getPaddingRight();
            this.f.top = getPaddingTop();
            this.f.bottom = i3 - getPaddingBottom();
            if (this.i) {
                b();
            } else {
                c();
            }
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        this.k = i2;
        invalidate();
    }

    @DexIgnore
    public void setColor(int i2) {
        this.l = i2;
        invalidate();
    }

    @DexIgnore
    public void setOriginalColor(int i2) {
        Paint paint = this.e;
        if (paint != null) {
            paint.setColor(i2);
        }
    }

    @DexIgnore
    public void setShape(int i2) {
        this.m = i2;
        invalidate();
    }
}
