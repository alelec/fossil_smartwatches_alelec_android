package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTabLayout extends TabLayout {
    @DexIgnore
    public String V; // = "";
    @DexIgnore
    public String W; // = "";
    @DexIgnore
    public String a0; // = "";
    @DexIgnore
    public String b0; // = "";
    @DexIgnore
    public String c0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context) {
        super(context);
        Wg6.c(context, "context");
        N(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        N(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        N(attributeSet);
    }

    @DexIgnore
    public final void N(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.TabLayout);
            String string = obtainStyledAttributes.getString(25);
            if (string == null) {
                string = "";
            }
            this.V = string;
            String string2 = obtainStyledAttributes.getString(26);
            if (string2 == null) {
                string2 = "";
            }
            this.W = string2;
            String string3 = obtainStyledAttributes.getString(27);
            if (string3 == null) {
                string3 = "";
            }
            this.a0 = string3;
            String string4 = obtainStyledAttributes.getString(28);
            if (string4 == null) {
                string4 = "";
            }
            this.b0 = string4;
            String string5 = obtainStyledAttributes.getString(29);
            if (string5 == null) {
                string5 = "";
            }
            this.c0 = string5;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.V)) {
            this.V = "nonBrandSurface";
        }
        if (TextUtils.isEmpty(this.W)) {
            this.W = "primaryColor";
        }
        if (TextUtils.isEmpty(this.a0)) {
            this.a0 = "primaryText";
        }
        if (TextUtils.isEmpty(this.b0)) {
            this.b0 = "secondaryText";
        }
        if (TextUtils.isEmpty(this.c0)) {
            this.c0 = "descriptionText1";
        }
        O();
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void O() {
        if (!TextUtils.isEmpty(this.V) && !TextUtils.isEmpty(this.W) && !TextUtils.isEmpty(this.a0) && !TextUtils.isEmpty(this.b0)) {
            String d = ThemeManager.l.a().d(this.V);
            String d2 = ThemeManager.l.a().d(this.W);
            String d3 = ThemeManager.l.a().d(this.a0);
            String d4 = ThemeManager.l.a().d(this.b0);
            if (!TextUtils.isEmpty(d2)) {
                setSelectedTabIndicatorColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d4) && !TextUtils.isEmpty(d3)) {
                H(Color.parseColor(d4), Color.parseColor(d3));
            }
            if (!TextUtils.isEmpty(d)) {
                setBackgroundColor(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    @Override // com.google.android.material.tabs.TabLayout
    public void d(TabLayout.g gVar) {
        Typeface f;
        Wg6.c(gVar, "tab");
        super.d(gVar);
        if (!TextUtils.isEmpty(this.c0) && (f = ThemeManager.l.a().f(this.c0)) != null) {
            View childAt = getChildAt(0);
            if (childAt != null) {
                View childAt2 = ((ViewGroup) childAt).getChildAt(gVar.f());
                if (childAt2 != null) {
                    ViewGroup viewGroup = (ViewGroup) childAt2;
                    int childCount = viewGroup.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View childAt3 = viewGroup.getChildAt(i);
                        Wg6.b(childAt3, "tabView.getChildAt(i)");
                        if (childAt3 instanceof TextView) {
                            ((TextView) childAt3).setTypeface(f, 0);
                        }
                    }
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
            }
            throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }
}
