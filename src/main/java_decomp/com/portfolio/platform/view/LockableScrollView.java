package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.core.widget.NestedScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LockableScrollView extends NestedScrollView {
    @DexIgnore
    public boolean I; // = true;

    @DexIgnore
    public LockableScrollView(Context context) {
        super(context);
    }

    @DexIgnore
    public LockableScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public LockableScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @DexIgnore
    @Override // androidx.core.widget.NestedScrollView
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.I && super.onInterceptTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // androidx.core.widget.NestedScrollView
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return super.onTouchEvent(motionEvent);
        }
        if (this.I) {
            return super.onTouchEvent(motionEvent);
        }
        return false;
    }
}
