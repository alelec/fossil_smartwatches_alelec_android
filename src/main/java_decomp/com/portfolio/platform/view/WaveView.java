package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WaveView extends View {
    @DexIgnore
    public Paint b;
    @DexIgnore
    public int c;
    @DexIgnore
    public float d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l;
    @DexIgnore
    public long m;

    @DexIgnore
    public WaveView(Context context) {
        this(context, null, 2, null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.b = new Paint(1);
        this.c = 3;
        this.d = 374.0f;
        this.e = 1670;
        this.f = 330;
        this.g = 244.0f;
        this.h = 618.0f;
        this.i = 318.0f;
        this.j = 6.0f;
        this.k = 50;
        this.l = "nonBrandSurface";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WaveView(Context context, AttributeSet attributeSet, int i2, Qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        Context context;
        TypedArray obtainStyledAttributes;
        this.b.setStyle(Paint.Style.STROKE);
        if (attributeSet != null && (context = getContext()) != null && (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.WaveView)) != null) {
            try {
                String string = obtainStyledAttributes.getString(0);
                if (string == null) {
                    string = "primaryText";
                }
                this.l = string;
                b();
                this.c = obtainStyledAttributes.getInteger(1, 3);
                this.e = obtainStyledAttributes.getInteger(2, 1670);
                this.f = obtainStyledAttributes.getInteger(3, 330);
                obtainStyledAttributes.getInteger(4, 417);
                this.g = (float) obtainStyledAttributes.getDimensionPixelSize(6, 244);
                float dimensionPixelSize = (float) obtainStyledAttributes.getDimensionPixelSize(5, 618);
                this.h = dimensionPixelSize;
                float f2 = dimensionPixelSize - this.g;
                this.d = f2;
                this.i = ((f2 * ((float) this.f)) / ((float) this.e)) + this.g;
                this.j = (float) obtainStyledAttributes.getDimensionPixelSize(8, 6);
                this.k = obtainStyledAttributes.getDimensionPixelSize(7, 50);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("WaveView", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        String d2 = ThemeManager.l.a().d(this.l);
        if (d2 != null) {
            this.b.setColor(Color.parseColor(d2));
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        long j2;
        int i2;
        float f2;
        Wg6.c(canvas, "canvas");
        super.dispatchDraw(canvas);
        long currentTimeMillis = System.currentTimeMillis();
        int width = getWidth() / 2;
        int height = getHeight() / 2;
        long j3 = this.m;
        if (j3 == 0) {
            this.m = currentTimeMillis;
            j2 = 0;
        } else {
            j2 = currentTimeMillis - j3;
        }
        float f3 = j2 == 0 ? this.g : ((((float) j2) * this.d) / ((float) this.e)) + this.g;
        if (f3 - ((float) ((this.c - 1) * this.k)) > this.h) {
            f3 = this.g;
            this.m = currentTimeMillis;
        }
        float f4 = f3;
        for (int i3 = 0; i3 < this.c; i3++) {
            float f5 = this.g;
            if (f4 < f5) {
                break;
            }
            float f6 = this.i;
            if (f4 < f6) {
                float f7 = (f4 - f5) / (f6 - f5);
                i2 = (int) (((float) 255) * f7);
                f2 = f7 * this.j;
            } else {
                float f8 = (f4 - f6) / (this.h - f6);
                i2 = 255 - ((int) (((float) 255) * f8));
                if (i2 < 0) {
                    i2 = 0;
                }
                float f9 = this.j;
                f2 = f9 - (f8 * f9);
                if (f2 < ((float) 0)) {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
            }
            this.b.setAlpha(i2);
            this.b.setStrokeWidth(f2);
            canvas.drawCircle((float) width, (float) height, f4, this.b);
            f4 -= (float) this.k;
        }
        postInvalidateDelayed(0);
    }
}
