package com.portfolio.platform.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.E01;
import com.fossil.Hn0;
import com.fossil.Zi0;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLViewPager extends ViewPager {
    @DexIgnore
    public /* final */ Map<ViewPager.i, d> q0; // = new Zi0(1);
    @DexIgnore
    public DataSetObserver r0;
    @DexIgnore
    public boolean s0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends E01 {
        @DexIgnore
        public /* final */ E01 b;

        @DexIgnore
        public a(E01 e01) {
            this.b = e01;
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void b(ViewGroup viewGroup, int i, Object obj) {
            this.b.b(viewGroup, i, obj);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void d(ViewGroup viewGroup) {
            this.b.d(viewGroup);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public int e() {
            return this.b.e();
        }

        @DexIgnore
        @Override // com.fossil.E01
        public int f(Object obj) {
            return this.b.f(obj);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public CharSequence g(int i) {
            return this.b.g(i);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public float h(int i) {
            return this.b.h(i);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public Object j(ViewGroup viewGroup, int i) {
            return this.b.j(viewGroup, i);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public boolean k(View view, Object obj) {
            return this.b.k(view, obj);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void l(DataSetObserver dataSetObserver) {
            this.b.l(dataSetObserver);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void m(Parcelable parcelable, ClassLoader classLoader) {
            this.b.m(parcelable, classLoader);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public Parcelable n() {
            return this.b.n();
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void p(ViewGroup viewGroup, int i, Object obj) {
            this.b.p(viewGroup, i, obj);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void s(ViewGroup viewGroup) {
            this.b.s(viewGroup);
        }

        @DexIgnore
        @Override // com.fossil.E01
        public void t(DataSetObserver dataSetObserver) {
            this.b.t(dataSetObserver);
        }

        @DexIgnore
        public E01 u() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends DataSetObserver {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onChanged() {
            super.onChanged();
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends a {
        @DexIgnore
        public int c;

        @DexIgnore
        public c(E01 e01) {
            super(e01);
            this.c = e01.e();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public void b(ViewGroup viewGroup, int i, Object obj) {
            super.b(viewGroup, w(i), obj);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public int f(Object obj) {
            int f = super.f(obj);
            return f < 0 ? f : w(f);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public CharSequence g(int i) {
            return super.g(w(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public float h(int i) {
            return super.h(w(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public Object j(ViewGroup viewGroup, int i) {
            return super.j(viewGroup, w(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.RTLViewPager.a, com.fossil.E01
        public void p(ViewGroup viewGroup, int i, Object obj) {
            super.p(viewGroup, (this.c - i) - 1, obj);
        }

        @DexIgnore
        public void v() {
            int e = e();
            int i = this.c;
            if (e != i) {
                RTLViewPager.this.setCurrentItemWithoutNotification(Math.max(0, i - 1));
                this.c = e;
            }
        }

        @DexIgnore
        public final int w(int i) {
            return (e() - i) - 1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements ViewPager.i {
        @DexIgnore
        public /* final */ ViewPager.i b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public d(ViewPager.i iVar) {
            this.b = iVar;
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void a(int i, float f, int i2) {
            if (!RTLViewPager.this.s0) {
                int i3 = (f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i3 == 0 && i2 == 0) {
                    this.c = b(i);
                } else {
                    this.c = b(i + 1);
                }
                ViewPager.i iVar = this.b;
                int i4 = this.c;
                if (i3 > 0) {
                    f = 1.0f - f;
                }
                iVar.a(i4, f, i2);
            }
        }

        @DexIgnore
        public final int b(int i) {
            E01 adapter = RTLViewPager.this.getAdapter();
            return adapter == null ? i : (adapter.e() - i) - 1;
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void c(int i) {
            if (!RTLViewPager.this.s0) {
                this.b.c(i);
            }
        }

        @DexIgnore
        @Override // androidx.viewpager.widget.ViewPager.i
        public void d(int i) {
            if (!RTLViewPager.this.s0) {
                this.b.d(b(i));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.ClassLoaderCreator<e> CREATOR; // = new a();
        @DexIgnore
        public /* final */ Parcelable b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.ClassLoaderCreator<e> {
            @DexIgnore
            public e a(Parcel parcel) {
                return new e(parcel, null);
            }

            @DexIgnore
            public e b(Parcel parcel, ClassLoader classLoader) {
                return new e(parcel, classLoader);
            }

            @DexIgnore
            public e[] c(int i) {
                return new e[i];
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.ClassLoaderCreator
            public /* bridge */ /* synthetic */ e createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return b(parcel, classLoader);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Object[] newArray(int i) {
                return c(i);
            }
        }

        @DexIgnore
        public e(Parcel parcel, ClassLoader classLoader) {
            this.b = parcel.readParcelable(classLoader == null ? e.class.getClassLoader() : classLoader);
            this.c = parcel.readInt();
            this.d = parcel.readByte() != 0;
        }

        @DexIgnore
        public e(Parcelable parcelable, int i, boolean z) {
            this.b = parcelable;
            this.c = i;
            this.d = z;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.b, i);
            parcel.writeInt(this.c);
            parcel.writeByte(this.d ? (byte) 1 : 0);
        }
    }

    @DexIgnore
    public RTLViewPager(Context context) {
        super(context);
    }

    @DexIgnore
    public RTLViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void N(ViewPager.i iVar) {
        super.N(a0() ? this.q0.remove(iVar) : iVar);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void R(int i, boolean z) {
        super.R(Z(i), z);
    }

    @DexIgnore
    public final int Z(int i) {
        if (i < 0 || !a0()) {
            return i;
        }
        if (getAdapter() == null) {
            return 0;
        }
        return (getAdapter().e() - i) - 1;
    }

    @DexIgnore
    public final boolean a0() {
        return Hn0.b(getContext().getResources().getConfiguration().locale) == 1;
    }

    @DexIgnore
    public final void b0(E01 e01) {
        if ((e01 instanceof c) && this.r0 == null) {
            c cVar = (c) e01;
            b bVar = new b(cVar);
            this.r0 = bVar;
            e01.l(bVar);
            cVar.v();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void c(ViewPager.i iVar) {
        if (a0()) {
            d dVar = new d(iVar);
            this.q0.put(iVar, dVar);
            iVar = dVar;
        }
        super.c(iVar);
    }

    @DexIgnore
    public final void c0() {
        DataSetObserver dataSetObserver;
        E01 adapter = super.getAdapter();
        if ((adapter instanceof c) && (dataSetObserver = this.r0) != null) {
            adapter.t(dataSetObserver);
            this.r0 = null;
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public E01 getAdapter() {
        E01 adapter = super.getAdapter();
        return adapter instanceof c ? ((c) adapter).u() : adapter;
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public int getCurrentItem() {
        return Z(super.getCurrentItem());
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        b0(super.getAdapter());
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onDetachedFromWindow() {
        c0();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void onRestoreInstanceState(Parcelable parcelable) {
        e eVar = (e) parcelable;
        super.onRestoreInstanceState(eVar.b);
        if (eVar.d != a0()) {
            R(eVar.c, false);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public Parcelable onSaveInstanceState() {
        return new e(super.onSaveInstanceState(), getCurrentItem(), a0());
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void s(float f) {
        if (!a0()) {
            f = -f;
        }
        super.s(f);
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void setAdapter(E01 e01) {
        c0();
        boolean z = e01 != null && a0();
        if (z) {
            c cVar = new c(e01);
            b0(cVar);
            e01 = cVar;
        }
        super.setAdapter(e01);
        if (z) {
            setCurrentItemWithoutNotification(0);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager
    public void setCurrentItem(int i) {
        super.setCurrentItem(Z(i));
    }

    @DexIgnore
    public void setCurrentItemWithoutNotification(int i) {
        this.s0 = true;
        R(i, false);
        this.s0 = false;
    }
}
