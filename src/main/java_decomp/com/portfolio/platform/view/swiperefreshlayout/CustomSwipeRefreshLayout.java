package com.portfolio.platform.view.swiperefreshlayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import com.facebook.internal.NativeProtocol;
import com.fossil.L67;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomSwipeRefreshLayout extends ViewGroup {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public /* final */ Runnable B;
    @DexIgnore
    public /* final */ j C;
    @DexIgnore
    public /* final */ Runnable D;
    @DexIgnore
    public /* final */ Runnable E;
    @DexIgnore
    public /* final */ f F;
    @DexIgnore
    public DecelerateInterpolator b;
    @DexIgnore
    public /* final */ e c;
    @DexIgnore
    public d d;
    @DexIgnore
    public d e;
    @DexIgnore
    public View f;
    @DexIgnore
    public View g;
    @DexIgnore
    public MotionEvent h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public float l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public c y;
    @DexIgnore
    public /* final */ i z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animation.AnimationListener {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
            Wg6.c(animation, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Wg6.c(animation, "animation");
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(d dVar, d dVar2);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void c(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public int a;
        @DexIgnore
        public float b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public d(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final void b(int i, int i2, int i3) {
            this.a = i;
            this.c = i2;
            this.d = i3;
            this.b = ((float) i2) / ((float) i3);
        }

        @DexIgnore
        public String toString() {
            return "[refreshState = " + this.a + ", percent = " + this.b + ", top = " + this.c + ", trigger = " + this.d + "]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends Animation {
        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            Wg6.c(transformation, "t");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends Animation {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            Wg6.c(transformation, "t");
            int mTargetOriginalTop$app_fossilRelease = this.b.getMTargetOriginalTop$app_fossilRelease();
            if (this.b.getMFrom$app_fossilRelease() != this.b.getMTargetOriginalTop$app_fossilRelease()) {
                mTargetOriginalTop$app_fossilRelease = this.b.getMFrom$app_fossilRelease() + ((int) (((float) (this.b.getMTargetOriginalTop$app_fossilRelease() - this.b.getMFrom$app_fossilRelease())) * f));
            }
            View mTarget$app_fossilRelease = this.b.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                int top = mTargetOriginalTop$app_fossilRelease - mTarget$app_fossilRelease.getTop();
                View mTarget$app_fossilRelease2 = this.b.getMTarget$app_fossilRelease();
                if (mTarget$app_fossilRelease2 != null) {
                    int top2 = mTarget$app_fossilRelease2.getTop();
                    if (top + top2 < 0) {
                        top = 0 - top2;
                    }
                    this.b.m(top, true);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        public g(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.b.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.b;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.e(mTarget$app_fossilRelease.getTop(), this.b.z);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        public h(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            this.b.setMInReturningAnimation$app_fossilRelease(true);
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.b;
            View mTarget$app_fossilRelease = customSwipeRefreshLayout.getMTarget$app_fossilRelease();
            if (mTarget$app_fossilRelease != null) {
                customSwipeRefreshLayout.e(mTarget$app_fossilRelease.getTop(), this.b.z);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout c;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super();
            this.c = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            Wg6.c(animation, "animation");
            this.c.setMInReturningAnimation$app_fossilRelease(false);
            this.c.setStartSwipe$app_fossilRelease(true);
            if (this.c.getLastState$app_fossilRelease().a() == 2) {
                c mListener$app_fossilRelease = this.c.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.c(true);
                    return;
                }
                return;
            }
            c mListener$app_fossilRelease2 = this.c.getMListener$app_fossilRelease();
            if (mListener$app_fossilRelease2 != null) {
                mListener$app_fossilRelease2.c(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends a {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout c;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            super();
            this.c = customSwipeRefreshLayout;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            Wg6.c(animation, "animation");
            this.c.getMReturnToStartPosition$app_fossilRelease().run();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CustomSwipeRefreshLayout b;

        @DexIgnore
        public k(CustomSwipeRefreshLayout customSwipeRefreshLayout) {
            this.b = customSwipeRefreshLayout;
        }

        @DexIgnore
        public final void run() {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.b;
            customSwipeRefreshLayout.f(customSwipeRefreshLayout.C);
        }
    }

    /*
    static {
        String simpleName = CustomSwipeRefreshLayout.class.getSimpleName();
        Wg6.b(simpleName, "CustomSwipeRefreshLayout::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.c = new e();
        this.d = new d(0);
        this.e = new d(-1);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        Wg6.b(viewConfiguration, "ViewConfiguration.get(context)");
        this.i = viewConfiguration.getScaledTouchSlop();
        this.k = -1;
        this.t = true;
        this.u = true;
        this.z = new i(this);
        this.B = new h(this);
        this.C = new j(this);
        this.D = new k(this);
        this.E = new g(this);
        this.F = new f(this);
        setWillNotDraw(false);
        this.b = new DecelerateInterpolator(2.0f);
        new AccelerateInterpolator(1.5f);
        l();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CustomSwipeRefreshLayout(Context context, AttributeSet attributeSet, int i2, int i3, Qg6 qg6) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    @DexIgnore
    private final View getContentView() {
        View childAt;
        String str;
        if (getChildAt(0) == this.f) {
            childAt = getChildAt(1);
            str = "getChildAt(1)";
        } else {
            childAt = getChildAt(0);
            str = "getChildAt(0)";
        }
        Wg6.b(childAt, str);
        return childAt;
    }

    @DexIgnore
    private final void setRefreshState(int i2) {
        this.d.b(i2, this.s, this.k);
        View view = this.f;
        if (view != null) {
            ((b) view).a(this.d, this.e);
            this.e.b(i2, this.s, this.k);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.CustomSwipeRefreshHeadLayout");
    }

    @DexIgnore
    private final void setRefreshing(boolean z2) {
        if (this.v != z2) {
            i();
            this.v = z2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G;
            local.d(str, "isRefreshing - mRefreshing: " + this.v);
            if (this.v) {
                this.D.run();
                return;
            }
            setRefreshState(3);
            removeCallbacks(this.B);
            removeCallbacks(this.E);
            this.D.run();
        }
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        Wg6.c(view, "child");
        Wg6.c(layoutParams, NativeProtocol.WEB_DIALOG_PARAMS);
        if (getChildCount() <= 1 || isInEditMode()) {
            super.addView(view, i2, layoutParams);
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one child content view");
    }

    @DexIgnore
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        Wg6.c(layoutParams, "p");
        return layoutParams instanceof ViewGroup.MarginLayoutParams;
    }

    @DexIgnore
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        boolean dispatchTouchEvent = super.dispatchTouchEvent(motionEvent);
        if (motionEvent.getAction() == 0) {
            return true;
        }
        return dispatchTouchEvent;
    }

    @DexIgnore
    public final void e(int i2, Animation.AnimationListener animationListener) {
        this.m = i2;
        this.F.reset();
        this.F.setDuration(500);
        this.F.setAnimationListener(animationListener);
        this.F.setInterpolator(this.b);
        View view = this.g;
        if (view != null) {
            view.startAnimation(this.F);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void f(Animation.AnimationListener animationListener) {
        this.c.reset();
        this.c.setDuration(50);
        this.c.setAnimationListener(animationListener);
        View view = this.g;
        if (view != null) {
            view.startAnimation(this.c);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final boolean g(View view, MotionEvent motionEvent) {
        if (!(view instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            Rect rect = new Rect();
            childAt.getHitRect(rect);
            if (rect.contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                Wg6.b(childAt, "child");
                return h(childAt, motionEvent);
            }
        }
        return false;
    }

    @DexIgnore
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -1);
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        Wg6.c(attributeSet, "attrs");
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    @DexIgnore
    @Override // android.view.ViewGroup
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        Wg6.c(layoutParams, "p");
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    @DexIgnore
    public final boolean getByPass() {
        return this.w;
    }

    @DexIgnore
    public final boolean getDisableSwipe() {
        return this.x;
    }

    @DexIgnore
    public final View getHeadView() {
        return this.f;
    }

    @DexIgnore
    public final d getLastState$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    public final int getMFrom$app_fossilRelease() {
        return this.m;
    }

    @DexIgnore
    public final boolean getMInReturningAnimation$app_fossilRelease() {
        return this.A;
    }

    @DexIgnore
    public final c getMListener$app_fossilRelease() {
        return this.y;
    }

    @DexIgnore
    public final Runnable getMReturnToStartPosition$app_fossilRelease() {
        return this.B;
    }

    @DexIgnore
    public final View getMTarget$app_fossilRelease() {
        return this.g;
    }

    @DexIgnore
    public final int getMTargetOriginalTop$app_fossilRelease() {
        return this.j;
    }

    @DexIgnore
    @SuppressLint({"ObsoleteSdkInt"})
    public final boolean h(View view, MotionEvent motionEvent) {
        motionEvent.offsetLocation((float) (view.getScrollX() - view.getLeft()), (float) (view.getScrollY() - view.getTop()));
        return view.canScrollVertically(-1) || g(view, motionEvent);
    }

    @DexIgnore
    public final void i() {
        if (this.g == null) {
            if (getChildCount() <= 2 || isInEditMode()) {
                View contentView = getContentView();
                this.g = contentView;
                if (contentView != null) {
                    this.j = contentView.getTop();
                    View view = this.g;
                    if (view != null) {
                        view.getHeight();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                throw new IllegalStateException("CustomSwipeRefreshLayout can host ONLY one direct child");
            }
        }
        if (this.k <= 0) {
            View view2 = this.f;
            this.k = view2 != null ? view2.getHeight() : SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS;
        }
    }

    @DexIgnore
    public final boolean j() {
        return this.v;
    }

    @DexIgnore
    public final void k() {
        setRefreshing(false);
    }

    @DexIgnore
    public final void l() {
        Context context = getContext();
        Wg6.b(context, "context");
        L67 l67 = new L67(context);
        this.f = l67;
        addView(l67, new ViewGroup.MarginLayoutParams(-1, -2));
    }

    @DexIgnore
    public final void m(int i2, boolean z2) {
        if (i2 != 0) {
            View headView = getHeadView();
            if (headView != null) {
                int i3 = this.s;
                if (i3 + i2 < 0) {
                    o(this.j, z2);
                } else if (i3 <= headView.getHeight() || i2 < 0) {
                    View view = this.g;
                    if (view != null) {
                        view.offsetTopAndBottom(i2);
                        this.s += i2;
                        invalidate();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            p(z2);
        }
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d(G, "startRefresh");
        setRefreshing(true);
        setRefreshState(2);
        c cVar = this.y;
        if (cVar == null) {
            return;
        }
        if (cVar != null) {
            cVar.a();
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2, boolean z2) {
        int i3;
        View view = this.g;
        if (view == null) {
            i3 = 0;
        } else if (view != null) {
            i3 = view.getTop();
        } else {
            Wg6.i();
            throw null;
        }
        int i4 = this.j;
        if (i2 < i4) {
            i2 = i4;
        }
        m(i2 - i3, z2);
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        removeCallbacks(this.E);
        removeCallbacks(this.B);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.B);
        removeCallbacks(this.E);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            r3 = 1
            r4 = 0
            r0 = 0
            java.lang.String r1 = "ev"
            com.mapped.Wg6.c(r6, r1)
            boolean r1 = r5.x
            if (r1 == 0) goto L_0x000d
        L_0x000c:
            return r0
        L_0x000d:
            r5.i()
            float r1 = r6.getY()
            boolean r2 = r5.isEnabled()
            if (r2 == 0) goto L_0x000c
            int r2 = r5.s
            if (r2 != 0) goto L_0x0020
            r5.A = r0
        L_0x0020:
            int r2 = r6.getAction()
            if (r2 == 0) goto L_0x006b
            if (r2 == r3) goto L_0x004d
            r3 = 2
            if (r2 == r3) goto L_0x004d
        L_0x002b:
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r6)
            boolean r2 = r5.A
            if (r2 != 0) goto L_0x0080
            android.view.View r2 = r5.g
            if (r2 == 0) goto L_0x007c
            java.lang.String r3 = "event"
            com.mapped.Wg6.b(r1, r3)
            boolean r1 = r5.h(r2, r1)
            if (r1 != 0) goto L_0x0080
            boolean r0 = r5.onTouchEvent(r6)
        L_0x0046:
            if (r0 != 0) goto L_0x000c
            boolean r0 = super.onInterceptTouchEvent(r6)
            goto L_0x000c
        L_0x004d:
            android.view.MotionEvent r2 = r5.h
            if (r2 == 0) goto L_0x002b
            if (r2 == 0) goto L_0x0067
            float r2 = r2.getY()
            float r2 = r1 - r2
            float r2 = java.lang.Math.abs(r2)
            int r3 = r5.i
            float r3 = (float) r3
            int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
            if (r2 >= 0) goto L_0x002b
            r5.l = r1
            goto L_0x000c
        L_0x0067:
            com.mapped.Wg6.i()
            throw r4
        L_0x006b:
            android.view.MotionEvent r1 = android.view.MotionEvent.obtain(r6)
            r5.h = r1
            if (r1 == 0) goto L_0x0087
            float r1 = r1.getY()
            r5.l = r1
            r5.u = r3
            goto L_0x002b
        L_0x007c:
            com.mapped.Wg6.i()
            throw r4
        L_0x0080:
            float r1 = r6.getY()
            r5.l = r1
            goto L_0x0046
        L_0x0087:
            com.mapped.Wg6.i()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        if (getChildCount() != 0) {
            View view = this.f;
            if (view != null) {
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    int paddingLeft = getPaddingLeft() + marginLayoutParams.leftMargin;
                    int paddingTop = marginLayoutParams.topMargin + getPaddingTop();
                    View view2 = this.f;
                    if (view2 != null) {
                        int measuredWidth = view2.getMeasuredWidth();
                        View view3 = this.f;
                        if (view3 != null) {
                            int measuredHeight = view3.getMeasuredHeight();
                            View view4 = this.f;
                            if (view4 != null) {
                                view4.layout(paddingLeft, paddingTop, measuredWidth + paddingLeft, measuredHeight + paddingTop);
                            }
                            View contentView = getContentView();
                            ViewGroup.LayoutParams layoutParams2 = contentView.getLayoutParams();
                            if (layoutParams2 != null) {
                                ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) layoutParams2;
                                int paddingLeft2 = getPaddingLeft() + marginLayoutParams2.leftMargin;
                                int paddingTop2 = marginLayoutParams2.topMargin + this.s + getPaddingTop();
                                contentView.layout(paddingLeft2, paddingTop2, contentView.getMeasuredWidth() + paddingLeft2, contentView.getMeasuredHeight() + paddingTop2);
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (getChildCount() <= 2 || isInEditMode()) {
            measureChildWithMargins(this.f, i2, 0, i3, 0);
            View contentView = getContentView();
            if (getChildCount() > 0) {
                ViewGroup.LayoutParams layoutParams = contentView.getLayoutParams();
                if (layoutParams != null) {
                    ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
                    contentView.measure(View.MeasureSpec.makeMeasureSpec((((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight()) - marginLayoutParams.leftMargin) - marginLayoutParams.rightMargin, 1073741824), View.MeasureSpec.makeMeasureSpec((((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom()) - marginLayoutParams.topMargin) - marginLayoutParams.bottomMargin, 1073741824));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup.MarginLayoutParams");
            }
            return;
        }
        throw new IllegalStateException("CustomSwipeRefreshLayout can host one child content view.");
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2;
        boolean z2 = false;
        Wg6.c(motionEvent, Constants.EVENT);
        if (!isEnabled() || this.x) {
            return false;
        }
        int action = motionEvent.getAction();
        View view = this.g;
        if (view != null) {
            int top = view.getTop();
            this.s = top - this.j;
            if (action != 1) {
                if (action != 2) {
                    if (action != 3 || (motionEvent2 = this.h) == null) {
                        return false;
                    }
                    if (motionEvent2 != null) {
                        motionEvent2.recycle();
                        this.h = null;
                        return false;
                    }
                    Wg6.i();
                    throw null;
                } else if (this.h == null || this.A) {
                    return false;
                } else {
                    float y2 = motionEvent.getY();
                    MotionEvent motionEvent3 = this.h;
                    if (motionEvent3 != null) {
                        float y3 = y2 - motionEvent3.getY();
                        boolean z3 = y2 - this.l > ((float) 0);
                        if (this.u) {
                            int i2 = this.i;
                            if (y3 > ((float) i2) || y3 < ((float) (-i2))) {
                                this.u = false;
                            }
                        }
                        if (z3 || top >= this.k || top >= this.j + 1) {
                            if (this.t) {
                                this.t = false;
                                c cVar = this.y;
                                if (cVar != null) {
                                    cVar.b();
                                }
                            }
                            int i3 = (int) ((y2 - this.l) * 0.3f);
                            if (top < this.j || j()) {
                                z2 = true;
                            }
                            m(i3, z2);
                            this.l = motionEvent.getY();
                        } else {
                            this.l = motionEvent.getY();
                            return false;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            } else if (this.v && this.e.a() == 2) {
                removeCallbacks(this.E);
                this.D.run();
                return false;
            } else if (this.s < this.k || this.w) {
                q();
            } else {
                n();
            }
            return true;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void p(boolean z2) {
        if (z2) {
            setRefreshState(this.d.a());
            return;
        }
        View view = this.g;
        if (view == null) {
            Wg6.i();
            throw null;
        } else if (view.getTop() > this.k) {
            setRefreshState(1);
        } else {
            setRefreshState(0);
        }
    }

    @DexIgnore
    public final void q() {
        removeCallbacks(this.E);
        postDelayed(this.E, 100);
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d(G, "updateRefreshingUI");
        setRefreshState(2);
        setRefreshing(true);
    }

    @DexIgnore
    public void requestDisallowInterceptTouchEvent(boolean z2) {
    }

    @DexIgnore
    public final void setByPass(boolean z2) {
        this.w = z2;
    }

    @DexIgnore
    public final void setDisableSwipe(boolean z2) {
        this.x = z2;
    }

    @DexIgnore
    public final void setLastState$app_fossilRelease(d dVar) {
        Wg6.c(dVar, "<set-?>");
        this.e = dVar;
    }

    @DexIgnore
    public final void setMFrom$app_fossilRelease(int i2) {
        this.m = i2;
    }

    @DexIgnore
    public final void setMInReturningAnimation$app_fossilRelease(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(c cVar) {
        this.y = cVar;
    }

    @DexIgnore
    public final void setMTarget$app_fossilRelease(View view) {
        this.g = view;
    }

    @DexIgnore
    public final void setMTargetOriginalTop$app_fossilRelease(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void setOnRefreshListener(c cVar) {
        Wg6.c(cVar, "listener");
        this.y = cVar;
    }

    @DexIgnore
    public final void setStartSwipe$app_fossilRelease(boolean z2) {
        this.t = z2;
    }
}
