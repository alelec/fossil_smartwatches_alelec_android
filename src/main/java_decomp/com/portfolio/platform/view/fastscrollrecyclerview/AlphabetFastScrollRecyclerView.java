package com.portfolio.platform.view.fastscrollrecyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Lz5;
import com.mapped.X24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AlphabetFastScrollRecyclerView extends RecyclerView {
    @DexIgnore
    public Lz5 b; // = null;
    @DexIgnore
    public GestureDetector c; // = null;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public int e; // = 12;
    @DexIgnore
    public float f; // = 20.0f;
    @DexIgnore
    public float g; // = 5.0f;
    @DexIgnore
    public int h; // = 5;
    @DexIgnore
    public int i; // = 5;
    @DexIgnore
    public float j; // = 0.6f;
    @DexIgnore
    public int k; // = -16777216;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public int m; // = -16777216;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return super.onFling(motionEvent, motionEvent2, f, f2);
        }
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context) {
        super(context);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes;
        if (!(attributeSet == null || (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.AlphabetFastScrollRecyclerView, 0, 0)) == null)) {
            try {
                this.e = obtainStyledAttributes.getInt(8, this.e);
                this.f = obtainStyledAttributes.getFloat(10, this.f);
                this.g = obtainStyledAttributes.getFloat(9, this.g);
                this.h = obtainStyledAttributes.getInt(11, this.h);
                this.i = obtainStyledAttributes.getInt(2, this.i);
                this.j = obtainStyledAttributes.getFloat(7, this.j);
                if (obtainStyledAttributes.hasValue(0)) {
                    this.k = Color.parseColor(obtainStyledAttributes.getString(0));
                }
                if (obtainStyledAttributes.hasValue(5)) {
                    this.l = Color.parseColor(obtainStyledAttributes.getString(5));
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.m = Color.parseColor(obtainStyledAttributes.getString(3));
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.k = obtainStyledAttributes.getColor(1, this.k);
                }
                if (obtainStyledAttributes.hasValue(6)) {
                    this.l = obtainStyledAttributes.getColor(6, this.l);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.m = obtainStyledAttributes.getColor(3, this.m);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.b = new Lz5(context, this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Lz5 lz5 = this.b;
        if (lz5 != null) {
            lz5.j(canvas);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Lz5 lz5;
        return (this.d && (lz5 = this.b) != null && lz5.h(motionEvent.getX(), motionEvent.getY())) || super.onInterceptTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Lz5 lz5 = this.b;
        if (lz5 != null) {
            lz5.m(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.d) {
            Lz5 lz5 = this.b;
            if (lz5 != null && lz5.n(motionEvent)) {
                return true;
            }
            if (this.c == null) {
                this.c = new GestureDetector(getContext(), new a());
            }
            this.c.onTouchEvent(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView
    public void setAdapter(RecyclerView.g gVar) {
        super.setAdapter(gVar);
        Lz5 lz5 = this.b;
        if (lz5 != null) {
            lz5.p(gVar);
        }
    }

    @DexIgnore
    public void setIndexBarColor(int i2) {
        this.b.q(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setIndexBarColor(String str) {
        this.b.q(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarCornerRadius(int i2) {
        this.b.r(i2);
    }

    @DexIgnore
    public void setIndexBarHighLateTextVisibility(boolean z) {
        this.b.t(z);
    }

    @DexIgnore
    public void setIndexBarTextColor(int i2) {
        this.b.u(i2);
    }

    @DexIgnore
    public void setIndexBarTextColor(String str) {
        this.b.u(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarTransparentValue(float f2) {
        this.b.v(f2);
    }

    @DexIgnore
    public void setIndexBarVisibility(boolean z) {
        this.b.w(z);
        this.d = z;
    }

    @DexIgnore
    public void setIndexTextSize(int i2) {
        this.b.x(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(int i2) {
        this.b.s(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(String str) {
        this.b.s(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexbarMargin(float f2) {
        this.b.y(f2);
    }

    @DexIgnore
    public void setIndexbarWidth(float f2) {
        this.b.z(f2);
    }

    @DexIgnore
    public void setPreviewPadding(int i2) {
        this.b.A(i2);
    }

    @DexIgnore
    public void setPreviewVisibility(boolean z) {
        this.b.B(z);
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        this.b.C(typeface);
    }
}
