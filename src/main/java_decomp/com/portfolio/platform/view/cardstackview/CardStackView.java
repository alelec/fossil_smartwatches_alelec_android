package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.P47;
import com.fossil.P57;
import com.fossil.R57;
import com.mapped.Cz5;
import com.mapped.Fz5;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.cardstackview.CardContainerView;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CardStackView extends FrameLayout {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ Cz5 b;
    @DexIgnore
    public /* final */ P57 c;
    @DexIgnore
    public BaseAdapter d;
    @DexIgnore
    public /* final */ LinkedList<CardContainerView> e;
    @DexIgnore
    public a f;
    @DexIgnore
    public /* final */ c g;
    @DexIgnore
    public /* final */ b h;

    @DexIgnore
    public interface a {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(int i);

        @DexIgnore
        void c(float f, float f2);

        @DexIgnore
        void d(Fz5 fz5);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CardContainerView.c {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void a() {
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.b(this.a.getState$app_fossilRelease().a);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void b(Point point, Fz5 fz5) {
            Wg6.c(point, "point");
            Wg6.c(fz5, "direction");
            this.a.m(point, fz5);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void c() {
            this.a.e();
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a();
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.cardstackview.CardContainerView.c
        public void d(float f, float f2) {
            this.a.n(f, f2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends DataSetObserver {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void onChanged() {
            boolean z = false;
            if (this.a.getAdapter$app_fossilRelease() != null) {
                if (this.a.getState$app_fossilRelease().d) {
                    this.a.getState$app_fossilRelease().d = false;
                } else {
                    int i = this.a.getState$app_fossilRelease().c;
                    BaseAdapter adapter$app_fossilRelease = this.a.getAdapter$app_fossilRelease();
                    if (adapter$app_fossilRelease != null) {
                        if (i == adapter$app_fossilRelease.getCount()) {
                            z = true;
                        }
                        z = !z;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                this.a.d(z);
                P57 state$app_fossilRelease = this.a.getState$app_fossilRelease();
                BaseAdapter adapter$app_fossilRelease2 = this.a.getAdapter$app_fossilRelease();
                if (adapter$app_fossilRelease2 != null) {
                    state$app_fossilRelease.c = adapter$app_fossilRelease2.getCount();
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;
        @DexIgnore
        public /* final */ /* synthetic */ Point b;
        @DexIgnore
        public /* final */ /* synthetic */ Fz5 c;

        @DexIgnore
        public d(CardStackView cardStackView, Point point, Fz5 fz5) {
            this.a = cardStackView;
            this.b = point;
            this.c = fz5;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animator");
            this.a.b(this.b, this.c);
        }
    }

    /*
    static {
        String simpleName = CardStackView.class.getSimpleName();
        Wg6.b(simpleName, "CardStackView::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context) {
        this(context, null);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        Wg6.c(context, "context");
        this.b = new Cz5();
        this.c = new P57();
        this.e = new LinkedList<>();
        this.g = new c(this);
        this.h = new b(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.CardStackView);
        setVisibleCount(obtainStyledAttributes.getInt(11, this.b.a));
        setSwipeThreshold(obtainStyledAttributes.getFloat(8, this.b.b));
        setTranslationDiff(obtainStyledAttributes.getFloat(10, this.b.c));
        setScaleDiff(obtainStyledAttributes.getFloat(4, this.b.d));
        setStackFrom(R57.values()[obtainStyledAttributes.getInt(5, this.b.e.ordinal())]);
        setElevationEnabled(obtainStyledAttributes.getBoolean(1, this.b.f));
        setSwipeEnabled(obtainStyledAttributes.getBoolean(7, this.b.g));
        List<Fz5> from = Fz5.from(obtainStyledAttributes.getInt(6, 0));
        Wg6.b(from, "SwipeDirection.from(arra\u2026kView_swipeDirection, 0))");
        setSwipeDirection(from);
        setLeftOverlay(obtainStyledAttributes.getResourceId(2, 0));
        setRightOverlay(obtainStyledAttributes.getResourceId(3, 0));
        setBottomOverlay(obtainStyledAttributes.getResourceId(0, 0));
        setTopOverlay(obtainStyledAttributes.getResourceId(9, 0));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final CardContainerView getBottomView() {
        CardContainerView last = this.e.getLast();
        Wg6.b(last, "containers.last");
        return last;
    }

    @DexIgnore
    private final CardContainerView getTopView() {
        CardContainerView first = this.e.getFirst();
        Wg6.b(first, "containers.first");
        return first;
    }

    @DexIgnore
    private final void setBottomOverlay(int i2) {
        this.b.j = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setElevationEnabled(boolean z) {
        this.b.f = z;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setLeftOverlay(int i2) {
        this.b.h = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setRightOverlay(int i2) {
        this.b.i = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setScaleDiff(float f2) {
        this.b.d = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setStackFrom(R57 r57) {
        this.b.e = r57;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.mapped.Fz5> */
    /* JADX WARN: Multi-variable type inference failed */
    private final void setSwipeDirection(List<? extends Fz5> list) {
        this.b.l = list;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setSwipeEnabled(boolean z) {
        this.b.g = z;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setSwipeThreshold(float f2) {
        this.b.b = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setTopOverlay(int i2) {
        this.b.k = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setTranslationDiff(float f2) {
        this.b.c = f2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    private final void setVisibleCount(int i2) {
        this.b.a = i2;
        if (this.d != null) {
            d(false);
        }
    }

    @DexIgnore
    public final void a() {
        int i2 = this.b.a;
        for (int i3 = 0; i3 < i2; i3++) {
            CardContainerView cardContainerView = this.e.get(i3);
            Wg6.b(cardContainerView, "containers[i]");
            CardContainerView cardContainerView2 = cardContainerView;
            cardContainerView2.e();
            cardContainerView2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            cardContainerView2.setScaleX(1.0f);
            cardContainerView2.setScaleY(1.0f);
            cardContainerView2.setRotation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final void b(Point point, Fz5 fz5) {
        Wg6.c(point, "point");
        Wg6.c(fz5, "direction");
        k();
        this.c.b = point;
        e();
        this.c.a++;
        a aVar = this.f;
        if (aVar != null) {
            if (aVar != null) {
                aVar.d(fz5);
            } else {
                Wg6.i();
                throw null;
            }
        }
        h();
        this.e.getLast().setContainerEventListener(null);
        this.e.getFirst().setContainerEventListener(this.h);
    }

    @DexIgnore
    public final void c() {
        this.e.getFirst().setContainerEventListener(null);
        this.e.getFirst().setDraggable(false);
        if (this.e.size() > 1) {
            this.e.get(1).setContainerEventListener(this.h);
            this.e.get(1).setDraggable(true);
        }
    }

    @DexIgnore
    public final void d(boolean z) {
        l(z);
        g();
        e();
        f();
    }

    @DexIgnore
    public final void e() {
        a();
        n(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void f() {
        if (this.d != null) {
            int i2 = this.b.a;
            for (int i3 = 0; i3 < i2; i3++) {
                CardContainerView cardContainerView = this.e.get(i3);
                Wg6.b(cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                int i4 = this.c.a + i3;
                BaseAdapter baseAdapter = this.d;
                if (baseAdapter != null) {
                    if (i4 < baseAdapter.getCount()) {
                        ViewGroup contentContainer = cardContainerView2.getContentContainer();
                        BaseAdapter baseAdapter2 = this.d;
                        if (baseAdapter2 != null) {
                            View view = baseAdapter2.getView(i4, contentContainer.getChildAt(0), contentContainer);
                            Wg6.b(contentContainer, "parent");
                            if (contentContainer.getChildCount() == 0) {
                                contentContainer.addView(view);
                            }
                            cardContainerView2.setVisibility(0);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        cardContainerView2.setVisibility(8);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            BaseAdapter baseAdapter3 = this.d;
            if (baseAdapter3 == null) {
                Wg6.i();
                throw null;
            } else if (!baseAdapter3.isEmpty()) {
                getTopView().setDraggable(true);
            }
        }
    }

    @DexIgnore
    public final void g() {
        removeAllViews();
        this.e.clear();
        int i2 = this.b.a;
        for (int i3 = 0; i3 < i2; i3++) {
            View inflate = LayoutInflater.from(getContext()).inflate(2131558447, (ViewGroup) this, false);
            if (inflate != null) {
                CardContainerView cardContainerView = (CardContainerView) inflate;
                cardContainerView.setDraggable(false);
                cardContainerView.setCardStackOption(this.b);
                Cz5 cz5 = this.b;
                cardContainerView.f(cz5.h, cz5.i, cz5.j, cz5.k);
                this.e.add(0, cardContainerView);
                addView(cardContainerView);
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.cardstackview.CardContainerView");
            }
        }
        this.e.getFirst().setContainerEventListener(this.h);
        this.c.e = true;
    }

    @DexIgnore
    public final BaseAdapter getAdapter$app_fossilRelease() {
        return this.d;
    }

    @DexIgnore
    public final a getCardEventListener$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final P57 getState$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final void h() {
        boolean z = false;
        int i2 = (this.c.a + this.b.a) - 1;
        BaseAdapter baseAdapter = this.d;
        if (baseAdapter == null) {
            return;
        }
        if (baseAdapter != null) {
            if (i2 < baseAdapter.getCount()) {
                CardContainerView bottomView = getBottomView();
                bottomView.setDraggable(false);
                ViewGroup contentContainer = bottomView.getContentContainer();
                BaseAdapter baseAdapter2 = this.d;
                if (baseAdapter2 != null) {
                    View view = baseAdapter2.getView(i2, contentContainer.getChildAt(0), contentContainer);
                    Wg6.b(contentContainer, "parent");
                    if (contentContainer.getChildCount() == 0) {
                        contentContainer.addView(view);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                CardContainerView bottomView2 = getBottomView();
                bottomView2.setDraggable(false);
                bottomView2.setVisibility(8);
            }
            int i3 = this.c.a;
            BaseAdapter baseAdapter3 = this.d;
            if (baseAdapter3 != null) {
                if (i3 < baseAdapter3.getCount()) {
                    z = true;
                }
                if (z) {
                    getTopView().setDraggable(true);
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void i(CardContainerView cardContainerView) {
        CardStackView cardStackView = (CardStackView) cardContainerView.getParent();
        if (cardStackView != null) {
            cardStackView.removeView(cardContainerView);
            cardStackView.addView(cardContainerView, 0);
        }
    }

    @DexIgnore
    public final void j(Point point, Animator.AnimatorListener animatorListener) {
        getTopView().animate().translationX((float) point.x).translationY(-((float) point.y)).setDuration(400).setListener(animatorListener).start();
    }

    @DexIgnore
    public final void k() {
        i(getTopView());
        LinkedList<CardContainerView> linkedList = this.e;
        linkedList.addLast(linkedList.removeFirst());
    }

    @DexIgnore
    public final void l(boolean z) {
        if (z) {
            this.c.a();
        }
    }

    @DexIgnore
    public final void m(Point point, Fz5 fz5) {
        Wg6.c(point, "point");
        Wg6.c(fz5, "direction");
        c();
        j(point, new d(this, point, fz5));
    }

    @DexIgnore
    public final void n(float f2, float f3) {
        a aVar = this.f;
        if (aVar != null) {
            if (aVar != null) {
                aVar.c(f2, f3);
            } else {
                Wg6.i();
                throw null;
            }
        }
        Cz5 cz5 = this.b;
        if (cz5.f) {
            int i2 = cz5.a;
            for (int i3 = 1; i3 < i2; i3++) {
                CardContainerView cardContainerView = this.e.get(i3);
                Wg6.b(cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                float f4 = (float) i3;
                float f5 = this.b.d;
                float f6 = 1.0f - (f4 * f5);
                float f7 = (float) (i3 - 1);
                float abs = (((1.0f - (f5 * f7)) - f6) * Math.abs(f2)) + f6;
                cardContainerView2.setScaleX(abs);
                cardContainerView2.setScaleY(abs);
                float b2 = f4 * P47.b(this.b.c);
                if (this.b.e == R57.Top) {
                    b2 *= -1.0f;
                }
                float b3 = P47.b(this.b.c) * f7;
                if (this.b.e == R57.Top) {
                    b3 *= -1.0f;
                }
                cardContainerView2.setTranslationY(b2 - ((b2 - b3) * Math.abs(f2)));
            }
        }
    }

    @DexIgnore
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        if (this.c.e && i2 == 0) {
            e();
        }
    }

    @DexIgnore
    public final void setAdapter(BaseAdapter baseAdapter) {
        Wg6.c(baseAdapter, "adapter");
        BaseAdapter baseAdapter2 = this.d;
        if (baseAdapter2 != null) {
            try {
                baseAdapter2.unregisterDataSetObserver(this.g);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = i;
                local.d(str, "Exception when unregisterDataSetObserver e=" + e2);
            }
        }
        this.d = baseAdapter;
        if (baseAdapter != null) {
            baseAdapter.registerDataSetObserver(this.g);
            this.c.c = baseAdapter.getCount();
            d(true);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void setAdapter$app_fossilRelease(BaseAdapter baseAdapter) {
        this.d = baseAdapter;
    }

    @DexIgnore
    public final void setCardEventListener$app_fossilRelease(a aVar) {
        this.f = aVar;
    }
}
