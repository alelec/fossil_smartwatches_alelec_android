package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.Dd1;
import com.fossil.Ej1;
import com.fossil.Fj1;
import com.fossil.Gb1;
import com.fossil.Hk5;
import com.fossil.I37;
import com.fossil.Qj1;
import com.fossil.Sj5;
import com.fossil.Tj5;
import com.fossil.Wa1;
import com.fossil.Wj5;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.ref.WeakReference;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilCircleImageView extends AppCompatImageView {
    @DexIgnore
    public static /* final */ String x; // = FossilCircleImageView.class.getSimpleName();
    @DexIgnore
    public static /* final */ ImageView.ScaleType y; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public ColorFilter s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Ej1<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ Wj5 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void run() {
                a.this.b.I(new Sj5("", a.this.c)).T0(new Fj1().o0(new Hk5())).F0(FossilCircleImageView.this);
            }
        }

        @DexIgnore
        public a(Wj5 wj5, String str) {
            this.b = wj5;
            this.c = str;
        }

        @DexIgnore
        public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Ej1
        public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
            new Handler(Looper.getMainLooper()).post(new a());
            return true;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
        @Override // com.fossil.Ej1
        public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
            return a(drawable, obj, qj1, gb1, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Bitmap, Void, Bitmap> {
        @DexIgnore
        public WeakReference<FossilCircleImageView> a;

        @DexIgnore
        public b(WeakReference<FossilCircleImageView> weakReference) {
            this.a = weakReference;
        }

        @DexIgnore
        public Bitmap a(Bitmap... bitmapArr) {
            int length = bitmapArr.length;
            if (length == 1) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.x, "doInBackground bitmap list = 0");
                return bitmapArr[0];
            } else if (length == 2) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.x, "doInBackground bitmap list = 2");
                return I37.c(bitmapArr[0], bitmapArr[1], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length == 3) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.x, "doInBackground bitmap list = 3");
                return I37.b(bitmapArr[0], bitmapArr[1], bitmapArr[2], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            } else if (length != 4) {
                return null;
            } else {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.x, "doInBackground bitmap list = 4");
                return I37.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], bitmapArr[3], ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            }
        }

        @DexIgnore
        public void b(Bitmap bitmap) {
            FLogger.INSTANCE.getLocal().d(FossilCircleImageView.x, "onPostExecute");
            if (this.a.get() != null) {
                this.a.get().setImageBitmap(bitmap);
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Bitmap doInBackground(Bitmap[] bitmapArr) {
            return a(bitmapArr);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ void onPostExecute(Bitmap bitmap) {
            b(bitmap);
        }

        @DexIgnore
        public void onPreExecute() {
            super.onPreExecute();
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context) {
        super(context);
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Paint();
        this.g = new Paint();
        this.h = -16777216;
        this.i = 0;
        this.j = 0;
        e();
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Paint();
        this.g = new Paint();
        this.h = -16777216;
        this.i = 0;
        this.j = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.FossilCircleImageView, i2, 0);
        this.i = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.h = obtainStyledAttributes.getColor(0, -16777216);
        this.v = obtainStyledAttributes.getBoolean(1, false);
        this.j = obtainStyledAttributes.getColor(3, 0);
        String string = obtainStyledAttributes.getString(4);
        this.k = string;
        if (string == null) {
            this.k = "";
        }
        obtainStyledAttributes.recycle();
        FLogger.INSTANCE.getLocal().d(x, "FossilCircleImageView constructor");
        e();
    }

    @DexIgnore
    public final void b() {
    }

    @DexIgnore
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = (((float) (width - min)) / 2.0f) + ((float) getPaddingLeft());
        float paddingTop = (((float) (height - min)) / 2.0f) + ((float) getPaddingTop());
        float f2 = (float) min;
        return new RectF(paddingLeft, paddingTop, paddingLeft + f2, f2 + paddingTop);
    }

    @DexIgnore
    public final void e() {
        super.setScaleType(y);
        this.t = true;
        if (this.u) {
            m();
            this.u = false;
        }
        FLogger.INSTANCE.getLocal().d(x, "init()");
    }

    @DexIgnore
    public void f(Bitmap bitmap, Bitmap bitmap2) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2);
    }

    @DexIgnore
    public void g(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2, bitmap3);
    }

    @DexIgnore
    public int getBorderColor() {
        return this.h;
    }

    @DexIgnore
    public int getBorderWidth() {
        return this.i;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.s;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.j;
    }

    @DexIgnore
    public String getHandNumber() {
        return this.k;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return y;
    }

    @DexIgnore
    public void h(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        new b(new WeakReference(this)).execute(bitmap, bitmap2, bitmap3, bitmap4);
    }

    @DexIgnore
    public void i(int i2, Wa1 wa1) {
        wa1.r(Integer.valueOf(i2)).u0(new Fj1().o0(new Hk5())).F0(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setImageResource resId = " + i2);
    }

    @DexIgnore
    public void j(Wj5 wj5, String str, String str2) {
        wj5.J(str).t1(3000).T0(new Fj1().o0(new Hk5())).b1(new a(wj5, str2)).F0(this);
    }

    @DexIgnore
    public void k(String str, Wa1 wa1) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "setImageUrl without defaultName url=" + str);
        wa1.t(str).u0(new Fj1().o0(new Hk5())).F0(this);
    }

    @DexIgnore
    public void l(String str, String str2, Wa1 wa1) {
        Tj5.b(this).I(new Sj5(str, str2)).T0(new Fj1().o0(new Hk5())).F0(this);
    }

    @DexIgnore
    public final void m() {
        int i2;
        if (!this.t) {
            this.u = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            this.f.setStyle(Paint.Style.STROKE);
            this.f.setAntiAlias(true);
            this.f.setColor(this.h);
            this.f.setStrokeWidth((float) this.i);
            this.g.setStyle(Paint.Style.FILL);
            this.g.setAntiAlias(true);
            this.g.setColor(this.j);
            this.g.setFilterBitmap(true);
            this.g.setDither(true);
            this.e.set(d());
            this.m = Math.min((this.e.height() - ((float) this.i)) / 2.0f, (this.e.width() - ((float) this.i)) / 2.0f);
            this.d.set(this.e);
            if (!this.v && (i2 = this.i) > 0) {
                this.d.inset(((float) i2) - 1.0f, ((float) i2) - 1.0f);
            }
            this.l = Math.min(this.d.height() / 2.0f, this.d.width() / 2.0f);
            b();
            n();
            invalidate();
        }
    }

    @DexIgnore
    public final void n() {
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.j != 0) {
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.l, this.g);
        }
        if (this.i > 0) {
            canvas.drawCircle(this.e.centerX(), this.e.centerY(), this.m, this.f);
        }
        if (this.w) {
            this.f.setAntiAlias(true);
            this.f.setFilterBitmap(true);
            this.f.setDither(true);
            this.f.setColor(Color.parseColor("#EEEEEE"));
            canvas.drawCircle(this.e.centerX(), this.e.centerY(), this.m, this.f);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        m();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        if (i2 != this.h) {
            this.h = i2;
            this.f.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.i) {
            this.i = i2;
            m();
        }
    }

    @DexIgnore
    @Override // android.widget.ImageView
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.s)) {
            this.s = colorFilter;
            b();
            invalidate();
        }
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.w = z;
    }

    @DexIgnore
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.j) {
            this.j = i2;
            this.g.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.k = str;
    }

    @DexIgnore
    public void setPadding(int i2, int i3, int i4, int i5) {
        super.setPadding(i2, i3, i4, i5);
        m();
    }

    @DexIgnore
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        super.setPaddingRelative(i2, i3, i4, i5);
        m();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != y) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }
}
