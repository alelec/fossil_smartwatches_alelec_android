package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.C67;
import com.fossil.K37;
import com.fossil.Lq7;
import com.fossil.Nl0;
import com.fossil.Qq7;
import com.fossil.Rh5;
import com.fossil.Wt7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Jh6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepWeekDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String J;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public /* final */ int C;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public Rh5 H;
    @DexIgnore
    public /* final */ String[] I;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ Paint m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Qq7 implements Coroutine<Integer, List<? extends Integer>, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 $bottomTextHeight;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $charactersCenterXList;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Hh6 hh6, Jh6 jh6) {
            super(2);
            this.$bottomTextHeight = hh6;
            this.$charactersCenterXList = jh6;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Cd6 invoke(Integer num, List<? extends Integer> list) {
            invoke(num.intValue(), (List<Integer>) list);
            return Cd6.a;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<java.lang.Integer> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void invoke(int i, List<Integer> list) {
            Wg6.c(list, "list");
            this.$bottomTextHeight.element = i;
            this.$charactersCenterXList.element = list;
        }
    }

    /*
    static {
        String simpleName = SleepWeekDetailsChart.class.getSimpleName();
        Wg6.b(simpleName, "SleepWeekDetailsChart::class.java.simpleName");
        J = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context) {
        this(context, null, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepWeekDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.m = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = context.getResources().getDimensionPixelSize(2131165385);
        this.y = context.getResources().getDimensionPixelSize(2131165385);
        this.z = context.getResources().getDimensionPixelSize(2131165372);
        this.A = context.getResources().getDimensionPixelSize(2131165372);
        this.B = context.getResources().getDimensionPixelSize(2131165420);
        this.C = context.getResources().getDimensionPixelSize(2131165407);
        this.D = new ArrayList<>();
        this.H = Rh5.TOTAL_SLEEP;
        String[] stringArray = context.getResources().getStringArray(2130903040);
        Wg6.b(stringArray, "context.resources.getStr\u2026ay.days_of_week_alphabet)");
        this.I = stringArray;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, X24.SleepWeekDetailsChart, 0, 0));
        }
        int d = W6.d(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(3, d) : d;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(1, d) : d;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(0, d) : d;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(2, d) : d;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(4, d) : d;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getColor(5, d) : d;
        TypedArray mTypedArray7 = getMTypedArray();
        this.k = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.l = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296261) : 2131296261;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = C67.a[this.H.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = J;
            local.d(str, "Max of awake: " + this.G);
            i2 = this.G;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = J;
            local2.d(str2, "Max of light: " + this.F);
            i2 = this.F;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = J;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = J;
            local4.d(str4, "Max of restful: " + this.E);
            i2 = this.E;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.D.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                int sleepGoal = next.getSleepGoal();
                while (true) {
                    next = it.next();
                    sleepGoal = next.getSleepGoal();
                    if (sleepGoal >= sleepGoal) {
                        sleepGoal = sleepGoal;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.D.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = next.getTotalSleepMinutes();
                while (true) {
                    next = it.next();
                    totalSleepMinutes = next.getTotalSleepMinutes();
                    if (totalSleepMinutes >= totalSleepMinutes) {
                        totalSleepMinutes = totalSleepMinutes;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        if (t3 != null) {
            return t3.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = C67.b[this.H.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2, int i3, int i4, int i5, SleepDayData sleepDayData) {
        int i6 = i2 - (this.A / 2);
        int i7 = i6 >= 0 ? i6 : 0;
        int i8 = this.A;
        int size = (i4 - i5) - ((sleepDayData.getSessionList().size() + -1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0) * this.C);
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i9 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            FLogger.INSTANCE.getLocal().d(J, "session duration: " + component1);
            i9 = component1 + i9;
        }
        int size2 = sleepDayData.getSessionList().size();
        for (int i10 = 0; i10 < size2; i10++) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i10);
            Wg6.b(sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i11 = (int) ((f2 / ((float) i9)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i12 = 0;
            while (i12 < size3) {
                int i13 = sleepStates.get(i12).state;
                int i14 = (int) ((((float) (i12 < size3 + -1 ? ((int) sleepStates.get(i12 + 1).index) - ((int) sleepStates.get(i12).index) : durationInMinutes - ((int) sleepStates.get(i12).index))) / f2) * ((float) i11));
                if (i14 < 1) {
                    i14 = 1;
                }
                int i15 = i3 - i14;
                canvas.drawRect(new RectF((float) i7, (float) i15, (float) (i8 + i7), (float) i3), i13 != 1 ? i13 != 2 ? this.u : this.w : this.v);
                i12++;
                i3 = i15;
            }
            i3 -= this.C;
        }
    }

    @DexIgnore
    public final void d(Canvas canvas, List<Integer> list, int i2, int i3) {
        if (!this.D.isEmpty()) {
            float mChartMax = (float) getMChartMax();
            int size = list.size();
            for (int i4 = 0; i4 < size; i4++) {
                SleepDayData sleepDayData = this.D.get(i4);
                FLogger.INSTANCE.getLocal().d(J, "Actual sleep: " + sleepDayData.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int intValue = list.get(i4).intValue();
                int totalSleepMinutes = sleepDayData.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int dayAwake = sleepDayData.getDayAwake();
                    g(canvas, this.u, intValue, i2, (int) ((((float) dayAwake) / mChartMax) * ((float) i2)), i3, dayAwake, totalSleepMinutes);
                } else if (mSleepMode == 1) {
                    int dayLight = sleepDayData.getDayLight();
                    g(canvas, this.v, intValue, i2, (int) ((((float) dayLight) / mChartMax) * ((float) i2)), i3, dayLight, totalSleepMinutes);
                } else if (mSleepMode != 2) {
                    SleepDayData sleepDayData2 = this.D.get(i4);
                    Wg6.b(sleepDayData2, "mSleepDayDataList[i]");
                    c(canvas, list.get(i4).intValue(), i2, (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i2)), i3, sleepDayData2);
                } else {
                    int dayRestful = sleepDayData.getDayRestful();
                    g(canvas, this.w, intValue, i2, (int) ((((float) dayRestful) / mChartMax) * ((float) i2)), i3, dayRestful, totalSleepMinutes);
                }
            }
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int width = (getWidth() - getStartBitmap().getWidth()) - (this.B * 2);
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            Jh6 jh6 = new Jh6();
            jh6.element = null;
            f(canvas, width, new a(hh6, jh6));
            int height = (getHeight() - hh6.element) - this.z;
            T t2 = jh6.element;
            if (t2 != null) {
                d(canvas, t2, height, this.B * 2);
                h(canvas, t2, width, height, this.B * 2, height);
            }
            e(canvas, 0, height + 4);
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.m);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.m);
    }

    @DexIgnore
    public final void f(Canvas canvas, int i2, Coroutine<? super Integer, ? super List<Integer>, Cd6> coroutine) {
        int length = this.I.length;
        Rect rect = new Rect();
        String str = this.I[0];
        Lq7 lq7 = Lq7.a;
        Paint paint = this.t;
        Wg6.b(str, "sundayCharacter");
        paint.getTextBounds(str, 0, Wt7.A(str) > 0 ? Wt7.A(str) : 1, rect);
        float measureText = this.t.measureText(str);
        float height = (float) rect.height();
        float f2 = (((float) (i2 - (this.y * 2))) - (((float) length) * measureText)) / ((float) (length - 1));
        float height2 = (float) getHeight();
        float f3 = (float) 2;
        float f4 = height / f3;
        float f5 = (float) this.y;
        LinkedList linkedList = new LinkedList();
        for (String str2 : this.I) {
            canvas.drawText(str2, f5, height2 - f4, this.t);
            linkedList.add(Integer.valueOf((int) ((measureText / f3) + f5)));
            f5 += measureText + f2;
        }
        coroutine.invoke(Integer.valueOf((int) height), linkedList);
    }

    @DexIgnore
    public final void g(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i2 - (this.A / 2);
        if (i8 < 0) {
            i8 = 0;
        }
        RectF rectF = new RectF((float) i8, (float) ((i3 - ((int) ((((float) i6) / ((float) i7)) * ((float) i4)))) + i5), (float) (i8 + this.A), (float) i3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = J;
        local.d(str, "duration: " + i6 + ", totalMinutes: " + i7 + ", rect top: " + rectF.top + ", rect bot: " + rectF.bottom);
        K37.d(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.x;
    }

    @DexIgnore
    public final void h(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        if ((!this.D.isEmpty()) && this.D.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            float height = (float) getStartBitmap().getHeight();
            int size = list.size();
            for (int i6 = 1; i6 < size; i6++) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = J;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.D.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.D.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                height = Math.min(((1.0f - (((float) this.D.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list.get(i7).intValue();
                float min = Math.min(((1.0f - (((float) this.D.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (height == min) {
                    path.moveTo(intValue2, min);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (this.B + i2), height);
                    } else {
                        path.lineTo(intValue, height);
                    }
                    canvas.drawPath(path, this.s);
                } else {
                    path.moveTo(intValue2, min);
                    path.lineTo(intValue, min);
                    canvas.drawPath(path, this.s);
                    path.moveTo(intValue, min);
                    path.lineTo(intValue, height);
                    canvas.drawPath(path, this.s);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, height);
                        path.lineTo((float) (this.B + i2), height);
                        canvas.drawPath(path, this.s);
                    }
                }
            }
            canvas.drawBitmap(getStartBitmap(), (float) (this.B + i2), height - ((float) (getStartBitmap().getHeight() / 2)), this.t);
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.m.setColor(this.e);
        float f2 = (float) 4;
        this.m.setStrokeWidth(f2);
        this.t.setColor(this.j);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setTextSize((float) this.k);
        this.t.setTypeface(Nl0.b(getContext(), this.l));
        this.s.setColor(this.f);
        this.s.setStyle(Paint.Style.STROKE);
        this.s.setStrokeWidth(f2);
        this.s.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.u.setColor(this.g);
        this.u.setStrokeWidth((float) this.A);
        this.u.setStyle(Paint.Style.FILL);
        this.v.setColor(this.h);
        this.v.setStrokeWidth((float) this.A);
        this.v.setStyle(Paint.Style.FILL);
        this.w.setColor(this.i);
        this.w.setStrokeWidth((float) this.A);
        this.w.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
