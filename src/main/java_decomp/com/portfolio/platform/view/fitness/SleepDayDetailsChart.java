package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.K37;
import com.fossil.Nl0;
import com.fossil.Wt7;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDayDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public /* final */ int A;
    @DexIgnore
    public /* final */ int B;
    @DexIgnore
    public /* final */ int C;
    @DexIgnore
    public /* final */ ArrayList<SleepSessionData> D;
    @DexIgnore
    public a E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ Paint v;
    @DexIgnore
    public /* final */ Paint w;
    @DexIgnore
    public /* final */ Paint x;
    @DexIgnore
    public /* final */ Paint y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public enum a {
        All,
        Awake,
        Light,
        Restful
    }

    /*
    static {
        String simpleName = SleepDayDetailsChart.class.getSimpleName();
        Wg6.b(simpleName, "SleepDayDetailsChart::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context) {
        this(context, null, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepDayDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.k = W6.d(context, 2131100337);
        this.l = context.getResources().getDimensionPixelSize(2131165720);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = new Paint(1);
        this.w = new Paint(1);
        this.x = new Paint(1);
        this.y = new Paint(1);
        this.z = context.getResources().getDimensionPixelSize(2131165407);
        this.A = context.getResources().getDimensionPixelSize(2131165420);
        this.B = context.getResources().getDimensionPixelSize(2131165372);
        this.C = context.getResources().getDimensionPixelSize(2131165385);
        this.D = new ArrayList<>();
        this.E = a.All;
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, X24.SleepDayDetailsChart, 0, 0));
        }
        int d = W6.d(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(3, d) : d;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(4, d) : d;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(2, d) : d;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(0, d) : d;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(5, d) : d;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getColor(1, d) : d;
        TypedArray mTypedArray7 = getMTypedArray();
        this.m = mTypedArray7 != null ? mTypedArray7.getResourceId(6, 2131296258) : 2131296258;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    public final void c(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 2), this.y);
        canvas.drawRect(new Rect(0, i3 - 2, getWidth(), i3), this.y);
    }

    @DexIgnore
    public final void d(Canvas canvas, int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = G;
        local.d(str, "positionX: " + i2 + ", positionY: " + i3);
        canvas.drawBitmap(getStartBitmap(), (float) (i2 - (getStartBitmap().getWidth() / 2)), (float) this.A, this.v);
        Path path = new Path();
        float f2 = (float) i2;
        path.moveTo(f2, (float) (this.A + getStartBitmap().getHeight()));
        path.lineTo(f2, (float) i3);
        canvas.drawPath(path, this.x);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        int i2;
        RectF rectF;
        RectF rectF2;
        boolean z2;
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            int i3 = Integer.MAX_VALUE;
            Iterator<SleepSessionData> it = this.D.iterator();
            int i4 = 0;
            int i5 = 0;
            while (it.hasNext()) {
                SleepSessionData next = it.next();
                int component1 = next.component1();
                String component2 = next.component2();
                String component3 = next.component3();
                i5 += component1;
                Rect rect = new Rect();
                Rect rect2 = new Rect();
                this.w.getTextBounds(component2, 0, Wt7.A(component2), rect);
                this.w.getTextBounds(component3, 0, Wt7.A(component3), rect2);
                i3 = Math.min(rect2.height(), Math.min(rect.height(), i3));
                i4++;
            }
            int width = getWidth() - (((i4 > 0 ? i4 - 1 : 0) + 2) * this.A);
            int height = (getHeight() - i3) - (this.A * 2);
            if (height < 0) {
                height = getHeight() - (this.A * 2);
            }
            int i6 = this.A;
            int size = this.D.size();
            boolean z3 = false;
            int i7 = height;
            int i8 = i6;
            int i9 = 0;
            Canvas canvas2 = canvas;
            SleepDayDetailsChart sleepDayDetailsChart = this;
            for (int i10 = 0; i10 < size; i10++) {
                SleepSessionData sleepSessionData = sleepDayDetailsChart.D.get(i10);
                Wg6.b(sleepSessionData, "mListSleepSession[sleepDataId]");
                SleepSessionData sleepSessionData2 = sleepSessionData;
                float measureText = sleepDayDetailsChart.w.measureText(sleepSessionData2.getEndTimeString());
                int i11 = i3 / 2;
                canvas2.drawText(sleepSessionData2.getStartTimeString(), (float) i8, (float) (getHeight() - i11), sleepDayDetailsChart.w);
                int durationInMinutes = sleepSessionData2.getDurationInMinutes();
                float f2 = (float) durationInMinutes;
                float f3 = f2 / ((float) i5);
                float f4 = ((float) width) * f3;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = G;
                StringBuilder sb = new StringBuilder();
                sb.append("sessionPercentage: ");
                sb.append(f3);
                sb.append(", realWidth: ");
                sb.append(width);
                sb.append(", sessionWidth: ");
                sb.append(f4);
                local.d(str, sb.toString());
                List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
                int size2 = sleepStates.size();
                int i12 = i8;
                int i13 = 0;
                while (i13 < size2) {
                    int i14 = sleepStates.get(i13).state;
                    if (i13 < sleepStates.size() - 1) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = G;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("From ");
                        sb2.append(sleepStates.get(i13).index);
                        sb2.append(" to ");
                        int i15 = i13 + 1;
                        sb2.append(sleepStates.get(i15).index);
                        sb2.append(": stateType ");
                        sb2.append(i14);
                        local2.d(str2, sb2.toString());
                        i2 = ((int) sleepStates.get(i15).index) - ((int) sleepStates.get(i13).index);
                    } else {
                        FLogger.INSTANCE.getLocal().d(G, "From " + sleepStates.get(i13).index + " to " + durationInMinutes + ": stateType " + i14);
                        i2 = durationInMinutes - ((int) sleepStates.get(i13).index);
                    }
                    int i16 = (int) ((((float) i2) / f2) * f4);
                    int i17 = i16 < 1 ? 1 : i16;
                    if (i14 == 0) {
                        float f5 = (float) i7;
                        rectF = new RectF((float) i12, (float) ((int) (0.8f * f5)), (float) (i12 + i17), f5);
                        Paint paint = this.s;
                        a aVar = this.E;
                        if (aVar == a.All) {
                            if (i13 == 0) {
                                if (sleepStates.get(i13 + 1).state != 0) {
                                    K37.b(canvas, rectF, paint, (float) this.z);
                                    rectF2 = rectF;
                                } else {
                                    K37.d(canvas, rectF, paint, (float) this.z);
                                    rectF2 = rectF;
                                }
                            } else if (i13 != sleepStates.size() - 1) {
                                int i18 = i13 - 1;
                                if (sleepStates.get(i18).state != 0 && sleepStates.get(i13 + 1).state != 0) {
                                    canvas.drawRect(rectF, paint);
                                } else if (sleepStates.get(i18).state != 0) {
                                    K37.c(canvas, rectF, paint, (float) this.z);
                                } else if (sleepStates.get(i13 + 1).state != 0) {
                                    K37.b(canvas, rectF, paint, (float) this.z);
                                } else {
                                    K37.d(canvas, rectF, paint, (float) this.z);
                                }
                            } else if (sleepStates.get(i13 - 1).state != 0) {
                                K37.c(canvas, rectF, paint, (float) this.z);
                            } else {
                                K37.d(canvas, rectF, paint, (float) this.z);
                            }
                        } else if (aVar == a.Awake) {
                            K37.d(canvas, rectF, paint, (float) this.z);
                        }
                        i9 += i2;
                        if (i9 >= this.F || z3) {
                            z2 = z3;
                        } else {
                            d(canvas, (int) (rectF.left + (rectF.width() / ((float) 2))), i7);
                            z2 = true;
                        }
                        i12 = i17 + i12;
                        i13++;
                        z3 = z2;
                        canvas2 = canvas;
                        sleepDayDetailsChart = this;
                    } else if (i14 != 1) {
                        float f6 = (float) i7;
                        rectF2 = new RectF((float) i12, (float) ((int) (0.3f * f6)), (float) (i12 + i17), f6);
                        a aVar2 = this.E;
                        if (aVar2 == a.All || aVar2 == a.Restful) {
                            K37.d(canvas, rectF2, this.u, (float) this.z);
                        }
                    } else {
                        Paint paint2 = this.t;
                        float f7 = (float) i7;
                        RectF rectF3 = new RectF((float) i12, (float) ((int) (0.55f * f7)), (float) (i12 + i17), f7);
                        a aVar3 = this.E;
                        if (aVar3 == a.All) {
                            if (i13 == 0) {
                                if (sleepStates.get(i13 + 1).state != 0) {
                                    K37.b(canvas, rectF3, paint2, (float) this.z);
                                } else {
                                    K37.d(canvas, rectF3, paint2, (float) this.z);
                                }
                            } else if (i13 != sleepStates.size() - 1) {
                                int i19 = i13 - 1;
                                if (sleepStates.get(i19).state != 0 && sleepStates.get(i13 + 1).state != 0) {
                                    canvas.drawRect(rectF3, paint2);
                                } else if (sleepStates.get(i19).state != 0) {
                                    K37.c(canvas, rectF3, paint2, (float) this.z);
                                } else if (sleepStates.get(i13 + 1).state != 0) {
                                    K37.b(canvas, rectF3, paint2, (float) this.z);
                                } else {
                                    K37.d(canvas, rectF3, paint2, (float) this.z);
                                }
                            } else if (sleepStates.get(i13 - 1).state != 0) {
                                K37.c(canvas, rectF3, paint2, (float) this.z);
                            } else {
                                K37.d(canvas, rectF3, paint2, (float) this.z);
                            }
                        } else if (aVar3 == a.Light) {
                            K37.d(canvas, rectF3, paint2, (float) this.z);
                        }
                        rectF2 = rectF3;
                    }
                    rectF = rectF2;
                    i9 += i2;
                    if (i9 >= this.F) {
                    }
                    z2 = z3;
                    i12 = i17 + i12;
                    i13++;
                    z3 = z2;
                    canvas2 = canvas;
                    sleepDayDetailsChart = this;
                }
                canvas2.drawText(sleepSessionData2.getEndTimeString(), ((float) i12) - measureText, (float) (getHeight() - i11), sleepDayDetailsChart.w);
                i7 = i7;
                i8 = sleepDayDetailsChart.A + i12;
            }
            sleepDayDetailsChart.c(canvas2, 0, i7);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 2131231085;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.C;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.y.setColor(this.e);
        float f2 = (float) 2;
        this.y.setStrokeWidth(f2);
        this.s.setColor(this.h);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setStrokeWidth((float) this.B);
        this.t.setColor(this.g);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setStrokeWidth((float) this.B);
        this.u.setColor(this.f);
        this.u.setStyle(Paint.Style.FILL);
        this.u.setStrokeWidth((float) this.B);
        this.w.setColor(this.i);
        this.w.setStyle(Paint.Style.FILL);
        this.w.setTextSize((float) this.l);
        this.w.setTypeface(Nl0.b(getContext(), this.m));
        this.x.setColor(this.j);
        this.x.setStyle(Paint.Style.STROKE);
        this.x.setStrokeWidth(f2);
        this.x.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.v.setStyle(Paint.Style.FILL);
        this.v.setColorFilter(new PorterDuffColorFilter(this.k, PorterDuff.Mode.SRC_IN));
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
