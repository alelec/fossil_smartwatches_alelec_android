package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatButton;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Hm7;
import com.fossil.Um5;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleButton extends AppCompatButton {
    @DexIgnore
    public static /* final */ ArrayList<String> i; // = Hm7.c("success", "textButtonDisable", "success", "success");
    @DexIgnore
    public static /* final */ ArrayList<String> j; // = Hm7.c("onPrimaryButton", "textButtonPrimary", "primaryButton", "primaryButtonBorder");
    @DexIgnore
    public static /* final */ ArrayList<String> k; // = Hm7.c("onSecondaryButton", "textButtonSecondary", "secondaryButton", "secondaryButtonBorder");
    @DexIgnore
    public static /* final */ ArrayList<String> l; // = Hm7.c("onDisabledButton", "textButtonDisable", "disabledButton", "disabledButtonBorder");
    @DexIgnore
    public static /* final */ ArrayList<String> m; // = Hm7.c("nonBrandSurface", "complicationCategory", "primaryButton", "primaryButton");
    @DexIgnore
    public static /* final */ ArrayList<String> s; // = Hm7.c("primaryText", "checkBox", "nonBrandSurface", "disabledButtonBorder");
    @DexIgnore
    public static /* final */ ArrayList<String> t; // = Hm7.c("nonBrandPlaceholderText", "checkBox", "nonBrandPlaceholderBackground", "nonBrandPlaceholderBackground");
    @DexIgnore
    public static /* final */ ArrayList<String> u; // = Hm7.c("onNavigationRightApplyButton", "textButtonSecondary", "navigationRightApplyButton", "navigationRightApplyButton");
    @DexIgnore
    public static /* final */ ArrayList<String> v; // = Hm7.c("onNavigationRightAppliedButton", "textButtonPrimary", "navigationRightAppliedButton", "navigationRightAppliedButton");
    @DexIgnore
    public /* final */ String d; // = "FlexibleButton";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context) {
        super(context);
        Wg6.c(context, "context");
        b(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    public final String a(int i2) {
        String c = Um5.c(PortfolioApp.get.instance(), i2);
        Wg6.b(c, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return c;
    }

    @DexIgnore
    public final void b(AttributeSet attributeSet) {
        CharSequence charSequence;
        CharSequence text = getText();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleButton);
            String string = obtainStyledAttributes.getString(2);
            if (string == null) {
                string = "";
            }
            this.e = string;
            String string2 = obtainStyledAttributes.getString(3);
            if (string2 == null) {
                string2 = "";
            }
            this.f = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 == null) {
                string3 = "";
            }
            this.g = string3;
            String string4 = obtainStyledAttributes.getString(1);
            if (string4 == null) {
                string4 = "";
            }
            this.h = string4;
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            charSequence = resourceId != -1 ? a(resourceId) : text;
            obtainStyledAttributes2.recycle();
        } else {
            charSequence = text;
        }
        if (!TextUtils.isEmpty(charSequence)) {
            setText(charSequence);
        }
        if (TextUtils.isEmpty(this.e)) {
            this.e = "onPrimaryButton";
        }
        if (TextUtils.isEmpty(this.f)) {
            this.f = "textButtonPrimary";
        }
        c(this.e, this.f, this.g, this.h);
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void c(String str, String str2, String str3, String str4) {
        Wg6.c(str, "textColorStyle");
        Wg6.c(str2, "textFontStyle");
        Wg6.c(str3, "backgroundColorStyle");
        Wg6.c(str4, "borderColorStyle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = this.d;
        local.d(str5, "setStyle textColorStyle=" + str + " textFontStyle=" + str2 + " backgroundColorStyle=" + str3 + " borderColorStyle=" + str4);
        String d2 = ThemeManager.l.a().d(str);
        Typeface f2 = ThemeManager.l.a().f(str2);
        String d3 = ThemeManager.l.a().d(str3);
        String d4 = ThemeManager.l.a().d(str4);
        if (d2 != null) {
            setTextColor(Color.parseColor(d2));
        }
        if (f2 != null) {
            setTypeface(f2);
        }
        if (d3 != null) {
            setBackgroundColor(Color.parseColor(d3));
        }
        if (d4 != null) {
            LayerDrawable layerDrawable = (LayerDrawable) W6.f(getContext(), 2131230848);
            GradientDrawable gradientDrawable = (GradientDrawable) (layerDrawable != null ? layerDrawable.findDrawableByLayerId(2131363115) : null);
            if (!(d3 == null || gradientDrawable == null)) {
                gradientDrawable.setColor(Color.parseColor(d3));
            }
            if (gradientDrawable != null) {
                gradientDrawable.setStroke(4, Color.parseColor(d4));
            }
            setBackground(layerDrawable);
        }
    }

    @DexIgnore
    public final void d(String str) {
        Wg6.c(str, "state");
        switch (str.hashCode()) {
            case -1362481035:
                if (str.equals("flexible_button_disabled")) {
                    String str2 = l.get(0);
                    Wg6.b(str2, "DISABLED_BUTTON_STYLES[0]");
                    String str3 = l.get(1);
                    Wg6.b(str3, "DISABLED_BUTTON_STYLES[1]");
                    String str4 = l.get(2);
                    Wg6.b(str4, "DISABLED_BUTTON_STYLES[2]");
                    String str5 = l.get(3);
                    Wg6.b(str5, "DISABLED_BUTTON_STYLES[3]");
                    c(str2, str3, str4, str5);
                    return;
                }
                return;
            case -607282431:
                if (str.equals("flexible_button_right_applied")) {
                    String str6 = v.get(0);
                    Wg6.b(str6, "RIGHT_APPLIED_BUTTON_STYLES[0]");
                    String str7 = v.get(1);
                    Wg6.b(str7, "RIGHT_APPLIED_BUTTON_STYLES[1]");
                    String str8 = v.get(2);
                    Wg6.b(str8, "RIGHT_APPLIED_BUTTON_STYLES[2]");
                    String str9 = v.get(3);
                    Wg6.b(str9, "RIGHT_APPLIED_BUTTON_STYLES[3]");
                    c(str6, str7, str8, str9);
                    return;
                }
                return;
            case -582367622:
                if (str.equals("flexible_button_nonBrandSurface")) {
                    String str10 = m.get(0);
                    Wg6.b(str10, "nonBrandSurface_BUTTON_STYLES[0]");
                    String str11 = m.get(1);
                    Wg6.b(str11, "nonBrandSurface_BUTTON_STYLES[1]");
                    String str12 = m.get(2);
                    Wg6.b(str12, "nonBrandSurface_BUTTON_STYLES[2]");
                    String str13 = m.get(3);
                    Wg6.b(str13, "nonBrandSurface_BUTTON_STYLES[3]");
                    c(str10, str11, str12, str13);
                    return;
                }
                return;
            case -152730143:
                if (str.equals("flexible_button_search")) {
                    String str14 = t.get(0);
                    Wg6.b(str14, "SEARCH_BUTTON_STYLES[0]");
                    String str15 = t.get(1);
                    Wg6.b(str15, "SEARCH_BUTTON_STYLES[1]");
                    String str16 = t.get(2);
                    Wg6.b(str16, "SEARCH_BUTTON_STYLES[2]");
                    String str17 = t.get(3);
                    Wg6.b(str17, "SEARCH_BUTTON_STYLES[3]");
                    c(str14, str15, str16, str17);
                    return;
                }
                return;
            case 85932699:
                if (str.equals("flexible_button_secondary")) {
                    String str18 = k.get(0);
                    Wg6.b(str18, "SECONDARY_BUTTON_STYLES[0]");
                    String str19 = k.get(1);
                    Wg6.b(str19, "SECONDARY_BUTTON_STYLES[1]");
                    String str20 = k.get(2);
                    Wg6.b(str20, "SECONDARY_BUTTON_STYLES[2]");
                    String str21 = k.get(3);
                    Wg6.b(str21, "SECONDARY_BUTTON_STYLES[3]");
                    c(str18, str19, str20, str21);
                    return;
                }
                return;
            case 324320304:
                if (str.equals("flexible_button_connected")) {
                    String str22 = i.get(0);
                    Wg6.b(str22, "CONNECTED_BUTTON_STYLES[0]");
                    String str23 = i.get(1);
                    Wg6.b(str23, "CONNECTED_BUTTON_STYLES[1]");
                    String str24 = i.get(2);
                    Wg6.b(str24, "CONNECTED_BUTTON_STYLES[2]");
                    String str25 = i.get(3);
                    Wg6.b(str25, "CONNECTED_BUTTON_STYLES[3]");
                    c(str22, str23, str24, str25);
                    return;
                }
                return;
            case 625065714:
                if (str.equals("flexible_button_right_apply")) {
                    String str26 = u.get(0);
                    Wg6.b(str26, "RIGHT_APPLY_BUTTON_STYLES[0]");
                    String str27 = u.get(1);
                    Wg6.b(str27, "RIGHT_APPLY_BUTTON_STYLES[1]");
                    String str28 = u.get(2);
                    Wg6.b(str28, "RIGHT_APPLY_BUTTON_STYLES[2]");
                    String str29 = u.get(3);
                    Wg6.b(str29, "RIGHT_APPLY_BUTTON_STYLES[3]");
                    c(str26, str27, str28, str29);
                    return;
                }
                return;
            case 1076923322:
                if (str.equals("flexible_button_checkbox_typeface")) {
                    String str30 = s.get(0);
                    Wg6.b(str30, "CHECKBOX_BUTTON_STYLES[0]");
                    String str31 = s.get(1);
                    Wg6.b(str31, "CHECKBOX_BUTTON_STYLES[1]");
                    String str32 = s.get(2);
                    Wg6.b(str32, "CHECKBOX_BUTTON_STYLES[2]");
                    String str33 = s.get(3);
                    Wg6.b(str33, "CHECKBOX_BUTTON_STYLES[3]");
                    c(str30, str31, str32, str33);
                    return;
                }
                return;
            case 1572205801:
                if (str.equals("flexible_button_primary")) {
                    String str34 = j.get(0);
                    Wg6.b(str34, "PRIMARY_BUTTON_STYLES[0]");
                    String str35 = j.get(1);
                    Wg6.b(str35, "PRIMARY_BUTTON_STYLES[1]");
                    String str36 = j.get(2);
                    Wg6.b(str36, "PRIMARY_BUTTON_STYLES[2]");
                    String str37 = j.get(3);
                    Wg6.b(str37, "PRIMARY_BUTTON_STYLES[3]");
                    c(str34, str35, str36, str37);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
