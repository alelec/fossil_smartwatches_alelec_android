package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.Hr7;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AnimationImageView extends AppCompatImageView {
    @DexIgnore
    public /* final */ ArrayList<String> d; // = new ArrayList<>();
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public Bitmap k;
    @DexIgnore
    public Bitmap l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public Runnable t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AnimationImageView b;

        @DexIgnore
        public a(AnimationImageView animationImageView) {
            this.b = animationImageView;
        }

        @DexIgnore
        public final void run() {
            AnimationImageView animationImageView = this.b;
            animationImageView.setImageBitmap(animationImageView.l);
            AnimationImageView animationImageView2 = this.b;
            animationImageView2.p(animationImageView2.k);
            AnimationImageView animationImageView3 = this.b;
            animationImageView3.k = animationImageView3.l;
            if (!(!this.b.d.isEmpty())) {
                return;
            }
            if (this.b.j == this.b.g) {
                AnimationImageView animationImageView4 = this.b;
                Object obj = animationImageView4.d.get(0);
                Wg6.b(obj, "mListImagesPathFromAssets[0]");
                animationImageView4.l = animationImageView4.m((String) obj);
                this.b.o(0);
                return;
            }
            AnimationImageView animationImageView5 = this.b;
            Object obj2 = animationImageView5.d.get(this.b.j + 1);
            Wg6.b(obj2, "mListImagesPathFromAssets[mFrameNo + 1]");
            animationImageView5.l = animationImageView5.m((String) obj2);
            AnimationImageView animationImageView6 = this.b;
            animationImageView6.o(animationImageView6.j + 1);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context) {
        super(context);
        Wg6.c(context, "context");
        Context context2 = getContext();
        Wg6.b(context2, "context");
        Resources resources = context2.getResources();
        Wg6.b(resources, "context.resources");
        this.m = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        Wg6.b(context3, "context");
        Resources resources2 = context3.getResources();
        Wg6.b(resources2, "context.resources");
        this.s = resources2.getDisplayMetrics().widthPixels;
        this.t = new a(this);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        Context context2 = getContext();
        Wg6.b(context2, "context");
        Resources resources = context2.getResources();
        Wg6.b(resources, "context.resources");
        this.m = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        Wg6.b(context3, "context");
        Resources resources2 = context3.getResources();
        Wg6.b(resources2, "context.resources");
        this.s = resources2.getDisplayMetrics().widthPixels;
        this.t = new a(this);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        Context context2 = getContext();
        Wg6.b(context2, "context");
        Resources resources = context2.getResources();
        Wg6.b(resources, "context.resources");
        this.m = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        Wg6.b(context3, "context");
        Resources resources2 = context3.getResources();
        Wg6.b(resources2, "context.resources");
        this.s = resources2.getDisplayMetrics().widthPixels;
        this.t = new a(this);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0021, code lost:
        return r2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.Bitmap m(java.lang.String r5) {
        /*
            r4 = this;
            r2 = 0
            android.content.Context r0 = r4.getContext()     // Catch:{ IOException -> 0x0022, all -> 0x0032 }
            java.lang.String r1 = "context"
            com.mapped.Wg6.b(r0, r1)     // Catch:{ IOException -> 0x0022, all -> 0x0032 }
            android.content.res.AssetManager r0 = r0.getAssets()     // Catch:{ IOException -> 0x0022, all -> 0x0032 }
            java.io.InputStream r1 = r0.open(r5)     // Catch:{ IOException -> 0x0022, all -> 0x0032 }
            int r0 = r4.s     // Catch:{ IOException -> 0x0034 }
            int r3 = r4.m     // Catch:{ IOException -> 0x0034 }
            int r3 = r3 / 2
            android.graphics.Bitmap r2 = com.fossil.Vk5.b(r1, r0, r3)     // Catch:{ IOException -> 0x0034 }
            if (r1 == 0) goto L_0x0021
        L_0x001e:
            r1.close()
        L_0x0021:
            return r2
        L_0x0022:
            r0 = move-exception
            r1 = r2
        L_0x0024:
            r0.printStackTrace()     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0021
            goto L_0x001e
        L_0x002a:
            r0 = move-exception
            r2 = r1
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()
        L_0x0031:
            throw r0
        L_0x0032:
            r0 = move-exception
            goto L_0x002c
        L_0x0034:
            r0 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.AnimationImageView.m(java.lang.String):android.graphics.Bitmap");
    }

    @DexIgnore
    public final void n() {
        if (this.f) {
            this.j = 0;
            return;
        }
        this.f = true;
        removeCallbacks(this.t);
        o(1);
    }

    @DexIgnore
    public final void o(int i2) {
        int i3;
        if (this.f) {
            int i4 = this.e;
            if ((i2 != this.g || (i3 = this.i) <= 0) && (i2 != 1 || (i3 = this.h) <= 0)) {
                i3 = i4;
            }
            this.j = i2;
            try {
                postDelayed(this.t, (long) i3);
            } catch (Exception e2) {
            }
        }
    }

    @DexIgnore
    public final void p(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception e2) {
            }
        }
    }

    @DexIgnore
    public final void q(String str, int i2, int i3, int i4, int i5, int i6) {
        Wg6.c(str, "imagesPathFromAssets");
        this.d.clear();
        if (i2 <= i3) {
            while (true) {
                ArrayList<String> arrayList = this.d;
                Hr7 hr7 = Hr7.a;
                String format = String.format(str, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                arrayList.add(format);
                if (i2 == i3) {
                    break;
                }
                i2++;
            }
        }
        if (this.d.size() > 1) {
            this.e = i4;
            this.h = i5;
            this.i = i6;
            this.g = this.d.size() - 1;
            String str2 = this.d.get(0);
            Wg6.b(str2, "mListImagesPathFromAssets[0]");
            this.k = m(str2);
            String str3 = this.d.get(1);
            Wg6.b(str3, "mListImagesPathFromAssets[1]");
            this.l = m(str3);
            setImageBitmap(this.k);
        }
    }

    @DexIgnore
    public final void r() {
        removeCallbacks(this.t);
        this.f = false;
    }
}
