package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Oa1;
import com.mapped.W6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BarChart extends View {
    @DexIgnore
    public Paint A;
    @DexIgnore
    public Paint B;
    @DexIgnore
    public Paint C;
    @DexIgnore
    public /* final */ Path D; // = new Path();
    @DexIgnore
    public Bitmap E;
    @DexIgnore
    public Canvas F;
    @DexIgnore
    public /* final */ List<b> b; // = new ArrayList();
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public /* final */ Rect g; // = new Rect();
    @DexIgnore
    public float h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float j;
    @DexIgnore
    public String k;
    @DexIgnore
    public float l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public Drawable v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public GestureDetector y;
    @DexIgnore
    public View.OnClickListener z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            return BarChart.this.z != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (BarChart.this.z != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float x2 = BarChart.this.getX();
                float y2 = BarChart.this.getY();
                float measuredWidth = ((float) (BarChart.this.getMeasuredWidth() / 2)) - x2;
                float f = (x - x2) - measuredWidth;
                float f2 = (y - y2) - measuredWidth;
                BarChart barChart = BarChart.this;
                float f3 = barChart.d;
                if ((f * f) + (f2 * f2) < (measuredWidth + f3) * (f3 + measuredWidth)) {
                    barChart.z.onClick(barChart);
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public float b;
        @DexIgnore
        public List<Pair<Float, Integer>> c;
    }

    @DexIgnore
    public BarChart(Context context) {
        super(context);
        b();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public BarChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, X24.BarChart, 0, 0);
        try {
            this.d = obtainStyledAttributes.getDimension(14, getResources().getDimension(2131165413));
            this.e = obtainStyledAttributes.getDimension(8, getResources().getDimension(2131165437));
            this.f = obtainStyledAttributes.getDimension(0, getResources().getDimension(2131165392));
            this.h = obtainStyledAttributes.getDimension(11, getResources().getDimension(2131165400));
            this.i = obtainStyledAttributes.getColor(9, W6.d(context, 2131099827));
            this.j = obtainStyledAttributes.getDimension(13, getResources().getDimension(2131165726));
            this.k = obtainStyledAttributes.getString(10);
            this.l = obtainStyledAttributes.getDimension(12, getResources().getDimension(2131165437));
            this.m = obtainStyledAttributes.getColor(1, W6.d(context, 2131099827));
            this.s = obtainStyledAttributes.getDimension(4, getResources().getDimension(2131165371));
            this.t = obtainStyledAttributes.getDimension(3, getResources().getDimension(2131165407));
            this.u = obtainStyledAttributes.getDimension(2, getResources().getDimension(2131165413));
            this.v = obtainStyledAttributes.getDrawable(5);
            this.w = obtainStyledAttributes.getDimension(7, getResources().getDimension(2131165398));
            this.x = obtainStyledAttributes.getDimension(6, getResources().getDimension(2131165426));
            obtainStyledAttributes.recycle();
            b();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public void a(String str, float f2, List<Pair<Float, Integer>> list) {
        b bVar = new b();
        bVar.a = str;
        bVar.b = f2;
        bVar.c = new ArrayList(list);
        this.b.add(bVar);
    }

    @DexIgnore
    public final void b() {
        Paint paint = new Paint(1);
        this.A = paint;
        paint.setStrokeWidth(this.d);
        Paint paint2 = new Paint(1);
        this.B = paint2;
        if (this.k != null) {
            paint2.setTypeface(Typeface.createFromAsset(getContext().getAssets(), this.k));
        }
        this.B.setTextSize(this.j);
        this.B.setColor(this.i);
        Paint paint3 = new Paint(1);
        this.C = paint3;
        paint3.setColor(this.m);
        this.C.setStrokeWidth(this.s);
        this.C.setStyle(Paint.Style.STROKE);
        this.C.setPathEffect(new DashPathEffect(new float[]{this.t, this.u}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.y = new GestureDetector(getContext(), new a());
        if (isInEditMode()) {
            Context context = getContext();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new Pair<>(Float.valueOf(123.0f), Integer.valueOf(W6.d(context, 2131099850))));
            arrayList.add(new Pair<>(Float.valueOf(234.0f), Integer.valueOf(W6.d(context, 2131099829))));
            arrayList.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(W6.d(context, 2131099847))));
            a(DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, 500.0f, arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new Pair<>(Float.valueOf(234.0f), Integer.valueOf(W6.d(context, 2131099850))));
            arrayList2.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(W6.d(context, 2131099829))));
            arrayList2.add(new Pair<>(Float.valueOf(456.0f), Integer.valueOf(W6.d(context, 2131099847))));
            a("M", 1000.0f, arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new Pair<>(Float.valueOf(345.0f), Integer.valueOf(W6.d(context, 2131099850))));
            arrayList3.add(new Pair<>(Float.valueOf(456.0f), Integer.valueOf(W6.d(context, 2131099829))));
            arrayList3.add(new Pair<>(Float.valueOf(678.0f), Integer.valueOf(W6.d(context, 2131099847))));
            a("T", 1500.0f, arrayList3);
            this.c = 1479.0f;
            this.v = W6.f(context, 2131230890);
        }
    }

    @DexIgnore
    public void c() {
        for (b bVar : this.b) {
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (Pair<Float, Integer> pair : bVar.c) {
                f2 = Math.max(f2, ((Float) pair.first).floatValue());
            }
            float max = Math.max(this.c, f2);
            this.c = max;
            this.c = Math.max(max, bVar.b);
        }
        invalidate();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Bitmap bitmap = this.E;
        if (bitmap != null) {
            bitmap.eraseColor(0);
            this.D.reset();
            float f2 = this.h;
            float measuredHeight = ((float) getMeasuredHeight()) - f2;
            float ascent = ((f2 - this.B.ascent()) - this.B.descent()) / 2.0f;
            float measuredWidth = ((((float) getMeasuredWidth()) - this.e) - this.f) / ((float) this.b.size());
            float f3 = measuredHeight - this.l;
            float f4 = f3 - (this.w / 2.0f);
            float f5 = (measuredWidth / 2.0f) + this.e;
            float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f7 = 0.0f;
            for (b bVar : this.b) {
                float f8 = f3;
                for (Pair<Float, Integer> pair : bVar.c) {
                    this.A.setColor(((Integer) pair.second).intValue());
                    float floatValue = f8 - ((((Float) pair.first).floatValue() * f4) / this.c);
                    this.F.drawLine(f5, f8, f5, floatValue, this.A);
                    f8 = floatValue;
                }
                Paint paint = this.B;
                String str = bVar.a;
                paint.getTextBounds(str, 0, str.length(), this.g);
                this.F.drawText(bVar.a, f5 - ((float) (this.g.width() / 2)), ascent + measuredHeight, this.B);
                float f9 = bVar.b;
                float f10 = f3 - ((f4 * f9) / this.c);
                if (f10 == f6 || f9 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f10 = f6;
                } else {
                    this.D.moveTo(f7, f10);
                }
                float f11 = this.b.indexOf(bVar) == 0 ? this.e : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                float f12 = f11 + measuredWidth + (this.b.indexOf(bVar) == this.b.size() - 1 ? this.f - (this.v != null ? this.w + (this.x * 2.0f) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) + f7;
                if (f10 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && bVar.b > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    this.D.lineTo(f12, f10);
                }
                f5 += measuredWidth;
                f6 = f10;
                f7 = f12;
            }
            this.F.drawPath(this.D, this.C);
            Drawable drawable = this.v;
            if (drawable != null && f6 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                float f13 = this.x + f7;
                float f14 = this.w;
                drawable.setBounds((int) f13, (int) (f6 - (f14 / 2.0f)), (int) (f13 + f14), (int) ((f14 / 2.0f) + f6));
                this.v.draw(this.F);
            }
            canvas.drawBitmap(this.E, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Bitmap bitmap = this.E;
        if (bitmap == null || i2 > bitmap.getWidth() || i3 > this.E.getHeight()) {
            this.E = Oa1.c(getContext()).f().c(i2, i3, Bitmap.Config.ARGB_8888);
        } else {
            this.E.setWidth(i2);
            this.E.setHeight(i3);
        }
        Canvas canvas = this.F;
        if (canvas == null) {
            this.F = new Canvas(this.E);
        } else {
            canvas.setBitmap(this.E);
        }
        c();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.y.onTouchEvent(motionEvent);
    }
}
