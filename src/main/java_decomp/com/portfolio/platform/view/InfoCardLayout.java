package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InfoCardLayout extends FrameLayout {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ InfoCardLayout b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(InfoCardLayout infoCardLayout) {
            this.b = infoCardLayout;
        }

        @DexIgnore
        public void onGlobalLayout() {
            this.b.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            InfoCardLayout infoCardLayout = this.b;
            BottomSheetBehavior<FrameLayout> o = BottomSheetBehavior.o(infoCardLayout);
            Wg6.b(o, "BottomSheetBehavior.from(this@InfoCardLayout)");
            infoCardLayout.setMBottomSheetBehavior$app_fossilRelease(o);
            this.b.getMBottomSheetBehavior$app_fossilRelease().z(false);
            this.b.getMBottomSheetBehavior$app_fossilRelease().A(0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InfoCardLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        getViewTreeObserver().addOnGlobalLayoutListener(new a(this));
    }

    @DexIgnore
    public final BottomSheetBehavior<FrameLayout> getMBottomSheetBehavior$app_fossilRelease() {
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.b;
        if (bottomSheetBehavior != null) {
            return bottomSheetBehavior;
        }
        Wg6.n("mBottomSheetBehavior");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        return true;
    }

    @DexIgnore
    public final void setMBottomSheetBehavior$app_fossilRelease(BottomSheetBehavior<FrameLayout> bottomSheetBehavior) {
        Wg6.c(bottomSheetBehavior, "<set-?>");
        this.b = bottomSheetBehavior;
    }
}
