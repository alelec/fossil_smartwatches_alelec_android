package com.misfit.frameworks.common.model.Cucumber;

import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CucumberRegion {
    @DexIgnore
    public float regionCenterLat;
    @DexIgnore
    public float regionCenterLong;
    @DexIgnore
    public float regionSpanLat;
    @DexIgnore
    public float regionSpanLong;

    @DexIgnore
    public float getRegionCenterLat() {
        return this.regionCenterLat;
    }

    @DexIgnore
    public float getRegionCenterLong() {
        return this.regionCenterLong;
    }

    @DexIgnore
    public float getRegionSpanLat() {
        return this.regionSpanLat;
    }

    @DexIgnore
    public float getRegionSpanLong() {
        return this.regionSpanLong;
    }

    @DexIgnore
    public void setRegionCenterLat(float f) {
        this.regionCenterLat = f;
    }

    @DexIgnore
    public void setRegionCenterLong(float f) {
        this.regionCenterLong = f;
    }

    @DexIgnore
    public void setRegionSpanLat(float f) {
        this.regionSpanLat = f;
    }

    @DexIgnore
    public void setRegionSpanLong(float f) {
        this.regionSpanLong = f;
    }

    @DexIgnore
    public JSONObject toJson() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(Constants.REGION_CENTER_LAT, (double) this.regionCenterLat);
            jSONObject.put(Constants.REGION_CENTER_LONG, (double) this.regionCenterLong);
            jSONObject.put(Constants.REGION_SPAN_LAT, (double) this.regionSpanLat);
            jSONObject.put(Constants.REGION_SPAN_LONG, (double) this.regionSpanLong);
            return jSONObject;
        } catch (Exception e) {
            return null;
        }
    }
}
