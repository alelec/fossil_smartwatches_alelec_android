package com.misfit.frameworks.network.configuration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFConfiguration {
    @DexIgnore
    public String baseServerUrl;
    @DexIgnore
    public MFHeader header;

    @DexIgnore
    public MFConfiguration(String str, MFHeader mFHeader) {
        this.baseServerUrl = str;
        this.header = mFHeader;
    }

    @DexIgnore
    public String getBaseServerUrl() {
        return this.baseServerUrl;
    }

    @DexIgnore
    public MFHeader getHeader() {
        return this.header;
    }
}
