package com.misfit.frameworks.network.utils;

import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DateTimeUtil {
    @DexIgnore
    public static /* final */ String TAG; // = "DateTimeUtil";
    @DexIgnore
    public static /* final */ String UTC_DATETIME_FORMAT; // = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    @DexIgnore
    public static int compare(Date date, Date date2) {
        if (date == null) {
            return 1;
        }
        if (date2 == null) {
            return -1;
        }
        if (date.after(date2)) {
            return -1;
        }
        return !date.before(date2) ? 0 : 1;
    }

    @DexIgnore
    public static Date parseUTCDateTime(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US).parse(str);
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "Error inside " + TAG + ".parseUTCDateTime - e=" + e);
            return null;
        }
    }
}
