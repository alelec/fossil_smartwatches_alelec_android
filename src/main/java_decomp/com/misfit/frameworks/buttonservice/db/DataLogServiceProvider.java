package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataLogServiceProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "log_service.db";
    @DexIgnore
    public static /* final */ int KEY_NOTIFICATION_DEBUGLOG_ID; // = 999;
    @DexIgnore
    public static /* final */ String TAG; // = "DataLogServiceProvider";
    @DexIgnore
    public static DataLogServiceProvider sInstance;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public DataLogServiceProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataLogService, Integer> getDataLogServicesDAO() throws SQLException {
        return this.databaseHelper.getDao(DataLogService.class);
    }

    @DexIgnore
    public static DataLogServiceProvider getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DataLogServiceProvider(context, DB_NAME);
        }
        return sInstance;
    }

    @DexIgnore
    public void addNotificationDebugLog(String str) {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "addNotificationDebugLog - content=" + str);
            DataLogService dataLogServiceById = getDataLogServiceById(999);
            if (dataLogServiceById != null) {
                try {
                    dataLogServiceById.setContent(dataLogServiceById.getContent() + str);
                    getDataLogServicesDAO().update((Dao<DataLogService, Integer>) dataLogServiceById);
                } catch (Exception e) {
                    e.printStackTrace();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.e(str3, "addNotificationDebugLog - e=" + e);
                }
            } else {
                DataLogService dataLogService = new DataLogService();
                dataLogService.setContent(str);
                dataLogService.setId(999);
                getDataLogServicesDAO().create((Dao<DataLogService, Integer>) dataLogService);
            }
        }
    }

    @DexIgnore
    public void createOrUpdate(DataLogService dataLogService) {
        synchronized (this) {
            if (dataLogService == null) {
                FLogger.INSTANCE.getLocal().e(TAG, "createOrUpdate - dataLogService=null");
                return;
            }
            int id = dataLogService.getId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "createOrUpdate - logId=" + id);
            DataLogService dataLogServiceById = getDataLogServiceById(id);
            try {
                if (dataLogService.getContent() == null || dataLogService.getContent().getBytes().length <= 819200) {
                    if (dataLogServiceById != null) {
                        getDataLogServicesDAO().update((Dao<DataLogService, Integer>) dataLogService);
                    } else {
                        getDataLogServicesDAO().create((Dao<DataLogService, Integer>) dataLogService);
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "createOrUpdate - logId=" + id + ", currentTimeMillis=" + System.currentTimeMillis());
                    return;
                }
                FLogger.INSTANCE.getLocal().e(TAG, "createOrUpdate - content data log > 800Kb");
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.e(str3, "createOrUpdate - logId=" + id + ", e=" + e);
            }
        }
    }

    @DexIgnore
    public List<DataLogService> getAllDataLogServiceByStatus(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getAllDataLogServiceByStatus - status=" + i);
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DataLogService, Integer> queryBuilder = getDataLogServicesDAO().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(i));
            return queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "getAllDataLogServiceByStatus - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public List<DataLogService> getAllDataLogServiceByStatusAndLogStyle(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getAllDataLogServiceByStatusAndLogStyle - status=" + i + ", logStyle=" + i2);
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DataLogService, Integer> queryBuilder = getDataLogServicesDAO().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(i)).and().eq(DataLogService.COLUMN_LOG_STYLE, Integer.valueOf(i2));
            return queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "getAllDataLogServiceByStatusAndLogStyle - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public DataLogService getDataLogServiceById(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataLogServiceById - id=" + i);
        try {
            return getDataLogServicesDAO().queryBuilder().where().eq("id", Integer.valueOf(i)).queryForFirst();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "getDataLogServiceById - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DataLogService.class};
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public int getDbVersion() {
        return 3;
    }

    @DexIgnore
    public void remove(DataLogService dataLogService) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "remove - dataLogService=" + dataLogService);
        if (dataLogService != null) {
            try {
                DeleteBuilder<DataLogService, Integer> deleteBuilder = getDataLogServicesDAO().deleteBuilder();
                deleteBuilder.where().eq("id", Integer.valueOf(dataLogService.getId()));
                deleteBuilder.delete();
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, "remove - e=" + e);
            }
        }
    }

    @DexIgnore
    public void removeById(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "removeById - id=" + i);
        try {
            DeleteBuilder<DataLogService, Integer> deleteBuilder = getDataLogServicesDAO().deleteBuilder();
            deleteBuilder.where().eq("id", Integer.valueOf(i));
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "removeById - e=" + e);
        }
    }
}
