package com.misfit.frameworks.buttonservice.enums;

import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.V60;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum HeartRateMode {
    NONE(0),
    CONTINUOUS(1),
    LOW_POWER(2),
    DISABLE(3);
    
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[V60.Ai.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[V60.Ai.CONTINUOUS.ordinal()] = 1;
                $EnumSwitchMapping$0[V60.Ai.LOW_POWER.ordinal()] = 2;
                $EnumSwitchMapping$0[V60.Ai.DISABLE.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HeartRateMode consume(V60.Ai ai) {
            Wg6.c(ai, "sdkHeartRateMode");
            int i = WhenMappings.$EnumSwitchMapping$0[ai.ordinal()];
            if (i == 1) {
                return HeartRateMode.CONTINUOUS;
            }
            if (i == 2) {
                return HeartRateMode.LOW_POWER;
            }
            if (i == 3) {
                return HeartRateMode.DISABLE;
            }
            throw new Kc6();
        }

        @DexIgnore
        public final HeartRateMode fromValue(int i) {
            HeartRateMode heartRateMode;
            HeartRateMode[] values = HeartRateMode.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    heartRateMode = null;
                    break;
                }
                heartRateMode = values[i2];
                if (heartRateMode.getValue() == i) {
                    break;
                }
                i2++;
            }
            return heartRateMode != null ? heartRateMode : HeartRateMode.NONE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[HeartRateMode.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[HeartRateMode.CONTINUOUS.ordinal()] = 1;
            $EnumSwitchMapping$0[HeartRateMode.LOW_POWER.ordinal()] = 2;
            $EnumSwitchMapping$0[HeartRateMode.DISABLE.ordinal()] = 3;
            $EnumSwitchMapping$0[HeartRateMode.NONE.ordinal()] = 4;
        }
        */
    }

    @DexIgnore
    public HeartRateMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final V60.Ai toSDKHeartRateMode() {
        int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
        if (i == 1) {
            return V60.Ai.CONTINUOUS;
        }
        if (i == 2) {
            return V60.Ai.LOW_POWER;
        }
        if (i == 3) {
            return V60.Ai.DISABLE;
        }
        if (i == 4) {
            return V60.Ai.DISABLE;
        }
        throw new Kc6();
    }
}
