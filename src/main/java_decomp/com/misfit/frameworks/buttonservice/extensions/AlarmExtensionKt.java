package com.misfit.frameworks.buttonservice.extensions;

import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Z40;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmExtensionKt {
    @DexIgnore
    public static final AlarmSetting.AlarmDay convertCalendarDayToBleDay(int i) {
        switch (i) {
            case 1:
                return AlarmSetting.AlarmDay.SUNDAY;
            case 2:
                return AlarmSetting.AlarmDay.MONDAY;
            case 3:
                return AlarmSetting.AlarmDay.TUESDAY;
            case 4:
                return AlarmSetting.AlarmDay.WEDNESDAY;
            case 5:
                return AlarmSetting.AlarmDay.THURSDAY;
            case 6:
                return AlarmSetting.AlarmDay.FRIDAY;
            case 7:
                return AlarmSetting.AlarmDay.SATURDAY;
            default:
                FLogger.INSTANCE.getLocal().e("AlarmExtension", "Calendar day isn't correct");
                return AlarmSetting.AlarmDay.MONDAY;
        }
    }

    @DexIgnore
    public static final boolean isSame(List<AlarmSetting> list, List<AlarmSetting> list2) {
        boolean z = true;
        boolean z2 = list == null || list.isEmpty();
        boolean z3 = list2 == null || list2.isEmpty();
        if (z2 || z3) {
            return z2 && z3;
        }
        if (list != null) {
            int size = list.size();
            if (list2 == null) {
                Wg6.i();
                throw null;
            } else if (size != list2.size()) {
                return false;
            } else {
                ArrayList arrayList = new ArrayList(list);
                ArrayList arrayList2 = new ArrayList(list2);
                sortMultiAlarmSettingList(arrayList);
                sortMultiAlarmSettingList(arrayList2);
                int size2 = arrayList.size();
                for (int i = 0; i < size2; i++) {
                    Object obj = arrayList.get(i);
                    Wg6.b(obj, "tempSetting1[i]");
                    Object obj2 = arrayList2.get(i);
                    Wg6.b(obj2, "tempSetting2[i]");
                    z = Wg6.a(((AlarmSetting) obj).toString(), ((AlarmSetting) obj2).toString());
                    if (!z) {
                        return z;
                    }
                }
                return z;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public static final void sortMultiAlarmSettingList(List<AlarmSetting> list) {
        Collections.sort(list, AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1.INSTANCE);
    }

    @DexIgnore
    public static final ArrayList<AlarmSetting> toBleAlarmSettings(List<? extends Alarm> list) {
        Wg6.c(list, "$this$toBleAlarmSettings");
        ArrayList<AlarmSetting> arrayList = new ArrayList<>();
        for (Alarm alarm : list) {
            int[] days = alarm.getDays();
            if ((days != null ? days.length : 0) == 0) {
                int alarmMinute = alarm.getAlarmMinute() / 60;
                int alarmMinute2 = alarm.getAlarmMinute();
                String alarmTitle = alarm.getAlarmTitle();
                Wg6.b(alarmTitle, "alarm.alarmTitle");
                String alarmMessage = alarm.getAlarmMessage();
                Wg6.b(alarmMessage, "alarm.alarmMessage");
                arrayList.add(new AlarmSetting(alarmTitle, alarmMessage, alarmMinute, alarmMinute2 - (alarmMinute * 60)));
            } else {
                HashSet hashSet = new HashSet();
                if (days != null) {
                    for (int i : days) {
                        hashSet.add(convertCalendarDayToBleDay(i));
                    }
                    int alarmMinute3 = alarm.getAlarmMinute() / 60;
                    arrayList.add(new AlarmSetting(alarm.getAlarmTitle(), alarm.getAlarmMessage(), alarmMinute3, alarm.getAlarmMinute() - (alarmMinute3 * 60), alarm.isRepeat(), hashSet));
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final Z40[] toSDKV2Setting(List<AlarmSetting> list) {
        Wg6.c(list, "$this$toSDKV2Setting");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Z40 sDKV2Setting = it.next().toSDKV2Setting();
            if (sDKV2Setting != null) {
                arrayList.add(sDKV2Setting);
            }
        }
        Object[] array = arrayList.toArray(new Z40[0]);
        if (array != null) {
            return (Z40[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
