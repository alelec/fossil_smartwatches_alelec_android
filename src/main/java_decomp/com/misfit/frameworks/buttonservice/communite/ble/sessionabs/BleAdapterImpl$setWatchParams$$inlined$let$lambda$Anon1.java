package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1 extends Qq7 implements Hg6<Cd6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ WatchParamsFileMapping $watchParamFileMapping$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, WatchParamsFileMapping watchParamsFileMapping, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$watchParamFileMapping$inlined = watchParamsFileMapping;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Cd6 cd6) {
        invoke(cd6);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Cd6 cd6) {
        Wg6.c(cd6, "it");
        this.this$0.log(this.$logSession$inlined, "Set WatchParam Success");
        this.$callback$inlined.onSetWatchParamsSuccess();
    }
}
