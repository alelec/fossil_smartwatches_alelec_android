package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Us1;
import com.fossil.Yx1;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Nd0;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoMappingsSession extends SetAutoSettingsSession {
    @DexIgnore
    public List<? extends MicroAppMapping> mappings;
    @DexIgnore
    public List<? extends MicroAppMapping> oldMappings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoMappingState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneSetAutoMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoMappingsSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class WatchSetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public Yb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public WatchSetMicroAppMappingState() {
            super(SetAutoMappingsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppFail(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(SetAutoMappingsSession.this.getContext(), SetAutoMappingsSession.this.getSerial())) {
                SetAutoMappingsSession.this.log("Reach the limit retry. Stop.");
                SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
                setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, true);
                SetAutoMappingsSession.this.stop(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppSuccess() {
            SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
            setAutoMappingsSession.storeMappings(setAutoMappingsSession.mappings, false);
            SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
            setAutoMappingsSession2.enterState(setAutoMappingsSession2.createConcreteState((SetAutoMappingsSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (!SetAutoMappingsSession.this.mappings.isEmpty()) {
                BleAdapterImpl bleAdapter = SetAutoMappingsSession.this.getBleAdapter();
                FLogger.Session logSession = SetAutoMappingsSession.this.getLogSession();
                List<Nd0> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(SetAutoMappingsSession.this.mappings);
                Wg6.b(convertToSDKMapping, "MicroAppMapping.convertToSDKMapping(mappings)");
                Yb0<Cd6> configureMicroApp = bleAdapter.configureMicroApp(logSession, convertToSDKMapping, this);
                this.task = configureMicroApp;
                if (configureMicroApp == null) {
                    SetAutoMappingsSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetAutoMappingsSession setAutoMappingsSession = SetAutoMappingsSession.this;
            setAutoMappingsSession.log("Micro app mapping is empty, size=" + SetAutoMappingsSession.this.mappings.size());
            SetAutoMappingsSession setAutoMappingsSession2 = SetAutoMappingsSession.this;
            setAutoMappingsSession2.enterStateAsync(setAutoMappingsSession2.createConcreteState((SetAutoMappingsSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Yb0<Cd6> yb0 = this.task;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoMappingsSession(List<? extends BLEMapping> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_MAPPING, bleAdapterImpl, bleSessionCallback);
        Wg6.c(list, "microAppMappings");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(list);
        Wg6.b(convertToMicroAppMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
        this.mappings = convertToMicroAppMapping;
    }

    @DexIgnore
    private final void storeMappings(List<? extends MicroAppMapping> list, boolean z) {
        DevicePreferenceUtils.setAutoSetMapping(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().t(list));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        List<BLEMapping> convertToBLEMapping = MicroAppMapping.convertToBLEMapping(this.mappings);
        Wg6.b(convertToBLEMapping, "MicroAppMapping.convertToBLEMapping(mappings)");
        SetAutoMappingsSession setAutoMappingsSession = new SetAutoMappingsSession(convertToBLEMapping, getBleAdapter(), getBleSessionCallback());
        setAutoMappingsSession.setDevice(getDevice());
        return setAutoMappingsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        if (getBleAdapter().isDeviceReady()) {
            List<? extends MicroAppMapping> list = this.oldMappings;
            if (list == null) {
                Wg6.n("oldMappings");
                throw null;
            } else if (BLEMapping.isMappingTheSame(list, this.mappings)) {
                log("New mappings and old mappings are the same. No need to set again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                List<? extends MicroAppMapping> list2 = this.mappings;
                List<? extends MicroAppMapping> list3 = this.oldMappings;
                if (list3 == null) {
                    Wg6.n("oldMappings");
                    throw null;
                } else if (BLEMapping.compareTimeStamp(list2, list3).longValue() > 0) {
                    storeMappings(this.mappings, true);
                    return createConcreteState(BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE);
                } else {
                    log("Old mappings timestamp is greater then the new one. No need to set again.");
                    return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                }
            }
        } else {
            storeMappings(this.mappings, true);
            log("Device is not ready. Cannot set mapping.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getBleAdapter().getContext(), getBleAdapter().getSerial()));
        Wg6.b(convertToMicroAppMapping, "MicroAppMapping.convertT\u2026text, bleAdapter.serial))");
        this.oldMappings = convertToMicroAppMapping;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE;
        String name = WatchSetMicroAppMappingState.class.getName();
        Wg6.b(name, "WatchSetMicroAppMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoMappingState.class.getName();
        Wg6.b(name2, "DoneSetAutoMappingState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }
}
