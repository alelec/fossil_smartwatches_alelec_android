package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.Zm1;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.R60;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1 extends Qq7 implements Hg6<Zm1[], Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ R60[] $deviceConfigItems$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, R60[] r60Arr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$deviceConfigItems$inlined = r60Arr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Zm1[] zm1Arr) {
        invoke(zm1Arr);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Zm1[] zm1Arr) {
        Wg6.c(zm1Arr, "it");
        this.this$0.log(this.$logSession$inlined, "Set Device Config Success");
        this.$callback$inlined.onSetDeviceConfigSuccess();
    }
}
