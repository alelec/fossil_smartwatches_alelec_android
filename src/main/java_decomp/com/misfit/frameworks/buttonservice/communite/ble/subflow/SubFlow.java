package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Uk1;
import com.fossil.Vt7;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.fossil.Zm1;
import com.fossil.fitness.FitnessData;
import com.mapped.C90;
import com.mapped.Q40;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import java.lang.reflect.Constructor;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SubFlow extends BleStateAbs implements ISessionSdkCallback {
    @DexIgnore
    public /* final */ BleAdapterImpl bleAdapter;
    @DexIgnore
    public /* final */ BleSession bleSession;
    @DexIgnore
    public /* final */ FLogger.Session logSession;
    @DexIgnore
    public BleStateAbs mCurrentState; // = new NullBleState(getTAG());
    @DexIgnore
    public /* final */ MFLog mfLog;
    @DexIgnore
    public /* final */ String serial;
    @DexIgnore
    public HashMap<SessionState, String> sessionStateMap;

    @DexIgnore
    public enum SessionState {
        SCANNING_STATE,
        ENABLE_MAINTAINING_CONNECTION_STATE,
        FETCH_DEVICE_INFO_STATE,
        GET_DEVICE_CONFIG_STATE,
        PLAY_DEVICE_ANIMATION_STATE,
        ERASE_DATA_FILE_STATE,
        DONE_STATE,
        OTA_STATE,
        SET_DEVICE_CONFIG_STATE,
        SET_WATCH_PARAMS,
        READ_OR_ERASE_STATE,
        READ_DATA_FILE_STATE,
        PROCESS_AND_STORE_DATA_STATE,
        SET_COMPLICATIONS_STATE,
        SET_WATCH_APPS_STATE,
        SET_WATCH_APP_FILES,
        SET_BACKGROUND_IMAGE_CONFIG_STATE,
        SET_LOCALIZATION_STATE,
        SET_THEME_STATE,
        SET_MICRO_APP_MAPPING,
        CLOSE_CONNECTION_STATE,
        REQUEST_HAND_CONTROL_STATE,
        RESET_HANDS_STATE,
        MOVE_HAND_STATE,
        APPLY_HAND_STATE,
        RELEASE_HAND_CONTROL_STATE,
        SET_STEP_GOAL_STATE,
        READ_RSSI_STATE,
        READ_REAL_TIME_STEPS_STATE,
        UPDATE_CURRENT_TIME_STATE,
        GET_BATTERY_LEVEL_STATE,
        SET_VIBRATION_STRENGTH_STATE,
        GET_VIBRATION_STRENGTH_STATE,
        SET_LIST_ALARMS_STATE,
        SET_BIOMETRIC_DATA_STATE,
        SET_SETTING_DONE_STATE,
        SET_HEART_RATE_MODE_STATE,
        SET_FRONT_LIGHT_ENABLE_STATE,
        READ_CURRENT_WORKOUT_STATE,
        STOP_CURRENT_WORKOUT_STATE,
        SET_NOTIFICATION_FILTERS_STATE,
        VERIFY_SECRET_KEY,
        GENERATE_RANDOM_KEY,
        GET_SECRET_KEY_THROUGH_SDK,
        AUTHENTICATE_DEVICE,
        EXCHANGE_SECRET_KEY,
        PUSH_SECRET_KEY_TO_CLOUD,
        SET_SECRET_KEY_TO_DEVICE,
        PROCESS_MAPPING,
        SET_MICRO_APP_MAPPING_AFTER_OTA_STATE,
        SET_REPLY_MESSAGE_MAPPING_STATE,
        PROCESS_HID
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow(String str, BleSession bleSession2, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl) {
        super(str);
        Wg6.c(str, "tagName");
        Wg6.c(bleSession2, "bleSession");
        Wg6.c(session, "logSession");
        Wg6.c(str2, "serial");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.bleSession = bleSession2;
        this.mfLog = mFLog;
        this.logSession = session;
        this.serial = str2;
        this.bleAdapter = bleAdapterImpl;
        initMap();
    }

    @DexIgnore
    private final boolean enterSubState(BleStateAbs bleStateAbs) {
        if (!isExist()) {
            return false;
        }
        if (!BleState.Companion.isNull(this.mCurrentState) && !BleState.Companion.isNull(bleStateAbs) && Vt7.j(this.mCurrentState.getClass().getName(), bleStateAbs.getClass().getName(), true)) {
            return true;
        }
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.onExit();
        }
        this.mCurrentState = bleStateAbs;
        if (BleState.Companion.isNull(bleStateAbs)) {
            return false;
        }
        boolean onEnter = bleStateAbs.onEnter();
        if (onEnter) {
            return onEnter;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, "Failed to enter state: " + bleStateAbs);
        this.mCurrentState = new NullBleState(getTAG());
        return onEnter;
    }

    @DexIgnore
    private final void initMap() {
        this.sessionStateMap = new HashMap<>();
        initStateMap();
    }

    @DexIgnore
    public final void addFailureCode(int i) {
        this.bleSession.addFailureCode(i);
    }

    @DexIgnore
    public final BleStateAbs createConcreteState(SessionState sessionState) {
        BleStateAbs bleStateAbs;
        Wg6.c(sessionState, "state");
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            String str = hashMap.get(sessionState);
            if (str != null) {
                try {
                    Class<?> cls = Class.forName(str);
                    Wg6.b(cls, "Class.forName(stateClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(cls.getDeclaringClass());
                    Wg6.b(declaredConstructor, "innerClass.getDeclaredConstructor(parentClass)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(this);
                    if (newInstance != null) {
                        bleStateAbs = (BleStateAbs) newInstance;
                        return bleStateAbs != null ? bleStateAbs : new NullBleState(getTAG());
                    }
                    throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = getTAG();
                    local.e(tag, "Inside getState method, cannot instance state " + str + ", e = " + e);
                    bleStateAbs = null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("sessionStateMap");
            throw null;
        }
    }

    @DexIgnore
    public final Rm6 enterSubStateAsync(BleStateAbs bleStateAbs) {
        Wg6.c(bleStateAbs, "newState");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new SubFlow$enterSubStateAsync$Anon1(this, bleStateAbs, null), 3, null);
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, Yx1 yx1) {
        Wg6.c(str, "message");
        Wg6.c(step, "step");
        Wg6.c(yx1, "sdkError");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final void errorLog(String str, FLogger.Component component, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        Wg6.c(str, "message");
        Wg6.c(component, "component");
        Wg6.c(step, "step");
        Wg6.c(appError, "error");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.APP, appError);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(component, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapter() {
        return this.bleAdapter;
    }

    @DexIgnore
    public final BleSession getBleSession() {
        return this.bleSession;
    }

    @DexIgnore
    public final FLogger.Session getLogSession() {
        return this.logSession;
    }

    @DexIgnore
    public final BleStateAbs getMCurrentState() {
        return this.mCurrentState;
    }

    @DexIgnore
    public final MFLog getMfLog() {
        return this.mfLog;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final HashMap<SessionState, String> getSessionStateMap() {
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            return hashMap;
        }
        Wg6.n("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public abstract void initStateMap();

    @DexIgnore
    public void log(String str) {
        Wg6.c(str, "message");
        FLogger.INSTANCE.getLocal().d(getTAG(), str);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str);
        }
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onApplyHandPositionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onDeviceFound(Q40 q40, int i) {
        Wg6.c(q40, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseDataFilesFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseHWLogFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public void onExit() {
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.stopTimeout();
            enterSubState(new NullBleState(getTAG()));
        }
        super.onExit();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onFetchDeviceInfoFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onFetchDeviceInfoSuccess(Zk1 zk1) {
        Wg6.c(zk1, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onGetDeviceConfigFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
        Wg6.c(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onMoveHandFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onNotifyNotificationEventFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onPlayDeviceAnimation(boolean z, Yx1 yx1) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadCurrentWorkoutSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadCurrentWorkoutSessionSuccess(C90 c90) {
        Wg6.c(c90, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        Wg6.c(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadRssiFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReleaseHandControlFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onRequestHandControlFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onResetHandsFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onScanFail(Uk1 uk1) {
        Wg6.c(uk1, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetAlarmFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetBackgroundImageFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetComplicationFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetDeviceConfigFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetFrontLightFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppFileProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public abstract void onStop(int i);

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onStopCurrentWorkoutSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onVerifySecretKeyFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final void setMCurrentState(BleStateAbs bleStateAbs) {
        Wg6.c(bleStateAbs, "<set-?>");
        this.mCurrentState = bleStateAbs;
    }

    @DexIgnore
    public final void setSessionStateMap(HashMap<SessionState, String> hashMap) {
        Wg6.c(hashMap, "<set-?>");
        this.sessionStateMap = hashMap;
    }

    @DexIgnore
    public final void stopSubFlow(int i) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new SubFlow$stopSubFlow$Anon1(this, i, null), 3, null);
    }
}
