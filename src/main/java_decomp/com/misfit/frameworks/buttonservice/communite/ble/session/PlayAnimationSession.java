package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PlayAnimationSession extends EnableMaintainingSession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PlayDeviceAnimationState() {
            super(PlayAnimationSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Cd6> playDeviceAnimation = PlayAnimationSession.this.getBleAdapter().playDeviceAnimation(PlayAnimationSession.this.getLogSession(), this);
            this.task = playDeviceAnimation;
            if (playDeviceAnimation == null) {
                PlayAnimationSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPlayDeviceAnimation(boolean z, Yx1 yx1) {
            stopTimeout();
            if (z) {
                PlayAnimationSession.this.stop(0);
            } else {
                PlayAnimationSession.this.stop(201);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PlayAnimationSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.PLAY_ANIMATION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        PlayAnimationSession playAnimationSession = new PlayAnimationSession(getBleAdapter(), getBleSessionCallback());
        playAnimationSession.setDevice(getDevice());
        return playAnimationSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.PLAY_DEVICE_ANIMATION_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        Wg6.b(name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
