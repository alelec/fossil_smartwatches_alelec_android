package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetNotificationFiltersConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ AppNotificationFilterSettings mNotificationFilterSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilters extends BleStateAbs {
        @DexIgnore
        public Yb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetNotificationFilters() {
            super(SetNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Yb0<Cd6> notificationFilters = SetNotificationFiltersConfigSession.this.getBleAdapter().setNotificationFilters(SetNotificationFiltersConfigSession.this.getLogSession(), SetNotificationFiltersConfigSession.this.mNotificationFilterSettings.getNotificationFilters(), this);
            this.task = notificationFilters;
            if (notificationFilters == null) {
                SetNotificationFiltersConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(SetNotificationFiltersConfigSession.this.getContext(), SetNotificationFiltersConfigSession.this.getSerial())) {
                SetNotificationFiltersConfigSession.this.log("Reach the limit retry. Stop.");
                SetNotificationFiltersConfigSession.this.stop(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoNotificationFiltersConfig(SetNotificationFiltersConfigSession.this.getBleAdapter().getContext(), SetNotificationFiltersConfigSession.this.getBleAdapter().getSerial(), SetNotificationFiltersConfigSession.this.mNotificationFilterSettings);
            SetNotificationFiltersConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Yb0<Cd6> yb0 = this.task;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFiltersConfigSession(AppNotificationFilterSettings appNotificationFilterSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_NOTIFICATION_FILTERS, bleAdapterImpl, bleSessionCallback);
        Wg6.c(appNotificationFilterSettings, "mNotificationFilterSettings");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.mNotificationFilterSettings = appNotificationFilterSettings;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetNotificationFiltersConfigSession setNotificationFiltersConfigSession = new SetNotificationFiltersConfigSession(this.mNotificationFilterSettings, getBleAdapter(), getBleSessionCallback());
        setNotificationFiltersConfigSession.setDevice(getDevice());
        return setNotificationFiltersConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name = SetNotificationFilters.class.getName();
        Wg6.b(name, "SetNotificationFilters::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
