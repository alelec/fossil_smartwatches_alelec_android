package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.Bl1;
import com.fossil.Ou1;
import com.fossil.Ry1;
import com.fossil.Us1;
import com.fossil.Vt7;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Rc6;
import com.mapped.U40;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseExchangeSecretKeySubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseOTASubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.pairing.LabelResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FirmwareUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingNewDeviceSession extends BasePairingNewDeviceSession implements IPairDeviceSession, IExchangeKeySession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public ThemeConfig themeConfig;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class AuthorizeDeviceState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public AuthorizeDeviceState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final void authorizeDevice(long j) {
            BleSession.BleSessionCallback bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onNeedStartTimer(PairingNewDeviceSession.this.getSerial());
            }
            Zb0<Cd6> confirmAuthorization = PairingNewDeviceSession.this.getBleAdapter().confirmAuthorization(PairingNewDeviceSession.this.getLogSession(), j, this);
            this.task = confirmAuthorization;
            if (confirmAuthorization == null) {
                PairingNewDeviceSession.this.stop(10000);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }

        @DexIgnore
        public final Zb0<Cd6> getTask() {
            return this.task;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthorizeDeviceFailed() {
            FLogger.INSTANCE.getLocal().e(getTAG(), "onAuthorizeDeviceState() failed");
            PairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_AUTHORIZE_DEVICE);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthorizeDeviceSuccess() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onAuthorizeDeviceState() success");
            BleSession.BleSessionCallback bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onAuthorizeDeviceSuccess(PairingNewDeviceSession.this.getSerial());
            }
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        public final void setTask(Zb0<Cd6> zb0) {
            this.task = zb0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public int failureCode;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public CloseConnectionState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final int getFailureCode() {
            return this.failureCode;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            PairingNewDeviceSession.this.getBleAdapter().closeConnection(PairingNewDeviceSession.this.getLogSession(), true);
            PairingNewDeviceSession.this.stop(this.failureCode);
            return true;
        }

        @DexIgnore
        public final void setFailureCode(int i) {
            this.failureCode = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ExchangeSecretKeySubFlow() {
            super(CommunicateMode.LINK, PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i == U40.REQUEST_UNSUPPORTED.getCode() || i == U40.UNSUPPORTED_FORMAT.getCode() || i == 0) {
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.SET_LABEL_STATE));
                return;
            }
            BleState createConcreteState = PairingNewDeviceSession.this.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class LinkServerState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public LinkServerState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            BleSession.BleSessionCallback bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            CommunicateMode communicateMode = CommunicateMode.LINK;
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(PairingNewDeviceSession.this.getBleAdapter()));
            bundle.putBoolean(Constants.IS_JUST_OTA, PairingNewDeviceSession.this.isJustUpdateFW());
            bleSessionCallback.onAskForLinkServer(communicateMode, bundle);
            return true;
        }

        @DexIgnore
        public final void onLinkServerCompleted(boolean z, int i) {
            if (z) {
                PairingNewDeviceSession.this.stop(0);
                return;
            }
            BleState createConcreteState = PairingNewDeviceSession.this.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTASubFlow extends BaseOTASubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public OTASubFlow() {
            /*
                r11 = this;
                r0 = 0
                com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.this = r12
                java.lang.String r1 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getTAG$p(r12)
                com.misfit.frameworks.buttonservice.log.MFLog r3 = r12.getMfLog()
                com.misfit.frameworks.buttonservice.log.FLogger$Session r4 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getLogSession$p(r12)
                java.lang.String r5 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getSerial$p(r12)
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r6 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getBleAdapter$p(r12)
                com.misfit.frameworks.buttonservice.model.FirmwareData r7 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getFirmwareData$p(r12)
                if (r7 == 0) goto L_0x0035
                byte[] r8 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getFirmwareBytes$p(r12)
                if (r8 == 0) goto L_0x0031
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r9 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getBleSessionCallback$p(r12)
                com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator$CommunicationResultCallback r10 = r12.getCommunicationResultCallback()
                r0 = r11
                r2 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
                return
            L_0x0031:
                com.mapped.Wg6.i()
                throw r0
            L_0x0035:
                com.mapped.Wg6.i()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.OTASubFlow.<init>(com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession):void");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i == 0) {
                PairingNewDeviceSession.this.setJustUpdateFW(true);
                BleSession.BleSessionCallback bleSessionCallback = getBleSessionCallback();
                if (bleSessionCallback != null) {
                    bleSessionCallback.onUpdateFirmwareSuccess();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.AUTHORIZE_DEVICE));
                return;
            }
            BleSession.BleSessionCallback bleSessionCallback2 = getBleSessionCallback();
            if (bleSessionCallback2 != null) {
                bleSessionCallback2.onUpdateFirmwareFailed();
            }
            addFailureCode(FailureCode.FAILED_TO_OTA);
            BleState createConcreteState = PairingNewDeviceSession.this.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE);
            if (createConcreteState instanceof PairingCheckFirmware) {
                ((PairingCheckFirmware) createConcreteState).setFromOTAFailed(true);
            }
            PairingNewDeviceSession.this.enterStateAsync(createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PairingCheckFirmware extends BleStateAbs {
        @DexIgnore
        public boolean isFromOTAFailed;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PairingCheckFirmware() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final boolean isFromOTAFailed() {
            return this.isFromOTAFailed;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            BleSession.BleSessionCallback bleSessionCallback;
            super.onEnter();
            if (this.isFromOTAFailed || (bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback()) == null) {
                return true;
            }
            Bundle bundle = new Bundle();
            bundle.putString("device_model", PairingNewDeviceSession.this.getBleAdapter().getDeviceModel());
            bleSessionCallback.onRequestLatestFirmware(bundle);
            return true;
        }

        @DexIgnore
        public final void onReceiveLatestFirmwareData(FirmwareData firmwareData) {
            Wg6.c(firmwareData, "firmwareData");
            if ((firmwareData instanceof SkipFirmwareData) || Vt7.j(PairingNewDeviceSession.this.getBleAdapter().getFirmwareVersion(), firmwareData.getFirmwareVersion(), true)) {
                BleSession.BleSessionCallback bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback();
                if (bleSessionCallback != null) {
                    bleSessionCallback.onFirmwareLatest();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.AUTHORIZE_DEVICE));
                return;
            }
            String firmwareVersion = firmwareData.getFirmwareVersion();
            String checkSum = firmwareData.getCheckSum();
            boolean isEmbedded = firmwareData.isEmbedded();
            byte[] readFirmware = FirmwareUtils.INSTANCE.readFirmware(firmwareData, PairingNewDeviceSession.this.getContext());
            PairingNewDeviceSession.this.log("Verifying firmware...");
            PairingNewDeviceSession pairingNewDeviceSession2 = PairingNewDeviceSession.this;
            pairingNewDeviceSession2.log("- Version: " + firmwareVersion);
            PairingNewDeviceSession pairingNewDeviceSession3 = PairingNewDeviceSession.this;
            pairingNewDeviceSession3.log("- Checksum: " + checkSum);
            PairingNewDeviceSession pairingNewDeviceSession4 = PairingNewDeviceSession.this;
            pairingNewDeviceSession4.log("- Length: " + readFirmware.length);
            PairingNewDeviceSession pairingNewDeviceSession5 = PairingNewDeviceSession.this;
            pairingNewDeviceSession5.log("- In-app Bundled: " + isEmbedded);
            if (isEmbedded) {
                PairingNewDeviceSession.this.log("In-app bundled firmware, skip verifying!");
            } else if (FirmwareUtils.INSTANCE.verifyFirmware(readFirmware, checkSum)) {
                PairingNewDeviceSession.this.log("Verified: OK");
            } else {
                PairingNewDeviceSession.this.log("Verified: FAILED. OTA: FAILED");
                PairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_OTA_FILE_NOT_READY);
            }
            PairingNewDeviceSession.this.setFirmwareData(firmwareData);
            PairingNewDeviceSession.this.setFirmwareBytes(readFirmware);
            PairingNewDeviceSession pairingNewDeviceSession6 = PairingNewDeviceSession.this;
            pairingNewDeviceSession6.enterStateAsync(pairingNewDeviceSession6.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.OTA_STATE));
        }

        @DexIgnore
        public final void setFromOTAFailed(boolean z) {
            this.isFromOTAFailed = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLabelState extends BleStateAbs {
        @DexIgnore
        public Yb0<Ry1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetLabelState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        private final void closeSession(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "closeSession, errorCode = " + i);
            PairingNewDeviceSession.this.stop(i);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0042, code lost:
            com.fossil.So7.a(r2, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0045, code lost:
            throw r1;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.fossil.Ou1 getLabelFile() {
            /*
                r6 = this;
                r1 = 0
                r0 = 1
                r2 = 0
                java.io.File r3 = new java.io.File
                com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession r4 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.this
                android.content.Context r4 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.access$getContext$p(r4)
                com.misfit.frameworks.buttonservice.model.FileType r5 = com.misfit.frameworks.buttonservice.model.FileType.LABEL
                java.lang.String r4 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r4, r5)
                r3.<init>(r4)
                java.io.File[] r4 = r3.listFiles()
                if (r4 == 0) goto L_0x0020
                int r3 = r4.length
                if (r3 != 0) goto L_0x003b
                r3 = r0
            L_0x001e:
                if (r3 == 0) goto L_0x003d
            L_0x0020:
                if (r0 != 0) goto L_0x0046
                r0 = r4[r2]
                java.lang.String r2 = "files[0]"
                com.mapped.Wg6.b(r0, r2)
                java.io.FileInputStream r2 = new java.io.FileInputStream
                r2.<init>(r0)
                com.fossil.Ou1 r0 = new com.fossil.Ou1     // Catch:{ all -> 0x003f }
                byte[] r3 = com.fossil.Ro7.c(r2)     // Catch:{ all -> 0x003f }
                r0.<init>(r3)     // Catch:{ all -> 0x003f }
                com.fossil.So7.a(r2, r1)
            L_0x003a:
                return r0
            L_0x003b:
                r3 = r2
                goto L_0x001e
            L_0x003d:
                r0 = r2
                goto L_0x0020
            L_0x003f:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0041 }
            L_0x0041:
                r1 = move-exception
                com.fossil.So7.a(r2, r0)
                throw r1
            L_0x0046:
                r0 = r1
                goto L_0x003a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.SetLabelState.getLabelFile():com.fossil.Ou1");
        }

        @DexIgnore
        private final void skipStepForNotSupportedDevice() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "skipStepForNotSupportedDevice, FEATURE_IS_NOT_SUPPORTED");
            PairingNewDeviceSession.this.addFailureCode(10000);
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (PairingNewDeviceSession.this.getBleSessionCallback() != null) {
                BleSession.BleSessionCallback bleSessionCallback = PairingNewDeviceSession.this.getBleSessionCallback();
                if (bleSessionCallback != null) {
                    bleSessionCallback.onAskForLabelFile(PairingNewDeviceSession.this.getSerial(), CommunicateMode.LINK);
                    startTimeout();
                    return true;
                }
                Wg6.i();
                throw null;
            }
            closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLabelFileFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.log("onSetLabelFileFailed, error = " + yx1);
            stopTimeout();
            if (((yx1 instanceof Bl1) && ((Bl1) yx1).getErrorCode() == U40.REQUEST_UNSUPPORTED) || yx1.getErrorCode() == U40.UNSUPPORTED_FORMAT) {
                skipStepForNotSupportedDevice();
            } else if (!retry(PairingNewDeviceSession.this.getContext(), PairingNewDeviceSession.this.getSerial())) {
                PairingNewDeviceSession.this.log("Reach the limit retry. Stop.");
                closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLabelFileSuccess() {
            stopTimeout();
            PairingNewDeviceSession.this.log("onSetLabelFileSuccess");
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            PairingNewDeviceSession.this.log("onSetLabelFile Timeout");
            Yb0<Ry1> yb0 = this.task;
            if (yb0 == null) {
                closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            } else if (yb0 != null) {
                Us1.a(yb0);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final void setLabelToDevice(LabelResponse labelResponse) {
            Wg6.c(labelResponse, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "setLabelToDevice, response = " + labelResponse + " for currentVersion = " + PairingNewDeviceSession.this.getBleAdapter().getCurrentLabelVersion());
            int code = labelResponse.getCode();
            if (code != 0) {
                if (code != 10000) {
                    closeSession(labelResponse.getCode());
                } else {
                    skipStepForNotSupportedDevice();
                }
            } else if (Wg6.a(labelResponse.getVersion(), "0.0") || (!Wg6.a(labelResponse.getVersion(), PairingNewDeviceSession.this.getBleAdapter().getCurrentLabelVersion()))) {
                Ou1 labelFile = getLabelFile();
                if (labelFile != null) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "setLabelToDevice, start install label");
                    Yb0<Ry1> labelFile2 = PairingNewDeviceSession.this.getBleAdapter().setLabelFile(PairingNewDeviceSession.this.getLogSession(), labelFile, this);
                    this.task = labelFile2;
                    if (labelFile2 == null) {
                        skipStepForNotSupportedDevice();
                    } else {
                        startTimeout();
                    }
                } else {
                    closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
                }
            } else {
                FLogger.INSTANCE.getLocal().d(getTAG(), "setLabelToDevice, label is up-to-date, skip this step");
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferDataSubFlow() {
            super(PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getUserProfile(), PairingNewDeviceSession.this.getBleSessionCallback(), true);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i != 0) {
                PairingNewDeviceSession.this.stop(i);
                return;
            }
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferSettingsSubFlow() {
            super(PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), true, PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getUserProfile(), PairingNewDeviceSession.this.multiAlarmSettings, PairingNewDeviceSession.this.complicationAppMappingSettings, PairingNewDeviceSession.this.watchAppMappingSettings, PairingNewDeviceSession.this.backgroundConfig, PairingNewDeviceSession.this.themeConfig, PairingNewDeviceSession.this.notificationFilterSettings, PairingNewDeviceSession.this.localizationData, PairingNewDeviceSession.this.microAppMappings, PairingNewDeviceSession.this.secondTimezoneOffset, PairingNewDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState((PairingNewDeviceSession) BleSessionAbs.SessionState.LINK_SERVER));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingNewDeviceSession(UserProfile userProfile, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(userProfile, bleAdapterImpl, bleSessionCallback, communicationResultCallback);
        Wg6.c(userProfile, "userProfile");
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.PAIR);
        userProfile.setNewDevice(true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        PairingNewDeviceSession pairingNewDeviceSession = new PairingNewDeviceSession(getUserProfile(), getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback());
        pairingNewDeviceSession.setDevice(getDevice());
        return pairingNewDeviceSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
        this.themeConfig = DevicePreferenceUtils.getThemeConfig(getContext(), getSerial());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW;
        String name = ExchangeSecretKeySubFlow.class.getName();
        Wg6.b(name, "ExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_LABEL_STATE;
        String name2 = SetLabelState.class.getName();
        Wg6.b(name2, "SetLabelState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE;
        String name3 = PairingCheckFirmware.class.getName();
        Wg6.b(name3, "PairingCheckFirmware::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.OTA_STATE;
        String name4 = OTASubFlow.class.getName();
        Wg6.b(name4, "OTASubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.AUTHORIZE_DEVICE;
        String name5 = AuthorizeDeviceState.class.getName();
        Wg6.b(name5, "AuthorizeDeviceState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap6 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState6 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name6 = TransferDataSubFlow.class.getName();
        Wg6.b(name6, "TransferDataSubFlow::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap7 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState7 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name7 = TransferSettingsSubFlow.class.getName();
        Wg6.b(name7, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap8 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState8 = BleSessionAbs.SessionState.LINK_SERVER;
        String name8 = LinkServerState.class.getName();
        Wg6.b(name8, "LinkServerState::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap9 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState9 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name9 = CloseConnectionState.class.getName();
        Wg6.b(name9, "CloseConnectionState::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onAuthorizeDevice(long j) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onAuthorizeDevice(), timeout=" + j);
        BleState currentState = getCurrentState();
        if (currentState instanceof AuthorizeDeviceState) {
            ((AuthorizeDeviceState) currentState).authorizeDevice(j);
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onAuthorizeDevice() failed, can't execute because currentState is not an instance of AuthorizeDeviceState");
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onDownloadLabelResponse(LabelResponse labelResponse) {
        Wg6.c(labelResponse, "response");
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onDownloadLabelResponse response=" + labelResponse + " state=" + currentState);
        if (currentState instanceof SetLabelState) {
            ((SetLabelState) currentState).setLabelToDevice(labelResponse);
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onDownloadLabelResponse, can't set Label because currentState is not an instance of SetLabelState");
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).onGetWatchParamFailed();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onLinkServerSuccess(boolean z, int i) {
        BleState currentState = getCurrentState();
        if (currentState instanceof LinkServerState) {
            ((LinkServerState) currentState).onLinkServerCompleted(z, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onWatchAppFilesReady(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onWatchAppFilesReady, isSuccess = " + z);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setWatchAppFiles(z);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        Wg6.c(str, "serial");
        Wg6.c(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void updateFirmware(FirmwareData firmwareData) {
        Wg6.c(firmwareData, "firmwareData");
        BleState currentState = getCurrentState();
        if (currentState instanceof PairingCheckFirmware) {
            ((PairingCheckFirmware) currentState).onReceiveLatestFirmwareData(firmwareData);
        }
    }
}
