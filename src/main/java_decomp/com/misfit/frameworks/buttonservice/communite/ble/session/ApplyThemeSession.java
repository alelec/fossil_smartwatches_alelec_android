package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ax1;
import com.fossil.Bx1;
import com.fossil.Lw1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApplyThemeSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ ThemeConfig themeConfig;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ApplyThemeState extends BleStateAbs {
        @DexIgnore
        public Zb0<Lw1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ApplyThemeState() {
            super(ApplyThemeSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyThemeError(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (yx1 instanceof Ax1) {
                if (((Ax1) yx1).getErrorCode() == Bx1.NETWORK_UNAVAILABLE) {
                    ApplyThemeSession.this.stop(FailureCode.FAIL_TO_DOWNLOAD_TEMPLATE_NO_INTERNET);
                } else {
                    ApplyThemeSession.this.stop(FailureCode.FAIL_TO_DOWNLOAD_TEMPLATE_SERVER_ERROR);
                }
            } else if (!retry(ApplyThemeSession.this.getContext(), ApplyThemeSession.this.getSerial())) {
                ApplyThemeSession.this.log("Reach the limit retry. Stop.");
                ApplyThemeSession.this.stop(FailureCode.FAILED_TO_APPLY_THEME);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyThemeProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyThemeSuccess(byte[] bArr) {
            Wg6.c(bArr, "themeBinary");
            ApplyThemeSession.this.getExtraInfoReturned().putByteArray(ButtonService.Companion.getTHEME_BINARY_EXTRA(), bArr);
            ApplyThemeSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Lw1> zb0 = null;
            if (ApplyThemeSession.this.themeConfig.getPathThemeData() != null) {
                Lw1 themePackage = ThemeExtentionKt.toThemePackage(ApplyThemeSession.this.themeConfig.getPathThemeData());
                if (themePackage != null) {
                    zb0 = ApplyThemeSession.this.getBleAdapter().applyTheme(ApplyThemeSession.this.getLogSession(), themePackage, this);
                }
            } else if (ApplyThemeSession.this.themeConfig.getThemeData() != null) {
                zb0 = ApplyThemeSession.this.getBleAdapter().applyTheme(ApplyThemeSession.this.getLogSession(), ApplyThemeSession.this.themeConfig.getThemeData(), this);
            }
            this.task = zb0;
            if (zb0 == null) {
                ApplyThemeSession.this.stop(FailureCode.FAILED_TO_APPLY_THEME);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Lw1> zb0 = this.task;
            if (zb0 == null) {
                ApplyThemeSession.this.stop(FailureCode.FAILED_TO_APPLY_THEME);
            } else if (zb0 != null) {
                Us1.a(zb0);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ApplyThemeSession(ThemeConfig themeConfig2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.APPLY_THEME_SESSION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(themeConfig2, "themeConfig");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.themeConfig = themeConfig2;
    }

    @DexIgnore
    private final void storeMappings(ThemeConfig themeConfig2, boolean z) {
        DevicePreferenceUtils.setThemeData(getBleAdapter().getContext(), getBleAdapter().getSerial(), themeConfig2);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.THEME);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ApplyThemeSession applyThemeSession = new ApplyThemeSession(this.themeConfig, getBleAdapter(), getBleSessionCallback());
        applyThemeSession.setDevice(getDevice());
        return applyThemeSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.APPLY_THEME_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initSettings() {
        super.initSettings();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, "initSettings - themeConfig: " + this.themeConfig);
        storeMappings(this.themeConfig, true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.APPLY_THEME_STATE;
        String name = ApplyThemeState.class.getName();
        Wg6.b(name, "ApplyThemeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
