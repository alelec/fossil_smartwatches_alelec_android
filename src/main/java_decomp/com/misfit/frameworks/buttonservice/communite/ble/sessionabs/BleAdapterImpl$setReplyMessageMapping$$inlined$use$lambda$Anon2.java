package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jh6;
import com.mapped.Qb0;
import com.mapped.Wg6;
import com.mapped.Zd0;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Qb0 $replyMessageFeature$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ReplyMessageMappingGroup $replyMessageGroup$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Jh6 $replyMessageIcon$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Zd0[] $replyMessageMapping$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon2(Zd0[] zd0Arr, BleAdapterImpl bleAdapterImpl, Jh6 jh6, ReplyMessageMappingGroup replyMessageMappingGroup, Qb0 qb0, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.$replyMessageMapping$inlined = zd0Arr;
        this.this$0 = bleAdapterImpl;
        this.$replyMessageIcon$inlined = jh6;
        this.$replyMessageGroup$inlined = replyMessageMappingGroup;
        this.$replyMessageFeature$inlined = qb0;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
        invoke(yx1);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Yx1 yx1) {
        Wg6.c(yx1, "error");
        this.this$0.logSdkError(this.$logSession$inlined, "setReplyMessageMapping", ErrorCodeBuilder.Step.SET_REPLY_MESSAGE_MAPPING, yx1);
        this.$callback$inlined.onSetReplyMessageMappingError(yx1);
    }
}
