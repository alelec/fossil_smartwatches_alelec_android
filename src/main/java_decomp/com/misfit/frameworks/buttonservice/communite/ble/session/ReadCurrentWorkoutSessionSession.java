package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.C90;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadCurrentWorkoutSessionSession extends EnableMaintainingSession {
    @DexIgnore
    public C90 mCurrentWorkoutSession;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadCurrentWorkoutSession extends BleStateAbs {
        @DexIgnore
        public Zb0<C90> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadCurrentWorkoutSession() {
            super(ReadCurrentWorkoutSessionSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<C90> readCurrentWorkoutSession = ReadCurrentWorkoutSessionSession.this.getBleAdapter().readCurrentWorkoutSession(ReadCurrentWorkoutSessionSession.this.getLogSession(), this);
            this.task = readCurrentWorkoutSession;
            if (readCurrentWorkoutSession == null) {
                ReadCurrentWorkoutSessionSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadCurrentWorkoutSessionFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadCurrentWorkoutSessionSuccess(C90 c90) {
            Wg6.c(c90, "workoutSession");
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.mCurrentWorkoutSession = c90;
            ReadCurrentWorkoutSessionSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<C90> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadCurrentWorkoutSessionSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(Constants.CURRENT_WORKOUT_SESSION, this.mCurrentWorkoutSession);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ReadCurrentWorkoutSessionSession readCurrentWorkoutSessionSession = new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback());
        readCurrentWorkoutSessionSession.setDevice(getDevice());
        return readCurrentWorkoutSessionSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE;
        String name = ReadCurrentWorkoutSession.class.getName();
        Wg6.b(name, "ReadCurrentWorkoutSession::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
