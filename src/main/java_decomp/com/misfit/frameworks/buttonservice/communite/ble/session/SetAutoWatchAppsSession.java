package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Js1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoWatchAppsSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ WatchAppMappingSettings mNewWatchAppMappingSettings;
    @DexIgnore
    public WatchAppMappingSettings mOldWatchAppMappingSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoWatchAppsSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppsState() {
            super(SetAutoWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Cd6> watchApps = SetAutoWatchAppsSession.this.getBleAdapter().setWatchApps(SetAutoWatchAppsSession.this.getLogSession(), SetAutoWatchAppsSession.this.mNewWatchAppMappingSettings, this);
            this.task = watchApps;
            if (watchApps == null) {
                SetAutoWatchAppsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(SetAutoWatchAppsSession.this.getContext(), SetAutoWatchAppsSession.this.getSerial())) {
                SetAutoWatchAppsSession.this.log("Reach the limit retry. Stop.");
                SetAutoWatchAppsSession setAutoWatchAppsSession = SetAutoWatchAppsSession.this;
                setAutoWatchAppsSession.storeMappings(setAutoWatchAppsSession.mNewWatchAppMappingSettings, true);
                SetAutoWatchAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppSuccess() {
            stopTimeout();
            SetAutoWatchAppsSession setAutoWatchAppsSession = SetAutoWatchAppsSession.this;
            setAutoWatchAppsSession.storeMappings(setAutoWatchAppsSession.mNewWatchAppMappingSettings, false);
            SetAutoWatchAppsSession setAutoWatchAppsSession2 = SetAutoWatchAppsSession.this;
            setAutoWatchAppsSession2.enterStateAsync(setAutoWatchAppsSession2.createConcreteState((SetAutoWatchAppsSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoWatchAppsSession(WatchAppMappingSettings watchAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_WATCH_APPS, bleAdapterImpl, bleSessionCallback);
        Wg6.c(watchAppMappingSettings, "mNewWatchAppMappingSettings");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.mNewWatchAppMappingSettings = watchAppMappingSettings;
        setLogSession(FLogger.Session.SET_WATCH_APPS);
    }

    @DexIgnore
    private final void storeMappings(WatchAppMappingSettings watchAppMappingSettings, boolean z) {
        DevicePreferenceUtils.setAutoWatchAppSettings(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().t(watchAppMappingSettings));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoWatchAppsSession setAutoWatchAppsSession = new SetAutoWatchAppsSession(this.mNewWatchAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setAutoWatchAppsSession.setDevice(getDevice());
        return setAutoWatchAppsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState createConcreteState;
        super.initSettings();
        this.mOldWatchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(Js1.class) == null) {
            log("This device does not support set complication apps.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (WatchAppMappingSettings.Companion.isSettingsSame(this.mOldWatchAppMappingSettings, this.mNewWatchAppMappingSettings)) {
            log("New complication settings and complication settings are the same, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (WatchAppMappingSettings.Companion.compareTimeStamp(this.mNewWatchAppMappingSettings, this.mOldWatchAppMappingSettings) > 0) {
            storeMappings(this.mNewWatchAppMappingSettings, true);
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
        } else {
            log("Old complication settings timestamp is greater than the new one, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(createConcreteState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name = SetWatchAppsState.class.getName();
        Wg6.b(name, "SetWatchAppsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        Wg6.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        Wg6.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
