package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ln1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetHeartRateModeSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ HeartRateMode heartRateMode;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetHeartRateModeState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetHeartRateModeState() {
            super(SetHeartRateModeSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            if (SetHeartRateModeSession.this.heartRateMode != HeartRateMode.NONE) {
                SetHeartRateModeSession setHeartRateModeSession = SetHeartRateModeSession.this;
                setHeartRateModeSession.log("Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
                Ln1 ln1 = new Ln1();
                ln1.o(SetHeartRateModeSession.this.heartRateMode.toSDKHeartRateMode());
                Zb0<Zm1[]> deviceConfig = SetHeartRateModeSession.this.getBleAdapter().setDeviceConfig(SetHeartRateModeSession.this.getLogSession(), ln1.b(), this);
                this.task = deviceConfig;
                if (deviceConfig == null) {
                    SetHeartRateModeSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetHeartRateModeSession setHeartRateModeSession2 = SetHeartRateModeSession.this;
            setHeartRateModeSession2.log("Not Set Heart Rate Mode: " + SetHeartRateModeSession.this.heartRateMode);
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            SetHeartRateModeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetHeartRateModeSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zm1[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetHeartRateModeSession(HeartRateMode heartRateMode2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_HEART_RATE_MODE, bleAdapterImpl, bleSessionCallback);
        Wg6.c(heartRateMode2, "heartRateMode");
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
        this.heartRateMode = heartRateMode2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetHeartRateModeSession setHeartRateModeSession = new SetHeartRateModeSession(this.heartRateMode, getBleAdapter(), getBleSessionCallback());
        setHeartRateModeSession.setDevice(getDevice());
        return setHeartRateModeSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_HEART_RATE_MODE_STATE;
        String name = SetHeartRateModeState.class.getName();
        Wg6.b(name, "SetHeartRateModeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
