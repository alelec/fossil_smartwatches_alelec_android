package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ax1;
import com.fossil.Bx1;
import com.fossil.Lw1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreviewThemeSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ ThemeEditor themeEditor;
    @DexIgnore
    public /* final */ Lw1 themePackage;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PreviewThemeState extends BleStateAbs {
        @DexIgnore
        public Yb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PreviewThemeState() {
            super(PreviewThemeSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Yb0<Cd6> previewTheme = PreviewThemeSession.this.themePackage != null ? PreviewThemeSession.this.getBleAdapter().previewTheme(PreviewThemeSession.this.getLogSession(), PreviewThemeSession.this.themePackage, this) : PreviewThemeSession.this.themeEditor != null ? PreviewThemeSession.this.getBleAdapter().previewTheme(PreviewThemeSession.this.getLogSession(), PreviewThemeSession.this.themeEditor, this) : null;
            this.task = previewTheme;
            if (previewTheme == null) {
                PreviewThemeSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPreviewThemeError(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (yx1 instanceof Ax1) {
                if (((Ax1) yx1).getErrorCode() == Bx1.NETWORK_UNAVAILABLE) {
                    PreviewThemeSession.this.stop(FailureCode.FAIL_TO_DOWNLOAD_TEMPLATE_NO_INTERNET);
                } else {
                    PreviewThemeSession.this.stop(FailureCode.FAIL_TO_DOWNLOAD_TEMPLATE_SERVER_ERROR);
                }
            } else if (!retry(PreviewThemeSession.this.getContext(), PreviewThemeSession.this.getSerial())) {
                PreviewThemeSession.this.log("Reach the limit retry. Stop.");
                PreviewThemeSession.this.stop(FailureCode.FAILED_TO_PREVIEW_THEME);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPreviewThemeProgressChanged(float f) {
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPreviewThemeSuccess() {
            PreviewThemeSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Yb0<Cd6> yb0 = this.task;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PreviewThemeSession(ThemeEditor themeEditor2, Lw1 lw1, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.PREVIEW_THEME_SESSION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.themeEditor = themeEditor2;
        this.themePackage = lw1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        PreviewThemeSession previewThemeSession = new PreviewThemeSession(this.themeEditor, this.themePackage, getBleAdapter(), getBleSessionCallback());
        previewThemeSession.setDevice(getDevice());
        return previewThemeSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.PREVIEW_THEME_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.PREVIEW_THEME_STATE;
        String name = PreviewThemeState.class.getName();
        Wg6.b(name, "PreviewThemeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
