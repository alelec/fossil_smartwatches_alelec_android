package com.misfit.frameworks.buttonservice.communite.ble;

import android.content.Context;
import com.fossil.Ry1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ HeartRateMode heartRateMode; // = HeartRateMode.NONE;
    @DexIgnore
    public String macAddress;
    @DexIgnore
    public /* final */ String serial;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = BleAdapter.class.getSimpleName();
        Wg6.b(simpleName, "BleAdapter::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public BleAdapter(Context context2, String str, String str2) {
        Wg6.c(context2, "context");
        Wg6.c(str, "serial");
        Wg6.c(str2, "macAddress");
        this.context = context2;
        this.serial = str;
        this.macAddress = str2;
    }

    @DexIgnore
    public abstract void closeConnection(FLogger.Session session, boolean z);

    @DexIgnore
    public abstract int getBatteryLevel();

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public abstract String getCurrentLabelVersion();

    @DexIgnore
    public abstract String getCurrentWatchParamVersion();

    @DexIgnore
    public abstract String getDeviceModel();

    @DexIgnore
    public abstract String getFirmwareVersion();

    @DexIgnore
    public abstract int getGattState();

    @DexIgnore
    public HeartRateMode getHeartRateMode() {
        return this.heartRateMode;
    }

    @DexIgnore
    public abstract int getHidState();

    @DexIgnore
    public abstract String getLocale();

    @DexIgnore
    public abstract String getLocaleVersion();

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public abstract short getMicroAppMajorVersion();

    @DexIgnore
    public abstract short getMicroAppMinorVersion();

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public abstract int getSupportedWatchParamMajor();

    @DexIgnore
    public abstract int getSupportedWatchParamMinor();

    @DexIgnore
    public abstract String getSupportedWatchParamVersion();

    @DexIgnore
    public abstract byte[] getTSecretKey();

    @DexIgnore
    public abstract Ry1 getUiPackageOSVersion();

    @DexIgnore
    public abstract VibrationStrengthObj getVibrationStrength();

    @DexIgnore
    public abstract boolean isDeviceReady();

    @DexIgnore
    public abstract <T> T isSupportedFeature(Class<T> cls);

    @DexIgnore
    public final void log(FLogger.Session session, String str) {
        Wg6.c(session, "logSession");
        Wg6.c(str, "message");
        MFLog activeLog = MFLogManager.getInstance(this.context).getActiveLog(this.serial);
        if (activeLog != null) {
            activeLog.log('[' + this.serial + "] " + str);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + this.serial + "] " + str);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, this.serial, TAG, str);
    }

    @DexIgnore
    public final void setMacAddress(String str) {
        Wg6.c(str, "<set-?>");
        this.macAddress = str;
    }
}
