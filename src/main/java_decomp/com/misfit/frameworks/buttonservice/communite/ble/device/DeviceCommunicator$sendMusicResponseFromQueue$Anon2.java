package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Qq7;
import com.fossil.Yn7;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendMusicResponseFromQueue$Anon2 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendMusicResponseFromQueue$2$2", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendMusicResponseFromQueue$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceCommunicator$sendMusicResponseFromQueue$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, xe6);
            anon2_Level2.p$ = (Il6) obj;
            throw null;
            //return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon2_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2 = this.this$0;
                deviceCommunicator$sendMusicResponseFromQueue$Anon2.this$0.startSendMusicAppResponse(deviceCommunicator$sendMusicResponseFromQueue$Anon2.$response);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendMusicResponseFromQueue$Anon2(DeviceCommunicator deviceCommunicator, MusicResponse musicResponse) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$response = musicResponse;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
        invoke(yx1);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Yx1 yx1) {
        Wg6.c(yx1, "error");
        MusicResponse musicResponse = this.$response;
        ErrorCodeBuilder.Step step = musicResponse instanceof NotifyMusicEventResponse ? ErrorCodeBuilder.Step.NOTIFY_MUSIC_EVENT : musicResponse instanceof MusicTrackInfoResponse ? ErrorCodeBuilder.Step.SEND_TRACK_INFO : null;
        if (step != null) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = this.this$0.getSerial();
            String access$getTAG$p = DeviceCommunicator.access$getTAG$p(this.this$0);
            remote.e(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getType() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = DeviceCommunicator.access$getTAG$p(this.this$0);
        local.d(access$getTAG$p2, "device with serial = " + this.this$0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", push back by result error=" + yx1.getErrorCode());
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Anon2_Level2(this, null), 3, null);
    }
}
