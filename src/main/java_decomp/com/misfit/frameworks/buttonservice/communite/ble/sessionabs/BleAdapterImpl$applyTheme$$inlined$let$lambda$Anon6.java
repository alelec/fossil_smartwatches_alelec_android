package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon6 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeData $themeData$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon6(BleAdapterImpl bleAdapterImpl, ThemeData themeData, ISessionSdkCallback iSessionSdkCallback, FLogger.Session session) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$themeData$inlined = themeData;
        this.$callback$inlined = iSessionSdkCallback;
        this.$logSession$inlined = session;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
        invoke(yx1);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Yx1 yx1) {
        Wg6.c(yx1, "it");
        this.this$0.logSdkError(this.$logSession$inlined, "applyTheme with themeEditor", ErrorCodeBuilder.Step.APPLY_THEME, yx1);
        this.$callback$inlined.onApplyThemeError(yx1);
    }
}
