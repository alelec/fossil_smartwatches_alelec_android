package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationDeviceSession extends EnableMaintainingSession implements ICalibrationSession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ApplyHandState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ApplyHandState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyHandPositionFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_APPLY_HAND_POSITION);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onApplyHandPositionSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = CalibrationDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = Bundle.EMPTY;
                Wg6.b(bundle, "Bundle.EMPTY");
                bleSessionCallback.onBleStateResult(0, bundle);
            }
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState((CalibrationDeviceSession) BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.APPLY_HAND_POSITION);
            Zb0<Cd6> calibrationApplyHandPosition = CalibrationDeviceSession.this.getBleAdapter().calibrationApplyHandPosition(CalibrationDeviceSession.this.getLogSession(), this);
            this.task = calibrationApplyHandPosition;
            if (calibrationApplyHandPosition == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MoveHandState extends BleStateAbs {
        @DexIgnore
        public boolean isSending;
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public MoveHandState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final boolean isSending() {
            return this.isSending;
        }

        @DexIgnore
        public final boolean moveHand(HandCalibrationObj handCalibrationObj) {
            Wg6.c(handCalibrationObj, "handCalibrationObj");
            Zb0<Cd6> calibrationMoveHand = CalibrationDeviceSession.this.getBleAdapter().calibrationMoveHand(CalibrationDeviceSession.this.getLogSession(), handCalibrationObj, this);
            this.task = calibrationMoveHand;
            if (calibrationMoveHand == null) {
                CalibrationDeviceSession.this.stop(10000);
            } else {
                this.isSending = true;
                startTimeout();
            }
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.MOVE_HAND);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onMoveHandFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            this.isSending = false;
            CalibrationDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_MOVE_HAND);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onMoveHandSuccess() {
            stopTimeout();
            this.isSending = false;
            BleSession.BleSessionCallback bleSessionCallback = CalibrationDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = Bundle.EMPTY;
                Wg6.b(bundle, "Bundle.EMPTY");
                bleSessionCallback.onBleStateResult(0, bundle);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }

        @DexIgnore
        public final void setSending(boolean z) {
            this.isSending = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReleaseHandControlState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReleaseHandControlState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.EXIT_CALIBRATION);
            Zb0<Cd6> calibrationReleaseHandControl = CalibrationDeviceSession.this.getBleAdapter().calibrationReleaseHandControl(CalibrationDeviceSession.this.getLogSession(), this);
            this.task = calibrationReleaseHandControl;
            if (calibrationReleaseHandControl == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReleaseHandControlFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_RELEASE_HAND_CONTROL);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReleaseHandControlSuccess() {
            stopTimeout();
            CalibrationDeviceSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class RequestHandControlState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public RequestHandControlState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.ENTER_CALIBRATION);
            Zb0<Cd6> calibrationRequestHandControl = CalibrationDeviceSession.this.getBleAdapter().calibrationRequestHandControl(CalibrationDeviceSession.this.getLogSession(), this);
            this.task = calibrationRequestHandControl;
            if (calibrationRequestHandControl == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onRequestHandControlFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_REQUEST_HAND_CONTROL);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onRequestHandControlSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = CalibrationDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = Bundle.EMPTY;
                Wg6.b(bundle, "Bundle.EMPTY");
                bleSessionCallback.onBleStateResult(0, bundle);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ResetHandsState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ResetHandsState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.RESET_HAND);
            Zb0<Cd6> calibrationResetHandToZeroDegree = CalibrationDeviceSession.this.getBleAdapter().calibrationResetHandToZeroDegree(CalibrationDeviceSession.this.getLogSession(), this);
            this.task = calibrationResetHandToZeroDegree;
            if (calibrationResetHandToZeroDegree == null) {
                CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
                calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState((CalibrationDeviceSession) BleSessionAbs.SessionState.MOVE_HAND_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onResetHandsFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            CalibrationDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_RESET_HAND_CONTROL);
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState((CalibrationDeviceSession) BleSessionAbs.SessionState.MOVE_HAND_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onResetHandsSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = CalibrationDeviceSession.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = Bundle.EMPTY;
                Wg6.b(bundle, "Bundle.EMPTY");
                bleSessionCallback.onBleStateResult(0, bundle);
            }
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState((CalibrationDeviceSession) BleSessionAbs.SessionState.MOVE_HAND_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CalibrationDeviceSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.ENTER_CALIBRATION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        CalibrationDeviceSession calibrationDeviceSession = new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback());
        calibrationDeviceSession.setDevice(getDevice());
        return calibrationDeviceSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.REQUEST_HAND_CONTROL_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession
    public boolean handleApplyHandsPosition() {
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.APPLY_HAND_STATE));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession
    public boolean handleMoveHandRequest(HandCalibrationObj handCalibrationObj) {
        Wg6.c(handCalibrationObj, "handCalibrationObj");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleMoveHandRequest() - currentState=" + getCurrentState());
        if (BleState.Companion.isNull(getCurrentState())) {
            onStart(new Object[0]);
            return true;
        } else if (getCurrentState() instanceof MoveHandState) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                MoveHandState moveHandState = (MoveHandState) currentState;
                if (moveHandState.isSending()) {
                    return true;
                }
                moveHandState.moveHand(handCalibrationObj);
                return true;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession.MoveHandState");
        } else {
            FLogger.INSTANCE.getLocal().e(getTAG(), ".handleMoveHandRequest() - currentState is not move hand state");
            return true;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession
    public boolean handleReleaseHandControl() {
        return enterStateAsync(createConcreteState(BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession
    public boolean handleResetHandsPosition() {
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.RESET_HANDS_STATE));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.REQUEST_HAND_CONTROL_STATE;
        String name = RequestHandControlState.class.getName();
        Wg6.b(name, "RequestHandControlState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.RESET_HANDS_STATE;
        String name2 = ResetHandsState.class.getName();
        Wg6.b(name2, "ResetHandsState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.MOVE_HAND_STATE;
        String name3 = MoveHandState.class.getName();
        Wg6.b(name3, "MoveHandState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.APPLY_HAND_STATE;
        String name4 = ApplyHandState.class.getName();
        Wg6.b(name4, "ApplyHandState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE;
        String name5 = ReleaseHandControlState.class.getName();
        Wg6.b(name5, "ReleaseHandControlState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }
}
