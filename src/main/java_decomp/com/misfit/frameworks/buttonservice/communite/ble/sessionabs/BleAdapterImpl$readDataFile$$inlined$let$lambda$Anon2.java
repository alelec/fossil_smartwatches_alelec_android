package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.fitness.FitnessData;
import com.mapped.Cd6;
import com.mapped.H60;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2 extends Qq7 implements Hg6<FitnessData[], Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ H60 $biometricProfile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, H60 h60, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$biometricProfile$inlined = h60;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(FitnessData[] fitnessDataArr) {
        invoke(fitnessDataArr);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(FitnessData[] fitnessDataArr) {
        Wg6.c(fitnessDataArr, "it");
        this.this$0.log(this.$logSession$inlined, "Read Data Files Success");
        this.$callback$inlined.onReadDataFilesSuccess(fitnessDataArr);
    }
}
