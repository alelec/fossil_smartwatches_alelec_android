package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Mp1;
import com.mapped.Q40;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetBatteryLevelSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PlayAnimationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRealTimeStepsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetActivityGoalSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWorkoutDetectionConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UnlinkSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UpdateCurrentTimeSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.QuickCommandQueue;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleCommunicatorAbs extends BleCommunicator {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ HandlerThread handlerThread;
    @DexIgnore
    public BleSession currentSession;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapter;
    @DexIgnore
    public /* final */ Handler mHandler; // = new Handler(getHandlerThread().getLooper());
    @DexIgnore
    public QuickCommandQueue mQuickCommandQueue; // = new QuickCommandQueue();
    @DexIgnore
    public /* final */ Q40.Bi mStateCallback;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HandlerThread getHandlerThread() {
            return BleCommunicatorAbs.handlerThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[Q40.Ci.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Q40.Ci.CONNECTED.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = BleCommunicatorAbs.class.getSimpleName();
        Wg6.b(simpleName, "BleCommunicatorAbs::class.java.simpleName");
        TAG = simpleName;
        HandlerThread handlerThread2 = new HandlerThread(TAG);
        handlerThread = handlerThread2;
        handlerThread2.start();
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs(BleAdapterImpl bleAdapterImpl, Context context, String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(str, communicationResultCallback);
        Wg6.c(bleAdapterImpl, "mBleAdapter");
        Wg6.c(context, "context");
        Wg6.c(str, "serial");
        Wg6.c(communicationResultCallback, "communicationResultCallback");
        this.mBleAdapter = bleAdapterImpl;
        this.currentSession = BleSessionAbs.Companion.createNullSession(context);
        BleCommunicatorAbs$mStateCallback$Anon1 bleCommunicatorAbs$mStateCallback$Anon1 = new BleCommunicatorAbs$mStateCallback$Anon1(this);
        this.mStateCallback = bleCommunicatorAbs$mStateCallback$Anon1;
        this.mBleAdapter.registerBluetoothStateCallback(bleCommunicatorAbs$mStateCallback$Anon1);
    }

    @DexIgnore
    private final void processQuickCommandQueue() {
        synchronized (this) {
            Object poll = this.mQuickCommandQueue.poll();
            if (poll != null) {
                if (getBleAdapter().isDeviceReady()) {
                    onQuickCommandAction(poll);
                }
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new BleCommunicatorAbs$processQuickCommandQueue$Anon1(this, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final void addToQuickCommandQueue(Object obj) {
        synchronized (this) {
            Wg6.c(obj, "obj");
            this.mQuickCommandQueue.add(obj);
            processQuickCommandQueue();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void cleanUp() {
        super.cleanUp();
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void clearQuickCommandQueue() {
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public BleAdapterImpl getBleAdapter() {
        return this.mBleAdapter;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public BleSession getCurrentSession() {
        return this.currentSession;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapter() {
        return this.mBleAdapter;
    }

    @DexIgnore
    public final Handler getMHandler() {
        return this.mHandler;
    }

    @DexIgnore
    public void handleDeviceStateChanged(Q40 q40, Q40.Ci ci, Q40.Ci ci2) {
        Wg6.c(q40, "device");
        Wg6.c(ci, "previousState");
        Wg6.c(ci2, "newState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onDeviceStateChanged, device=" + q40.M().getSerialNumber() + ", previousState=" + ci + ", newState=" + ci2);
        if (WhenMappings.$EnumSwitchMapping$0[ci2.ordinal()] != 1) {
            getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 0);
            return;
        }
        getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 2);
        processQuickCommandQueue();
    }

    @DexIgnore
    public void handleEventReceived(Q40 q40, Mp1 mp1) {
        Wg6.c(q40, "device");
        Wg6.c(mp1, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onEventReceived(), device=" + q40.M().getSerialNumber() + ", event=" + mp1);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean isDeviceReady() {
        return this.mBleAdapter.isDeviceReady();
    }

    @DexIgnore
    public abstract void onQuickCommandAction(Object obj);

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void sendCustomCommand(CustomRequest customRequest) {
        Wg6.c(customRequest, Constants.COMMAND);
        this.mBleAdapter.sendCustomCommand(customRequest);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setCurrentSession(BleSession bleSession) {
        Wg6.c(bleSession, "<set-?>");
        this.currentSession = bleSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setNullCurrentSession() {
        setCurrentSession(BleSessionAbs.Companion.createNullSession(getBleAdapter().getContext()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCalibrationSession() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetBatteryLevelSession() {
        queueSessionAndStart(new GetBatteryLevelSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetRssiSession() {
        queueSessionAndStart(new ReadRssiSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startGetVibrationStrengthSession() {
        queueSessionAndStart(new GetVibrationStrengthSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startPlayAnimationSession() {
        queueSessionAndStart(new PlayAnimationSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startReadRealTimeStepSession() {
        queueSessionAndStart(new ReadRealTimeStepsSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        Wg6.c(notificationBaseObj, "newNotification");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSessionInQueueProcess() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            BleSession poll = getHighSessionQueue().poll();
            if (poll == null) {
                poll = getLowSessionQueue().poll();
            }
            if (poll == null || !(poll instanceof BleSessionAbs)) {
                FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - queue is empty. Be idle now.");
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".startSessionInQueueProcess() - next session is " + poll);
            setCurrentSession(poll);
            getCurrentSession().start(new Object[0]);
            return;
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - a session is exist, session will be start later.");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetActivityGoals(int i, int i2, int i3) {
        queueSessionAndStart(new SetActivityGoalSession(i, i2, i3, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        Wg6.c(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        Wg6.c(localizationData, "localizationData");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        Wg6.c(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj) {
        Wg6.c(vibrationStrengthObj, "vibrationStrengthLevelObj");
        queueSessionAndStart(new SetVibrationStrengthSession(vibrationStrengthObj, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting) {
        Wg6.c(workoutDetectionSetting, "workoutDetectionSetting");
        queueSessionAndStart(new SetWorkoutDetectionConfigSession(workoutDetectionSetting, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startUnlinkSession() {
        queueSessionAndStart(new UnlinkSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startUpdateCurrentTime() {
        queueSessionAndStart(new UpdateCurrentTimeSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }
}
