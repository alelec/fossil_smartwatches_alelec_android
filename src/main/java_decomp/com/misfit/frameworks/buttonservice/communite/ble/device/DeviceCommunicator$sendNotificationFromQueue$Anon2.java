package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Qq7;
import com.fossil.Yn7;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendNotificationFromQueue$Anon2 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2$1", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendNotificationFromQueue$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(DeviceCommunicator$sendNotificationFromQueue$Anon2 deviceCommunicator$sendNotificationFromQueue$Anon2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceCommunicator$sendNotificationFromQueue$Anon2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                DeviceCommunicator$sendNotificationFromQueue$Anon2 deviceCommunicator$sendNotificationFromQueue$Anon2 = this.this$0;
                deviceCommunicator$sendNotificationFromQueue$Anon2.this$0.sendDianaNotification(deviceCommunicator$sendNotificationFromQueue$Anon2.$notification);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$Anon2(DeviceCommunicator deviceCommunicator, NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
        invoke(yx1);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Yx1 yx1) {
        Wg6.c(yx1, "error");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = this.this$0.getSerial();
        String access$getTAG$p = DeviceCommunicator.access$getTAG$p(this.this$0);
        remote.e(component, session, serial, access$getTAG$p, "Send notification: " + this.$notification.toRemoteLogString() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_APP_NOTIFICATION, ErrorCodeBuilder.Component.SDK, yx1));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = DeviceCommunicator.access$getTAG$p(this.this$0);
        local.d(access$getTAG$p2, " .sendNotificationFromQueue() = " + this.$notification + ", error=" + yx1.getErrorCode());
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Anon1_Level2(this, null), 3, null);
    }
}
