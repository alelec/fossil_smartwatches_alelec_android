package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.Qq7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendResponseToWatch$Anon1 extends Qq7 implements Hg6<Cd6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceAppResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendResponseToWatch$Anon1(DeviceCommunicator deviceCommunicator, DeviceAppResponse deviceAppResponse) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$response = deviceAppResponse;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Cd6 cd6) {
        invoke(cd6);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Cd6 cd6) {
        Wg6.c(cd6, "it");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = this.this$0.getSerial();
        String access$getTAG$p = DeviceCommunicator.access$getTAG$p(this.this$0);
        remote.i(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getDeviceEventId().name() + " Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = DeviceCommunicator.access$getTAG$p(this.this$0);
        local.d(access$getTAG$p2, "device with serial = " + this.this$0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", result success");
    }
}
