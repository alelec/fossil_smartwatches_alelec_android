package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.mapped.Ta0;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BasePairingNewDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public byte[] firmwareBytes;
    @DexIgnore
    public FirmwareData firmwareData;
    @DexIgnore
    public boolean isJustUpdateFW;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zk1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public FetchDeviceInfoState() {
            super(BasePairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Zk1> fetchDeviceInfo = BasePairingNewDeviceSession.this.getBleAdapter().fetchDeviceInfo(BasePairingNewDeviceSession.this.getLogSession(), this);
            this.task = fetchDeviceInfo;
            if (fetchDeviceInfo == null) {
                BasePairingNewDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            BasePairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoSuccess(Zk1 zk1) {
            Wg6.c(zk1, "deviceInformation");
            stopTimeout();
            if (BasePairingNewDeviceSession.this.getBleAdapter().isSupportedFeature(Ta0.class) != null) {
                BasePairingNewDeviceSession basePairingNewDeviceSession = BasePairingNewDeviceSession.this;
                basePairingNewDeviceSession.enterStateAsync(basePairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW));
                return;
            }
            BasePairingNewDeviceSession basePairingNewDeviceSession2 = BasePairingNewDeviceSession.this;
            basePairingNewDeviceSession2.enterStateAsync(basePairingNewDeviceSession2.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zk1> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BasePairingNewDeviceSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.SPECIAL, CommunicateMode.LINK, bleAdapterImpl, bleSessionCallback);
        Wg6.c(userProfile2, "userProfile");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
        this.communicationResultCallback = communicationResultCallback2;
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.PAIR);
        this.userProfile.setNewDevice(true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final byte[] getFirmwareBytes() {
        return this.firmwareBytes;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        Wg6.b(name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    public final boolean isJustUpdateFW() {
        return this.isJustUpdateFW;
    }

    @DexIgnore
    public final void setFirmwareBytes(byte[] bArr) {
        this.firmwareBytes = bArr;
    }

    @DexIgnore
    public final void setFirmwareData(FirmwareData firmwareData2) {
        this.firmwareData = firmwareData2;
    }

    @DexIgnore
    public final void setJustUpdateFW(boolean z) {
        this.isJustUpdateFW = z;
    }
}
