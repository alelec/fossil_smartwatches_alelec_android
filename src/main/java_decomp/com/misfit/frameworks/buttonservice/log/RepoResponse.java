package com.misfit.frameworks.buttonservice.log;

import com.fossil.Q88;
import com.fossil.W18;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class RepoResponse<T> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final <T> Failure<T> create(Throwable th) {
            Wg6.c(th, "error");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("create=");
            th.printStackTrace();
            sb.append(Cd6.a);
            local.d("RepoResponse", sb.toString());
            return th instanceof SocketTimeoutException ? new Failure<>(MFNetworkReturnCode.CLIENT_TIMEOUT, null, th, null, 8, null) : new Failure<>(601, null, th, null, 8, null);
        }

        @DexIgnore
        public final <T> RepoResponse<T> create(Q88<T> q88) {
            String f;
            String f2;
            Integer code;
            String string;
            Wg6.c(q88, "response");
            if (q88.e()) {
                return new Success(q88.a());
            }
            int b = q88.b();
            if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(b));
                W18 d = q88.d();
                if (d == null || (f = d.string()) == null) {
                    f = q88.f();
                }
                serverError.setMessage(f);
                return new Failure(b, serverError, null, null, 8, null);
            }
            W18 d2 = q88.d();
            String f3 = (d2 == null || (string = d2.string()) == null) ? q88.f() : string;
            try {
                ServerError serverError2 = (ServerError) new Gson().k(f3, ServerError.class);
                return (serverError2 == null || ((code = serverError2.getCode()) != null && code.intValue() == 0)) ? new Failure(q88.b(), null, null, f3) : new Failure(q88.b(), serverError2, null, null, 8, null);
            } catch (Exception e) {
                W18 d3 = q88.d();
                if (d3 == null || (f2 = d3.string()) == null) {
                    f2 = q88.f();
                }
                return new Failure(q88.b(), new ServerError(b, f2), null, null, 8, null);
            }
        }
    }

    @DexIgnore
    public RepoResponse() {
    }

    @DexIgnore
    public /* synthetic */ RepoResponse(Qg6 qg6) {
        this();
    }
}
