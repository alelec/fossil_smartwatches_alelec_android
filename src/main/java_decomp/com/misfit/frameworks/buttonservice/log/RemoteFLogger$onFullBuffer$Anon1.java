package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1", f = "RemoteFLogger.kt", l = {157}, m = "invokeSuspend")
public final class RemoteFLogger$onFullBuffer$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public /* final */ /* synthetic */ List $logLines;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onFullBuffer$Anon1(RemoteFLogger remoteFLogger, List list, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = remoteFLogger;
        this.$logLines = list;
        this.$forceFlush = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        RemoteFLogger$onFullBuffer$Anon1 remoteFLogger$onFullBuffer$Anon1 = new RemoteFLogger$onFullBuffer$Anon1(this.this$0, this.$logLines, this.$forceFlush, xe6);
        remoteFLogger$onFullBuffer$Anon1.p$ = (Il6) obj;
        throw null;
        //return remoteFLogger$onFullBuffer$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((RemoteFLogger$onFullBuffer$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            if (this.this$0.isMainFLogger) {
                DBLogWriter dBLogWriter = this.this$0.dbLogWriter;
                if (dBLogWriter != null) {
                    dBLogWriter.writeLog(this.$logLines);
                }
                if (this.$forceFlush) {
                    RemoteFLogger remoteFLogger = this.this$0;
                    this.L$0 = il6;
                    this.label = 1;
                    if (remoteFLogger.flushDB(this) == d) {
                        return d;
                    }
                }
            } else if (!this.$forceFlush) {
                FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, ".onFullBuffer(), broadcast");
                RemoteFLogger remoteFLogger2 = this.this$0;
                String str = remoteFLogger2.floggerName;
                RemoteFLogger.MessageTarget messageTarget = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle = Bundle.EMPTY;
                Wg6.b(bundle, "Bundle.EMPTY");
                remoteFLogger2.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER, str, messageTarget, bundle);
            } else {
                RemoteFLogger remoteFLogger3 = this.this$0;
                String str2 = remoteFLogger3.floggerName;
                RemoteFLogger.MessageTarget messageTarget2 = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle2 = Bundle.EMPTY;
                Wg6.b(bundle2, "Bundle.EMPTY");
                remoteFLogger3.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, str2, messageTarget2, bundle2);
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Cd6.a;
    }
}
