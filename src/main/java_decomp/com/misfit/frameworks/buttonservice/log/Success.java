package com.misfit.frameworks.buttonservice.log;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Success<T> extends RepoResponse<T> {
    @DexIgnore
    public /* final */ T response;

    @DexIgnore
    public Success(T t) {
        super(null);
        this.response = t;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.misfit.frameworks.buttonservice.log.Success */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Success copy$default(Success success, Object obj, int i, Object obj2) {
        if ((i & 1) != 0) {
            obj = success.response;
        }
        return success.copy(obj);
    }

    @DexIgnore
    public final T component1() {
        return this.response;
    }

    @DexIgnore
    public final Success<T> copy(T t) {
        return new Success<>(t);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Success) && Wg6.a(this.response, ((Success) obj).response));
    }

    @DexIgnore
    public final T getResponse() {
        return this.response;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.response;
        if (t != null) {
            return t.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + ((Object) this.response) + ")";
    }
}
