package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.db.DataLogService;
import com.misfit.frameworks.buttonservice.db.DataLogServiceProvider;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFLogManager {
    @DexIgnore
    public static /* final */ String TAG; // = "MFLogManager";
    @DexIgnore
    public static MFLogManager sInstance;
    @DexIgnore
    public HashMap<String, List<CommunicateMode>> activeLogs; // = new HashMap<>();
    @DexIgnore
    public Context context;
    @DexIgnore
    public HashMap<String, MFLog> pendingLogs; // = new LinkedHashMap();
    @DexIgnore
    public HashMap<String, MFLog> pendingLogsToSaveDB; // = new LinkedHashMap();

    @DexIgnore
    private String generatePendingLogKey(CommunicateMode communicateMode, String str) {
        if (TextUtils.isEmpty(str)) {
            return communicateMode.toString();
        }
        return str + communicateMode;
    }

    @DexIgnore
    public static MFLogManager getInstance(Context context2) {
        MFLogManager mFLogManager;
        synchronized (MFLogManager.class) {
            try {
                if (sInstance == null) {
                    MFLogManager mFLogManager2 = new MFLogManager();
                    sInstance = mFLogManager2;
                    mFLogManager2.context = context2.getApplicationContext();
                }
                mFLogManager = sInstance;
            } catch (Throwable th) {
                throw th;
            }
        }
        return mFLogManager;
    }

    @DexIgnore
    private MFLog getLog(CommunicateMode communicateMode, String str) {
        MFLog mFLog;
        synchronized (this) {
            mFLog = this.pendingLogs.get(generatePendingLogKey(communicateMode, str));
        }
        return mFLog;
    }

    @DexIgnore
    private MFLog getMFLogFromDBDataLogService(int i) {
        DataLogService dataLogServiceById;
        if (!(i == -1 || (dataLogServiceById = DataLogServiceProvider.getInstance(this.context).getDataLogServiceById(i)) == null)) {
            try {
                return (MFLog) new Gson().k(dataLogServiceById.getContent(), MFLog.class);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("MFLogService", "Inside .handleAddLog - ex=" + e.toString());
            }
        }
        return null;
    }

    @DexIgnore
    private void saveLogToDB(CommunicateMode communicateMode, String str) {
        String u;
        synchronized (this) {
            MFLog pendingLog = getPendingLog(communicateMode, str);
            removePendingLog(communicateMode, str);
            if (pendingLog == null) {
                FLogger.INSTANCE.getLocal().e(TAG, "Cannot save Log to DB - getPendingLong is null");
                return;
            }
            if (pendingLog instanceof MFSyncLog) {
                u = new Gson().u(pendingLog, MFSyncLog.class);
            } else if (pendingLog instanceof MFOtaLog) {
                u = new Gson().u(pendingLog, MFOtaLog.class);
            } else if (pendingLog instanceof MFSetupLog) {
                u = new Gson().u(pendingLog, MFSetupLog.class);
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Cannot save Log to DB - cannot parse dataLogGson");
                return;
            }
            DataLogServiceProvider.getInstance(this.context).createOrUpdate(new DataLogService(pendingLog.getStartTimeEpoch(), 0, u, pendingLog.getLogType(), pendingLog.getStartTime(), Calendar.getInstance().getTimeInMillis()));
        }
    }

    @DexIgnore
    private void setActiveLog(String str, CommunicateMode communicateMode) {
        synchronized (this) {
            List<CommunicateMode> list = this.activeLogs.get(str);
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(communicateMode);
            this.activeLogs.put(str, list);
        }
    }

    @DexIgnore
    public MFLog addLogForActiveLog(String str, String str2) {
        MFLog activeLog = getActiveLog(str);
        if (activeLog != null && !TextUtils.isEmpty(str2)) {
            activeLog.log(str2);
        }
        return activeLog;
    }

    @DexIgnore
    public void changePendingLogKey(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        synchronized (this) {
            this.pendingLogs.put(generatePendingLogKey(communicateMode2, str2), this.pendingLogs.remove(generatePendingLogKey(communicateMode, str)));
            List<CommunicateMode> remove = this.activeLogs.remove(str);
            if (remove != null && !remove.isEmpty()) {
                remove.remove(communicateMode);
                remove.add(communicateMode2);
            }
            this.activeLogs.put(str2, remove);
        }
    }

    @DexIgnore
    public MFLog end(CommunicateMode communicateMode, String str) {
        MFLog log;
        synchronized (this) {
            log = getLog(communicateMode, str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".end - log=" + log);
            if (log != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local2.d(str3, "end - serial=" + str + ", communicateMode=" + communicateMode + ", resultCode=" + log.getResultCode());
                log.setEndTime(System.currentTimeMillis());
                this.pendingLogsToSaveDB.put(generatePendingLogKey(communicateMode, str), log);
                saveLogToDB(communicateMode, str);
            }
            this.pendingLogs.remove(generatePendingLogKey(communicateMode, str));
            List<CommunicateMode> remove = this.activeLogs.remove(str);
            if (remove != null && !remove.isEmpty()) {
                remove.remove(communicateMode);
            }
            this.activeLogs.put(str, remove);
        }
        return log;
    }

    @DexIgnore
    public CommunicateMode getActiveCommunicateMode(String str) {
        List<CommunicateMode> list = this.activeLogs.get(str);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public MFLog getActiveLog(String str) {
        MFLog log;
        List<CommunicateMode> list = this.activeLogs.get(str);
        CommunicateMode communicateMode = (list == null || list.isEmpty()) ? null : list.get(list.size() - 1);
        if (communicateMode == null || (log = getLog(communicateMode, str)) == null || log.isHidden()) {
            return null;
        }
        return log;
    }

    @DexIgnore
    public MFLog getActiveLogIncludeHidden(String str) {
        List<CommunicateMode> list = this.activeLogs.get(str);
        CommunicateMode communicateMode = (list == null || list.isEmpty()) ? null : list.get(list.size() - 1);
        if (communicateMode != null) {
            return getLog(communicateMode, str);
        }
        return null;
    }

    @DexIgnore
    public MFLog getPendingLog(CommunicateMode communicateMode, String str) {
        MFLog mFLog;
        synchronized (this) {
            mFLog = this.pendingLogsToSaveDB.get(generatePendingLogKey(communicateMode, str));
        }
        return mFLog;
    }

    @DexIgnore
    public void removePendingLog(CommunicateMode communicateMode, String str) {
        synchronized (this) {
            this.pendingLogsToSaveDB.remove(generatePendingLogKey(communicateMode, str));
        }
    }

    @DexIgnore
    public MFOtaLog startOtaLog(String str) {
        MFOtaLog mFOtaLog;
        synchronized (this) {
            end(CommunicateMode.OTA, str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "startOtaLog - serial=" + str);
            mFOtaLog = new MFOtaLog(this.context);
            String generatePendingLogKey = generatePendingLogKey(CommunicateMode.OTA, str);
            mFOtaLog.log("Start new ota session.");
            this.pendingLogs.put(generatePendingLogKey, mFOtaLog);
            setActiveLog(str, CommunicateMode.OTA);
        }
        return mFOtaLog;
    }

    @DexIgnore
    public MFSetupLog startSetupLog(String str, FossilDeviceSerialPatternUtil.BRAND brand) {
        MFSetupLog mFSetupLog;
        synchronized (this) {
            end(CommunicateMode.LINK, str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "startSetupLog - serial=" + str + ", brand=" + brand.toString());
            mFSetupLog = new MFSetupLog(this.context);
            String generatePendingLogKey = generatePendingLogKey(CommunicateMode.LINK, str);
            mFSetupLog.log("Start new setup session. Supported devices: " + Arrays.toString(ButtonService.Companion.getSupportedDevices(brand).toArray()));
            this.pendingLogs.put(generatePendingLogKey, mFSetupLog);
            setActiveLog(str, CommunicateMode.LINK);
        }
        return mFSetupLog;
    }

    @DexIgnore
    public MFSyncLog startSyncLog(String str) {
        MFSyncLog mFSyncLog;
        synchronized (this) {
            end(CommunicateMode.SYNC, str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "startSyncLog - serial=" + str);
            mFSyncLog = new MFSyncLog(this.context);
            String generatePendingLogKey = generatePendingLogKey(CommunicateMode.SYNC, str);
            mFSyncLog.log("Start new sync session.");
            this.pendingLogs.put(generatePendingLogKey, mFSyncLog);
            setActiveLog(str, CommunicateMode.SYNC);
        }
        return mFSyncLog;
    }

    @DexIgnore
    public void stopLogService(int i) {
        synchronized (this) {
            if (this.pendingLogs != null) {
                for (MFLog mFLog : this.pendingLogs.values()) {
                    if (mFLog != null && mFLog.getEndTime() <= 0) {
                        if (i == 1905) {
                            mFLog.log("App was crashed from button service");
                        } else if (i != 1906) {
                            mFLog.log("App was removed from task manager");
                        } else {
                            mFLog.log("App was crashed from app layer");
                        }
                        mFLog.setResultCode(i);
                    }
                }
                this.pendingLogs.clear();
            }
            if (this.pendingLogsToSaveDB != null) {
                this.pendingLogsToSaveDB.clear();
            }
            if (this.activeLogs != null) {
                this.activeLogs.clear();
            }
        }
    }
}
