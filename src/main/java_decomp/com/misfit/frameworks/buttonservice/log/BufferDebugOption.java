package com.misfit.frameworks.buttonservice.log;

import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferDebugOption {
    @DexIgnore
    public /* final */ IFinishCallback callback;

    @DexIgnore
    public BufferDebugOption(IFinishCallback iFinishCallback) {
        Wg6.c(iFinishCallback, Constants.CALLBACK);
        this.callback = iFinishCallback;
    }

    @DexIgnore
    public static /* synthetic */ BufferDebugOption copy$default(BufferDebugOption bufferDebugOption, IFinishCallback iFinishCallback, int i, Object obj) {
        if ((i & 1) != 0) {
            iFinishCallback = bufferDebugOption.callback;
        }
        return bufferDebugOption.copy(iFinishCallback);
    }

    @DexIgnore
    public final IFinishCallback component1() {
        return this.callback;
    }

    @DexIgnore
    public final BufferDebugOption copy(IFinishCallback iFinishCallback) {
        Wg6.c(iFinishCallback, Constants.CALLBACK);
        return new BufferDebugOption(iFinishCallback);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof BufferDebugOption) && Wg6.a(this.callback, ((BufferDebugOption) obj).callback));
    }

    @DexIgnore
    public final IFinishCallback getCallback() {
        return this.callback;
    }

    @DexIgnore
    public int hashCode() {
        IFinishCallback iFinishCallback = this.callback;
        if (iFinishCallback != null) {
            return iFinishCallback.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "BufferDebugOption(callback=" + this.callback + ")";
    }
}
