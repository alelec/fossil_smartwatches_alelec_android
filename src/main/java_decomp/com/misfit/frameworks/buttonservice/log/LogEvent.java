package com.misfit.frameworks.buttonservice.log;

import com.google.gson.Gson;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LogEvent {
    @DexIgnore
    public String appVersion;
    @DexIgnore
    public FLogger.Component component;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String fwVersion;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public FLogger.LogLevel logLevel;
    @DexIgnore
    public String logMessage;
    @DexIgnore
    public String phoneId;
    @DexIgnore
    public String phoneModel;
    @DexIgnore
    public String platform;
    @DexIgnore
    public String platformVersion;
    @DexIgnore
    public String sdkVersion;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public FLogger.Session session;
    @DexIgnore
    public transient Object tag;
    @DexIgnore
    public long timestamp;
    @DexIgnore
    public String userId;

    @DexIgnore
    public LogEvent(FLogger.LogLevel logLevel2, long j, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, FLogger.Component component2, FLogger.Session session2, String str10, String str11) {
        Wg6.c(logLevel2, "logLevel");
        Wg6.c(str, ButtonService.USER_ID);
        Wg6.c(str2, "phoneId");
        Wg6.c(str3, "appVersion");
        Wg6.c(str4, "platform");
        Wg6.c(str5, "platformVersion");
        Wg6.c(str6, "phoneModel");
        Wg6.c(str7, "fwVersion");
        Wg6.c(str8, "sdkVersion");
        Wg6.c(str9, "deviceModel");
        Wg6.c(component2, "component");
        Wg6.c(session2, Constants.SESSION);
        Wg6.c(str10, "serialNumber");
        Wg6.c(str11, "logMessage");
        this.logLevel = logLevel2;
        this.timestamp = j;
        this.userId = str;
        this.phoneId = str2;
        this.appVersion = str3;
        this.platform = str4;
        this.platformVersion = str5;
        this.phoneModel = str6;
        this.fwVersion = str7;
        this.sdkVersion = str8;
        this.deviceModel = str9;
        this.component = component2;
        this.session = session2;
        this.serialNumber = str10;
        this.logMessage = str11;
        String uuid = UUID.randomUUID().toString();
        Wg6.b(uuid, "UUID.randomUUID().toString()");
        this.id = uuid;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final FLogger.Component getComponent() {
        return this.component;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getFwVersion() {
        return this.fwVersion;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final FLogger.LogLevel getLogLevel() {
        return this.logLevel;
    }

    @DexIgnore
    public final String getLogMessage() {
        return this.logMessage;
    }

    @DexIgnore
    public final String getPhoneId() {
        return this.phoneId;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final FLogger.Session getSession() {
        return this.session;
    }

    @DexIgnore
    public final Object getTag() {
        return this.tag;
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public final void setAppVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.appVersion = str;
    }

    @DexIgnore
    public final void setComponent(FLogger.Component component2) {
        Wg6.c(component2, "<set-?>");
        this.component = component2;
    }

    @DexIgnore
    public final void setDeviceModel(String str) {
        Wg6.c(str, "<set-?>");
        this.deviceModel = str;
    }

    @DexIgnore
    public final void setFwVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.fwVersion = str;
    }

    @DexIgnore
    public final void setLogLevel(FLogger.LogLevel logLevel2) {
        Wg6.c(logLevel2, "<set-?>");
        this.logLevel = logLevel2;
    }

    @DexIgnore
    public final void setLogMessage(String str) {
        Wg6.c(str, "value");
        String substring = str.substring(0, Math.min(str.length(), 2000));
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        this.logMessage = substring;
    }

    @DexIgnore
    public final void setPhoneId(String str) {
        Wg6.c(str, "<set-?>");
        this.phoneId = str;
    }

    @DexIgnore
    public final void setPhoneModel(String str) {
        Wg6.c(str, "<set-?>");
        this.phoneModel = str;
    }

    @DexIgnore
    public final void setPlatform(String str) {
        Wg6.c(str, "<set-?>");
        this.platform = str;
    }

    @DexIgnore
    public final void setPlatformVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.platformVersion = str;
    }

    @DexIgnore
    public final void setSdkVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.sdkVersion = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setSession(FLogger.Session session2) {
        Wg6.c(session2, "<set-?>");
        this.session = session2;
    }

    @DexIgnore
    public final void setTag(Object obj) {
        this.tag = obj;
    }

    @DexIgnore
    public final void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public final void setUserId(String str) {
        Wg6.c(str, "<set-?>");
        this.userId = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }
}
