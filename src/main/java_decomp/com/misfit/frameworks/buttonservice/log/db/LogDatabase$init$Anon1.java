package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ji;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$init$1", f = "LogDatabase.kt", l = {}, m = "invokeSuspend")
public final class LogDatabase$init$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LogDatabase this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LogDatabase$init$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(LogDatabase$init$Anon1 logDatabase$init$Anon1) {
            this.this$0 = logDatabase$init$Anon1;
        }

        @DexIgnore
        public final void run() {
            Ji openHelper = this.this$0.this$0.getOpenHelper();
            Wg6.b(openHelper, "openHelper");
            openHelper.getWritableDatabase().execSQL("CREATE TRIGGER IF NOT EXISTS delete_keep_2000 after insert on log WHEN (select count(*) from log) >= 2000 BEGIN DELETE FROM log WHERE id NOT IN  (SELECT id FROM log ORDER BY timeStamp desc limit 2000); END");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogDatabase$init$Anon1(LogDatabase logDatabase, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = logDatabase;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        LogDatabase$init$Anon1 logDatabase$init$Anon1 = new LogDatabase$init$Anon1(this.this$0, xe6);
        logDatabase$init$Anon1.p$ = (Il6) obj;
        throw null;
        //return logDatabase$init$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((LogDatabase$init$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            this.this$0.runInTransaction(new Anon1_Level2(this));
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
