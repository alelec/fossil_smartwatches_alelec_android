package com.misfit.frameworks.buttonservice.log;

import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDebugOption {
    @DexIgnore
    public /* final */ IFinishCallback callback;
    @DexIgnore
    public /* final */ int maximumLog;

    @DexIgnore
    public FileDebugOption(int i, IFinishCallback iFinishCallback) {
        Wg6.c(iFinishCallback, Constants.CALLBACK);
        this.maximumLog = i;
        this.callback = iFinishCallback;
    }

    @DexIgnore
    public static /* synthetic */ FileDebugOption copy$default(FileDebugOption fileDebugOption, int i, IFinishCallback iFinishCallback, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = fileDebugOption.maximumLog;
        }
        if ((i2 & 2) != 0) {
            iFinishCallback = fileDebugOption.callback;
        }
        return fileDebugOption.copy(i, iFinishCallback);
    }

    @DexIgnore
    public final int component1() {
        return this.maximumLog;
    }

    @DexIgnore
    public final IFinishCallback component2() {
        return this.callback;
    }

    @DexIgnore
    public final FileDebugOption copy(int i, IFinishCallback iFinishCallback) {
        Wg6.c(iFinishCallback, Constants.CALLBACK);
        return new FileDebugOption(i, iFinishCallback);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof FileDebugOption) {
                FileDebugOption fileDebugOption = (FileDebugOption) obj;
                if (this.maximumLog != fileDebugOption.maximumLog || !Wg6.a(this.callback, fileDebugOption.callback)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final IFinishCallback getCallback() {
        return this.callback;
    }

    @DexIgnore
    public final int getMaximumLog() {
        return this.maximumLog;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.maximumLog;
        IFinishCallback iFinishCallback = this.callback;
        return (iFinishCallback != null ? iFinishCallback.hashCode() : 0) + (i * 31);
    }

    @DexIgnore
    public String toString() {
        return "FileDebugOption(maximumLog=" + this.maximumLog + ", callback=" + this.callback + ")";
    }
}
