package com.misfit.frameworks.buttonservice.log;

import com.fossil.Jj4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Pu3;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FLogUtils$getGsonForLogEvent$Anon1<T> implements Pu3<Long> {
    @DexIgnore
    public static /* final */ FLogUtils$getGsonForLogEvent$Anon1 INSTANCE; // = new FLogUtils$getGsonForLogEvent$Anon1();

    @DexIgnore
    public final Jj4 serialize(Long l, Type type, Kj4 kj4) {
        return new Jj4((Number) new BigDecimal(((double) l.longValue()) / ((double) 1000)).setScale(6, RoundingMode.HALF_UP));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(Long l, Type type, Kj4 kj4) {
        return serialize(l, type, kj4);
    }
}
