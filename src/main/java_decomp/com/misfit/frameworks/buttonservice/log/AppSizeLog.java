package com.misfit.frameworks.buttonservice.log;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppSizeLog {
    @DexIgnore
    @Vu3("app")
    public AppLog app;
    @DexIgnore
    @Vu3("sdk")
    public SDKLog sdk;

    @DexIgnore
    public AppSizeLog() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public AppSizeLog(SDKLog sDKLog, AppLog appLog) {
        Wg6.c(sDKLog, "sdk");
        Wg6.c(appLog, "app");
        this.sdk = sDKLog;
        this.app = appLog;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AppSizeLog(SDKLog sDKLog, AppLog appLog, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new SDKLog(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 15, null) : sDKLog, (i & 2) != 0 ? new AppLog(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 7, null) : appLog);
    }

    @DexIgnore
    public static /* synthetic */ AppSizeLog copy$default(AppSizeLog appSizeLog, SDKLog sDKLog, AppLog appLog, int i, Object obj) {
        if ((i & 1) != 0) {
            sDKLog = appSizeLog.sdk;
        }
        if ((i & 2) != 0) {
            appLog = appSizeLog.app;
        }
        return appSizeLog.copy(sDKLog, appLog);
    }

    @DexIgnore
    public final SDKLog component1() {
        return this.sdk;
    }

    @DexIgnore
    public final AppLog component2() {
        return this.app;
    }

    @DexIgnore
    public final AppSizeLog copy(SDKLog sDKLog, AppLog appLog) {
        Wg6.c(sDKLog, "sdk");
        Wg6.c(appLog, "app");
        return new AppSizeLog(sDKLog, appLog);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AppSizeLog) {
                AppSizeLog appSizeLog = (AppSizeLog) obj;
                if (!Wg6.a(this.sdk, appSizeLog.sdk) || !Wg6.a(this.app, appSizeLog.app)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final AppLog getApp() {
        return this.app;
    }

    @DexIgnore
    public final SDKLog getSdk() {
        return this.sdk;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        SDKLog sDKLog = this.sdk;
        int hashCode = sDKLog != null ? sDKLog.hashCode() : 0;
        AppLog appLog = this.app;
        if (appLog != null) {
            i = appLog.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setApp(AppLog appLog) {
        Wg6.c(appLog, "<set-?>");
        this.app = appLog;
    }

    @DexIgnore
    public final void setSdk(SDKLog sDKLog) {
        Wg6.c(sDKLog, "<set-?>");
        this.sdk = sDKLog;
    }

    @DexIgnore
    public String toString() {
        return "AppSizeLog(sdk=" + this.sdk + ", app=" + this.app + ")";
    }
}
