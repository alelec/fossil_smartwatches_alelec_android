package com.misfit.frameworks.buttonservice.log;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Qg6;
import com.mapped.Vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SDKLog {
    @DexIgnore
    @Vu3("activity_file")
    public float activityFile;
    @DexIgnore
    @Vu3("gps_log")
    public float gpsLogSize;
    @DexIgnore
    @Vu3("hw_log")
    public float hwLog;
    @DexIgnore
    @Vu3("sdk_log")
    public float sdkLogSize;

    @DexIgnore
    public SDKLog() {
        this(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 15, null);
    }

    @DexIgnore
    public SDKLog(float f, float f2, float f3, float f4) {
        this.hwLog = f;
        this.activityFile = f2;
        this.sdkLogSize = f3;
        this.gpsLogSize = f4;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SDKLog(float f, float f2, float f3, float f4, int i, Qg6 qg6) {
        this((i & 1) != 0 ? 0.0f : f, (i & 2) != 0 ? 0.0f : f2, (i & 4) != 0 ? 0.0f : f3, (i & 8) != 0 ? 0.0f : f4);
    }

    @DexIgnore
    public static /* synthetic */ SDKLog copy$default(SDKLog sDKLog, float f, float f2, float f3, float f4, int i, Object obj) {
        if ((i & 1) != 0) {
            f = sDKLog.hwLog;
        }
        if ((i & 2) != 0) {
            f2 = sDKLog.activityFile;
        }
        if ((i & 4) != 0) {
            f3 = sDKLog.sdkLogSize;
        }
        if ((i & 8) != 0) {
            f4 = sDKLog.gpsLogSize;
        }
        return sDKLog.copy(f, f2, f3, f4);
    }

    @DexIgnore
    public final float component1() {
        return this.hwLog;
    }

    @DexIgnore
    public final float component2() {
        return this.activityFile;
    }

    @DexIgnore
    public final float component3() {
        return this.sdkLogSize;
    }

    @DexIgnore
    public final float component4() {
        return this.gpsLogSize;
    }

    @DexIgnore
    public final SDKLog copy(float f, float f2, float f3, float f4) {
        return new SDKLog(f, f2, f3, f4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SDKLog) {
                SDKLog sDKLog = (SDKLog) obj;
                if (!(Float.compare(this.hwLog, sDKLog.hwLog) == 0 && Float.compare(this.activityFile, sDKLog.activityFile) == 0 && Float.compare(this.sdkLogSize, sDKLog.sdkLogSize) == 0 && Float.compare(this.gpsLogSize, sDKLog.gpsLogSize) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getActivityFile() {
        return this.activityFile;
    }

    @DexIgnore
    public final float getGpsLogSize() {
        return this.gpsLogSize;
    }

    @DexIgnore
    public final float getHwLog() {
        return this.hwLog;
    }

    @DexIgnore
    public final float getSdkLogSize() {
        return this.sdkLogSize;
    }

    @DexIgnore
    public int hashCode() {
        return (((((Float.floatToIntBits(this.hwLog) * 31) + Float.floatToIntBits(this.activityFile)) * 31) + Float.floatToIntBits(this.sdkLogSize)) * 31) + Float.floatToIntBits(this.gpsLogSize);
    }

    @DexIgnore
    public final void setActivityFile(float f) {
        this.activityFile = f;
    }

    @DexIgnore
    public final void setGpsLogSize(float f) {
        this.gpsLogSize = f;
    }

    @DexIgnore
    public final void setHwLog(float f) {
        this.hwLog = f;
    }

    @DexIgnore
    public final void setSdkLogSize(float f) {
        this.sdkLogSize = f;
    }

    @DexIgnore
    public String toString() {
        return "SDKLog(hwLog=" + this.hwLog + ", activityFile=" + this.activityFile + ", sdkLogSize=" + this.sdkLogSize + ", gpsLogSize=" + this.gpsLogSize + ")";
    }
}
