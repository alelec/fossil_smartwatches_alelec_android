package com.misfit.frameworks.buttonservice.log;

import com.fossil.Zi4;
import com.google.gson.Gson;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FLogUtils {
    @DexIgnore
    public static /* final */ FLogUtils INSTANCE; // = new FLogUtils();

    @DexIgnore
    public final Gson getGsonForLogEvent() {
        Zi4 zi4 = new Zi4();
        zi4.f(Long.class, FLogUtils$getGsonForLogEvent$Anon1.INSTANCE);
        Gson d = zi4.d();
        Wg6.b(d, "GsonBuilder()\n          \u2026                .create()");
        return d;
    }
}
