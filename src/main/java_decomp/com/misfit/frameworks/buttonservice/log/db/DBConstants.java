package com.misfit.frameworks.buttonservice.log.db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBConstants {
    @DexIgnore
    public static /* final */ String BASE_DOWNLOADED_FILE_DIR_NAME; // = "downloaded_file";
    @DexIgnore
    public static /* final */ DBConstants INSTANCE; // = new DBConstants();
    @DexIgnore
    public static /* final */ String LOG_DB_NAME; // = "log_db.db";
    @DexIgnore
    public static /* final */ int LOG_DB_VERSION; // = 1;
}
