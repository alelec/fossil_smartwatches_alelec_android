package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.fossil.Ao7;
import com.fossil.At7;
import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Pm7;
import com.fossil.Pw0;
import com.fossil.U08;
import com.fossil.W08;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.misfit.frameworks.buttonservice.log.db.Log;
import com.misfit.frameworks.buttonservice.log.db.LogDao;
import com.misfit.frameworks.buttonservice.log.db.LogDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int LIMIT_INPUT_IDS_PARAM; // = 500;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ IDBLogWriterCallback callback;
    @DexIgnore
    public /* final */ LogDao logDao;
    @DexIgnore
    public /* final */ U08 mMutex; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ int thresholdValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface IDBLogWriterCallback {
        @DexIgnore
        Object onDeleteLogs();  // void declaration

        @DexIgnore
        Object onReachDBThreshold();  // void declaration
    }

    /*
    static {
        String simpleName = DBLogWriter.class.getSimpleName();
        Wg6.b(simpleName, "DBLogWriter::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DBLogWriter(Context context, int i, IDBLogWriterCallback iDBLogWriterCallback) {
        Wg6.c(context, "context");
        this.thresholdValue = i;
        this.callback = iDBLogWriterCallback;
        Oh.Ai a2 = Pw0.a(context, LogDatabase.class, DBConstants.LOG_DB_NAME);
        a2.f();
        this.logDao = ((LogDatabase) a2.d()).getLogDao();
    }

    @DexIgnore
    public static final /* synthetic */ IDBLogWriterCallback access$getCallback$p(DBLogWriter dBLogWriter) {
        return dBLogWriter.callback;
    }

    @DexIgnore
    public static final /* synthetic */ LogDao access$getLogDao$p(DBLogWriter dBLogWriter) {
        return dBLogWriter.logDao;
    }

    @DexIgnore
    public final /* synthetic */ Object deleteLogs(List<Integer> list, Xe6<? super Integer> xe6) {
        return list.isEmpty() ^ true ? Eu7.g(Bw7.a(), new DBLogWriter$deleteLogs$Anon2(this, list, null), xe6) : Ao7.e(0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01aa A[Catch:{ all -> 0x021b }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0205  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0208  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0214  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x022f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r16, com.mapped.Xe6<? super com.mapped.Cd6> r17) {
        /*
        // Method dump skipped, instructions count: 580
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.DBLogWriter.flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object getAllLogsExceptSyncing(Xe6<? super List<LogEvent>> xe6) {
        return Eu7.g(Bw7.a(), new DBLogWriter$getAllLogsExceptSyncing$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final /* synthetic */ Object updateCloudFlag(List<Integer> list, Log.Flag flag, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.a(), new DBLogWriter$updateCloudFlag$Anon2(this, list, flag, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void writeLog(List<LogEvent> list) {
        synchronized (this) {
            Wg6.c(list, "logEvents");
            this.logDao.insertLogEvent(At7.u(At7.o(Pm7.z(list), DBLogWriter$writeLog$Anon1.INSTANCE)));
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new DBLogWriter$writeLog$Anon2(this, null), 3, null);
        }
    }
}
