package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMapping$Companion$CREATOR$Anon1 implements Parcelable.Creator<WatchAppMapping> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public WatchAppMapping createFromParcel(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
            try {
                Class<?> cls = Class.forName(readString);
                Wg6.b(cls, "Class.forName(dynamicClassName!!)");
                Constructor<?> declaredConstructor = cls.getDeclaredConstructor(Parcel.class);
                Wg6.b(declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                declaredConstructor.setAccessible(true);
                Object newInstance = declaredConstructor.newInstance(parcel);
                if (newInstance != null) {
                    return (WatchAppMapping) newInstance;
                }
                throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public WatchAppMapping[] newArray(int i) {
        return new WatchAppMapping[i];
    }
}
