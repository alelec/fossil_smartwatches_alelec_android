package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Co1;
import com.fossil.Do1;
import com.fossil.Im7;
import com.fossil.Mo1;
import com.fossil.Zn1;
import com.google.gson.Gson;
import com.mapped.Kc6;
import com.mapped.NotificationFlag;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NotificationBaseObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<NotificationBaseObj> CREATOR; // = new NotificationBaseObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(7);
    @DexIgnore
    public String message;
    @DexIgnore
    public NotificationControlActionStatus notificationControlActionStatus;
    @DexIgnore
    public NotificationControlActionType notificationControlActionType;
    @DexIgnore
    public List<ANotificationFlag> notificationFlags;
    @DexIgnore
    public ANotificationType notificationType;
    @DexIgnore
    public String sender;
    @DexIgnore
    public int senderId; // = -1;
    @DexIgnore
    public String title;
    @DexIgnore
    public int uid;

    @DexIgnore
    public enum ANotificationFlag {
        SILENT,
        IMPORTANT,
        PRE_EXISTING,
        ALLOW_USER_REPLY_MESSAGE,
        ALLOW_USER_ACCEPT_CALL,
        ALLOW_USER_REJECT_CALL;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[ANotificationFlag.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[ANotificationFlag.SILENT.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationFlag.IMPORTANT.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationFlag.PRE_EXISTING.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REPLY_MESSAGE.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_ACCEPT_CALL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REJECT_CALL.ordinal()] = 6;
            }
            */
        }

        @DexIgnore
        public final NotificationFlag toSDKNotificationFlag() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return NotificationFlag.SILENT;
                case 2:
                    return NotificationFlag.IMPORTANT;
                case 3:
                    return NotificationFlag.PRE_EXISTING;
                case 4:
                    return NotificationFlag.ALLOW_USER_REPLY_ACTION;
                case 5:
                    return NotificationFlag.ALLOW_USER_POSITIVE_ACTION;
                case 6:
                    return NotificationFlag.ALLOW_USER_NEGATIVE_ACTION;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore
    public enum ANotificationType {
        UNSUPPORTED,
        INCOMING_CALL,
        TEXT,
        NOTIFICATION,
        EMAIL,
        CALENDAR,
        MISSED_CALL,
        REMOVED;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[ANotificationType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[ANotificationType.UNSUPPORTED.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationType.INCOMING_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationType.TEXT.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationType.NOTIFICATION.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationType.EMAIL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationType.CALENDAR.ordinal()] = 6;
                $EnumSwitchMapping$0[ANotificationType.MISSED_CALL.ordinal()] = 7;
                $EnumSwitchMapping$0[ANotificationType.REMOVED.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final Mo1 toSDKNotificationType() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return Mo1.UNSUPPORTED;
                case 2:
                    return Mo1.INCOMING_CALL;
                case 3:
                    return Mo1.TEXT;
                case 4:
                    return Mo1.NOTIFICATION;
                case 5:
                    return Mo1.EMAIL;
                case 6:
                    return Mo1.CALENDAR;
                case 7:
                    return Mo1.MISSED_CALL;
                case 8:
                    return Mo1.REMOVED;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum NotificationControlActionStatus {
        SUCCESS,
        FAILED,
        NOT_FOUND;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[NotificationControlActionStatus.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[NotificationControlActionStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionStatus.FAILED.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionStatus.NOT_FOUND.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final Co1 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return Co1.SUCCESS;
            }
            if (i == 2) {
                return Co1.FAILED;
            }
            if (i == 3) {
                return Co1.NOT_FOUND;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public enum NotificationControlActionType {
        ACCEPT_PHONE_CALL,
        REJECT_PHONE_CALL,
        DISMISS_NOTIFICATION,
        REPLY_MESSAGE;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[NotificationControlActionType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[NotificationControlActionType.ACCEPT_PHONE_CALL.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionType.REJECT_PHONE_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionType.DISMISS_NOTIFICATION.ordinal()] = 3;
                $EnumSwitchMapping$0[NotificationControlActionType.REPLY_MESSAGE.ordinal()] = 4;
            }
            */
        }

        @DexIgnore
        public final Do1 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return Do1.ACCEPT_PHONE_CALL;
            }
            if (i == 2) {
                return Do1.REJECT_PHONE_CALL;
            }
            if (i == 3) {
                return Do1.DISMISS_NOTIFICATION;
            }
            if (i == 4) {
                return Do1.REPLY_MESSAGE;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public NotificationBaseObj() {
    }

    @DexIgnore
    public NotificationBaseObj(Parcel parcel) {
        Wg6.c(parcel, "parcel");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getMessage() {
        String str = this.message;
        if (str != null) {
            return str;
        }
        Wg6.n("message");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionStatus getNotificationControlActionStatus() {
        NotificationControlActionStatus notificationControlActionStatus2 = this.notificationControlActionStatus;
        if (notificationControlActionStatus2 != null) {
            return notificationControlActionStatus2;
        }
        Wg6.n("notificationControlActionStatus");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionType getNotificationControlActionType() {
        NotificationControlActionType notificationControlActionType2 = this.notificationControlActionType;
        if (notificationControlActionType2 != null) {
            return notificationControlActionType2;
        }
        Wg6.n("notificationControlActionType");
        throw null;
    }

    @DexIgnore
    public final List<ANotificationFlag> getNotificationFlags() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list != null) {
            return list;
        }
        Wg6.n("notificationFlags");
        throw null;
    }

    @DexIgnore
    public final ANotificationType getNotificationType() {
        ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            return aNotificationType;
        }
        Wg6.n("notificationType");
        throw null;
    }

    @DexIgnore
    public final String getSender() {
        String str = this.sender;
        if (str != null) {
            return str;
        }
        Wg6.n(RemoteFLogger.MESSAGE_SENDER_KEY);
        throw null;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.senderId;
    }

    @DexIgnore
    public final String getTitle() {
        String str = this.title;
        if (str != null) {
            return str;
        }
        Wg6.n("title");
        throw null;
    }

    @DexIgnore
    public final int getUid() {
        return this.uid;
    }

    @DexIgnore
    public final void setMessage(String str) {
        Wg6.c(str, "<set-?>");
        this.message = str;
    }

    @DexIgnore
    public final void setNotificationControlActionStatus(NotificationControlActionStatus notificationControlActionStatus2) {
        Wg6.c(notificationControlActionStatus2, "<set-?>");
        this.notificationControlActionStatus = notificationControlActionStatus2;
    }

    @DexIgnore
    public final void setNotificationControlActionType(NotificationControlActionType notificationControlActionType2) {
        Wg6.c(notificationControlActionType2, "<set-?>");
        this.notificationControlActionType = notificationControlActionType2;
    }

    @DexIgnore
    public final void setNotificationFlags(List<ANotificationFlag> list) {
        Wg6.c(list, "<set-?>");
        this.notificationFlags = list;
    }

    @DexIgnore
    public final void setNotificationType(ANotificationType aNotificationType) {
        Wg6.c(aNotificationType, "<set-?>");
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public final void setSender(String str) {
        Wg6.c(str, "<set-?>");
        this.sender = str;
    }

    @DexIgnore
    public final void setSenderId(int i) {
        this.senderId = i;
    }

    @DexIgnore
    public final void setTitle(String str) {
        Wg6.c(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setUid(int i) {
        this.uid = i;
    }

    @DexIgnore
    public final void toExistedNotification() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list == null) {
            return;
        }
        if (list == null) {
            Wg6.n("notificationFlags");
            throw null;
        } else if (!list.contains(ANotificationFlag.PRE_EXISTING)) {
            List<ANotificationFlag> list2 = this.notificationFlags;
            if (list2 != null) {
                list2.add(ANotificationFlag.PRE_EXISTING);
            } else {
                Wg6.n("notificationFlags");
                throw null;
            }
        }
    }

    @DexIgnore
    public String toRemoteLogString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public abstract Zn1 toSDKNotification();

    @DexIgnore
    public final List<NotificationFlag> toSDKNotificationFlags(List<? extends ANotificationFlag> list) {
        Wg6.c(list, "$this$toSDKNotificationFlags");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKNotificationFlag());
        }
        return arrayList;
    }

    @DexIgnore
    public final void toSilentNotification() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list == null) {
            return;
        }
        if (list == null) {
            Wg6.n("notificationFlags");
            throw null;
        } else if (!list.contains(ANotificationFlag.SILENT)) {
            List<ANotificationFlag> list2 = this.notificationFlags;
            if (list2 != null) {
                list2.add(ANotificationFlag.SILENT);
            } else {
                Wg6.n("notificationFlags");
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }
}
