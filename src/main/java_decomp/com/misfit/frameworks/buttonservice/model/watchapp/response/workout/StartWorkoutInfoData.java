package com.misfit.frameworks.buttonservice.model.watchapp.response.workout;

import android.os.Parcel;
import com.fossil.Nt1;
import com.fossil.Pq1;
import com.fossil.Ry1;
import com.fossil.Ut1;
import com.mapped.Dd0;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StartWorkoutInfoData extends DeviceAppResponse {
    @DexIgnore
    public Dd0 deviceMessageType; // = Dd0.SUCCESS;
    @DexIgnore
    public String message; // = "";
    @DexIgnore
    public Pq1 startRequest;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartWorkoutInfoData(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
        this.deviceMessageType = Dd0.values()[parcel.readInt()];
        this.startRequest = (Pq1) parcel.readParcelable(Pq1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartWorkoutInfoData(Pq1 pq1, String str, Dd0 dd0) {
        super(E90.WORKOUT_START);
        Wg6.c(pq1, "startRequest");
        Wg6.c(str, "message");
        Wg6.c(dd0, "deviceMessageType");
        this.message = str;
        this.deviceMessageType = dd0;
        this.startRequest = pq1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (x90 instanceof Pq1) {
            return new Ut1((Pq1) x90, new Nt1(this.message, this.deviceMessageType));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
        parcel.writeParcelable(this.startRequest, i);
    }
}
