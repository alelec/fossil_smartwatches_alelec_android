package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import com.fossil.Im7;
import com.fossil.Rq1;
import com.fossil.Ry1;
import com.fossil.Yt1;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.mapped.Z80;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherInfoWatchAppResponse extends DeviceAppResponse {
    @DexIgnore
    public /* final */ List<WeatherWatchAppInfo> listOfWeatherInfo;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.listOfWeatherInfo = arrayList;
        parcel.readTypedList(arrayList, WeatherWatchAppInfo.CREATOR);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3) {
        super(E90.WEATHER_WATCH_APP);
        Wg6.c(weatherWatchAppInfo, "firstWeatherWatchAppInfo");
        ArrayList arrayList = new ArrayList();
        this.listOfWeatherInfo = arrayList;
        arrayList.add(weatherWatchAppInfo);
        if (weatherWatchAppInfo2 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo2);
        }
        if (weatherWatchAppInfo3 != null) {
            this.listOfWeatherInfo.add(weatherWatchAppInfo3);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WeatherInfoWatchAppResponse(WeatherWatchAppInfo weatherWatchAppInfo, WeatherWatchAppInfo weatherWatchAppInfo2, WeatherWatchAppInfo weatherWatchAppInfo3, int i, Qg6 qg6) {
        this(weatherWatchAppInfo, (i & 2) != 0 ? null : weatherWatchAppInfo2, (i & 4) != 0 ? null : weatherWatchAppInfo3);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherAppInfo());
        }
        Object[] array = arrayList.toArray(new Z80[0]);
        if (array != null) {
            return new Yt1((Z80[]) array);
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Rq1)) {
            return null;
        }
        List<WeatherWatchAppInfo> list = this.listOfWeatherInfo;
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherAppInfo());
        }
        Rq1 rq1 = (Rq1) x90;
        Object[] array = arrayList.toArray(new Z80[0]);
        if (array != null) {
            return new Yt1(rq1, (Z80[]) array);
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeTypedList(this.listOfWeatherInfo);
    }
}
