package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkipFirmwareData extends FirmwareData {
    @DexIgnore
    public SkipFirmwareData() {
        super("", "", "");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SkipFirmwareData(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
    }
}
