package com.misfit.frameworks.buttonservice.model.calibration;

import com.mapped.Kc6;
import com.mapped.N50;
import com.mapped.P50;
import com.mapped.Q50;
import com.mapped.Qg6;
import com.mapped.R50;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCalibrationObj {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ N50 handId;
    @DexIgnore
    public /* final */ P50 handMovingDirection;
    @DexIgnore
    public /* final */ Q50 handMovingSpeed;
    @DexIgnore
    public /* final */ R50 handMovingType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$2;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$3;

            /*
            static {
                int[] iArr = new int[CalibrationEnums.MovingType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[CalibrationEnums.MovingType.DISTANCE.ordinal()] = 1;
                $EnumSwitchMapping$0[CalibrationEnums.MovingType.POSITION.ordinal()] = 2;
                int[] iArr2 = new int[CalibrationEnums.HandId.values().length];
                $EnumSwitchMapping$1 = iArr2;
                iArr2[CalibrationEnums.HandId.HOUR.ordinal()] = 1;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.MINUTE.ordinal()] = 2;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.SUB_EYE.ordinal()] = 3;
                int[] iArr3 = new int[CalibrationEnums.Direction.values().length];
                $EnumSwitchMapping$2 = iArr3;
                iArr3[CalibrationEnums.Direction.CLOCKWISE.ordinal()] = 1;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.COUNTER_CLOCKWISE.ordinal()] = 2;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.SHORTEST_PATH.ordinal()] = 3;
                int[] iArr4 = new int[CalibrationEnums.Speed.values().length];
                $EnumSwitchMapping$3 = iArr4;
                iArr4[CalibrationEnums.Speed.SIXTEENTH.ordinal()] = 1;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.EIGHTH.ordinal()] = 2;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.QUARTER.ordinal()] = 3;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.HALF.ordinal()] = 4;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.FULL.ordinal()] = 5;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final DianaCalibrationObj consume(HandCalibrationObj handCalibrationObj) {
            R50 r50;
            N50 n50;
            P50 p50;
            Q50 q50;
            Wg6.c(handCalibrationObj, "handCalibrationObj");
            int i = WhenMappings.$EnumSwitchMapping$0[handCalibrationObj.getMovingType().ordinal()];
            if (i == 1) {
                r50 = R50.DISTANCE;
            } else if (i == 2) {
                r50 = R50.POSITION;
            } else {
                throw new Kc6();
            }
            int i2 = WhenMappings.$EnumSwitchMapping$1[handCalibrationObj.getHandId().ordinal()];
            if (i2 == 1) {
                n50 = N50.HOUR;
            } else if (i2 == 2) {
                n50 = N50.MINUTE;
            } else if (i2 == 3) {
                n50 = N50.SUB_EYE;
            } else {
                throw new Kc6();
            }
            int i3 = WhenMappings.$EnumSwitchMapping$2[handCalibrationObj.getDirection().ordinal()];
            if (i3 == 1) {
                p50 = P50.CLOCKWISE;
            } else if (i3 == 2) {
                p50 = P50.COUNTER_CLOCKWISE;
            } else if (i3 == 3) {
                p50 = P50.SHORTEST_PATH;
            } else {
                throw new Kc6();
            }
            int i4 = WhenMappings.$EnumSwitchMapping$3[handCalibrationObj.getSpeed().ordinal()];
            if (i4 == 1) {
                q50 = Q50.SIXTEENTH;
            } else if (i4 == 2) {
                q50 = Q50.EIGHTH;
            } else if (i4 == 3) {
                q50 = Q50.QUARTER;
            } else if (i4 == 4) {
                q50 = Q50.HALF;
            } else if (i4 == 5) {
                q50 = Q50.FULL;
            } else {
                throw new Kc6();
            }
            return new DianaCalibrationObj(r50, n50, p50, q50, handCalibrationObj.getDegree());
        }
    }

    @DexIgnore
    public DianaCalibrationObj(R50 r50, N50 n50, P50 p50, Q50 q50, int i) {
        Wg6.c(r50, "handMovingType");
        Wg6.c(n50, "handId");
        Wg6.c(p50, "handMovingDirection");
        Wg6.c(q50, "handMovingSpeed");
        this.handMovingType = r50;
        this.handId = n50;
        this.handMovingDirection = p50;
        this.handMovingSpeed = q50;
        this.degree = i;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final N50 getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final P50 getHandMovingDirection() {
        return this.handMovingDirection;
    }

    @DexIgnore
    public final Q50 getHandMovingSpeed() {
        return this.handMovingSpeed;
    }

    @DexIgnore
    public final R50 getHandMovingType() {
        return this.handMovingType;
    }
}
