package com.misfit.frameworks.buttonservice.model.vibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import com.mapped.A70;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VibrationStrengthObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<VibrationStrengthObj> CREATOR; // = new VibrationStrengthObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ boolean isDefaultValue;
    @DexIgnore
    public /* final */ int vibrationStrengthLevel;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[A70.Ai.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[A70.Ai.LOW.ordinal()] = 1;
                $EnumSwitchMapping$0[A70.Ai.MEDIUM.ordinal()] = 2;
                $EnumSwitchMapping$0[A70.Ai.HIGH.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ VibrationStrengthObj consumeSDKVibrationStrengthLevel$default(Companion companion, A70.Ai ai, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return companion.consumeSDKVibrationStrengthLevel(ai, z);
        }

        @DexIgnore
        public final VibrationStrengthObj consumeSDKVibrationStrengthLevel(A70.Ai ai, boolean z) {
            int i = 3;
            Wg6.c(ai, "vibrationStrengthLevel");
            int i2 = WhenMappings.$EnumSwitchMapping$0[ai.ordinal()];
            if (i2 == 1) {
                i = 1;
            } else if (i2 == 2 || i2 != 3) {
                i = 2;
            }
            return new VibrationStrengthObj(i, z);
        }
    }

    @DexIgnore
    public VibrationStrengthObj(int i, boolean z) {
        this.vibrationStrengthLevel = i;
        this.isDefaultValue = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ VibrationStrengthObj(int i, boolean z, int i2, Qg6 qg6) {
        this(i, (i2 & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public VibrationStrengthObj(android.os.Parcel r4) {
        /*
            r3 = this;
            r0 = 1
            java.lang.String r1 = "parcel"
            com.mapped.Wg6.c(r4, r1)
            int r1 = r4.readInt()
            int r2 = r4.readInt()
            if (r2 != r0) goto L_0x0014
        L_0x0010:
            r3.<init>(r1, r0)
            return
        L_0x0014:
            r0 = 0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getVibrationStrengthLevel() {
        return this.vibrationStrengthLevel;
    }

    @DexIgnore
    public final boolean isDefaultValue() {
        return this.isDefaultValue;
    }

    @DexIgnore
    public final A70.Ai toSDKVibrationStrengthLevel() {
        int i = this.vibrationStrengthLevel;
        return i != 1 ? i != 2 ? i != 3 ? A70.Ai.MEDIUM : A70.Ai.HIGH : A70.Ai.MEDIUM : A70.Ai.LOW;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(VibrationStrengthObj.class.getName());
        parcel.writeInt(this.vibrationStrengthLevel);
        parcel.writeInt(this.isDefaultValue ? 1 : 0);
    }
}
