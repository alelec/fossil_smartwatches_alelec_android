package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.util.Log;
import com.fossil.Hj4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLECustomizationDeserializer implements Hu3<BLECustomization> {
    @DexIgnore
    @Override // com.mapped.Hu3
    public BLECustomization deserialize(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        Log.d(BLECustomizationDeserializer.class.getName(), jsonElement.toString());
        return jsonElement.d().p("type").b() != 1 ? new BLENonCustomization() : (BLECustomization) new Gson().g(jsonElement, BLEGoalTrackingCustomization.class);
    }
}
