package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserProfile implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeInMinute;
    @DexIgnore
    public long calories;
    @DexIgnore
    public long caloriesGoal;
    @DexIgnore
    public long currentSteps;
    @DexIgnore
    public int deepSleepInMinute;
    @DexIgnore
    public UserDisplayUnit displayUnit;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public long goalSteps;
    @DexIgnore
    public InactiveNudgeData inactiveNudgeData;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightSleepInMinute;
    @DexIgnore
    public int originalSyncMode;
    @DexIgnore
    public ReplyMessageMappingGroup replyMessageMapping;
    @DexIgnore
    public long syncId;
    @DexIgnore
    public int syncMode;
    @DexIgnore
    public int totalSleepInMinute;
    @DexIgnore
    public UserBiometricData userBiometricData;
    @DexIgnore
    public WorkoutDetectionSetting workoutDetectionSetting;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<UserProfile> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserProfile createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new UserProfile(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserProfile[] newArray(int i) {
            return new UserProfile[i];
        }
    }

    @DexIgnore
    public UserProfile(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        UserBiometricData userBiometricData2 = (UserBiometricData) parcel.readParcelable(UserBiometricData.class.getClassLoader());
        this.userBiometricData = userBiometricData2 == null ? new UserBiometricData() : userBiometricData2;
        this.goalSteps = parcel.readLong();
        this.currentSteps = parcel.readLong();
        this.activeMinuteGoal = parcel.readInt();
        this.activeMinute = parcel.readInt();
        this.caloriesGoal = parcel.readLong();
        this.calories = parcel.readLong();
        this.distanceInCentimeter = parcel.readLong();
        if (parcel.readByte() != 0) {
            this.displayUnit = (UserDisplayUnit) parcel.readParcelable(DisplayUnit.class.getClassLoader());
        }
        this.totalSleepInMinute = parcel.readInt();
        this.awakeInMinute = parcel.readInt();
        this.lightSleepInMinute = parcel.readInt();
        this.deepSleepInMinute = parcel.readInt();
        if (parcel.readByte() != 0) {
            this.inactiveNudgeData = (InactiveNudgeData) parcel.readParcelable(InactiveNudgeData.class.getClassLoader());
        }
        this.isNewDevice = parcel.readByte() != 0;
        this.syncMode = parcel.readInt();
        this.syncId = parcel.readLong();
        this.originalSyncMode = parcel.readInt();
        Parcelable readParcelable = parcel.readParcelable(ReplyMessageMappingGroup.class.getClassLoader());
        if (readParcelable != null) {
            this.replyMessageMapping = (ReplyMessageMappingGroup) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(WorkoutDetectionSetting.class.getClassLoader());
            if (readParcelable2 != null) {
                this.workoutDetectionSetting = (WorkoutDetectionSetting) readParcelable2;
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public UserProfile(UserBiometricData userBiometricData2, long j, long j2, int i, int i2, long j3, long j4, long j5, UserDisplayUnit userDisplayUnit, int i3, int i4, int i5, int i6, InactiveNudgeData inactiveNudgeData2, boolean z, int i7, int i8, long j6, ReplyMessageMappingGroup replyMessageMappingGroup, WorkoutDetectionSetting workoutDetectionSetting2) {
        Wg6.c(userBiometricData2, "userBiometricData");
        Wg6.c(replyMessageMappingGroup, "replyMessageMappingGroup");
        Wg6.c(workoutDetectionSetting2, "workoutDetectionSetting");
        this.userBiometricData = userBiometricData2;
        this.goalSteps = j;
        this.currentSteps = j2;
        this.activeMinuteGoal = i;
        this.activeMinute = i2;
        this.caloriesGoal = j3;
        this.calories = j4;
        this.distanceInCentimeter = j5;
        this.displayUnit = userDisplayUnit;
        this.totalSleepInMinute = i3;
        this.awakeInMinute = i4;
        this.lightSleepInMinute = i5;
        this.deepSleepInMinute = i6;
        this.inactiveNudgeData = inactiveNudgeData2;
        this.isNewDevice = z;
        this.syncMode = i7;
        this.syncId = j6;
        this.originalSyncMode = i8;
        this.replyMessageMapping = replyMessageMappingGroup;
        this.workoutDetectionSetting = workoutDetectionSetting2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getActiveMinute() {
        return this.activeMinute;
    }

    @DexIgnore
    public final int getActiveMinuteGoal() {
        return this.activeMinuteGoal;
    }

    @DexIgnore
    public final int getAwakeInMinute() {
        return this.awakeInMinute;
    }

    @DexIgnore
    public final long getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final long getCaloriesGoal() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final long getCurrentSteps() {
        return this.currentSteps;
    }

    @DexIgnore
    public final int getDeepSleepInMinute() {
        return this.deepSleepInMinute;
    }

    @DexIgnore
    public final UserDisplayUnit getDisplayUnit() {
        return this.displayUnit;
    }

    @DexIgnore
    public final long getDistanceInCentimeter() {
        return this.distanceInCentimeter;
    }

    @DexIgnore
    public final long getGoalSteps() {
        return this.goalSteps;
    }

    @DexIgnore
    public final InactiveNudgeData getInactiveNudgeData() {
        return this.inactiveNudgeData;
    }

    @DexIgnore
    public final int getLightSleepInMinute() {
        return this.lightSleepInMinute;
    }

    @DexIgnore
    public final int getOriginalSyncMode() {
        return this.originalSyncMode;
    }

    @DexIgnore
    public final ReplyMessageMappingGroup getReplyMessageMapping() {
        return this.replyMessageMapping;
    }

    @DexIgnore
    public final long getSyncId() {
        return this.syncId;
    }

    @DexIgnore
    public final int getSyncMode() {
        return this.syncMode;
    }

    @DexIgnore
    public final int getTotalSleepInMinute() {
        return this.totalSleepInMinute;
    }

    @DexIgnore
    public final UserBiometricData getUserBiometricData() {
        return this.userBiometricData;
    }

    @DexIgnore
    public final WorkoutDetectionSetting getWorkoutDetectionSetting() {
        return this.workoutDetectionSetting;
    }

    @DexIgnore
    public final boolean isNewDevice() {
        return this.isNewDevice;
    }

    @DexIgnore
    public final void setActiveMinute(int i) {
        this.activeMinute = i;
    }

    @DexIgnore
    public final void setActiveMinuteGoal(int i) {
        this.activeMinuteGoal = i;
    }

    @DexIgnore
    public final void setAwakeInMinute(int i) {
        this.awakeInMinute = i;
    }

    @DexIgnore
    public final void setCalories(long j) {
        this.calories = j;
    }

    @DexIgnore
    public final void setCaloriesGoal(long j) {
        this.caloriesGoal = j;
    }

    @DexIgnore
    public final void setCurrentSteps(long j) {
        this.currentSteps = j;
    }

    @DexIgnore
    public final void setDeepSleepInMinute(int i) {
        this.deepSleepInMinute = i;
    }

    @DexIgnore
    public final void setDisplayUnit(UserDisplayUnit userDisplayUnit) {
        this.displayUnit = userDisplayUnit;
    }

    @DexIgnore
    public final void setDistanceInCentimeter(long j) {
        this.distanceInCentimeter = j;
    }

    @DexIgnore
    public final void setGoalSteps(long j) {
        this.goalSteps = j;
    }

    @DexIgnore
    public final void setInactiveNudgeData(InactiveNudgeData inactiveNudgeData2) {
        this.inactiveNudgeData = inactiveNudgeData2;
    }

    @DexIgnore
    public final void setLightSleepInMinute(int i) {
        this.lightSleepInMinute = i;
    }

    @DexIgnore
    public final void setNewDevice(boolean z) {
        this.isNewDevice = z;
    }

    @DexIgnore
    public final void setOriginalSyncMode(int i) {
        this.originalSyncMode = i;
    }

    @DexIgnore
    public final void setSyncId(long j) {
        this.syncId = j;
    }

    @DexIgnore
    public final void setSyncMode(int i) {
        this.syncMode = i;
    }

    @DexIgnore
    public final void setTotalSleepInMinute(int i) {
        this.totalSleepInMinute = i;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "dest");
        parcel.writeParcelable(this.userBiometricData, i);
        parcel.writeLong(this.goalSteps);
        parcel.writeLong(this.currentSteps);
        parcel.writeInt(this.activeMinuteGoal);
        parcel.writeInt(this.activeMinute);
        parcel.writeLong(this.caloriesGoal);
        parcel.writeLong(this.calories);
        parcel.writeLong(this.distanceInCentimeter);
        if (this.displayUnit != null) {
            parcel.writeByte((byte) 1);
            parcel.writeParcelable(this.displayUnit, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.totalSleepInMinute);
        parcel.writeInt(this.awakeInMinute);
        parcel.writeInt(this.lightSleepInMinute);
        parcel.writeInt(this.deepSleepInMinute);
        if (this.inactiveNudgeData != null) {
            parcel.writeByte((byte) 1);
            parcel.writeParcelable(this.inactiveNudgeData, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeByte(this.isNewDevice ? (byte) 1 : 0);
        parcel.writeInt(this.syncMode);
        parcel.writeLong(this.syncId);
        parcel.writeInt(this.originalSyncMode);
        parcel.writeParcelable(this.replyMessageMapping, i);
        parcel.writeParcelable(this.workoutDetectionSetting, i);
    }
}
