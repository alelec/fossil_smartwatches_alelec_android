package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.FirmwareData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingUpdateFWResponse extends PairingResponse {
    @DexIgnore
    public /* final */ FirmwareData firmwareData;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingUpdateFWResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        FirmwareData firmwareData2 = (FirmwareData) parcel.readParcelable(FirmwareData.class.getClassLoader());
        this.firmwareData = firmwareData2 == null ? new FirmwareData() : firmwareData2;
    }

    @DexIgnore
    public PairingUpdateFWResponse(FirmwareData firmwareData2) {
        Wg6.c(firmwareData2, "firmwareData");
        this.firmwareData = firmwareData2;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.pairing.PairingResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.firmwareData, i);
    }
}
