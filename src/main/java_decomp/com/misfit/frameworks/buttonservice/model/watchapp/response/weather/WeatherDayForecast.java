package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.K70;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.X80;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherDayForecast implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ WeatherWeekDay weekDay;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new WeatherDayForecast(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public enum WeatherWeekDay {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherWeekDay.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherWeekDay.SUNDAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherWeekDay.MONDAY.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherWeekDay.TUESDAY.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherWeekDay.WEDNESDAY.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherWeekDay.THURSDAY.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherWeekDay.FRIDAY.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherWeekDay.SATURDAY.ordinal()] = 7;
            }
            */
        }

        @DexIgnore
        public final K70 toSDKWeekDay() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return K70.SUNDAY;
                case 2:
                    return K70.MONDAY;
                case 3:
                    return K70.TUESDAY;
                case 4:
                    return K70.WEDNESDAY;
                case 5:
                    return K70.THURSDAY;
                case 6:
                    return K70.FRIDAY;
                case 7:
                    return K70.SATURDAY;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore
    public WeatherDayForecast(float f, float f2, WeatherComplicationAppInfo.WeatherCondition weatherCondition2, WeatherWeekDay weatherWeekDay) {
        Wg6.c(weatherCondition2, "weatherCondition");
        Wg6.c(weatherWeekDay, "weekDay");
        this.highTemperature = f;
        this.lowTemperature = f2;
        this.weatherCondition = weatherCondition2;
        this.weekDay = weatherWeekDay;
    }

    @DexIgnore
    public WeatherDayForecast(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        this.highTemperature = parcel.readFloat();
        this.lowTemperature = parcel.readFloat();
        this.weatherCondition = WeatherComplicationAppInfo.WeatherCondition.values()[parcel.readInt()];
        this.weekDay = WeatherWeekDay.values()[parcel.readInt()];
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final X80 toSDKWeatherDayForecast() {
        return new X80(this.weekDay.toSDKWeekDay(), this.highTemperature, this.lowTemperature, this.weatherCondition.toSdkWeatherCondition());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeFloat(this.highTemperature);
        parcel.writeFloat(this.lowTemperature);
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeInt(this.weekDay.ordinal());
    }
}
