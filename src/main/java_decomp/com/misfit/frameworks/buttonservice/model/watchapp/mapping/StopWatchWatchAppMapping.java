package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.Wo1;
import com.mapped.O80;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StopWatchWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public StopWatchWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StopWatchWatchAppMapping(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.watchapp.mapping.StopWatchWatchAppMapping A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping.getMType():int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        Wg6.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public O80 toSDKSetting() {
        return new Wo1();
    }
}
