package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShineDevice extends Device implements Parcelable, Comparable<Object> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ShineDevice> CREATOR; // = new ShineDevice$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ShineDevice clone(ScannedDevice scannedDevice) {
            Wg6.c(scannedDevice, "device");
            return new ShineDevice(scannedDevice.getDeviceSerial(), scannedDevice.getDeviceName(), scannedDevice.getDeviceMACAddress(), scannedDevice.getRssi(), scannedDevice.getFastPairIdInHex());
        }
    }

    @DexIgnore
    public ShineDevice(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        this.serial = parcel.readString();
        this.name = parcel.readString();
        this.macAddress = parcel.readString();
        this.rssi = parcel.readInt();
        this.fastPairId = parcel.readString();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShineDevice(String str, String str2, String str3, int i, String str4) {
        super(str, str2, str3, i, str4);
        Wg6.c(str, "serial");
        Wg6.c(str2, "name");
        Wg6.c(str3, "macAddress");
        Wg6.c(str4, "fastPairId");
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        Wg6.c(obj, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(obj instanceof ShineDevice)) {
            return 1;
        }
        ShineDevice shineDevice = (ShineDevice) obj;
        if (shineDevice.getRssi() == getRssi()) {
            return 0;
        }
        return shineDevice.getRssi() <= getRssi() ? -1 : 1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "dest");
        parcel.writeString(this.serial);
        parcel.writeString(this.name);
        parcel.writeString(this.macAddress);
        parcel.writeInt(this.rssi);
        parcel.writeString(this.fastPairId);
    }
}
