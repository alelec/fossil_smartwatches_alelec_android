package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.Dq1;
import com.fossil.Ht1;
import com.fossil.Ry1;
import com.fossil.Xs1;
import com.mapped.E90;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCListChallengeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public List<Xs1> challenges;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        this.challenges = arrayList;
        parcel.readList(arrayList, Xs1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCListChallengeWatchAppInfo(List<Xs1> list) {
        super(E90.BUDDY_CHALLENGE_LIST_CHALLENGES);
        Wg6.c(list, "challenges");
        this.challenges = new ArrayList();
        this.challenges = list;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Dq1)) {
            return null;
        }
        Dq1 dq1 = (Dq1) x90;
        Object[] array = this.challenges.toArray(new Xs1[0]);
        if (array != null) {
            return new Ht1(dq1, (Xs1[]) array);
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeList(this.challenges);
    }
}
