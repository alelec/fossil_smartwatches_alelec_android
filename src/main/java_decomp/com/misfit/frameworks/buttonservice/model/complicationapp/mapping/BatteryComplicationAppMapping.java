package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.Am1;
import com.fossil.Bt1;
import com.fossil.Dm1;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BatteryComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public BatteryComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getBATTERY());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BatteryComplicationAppMapping(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.complicationapp.mapping.BatteryComplicationAppMapping A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping.getMType():int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        Wg6.b(sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping
    public Dm1 toSDKSetting(boolean z) {
        return new Am1(new Bt1(false));
    }
}
