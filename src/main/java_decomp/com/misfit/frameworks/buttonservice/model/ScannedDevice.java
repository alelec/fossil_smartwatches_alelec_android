package com.misfit.frameworks.buttonservice.model;

import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ScannedDevice {
    @DexIgnore
    public String deviceMACAddress;
    @DexIgnore
    public String deviceName;
    @DexIgnore
    public String deviceSerial;
    @DexIgnore
    public String fastPairIdInHex;
    @DexIgnore
    public int rssi;
    @DexIgnore
    public long time;

    @DexIgnore
    public ScannedDevice(String str, String str2, String str3, int i, String str4) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        Wg6.c(str2, "deviceName");
        Wg6.c(str3, "deviceMACAddress");
        Wg6.c(str4, "fastPairIdInHex");
        this.deviceSerial = str;
        this.deviceName = str2;
        this.deviceMACAddress = str3;
        this.rssi = i;
        this.fastPairIdInHex = str4;
        this.time = System.currentTimeMillis();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ScannedDevice(String str, String str2, String str3, int i, String str4, int i2, Qg6 qg6) {
        this(str, str2, str3, i, (i2 & 16) != 0 ? "" : str4);
    }

    @DexIgnore
    public final void clone(ScannedDevice scannedDevice) {
        Wg6.c(scannedDevice, "from");
        this.deviceSerial = scannedDevice.deviceSerial;
        this.deviceMACAddress = scannedDevice.deviceMACAddress;
        this.time = scannedDevice.time;
        this.rssi = scannedDevice.rssi;
        this.fastPairIdInHex = scannedDevice.fastPairIdInHex;
    }

    @DexIgnore
    public final String getDeviceMACAddress() {
        return this.deviceMACAddress;
    }

    @DexIgnore
    public final String getDeviceName() {
        return this.deviceName;
    }

    @DexIgnore
    public final String getDeviceSerial() {
        return this.deviceSerial;
    }

    @DexIgnore
    public final String getFastPairIdInHex() {
        return this.fastPairIdInHex;
    }

    @DexIgnore
    public final int getRssi() {
        return this.rssi;
    }

    @DexIgnore
    public final long getTime() {
        return this.time;
    }

    @DexIgnore
    public final void setDeviceMACAddress(String str) {
        Wg6.c(str, "<set-?>");
        this.deviceMACAddress = str;
    }

    @DexIgnore
    public final void setDeviceName(String str) {
        Wg6.c(str, "<set-?>");
        this.deviceName = str;
    }

    @DexIgnore
    public final void setDeviceSerial(String str) {
        Wg6.c(str, "<set-?>");
        this.deviceSerial = str;
    }

    @DexIgnore
    public final void setFastPairIdInHex(String str) {
        Wg6.c(str, "<set-?>");
        this.fastPairIdInHex = str;
    }

    @DexIgnore
    public final void setRssi(int i) {
        this.rssi = i;
    }

    @DexIgnore
    public final void setTime(long j) {
        this.time = j;
    }
}
