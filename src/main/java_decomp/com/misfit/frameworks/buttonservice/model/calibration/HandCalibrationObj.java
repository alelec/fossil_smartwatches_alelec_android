package com.misfit.frameworks.buttonservice.model.calibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HandCalibrationObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<HandCalibrationObj> CREATOR; // = new HandCalibrationObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ CalibrationEnums.Direction direction;
    @DexIgnore
    public /* final */ CalibrationEnums.HandId handId;
    @DexIgnore
    public /* final */ CalibrationEnums.MovingType movingType;
    @DexIgnore
    public /* final */ CalibrationEnums.Speed speed;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HandCalibrationObj(android.os.Parcel r7) {
        /*
            r6 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r7, r0)
            int r0 = r7.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$HandId r1 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.HandId.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.HandId.\u2026omValue(parcel.readInt())"
            com.mapped.Wg6.b(r1, r0)
            int r0 = r7.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$MovingType r2 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.MovingType.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.MovingT\u2026omValue(parcel.readInt())"
            com.mapped.Wg6.b(r2, r0)
            int r0 = r7.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$Direction r3 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.Direction.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.Directi\u2026omValue(parcel.readInt())"
            com.mapped.Wg6.b(r3, r0)
            int r0 = r7.readInt()
            com.misfit.frameworks.buttonservice.model.CalibrationEnums$Speed r4 = com.misfit.frameworks.buttonservice.model.CalibrationEnums.Speed.fromValue(r0)
            java.lang.String r0 = "CalibrationEnums.Speed.fromValue(parcel.readInt())"
            com.mapped.Wg6.b(r4, r0)
            int r5 = r7.readInt()
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public HandCalibrationObj(CalibrationEnums.HandId handId2, CalibrationEnums.MovingType movingType2, CalibrationEnums.Direction direction2, CalibrationEnums.Speed speed2, int i) {
        Wg6.c(handId2, "handId");
        Wg6.c(movingType2, "movingType");
        Wg6.c(direction2, "direction");
        Wg6.c(speed2, PlaceManager.PARAM_SPEED);
        this.handId = handId2;
        this.movingType = movingType2;
        this.direction = direction2;
        this.speed = speed2;
        this.degree = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final CalibrationEnums.Direction getDirection() {
        return this.direction;
    }

    @DexIgnore
    public final CalibrationEnums.HandId getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final CalibrationEnums.MovingType getMovingType() {
        return this.movingType;
    }

    @DexIgnore
    public final CalibrationEnums.Speed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(HandCalibrationObj.class.getName());
        parcel.writeInt(this.handId.getValue());
        parcel.writeInt(this.movingType.getValue());
        parcel.writeInt(this.direction.getValue());
        parcel.writeInt(this.speed.getValue());
        parcel.writeInt(this.degree);
    }
}
