package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import com.fossil.Hj4;
import com.fossil.Zi4;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingDeserializer implements Hu3<WatchAppMapping> {
    @DexIgnore
    @Override // com.mapped.Hu3
    public WatchAppMapping deserialize(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        Wg6.c(jsonElement, "json");
        Wg6.c(type, "typeOfT");
        Wg6.c(gu3, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = WatchAppMappingDeserializer.class.getName();
        Wg6.b(name, "WatchAppMappingDeserializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement p = jsonElement.d().p(WatchAppMapping.Companion.getFIELD_TYPE());
        Wg6.b(p, "jsonType");
        int b = p.b();
        Zi4 zi4 = new Zi4();
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, DiagnosticsWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WellnessWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WorkoutWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, NoneWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getMUSIC()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, MusicWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getNOTIFICATION_PANEL()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, NotificationPanelWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, StopWatchWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, TimerWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, WeatherWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, CommuteTimeWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getBUDDY_CHALLENGE()) {
            return (WatchAppMapping) zi4.d().g(jsonElement, BuddyChallengeMapping.class);
        }
        return null;
    }
}
