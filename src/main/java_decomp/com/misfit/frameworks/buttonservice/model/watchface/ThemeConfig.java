package com.misfit.frameworks.buttonservice.model.watchface;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeConfig {
    @DexIgnore
    public /* final */ String pathThemeData;
    @DexIgnore
    public /* final */ ThemeData themeData;

    @DexIgnore
    public ThemeConfig() {
        this(null, null, 3, null);
    }

    @DexIgnore
    public ThemeConfig(String str, ThemeData themeData2) {
        this.pathThemeData = str;
        this.themeData = themeData2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ThemeConfig(String str, ThemeData themeData2, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : themeData2);
    }

    @DexIgnore
    public static /* synthetic */ ThemeConfig copy$default(ThemeConfig themeConfig, String str, ThemeData themeData2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = themeConfig.pathThemeData;
        }
        if ((i & 2) != 0) {
            themeData2 = themeConfig.themeData;
        }
        return themeConfig.copy(str, themeData2);
    }

    @DexIgnore
    public final String component1() {
        return this.pathThemeData;
    }

    @DexIgnore
    public final ThemeData component2() {
        return this.themeData;
    }

    @DexIgnore
    public final ThemeConfig copy(String str, ThemeData themeData2) {
        return new ThemeConfig(str, themeData2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ThemeConfig) {
                ThemeConfig themeConfig = (ThemeConfig) obj;
                if (!Wg6.a(this.pathThemeData, themeConfig.pathThemeData) || !Wg6.a(this.themeData, themeConfig.themeData)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getPathThemeData() {
        return this.pathThemeData;
    }

    @DexIgnore
    public final ThemeData getThemeData() {
        return this.themeData;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.pathThemeData;
        int hashCode = str != null ? str.hashCode() : 0;
        ThemeData themeData2 = this.themeData;
        if (themeData2 != null) {
            i = themeData2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ThemeConfig(pathThemeData=" + this.pathThemeData + ", themeData=" + this.themeData + ")";
    }
}
