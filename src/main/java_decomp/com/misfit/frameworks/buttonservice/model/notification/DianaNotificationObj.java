package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Hm7;
import com.fossil.Ho1;
import com.fossil.Mo1;
import com.fossil.Zn1;
import com.mapped.NotificationFlag;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.sina.weibo.sdk.WeiboAppManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaNotificationObj extends NotificationBaseObj {
    @DexIgnore
    public AApplicationName notificationApp;
    @DexIgnore
    public long notifyAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class AApplicationName implements Parcelable {
        @DexIgnore
        public static /* final */ AApplicationName AIRBNB; // = new AApplicationName("Airbnb", "com.airbnb.android", "icAirbnb.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName AMERICAN_AIRLINES; // = new AApplicationName("American Airlines", "com.aa.android", "icAmericanairlines.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName BADOO; // = new AApplicationName("Badoo", "com.badoo.mobile", "icBadoo.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName BANK_OF_AMERICA; // = new AApplicationName("Bank of America", "com.bankofamerica.eventsplanner", "icBankofamerica.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName CAPITAL_ONE; // = new AApplicationName("Capital One", "com.ie.capitalone.uk", "icCapitalone.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName CITIBANK; // = new AApplicationName("Citi Mobile", "com.citi.citimobile", "icCitigroup.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ Parcelable.Creator<AApplicationName> CREATOR; // = new DianaNotificationObj$AApplicationName$Companion$CREATOR$Anon1();
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public static /* final */ AApplicationName DELTA; // = new AApplicationName("Fly Delta", "com.delta.mobile.android", "icDelta.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName ESPN; // = new AApplicationName("ESPN", "com.espn.score_center", "icEspn.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName FACEBOOK; // = new AApplicationName("Facebook", "com.facebook.katana", "icFacebook.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName FLICKR; // = new AApplicationName("Flickr", "com.flickr.android", "icFlickr.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName FOSSIL;
        @DexIgnore
        public static /* final */ AApplicationName FOURSQUARE; // = new AApplicationName("Foursquare", "com.joelapenna.foursquared", "icFoursquare.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName GMAIL; // = new AApplicationName("Gmail", "com.google.android.gm", "icEmail.icon", NotificationBaseObj.ANotificationType.EMAIL);
        @DexIgnore
        public static /* final */ AApplicationName GOOGLE_CALENDAR; // = new AApplicationName("Google Calendar", "com.google.android.calendar", "icCalendar.icon", NotificationBaseObj.ANotificationType.CALENDAR);
        @DexIgnore
        public static /* final */ AApplicationName GOOGLE_FIT; // = new AApplicationName("Google Fit", "com.google.android.apps.fitness", "", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName GOOGLE_PLUS; // = new AApplicationName("Google+", "com.google.android.apps.plus", "icGoogleplus.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName GRUBHUB; // = new AApplicationName("Grubhub", "com.grubhub.androido", "food_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName HANGOUTS; // = new AApplicationName("Hangouts", "com.google.android.talk", "icHangouts.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName HIPCHAT; // = new AApplicationName("Hipchat", "com.hipchat", "icHipchat.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName INSTAGRAM; // = new AApplicationName("Instagram", "com.instagram.android", "icInstagram.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName KAKAO_TALK; // = new AApplicationName("KakaoTalk", "com.kakao.talk", "icKakaoTalk.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName KIK; // = new AApplicationName("Kik", "kik.android", "icKik.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName LINE; // = new AApplicationName("Line", "jp.naver.line.android", "icLine.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName LINKEDIN; // = new AApplicationName("LinkedIn", "com.linkedin.android", "icLinkedin.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName LYFT; // = new AApplicationName("Lyft", "me.lyft.android", "icLyft.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName MESSAGES; // = new AApplicationName("Messages", "text message", "icMessage.icon", NotificationBaseObj.ANotificationType.TEXT);
        @DexIgnore
        public static /* final */ AApplicationName MESSENGER; // = new AApplicationName("Messenger", "com.facebook.orca", "icMessenger.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName MINT; // = new AApplicationName("Mint", "com.mint", "icMint.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName OUTLOOK; // = new AApplicationName("Outlook", "com.microsoft.office.outlook", "icOutlook.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName PANDORA; // = new AApplicationName("Pandora", "com.pandora.android", "icPandora.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName PAYPAL_BUSINESS; // = new AApplicationName("PayPal", "com.paypal.merchant.client", "icPaypal.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName PAYPAL_MOBILE_CASH; // = new AApplicationName("PayPal", "com.paypal.android.p2pmobile", "icPaypal.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName PHONE_INCOMING_CALL; // = new AApplicationName("Phone", "com.apple.mobilephone", "icIncomingCall.icon", NotificationBaseObj.ANotificationType.INCOMING_CALL);
        @DexIgnore
        public static /* final */ AApplicationName PHONE_MISSED_CALL; // = new AApplicationName("Phone", "miss call", "icMissedCall.icon", NotificationBaseObj.ANotificationType.MISSED_CALL);
        @DexIgnore
        public static /* final */ AApplicationName PINTEREST; // = new AApplicationName("Pinterest", "com.pinterest", "icPinterest.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName POSTMATES; // = new AApplicationName("Postmates", "com.postmates.android", "icPostmates.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SEAMLESS; // = new AApplicationName("Seamless", "com.seamlessweb.android.view", "icSeamlessweb.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SHAZAM; // = new AApplicationName("Shazam", "com.shazam.android", "icShazam.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SINA_WEIBO; // = new AApplicationName("Sina Weibo", WeiboAppManager.WEIBO_PACKAGENAME, "icWeibo.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SKYPE; // = new AApplicationName("Skype", "com.skype.raider", "icSkype.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SKYPE_BUSINESS; // = new AApplicationName("Skype Business", "com.microsoft.office.lync15", "icSkypebusiness.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SLACK; // = new AApplicationName("Slack", "com.Slack", "icSlack.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SNAPCHAT; // = new AApplicationName("Snapchat", "com.snapchat.android", "icSnapchat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SPOTIFY; // = new AApplicationName("Spotify", "com.spotify.music", "icSpotify.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName SQUARE_CASH; // = new AApplicationName("Square Cash", "com.squareup.cash", "icSquareUp.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ ArrayList<AApplicationName> SUPPORTED_ICON_NOTIFICATION_APPS;
        @DexIgnore
        public static /* final */ AApplicationName SWARM; // = new AApplicationName("Swarm", "com.foursquare.robin", "icSwarm.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName TELEGRAM; // = new AApplicationName("Telegram", "org.telegram.messenger", "icTelegram.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName TINDER; // = new AApplicationName("Tinder", "com.tinder", "icTinder.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName TUMBLR; // = new AApplicationName("Tumblr", "com.tumblr", "icTumblr.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName TWITTER; // = new AApplicationName("Twitter", "com.twitter.android", "icTwitter.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName UBER; // = new AApplicationName("Uber", "com.ubercab", "icUber.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName UNITED_AIRLINES; // = new AApplicationName("United Airlines", "com.united.mobile.android", "icKakaoTalk.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName VENMO; // = new AApplicationName("Venmo", "com.venmo", "icVenmo.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName VIADEO; // = new AApplicationName("Viadeo", "com.viadeo.android", "icViadeo.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName VIBER; // = new AApplicationName("Viber", "com.viber.voip", "icViber.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName WECHAT; // = new AApplicationName("WeChat", "com.tencent.mm", "icWeChat.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName WEIBO; // = new AApplicationName("Weibo", WeiboAppManager.WEIBO_PACKAGENAME, "icWeibo.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public static /* final */ AApplicationName WHATSAPP; // = new AApplicationName("WhatsApp", "com.whatsapp", "icWhatsapp.icon", NotificationBaseObj.ANotificationType.NOTIFICATION);
        @DexIgnore
        public /* final */ String appName;
        @DexIgnore
        public /* final */ String iconFwPath;
        @DexIgnore
        public /* final */ NotificationBaseObj.ANotificationType notificationType;
        @DexIgnore
        public /* final */ String packageName;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final AApplicationName getAIRBNB() {
                return AApplicationName.AIRBNB;
            }

            @DexIgnore
            public final AApplicationName getAMERICAN_AIRLINES() {
                return AApplicationName.AMERICAN_AIRLINES;
            }

            @DexIgnore
            public final AApplicationName getBADOO() {
                return AApplicationName.BADOO;
            }

            @DexIgnore
            public final AApplicationName getBANK_OF_AMERICA() {
                return AApplicationName.BANK_OF_AMERICA;
            }

            @DexIgnore
            public final AApplicationName getCAPITAL_ONE() {
                return AApplicationName.CAPITAL_ONE;
            }

            @DexIgnore
            public final AApplicationName getCITIBANK() {
                return AApplicationName.CITIBANK;
            }

            @DexIgnore
            public final AApplicationName getDELTA() {
                return AApplicationName.DELTA;
            }

            @DexIgnore
            public final AApplicationName getESPN() {
                return AApplicationName.ESPN;
            }

            @DexIgnore
            public final AApplicationName getFACEBOOK() {
                return AApplicationName.FACEBOOK;
            }

            @DexIgnore
            public final AApplicationName getFLICKR() {
                return AApplicationName.FLICKR;
            }

            @DexIgnore
            public final AApplicationName getFOSSIL() {
                return AApplicationName.FOSSIL;
            }

            @DexIgnore
            public final AApplicationName getFOURSQUARE() {
                return AApplicationName.FOURSQUARE;
            }

            @DexIgnore
            public final AApplicationName getGMAIL() {
                return AApplicationName.GMAIL;
            }

            @DexIgnore
            public final AApplicationName getGOOGLE_CALENDAR() {
                return AApplicationName.GOOGLE_CALENDAR;
            }

            @DexIgnore
            public final AApplicationName getGOOGLE_FIT() {
                return AApplicationName.GOOGLE_FIT;
            }

            @DexIgnore
            public final AApplicationName getGOOGLE_PLUS() {
                return AApplicationName.GOOGLE_PLUS;
            }

            @DexIgnore
            public final AApplicationName getGRUBHUB() {
                return AApplicationName.GRUBHUB;
            }

            @DexIgnore
            public final AApplicationName getHANGOUTS() {
                return AApplicationName.HANGOUTS;
            }

            @DexIgnore
            public final AApplicationName getHIPCHAT() {
                return AApplicationName.HIPCHAT;
            }

            @DexIgnore
            public final AApplicationName getINSTAGRAM() {
                return AApplicationName.INSTAGRAM;
            }

            @DexIgnore
            public final AApplicationName getKAKAO_TALK() {
                return AApplicationName.KAKAO_TALK;
            }

            @DexIgnore
            public final AApplicationName getKIK() {
                return AApplicationName.KIK;
            }

            @DexIgnore
            public final AApplicationName getLINE() {
                return AApplicationName.LINE;
            }

            @DexIgnore
            public final AApplicationName getLINKEDIN() {
                return AApplicationName.LINKEDIN;
            }

            @DexIgnore
            public final AApplicationName getLYFT() {
                return AApplicationName.LYFT;
            }

            @DexIgnore
            public final AApplicationName getMESSAGES() {
                return AApplicationName.MESSAGES;
            }

            @DexIgnore
            public final AApplicationName getMESSENGER() {
                return AApplicationName.MESSENGER;
            }

            @DexIgnore
            public final AApplicationName getMINT() {
                return AApplicationName.MINT;
            }

            @DexIgnore
            public final AApplicationName getOUTLOOK() {
                return AApplicationName.OUTLOOK;
            }

            @DexIgnore
            public final AApplicationName getPANDORA() {
                return AApplicationName.PANDORA;
            }

            @DexIgnore
            public final AApplicationName getPAYPAL_BUSINESS() {
                return AApplicationName.PAYPAL_BUSINESS;
            }

            @DexIgnore
            public final AApplicationName getPAYPAL_MOBILE_CASH() {
                return AApplicationName.PAYPAL_MOBILE_CASH;
            }

            @DexIgnore
            public final AApplicationName getPHONE_INCOMING_CALL() {
                return AApplicationName.PHONE_INCOMING_CALL;
            }

            @DexIgnore
            public final AApplicationName getPHONE_MISSED_CALL() {
                return AApplicationName.PHONE_MISSED_CALL;
            }

            @DexIgnore
            public final AApplicationName getPINTEREST() {
                return AApplicationName.PINTEREST;
            }

            @DexIgnore
            public final AApplicationName getPOSTMATES() {
                return AApplicationName.POSTMATES;
            }

            @DexIgnore
            public final AApplicationName getSEAMLESS() {
                return AApplicationName.SEAMLESS;
            }

            @DexIgnore
            public final AApplicationName getSHAZAM() {
                return AApplicationName.SHAZAM;
            }

            @DexIgnore
            public final AApplicationName getSINA_WEIBO() {
                return AApplicationName.SINA_WEIBO;
            }

            @DexIgnore
            public final AApplicationName getSKYPE() {
                return AApplicationName.SKYPE;
            }

            @DexIgnore
            public final AApplicationName getSKYPE_BUSINESS() {
                return AApplicationName.SKYPE_BUSINESS;
            }

            @DexIgnore
            public final AApplicationName getSLACK() {
                return AApplicationName.SLACK;
            }

            @DexIgnore
            public final AApplicationName getSNAPCHAT() {
                return AApplicationName.SNAPCHAT;
            }

            @DexIgnore
            public final AApplicationName getSPOTIFY() {
                return AApplicationName.SPOTIFY;
            }

            @DexIgnore
            public final AApplicationName getSQUARE_CASH() {
                return AApplicationName.SQUARE_CASH;
            }

            @DexIgnore
            public final ArrayList<AApplicationName> getSUPPORTED_ICON_NOTIFICATION_APPS() {
                return AApplicationName.SUPPORTED_ICON_NOTIFICATION_APPS;
            }

            @DexIgnore
            public final AApplicationName getSWARM() {
                return AApplicationName.SWARM;
            }

            @DexIgnore
            public final AApplicationName getSupportedBySDKApp(String str) {
                T t;
                Wg6.c(str, "packageName");
                Iterator<T> it = getSUPPORTED_ICON_NOTIFICATION_APPS().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getPackageName(), str)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                return t2 != null ? t2 : getFOSSIL();
            }

            @DexIgnore
            public final AApplicationName getTELEGRAM() {
                return AApplicationName.TELEGRAM;
            }

            @DexIgnore
            public final AApplicationName getTINDER() {
                return AApplicationName.TINDER;
            }

            @DexIgnore
            public final AApplicationName getTUMBLR() {
                return AApplicationName.TUMBLR;
            }

            @DexIgnore
            public final AApplicationName getTWITTER() {
                return AApplicationName.TWITTER;
            }

            @DexIgnore
            public final AApplicationName getUBER() {
                return AApplicationName.UBER;
            }

            @DexIgnore
            public final AApplicationName getUNITED_AIRLINES() {
                return AApplicationName.UNITED_AIRLINES;
            }

            @DexIgnore
            public final AApplicationName getVENMO() {
                return AApplicationName.VENMO;
            }

            @DexIgnore
            public final AApplicationName getVIADEO() {
                return AApplicationName.VIADEO;
            }

            @DexIgnore
            public final AApplicationName getVIBER() {
                return AApplicationName.VIBER;
            }

            @DexIgnore
            public final AApplicationName getWECHAT() {
                return AApplicationName.WECHAT;
            }

            @DexIgnore
            public final AApplicationName getWEIBO() {
                return AApplicationName.WEIBO;
            }

            @DexIgnore
            public final AApplicationName getWHATSAPP() {
                return AApplicationName.WHATSAPP;
            }

            @DexIgnore
            public final boolean isPackageNameSupportedBySDK(String str) {
                T t;
                Wg6.c(str, "packageName");
                Iterator<T> it = getSUPPORTED_ICON_NOTIFICATION_APPS().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (Wg6.a(t.getPackageName(), str)) {
                        break;
                    }
                }
                return t != null;
            }
        }

        /*
        static {
            AApplicationName aApplicationName = new AApplicationName("Fossil Smartwatches", "com.fossil.wearables.fossil", "icFossil.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
            FOSSIL = aApplicationName;
            AApplicationName aApplicationName2 = PHONE_INCOMING_CALL;
            AApplicationName aApplicationName3 = PHONE_MISSED_CALL;
            AApplicationName aApplicationName4 = MESSAGES;
            SUPPORTED_ICON_NOTIFICATION_APPS = Hm7.c(aApplicationName2, aApplicationName3, aApplicationName4, GOOGLE_CALENDAR, GMAIL, HANGOUTS, aApplicationName4, WHATSAPP, FACEBOOK, TWITTER, INSTAGRAM, SNAPCHAT, WECHAT, WEIBO, LINE, GOOGLE_FIT, KAKAO_TALK, aApplicationName, BANK_OF_AMERICA, CITIBANK, CAPITAL_ONE, MINT, PAYPAL_BUSINESS, PAYPAL_MOBILE_CASH, VENMO, SQUARE_CASH, AIRBNB, LYFT, UBER, DELTA, AMERICAN_AIRLINES, UNITED_AIRLINES, ESPN, FLICKR, FOURSQUARE, GOOGLE_PLUS, LINKEDIN, PINTEREST, SINA_WEIBO, TUMBLR, VIADEO, PANDORA, SHAZAM, SPOTIFY, BADOO, HIPCHAT, KIK, OUTLOOK, SKYPE, SKYPE_BUSINESS, SLACK, TINDER, VIBER, TELEGRAM, GRUBHUB, POSTMATES, SEAMLESS, MESSENGER, SWARM);
        }
        */

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public AApplicationName(android.os.Parcel r6) {
            /*
                r5 = this;
                java.lang.String r0 = "parcel"
                com.mapped.Wg6.c(r6, r0)
                java.lang.String r0 = r6.readString()
                java.lang.String r3 = ""
                if (r0 == 0) goto L_0x0027
            L_0x000d:
                java.lang.String r1 = r6.readString()
                if (r1 == 0) goto L_0x002a
            L_0x0013:
                java.lang.String r2 = r6.readString()
                if (r2 == 0) goto L_0x002d
            L_0x0019:
                com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType[] r3 = com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj.ANotificationType.values()
                int r4 = r6.readInt()
                r3 = r3[r4]
                r5.<init>(r0, r1, r2, r3)
                return
            L_0x0027:
                java.lang.String r0 = ""
                goto L_0x000d
            L_0x002a:
                java.lang.String r1 = ""
                goto L_0x0013
            L_0x002d:
                r2 = r3
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.<init>(android.os.Parcel):void");
        }

        @DexIgnore
        public AApplicationName(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
            Wg6.c(str, "appName");
            Wg6.c(str2, "packageName");
            Wg6.c(str3, "iconFwPath");
            Wg6.c(aNotificationType, "notificationType");
            this.appName = str;
            this.packageName = str2;
            this.iconFwPath = str3;
            this.notificationType = aNotificationType;
        }

        @DexIgnore
        public static /* synthetic */ AApplicationName copy$default(AApplicationName aApplicationName, String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType, int i, Object obj) {
            if ((i & 1) != 0) {
                str = aApplicationName.appName;
            }
            if ((i & 2) != 0) {
                str2 = aApplicationName.packageName;
            }
            if ((i & 4) != 0) {
                str3 = aApplicationName.iconFwPath;
            }
            if ((i & 8) != 0) {
                aNotificationType = aApplicationName.notificationType;
            }
            return aApplicationName.copy(str, str2, str3, aNotificationType);
        }

        @DexIgnore
        public final String component1() {
            return this.appName;
        }

        @DexIgnore
        public final String component2() {
            return this.packageName;
        }

        @DexIgnore
        public final String component3() {
            return this.iconFwPath;
        }

        @DexIgnore
        public final NotificationBaseObj.ANotificationType component4() {
            return this.notificationType;
        }

        @DexIgnore
        public final AApplicationName copy(String str, String str2, String str3, NotificationBaseObj.ANotificationType aNotificationType) {
            Wg6.c(str, "appName");
            Wg6.c(str2, "packageName");
            Wg6.c(str3, "iconFwPath");
            Wg6.c(aNotificationType, "notificationType");
            return new AApplicationName(str, str2, str3, aNotificationType);
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof AApplicationName) {
                    AApplicationName aApplicationName = (AApplicationName) obj;
                    if (!Wg6.a(this.appName, aApplicationName.appName) || !Wg6.a(this.packageName, aApplicationName.packageName) || !Wg6.a(this.iconFwPath, aApplicationName.iconFwPath) || !Wg6.a(this.notificationType, aApplicationName.notificationType)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String getAppName() {
            return this.appName;
        }

        @DexIgnore
        public final String getIconFwPath() {
            return this.iconFwPath;
        }

        @DexIgnore
        public final NotificationBaseObj.ANotificationType getNotificationType() {
            return this.notificationType;
        }

        @DexIgnore
        public final String getPackageName() {
            return this.packageName;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.appName;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.packageName;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.iconFwPath;
            int hashCode3 = str3 != null ? str3.hashCode() : 0;
            NotificationBaseObj.ANotificationType aNotificationType = this.notificationType;
            if (aNotificationType != null) {
                i = aNotificationType.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "AApplicationName(appName=" + this.appName + ", packageName=" + this.packageName + ", iconFwPath=" + this.iconFwPath + ", notificationType=" + this.notificationType + ")";
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            Wg6.c(parcel, "parcel");
            parcel.writeString(this.appName);
            parcel.writeString(this.packageName);
            parcel.writeString(this.iconFwPath);
            parcel.writeInt(this.notificationType.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DianaNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, AApplicationName aApplicationName, String str, String str2, int i2, String str3, List<NotificationBaseObj.ANotificationFlag> list, Long l, NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType notificationControlActionType) {
        this(aApplicationName);
        Wg6.c(aNotificationType, "notificationType");
        Wg6.c(aApplicationName, "appName");
        Wg6.c(str, "title");
        Wg6.c(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        Wg6.c(str3, "message");
        Wg6.c(list, "notificationFlags");
        setUid(i);
        setNotificationType(aNotificationType);
        setTitle(str);
        setSender(str2);
        setMessage(str3);
        setSenderId(i2);
        setNotificationFlags(list);
        if (l != null) {
            this.notifyAt = l.longValue();
            if (notificationControlActionStatus != null) {
                setNotificationControlActionStatus(notificationControlActionStatus);
                if (notificationControlActionType != null) {
                    setNotificationControlActionType(notificationControlActionType);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DianaNotificationObj(int i, NotificationBaseObj.ANotificationType aNotificationType, AApplicationName aApplicationName, String str, String str2, int i2, String str3, List list, Long l, NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType notificationControlActionType, int i3, Qg6 qg6) {
        this(i, aNotificationType, aApplicationName, str, str2, i2, str3, list, (i3 & 256) != 0 ? Long.valueOf(System.currentTimeMillis()) : l, (i3 & 512) != 0 ? NotificationBaseObj.NotificationControlActionStatus.NOT_FOUND : notificationControlActionStatus, (i3 & 1024) != 0 ? NotificationBaseObj.NotificationControlActionType.DISMISS_NOTIFICATION : notificationControlActionType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaNotificationObj(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        setUid(parcel.readInt());
        setNotificationType(NotificationBaseObj.ANotificationType.values()[parcel.readInt()]);
        AApplicationName aApplicationName = (AApplicationName) parcel.readParcelable(AApplicationName.class.getClassLoader());
        this.notificationApp = aApplicationName == null ? AApplicationName.Companion.getFOSSIL() : aApplicationName;
        String readString = parcel.readString();
        setTitle(readString == null ? "" : readString);
        String readString2 = parcel.readString();
        setSender(readString2 == null ? "" : readString2);
        setSenderId(parcel.readInt());
        String readString3 = parcel.readString();
        setMessage(readString3 == null ? "" : readString3);
        setNotificationFlags(new ArrayList());
        ArrayList<Number> arrayList = new ArrayList();
        parcel.readList(arrayList, null);
        for (Number number : arrayList) {
            getNotificationFlags().add(NotificationBaseObj.ANotificationFlag.values()[number.intValue()]);
        }
        this.notifyAt = parcel.readLong();
        setNotificationControlActionStatus(NotificationBaseObj.NotificationControlActionStatus.values()[parcel.readInt()]);
        setNotificationControlActionType(NotificationBaseObj.NotificationControlActionType.values()[parcel.readInt()]);
    }

    @DexIgnore
    public DianaNotificationObj(AApplicationName aApplicationName) {
        Wg6.c(aApplicationName, "appName");
        this.notificationApp = aApplicationName;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public String toRemoteLogString() {
        return "UID=" + getUid() + ", notificationApp=" + this.notificationApp + ", flag=" + getNotificationFlags();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public Zn1 toSDKNotification() {
        Mo1 sDKNotificationType = getNotificationType().toSDKNotificationType();
        int uid = getUid();
        String packageName = (Wg6.a(this.notificationApp.getPackageName(), AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) ? AApplicationName.Companion.getPHONE_INCOMING_CALL() : this.notificationApp).getPackageName();
        String title = getTitle();
        String sender = getSender();
        int senderId = getSenderId();
        String message = getMessage();
        Object[] array = toSDKNotificationFlags(getNotificationFlags()).toArray(new NotificationFlag[0]);
        if (array != null) {
            return new Zn1(sDKNotificationType, uid, packageName, title, sender, senderId, message, (NotificationFlag[]) array, this.notifyAt);
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Ho1 toSDKNotifyNotificationEvent() {
        return new Ho1(getUid(), getNotificationControlActionType().toSDKNotificationType(), getNotificationControlActionStatus().toSDKNotificationType());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(getUid());
        parcel.writeInt(getNotificationType().ordinal());
        parcel.writeParcelable(this.notificationApp, i);
        parcel.writeString(getTitle());
        parcel.writeString(getSender());
        parcel.writeInt(getSenderId());
        parcel.writeString(getMessage());
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = getNotificationFlags().iterator();
        while (it.hasNext()) {
            arrayList.add(Integer.valueOf(it.next().ordinal()));
        }
        parcel.writeList(arrayList);
        parcel.writeLong(this.notifyAt);
        parcel.writeInt(getNotificationControlActionStatus().ordinal());
        parcel.writeInt(getNotificationControlActionType().ordinal());
    }
}
