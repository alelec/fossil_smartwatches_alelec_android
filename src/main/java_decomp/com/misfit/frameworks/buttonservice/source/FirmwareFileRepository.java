package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import com.fossil.A68;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileRepository implements FirmwareFileSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static FirmwareFileRepository INSTANCE;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public String applicationFileDir;
    @DexIgnore
    public /* final */ FirmwareFileLocalSource mLocalFirmwareFileSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final FirmwareFileRepository getInstance(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
            Wg6.c(context, "context");
            Wg6.c(firmwareFileLocalSource, "firmwareFileLocalSource");
            FirmwareFileRepository firmwareFileRepository = FirmwareFileRepository.INSTANCE;
            return firmwareFileRepository != null ? firmwareFileRepository : new FirmwareFileRepository(context, firmwareFileLocalSource);
        }
    }

    @DexIgnore
    public FirmwareFileRepository(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
        Wg6.c(context, "applicationContext");
        Wg6.c(firmwareFileLocalSource, "mLocalFirmwareFileSource");
        this.mLocalFirmwareFileSource = firmwareFileLocalSource;
        String name = FirmwareFileRepository.class.getName();
        Wg6.b(name, "FirmwareFileRepository::class.java.name");
        this.TAG = name;
        String file = context.getFilesDir().toString();
        Wg6.b(file, "applicationContext.filesDir.toString()");
        this.applicationFileDir = file;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0127  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, com.mapped.Xe6 r14) {
        /*
        // Method dump skipped, instructions count: 301
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.source.FirmwareFileRepository.downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository, java.lang.String, java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public Object downloadFirmware(String str, String str2, String str3, Xe6<? super File> xe6) {
        return downloadFirmware$suspendImpl(this, str, str2, str3, xe6);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public String getFirmwareFilePath(String str) {
        Wg6.c(str, "firmwareVersion");
        return this.applicationFileDir + "/" + A68.a(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public boolean isDownloaded(String str, String str2) {
        Wg6.c(str, "firmwareVersion");
        Wg6.c(str2, "checkSum");
        String firmwareFilePath = getFirmwareFilePath(str);
        return !(str2.length() == 0) ? this.mLocalFirmwareFileSource.verify(firmwareFilePath, str2) : this.mLocalFirmwareFileSource.getFile(firmwareFilePath) != null;
    }

    @DexIgnore
    public BufferedInputStream openConnectURL$buttonservice_release(String str) {
        Wg6.c(str, "fileUrl");
        try {
            URL url = new URL(str);
            URLConnection openConnection = url.openConnection();
            openConnection.connect();
            Wg6.b(openConnection, "connection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, "openConnectURL(), filePath=" + url + ", size=" + ((long) openConnection.getContentLength()));
            return new BufferedInputStream(openConnection.getInputStream());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, "openConnectURL(), ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public byte[] readFirmware(String str) {
        byte[] bArr = null;
        Wg6.c(str, "firmwareVersion");
        FileInputStream readFile = this.mLocalFirmwareFileSource.readFile(getFirmwareFilePath(str));
        if (readFile != null) {
            try {
                int size = (int) readFile.getChannel().size();
                byte[] bArr2 = new byte[size];
                int read = readFile.read(bArr2);
                if (read != size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.TAG;
                    local.e(str2, "getOtaData() - expectedSize=" + size + ", readSize=" + read);
                } else {
                    bArr = bArr2;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = this.TAG;
                local2.e(str3, "getOtaData() - e=" + e);
            } catch (Throwable th) {
                readFile.close();
                throw th;
            }
            readFile.close();
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("readFirmware() - firmwareVersion=");
        sb.append(str);
        sb.append(", dataExist=");
        sb.append(bArr != null);
        local3.d(str4, sb.toString());
        return bArr;
    }
}
