package com.misfit.frameworks.buttonservice.utils;

import com.fossil.Aw1;
import com.fossil.Bw1;
import com.fossil.Cw1;
import com.fossil.Dw1;
import com.fossil.Ew1;
import com.fossil.Jv1;
import com.fossil.Jy1;
import com.fossil.Kv1;
import com.fossil.Lw1;
import com.fossil.Nv1;
import com.fossil.Nw1;
import com.fossil.Ov1;
import com.fossil.Pv1;
import com.fossil.Qv1;
import com.fossil.Rv1;
import com.fossil.Sv1;
import com.fossil.Tv1;
import com.fossil.Wv1;
import com.fossil.Xv1;
import com.fossil.Yv1;
import com.fossil.Zv1;
import com.mapped.Kc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchface.Background;
import com.misfit.frameworks.buttonservice.model.watchface.ComplicationData;
import com.misfit.frameworks.buttonservice.model.watchface.MetricObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingType;
import com.misfit.frameworks.buttonservice.model.watchface.TextData;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeColour;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchface.TickerData;
import com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData;
import java.io.File;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeExtentionKt {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$2;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$3;

        /*
        static {
            int[] iArr = new int[MovingType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MovingType.ACTIVE.ordinal()] = 1;
            $EnumSwitchMapping$0[MovingType.BATTERY.ordinal()] = 2;
            $EnumSwitchMapping$0[MovingType.CALORIES.ordinal()] = 3;
            $EnumSwitchMapping$0[MovingType.RAIN.ordinal()] = 4;
            $EnumSwitchMapping$0[MovingType.STEP.ordinal()] = 5;
            $EnumSwitchMapping$0[MovingType.SECOND_TIME.ordinal()] = 6;
            $EnumSwitchMapping$0[MovingType.HEART.ordinal()] = 7;
            $EnumSwitchMapping$0[MovingType.WEATHER.ordinal()] = 8;
            $EnumSwitchMapping$0[MovingType.DATE.ordinal()] = 9;
            $EnumSwitchMapping$0[MovingType.TEXT.ordinal()] = 10;
            $EnumSwitchMapping$0[MovingType.TICKER.ordinal()] = 11;
            int[] iArr2 = new int[ThemeColour.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[ThemeColour.WHITE.ordinal()] = 1;
            $EnumSwitchMapping$1[ThemeColour.LIGHT_GREY.ordinal()] = 2;
            $EnumSwitchMapping$1[ThemeColour.DARK_GREY.ordinal()] = 3;
            $EnumSwitchMapping$1[ThemeColour.BLACK.ordinal()] = 4;
            $EnumSwitchMapping$1[ThemeColour.DEFAULT.ordinal()] = 5;
            int[] iArr3 = new int[MovingType.values().length];
            $EnumSwitchMapping$2 = iArr3;
            iArr3[MovingType.STEP.ordinal()] = 1;
            $EnumSwitchMapping$2[MovingType.RAIN.ordinal()] = 2;
            $EnumSwitchMapping$2[MovingType.CALORIES.ordinal()] = 3;
            $EnumSwitchMapping$2[MovingType.ACTIVE.ordinal()] = 4;
            $EnumSwitchMapping$2[MovingType.BATTERY.ordinal()] = 5;
            $EnumSwitchMapping$2[MovingType.DATE.ordinal()] = 6;
            $EnumSwitchMapping$2[MovingType.WEATHER.ordinal()] = 7;
            $EnumSwitchMapping$2[MovingType.HEART.ordinal()] = 8;
            $EnumSwitchMapping$2[MovingType.SECOND_TIME.ordinal()] = 9;
            int[] iArr4 = new int[Cw1.values().length];
            $EnumSwitchMapping$3 = iArr4;
            iArr4[Cw1.DEFAULT.ordinal()] = 1;
            $EnumSwitchMapping$3[Cw1.WHITE.ordinal()] = 2;
            $EnumSwitchMapping$3[Cw1.LIGHT_GRAY.ordinal()] = 3;
            $EnumSwitchMapping$3[Cw1.DARK_GRAY.ordinal()] = 4;
            $EnumSwitchMapping$3[Cw1.BLACK.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    public static final Sv1 toComplicationElement(ComplicationData complicationData, MovingType movingType, MetricObject metricObject) {
        Pv1 pv1;
        String str;
        Wg6.c(complicationData, "$this$toComplicationElement");
        Wg6.c(movingType, "type");
        Wg6.c(metricObject, "metric");
        switch (WhenMappings.$EnumSwitchMapping$2[movingType.ordinal()]) {
            case 1:
                Zv1 zv1 = new Zv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                zv1.setPercentageCircleEnable(complicationData.getEnableRingGoal());
                pv1 = zv1;
                break;
            case 2:
                pv1 = new Rv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                break;
            case 3:
                Qv1 qv1 = new Qv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                qv1.setPercentageCircleEnable(complicationData.getEnableRingGoal());
                pv1 = qv1;
                break;
            case 4:
                Ov1 ov1 = new Ov1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                ov1.setPercentageCircleEnable(complicationData.getEnableRingGoal());
                pv1 = ov1;
                break;
            case 5:
                Pv1 pv12 = new Pv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                pv12.setPercentageCircleEnable(complicationData.getEnableRingGoal());
                pv1 = pv12;
                break;
            case 6:
                pv1 = new Wv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                break;
            case 7:
                pv1 = new Aw1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                break;
            case 8:
                pv1 = new Xv1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()));
                break;
            case 9:
                Jv1 jv1 = new Jv1(metricObject.getScaledX(), metricObject.getScaledY());
                Kv1 kv1 = new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight());
                TimeZoneData timeZoneData = complicationData.getTimeZoneData();
                if (timeZoneData == null || (str = timeZoneData.getLocation()) == null) {
                    str = "";
                }
                TimeZoneData timeZoneData2 = complicationData.getTimeZoneData();
                pv1 = new Yv1(jv1, kv1, new Bw1(str, timeZoneData2 != null ? timeZoneData2.getOffsetMin() : 1));
                break;
            default:
                throw new Exception("wrong type");
        }
        if (!complicationData.getEnableRingGoal()) {
            pv1.setBackgroundImage(new Tv1(complicationData.getName(), complicationData.getData()));
        }
        return pv1;
    }

    @DexIgnore
    public static final Dw1 toImageElement(TickerData tickerData, MetricObject metricObject) {
        Wg6.c(tickerData, "$this$toImageElement");
        Wg6.c(metricObject, "metric");
        return new Dw1(tickerData.getName(), new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()), tickerData.getData());
    }

    @DexIgnore
    public static final Nv1 toRawFormatImage(Background background) {
        Wg6.c(background, "$this$toRawFormatImage");
        String id = background.getId();
        byte[] data = background.getData();
        if (data == null) {
            data = new byte[0];
        }
        return new Nv1(id, data);
    }

    @DexIgnore
    public static final Cw1 toSpaceColor(ThemeColour themeColour) {
        Wg6.c(themeColour, "$this$toSpaceColor");
        int i = WhenMappings.$EnumSwitchMapping$1[themeColour.ordinal()];
        if (i == 1) {
            return Cw1.WHITE;
        }
        if (i == 2) {
            return Cw1.LIGHT_GRAY;
        }
        if (i == 3) {
            return Cw1.DARK_GRAY;
        }
        if (i == 4) {
            return Cw1.BLACK;
        }
        if (i == 5) {
            return Cw1.DEFAULT;
        }
        throw new Kc6();
    }

    @DexIgnore
    public static final Ew1 toTextElement(TextData textData, MetricObject metricObject) {
        Wg6.c(textData, "$this$toTextElement");
        Wg6.c(metricObject, "metric");
        return new Ew1(new Jv1(metricObject.getScaledX(), metricObject.getScaledY()), new Kv1(metricObject.getScaledWidth(), metricObject.getScaledHeight()), textData.getText(), textData.getFontScaled(), textData.getFont(), toSpaceColor(textData.getColour()));
    }

    @DexIgnore
    public static final ThemeColour toThemeColour(Cw1 cw1) {
        Wg6.c(cw1, "$this$toThemeColour");
        int i = WhenMappings.$EnumSwitchMapping$3[cw1.ordinal()];
        if (i == 1) {
            return ThemeColour.DEFAULT;
        }
        if (i == 2) {
            return ThemeColour.WHITE;
        }
        if (i == 3) {
            return ThemeColour.LIGHT_GREY;
        }
        if (i == 4) {
            return ThemeColour.DARK_GREY;
        }
        if (i == 5) {
            return ThemeColour.BLACK;
        }
        throw new Kc6();
    }

    @DexIgnore
    public static final ThemeData toThemeData(Nw1 nw1) {
        byte[] bArr;
        String str;
        byte[] bArr2;
        String str2;
        byte[] bArr3;
        String str3;
        byte[] bArr4;
        String str4;
        byte[] bArr5;
        String str5;
        byte[] bArr6;
        String str6;
        byte[] bArr7;
        String str7;
        byte[] bArr8;
        String str8;
        byte[] bArr9;
        String str9;
        Wg6.c(nw1, "$this$toThemeData");
        Nv1 i = nw1.i();
        Background background = i != null ? new Background(i.getBitmapImageData(), i.getName()) : null;
        ArrayList arrayList = new ArrayList();
        for (T t : nw1.j()) {
            MetricObject metricObject = new MetricObject(t.getScaledPosition().getScaledX(), t.getScaledPosition().getScaledY(), t.getScaledSize().getScaledWidth(), t.getScaledSize().getScaledHeight());
            if (t instanceof Ew1) {
                T t2 = t;
                arrayList.add(new MovingObject(metricObject, null, null, new TextData(t2.getText(), t2.getFontName(), t2.getFontScaledSize(), toThemeColour(t2.getTextColor())), MovingType.TEXT, 6, null));
            } else if (t instanceof Dw1) {
                arrayList.add(new MovingObject(metricObject, null, new TickerData(t.getImageData(), t.getName(), ThemeColour.DEFAULT), null, MovingType.TICKER, 10, null));
            } else if (t instanceof Zv1) {
                T t3 = t;
                Tv1 backgroundImage = t3.getBackgroundImage();
                if (backgroundImage == null || (bArr9 = backgroundImage.getBitmapImageData()) == null) {
                    bArr9 = new byte[0];
                }
                Tv1 backgroundImage2 = t3.getBackgroundImage();
                if (backgroundImage2 == null || (str9 = backgroundImage2.getName()) == null) {
                    str9 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr9, str9, t3.isPercentageCircleEnable(), null, toThemeColour(t3.getTheme().getFontColor())), null, null, MovingType.STEP, 12, null));
            } else if (t instanceof Rv1) {
                T t4 = t;
                Tv1 backgroundImage3 = t4.getBackgroundImage();
                if (backgroundImage3 == null || (bArr8 = backgroundImage3.getBitmapImageData()) == null) {
                    bArr8 = new byte[0];
                }
                Tv1 backgroundImage4 = t4.getBackgroundImage();
                if (backgroundImage4 == null || (str8 = backgroundImage4.getName()) == null) {
                    str8 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr8, str8, t4.isPercentageCircleEnable(), null, toThemeColour(t4.getTheme().getFontColor())), null, null, MovingType.RAIN, 12, null));
            } else if (t instanceof Qv1) {
                T t5 = t;
                Tv1 backgroundImage5 = t5.getBackgroundImage();
                if (backgroundImage5 == null || (bArr7 = backgroundImage5.getBitmapImageData()) == null) {
                    bArr7 = new byte[0];
                }
                Tv1 backgroundImage6 = t5.getBackgroundImage();
                if (backgroundImage6 == null || (str7 = backgroundImage6.getName()) == null) {
                    str7 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr7, str7, t5.isPercentageCircleEnable(), null, toThemeColour(t5.getTheme().getFontColor())), null, null, MovingType.CALORIES, 12, null));
            } else if (t instanceof Ov1) {
                T t6 = t;
                Tv1 backgroundImage7 = t6.getBackgroundImage();
                if (backgroundImage7 == null || (bArr6 = backgroundImage7.getBitmapImageData()) == null) {
                    bArr6 = new byte[0];
                }
                Tv1 backgroundImage8 = t6.getBackgroundImage();
                if (backgroundImage8 == null || (str6 = backgroundImage8.getName()) == null) {
                    str6 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr6, str6, t6.isPercentageCircleEnable(), null, toThemeColour(t6.getTheme().getFontColor())), null, null, MovingType.ACTIVE, 12, null));
            } else if (t instanceof Pv1) {
                T t7 = t;
                Tv1 backgroundImage9 = t7.getBackgroundImage();
                if (backgroundImage9 == null || (bArr5 = backgroundImage9.getBitmapImageData()) == null) {
                    bArr5 = new byte[0];
                }
                Tv1 backgroundImage10 = t7.getBackgroundImage();
                if (backgroundImage10 == null || (str5 = backgroundImage10.getName()) == null) {
                    str5 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr5, str5, t7.isPercentageCircleEnable(), null, toThemeColour(t7.getTheme().getFontColor())), null, null, MovingType.BATTERY, 12, null));
            } else if (t instanceof Wv1) {
                T t8 = t;
                Tv1 backgroundImage11 = t8.getBackgroundImage();
                if (backgroundImage11 == null || (bArr4 = backgroundImage11.getBitmapImageData()) == null) {
                    bArr4 = new byte[0];
                }
                Tv1 backgroundImage12 = t8.getBackgroundImage();
                if (backgroundImage12 == null || (str4 = backgroundImage12.getName()) == null) {
                    str4 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr4, str4, t8.isPercentageCircleEnable(), null, toThemeColour(t8.getTheme().getFontColor())), null, null, MovingType.DATE, 12, null));
            } else if (t instanceof Aw1) {
                T t9 = t;
                Tv1 backgroundImage13 = t9.getBackgroundImage();
                if (backgroundImage13 == null || (bArr3 = backgroundImage13.getBitmapImageData()) == null) {
                    bArr3 = new byte[0];
                }
                Tv1 backgroundImage14 = t9.getBackgroundImage();
                if (backgroundImage14 == null || (str3 = backgroundImage14.getName()) == null) {
                    str3 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr3, str3, t9.isPercentageCircleEnable(), null, toThemeColour(t9.getTheme().getFontColor())), null, null, MovingType.WEATHER, 12, null));
            } else if (t instanceof Xv1) {
                T t10 = t;
                Tv1 backgroundImage15 = t10.getBackgroundImage();
                if (backgroundImage15 == null || (bArr2 = backgroundImage15.getBitmapImageData()) == null) {
                    bArr2 = new byte[0];
                }
                Tv1 backgroundImage16 = t10.getBackgroundImage();
                if (backgroundImage16 == null || (str2 = backgroundImage16.getName()) == null) {
                    str2 = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr2, str2, t10.isPercentageCircleEnable(), null, toThemeColour(t10.getTheme().getFontColor())), null, null, MovingType.HEART, 12, null));
            } else if (t instanceof Yv1) {
                T t11 = t;
                Tv1 backgroundImage17 = t11.getBackgroundImage();
                if (backgroundImage17 == null || (bArr = backgroundImage17.getBitmapImageData()) == null) {
                    bArr = new byte[0];
                }
                Tv1 backgroundImage18 = t11.getBackgroundImage();
                if (backgroundImage18 == null || (str = backgroundImage18.getName()) == null) {
                    str = "";
                }
                arrayList.add(new MovingObject(metricObject, new ComplicationData(bArr, str, t11.isPercentageCircleEnable(), toTimeZoneData(t11.getTimeZoneDataConfig()), toThemeColour(t11.getTheme().getFontColor())), null, null, MovingType.SECOND_TIME, 12, null));
            }
        }
        return new ThemeData(background, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:30:0x0092 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10, types: [com.fossil.Sv1] */
    /* JADX WARN: Type inference failed for: r0v14, types: [com.fossil.Ew1] */
    /* JADX WARN: Type inference failed for: r0v15 */
    /* JADX WARN: Type inference failed for: r0v16, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r0v18, types: [com.fossil.Dw1] */
    /* JADX WARN: Type inference failed for: r0v20 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor toThemeEditor(com.misfit.frameworks.buttonservice.model.watchface.ThemeData r7) {
        /*
            r2 = 0
            java.lang.String r0 = "$this$toThemeEditor"
            com.mapped.Wg6.c(r7, r0)
            com.fossil.Nw1 r3 = new com.fossil.Nw1
            r3.<init>()
            com.misfit.frameworks.buttonservice.model.watchface.Background r0 = r7.getBackground()
            if (r0 == 0) goto L_0x0045
            com.fossil.Nv1 r0 = toRawFormatImage(r0)
        L_0x0015:
            if (r0 == 0) goto L_0x001a
            r3.k(r0)
        L_0x001a:
            java.util.ArrayList r0 = r7.getMovingObjects()
            java.util.Iterator r4 = r0.iterator()
        L_0x0022:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00a3
            java.lang.Object r0 = r4.next()
            com.misfit.frameworks.buttonservice.model.watchface.MovingObject r0 = (com.misfit.frameworks.buttonservice.model.watchface.MovingObject) r0
            com.misfit.frameworks.buttonservice.model.watchface.MovingType r1 = r0.getType()
            int[] r5 = com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt.WhenMappings.$EnumSwitchMapping$0
            int r1 = r1.ordinal()
            r1 = r5[r1]
            switch(r1) {
                case 1: goto L_0x0070;
                case 2: goto L_0x0070;
                case 3: goto L_0x0070;
                case 4: goto L_0x0070;
                case 5: goto L_0x0070;
                case 6: goto L_0x0070;
                case 7: goto L_0x0070;
                case 8: goto L_0x0070;
                case 9: goto L_0x0070;
                case 10: goto L_0x0061;
                case 11: goto L_0x0047;
                default: goto L_0x003d;
            }
        L_0x003d:
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.String r1 = "wrong type"
            r0.<init>(r1)
            throw r0
        L_0x0045:
            r0 = r2
            goto L_0x0015
        L_0x0047:
            com.misfit.frameworks.buttonservice.model.watchface.TickerData r1 = r0.getTickerData()
            if (r1 == 0) goto L_0x005f
            com.misfit.frameworks.buttonservice.model.watchface.MetricObject r0 = r0.getMetric()
            com.fossil.Dw1 r0 = toImageElement(r1, r0)
        L_0x0055:
            if (r0 == 0) goto L_0x0022
            java.util.ArrayList r1 = r3.j()
            r1.add(r0)
            goto L_0x0022
        L_0x005f:
            r0 = r2
            goto L_0x0055
        L_0x0061:
            com.misfit.frameworks.buttonservice.model.watchface.TextData r1 = r0.getTextData()
            if (r1 == 0) goto L_0x005f
            com.misfit.frameworks.buttonservice.model.watchface.MetricObject r0 = r0.getMetric()
            com.fossil.Ew1 r0 = toTextElement(r1, r0)
            goto L_0x0055
        L_0x0070:
            com.misfit.frameworks.buttonservice.model.watchface.ComplicationData r1 = r0.getComplicationData()
            if (r1 == 0) goto L_0x009f
            com.misfit.frameworks.buttonservice.model.watchface.ThemeColour r1 = r1.getColour()
            if (r1 == 0) goto L_0x009f
            com.fossil.Cw1 r1 = toSpaceColor(r1)
        L_0x0080:
            com.misfit.frameworks.buttonservice.model.watchface.ComplicationData r5 = r0.getComplicationData()
            if (r5 == 0) goto L_0x00a1
            com.misfit.frameworks.buttonservice.model.watchface.MovingType r6 = r0.getType()
            com.misfit.frameworks.buttonservice.model.watchface.MetricObject r0 = r0.getMetric()
            com.fossil.Sv1 r0 = toComplicationElement(r5, r6, r0)
        L_0x0092:
            if (r1 == 0) goto L_0x0055
            if (r0 == 0) goto L_0x0055
            com.fossil.Uv1 r5 = new com.fossil.Uv1
            r5.<init>(r1)
            r0.setTheme(r5)
            goto L_0x0055
        L_0x009f:
            r1 = r2
            goto L_0x0080
        L_0x00a1:
            r0 = r2
            goto L_0x0092
        L_0x00a3:
            return r3
            switch-data {1->0x0070, 2->0x0070, 3->0x0070, 4->0x0070, 5->0x0070, 6->0x0070, 7->0x0070, 8->0x0070, 9->0x0070, 10->0x0061, 11->0x0047, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt.toThemeEditor(com.misfit.frameworks.buttonservice.model.watchface.ThemeData):com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor");
    }

    @DexIgnore
    public static final Lw1 toThemePackage(String str) {
        Wg6.c(str, "$this$toThemePackage");
        File file = new File(str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("ThemeExtension", "toThemePackage - filePath " + str + " isThemeFileExist " + file.exists());
        byte[] b = Jy1.a.b(file);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.e("ThemeExtension", "toThemePackage - binaryThemeData " + b);
        if (b == null) {
            return null;
        }
        return Lw1.CREATOR.a(b);
    }

    @DexIgnore
    public static final TimeZoneData toTimeZoneData(Bw1 bw1) {
        Wg6.c(bw1, "$this$toTimeZoneData");
        return new TimeZoneData(bw1.getLocation(), bw1.getOffsetInMinutes());
    }
}
