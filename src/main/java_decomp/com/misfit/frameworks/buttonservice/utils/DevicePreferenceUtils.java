package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.Zi4;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Bv6;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingDeserializer;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMappingDeserializer;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingDeserializer;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DevicePreferenceUtils {
    @DexIgnore
    public static /* final */ String TAG; // = "DevicePreferenceUtils";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends TypeToken<List<BLEMapping>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends TypeToken<List<AlarmSetting>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3 extends TypeToken<LocalizationData> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon4 extends TypeToken<Set<String>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon5 extends TypeToken<ThemeConfig> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon6 extends Thread {
        @DexIgnore
        public /* final */ /* synthetic */ Context val$context;

        @DexIgnore
        public Anon6(Context context) {
            this.val$context = context;
        }

        @DexIgnore
        public void run() {
            for (String str : DevicePreferenceUtils.getAllPairedButtonSerial(this.val$context)) {
                DevicePreferenceUtils.clearAutoSetMapping(this.val$context, str);
                DevicePreferenceUtils.clearAutoWatchAppSettings(this.val$context, str);
                DevicePreferenceUtils.clearAutoComplicationAppSettings(this.val$context, str);
            }
            DevicePreferenceUtils.clearAutoBiometricSettings(this.val$context);
            Context context = this.val$context;
            DevicePreferenceUtils.removeString(context, KeyUtils.getKeyDeviceSettingData(context));
            Context context2 = this.val$context;
            DevicePreferenceUtils.removeString(context2, KeyUtils.getKeyAllActiveButton(context2));
            Context context3 = this.val$context;
            DevicePreferenceUtils.removeString(context3, KeyUtils.getKeyAllPairedButton(context3));
            Context context4 = this.val$context;
            DevicePreferenceUtils.removeString(context4, KeyUtils.getKeyDeviceProfile(context4));
            Context context5 = this.val$context;
            DevicePreferenceUtils.removeString(context5, KeyUtils.getKeySecondTimezone(context5));
            Context context6 = this.val$context;
            DevicePreferenceUtils.removeString(context6, KeyUtils.getKeyCurrentSecondTimezoneOffset(context6));
            Context context7 = this.val$context;
            DevicePreferenceUtils.removeString(context7, KeyUtils.getKeyAlarmSetting(context7));
            Context context8 = this.val$context;
            DevicePreferenceUtils.removeString(context8, KeyUtils.getKeyListAlarm(context8));
            Context context9 = this.val$context;
            DevicePreferenceUtils.removeString(context9, KeyUtils.getKeyAutoCountdownSetting(context9));
            Context context10 = this.val$context;
            DevicePreferenceUtils.removeString(context10, KeyUtils.getKeyGoalRingEnabled(context10));
        }
    }

    @DexIgnore
    public static void addActiveButtonSerial(Context context, String str) {
        removeString(context, KeyUtils.getKeyAllActiveButton(context));
        addButtonSerialTo(context, str, KeyUtils.getKeyAllActiveButton(context));
    }

    @DexIgnore
    public static void addButtonSerialTo(Context context, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".addButtonSerialTo - serial=" + str + ", key=" + str2);
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str2, "");
            if (TextUtils.isEmpty(string) || !string.contains(str)) {
                SharedPreferences.Editor edit = preferences.edit();
                edit.putString(str2, string + str + LocaleConverter.LOCALE_DELIMITER);
                edit.apply();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local2.e(str4, ".addButtonSerialTo - is exist in " + string + ", no need to add again ");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, ".addButtonSerialTo - preferences=NULL");
    }

    @DexIgnore
    public static void addPairedButtonSerial(Context context, String str) {
        addButtonSerialTo(context, str, KeyUtils.getKeyAllPairedButton(context));
    }

    @DexIgnore
    public static void clearAllSettingFlag(Context context) {
        removeString(context, KeyUtils.getKeyDeviceSettingData(context));
    }

    @DexIgnore
    public static void clearAutoBackgroundImageConfig(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".clearAutoBackgroundImageConfig: " + str + ", keyAutoBackgroundImageConfig=" + KeyUtils.getKeyAutoSetBackgroundImageConfig(context));
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetBackgroundImageConfig(context));
            edit.remove(sb.toString());
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.BACKGROUND_IMAGE);
    }

    @DexIgnore
    public static void clearAutoBiometricSettings(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".clearAutoBiometricSettings: keyAutoBiometricSettings=" + KeyUtils.getKeyAutoSetBiometricSettings(context));
            SharedPreferences.Editor edit = preferences.edit();
            edit.remove(KeyUtils.getKeyAutoSetBiometricSettings(context));
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.BIOMETRIC);
    }

    @DexIgnore
    public static void clearAutoComplicationAppSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".clearAutoComplicationAppSettings: " + str + ", keyAutoComplicationSettings=" + KeyUtils.getKeyAutoSetComplicationAppSettings(context));
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetComplicationAppSettings(context));
            edit.remove(sb.toString());
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.COMPLICATION_APPS);
    }

    @DexIgnore
    public static void clearAutoListAlarm(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".clearAutoAlarmSetting - key=" + KeyUtils.getKeyListAlarm(context));
            SharedPreferences.Editor edit = preferences.edit();
            edit.remove(KeyUtils.getKeyListAlarm(context));
            edit.apply();
        }
    }

    @DexIgnore
    public static void clearAutoNotificationFiltersConfig(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".clearAutoNotificationFiltersConfig: " + str + ", autoNotificationFiltersConfig=" + KeyUtils.getKeyAutoSetNotificationFiltersConfig(context));
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetNotificationFiltersConfig(context));
            edit.remove(sb.toString());
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public static void clearAutoSecondTimezone(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".clearAutoSecondTimezone - key=" + KeyUtils.getKeySecondTimezone(context));
            SharedPreferences.Editor edit = preferences.edit();
            edit.remove(KeyUtils.getKeySecondTimezone(context));
            edit.apply();
        }
    }

    @DexIgnore
    public static void clearAutoSetMapping(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".clearAutoSetMapping: " + str + ", keyAutoMapping=" + KeyUtils.getKeyAutoSetMapping(context));
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(DeviceIdentityUtils.getDeviceFamily(str));
            sb.append(KeyUtils.getKeyAutoSetMapping(context));
            edit.remove(sb.toString());
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.MAPPINGS);
    }

    @DexIgnore
    public static void clearAutoWatchAppSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".clearAutoWatchAppSettings: " + str + ", keyAutoWatchAppSettings=" + KeyUtils.getKeyAutoSetWatchAppSettings(context));
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetWatchAppSettings(context));
            edit.remove(sb.toString());
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.WATCH_APPS);
    }

    @DexIgnore
    public static List<String> getAllActiveButtonSerial(Context context) {
        return getAllButtonSerialFrom(context, KeyUtils.getKeyAllActiveButton(context));
    }

    @DexIgnore
    public static List<String> getAllButtonSerialFrom(Context context, String str) {
        ArrayList arrayList = new ArrayList();
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str, "");
            if (!TextUtils.isEmpty(string)) {
                String[] split = string.split(LocaleConverter.LOCALE_DELIMITER);
                for (String str2 : split) {
                    if (!TextUtils.isEmpty(str2)) {
                        arrayList.add(str2);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<String> getAllPairedButtonSerial(Context context) {
        return getAllButtonSerialFrom(context, KeyUtils.getKeyAllPairedButton(context));
    }

    @DexIgnore
    public static BackgroundConfig getAutoBackgroundImageConfig(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getKeyAutoSetBackgroundImageConfig(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".getAutoBackgroundImageConfig - serial=" + str + ", key=" + KeyUtils.getKeyAutoSetBackgroundImageConfig(context));
            if (!Bv6.b(string)) {
                try {
                    BackgroundConfig backgroundConfig = (BackgroundConfig) new Zi4().d().k(string, BackgroundConfig.class);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, ".getAutoBackgroundImageConfig - autoBackgroundImageConfig=" + backgroundConfig);
                    return backgroundConfig;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.e(str4, ".getAutoBackgroundImageConfig - ex=" + e.toString() + ", backgroundJsonArray=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                local4.d(str5, ".getAutoBackgroundImageConfig - backgroundJsonArray=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static UserBiometricData getAutoBiometricSettings(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(KeyUtils.getKeyAutoSetBiometricSettings(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.v(str, ".getAutoBiometricSettings - key=" + KeyUtils.getKeyAutoSetBiometricSettings(context));
            if (!Bv6.b(string)) {
                try {
                    UserBiometricData userBiometricData = (UserBiometricData) new Zi4().d().k(string, UserBiometricData.class);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.v(str2, ".getAutoBiometricSettings - autoBiometricData=" + userBiometricData);
                    return userBiometricData;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.e(str3, ".getAutoBiometricSettings - ex=" + e.toString() + ", jsonData=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = TAG;
                local4.v(str4, ".getAutoBiometricSettings - jsonData=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static ComplicationAppMappingSettings getAutoComplicationAppSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getKeyAutoSetComplicationAppSettings(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".getAutoComplicationAppSettings - serial=" + str + ", key=" + KeyUtils.getKeyAutoSetComplicationAppSettings(context));
            if (!Bv6.b(string)) {
                try {
                    Zi4 zi4 = new Zi4();
                    zi4.f(ComplicationAppMapping.class, new ComplicationAppMappingDeserializer());
                    ComplicationAppMappingSettings complicationAppMappingSettings = (ComplicationAppMappingSettings) zi4.d().k(string, ComplicationAppMappingSettings.class);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, ".getAutoComplicationAppSettings - autoComplicationAppMappingSettings=" + complicationAppMappingSettings);
                    return complicationAppMappingSettings;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.e(str4, ".getAutoComplicationAppSettings - ex=" + e.toString() + ", jsonData=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                local4.d(str5, ".getAutoComplicationAppSettings - jsonData=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static List<AlarmSetting> getAutoListAlarm(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(KeyUtils.getKeyListAlarm(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".getAutoListAlarm - alarmJson=" + string);
            if (!Bv6.b(string)) {
                try {
                    return (List) new Gson().l(string, new Anon2().getType());
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.e(str2, ".getAutoListAlarm - ex=" + e.toString());
                    return new ArrayList();
                }
            }
        }
        return new ArrayList();
    }

    @DexIgnore
    public static LocalizationData getAutoLocalizationDataSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getKeyAutoLocalizationDataSettings(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".getAutoLocalizationDataSettings - serial=" + str + ", key=" + KeyUtils.getKeyAutoLocalizationDataSettings(context));
            if (!Bv6.b(string)) {
                try {
                    LocalizationData localizationData = (LocalizationData) new Gson().l(string, new Anon3().getType());
                    localizationData.initialize();
                    return localizationData;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.e(str3, ".getAutoLocalizationDataSettings - ex=" + e.toString() + ", jsonData=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = TAG;
                local3.d(str4, ".getAutoLocalizationDataSettings - preferencesString=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static List<BLEMapping> getAutoMapping(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(DeviceIdentityUtils.getDeviceFamily(str) + KeyUtils.getKeyAutoSetMapping(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.v(str2, ".getAutoMapping - deviceFamily=" + DeviceIdentityUtils.getDeviceFamily(str) + ", key=" + KeyUtils.getKeyAutoSetMapping(context) + ", mappingJsonArray=" + string);
            if (!Bv6.b(string)) {
                try {
                    Zi4 zi4 = new Zi4();
                    zi4.f(BLEMapping.class, new BLEMappingDeserializer());
                    List<BLEMapping> list = (List) zi4.d().l(string, new Anon1().getType());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.v(str3, ".getAutoMapping - autoMapping=" + list.size());
                    return list;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.e(str4, ".getAutoMapping - ex=" + e.toString());
                    return new ArrayList();
                }
            }
        }
        return new ArrayList();
    }

    @DexIgnore
    public static AppNotificationFilterSettings getAutoNotificationFiltersConfig(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getKeyAutoSetNotificationFiltersConfig(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".getAutoNotificationFiltersConfig - serial=" + str + ", key=" + KeyUtils.getKeyAutoSetNotificationFiltersConfig(context));
            if (!Bv6.b(string)) {
                try {
                    AppNotificationFilterSettings appNotificationFilterSettings = (AppNotificationFilterSettings) new Zi4().d().k(string, AppNotificationFilterSettings.class);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, ".getAutoNotificationFiltersConfig - autoNotificationFiltersConfig=" + appNotificationFilterSettings);
                    return appNotificationFilterSettings;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.e(str4, ".getAutoNotificationFiltersConfig - ex=" + e.toString() + ", notificationFiltersJsonArray=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                local4.d(str5, ".getAutoNotificationFiltersConfig - notificationFiltersJsonArray=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static int getAutoSecondTimezone(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(KeyUtils.getKeySecondTimezone(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".getAutoSecondTimezone - timezoneId=" + string);
            if (!TextUtils.isEmpty(string)) {
                return ConversionUtils.INSTANCE.getTimezoneRawOffsetById(string);
            }
        }
        return 1024;
    }

    @DexIgnore
    public static String getAutoSecondTimezoneId(Context context) {
        SharedPreferences preferences = getPreferences(context);
        return preferences != null ? preferences.getString(KeyUtils.getKeySecondTimezone(context), "") : "";
    }

    @DexIgnore
    public static WatchAppMappingSettings getAutoWatchAppSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getKeyAutoSetWatchAppSettings(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".getAutoWatchAppSettings - serial=" + str + ", key=" + KeyUtils.getKeyAutoSetWatchAppSettings(context));
            if (!Bv6.b(string)) {
                try {
                    Zi4 zi4 = new Zi4();
                    zi4.f(WatchAppMapping.class, new WatchAppMappingDeserializer());
                    WatchAppMappingSettings watchAppMappingSettings = (WatchAppMappingSettings) zi4.d().k(string, WatchAppMappingSettings.class);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, ".getAutoWatchAppSettings - autoWatchAppMappingSettings=" + watchAppMappingSettings);
                    return watchAppMappingSettings;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.e(str4, ".getAutoWatchAppSettings - ex=" + e.toString() + ", mappingJsonArray=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                local4.d(str5, ".getAutoWatchAppSettings - mappingJsonArray=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static int getCurrentSecondTimezoneOffset(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return -1;
        }
        FLogger.INSTANCE.getLocal().d(TAG, ".getCurrentSecondTimezoneOffset");
        return preferences.getInt(KeyUtils.getKeyCurrentSecondTimezoneOffset(context), -1);
    }

    @DexIgnore
    public static int getInt(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.v(str2, "getString: " + str);
            return preferences.getInt(str, -1);
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.v(str3, "getString: " + str);
        return -1;
    }

    @DexIgnore
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(KeyUtils.getButtonPreferenceKey(context), 0);
    }

    @DexIgnore
    public static Set<String> getSettingFlags(Context context) {
        Set<String> set = (Set) new Gson().l(getString(context, KeyUtils.getKeyDeviceSettingData(context)), new Anon4().getType());
        return set == null ? new HashSet() : set;
    }

    @DexIgnore
    public static String getString(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.v(str2, "getString: " + str);
            return preferences.getString(str, "");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.v(str3, "getString: " + str);
        return "";
    }

    @DexIgnore
    public static ThemeConfig getThemeConfig(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String string = preferences.getString(str + KeyUtils.getThemeDataKey(context), "");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, ".getThemeData - serial=" + str + ", key=" + KeyUtils.getThemeDataKey(context));
            if (!Bv6.b(string)) {
                try {
                    return (ThemeConfig) new Gson().l(string, new Anon5().getType());
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.e(str3, ".getThemeData - ex=" + e.toString() + ", jsonData=" + string);
                    return null;
                }
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = TAG;
                local3.d(str4, ".getThemeData - preferencesString=" + string);
            }
        }
        return null;
    }

    @DexIgnore
    public static int getUiMajorVersion(Context context, String str) {
        return getInt(context, KeyUtils.getLastDeviceUiVersionMajor(context) + str);
    }

    @DexIgnore
    public static int getUiMinorVersion(Context context, String str) {
        return getInt(context, KeyUtils.getLastDeviceUiVersionMinor(context) + str);
    }

    @DexIgnore
    public static boolean isActiveDevice(Context context, String str) {
        List<String> allActiveButtonSerial = getAllActiveButtonSerial(context);
        return allActiveButtonSerial != null && allActiveButtonSerial.size() > 0 && allActiveButtonSerial.contains(str);
    }

    @DexIgnore
    public static boolean isButtonExist(Context context, String str) {
        return isButtonExistIn(context, str, KeyUtils.getKeyAllActiveButton(context));
    }

    @DexIgnore
    public static boolean isButtonExistIn(Context context, String str, String str2) {
        String string = getString(context, str2);
        return !TextUtils.isEmpty(string) && string.contains(str);
    }

    @DexIgnore
    public static boolean isNeedToSetSetting(Context context, DeviceSettings deviceSettings) {
        return getSettingFlags(context).contains(deviceSettings.name());
    }

    @DexIgnore
    public static void logOut(Context context) {
        new Anon6(context).start();
    }

    @DexIgnore
    public static boolean needToSetTheme(Context context, DeviceSettings deviceSettings) {
        return getSettingFlags(context).contains(deviceSettings.name());
    }

    @DexIgnore
    public static void removeActiveButtonSerial(Context context, String str) {
        removeButtonSerialFrom(context, str, KeyUtils.getKeyAllActiveButton(context));
    }

    @DexIgnore
    public static void removeButtonSerialFrom(Context context, String str, String str2) {
        List<String> allButtonSerialFrom = getAllButtonSerialFrom(context, str2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, ".removeButtonSerialFrom - serial=" + str + ", key=" + str2);
        if (allButtonSerialFrom != null && allButtonSerialFrom.contains(str)) {
            allButtonSerialFrom.remove(str);
            removeString(context, str2);
            for (String str4 : allButtonSerialFrom) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                local2.d(str5, ".removeButtonSerialFrom re-Add button serial " + str4 + " to " + str2);
                addButtonSerialTo(context, str4, str2);
            }
        }
    }

    @DexIgnore
    public static void removePairedButtonSerial(Context context, String str) {
        removeButtonSerialFrom(context, str, KeyUtils.getKeyAllPairedButton(context));
    }

    @DexIgnore
    public static void removeSettingFlag(Context context, DeviceSettings deviceSettings) {
        Set<String> settingFlags = getSettingFlags(context);
        settingFlags.remove(deviceSettings.name());
        String t = new Gson().t(settingFlags);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".removeSettingFlag - flags=" + t + ", removed=" + deviceSettings.name());
        setString(context, KeyUtils.getKeyDeviceSettingData(context), t);
    }

    @DexIgnore
    public static void removeString(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "removeString: " + str);
            SharedPreferences.Editor edit = preferences.edit();
            edit.remove(str);
            edit.apply();
        }
    }

    @DexIgnore
    public static void setAutoBackgroundImageConfig(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".setAutoBackgroundImageConfig: serial=" + str + ", key=" + KeyUtils.getKeyAutoSetBackgroundImageConfig(context) + ", backgroundJsonArray=" + str2);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetBackgroundImageConfig(context));
            edit.putString(sb.toString(), str2);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.BACKGROUND_IMAGE);
    }

    @DexIgnore
    public static void setAutoBiometricSettings(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setAutoBiometricSettings: key=" + KeyUtils.getKeyAutoSetBiometricSettings(context) + ", mappingJsonArray=" + str);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(KeyUtils.getKeyAutoSetBiometricSettings(context), str);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.BIOMETRIC);
    }

    @DexIgnore
    public static void setAutoComplicationAppSettings(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".setAutoComplicationAppSettings: serial=" + str + ", key=" + KeyUtils.getKeyAutoSetComplicationAppSettings(context) + ", mappingJsonArray=" + str2);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetComplicationAppSettings(context));
            edit.putString(sb.toString(), str2);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.COMPLICATION_APPS);
    }

    @DexIgnore
    public static void setAutoListAlarm(Context context, List<AlarmSetting> list) {
        SharedPreferences preferences = getPreferences(context);
        String t = new Gson().t(list);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".setAutoListAlarm - alarmsJson=" + t);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(KeyUtils.getKeyListAlarm(context), t);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.MULTI_ALARM);
    }

    @DexIgnore
    public static void setAutoLocalizationData(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".setAutoLocalizationData: serial=" + str + ", key=" + KeyUtils.getKeyAutoLocalizationDataSettings(context) + ", mappingJsonArray=" + str2);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoLocalizationDataSettings(context));
            edit.putString(sb.toString(), str2);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.LOCALIZATION_DATA);
    }

    @DexIgnore
    public static void setAutoNotificationFiltersConfig(Context context, String str, AppNotificationFilterSettings appNotificationFilterSettings) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String t = new Gson().t(appNotificationFilterSettings);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setAutoNotificationFiltersConfig: serial=" + str + ", key=" + KeyUtils.getKeyAutoSetNotificationFiltersConfig(context) + ", notificationFiltersSettingJsonArray=" + t);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetNotificationFiltersConfig(context));
            edit.putString(sb.toString(), t);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public static void setAutoSecondTimezone(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".setAutoSecondTimezone - timezoneId=" + str);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(KeyUtils.getKeySecondTimezone(context), str);
            edit.apply();
        }
        setCurrentSecondTimezoneOffset(context, ConversionUtils.INSTANCE.getTimezoneRawOffsetById(str));
    }

    @DexIgnore
    public static void setAutoSetMapping(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".setAutoSetMapping: " + DeviceIdentityUtils.getDeviceFamily(str) + KeyUtils.getKeyAutoSetMapping(context) + ", mappingJsonArray=" + str2);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(DeviceIdentityUtils.getDeviceFamily(str));
            sb.append(KeyUtils.getKeyAutoSetMapping(context));
            edit.putString(sb.toString(), str2);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.MAPPINGS);
    }

    @DexIgnore
    public static void setAutoWatchAppSettings(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, ".setAutoWatchAppSettings: serial=" + str + ", key=" + KeyUtils.getKeyAutoSetWatchAppSettings(context) + ", mappingJsonArray=" + str2);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getKeyAutoSetWatchAppSettings(context));
            edit.putString(sb.toString(), str2);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.WATCH_APPS);
    }

    @DexIgnore
    public static void setCurrentSecondTimezoneOffset(Context context, int i) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".setCurrentSecondTimezoneOffset - timezoneOffset=" + i);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt(KeyUtils.getKeyCurrentSecondTimezoneOffset(context), i);
            edit.apply();
        }
    }

    @DexIgnore
    public static void setInt(Context context, String str, int i) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, "setInt: " + str);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt(str, i);
            edit.apply();
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local2.d(str3, "setString: " + str);
    }

    @DexIgnore
    public static void setSettingFlag(Context context, DeviceSettings deviceSettings) {
        Set<String> settingFlags = getSettingFlags(context);
        settingFlags.add(deviceSettings.name());
        String t = new Gson().t(settingFlags);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".setSettingFlag - flags=" + t);
        setString(context, KeyUtils.getKeyDeviceSettingData(context), t);
    }

    @DexIgnore
    public static void setString(Context context, String str, String str2) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.d(str3, "setString: " + str);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(str, str2);
            edit.apply();
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local2.d(str4, "setString: " + str);
    }

    @DexIgnore
    public static void setThemeData(Context context, String str, ThemeConfig themeConfig) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            String t = new Gson().t(themeConfig);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, ".setThemeData: serial=" + str + ", key=" + KeyUtils.getThemeDataKey(context) + ", themeDataJsonArray=" + t);
            SharedPreferences.Editor edit = preferences.edit();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(KeyUtils.getThemeDataKey(context));
            edit.putString(sb.toString(), t);
            edit.apply();
        }
        removeSettingFlag(context, DeviceSettings.THEME);
    }

    @DexIgnore
    public static void setUiMajorVersion(Context context, String str, int i) {
        setInt(context, KeyUtils.getLastDeviceUiVersionMajor(context) + str, i);
    }

    @DexIgnore
    public static void setUiMinorVersion(Context context, String str, int i) {
        setInt(context, KeyUtils.getLastDeviceUiVersionMinor(context) + str, i);
    }
}
