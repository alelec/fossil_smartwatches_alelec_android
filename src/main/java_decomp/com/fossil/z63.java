package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z63 implements A73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.client.freeride_engagement_fix", true);

    @DexIgnore
    @Override // com.fossil.A73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.A73
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
