package com.fossil;

import com.mapped.An4;
import com.mapped.TimeTickReceiver;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.usecase.DSTChangeUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ho5 implements MembersInjector<TimeTickReceiver> {
    @DexIgnore
    public static void a(TimeTickReceiver timeTickReceiver, AlarmHelper alarmHelper) {
        timeTickReceiver.c = alarmHelper;
    }

    @DexIgnore
    public static void b(TimeTickReceiver timeTickReceiver, DSTChangeUseCase dSTChangeUseCase) {
        timeTickReceiver.b = dSTChangeUseCase;
    }

    @DexIgnore
    public static void c(TimeTickReceiver timeTickReceiver, An4 an4) {
        timeTickReceiver.a = an4;
    }
}
