package com.fossil;

import com.mapped.Kc6;
import com.mapped.Lc6;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ke {
    @DexIgnore
    public static /* final */ HashMap<Lc6<String, Ob>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Ke b; // = new Ke();

    @DexIgnore
    public final short a(String str, Ob ob) {
        return ob.b;
    }

    @DexIgnore
    public final short b(String str, Ob ob) {
        Byte b2 = a.get(new Lc6(str, ob));
        byte byteValue = b2 != null ? b2.byteValue() : 0;
        switch (Je.b[ob.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return ob.b;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return ob.a((byte) 0);
            case 15:
            case 16:
                return ob.a((byte) 254);
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                a.put(new Lc6<>(str, ob), Byte.valueOf((byte) ((Hy1.p(byteValue) + 1) % Hy1.p((byte) -1))));
                return ob.a(byteValue);
            default:
                throw new Kc6();
        }
    }
}
