package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.V18;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.zendesk.sdk.network.Constants;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zc0 implements Interceptor {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Zc0(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        String str;
        String m = Hy1.m(System.currentTimeMillis(), false);
        V18.Ai h = chain.c().h();
        h.e("Content-Type", Constants.APPLICATION_JSON);
        h.e("X-Cyc-Auth-Method", "signature");
        h.e("X-Cyc-Access-Key-Id", this.a);
        h.e("X-Cyc-Timestamp", m);
        StringBuilder e = E.e("Signature=");
        String str2 = this.b;
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            String str3 = m + str2;
            Charset c = Hd0.y.c();
            if (str3 != null) {
                byte[] bytes = str3.getBytes(c);
                Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                instance.update(bytes);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b2 : digest) {
                    Hr7 hr7 = Hr7.a;
                    String format = String.format("%02X", Arrays.copyOf(new Object[]{Byte.valueOf(b2)}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                }
                str = sb.toString();
                Wg6.b(str, "stringBuilder.toString()");
                e.append(str);
                h.e("Authorization", e.toString());
                Response d = chain.d(h.b());
                Wg6.b(d, "chain.proceed(request)");
                return d;
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        } catch (Exception e2) {
            D90.i.i(e2);
            str = "";
        }
    }
}
