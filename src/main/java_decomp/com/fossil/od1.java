package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Od1 {
    @DexIgnore
    void a(int i);

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    <T> T e(int i, Class<T> cls);

    @DexIgnore
    <T> void f(T t);

    @DexIgnore
    <T> T g(int i, Class<T> cls);
}
