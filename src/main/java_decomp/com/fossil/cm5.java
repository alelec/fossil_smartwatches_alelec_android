package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cm5 implements Cloneable {
    @DexIgnore
    public int b;
    @DexIgnore
    public byte[] c;

    @DexIgnore
    public Cm5(int i, byte[] bArr) {
        if (bArr != null) {
            this.b = i;
            byte[] bArr2 = new byte[bArr.length];
            this.c = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    public Cm5(String str) {
        try {
            this.c = str.getBytes(Ia1.PROTOCOL_CHARSET);
            this.b = 106;
        } catch (UnsupportedEncodingException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "Default encoding must be supported=" + e);
        }
    }

    @DexIgnore
    public Cm5(byte[] bArr) {
        this(106, bArr);
    }

    @DexIgnore
    @Override // java.lang.Object
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        byte[] bArr = this.c;
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        try {
            return new Cm5(this.b, bArr2);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    @DexIgnore
    public String d() {
        int i = this.b;
        if (i == 0) {
            return new String(this.c);
        }
        try {
            return new String(this.c, Am5.b(i));
        } catch (Exception e) {
            try {
                return new String(this.c, "iso-8859-1");
            } catch (Exception e2) {
                return new String(this.c);
            }
        }
    }

    @DexIgnore
    public byte[] e() {
        byte[] bArr = this.c;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public void g(byte[] bArr) {
        if (bArr != null) {
            byte[] bArr2 = new byte[bArr.length];
            this.c = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }
}
