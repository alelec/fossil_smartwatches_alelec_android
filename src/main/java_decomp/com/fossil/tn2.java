package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tn2 implements IInterface {
    @DexIgnore
    public /* final */ IBinder b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Tn2(IBinder iBinder, String str) {
        this.b = iBinder;
        this.c = str;
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.b;
    }

    @DexIgnore
    public final Parcel d() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.c);
        return obtain;
    }

    @DexIgnore
    public final void e(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.b.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public final void i(int i, Parcel parcel) throws RemoteException {
        try {
            this.b.transact(i, parcel, null, 1);
        } finally {
            parcel.recycle();
        }
    }
}
