package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C85 extends B85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d G0; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H0;
    @DexIgnore
    public long F0;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        H0 = sparseIntArray;
        sparseIntArray.put(2131362709, 1);
        H0.put(2131362765, 2);
        H0.put(2131362766, 3);
        H0.put(2131363417, 4);
        H0.put(2131363402, 5);
        H0.put(2131363341, 6);
        H0.put(2131363362, 7);
        H0.put(2131362587, 8);
        H0.put(2131363384, 9);
        H0.put(2131361993, 10);
        H0.put(2131363473, 11);
        H0.put(2131362588, 12);
        H0.put(2131362037, 13);
        H0.put(2131362612, 14);
        H0.put(2131363273, 15);
        H0.put(2131363275, 16);
        H0.put(2131363274, 17);
        H0.put(2131362033, 18);
        H0.put(2131362611, 19);
        H0.put(2131363264, 20);
        H0.put(2131363266, 21);
        H0.put(2131363265, 22);
        H0.put(2131362038, 23);
        H0.put(2131362613, 24);
        H0.put(2131363267, 25);
        H0.put(2131363269, 26);
        H0.put(2131363268, 27);
        H0.put(2131362048, 28);
        H0.put(2131362614, 29);
        H0.put(2131363270, 30);
        H0.put(2131363272, 31);
        H0.put(2131363271, 32);
        H0.put(2131362080, 33);
        H0.put(2131362549, 34);
        H0.put(2131362411, 35);
        H0.put(2131361956, 36);
        H0.put(2131363318, 37);
        H0.put(2131362779, 38);
        H0.put(2131362294, 39);
        H0.put(2131363034, 40);
        H0.put(2131362177, 41);
        H0.put(2131361925, 42);
        H0.put(2131363395, 43);
        H0.put(2131362297, 44);
        H0.put(2131362825, 45);
        H0.put(2131361958, 46);
        H0.put(2131362751, 47);
        H0.put(2131361935, 48);
        H0.put(2131362664, 49);
        H0.put(2131363448, 50);
        H0.put(2131361937, 51);
        H0.put(2131362676, 52);
        H0.put(2131363453, 53);
        H0.put(2131361963, 54);
        H0.put(2131362764, 55);
        H0.put(2131361940, 56);
        H0.put(2131362689, 57);
        H0.put(2131361951, 58);
        H0.put(2131362739, 59);
        H0.put(2131361945, 60);
        H0.put(2131362723, 61);
        H0.put(2131361932, 62);
        H0.put(2131362656, 63);
        H0.put(2131361961, 64);
        H0.put(2131362757, 65);
        H0.put(2131363359, 66);
    }
    */

    @DexIgnore
    public C85(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 67, G0, H0));
    }

    @DexIgnore
    public C85(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (Barrier) objArr[42], (RelativeLayout) objArr[62], (RelativeLayout) objArr[48], (RelativeLayout) objArr[51], (RelativeLayout) objArr[56], (RelativeLayout) objArr[60], (RelativeLayout) objArr[58], (FlexibleButton) objArr[36], (RelativeLayout) objArr[46], (RelativeLayout) objArr[64], (RelativeLayout) objArr[54], (ConstraintLayout) objArr[10], (ConstraintLayout) objArr[18], (ConstraintLayout) objArr[13], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[28], (ConstraintLayout) objArr[33], (FlexibleButton) objArr[41], (FlexibleButton) objArr[39], (FlexibleButton) objArr[44], (FlexibleTextView) objArr[35], (FlexibleTextView) objArr[34], (Barrier) objArr[8], (Guideline) objArr[12], (ImageView) objArr[19], (ImageView) objArr[14], (ImageView) objArr[24], (ImageView) objArr[29], (RTLImageView) objArr[63], (RTLImageView) objArr[49], (RTLImageView) objArr[52], (RTLImageView) objArr[57], (RTLImageView) objArr[1], (RTLImageView) objArr[61], (RTLImageView) objArr[59], (RTLImageView) objArr[47], (RTLImageView) objArr[65], (RTLImageView) objArr[55], (FossilCircleImageView) objArr[2], (FossilCircleImageView) objArr[3], (ConstraintLayout) objArr[38], (LinearLayout) objArr[45], (NestedScrollView) objArr[0], (RecyclerView) objArr[40], (AutoResizeTextView) objArr[20], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[21], (AutoResizeTextView) objArr[25], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[26], (AutoResizeTextView) objArr[30], (FlexibleTextView) objArr[32], (FlexibleTextView) objArr[31], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[37], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[66], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[43], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (View) objArr[50], (View) objArr[53], (Guideline) objArr[11]);
        this.F0 = -1;
        this.g0.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.F0 = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.F0 != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.F0 = 1;
        }
        w();
    }
}
