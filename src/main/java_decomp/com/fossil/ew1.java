package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Cw1;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ew1 extends Mv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Vb0 e;
    @DexIgnore
    public String f;
    @DexIgnore
    public float g;
    @DexIgnore
    public String h;
    @DexIgnore
    public Cw1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ew1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ew1 createFromParcel(Parcel parcel) {
            return new Ew1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ew1[] newArray(int i) {
            return new Ew1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Ew1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = Vb0.d;
        String readString = parcel.readString();
        if (readString != null) {
            this.f = readString;
            this.g = parcel.readFloat();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                this.h = readString2;
                this.i = Cw1.values()[parcel.readInt()];
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Ew1(Jv1 jv1, Kv1 kv1, String str, float f2, String str2, Cw1 cw1) {
        this(G80.e(0, 1), jv1, kv1, str, f2, str2, cw1);
    }

    @DexIgnore
    public Ew1(String str, Jv1 jv1, Kv1 kv1, String str2, float f2, String str3, Cw1 cw1) {
        super(str, jv1, kv1);
        this.e = Vb0.d;
        this.f = str2;
        this.g = f2;
        this.h = str3;
        this.i = cw1;
    }

    @DexIgnore
    public Ew1(JSONObject jSONObject, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        this.e = Vb0.d;
        try {
            String string = jSONObject.getString("text");
            Wg6.b(string, "jsonObject.getString(UIScriptConstant.TEXT)");
            String string2 = jSONObject.getString("font_name");
            Wg6.b(string2, "jsonObject.getString(UIScriptConstant.FONT_NAME)");
            int i2 = jSONObject.getInt(ViewHierarchy.TEXT_SIZE);
            Cw1.Ai ai = Cw1.d;
            String string3 = jSONObject.getString("font_color");
            Wg6.b(string3, "jsonObject.getString(UIScriptConstant.FONT_COLOR)");
            Cw1 a2 = ai.a(string3);
            if (a2 != null) {
                this.f = string;
                this.g = (((float) i2) * 1.0f) / ((float) 240);
                this.h = string2;
                this.i = a2;
                return;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final Cc0 a(String str) {
        String jSONObject = d().toString();
        Wg6.b(jSONObject, "getUIScript().toString()");
        String a2 = Iy1.a(jSONObject);
        Charset c = Hd0.y.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return new Cc0(str, bytes);
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Vb0 a() {
        return this.e;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public Ew1 clone() {
        return new Ew1(getName(), b().clone(), c().clone(), this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public JSONObject d() {
        JSONObject put = super.d().put("text", this.f).put("font_name", this.h).put(ViewHierarchy.TEXT_SIZE, Lr7.b(this.g * ((float) 240))).put("font_color", Ey1.a(this.i));
        Wg6.b(put, "super.getUIScript()\n    \u2026 textColor.lowerCaseName)");
        return put;
    }

    @DexIgnore
    public final String getFontName() {
        return this.h;
    }

    @DexIgnore
    public final float getFontScaledSize() {
        return this.g;
    }

    @DexIgnore
    public final String getText() {
        return this.f;
    }

    @DexIgnore
    public final Cw1 getTextColor() {
        return this.i;
    }

    @DexIgnore
    public final Ew1 setFontName(String str) {
        this.h = str;
        return this;
    }

    @DexIgnore
    public final Ew1 setFontScaledSize(float f2) {
        this.g = f2;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledHeight(float f2) {
        Mv1 scaledHeight = super.setScaledHeight(f2);
        if (scaledHeight != null) {
            return (Ew1) scaledHeight;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledPosition(Jv1 jv1) {
        Mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (Ew1) scaledPosition;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledSize(Kv1 kv1) {
        Mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (Ew1) scaledSize;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledWidth(float f2) {
        Mv1 scaledWidth = super.setScaledWidth(f2);
        if (scaledWidth != null) {
            return (Ew1) scaledWidth;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledX(float f2) {
        Mv1 scaledX = super.setScaledX(f2);
        if (scaledX != null) {
            return (Ew1) scaledX;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Ew1 setScaledY(float f2) {
        Mv1 scaledY = super.setScaledY(f2);
        if (scaledY != null) {
            return (Ew1) scaledY;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.text.TextElement");
    }

    @DexIgnore
    public final Ew1 setText(String str) {
        this.f = str;
        return this;
    }

    @DexIgnore
    public final Ew1 setTextColor(Cw1 cw1) {
        this.i = cw1;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeString(this.f);
        }
        if (parcel != null) {
            parcel.writeFloat(this.g);
        }
        if (parcel != null) {
            parcel.writeString(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i.ordinal());
        }
    }
}
