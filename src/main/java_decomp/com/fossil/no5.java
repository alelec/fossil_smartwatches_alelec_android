package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class No5 {
    @DexIgnore
    public String a;
    @DexIgnore
    @Vu3("id")
    public String b;
    @DexIgnore
    @Vu3("name")
    public String c;
    @DexIgnore
    @Vu3("buttons")
    public List<Oo5> d;
    @DexIgnore
    @Vu3("isDefault")
    public boolean e;
    @DexIgnore
    @Vu3("downloadFaceUrl")
    public String f;
    @DexIgnore
    @Vu3("checksumFace")
    public String g;
    @DexIgnore
    @Vu3("createdAt")
    public String h;
    @DexIgnore
    @Vu3("updatedAt")
    public String i;

    @DexIgnore
    public final List<Oo5> a() {
        return this.d;
    }

    @DexIgnore
    public final String b() {
        return this.g;
    }

    @DexIgnore
    public final String c() {
        return this.h;
    }

    @DexIgnore
    public final String d() {
        return this.f;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof No5) {
                No5 no5 = (No5) obj;
                if (!Wg6.a(this.b, no5.b) || !Wg6.a(this.c, no5.c) || !Wg6.a(this.d, no5.d) || this.e != no5.e || !Wg6.a(this.f, no5.f) || !Wg6.a(this.g, no5.g) || !Wg6.a(this.h, no5.h) || !Wg6.a(this.i, no5.i)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final String g() {
        return this.a;
    }

    @DexIgnore
    public final String h() {
        return this.i;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        List<Oo5> list = this.d;
        int hashCode3 = list != null ? list.hashCode() : 0;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        String str3 = this.f;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.g;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.h;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.i;
        if (str6 != null) {
            i2 = str6.hashCode();
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i2;
    }

    @DexIgnore
    public final boolean i() {
        return this.e;
    }

    @DexIgnore
    public final void j(String str) {
        Wg6.c(str, "<set-?>");
        this.a = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaRecommendedPreset(id=" + this.b + ", name=" + this.c + ", button=" + this.d + ", isDefault=" + this.e + ", faceUrl=" + this.f + ", checkSum=" + this.g + ", createdAt=" + this.h + ", updatedAt=" + this.i + ")";
    }
}
