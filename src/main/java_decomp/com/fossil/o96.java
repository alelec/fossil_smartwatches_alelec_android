package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.fossil.ry5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o96 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<d> f2655a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<b> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<a> c; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<n66> d;
    @DexIgnore
    public /* final */ k97 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ FilterResult f2656a;

        @DexIgnore
        public a(FilterResult filterResult) {
            pq7.c(filterResult, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.f2656a = filterResult;
        }

        @DexIgnore
        public final FilterResult a() {
            return this.f2656a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof a) && pq7.a(this.f2656a, ((a) obj).f2656a));
        }

        @DexIgnore
        public int hashCode() {
            FilterResult filterResult = this.f2656a;
            if (filterResult != null) {
                return filterResult.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "ApplyFiltersResult(filters=" + this.f2656a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<FilterResult> f2657a;

        @DexIgnore
        public b(List<FilterResult> list) {
            pq7.c(list, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.f2657a = list;
        }

        @DexIgnore
        public final List<FilterResult> a() {
            return this.f2657a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof b) && pq7.a(this.f2657a, ((b) obj).f2657a));
        }

        @DexIgnore
        public int hashCode() {
            List<FilterResult> list = this.f2657a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "BuildFiltersResult(filters=" + this.f2657a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Bitmap f2658a;
        @DexIgnore
        public /* final */ float[] b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public /* final */ boolean h;

        @DexIgnore
        public c(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
            pq7.c(bitmap, "bitmap");
            pq7.c(fArr, "points");
            this.f2658a = bitmap;
            this.b = fArr;
            this.c = i;
            this.d = z;
            this.e = i2;
            this.f = i3;
            this.g = z2;
            this.h = z3;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.f;
        }

        @DexIgnore
        public final Bitmap c() {
            return this.f2658a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final boolean e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.h;
        }

        @DexIgnore
        public final float[] h() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f2659a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public d() {
            this(false, null, 3, null);
        }

        @DexIgnore
        public d(boolean z, String str) {
            pq7.c(str, "data");
            this.f2659a = z;
            this.b = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(boolean z, String str, int i, kq7 kq7) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? "" : str);
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.f2659a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof d) {
                    d dVar = (d) obj;
                    if (this.f2659a != dVar.f2659a || !pq7.a(this.b, dVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.f2659a;
            if (z) {
                z = true;
            }
            String str = this.b;
            int hashCode = str != null ? str.hashCode() : 0;
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (i * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "CropImageResult(isSuccess=" + this.f2659a + ", data=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1", f = "EditPhotoViewModel.kt", l = {83}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filter;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    EInkImageFilter create = EInkImageFilter.create();
                    pq7.b(create, "EInkImageFilter.create()");
                    OutputSettings outputSettings = new OutputSettings(this.this$0.$bitmap.getWidth(), this.this$0.$bitmap.getHeight(), Format.RAW, false, false);
                    e eVar = this.this$0;
                    FilterResult apply = create.apply(eVar.$bitmap, eVar.$filter, false, false, outputSettings);
                    pq7.b(apply, "algorithm.apply(bitmap, \u2026, false, false, settings)");
                    this.this$0.this$0.e().l(new a(apply));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(o96 o96, Bitmap bitmap, FilterType filterType, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o96;
            this.$bitmap = bitmap;
            this.$filter = filterType;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$bitmap, this.$filter, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1", f = "EditPhotoViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ c $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filterList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1$filters$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ArrayList<FilterResult>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ArrayList<FilterResult>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    c cVar = this.this$0.$cropImageInfo;
                    Bitmap a2 = ry5.a(ry5.h(cVar.c(), cVar.h(), cVar.d(), cVar.e(), cVar.a(), cVar.b(), cVar.f(), cVar.g()).f3184a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    pq7.b(create, "EInkImageFilter.create()");
                    pq7.b(a2, "bitmap");
                    return create.applyFilters(a2, this.this$0.$filterList, false, false, new OutputSettings(a2.getWidth(), a2.getHeight(), Format.RAW, false, false));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(o96 o96, c cVar, ArrayList arrayList, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o96;
            this.$cropImageInfo = cVar;
            this.$filterList = arrayList;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$cropImageInfo, this.$filterList, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            pq7.b(g, "withContext(IO) {\n      \u2026          }\n            }");
            this.this$0.f().l(new b((ArrayList) g));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1", f = "EditPhotoViewModel.kt", l = {43}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ c $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filterType;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ o96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1$1", f = "EditPhotoViewModel.kt", l = {56}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    c cVar = this.this$0.$cropImageInfo;
                    ry5.a h = ry5.h(cVar.c(), cVar.h(), cVar.d(), cVar.e(), cVar.a(), cVar.b(), cVar.f(), cVar.g());
                    Bitmap a2 = ry5.a(h.f3184a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    pq7.b(create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(a2, this.this$0.$filterType, false, false, OutputSettings.BACKGROUND);
                    pq7.b(apply, "algorithm.apply(rawBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap s = ry5.s(apply.getPreview(), false);
                    String e = ym5.e(24);
                    String str2 = FileUtils.getDirectory(this.this$0.$context, FileType.WATCH_FACE) + File.separator;
                    qk5 qk5 = qk5.f2994a;
                    pq7.b(e, "imageName");
                    pq7.b(s, "circleBitmap");
                    qk5.e(str2, e, s);
                    k97 k97 = this.this$0.this$0.e;
                    String str3 = str2 + e;
                    this.L$0 = iv7;
                    this.L$1 = cVar;
                    this.L$2 = h;
                    this.L$3 = a2;
                    this.L$4 = create;
                    this.L$5 = apply;
                    this.L$6 = s;
                    this.L$7 = e;
                    this.L$8 = str2;
                    this.label = 1;
                    if (k97.f(e, str3, this) == d) {
                        return d;
                    }
                    str = e;
                } else if (i == 1) {
                    String str4 = (String) this.L$8;
                    str = (String) this.L$7;
                    Bitmap bitmap = (Bitmap) this.L$6;
                    FilterResult filterResult = (FilterResult) this.L$5;
                    EInkImageFilter eInkImageFilter = (EInkImageFilter) this.L$4;
                    Bitmap bitmap2 = (Bitmap) this.L$3;
                    ry5.a aVar = (ry5.a) this.L$2;
                    c cVar2 = (c) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MutableLiveData<d> h2 = this.this$0.this$0.h();
                pq7.b(str, "imageName");
                h2.l(new d(true, str));
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(o96 o96, c cVar, FilterType filterType, Context context, qn7 qn7) {
            super(2, qn7);
            this.this$0 = o96;
            this.$cropImageInfo = cVar;
            this.$filterType = filterType;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$cropImageInfo, this.$filterType, this.$context, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public o96(k97 k97) {
        pq7.c(k97, "wfBackgroundPhotoRepository");
        this.e = k97;
    }

    @DexIgnore
    public final void b(Bitmap bitmap, FilterType filterType) {
        pq7.c(bitmap, "bitmap");
        pq7.c(filterType, "filter");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "applyFilter()");
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, bitmap, filterType, null), 3, null);
    }

    @DexIgnore
    public final void c(c cVar, ArrayList<FilterType> arrayList) {
        pq7.c(cVar, "cropImageInfo");
        pq7.c(arrayList, "filterList");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "buildFilterList()");
        xw7 unused = gu7.d(us0.a(this), null, null, new f(this, cVar, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void d(c cVar, FilterType filterType) {
        pq7.c(cVar, "cropImageInfo");
        pq7.c(filterType, "filterType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoViewModel", "cropImage(), filterType = " + filterType);
        xw7 unused = gu7.d(us0.a(this), null, null, new g(this, cVar, filterType, PortfolioApp.h0.c().getApplicationContext(), null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<a> e() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<b> f() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<n66> g() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<d> h() {
        return this.f2655a;
    }

    @DexIgnore
    public final void i(ArrayList<n66> arrayList) {
        this.d = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        jv7.d(us0.a(this), null, 1, null);
        super.onCleared();
    }
}
