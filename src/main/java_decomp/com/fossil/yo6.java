package com.fossil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Jn5;
import com.mapped.AlertDialogFragment;
import com.mapped.Iface;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.TimeUtils;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yo6 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public ProfileEditViewModel g;
    @DexIgnore
    public G37<La5> h;
    @DexIgnore
    public Wj5 i;
    @DexIgnore
    public Sv5 j;
    @DexIgnore
    public Z67 k;
    @DexIgnore
    public Po4 l;
    @DexIgnore
    public Kx6 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Yo6 a() {
            return new Yo6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Bi(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(Qh5.FEMALE);
            Yo6.O6(this.b).q(Qh5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Ci(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(Qh5.OTHER);
            Yo6.O6(this.b).q(Qh5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Di(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Yo6.O6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<ProfileEditViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 a;

        @DexIgnore
        public Ei(Yo6 yo6) {
            this.a = yo6;
        }

        @DexIgnore
        public final void a(ProfileEditViewModel.Ci ci) {
            FlexibleTextInputLayout flexibleTextInputLayout;
            FlexibleTextInputLayout flexibleTextInputLayout2;
            FossilCircleImageView fossilCircleImageView;
            FossilCircleImageView fossilCircleImageView2;
            FossilCircleImageView fossilCircleImageView3;
            FossilCircleImageView fossilCircleImageView4;
            FlexibleTextInputLayout flexibleTextInputLayout3;
            La5 la5;
            FlexibleTextInputEditText flexibleTextInputEditText;
            MFUser j = ci.j();
            if (j != null) {
                this.a.b7(j);
            }
            Boolean k = ci.k();
            if (k != null) {
                this.a.d7(k.booleanValue());
            }
            Uri e = ci.e();
            if (e != null) {
                this.a.a7(e);
            }
            if (ci.f()) {
                this.a.m();
            } else {
                this.a.k();
            }
            if (ci.i() != null) {
                this.a.e0();
            }
            Lc6<Integer, String> h = ci.h();
            if (h != null) {
                this.a.o(h.getFirst().intValue(), h.getSecond());
            }
            if (ci.g()) {
                this.a.E0();
            }
            Bundle d = ci.d();
            if (d != null) {
                this.a.z0(d);
            }
            String a2 = ci.a();
            if (!(a2 == null || (la5 = (La5) Yo6.M6(this.a).a()) == null || (flexibleTextInputEditText = la5.L) == null)) {
                flexibleTextInputEditText.setText(a2);
            }
            if (ci.b() == null) {
                La5 la52 = (La5) Yo6.M6(this.a).a();
                if (!(la52 == null || (flexibleTextInputLayout3 = la52.C) == null)) {
                    flexibleTextInputLayout3.setErrorEnabled(false);
                }
            } else {
                La5 la53 = (La5) Yo6.M6(this.a).a();
                if (!(la53 == null || (flexibleTextInputLayout2 = la53.C) == null)) {
                    flexibleTextInputLayout2.setErrorEnabled(true);
                }
                La5 la54 = (La5) Yo6.M6(this.a).a();
                if (!(la54 == null || (flexibleTextInputLayout = la54.C) == null)) {
                    flexibleTextInputLayout.setError(ci.b());
                }
            }
            ProfileEditViewModel.Ai c = ci.c();
            if (c != null && c.a() != null) {
                String a3 = c.a();
                if (a3 != null) {
                    Bitmap e2 = I37.e(a3);
                    if (e2 != null) {
                        Vj5<Drawable> T0 = Yo6.N6(this.a).F(e2).T0(new Fj1().o0(new Hk5()));
                        La5 la55 = (La5) Yo6.M6(this.a).a();
                        FossilCircleImageView fossilCircleImageView5 = la55 != null ? la55.q : null;
                        if (fossilCircleImageView5 != null) {
                            T0.F0(fossilCircleImageView5);
                            La5 la56 = (La5) Yo6.M6(this.a).a();
                            if (la56 != null && (fossilCircleImageView4 = la56.q) != null) {
                                fossilCircleImageView4.setBorderColor(W6.d(this.a.requireContext(), R.color.transparent));
                                return;
                            }
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    String str = c.b() + " " + c.c();
                    if (!(str == null || Vt7.l(str))) {
                        Vj5<Drawable> T02 = Yo6.N6(this.a).I(new Sj5("", str)).T0(new Fj1().o0(new Hk5()));
                        La5 la57 = (La5) Yo6.M6(this.a).a();
                        FossilCircleImageView fossilCircleImageView6 = la57 != null ? la57.q : null;
                        if (fossilCircleImageView6 != null) {
                            T02.F0(fossilCircleImageView6);
                            La5 la58 = (La5) Yo6.M6(this.a).a();
                            if (!(la58 == null || (fossilCircleImageView3 = la58.q) == null)) {
                                fossilCircleImageView3.setBorderColor(W6.d(PortfolioApp.get.instance(), 2131099830));
                            }
                            La5 la59 = (La5) Yo6.M6(this.a).a();
                            if (!(la59 == null || (fossilCircleImageView2 = la59.q) == null)) {
                                fossilCircleImageView2.setBorderWidth(3);
                            }
                            La5 la510 = (La5) Yo6.M6(this.a).a();
                            if (la510 != null && (fossilCircleImageView = la510.q) != null) {
                                fossilCircleImageView.setBackground(W6.f(PortfolioApp.get.instance(), 2131231287));
                                return;
                            }
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    return;
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(ProfileEditViewModel.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 a;

        @DexIgnore
        public Fi(Yo6 yo6) {
            this.a = yo6;
        }

        @DexIgnore
        public final void a(Date date) {
            if (date != null) {
                Yo6.O6(this.a).n(date);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Date date) {
            a(date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Gi(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hi(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            ProfileEditViewModel O6 = Yo6.O6(this.b);
            La5 la5 = (La5) Yo6.M6(this.b).a();
            String valueOf = String.valueOf((la5 == null || (flexibleTextInputEditText = la5.s) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                O6.p(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ii(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            ProfileEditViewModel O6 = Yo6.O6(this.b);
            La5 la5 = (La5) Yo6.M6(this.b).a();
            String valueOf = String.valueOf((la5 == null || (flexibleTextInputEditText = la5.t) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                O6.s(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Ji(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.c7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Ki(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements K67 {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 a;
        @DexIgnore
        public /* final */ /* synthetic */ La5 b;

        @DexIgnore
        public Li(Yo6 yo6, La5 la5) {
            this.a = yo6;
            this.b = la5;
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void a(int i) {
            if (this.b.I.getUnit() == Ai5.METRIC) {
                Yo6.O6(this.a).r(i);
            } else {
                Yo6.O6(this.a).r(Lr7.b(Jk5.d((float) (i / 12), ((float) i) % 12.0f)));
            }
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void c(boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements K67 {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 a;
        @DexIgnore
        public /* final */ /* synthetic */ La5 b;

        @DexIgnore
        public Mi(Yo6 yo6, La5 la5) {
            this.a = yo6;
            this.b = la5;
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void a(int i) {
            if (this.b.J.getUnit() == Ai5.METRIC) {
                Yo6.O6(this.a).u(Lr7.b((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            Yo6.O6(this.a).u(Lr7.b(Jk5.m(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void c(boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Yo6 b;

        @DexIgnore
        public Ni(Yo6 yo6) {
            this.b = yo6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6(Qh5.MALE);
            Yo6.O6(this.b).q(Qh5.MALE);
        }
    }

    /*
    static {
        String simpleName = Yo6.class.getSimpleName();
        Wg6.b(simpleName, "ProfileEditFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 M6(Yo6 yo6) {
        G37<La5> g37 = yo6.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Wj5 N6(Yo6 yo6) {
        Wj5 wj5 = yo6.i;
        if (wj5 != null) {
            return wj5;
        }
        Wg6.n("mGlideRequests");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProfileEditViewModel O6(Yo6 yo6) {
        ProfileEditViewModel profileEditViewModel = yo6.g;
        if (profileEditViewModel != null) {
            return profileEditViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @T78(1222)
    public final void doCameraTask() {
        FragmentActivity activity;
        Intent f;
        if (Jn5.c(Jn5.b, getActivity(), Jn5.Ai.EDIT_AVATAR, false, false, false, null, 60, null) && (activity = getActivity()) != null && (f = Vk5.f(activity)) != null) {
            startActivityForResult(f, 1234);
        }
    }

    @DexIgnore
    public final void E0() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                Wg6.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    ProfileEditViewModel profileEditViewModel = this.g;
                    if (profileEditViewModel == null) {
                        Wg6.n("mViewModel");
                        throw null;
                    } else if (profileEditViewModel.l()) {
                        S37 s37 = S37.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        Wg6.b(childFragmentManager, "childFragmentManager");
                        s37.y0(childFragmentManager);
                    } else {
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            activity.finish();
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void H1(int i2, Ai5 ai5) {
        int i3 = Zo6.b[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "updateData weight=" + i2 + " metric");
            G37<La5> g37 = this.h;
            if (g37 != null) {
                La5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    Wg6.b(flexibleTextView, "it.ftvWeightUnit");
                    flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131886955));
                    a2.J.setUnit(Ai5.METRIC);
                    a2.J.setFormatter(new ProfileFormatter(4));
                    a2.J.l(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            G37<La5> g372 = this.h;
            if (g372 != null) {
                La5 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.B;
                    Wg6.b(flexibleTextView2, "it.ftvWeightUnit");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131886956));
                    float h2 = Jk5.h((float) i2);
                    a3.J.setUnit(Ai5.IMPERIAL);
                    a3.J.setFormatter(new ProfileFormatter(4));
                    a3.J.l(780, 4401, Math.round(h2 * ((float) 10)));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void R0(int i2, Ai5 ai5) {
        int i3 = Zo6.a[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "updateData height=" + i2 + " metric");
            G37<La5> g37 = this.h;
            if (g37 != null) {
                La5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    Wg6.b(flexibleTextView, "it.ftvHeightUnit");
                    flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131887202));
                    a2.I.setUnit(Ai5.METRIC);
                    a2.I.setFormatter(new ProfileFormatter(-1));
                    a2.I.l(100, 251, i2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            G37<La5> g372 = this.h;
            if (g372 != null) {
                La5 a3 = g372.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.z;
                    Wg6.b(flexibleTextView2, "it.ftvHeightUnit");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887203));
                    Lc6<Integer, Integer> b = Jk5.b((float) i2);
                    a3.I.setUnit(Ai5.IMPERIAL);
                    a3.I.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.I;
                    Integer first = b.getFirst();
                    Wg6.b(first, "currentHeightInFeetAndInches.first");
                    int e = Jk5.e(first.intValue());
                    Integer second = b.getSecond();
                    Wg6.b(second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.l(40, 99, second.intValue() + e);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        Wg6.c(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).R5(str, i2, intent);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363291) {
            e0();
        } else if (i2 == 2131363373) {
            c7();
        }
    }

    @DexIgnore
    public final void Z6(Qh5 qh5) {
        G37<La5> g37 = this.h;
        if (g37 != null) {
            La5 a2 = g37.a();
            if (a2 != null) {
                a2.u.d("flexible_button_secondary");
                a2.v.d("flexible_button_secondary");
                a2.w.d("flexible_button_secondary");
                int i2 = Zo6.c[qh5.ordinal()];
                if (i2 == 1) {
                    G37<La5> g372 = this.h;
                    if (g372 != null) {
                        La5 a3 = g372.a();
                        if (a3 != null && a3.u != null) {
                            a2.u.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    G37<La5> g373 = this.h;
                    if (g373 != null) {
                        La5 a4 = g373.a();
                        if (a4 != null && a4.w != null) {
                            a2.w.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                } else {
                    G37<La5> g374 = this.h;
                    if (g374 != null) {
                        La5 a5 = g374.a();
                        if (a5 != null && a5.v != null) {
                            a2.v.d("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(Uri uri) {
        if (isActive()) {
            Wj5 wj5 = this.i;
            if (wj5 != null) {
                Vj5<Drawable> T0 = wj5.G(uri).X0(Wc1.a).T0(new Fj1().o0(new Hk5()));
                G37<La5> g37 = this.h;
                if (g37 != null) {
                    La5 a2 = g37.a();
                    FossilCircleImageView fossilCircleImageView = a2 != null ? a2.q : null;
                    if (fossilCircleImageView != null) {
                        T0.F0(fossilCircleImageView);
                        d7(true);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"SimpleDateFormat"})
    public final void b7(MFUser mFUser) {
        FossilCircleImageView fossilCircleImageView;
        FossilCircleImageView fossilCircleImageView2;
        FossilCircleImageView fossilCircleImageView3;
        FlexibleTextInputEditText flexibleTextInputEditText;
        FlexibleTextView flexibleTextView;
        FlexibleTextInputEditText flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3;
        FossilCircleImageView fossilCircleImageView4;
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            Wj5 wj5 = this.i;
            if (wj5 != null) {
                Vj5<Drawable> T0 = wj5.I(new Sj5("", str)).T0(new Fj1().o0(new Hk5()));
                G37<La5> g37 = this.h;
                if (g37 != null) {
                    La5 a2 = g37.a();
                    FossilCircleImageView fossilCircleImageView5 = a2 != null ? a2.q : null;
                    if (fossilCircleImageView5 != null) {
                        T0.F0(fossilCircleImageView5);
                        G37<La5> g372 = this.h;
                        if (g372 != null) {
                            La5 a3 = g372.a();
                            if (!(a3 == null || (fossilCircleImageView3 = a3.q) == null)) {
                                fossilCircleImageView3.setBorderColor(W6.d(PortfolioApp.get.instance(), 2131099830));
                            }
                            G37<La5> g373 = this.h;
                            if (g373 != null) {
                                La5 a4 = g373.a();
                                if (!(a4 == null || (fossilCircleImageView2 = a4.q) == null)) {
                                    fossilCircleImageView2.setBorderWidth(3);
                                }
                                G37<La5> g374 = this.h;
                                if (g374 != null) {
                                    La5 a5 = g374.a();
                                    if (!(a5 == null || (fossilCircleImageView = a5.q) == null)) {
                                        fossilCircleImageView.setBackground(W6.f(PortfolioApp.get.instance(), 2131231287));
                                    }
                                } else {
                                    Wg6.n("mBinding");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mBinding");
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mGlideRequests");
                throw null;
            }
        } else {
            G37<La5> g375 = this.h;
            if (g375 != null) {
                La5 a6 = g375.a();
                if (!(a6 == null || (fossilCircleImageView4 = a6.q) == null)) {
                    Wj5 wj52 = this.i;
                    if (wj52 != null) {
                        fossilCircleImageView4.j(wj52, profilePicture, str);
                    } else {
                        Wg6.n("mGlideRequests");
                        throw null;
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        G37<La5> g376 = this.h;
        if (g376 != null) {
            La5 a7 = g376.a();
            if (!(a7 == null || (flexibleTextInputEditText3 = a7.s) == null)) {
                flexibleTextInputEditText3.setText(mFUser.getFirstName());
            }
            G37<La5> g377 = this.h;
            if (g377 != null) {
                La5 a8 = g377.a();
                if (!(a8 == null || (flexibleTextInputEditText2 = a8.t) == null)) {
                    flexibleTextInputEditText2.setText(mFUser.getLastName());
                }
                G37<La5> g378 = this.h;
                if (g378 != null) {
                    La5 a9 = g378.a();
                    if (!(a9 == null || (flexibleTextView = a9.M) == null)) {
                        flexibleTextView.setText(mFUser.getEmail());
                    }
                    Gk5 gk5 = Gk5.a;
                    Qh5 a10 = Qh5.Companion.a(mFUser.getGender());
                    String birthday = mFUser.getBirthday();
                    if (birthday != null) {
                        Lc6<Integer, Integer> c = gk5.c(a10, mFUser.getAge(birthday));
                        if (mFUser.getHeightInCentimeters() == 0) {
                            mFUser.setHeightInCentimeters(c.getFirst().intValue());
                        }
                        if (mFUser.getHeightInCentimeters() > 0) {
                            int heightInCentimeters = mFUser.getHeightInCentimeters();
                            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                            String height = unitGroup != null ? unitGroup.getHeight() : null;
                            if (height != null) {
                                Ai5 fromString = Ai5.fromString(height);
                                Wg6.b(fromString, "Unit.fromString(user.unitGroup?.height!!)");
                                R0(heightInCentimeters, fromString);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        if (mFUser.getWeightInGrams() == 0) {
                            mFUser.setWeightInGrams(c.getSecond().intValue() * 1000);
                        }
                        if (mFUser.getWeightInGrams() > 0) {
                            int weightInGrams = mFUser.getWeightInGrams();
                            MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                            String weight = unitGroup2 != null ? unitGroup2.getWeight() : null;
                            if (weight != null) {
                                Ai5 fromString2 = Ai5.fromString(weight);
                                Wg6.b(fromString2, "Unit.fromString(user.unitGroup?.weight!!)");
                                H1(weightInGrams, fromString2);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        String birthday2 = mFUser.getBirthday();
                        if (!TextUtils.isEmpty(birthday2)) {
                            try {
                                Date r0 = TimeUtils.r0(birthday2);
                                G37<La5> g379 = this.h;
                                if (g379 != null) {
                                    La5 a11 = g379.a();
                                    if (!(a11 == null || (flexibleTextInputEditText = a11.L) == null)) {
                                        flexibleTextInputEditText.setText(TimeUtils.f(r0));
                                    }
                                } else {
                                    Wg6.n("mBinding");
                                    throw null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Z6(Qh5.Companion.a(mFUser.getGender()));
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7() {
        ProfileEditViewModel profileEditViewModel = this.g;
        if (profileEditViewModel != null) {
            profileEditViewModel.v();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0048, code lost:
        if ((com.fossil.Wt7.u0(r3).length() > 0) != false) goto L_0x004a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d7(boolean r6) {
        /*
            r5 = this;
            r1 = 1
            r2 = 0
            com.fossil.G37<com.fossil.La5> r0 = r5.h
            if (r0 == 0) goto L_0x006c
            java.lang.Object r0 = r0.a()
            com.fossil.La5 r0 = (com.fossil.La5) r0
            if (r0 == 0) goto L_0x005d
            if (r6 == 0) goto L_0x0062
            com.portfolio.platform.view.FlexibleTextInputEditText r3 = r0.s
            java.lang.String r4 = "it.etFirstName"
            com.mapped.Wg6.b(r3, r4)
            android.text.Editable r3 = r3.getEditableText()
            java.lang.String r4 = "it.etFirstName.editableText"
            com.mapped.Wg6.b(r3, r4)
            java.lang.CharSequence r3 = com.fossil.Wt7.u0(r3)
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x005e
            r3 = r1
        L_0x002b:
            if (r3 == 0) goto L_0x0062
            com.portfolio.platform.view.FlexibleTextInputEditText r3 = r0.t
            java.lang.String r4 = "it.etLastName"
            com.mapped.Wg6.b(r3, r4)
            android.text.Editable r3 = r3.getEditableText()
            java.lang.String r4 = "it.etLastName.editableText"
            com.mapped.Wg6.b(r3, r4)
            java.lang.CharSequence r3 = com.fossil.Wt7.u0(r3)
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x0060
            r3 = r1
        L_0x0048:
            if (r3 == 0) goto L_0x0062
        L_0x004a:
            com.portfolio.platform.view.ProgressButton r2 = r0.K
            java.lang.String r3 = "it.save"
            com.mapped.Wg6.b(r2, r3)
            r2.setEnabled(r1)
            if (r1 == 0) goto L_0x0064
            com.portfolio.platform.view.ProgressButton r0 = r0.K
            java.lang.String r1 = "flexible_button_primary"
            r0.d(r1)
        L_0x005d:
            return
        L_0x005e:
            r3 = r2
            goto L_0x002b
        L_0x0060:
            r3 = r2
            goto L_0x0048
        L_0x0062:
            r1 = r2
            goto L_0x004a
        L_0x0064:
            com.portfolio.platform.view.ProgressButton r0 = r0.K
            java.lang.String r1 = "flexible_button_disabled"
            r0.d(r1)
            goto L_0x005d
        L_0x006c:
            java.lang.String r0 = "mBinding"
            com.mapped.Wg6.n(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yo6.d7(boolean):void");
    }

    @DexIgnore
    public final void e0() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                Wg6.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    requireActivity().finish();
                }
            }
        }
    }

    @DexIgnore
    public final void k() {
        ProgressButton progressButton;
        G37<La5> g37 = this.h;
        if (g37 != null) {
            La5 a2 = g37.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.f();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void m() {
        ProgressButton progressButton;
        G37<La5> g37 = this.h;
        if (g37 != null) {
            La5 a2 = g37.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.g();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void o(int i2, String str) {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            ProfileEditViewModel profileEditViewModel = this.g;
            if (profileEditViewModel != null) {
                profileEditViewModel.t(intent);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        La5 la5 = (La5) Aq0.f(LayoutInflater.from(getContext()), 2131558610, null, false, A6());
        Sv5 sv5 = (Sv5) getChildFragmentManager().Z(Sv5.u.a());
        this.j = sv5;
        if (sv5 == null) {
            this.j = Sv5.u.b();
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        Sv5 sv52 = this.j;
        if (sv52 != null) {
            iface.C1(new Ix6(sv52)).a(this);
            Po4 po4 = this.l;
            if (po4 != null) {
                Ts0 a2 = Vs0.d(this, po4).a(ProfileEditViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
                this.g = (ProfileEditViewModel) a2;
                Ts0 a3 = Vs0.e(requireActivity()).a(Z67.class);
                Wg6.b(a3, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
                Z67 z67 = (Z67) a3;
                this.k = z67;
                if (z67 != null) {
                    z67.a().h(getViewLifecycleOwner(), new Fi(this));
                    la5.F.setOnClickListener(new Gi(this));
                    la5.s.addTextChangedListener(new Hi(this));
                    la5.t.addTextChangedListener(new Ii(this));
                    la5.K.setOnClickListener(new Ji(this));
                    FossilCircleImageView fossilCircleImageView = la5.q;
                    Wg6.b(fossilCircleImageView, "binding.avatar");
                    Nl5.a(fossilCircleImageView, new Ki(this));
                    la5.I.setValuePickerListener(new Li(this, la5));
                    la5.J.setValuePickerListener(new Mi(this, la5));
                    la5.v.setOnClickListener(new Ni(this));
                    la5.u.setOnClickListener(new Bi(this));
                    la5.w.setOnClickListener(new Ci(this));
                    la5.L.setOnClickListener(new Di(this));
                    ProfileEditViewModel profileEditViewModel = this.g;
                    if (profileEditViewModel != null) {
                        profileEditViewModel.i().h(getViewLifecycleOwner(), new Ei(this));
                        this.h = new G37<>(this, la5);
                        Wg6.b(la5, "binding");
                        return la5.n();
                    }
                    Wg6.n("mViewModel");
                    throw null;
                }
                Wg6.n("mUserBirthDayViewModel");
                throw null;
            }
            Wg6.n("viewModelFactory");
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ProfileEditViewModel profileEditViewModel = this.g;
        if (profileEditViewModel != null) {
            profileEditViewModel.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("edit_profile_view");
        Wj5 c = Tj5.c(this);
        Wg6.b(c, "GlideApp.with(this)");
        this.i = c;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void z0(Bundle bundle) {
        Sv5 sv5 = this.j;
        if (sv5 != null) {
            sv5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            sv5.show(childFragmentManager, Sv5.u.a());
        }
    }
}
