package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ds {
    @DexIgnore
    public /* final */ /* synthetic */ Fs a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public Ds(Fs fs) {
        this.a = fs;
    }

    @DexIgnore
    public void a(N7 n7) {
        if (n7 instanceof O7) {
            O7 o7 = (O7) n7;
            if (this.a.a(o7) > 0) {
                Fs fs = this.a;
                fs.f(fs.a(o7));
                Fs fs2 = this.a;
                fs2.n(fs2.p);
                Fs fs3 = this.a;
                fs3.g.add(new Hw(0, o7.a, o7.b, G80.k(new JSONObject(), Jd0.i1, Long.valueOf(this.a.x())), 1));
                return;
            }
        }
        this.a.h(n7);
    }
}
