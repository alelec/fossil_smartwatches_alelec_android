package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uj5 extends Fj1 implements Cloneable {
    @DexIgnore
    public Uj5 A0(Wc1 wc1) {
        return (Uj5) super.l(wc1);
    }

    @DexIgnore
    public Uj5 B0() {
        return (Uj5) super.n();
    }

    @DexIgnore
    public Uj5 C0(Fg1 fg1) {
        return (Uj5) super.o(fg1);
    }

    @DexIgnore
    public Uj5 D0() {
        super.V();
        return this;
    }

    @DexIgnore
    public Uj5 E0() {
        return (Uj5) super.W();
    }

    @DexIgnore
    public Uj5 F0() {
        return (Uj5) super.X();
    }

    @DexIgnore
    public Uj5 G0() {
        return (Uj5) super.Y();
    }

    @DexIgnore
    public Uj5 H0(int i, int i2) {
        return (Uj5) super.b0(i, i2);
    }

    @DexIgnore
    public Uj5 I0(int i) {
        return (Uj5) super.c0(i);
    }

    @DexIgnore
    public Uj5 J0(Drawable drawable) {
        return (Uj5) super.d0(drawable);
    }

    @DexIgnore
    public Uj5 K0(Sa1 sa1) {
        return (Uj5) super.e0(sa1);
    }

    @DexIgnore
    public <Y> Uj5 L0(Nb1<Y> nb1, Y y) {
        return (Uj5) super.j0(nb1, y);
    }

    @DexIgnore
    public Uj5 M0(Mb1 mb1) {
        return (Uj5) super.k0(mb1);
    }

    @DexIgnore
    public Uj5 N0(float f) {
        return (Uj5) super.l0(f);
    }

    @DexIgnore
    public Uj5 O0(boolean z) {
        return (Uj5) super.m0(z);
    }

    @DexIgnore
    public Uj5 P0(Sb1<Bitmap> sb1) {
        return (Uj5) super.o0(sb1);
    }

    @DexIgnore
    public Uj5 Q0(boolean z) {
        return (Uj5) super.s0(z);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 V() {
        return D0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 W() {
        return E0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 X() {
        return F0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 Y() {
        return G0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 b0(int i, int i2) {
        return H0(i, i2);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 c0(int i) {
        return I0(i);
    }

    @DexIgnore
    @Override // com.fossil.Yi1, java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return y0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 d(Yi1 yi1) {
        return w0(yi1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 d0(Drawable drawable) {
        return J0(drawable);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 e() {
        return x0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 e0(Sa1 sa1) {
        return K0(sa1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 i() {
        return y0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 j(Class cls) {
        return z0(cls);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 j0(Nb1 nb1, Object obj) {
        return L0(nb1, obj);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 k0(Mb1 mb1) {
        return M0(mb1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 l(Wc1 wc1) {
        return A0(wc1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 l0(float f) {
        return N0(f);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 m0(boolean z) {
        return O0(z);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 n() {
        return B0();
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 o(Fg1 fg1) {
        return C0(fg1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 o0(Sb1 sb1) {
        return P0(sb1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Yi1' to match base method */
    @Override // com.fossil.Yi1
    public /* bridge */ /* synthetic */ Fj1 s0(boolean z) {
        return Q0(z);
    }

    @DexIgnore
    public Uj5 w0(Yi1<?> yi1) {
        return (Uj5) super.d(yi1);
    }

    @DexIgnore
    public Uj5 x0() {
        return (Uj5) super.e();
    }

    @DexIgnore
    public Uj5 y0() {
        return (Uj5) super.i();
    }

    @DexIgnore
    public Uj5 z0(Class<?> cls) {
        return (Uj5) super.j(cls);
    }
}
