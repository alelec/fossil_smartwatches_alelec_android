package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bc1 extends Ec1<ParcelFileDescriptor> {
    @DexIgnore
    public Bc1(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ void b(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        f(parcelFileDescriptor);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ ParcelFileDescriptor e(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        return g(uri, contentResolver);
    }

    @DexIgnore
    public void f(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        parcelFileDescriptor.close();
    }

    @DexIgnore
    public ParcelFileDescriptor g(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor.getParcelFileDescriptor();
        }
        throw new FileNotFoundException("FileDescriptor is null for: " + uri);
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Class<ParcelFileDescriptor> getDataClass() {
        return ParcelFileDescriptor.class;
    }
}
