package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ur2 implements Parcelable.Creator<Tr2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Tr2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Za3 za3 = Tr2.f;
        List<Zb2> list = Tr2.e;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                za3 = (Za3) Ad2.e(parcel, t, Za3.CREATOR);
            } else if (l == 2) {
                list = Ad2.j(parcel, t, Zb2.CREATOR);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                str = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Tr2(za3, list, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Tr2[] newArray(int i) {
        return new Tr2[i];
    }
}
