package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vn1 {
    PLAY((byte) 0),
    PAUSE((byte) 1),
    TOGGLE_PLAY_PAUSE((byte) 2),
    NEXT((byte) 3),
    PREVIOUS((byte) 4),
    VOLUME_UP((byte) 5),
    VOLUME_DOWN((byte) 6);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        public final vn1 a(byte b) {
            vn1[] values = vn1.values();
            for (vn1 vn1 : values) {
                if (vn1.a() == b) {
                    return vn1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public vn1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
