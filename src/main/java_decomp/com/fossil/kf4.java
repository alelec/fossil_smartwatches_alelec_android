package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kf4 {
    @DexIgnore
    public static Kf4 e;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public Bi c; // = new Bi();
    @DexIgnore
    public int d; // = 1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements ServiceConnection {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ Messenger b;
        @DexIgnore
        public Ci c;
        @DexIgnore
        public /* final */ Queue<Ei<?>> d;
        @DexIgnore
        public /* final */ SparseArray<Ei<?>> e;

        @DexIgnore
        public Bi() {
            this.a = 0;
            this.b = new Messenger(new Dm2(Looper.getMainLooper(), new Lf4(this)));
            this.d = new ArrayDeque();
            this.e = new SparseArray<>();
        }

        @DexIgnore
        public boolean a(Ei<?> ei) {
            synchronized (this) {
                int i = this.a;
                if (i == 0) {
                    this.d.add(ei);
                    k();
                    return true;
                } else if (i == 1) {
                    this.d.add(ei);
                    return true;
                } else if (i == 2) {
                    this.d.add(ei);
                    i();
                    return true;
                } else if (i == 3 || i == 4) {
                    return false;
                } else {
                    int i2 = this.a;
                    StringBuilder sb = new StringBuilder(26);
                    sb.append("Unknown state: ");
                    sb.append(i2);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }

        @DexIgnore
        public void b(Fi fi) {
            for (Ei<?> ei : this.d) {
                ei.b(fi);
            }
            this.d.clear();
            for (int i = 0; i < this.e.size(); i++) {
                this.e.valueAt(i).b(fi);
            }
            this.e.clear();
        }

        @DexIgnore
        public void c(int i, String str) {
            synchronized (this) {
                if (Log.isLoggable("MessengerIpcClient", 3)) {
                    String valueOf = String.valueOf(str);
                    Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
                }
                int i2 = this.a;
                if (i2 == 0) {
                    throw new IllegalStateException();
                } else if (i2 == 1 || i2 == 2) {
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Unbinding service");
                    }
                    this.a = 4;
                    Ve2.b().c(Kf4.this.a, this);
                    b(new Fi(i, str));
                } else if (i2 == 3) {
                    this.a = 4;
                } else if (i2 != 4) {
                    int i3 = this.a;
                    StringBuilder sb = new StringBuilder(26);
                    sb.append("Unknown state: ");
                    sb.append(i3);
                    throw new IllegalStateException(sb.toString());
                }
            }
        }

        @DexIgnore
        public final /* synthetic */ void d(IBinder iBinder) {
            synchronized (this) {
                if (iBinder == null) {
                    c(0, "Null service connection");
                    return;
                }
                try {
                    this.c = new Ci(iBinder);
                    this.a = 2;
                    i();
                } catch (RemoteException e2) {
                    c(0, e2.getMessage());
                }
            }
        }

        @DexIgnore
        public final /* synthetic */ void e() {
            c(2, "Service disconnected");
        }

        @DexIgnore
        public final /* synthetic */ void f(Ei ei) {
            m(ei.a);
        }

        @DexIgnore
        public final /* synthetic */ void g() {
            Ei<?> poll;
            while (true) {
                synchronized (this) {
                    if (this.a == 2) {
                        if (this.d.isEmpty()) {
                            n();
                            return;
                        }
                        poll = this.d.poll();
                        this.e.put(poll.a, poll);
                        Kf4.this.b.schedule(new Qf4(this, poll), 30, TimeUnit.SECONDS);
                    } else {
                        return;
                    }
                }
                j(poll);
            }
        }

        @DexIgnore
        public boolean h(Message message) {
            int i = message.arg1;
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                StringBuilder sb = new StringBuilder(41);
                sb.append("Received response to request: ");
                sb.append(i);
                Log.d("MessengerIpcClient", sb.toString());
            }
            synchronized (this) {
                Ei<?> ei = this.e.get(i);
                if (ei == null) {
                    StringBuilder sb2 = new StringBuilder(50);
                    sb2.append("Received response for unknown request: ");
                    sb2.append(i);
                    Log.w("MessengerIpcClient", sb2.toString());
                } else {
                    this.e.remove(i);
                    n();
                    ei.e(message.getData());
                }
            }
            return true;
        }

        @DexIgnore
        public void i() {
            Kf4.this.b.execute(new Of4(this));
        }

        @DexIgnore
        public void j(Ei<?> ei) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(ei);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
                sb.append("Sending ");
                sb.append(valueOf);
                Log.d("MessengerIpcClient", sb.toString());
            }
            try {
                this.c.a(ei.a(Kf4.this.a, this.b));
            } catch (RemoteException e2) {
                c(2, e2.getMessage());
            }
        }

        @DexIgnore
        public void k() {
            Rc2.n(this.a == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!Ve2.b().a(Kf4.this.a, intent, this, 1)) {
                c(0, "Unable to bind to service");
            } else {
                Kf4.this.b.schedule(new Mf4(this), 30, TimeUnit.SECONDS);
            }
        }

        @DexIgnore
        public void l() {
            synchronized (this) {
                if (this.a == 1) {
                    c(1, "Timed out while binding");
                }
            }
        }

        @DexIgnore
        public void m(int i) {
            synchronized (this) {
                Ei<?> ei = this.e.get(i);
                if (ei != null) {
                    StringBuilder sb = new StringBuilder(31);
                    sb.append("Timing out request: ");
                    sb.append(i);
                    Log.w("MessengerIpcClient", sb.toString());
                    this.e.remove(i);
                    ei.b(new Fi(3, "Timed out waiting for response"));
                    n();
                }
            }
        }

        @DexIgnore
        public void n() {
            synchronized (this) {
                if (this.a == 2 && this.d.isEmpty() && this.e.size() == 0) {
                    if (Log.isLoggable("MessengerIpcClient", 2)) {
                        Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
                    }
                    this.a = 3;
                    Ve2.b().c(Kf4.this.a, this);
                }
            }
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service connected");
            }
            Kf4.this.b.execute(new Nf4(this, iBinder));
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Service disconnected");
            }
            Kf4.this.b.execute(new Pf4(this));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ Messenger a;
        @DexIgnore
        public /* final */ Te4 b;

        @DexIgnore
        public Ci(IBinder iBinder) throws RemoteException {
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("android.os.IMessenger".equals(interfaceDescriptor)) {
                this.a = new Messenger(iBinder);
                this.b = null;
            } else if ("com.google.android.gms.iid.IMessengerCompat".equals(interfaceDescriptor)) {
                this.b = new Te4(iBinder);
                this.a = null;
            } else {
                String valueOf = String.valueOf(interfaceDescriptor);
                Log.w("MessengerIpcClient", valueOf.length() != 0 ? "Invalid interface descriptor: ".concat(valueOf) : new String("Invalid interface descriptor: "));
                throw new RemoteException();
            }
        }

        @DexIgnore
        public void a(Message message) throws RemoteException {
            Messenger messenger = this.a;
            if (messenger != null) {
                messenger.send(message);
                return;
            }
            Te4 te4 = this.b;
            if (te4 != null) {
                te4.b(message);
                return;
            }
            throw new IllegalStateException("Both messengers are null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends Ei<Void> {
        @DexIgnore
        public Di(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.Kf4.Ei
        public void f(Bundle bundle) {
            if (bundle.getBoolean("ack", false)) {
                c(null);
            } else {
                b(new Fi(4, "Invalid response to one way request"));
            }
        }

        @DexIgnore
        @Override // com.fossil.Kf4.Ei
        public boolean g() {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Ot3<T> b; // = new Ot3<>();
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ Bundle d;

        @DexIgnore
        public Ei(int i, int i2, Bundle bundle) {
            this.a = i;
            this.c = i2;
            this.d = bundle;
        }

        @DexIgnore
        public Message a(Context context, Messenger messenger) {
            Message obtain = Message.obtain();
            obtain.what = this.c;
            obtain.arg1 = this.a;
            obtain.replyTo = messenger;
            Bundle bundle = new Bundle();
            bundle.putBoolean("oneWay", g());
            bundle.putString("pkg", context.getPackageName());
            bundle.putBundle("data", this.d);
            obtain.setData(bundle);
            return obtain;
        }

        @DexIgnore
        public void b(Fi fi) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(fi);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14 + String.valueOf(valueOf2).length());
                sb.append("Failing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.b(fi);
        }

        @DexIgnore
        public void c(T t) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(this);
                String valueOf2 = String.valueOf(t);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 16 + String.valueOf(valueOf2).length());
                sb.append("Finishing ");
                sb.append(valueOf);
                sb.append(" with ");
                sb.append(valueOf2);
                Log.d("MessengerIpcClient", sb.toString());
            }
            this.b.c(t);
        }

        @DexIgnore
        public Nt3<T> d() {
            return this.b.a();
        }

        @DexIgnore
        public void e(Bundle bundle) {
            if (bundle.getBoolean("unsupported", false)) {
                b(new Fi(4, "Not supported by GmsCore"));
            } else {
                f(bundle);
            }
        }

        @DexIgnore
        public abstract void f(Bundle bundle);

        @DexIgnore
        public abstract boolean g();

        @DexIgnore
        public String toString() {
            int i = this.c;
            int i2 = this.a;
            boolean g = g();
            StringBuilder sb = new StringBuilder(55);
            sb.append("Request { what=");
            sb.append(i);
            sb.append(" id=");
            sb.append(i2);
            sb.append(" oneWay=");
            sb.append(g);
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi extends Exception {
        @DexIgnore
        public /* final */ int errorCode;

        @DexIgnore
        public Fi(int i, String str) {
            super(str);
            this.errorCode = i;
        }

        @DexIgnore
        public int getErrorCode() {
            return this.errorCode;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi extends Ei<Bundle> {
        @DexIgnore
        public Gi(int i, int i2, Bundle bundle) {
            super(i, i2, bundle);
        }

        @DexIgnore
        @Override // com.fossil.Kf4.Ei
        public void f(Bundle bundle) {
            Bundle bundle2 = bundle.getBundle("data");
            if (bundle2 == null) {
                bundle2 = Bundle.EMPTY;
            }
            c(bundle2);
        }

        @DexIgnore
        @Override // com.fossil.Kf4.Ei
        public boolean g() {
            return false;
        }
    }

    @DexIgnore
    public Kf4(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static Kf4 c(Context context) {
        Kf4 kf4;
        synchronized (Kf4.class) {
            try {
                if (e == null) {
                    e = new Kf4(context, Zl2.a().a(1, new Sf2("MessengerIpcClient"), Em2.a));
                }
                kf4 = e;
            } catch (Throwable th) {
                throw th;
            }
        }
        return kf4;
    }

    @DexIgnore
    public final int d() {
        int i;
        synchronized (this) {
            i = this.d;
            this.d = i + 1;
        }
        return i;
    }

    @DexIgnore
    public Nt3<Void> e(int i, Bundle bundle) {
        return f(new Di(d(), i, bundle));
    }

    @DexIgnore
    public final <T> Nt3<T> f(Ei<T> ei) {
        Nt3<T> d2;
        synchronized (this) {
            if (Log.isLoggable("MessengerIpcClient", 3)) {
                String valueOf = String.valueOf(ei);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
                sb.append("Queueing ");
                sb.append(valueOf);
                Log.d("MessengerIpcClient", sb.toString());
            }
            if (!this.c.a(ei)) {
                Bi bi = new Bi();
                this.c = bi;
                bi.a(ei);
            }
            d2 = ei.d();
        }
        return d2;
    }

    @DexIgnore
    public Nt3<Bundle> g(int i, Bundle bundle) {
        return f(new Gi(d(), i, bundle));
    }
}
