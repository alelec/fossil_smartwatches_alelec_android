package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public Fo3(Un3 un3, AtomicReference atomicReference) {
        this.c = un3;
        this.b = atomicReference;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            try {
                this.b.set(this.c.m().L(this.c.q().C()));
                this.b.notify();
            } catch (Throwable th) {
                this.b.notify();
                throw th;
            }
        }
    }
}
