package com.fossil;

import com.fossil.Bv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fv2 implements I13 {
    @DexIgnore
    public static /* final */ I13 a; // = new Fv2();

    @DexIgnore
    @Override // com.fossil.I13
    public final boolean zza(int i) {
        return Bv2.Bi.zza(i) != null;
    }
}
