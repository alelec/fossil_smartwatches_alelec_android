package com.fossil;

import android.graphics.drawable.Drawable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z41 {
    @DexIgnore
    public /* final */ Dv7 a;
    @DexIgnore
    public /* final */ N81 b;
    @DexIgnore
    public /* final */ D81 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Drawable f;
    @DexIgnore
    public /* final */ Drawable g;
    @DexIgnore
    public /* final */ Drawable h;

    @DexIgnore
    public Z41() {
        this(null, null, null, false, false, null, null, null, 255, null);
    }

    @DexIgnore
    public Z41(Dv7 dv7, N81 n81, D81 d81, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3) {
        Wg6.c(dv7, "dispatcher");
        Wg6.c(d81, "precision");
        this.a = dv7;
        this.b = n81;
        this.c = d81;
        this.d = z;
        this.e = z2;
        this.f = drawable;
        this.g = drawable2;
        this.h = drawable3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Z41(Dv7 dv7, N81 n81, D81 d81, boolean z, boolean z2, Drawable drawable, Drawable drawable2, Drawable drawable3, int i, Qg6 qg6) {
        this((i & 1) != 0 ? Bw7.b() : dv7, (i & 2) != 0 ? null : n81, (i & 4) != 0 ? D81.AUTOMATIC : d81, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? null : drawable, (i & 64) != 0 ? null : drawable2, (i & 128) == 0 ? drawable3 : null);
    }

    @DexIgnore
    public final boolean a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final Dv7 c() {
        return this.a;
    }

    @DexIgnore
    public final Drawable d() {
        return this.g;
    }

    @DexIgnore
    public final Drawable e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Z41) {
                Z41 z41 = (Z41) obj;
                if (!Wg6.a(this.a, z41.a) || !Wg6.a(this.b, z41.b) || !Wg6.a(this.c, z41.c) || this.d != z41.d || this.e != z41.e || !Wg6.a(this.f, z41.f) || !Wg6.a(this.g, z41.g) || !Wg6.a(this.h, z41.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Drawable f() {
        return this.f;
    }

    @DexIgnore
    public final D81 g() {
        return this.c;
    }

    @DexIgnore
    public final N81 h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        Dv7 dv7 = this.a;
        int hashCode = dv7 != null ? dv7.hashCode() : 0;
        N81 n81 = this.b;
        int hashCode2 = n81 != null ? n81.hashCode() : 0;
        D81 d81 = this.c;
        int hashCode3 = d81 != null ? d81.hashCode() : 0;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        boolean z2 = this.e;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        Drawable drawable = this.f;
        int hashCode4 = drawable != null ? drawable.hashCode() : 0;
        Drawable drawable2 = this.g;
        int hashCode5 = drawable2 != null ? drawable2.hashCode() : 0;
        Drawable drawable3 = this.h;
        if (drawable3 != null) {
            i2 = drawable3.hashCode();
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + i) * 31) + hashCode4) * 31) + hashCode5) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "DefaultRequestOptions(dispatcher=" + this.a + ", transition=" + this.b + ", precision=" + this.c + ", allowHardware=" + this.d + ", allowRgb565=" + this.e + ", placeholder=" + this.f + ", error=" + this.g + ", fallback=" + this.h + ")";
    }
}
