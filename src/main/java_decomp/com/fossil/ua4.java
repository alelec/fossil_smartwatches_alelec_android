package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ua4<E> implements List<E>, RandomAccess {
    @DexIgnore
    public /* final */ List<E> b;

    @DexIgnore
    public Ua4(List<E> list) {
        this.b = Collections.unmodifiableList(list);
    }

    @DexIgnore
    public static <E> Ua4<E> a(List<E> list) {
        return new Ua4<>(list);
    }

    @DexIgnore
    public static <E> Ua4<E> b(E... eArr) {
        return new Ua4<>(Arrays.asList(eArr));
    }

    @DexIgnore
    @Override // java.util.List
    public void add(int i, E e) {
        this.b.add(i, e);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean add(E e) {
        return this.b.add(e);
    }

    @DexIgnore
    @Override // java.util.List
    public boolean addAll(int i, Collection<? extends E> collection) {
        return this.b.addAll(i, collection);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean addAll(Collection<? extends E> collection) {
        return this.b.addAll(collection);
    }

    @DexIgnore
    public void clear() {
        this.b.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.b.contains(obj);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean containsAll(Collection<?> collection) {
        return this.b.containsAll(collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this.b.equals(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return this.b.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public int indexOf(Object obj) {
        return this.b.indexOf(obj);
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.b.isEmpty();
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return this.b.iterator();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        return this.b.lastIndexOf(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator() {
        return this.b.listIterator();
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator(int i) {
        return this.b.listIterator(i);
    }

    @DexIgnore
    @Override // java.util.List
    public E remove(int i) {
        return this.b.remove(i);
    }

    @DexIgnore
    @Override // java.util.List
    public boolean remove(Object obj) {
        return this.b.remove(obj);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean removeAll(Collection<?> collection) {
        return this.b.removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean retainAll(Collection<?> collection) {
        return this.b.retainAll(collection);
    }

    @DexIgnore
    @Override // java.util.List
    public E set(int i, E e) {
        return this.b.set(i, e);
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    @Override // java.util.List
    public List<E> subList(int i, int i2) {
        return this.b.subList(i, i2);
    }

    @DexIgnore
    public Object[] toArray() {
        return this.b.toArray();
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) this.b.toArray(tArr);
    }
}
