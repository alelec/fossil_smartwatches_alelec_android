package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xp0 {
    @DexIgnore
    public List<Xp0> a() {
        return Collections.emptyList();
    }

    @DexIgnore
    public abstract ViewDataBinding b(Zp0 zp0, View view, int i);

    @DexIgnore
    public abstract ViewDataBinding c(Zp0 zp0, View[] viewArr, int i);
}
