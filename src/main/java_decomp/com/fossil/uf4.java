package com.fossil;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Uf4 implements Ft3 {
    @DexIgnore
    public /* final */ Vf4 a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public Uf4(Vf4 vf4, Pair pair) {
        this.a = vf4;
        this.b = pair;
    }

    @DexIgnore
    @Override // com.fossil.Ft3
    public final Object then(Nt3 nt3) {
        this.a.b(this.b, nt3);
        return nt3;
    }
}
