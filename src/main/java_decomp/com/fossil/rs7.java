package com.fossil;

import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rs7<T> implements Ts7<T> {
    @DexIgnore
    public /* final */ Gg6<T> a;
    @DexIgnore
    public /* final */ Hg6<T, T> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterator<T>, Jr7 {
        @DexIgnore
        public T b;
        @DexIgnore
        public int c; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ Rs7 d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Rs7 rs7) {
            this.d = rs7;
        }

        @DexIgnore
        public final void a() {
            T t;
            if (this.c == -2) {
                t = (T) this.d.a.invoke();
            } else {
                Hg6 hg6 = this.d.b;
                T t2 = this.b;
                if (t2 != null) {
                    t = (T) hg6.invoke(t2);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.b = t;
            this.c = t == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c < 0) {
                a();
            }
            return this.c == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.c < 0) {
                a();
            }
            if (this.c != 0) {
                T t = this.b;
                if (t != null) {
                    this.c = -1;
                    return t;
                }
                throw new Rc6("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Gg6<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Hg6<? super T, ? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Rs7(Gg6<? extends T> gg6, Hg6<? super T, ? extends T> hg6) {
        Wg6.c(gg6, "getInitialValue");
        Wg6.c(hg6, "getNextValue");
        this.a = gg6;
        this.b = hg6;
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<T> iterator() {
        return new Ai(this);
    }
}
