package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P22 implements Factory<L22> {
    @DexIgnore
    public static /* final */ P22 a; // = new P22();

    @DexIgnore
    public static P22 a() {
        return a;
    }

    @DexIgnore
    public static L22 c() {
        L22 c = M22.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    public L22 b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
