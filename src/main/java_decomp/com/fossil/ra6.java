package com.fossil;

import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ra6 extends Fq4 {
    @DexIgnore
    public abstract void n(String str, String str2, AutocompleteSessionToken autocompleteSessionToken);

    @DexIgnore
    public abstract WeatherWatchAppSetting o();

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q(int i, boolean z);
}
