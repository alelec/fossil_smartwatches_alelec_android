package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import com.fossil.common.cipher.TextEncryption;
import com.mapped.Wg6;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C90 {
    @DexIgnore
    public static /* final */ String a; // = E.a("UUID.randomUUID().toString()");
    @DexIgnore
    public static /* final */ Hashtable<String, Zk1> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
    @DexIgnore
    public static E90 d; // = new E90(Id0.i.g(), Id0.i.h(), Id0.i.l(), Id0.i.k(), "5.13.5-production-release", null, 32);
    @DexIgnore
    public static /* final */ C90 e; // = new C90();

    /*
    static {
        String string;
        SharedPreferences b2 = G80.b(Ld0.c);
        if (b2 != null && (string = b2.getString("log.device.mapping", null)) != null) {
            Wg6.b(string, "getSharedPreferences(Sha\u2026_MAP_KEY, null) ?: return");
            String n = TextEncryption.j.n(string);
            if (n != null) {
                try {
                    JSONObject jSONObject = new JSONObject(n);
                    b.clear();
                    b.putAll(e.c(jSONObject));
                } catch (Exception e2) {
                    D90.i.i(e2);
                }
            }
        }
    }
    */

    @DexIgnore
    public final E90 a() {
        return d;
    }

    @DexIgnore
    public final Zk1 b(String str) {
        return b.get(str);
    }

    @DexIgnore
    public final HashMap<String, Zk1> c(JSONObject jSONObject) {
        JSONObject optJSONObject;
        Zk1 b2;
        HashMap<String, Zk1> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        Wg6.b(keys, "jsonObject.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            if (!(!BluetoothAdapter.checkBluetoothAddress(next) || (optJSONObject = jSONObject.optJSONObject(next)) == null || (b2 = Zk1.u.b(optJSONObject)) == null)) {
                Wg6.b(next, "key");
                hashMap.put(next, b2);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void d(String str, Zk1 zk1) {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putString;
        Zk1 zk12 = b.get(str);
        Hashtable<String, Zk1> hashtable = b;
        if (zk12 != null) {
            zk1 = Zk1.u.a(zk12, zk1);
        }
        hashtable.put(str, zk1);
        Hashtable<String, Zk1> hashtable2 = b;
        JSONObject jSONObject = new JSONObject();
        for (T t : hashtable2.keySet()) {
            Zk1 zk13 = hashtable2.get(t);
            if (zk13 != null) {
                jSONObject.put(t, zk13.toJSONObject());
            }
        }
        String jSONObject2 = jSONObject.toString();
        Wg6.b(jSONObject2, "convertDeviceInfoMapToJS\u2026formationMaps).toString()");
        String o = TextEncryption.j.o(jSONObject2);
        SharedPreferences b2 = G80.b(Ld0.c);
        if (!(b2 == null || (edit = b2.edit()) == null || (putString = edit.putString("log.device.mapping", o)) == null)) {
            putString.apply();
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        c.put(str, str2);
    }

    @DexIgnore
    public final String f(String str) {
        String str2 = c.get(str);
        return str2 != null ? str2 : a;
    }
}
