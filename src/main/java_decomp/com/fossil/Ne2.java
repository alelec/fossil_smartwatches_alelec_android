package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ne2 extends IInterface {
    @DexIgnore
    boolean y2(Kg2 kg2, Rg2 rg2) throws RemoteException;
}
