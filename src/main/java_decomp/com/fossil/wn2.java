package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wn2 extends Tn2 implements Vn2 {
    @DexIgnore
    public Wn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    @Override // com.fossil.Vn2
    public final void V(Dj2 dj2) throws RemoteException {
        Parcel d = d();
        Qo2.b(d, dj2);
        i(1, d);
    }
}
