package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import com.facebook.LegacyTokenHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Rl0;
import java.io.IOException;
import java.util.ArrayList;
import net.sqlcipher.database.SQLiteDatabase;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wz0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements TypeEvaluator<Rl0.Bi[]> {
        @DexIgnore
        public Rl0.Bi[] a;

        @DexIgnore
        public Rl0.Bi[] a(float f, Rl0.Bi[] biArr, Rl0.Bi[] biArr2) {
            if (Rl0.b(biArr, biArr2)) {
                if (!Rl0.b(this.a, biArr)) {
                    this.a = Rl0.f(biArr);
                }
                for (int i = 0; i < biArr.length; i++) {
                    this.a[i].d(biArr[i], biArr2[i], f);
                }
                return this.a;
            }
            throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [float, java.lang.Object, java.lang.Object] */
        @Override // android.animation.TypeEvaluator
        public /* bridge */ /* synthetic */ Rl0.Bi[] evaluate(float f, Rl0.Bi[] biArr, Rl0.Bi[] biArr2) {
            return a(f, biArr, biArr2);
        }
    }

    @DexIgnore
    public static Animator a(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, float f) throws XmlPullParserException, IOException {
        return b(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser), null, 0, f);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: android.animation.AnimatorSet */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.animation.Animator b(android.content.Context r13, android.content.res.Resources r14, android.content.res.Resources.Theme r15, org.xmlpull.v1.XmlPullParser r16, android.util.AttributeSet r17, android.animation.AnimatorSet r18, int r19, float r20) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
        // Method dump skipped, instructions count: 253
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wz0.b(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.animation.AnimatorSet, int, float):android.animation.Animator");
    }

    @DexIgnore
    public static Keyframe c(Keyframe keyframe, float f) {
        return keyframe.getType() == Float.TYPE ? Keyframe.ofFloat(f) : keyframe.getType() == Integer.TYPE ? Keyframe.ofInt(f) : Keyframe.ofObject(f);
    }

    @DexIgnore
    public static void d(Keyframe[] keyframeArr, float f, int i, int i2) {
        float f2 = f / ((float) ((i2 - i) + 2));
        while (i <= i2) {
            keyframeArr[i].setFraction(keyframeArr[i - 1].getFraction() + f2);
            i++;
        }
    }

    @DexIgnore
    public static PropertyValuesHolder e(TypedArray typedArray, int i, int i2, int i3, String str) {
        PropertyValuesHolder propertyValuesHolder;
        TypedValue peekValue = typedArray.peekValue(i2);
        boolean z = peekValue != null;
        int i4 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i3);
        boolean z2 = peekValue2 != null;
        int i5 = z2 ? peekValue2.type : 0;
        if (i == 4) {
            i = ((!z || !h(i4)) && (!z2 || !h(i5))) ? 0 : 3;
        }
        boolean z3 = i == 0;
        if (i == 2) {
            String string = typedArray.getString(i2);
            String string2 = typedArray.getString(i3);
            Rl0.Bi[] d = Rl0.d(string);
            Rl0.Bi[] d2 = Rl0.d(string2);
            if (!(d == null && d2 == null)) {
                if (d != null) {
                    Ai ai = new Ai();
                    if (d2 == null) {
                        return PropertyValuesHolder.ofObject(str, ai, d);
                    } else if (Rl0.b(d, d2)) {
                        return PropertyValuesHolder.ofObject(str, ai, d, d2);
                    } else {
                        throw new InflateException(" Can't morph from " + string + " to " + string2);
                    }
                } else if (d2 != null) {
                    return PropertyValuesHolder.ofObject(str, new Ai(), d2);
                }
            }
            return null;
        }
        Xz0 a2 = i == 3 ? Xz0.a() : null;
        if (z3) {
            if (z) {
                float dimension = i4 == 5 ? typedArray.getDimension(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : typedArray.getFloat(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                if (z2) {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, dimension, i5 == 5 ? typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : typedArray.getFloat(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                } else {
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, dimension);
                }
            } else {
                propertyValuesHolder = PropertyValuesHolder.ofFloat(str, i5 == 5 ? typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : typedArray.getFloat(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            }
        } else if (z) {
            int dimension2 = i4 == 5 ? (int) typedArray.getDimension(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : h(i4) ? typedArray.getColor(i2, 0) : typedArray.getInt(i2, 0);
            if (z2) {
                propertyValuesHolder = PropertyValuesHolder.ofInt(str, dimension2, i5 == 5 ? (int) typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : h(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0));
            } else {
                propertyValuesHolder = PropertyValuesHolder.ofInt(str, dimension2);
            }
        } else if (z2) {
            propertyValuesHolder = PropertyValuesHolder.ofInt(str, i5 == 5 ? (int) typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) : h(i5) ? typedArray.getColor(i3, 0) : typedArray.getInt(i3, 0));
        } else {
            propertyValuesHolder = null;
        }
        if (propertyValuesHolder == null || a2 == null) {
            return propertyValuesHolder;
        }
        propertyValuesHolder.setEvaluator(a2);
        return propertyValuesHolder;
    }

    @DexIgnore
    public static int f(TypedArray typedArray, int i, int i2) {
        TypedValue peekValue = typedArray.peekValue(i);
        boolean z = peekValue != null;
        int i3 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i2);
        boolean z2 = peekValue2 != null;
        return ((!z || !h(i3)) && (!z2 || !h(z2 ? peekValue2.type : 0))) ? 0 : 3;
    }

    @DexIgnore
    public static int g(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        int i = 0;
        TypedArray k = Ol0.k(resources, theme, attributeSet, Sz0.j);
        TypedValue l = Ol0.l(k, xmlPullParser, "value", 0);
        if ((l != null) && h(l.type)) {
            i = 3;
        }
        k.recycle();
        return i;
    }

    @DexIgnore
    public static boolean h(int i) {
        return i >= 28 && i <= 31;
    }

    @DexIgnore
    public static Animator i(Context context, int i) throws Resources.NotFoundException {
        return Build.VERSION.SDK_INT >= 24 ? AnimatorInflater.loadAnimator(context, i) : j(context, context.getResources(), context.getTheme(), i);
    }

    @DexIgnore
    public static Animator j(Context context, Resources resources, Resources.Theme theme, int i) throws Resources.NotFoundException {
        return k(context, resources, theme, i, 1.0f);
    }

    @DexIgnore
    public static Animator k(Context context, Resources resources, Resources.Theme theme, int i, float f) throws Resources.NotFoundException {
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = resources.getAnimation(i);
            Animator a2 = a(context, resources, theme, xmlResourceParser, f);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            return a2;
        } catch (XmlPullParserException e) {
            Resources.NotFoundException notFoundException = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException.initCause(e);
            throw notFoundException;
        } catch (IOException e2) {
            Resources.NotFoundException notFoundException2 = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException2.initCause(e2);
            throw notFoundException2;
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public static ValueAnimator l(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, ValueAnimator valueAnimator, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        TypedArray k = Ol0.k(resources, theme, attributeSet, Sz0.g);
        TypedArray k2 = Ol0.k(resources, theme, attributeSet, Sz0.k);
        if (valueAnimator == null) {
            valueAnimator = new ValueAnimator();
        }
        q(valueAnimator, k, k2, f, xmlPullParser);
        int h = Ol0.h(k, xmlPullParser, "interpolator", 0, 0);
        if (h > 0) {
            valueAnimator.setInterpolator(Vz0.b(context, h));
        }
        k.recycle();
        if (k2 != null) {
            k2.recycle();
        }
        return valueAnimator;
    }

    @DexIgnore
    public static Keyframe m(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, int i, XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        TypedArray k = Ol0.k(resources, theme, attributeSet, Sz0.j);
        float f = Ol0.f(k, xmlPullParser, "fraction", 3, -1.0f);
        TypedValue l = Ol0.l(k, xmlPullParser, "value", 0);
        boolean z = l != null;
        if (i == 4) {
            i = (!z || !h(l.type)) ? 0 : 3;
        }
        Keyframe ofInt = z ? i != 0 ? (i == 1 || i == 3) ? Keyframe.ofInt(f, Ol0.g(k, xmlPullParser, "value", 0, 0)) : null : Keyframe.ofFloat(f, Ol0.f(k, xmlPullParser, "value", 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) : i == 0 ? Keyframe.ofFloat(f) : Keyframe.ofInt(f);
        int h = Ol0.h(k, xmlPullParser, "interpolator", 1, 0);
        if (h > 0) {
            ofInt.setInterpolator(Vz0.b(context, h));
        }
        k.recycle();
        return ofInt;
    }

    @DexIgnore
    public static ObjectAnimator n(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        ObjectAnimator objectAnimator = new ObjectAnimator();
        l(context, resources, theme, attributeSet, objectAnimator, f, xmlPullParser);
        return objectAnimator;
    }

    @DexIgnore
    public static PropertyValuesHolder o(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, String str, int i) throws XmlPullParserException, IOException {
        int size;
        int i2;
        ArrayList arrayList = null;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 3 || next == 1) {
                if (arrayList != null || (size = arrayList.size()) <= 0) {
                    return null;
                }
                Keyframe keyframe = (Keyframe) arrayList.get(0);
                Keyframe keyframe2 = (Keyframe) arrayList.get(size - 1);
                float fraction = keyframe2.getFraction();
                if (fraction >= 1.0f) {
                    i2 = size;
                } else if (fraction < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    keyframe2.setFraction(1.0f);
                    i2 = size;
                } else {
                    arrayList.add(arrayList.size(), c(keyframe2, 1.0f));
                    i2 = size + 1;
                }
                float fraction2 = keyframe.getFraction();
                if (fraction2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    if (fraction2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        keyframe.setFraction(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else {
                        arrayList.add(0, c(keyframe, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                        i2++;
                    }
                }
                Keyframe[] keyframeArr = new Keyframe[i2];
                arrayList.toArray(keyframeArr);
                for (int i3 = 0; i3 < i2; i3++) {
                    Keyframe keyframe3 = keyframeArr[i3];
                    if (keyframe3.getFraction() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (i3 == 0) {
                            keyframe3.setFraction(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            int i4 = i2 - 1;
                            if (i3 == i4) {
                                keyframe3.setFraction(1.0f);
                            } else {
                                int i5 = i3 + 1;
                                int i6 = i3;
                                while (i5 < i4 && keyframeArr[i5].getFraction() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                    i6 = i5;
                                    i5++;
                                }
                                d(keyframeArr, keyframeArr[i6 + 1].getFraction() - keyframeArr[i3 - 1].getFraction(), i3, i6);
                            }
                        }
                    }
                }
                PropertyValuesHolder ofKeyframe = PropertyValuesHolder.ofKeyframe(str, keyframeArr);
                if (i != 3) {
                    return ofKeyframe;
                }
                ofKeyframe.setEvaluator(Xz0.a());
                return ofKeyframe;
            } else if (xmlPullParser.getName().equals("keyframe")) {
                int g = i == 4 ? g(resources, theme, Xml.asAttributeSet(xmlPullParser), xmlPullParser) : i;
                Keyframe m = m(context, resources, theme, Xml.asAttributeSet(xmlPullParser), g, xmlPullParser);
                if (m != null) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(m);
                }
                xmlPullParser.next();
                i = g;
                arrayList = arrayList;
            }
        }
        if (arrayList != null) {
        }
        return null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.animation.PropertyValuesHolder[] p(android.content.Context r9, android.content.res.Resources r10, android.content.res.Resources.Theme r11, org.xmlpull.v1.XmlPullParser r12, android.util.AttributeSet r13) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            r7 = 0
            r6 = 0
        L_0x0002:
            int r1 = r12.getEventType()
            r0 = 0
            r2 = 3
            if (r1 == r2) goto L_0x005a
            r2 = 1
            if (r1 == r2) goto L_0x005a
            r0 = 2
            if (r1 == r0) goto L_0x0014
            r12.next()
            goto L_0x0002
        L_0x0014:
            java.lang.String r0 = r12.getName()
            java.lang.String r1 = "propertyValuesHolder"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0075
            int[] r0 = com.fossil.Sz0.i
            android.content.res.TypedArray r8 = com.fossil.Ol0.k(r10, r11, r13, r0)
            java.lang.String r0 = "propertyName"
            r1 = 3
            java.lang.String r4 = com.fossil.Ol0.i(r8, r12, r0, r1)
            java.lang.String r0 = "valueType"
            r1 = 2
            r2 = 4
            int r5 = com.fossil.Ol0.g(r8, r12, r0, r1, r2)
            r0 = r9
            r1 = r10
            r2 = r11
            r3 = r12
            android.animation.PropertyValuesHolder r0 = o(r0, r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x0045
            r0 = 0
            r1 = 1
            android.animation.PropertyValuesHolder r0 = e(r8, r5, r0, r1, r4)
        L_0x0045:
            if (r0 == 0) goto L_0x0051
            if (r6 != 0) goto L_0x004e
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
        L_0x004e:
            r6.add(r0)
        L_0x0051:
            r0 = r6
            r8.recycle()
        L_0x0055:
            r12.next()
            r6 = r0
            goto L_0x0002
        L_0x005a:
            if (r6 == 0) goto L_0x0073
            int r3 = r6.size()
            android.animation.PropertyValuesHolder[] r1 = new android.animation.PropertyValuesHolder[r3]
            r2 = r0
        L_0x0063:
            if (r2 >= r3) goto L_0x0071
            java.lang.Object r0 = r6.get(r2)
            android.animation.PropertyValuesHolder r0 = (android.animation.PropertyValuesHolder) r0
            r1[r2] = r0
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0063
        L_0x0071:
            r0 = r1
        L_0x0072:
            return r0
        L_0x0073:
            r0 = r7
            goto L_0x0072
        L_0x0075:
            r0 = r6
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wz0.p(android.content.Context, android.content.res.Resources, android.content.res.Resources$Theme, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet):android.animation.PropertyValuesHolder[]");
    }

    @DexIgnore
    public static void q(ValueAnimator valueAnimator, TypedArray typedArray, TypedArray typedArray2, float f, XmlPullParser xmlPullParser) {
        long g = (long) Ol0.g(typedArray, xmlPullParser, "duration", 1, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
        long g2 = (long) Ol0.g(typedArray, xmlPullParser, "startOffset", 2, 0);
        int g3 = Ol0.g(typedArray, xmlPullParser, LegacyTokenHelper.JSON_VALUE_TYPE, 7, 4);
        if (Ol0.j(xmlPullParser, "valueFrom") && Ol0.j(xmlPullParser, "valueTo")) {
            if (g3 == 4) {
                g3 = f(typedArray, 5, 6);
            }
            PropertyValuesHolder e = e(typedArray, g3, 5, 6, "");
            if (e != null) {
                valueAnimator.setValues(e);
            }
        }
        valueAnimator.setDuration(g);
        valueAnimator.setStartDelay(g2);
        valueAnimator.setRepeatCount(Ol0.g(typedArray, xmlPullParser, "repeatCount", 3, 0));
        valueAnimator.setRepeatMode(Ol0.g(typedArray, xmlPullParser, "repeatMode", 4, 1));
        if (typedArray2 != null) {
            r(valueAnimator, typedArray2, g3, f, xmlPullParser);
        }
    }

    @DexIgnore
    public static void r(ValueAnimator valueAnimator, TypedArray typedArray, int i, float f, XmlPullParser xmlPullParser) {
        ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator;
        String i2 = Ol0.i(typedArray, xmlPullParser, "pathData", 1);
        if (i2 != null) {
            String i3 = Ol0.i(typedArray, xmlPullParser, "propertyXName", 2);
            String i4 = Ol0.i(typedArray, xmlPullParser, "propertyYName", 3);
            if (i != 2) {
            }
            if (i3 == null && i4 == null) {
                throw new InflateException(typedArray.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
            }
            s(Rl0.e(i2), objectAnimator, 0.5f * f, i3, i4);
            return;
        }
        objectAnimator.setPropertyName(Ol0.i(typedArray, xmlPullParser, "propertyName", 0));
    }

    @DexIgnore
    public static void s(Path path, ObjectAnimator objectAnimator, float f, String str, String str2) {
        PropertyValuesHolder propertyValuesHolder;
        int i;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        do {
            f2 += pathMeasure.getLength();
            arrayList.add(Float.valueOf(f2));
        } while (pathMeasure.nextContour());
        PathMeasure pathMeasure2 = new PathMeasure(path, false);
        int min = Math.min(100, ((int) (f2 / f)) + 1);
        float[] fArr = new float[min];
        float[] fArr2 = new float[min];
        float[] fArr3 = new float[2];
        float f3 = f2 / ((float) (min - 1));
        int i2 = 0;
        int i3 = 0;
        float f4 = 0.0f;
        while (true) {
            propertyValuesHolder = null;
            if (i3 >= min) {
                break;
            }
            pathMeasure2.getPosTan(f4 - ((Float) arrayList.get(i2)).floatValue(), fArr3, null);
            fArr[i3] = fArr3[0];
            fArr2[i3] = fArr3[1];
            f4 += f3;
            int i4 = i2 + 1;
            if (i4 >= arrayList.size() || f4 <= ((Float) arrayList.get(i4)).floatValue()) {
                i = i2;
            } else {
                pathMeasure2.nextContour();
                i = i4;
            }
            i2 = i;
            i3++;
        }
        PropertyValuesHolder ofFloat = str != null ? PropertyValuesHolder.ofFloat(str, fArr) : null;
        if (str2 != null) {
            propertyValuesHolder = PropertyValuesHolder.ofFloat(str2, fArr2);
        }
        if (ofFloat == null) {
            objectAnimator.setValues(propertyValuesHolder);
        } else if (propertyValuesHolder == null) {
            objectAnimator.setValues(ofFloat);
        } else {
            objectAnimator.setValues(ofFloat, propertyValuesHolder);
        }
    }
}
