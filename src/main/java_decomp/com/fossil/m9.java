package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M9 extends Wx1<El1[]> {
    @DexIgnore
    public M9(byte b) {
        super(b, (byte) 0, null, 6, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Wx1
    public El1[] d(byte[] bArr) {
        return V9.d.j(bArr);
    }
}
