package com.fossil;

import com.mapped.An4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R27 implements Factory<Q27> {
    @DexIgnore
    public /* final */ Provider<An4> a;

    @DexIgnore
    public R27(Provider<An4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static R27 a(Provider<An4> provider) {
        return new R27(provider);
    }

    @DexIgnore
    public static Q27 c(An4 an4) {
        return new Q27(an4);
    }

    @DexIgnore
    public Q27 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
