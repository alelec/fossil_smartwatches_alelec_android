package com.fossil;

import android.util.Log;
import com.fossil.Af1;
import com.fossil.Sc1;
import com.fossil.Wb1;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Md1 implements Sc1, Sc1.Ai {
    @DexIgnore
    public /* final */ Tc1<?> b;
    @DexIgnore
    public /* final */ Sc1.Ai c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Pc1 e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public volatile Af1.Ai<?> g;
    @DexIgnore
    public Qc1 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Wb1.Ai<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Af1.Ai b;

        @DexIgnore
        public Ai(Af1.Ai ai) {
            this.b = ai;
        }

        @DexIgnore
        @Override // com.fossil.Wb1.Ai
        public void b(Exception exc) {
            if (Md1.this.g(this.b)) {
                Md1.this.i(this.b, exc);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1.Ai
        public void e(Object obj) {
            if (Md1.this.g(this.b)) {
                Md1.this.h(this.b, obj);
            }
        }
    }

    @DexIgnore
    public Md1(Tc1<?> tc1, Sc1.Ai ai) {
        this.b = tc1;
        this.c = ai;
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void a(Mb1 mb1, Exception exc, Wb1<?> wb1, Gb1 gb1) {
        this.c.a(mb1, exc, wb1, this.g.c.c());
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void b() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.Sc1
    public boolean c() {
        Object obj = this.f;
        if (obj != null) {
            this.f = null;
            d(obj);
        }
        Pc1 pc1 = this.e;
        if (pc1 != null && pc1.c()) {
            return true;
        }
        this.e = null;
        this.g = null;
        boolean z = false;
        while (!z && f()) {
            List<Af1.Ai<?>> g2 = this.b.g();
            int i = this.d;
            this.d = i + 1;
            this.g = g2.get(i);
            if (this.g != null && (this.b.e().c(this.g.c.c()) || this.b.t(this.g.c.getDataClass()))) {
                j(this.g);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Sc1
    public void cancel() {
        Af1.Ai<?> ai = this.g;
        if (ai != null) {
            ai.c.cancel();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void d(Object obj) {
        long b2 = Ek1.b();
        try {
            Jb1<X> p = this.b.p(obj);
            Rc1 rc1 = new Rc1(p, obj, this.b.k());
            this.h = new Qc1(this.g.a, this.b.o());
            this.b.d().a(this.h, rc1);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.h + ", data: " + obj + ", encoder: " + p + ", duration: " + Ek1.a(b2));
            }
            this.g.c.a();
            this.e = new Pc1(Collections.singletonList(this.g.a), this.b, this);
        } catch (Throwable th) {
            this.g.c.a();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void e(Mb1 mb1, Object obj, Wb1<?> wb1, Gb1 gb1, Mb1 mb12) {
        this.c.e(mb1, obj, wb1, this.g.c.c(), mb1);
    }

    @DexIgnore
    public final boolean f() {
        return this.d < this.b.g().size();
    }

    @DexIgnore
    public boolean g(Af1.Ai<?> ai) {
        Af1.Ai<?> ai2 = this.g;
        return ai2 != null && ai2 == ai;
    }

    @DexIgnore
    public void h(Af1.Ai<?> ai, Object obj) {
        Wc1 e2 = this.b.e();
        if (obj == null || !e2.c(ai.c.c())) {
            Sc1.Ai ai2 = this.c;
            Mb1 mb1 = ai.a;
            Wb1<Data> wb1 = ai.c;
            ai2.e(mb1, obj, wb1, wb1.c(), this.h);
            return;
        }
        this.f = obj;
        this.c.b();
    }

    @DexIgnore
    public void i(Af1.Ai<?> ai, Exception exc) {
        Sc1.Ai ai2 = this.c;
        Qc1 qc1 = this.h;
        Wb1<Data> wb1 = ai.c;
        ai2.a(qc1, exc, wb1, wb1.c());
    }

    @DexIgnore
    public final void j(Af1.Ai<?> ai) {
        this.g.c.d(this.b.l(), new Ai(ai));
    }
}
