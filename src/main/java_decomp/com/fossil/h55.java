package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;

    @DexIgnore
    public H55(Object obj, View view, int i, FlexibleButton flexibleButton, ImageView imageView, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = constraintLayout;
    }
}
