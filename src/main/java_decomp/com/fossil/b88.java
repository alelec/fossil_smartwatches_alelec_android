package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface B88<R, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public static Type b(int i, ParameterizedType parameterizedType) {
            return U88.h(i, parameterizedType);
        }

        @DexIgnore
        public static Class<?> c(Type type) {
            return U88.i(type);
        }

        @DexIgnore
        public abstract B88<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3);
    }

    @DexIgnore
    Type a();

    @DexIgnore
    T b(Call<R> call);
}
