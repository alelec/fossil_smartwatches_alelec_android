package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class s05 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppSettingRepository.Anon8 b;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppSetting c;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppSettingDataSource.PushMicroAppSettingToServerCallback d;

    @DexIgnore
    public /* synthetic */ s05(MicroAppSettingRepository.Anon8 anon8, MicroAppSetting microAppSetting, MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
        this.b = anon8;
        this.c = microAppSetting;
        this.d = pushMicroAppSettingToServerCallback;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d);
    }
}
