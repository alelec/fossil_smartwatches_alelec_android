package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class X90 extends Enum<X90> {
    @DexIgnore
    public static /* final */ X90 c;
    @DexIgnore
    public static /* final */ X90 d;
    @DexIgnore
    public static /* final */ /* synthetic */ X90[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        X90 x90 = new X90("CONFIGURATION_FILE", 0, (byte) 0);
        c = x90;
        X90 x902 = new X90("ELEMENT_CONFIGURATION_FILE", 1, (byte) 1);
        X90 x903 = new X90("LAUNCH_FILE", 2, (byte) 2);
        X90 x904 = new X90("LAUNCH_ELEMENT_FILE", 3, (byte) 3);
        X90 x905 = new X90("DECLARATION_FILE", 4, (byte) 4);
        X90 x906 = new X90("DECLARATION_ELEMENT_FILE", 5, (byte) 5);
        X90 x907 = new X90("CUSTOMIZATION_FILE", 6, (byte) 6);
        X90 x908 = new X90("CUSTOMIZATION_ELEMENT_FAME", 7, (byte) 7);
        X90 x909 = new X90("REMOTE_ACTIVITY", 8, (byte) 8);
        d = x909;
        e = new X90[]{x90, x902, x903, x904, x905, x906, x907, x908, x909};
    }
    */

    @DexIgnore
    public X90(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static X90 valueOf(String str) {
        return (X90) Enum.valueOf(X90.class, str);
    }

    @DexIgnore
    public static X90[] values() {
        return (X90[]) e.clone();
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
