package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.fossil.Cg0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ng0 extends Cg0 implements SubMenu {
    @DexIgnore
    public Cg0 B;
    @DexIgnore
    public Eg0 C;

    @DexIgnore
    public Ng0(Context context, Cg0 cg0, Eg0 eg0) {
        super(context);
        this.B = cg0;
        this.C = eg0;
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public Cg0 F() {
        return this.B.F();
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean H() {
        return this.B.H();
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean I() {
        return this.B.I();
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean J() {
        return this.B.J();
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public void V(Cg0.Ai ai) {
        this.B.V(ai);
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean f(Eg0 eg0) {
        return this.B.f(eg0);
    }

    @DexIgnore
    public MenuItem getItem() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean h(Cg0 cg0, MenuItem menuItem) {
        return super.h(cg0, menuItem) || this.B.h(cg0, menuItem);
    }

    @DexIgnore
    public Menu i0() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public boolean m(Eg0 eg0) {
        return this.B.m(eg0);
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public void setGroupDividerEnabled(boolean z) {
        this.B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(int i) {
        super.Y(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.Z(drawable);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(int i) {
        super.b0(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.c0(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        super.d0(view);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(int i) {
        this.C.setIcon(i);
        return this;
    }

    @DexIgnore
    @Override // android.view.SubMenu
    public SubMenu setIcon(Drawable drawable) {
        this.C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public void setQwertyMode(boolean z) {
        this.B.setQwertyMode(z);
    }

    @DexIgnore
    @Override // com.fossil.Cg0
    public String v() {
        Eg0 eg0 = this.C;
        int itemId = eg0 != null ? eg0.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.v() + ":" + itemId;
    }
}
