package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uk2 extends Pk2 implements Sk2 {
    @DexIgnore
    public Uk2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @DexIgnore
    @Override // com.fossil.Sk2
    public final String getId() throws RemoteException {
        Parcel e = e(1, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.Sk2
    public final boolean x0(boolean z) throws RemoteException {
        Parcel d = d();
        Rk2.a(d, true);
        Parcel e = e(2, d);
        boolean b = Rk2.b(e);
        e.recycle();
        return b;
    }
}
