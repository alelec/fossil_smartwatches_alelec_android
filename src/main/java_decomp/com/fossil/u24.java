package com.fossil;

import com.fossil.Y24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U24<E> extends AbstractCollection<E> implements Serializable {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<E> extends Bi<E> {
        @DexIgnore
        public Object[] a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public Ai(int i) {
            A24.b(i, "initialCapacity");
            this.a = new Object[i];
        }

        @DexIgnore
        @Override // com.fossil.U24.Bi
        @CanIgnoreReturnValue
        public Bi<E> b(E... eArr) {
            H44.c(eArr);
            f(this.b + eArr.length);
            System.arraycopy(eArr, 0, this.a, this.b, eArr.length);
            this.b += eArr.length;
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> e(E e) {
            I14.l(e);
            f(this.b + 1);
            Object[] objArr = this.a;
            int i = this.b;
            this.b = i + 1;
            objArr[i] = e;
            return this;
        }

        @DexIgnore
        public final void f(int i) {
            Object[] objArr = this.a;
            if (objArr.length < i) {
                this.a = H44.a(objArr, Bi.d(objArr.length, i));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<E> {
        @DexIgnore
        public static int d(int i, int i2) {
            if (i2 >= 0) {
                int i3 = (i >> 1) + i + 1;
                if (i3 < i2) {
                    i3 = Integer.highestOneBit(i2 - 1) << 1;
                }
                if (i3 < 0) {
                    return Integer.MAX_VALUE;
                }
                return i3;
            }
            throw new AssertionError("cannot store more than MAX_VALUE elements");
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public abstract Bi<E> a(E e);

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<E> b(E... eArr) {
            for (E e : eArr) {
                a(e);
            }
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.U24$Bi<E> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public Bi<E> c(Iterator<? extends E> it) {
            while (it.hasNext()) {
                a(it.next());
            }
            return this;
        }
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean add(E e) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Y24<E> asList() {
        int size = size();
        return size != 0 ? size != 1 ? new M44(this, toArray()) : Y24.of((Object) iterator().next()) : Y24.of();
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean contains(Object obj);

    @DexIgnore
    @CanIgnoreReturnValue
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = iterator();
        while (it.hasNext()) {
            objArr[i] = it.next();
            i++;
        }
        return i;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract H54<E> iterator();

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final Object[] toArray() {
        int size = size();
        if (size == 0) {
            return H44.a;
        }
        Object[] objArr = new Object[size];
        copyIntoArray(objArr, 0);
        return objArr;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public final <T> T[] toArray(T[] tArr) {
        I14.l(tArr);
        int size = size();
        if (tArr.length < size) {
            tArr = (T[]) H44.f(tArr, size);
        } else if (tArr.length > size) {
            tArr[size] = null;
        }
        copyIntoArray(tArr, 0);
        return tArr;
    }

    @DexIgnore
    public Object writeReplace() {
        return new Y24.Di(toArray());
    }
}
