package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class De6 {
    @DexIgnore
    public /* final */ Xd6 a;
    @DexIgnore
    public /* final */ Oe6 b;
    @DexIgnore
    public /* final */ Ie6 c;

    @DexIgnore
    public De6(Xd6 xd6, Oe6 oe6, Ie6 ie6) {
        Wg6.c(xd6, "mActiveTimeOverviewDayView");
        Wg6.c(oe6, "mActiveTimeOverviewWeekView");
        Wg6.c(ie6, "mActiveTimeOverviewMonthView");
        this.a = xd6;
        this.b = oe6;
        this.c = ie6;
    }

    @DexIgnore
    public final Xd6 a() {
        return this.a;
    }

    @DexIgnore
    public final Ie6 b() {
        return this.c;
    }

    @DexIgnore
    public final Oe6 c() {
        return this.b;
    }
}
