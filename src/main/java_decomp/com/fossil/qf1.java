package com.fossil;

import com.fossil.Af1;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qf1 implements Af1<URL, InputStream> {
    @DexIgnore
    public /* final */ Af1<Te1, InputStream> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Bf1<URL, InputStream> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<URL, InputStream> b(Ef1 ef1) {
            return new Qf1(ef1.d(Te1.class, InputStream.class));
        }
    }

    @DexIgnore
    public Qf1(Af1<Te1, InputStream> af1) {
        this.a = af1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(URL url) {
        return d(url);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(URL url, int i, int i2, Ob1 ob1) {
        return c(url, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(URL url, int i, int i2, Ob1 ob1) {
        return this.a.b(new Te1(url), i, i2, ob1);
    }

    @DexIgnore
    public boolean d(URL url) {
        return true;
    }
}
