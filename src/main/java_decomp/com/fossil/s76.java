package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface S76 extends Gq4<R76> {
    @DexIgnore
    void H(PlacesClient placesClient);

    @DexIgnore
    void Q4(String str);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    void c2(String str, String str2);

    @DexIgnore
    void h0(List<String> list);

    @DexIgnore
    void k2(String str);

    @DexIgnore
    void p6(boolean z);

    @DexIgnore
    void s4(CommuteTimeSetting commuteTimeSetting);

    @DexIgnore
    void w2(String str);

    @DexIgnore
    void x0(boolean z);
}
