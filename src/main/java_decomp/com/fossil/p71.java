package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.O71;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class P71 implements O71 {
    @DexIgnore
    public static /* final */ IntentFilter e; // = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ ConnectivityManager d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ P71 a;
        @DexIgnore
        public /* final */ /* synthetic */ O71.Bi b;

        @DexIgnore
        public Ai(P71 p71, O71.Bi bi) {
            this.a = p71;
            this.b = bi;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            if (Wg6.a(intent != null ? intent.getAction() : null, "android.net.conn.CONNECTIVITY_CHANGE")) {
                this.b.a(this.a.a());
            }
        }
    }

    @DexIgnore
    public P71(Context context, ConnectivityManager connectivityManager, O71.Bi bi) {
        Wg6.c(context, "context");
        Wg6.c(connectivityManager, "connectivityManager");
        Wg6.c(bi, "listener");
        this.c = context;
        this.d = connectivityManager;
        this.b = new Ai(this, bi);
    }

    @DexIgnore
    @Override // com.fossil.O71
    public boolean a() {
        NetworkInfo activeNetworkInfo = this.d.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    @DexIgnore
    @Override // com.fossil.O71
    public void start() {
        this.c.registerReceiver(this.b, e);
    }

    @DexIgnore
    @Override // com.fossil.O71
    public void stop() {
        this.c.unregisterReceiver(this.b);
    }
}
