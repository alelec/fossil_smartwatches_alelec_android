package com.fossil;

import java.io.IOException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface R43 {
    @DexIgnore
    @Deprecated
    void a(int i, List<?> list, F33 f33) throws IOException;

    @DexIgnore
    void b(int i, Object obj, F33 f33) throws IOException;

    @DexIgnore
    <K, V> void c(int i, H23<K, V> h23, Map<K, V> map) throws IOException;

    @DexIgnore
    @Deprecated
    void d(int i, Object obj, F33 f33) throws IOException;

    @DexIgnore
    void e(int i, List<?> list, F33 f33) throws IOException;

    @DexIgnore
    void f(int i, Xz2 xz2) throws IOException;

    @DexIgnore
    int zza();

    @DexIgnore
    @Deprecated
    void zza(int i) throws IOException;

    @DexIgnore
    void zza(int i, double d) throws IOException;

    @DexIgnore
    void zza(int i, float f) throws IOException;

    @DexIgnore
    void zza(int i, int i2) throws IOException;

    @DexIgnore
    void zza(int i, long j) throws IOException;

    @DexIgnore
    void zza(int i, Object obj) throws IOException;

    @DexIgnore
    void zza(int i, String str) throws IOException;

    @DexIgnore
    void zza(int i, List<String> list) throws IOException;

    @DexIgnore
    void zza(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zza(int i, boolean z) throws IOException;

    @DexIgnore
    @Deprecated
    void zzb(int i) throws IOException;

    @DexIgnore
    void zzb(int i, int i2) throws IOException;

    @DexIgnore
    void zzb(int i, long j) throws IOException;

    @DexIgnore
    void zzb(int i, List<Xz2> list) throws IOException;

    @DexIgnore
    void zzb(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzc(int i, int i2) throws IOException;

    @DexIgnore
    void zzc(int i, long j) throws IOException;

    @DexIgnore
    void zzc(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzd(int i, int i2) throws IOException;

    @DexIgnore
    void zzd(int i, long j) throws IOException;

    @DexIgnore
    void zzd(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zze(int i, int i2) throws IOException;

    @DexIgnore
    void zze(int i, long j) throws IOException;

    @DexIgnore
    void zze(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzf(int i, int i2) throws IOException;

    @DexIgnore
    void zzf(int i, List<Float> list, boolean z) throws IOException;

    @DexIgnore
    void zzg(int i, List<Double> list, boolean z) throws IOException;

    @DexIgnore
    void zzh(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzi(int i, List<Boolean> list, boolean z) throws IOException;

    @DexIgnore
    void zzj(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzk(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzl(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzm(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzn(int i, List<Long> list, boolean z) throws IOException;
}
