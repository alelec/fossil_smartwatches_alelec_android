package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P01 {
    @DexIgnore
    public static /* final */ P01 i; // = new Ai().a();
    @DexIgnore
    public Y01 a; // = Y01.NOT_REQUIRED;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f; // = -1;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public Q01 h; // = new Q01();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public Y01 c; // = Y01.NOT_REQUIRED;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public Q01 h; // = new Q01();

        @DexIgnore
        public P01 a() {
            return new P01(this);
        }

        @DexIgnore
        public Ai b(Y01 y01) {
            this.c = y01;
            return this;
        }
    }

    @DexIgnore
    public P01() {
    }

    @DexIgnore
    public P01(Ai ai) {
        this.b = ai.a;
        this.c = Build.VERSION.SDK_INT >= 23 && ai.b;
        this.a = ai.c;
        this.d = ai.d;
        this.e = ai.e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.h = ai.h;
            this.f = ai.f;
            this.g = ai.g;
        }
    }

    @DexIgnore
    public P01(P01 p01) {
        this.b = p01.b;
        this.c = p01.c;
        this.a = p01.a;
        this.d = p01.d;
        this.e = p01.e;
        this.h = p01.h;
    }

    @DexIgnore
    public Q01 a() {
        return this.h;
    }

    @DexIgnore
    public Y01 b() {
        return this.a;
    }

    @DexIgnore
    public long c() {
        return this.f;
    }

    @DexIgnore
    public long d() {
        return this.g;
    }

    @DexIgnore
    public boolean e() {
        return this.h.c() > 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || P01.class != obj.getClass()) {
            return false;
        }
        P01 p01 = (P01) obj;
        if (this.b == p01.b && this.c == p01.c && this.d == p01.d && this.e == p01.e && this.f == p01.f && this.g == p01.g && this.a == p01.a) {
            return this.h.equals(p01.h);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.b;
    }

    @DexIgnore
    public boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        boolean z = this.b;
        boolean z2 = this.c;
        boolean z3 = this.d;
        boolean z4 = this.e;
        long j = this.f;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.g;
        return (((((((((((((hashCode * 31) + (z ? 1 : 0)) * 31) + (z2 ? 1 : 0)) * 31) + (z3 ? 1 : 0)) * 31) + (z4 ? 1 : 0)) * 31) + i2) * 31) + ((int) ((j2 >>> 32) ^ j2))) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void j(Q01 q01) {
        this.h = q01;
    }

    @DexIgnore
    public void k(Y01 y01) {
        this.a = y01;
    }

    @DexIgnore
    public void l(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void m(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public void n(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void o(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void p(long j) {
        this.f = j;
    }

    @DexIgnore
    public void q(long j) {
        this.g = j;
    }
}
