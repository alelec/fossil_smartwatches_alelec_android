package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tn7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tn7$a$a")
        /* renamed from: com.fossil.tn7$a$a  reason: collision with other inner class name */
        public static final class C0243a extends qq7 implements vp7<tn7, b, tn7> {
            @DexIgnore
            public static /* final */ C0243a INSTANCE; // = new C0243a();

            @DexIgnore
            public C0243a() {
                super(2);
            }

            @DexIgnore
            public final tn7 invoke(tn7 tn7, b bVar) {
                pq7.c(tn7, "acc");
                pq7.c(bVar, "element");
                tn7 minusKey = tn7.minusKey(bVar.getKey());
                if (minusKey == un7.INSTANCE) {
                    return bVar;
                }
                rn7 rn7 = (rn7) minusKey.get(rn7.p);
                if (rn7 == null) {
                    return new pn7(minusKey, bVar);
                }
                tn7 minusKey2 = minusKey.minusKey(rn7.p);
                return minusKey2 == un7.INSTANCE ? new pn7(bVar, rn7) : new pn7(new pn7(minusKey2, bVar), rn7);
            }
        }

        @DexIgnore
        public static tn7 a(tn7 tn7, tn7 tn72) {
            pq7.c(tn72, "context");
            return tn72 == un7.INSTANCE ? tn7 : (tn7) tn72.fold(tn7, C0243a.INSTANCE);
        }
    }

    @DexIgnore
    public interface b extends tn7 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public static <R> R a(b bVar, R r, vp7<? super R, ? super b, ? extends R> vp7) {
                pq7.c(vp7, "operation");
                return (R) vp7.invoke(r, bVar);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.tn7$b */
            /* JADX WARN: Multi-variable type inference failed */
            public static <E extends b> E b(b bVar, c<E> cVar) {
                pq7.c(cVar, "key");
                if (!pq7.a(bVar.getKey(), cVar)) {
                    return null;
                }
                if (bVar != 0) {
                    return bVar;
                }
                throw new il7("null cannot be cast to non-null type E");
            }

            @DexIgnore
            public static tn7 c(b bVar, c<?> cVar) {
                pq7.c(cVar, "key");
                return pq7.a(bVar.getKey(), cVar) ? un7.INSTANCE : bVar;
            }

            @DexIgnore
            public static tn7 d(b bVar, tn7 tn7) {
                pq7.c(tn7, "context");
                return a.a(bVar, tn7);
            }
        }

        @DexIgnore
        @Override // com.fossil.tn7
        <E extends b> E get(c<E> cVar);

        @DexIgnore
        c<?> getKey();
    }

    @DexIgnore
    public interface c<E extends b> {
    }

    @DexIgnore
    <R> R fold(R r, vp7<? super R, ? super b, ? extends R> vp7);

    @DexIgnore
    <E extends b> E get(c<E> cVar);

    @DexIgnore
    tn7 minusKey(c<?> cVar);

    @DexIgnore
    tn7 plus(tn7 tn7);
}
