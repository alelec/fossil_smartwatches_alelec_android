package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.Nw0;
import com.mapped.Oh;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uw0<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ Oh k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ Mw0 n;
    @DexIgnore
    public /* final */ Nw0.Ci o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new Ai();
    @DexIgnore
    public /* final */ Runnable t; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (Uw0.this.r.compareAndSet(false, true)) {
                Uw0.this.k.getInvalidationTracker().b(Uw0.this.o);
            }
            do {
                if (Uw0.this.q.compareAndSet(false, true)) {
                    T t = null;
                    z = false;
                    while (Uw0.this.p.compareAndSet(true, false)) {
                        try {
                            try {
                                t = Uw0.this.m.call();
                                z = true;
                            } catch (Exception e) {
                                throw new RuntimeException("Exception while computing database live data.", e);
                            }
                        } finally {
                            Uw0.this.q.set(false);
                        }
                    }
                    if (z) {
                        Uw0.this.l(t);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (Uw0.this.p.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void run() {
            boolean g = Uw0.this.g();
            if (Uw0.this.p.compareAndSet(false, true) && g) {
                Uw0.this.q().execute(Uw0.this.s);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Nw0.Ci {
        @DexIgnore
        public Ci(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Bi0.f().b(Uw0.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Uw0(Oh oh, Mw0 mw0, boolean z, Callable<T> callable, String[] strArr) {
        this.k = oh;
        this.l = z;
        this.m = callable;
        this.n = mw0;
        this.o = new Ci(strArr);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void j() {
        super.j();
        this.n.b(this);
        q().execute(this.s);
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData
    public void k() {
        super.k();
        this.n.c(this);
    }

    @DexIgnore
    public Executor q() {
        return this.l ? this.k.getTransactionExecutor() : this.k.getQueryExecutor();
    }
}
