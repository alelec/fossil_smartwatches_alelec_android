package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Y36 extends Rv5<X36> {
    @DexIgnore
    void F(boolean z);

    @DexIgnore
    void N1(boolean z);

    @DexIgnore
    Object U();  // void declaration

    @DexIgnore
    Object W();  // void declaration

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    void j4(boolean z);

    @DexIgnore
    void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm);

    @DexIgnore
    void p0(List<Alarm> list);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    void u3(boolean z);

    @DexIgnore
    Object v0();  // void declaration
}
