package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.A21;
import com.fossil.G41;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z11 implements G21, K11, G41.Bi {
    @DexIgnore
    public static /* final */ String k; // = X01.f("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ A21 e;
    @DexIgnore
    public /* final */ H21 f;
    @DexIgnore
    public /* final */ Object g; // = new Object();
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public PowerManager.WakeLock i;
    @DexIgnore
    public boolean j; // = false;

    @DexIgnore
    public Z11(Context context, int i2, String str, A21 a21) {
        this.b = context;
        this.c = i2;
        this.e = a21;
        this.d = str;
        this.f = new H21(this.b, a21.f(), this);
    }

    @DexIgnore
    @Override // com.fossil.G41.Bi
    public void a(String str) {
        X01.c().a(k, String.format("Exceeded time limits on execution for %s", str), new Throwable[0]);
        g();
    }

    @DexIgnore
    @Override // com.fossil.G21
    public void b(List<String> list) {
        g();
    }

    @DexIgnore
    public final void c() {
        synchronized (this.g) {
            this.f.e();
            this.e.h().c(this.d);
            if (this.i != null && this.i.isHeld()) {
                X01.c().a(k, String.format("Releasing wakelock %s for WorkSpec %s", this.i, this.d), new Throwable[0]);
                this.i.release();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.K11
    public void d(String str, boolean z) {
        X01.c().a(k, String.format("onExecuted %s, %s", str, Boolean.valueOf(z)), new Throwable[0]);
        c();
        if (z) {
            Intent f2 = X11.f(this.b, this.d);
            A21 a21 = this.e;
            a21.k(new A21.Bi(a21, f2, this.c));
        }
        if (this.j) {
            Intent a2 = X11.a(this.b);
            A21 a212 = this.e;
            a212.k(new A21.Bi(a212, a2, this.c));
        }
    }

    @DexIgnore
    public void e() {
        this.i = D41.b(this.b, String.format("%s (%s)", this.d, Integer.valueOf(this.c)));
        X01.c().a(k, String.format("Acquiring wakelock %s for WorkSpec %s", this.i, this.d), new Throwable[0]);
        this.i.acquire();
        O31 o = this.e.g().p().j().o(this.d);
        if (o == null) {
            g();
            return;
        }
        boolean b2 = o.b();
        this.j = b2;
        if (!b2) {
            X01.c().a(k, String.format("No constraints for %s", this.d), new Throwable[0]);
            f(Collections.singletonList(this.d));
            return;
        }
        this.f.d(Collections.singletonList(o));
    }

    @DexIgnore
    @Override // com.fossil.G21
    public void f(List<String> list) {
        if (list.contains(this.d)) {
            synchronized (this.g) {
                if (this.h == 0) {
                    this.h = 1;
                    X01.c().a(k, String.format("onAllConstraintsMet for %s", this.d), new Throwable[0]);
                    if (this.e.e().i(this.d)) {
                        this.e.h().b(this.d, 600000, this);
                    } else {
                        c();
                    }
                } else {
                    X01.c().a(k, String.format("Already started work for %s", this.d), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        synchronized (this.g) {
            if (this.h < 2) {
                this.h = 2;
                X01.c().a(k, String.format("Stopping work for WorkSpec %s", this.d), new Throwable[0]);
                this.e.k(new A21.Bi(this.e, X11.g(this.b, this.d), this.c));
                if (this.e.e().f(this.d)) {
                    X01.c().a(k, String.format("WorkSpec %s needs to be rescheduled", this.d), new Throwable[0]);
                    this.e.k(new A21.Bi(this.e, X11.f(this.b, this.d), this.c));
                } else {
                    X01.c().a(k, String.format("Processor does not have WorkSpec %s. No need to reschedule ", this.d), new Throwable[0]);
                }
            } else {
                X01.c().a(k, String.format("Already stopped work for %s", this.d), new Throwable[0]);
            }
        }
    }
}
