package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zf1 implements Sb1<Bitmap> {
    @DexIgnore
    @Override // com.fossil.Sb1
    public final Id1<Bitmap> b(Context context, Id1<Bitmap> id1, int i, int i2) {
        if (Jk1.s(i, i2)) {
            Rd1 f = Oa1.c(context).f();
            Bitmap bitmap = id1.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap c = c(f, bitmap, i, i2);
            return bitmap.equals(c) ? id1 : Yf1.f(c, f);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }

    @DexIgnore
    public abstract Bitmap c(Rd1 rd1, Bitmap bitmap, int i, int i2);
}
