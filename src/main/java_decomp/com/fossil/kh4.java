package com.fossil;

import com.fossil.Hh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Kh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Kh4 a();

        @DexIgnore
        public abstract Ai b(Bi bi);

        @DexIgnore
        public abstract Ai c(String str);

        @DexIgnore
        public abstract Ai d(long j);
    }

    @DexIgnore
    public enum Bi {
        OK,
        BAD_CONFIG,
        AUTH_ERROR
    }

    @DexIgnore
    public static Ai a() {
        Hh4.Bi bi = new Hh4.Bi();
        bi.d(0);
        return bi;
    }

    @DexIgnore
    public abstract Bi b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract long d();
}
