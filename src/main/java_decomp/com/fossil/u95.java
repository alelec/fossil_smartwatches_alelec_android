package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U95 extends T95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        E.put(2131362666, 2);
        E.put(2131362627, 3);
        E.put(2131363170, 4);
        E.put(2131362070, 5);
        E.put(2131362389, 6);
        E.put(2131362474, 7);
        E.put(2131362394, 8);
        E.put(2131362507, 9);
        E.put(2131362981, 10);
        E.put(2131362428, 11);
    }
    */

    @DexIgnore
    public U95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public U95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[5], (TimerTextView) objArr[6], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[11], (TimerTextView) objArr[7], (FlexibleTextView) objArr[9], (ImageView) objArr[3], (RTLImageView) objArr[2], (RecyclerView) objArr[10], (SwipeRefreshLayout) objArr[4], (FlexibleTextView) objArr[1]);
        this.C = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.B = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
