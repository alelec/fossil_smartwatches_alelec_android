package com.fossil;

import android.os.Looper;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T94 {
    @DexIgnore
    public static /* final */ FilenameFilter a; // = new Ai();
    @DexIgnore
    public static /* final */ ExecutorService b; // = F94.c("awaitEvenIfOnMainThread task continuation executor");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Ft3<T, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Ot3 a;

        @DexIgnore
        public Bi(Ot3 ot3) {
            this.a = ot3;
        }

        @DexIgnore
        public Void a(Nt3<T> nt3) throws Exception {
            if (nt3.q()) {
                this.a.e(nt3.m());
                return null;
            }
            this.a.d(nt3.l());
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Ft3
        public /* bridge */ /* synthetic */ Void then(Nt3 nt3) throws Exception {
            return a(nt3);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Callable b;
        @DexIgnore
        public /* final */ /* synthetic */ Ot3 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Ft3<T, Void> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public Void a(Nt3<T> nt3) throws Exception {
                if (nt3.q()) {
                    Ci.this.c.c(nt3.m());
                    return null;
                }
                Ci.this.c.b(nt3.l());
                return null;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Ft3
            public /* bridge */ /* synthetic */ Void then(Nt3 nt3) throws Exception {
                return a(nt3);
            }
        }

        @DexIgnore
        public Ci(Callable callable, Ot3 ot3) {
            this.b = callable;
            this.c = ot3;
        }

        @DexIgnore
        public void run() {
            try {
                ((Nt3) this.b.call()).h(new Aii());
            } catch (Exception e) {
                this.c.b(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Ft3<T, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ CountDownLatch a;

        @DexIgnore
        public Di(CountDownLatch countDownLatch) {
            this.a = countDownLatch;
        }

        @DexIgnore
        @Override // com.fossil.Ft3
        public Object then(Nt3<T> nt3) throws Exception {
            this.a.countDown();
            return null;
        }
    }

    @DexIgnore
    public static <T> T a(Nt3<T> nt3) throws InterruptedException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        nt3.i(b, new Di(countDownLatch));
        if (Looper.getMainLooper() == Looper.myLooper()) {
            countDownLatch.await(4, TimeUnit.SECONDS);
        } else {
            countDownLatch.await();
        }
        if (nt3.p()) {
            return nt3.m();
        }
        throw new TimeoutException();
    }

    @DexIgnore
    public static <T> Nt3<T> b(Executor executor, Callable<Nt3<T>> callable) {
        Ot3 ot3 = new Ot3();
        executor.execute(new Ci(callable, ot3));
        return ot3.a();
    }

    @DexIgnore
    public static int c(File file, int i, Comparator<File> comparator) {
        return d(file, a, i, comparator);
    }

    @DexIgnore
    public static int d(File file, FilenameFilter filenameFilter, int i, Comparator<File> comparator) {
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles == null) {
            return 0;
        }
        return e(Arrays.asList(listFiles), i, comparator);
    }

    @DexIgnore
    public static int e(List<File> list, int i, Comparator<File> comparator) {
        int size = list.size();
        Collections.sort(list, comparator);
        for (File file : list) {
            if (size <= i) {
                break;
            }
            h(file);
            size--;
        }
        return size;
    }

    @DexIgnore
    public static int f(File file, File file2, int i, Comparator<File> comparator) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        File[] listFiles2 = file2.listFiles(a);
        if (listFiles == null) {
            listFiles = new File[0];
        }
        if (listFiles2 == null) {
            listFiles2 = new File[0];
        }
        arrayList.addAll(Arrays.asList(listFiles));
        arrayList.addAll(Arrays.asList(listFiles2));
        return e(arrayList, i, comparator);
    }

    @DexIgnore
    public static <T> Nt3<T> g(Nt3<T> nt3, Nt3<T> nt32) {
        Ot3 ot3 = new Ot3();
        Bi bi = new Bi(ot3);
        nt3.h(bi);
        nt32.h(bi);
        return ot3.a();
    }

    @DexIgnore
    public static void h(File file) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                h(file2);
            }
        }
        file.delete();
    }
}
