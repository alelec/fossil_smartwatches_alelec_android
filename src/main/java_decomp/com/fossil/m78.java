package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M78 extends L78 {
    @DexIgnore
    public static /* final */ M78 NOP_LOGGER; // = new M78();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -517220405410904473L;

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void debug(String str) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void debug(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void debug(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void debug(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void debug(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void error(String str) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void error(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void error(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void error(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void error(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public String getName() {
        return "NOP";
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void info(String str) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void info(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void info(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void info(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void info(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final boolean isDebugEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final boolean isErrorEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final boolean isInfoEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final boolean isTraceEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final boolean isWarnEnabled() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void trace(String str) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void trace(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void trace(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void trace(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void trace(String str, Object... objArr) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void warn(String str) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void warn(String str, Object obj) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void warn(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78, com.fossil.E78
    public final void warn(String str, Throwable th) {
    }

    @DexIgnore
    @Override // com.fossil.L78, com.fossil.O78
    public final void warn(String str, Object... objArr) {
    }
}
