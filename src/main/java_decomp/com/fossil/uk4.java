package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uk4 implements Tk4 {
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(Uk4.class.getName());
    @DexIgnore
    public /* final */ Map<String, Bl4> a;
    @DexIgnore
    public /* final */ Map<Integer, Bl4> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Rk4 d;

    @DexIgnore
    public Uk4(Rk4 rk4) {
        this("/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto", rk4);
    }

    @DexIgnore
    public Uk4(String str, Rk4 rk4) {
        this.a = Collections.synchronizedMap(new HashMap());
        this.b = Collections.synchronizedMap(new HashMap());
        this.c = str;
        this.d = rk4;
    }

    @DexIgnore
    public static Cl4 c(ObjectInputStream objectInputStream) {
        Cl4 cl4 = new Cl4();
        try {
            cl4.d(Sk4.a(objectInputStream, 16384));
            try {
                objectInputStream.close();
            } catch (IOException e2) {
                e.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e2);
            }
        } catch (IOException e3) {
            e.log(Level.WARNING, "error reading input (ignored)", (Throwable) e3);
            objectInputStream.close();
        } catch (Throwable th) {
            try {
                objectInputStream.close();
            } catch (IOException e4) {
                e.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e4);
            }
            throw th;
        }
        return cl4;
    }

    @DexIgnore
    @Override // com.fossil.Tk4
    public Bl4 a(int i) {
        synchronized (this.b) {
            if (!this.b.containsKey(Integer.valueOf(i))) {
                List<String> list = Qk4.a().get(Integer.valueOf(i));
                if (list.size() == 1 && "001".equals(list.get(0))) {
                    d("001", i);
                }
            }
        }
        return this.b.get(Integer.valueOf(i));
    }

    @DexIgnore
    @Override // com.fossil.Tk4
    public Bl4 b(String str) {
        synchronized (this.a) {
            if (!this.a.containsKey(str)) {
                d(str, 0);
            }
        }
        return this.a.get(str);
    }

    @DexIgnore
    public void d(String str, int i) {
        boolean equals = "001".equals(str);
        String valueOf = String.valueOf(String.valueOf(this.c));
        String valueOf2 = String.valueOf(String.valueOf(equals ? String.valueOf(i) : str));
        StringBuilder sb = new StringBuilder(valueOf.length() + 1 + valueOf2.length());
        sb.append(valueOf);
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        sb.append(valueOf2);
        String sb2 = sb.toString();
        InputStream a2 = this.d.a(sb2);
        if (a2 == null) {
            Logger logger = e;
            Level level = Level.SEVERE;
            String valueOf3 = String.valueOf(sb2);
            logger.log(level, valueOf3.length() != 0 ? "missing metadata: ".concat(valueOf3) : new String("missing metadata: "));
            String valueOf4 = String.valueOf(sb2);
            throw new IllegalStateException(valueOf4.length() != 0 ? "missing metadata: ".concat(valueOf4) : new String("missing metadata: "));
        }
        try {
            Bl4[] bl4Arr = c(new ObjectInputStream(a2)).a;
            if (bl4Arr.length == 0) {
                Logger logger2 = e;
                Level level2 = Level.SEVERE;
                String valueOf5 = String.valueOf(sb2);
                logger2.log(level2, valueOf5.length() != 0 ? "empty metadata: ".concat(valueOf5) : new String("empty metadata: "));
                String valueOf6 = String.valueOf(sb2);
                throw new IllegalStateException(valueOf6.length() != 0 ? "empty metadata: ".concat(valueOf6) : new String("empty metadata: "));
            }
            if (bl4Arr.length > 1) {
                Logger logger3 = e;
                Level level3 = Level.WARNING;
                String valueOf7 = String.valueOf(sb2);
                logger3.log(level3, valueOf7.length() != 0 ? "invalid metadata (too many entries): ".concat(valueOf7) : new String("invalid metadata (too many entries): "));
            }
            Bl4 bl4 = bl4Arr[0];
            if (equals) {
                this.b.put(Integer.valueOf(i), bl4);
            } else {
                this.a.put(str, bl4);
            }
        } catch (IOException e2) {
            Logger logger4 = e;
            Level level4 = Level.SEVERE;
            String valueOf8 = String.valueOf(sb2);
            logger4.log(level4, valueOf8.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf8) : new String("cannot load/parse metadata: "), (Throwable) e2);
            String valueOf9 = String.valueOf(sb2);
            throw new RuntimeException(valueOf9.length() != 0 ? "cannot load/parse metadata: ".concat(valueOf9) : new String("cannot load/parse metadata: "), e2);
        }
    }
}
