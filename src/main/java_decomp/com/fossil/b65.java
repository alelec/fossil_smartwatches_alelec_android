package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.WindowInsetsFrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class B65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CropImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ WindowInsetsFrameLayout t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public B65(Object obj, View view, int i, CropImageView cropImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, WindowInsetsFrameLayout windowInsetsFrameLayout, RecyclerView recyclerView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = cropImageView;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = windowInsetsFrameLayout;
        this.u = recyclerView;
        this.v = flexibleTextView;
        this.w = flexibleTextView2;
        this.x = flexibleTextView3;
    }
}
