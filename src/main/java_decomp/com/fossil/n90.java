package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum N90 {
    c("req_route"),
    d("req_distance");
    
    @DexIgnore
    public static /* final */ M90 f; // = new M90(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public N90(String str) {
        this.b = str;
    }
}
