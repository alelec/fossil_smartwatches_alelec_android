package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G77 {
    @DexIgnore
    public int a; // = -1;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ E77 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ge5 a;
        @DexIgnore
        public /* final */ E77 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ Fb7 c;

            @DexIgnore
            public Aii(Ai ai, Fb7 fb7, int i) {
                this.b = ai;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.a(this.c, this.b.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ge5 ge5, E77 e77) {
            super(ge5.n());
            Wg6.c(ge5, "binding");
            Wg6.c(e77, "templateListener");
            this.a = ge5;
            this.b = e77;
        }

        @DexIgnore
        public final void b(Fb7 fb7, int i) {
            Wg6.c(fb7, "wf");
            Ge5 ge5 = this.a;
            ge5.q.setOnClickListener(new Aii(this, fb7, i));
            ImageView imageView = ge5.q;
            Wg6.b(imageView, "ivBackgroundPreview");
            Ty4.a(imageView, fb7.f(), fb7.e());
            if (i == getAdapterPosition()) {
                View view = ge5.t;
                Wg6.b(view, "vBackgroundSelected");
                view.setVisibility(0);
                return;
            }
            View view2 = ge5.t;
            Wg6.b(view2, "vBackgroundSelected");
            view2.setVisibility(8);
        }
    }

    @DexIgnore
    public G77(int i, E77 e77) {
        Wg6.c(e77, "templateListener");
        this.b = i;
        this.c = e77;
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public boolean c(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof Fb7) && ((Fb7) obj).a() == L77.BACKGROUND_TEMPLATE;
    }

    @DexIgnore
    public void d(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Fb7)) {
            obj = null;
        }
        Fb7 fb7 = (Fb7) obj;
        if (fb7 != null) {
            if (viewHolder instanceof Ai) {
                ai = viewHolder;
            }
            Ai ai2 = ai;
            if (ai2 != null) {
                ai2.b(fb7, this.a);
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder e(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Ge5 z = Ge5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemDianaBackgroundBindi\u2026(inflater, parent, false)");
        return new Ai(z, this.c);
    }
}
