package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O95 extends N95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d Z; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray a0;
    @DexIgnore
    public long Y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        a0 = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        a0.put(2131362546, 2);
        a0.put(2131363156, 3);
        a0.put(2131362457, 4);
        a0.put(2131363162, 5);
        a0.put(2131362071, 6);
        a0.put(2131362460, 7);
        a0.put(2131362813, 8);
        a0.put(2131362461, 9);
        a0.put(2131363462, 10);
        a0.put(2131362458, 11);
        a0.put(2131362812, 12);
        a0.put(2131362459, 13);
        a0.put(2131363463, 14);
        a0.put(2131362511, 15);
        a0.put(2131362821, 16);
        a0.put(2131362512, 17);
        a0.put(2131363464, 18);
        a0.put(2131362509, 19);
        a0.put(2131362099, 20);
        a0.put(2131362467, 21);
        a0.put(2131363163, 22);
        a0.put(2131362074, 23);
        a0.put(2131362456, 24);
        a0.put(2131362533, 25);
        a0.put(2131363168, 26);
        a0.put(2131362110, 27);
        a0.put(2131362510, 28);
        a0.put(2131362373, 29);
        a0.put(2131363160, 30);
        a0.put(2131362042, 31);
        a0.put(2131362529, 32);
        a0.put(2131362285, 33);
    }
    */

    @DexIgnore
    public O95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 34, Z, a0));
    }

    @DexIgnore
    public O95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[31], (ConstraintLayout) objArr[6], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[27], (FlexibleButton) objArr[33], (FlexibleTextView) objArr[29], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[32], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (LinearLayout) objArr[12], (LinearLayout) objArr[8], (LinearLayout) objArr[16], (ConstraintLayout) objArr[0], (ScrollView) objArr[3], (FlexibleSwitchCompat) objArr[30], (FlexibleSwitchCompat) objArr[5], (FlexibleSwitchCompat) objArr[22], (FlexibleSwitchCompat) objArr[26], (View) objArr[10], (View) objArr[14], (View) objArr[18]);
        this.Y = -1;
        this.P.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.Y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.Y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.Y = 1;
        }
        w();
    }
}
