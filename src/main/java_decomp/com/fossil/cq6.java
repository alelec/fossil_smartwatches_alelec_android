package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cq6 implements Factory<DeleteAccountPresenter> {
    @DexIgnore
    public static DeleteAccountPresenter a(Yp6 yp6, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, UserRepository userRepository, GetZendeskInformation getZendeskInformation, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new DeleteAccountPresenter(yp6, deviceRepository, analyticsHelper, userRepository, getZendeskInformation, deleteLogoutUserUseCase);
    }
}
