package com.fossil;

import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nw {
    @DexIgnore
    public /* final */ Hashtable<Ow, LinkedHashSet<Lp>> a; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Hashtable<Ow, Integer> b;

    @DexIgnore
    public /* synthetic */ Nw(Hashtable hashtable, Qg6 qg6) {
        this.b = hashtable;
        Set<Ow> keySet = this.b.keySet();
        Wg6.b(keySet, "resourceQuotas.keys");
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            this.a.put(it.next(), new LinkedHashSet<>());
        }
    }

    @DexIgnore
    public final boolean a(Lp lp) {
        T t;
        synchronized (this.a) {
            synchronized (this.b) {
                M80.c.a("ResourcePool", "Before allocateResource for %s(%s), current resourceHolders=%s.", Ey1.a(lp.y), lp.z, this.a);
                Iterator<Ow> it = lp.z().iterator();
                while (it.hasNext()) {
                    Ow next = it.next();
                    LinkedHashSet<Lp> linkedHashSet = this.a.get(next);
                    LinkedHashSet<Lp> linkedHashSet2 = linkedHashSet != null ? linkedHashSet : new LinkedHashSet<>();
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    Wg6.b(num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator<T> it2 = linkedHashSet2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it2.next();
                        if (t.u(lp)) {
                            break;
                        }
                    }
                    if (t == null) {
                        if (linkedHashSet2.size() < intValue) {
                            linkedHashSet2.add(lp);
                            this.a.put(next, linkedHashSet2);
                        } else {
                            Set<Map.Entry<Ow, LinkedHashSet<Lp>>> entrySet = this.a.entrySet();
                            Wg6.b(entrySet, "resourceHolders.entries");
                            Iterator<T> it3 = entrySet.iterator();
                            while (it3.hasNext()) {
                                ((LinkedHashSet) it3.next().getValue()).remove(lp);
                            }
                            M80.c.a("ResourcePool", "After allocateResource for %s(%s), current resourceHolders=%s.", Ey1.a(lp.y), lp.z, this.a);
                            return false;
                        }
                    }
                }
                M80.c.a("ResourcePool", "After allocateResource for %s(%s), current resourceHolders=%s.", Ey1.a(lp.y), lp.z, this.a);
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(Lp lp) {
        synchronized (this.a) {
            synchronized (this.b) {
                M80.c.a("ResourcePool", "Before releaseResource for %s(%s), current resourceHolders=%s.", Ey1.a(lp.y), lp.z, this.a);
                Iterator<Ow> it = lp.z().iterator();
                while (it.hasNext()) {
                    LinkedHashSet<Lp> linkedHashSet = this.a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(lp);
                    }
                }
                M80.c.a("ResourcePool", "After releaseResource for %s(%s), current resourceHolders=%s.", Ey1.a(lp.y), lp.z, this.a);
                Cd6 cd6 = Cd6.a;
            }
            Cd6 cd62 = Cd6.a;
        }
    }
}
