package com.fossil;

import com.fossil.Tq4.Ai;
import com.fossil.Tq4.Bi;
import com.fossil.Tq4.Ci;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tq4<Q extends Bi, R extends Ci, E extends Ai> {
    @DexIgnore
    public static /* final */ String c; // = "tq4";
    @DexIgnore
    public Q a;
    @DexIgnore
    public Di<R, E> b;

    @DexIgnore
    public interface Ai {
    }

    @DexIgnore
    public interface Bi {
    }

    @DexIgnore
    public interface Ci {
    }

    @DexIgnore
    public interface Di<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore
    public abstract void a(Q q);

    @DexIgnore
    public Di<R, E> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        String simpleName = getClass().getSimpleName();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "Inside .run useCase=" + simpleName);
        Thread.currentThread().setName(simpleName);
        a(this.a);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local2.d(str2, "Inside .done useCase=" + simpleName);
    }

    @DexIgnore
    public void d(Q q) {
        this.a = q;
    }

    @DexIgnore
    public void e(Di<R, E> di) {
        this.b = di;
    }
}
