package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A54<E> extends H34<E> {
    @DexIgnore
    @LazyInit
    public transient int c;
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public A54(E e) {
        I14.l(e);
        this.element = e;
    }

    @DexIgnore
    public A54(E e, int i) {
        this.element = e;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        return this.element.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.U24
    public int copyIntoArray(Object[] objArr, int i) {
        objArr[i] = this.element;
        return i + 1;
    }

    @DexIgnore
    @Override // com.fossil.H34
    public Y24<E> createAsList() {
        return Y24.of((Object) this.element);
    }

    @DexIgnore
    @Override // com.fossil.H34
    public final int hashCode() {
        int i = this.c;
        if (i != 0) {
            return i;
        }
        int hashCode = this.element.hashCode();
        this.c = hashCode;
        return hashCode;
    }

    @DexIgnore
    @Override // com.fossil.H34
    public boolean isHashCodeFast() {
        return this.c != 0;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
    public H54<E> iterator() {
        return P34.u(this.element);
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }
}
