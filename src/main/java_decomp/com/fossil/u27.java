package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u27 extends iq4<b, c, a> {
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ SleepSummariesRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3508a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ qh5 d;

        @DexIgnore
        public b(int i, int i2, int i3, qh5 qh5) {
            pq7.c(qh5, "gender");
            this.f3508a = i;
            this.b = i2;
            this.c = i3;
            this.d = qh5;
        }

        @DexIgnore
        public final qh5 a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.f3508a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.GetRecommendedGoalUseCase", f = "GetRecommendedGoalUseCase.kt", l = {30, 32, 59, 60, 64, 65}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ u27 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(u27 u27, qn7 qn7) {
            super(qn7);
            this.this$0 = u27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public u27(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        pq7.c(summariesRepository, "mSummaryRepository");
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        this.d = summariesRepository;
        this.e = sleepSummariesRepository;
        this.f = sleepSessionsRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GetRecommendedGoalUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x02e8  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x031b  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.u27.b r18, com.fossil.qn7<java.lang.Object> r19) {
        /*
        // Method dump skipped, instructions count: 842
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u27.k(com.fossil.u27$b, com.fossil.qn7):java.lang.Object");
    }
}
