package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ai7 {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean a; // = (!Ai7.class.desiredAssertionStatus());

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        Ci7 ci7 = new Ci7(i3, new byte[((i2 * 3) / 4)]);
        if (ci7.a(bArr, i, i2, true)) {
            int i4 = ci7.b;
            byte[] bArr2 = ci7.a;
            if (i4 == bArr2.length) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
        throw new IllegalArgumentException("bad base-64");
    }

    @DexIgnore
    public static byte[] c(byte[] bArr, int i) {
        return d(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] d(byte[] bArr, int i, int i2, int i3) {
        int i4 = 2;
        Di7 di7 = new Di7(i3, null);
        int i5 = (i2 / 3) * 4;
        if (!di7.f) {
            int i6 = i2 % 3;
            if (i6 == 1) {
                i5 += 2;
            } else if (i6 == 2) {
                i5 += 3;
            }
        } else if (i2 % 3 > 0) {
            i5 += 4;
        }
        if (di7.g && i2 > 0) {
            int i7 = (i2 - 1) / 57;
            if (!di7.h) {
                i4 = 1;
            }
            i5 += i4 * (i7 + 1);
        }
        di7.a = new byte[i5];
        di7.a(bArr, i, i2, true);
        if (a || di7.b == i5) {
            return di7.a;
        }
        throw new AssertionError();
    }
}
