package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J76 implements MembersInjector<WatchAppEditPresenter> {
    @DexIgnore
    public static void a(WatchAppEditPresenter watchAppEditPresenter) {
        watchAppEditPresenter.D();
    }
}
