package com.fossil;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E57 {
    @DexIgnore
    public static /* final */ float w; // = ((float) (Math.log(0.75d) / Math.log(0.9d)));
    @DexIgnore
    public static /* final */ float[] x; // = new float[101];
    @DexIgnore
    public static /* final */ float y; // = 8.0f;
    @DexIgnore
    public static float z;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public Interpolator r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public /* final */ float v;

    /*
    static {
        float f2;
        float f3;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i2 = 0;
        while (i2 <= 100) {
            float f5 = ((float) i2) / 100.0f;
            float f6 = 1.0f;
            float f7 = f4;
            while (true) {
                float f8 = ((f6 - f7) / 2.0f) + f7;
                float f9 = 1.0f - f8;
                f2 = 3.0f * f8 * f9;
                f3 = f8 * f8 * f8;
                float f10 = (((f9 * 0.4f) + (0.6f * f8)) * f2) + f3;
                if (((double) Math.abs(f10 - f5)) < 1.0E-5d) {
                    break;
                } else if (f10 > f5) {
                    f6 = f8;
                } else {
                    f7 = f8;
                }
            }
            x[i2] = f2 + f3;
            i2++;
            f4 = f7;
        }
        x[100] = 1.0f;
        z = 1.0f;
        z = 1.0f / l(1.0f);
    }
    */

    @DexIgnore
    public E57(Context context, Interpolator interpolator) {
        this(context, interpolator, true);
    }

    @DexIgnore
    public E57(Context context, Interpolator interpolator, boolean z2) {
        this.q = true;
        this.r = interpolator;
        this.v = context.getResources().getDisplayMetrics().density * 160.0f;
        this.u = a(ViewConfiguration.getScrollFriction());
        this.s = z2;
    }

    @DexIgnore
    public static float l(float f2) {
        float f3 = y * f2;
        return (f3 < 1.0f ? f3 - (1.0f - ((float) Math.exp((double) (-f3)))) : ((1.0f - ((float) Math.exp((double) (1.0f - f3)))) * 0.63212055f) + 0.36787945f) * z;
    }

    @DexIgnore
    public final float a(float f2) {
        return this.v * 386.0878f * f2;
    }

    @DexIgnore
    public boolean b() {
        if (this.q) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
        int i2 = this.m;
        if (currentAnimationTimeMillis < i2) {
            int i3 = this.a;
            if (i3 == 0) {
                float f2 = ((float) currentAnimationTimeMillis) * this.n;
                Interpolator interpolator = this.r;
                float l2 = interpolator == null ? l(f2) : interpolator.getInterpolation(f2);
                this.j = this.b + Math.round(this.o * l2);
                this.k = Math.round(l2 * this.p) + this.c;
            } else if (i3 == 1) {
                float f3 = ((float) currentAnimationTimeMillis) / ((float) i2);
                int i4 = (int) (f3 * 100.0f);
                float f4 = ((float) i4) / 100.0f;
                int i5 = i4 + 1;
                float[] fArr = x;
                float f5 = fArr[i4];
                float f6 = (((f3 - f4) / ((((float) i5) / 100.0f) - f4)) * (fArr[i5] - f5)) + f5;
                int i6 = this.b;
                int round = i6 + Math.round(((float) (this.d - i6)) * f6);
                this.j = round;
                int min = Math.min(round, this.g);
                this.j = min;
                this.j = Math.max(min, this.f);
                int i7 = this.c;
                int round2 = Math.round(f6 * ((float) (this.e - i7))) + i7;
                this.k = round2;
                int min2 = Math.min(round2, this.i);
                this.k = min2;
                int max = Math.max(min2, this.h);
                this.k = max;
                if (this.j == this.d && max == this.e) {
                    this.q = true;
                }
            }
        } else {
            this.j = this.d;
            this.k = this.e;
            this.q = true;
        }
        return true;
    }

    @DexIgnore
    public void c(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        if (this.s && !this.q) {
            float e2 = e();
            float f2 = (float) (this.d - this.b);
            float f3 = (float) (this.e - this.c);
            float sqrt = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
            float f4 = (f2 / sqrt) * e2;
            float f5 = e2 * (f3 / sqrt);
            float f6 = (float) i4;
            if (Math.signum(f6) == Math.signum(f4)) {
                float f7 = (float) i5;
                if (Math.signum(f7) == Math.signum(f5)) {
                    i4 = (int) (f4 + f6);
                    i5 = (int) (f5 + f7);
                }
            }
        }
        this.a = 1;
        this.q = false;
        float sqrt2 = (float) Math.sqrt((double) ((i4 * i4) + (i5 * i5)));
        this.t = sqrt2;
        double log = Math.log((double) ((0.4f * sqrt2) / 800.0f));
        this.m = (int) (Math.exp(log / (((double) w) - 1.0d)) * 1000.0d);
        this.l = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        float f8 = 1.0f;
        int i10 = (sqrt2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (sqrt2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
        float f9 = i10 == 0 ? 1.0f : ((float) i4) / sqrt2;
        if (i10 != 0) {
            f8 = ((float) i5) / sqrt2;
        }
        float f10 = w;
        this.f = i6;
        this.g = i7;
        this.h = i8;
        this.i = i9;
        float exp = (float) ((int) (Math.exp((((double) f10) / (((double) f10) - 1.0d)) * log) * 800.0d));
        int round = Math.round(f9 * exp) + i2;
        this.d = round;
        int min = Math.min(round, this.g);
        this.d = min;
        this.d = Math.max(min, this.f);
        int round2 = Math.round(f8 * exp) + i3;
        this.e = round2;
        int min2 = Math.min(round2, this.i);
        this.e = min2;
        this.e = Math.max(min2, this.h);
    }

    @DexIgnore
    public final void d(boolean z2) {
        this.q = z2;
    }

    @DexIgnore
    public final float e() {
        return this.t - ((this.u * ((float) k())) / 2000.0f);
    }

    @DexIgnore
    public final int f() {
        return this.k;
    }

    @DexIgnore
    public final int g() {
        return this.e;
    }

    @DexIgnore
    public final int h() {
        return this.c;
    }

    @DexIgnore
    public final boolean i() {
        return this.q;
    }

    @DexIgnore
    public void j(int i2, int i3, int i4, int i5, int i6) {
        this.a = 0;
        this.q = false;
        this.m = i6;
        this.l = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        this.d = i2 + i4;
        this.e = i3 + i5;
        this.o = (float) i4;
        this.p = (float) i5;
        this.n = 1.0f / ((float) this.m);
    }

    @DexIgnore
    public final int k() {
        return (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
    }
}
