package com.fossil;

import android.util.Log;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Th7 {
    @DexIgnore
    public String a; // = Endpoints.DEFAULT_NAME;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public int c; // = 2;

    @DexIgnore
    public Th7(String str) {
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(Th7.class.getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    @DexIgnore
    public final void b(Object obj) {
        if (j()) {
            c(obj);
        }
    }

    @DexIgnore
    public final void c(Object obj) {
        String str;
        if (this.c <= 3) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.d(this.a, str);
            Oi7 t = Fg7.t();
            if (t != null) {
                t.c(str);
            }
        }
    }

    @DexIgnore
    public final void d(Object obj) {
        if (j()) {
            f(obj);
        }
    }

    @DexIgnore
    public final void e(Throwable th) {
        if (j()) {
            g(th);
        }
    }

    @DexIgnore
    public final void f(Object obj) {
        String str;
        if (this.c <= 6) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.e(this.a, str);
            Oi7 t = Fg7.t();
            if (t != null) {
                t.d(str);
            }
        }
    }

    @DexIgnore
    public final void g(Throwable th) {
        if (this.c <= 6) {
            Log.e(this.a, "", th);
            Oi7 t = Fg7.t();
            if (t != null) {
                t.d(th);
            }
        }
    }

    @DexIgnore
    public final void h(Object obj) {
        if (j()) {
            i(obj);
        }
    }

    @DexIgnore
    public final void i(Object obj) {
        String str;
        if (this.c <= 4) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.i(this.a, str);
            Oi7 t = Fg7.t();
            if (t != null) {
                t.a(str);
            }
        }
    }

    @DexIgnore
    public final boolean j() {
        return this.b;
    }

    @DexIgnore
    public final void k(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void l(Object obj) {
        if (j()) {
            m(obj);
        }
    }

    @DexIgnore
    public final void m(Object obj) {
        String str;
        if (this.c <= 5) {
            String a2 = a();
            if (a2 == null) {
                str = obj.toString();
            } else {
                str = a2 + " - " + obj;
            }
            Log.w(this.a, str);
            Oi7 t = Fg7.t();
            if (t != null) {
                t.b(str);
            }
        }
    }
}
