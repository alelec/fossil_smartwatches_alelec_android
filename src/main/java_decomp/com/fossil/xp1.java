package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.L70;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ L70 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xp1 createFromParcel(Parcel parcel) {
            return new Xp1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xp1[] newArray(int i) {
            return new Xp1[i];
        }
    }

    @DexIgnore
    public Xp1(byte b, L70 l70) {
        super(E90.MUSIC_CONTROL, b);
        this.d = l70;
    }

    @DexIgnore
    public /* synthetic */ Xp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.d = L70.valueOf(readString);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Xp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((Xp1) obj).d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.MusicControlNotification");
    }

    @DexIgnore
    public final L70 getAction() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.v0, Ey1.a(this.d));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
