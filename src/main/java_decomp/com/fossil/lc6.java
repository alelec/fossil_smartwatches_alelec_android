package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc6 extends ic6 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public sb6 e;
    @DexIgnore
    public ArrayList<Category> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<MicroApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<String, Boolean, Parcelable>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ ls0<String> l;
    @DexIgnore
    public /* final */ LiveData<List<MicroApp>> m;
    @DexIgnore
    public /* final */ ls0<List<MicroApp>> n;
    @DexIgnore
    public /* final */ ls0<gl7<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ jc6 p;
    @DexIgnore
    public /* final */ CategoryRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1", f = "MicroAppPresenter.kt", l = {229}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lc6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lc6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.lc6$a$a  reason: collision with other inner class name */
        public static final class C0139a extends ko7 implements vp7<iv7, qn7<? super Parcelable>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0139a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0139a aVar = new C0139a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Parcelable> qn7) {
                throw null;
                //return ((C0139a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return lc6.z(this.this$0.this$0).q(this.this$0.$id);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lc6 lc6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lc6;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$id, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            boolean d;
            Object g;
            Parcelable parcelable = null;
            Object d2 = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                d = bl5.c.d(this.$id);
                if (d) {
                    dv7 h = this.this$0.h();
                    C0139a aVar = new C0139a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = null;
                    this.Z$0 = d;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d2) {
                        return d2;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = lc6.r;
                local.d(str, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.j.l(new gl7(this.$id, ao7.a(d), parcelable));
                return tl7.f3441a;
            } else if (i == 1) {
                d = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) g;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = lc6.r;
            local2.d(str2, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.j.l(new gl7(this.$id, ao7.a(d), parcelable));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2175a;

        @DexIgnore
        public b(lc6 lc6) {
            this.f2175a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = lc6.r;
            local.d(str2, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.f2175a.p.u0(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2176a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1", f = "MicroAppPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ lc6 $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lc6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.lc6$c$a$a  reason: collision with other inner class name */
            public static final class C0140a extends ko7 implements vp7<iv7, qn7<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0140a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0140a aVar = new C0140a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends Category>> qn7) {
                    throw null;
                    //return ((C0140a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(lc6 lc6, qn7 qn7) {
                super(2, qn7);
                this.$this_run = lc6;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$this_run, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 b = bw7.b();
                    C0140a aVar = new C0140a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (!list.isEmpty()) {
                    this.$this_run.h.l(((Category) list.get(0)).getId());
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(lc6 lc6) {
            this.f2176a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(MicroApp microApp) {
            this.f2176a.G(microApp.getId());
            if (microApp != null) {
                String str = (String) this.f2176a.h.e();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = lc6.r;
                local.d(str2, "transform from selected microApp to category currentCategory=" + str + " compsCategories=" + microApp.getCategories());
                ArrayList<String> categories = microApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.f2176a.h.l(categories.get(0));
                } else {
                    this.f2176a.h.l(str);
                }
            } else {
                lc6 lc6 = this.f2176a;
                xw7 unused = gu7.d(lc6.k(), null, null, new a(lc6, null), 3, null);
            }
            return this.f2176a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<List<? extends MicroApp>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2177a;

        @DexIgnore
        public d(lc6 lc6) {
            this.f2177a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<MicroApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = lc6.r;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged microApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(str, sb.toString());
            if (list != null) {
                this.f2177a.p.U5(list);
                MicroApp e = lc6.z(this.f2177a).s().e();
                if (e != null) {
                    this.f2177a.p.A1(e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2178a;

        @DexIgnore
        public e(lc6 lc6) {
            this.f2178a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<MicroApp>> apply(String str) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d(lc6.r, "transform from category to list microApp with category=" + str);
            sb6 z2 = lc6.z(this.f2178a);
            pq7.b(str, "category");
            List<MicroApp> m = z2.m(str);
            ArrayList arrayList = new ArrayList();
            HybridPreset e = lc6.z(this.f2178a).o().e();
            MicroApp e2 = lc6.z(this.f2178a).s().e();
            String id = e2 != null ? e2.getId() : null;
            if (e != null) {
                for (MicroApp microApp : m) {
                    Iterator<T> it = e.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        T t2 = next;
                        if (!pq7.a(t2.getAppId(), microApp.getId()) || !(!pq7.a(t2.getAppId(), id))) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    if (t == null || pq7.a(microApp.getId(), "empty")) {
                        arrayList.add(microApp);
                    }
                }
            }
            this.f2178a.i.l(arrayList);
            return this.f2178a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<gl7<? extends String, ? extends Boolean, ? extends Parcelable>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2179a;

        @DexIgnore
        public f(lc6 lc6) {
            this.f2179a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<String, Boolean, ? extends Parcelable> gl7) {
            String str;
            String str2;
            if (gl7 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = lc6.r;
                local.d(str3, "onLiveDataChanged setting of " + gl7.getFirst() + " isSettingRequired " + gl7.getSecond().booleanValue() + " setting " + ((Parcelable) gl7.getThird()));
                boolean booleanValue = gl7.getSecond().booleanValue();
                MicroApp e = lc6.z(this.f2179a).s().e();
                if (!pq7.a(e != null ? e.getId() : null, gl7.getFirst())) {
                    return;
                }
                if (booleanValue) {
                    Parcelable parcelable = (Parcelable) gl7.getThird();
                    if (parcelable == null) {
                        str = bl5.c.a(gl7.getFirst());
                        str2 = "";
                    } else {
                        try {
                            String first = gl7.getFirst();
                            if (pq7.a(first, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                                str = "";
                                str2 = ((CommuteTimeSetting) parcelable).getAddress();
                            } else if (pq7.a(first, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                str = "";
                                str2 = ((SecondTimezoneSetting) parcelable).getTimeZoneName();
                            } else {
                                if (pq7.a(first, MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                                    str = "";
                                    str2 = ((Ringtone) parcelable).getRingtoneName();
                                }
                                str = "";
                                str2 = "";
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = lc6.r;
                            local2.d(str4, "exception when parse micro app setting " + e2);
                        }
                    }
                    this.f2179a.p.g0(true, gl7.getFirst(), str, str2);
                    return;
                }
                this.f2179a.p.g0(false, gl7.getFirst(), "", null);
                if (e != null) {
                    jc6 jc6 = this.f2179a.p;
                    String d = um5.d(PortfolioApp.h0.c(), e.getDescriptionKey(), e.getDescription());
                    pq7.b(d, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    jc6.g3(d);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1", f = "MicroAppPresenter.kt", l = {187}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lc6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Category>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(lc6 lc6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lc6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) g) {
                if (!lc6.z(this.this$0).m(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.f.clear();
            this.this$0.f.addAll(arrayList);
            this.this$0.p.S(this.this$0.f);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<MicroApp> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ lc6 f2180a;

        @DexIgnore
        public h(lc6 lc6) {
            this.f2180a = lc6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(MicroApp microApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = lc6.r;
            local.d(str, "onLiveDataChanged selectedMicroApp value=" + microApp);
            this.f2180a.g.l(microApp);
        }
    }

    /*
    static {
        String simpleName = lc6.class.getSimpleName();
        pq7.b(simpleName, "MicroAppPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public lc6(jc6 jc6, CategoryRepository categoryRepository) {
        pq7.c(jc6, "mView");
        pq7.c(categoryRepository, "mCategoryRepository");
        this.p = jc6;
        this.q = categoryRepository;
        LiveData<String> c2 = ss0.c(this.g, new c(this));
        pq7.b(c2, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.k = c2;
        this.l = new b(this);
        LiveData<List<MicroApp>> c3 = ss0.c(this.k, new e(this));
        pq7.b(c3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = c3;
        this.n = new d(this);
        this.o = new f(this);
    }

    @DexIgnore
    public static final /* synthetic */ sb6 z(lc6 lc6) {
        sb6 sb6 = lc6.e;
        if (sb6 != null) {
            return sb6;
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final xw7 G(String str) {
        return gu7.d(k(), null, null, new a(this, str, null), 3, null);
    }

    @DexIgnore
    public void H() {
        this.p.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, "onStart");
        this.k.i(this.l);
        this.m.i(this.n);
        this.j.i(this.o);
        if (this.f.isEmpty()) {
            xw7 unused = gu7.d(k(), null, null, new g(this, null), 3, null);
        } else {
            this.p.S(this.f);
        }
        sb6 sb6 = this.e;
        if (sb6 != null) {
            LiveData<MicroApp> s = sb6.s();
            jc6 jc6 = this.p;
            if (jc6 != null) {
                s.h((ub6) jc6, new h(this));
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        sb6 sb6 = this.e;
        if (sb6 != null) {
            LiveData<MicroApp> s = sb6.s();
            jc6 jc6 = this.p;
            if (jc6 != null) {
                s.n((ub6) jc6);
                this.i.m(this.n);
                this.h.m(this.l);
                this.j.m(this.o);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ic6
    public void n() {
        T t;
        T t2;
        T t3;
        String str;
        String appId;
        String appId2;
        sb6 sb6 = this.e;
        if (sb6 != null) {
            HybridPreset e2 = sb6.o().e();
            if (e2 != null) {
                Iterator<T> it = e2.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.getPosition(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                String str2 = (t4 == null || (appId2 = t4.getAppId()) == null) ? "empty" : appId2;
                Iterator<T> it2 = e2.getButtons().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (pq7.a(next2.getPosition(), "middle")) {
                        t2 = next2;
                        break;
                    }
                }
                T t5 = t2;
                String str3 = (t5 == null || (appId = t5.getAppId()) == null) ? "empty" : appId;
                Iterator<T> it3 = e2.getButtons().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (pq7.a(next3.getPosition(), "bottom")) {
                        t3 = next3;
                        break;
                    }
                }
                T t6 = t3;
                if (t6 == null || (str = t6.getAppId()) == null) {
                    str = "empty";
                }
                this.p.a0(str2, str3, str);
                return;
            }
            this.p.a0("empty", "empty", "empty");
            return;
        }
        pq7.n("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        if (r1 != null) goto L_0x0051;
     */
    @DexIgnore
    @Override // com.fossil.ic6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void o() {
        /*
            r6 = this;
            r3 = 0
            com.fossil.sb6 r0 = r6.e
            if (r0 == 0) goto L_0x0094
            androidx.lifecycle.LiveData r0 = r0.s()
            java.lang.Object r0 = r0.e()
            com.portfolio.platform.data.model.room.microapp.MicroApp r0 = (com.portfolio.platform.data.model.room.microapp.MicroApp) r0
            if (r0 == 0) goto L_0x0066
            com.fossil.sb6 r1 = r6.e
            if (r1 == 0) goto L_0x008e
            androidx.lifecycle.MutableLiveData r1 = r1.o()
            java.lang.Object r1 = r1.e()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r1
            if (r1 == 0) goto L_0x0067
            java.util.ArrayList r1 = r1.getButtons()
            if (r1 == 0) goto L_0x0067
            java.util.Iterator r4 = r1.iterator()
        L_0x002b:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x009a
            java.lang.Object r2 = r4.next()
            r1 = r2
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r1 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r1
            java.lang.String r5 = r0.getId()
            java.lang.String r1 = r1.getAppId()
            boolean r1 = com.fossil.pq7.a(r5, r1)
            if (r1 == 0) goto L_0x002b
            r1 = r2
        L_0x0047:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r1 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r1
            if (r1 == 0) goto L_0x0067
            java.lang.String r1 = r1.getSettings()
            if (r1 == 0) goto L_0x0067
        L_0x0051:
            java.lang.String r0 = r0.getId()
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.fossil.pq7.a(r0, r2)
            if (r2 == 0) goto L_0x006a
            com.fossil.jc6 r0 = r6.p
            r0.f0(r1)
        L_0x0066:
            return
        L_0x0067:
            java.lang.String r1 = ""
            goto L_0x0051
        L_0x006a:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.fossil.pq7.a(r0, r2)
            if (r2 == 0) goto L_0x007c
            com.fossil.jc6 r0 = r6.p
            r0.d1(r1)
            goto L_0x0066
        L_0x007c:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE
            java.lang.String r2 = r2.getValue()
            boolean r0 = com.fossil.pq7.a(r0, r2)
            if (r0 == 0) goto L_0x0066
            com.fossil.jc6 r0 = r6.p
            r0.a5(r1)
            goto L_0x0066
        L_0x008e:
            java.lang.String r0 = "mHybridCustomizeViewModel"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x0094:
            java.lang.String r0 = "mHybridCustomizeViewModel"
            com.fossil.pq7.n(r0)
            throw r3
        L_0x009a:
            r1 = r3
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lc6.o():void");
    }

    @DexIgnore
    @Override // com.fossil.ic6
    public void p(Category category) {
        pq7.c(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "category change " + category);
        this.h.l(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.ic6
    public void q(String str) {
        T t;
        pq7.c(str, "newMicroAppId");
        sb6 sb6 = this.e;
        if (sb6 != null) {
            MicroApp n2 = sb6.n(str);
            FLogger.INSTANCE.getLocal().d(r, "onUserChooseMicroApp " + n2);
            if (n2 != null) {
                sb6 sb62 = this.e;
                if (sb62 != null) {
                    HybridPreset e2 = sb62.o().e();
                    if (e2 != null) {
                        HybridPreset clone = e2.clone();
                        ArrayList arrayList = new ArrayList();
                        sb6 sb63 = this.e;
                        if (sb63 != null) {
                            String e3 = sb63.t().e();
                            if (e3 != null) {
                                pq7.b(e3, "mHybridCustomizeViewMode\u2026ctedMicroAppPos().value!!");
                                String str2 = e3;
                                sb6 sb64 = this.e;
                                if (sb64 == null) {
                                    pq7.n("mHybridCustomizeViewModel");
                                    throw null;
                                } else if (!sb64.y(str)) {
                                    Iterator<HybridPresetAppSetting> it = clone.getButtons().iterator();
                                    while (it.hasNext()) {
                                        HybridPresetAppSetting next = it.next();
                                        if (pq7.a(next.getPosition(), str2)) {
                                            arrayList.add(new HybridPresetAppSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getButtons().clear();
                                    clone.getButtons().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(r, "Update current preset=" + clone);
                                    sb6 sb65 = this.e;
                                    if (sb65 != null) {
                                        sb65.B(clone);
                                    } else {
                                        pq7.n("mHybridCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = e2.getButtons().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t = null;
                                            break;
                                        }
                                        T next2 = it2.next();
                                        if (pq7.a(next2.getAppId(), str)) {
                                            t = next2;
                                            break;
                                        }
                                    }
                                    T t2 = t;
                                    if (t2 != null) {
                                        sb6 sb66 = this.e;
                                        if (sb66 != null) {
                                            sb66.A(t2.getPosition());
                                        } else {
                                            pq7.n("mHybridCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.n("mHybridCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    pq7.n("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ic6
    public void r(sb6 sb6) {
        pq7.c(sb6, "viewModel");
        this.e = sb6;
    }

    @DexIgnore
    @Override // com.fossil.ic6
    public void s(String str, String str2) {
        T t;
        pq7.c(str, "microAppId");
        pq7.c(str2, MicroAppSetting.SETTING);
        sb6 sb6 = this.e;
        if (sb6 != null) {
            HybridPreset e2 = sb6.o().e();
            if (e2 != null) {
                HybridPreset clone = e2.clone();
                Iterator<T> it = clone.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.getAppId(), str)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    t2.setSettings(str2);
                }
                FLogger.INSTANCE.getLocal().d(r, "update new setting " + str2 + " of " + str);
                sb6 sb62 = this.e;
                if (sb62 != null) {
                    sb62.B(clone);
                } else {
                    pq7.n("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            pq7.n("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
