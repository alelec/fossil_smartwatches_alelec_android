package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ij2 implements Parcelable.Creator<Gj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Gj2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        IBinder iBinder = null;
        Uh2 uh2 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                uh2 = (Uh2) Ad2.e(parcel, t, Uh2.CREATOR);
            } else if (l == 2) {
                iBinder = Ad2.u(parcel, t);
            } else if (l == 3) {
                j2 = Ad2.y(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                j = Ad2.y(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Gj2(uh2, iBinder, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Gj2[] newArray(int i) {
        return new Gj2[i];
    }
}
