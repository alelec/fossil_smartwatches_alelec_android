package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wn4 {
    @DexIgnore
    public static /* final */ byte[] a; // = {48, Framer.STDOUT_FRAME_PREFIX, Framer.STDERR_FRAME_PREFIX, 51, 52, 53, 54, 55, 56, 57, 38, 13, 9, 44, 58, 35, Framer.STDIN_FRAME_PREFIX, 46, 36, 47, 43, 37, 42, 61, 94, 0, 32, 0, 0, 0};
    @DexIgnore
    public static /* final */ byte[] b; // = {59, 60, 62, 64, 91, 92, 93, Framer.STDIN_REQUEST_FRAME_PREFIX, 96, 126, Framer.ENTER_FRAME_PREFIX, 13, 9, 44, 58, 10, Framer.STDIN_FRAME_PREFIX, 46, 36, 47, 34, 124, 42, 40, 41, 63, 123, 125, 39, 0};
    @DexIgnore
    public static /* final */ byte[] c; // = new byte[128];
    @DexIgnore
    public static /* final */ byte[] d; // = new byte[128];
    @DexIgnore
    public static /* final */ Charset e; // = Charset.forName("ISO-8859-1");

    /*
    static {
        int i = 0;
        Arrays.fill(c, (byte) -1);
        int i2 = 0;
        while (true) {
            byte[] bArr = a;
            if (i2 >= bArr.length) {
                break;
            }
            byte b2 = bArr[i2];
            if (b2 > 0) {
                c[b2] = (byte) ((byte) i2);
            }
            i2++;
        }
        Arrays.fill(d, (byte) -1);
        while (true) {
            byte[] bArr2 = b;
            if (i < bArr2.length) {
                byte b3 = bArr2[i];
                if (b3 > 0) {
                    d[b3] = (byte) ((byte) i);
                }
                i++;
            } else {
                return;
            }
        }
    }
    */

    @DexIgnore
    public static int a(String str, int i, Charset charset) throws Rl4 {
        CharsetEncoder newEncoder = charset.newEncoder();
        int length = str.length();
        int i2 = i;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            int i3 = 0;
            while (i3 < 13 && k(charAt)) {
                i3++;
                int i4 = i2 + i3;
                if (i4 >= length) {
                    break;
                }
                charAt = str.charAt(i4);
            }
            if (i3 >= 13) {
                return i2 - i;
            }
            char charAt2 = str.charAt(i2);
            if (newEncoder.canEncode(charAt2)) {
                i2++;
            } else {
                throw new Rl4("Non-encodable character detected: " + charAt2 + " (Unicode: " + ((int) charAt2) + ')');
            }
        }
        return i2 - i;
    }

    @DexIgnore
    public static int b(CharSequence charSequence, int i) {
        int i2 = 0;
        int length = charSequence.length();
        if (i < length) {
            char charAt = charSequence.charAt(i);
            while (k(charAt) && i < length) {
                i2++;
                i++;
                if (i < length) {
                    charAt = charSequence.charAt(i);
                }
            }
        }
        return i2;
    }

    @DexIgnore
    public static int c(CharSequence charSequence, int i) {
        int length = charSequence.length();
        int i2 = i;
        while (i2 < length) {
            char charAt = charSequence.charAt(i2);
            int i3 = 0;
            while (i3 < 13 && k(charAt) && i2 < length) {
                i3++;
                i2++;
                if (i2 < length) {
                    charAt = charSequence.charAt(i2);
                }
            }
            if (i3 < 13) {
                if (i3 <= 0) {
                    if (!n(charSequence.charAt(i2))) {
                        break;
                    }
                    i2++;
                }
            } else {
                return (i2 - i) - i3;
            }
        }
        return i2 - i;
    }

    @DexIgnore
    public static void d(byte[] bArr, int i, int i2, int i3, StringBuilder sb) {
        int i4;
        if (i2 == 1 && i3 == 0) {
            sb.append('\u0391');
        } else if (i2 % 6 == 0) {
            sb.append('\u039c');
        } else {
            sb.append('\u0385');
        }
        if (i2 >= 6) {
            char[] cArr = new char[5];
            i4 = i;
            while ((i + i2) - i4 >= 6) {
                long j = 0;
                for (int i5 = 0; i5 < 6; i5++) {
                    j = (j << 8) + ((long) (bArr[i4 + i5] & 255));
                }
                for (int i6 = 0; i6 < 5; i6++) {
                    cArr[i6] = (char) ((char) ((int) (j % 900)));
                    j /= 900;
                }
                for (int i7 = 4; i7 >= 0; i7--) {
                    sb.append(cArr[i7]);
                }
                i4 += 6;
            }
        } else {
            i4 = i;
        }
        while (i4 < i + i2) {
            sb.append((char) (bArr[i4] & 255));
            i4++;
        }
    }

    @DexIgnore
    public static String e(String str, Sn4 sn4, Charset charset) throws Rl4 {
        Cm4 characterSetECIByName;
        StringBuilder sb = new StringBuilder(str.length());
        if (charset == null) {
            charset = e;
        } else if (!e.equals(charset) && (characterSetECIByName = Cm4.getCharacterSetECIByName(charset.name())) != null) {
            h(characterSetECIByName.getValue(), sb);
        }
        int length = str.length();
        if (sn4 == Sn4.TEXT) {
            g(str, 0, length, sb, 0);
        } else if (sn4 == Sn4.BYTE) {
            byte[] bytes = str.getBytes(charset);
            d(bytes, 0, bytes.length, 1, sb);
        } else if (sn4 == Sn4.NUMERIC) {
            sb.append('\u0386');
            f(str, 0, length, sb);
        } else {
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (i3 < length) {
                int b2 = b(str, i3);
                if (b2 >= 13) {
                    sb.append('\u0386');
                    i = 2;
                    f(str, i3, b2, sb);
                    i2 = 0;
                    i3 = b2 + i3;
                } else {
                    int c2 = c(str, i3);
                    if (c2 >= 5 || b2 == length) {
                        if (i != 0) {
                            sb.append('\u0384');
                            i = 0;
                            i2 = 0;
                        }
                        i2 = g(str, i3, c2, sb, i2);
                        i3 += c2;
                    } else {
                        int a2 = a(str, i3, charset);
                        if (a2 == 0) {
                            a2 = 1;
                        }
                        int i4 = a2 + i3;
                        byte[] bytes2 = str.substring(i3, i4).getBytes(charset);
                        if (bytes2.length == 1 && i == 0) {
                            d(bytes2, 0, 1, 0, sb);
                        } else {
                            d(bytes2, 0, bytes2.length, i, sb);
                            i = 1;
                            i2 = 0;
                        }
                        i3 = i4;
                    }
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static void f(String str, int i, int i2, StringBuilder sb) {
        StringBuilder sb2 = new StringBuilder((i2 / 3) + 1);
        BigInteger valueOf = BigInteger.valueOf(900);
        BigInteger valueOf2 = BigInteger.valueOf(0);
        int i3 = 0;
        while (i3 < i2) {
            sb2.setLength(0);
            int min = Math.min(44, i2 - i3);
            StringBuilder sb3 = new StringBuilder("1");
            int i4 = i + i3;
            sb3.append(str.substring(i4, i4 + min));
            BigInteger bigInteger = new BigInteger(sb3.toString());
            do {
                sb2.append((char) bigInteger.mod(valueOf).intValue());
                bigInteger = bigInteger.divide(valueOf);
            } while (!bigInteger.equals(valueOf2));
            for (int length = sb2.length() - 1; length >= 0; length--) {
                sb.append(sb2.charAt(length));
            }
            i3 += min;
        }
    }

    @DexIgnore
    public static int g(CharSequence charSequence, int i, int i2, StringBuilder sb, int i3) {
        char charAt;
        StringBuilder sb2 = new StringBuilder(i2);
        int i4 = 0;
        while (true) {
            int i5 = i + i4;
            char charAt2 = charSequence.charAt(i5);
            if (i3 != 0) {
                if (i3 != 1) {
                    if (i3 != 2) {
                        if (m(charAt2)) {
                            sb2.append((char) d[charAt2]);
                        } else {
                            sb2.append((char) 29);
                        }
                    } else if (l(charAt2)) {
                        sb2.append((char) c[charAt2]);
                    } else if (j(charAt2)) {
                        sb2.append((char) 28);
                    } else if (i(charAt2)) {
                        sb2.append((char) 27);
                        i3 = 1;
                    } else {
                        int i6 = i5 + 1;
                        if (i6 >= i2 || !m(charSequence.charAt(i6))) {
                            sb2.append((char) 29);
                            sb2.append((char) d[charAt2]);
                        } else {
                            i3 = 3;
                            sb2.append((char) 25);
                        }
                    }
                    i3 = 0;
                } else if (i(charAt2)) {
                    if (charAt2 == ' ') {
                        sb2.append((char) 26);
                    } else {
                        sb2.append((char) (charAt2 - 'a'));
                    }
                } else if (j(charAt2)) {
                    sb2.append((char) 27);
                    sb2.append((char) (charAt2 - 'A'));
                } else if (l(charAt2)) {
                    sb2.append((char) 28);
                    i3 = 2;
                } else {
                    sb2.append((char) 29);
                    sb2.append((char) d[charAt2]);
                }
            } else if (j(charAt2)) {
                if (charAt2 == ' ') {
                    sb2.append((char) 26);
                } else {
                    sb2.append((char) (charAt2 - 'A'));
                }
            } else if (i(charAt2)) {
                sb2.append((char) 27);
                i3 = 1;
            } else if (l(charAt2)) {
                sb2.append((char) 28);
                i3 = 2;
            } else {
                sb2.append((char) 29);
                sb2.append((char) d[charAt2]);
            }
            i4++;
            if (i4 >= i2) {
                break;
            }
        }
        int length = sb2.length();
        char c2 = 0;
        int i7 = 0;
        while (i7 < length) {
            if (i7 % 2 != 0) {
                charAt = (char) ((c2 * 30) + sb2.charAt(i7));
                sb.append(charAt);
            } else {
                charAt = sb2.charAt(i7);
            }
            i7++;
            c2 = charAt;
        }
        if (length % 2 != 0) {
            sb.append((char) ((c2 * 30) + 29));
        }
        return i3;
    }

    @DexIgnore
    public static void h(int i, StringBuilder sb) throws Rl4 {
        if (i >= 0 && i < 900) {
            sb.append('\u039f');
            sb.append((char) i);
        } else if (i < 810900) {
            sb.append('\u039e');
            sb.append((char) ((i / 900) - 1));
            sb.append((char) (i % 900));
        } else if (i < 811800) {
            sb.append('\u039d');
            sb.append((char) (810900 - i));
        } else {
            throw new Rl4("ECI number not in valid range from 0..811799, but was " + i);
        }
    }

    @DexIgnore
    public static boolean i(char c2) {
        return c2 == ' ' || (c2 >= 'a' && c2 <= 'z');
    }

    @DexIgnore
    public static boolean j(char c2) {
        return c2 == ' ' || (c2 >= 'A' && c2 <= 'Z');
    }

    @DexIgnore
    public static boolean k(char c2) {
        return c2 >= '0' && c2 <= '9';
    }

    @DexIgnore
    public static boolean l(char c2) {
        return c[c2] != -1;
    }

    @DexIgnore
    public static boolean m(char c2) {
        return d[c2] != -1;
    }

    @DexIgnore
    public static boolean n(char c2) {
        return c2 == '\t' || c2 == '\n' || c2 == '\r' || (c2 >= ' ' && c2 <= '~');
    }
}
