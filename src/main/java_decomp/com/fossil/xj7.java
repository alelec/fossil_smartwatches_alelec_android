package com.fossil;

import android.app.Application;
import com.google.errorprone.annotations.ForOverride;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xj7 extends Application implements Dk7 {
    @DexIgnore
    public volatile Ck7<Object> b;

    @DexIgnore
    @ForOverride
    public abstract Vj7<? extends Xj7> a();

    @DexIgnore
    public final void b() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    a().a(this);
                    if (this.b == null) {
                        throw new IllegalStateException("The AndroidInjector returned from applicationInjector() did not inject the DaggerApplication");
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Dk7
    public Vj7<Object> c() {
        b();
        return this.b;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        b();
    }
}
