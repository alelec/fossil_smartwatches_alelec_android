package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Wo3 implements Runnable {
    @DexIgnore
    public /* final */ To3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Exception d;
    @DexIgnore
    public /* final */ byte[] e;
    @DexIgnore
    public /* final */ Map f;

    @DexIgnore
    public Wo3(To3 to3, int i, Exception exc, byte[] bArr, Map map) {
        this.b = to3;
        this.c = i;
        this.d = exc;
        this.e = bArr;
        this.f = map;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d, this.e, this.f);
    }
}
