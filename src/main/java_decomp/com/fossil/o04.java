package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O04 extends S04 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new Ai();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new Bi();
    @DexIgnore
    public AnimatorSet f;
    @DexIgnore
    public ValueAnimator g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements TextWatcher {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (!O04.j(editable)) {
                O04.this.f.cancel();
                O04.this.g.start();
            } else if (!O04.this.a.E()) {
                O04.this.g.cancel();
                O04.this.f.start();
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements TextInputLayout.f {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(O04.j(editText.getText()));
            textInputLayout.setEndIconCheckable(false);
            editText.removeTextChangedListener(O04.this.d);
            editText.addTextChangedListener(O04.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements View.OnClickListener {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void onClick(View view) {
            O04.this.a.getEditText().setText((CharSequence) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends AnimatorListenerAdapter {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            O04.this.a.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends AnimatorListenerAdapter {
        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            O04.this.a.setEndIconVisible(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            O04.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            O04.this.c.setScaleX(floatValue);
            O04.this.c.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public O04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public static boolean j(Editable editable) {
        return editable.length() > 0;
    }

    @DexIgnore
    @Override // com.fossil.S04
    public void a() {
        this.a.setEndIconDrawable(Gf0.d(this.b, Mw3.mtrl_ic_cancel));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(Rw3.clear_text_end_icon_content_description));
        this.a.setEndIconOnClickListener(new Ci());
        this.a.c(this.e);
        k();
    }

    @DexIgnore
    public final ValueAnimator h(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(Uw3.a);
        ofFloat.setDuration(100L);
        ofFloat.addUpdateListener(new Fi());
        return ofFloat;
    }

    @DexIgnore
    public final ValueAnimator i() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.8f, 1.0f);
        ofFloat.setInterpolator(Uw3.d);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new Gi());
        return ofFloat;
    }

    @DexIgnore
    public final void k() {
        ValueAnimator i = i();
        ValueAnimator h = h(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        AnimatorSet animatorSet = new AnimatorSet();
        this.f = animatorSet;
        animatorSet.playTogether(i, h);
        this.f.addListener(new Di());
        ValueAnimator h2 = h(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = h2;
        h2.addListener(new Ei());
    }
}
