package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cy2 extends Ss2 implements Bw2 {
    @DexIgnore
    public Cy2(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    @Override // com.fossil.Bw2
    public final Bundle c(Bundle bundle) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        Parcel e = e(1, d);
        Bundle bundle2 = (Bundle) Qt2.a(e, Bundle.CREATOR);
        e.recycle();
        return bundle2;
    }
}
