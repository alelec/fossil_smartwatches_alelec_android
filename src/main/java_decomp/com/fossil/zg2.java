package com.fossil;

import android.os.Bundle;
import com.fossil.Qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zg2 implements Qg2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ Qg2 b;

    @DexIgnore
    public Zg2(Qg2 qg2, Bundle bundle) {
        this.b = qg2;
        this.a = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final void a(Sg2 sg2) {
        this.b.a.onCreate(this.a);
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final int getState() {
        return 1;
    }
}
