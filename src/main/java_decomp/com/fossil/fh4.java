package com.fossil;

import com.fossil.Ch4;
import com.fossil.Eh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fh4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Fh4 a();

        @DexIgnore
        public abstract Ai b(String str);

        @DexIgnore
        public abstract Ai c(long j);

        @DexIgnore
        public abstract Ai d(String str);

        @DexIgnore
        public abstract Ai e(String str);

        @DexIgnore
        public abstract Ai f(String str);

        @DexIgnore
        public abstract Ai g(Eh4.Ai ai);

        @DexIgnore
        public abstract Ai h(long j);
    }

    /*
    static {
        a().a();
    }
    */

    @DexIgnore
    public static Ai a() {
        Ch4.Bi bi = new Ch4.Bi();
        bi.h(0);
        bi.g(Eh4.Ai.ATTEMPT_MIGRATION);
        bi.c(0);
        return bi;
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract Eh4.Ai g();

    @DexIgnore
    public abstract long h();

    @DexIgnore
    public boolean i() {
        return g() == Eh4.Ai.REGISTER_ERROR;
    }

    @DexIgnore
    public boolean j() {
        return g() == Eh4.Ai.NOT_GENERATED || g() == Eh4.Ai.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public boolean k() {
        return g() == Eh4.Ai.REGISTERED;
    }

    @DexIgnore
    public boolean l() {
        return g() == Eh4.Ai.UNREGISTERED;
    }

    @DexIgnore
    public boolean m() {
        return g() == Eh4.Ai.ATTEMPT_MIGRATION;
    }

    @DexIgnore
    public abstract Ai n();

    @DexIgnore
    public Fh4 o(String str, long j, long j2) {
        Ai n = n();
        n.b(str);
        n.c(j);
        n.h(j2);
        return n.a();
    }

    @DexIgnore
    public Fh4 p() {
        Ai n = n();
        n.b(null);
        return n.a();
    }

    @DexIgnore
    public Fh4 q(String str) {
        Ai n = n();
        n.e(str);
        n.g(Eh4.Ai.REGISTER_ERROR);
        return n.a();
    }

    @DexIgnore
    public Fh4 r() {
        Ai n = n();
        n.g(Eh4.Ai.NOT_GENERATED);
        return n.a();
    }

    @DexIgnore
    public Fh4 s(String str, String str2, long j, String str3, long j2) {
        Ai n = n();
        n.d(str);
        n.g(Eh4.Ai.REGISTERED);
        n.b(str3);
        n.f(str2);
        n.c(j2);
        n.h(j);
        return n.a();
    }

    @DexIgnore
    public Fh4 t(String str) {
        Ai n = n();
        n.d(str);
        n.g(Eh4.Ai.UNREGISTERED);
        return n.a();
    }
}
