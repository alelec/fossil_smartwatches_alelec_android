package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W75 extends V75 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d h0; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray i0;
    @DexIgnore
    public long g0;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        i0 = sparseIntArray;
        sparseIntArray.put(2131362759, 1);
        i0.put(2131362737, 2);
        i0.put(2131362120, 3);
        i0.put(2131362927, 4);
        i0.put(2131363416, 5);
        i0.put(2131363415, 6);
        i0.put(2131363144, 7);
        i0.put(2131362096, 8);
        i0.put(2131363450, 9);
        i0.put(2131363172, 10);
        i0.put(2131362011, 11);
        i0.put(2131361813, 12);
        i0.put(2131362137, 13);
        i0.put(2131362125, 14);
        i0.put(2131363018, 15);
        i0.put(2131363017, 16);
        i0.put(2131363019, 17);
        i0.put(2131363020, 18);
        i0.put(2131362087, 19);
        i0.put(2131363451, 20);
        i0.put(2131362406, 21);
        i0.put(2131362295, 22);
        i0.put(2131362086, 23);
        i0.put(2131363412, 24);
        i0.put(2131362899, 25);
        i0.put(2131362080, 26);
        i0.put(2131362480, 27);
        i0.put(2131362411, 28);
        i0.put(2131361986, 29);
        i0.put(2131362321, 30);
        i0.put(2131362320, 31);
        i0.put(2131362322, 32);
        i0.put(2131362324, 33);
        i0.put(2131362325, 34);
        i0.put(2131362323, 35);
        i0.put(2131363521, 36);
        i0.put(2131363058, 37);
        i0.put(2131362068, 38);
        i0.put(2131363410, 39);
        i0.put(2131361954, 40);
        i0.put(2131361946, 41);
    }
    */

    @DexIgnore
    public W75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 42, h0, i0));
    }

    @DexIgnore
    public W75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (AppBarLayout) objArr[12], (FlexibleTextView) objArr[41], (FlexibleTextView) objArr[40], (ConstraintLayout) objArr[29], (CoordinatorLayout) objArr[11], (ConstraintLayout) objArr[38], (ConstraintLayout) objArr[26], (ConstraintLayout) objArr[23], (ConstraintLayout) objArr[19], (ConstraintLayout) objArr[8], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[14], (CollapsingToolbarLayout) objArr[13], (FlexibleButton) objArr[22], (FlexibleFitnessTab) objArr[31], (FlexibleFitnessTab) objArr[30], (FlexibleFitnessTab) objArr[32], (FlexibleFitnessTab) objArr[35], (FlexibleFitnessTab) objArr[33], (FlexibleFitnessTab) objArr[34], (FlexibleTextView) objArr[21], (FlexibleTextView) objArr[28], (FlexibleTextView) objArr[27], (ImageView) objArr[2], (RTLImageView) objArr[1], (NestedScrollView) objArr[25], (FlexibleProgressBar) objArr[4], (ConstraintLayout) objArr[0], (RingProgressBar) objArr[16], (RingProgressBar) objArr[15], (RingProgressBar) objArr[17], (RingProgressBar) objArr[18], (ViewPager2) objArr[37], (CustomSwipeRefreshLayout) objArr[7], (FlexibleProgressBar) objArr[10], (FlexibleTextView) objArr[39], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (View) objArr[9], (View) objArr[20], (View) objArr[36]);
        this.g0 = -1;
        this.R.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.g0 = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.g0 != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.g0 = 1;
        }
        w();
    }
}
