package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dl1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ry1 b;
    @DexIgnore
    public /* final */ Ry1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Dl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dl1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Ry1.class.getClassLoader());
            if (readParcelable != null) {
                Ry1 ry1 = (Ry1) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(Ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new Dl1(ry1, (Ry1) readParcelable2);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dl1[] newArray(int i) {
            return new Dl1[i];
        }
    }

    @DexIgnore
    public Dl1(Ry1 ry1, Ry1 ry12) {
        this.b = ry1;
        this.c = ry12;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Dl1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Dl1 dl1 = (Dl1) obj;
            if (!Wg6.a(this.b, dl1.b)) {
                return false;
            }
            return !(Wg6.a(this.c, dl1.c) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.VersionInformation");
    }

    @DexIgnore
    public final Ry1 getCurrentVersion() {
        return this.b;
    }

    @DexIgnore
    public final Ry1 getSupportedVersion() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.B4, this.b), Jd0.C4, this.c);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
