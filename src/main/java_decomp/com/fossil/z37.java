package com.fossil;

import android.annotation.TargetApi;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.Calendar;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z37 {
    @DexIgnore
    public static /* final */ KeyStore a;
    @DexIgnore
    public static /* final */ Z37 b;

    /*
    static {
        Z37 z37 = new Z37();
        b = z37;
        a = z37.a();
    }
    */

    @DexIgnore
    public final KeyStore a() {
        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
        instance.load(null);
        Wg6.b(instance, "mKeyStore");
        return instance;
    }

    @DexIgnore
    public final KeyPair b(String str) {
        Wg6.c(str, "alias");
        KeyPairGenerator instance = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
        Calendar instance2 = Calendar.getInstance();
        Calendar instance3 = Calendar.getInstance();
        instance3.add(1, 20);
        KeyPairGeneratorSpec.Builder serialNumber = new KeyPairGeneratorSpec.Builder(PortfolioApp.get.instance()).setAlias(str).setSerialNumber(BigInteger.ONE);
        KeyPairGeneratorSpec.Builder subject = serialNumber.setSubject(new X500Principal("CN=" + str + " CA Certificate"));
        Wg6.b(instance2, GoalPhase.COLUMN_START_DATE);
        KeyPairGeneratorSpec.Builder startDate = subject.setStartDate(instance2.getTime());
        Wg6.b(instance3, GoalPhase.COLUMN_END_DATE);
        KeyPairGeneratorSpec.Builder endDate = startDate.setEndDate(instance3.getTime());
        Wg6.b(endDate, "KeyPairGeneratorSpec.Bui\u2026.setEndDate(endDate.time)");
        instance.initialize(endDate.build());
        KeyPair generateKeyPair = instance.generateKeyPair();
        Wg6.b(generateKeyPair, "generator.generateKeyPair()");
        return generateKeyPair;
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey c(String str) {
        KeyGenerator instance = KeyGenerator.getInstance("AES", "AndroidKeyStore");
        KeyGenParameterSpec build = new KeyGenParameterSpec.Builder(str, 3).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build();
        Wg6.b(build, "KeyGenParameterSpec.Buil\u2026\n                .build()");
        instance.init(build);
        SecretKey generateKey = instance.generateKey();
        Wg6.b(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final KeyPair d(String str) {
        Wg6.c(str, "alias");
        PrivateKey privateKey = (PrivateKey) a.getKey(str, null);
        Certificate certificate = a.getCertificate(str);
        PublicKey publicKey = certificate != null ? certificate.getPublicKey() : null;
        if (privateKey == null || publicKey == null) {
            return null;
        }
        return new KeyPair(publicKey, privateKey);
    }

    @DexIgnore
    @TargetApi(23)
    public final SecretKey e(String str) {
        Wg6.c(str, "aliasName");
        if (!a.containsAlias(str)) {
            return c(str);
        }
        KeyStore.Entry entry = a.getEntry(str, null);
        if (entry != null) {
            SecretKey secretKey = ((KeyStore.SecretKeyEntry) entry).getSecretKey();
            Wg6.b(secretKey, "secretKeyEntry.secretKey");
            return secretKey;
        }
        throw new Rc6("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    @TargetApi(23)
    public final KeyStore.SecretKeyEntry f(String str) {
        Wg6.c(str, "aliasName");
        KeyStore.Entry entry = a.getEntry(str, null);
        if (entry != null) {
            return (KeyStore.SecretKeyEntry) entry;
        }
        throw new Rc6("null cannot be cast to non-null type java.security.KeyStore.SecretKeyEntry");
    }

    @DexIgnore
    public final void g(String str) {
        Wg6.c(str, "alias");
        a.deleteEntry(str);
    }
}
