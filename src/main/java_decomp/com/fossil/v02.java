package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V02 {

    @DexIgnore
    public enum Ai {
        OK,
        TRANSIENT_ERROR,
        FATAL_ERROR
    }

    @DexIgnore
    public static V02 a() {
        return new Q02(Ai.FATAL_ERROR, -1);
    }

    @DexIgnore
    public static V02 d(long j) {
        return new Q02(Ai.OK, j);
    }

    @DexIgnore
    public static V02 e() {
        return new Q02(Ai.TRANSIENT_ERROR, -1);
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract Ai c();
}
