package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y63 implements V63 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.client.sessions.check_on_reset_and_enable2", true);
        hw2.d("measurement.client.sessions.check_on_startup", true);
        hw2.d("measurement.client.sessions.start_session_before_view_screen", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.V63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.V63
    public final boolean zzb() {
        return a.o().booleanValue();
    }
}
