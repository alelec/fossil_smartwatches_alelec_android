package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ug7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public Ug7(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    @DexIgnore
    public final void run() {
        try {
            Ig7.u(this.b);
            Gh7.b(this.b).d(this.c);
        } catch (Throwable th) {
            Ig7.m.e(th);
            Ig7.f(this.b, th);
        }
    }
}
