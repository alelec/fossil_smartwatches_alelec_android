package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ix1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ou1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Ry1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ou1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ou1 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                Wg6.b(createByteArray, "parcel.createByteArray()!!");
                return new Ou1(createByteArray);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ou1[] newArray(int i) {
            return new Ou1[i];
        }
    }

    @DexIgnore
    public Ou1(byte[] bArr) throws IllegalArgumentException {
        this.b = bArr;
        this.c = new Ry1(bArr[2], bArr[3]);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.b;
    }

    @DexIgnore
    public final Ry1 getFileVersion() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.j2, this.c.toString()), Jd0.f3, Long.valueOf(Ix1.a.b(this.b, Ix1.Ai.CRC32C))), Jd0.I, Integer.valueOf(this.b.length));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
