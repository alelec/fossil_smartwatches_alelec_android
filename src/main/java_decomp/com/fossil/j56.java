package com.fossil;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J56 implements Factory<I56> {
    @DexIgnore
    public static I56 a(D56 d56, int i, ArrayList<String> arrayList, Uq4 uq4, L56 l56) {
        return new I56(d56, i, arrayList, uq4, l56);
    }
}
