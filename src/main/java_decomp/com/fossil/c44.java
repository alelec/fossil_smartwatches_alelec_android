package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface C44<E> extends Collection<E> {

    @DexIgnore
    public interface Ai<E> {
        @DexIgnore
        int getCount();

        @DexIgnore
        E getElement();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    int add(E e, int i);

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    boolean add(E e);

    @DexIgnore
    boolean contains(Object obj);

    @DexIgnore
    @Override // java.util.Collection
    boolean containsAll(Collection<?> collection);

    @DexIgnore
    int count(Object obj);

    @DexIgnore
    Set<E> elementSet();

    @DexIgnore
    Set<Ai<E>> entrySet();

    @DexIgnore
    @CanIgnoreReturnValue
    int remove(Object obj, int i);

    @DexIgnore
    @CanIgnoreReturnValue
    int setCount(E e, int i);

    @DexIgnore
    @CanIgnoreReturnValue
    boolean setCount(E e, int i, int i2);
}
