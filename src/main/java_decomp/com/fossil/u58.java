package com.fossil;

import com.facebook.internal.ServerProtocol;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U58 implements J58 {
    @DexIgnore
    public I58 a;
    @DexIgnore
    public S58 b;
    @DexIgnore
    public List c;

    @DexIgnore
    @Override // com.fossil.J58
    public I58 a(S58 s58, String[] strArr, boolean z) throws T58 {
        return f(s58, strArr, null, z);
    }

    @DexIgnore
    public void b() throws O58 {
        if (!e().isEmpty()) {
            throw new O58(e());
        }
    }

    @DexIgnore
    public abstract String[] c(S58 s58, String[] strArr, boolean z);

    @DexIgnore
    public S58 d() {
        return this.b;
    }

    @DexIgnore
    public List e() {
        return this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009b, code lost:
        if (r8 != false) goto L_0x004b;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004e A[LOOP:2: B:14:0x004e->B:39:0x004e, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0037 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.I58 f(com.fossil.S58 r5, java.lang.String[] r6, java.util.Properties r7, boolean r8) throws com.fossil.T58 {
        /*
            r4 = this;
            r1 = 0
            java.util.List r0 = r5.helpOptions()
            java.util.Iterator r2 = r0.iterator()
        L_0x0009:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0019
            java.lang.Object r0 = r2.next()
            com.fossil.P58 r0 = (com.fossil.P58) r0
            r0.clearValues()
            goto L_0x0009
        L_0x0019:
            r4.j(r5)
            com.fossil.I58 r0 = new com.fossil.I58
            r0.<init>()
            r4.a = r0
            if (r6 != 0) goto L_0x0027
            java.lang.String[] r6 = new java.lang.String[r1]
        L_0x0027:
            com.fossil.S58 r0 = r4.d()
            java.lang.String[] r0 = r4.c(r0, r6, r8)
            java.util.List r0 = java.util.Arrays.asList(r0)
            java.util.ListIterator r2 = r0.listIterator()
        L_0x0037:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x009e
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "--"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0068
        L_0x004b:
            r1 = 1
        L_0x004c:
            if (r1 == 0) goto L_0x0037
        L_0x004e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0037
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r3 = "--"
            boolean r3 = r3.equals(r0)
            if (r3 != 0) goto L_0x004e
            com.fossil.I58 r3 = r4.a
            r3.addArg(r0)
            goto L_0x004e
        L_0x0068:
            java.lang.String r3 = "-"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0078
            if (r8 != 0) goto L_0x004b
            com.fossil.I58 r3 = r4.a
            r3.addArg(r0)
            goto L_0x004c
        L_0x0078:
            java.lang.String r3 = "-"
            boolean r3 = r0.startsWith(r3)
            if (r3 == 0) goto L_0x0096
            if (r8 == 0) goto L_0x0092
            com.fossil.S58 r3 = r4.d()
            boolean r3 = r3.hasOption(r0)
            if (r3 != 0) goto L_0x0092
            com.fossil.I58 r1 = r4.a
            r1.addArg(r0)
            goto L_0x004b
        L_0x0092:
            r4.h(r0, r2)
            goto L_0x004c
        L_0x0096:
            com.fossil.I58 r3 = r4.a
            r3.addArg(r0)
            if (r8 == 0) goto L_0x004c
            goto L_0x004b
        L_0x009e:
            r4.i(r7)
            r4.b()
            com.fossil.I58 r0 = r4.a
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U58.f(com.fossil.S58, java.lang.String[], java.util.Properties, boolean):com.fossil.I58");
    }

    @DexIgnore
    public void g(P58 p58, ListIterator listIterator) throws T58 {
        while (true) {
            if (!listIterator.hasNext()) {
                break;
            }
            String str = (String) listIterator.next();
            if (d().hasOption(str) && str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
                listIterator.previous();
                break;
            } else {
                try {
                    p58.addValueForProcessing(Y58.a(str));
                } catch (RuntimeException e) {
                    listIterator.previous();
                }
            }
        }
        if (p58.getValues() == null && !p58.hasOptionalArg()) {
            throw new N58(p58);
        }
    }

    @DexIgnore
    public void h(String str, ListIterator listIterator) throws T58 {
        if (d().hasOption(str)) {
            P58 p58 = (P58) d().getOption(str).clone();
            if (p58.isRequired()) {
                e().remove(p58.getKey());
            }
            if (d().getOptionGroup(p58) != null) {
                Q58 optionGroup = d().getOptionGroup(p58);
                if (optionGroup.isRequired()) {
                    e().remove(optionGroup);
                }
                optionGroup.setSelected(p58);
            }
            if (p58.hasArg()) {
                g(p58, listIterator);
            }
            this.a.addOption(p58);
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Unrecognized option: ");
        stringBuffer.append(str);
        throw new X58(stringBuffer.toString(), str);
    }

    @DexIgnore
    public void i(Properties properties) {
        if (properties != null) {
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String obj = propertyNames.nextElement().toString();
                if (!this.a.hasOption(obj)) {
                    P58 option = d().getOption(obj);
                    String property = properties.getProperty(obj);
                    if (option.hasArg()) {
                        if (option.getValues() == null || option.getValues().length == 0) {
                            try {
                                option.addValueForProcessing(property);
                            } catch (RuntimeException e) {
                            }
                        }
                    } else if (!"yes".equalsIgnoreCase(property) && !ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equalsIgnoreCase(property) && !"1".equalsIgnoreCase(property)) {
                        return;
                    }
                    this.a.addOption(option);
                }
            }
        }
    }

    @DexIgnore
    public void j(S58 s58) {
        this.b = s58;
        this.c = new ArrayList(s58.getRequiredOptions());
    }
}
