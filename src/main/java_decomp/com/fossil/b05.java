package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<ArrayList<HybridPresetAppSetting>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<ArrayList<HybridPresetAppSetting>> {
    }

    @DexIgnore
    public final String a(ArrayList<HybridPresetAppSetting> arrayList) {
        Wg6.c(arrayList, "configurationList");
        if (arrayList.isEmpty()) {
            return "";
        }
        return new Gson().u(arrayList, new Ai().getType());
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> b(String str) {
        Wg6.c(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object l = new Gson().l(str, new Bi().getType());
        Wg6.b(l, "Gson().fromJson(data, type)");
        return (ArrayList) l;
    }
}
