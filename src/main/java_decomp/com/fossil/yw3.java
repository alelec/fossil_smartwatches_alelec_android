package com.fossil;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Property;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yw3 extends Property<Drawable, Integer> {
    @DexIgnore
    public static /* final */ Property<Drawable, Integer> b; // = new Yw3();
    @DexIgnore
    public /* final */ WeakHashMap<Drawable, Integer> a; // = new WeakHashMap<>();

    @DexIgnore
    public Yw3() {
        super(Integer.class, "drawableAlphaCompat");
    }

    @DexIgnore
    public Integer a(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 19) {
            return Integer.valueOf(drawable.getAlpha());
        }
        if (this.a.containsKey(drawable)) {
            return this.a.get(drawable);
        }
        return 255;
    }

    @DexIgnore
    public void b(Drawable drawable, Integer num) {
        if (Build.VERSION.SDK_INT < 19) {
            this.a.put(drawable, num);
        }
        drawable.setAlpha(num.intValue());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ Integer get(Drawable drawable) {
        return a(drawable);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ void set(Drawable drawable, Integer num) {
        b(drawable, num);
    }
}
