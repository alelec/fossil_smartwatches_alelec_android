package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface S28 {
    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void b(V18 v18) throws IOException;

    @DexIgnore
    W18 c(Response response) throws IOException;

    @DexIgnore
    Object cancel();  // void declaration

    @DexIgnore
    Response.a d(boolean z) throws IOException;

    @DexIgnore
    void e() throws IOException;

    @DexIgnore
    A58 f(V18 v18, long j);
}
