package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ng7 {
    @DexIgnore
    public static String l;
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public Uh7 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public Jg7 k; // = null;

    @DexIgnore
    public Ng7(Context context, int i2, Jg7 jg7) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = Fg7.v(context);
        this.h = Ei7.F(context);
        this.a = Fg7.s(context);
        if (jg7 != null) {
            this.k = jg7;
            if (Ei7.t(jg7.a())) {
                this.a = jg7.a();
            }
            if (Ei7.t(jg7.b())) {
                this.g = jg7.b();
            }
            if (Ei7.t(jg7.c())) {
                this.h = jg7.c();
            }
            this.i = jg7.d();
        }
        this.f = Fg7.u(context);
        this.d = Gh7.b(context).v(context);
        Og7 a2 = a();
        Og7 og7 = Og7.i;
        this.e = a2 != og7 ? Ei7.O(context).intValue() : -og7.a();
        if (!Se7.g(l)) {
            String w = Fg7.w(context);
            l = w;
            if (!Ei7.t(w)) {
                l = "0";
            }
        }
    }

    @DexIgnore
    public abstract Og7 a();

    @DexIgnore
    public abstract boolean b(JSONObject jSONObject);

    @DexIgnore
    public boolean c(JSONObject jSONObject) {
        try {
            Ji7.d(jSONObject, "ky", this.a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.c());
                Ji7.d(jSONObject, "mc", this.d.d());
                int e2 = this.d.e();
                jSONObject.put("ut", e2);
                if (e2 == 0 && Ei7.S(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            Ji7.d(jSONObject, "cui", this.f);
            if (a() != Og7.b) {
                Ji7.d(jSONObject, "av", this.h);
                Ji7.d(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            Ji7.d(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", Ei7.e(this.j, false));
            return b(jSONObject);
        } catch (Throwable th) {
            return false;
        }
    }

    @DexIgnore
    public long d() {
        return this.b;
    }

    @DexIgnore
    public Jg7 e() {
        return this.k;
    }

    @DexIgnore
    public Context f() {
        return this.j;
    }

    @DexIgnore
    public boolean g() {
        return this.i;
    }

    @DexIgnore
    public String h() {
        try {
            JSONObject jSONObject = new JSONObject();
            c(jSONObject);
            return jSONObject.toString();
        } catch (Throwable th) {
            return "";
        }
    }
}
