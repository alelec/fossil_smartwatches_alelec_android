package com.fossil;

import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z78 extends W78<Fragment> {
    @DexIgnore
    public Z78(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    @Override // com.fossil.Y78
    public boolean d(String str) {
        return ((Fragment) a()).shouldShowRequestPermissionRationale(str);
    }
}
