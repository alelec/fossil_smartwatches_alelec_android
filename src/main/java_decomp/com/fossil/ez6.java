package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Jn5;
import com.fossil.Lw5;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ez6 extends BaseFragment implements Dz6 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public boolean g;
    @DexIgnore
    public G37<Da5> h;
    @DexIgnore
    public Cz6 i;
    @DexIgnore
    public Lw5 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ez6.l;
        }

        @DexIgnore
        public final Ez6 b() {
            Bundle bundle = new Bundle();
            Ez6 ez6 = new Ez6();
            ez6.setArguments(bundle);
            return ez6;
        }

        @DexIgnore
        public final Ez6 c(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("PMS_SKIP_ABLE", z);
            Ez6 ez6 = new Ez6();
            ez6.setArguments(bundle);
            return ez6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ez6 b;

        @DexIgnore
        public Bi(Ez6 ez6) {
            this.b = ez6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ez6 b;

        @DexIgnore
        public Ci(Ez6 ez6) {
            this.b = ez6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Lw5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Ez6 a;

        @DexIgnore
        public Di(Ez6 ez6) {
            this.a = ez6;
        }

        @DexIgnore
        @Override // com.fossil.Lw5.Ai
        public void a(PermissionData permissionData) {
            Wg6.c(permissionData, "permissionData");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ez6.m.a();
            local.d(a2, "requestPermission " + permissionData);
            String type = permissionData.getType();
            int hashCode = type.hashCode();
            if (hashCode != -1168251815) {
                if (hashCode != -381825158 || !type.equals("PERMISSION_REQUEST_TYPE")) {
                    return;
                }
                if (!permissionData.getAndroidPermissionSet().isEmpty()) {
                    Ez6 ez6 = this.a;
                    Object[] array = permissionData.getAndroidPermissionSet().toArray(new String[0]);
                    if (array != null) {
                        ez6.requestPermissions((String[]) array, 111);
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
                Jn5.Ci settingPermissionId = permissionData.getSettingPermissionId();
                if (settingPermissionId != null) {
                    int i = Fz6.a[settingPermissionId.ordinal()];
                    if (i == 1) {
                        this.a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
                    } else if (i == 2) {
                        this.a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                    } else if (i == 3) {
                        this.a.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                    }
                }
            } else if (type.equals("PERMISSION_SETTING_TYPE")) {
                this.a.requireActivity().finish();
            }
        }
    }

    /*
    static {
        String simpleName = Ez6.class.getSimpleName();
        Wg6.b(simpleName, "PermissionFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        M6(2);
        return true;
    }

    @DexIgnore
    public void L6(Cz6 cz6) {
        Wg6.c(cz6, "presenter");
        this.i = cz6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Cz6 cz6) {
        L6(cz6);
    }

    @DexIgnore
    public void M6(int i2) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(i2);
            activity.finish();
        }
    }

    @DexIgnore
    public final void N6() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        StringBuilder sb = new StringBuilder();
        sb.append("package:");
        FragmentActivity activity = getActivity();
        sb.append(activity != null ? activity.getPackageName() : null);
        intent.setData(Uri.parse(sb.toString()));
        startActivity(intent);
    }

    @DexIgnore
    @Override // com.fossil.Dz6
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.Dz6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d5(java.util.List<com.portfolio.platform.data.model.PermissionData> r8) {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ez6.d5(java.util.List):void");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        G37<Da5> g37 = new G37<>(this, (Da5) Aq0.f(layoutInflater, 2131558606, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            Da5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Cz6 cz6 = this.i;
        if (cz6 != null) {
            cz6.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Rk0.Bi, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        Wg6.c(strArr, "permissions");
        Wg6.c(iArr, "grantResults");
        super.onRequestPermissionsResult(i2, strArr, iArr);
        Integer[] w = Dm7.w(iArr);
        int length = w.length;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            if (w[i3].intValue() == -1) {
                String str = strArr[i4];
                boolean shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale(str);
                Cz6 cz6 = this.i;
                if (cz6 != null) {
                    boolean n = cz6.n(str);
                    FLogger.INSTANCE.getLocal().d(l, "permission " + str + " is denied, isDenyOnly " + shouldShowRequestPermissionRationale + " isFirstTime " + n + ' ');
                    if (shouldShowRequestPermissionRationale) {
                        return;
                    }
                    if (!n) {
                        N6();
                        return;
                    }
                    Cz6 cz62 = this.i;
                    if (cz62 != null) {
                        cz62.o(str, false);
                        return;
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else {
                i3++;
                i4++;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(l, "onResume");
        Cz6 cz6 = this.i;
        if (cz6 != null) {
            cz6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ImageView imageView;
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            boolean z = arguments.getBoolean("PMS_SKIP_ABLE", false);
            this.g = z;
            if (z) {
                G37<Da5> g37 = this.h;
                if (g37 != null) {
                    Da5 a2 = g37.a();
                    if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                        flexibleButton2.setVisibility(0);
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            }
        }
        G37<Da5> g372 = this.h;
        if (g372 != null) {
            Da5 a3 = g372.a();
            if (!(a3 == null || (flexibleButton = a3.q) == null)) {
                flexibleButton.setOnClickListener(new Bi(this));
            }
            G37<Da5> g373 = this.h;
            if (g373 != null) {
                Da5 a4 = g373.a();
                if (a4 != null && (imageView = a4.s) != null) {
                    imageView.setOnClickListener(new Ci(this));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
