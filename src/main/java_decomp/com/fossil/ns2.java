package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ns2 extends As2 implements Ls2 {
    @DexIgnore
    public Ns2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final boolean I1(Ls2 ls2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, ls2);
        Parcel e = e(16, d);
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void K0() throws RemoteException {
        i(12, d());
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void X0(Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final int a() throws RemoteException {
        Parcel e = e(17, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void e2(String str) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final String getId() throws RemoteException {
        Parcel e = e(2, d());
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final LatLng getPosition() throws RemoteException {
        Parcel e = e(4, d());
        LatLng latLng = (LatLng) Es2.b(e, LatLng.CREATOR);
        e.recycle();
        return latLng;
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void remove() throws RemoteException {
        i(1, d());
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setAlpha(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(25, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setAnchor(float f, float f2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeFloat(f2);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setDraggable(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setFlat(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(20, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setInfoWindowAnchor(float f, float f2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeFloat(f2);
        i(24, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setPosition(LatLng latLng) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLng);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setRotation(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(22, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setTitle(String str) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setVisible(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(14, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void setZIndex(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(27, d);
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final void t() throws RemoteException {
        i(11, d());
    }

    @DexIgnore
    @Override // com.fossil.Ls2
    public final boolean y1() throws RemoteException {
        Parcel e = e(13, d());
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }
}
