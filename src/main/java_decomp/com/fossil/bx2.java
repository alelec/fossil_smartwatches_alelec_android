package com.fossil;

import java.io.Serializable;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bx2<T> implements Xw2<T>, Serializable {
    @DexIgnore
    @NullableDecl
    public /* final */ T zza;

    @DexIgnore
    public Bx2(@NullableDecl T t) {
        this.zza = t;
    }

    @DexIgnore
    public final boolean equals(@NullableDecl Object obj) {
        if (obj instanceof Bx2) {
            return Qw2.a(this.zza, ((Bx2) obj).zza);
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.zza});
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Suppliers.ofInstance(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.Xw2
    public final T zza() {
        return this.zza;
    }
}
