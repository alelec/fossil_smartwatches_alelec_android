package com.fossil;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zq2 extends Jb3 {
    @DexIgnore
    public /* final */ P72<Yv2> b;

    @DexIgnore
    public Zq2(P72<Yv2> p72) {
        this.b = p72;
    }

    @DexIgnore
    @Override // com.fossil.Ib3
    public final void S1(LocationResult locationResult) {
        this.b.c(new Ar2(this, locationResult));
    }

    @DexIgnore
    public final void i() {
        synchronized (this) {
            this.b.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ib3
    public final void j1(LocationAvailability locationAvailability) {
        this.b.c(new Br2(this, locationAvailability));
    }
}
