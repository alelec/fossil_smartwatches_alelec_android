package com.fossil;

import android.app.Dialog;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ze0 extends Kq0 {
    @DexIgnore
    @Override // com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        return new Ye0(getContext(), getTheme());
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void setupDialog(Dialog dialog, int i) {
        if (dialog instanceof Ye0) {
            Ye0 ye0 = (Ye0) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            ye0.d(1);
            return;
        }
        super.setupDialog(dialog, i);
    }
}
