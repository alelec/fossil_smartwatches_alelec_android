package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zn6 extends Fq4 {
    @DexIgnore
    public abstract void n(boolean z);

    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE o();

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q();

    @DexIgnore
    public abstract void r(Intent intent);

    @DexIgnore
    public abstract void s();
}
