package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pv4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<Boolean> f2874a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<a> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Integer> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ hu4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public boolean f2875a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(boolean z, boolean z2) {
            this.f2875a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.f2875a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f2875a == aVar.f2875a && this.b == aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.f2875a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (i2 * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Tab(shouldGoToCreatedPage=" + this.f2875a + ", shouldGoToTabs=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel", f = "BCMainViewModel.kt", l = {54}, m = "checkOnServer")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(pv4 pv4, qn7 qn7) {
            super(qn7);
            this.this$0 = pv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkOnServer$result$1", f = "BCMainViewModel.kt", l = {54}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super kz4<it4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pv4 pv4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<it4>> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                hu4 hu4 = this.this$0.d;
                this.L$0 = iv7;
                this.label = 1;
                Object c = hu4.c(this);
                return c == d ? d : c;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1", f = "BCMainViewModel.kt", l = {36, 41}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pv4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1$socialProfile$1", f = "BCMainViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super it4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super it4> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.d.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pv4 pv4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x003a
                if (r0 == r4) goto L_0x0025
                if (r0 != r5) goto L_0x001d
                java.lang.Object r0 = r7.L$1
                com.fossil.it4 r0 = (com.fossil.it4) r0
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x001a:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0025:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r2 = r0
                r1 = r8
            L_0x002e:
                r0 = r1
                com.fossil.it4 r0 = (com.fossil.it4) r0
                if (r0 == 0) goto L_0x0054
                com.fossil.pv4 r0 = r7.this$0
                r1 = 0
                com.fossil.pv4.f(r0, r4, r1, r5, r6)
                goto L_0x001a
            L_0x003a:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.dv7 r1 = com.fossil.bw7.b()
                com.fossil.pv4$d$a r2 = new com.fossil.pv4$d$a
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0064
                r0 = r3
                goto L_0x001c
            L_0x0054:
                com.fossil.pv4 r1 = r7.this$0
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = r1.c(r7)
                if (r0 != r3) goto L_0x001a
                r0 = r3
                goto L_0x001c
            L_0x0064:
                r2 = r0
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pv4.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$retry$1", f = "BCMainViewModel.kt", l = {49}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(pv4 pv4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.f2874a.l(ao7.a(true));
                pv4 pv4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (pv4.c(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        pq7.b(pv4.class.getName(), "BCMainViewModel::class.java.name");
    }
    */

    @DexIgnore
    public pv4(hu4 hu4) {
        pq7.c(hu4, "socialProfileRepository");
        this.d = hu4;
    }

    @DexIgnore
    public static /* synthetic */ void f(pv4 pv4, boolean z, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        if ((i & 2) != 0) {
            z2 = false;
        }
        pv4.e(z, z2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c(com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r7 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 0
            r2 = 0
            r5 = 1
            boolean r0 = r8 instanceof com.fossil.pv4.b
            if (r0 == 0) goto L_0x0039
            r0 = r8
            com.fossil.pv4$b r0 = (com.fossil.pv4.b) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0039
            int r1 = r1 + r4
            r0.label = r1
        L_0x0015:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0047
            if (r4 != r5) goto L_0x003f
            java.lang.Object r0 = r0.L$0
            com.fossil.pv4 r0 = (com.fossil.pv4) r0
            com.fossil.el7.b(r1)
            r7 = r0
        L_0x0029:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r1 = r0.c()
            if (r1 == 0) goto L_0x005f
            r0 = 2
            f(r7, r5, r6, r0, r2)
        L_0x0036:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0038:
            return r0
        L_0x0039:
            com.fossil.pv4$b r0 = new com.fossil.pv4$b
            r0.<init>(r7, r8)
            goto L_0x0015
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.pv4$c r4 = new com.fossil.pv4$c
            r4.<init>(r7, r2)
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0029
            r0 = r3
            goto L_0x0038
        L_0x005f:
            com.portfolio.platform.data.model.ServerError r1 = r0.a()
            if (r1 == 0) goto L_0x0084
            java.lang.Integer r1 = r1.getCode()
        L_0x0069:
            if (r1 != 0) goto L_0x0086
        L_0x006b:
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r7.f2874a
            java.lang.Boolean r3 = com.fossil.ao7.a(r6)
            r1.l(r3)
            androidx.lifecycle.MutableLiveData<java.lang.Integer> r1 = r7.c
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            if (r0 == 0) goto L_0x0092
            java.lang.Integer r0 = r0.getCode()
        L_0x0080:
            r1.l(r0)
            goto L_0x0036
        L_0x0084:
            r1 = r2
            goto L_0x0069
        L_0x0086:
            r3 = 404(0x194, float:5.66E-43)
            int r1 = r1.intValue()
            if (r3 != r1) goto L_0x006b
            f(r7, r6, r5, r5, r2)
            goto L_0x0036
        L_0x0092:
            r0 = r2
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pv4.c(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void d() {
        this.f2874a.l(Boolean.TRUE);
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final void e(boolean z, boolean z2) {
        this.b.l(new a(z2, z));
    }

    @DexIgnore
    public final LiveData<Boolean> g() {
        return this.f2874a;
    }

    @DexIgnore
    public final LiveData<Integer> h() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<a> i() {
        return this.b;
    }

    @DexIgnore
    public final void j() {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }
}
