package com.fossil;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface E88<F, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public static Type a(int i, ParameterizedType parameterizedType) {
            return U88.h(i, parameterizedType);
        }

        @DexIgnore
        public static Class<?> b(Type type) {
            return U88.i(type);
        }

        @DexIgnore
        public E88<?, RequestBody> c(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
            return null;
        }

        @DexIgnore
        public E88<W18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
            return null;
        }

        @DexIgnore
        public E88<?, String> e(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
            return null;
        }
    }

    @DexIgnore
    T a(F f) throws IOException;
}
