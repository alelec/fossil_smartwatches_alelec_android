package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.S87;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S97 extends BaseFragment {
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public WatchFaceStickerViewModel h;
    @DexIgnore
    public Zg5 i;
    @DexIgnore
    public W97 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<S87.Bi, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ S97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(S97 s97) {
            super(1);
            this.this$0 = s97;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(S87.Bi bi) {
            invoke(bi);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(S87.Bi bi) {
            Wg6.c(bi, "it");
            Gc7 e = Hc7.c.e(this.this$0);
            if (e != null) {
                e.a(bi);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ S97 a;

        @DexIgnore
        public Bi(S97 s97) {
            this.a = s97;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            S97.K6(this.a).j(t);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ S97 a;

        @DexIgnore
        public Ci(S97 s97) {
            this.a = s97;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            Boolean bool = (Boolean) t.a();
            if (bool != null) {
                bool.booleanValue();
                S97.K6(this.a).notifyDataSetChanged();
                Gc7 e = Hc7.c.e(this.a);
                if (e != null) {
                    e.F();
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ W97 K6(S97 s97) {
        W97 w97 = s97.j;
        if (w97 != null) {
            return w97;
        }
        Wg6.n("adapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceStickerFragment";
    }

    @DexIgnore
    public final void L6() {
        this.j = new W97(new Ai(this));
        Zg5 zg5 = this.i;
        if (zg5 != null) {
            RecyclerView recyclerView = zg5.r;
            Wg6.b(recyclerView, "binding.rvStickerPreview");
            W97 w97 = this.j;
            if (w97 != null) {
                recyclerView.setAdapter(w97);
            } else {
                Wg6.n("adapter");
                throw null;
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void M6() {
        WatchFaceStickerViewModel watchFaceStickerViewModel = this.h;
        if (watchFaceStickerViewModel != null) {
            LiveData<List<S87.Bi>> t = watchFaceStickerViewModel.t();
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
            t.h(viewLifecycleOwner, new Bi(this));
            WatchFaceStickerViewModel watchFaceStickerViewModel2 = this.h;
            if (watchFaceStickerViewModel2 != null) {
                LiveData<U37<Boolean>> s = watchFaceStickerViewModel2.s();
                LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                Wg6.b(viewLifecycleOwner2, "viewLifecycleOwner");
                s.h(viewLifecycleOwner2, new Ci(this));
                return;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().d1().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            Po4 po4 = this.g;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(watchFaceEditActivity, po4).a(WatchFaceStickerViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026kerViewModel::class.java)");
                this.h = (WatchFaceStickerViewModel) a2;
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    Zg5 zg5 = this.i;
                    if (zg5 != null) {
                        zg5.q.setBackgroundColor(Color.parseColor(d));
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
                L6();
                M6();
                return;
            }
            Wg6.n("viewModelFactory");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Zg5 z = Zg5.z(layoutInflater, viewGroup, false);
        Wg6.b(z, "WatchFaceStickerFragment\u2026flater, container, false)");
        this.i = z;
        if (z != null) {
            View n = z.n();
            Wg6.b(n, "binding.root");
            return n;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
