package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La0 implements Parcelable.Creator<Ma0> {
    @DexIgnore
    public /* synthetic */ La0(Qg6 qg6) {
    }

    @DexIgnore
    public Ma0 a(Parcel parcel) {
        return new Ma0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ma0 createFromParcel(Parcel parcel) {
        return new Ma0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ma0[] newArray(int i) {
        return new Ma0[i];
    }
}
