package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.os.Build;
import com.fossil.P18;
import com.fossil.W71;
import com.fossil.X71;
import com.fossil.Y71;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Y71<T extends Y71<T>> {
    @DexIgnore
    public Object a;
    @DexIgnore
    public String b;
    @DexIgnore
    public List<String> c;
    @DexIgnore
    public X71.Ai d;
    @DexIgnore
    public G81 e;
    @DexIgnore
    public E81 f;
    @DexIgnore
    public D81 g;
    @DexIgnore
    public U51 h;
    @DexIgnore
    public Dv7 i;
    @DexIgnore
    public List<? extends M81> j;
    @DexIgnore
    public Bitmap.Config k;
    @DexIgnore
    public ColorSpace l;
    @DexIgnore
    public P18.Ai m;
    @DexIgnore
    public W71.Ai n;
    @DexIgnore
    public S71 o;
    @DexIgnore
    public S71 p;
    @DexIgnore
    public S71 q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;

    @DexIgnore
    public Y71(Z41 z41) {
        this.a = null;
        this.b = null;
        this.c = Hm7.e();
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = z41.g();
        this.h = null;
        this.i = z41.c();
        this.j = Hm7.e();
        this.k = Y81.a.e();
        if (Build.VERSION.SDK_INT >= 26) {
            this.l = null;
        }
        this.m = null;
        this.n = null;
        S71 s71 = S71.ENABLED;
        this.o = s71;
        this.p = s71;
        this.q = s71;
        this.r = z41.a();
        this.s = z41.b();
    }

    @DexIgnore
    public /* synthetic */ Y71(Z41 z41, Qg6 qg6) {
        this(z41);
    }

    @DexIgnore
    public final List<String> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.r;
    }

    @DexIgnore
    public final boolean c() {
        return this.s;
    }

    @DexIgnore
    public final Bitmap.Config d() {
        return this.k;
    }

    @DexIgnore
    public final ColorSpace e() {
        return this.l;
    }

    @DexIgnore
    public final Object f() {
        return this.a;
    }

    @DexIgnore
    public final U51 g() {
        return this.h;
    }

    @DexIgnore
    public final S71 h() {
        return this.p;
    }

    @DexIgnore
    public final Dv7 i() {
        return this.i;
    }

    @DexIgnore
    public final P18.Ai j() {
        return this.m;
    }

    @DexIgnore
    public final String k() {
        return this.b;
    }

    @DexIgnore
    public final X71.Ai l() {
        return this.d;
    }

    @DexIgnore
    public final S71 m() {
        return this.q;
    }

    @DexIgnore
    public final S71 n() {
        return this.o;
    }

    @DexIgnore
    public final W71.Ai o() {
        return this.n;
    }

    @DexIgnore
    public final D81 p() {
        return this.g;
    }

    @DexIgnore
    public final E81 q() {
        return this.f;
    }

    @DexIgnore
    public final G81 r() {
        return this.e;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.M81>, java.util.List<com.fossil.M81> */
    public final List<M81> s() {
        return this.j;
    }

    @DexIgnore
    public final T t(X71.Ai ai) {
        this.d = ai;
        return this;
    }

    @DexIgnore
    public final void u(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public final T v(M81... m81Arr) {
        Wg6.c(m81Arr, "transformations");
        this.j = Em7.d0(m81Arr);
        return this;
    }
}
