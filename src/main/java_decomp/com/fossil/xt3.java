package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xt3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Ut3 c;

    @DexIgnore
    public Xt3(Ut3 ut3, Nt3 nt3) {
        this.c = ut3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        try {
            Nt3 nt3 = (Nt3) this.c.b.then(this.b);
            if (nt3 == null) {
                this.c.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            nt3.g(Pt3.b, this.c);
            nt3.e(Pt3.b, this.c);
            nt3.a(Pt3.b, this.c);
        } catch (Lt3 e) {
            if (e.getCause() instanceof Exception) {
                this.c.c.t((Exception) e.getCause());
            } else {
                this.c.c.t(e);
            }
        } catch (Exception e2) {
            this.c.c.t(e2);
        }
    }
}
