package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Js extends Ss {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    public Js(int i, K5 k5) {
        super(Hs.g, k5);
        this.B = i;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.k1, Integer.valueOf(this.A));
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new H6(this.B, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void g(U5 u5) {
        this.A = ((H6) u5).k;
        this.g.add(new Hw(0, null, null, G80.k(new JSONObject(), Jd0.k1, Integer.valueOf(this.A)), 7));
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.j1, Integer.valueOf(this.B));
    }
}
