package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bs b;
    @DexIgnore
    public /* final */ /* synthetic */ U5 c;

    @DexIgnore
    public W7(Bs bs, U5 u5) {
        this.b = bs;
        this.c = u5;
    }

    @DexIgnore
    public final void run() {
        Bs bs = this.b;
        U5 u5 = this.c;
        if (Wg6.a(bs.a.k, u5)) {
            bs.a.k = null;
            bs.a.r(u5);
        }
    }
}
