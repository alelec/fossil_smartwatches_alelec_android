package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kg4 implements ServiceConnection {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ ScheduledExecutorService c;
    @DexIgnore
    public /* final */ Queue<Ai> d;
    @DexIgnore
    public Hg4 e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ Ot3<Void> b; // = new Ot3<>();

        @DexIgnore
        public Ai(Intent intent) {
            this.a = intent;
        }

        @DexIgnore
        public void a(ScheduledExecutorService scheduledExecutorService) {
            c().c(scheduledExecutorService, new Jg4(scheduledExecutorService.schedule(new Ig4(this), 9000, TimeUnit.MILLISECONDS)));
        }

        @DexIgnore
        public void b() {
            this.b.e(null);
        }

        @DexIgnore
        public Nt3<Void> c() {
            return this.b.a();
        }

        @DexIgnore
        public final /* synthetic */ void d() {
            String action = this.a.getAction();
            StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
            sb.append("Service took too long to process intent: ");
            sb.append(action);
            sb.append(" App may get closed.");
            Log.w("FirebaseInstanceId", sb.toString());
            b();
        }
    }

    @DexIgnore
    public Kg4(Context context, String str) {
        this(context, str, new ScheduledThreadPoolExecutor(0, new Sf2("Firebase-FirebaseInstanceIdServiceConnection")));
    }

    @DexIgnore
    public Kg4(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.d = new ArrayDeque();
        this.f = false;
        this.a = context.getApplicationContext();
        this.b = new Intent(str).setPackage(this.a.getPackageName());
        this.c = scheduledExecutorService;
    }

    @DexIgnore
    public final void a() {
        while (!this.d.isEmpty()) {
            this.d.poll().b();
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (this) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "flush queue called");
            }
            while (!this.d.isEmpty()) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "found intent to be delivered");
                }
                if (this.e == null || !this.e.isBinderAlive()) {
                    d();
                    return;
                }
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "binder is alive, sending the intent.");
                }
                this.e.b(this.d.poll());
            }
        }
    }

    @DexIgnore
    public Nt3<Void> c(Intent intent) {
        Nt3<Void> c2;
        synchronized (this) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "new intent queued in the bind-strategy delivery");
            }
            Ai ai = new Ai(intent);
            ai.a(this.c);
            this.d.add(ai);
            b();
            c2 = ai.c();
        }
        return c2;
    }

    @DexIgnore
    public final void d() {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            boolean z = this.f;
            StringBuilder sb = new StringBuilder(39);
            sb.append("binder is dead. start connection? ");
            sb.append(!z);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        if (!this.f) {
            this.f = true;
            try {
                if (!Ve2.b().a(this.a, this.b, this, 65)) {
                    Log.e("FirebaseInstanceId", "binding to the service failed");
                    this.f = false;
                    a();
                }
            } catch (SecurityException e2) {
                Log.e("FirebaseInstanceId", "Exception while binding the service", e2);
            }
        }
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(componentName);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
                sb.append("onServiceConnected: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            this.f = false;
            if (!(iBinder instanceof Hg4)) {
                String valueOf2 = String.valueOf(iBinder);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 28);
                sb2.append("Invalid service connection: ");
                sb2.append(valueOf2);
                Log.e("FirebaseInstanceId", sb2.toString());
                a();
                return;
            }
            this.e = (Hg4) iBinder;
            b();
        }
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        b();
    }
}
