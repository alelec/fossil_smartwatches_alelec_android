package com.fossil;

import com.fossil.A34;
import com.fossil.F34;
import com.fossil.H34;
import com.fossil.M34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import com.google.j2objc.annotations.RetainedWith;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I34<K, V> extends F34<K, V> implements W44<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient H34<V> g;
    @DexIgnore
    @RetainedWith
    @LazyInit
    public transient I34<V, K> h;
    @DexIgnore
    public transient H34<Map.Entry<K, V>> i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> extends F34.Ci<K, V> {
        @DexIgnore
        public Ai() {
            super(A44.a().d().c());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.F34.Ci
        public /* bridge */ /* synthetic */ F34.Ci b(Object obj, Object obj2) {
            f(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.F34.Ci
        public /* bridge */ /* synthetic */ F34.Ci c(Map.Entry entry) {
            g(entry);
            return this;
        }

        @DexIgnore
        public I34<K, V> e() {
            if (this.b != null) {
                W44<K, V> c = A44.a().d().c();
                for (Map.Entry entry : I44.from(this.b).onKeys().immutableSortedCopy(this.a.asMap().entrySet())) {
                    c.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                this.a = c;
            }
            return I34.a(this.a, this.c);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> f(K k, V v) {
            Y34<K, V> y34 = this.a;
            I14.l(k);
            I14.l(v);
            y34.put(k, v);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Y34<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public Ai<K, V> g(Map.Entry<? extends K, ? extends V> entry) {
            Object key = entry.getKey();
            I14.l(key);
            Object value = entry.getValue();
            I14.l(value);
            this.a.put(key, value);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> h(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.d(iterable);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<K, V> extends H34<Map.Entry<K, V>> {
        @DexIgnore
        @Weak
        public /* final */ transient I34<K, V> c;

        @DexIgnore
        public Bi(I34<K, V> i34) {
            this.c = i34;
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.c.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
        public H54<Map.Entry<K, V>> iterator() {
            return this.c.entryIterator();
        }

        @DexIgnore
        public int size() {
            return this.c.size();
        }
    }

    @DexIgnore
    public I34(A34<K, H34<V>> a34, int i2, Comparator<? super V> comparator) {
        super(a34, i2);
        this.g = b(comparator);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.A34$Bi */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> I34<K, V> a(Y34<? extends K, ? extends V> y34, Comparator<? super V> comparator) {
        I14.l(y34);
        if (y34.isEmpty() && comparator == null) {
            return of();
        }
        if (y34 instanceof I34) {
            I34<K, V> i34 = (I34) y34;
            if (!i34.isPartialView()) {
                return i34;
            }
        }
        A34.Bi bi = new A34.Bi(y34.asMap().size());
        int i2 = 0;
        for (Map.Entry<? extends K, Collection<? extends V>> entry : y34.asMap().entrySet()) {
            Object key = entry.getKey();
            H34 d = d(comparator, entry.getValue());
            if (!d.isEmpty()) {
                bi.c(key, d);
                i2 = d.size() + i2;
            }
        }
        return new I34<>(bi.a(), i2, comparator);
    }

    @DexIgnore
    public static <V> H34<V> b(Comparator<? super V> comparator) {
        return comparator == null ? H34.of() : M34.emptySet(comparator);
    }

    @DexIgnore
    public static <K, V> Ai<K, V> builder() {
        return new Ai<>();
    }

    @DexIgnore
    public static <K, V> I34<K, V> copyOf(Y34<? extends K, ? extends V> y34) {
        return a(y34, null);
    }

    @DexIgnore
    public static <K, V> I34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Ai ai = new Ai();
        ai.h(iterable);
        return ai.e();
    }

    @DexIgnore
    public static <V> H34<V> d(Comparator<? super V> comparator, Collection<? extends V> collection) {
        return comparator == null ? H34.copyOf((Collection) collection) : M34.copyOf((Comparator) comparator, (Collection) collection);
    }

    @DexIgnore
    public static <V> H34.Ai<V> e(Comparator<? super V> comparator) {
        return comparator == null ? new H34.Ai<>() : new M34.Ai(comparator);
    }

    @DexIgnore
    public static <K, V> I34<K, V> of() {
        return H24.INSTANCE;
    }

    @DexIgnore
    public static <K, V> I34<K, V> of(K k, V v) {
        Ai builder = builder();
        builder.f(k, v);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> I34<K, V> of(K k, V v, K k2, V v2) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> I34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> I34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        return builder.e();
    }

    @DexIgnore
    public static <K, V> I34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        Ai builder = builder();
        builder.f(k, v);
        builder.f(k2, v2);
        builder.f(k3, v3);
        builder.f(k4, v4);
        builder.f(k5, v5);
        return builder.e();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.fossil.A34$Bi */
    /* JADX WARN: Multi-variable type inference failed */
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        Comparator comparator = (Comparator) objectInputStream.readObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            A34.Bi builder = A34.builder();
            int i2 = 0;
            int i3 = 0;
            while (i3 < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    H34.Ai e = e(comparator);
                    for (int i4 = 0; i4 < readInt2; i4++) {
                        e.g(objectInputStream.readObject());
                    }
                    H34 j = e.j();
                    if (j.size() == readInt2) {
                        builder.c(readObject, j);
                        i3++;
                        i2 += readInt2;
                    } else {
                        throw new InvalidObjectException("Duplicate key-value pairs exist for key " + readObject);
                    }
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                F34.Ei.a.b(this, builder.a());
                F34.Ei.b.a(this, i2);
                F34.Ei.c.b(this, b(comparator));
            } catch (IllegalArgumentException e2) {
                throw ((InvalidObjectException) new InvalidObjectException(e2.getMessage()).initCause(e2));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(valueComparator());
        V44.d(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.I34<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.I34$Ai */
    /* JADX WARN: Multi-variable type inference failed */
    public final I34<V, K> c() {
        Ai builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.f(entry.getValue(), entry.getKey());
        }
        I34<V, K> e = builder.e();
        e.h = this;
        return e;
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34, com.fossil.U14, com.fossil.Y34
    public H34<Map.Entry<K, V>> entries() {
        H34<Map.Entry<K, V>> h34 = this.i;
        if (h34 != null) {
            return h34;
        }
        Bi bi = new Bi(this);
        this.i = bi;
        return bi;
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34, com.fossil.Y34
    public H34<V> get(K k) {
        return (H34) E14.a((H34) this.map.get(k), this.g);
    }

    @DexIgnore
    @Override // com.fossil.F34
    public I34<V, K> inverse() {
        I34<V, K> i34 = this.h;
        if (i34 != null) {
            return i34;
        }
        I34<V, K> c = c();
        this.h = c;
        return c;
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34
    @CanIgnoreReturnValue
    @Deprecated
    public H34<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.F34, com.fossil.F34, com.fossil.U14
    @CanIgnoreReturnValue
    @Deprecated
    public H34<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Comparator<? super V> valueComparator() {
        H34<V> h34 = this.g;
        if (h34 instanceof M34) {
            return ((M34) h34).comparator();
        }
        return null;
    }
}
