package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.Xq0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class Jq0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Jq0> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ ArrayList<String> c;
    @DexIgnore
    public /* final */ int[] d;
    @DexIgnore
    public /* final */ int[] e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ CharSequence j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ CharSequence l;
    @DexIgnore
    public /* final */ ArrayList<String> m;
    @DexIgnore
    public /* final */ ArrayList<String> s;
    @DexIgnore
    public /* final */ boolean t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Jq0> {
        @DexIgnore
        public Jq0 a(Parcel parcel) {
            return new Jq0(parcel);
        }

        @DexIgnore
        public Jq0[] b(int i) {
            return new Jq0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Jq0 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Jq0[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Jq0(Parcel parcel) {
        this.b = parcel.createIntArray();
        this.c = parcel.createStringArrayList();
        this.d = parcel.createIntArray();
        this.e = parcel.createIntArray();
        this.f = parcel.readInt();
        this.g = parcel.readString();
        this.h = parcel.readInt();
        this.i = parcel.readInt();
        this.j = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.k = parcel.readInt();
        this.l = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.m = parcel.createStringArrayList();
        this.s = parcel.createStringArrayList();
        this.t = parcel.readInt() != 0;
    }

    @DexIgnore
    public Jq0(Iq0 iq0) {
        int size = iq0.a.size();
        this.b = new int[(size * 5)];
        if (iq0.g) {
            this.c = new ArrayList<>(size);
            this.d = new int[size];
            this.e = new int[size];
            int i2 = 0;
            int i3 = 0;
            while (i3 < size) {
                Xq0.Ai ai = iq0.a.get(i3);
                int i4 = i2 + 1;
                this.b[i2] = ai.a;
                ArrayList<String> arrayList = this.c;
                Fragment fragment = ai.b;
                arrayList.add(fragment != null ? fragment.mWho : null);
                int[] iArr = this.b;
                int i5 = i4 + 1;
                iArr[i4] = ai.c;
                int i6 = i5 + 1;
                iArr[i5] = ai.d;
                int i7 = i6 + 1;
                iArr[i6] = ai.e;
                iArr[i7] = ai.f;
                this.d[i3] = ai.g.ordinal();
                this.e[i3] = ai.h.ordinal();
                i3++;
                i2 = i7 + 1;
            }
            this.f = iq0.f;
            this.g = iq0.i;
            this.h = iq0.t;
            this.i = iq0.j;
            this.j = iq0.k;
            this.k = iq0.l;
            this.l = iq0.m;
            this.m = iq0.n;
            this.s = iq0.o;
            this.t = iq0.p;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    @DexIgnore
    public Iq0 a(FragmentManager fragmentManager) {
        Iq0 iq0 = new Iq0(fragmentManager);
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.b.length) {
            Xq0.Ai ai = new Xq0.Ai();
            int i4 = i3 + 1;
            ai.a = this.b[i3];
            if (FragmentManager.s0(2)) {
                Log.v("FragmentManager", "Instantiate " + iq0 + " op #" + i2 + " base fragment #" + this.b[i4]);
            }
            String str = this.c.get(i2);
            if (str != null) {
                ai.b = fragmentManager.X(str);
            } else {
                ai.b = null;
            }
            ai.g = Lifecycle.State.values()[this.d[i2]];
            ai.h = Lifecycle.State.values()[this.e[i2]];
            int[] iArr = this.b;
            int i5 = i4 + 1;
            int i6 = iArr[i4];
            ai.c = i6;
            int i7 = i5 + 1;
            int i8 = iArr[i5];
            ai.d = i8;
            int i9 = i7 + 1;
            int i10 = iArr[i7];
            ai.e = i10;
            int i11 = iArr[i9];
            ai.f = i11;
            iq0.b = i6;
            iq0.c = i8;
            iq0.d = i10;
            iq0.e = i11;
            iq0.e(ai);
            i3 = i9 + 1;
            i2++;
        }
        iq0.f = this.f;
        iq0.i = this.g;
        iq0.t = this.h;
        iq0.g = true;
        iq0.j = this.i;
        iq0.k = this.j;
        iq0.l = this.k;
        iq0.m = this.l;
        iq0.n = this.m;
        iq0.o = this.s;
        iq0.p = this.t;
        iq0.w(1);
        return iq0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.b);
        parcel.writeStringList(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeIntArray(this.e);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
        TextUtils.writeToParcel(this.j, parcel, 0);
        parcel.writeInt(this.k);
        TextUtils.writeToParcel(this.l, parcel, 0);
        parcel.writeStringList(this.m);
        parcel.writeStringList(this.s);
        parcel.writeInt(this.t ? 1 : 0);
    }
}
