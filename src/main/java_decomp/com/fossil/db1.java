package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Db1 {
    @DexIgnore
    public int[] a; // = null;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public Cb1 d;
    @DexIgnore
    public /* final */ List<Cb1> e; // = new ArrayList();
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;

    @DexIgnore
    public int a() {
        return this.g;
    }

    @DexIgnore
    public int b() {
        return this.c;
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public int d() {
        return this.f;
    }
}
