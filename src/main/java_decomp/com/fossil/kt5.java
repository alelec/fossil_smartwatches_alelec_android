package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kt5 implements Factory<SetActivityGoalUserCase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<UserSettingDao> c;

    @DexIgnore
    public Kt5(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Kt5 a(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        return new Kt5(provider, provider2, provider3);
    }

    @DexIgnore
    public static SetActivityGoalUserCase c(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        return new SetActivityGoalUserCase(portfolioApp, userRepository, userSettingDao);
    }

    @DexIgnore
    public SetActivityGoalUserCase b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
