package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.O71;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N71 implements O71.Bi {
    @DexIgnore
    public /* final */ O71 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public N71(Context context) {
        Wg6.c(context, "context");
        O71 a2 = O71.a.a(context, this);
        this.a = a2;
        this.b = a2.a();
        c();
        this.a.start();
    }

    @DexIgnore
    @Override // com.fossil.O71.Bi
    public void a(boolean z) {
        this.b = z;
        c();
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final void c() {
        if (Q81.c.a() && Q81.c.b() <= 4) {
            Log.println(4, "NetworkObserver", this.b ? "ONLINE" : "OFFLINE");
        }
    }

    @DexIgnore
    public final void d() {
        if (!this.c) {
            this.c = true;
            this.a.stop();
        }
    }
}
