package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hd extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Zk1 zk1;
        String str = null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == -343131398 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED")) {
            E60 e60 = (E60) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            Q40.Di di = (Q40.Di) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE");
            Q40.Di di2 = (Q40.Di) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE");
            M80 m80 = M80.c;
            Zw zw = Zw.i;
            if (!(e60 == null || (zk1 = e60.u) == null)) {
                str = zk1.getMacAddress();
            }
            m80.a("ConnectionManager", "deviceHIDStateChangeReceiver: device=%s, previousState=%s, newState=%s.", str, di, di2);
            if (e60 != null && di != null && di2 != null) {
                Zw.i.d(e60, di2);
            }
        }
    }
}
