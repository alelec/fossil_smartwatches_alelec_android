package com.fossil;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q88<T> {
    @DexIgnore
    public /* final */ Response a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ W18 c;

    @DexIgnore
    public Q88(Response response, T t, W18 w18) {
        this.a = response;
        this.b = t;
        this.c = w18;
    }

    @DexIgnore
    public static <T> Q88<T> c(W18 w18, Response response) {
        U88.b(w18, "body == null");
        U88.b(response, "rawResponse == null");
        if (!response.m()) {
            return new Q88<>(response, null, w18);
        }
        throw new IllegalArgumentException("rawResponse should not be successful response");
    }

    @DexIgnore
    public static <T> Q88<T> h(T t, Response response) {
        U88.b(response, "rawResponse == null");
        if (response.m()) {
            return new Q88<>(response, t, null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    @DexIgnore
    public T a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a.f();
    }

    @DexIgnore
    public W18 d() {
        return this.c;
    }

    @DexIgnore
    public boolean e() {
        return this.a.m();
    }

    @DexIgnore
    public String f() {
        return this.a.o();
    }

    @DexIgnore
    public Response g() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }
}
