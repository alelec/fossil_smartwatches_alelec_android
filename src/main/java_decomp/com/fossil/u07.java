package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U07 extends BaseFragment implements T07 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ String m; // = m;
    @DexIgnore
    public static /* final */ String s; // = s;
    @DexIgnore
    public static /* final */ String t; // = t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public Tb5 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return U07.m;
        }

        @DexIgnore
        public final String b() {
            return U07.t;
        }

        @DexIgnore
        public final String c() {
            return U07.s;
        }

        @DexIgnore
        public final String d() {
            return U07.l;
        }

        @DexIgnore
        public final U07 e(boolean z, boolean z2, boolean z3) {
            U07 u07 = new U07();
            Bundle bundle = new Bundle();
            bundle.putBoolean(a(), z);
            bundle.putBoolean(c(), z2);
            bundle.putBoolean(b(), z3);
            u07.setArguments(bundle);
            return u07;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ U07 b;

        @DexIgnore
        public Bi(U07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.i) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Context context = this.b.getContext();
                if (context != null) {
                    Wg6.b(context, "context!!");
                    aVar.a(context, this.b.j, true);
                    return;
                }
                Wg6.i();
                throw null;
            }
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ U07 b;

        @DexIgnore
        public Ci(U07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ U07 b;

        @DexIgnore
        public Di(U07 u07) {
            this.b = u07;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wr4.a.a().k(this.b);
        }
    }

    /*
    static {
        String simpleName = U07.class.getSimpleName();
        Wg6.b(simpleName, "TroubleshootingFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return l;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(S07 s07) {
        Q6(s07);
    }

    @DexIgnore
    public void Q6(S07 s07) {
        Wg6.c(s07, "presenter");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Tb5 c = Tb5.c(getLayoutInflater());
        Wg6.b(c, "FragmentTroubleshootingB\u2026g.inflate(layoutInflater)");
        this.g = c;
        if (c != null) {
            return c.b();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        boolean z = false;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.h = arguments != null ? arguments.getBoolean(m) : false;
        Bundle arguments2 = getArguments();
        this.i = arguments2 != null ? arguments2.getBoolean(s) : false;
        Bundle arguments3 = getArguments();
        if (arguments3 != null) {
            z = arguments3.getBoolean(t);
        }
        this.j = z;
        Tb5 tb5 = this.g;
        if (tb5 != null) {
            LinearLayout linearLayout = tb5.e.b;
            Wg6.b(linearLayout, "mBinding.includeLayoutTr\u2026eshoot.llBatteryReinstall");
            Tb5 tb52 = this.g;
            if (tb52 != null) {
                LinearLayout linearLayout2 = tb52.e.c;
                Wg6.b(linearLayout2, "mBinding.includeLayoutTr\u2026llTouchscreenSmartwatches");
                if (!Wr4.a.a().b()) {
                    linearLayout2.setVisibility(8);
                }
                if (this.h) {
                    linearLayout.setVisibility(8);
                }
                Tb5 tb53 = this.g;
                if (tb53 != null) {
                    tb53.c.setOnClickListener(new Bi(this));
                    Tb5 tb54 = this.g;
                    if (tb54 != null) {
                        tb54.f.setOnClickListener(new Ci(this));
                        Tb5 tb55 = this.g;
                        if (tb55 != null) {
                            tb55.b.setOnClickListener(new Di(this));
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
