package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lt4 {
    @DexIgnore
    @Vu3("id")
    public String a;
    @DexIgnore
    @Vu3("socialId")
    public String b;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_PROFILE_PIC)
    public String e;

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Lt4) {
                Lt4 lt4 = (Lt4) obj;
                if (!Wg6.a(this.a, lt4.a) || !Wg6.a(this.b, lt4.b) || !Wg6.a(this.c, lt4.c) || !Wg6.a(this.d, lt4.d) || !Wg6.a(this.e, lt4.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ProfileData(id=" + this.a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", avatar=" + this.e + ")";
    }
}
