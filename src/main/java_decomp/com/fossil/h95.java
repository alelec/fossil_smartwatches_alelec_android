package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView w;

    @DexIgnore
    public H95(Object obj, View view, int i, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, ConstraintLayout constraintLayout, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = flexibleTextView;
        this.s = rTLImageView;
        this.t = rTLImageView2;
        this.u = rTLImageView3;
        this.v = constraintLayout;
        this.w = alphabetFastScrollRecyclerView;
    }
}
