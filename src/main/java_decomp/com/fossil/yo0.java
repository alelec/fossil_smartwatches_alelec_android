package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.fossil.Bp0;
import com.sina.weibo.sdk.api.ImageObject;
import io.flutter.view.AccessibilityBridge;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yo0 {
    @DexIgnore
    public static int d;
    @DexIgnore
    public /* final */ AccessibilityNodeInfo a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static /* final */ Ai e; // = new Ai(1, null);
        @DexIgnore
        public static /* final */ Ai f; // = new Ai(2, null);
        @DexIgnore
        public static /* final */ Ai g; // = new Ai(16, null);
        @DexIgnore
        public static /* final */ Ai h; // = new Ai(4096, null);
        @DexIgnore
        public static /* final */ Ai i; // = new Ai(8192, null);
        @DexIgnore
        public static /* final */ Ai j; // = new Ai(262144, null);
        @DexIgnore
        public static /* final */ Ai k; // = new Ai(524288, null);
        @DexIgnore
        public static /* final */ Ai l; // = new Ai(1048576, null);
        @DexIgnore
        public static /* final */ Ai m; // = new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null, 16908344, null, null, null);
        @DexIgnore
        public static /* final */ Ai n; // = new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null, 16908346, null, null, null);
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Class<? extends Bp0.Ai> c;
        @DexIgnore
        public /* final */ Bp0 d;

        /*
        static {
            new Ai(4, null);
            new Ai(8, null);
            new Ai(32, null);
            new Ai(64, null);
            new Ai(128, null);
            new Ai(256, null, Bp0.Bi.class);
            new Ai(512, null, Bp0.Bi.class);
            new Ai(1024, null, Bp0.Ci.class);
            new Ai(2048, null, Bp0.Ci.class);
            new Ai(16384, null);
            new Ai(32768, null);
            new Ai(65536, null);
            new Ai(131072, null, Bp0.Gi.class);
            new Ai(ImageObject.DATA_SIZE, null, Bp0.Hi.class);
            new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null, AccessibilityBridge.ACTION_SHOW_ON_SCREEN, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null, 16908343, null, null, Bp0.Ei.class);
            new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null, 16908345, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null, 16908347, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_UP : null, 16908358, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_DOWN : null, 16908359, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_LEFT : null, 16908360, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_RIGHT : null, 16908361, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null, 16908348, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null, 16908349, null, null, Bp0.Fi.class);
            new Ai(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null, 16908354, null, null, Bp0.Di.class);
            new Ai(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null, 16908356, null, null, null);
            new Ai(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP : null, 16908357, null, null, null);
        }
        */

        @DexIgnore
        public Ai(int i2, CharSequence charSequence) {
            this(null, i2, charSequence, null, null);
        }

        @DexIgnore
        public Ai(int i2, CharSequence charSequence, Class<? extends Bp0.Ai> cls) {
            this(null, i2, charSequence, null, cls);
        }

        @DexIgnore
        public Ai(Object obj) {
            this(obj, 0, null, null, null);
        }

        @DexIgnore
        public Ai(Object obj, int i2, CharSequence charSequence, Bp0 bp0, Class<? extends Bp0.Ai> cls) {
            this.b = i2;
            this.d = bp0;
            if (Build.VERSION.SDK_INT < 21 || obj != null) {
                this.a = obj;
            } else {
                this.a = new AccessibilityNodeInfo.AccessibilityAction(i2, charSequence);
            }
            this.c = cls;
        }

        @DexIgnore
        public Ai a(CharSequence charSequence, Bp0 bp0) {
            return new Ai(null, this.b, charSequence, bp0, this.c);
        }

        @DexIgnore
        public int b() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getId();
            }
            return 0;
        }

        @DexIgnore
        public CharSequence c() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getLabel();
            }
            return null;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x002b  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0047  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean d(android.view.View r6, android.os.Bundle r7) {
            /*
                r5 = this;
                r2 = 0
                r0 = 0
                com.fossil.Bp0 r1 = r5.d
                if (r1 == 0) goto L_0x0024
                java.lang.Class<? extends com.fossil.Bp0$Ai> r0 = r5.c
                if (r0 == 0) goto L_0x001e
                r1 = 0
                java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0044 }
                java.lang.reflect.Constructor r0 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x0044 }
                r1 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0044 }
                java.lang.Object r0 = r0.newInstance(r1)     // Catch:{ Exception -> 0x0044 }
                com.fossil.Bp0$Ai r0 = (com.fossil.Bp0.Ai) r0     // Catch:{ Exception -> 0x0044 }
                r0.a(r7)     // Catch:{ Exception -> 0x0025 }
                r2 = r0
            L_0x001e:
                com.fossil.Bp0 r0 = r5.d
                boolean r0 = r0.a(r6, r2)
            L_0x0024:
                return r0
            L_0x0025:
                r1 = move-exception
                r2 = r0
            L_0x0027:
                java.lang.Class<? extends com.fossil.Bp0$Ai> r0 = r5.c
                if (r0 != 0) goto L_0x0047
                java.lang.String r0 = "null"
            L_0x002d:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Failed to execute command with argument class ViewCommandArgument: "
                r3.append(r4)
                r3.append(r0)
                java.lang.String r0 = "A11yActionCompat"
                java.lang.String r3 = r3.toString()
                android.util.Log.e(r0, r3, r1)
                goto L_0x001e
            L_0x0044:
                r0 = move-exception
                r1 = r0
                goto L_0x0027
            L_0x0047:
                java.lang.String r0 = r0.getName()
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yo0.Ai.d(android.view.View, android.os.Bundle):boolean");
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            Object obj2 = this.a;
            if (obj2 == null) {
                return ai.a == null;
            }
            if (!obj2.equals(ai.a)) {
                return false;
            }
        }

        @DexIgnore
        public int hashCode() {
            Object obj = this.a;
            if (obj != null) {
                return obj.hashCode();
            }
            return 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public Bi(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static Bi a(int i, int i2, boolean z) {
            return Build.VERSION.SDK_INT >= 19 ? new Bi(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z)) : new Bi(null);
        }

        @DexIgnore
        public static Bi b(int i, int i2, boolean z, int i3) {
            int i4 = Build.VERSION.SDK_INT;
            return i4 >= 21 ? new Bi(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3)) : i4 >= 19 ? new Bi(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z)) : new Bi(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public Ci(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static Ci a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = Build.VERSION.SDK_INT;
            return i5 >= 21 ? new Ci(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2)) : i5 >= 19 ? new Ci(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z)) : new Ci(null);
        }
    }

    @DexIgnore
    public Yo0(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.a = accessibilityNodeInfo;
    }

    @DexIgnore
    public static Yo0 E0(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new Yo0(accessibilityNodeInfo);
    }

    @DexIgnore
    public static Yo0 O() {
        return E0(AccessibilityNodeInfo.obtain());
    }

    @DexIgnore
    public static Yo0 P(View view) {
        return E0(AccessibilityNodeInfo.obtain(view));
    }

    @DexIgnore
    public static Yo0 Q(Yo0 yo0) {
        return E0(AccessibilityNodeInfo.obtain(yo0.a));
    }

    @DexIgnore
    public static String j(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            case 262144:
                return "ACTION_EXPAND";
            case 524288:
                return "ACTION_COLLAPSE";
            case ImageObject.DATA_SIZE /* 2097152 */:
                return "ACTION_SET_TEXT";
            case 16908354:
                return "ACTION_MOVE_WINDOW";
            default:
                switch (i) {
                    case AccessibilityBridge.ACTION_SHOW_ON_SCREEN /* 16908342 */:
                        return "ACTION_SHOW_ON_SCREEN";
                    case 16908343:
                        return "ACTION_SCROLL_TO_POSITION";
                    case 16908344:
                        return "ACTION_SCROLL_UP";
                    case 16908345:
                        return "ACTION_SCROLL_LEFT";
                    case 16908346:
                        return "ACTION_SCROLL_DOWN";
                    case 16908347:
                        return "ACTION_SCROLL_RIGHT";
                    case 16908348:
                        return "ACTION_CONTEXT_CLICK";
                    case 16908349:
                        return "ACTION_SET_PROGRESS";
                    default:
                        switch (i) {
                            case 16908356:
                                return "ACTION_SHOW_TOOLTIP";
                            case 16908357:
                                return "ACTION_HIDE_TOOLTIP";
                            case 16908358:
                                return "ACTION_PAGE_UP";
                            case 16908359:
                                return "ACTION_PAGE_DOWN";
                            case 16908360:
                                return "ACTION_PAGE_LEFT";
                            case 16908361:
                                return "ACTION_PAGE_RIGHT";
                            default:
                                return "ACTION_UNKNOWN";
                        }
                }
        }
    }

    @DexIgnore
    public static ClickableSpan[] q(CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
        }
        return null;
    }

    @DexIgnore
    public final int A(ClickableSpan clickableSpan, SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); i++) {
                if (clickableSpan.equals(sparseArray.valueAt(i).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        int i2 = d;
        d = i2 + 1;
        return i2;
    }

    @DexIgnore
    public void A0(View view, int i) {
        this.c = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setSource(view, i);
        }
    }

    @DexIgnore
    public boolean B() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isAccessibilityFocused();
        }
        return false;
    }

    @DexIgnore
    public void B0(CharSequence charSequence) {
        this.a.setText(charSequence);
    }

    @DexIgnore
    public boolean C() {
        return this.a.isCheckable();
    }

    @DexIgnore
    public void C0(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setVisibleToUser(z);
        }
    }

    @DexIgnore
    public boolean D() {
        return this.a.isChecked();
    }

    @DexIgnore
    public AccessibilityNodeInfo D0() {
        return this.a;
    }

    @DexIgnore
    public boolean E() {
        return this.a.isClickable();
    }

    @DexIgnore
    public boolean F() {
        return this.a.isEnabled();
    }

    @DexIgnore
    public boolean G() {
        return this.a.isFocusable();
    }

    @DexIgnore
    public boolean H() {
        return this.a.isFocused();
    }

    @DexIgnore
    public boolean I() {
        return this.a.isLongClickable();
    }

    @DexIgnore
    public boolean J() {
        return this.a.isPassword();
    }

    @DexIgnore
    public boolean K() {
        return this.a.isScrollable();
    }

    @DexIgnore
    public boolean L() {
        return this.a.isSelected();
    }

    @DexIgnore
    public boolean M() {
        return Build.VERSION.SDK_INT >= 26 ? this.a.isShowingHintText() : l(4);
    }

    @DexIgnore
    public boolean N() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isVisibleToUser();
        }
        return false;
    }

    @DexIgnore
    public boolean R(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.performAction(i, bundle);
        }
        return false;
    }

    @DexIgnore
    public void S() {
        this.a.recycle();
    }

    @DexIgnore
    public boolean T(Ai ai) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.a.removeAction((AccessibilityNodeInfo.AccessibilityAction) ai.a);
        }
        return false;
    }

    @DexIgnore
    public final void U(View view) {
        SparseArray<WeakReference<ClickableSpan>> w = w(view);
        if (w != null) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < w.size(); i++) {
                if (w.valueAt(i).get() == null) {
                    arrayList.add(Integer.valueOf(i));
                }
            }
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                w.remove(((Integer) arrayList.get(i2)).intValue());
            }
        }
    }

    @DexIgnore
    public void V(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setAccessibilityFocused(z);
        }
    }

    @DexIgnore
    public final void W(int i, boolean z) {
        int i2 = 0;
        Bundle s = s();
        if (s != null) {
            int i3 = s.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0);
            if (z) {
                i2 = i;
            }
            s.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i2 | (i3 & i));
        }
    }

    @DexIgnore
    @Deprecated
    public void X(Rect rect) {
        this.a.setBoundsInParent(rect);
    }

    @DexIgnore
    public void Y(Rect rect) {
        this.a.setBoundsInScreen(rect);
    }

    @DexIgnore
    public void Z(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCanOpenPopup(z);
        }
    }

    @DexIgnore
    public void a(int i) {
        this.a.addAction(i);
    }

    @DexIgnore
    public void a0(boolean z) {
        this.a.setCheckable(z);
    }

    @DexIgnore
    public void b(Ai ai) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.addAction((AccessibilityNodeInfo.AccessibilityAction) ai.a);
        }
    }

    @DexIgnore
    public void b0(boolean z) {
        this.a.setChecked(z);
    }

    @DexIgnore
    public void c(View view) {
        this.a.addChild(view);
    }

    @DexIgnore
    public void c0(CharSequence charSequence) {
        this.a.setClassName(charSequence);
    }

    @DexIgnore
    public void d(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.addChild(view, i);
        }
    }

    @DexIgnore
    public void d0(boolean z) {
        this.a.setClickable(z);
    }

    @DexIgnore
    public final void e(ClickableSpan clickableSpan, Spanned spanned, int i) {
        h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan)));
        h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan)));
        h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan)));
        h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
    }

    @DexIgnore
    public void e0(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionInfo) ((Bi) obj).a);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Yo0)) {
            return false;
        }
        Yo0 yo0 = (Yo0) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            if (yo0.a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(yo0.a)) {
            return false;
        }
        if (this.c != yo0.c) {
            return false;
        }
        return this.b == yo0.b;
    }

    @DexIgnore
    public void f(CharSequence charSequence, View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19 && i < 26) {
            g();
            U(view);
            ClickableSpan[] q = q(charSequence);
            if (q != null && q.length > 0) {
                s().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", Pk0.accessibility_action_clickable_span);
                SparseArray<WeakReference<ClickableSpan>> u = u(view);
                int i2 = 0;
                while (q != null && i2 < q.length) {
                    int A = A(q[i2], u);
                    u.put(A, new WeakReference<>(q[i2]));
                    e(q[i2], (Spanned) charSequence, A);
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public void f0(Object obj) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCollectionItemInfo(obj == null ? null : (AccessibilityNodeInfo.CollectionItemInfo) ((Ci) obj).a);
        }
    }

    @DexIgnore
    public final void g() {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }

    @DexIgnore
    public void g0(CharSequence charSequence) {
        this.a.setContentDescription(charSequence);
    }

    @DexIgnore
    public final List<Integer> h(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.a.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        this.a.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    @DexIgnore
    public void h0(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setContentInvalid(z);
        }
    }

    @DexIgnore
    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    @DexIgnore
    public List<Ai> i() {
        List<AccessibilityNodeInfo.AccessibilityAction> actionList = Build.VERSION.SDK_INT >= 21 ? this.a.getActionList() : null;
        if (actionList == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        int size = actionList.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(new Ai(actionList.get(i)));
        }
        return arrayList;
    }

    @DexIgnore
    public void i0(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setDismissable(z);
        }
    }

    @DexIgnore
    public void j0(boolean z) {
        this.a.setEnabled(z);
    }

    @DexIgnore
    public int k() {
        return this.a.getActions();
    }

    @DexIgnore
    public void k0(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.setError(charSequence);
        }
    }

    @DexIgnore
    public final boolean l(int i) {
        Bundle s = s();
        return s != null && (s.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & i) == i;
    }

    @DexIgnore
    public void l0(boolean z) {
        this.a.setFocusable(z);
    }

    @DexIgnore
    @Deprecated
    public void m(Rect rect) {
        this.a.getBoundsInParent(rect);
    }

    @DexIgnore
    public void m0(boolean z) {
        this.a.setFocused(z);
    }

    @DexIgnore
    public void n(Rect rect) {
        this.a.getBoundsInScreen(rect);
    }

    @DexIgnore
    public void n0(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setHeading(z);
        } else {
            W(2, z);
        }
    }

    @DexIgnore
    public int o() {
        return this.a.getChildCount();
    }

    @DexIgnore
    public void o0(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.a.setHintText(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    @DexIgnore
    public CharSequence p() {
        return this.a.getClassName();
    }

    @DexIgnore
    public void p0(boolean z) {
        this.a.setLongClickable(z);
    }

    @DexIgnore
    public void q0(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setMovementGranularities(i);
        }
    }

    @DexIgnore
    public CharSequence r() {
        return this.a.getContentDescription();
    }

    @DexIgnore
    public void r0(CharSequence charSequence) {
        this.a.setPackageName(charSequence);
    }

    @DexIgnore
    public Bundle s() {
        return Build.VERSION.SDK_INT >= 19 ? this.a.getExtras() : new Bundle();
    }

    @DexIgnore
    public void s0(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.a.setPaneTitle(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    @DexIgnore
    public int t() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.getMovementGranularities();
        }
        return 0;
    }

    @DexIgnore
    public void t0(View view) {
        this.b = -1;
        this.a.setParent(view);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        m(rect);
        sb.append("; boundsInParent: " + rect);
        n(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(v());
        sb.append("; className: ");
        sb.append(p());
        sb.append("; text: ");
        sb.append(x());
        sb.append("; contentDescription: ");
        sb.append(r());
        sb.append("; viewId: ");
        sb.append(y());
        sb.append("; checkable: ");
        sb.append(C());
        sb.append("; checked: ");
        sb.append(D());
        sb.append("; focusable: ");
        sb.append(G());
        sb.append("; focused: ");
        sb.append(H());
        sb.append("; selected: ");
        sb.append(L());
        sb.append("; clickable: ");
        sb.append(E());
        sb.append("; longClickable: ");
        sb.append(I());
        sb.append("; enabled: ");
        sb.append(F());
        sb.append("; password: ");
        sb.append(J());
        sb.append("; scrollable: " + K());
        sb.append("; [");
        if (Build.VERSION.SDK_INT >= 21) {
            List<Ai> i = i();
            for (int i2 = 0; i2 < i.size(); i2++) {
                Ai ai = i.get(i2);
                String j = j(ai.b());
                sb.append((!j.equals("ACTION_UNKNOWN") || ai.c() == null) ? j : ai.c().toString());
                if (i2 != i.size() - 1) {
                    sb.append(", ");
                }
            }
        } else {
            int k = k();
            while (k != 0) {
                int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(k);
                k &= numberOfTrailingZeros;
                sb.append(j(numberOfTrailingZeros));
                if (k != 0) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> u(View view) {
        SparseArray<WeakReference<ClickableSpan>> w = w(view);
        if (w != null) {
            return w;
        }
        SparseArray<WeakReference<ClickableSpan>> sparseArray = new SparseArray<>();
        view.setTag(Pk0.tag_accessibility_clickable_spans, sparseArray);
        return sparseArray;
    }

    @DexIgnore
    public void u0(View view, int i) {
        this.b = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setParent(view, i);
        }
    }

    @DexIgnore
    public CharSequence v() {
        return this.a.getPackageName();
    }

    @DexIgnore
    public void v0(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setScreenReaderFocusable(z);
        } else {
            W(1, z);
        }
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> w(View view) {
        return (SparseArray) view.getTag(Pk0.tag_accessibility_clickable_spans);
    }

    @DexIgnore
    public void w0(boolean z) {
        this.a.setScrollable(z);
    }

    @DexIgnore
    public CharSequence x() {
        if (!z()) {
            return this.a.getText();
        }
        List<Integer> h = h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List<Integer> h2 = h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List<Integer> h3 = h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List<Integer> h4 = h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.a.getText(), 0, this.a.getText().length()));
        for (int i = 0; i < h.size(); i++) {
            spannableString.setSpan(new Wo0(h4.get(i).intValue(), this, s().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), h.get(i).intValue(), h2.get(i).intValue(), h3.get(i).intValue());
        }
        return spannableString;
    }

    @DexIgnore
    public void x0(boolean z) {
        this.a.setSelected(z);
    }

    @DexIgnore
    public String y() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.a.getViewIdResourceName();
        }
        return null;
    }

    @DexIgnore
    public void y0(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.a.setShowingHintText(z);
        } else {
            W(4, z);
        }
    }

    @DexIgnore
    public final boolean z() {
        return !h("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty();
    }

    @DexIgnore
    public void z0(View view) {
        this.c = -1;
        this.a.setSource(view);
    }
}
