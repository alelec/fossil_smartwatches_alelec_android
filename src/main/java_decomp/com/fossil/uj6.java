package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.bn6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj6 extends rj6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public /* final */ MutableLiveData<Date> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ LiveData<h47<List<MFSleepDay>>> j;
    @DexIgnore
    public /* final */ LiveData<h47<List<MFSleepSession>>> k;
    @DexIgnore
    public /* final */ sj6 l;
    @DexIgnore
    public /* final */ SleepSummariesRepository m;
    @DexIgnore
    public /* final */ SleepSessionsRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uj6 f3603a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uj6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSessions$1$1", f = "SleepOverviewDayPresenter.kt", l = {52, 52}, m = "invokeSuspend")
        /* renamed from: com.fossil.uj6$a$a  reason: collision with other inner class name */
        public static final class C0251a extends ko7 implements vp7<hs0<h47<? extends List<MFSleepSession>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0251a(a aVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0251a aVar = new C0251a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<MFSleepSession>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0251a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.uj6$a r1 = r8.this$0
                    com.fossil.uj6 r1 = r1.f3603a
                    com.portfolio.platform.data.source.SleepSessionsRepository r1 = com.fossil.uj6.q(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getSleepSessionList(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj6.a.C0251a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public a(uj6 uj6) {
            this.f3603a = uj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<MFSleepSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSessions onDateChange " + date);
            return or0.c(null, 0, new C0251a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uj6 f3604a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSummaries$1$1", f = "SleepOverviewDayPresenter.kt", l = {46, 46}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<MFSleepDay>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<MFSleepDay>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.uj6$b r1 = r8.this$0
                    com.fossil.uj6 r1 = r1.f3604a
                    com.portfolio.platform.data.source.SleepSummariesRepository r1 = com.fossil.uj6.s(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getSleepSummaries(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(uj6 uj6) {
            this.f3604a = uj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<MFSleepDay>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSummaries onDateChange " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1", f = "SleepOverviewDayPresenter.kt", l = {172}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends bn6.b>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(List list, qn7 qn7, c cVar) {
                super(2, qn7);
                this.$it = list;
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$it, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends bn6.b>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    uj6 uj6 = this.this$0.this$0;
                    Date date = uj6.f;
                    if (date != null) {
                        return uj6.C(date, this.$it);
                    }
                    pq7.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uj6 uj6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = uj6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                h47 h47 = (h47) this.this$0.k.e();
                if (h47 == null || (list = (List) h47.c()) == null) {
                    this.this$0.l.S4(new ArrayList());
                    return tl7.f3441a;
                }
                dv7 h = this.this$0.h();
                a aVar = new a(list, null, this);
                this.L$0 = iv7;
                this.L$1 = list;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.l.S4((List) g);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<h47<? extends List<MFSleepDay>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uj6 f3605a;

        @DexIgnore
        public d(uj6 uj6) {
            this.f3605a = uj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<MFSleepDay>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                this.f3605a.h = true;
                if (this.f3605a.h && this.f3605a.i) {
                    this.f3605a.B();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<h47<? extends List<MFSleepSession>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uj6 f3606a;

        @DexIgnore
        public e(uj6 uj6) {
            this.f3606a = uj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<MFSleepSession>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSessions -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            sb.append('\"');
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                this.f3606a.i = true;
                if (this.f3606a.h && this.f3606a.i) {
                    this.f3606a.B();
                }
            }
        }
    }

    @DexIgnore
    public uj6(sj6 sj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        pq7.c(sj6, "mView");
        pq7.c(sleepSummariesRepository, "mSummariesRepository");
        pq7.c(sleepSessionsRepository, "mSessionsRepository");
        pq7.c(portfolioApp, "mApp");
        this.l = sj6;
        this.m = sleepSummariesRepository;
        this.n = sleepSessionsRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<h47<List<MFSleepDay>>> c2 = ss0.c(mutableLiveData, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026 false) )\n        }\n    }");
        this.j = c2;
        LiveData<h47<List<MFSleepSession>>> c3 = ss0.c(this.g, new a(this));
        pq7.b(c3, "Transformations.switchMa\u2026false)  )\n        }\n    }");
        this.k = c3;
    }

    @DexIgnore
    public void A() {
        this.l.M5(this);
    }

    @DexIgnore
    public final xw7 B() {
        return gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final List<bn6.b> C(Date date, List<MFSleepSession> list) {
        T t;
        List<MFSleepDay> c2;
        T t2;
        ArrayList<MFSleepSession> arrayList = new ArrayList();
        for (T t3 : list) {
            if (lk5.l0(Long.valueOf(date.getTime()), Long.valueOf(t3.getDate()))) {
                arrayList.add(t3);
            }
        }
        h47<List<MFSleepDay>> e2 = this.j.e();
        if (e2 == null || (c2 = e2.c()) == null) {
            t = null;
        } else {
            Iterator<T> it = c2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (lk5.m0(date, next.getDate())) {
                    t2 = next;
                    break;
                }
            }
            t = t2;
        }
        int e3 = sk5.c.e(t);
        ArrayList arrayList2 = new ArrayList();
        for (MFSleepSession mFSleepSession : arrayList) {
            BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
            ArrayList arrayList3 = new ArrayList();
            SleepDistribution realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute();
            List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
            ArrayList arrayList4 = new ArrayList();
            int realStartTime = mFSleepSession.getRealStartTime();
            int totalMinuteBySleepDistribution = realSleepStateDistInMinute.getTotalMinuteBySleepDistribution();
            if (sleepStateChange != null) {
                for (T t4 : sleepStateChange) {
                    BarChart.b bVar = new BarChart.b(0, null, 0, 0, null, 31, null);
                    bVar.g((int) t4.index);
                    bVar.j(realStartTime);
                    bVar.i(totalMinuteBySleepDistribution);
                    int i2 = t4.state;
                    if (i2 == 0) {
                        bVar.h(BarChart.e.LOWEST);
                    } else if (i2 == 1) {
                        bVar.h(BarChart.e.DEFAULT);
                    } else if (i2 == 2) {
                        bVar.h(BarChart.e.HIGHEST);
                    }
                    arrayList4.add(bVar);
                }
            }
            arrayList3.add(arrayList4);
            cVar.b().add(new BarChart.a(e3, arrayList3.size() != 0 ? arrayList3 : hm7.c(hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), 0, false, 12, null));
            cVar.f(e3);
            cVar.e(e3);
            int awake = realSleepStateDistInMinute.getAwake();
            int light = realSleepStateDistInMinute.getLight();
            int deep = realSleepStateDistInMinute.getDeep();
            if (totalMinuteBySleepDistribution > 0) {
                float f2 = (float) totalMinuteBySleepDistribution;
                float f3 = ((float) awake) / f2;
                float f4 = ((float) light) / f2;
                arrayList2.add(new bn6.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession.getTimezoneOffset()));
            }
        }
        return arrayList2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.f;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.h = false;
            this.i = false;
            Date date2 = new Date();
            this.f = date2;
            this.g.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewDayPresenter", "loadData - mDate=" + this.f);
        LiveData<h47<List<MFSleepDay>>> liveData = this.j;
        sj6 sj6 = this.l;
        if (sj6 != null) {
            liveData.h((tj6) sj6, new d(this));
            this.k.h((LifecycleOwner) this.l, new e(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "stop");
        try {
            LiveData<h47<List<MFSleepDay>>> liveData = this.j;
            sj6 sj6 = this.l;
            if (sj6 != null) {
                liveData.n((tj6) sj6);
                this.k.n((LifecycleOwner) this.l);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE z() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }
}
