package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bt extends Ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ N6 I;
    @DexIgnore
    public /* final */ N6 J;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Bt(K5 k5, Ut ut, Hs hs, int i, int i2) {
        super(hs, k5, (i2 & 8) != 0 ? 3 : i);
        this.G = ut.a();
        byte[] array = ByteBuffer.allocate(2).put(ut.d.a()).put(ut.e.b).array();
        Wg6.b(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        this.H = array;
        N6 n6 = N6.n;
        this.I = n6;
        this.J = n6;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final Mt E(byte b) {
        return Wt.g.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final long a(O7 o7) {
        return 0;
    }
}
