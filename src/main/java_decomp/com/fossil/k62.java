package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K62 extends Kq0 {
    @DexIgnore
    public Dialog b;
    @DexIgnore
    public DialogInterface.OnCancelListener c;

    @DexIgnore
    public static K62 v6(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        K62 k62 = new K62();
        Rc2.l(dialog, "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        k62.b = dialog2;
        if (onCancelListener != null) {
            k62.c = onCancelListener;
        }
        return k62;
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.c;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.b == null) {
            setShowsDialog(false);
        }
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
