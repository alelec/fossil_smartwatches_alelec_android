package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class En1 extends R60 {
    @DexIgnore
    public static /* final */ Bi CREATOR; // = new Bi(null);
    @DexIgnore
    public static /* final */ short i; // = Hy1.c(Fq7.a);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ short g;
    @DexIgnore
    public /* final */ Ai h;

    @DexIgnore
    public enum Ai {
        DISABLE((byte) 0),
        ENABLE((byte) 1);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) throws IllegalArgumentException {
                Ai ai;
                Ai[] values = Ai.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        ai = null;
                        break;
                    }
                    ai = values[i];
                    if (ai.a() == b) {
                        break;
                    }
                    i++;
                }
                if (ai != null) {
                    return ai;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Parcelable.Creator<En1> {
        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
        }

        @DexIgnore
        public final En1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 6) {
                return new En1(bArr[0], bArr[1], bArr[2], bArr[3], Hy1.p(bArr[4]), Ai.d.a(bArr[5]));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        public En1 b(Parcel parcel) {
            return new En1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public En1 createFromParcel(Parcel parcel) {
            return new En1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public En1[] newArray(int i) {
            return new En1[i];
        }
    }

    @DexIgnore
    public En1(byte b, byte b2, byte b3, byte b4, short s, Ai ai) throws IllegalArgumentException {
        super(Zm1.INACTIVE_NUDGE);
        this.c = (byte) b;
        this.d = (byte) b2;
        this.e = (byte) b3;
        this.f = (byte) b4;
        this.g = (short) s;
        this.h = ai;
        d();
    }

    @DexIgnore
    public /* synthetic */ En1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        this.f = parcel.readByte();
        this.g = (short) ((short) parcel.readInt());
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.h = Ai.valueOf(readString);
            d();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put(this.c).put(this.d).put(this.e).put(this.f).put((byte) this.g).put(this.h.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.c));
            jSONObject.put("start_minute", Byte.valueOf(this.d));
            jSONObject.put("stop_hour", Byte.valueOf(this.e));
            jSONObject.put("stop_minute", Byte.valueOf(this.f));
            jSONObject.put("repeat_interval", Short.valueOf(this.g));
            jSONObject.put("state", Ey1.a(this.h));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        byte b = this.c;
        if (b >= 0 && 23 >= b) {
            byte b2 = this.d;
            if (b2 >= 0 && 59 >= b2) {
                byte b3 = this.e;
                if (b3 >= 0 && 23 >= b3) {
                    byte b4 = this.f;
                    if (b4 >= 0 && 59 >= b4) {
                        short s = i;
                        short s2 = this.g;
                        if (s2 < 0 || s < s2) {
                            z = false;
                        }
                        if (!z) {
                            StringBuilder e2 = E.e("repeatInterval(");
                            e2.append((int) this.g);
                            e2.append(") is out of range ");
                            e2.append("[0, ");
                            throw new IllegalArgumentException(E.b(e2, i, "] (in minute."));
                        }
                        return;
                    }
                    throw new IllegalArgumentException(E.c(E.e("stopMinute("), this.f, ") is out of range ", "[0, 59]."));
                }
                throw new IllegalArgumentException(E.c(E.e("stopHour("), this.e, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(E.c(E.e("startMinute("), this.d, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(E.c(E.e("startHour("), this.c, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(En1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            En1 en1 = (En1) obj;
            if (this.c != en1.c) {
                return false;
            }
            if (this.d != en1.d) {
                return false;
            }
            if (this.e != en1.e) {
                return false;
            }
            if (this.f != en1.f) {
                return false;
            }
            if (this.g != en1.g) {
                return false;
            }
            return this.h == en1.h;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.InactiveNudgeConfig");
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.g;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.c;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.d;
    }

    @DexIgnore
    public final Ai getState() {
        return this.h;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.e;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        int hashCode = super.hashCode();
        byte b = this.c;
        byte b2 = this.d;
        byte b3 = this.e;
        byte b4 = this.f;
        return (((((((((((hashCode * 31) + b) * 31) + b2) * 31) + b3) * 31) + b4) * 31) + this.g) * 31) + this.h.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeByte(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.g));
        }
        if (parcel != null) {
            parcel.writeString(this.h.name());
        }
    }
}
