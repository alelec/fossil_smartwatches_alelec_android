package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vq7 implements Hq7 {
    @DexIgnore
    public /* final */ Class<?> b;

    @DexIgnore
    public Vq7(Class<?> cls, String str) {
        Wg6.c(cls, "jClass");
        Wg6.c(str, "moduleName");
        this.b = cls;
    }

    @DexIgnore
    @Override // com.fossil.Hq7
    public Class<?> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Vq7) && Wg6.a(b(), ((Vq7) obj).b());
    }

    @DexIgnore
    public int hashCode() {
        return b().hashCode();
    }

    @DexIgnore
    public String toString() {
        return b().toString() + " (Kotlin reflection is not available)";
    }
}
