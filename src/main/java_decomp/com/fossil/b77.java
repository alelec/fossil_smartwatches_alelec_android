package com.fossil;

import android.app.Activity;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.watchface.edit.WatchFaceEditViewModel;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B77 {
    @DexIgnore
    public static /* final */ B77 a; // = new B77();

    @DexIgnore
    public final void a(String str) {
        Wg6.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("wf_id", str);
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logAppliedWatchFace, value = " + hashMap);
        AnalyticsHelper.f.g().l("wf_apply", hashMap);
    }

    @DexIgnore
    public final void b(int i, Activity activity) {
        String str = "wf_customize_background";
        if (i == 0) {
            str = "wf_customize_text";
        } else if (i == 1) {
            str = "wf_customize_sticker";
        } else if (i != 2) {
            if (i == 3) {
                str = "wf_customize_photo";
            } else if (i == 4) {
                str = "wf_customize_complication";
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logCustomizationScreen, screenName = " + str);
        AnalyticsHelper.f.g().m(str, activity);
    }

    @DexIgnore
    public final void c(String str, String str2, String str3) {
        Wg6.c(str, "eventName");
        Wg6.c(str2, "watchFaceId");
        Wg6.c(str3, "watchFaceType");
        HashMap hashMap = new HashMap();
        hashMap.put("wf_id", str2);
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        hashMap.put("wf_type", str3);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logGalleryEvent, eventName = " + str + ", value = " + hashMap);
        AnalyticsHelper.f.g().l(str, hashMap);
    }

    @DexIgnore
    public final void d(String str) {
        Wg6.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("source_wf_id", str);
        hashMap.put("start_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logNewPreset, value = " + hashMap);
        AnalyticsHelper.f.g().l("wf_myfaces_create_preset", hashMap);
    }

    @DexIgnore
    public final void e(String str) {
        Wg6.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("source_wf_id", str);
        hashMap.put("start_timestamp", Long.valueOf(System.currentTimeMillis()));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logNewWatchFace, value = " + hashMap);
        AnalyticsHelper.f.g().l("wf_myfaces_create_face", hashMap);
    }

    @DexIgnore
    public final void f(String str) {
        Wg6.c(str, "watchFaceId");
        HashMap hashMap = new HashMap();
        hashMap.put("event_timestamp", Long.valueOf(System.currentTimeMillis()));
        hashMap.put("wf_id", str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logOpenEvent, value = " + hashMap);
        AnalyticsHelper.f.g().l("wf_sharedwf_open_event", hashMap);
    }

    @DexIgnore
    public final void g(Ob7 ob7, WatchFaceEditViewModel.Ai ai) {
        Wg6.c(ob7, "theme");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        for (T t : ob7.a()) {
            if (t instanceof Mb7) {
                arrayList.add(t.e());
            } else if (t instanceof Pb7) {
                arrayList2.add(t.d());
            } else if (t instanceof Ib7) {
                arrayList3.add(V87.b(t.getType()));
            }
        }
        Sl5 b = AnalyticsHelper.f.b("wf_custom_stats");
        b.a("comp_text", Jj5.a(arrayList));
        b.a("comp_sticker", Jj5.a(arrayList2));
        b.a("comp_complication", Jj5.a(arrayList3));
        if (ai != null) {
            int i = A77.a[ai.b().ordinal()];
            if (i == 1) {
                b.a("comp_cus_photo", ai.a());
                b.a("comp_background", "");
            } else if (i == 2) {
                b.a("comp_cus_photo", "");
                b.a("comp_background", ai.a());
            }
        }
        b.b();
    }

    @DexIgnore
    public final void h(String str, long j, long j2) {
        Wg6.c(str, Constants.SESSION);
        HashMap hashMap = new HashMap();
        hashMap.put("start_timestamp", Long.valueOf(j));
        hashMap.put("end_timestamp", Long.valueOf(j2));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceAnalytics", "logEndSession, value = " + hashMap);
        AnalyticsHelper.f.g().l(str, hashMap);
    }
}
