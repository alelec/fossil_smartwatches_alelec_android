package com.fossil;

import com.fossil.A34;
import com.fossil.F64;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C64 {
    @DexIgnore
    public /* final */ Ci a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E64 {
        @DexIgnore
        public /* final */ /* synthetic */ Map b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public Ai(Map map, Type type) {
            this.b = map;
            this.c = type;
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void b(Class<?> cls) {
            if (!(this.c instanceof WildcardType)) {
                throw new IllegalArgumentException("No type mapping from " + cls + " to " + this.c);
            }
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void c(GenericArrayType genericArrayType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                Type j = F64.j(type);
                I14.h(j != null, "%s is not an array type.", this.c);
                C64.f(this.b, genericArrayType.getGenericComponentType(), j);
            }
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void d(ParameterizedType parameterizedType) {
            Type type = this.c;
            if (!(type instanceof WildcardType)) {
                ParameterizedType parameterizedType2 = (ParameterizedType) C64.e(ParameterizedType.class, type);
                if (!(parameterizedType.getOwnerType() == null || parameterizedType2.getOwnerType() == null)) {
                    C64.f(this.b, parameterizedType.getOwnerType(), parameterizedType2.getOwnerType());
                }
                I14.i(parameterizedType.getRawType().equals(parameterizedType2.getRawType()), "Inconsistent raw type: %s vs. %s", parameterizedType, this.c);
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                I14.i(actualTypeArguments.length == actualTypeArguments2.length, "%s not compatible with %s", parameterizedType, parameterizedType2);
                for (int i = 0; i < actualTypeArguments.length; i++) {
                    C64.f(this.b, actualTypeArguments[i], actualTypeArguments2[i]);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void e(TypeVariable<?> typeVariable) {
            this.b.put(new Di(typeVariable), this.c);
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void f(WildcardType wildcardType) {
            Type type = this.c;
            if (type instanceof WildcardType) {
                WildcardType wildcardType2 = (WildcardType) type;
                Type[] upperBounds = wildcardType.getUpperBounds();
                Type[] upperBounds2 = wildcardType2.getUpperBounds();
                Type[] lowerBounds = wildcardType.getLowerBounds();
                Type[] lowerBounds2 = wildcardType2.getLowerBounds();
                I14.i(upperBounds.length == upperBounds2.length && lowerBounds.length == lowerBounds2.length, "Incompatible type: %s vs. %s", wildcardType, this.c);
                for (int i = 0; i < upperBounds.length; i++) {
                    C64.f(this.b, upperBounds[i], upperBounds2[i]);
                }
                for (int i2 = 0; i2 < lowerBounds.length; i2++) {
                    C64.f(this.b, lowerBounds[i2], lowerBounds2[i2]);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends E64 {
        @DexIgnore
        public static /* final */ Ei c; // = new Ei(null);
        @DexIgnore
        public /* final */ Map<Di, Type> b; // = X34.j();

        @DexIgnore
        public static A34<Di, Type> g(Type type) {
            Bi bi = new Bi();
            bi.a(c.a(type));
            return A34.copyOf(bi.b);
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void b(Class<?> cls) {
            a(cls.getGenericSuperclass());
            a(cls.getGenericInterfaces());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void d(ParameterizedType parameterizedType) {
            Class cls = (Class) parameterizedType.getRawType();
            TypeVariable[] typeParameters = cls.getTypeParameters();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            I14.s(typeParameters.length == actualTypeArguments.length);
            for (int i = 0; i < typeParameters.length; i++) {
                h(new Di(typeParameters[i]), actualTypeArguments[i]);
            }
            a(cls);
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void e(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }

        @DexIgnore
        public final void h(Di di, Type type) {
            if (!this.b.containsKey(di)) {
                Type type2 = type;
                while (type2 != null) {
                    if (di.a(type2)) {
                        while (type != null) {
                            type = this.b.remove(Di.c(type));
                        }
                        return;
                    }
                    type2 = this.b.get(Di.c(type2));
                }
                this.b.put(di, type);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ A34<Di, Type> a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Ci {
            @DexIgnore
            public /* final */ /* synthetic */ TypeVariable b;
            @DexIgnore
            public /* final */ /* synthetic */ Ci c;

            @DexIgnore
            public Aii(Ci ci, TypeVariable typeVariable, Ci ci2) {
                this.b = typeVariable;
                this.c = ci2;
            }

            @DexIgnore
            @Override // com.fossil.C64.Ci
            public Type b(TypeVariable<?> typeVariable, Ci ci) {
                return typeVariable.getGenericDeclaration().equals(this.b.getGenericDeclaration()) ? typeVariable : this.c.b(typeVariable, ci);
            }
        }

        @DexIgnore
        public Ci() {
            this.a = A34.of();
        }

        @DexIgnore
        public Ci(A34<Di, Type> a34) {
            this.a = a34;
        }

        @DexIgnore
        public final Type a(TypeVariable<?> typeVariable) {
            return b(typeVariable, new Aii(this, typeVariable, this));
        }

        @DexIgnore
        public Type b(TypeVariable<?> typeVariable, Ci ci) {
            Type type = this.a.get(new Di(typeVariable));
            if (type != null) {
                return new C64(ci, null).i(type);
            }
            Type[] bounds = typeVariable.getBounds();
            if (bounds.length == 0) {
                return typeVariable;
            }
            Type[] j = new C64(ci, null).j(bounds);
            return (!F64.Fi.a || !Arrays.equals(bounds, j)) ? F64.l(typeVariable.getGenericDeclaration(), typeVariable.getName(), j) : typeVariable;
        }

        @DexIgnore
        public final Ci c(Map<Di, ? extends Type> map) {
            A34.Bi builder = A34.builder();
            builder.f(this.a);
            for (Map.Entry<Di, ? extends Type> entry : map.entrySet()) {
                Di key = entry.getKey();
                Type type = (Type) entry.getValue();
                I14.h(!key.a(type), "Type variable %s bound to itself", key);
                builder.c(key, type);
            }
            return new Ci(builder.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ TypeVariable<?> a;

        @DexIgnore
        public Di(TypeVariable<?> typeVariable) {
            I14.l(typeVariable);
            this.a = typeVariable;
        }

        @DexIgnore
        public static Di c(Type type) {
            if (type instanceof TypeVariable) {
                return new Di((TypeVariable) type);
            }
            return null;
        }

        @DexIgnore
        public boolean a(Type type) {
            if (type instanceof TypeVariable) {
                return b((TypeVariable) type);
            }
            return false;
        }

        @DexIgnore
        public final boolean b(TypeVariable<?> typeVariable) {
            return this.a.getGenericDeclaration().equals(typeVariable.getGenericDeclaration()) && this.a.getName().equals(typeVariable.getName());
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof Di) {
                return b(((Di) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return F14.b(this.a.getGenericDeclaration(), this.a.getName());
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei {
        @DexIgnore
        public /* final */ AtomicInteger a;

        @DexIgnore
        public Ei() {
            this.a = new AtomicInteger();
        }

        @DexIgnore
        public /* synthetic */ Ei(Ai ai) {
            this();
        }

        @DexIgnore
        public Type a(Type type) {
            I14.l(type);
            if ((type instanceof Class) || (type instanceof TypeVariable)) {
                return type;
            }
            if (type instanceof GenericArrayType) {
                return F64.k(a(((GenericArrayType) type).getGenericComponentType()));
            }
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                return F64.n(c(parameterizedType.getOwnerType()), (Class) parameterizedType.getRawType(), b(parameterizedType.getActualTypeArguments()));
            } else if (type instanceof WildcardType) {
                WildcardType wildcardType = (WildcardType) type;
                if (wildcardType.getLowerBounds().length != 0) {
                    return type;
                }
                Type[] upperBounds = wildcardType.getUpperBounds();
                return F64.l(Ei.class, "capture#" + this.a.incrementAndGet() + "-of ? extends " + D14.h('&').g(upperBounds), wildcardType.getUpperBounds());
            } else {
                throw new AssertionError("must have been one of the known types");
            }
        }

        @DexIgnore
        public final Type[] b(Type[] typeArr) {
            Type[] typeArr2 = new Type[typeArr.length];
            for (int i = 0; i < typeArr.length; i++) {
                typeArr2[i] = a(typeArr[i]);
            }
            return typeArr2;
        }

        @DexIgnore
        public final Type c(Type type) {
            if (type == null) {
                return null;
            }
            return a(type);
        }
    }

    @DexIgnore
    public C64() {
        this.a = new Ci();
    }

    @DexIgnore
    public C64(Ci ci) {
        this.a = ci;
    }

    @DexIgnore
    public /* synthetic */ C64(Ci ci, Ai ai) {
        this(ci);
    }

    @DexIgnore
    public static C64 d(Type type) {
        return new C64().m(Bi.g(type));
    }

    @DexIgnore
    public static <T> T e(Class<T> cls, Object obj) {
        try {
            return cls.cast(obj);
        } catch (ClassCastException e) {
            throw new IllegalArgumentException(obj + " is not a " + cls.getSimpleName());
        }
    }

    @DexIgnore
    public static void f(Map<Di, Type> map, Type type, Type type2) {
        if (!type.equals(type2)) {
            new Ai(map, type2).a(type);
        }
    }

    @DexIgnore
    public final Type g(GenericArrayType genericArrayType) {
        return F64.k(i(genericArrayType.getGenericComponentType()));
    }

    @DexIgnore
    public final ParameterizedType h(ParameterizedType parameterizedType) {
        Type ownerType = parameterizedType.getOwnerType();
        return F64.n(ownerType == null ? null : i(ownerType), (Class) i(parameterizedType.getRawType()), j(parameterizedType.getActualTypeArguments()));
    }

    @DexIgnore
    public Type i(Type type) {
        I14.l(type);
        return type instanceof TypeVariable ? this.a.a((TypeVariable) type) : type instanceof ParameterizedType ? h((ParameterizedType) type) : type instanceof GenericArrayType ? g((GenericArrayType) type) : type instanceof WildcardType ? k((WildcardType) type) : type;
    }

    @DexIgnore
    public final Type[] j(Type[] typeArr) {
        Type[] typeArr2 = new Type[typeArr.length];
        for (int i = 0; i < typeArr.length; i++) {
            typeArr2[i] = i(typeArr[i]);
        }
        return typeArr2;
    }

    @DexIgnore
    public final WildcardType k(WildcardType wildcardType) {
        return new F64.Ji(j(wildcardType.getLowerBounds()), j(wildcardType.getUpperBounds()));
    }

    @DexIgnore
    public C64 l(Type type, Type type2) {
        HashMap j = X34.j();
        I14.l(type);
        I14.l(type2);
        f(j, type, type2);
        return m(j);
    }

    @DexIgnore
    public C64 m(Map<Di, ? extends Type> map) {
        return new C64(this.a.c(map));
    }
}
