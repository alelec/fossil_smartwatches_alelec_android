package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A47 {
    @DexIgnore
    public static A47 a;

    @DexIgnore
    public static A47 c() {
        A47 a47;
        synchronized (A47.class) {
            try {
                if (a == null) {
                    a = new A47();
                }
                a47 = a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return a47;
    }

    @DexIgnore
    public static SharedPreferences f(Context context) {
        return context.getSharedPreferences(Constants.FRAMEWORKS_SHAREPREFS, 0);
    }

    @DexIgnore
    public final boolean a(Context context, String str, boolean z) {
        SharedPreferences f = f(context);
        return f != null && f.getBoolean(str, z);
    }

    @DexIgnore
    public String b(Context context) {
        return h(context, "com.misfit.frameworks.profile.createdAt");
    }

    @DexIgnore
    public String d(Context context) {
        return h(context, "com.misfit.frameworks.profile.integrations");
    }

    @DexIgnore
    public String e(Context context) {
        return h(context, "com.misfit.frameworks.profile.registration");
    }

    @DexIgnore
    public String g(Context context) {
        return h(context, "com.misfit.frameworks.profile.registerDate");
    }

    @DexIgnore
    public final String h(Context context, String str) {
        SharedPreferences f = f(context);
        return f != null ? f.getString(str, "") : "";
    }

    @DexIgnore
    public String i(Context context) {
        return h(context, "com.misfit.frameworks.profile.updateAt");
    }

    @DexIgnore
    public String j(Context context) {
        return h(context, "com.misfit.frameworks.profile.accessToken");
    }

    @DexIgnore
    public String k(Context context) {
        return h(context, "com.misfit.frameworks.profile.authType");
    }

    @DexIgnore
    public String l(Context context) {
        return h(context, "com.misfit.frameworks.profile.birthday");
    }

    @DexIgnore
    public String m(Context context) {
        return h(context, "com.misfit.frameworks.profile.email");
    }

    @DexIgnore
    public String n(Context context) {
        return h(context, "com.misfit.frameworks.profile.firstname");
    }

    @DexIgnore
    public Qh5 o(Context context) {
        return Qh5.Companion.a(h(context, "com.misfit.frameworks.profile.gender"));
    }

    @DexIgnore
    public String p(Context context) {
        return h(context, "com.misfit.frameworks.profile.height");
    }

    @DexIgnore
    public String q(Context context) {
        return h(context, "com.misfit.frameworks.profile.userId");
    }

    @DexIgnore
    public String r(Context context) {
        return h(context, "com.misfit.frameworks.profile.lastname");
    }

    @DexIgnore
    public String s(Context context) {
        return h(context, "com.misfit.frameworks.profile.profilePic");
    }

    @DexIgnore
    public String t(Context context) {
        return h(context, "com.misfit.frameworks.profile.units.distance");
    }

    @DexIgnore
    public String u(Context context) {
        return h(context, "com.misfit.frameworks.profile.units.height");
    }

    @DexIgnore
    public String v(Context context) {
        return h(context, "com.misfit.frameworks.profile.units.weight");
    }

    @DexIgnore
    public String w(Context context) {
        return h(context, "com.misfit.frameworks.profile.weight");
    }

    @DexIgnore
    public boolean x(Context context) {
        return a(context, "com.misfit.frameworks.profile.diagnosticEnable", true);
    }

    @DexIgnore
    public final void y(Context context, String str, String str2) {
        SharedPreferences f = f(context);
        if (f != null) {
            SharedPreferences.Editor edit = f.edit();
            edit.putString(str, str2);
            edit.apply();
        }
    }

    @DexIgnore
    public void z(Context context, String str) {
        y(context, "com.misfit.frameworks.profile.accessToken", str);
    }
}
