package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.Ix1;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fs {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public long e;
    @DexIgnore
    public A90 f;
    @DexIgnore
    public /* final */ ArrayList<Hw> g;
    @DexIgnore
    public long h;
    @DexIgnore
    public Gw i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public U5 k;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Fs, Cd6>> l;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Fs, Cd6>> m;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Fs, Cd6>> n;
    @DexIgnore
    public CopyOnWriteArrayList<Coroutine<Fs, Float, Cd6>> o;
    @DexIgnore
    public /* final */ Gg6<Cd6> p;
    @DexIgnore
    public /* final */ Bs q;
    @DexIgnore
    public /* final */ Ds r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public Mw v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ Hs x;
    @DexIgnore
    public /* final */ K5 y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public Fs(Hs hs, K5 k5, int i2) {
        this.x = hs;
        this.y = k5;
        this.z = i2;
        this.a = Ey1.a(hs);
        this.b = E.a("UUID.randomUUID().toString()");
        this.c = "";
        this.d = "";
        this.e = System.currentTimeMillis();
        System.currentTimeMillis();
        this.f = new A90(Ey1.a(this.x), V80.e, this.y.x, this.c, this.d, false, null, null, null, G80.k(new JSONObject(), Jd0.g2, this.b), 448);
        this.g = new ArrayList<>();
        this.h = 6500;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.j = new Handler(myLooper);
            this.l = new CopyOnWriteArrayList<>();
            this.m = new CopyOnWriteArrayList<>();
            this.n = new CopyOnWriteArrayList<>();
            this.o = new CopyOnWriteArrayList<>();
            this.p = new Zr(this);
            this.q = new Bs(this);
            this.r = new Ds(this);
            this.v = new Mw(this.x, this.b, Lw.c, null, null, 24);
            this.w = true;
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Fs(Hs hs, K5 k5, int i2, int i3) {
        this(hs, k5, (i3 & 4) != 0 ? 3 : i2);
    }

    @DexIgnore
    public JSONObject A() {
        return new JSONObject();
    }

    @DexIgnore
    public final void B() {
        Gw gw = this.i;
        if (gw != null) {
            this.j.removeCallbacks(gw);
        }
        Gw gw2 = this.i;
        if (gw2 != null) {
            gw2.b = true;
        }
        this.i = null;
    }

    @DexIgnore
    public final void C() {
        A90 a90 = this.f;
        if (a90 != null) {
            G80.k(a90.n, Jd0.u1, Double.valueOf(Hy1.f(System.currentTimeMillis())));
            if (this.w) {
                D90.i.d(a90);
            }
            this.f = null;
        }
    }

    @DexIgnore
    public long a(O7 o7) {
        return 0;
    }

    @DexIgnore
    public final Fs c(Hg6<? super Fs, Cd6> hg6) {
        if (!this.t) {
            this.m.add(hg6);
        } else if (this.v.d != Lw.b) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final void d() {
        boolean z2 = true;
        if (!this.t) {
            this.t = true;
            this.u = false;
            B();
            this.y.i.remove(this.q);
            this.y.j.remove(this.r);
            C();
            if (this.w) {
                D90 d90 = D90.i;
                Object[] array = this.g.toArray(new Hw[0]);
                if (array != null) {
                    Hw[] hwArr = (Hw[]) array;
                    JSONObject k2 = G80.k(G80.k(new JSONObject(), Jd0.k, u(this.v)), Jd0.g2, this.b);
                    Mw mw = this.v;
                    if (mw.d == Lw.b) {
                        Gy1.b(k2, A(), false, 2, null);
                    } else {
                        G80.k(k2, Jd0.P0, mw.toJSONObject());
                    }
                    if (!(hwArr.length == 0)) {
                        JSONArray jSONArray = new JSONArray();
                        for (Hw hw : hwArr) {
                            jSONArray.put(hw.toJSONObject());
                        }
                        G80.k(k2, Jd0.E0, jSONArray);
                    }
                    String a2 = Ey1.a(this.x);
                    V80 v80 = V80.f;
                    String str = this.y.x;
                    String str2 = this.c;
                    String str3 = this.d;
                    if (Lw.b != this.v.d) {
                        z2 = false;
                    }
                    d90.d(new A90(a2, v80, str, str2, str3, z2, null, null, null, k2, 448));
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            Lw lw = Lw.b;
            Mw mw2 = this.v;
            if (lw == mw2.d) {
                Ky1 ky1 = Ky1.DEBUG;
                Gy1.c(mw2.toJSONObject(), A()).toString(2);
                Iterator<T> it = this.l.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                Ky1 ky12 = Ky1.ERROR;
                Gy1.c(mw2.toJSONObject(), A()).toString(2);
                Iterator<T> it2 = this.m.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.n.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public final void e(float f2) {
        Iterator<T> it = this.o.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void f(long j2) {
        this.h = j2;
    }

    @DexIgnore
    public void g(U5 u5) {
    }

    @DexIgnore
    public final void h(N7 n7) {
        if (n7 instanceof P7) {
            i((P7) n7);
        } else if (n7 instanceof O7) {
            s((O7) n7);
        }
    }

    @DexIgnore
    public void i(P7 p7) {
        if (p7.a == F5.d) {
            return;
        }
        if (p7.b == X6.k.b) {
            l(Lw.o);
        } else {
            l(Lw.g);
        }
    }

    @DexIgnore
    public final void k(Hw hw) {
        this.g.add(hw);
    }

    @DexIgnore
    public final void l(Lw lw) {
        if (!this.t && !this.u) {
            Ky1 ky1 = Ky1.DEBUG;
            this.u = true;
            this.v = Mw.a(this.v, null, null, lw, null, null, 27);
            U5 u5 = this.k;
            if (u5 == null || u5.d) {
                d();
                return;
            }
            int i2 = Xr.d[lw.ordinal()];
            this.y.n(this.k, i2 != 1 ? i2 != 2 ? i2 != 3 ? R5.m : R5.g : R5.n : R5.f);
        }
    }

    @DexIgnore
    public final void m(Mw mw) {
        this.v = mw;
        d();
    }

    @DexIgnore
    public final void n(Gg6<Cd6> gg6) {
        B();
        if (x() > 0) {
            this.i = new Gw(this, gg6);
            Ky1 ky1 = Ky1.DEBUG;
            x();
            Gw gw = this.i;
            if (gw != null) {
                this.j.postDelayed(gw, x());
            }
        }
    }

    @DexIgnore
    public final Fs o(Hg6<? super Fs, Cd6> hg6) {
        if (!this.t) {
            this.l.add(hg6);
        } else if (this.v.d == Lw.b) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public String p(Mw mw) {
        Lw lw;
        if (Xr.b[mw.e.c.ordinal()] == 1) {
            return Ey1.a(X6.n.a(mw.e.d.c));
        }
        switch (Xr.a[mw.d.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                lw = mw.d;
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                lw = Lw.e;
                break;
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                lw = Lw.u;
                break;
            default:
                lw = Lw.u;
                break;
        }
        return Ey1.a(lw);
    }

    @DexIgnore
    public final void q() {
        JSONObject jSONObject;
        if (this.t || this.u) {
            d();
            return;
        }
        U5 w2 = w();
        this.k = w2;
        if (w2 != null) {
            A90 a90 = this.f;
            if (a90 == null || (jSONObject = a90.n) == null) {
                jSONObject = new JSONObject();
            }
            JSONObject c2 = Gy1.c(jSONObject, w2.b(false));
            JSONObject jSONObject2 = new JSONObject();
            boolean z2 = this instanceof Zs;
            byte[] bArr = z2 ? ((Zs) this).G.d : w2 instanceof J6 ? ((J6) w2).m : new byte[0];
            if (!(bArr.length == 0)) {
                int length = bArr.length;
                G80.k(G80.k(G80.k(jSONObject2, Jd0.S0, (length > 500 || (z2 && Wg6.a(this.c, Ey1.a(Yp.z)))) ? "" : Dy1.e(bArr, null, 1, null)), Jd0.T0, Integer.valueOf(length)), Jd0.U0, Long.valueOf(Ix1.a.b(bArr, Ix1.Ai.CRC32)));
            }
            JSONObject c3 = Gy1.c(c2, jSONObject2);
            A90 a902 = this.f;
            if (a902 != null) {
                a902.n = c3;
            }
            K5 k5 = this.y;
            U5 u5 = this.k;
            if (u5 != null) {
                k5.m(u5);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            y();
        }
    }

    @DexIgnore
    public void r(U5 u5) {
        JSONObject jSONObject;
        JSONObject k2;
        this.v = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
        A90 a90 = this.f;
        if (a90 != null) {
            a90.j = false;
        }
        A90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null || (k2 = G80.k(jSONObject, Jd0.k, p(this.v))) == null)) {
            G80.k(k2, Jd0.P0, u5.e.toJSONObject());
        }
        C();
        d();
    }

    @DexIgnore
    public void s(O7 o7) {
    }

    @DexIgnore
    public final Handler t() {
        return this.j;
    }

    @DexIgnore
    public String u(Mw mw) {
        String logName;
        switch (Xr.c[mw.d.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                Mt mt = mw.f;
                return (mt == null || (logName = mt.getLogName()) == null) ? Ey1.a(Lw.u) : logName;
            case 8:
            case 9:
                return Ey1.a(Lw.r);
            default:
                return p(mw);
        }
    }

    @DexIgnore
    public void v(U5 u5) {
        JSONObject jSONObject;
        this.v = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
        A90 a90 = this.f;
        if (a90 != null) {
            a90.j = true;
        }
        A90 a902 = this.f;
        if (!(a902 == null || (jSONObject = a902.n) == null)) {
            G80.k(jSONObject, Jd0.k, Ey1.a(Lw.b));
        }
        if (this.v.d == Lw.b) {
            g(u5);
        }
    }

    @DexIgnore
    public abstract U5 w();

    @DexIgnore
    public long x() {
        return this.h;
    }

    @DexIgnore
    public abstract void y();

    @DexIgnore
    public JSONObject z() {
        return new JSONObject();
    }
}
