package com.fossil;

import com.fossil.Cj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zi1 implements Cj1, Bj1 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Cj1 b;
    @DexIgnore
    public volatile Bj1 c;
    @DexIgnore
    public volatile Bj1 d;
    @DexIgnore
    public Cj1.Ai e;
    @DexIgnore
    public Cj1.Ai f;

    @DexIgnore
    public Zi1(Object obj, Cj1 cj1) {
        Cj1.Ai ai = Cj1.Ai.CLEARED;
        this.e = ai;
        this.f = ai;
        this.a = obj;
        this.b = cj1;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public void a(Bj1 bj1) {
        synchronized (this.a) {
            if (!bj1.equals(this.d)) {
                this.e = Cj1.Ai.FAILED;
                if (this.f != Cj1.Ai.RUNNING) {
                    this.f = Cj1.Ai.RUNNING;
                    this.d.f();
                }
                return;
            }
            this.f = Cj1.Ai.FAILED;
            if (this.b != null) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Bj1, com.fossil.Cj1
    public boolean b() {
        boolean z;
        synchronized (this.a) {
            z = this.c.b() || this.d.b();
        }
        return z;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.Cj1] */
    /* JADX WARN: Type inference failed for: r2v3 */
    @Override // com.fossil.Cj1
    public Cj1 c() {
        Object r2;
        synchronized (this.a) {
            Cj1 cj1 = this.b;
            this = this;
            if (cj1 != null) {
                r2 = this.b.c();
            }
        }
        return r2;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void clear() {
        synchronized (this.a) {
            this.e = Cj1.Ai.CLEARED;
            this.c.clear();
            if (this.f != Cj1.Ai.CLEARED) {
                this.f = Cj1.Ai.CLEARED;
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean d(Bj1 bj1) {
        boolean z;
        synchronized (this.a) {
            z = n() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean e(Bj1 bj1) {
        boolean z;
        synchronized (this.a) {
            z = o() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void f() {
        synchronized (this.a) {
            if (this.e != Cj1.Ai.RUNNING) {
                this.e = Cj1.Ai.RUNNING;
                this.c.f();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean g(Bj1 bj1) {
        if (!(bj1 instanceof Zi1)) {
            return false;
        }
        Zi1 zi1 = (Zi1) bj1;
        return this.c.g(zi1.c) && this.d.g(zi1.d);
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean h() {
        boolean z;
        synchronized (this.a) {
            z = this.e == Cj1.Ai.CLEARED && this.f == Cj1.Ai.CLEARED;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public void i(Bj1 bj1) {
        synchronized (this.a) {
            if (bj1.equals(this.c)) {
                this.e = Cj1.Ai.SUCCESS;
            } else if (bj1.equals(this.d)) {
                this.f = Cj1.Ai.SUCCESS;
            }
            if (this.b != null) {
                this.b.i(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean isRunning() {
        boolean z;
        synchronized (this.a) {
            z = this.e == Cj1.Ai.RUNNING || this.f == Cj1.Ai.RUNNING;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean j() {
        boolean z;
        synchronized (this.a) {
            z = this.e == Cj1.Ai.SUCCESS || this.f == Cj1.Ai.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean k(Bj1 bj1) {
        boolean z;
        synchronized (this.a) {
            z = m() && l(bj1);
        }
        return z;
    }

    @DexIgnore
    public final boolean l(Bj1 bj1) {
        return bj1.equals(this.c) || (this.e == Cj1.Ai.FAILED && bj1.equals(this.d));
    }

    @DexIgnore
    public final boolean m() {
        Cj1 cj1 = this.b;
        return cj1 == null || cj1.k(this);
    }

    @DexIgnore
    public final boolean n() {
        Cj1 cj1 = this.b;
        return cj1 == null || cj1.d(this);
    }

    @DexIgnore
    public final boolean o() {
        Cj1 cj1 = this.b;
        return cj1 == null || cj1.e(this);
    }

    @DexIgnore
    public void p(Bj1 bj1, Bj1 bj12) {
        this.c = bj1;
        this.d = bj12;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void pause() {
        synchronized (this.a) {
            if (this.e == Cj1.Ai.RUNNING) {
                this.e = Cj1.Ai.PAUSED;
                this.c.pause();
            }
            if (this.f == Cj1.Ai.RUNNING) {
                this.f = Cj1.Ai.PAUSED;
                this.d.pause();
            }
        }
    }
}
