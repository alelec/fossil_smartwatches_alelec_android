package com.fossil;

import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fv extends Dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public byte[] N; // = new byte[0];
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public int R; // = -1;
    @DexIgnore
    public float S;
    @DexIgnore
    public N6 T;
    @DexIgnore
    public long U;
    @DexIgnore
    public /* final */ Hw V;
    @DexIgnore
    public /* final */ Gg6<Cd6> W;

    @DexIgnore
    public Fv(Iu iu, short s, Hs hs, K5 k5, int i) {
        super(iu, s, hs, k5, i);
        N6 n6 = N6.d;
        this.T = n6;
        this.V = new Hw(0, n6, new byte[0], null, 9);
        this.W = new Ev(this, k5);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final JSONObject F(byte[] bArr) {
        if (!this.Q) {
            JSONObject jSONObject = new JSONObject();
            if (bArr.length >= 4) {
                long o = Hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                this.M = o;
                G80.k(jSONObject, Jd0.F0, Long.valueOf(o));
                if (this.M == 0) {
                    m(Mw.a(this.v, null, null, Lw.b, null, null, 27));
                } else {
                    N6 a2 = bArr.length >= 5 ? N6.q.a(bArr[4]) : N6.k;
                    this.T = a2;
                    G80.k(jSONObject, Jd0.G0, a2.b);
                    this.V.c = this.T;
                    if (this.s) {
                        Ix.b.a(this.y.x).d(this.T);
                    }
                }
            } else {
                this.v = Mw.a(this.v, null, null, Lw.k, null, null, 27);
            }
            this.Q = true;
            this.E = this.v.d != Lw.b;
            byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(Iu.i.a()).putShort(this.L).array();
            Wg6.b(array, "ByteBuffer.allocate(1 + \u2026                 .array()");
            this.H = array;
            return jSONObject;
        }
        B();
        JSONObject jSONObject2 = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        S(Hy1.o(order.getInt(0)));
        Q(Hy1.o(order.getInt(4)));
        G80.k(G80.k(jSONObject2, Jd0.I, Long.valueOf(U())), Jd0.J, Long.valueOf(T()));
        W();
        this.E = true;
        return jSONObject2;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final boolean G(O7 o7) {
        return o7.a == this.T;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final void J(O7 o7) {
        byte[] b = this.s ? Jx.b.b(this.y.x, this.T, o7.b) : o7.b;
        if (Hy1.n(Hy1.p((byte) (b[0] & 63))) == (this.R + 1) % 64) {
            if (((byte) (b[0] & ((byte) 128))) != ((byte) 0)) {
                f(5000);
                n(this.W);
            } else {
                n(this.p);
            }
            this.R++;
            R(Dy1.a(V(), Dm7.k(b, 1, b.length)));
            float length = (((float) V().length) * 1.0f) / ((float) this.M);
            if (length - this.S > 0.001f || length == 1.0f) {
                this.S = length;
                e(length);
            }
            if (this.U == 0) {
                k(this.V);
            }
            this.U = ((long) b.length) + this.U;
            G80.k(G80.k(G80.k(this.V.e, Jd0.V0, Integer.valueOf(this.R + 1)), Jd0.q3, Long.valueOf(this.U)), Jd0.H0, Integer.valueOf(V().length));
            return;
        }
        this.v = Mw.a(this.v, null, null, Lw.h, null, null, 27);
        this.E = true;
    }

    @DexIgnore
    public void Q(long j) {
        this.P = j;
    }

    @DexIgnore
    public void R(byte[] bArr) {
        this.N = bArr;
    }

    @DexIgnore
    public void S(long j) {
        this.O = j;
    }

    @DexIgnore
    public long T() {
        return this.P;
    }

    @DexIgnore
    public long U() {
        return this.O;
    }

    @DexIgnore
    public byte[] V() {
        return this.N;
    }

    @DexIgnore
    public void W() {
    }
}
