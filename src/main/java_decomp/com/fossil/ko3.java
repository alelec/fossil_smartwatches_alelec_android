package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ko3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public Ko3(Un3 un3, boolean z) {
        this.c = un3;
        this.b = z;
    }

    @DexIgnore
    public final void run() {
        boolean o = this.c.a.o();
        boolean n = this.c.a.n();
        this.c.a.m(this.b);
        if (n == this.b) {
            this.c.a.d().N().b("Default data collection state already set to", Boolean.valueOf(this.b));
        }
        if (this.c.a.o() == o || this.c.a.o() != this.c.a.n()) {
            this.c.a.d().K().c("Default data collection is different than actual status", Boolean.valueOf(this.b), Boolean.valueOf(o));
        }
        this.c.i0();
    }
}
