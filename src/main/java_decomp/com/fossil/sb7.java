package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sb7 implements Qb7 {
    @DexIgnore
    public Ib7 a;
    @DexIgnore
    public Bitmap b;
    @DexIgnore
    public String c;

    @DexIgnore
    public Sb7(Ib7 ib7, Bitmap bitmap, String str) {
        Wg6.c(ib7, "complicationData");
        this.a = ib7;
        this.b = bitmap;
        this.c = str;
    }

    @DexIgnore
    public final Ib7 a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Sb7) {
                Sb7 sb7 = (Sb7) obj;
                if (!Wg6.a(this.a, sb7.a) || !Wg6.a(this.b, sb7.b) || !Wg6.a(this.c, sb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Ib7 ib7 = this.a;
        int hashCode = ib7 != null ? ib7.hashCode() : 0;
        Bitmap bitmap = this.b;
        int hashCode2 = bitmap != null ? bitmap.hashCode() : 0;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UIComplication(complicationData=" + this.a + ", ringBitmap=" + this.b + ", ringPreviewUrl=" + this.c + ")";
    }
}
