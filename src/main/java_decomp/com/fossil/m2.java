package com.fossil;

import android.os.Parcel;
import com.fossil.Ix1;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M2 extends C2 {
    @DexIgnore
    public static /* final */ L2 CREATOR; // = new L2(null);
    @DexIgnore
    public /* final */ Gu1 e;
    @DexIgnore
    public /* final */ Fu1 f;
    @DexIgnore
    public /* final */ Iu1 g;
    @DexIgnore
    public /* final */ byte[] h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore
    public M2(byte b, Gu1 gu1, Fu1 fu1, Iu1 iu1, byte[] bArr, int i2, int i3) {
        super(Lt.n, b, true);
        this.e = gu1;
        this.f = fu1;
        this.g = iu1;
        this.h = bArr;
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public M2(Parcel parcel) {
        super(parcel);
        Gu1 a2 = Gu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.e = a2;
            Fu1 a3 = Fu1.d.a(parcel.readByte());
            if (a3 != null) {
                this.f = a3;
                Iu1 a4 = Iu1.d.a(parcel.readByte());
                if (a4 != null) {
                    this.g = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.h = createByteArray == null ? new byte[0] : createByteArray;
                    this.i = parcel.readInt();
                    this.j = parcel.readInt();
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(super.toJSONObject(), Jd0.j0, Ey1.a(this.e)), Jd0.k5, Ey1.a(this.f)), Jd0.l5, Ey1.a(this.g)), Jd0.U0, Long.valueOf(Ix1.a.b(this.h, Ix1.Ai.CRC32))), Jd0.z5, Integer.valueOf(this.i)), Jd0.A5, Integer.valueOf(this.j));
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.g.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i);
        }
        if (parcel != null) {
            parcel.writeInt(this.j);
        }
    }
}
