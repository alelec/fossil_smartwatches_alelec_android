package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gq5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        String simpleName = Gq5.class.getSimpleName();
        Wg6.b(simpleName, "TimeZoneReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Wg6.c(context, "context");
        Wg6.c(intent, "intent");
        String action = intent.getAction();
        if (Wg6.a("android.intent.action.TIME_SET", action) || Wg6.a("android.intent.action.TIMEZONE_CHANGED", action)) {
            FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver");
            TimeUtils.u0();
        } else if (Wg6.a("android.intent.action.TIME_TICK", action)) {
            TimeZone timeZone = TimeZone.getDefault();
            try {
                Calendar instance = Calendar.getInstance();
                Wg6.b(instance, "calendar");
                Calendar instance2 = Calendar.getInstance();
                Wg6.b(instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                Calendar instance3 = Calendar.getInstance();
                Wg6.b(instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver - DST change");
                    TimeUtils.u0();
                    PortfolioApp.get.instance().b1();
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.e(str, ".timeZoneChangeReceiver - ex=" + e);
            }
        }
    }
}
