package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class To extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Bg b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public To(Bg bg) {
        super(1);
        this.b = bg;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Lp lp) {
        Ac0 ac0;
        Ac0[] a0 = ((Oj) lp).a0();
        int length = a0.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                ac0 = null;
                break;
            }
            ac0 = a0[i];
            if (Wg6.a(ac0.a(), this.b.C.getBundleId()) && ac0.b() == this.b.C.getPackageCrc()) {
                break;
            }
            i++;
        }
        if (ac0 != null) {
            Bg.I(this.b);
        } else {
            Bg.H(this.b);
        }
        return Cd6.a;
    }
}
