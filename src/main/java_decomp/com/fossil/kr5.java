package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Kr5 extends Service {
    @DexIgnore
    public static Mr5 d;
    @DexIgnore
    public int b;
    @DexIgnore
    public Vh5 c; // = Vh5.NOT_START;

    @DexIgnore
    public static void e(Mr5 mr5) {
        d = mr5;
    }

    @DexIgnore
    public void a() {
        Vh5 vh5;
        if (d != null && this.c != (vh5 = Vh5.FINISHED)) {
            this.c = vh5;
            d.b(this.b, vh5);
            d.c(this);
        }
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public void d() {
        Vh5 vh5;
        if (d != null && this.c != (vh5 = Vh5.RUNNING)) {
            this.c = vh5;
            d.b(this.b, vh5);
            d.a(this);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return null;
    }
}
