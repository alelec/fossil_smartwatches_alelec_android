package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Re7 {
    @DexIgnore
    public static Re7 c;
    @DexIgnore
    public Map<Integer, Qe7> a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public Re7(Context context) {
        this.b = context.getApplicationContext();
        HashMap hashMap = new HashMap(3);
        this.a = hashMap;
        hashMap.put(1, new Pe7(context));
        this.a.put(2, new Me7(context));
        this.a.put(4, new Oe7(context));
    }

    @DexIgnore
    public static Re7 a(Context context) {
        Re7 re7;
        synchronized (Re7.class) {
            try {
                if (c == null) {
                    c = new Re7(context);
                }
                re7 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return re7;
    }

    @DexIgnore
    public final void b(String str) {
        Ne7 d = d();
        d.c = str;
        if (!Se7.f(d.a)) {
            d.a = Se7.a(this.b);
        }
        if (!Se7.f(d.b)) {
            d.b = Se7.e(this.b);
        }
        d.d = System.currentTimeMillis();
        for (Map.Entry<Integer, Qe7> entry : this.a.entrySet()) {
            entry.getValue().a(d);
        }
    }

    @DexIgnore
    public final Ne7 c(List<Integer> list) {
        Ne7 e;
        if (list.size() >= 0) {
            for (Integer num : list) {
                Qe7 qe7 = this.a.get(num);
                if (!(qe7 == null || (e = qe7.e()) == null || !Se7.g(e.c))) {
                    return e;
                }
            }
        }
        return new Ne7();
    }

    @DexIgnore
    public final Ne7 d() {
        return c(new ArrayList(Arrays.asList(1, 2, 4)));
    }
}
