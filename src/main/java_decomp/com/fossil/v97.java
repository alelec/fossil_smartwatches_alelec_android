package com.fossil;

import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V97 implements Factory<WatchFaceStickerViewModel> {
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public V97(Provider<WFAssetRepository> provider, Provider<FileRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static V97 a(Provider<WFAssetRepository> provider, Provider<FileRepository> provider2) {
        return new V97(provider, provider2);
    }

    @DexIgnore
    public static WatchFaceStickerViewModel c(WFAssetRepository wFAssetRepository, FileRepository fileRepository) {
        return new WatchFaceStickerViewModel(wFAssetRepository, fileRepository);
    }

    @DexIgnore
    public WatchFaceStickerViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
