package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y5 extends U5 {
    @DexIgnore
    public /* final */ N5 k; // = N5.d;
    @DexIgnore
    public F5 l; // = F5.b;
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore
    public Y5(boolean z, N4 n4) {
        super(V5.c, n4);
        this.m = z;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.t(this.m);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void f(H7 h7) {
        S5 a2;
        k(h7);
        G7 g7 = h7.a;
        if (g7.b == F7.b) {
            a2 = this.l == F5.d ? S5.a(this.e, null, R5.b, null, 5) : S5.a(this.e, null, R5.e, null, 5);
        } else {
            S5 a3 = S5.e.a(g7);
            a2 = S5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public N5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        F5 f5;
        return (h7 instanceof Z6) && ((f5 = ((Z6) h7).b) == F5.d || f5 == F5.b);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.e;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.l = ((Z6) h7).b;
    }
}
