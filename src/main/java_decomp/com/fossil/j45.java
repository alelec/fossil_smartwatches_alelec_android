package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class J45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ OverviewDayChart r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ View v;

    @DexIgnore
    public J45(Object obj, View view, int i, ConstraintLayout constraintLayout, OverviewDayChart overviewDayChart, FlexibleButton flexibleButton, ImageView imageView, FlexibleTextView flexibleTextView, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = overviewDayChart;
        this.s = flexibleButton;
        this.t = imageView;
        this.u = flexibleTextView;
        this.v = view2;
    }
}
