package com.fossil;

import android.content.res.Resources;
import android.widget.SpinnerAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ph0 extends SpinnerAdapter {
    @DexIgnore
    Resources.Theme getDropDownViewTheme();

    @DexIgnore
    void setDropDownViewTheme(Resources.Theme theme);
}
