package com.fossil;

import com.fossil.Bc4;
import java.io.File;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ac4 {
    @DexIgnore
    public /* final */ Bc4.Ci a;

    @DexIgnore
    public Ac4(Bc4.Ci ci) {
        this.a = ci;
    }

    @DexIgnore
    public boolean a() {
        File[] b = this.a.b();
        File[] a2 = this.a.a();
        if (b == null || b.length <= 0) {
            return a2 != null && a2.length > 0;
        }
        return true;
    }

    @DexIgnore
    public void b(Ec4 ec4) {
        ec4.remove();
    }

    @DexIgnore
    public void c(List<Ec4> list) {
        for (Ec4 ec4 : list) {
            b(ec4);
        }
    }

    @DexIgnore
    public List<Ec4> d() {
        X74.f().b("Checking for crash reports...");
        File[] b = this.a.b();
        File[] a2 = this.a.a();
        LinkedList linkedList = new LinkedList();
        if (b != null) {
            for (File file : b) {
                X74.f().b("Found crash report " + file.getPath());
                linkedList.add(new Fc4(file));
            }
        }
        if (a2 != null) {
            for (File file2 : a2) {
                linkedList.add(new Dc4(file2));
            }
        }
        if (linkedList.isEmpty()) {
            X74.f().b("No reports found.");
        }
        return linkedList;
    }
}
