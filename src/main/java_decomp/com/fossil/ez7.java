package com.fossil;

import com.mapped.Af6;
import com.mapped.Il6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ez7 implements Il6 {
    @DexIgnore
    public /* final */ Af6 b;

    @DexIgnore
    public Ez7(Af6 af6) {
        this.b = af6;
    }

    @DexIgnore
    @Override // com.mapped.Il6
    public Af6 h() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineScope(coroutineContext=" + h() + ')';
    }
}
