package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView A;
    @DexIgnore
    public /* final */ LinearLayout B;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat D;
    @DexIgnore
    public /* final */ View E;
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ FlexibleEditText u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    public R35(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, View view2, FlexibleButton flexibleButton, ImageView imageView, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, ImageView imageView2, ConstraintLayout constraintLayout, View view3, ImageView imageView3, LinearLayout linearLayout, ConstraintLayout constraintLayout2, FlexibleSwitchCompat flexibleSwitchCompat, View view4) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = view2;
        this.s = flexibleButton;
        this.t = imageView;
        this.u = flexibleEditText;
        this.v = flexibleTextView;
        this.w = rTLImageView;
        this.x = imageView2;
        this.y = constraintLayout;
        this.z = view3;
        this.A = imageView3;
        this.B = linearLayout;
        this.C = constraintLayout2;
        this.D = flexibleSwitchCompat;
        this.E = view4;
    }
}
