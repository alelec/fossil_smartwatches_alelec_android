package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lz3 {
    @DexIgnore
    public static float a(float f, float f2, float f3, float f4) {
        return (float) Math.hypot((double) (f3 - f), (double) (f4 - f2));
    }

    @DexIgnore
    public static float b(float f, float f2, float f3, float f4, float f5, float f6) {
        return e(a(f, f2, f3, f4), a(f, f2, f5, f4), a(f, f2, f5, f6), a(f, f2, f3, f6));
    }

    @DexIgnore
    public static boolean c(float f, float f2, float f3) {
        return f + f3 >= f2;
    }

    @DexIgnore
    public static float d(float f, float f2, float f3) {
        return ((1.0f - f3) * f) + (f3 * f2);
    }

    @DexIgnore
    public static float e(float f, float f2, float f3, float f4) {
        return (f <= f2 || f <= f3 || f <= f4) ? (f2 <= f3 || f2 <= f4) ? f3 > f4 ? f3 : f4 : f2 : f;
    }
}
