package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vr2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Vr2> CREATOR; // = new Wr2();
    @DexIgnore
    public int b;
    @DexIgnore
    public Tr2 c;
    @DexIgnore
    public Fb3 d;
    @DexIgnore
    public Rq2 e;

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [com.fossil.Rq2] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Vr2(int r3, com.fossil.Tr2 r4, android.os.IBinder r5, android.os.IBinder r6) {
        /*
            r2 = this;
            r1 = 0
            r2.<init>()
            r2.b = r3
            r2.c = r4
            if (r5 != 0) goto L_0x0012
            r0 = r1
        L_0x000b:
            r2.d = r0
            if (r6 != 0) goto L_0x0017
        L_0x000f:
            r2.e = r1
            return
        L_0x0012:
            com.fossil.Fb3 r0 = com.fossil.Gb3.e(r5)
            goto L_0x000b
        L_0x0017:
            if (r6 == 0) goto L_0x000f
            java.lang.String r0 = "com.google.android.gms.location.internal.IFusedLocationProviderCallback"
            android.os.IInterface r0 = r6.queryLocalInterface(r0)
            boolean r1 = r0 instanceof com.fossil.Rq2
            if (r1 == 0) goto L_0x0027
            com.fossil.Rq2 r0 = (com.fossil.Rq2) r0
            r1 = r0
            goto L_0x000f
        L_0x0027:
            com.fossil.Tq2 r1 = new com.fossil.Tq2
            r1.<init>(r6)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vr2.<init>(int, com.fossil.Tr2, android.os.IBinder, android.os.IBinder):void");
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder = null;
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.t(parcel, 2, this.c, i, false);
        Fb3 fb3 = this.d;
        Bd2.m(parcel, 3, fb3 == null ? null : fb3.asBinder(), false);
        Rq2 rq2 = this.e;
        if (rq2 != null) {
            iBinder = rq2.asBinder();
        }
        Bd2.m(parcel, 4, iBinder, false);
        Bd2.b(parcel, a2);
    }
}
