package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.TypedValue;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nl0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Typeface b;

            @DexIgnore
            public Aii(Typeface typeface) {
                this.b = typeface;
            }

            @DexIgnore
            public void run() {
                Ai.this.d(this.b);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ int b;

            @DexIgnore
            public Bii(int i) {
                this.b = i;
            }

            @DexIgnore
            public void run() {
                Ai.this.c(this.b);
            }
        }

        @DexIgnore
        public final void a(int i, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new Bii(i));
        }

        @DexIgnore
        public final void b(Typeface typeface, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new Aii(typeface));
        }

        @DexIgnore
        public abstract void c(int i);

        @DexIgnore
        public abstract void d(Typeface typeface);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public static /* final */ Object a; // = new Object();
            @DexIgnore
            public static Method b;
            @DexIgnore
            public static boolean c;

            @DexIgnore
            public static void a(Resources.Theme theme) {
                synchronized (a) {
                    if (!c) {
                        try {
                            Method declaredMethod = Resources.Theme.class.getDeclaredMethod("rebase", new Class[0]);
                            b = declaredMethod;
                            declaredMethod.setAccessible(true);
                        } catch (NoSuchMethodException e) {
                            Log.i("ResourcesCompat", "Failed to retrieve rebase() method", e);
                        }
                        c = true;
                    }
                    if (b != null) {
                        try {
                            b.invoke(theme, new Object[0]);
                        } catch (IllegalAccessException | InvocationTargetException e2) {
                            Log.i("ResourcesCompat", "Failed to invoke rebase() method via reflection", e2);
                            b = null;
                        }
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Bii {
            @DexIgnore
            public static void a(Resources.Theme theme) {
                theme.rebase();
            }
        }

        @DexIgnore
        public static void a(Resources.Theme theme) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                Bii.a(theme);
            } else if (i >= 23) {
                Aii.a(theme);
            }
        }
    }

    @DexIgnore
    public static Drawable a(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        return Build.VERSION.SDK_INT >= 21 ? resources.getDrawable(i, theme) : resources.getDrawable(i);
    }

    @DexIgnore
    public static Typeface b(Context context, int i) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return e(context, i, new TypedValue(), 0, null, null, false);
    }

    @DexIgnore
    public static Typeface c(Context context, int i, TypedValue typedValue, int i2, Ai ai) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return e(context, i, typedValue, i2, ai, null, true);
    }

    @DexIgnore
    public static void d(Context context, int i, Ai ai, Handler handler) throws Resources.NotFoundException {
        Pn0.d(ai);
        if (context.isRestricted()) {
            ai.a(-4, handler);
        } else {
            e(context, i, new TypedValue(), 0, ai, handler, false);
        }
    }

    @DexIgnore
    public static Typeface e(Context context, int i, TypedValue typedValue, int i2, Ai ai, Handler handler, boolean z) {
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        Typeface f = f(context, resources, typedValue, i, i2, ai, handler, z);
        if (f != null || ai != null) {
            return f;
        }
        throw new Resources.NotFoundException("Font resource ID #0x" + Integer.toHexString(i) + " could not be retrieved.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface f(android.content.Context r9, android.content.res.Resources r10, android.util.TypedValue r11, int r12, int r13, com.fossil.Nl0.Ai r14, android.os.Handler r15, boolean r16) {
        /*
            java.lang.CharSequence r0 = r11.string
            if (r0 == 0) goto L_0x009e
            java.lang.String r8 = r0.toString()
            java.lang.String r0 = "res/"
            boolean r0 = r8.startsWith(r0)
            if (r0 != 0) goto L_0x0018
            if (r14 == 0) goto L_0x0016
            r0 = -3
            r14.a(r0, r15)
        L_0x0016:
            r0 = 0
        L_0x0017:
            return r0
        L_0x0018:
            android.graphics.Typeface r0 = com.fossil.Sl0.f(r10, r12, r13)
            if (r0 == 0) goto L_0x0024
            if (r14 == 0) goto L_0x0017
            r14.b(r0, r15)
            goto L_0x0017
        L_0x0024:
            java.lang.String r0 = r8.toLowerCase()     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            java.lang.String r1 = ".xml"
            boolean r0 = r0.endsWith(r1)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            if (r0 == 0) goto L_0x0056
            android.content.res.XmlResourceParser r0 = r10.getXml(r12)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            com.fossil.Kl0$Ai r1 = com.fossil.Kl0.b(r0, r10)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            if (r1 != 0) goto L_0x0049
            java.lang.String r0 = "ResourcesCompat"
            java.lang.String r1 = "Failed to find font-family tag"
            android.util.Log.e(r0, r1)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            if (r14 == 0) goto L_0x0047
            r0 = -3
            r14.a(r0, r15)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
        L_0x0047:
            r0 = 0
            goto L_0x0017
        L_0x0049:
            r0 = r9
            r2 = r10
            r3 = r12
            r4 = r13
            r5 = r14
            r6 = r15
            r7 = r16
            android.graphics.Typeface r0 = com.fossil.Sl0.c(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            goto L_0x0017
        L_0x0056:
            android.graphics.Typeface r0 = com.fossil.Sl0.d(r9, r10, r12, r8, r13)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            if (r14 == 0) goto L_0x0017
            if (r0 == 0) goto L_0x0081
            r14.b(r0, r15)     // Catch:{ XmlPullParserException -> 0x0062, IOException -> 0x0086 }
            goto L_0x0017
        L_0x0062:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to parse xml resource "
            r1.append(r2)
            r1.append(r8)
            java.lang.String r2 = "ResourcesCompat"
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
        L_0x0079:
            if (r14 == 0) goto L_0x007f
            r0 = -3
            r14.a(r0, r15)
        L_0x007f:
            r0 = 0
            goto L_0x0017
        L_0x0081:
            r1 = -3
            r14.a(r1, r15)
            goto L_0x0017
        L_0x0086:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to read xml resource "
            r1.append(r2)
            r1.append(r8)
            java.lang.String r2 = "ResourcesCompat"
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x0079
        L_0x009e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Resource \""
            r0.append(r1)
            java.lang.String r1 = r10.getResourceName(r12)
            r0.append(r1)
            java.lang.String r1 = "\" ("
            r0.append(r1)
            java.lang.String r1 = java.lang.Integer.toHexString(r12)
            r0.append(r1)
            java.lang.String r1 = ") is not a Font: "
            r0.append(r1)
            r0.append(r11)
            android.content.res.Resources$NotFoundException r1 = new android.content.res.Resources$NotFoundException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nl0.f(android.content.Context, android.content.res.Resources, android.util.TypedValue, int, int, com.fossil.Nl0$Ai, android.os.Handler, boolean):android.graphics.Typeface");
    }
}
