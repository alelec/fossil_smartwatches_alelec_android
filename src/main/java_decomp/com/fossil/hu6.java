package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Oi5;
import com.mapped.Lc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hu6 extends RecyclerView.g<Bi> {
    @DexIgnore
    public ArrayList<WorkoutSetting> a; // = new ArrayList<>();
    @DexIgnore
    public Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void M3(WorkoutSetting workoutSetting, boolean z);

        @DexIgnore
        void m5(WorkoutSetting workoutSetting, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Gg5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Hu6 hu6, Gg5 gg5) {
            super(gg5.n());
            Wg6.c(gg5, "binding");
            this.a = gg5;
        }

        @DexIgnore
        public final Gg5 a() {
            return this.a;
        }

        @DexIgnore
        public final void b(WorkoutSetting workoutSetting) {
            Wg6.c(workoutSetting, "workout");
            View n = this.a.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            Oi5.Ai ai = Oi5.Companion;
            Lc6<Integer, Integer> a2 = ai.a(ai.d(workoutSetting.getType(), workoutSetting.getMode()));
            String c = Um5.c(context, a2.getSecond().intValue());
            String c2 = Um5.c(context, 2131886635);
            Gg5 gg5 = this.a;
            gg5.x.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = gg5.s;
            Wg6.b(flexibleTextView, "it.ftvName");
            flexibleTextView.setText(c);
            FlexibleSwitchCompat flexibleSwitchCompat = gg5.y;
            Wg6.b(flexibleSwitchCompat, "it.swEnabled");
            flexibleSwitchCompat.setChecked(workoutSetting.getEnable());
            FlexibleCheckBox flexibleCheckBox = gg5.q;
            Wg6.b(flexibleCheckBox, "it.cbConfirmation");
            flexibleCheckBox.setChecked(workoutSetting.getAskMeFirst());
            Hr7 hr7 = Hr7.a;
            String format = String.format("%d " + c2, Arrays.copyOf(new Object[]{Integer.valueOf(workoutSetting.getStartLatency())}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = gg5.r;
            Wg6.b(flexibleTextView2, "it.ftvLatency");
            flexibleTextView2.setText(format);
        }

        @DexIgnore
        public final void c(boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.a.t;
                Wg6.b(flexibleTextView, "binding.ftvUserConfirmation");
                flexibleTextView.setVisibility(0);
                FlexibleCheckBox flexibleCheckBox = this.a.q;
                Wg6.b(flexibleCheckBox, "binding.cbConfirmation");
                flexibleCheckBox.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = this.a.t;
            Wg6.b(flexibleTextView2, "binding.ftvUserConfirmation");
            flexibleTextView2.setVisibility(8);
            FlexibleCheckBox flexibleCheckBox2 = this.a.q;
            Wg6.b(flexibleCheckBox2, "binding.cbConfirmation");
            flexibleCheckBox2.setVisibility(8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Hu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ Bi c;

        @DexIgnore
        public Ci(Hu6 hu6, WorkoutSetting workoutSetting, Bi bi) {
            this.a = hu6;
            this.b = workoutSetting;
            this.c = bi;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setEnable(z);
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            Wg6.b(flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(z);
            this.c.c(this.b.getEnable());
            Ai ai = this.a.b;
            if (ai != null) {
                ai.m5(this.b, z);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Hu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSetting b;
        @DexIgnore
        public /* final */ /* synthetic */ Bi c;

        @DexIgnore
        public Di(Hu6 hu6, WorkoutSetting workoutSetting, Bi bi) {
            this.a = hu6;
            this.b = workoutSetting;
            this.c = bi;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            this.b.setAskMeFirst(z);
            FlexibleCheckBox flexibleCheckBox = this.c.a().q;
            Wg6.b(flexibleCheckBox, "holder.binding.cbConfirmation");
            flexibleCheckBox.setChecked(this.b.getAskMeFirst());
            Ai ai = this.a.b;
            if (ai != null) {
                ai.M3(this.b, z);
            }
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final List<WorkoutSetting> h() {
        return this.a;
    }

    @DexIgnore
    public void i(Bi bi, int i) {
        Wg6.c(bi, "holder");
        WorkoutSetting workoutSetting = this.a.get(i);
        Wg6.b(workoutSetting, "mData[position]");
        WorkoutSetting workoutSetting2 = workoutSetting;
        bi.a().y.setOnCheckedChangeListener(null);
        bi.a().q.setOnCheckedChangeListener(null);
        bi.b(workoutSetting2);
        bi.c(workoutSetting2.getEnable());
        bi.a().y.setOnCheckedChangeListener(new Ci(this, workoutSetting2, bi));
        bi.a().q.setOnCheckedChangeListener(new Di(this, workoutSetting2, bi));
    }

    @DexIgnore
    public Bi j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Gg5 z = Gg5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemWorkoutSettingBindin\u2026.context), parent, false)");
        return new Bi(this, z);
    }

    @DexIgnore
    public final void k(List<WorkoutSetting> list) {
        Wg6.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingAdapter", "setData, " + list.size());
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        i(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
