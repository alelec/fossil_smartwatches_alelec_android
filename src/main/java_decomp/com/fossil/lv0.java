package com.fossil;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lv0 {
    @DexIgnore
    public /* final */ RecyclerView.m a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Rect c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Lv0 {
        @DexIgnore
        public Ai(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int d(View view) {
            return ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).rightMargin + this.a.U(view);
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + this.a.T(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + this.a.S(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int g(View view) {
            return this.a.R(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int h() {
            return this.a.p0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int i() {
            return this.a.p0() - this.a.g0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int j() {
            return this.a.g0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int k() {
            return this.a.q0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int l() {
            return this.a.Y();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int m() {
            return this.a.f0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int n() {
            return (this.a.p0() - this.a.f0()) - this.a.g0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int p(View view) {
            this.a.o0(view, true, this.c);
            return this.c.right;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int q(View view) {
            this.a.o0(view, true, this.c);
            return this.c.left;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public void r(int i) {
            this.a.D0(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Lv0 {
        @DexIgnore
        public Bi(RecyclerView.m mVar) {
            super(mVar, null);
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int d(View view) {
            return ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).bottomMargin + this.a.P(view);
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int e(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin + this.a.S(view) + ((ViewGroup.MarginLayoutParams) layoutParams).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int f(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin + this.a.T(view) + ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int g(View view) {
            return this.a.V(view) - ((ViewGroup.MarginLayoutParams) ((RecyclerView.LayoutParams) view.getLayoutParams())).topMargin;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int h() {
            return this.a.X();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int i() {
            return this.a.X() - this.a.e0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int j() {
            return this.a.e0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int k() {
            return this.a.Y();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int l() {
            return this.a.q0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int m() {
            return this.a.h0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int n() {
            return (this.a.X() - this.a.h0()) - this.a.e0();
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int p(View view) {
            this.a.o0(view, true, this.c);
            return this.c.bottom;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public int q(View view) {
            this.a.o0(view, true, this.c);
            return this.c.top;
        }

        @DexIgnore
        @Override // com.fossil.Lv0
        public void r(int i) {
            this.a.E0(i);
        }
    }

    @DexIgnore
    public Lv0(RecyclerView.m mVar) {
        this.b = RecyclerView.UNDEFINED_DURATION;
        this.c = new Rect();
        this.a = mVar;
    }

    @DexIgnore
    public /* synthetic */ Lv0(RecyclerView.m mVar, Ai ai) {
        this(mVar);
    }

    @DexIgnore
    public static Lv0 a(RecyclerView.m mVar) {
        return new Ai(mVar);
    }

    @DexIgnore
    public static Lv0 b(RecyclerView.m mVar, int i) {
        if (i == 0) {
            return a(mVar);
        }
        if (i == 1) {
            return c(mVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    @DexIgnore
    public static Lv0 c(RecyclerView.m mVar) {
        return new Bi(mVar);
    }

    @DexIgnore
    public abstract int d(View view);

    @DexIgnore
    public abstract int e(View view);

    @DexIgnore
    public abstract int f(View view);

    @DexIgnore
    public abstract int g(View view);

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract int i();

    @DexIgnore
    public abstract int j();

    @DexIgnore
    public abstract int k();

    @DexIgnore
    public abstract int l();

    @DexIgnore
    public abstract int m();

    @DexIgnore
    public abstract int n();

    @DexIgnore
    public int o() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return n() - this.b;
    }

    @DexIgnore
    public abstract int p(View view);

    @DexIgnore
    public abstract int q(View view);

    @DexIgnore
    public abstract void r(int i);

    @DexIgnore
    public void s() {
        this.b = n();
    }
}
