package com.fossil;

import android.content.Context;
import com.fossil.Rd7;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bd7 extends Rd7 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Bd7(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean c(Pd7 pd7) {
        return "content".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public Rd7.Ai f(Pd7 pd7, int i) throws IOException {
        return new Rd7.Ai(j(pd7), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public InputStream j(Pd7 pd7) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(pd7.d);
    }
}
