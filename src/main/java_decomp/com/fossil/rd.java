package com.fossil;

import com.fossil.fitness.AlgorithmManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.DeviceImplementation$clearCache$1", f = "DeviceImplementation.kt", l = {}, m = "invokeSuspend")
public final class rd extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ e60 d;
    @DexIgnore
    public /* final */ /* synthetic */ qy1 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rd(e60 e60, qy1 qy1, qn7 qn7) {
        super(2, qn7);
        this.d = e60;
        this.e = qy1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        rd rdVar = new rd(this.d, this.e, qn7);
        rdVar.b = (iv7) obj;
        throw null;
        //return rdVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        rd rdVar = new rd(this.d, this.e, qn7);
        rdVar.b = iv7;
        return rdVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.c == 0) {
            el7.b(obj);
            u.f3499a.a(this.d.d.x);
            AlgorithmManager.defaultManager().clearCache();
            this.e.o(tl7.f3441a);
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
