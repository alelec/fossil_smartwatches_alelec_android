package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class If0 extends Drawable implements Drawable.Callback {
    @DexIgnore
    public Ci b;
    @DexIgnore
    public Rect c;
    @DexIgnore
    public Drawable d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public int f; // = 255;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public Runnable j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public Bi m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            If0.this.a(true);
            If0.this.invalidateSelf();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Drawable.Callback {
        @DexIgnore
        public Drawable.Callback b;

        @DexIgnore
        public Drawable.Callback a() {
            Drawable.Callback callback = this.b;
            this.b = null;
            return callback;
        }

        @DexIgnore
        public Bi b(Drawable.Callback callback) {
            this.b = callback;
            return this;
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            Drawable.Callback callback = this.b;
            if (callback != null) {
                callback.scheduleDrawable(drawable, runnable, j);
            }
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            Drawable.Callback callback = this.b;
            if (callback != null) {
                callback.unscheduleDrawable(drawable, runnable);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci extends Drawable.ConstantState {
        @DexIgnore
        public int A; // = 0;
        @DexIgnore
        public int B; // = 0;
        @DexIgnore
        public boolean C;
        @DexIgnore
        public ColorFilter D;
        @DexIgnore
        public boolean E;
        @DexIgnore
        public ColorStateList F;
        @DexIgnore
        public PorterDuff.Mode G;
        @DexIgnore
        public boolean H;
        @DexIgnore
        public boolean I;
        @DexIgnore
        public /* final */ If0 a;
        @DexIgnore
        public Resources b;
        @DexIgnore
        public int c; // = 160;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public SparseArray<Drawable.ConstantState> f;
        @DexIgnore
        public Drawable[] g;
        @DexIgnore
        public int h;
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public Rect k;
        @DexIgnore
        public boolean l; // = false;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public int n;
        @DexIgnore
        public int o;
        @DexIgnore
        public int p;
        @DexIgnore
        public int q;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public boolean u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public boolean w;
        @DexIgnore
        public boolean x; // = true;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public int z;

        @DexIgnore
        public Ci(Ci ci, If0 if0, Resources resources) {
            this.a = if0;
            this.b = resources != null ? resources : ci != null ? ci.b : null;
            int f2 = If0.f(resources, ci != null ? ci.c : 0);
            this.c = f2;
            if (ci != null) {
                this.d = ci.d;
                this.e = ci.e;
                this.v = true;
                this.w = true;
                this.i = ci.i;
                this.l = ci.l;
                this.x = ci.x;
                this.y = ci.y;
                this.z = ci.z;
                this.A = ci.A;
                this.B = ci.B;
                this.C = ci.C;
                this.D = ci.D;
                this.E = ci.E;
                this.F = ci.F;
                this.G = ci.G;
                this.H = ci.H;
                this.I = ci.I;
                if (ci.c == f2) {
                    if (ci.j) {
                        this.k = new Rect(ci.k);
                        this.j = true;
                    }
                    if (ci.m) {
                        this.n = ci.n;
                        this.o = ci.o;
                        this.p = ci.p;
                        this.q = ci.q;
                        this.m = true;
                    }
                }
                if (ci.r) {
                    this.s = ci.s;
                    this.r = true;
                }
                if (ci.t) {
                    this.u = ci.u;
                    this.t = true;
                }
                Drawable[] drawableArr = ci.g;
                this.g = new Drawable[drawableArr.length];
                this.h = ci.h;
                SparseArray<Drawable.ConstantState> sparseArray = ci.f;
                if (sparseArray != null) {
                    this.f = sparseArray.clone();
                } else {
                    this.f = new SparseArray<>(this.h);
                }
                int i2 = this.h;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null) {
                        Drawable.ConstantState constantState = drawableArr[i3].getConstantState();
                        if (constantState != null) {
                            this.f.put(i3, constantState);
                        } else {
                            this.g[i3] = drawableArr[i3];
                        }
                    }
                }
                return;
            }
            this.g = new Drawable[10];
            this.h = 0;
        }

        @DexIgnore
        public final int a(Drawable drawable) {
            int i2 = this.h;
            if (i2 >= this.g.length) {
                o(i2, i2 + 10);
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback(this.a);
            this.g[i2] = drawable;
            this.h++;
            this.e |= drawable.getChangingConfigurations();
            p();
            this.k = null;
            this.j = false;
            this.m = false;
            this.v = false;
            return i2;
        }

        @DexIgnore
        public final void b(Resources.Theme theme) {
            if (theme != null) {
                e();
                int i2 = this.h;
                Drawable[] drawableArr = this.g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null && drawableArr[i3].canApplyTheme()) {
                        drawableArr[i3].applyTheme(theme);
                        this.e |= drawableArr[i3].getChangingConfigurations();
                    }
                }
                y(theme.getResources());
            }
        }

        @DexIgnore
        public boolean c() {
            synchronized (this) {
                if (this.v) {
                    return this.w;
                }
                e();
                this.v = true;
                int i2 = this.h;
                Drawable[] drawableArr = this.g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3].getConstantState() == null) {
                        this.w = false;
                        return false;
                    }
                }
                this.w = true;
                return true;
            }
        }

        @DexIgnore
        public boolean canApplyTheme() {
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                if (drawable == null) {
                    Drawable.ConstantState constantState = this.f.get(i3);
                    if (constantState != null && constantState.canApplyTheme()) {
                        return true;
                    }
                } else if (drawable.canApplyTheme()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public void d() {
            this.m = true;
            e();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            this.o = -1;
            this.n = -1;
            this.q = 0;
            this.p = 0;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.n) {
                    this.n = intrinsicWidth;
                }
                int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.o) {
                    this.o = intrinsicHeight;
                }
                int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.p) {
                    this.p = minimumWidth;
                }
                int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.q) {
                    this.q = minimumHeight;
                }
            }
        }

        @DexIgnore
        public final void e() {
            SparseArray<Drawable.ConstantState> sparseArray = this.f;
            if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g[this.f.keyAt(i2)] = s(this.f.valueAt(i2).newDrawable(this.b));
                }
                this.f = null;
            }
        }

        @DexIgnore
        public final int f() {
            return this.g.length;
        }

        @DexIgnore
        public final Drawable g(int i2) {
            int indexOfKey;
            Drawable drawable = this.g[i2];
            if (drawable != null) {
                return drawable;
            }
            SparseArray<Drawable.ConstantState> sparseArray = this.f;
            if (sparseArray == null || (indexOfKey = sparseArray.indexOfKey(i2)) < 0) {
                return null;
            }
            Drawable s2 = s(this.f.valueAt(indexOfKey).newDrawable(this.b));
            this.g[i2] = s2;
            this.f.removeAt(indexOfKey);
            if (this.f.size() != 0) {
                return s2;
            }
            this.f = null;
            return s2;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.d | this.e;
        }

        @DexIgnore
        public final int h() {
            return this.h;
        }

        @DexIgnore
        public final int i() {
            if (!this.m) {
                d();
            }
            return this.o;
        }

        @DexIgnore
        public final int j() {
            if (!this.m) {
                d();
            }
            return this.q;
        }

        @DexIgnore
        public final int k() {
            if (!this.m) {
                d();
            }
            return this.p;
        }

        @DexIgnore
        public final Rect l() {
            Rect rect = null;
            if (this.i) {
                return null;
            }
            if (this.k != null || this.j) {
                return this.k;
            }
            e();
            Rect rect2 = new Rect();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getPadding(rect2)) {
                    if (rect == null) {
                        rect = new Rect(0, 0, 0, 0);
                    }
                    int i4 = rect2.left;
                    if (i4 > rect.left) {
                        rect.left = i4;
                    }
                    int i5 = rect2.top;
                    if (i5 > rect.top) {
                        rect.top = i5;
                    }
                    int i6 = rect2.right;
                    if (i6 > rect.right) {
                        rect.right = i6;
                    }
                    int i7 = rect2.bottom;
                    if (i7 > rect.bottom) {
                        rect.bottom = i7;
                    }
                }
            }
            this.j = true;
            this.k = rect;
            return rect;
        }

        @DexIgnore
        public final int m() {
            if (!this.m) {
                d();
            }
            return this.n;
        }

        @DexIgnore
        public final int n() {
            if (this.r) {
                return this.s;
            }
            e();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            int opacity = i2 > 0 ? drawableArr[0].getOpacity() : -2;
            for (int i3 = 1; i3 < i2; i3++) {
                opacity = Drawable.resolveOpacity(opacity, drawableArr[i3].getOpacity());
            }
            this.s = opacity;
            this.r = true;
            return opacity;
        }

        @DexIgnore
        public void o(int i2, int i3) {
            Drawable[] drawableArr = new Drawable[i3];
            System.arraycopy(this.g, 0, drawableArr, 0, i2);
            this.g = drawableArr;
        }

        @DexIgnore
        public void p() {
            this.r = false;
            this.t = false;
        }

        @DexIgnore
        public final boolean q() {
            return this.l;
        }

        @DexIgnore
        public abstract void r();

        @DexIgnore
        public final Drawable s(Drawable drawable) {
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(this.z);
            }
            Drawable mutate = drawable.mutate();
            mutate.setCallback(this.a);
            return mutate;
        }

        @DexIgnore
        public final void t(boolean z2) {
            this.l = z2;
        }

        @DexIgnore
        public final void u(int i2) {
            this.A = i2;
        }

        @DexIgnore
        public final void v(int i2) {
            this.B = i2;
        }

        @DexIgnore
        public final boolean w(int i2, int i3) {
            boolean z2;
            int i4 = this.h;
            Drawable[] drawableArr = this.g;
            boolean z3 = false;
            int i5 = 0;
            while (i5 < i4) {
                if (drawableArr[i5] != null) {
                    z2 = Build.VERSION.SDK_INT >= 23 ? drawableArr[i5].setLayoutDirection(i2) : false;
                    if (i5 == i3) {
                        i5++;
                        z3 = z2;
                    }
                }
                z2 = z3;
                i5++;
                z3 = z2;
            }
            this.z = i2;
            return z3;
        }

        @DexIgnore
        public final void x(boolean z2) {
            this.i = z2;
        }

        @DexIgnore
        public final void y(Resources resources) {
            if (resources != null) {
                this.b = resources;
                int f2 = If0.f(resources, this.c);
                int i2 = this.c;
                this.c = f2;
                if (i2 != f2) {
                    this.m = false;
                    this.j = false;
                }
            }
        }
    }

    @DexIgnore
    public static int f(Resources resources, int i2) {
        int i3 = resources == null ? i2 : resources.getDisplayMetrics().densityDpi;
        if (i3 == 0) {
            return 160;
        }
        return i3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r15) {
        /*
            r14 = this;
            r12 = 255(0xff, double:1.26E-321)
            r1 = 1
            r2 = 0
            r10 = 0
            r14.g = r1
            long r4 = android.os.SystemClock.uptimeMillis()
            android.graphics.drawable.Drawable r0 = r14.d
            if (r0 == 0) goto L_0x0059
            long r6 = r14.k
            int r3 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r3 == 0) goto L_0x0021
            int r3 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x0045
            int r3 = r14.f
            r0.setAlpha(r3)
            r14.k = r10
        L_0x0021:
            r0 = r2
        L_0x0022:
            android.graphics.drawable.Drawable r3 = r14.e
            if (r3 == 0) goto L_0x006e
            long r6 = r14.l
            int r8 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x0038
            int r8 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r8 > 0) goto L_0x005c
            r3.setVisible(r2, r2)
            r1 = 0
            r14.e = r1
            r14.l = r10
        L_0x0038:
            if (r15 == 0) goto L_0x0044
            if (r0 == 0) goto L_0x0044
            java.lang.Runnable r0 = r14.j
            r2 = 16
            long r2 = r2 + r4
            r14.scheduleSelf(r0, r2)
        L_0x0044:
            return
        L_0x0045:
            long r6 = r6 - r4
            long r6 = r6 * r12
            int r3 = (int) r6
            com.fossil.If0$Ci r6 = r14.b
            int r6 = r6.A
            int r3 = r3 / r6
            int r3 = 255 - r3
            int r6 = r14.f
            int r3 = r3 * r6
            int r3 = r3 / 255
            r0.setAlpha(r3)
            r0 = r1
            goto L_0x0022
        L_0x0059:
            r14.k = r10
            goto L_0x0021
        L_0x005c:
            long r6 = r6 - r4
            long r6 = r6 * r12
            int r0 = (int) r6
            com.fossil.If0$Ci r2 = r14.b
            int r2 = r2.B
            int r0 = r0 / r2
            int r2 = r14.f
            int r0 = r0 * r2
            int r0 = r0 / 255
            r3.setAlpha(r0)
            r0 = r1
            goto L_0x0038
        L_0x006e:
            r14.l = r10
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.If0.a(boolean):void");
    }

    @DexIgnore
    public void applyTheme(Resources.Theme theme) {
        this.b.b(theme);
    }

    @DexIgnore
    public abstract Ci b();

    @DexIgnore
    public int c() {
        return this.h;
    }

    @DexIgnore
    public boolean canApplyTheme() {
        return this.b.canApplyTheme();
    }

    @DexIgnore
    public final void d(Drawable drawable) {
        if (this.m == null) {
            this.m = new Bi();
        }
        Bi bi = this.m;
        bi.b(drawable.getCallback());
        drawable.setCallback(bi);
        try {
            if (this.b.A <= 0 && this.g) {
                drawable.setAlpha(this.f);
            }
            if (this.b.E) {
                drawable.setColorFilter(this.b.D);
            } else {
                if (this.b.H) {
                    Am0.o(drawable, this.b.F);
                }
                if (this.b.I) {
                    Am0.p(drawable, this.b.G);
                }
            }
            drawable.setVisible(isVisible(), true);
            drawable.setDither(this.b.x);
            drawable.setState(getState());
            drawable.setLevel(getLevel());
            drawable.setBounds(getBounds());
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(getLayoutDirection());
            }
            if (Build.VERSION.SDK_INT >= 19) {
                drawable.setAutoMirrored(this.b.C);
            }
            Rect rect = this.c;
            if (Build.VERSION.SDK_INT >= 21 && rect != null) {
                drawable.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            drawable.setCallback(this.m.a());
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.d;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        Drawable drawable2 = this.e;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    @DexIgnore
    public final boolean e() {
        return isAutoMirrored() && Am0.f(this) == 1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(int r10) {
        /*
            r9 = this;
            r8 = 0
            r1 = 1
            r6 = 0
            r0 = 0
            int r2 = r9.h
            if (r10 != r2) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            long r2 = android.os.SystemClock.uptimeMillis()
            com.fossil.If0$Ci r4 = r9.b
            int r4 = r4.B
            if (r4 <= 0) goto L_0x006c
            android.graphics.drawable.Drawable r4 = r9.e
            if (r4 == 0) goto L_0x001b
            r4.setVisible(r0, r0)
        L_0x001b:
            android.graphics.drawable.Drawable r0 = r9.d
            if (r0 == 0) goto L_0x0067
            r9.e = r0
            com.fossil.If0$Ci r0 = r9.b
            int r0 = r0.B
            long r4 = (long) r0
            long r4 = r4 + r2
            r9.l = r4
        L_0x0029:
            if (r10 < 0) goto L_0x0074
            com.fossil.If0$Ci r0 = r9.b
            int r4 = r0.h
            if (r10 >= r4) goto L_0x0074
            android.graphics.drawable.Drawable r0 = r0.g(r10)
            r9.d = r0
            r9.h = r10
            if (r0 == 0) goto L_0x0048
            com.fossil.If0$Ci r4 = r9.b
            int r4 = r4.A
            if (r4 <= 0) goto L_0x0045
            long r4 = (long) r4
            long r2 = r2 + r4
            r9.k = r2
        L_0x0045:
            r9.d(r0)
        L_0x0048:
            long r2 = r9.k
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0054
            long r2 = r9.l
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x0062
        L_0x0054:
            java.lang.Runnable r0 = r9.j
            if (r0 != 0) goto L_0x007a
            com.fossil.If0$Ai r0 = new com.fossil.If0$Ai
            r0.<init>()
            r9.j = r0
        L_0x005f:
            r9.a(r1)
        L_0x0062:
            r9.invalidateSelf()
            r0 = r1
            goto L_0x0009
        L_0x0067:
            r9.e = r8
            r9.l = r6
            goto L_0x0029
        L_0x006c:
            android.graphics.drawable.Drawable r4 = r9.d
            if (r4 == 0) goto L_0x0029
            r4.setVisible(r0, r0)
            goto L_0x0029
        L_0x0074:
            r9.d = r8
            r0 = -1
            r9.h = r0
            goto L_0x0048
        L_0x007a:
            r9.unscheduleSelf(r0)
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.If0.g(int):boolean");
    }

    @DexIgnore
    public int getAlpha() {
        return this.f;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.b.getChangingConfigurations();
    }

    @DexIgnore
    public final Drawable.ConstantState getConstantState() {
        if (!this.b.c()) {
            return null;
        }
        this.b.d = getChangingConfigurations();
        return this.b;
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.d;
    }

    @DexIgnore
    public void getHotspotBounds(Rect rect) {
        Rect rect2 = this.c;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        if (this.b.q()) {
            return this.b.i();
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return -1;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        if (this.b.q()) {
            return this.b.m();
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return -1;
    }

    @DexIgnore
    public int getMinimumHeight() {
        if (this.b.q()) {
            return this.b.j();
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return 0;
    }

    @DexIgnore
    public int getMinimumWidth() {
        if (this.b.q()) {
            return this.b.k();
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return 0;
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.d;
        if (drawable == null || !drawable.isVisible()) {
            return -2;
        }
        return this.b.n();
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        Drawable drawable = this.d;
        if (drawable != null) {
            drawable.getOutline(outline);
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        boolean padding;
        Rect l2 = this.b.l();
        if (l2 != null) {
            rect.set(l2);
            padding = (l2.right | ((l2.left | l2.top) | l2.bottom)) != 0;
        } else {
            Drawable drawable = this.d;
            padding = drawable != null ? drawable.getPadding(rect) : super.getPadding(rect);
        }
        if (e()) {
            int i2 = rect.left;
            rect.left = rect.right;
            rect.right = i2;
        }
        return padding;
    }

    @DexIgnore
    public void h(Ci ci) {
        this.b = ci;
        int i2 = this.h;
        if (i2 >= 0) {
            Drawable g2 = ci.g(i2);
            this.d = g2;
            if (g2 != null) {
                d(g2);
            }
        }
        this.e = null;
    }

    @DexIgnore
    public final void i(Resources resources) {
        this.b.y(resources);
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        Ci ci = this.b;
        if (ci != null) {
            ci.p();
        }
        if (drawable == this.d && getCallback() != null) {
            getCallback().invalidateDrawable(this);
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.b.C;
    }

    @DexIgnore
    public void jumpToCurrentState() {
        boolean z;
        boolean z2 = true;
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
            this.e = null;
            z = true;
        } else {
            z = false;
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
            if (this.g) {
                this.d.setAlpha(this.f);
            }
        }
        if (this.l != 0) {
            this.l = 0;
            z = true;
        }
        if (this.k != 0) {
            this.k = 0;
        } else {
            z2 = z;
        }
        if (z2) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.i && super.mutate() == this) {
            Ci b2 = b();
            b2.r();
            h(b2);
            this.i = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            drawable2.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i2) {
        return this.b.w(i2, c());
    }

    @DexIgnore
    public boolean onLevelChange(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            return drawable2.setLevel(i2);
        }
        return false;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            return drawable2.setState(iArr);
        }
        return false;
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        if (drawable == this.d && getCallback() != null) {
            getCallback().scheduleDrawable(this, runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        if (!this.g || this.f != i2) {
            this.g = true;
            this.f = i2;
            Drawable drawable = this.d;
            if (drawable == null) {
                return;
            }
            if (this.k == 0) {
                drawable.setAlpha(i2);
            } else {
                a(false);
            }
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Ci ci = this.b;
        if (ci.C != z) {
            ci.C = z;
            Drawable drawable = this.d;
            if (drawable != null) {
                Am0.j(drawable, z);
            }
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Ci ci = this.b;
        ci.E = true;
        if (ci.D != colorFilter) {
            ci.D = colorFilter;
            Drawable drawable = this.d;
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
        }
    }

    @DexIgnore
    public void setDither(boolean z) {
        Ci ci = this.b;
        if (ci.x != z) {
            ci.x = z;
            Drawable drawable = this.d;
            if (drawable != null) {
                drawable.setDither(z);
            }
        }
    }

    @DexIgnore
    public void setHotspot(float f2, float f3) {
        Drawable drawable = this.d;
        if (drawable != null) {
            Am0.k(drawable, f2, f3);
        }
    }

    @DexIgnore
    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        Rect rect = this.c;
        if (rect == null) {
            this.c = new Rect(i2, i3, i4, i5);
        } else {
            rect.set(i2, i3, i4, i5);
        }
        Drawable drawable = this.d;
        if (drawable != null) {
            Am0.l(drawable, i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Ci ci = this.b;
        ci.H = true;
        if (ci.F != colorStateList) {
            ci.F = colorStateList;
            Am0.o(this.d, colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Ci ci = this.b;
        ci.I = true;
        if (ci.G != mode) {
            ci.G = mode;
            Am0.p(this.d, mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setVisible(z, z2);
        }
        Drawable drawable2 = this.d;
        if (drawable2 != null) {
            drawable2.setVisible(z, z2);
        }
        return visible;
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.d && getCallback() != null) {
            getCallback().unscheduleDrawable(this, runnable);
        }
    }
}
