package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vb7 implements Qb7 {
    @DexIgnore
    public Pb7 a;
    @DexIgnore
    public Bitmap b;
    @DexIgnore
    public String c;

    @DexIgnore
    public Vb7(Pb7 pb7, Bitmap bitmap, String str) {
        Wg6.c(pb7, "tickerData");
        this.a = pb7;
        this.b = bitmap;
        this.c = str;
    }

    @DexIgnore
    public final Bitmap a() {
        return this.b;
    }

    @DexIgnore
    public final Pb7 b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Vb7) {
                Vb7 vb7 = (Vb7) obj;
                if (!Wg6.a(this.a, vb7.a) || !Wg6.a(this.b, vb7.b) || !Wg6.a(this.c, vb7.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Pb7 pb7 = this.a;
        int hashCode = pb7 != null ? pb7.hashCode() : 0;
        Bitmap bitmap = this.b;
        int hashCode2 = bitmap != null ? bitmap.hashCode() : 0;
        String str = this.c;
        if (str != null) {
            i = str.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UITicker(tickerData=" + this.a + ", tickerBitmap=" + this.b + ", tickerPreviewUrl=" + this.c + ")";
    }
}
