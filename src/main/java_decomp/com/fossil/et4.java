package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Et4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<Ss4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<Lt4> {
    }

    @DexIgnore
    public final String a(Ss4 ss4) {
        try {
            return new Gson().t(ss4);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String b(Date date) {
        String str;
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.n.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                return str;
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            str = null;
        }
    }

    @DexIgnore
    public final String c(Lt4 lt4) {
        try {
            return new Gson().t(lt4);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final Ss4 d(String str) {
        try {
            return (Ss4) new Gson().l(str, new Ai().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final Lt4 e(String str) {
        try {
            return (Lt4) new Gson().l(str, new Bi().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final Date f(String str) {
        Date date;
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.n.get();
            if (simpleDateFormat != null) {
                date = simpleDateFormat.parse(str);
                return date;
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            date = null;
        }
    }
}
