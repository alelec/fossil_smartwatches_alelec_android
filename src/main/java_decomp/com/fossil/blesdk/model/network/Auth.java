package com.fossil.blesdk.model.network;

import android.os.SystemClock;
import com.fossil.E;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Auth {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    @Vu3("uid")
    public String b;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_REFRESH_TOKEN)
    public String c;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_ACCESS_TOKEN)
    public String d;
    @DexIgnore
    @Vu3("accessTokenExpiresAt")
    public Date e;
    @DexIgnore
    @Vu3("accessTokenExpiresIn")
    public Integer f;

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final int b() {
        Integer num = this.f;
        if (num != null) {
            return num.intValue() - ((int) ((SystemClock.elapsedRealtime() - this.a) / ((long) 1000)));
        }
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Auth) {
                Auth auth = (Auth) obj;
                if (!Wg6.a(this.b, auth.b) || !Wg6.a(this.c, auth.c) || !Wg6.a(this.d, auth.d) || !Wg6.a(this.e, auth.e) || !Wg6.a(this.f, auth.f)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Date date = this.e;
        int hashCode4 = date != null ? date.hashCode() : 0;
        Integer num = this.f;
        if (num != null) {
            i = num.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder e2 = E.e("Auth(uid=");
        e2.append(this.b);
        e2.append(", refreshToken=");
        e2.append(this.c);
        e2.append(", accessToken=");
        e2.append(this.d);
        e2.append(", accessTokenExpiresAt=");
        e2.append(this.e);
        e2.append(", accessTokenExpiresIn=");
        e2.append(this.f);
        e2.append(")");
        return e2.toString();
    }
}
