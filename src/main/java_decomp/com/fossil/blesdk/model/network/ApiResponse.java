package com.fossil.blesdk.model.network;

import com.mapped.Vu3;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApiResponse<T> {
    @DexIgnore
    @Vu3(CloudLogWriter.ITEMS_PARAM)
    public List<T> a; // = new ArrayList();

    @DexIgnore
    public final List<T> a() {
        return this.a;
    }
}
