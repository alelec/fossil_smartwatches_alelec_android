package com.fossil.blesdk.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Base64;
import com.fossil.Dy1;
import com.fossil.Et7;
import com.fossil.Id0;
import com.fossil.Ix1;
import com.fossil.Lx0;
import com.fossil.M80;
import com.fossil.Pw0;
import com.fossil.Vt7;
import com.fossil.Z;
import com.fossil.common.cipher.TextEncryption;
import com.mapped.Lc6;
import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xh;
import java.security.SecureRandom;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SdkDatabase extends Oh {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static SdkDatabase b;
    @DexIgnore
    public static /* final */ Xh c; // = new a(1, 2);
    @DexIgnore
    public static /* final */ Xh d; // = new b(2, 3);
    @DexIgnore
    public static /* final */ Xh e; // = new c(3, 4);
    @DexIgnore
    public static /* final */ Xh f; // = new d(4, 5);
    @DexIgnore
    public static /* final */ e g; // = new e(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Xh {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("DROP TABLE ActivityFile");
            lx0.execSQL("CREATE TABLE DeviceFile(deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, PRIMARY KEY(deviceMacAddress, fileType, fileIndex))");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Xh {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE DeviceFile_New(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, isCompleted INTEGER NOT NULL)");
            Cursor query = lx0.query("SELECT * from DeviceFile");
            Wg6.b(query, "database.query(\"SELECT * from DeviceFile\")");
            int columnIndex = query.getColumnIndex("deviceMacAddress");
            int columnIndex2 = query.getColumnIndex("fileType");
            int columnIndex3 = query.getColumnIndex("fileIndex");
            int columnIndex4 = query.getColumnIndex("rawData");
            int columnIndex5 = query.getColumnIndex("fileLength");
            int columnIndex6 = query.getColumnIndex("fileCrc");
            int columnIndex7 = query.getColumnIndex("createdTimeStamp");
            while (query.moveToNext()) {
                String string = query.getString(columnIndex);
                Wg6.b(string, "oldDataCursor.getString(\u2026iceMacAddressColumnIndex)");
                byte b = (byte) query.getShort(columnIndex2);
                byte b2 = (byte) query.getShort(columnIndex3);
                byte[] blob = query.getBlob(columnIndex4);
                Wg6.b(blob, "oldDataCursor.getBlob(rawDataColumnIndex)");
                long j = query.getLong(columnIndex5);
                long j2 = query.getLong(columnIndex6);
                long j3 = query.getLong(columnIndex7);
                int i = j2 == Ix1.a.b(blob, Ix1.Ai.CRC32) ? 1 : 0;
                lx0.execSQL("Insert into DeviceFile_New(deviceMacAddress, fileType, fileIndex, rawData, fileLength, fileCrc, createdTimeStamp, isCompleted) values ('" + string + "', " + ((int) b) + ", " + ((int) b2) + ", X'" + Dy1.e(blob, null, 1, null) + "', " + j + ", " + j2 + ", " + j3 + ", " + i + ')');
            }
            lx0.execSQL("DROP TABLE DeviceFile");
            lx0.execSQL("ALTER TABLE DeviceFile_New RENAME TO DeviceFile");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Xh {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Xh {
        @DexIgnore
        public d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* synthetic */ e(Qg6 qg6) {
        }

        @DexIgnore
        public final SdkDatabase a(Context context, byte[] bArr) {
            Oh.Ai a2 = Pw0.a(context, SdkDatabase.class, "device.sdbk");
            a2.b(SdkDatabase.c, SdkDatabase.d, SdkDatabase.e, SdkDatabase.f);
            a2.g(new SupportFactory(bArr));
            a2.c();
            Oh d = a2.d();
            Wg6.b(d, "Room.databaseBuilder(con\u2026                 .build()");
            return (SdkDatabase) d;
        }

        @DexIgnore
        public final Lc6<byte[], String> b() {
            boolean z = false;
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[128];
            for (int i = 0; i < 128; i++) {
                bArr[i] = (byte) ((byte) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(secureRandom.nextInt(64)));
            }
            TextEncryption textEncryption = TextEncryption.j;
            String encodeToString = Base64.encodeToString(bArr, 2);
            Wg6.b(encodeToString, "Base64.encodeToString(passPhrase, Base64.NO_WRAP)");
            String o = textEncryption.o(encodeToString);
            if (o == null || Vt7.l(o)) {
                z = true;
            }
            if (!z) {
                return new Lc6<>(bArr, o);
            }
            return null;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0100  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0131  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0175  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.blesdk.database.SdkDatabase c() {
            /*
            // Method dump skipped, instructions count: 376
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.database.SdkDatabase.e.c():com.fossil.blesdk.database.SdkDatabase");
        }

        @DexIgnore
        public final byte[] d() {
            byte[] bArr = null;
            Context a2 = Id0.i.a();
            if (a2 == null) {
                return null;
            }
            SharedPreferences sharedPreferences = a2.getSharedPreferences("device.sdbk", 0);
            String string = sharedPreferences.getString("ppa", null);
            if (string == null) {
                M80.c.a(SdkDatabase.a, "Encrypted passphrase does not existed.", new Object[0]);
                Lc6<byte[], String> b = SdkDatabase.g.b();
                if (b != null) {
                    sharedPreferences.edit().putString("ppa", b.getSecond()).apply();
                    return b.getFirst();
                }
                M80.c.a(SdkDatabase.a, "Fail to generate new passphrase.", new Object[0]);
                return null;
            }
            String n = TextEncryption.j.n(string);
            if (n != null) {
                bArr = n.getBytes(Et7.a);
                Wg6.b(bArr, "(this as java.lang.String).getBytes(charset)");
            }
            if (bArr != null) {
                return bArr;
            }
            M80.c.a(SdkDatabase.a, "Fail to decrypt existing passphrase.", new Object[0]);
            sharedPreferences.edit().remove("ppa").apply();
            Lc6<byte[], String> b2 = SdkDatabase.g.b();
            if (b2 != null) {
                sharedPreferences.edit().putString("ppa", b2.getSecond()).apply();
                return b2.getFirst();
            }
            M80.c.a(SdkDatabase.a, "Fail to generate new passphrase.", new Object[0]);
            return bArr;
        }
    }

    /*
    static {
        String simpleName = SdkDatabase.class.getSimpleName();
        Wg6.b(simpleName, "SdkDatabase::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public abstract Z a();
}
