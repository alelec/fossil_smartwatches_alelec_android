package com.fossil.blesdk.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import com.fossil.E;
import com.fossil.Et7;
import com.fossil.G0;
import com.fossil.Id0;
import com.fossil.M80;
import com.fossil.Pw0;
import com.fossil.Q;
import com.fossil.Vt7;
import com.fossil.common.cipher.TextEncryption;
import com.mapped.Lc6;
import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.File;
import java.security.SecureRandom;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UIDatabase extends Oh {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static String b;
    @DexIgnore
    public static UIDatabase c;
    @DexIgnore
    public static /* final */ a d; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
        }

        @DexIgnore
        public final UIDatabase a(Context context, String str, byte[] bArr) {
            Oh.Ai a2 = Pw0.a(context, UIDatabase.class, str);
            a2.g(new SupportFactory(bArr));
            Oh d = a2.d();
            Wg6.b(d, "Room.databaseBuilder(con\u2026                 .build()");
            return (UIDatabase) d;
        }

        @DexIgnore
        public final Lc6<byte[], String> b() {
            boolean z = false;
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr = new byte[128];
            for (int i = 0; i < 128; i++) {
                bArr[i] = (byte) ((byte) "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(secureRandom.nextInt(64)));
            }
            TextEncryption textEncryption = TextEncryption.j;
            String encodeToString = Base64.encodeToString(bArr, 2);
            Wg6.b(encodeToString, "Base64.encodeToString(passPhrase, Base64.NO_WRAP)");
            String o = textEncryption.o(encodeToString);
            if (o == null || Vt7.l(o)) {
                z = true;
            }
            if (!z) {
                return new Lc6<>(bArr, o);
            }
            return null;
        }

        @DexIgnore
        public final byte[] c(String str) {
            byte[] bArr = null;
            Context a2 = Id0.i.a();
            if (a2 == null) {
                return null;
            }
            SharedPreferences sharedPreferences = a2.getSharedPreferences(str, 0);
            String string = sharedPreferences.getString("ppa", null);
            if (string == null) {
                M80.c.a(UIDatabase.a, "Encrypted passphrase does not existed.", new Object[0]);
                Lc6<byte[], String> b = UIDatabase.d.b();
                if (b != null) {
                    sharedPreferences.edit().putString("ppa", b.getSecond()).apply();
                    return b.getFirst();
                }
                M80.c.a(UIDatabase.a, "Fail to generate new passphrase.", new Object[0]);
                return null;
            }
            String n = TextEncryption.j.n(string);
            if (n != null) {
                bArr = n.getBytes(Et7.a);
                Wg6.b(bArr, "(this as java.lang.String).getBytes(charset)");
            }
            if (bArr != null) {
                return bArr;
            }
            M80.c.a(UIDatabase.a, "Fail to decrypt existing passphrase.", new Object[0]);
            sharedPreferences.edit().remove("ppa").apply();
            Lc6<byte[], String> b2 = UIDatabase.d.b();
            if (b2 != null) {
                sharedPreferences.edit().putString("ppa", b2.getSecond()).apply();
                return b2.getFirst();
            }
            M80.c.a(UIDatabase.a, "Fail to generate new passphrase.", new Object[0]);
            return bArr;
        }

        @DexIgnore
        public final String d() {
            String str;
            synchronized (this) {
                if (UIDatabase.b == null) {
                    String i = Id0.i.i();
                    if (!(i == null || Vt7.l(i))) {
                        UIDatabase.b = i + "_ui.sdbk";
                    }
                }
                str = UIDatabase.b;
            }
            return str;
        }

        @DexIgnore
        public final UIDatabase e() {
            UIDatabase uIDatabase;
            boolean z = false;
            synchronized (this) {
                if (UIDatabase.c == null) {
                    Context a2 = Id0.i.a();
                    if (a2 != null) {
                        String d = UIDatabase.d.d();
                        if (d == null || Vt7.l(d)) {
                            z = true;
                        }
                        if (!z) {
                            M80 m80 = M80.c;
                            String str = UIDatabase.a;
                            m80.a(str, "databaseName=" + d, new Object[0]);
                            byte[] c = UIDatabase.d.c(d);
                            if (c != null) {
                                M80 m802 = M80.c;
                                String str2 = UIDatabase.a;
                                StringBuilder e = E.e("passPhrase=");
                                e.append(new String(c, Et7.a));
                                m802.a(str2, e.toString(), new Object[0]);
                                Q q = Q.a;
                                SQLiteDatabase.loadLibs(a2);
                                File databasePath = a2.getDatabasePath(d);
                                Wg6.b(databasePath, "ctxt.getDatabasePath(dbName)");
                                if (!q.a(databasePath, c)) {
                                    M80.c.a(UIDatabase.a, "Fail to open database, drop database and create new one now.", new Object[0]);
                                    File databasePath2 = a2.getDatabasePath(d);
                                    if (databasePath2 != null) {
                                        databasePath2.delete();
                                    }
                                }
                                UIDatabase.c = UIDatabase.d.a(a2, d, c);
                            } else {
                                M80.c.a(UIDatabase.a, "Fail to get passphrase, skip opening database.", new Object[0]);
                            }
                        } else {
                            M80.c.a(UIDatabase.a, "Fail to get database name, skip opening database.", new Object[0]);
                        }
                    } else {
                        M80.c.a(UIDatabase.a, "Application context is not set, skip opening database.", new Object[0]);
                    }
                }
                uIDatabase = UIDatabase.c;
            }
            return uIDatabase;
        }
    }

    /*
    static {
        String simpleName = UIDatabase.class.getSimpleName();
        Wg6.b(simpleName, "UIDatabase::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public abstract G0 a();
}
