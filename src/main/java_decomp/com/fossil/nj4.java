package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Nj4 {
    DEFAULT {
        @DexIgnore
        @Override // com.fossil.Nj4
        public JsonElement serialize(Long l) {
            return new Jj4((Number) l);
        }
    },
    STRING {
        @DexIgnore
        @Override // com.fossil.Nj4
        public JsonElement serialize(Long l) {
            return new Jj4(String.valueOf(l));
        }
    };

    @DexIgnore
    public abstract JsonElement serialize(Long l);
}
