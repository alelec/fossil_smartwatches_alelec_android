package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cn4 extends Jn4 {
    @DexIgnore
    public static int b(boolean[] zArr, int i, int[] iArr, boolean z) {
        int length = iArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            zArr[i] = iArr[i2] != 0;
            i++;
        }
        return 9;
    }

    @DexIgnore
    public static int f(String str, int i) {
        int i2 = 0;
        int i3 = 1;
        for (int length = str.length() - 1; length >= 0; length--) {
            i2 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(length)) * i3;
            i3++;
            if (i3 > i) {
                i3 = 1;
            }
        }
        return i2 % 47;
    }

    @DexIgnore
    public static void g(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            iArr[i2] = ((1 << (8 - i2)) & i) == 0 ? 0 : 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jn4, com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (kl4 == Kl4.CODE_93) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_93, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.Jn4
    public boolean[] c(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            boolean[] zArr = new boolean[(((str.length() + 2 + 2) * 9) + 1)];
            g(Bn4.a[47], iArr);
            int b = b(zArr, 0, iArr, true);
            for (int i = 0; i < length; i++) {
                g(Bn4.a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(i))], iArr);
                b += b(zArr, b, iArr, true);
            }
            int f = f(str, 20);
            g(Bn4.a[f], iArr);
            int b2 = b + b(zArr, b, iArr, true);
            g(Bn4.a[f(str + "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".charAt(f), 15)], iArr);
            int b3 = b2 + b(zArr, b2, iArr, true);
            g(Bn4.a[47], iArr);
            zArr[b3 + b(zArr, b3, iArr, true)] = true;
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }
}
