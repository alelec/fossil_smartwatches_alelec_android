package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X51 {
    @DexIgnore
    public /* final */ Bitmap.Config a;
    @DexIgnore
    public /* final */ ColorSpace b;
    @DexIgnore
    public /* final */ E81 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ P18 f;
    @DexIgnore
    public /* final */ W71 g;
    @DexIgnore
    public /* final */ S71 h;
    @DexIgnore
    public /* final */ S71 i;

    @DexIgnore
    public X51(Bitmap.Config config, ColorSpace colorSpace, E81 e81, boolean z, boolean z2, P18 p18, W71 w71, S71 s71, S71 s712) {
        Wg6.c(config, "config");
        Wg6.c(e81, "scale");
        Wg6.c(p18, "headers");
        Wg6.c(w71, "parameters");
        Wg6.c(s71, "networkCachePolicy");
        Wg6.c(s712, "diskCachePolicy");
        this.a = config;
        this.b = colorSpace;
        this.c = e81;
        this.d = z;
        this.e = z2;
        this.f = p18;
        this.g = w71;
        this.h = s71;
        this.i = s712;
    }

    @DexIgnore
    public final boolean a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final ColorSpace c() {
        return this.b;
    }

    @DexIgnore
    public final Bitmap.Config d() {
        return this.a;
    }

    @DexIgnore
    public final S71 e() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof X51) {
                X51 x51 = (X51) obj;
                if (!Wg6.a(this.a, x51.a) || !Wg6.a(this.b, x51.b) || !Wg6.a(this.c, x51.c) || this.d != x51.d || this.e != x51.e || !Wg6.a(this.f, x51.f) || !Wg6.a(this.g, x51.g) || !Wg6.a(this.h, x51.h) || !Wg6.a(this.i, x51.i)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final P18 f() {
        return this.f;
    }

    @DexIgnore
    public final S71 g() {
        return this.h;
    }

    @DexIgnore
    public final E81 h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 1;
        int i3 = 0;
        Bitmap.Config config = this.a;
        int hashCode = config != null ? config.hashCode() : 0;
        ColorSpace colorSpace = this.b;
        int hashCode2 = colorSpace != null ? colorSpace.hashCode() : 0;
        E81 e81 = this.c;
        int hashCode3 = e81 != null ? e81.hashCode() : 0;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        boolean z2 = this.e;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        P18 p18 = this.f;
        int hashCode4 = p18 != null ? p18.hashCode() : 0;
        W71 w71 = this.g;
        int hashCode5 = w71 != null ? w71.hashCode() : 0;
        S71 s71 = this.h;
        int hashCode6 = s71 != null ? s71.hashCode() : 0;
        S71 s712 = this.i;
        if (s712 != null) {
            i3 = s712.hashCode();
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i4) * 31) + i2) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i3;
    }

    @DexIgnore
    public String toString() {
        return "Options(config=" + this.a + ", colorSpace=" + this.b + ", scale=" + this.c + ", allowInexactSize=" + this.d + ", allowRgb565=" + this.e + ", headers=" + this.f + ", parameters=" + this.g + ", networkCachePolicy=" + this.h + ", diskCachePolicy=" + this.i + ")";
    }
}
