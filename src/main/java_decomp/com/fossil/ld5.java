package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ld5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat w;

    @DexIgnore
    public Ld5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, View view2, ConstraintLayout constraintLayout2, FlexibleSwitchCompat flexibleSwitchCompat) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = imageView;
        this.u = view2;
        this.v = constraintLayout2;
        this.w = flexibleSwitchCompat;
    }

    @DexIgnore
    @Deprecated
    public static Ld5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Ld5) ViewDataBinding.p(layoutInflater, 2131558659, viewGroup, z, obj);
    }

    @DexIgnore
    public static Ld5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
