package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.fossil.Ig0;
import com.fossil.Jg0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ag0 implements Ig0, AdapterView.OnItemClickListener {
    @DexIgnore
    public Context b;
    @DexIgnore
    public LayoutInflater c;
    @DexIgnore
    public Cg0 d;
    @DexIgnore
    public ExpandedMenuView e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Ig0.Ai i;
    @DexIgnore
    public Ai j;
    @DexIgnore
    public int k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends BaseAdapter {
        @DexIgnore
        public int b; // = -1;

        @DexIgnore
        public Ai() {
            a();
        }

        @DexIgnore
        public void a() {
            Eg0 x = Ag0.this.d.x();
            if (x != null) {
                ArrayList<Eg0> B = Ag0.this.d.B();
                int size = B.size();
                for (int i = 0; i < size; i++) {
                    if (B.get(i) == x) {
                        this.b = i;
                        return;
                    }
                }
            }
            this.b = -1;
        }

        @DexIgnore
        public Eg0 b(int i) {
            ArrayList<Eg0> B = Ag0.this.d.B();
            int i2 = Ag0.this.f + i;
            int i3 = this.b;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return B.get(i2);
        }

        @DexIgnore
        public int getCount() {
            int size = Ag0.this.d.B().size() - Ag0.this.f;
            return this.b < 0 ? size : size - 1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object getItem(int i) {
            return b(i);
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                Ag0 ag0 = Ag0.this;
                view2 = ag0.c.inflate(ag0.h, viewGroup, false);
            } else {
                view2 = view;
            }
            ((Jg0.Ai) view2).f(b(i), 0);
            return view2;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Ag0(int i2, int i3) {
        this.h = i2;
        this.g = i3;
    }

    @DexIgnore
    public Ag0(Context context, int i2) {
        this(i2, 0);
        this.b = context;
        this.c = LayoutInflater.from(context);
    }

    @DexIgnore
    public ListAdapter a() {
        if (this.j == null) {
            this.j = new Ai();
        }
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z) {
        Ig0.Ai ai = this.i;
        if (ai != null) {
            ai.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z) {
        Ai ai = this.j;
        if (ai != null) {
            ai.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean e(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean f(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
        this.i = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public int getId() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
        if (this.g != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, this.g);
            this.b = contextThemeWrapper;
            this.c = LayoutInflater.from(contextThemeWrapper);
        } else if (this.b != null) {
            this.b = context;
            if (this.c == null) {
                this.c = LayoutInflater.from(context);
            }
        }
        this.d = cg0;
        Ai ai = this.j;
        if (ai != null) {
            ai.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
        m((Bundle) parcelable);
    }

    @DexIgnore
    public Jg0 j(ViewGroup viewGroup) {
        if (this.e == null) {
            this.e = (ExpandedMenuView) this.c.inflate(Re0.abc_expanded_menu_layout, viewGroup, false);
            if (this.j == null) {
                this.j = new Ai();
            }
            this.e.setAdapter((ListAdapter) this.j);
            this.e.setOnItemClickListener(this);
        }
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        if (!ng0.hasVisibleItems()) {
            return false;
        }
        new Dg0(ng0).d(null);
        Ig0.Ai ai = this.i;
        if (ai != null) {
            ai.c(ng0);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        if (this.e == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        n(bundle);
        return bundle;
    }

    @DexIgnore
    public void m(Bundle bundle) {
        SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.e.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    public void n(Bundle bundle) {
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        ExpandedMenuView expandedMenuView = this.e;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.d.O(this.j.b(i2), this, 0);
    }
}
