package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tb7 implements Qb7 {
    @DexIgnore
    public Mb7 a;

    @DexIgnore
    public Tb7(Mb7 mb7) {
        Wg6.c(mb7, "textData");
        this.a = mb7;
    }

    @DexIgnore
    public final Mb7 a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Tb7) && Wg6.a(this.a, ((Tb7) obj).a));
    }

    @DexIgnore
    public int hashCode() {
        Mb7 mb7 = this.a;
        if (mb7 != null) {
            return mb7.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "UIText(textData=" + this.a + ")";
    }
}
