package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yz4 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new Ai().getType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<List<? extends GFitWODistance>> {
    }

    @DexIgnore
    public final List<GFitWODistance> a(String str) {
        Wg6.c(str, "data");
        if (str.length() == 0) {
            return Hm7.e();
        }
        try {
            Object l = this.a.l(str, this.b);
            Wg6.b(l, "mGson.fromJson(data, mType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListGFitWODistance: ");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return Hm7.e();
        }
    }

    @DexIgnore
    public final String b(List<GFitWODistance> list) {
        Wg6.c(list, "distances");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String u = this.a.u(list, this.b);
            Wg6.b(u, "mGson.toJson(distances, mType)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return "";
        }
    }
}
