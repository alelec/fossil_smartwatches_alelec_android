package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Oh;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mw0 {
    @DexIgnore
    public /* final */ Set<LiveData> a; // = Collections.newSetFromMap(new IdentityHashMap());
    @DexIgnore
    public /* final */ Oh b;

    @DexIgnore
    public Mw0(Oh oh) {
        this.b = oh;
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new Uw0(this.b, this, z, callable, strArr);
    }

    @DexIgnore
    public void b(LiveData liveData) {
        this.a.add(liveData);
    }

    @DexIgnore
    public void c(LiveData liveData) {
        this.a.remove(liveData);
    }
}
