package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.Oi5;
import com.mapped.Lc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pi6 extends BaseFragment implements Oi6 {
    @DexIgnore
    public G37<D75> g;
    @DexIgnore
    public Ni6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public final void K6(Bf5 bf5, WorkoutSession workoutSession) {
        int i2 = 0;
        if (bf5 != null) {
            View n = bf5.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            Oi5.Ai ai = Oi5.Companion;
            Lc6<Integer, Integer> a2 = ai.a(ai.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = Um5.c(context, a2.getSecond().intValue());
            bf5.w.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = bf5.v;
            Wg6.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            FlexibleTextView flexibleTextView2 = bf5.t;
            Wg6.b(flexibleTextView2, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            flexibleTextView2.setText(String.valueOf(heartRate != null ? Lr7.b(heartRate.getAverage()) : 0));
            FlexibleTextView flexibleTextView3 = bf5.r;
            Wg6.b(flexibleTextView3, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i2 = heartRate2.getMax();
            }
            flexibleTextView3.setText(String.valueOf(i2));
            FlexibleTextView flexibleTextView4 = bf5.u;
            Wg6.b(flexibleTextView4, "it.ftvWorkoutTime");
            flexibleTextView4.setText(TimeUtils.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            String d = ThemeManager.l.a().d("dianaHeartRateTab");
            if (d != null) {
                bf5.w.setColorFilter(Color.parseColor(d));
            }
            String d2 = ThemeManager.l.a().d("nonBrandLineColor");
            if (d2 != null) {
                bf5.x.setBackgroundColor(Color.parseColor(d2));
            }
        }
    }

    @DexIgnore
    public final void L6() {
        D75 a2;
        TodayHeartRateChart todayHeartRateChart;
        G37<D75> g37 = this.g;
        if (g37 == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (g37 != null && (a2 = g37.a()) != null && (todayHeartRateChart = a2.w) != null) {
            todayHeartRateChart.o("maxHeartRate", "lowestHeartRate");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ni6 ni6) {
        M6(ni6);
    }

    @DexIgnore
    public void M6(Ni6 ni6) {
        Wg6.c(ni6, "presenter");
        this.h = ni6;
    }

    @DexIgnore
    @Override // com.fossil.Oi6
    public void b0(int i2, List<W57> list, List<Gl7<Integer, Lc6<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        Wg6.c(list, "listTodayHeartRateModel");
        Wg6.c(list2, "listTimeZoneChange");
        G37<D75> g37 = this.g;
        if (g37 != null) {
            D75 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.w) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.m(list);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        D75 d75 = (D75) Aq0.f(layoutInflater, 2131558565, viewGroup, false, A6());
        d75.u.setOnClickListener(Ai.b);
        this.g = new G37<>(this, d75);
        L6();
        G37<D75> g37 = this.g;
        if (g37 != null) {
            D75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Ni6 ni6 = this.h;
        if (ni6 != null) {
            ni6.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        L6();
        Ni6 ni6 = this.h;
        if (ni6 != null) {
            ni6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oi6
    public void v(boolean z, List<WorkoutSession> list) {
        View n;
        View n2;
        Wg6.c(list, "workoutSessions");
        G37<D75> g37 = this.g;
        if (g37 != null) {
            D75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.v;
                Wg6.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.r, list.get(0));
                if (size == 1) {
                    Bf5 bf5 = a2.s;
                    if (bf5 != null && (n2 = bf5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                Bf5 bf52 = a2.s;
                if (!(bf52 == null || (n = bf52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.s, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.v;
            Wg6.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
