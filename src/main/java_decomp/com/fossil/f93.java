package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F93 implements Xw2<D93> {
    @DexIgnore
    public static F93 c; // = new F93();
    @DexIgnore
    public /* final */ Xw2<D93> b;

    @DexIgnore
    public F93() {
        this(Ww2.b(new H93()));
    }

    @DexIgnore
    public F93(Xw2<D93> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((D93) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((D93) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ D93 zza() {
        return this.b.zza();
    }
}
