package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B23 implements N23 {
    @DexIgnore
    @Override // com.fossil.N23
    public final boolean zza(Class<?> cls) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.N23
    public final K23 zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
