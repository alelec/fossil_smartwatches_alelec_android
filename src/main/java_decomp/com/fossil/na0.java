package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Na0 implements Parcelable.Creator<Oa0> {
    @DexIgnore
    public /* synthetic */ Na0(Qg6 qg6) {
    }

    @DexIgnore
    public Oa0 a(Parcel parcel) {
        return new Oa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Oa0 createFromParcel(Parcel parcel) {
        return new Oa0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Oa0[] newArray(int i) {
        return new Oa0[i];
    }
}
