package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class At0<D> {
    @DexIgnore
    public boolean mAbandoned; // = false;
    @DexIgnore
    public boolean mContentChanged; // = false;
    @DexIgnore
    public Context mContext;
    @DexIgnore
    public int mId;
    @DexIgnore
    public Ci<D> mListener;
    @DexIgnore
    public Bi<D> mOnLoadCanceledListener;
    @DexIgnore
    public boolean mProcessingChange; // = false;
    @DexIgnore
    public boolean mReset; // = true;
    @DexIgnore
    public boolean mStarted; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends ContentObserver {
        @DexIgnore
        public Ai() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            At0.this.onContentChanged();
        }
    }

    @DexIgnore
    public interface Bi<D> {
        @DexIgnore
        void a(At0<D> at0);
    }

    @DexIgnore
    public interface Ci<D> {
        @DexIgnore
        void a(At0<D> at0, D d);
    }

    @DexIgnore
    public At0(Context context) {
        this.mContext = context.getApplicationContext();
    }

    @DexIgnore
    public void abandon() {
        this.mAbandoned = true;
        onAbandon();
    }

    @DexIgnore
    public boolean cancelLoad() {
        return onCancelLoad();
    }

    @DexIgnore
    public void commitContentChanged() {
        this.mProcessingChange = false;
    }

    @DexIgnore
    public String dataToString(D d) {
        StringBuilder sb = new StringBuilder(64);
        In0.a(d, sb);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void deliverCancellation() {
        Bi<D> bi = this.mOnLoadCanceledListener;
        if (bi != null) {
            bi.a(this);
        }
    }

    @DexIgnore
    public void deliverResult(D d) {
        Ci<D> ci = this.mListener;
        if (ci != null) {
            ci.a(this, d);
        }
    }

    @DexIgnore
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.mId);
        printWriter.print(" mListener=");
        printWriter.println(this.mListener);
        if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.mStarted);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.mContentChanged);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.mProcessingChange);
        }
        if (this.mAbandoned || this.mReset) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.mAbandoned);
            printWriter.print(" mReset=");
            printWriter.println(this.mReset);
        }
    }

    @DexIgnore
    public void forceLoad() {
        onForceLoad();
    }

    @DexIgnore
    public Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public boolean isAbandoned() {
        return this.mAbandoned;
    }

    @DexIgnore
    public boolean isReset() {
        return this.mReset;
    }

    @DexIgnore
    public boolean isStarted() {
        return this.mStarted;
    }

    @DexIgnore
    public void onAbandon() {
    }

    @DexIgnore
    public abstract boolean onCancelLoad();

    @DexIgnore
    public void onContentChanged() {
        if (this.mStarted) {
            forceLoad();
        } else {
            this.mContentChanged = true;
        }
    }

    @DexIgnore
    public void onForceLoad() {
    }

    @DexIgnore
    public void onReset() {
    }

    @DexIgnore
    public void onStartLoading() {
    }

    @DexIgnore
    public void onStopLoading() {
    }

    @DexIgnore
    public void registerListener(int i, Ci<D> ci) {
        if (this.mListener == null) {
            this.mListener = ci;
            this.mId = i;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void registerOnLoadCanceledListener(Bi<D> bi) {
        if (this.mOnLoadCanceledListener == null) {
            this.mOnLoadCanceledListener = bi;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void reset() {
        onReset();
        this.mReset = true;
        this.mStarted = false;
        this.mAbandoned = false;
        this.mContentChanged = false;
        this.mProcessingChange = false;
    }

    @DexIgnore
    public void rollbackContentChanged() {
        if (this.mProcessingChange) {
            onContentChanged();
        }
    }

    @DexIgnore
    public final void startLoading() {
        this.mStarted = true;
        this.mReset = false;
        this.mAbandoned = false;
        onStartLoading();
    }

    @DexIgnore
    public void stopLoading() {
        this.mStarted = false;
        onStopLoading();
    }

    @DexIgnore
    public boolean takeContentChanged() {
        boolean z = this.mContentChanged;
        this.mContentChanged = false;
        this.mProcessingChange |= z;
        return z;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        In0.a(this, sb);
        sb.append(" id=");
        sb.append(this.mId);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void unregisterListener(Ci<D> ci) {
        Ci<D> ci2 = this.mListener;
        if (ci2 == null) {
            throw new IllegalStateException("No listener register");
        } else if (ci2 == ci) {
            this.mListener = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    @DexIgnore
    public void unregisterOnLoadCanceledListener(Bi<D> bi) {
        Bi<D> bi2 = this.mOnLoadCanceledListener;
        if (bi2 == null) {
            throw new IllegalStateException("No listener register");
        } else if (bi2 == bi) {
            this.mOnLoadCanceledListener = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }
}
