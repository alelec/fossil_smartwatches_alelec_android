package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Jw1 {
    SYSTEM((byte) 0),
    THEME((byte) 1),
    WATCH_APP((byte) 2);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Jw1 a(byte b) {
            Jw1[] values = Jw1.values();
            for (Jw1 jw1 : values) {
                if (jw1.a() == b) {
                    return jw1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Jw1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
