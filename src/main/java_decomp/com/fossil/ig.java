package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.mapped.NotificationIcon;
import com.mapped.NotificationIconConfig;
import com.mapped.Rc6;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ig extends Lp {
    @DexIgnore
    public /* final */ NotificationFilter[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ig(K5 k5, I60 i60, NotificationFilter[] notificationFilterArr, String str, int i) {
        super(k5, i60, Yp.I, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = notificationFilterArr;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        boolean z = false;
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (NotificationFilter notificationFilter : this.C) {
            NotificationIconConfig iconConfig = notificationFilter.getIconConfig();
            if (iconConfig != null) {
                NotificationIcon[] a2 = iconConfig.a();
                for (NotificationIcon notificationIcon : a2) {
                    copyOnWriteArrayList.addIfAbsent(notificationIcon);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new NotificationIcon[0]);
        if (array != null) {
            NotificationIcon[] notificationIconArr = (NotificationIcon[]) array;
            if (notificationIconArr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    Xa xa = Xa.d;
                    Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.m.b));
                    if (ry1 == null) {
                        ry1 = Hd0.y.d();
                    }
                    Lp.h(this, new Zj(this.w, this.x, Yp.W, true, 1793, xa.h(notificationIconArr, 1793, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Qr(this), new Gf(this), new Uf(this), null, null, 48, null);
                } catch (Sx1 e) {
                    D90.i.i(e);
                    l(Nr.a(this.v, null, Zq.t, null, null, 13));
                }
            } else {
                H();
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.U, Px1.a(this.C));
    }

    @DexIgnore
    public final void H() {
        short b = Ke.b.b(this.w.x, Ob.p);
        try {
            R9 r9 = R9.d;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.p.b));
            if (ry1 == null) {
                ry1 = Hd0.y.d();
            }
            Lp.h(this, new Zj(this.w, this.x, Yp.X, true, b, r9.a(b, ry1, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Oq(this), new Cr(this), null, null, null, 56, null);
        } catch (Sx1 e) {
            D90.i.i(e);
            l(Nr.a(this.v, null, Zq.q, null, null, 13));
        }
    }
}
