package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zu1 {
    UNDEFINED((byte) 0),
    MUSIC_CONTROL((byte) 1),
    ACTIVITY_TAGGING((byte) 2),
    GOAL_TRACKING((byte) 3),
    DATE((byte) 4),
    TIME2((byte) 5),
    ALERT((byte) 6),
    ALARM((byte) 7),
    PROGRESS((byte) 8),
    TWENTY_FOUR_HOUR((byte) 9),
    BOLT_CONTROL((byte) 10),
    WEATHER((byte) 11),
    COMMUTE_TIME((byte) 12),
    SELFIE((byte) 13),
    RING_PHONE(DateTimeFieldType.HOUR_OF_HALFDAY),
    STOPWATCH(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY);

    @DexIgnore
    public zu1(byte b2) {
    }
}
