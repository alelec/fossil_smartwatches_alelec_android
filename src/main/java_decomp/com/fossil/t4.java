package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T4 implements Parcelable.Creator<K5> {
    @DexIgnore
    public /* synthetic */ T4(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public K5 createFromParcel(Parcel parcel) {
        X4 x4 = X4.b;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            return x4.a((BluetoothDevice) readParcelable);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public K5[] newArray(int i) {
        return new K5[i];
    }
}
