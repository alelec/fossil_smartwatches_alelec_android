package com.fossil;

import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.DownloadManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.SearchManager;
import android.app.UiModeManager;
import android.app.WallpaperManager;
import android.app.admin.DevicePolicyManager;
import android.app.job.JobScheduler;
import android.app.usage.UsageStatsManager;
import android.appwidget.AppWidgetManager;
import android.bluetooth.BluetoothManager;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.RestrictionsManager;
import android.content.pm.LauncherApps;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.hardware.ConsumerIrManager;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraManager;
import android.hardware.display.DisplayManager;
import android.hardware.input.InputManager;
import android.hardware.usb.UsbManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRouter;
import android.media.projection.MediaProjectionManager;
import android.media.session.MediaSessionManager;
import android.media.tv.TvInputManager;
import android.net.ConnectivityManager;
import android.net.nsd.NsdManager;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pManager;
import android.nfc.NfcManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.DropBoxManager;
import android.os.PowerManager;
import android.os.Process;
import android.os.UserManager;
import android.os.Vibrator;
import android.os.storage.StorageManager;
import android.print.PrintManager;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.CaptioningManager;
import android.view.inputmethod.InputMethodManager;
import android.view.textservice.TextServicesManager;
import com.facebook.internal.ServerProtocol;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gl0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ Object f1323a; // = new Object();
    @DexIgnore
    public static TypedValue b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public static /* final */ HashMap<Class<?>, String> f1324a; // = new HashMap<>();

        /*
        static {
            if (Build.VERSION.SDK_INT >= 22) {
                f1324a.put(SubscriptionManager.class, "telephony_subscription_service");
                f1324a.put(UsageStatsManager.class, "usagestats");
            }
            if (Build.VERSION.SDK_INT >= 21) {
                f1324a.put(AppWidgetManager.class, "appwidget");
                f1324a.put(BatteryManager.class, "batterymanager");
                f1324a.put(CameraManager.class, "camera");
                f1324a.put(JobScheduler.class, "jobscheduler");
                f1324a.put(LauncherApps.class, "launcherapps");
                f1324a.put(MediaProjectionManager.class, "media_projection");
                f1324a.put(MediaSessionManager.class, "media_session");
                f1324a.put(RestrictionsManager.class, "restrictions");
                f1324a.put(TelecomManager.class, "telecom");
                f1324a.put(TvInputManager.class, "tv_input");
            }
            if (Build.VERSION.SDK_INT >= 19) {
                f1324a.put(AppOpsManager.class, "appops");
                f1324a.put(CaptioningManager.class, "captioning");
                f1324a.put(ConsumerIrManager.class, "consumer_ir");
                f1324a.put(PrintManager.class, "print");
            }
            if (Build.VERSION.SDK_INT >= 18) {
                f1324a.put(BluetoothManager.class, PlaceManager.PARAM_BLUETOOTH);
            }
            if (Build.VERSION.SDK_INT >= 17) {
                f1324a.put(DisplayManager.class, ServerProtocol.DIALOG_PARAM_DISPLAY);
                f1324a.put(UserManager.class, "user");
            }
            if (Build.VERSION.SDK_INT >= 16) {
                f1324a.put(InputManager.class, "input");
                f1324a.put(MediaRouter.class, "media_router");
                f1324a.put(NsdManager.class, "servicediscovery");
            }
            f1324a.put(AccessibilityManager.class, "accessibility");
            f1324a.put(AccountManager.class, "account");
            f1324a.put(ActivityManager.class, Constants.ACTIVITY);
            f1324a.put(AlarmManager.class, Alarm.TABLE_NAME);
            f1324a.put(AudioManager.class, "audio");
            f1324a.put(ClipboardManager.class, "clipboard");
            f1324a.put(ConnectivityManager.class, "connectivity");
            f1324a.put(DevicePolicyManager.class, "device_policy");
            f1324a.put(DownloadManager.class, "download");
            f1324a.put(DropBoxManager.class, "dropbox");
            f1324a.put(InputMethodManager.class, "input_method");
            f1324a.put(KeyguardManager.class, "keyguard");
            f1324a.put(LayoutInflater.class, "layout_inflater");
            f1324a.put(LocationManager.class, PlaceFields.LOCATION);
            f1324a.put(NfcManager.class, "nfc");
            f1324a.put(NotificationManager.class, "notification");
            f1324a.put(PowerManager.class, "power");
            f1324a.put(SearchManager.class, "search");
            f1324a.put(SensorManager.class, "sensor");
            f1324a.put(StorageManager.class, "storage");
            f1324a.put(TelephonyManager.class, PlaceFields.PHONE);
            f1324a.put(TextServicesManager.class, "textservices");
            f1324a.put(UiModeManager.class, "uimode");
            f1324a.put(UsbManager.class, "usb");
            f1324a.put(Vibrator.class, "vibrator");
            f1324a.put(WallpaperManager.class, "wallpaper");
            f1324a.put(WifiP2pManager.class, "wifip2p");
            f1324a.put(WifiManager.class, PlaceManager.PARAM_WIFI);
            f1324a.put(WindowManager.class, "window");
        }
        */
    }

    @DexIgnore
    public static int a(Context context, String str) {
        if (str != null) {
            return context.checkPermission(str, Process.myPid(), Process.myUid());
        }
        throw new IllegalArgumentException("permission is null");
    }

    @DexIgnore
    public static Context b(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            return context.createDeviceProtectedStorageContext();
        }
        return null;
    }

    @DexIgnore
    public static File c(File file) {
        synchronized (gl0.class) {
            try {
                if (file.exists() || file.mkdirs()) {
                    return file;
                }
                if (file.exists()) {
                    return file;
                }
                Log.w("ContextCompat", "Unable to create files subdir " + file.getPath());
                return null;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public static int d(Context context, int i) {
        return Build.VERSION.SDK_INT >= 23 ? context.getColor(i) : context.getResources().getColor(i);
    }

    @DexIgnore
    public static ColorStateList e(Context context, int i) {
        return Build.VERSION.SDK_INT >= 23 ? context.getColorStateList(i) : context.getResources().getColorStateList(i);
    }

    @DexIgnore
    public static Drawable f(Context context, int i) {
        int i2;
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            return context.getDrawable(i);
        }
        if (i3 >= 16) {
            return context.getResources().getDrawable(i);
        }
        synchronized (f1323a) {
            if (b == null) {
                b = new TypedValue();
            }
            context.getResources().getValue(i, b, true);
            i2 = b.resourceId;
        }
        return context.getResources().getDrawable(i2);
    }

    @DexIgnore
    public static File[] g(Context context) {
        if (Build.VERSION.SDK_INT >= 19) {
            return context.getExternalCacheDirs();
        }
        return new File[]{context.getExternalCacheDir()};
    }

    @DexIgnore
    public static File[] h(Context context, String str) {
        if (Build.VERSION.SDK_INT >= 19) {
            return context.getExternalFilesDirs(str);
        }
        return new File[]{context.getExternalFilesDir(str)};
    }

    @DexIgnore
    public static File i(Context context) {
        return Build.VERSION.SDK_INT >= 21 ? context.getNoBackupFilesDir() : c(new File(context.getApplicationInfo().dataDir, "no_backup"));
    }

    @DexIgnore
    public static <T> T j(Context context, Class<T> cls) {
        if (Build.VERSION.SDK_INT >= 23) {
            return (T) context.getSystemService(cls);
        }
        String k = k(context, cls);
        if (k != null) {
            return (T) context.getSystemService(k);
        }
        return null;
    }

    @DexIgnore
    public static String k(Context context, Class<?> cls) {
        return Build.VERSION.SDK_INT >= 23 ? context.getSystemServiceName(cls) : a.f1324a.get(cls);
    }

    @DexIgnore
    public static boolean l(Context context) {
        if (Build.VERSION.SDK_INT >= 24) {
            return context.isDeviceProtectedStorage();
        }
        return false;
    }

    @DexIgnore
    public static boolean m(Context context, Intent[] intentArr, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            context.startActivities(intentArr, bundle);
            return true;
        }
        context.startActivities(intentArr);
        return true;
    }

    @DexIgnore
    public static void n(Context context, Intent intent, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public static void o(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }
}
