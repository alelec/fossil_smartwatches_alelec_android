package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ja7 extends Kq0 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String g; // = Er7.b(Ja7.class).a();
    @DexIgnore
    public static /* final */ Ai h; // = new Ai(null);
    @DexIgnore
    public Bi b;
    @DexIgnore
    public M25 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ja7.g;
        }

        @DexIgnore
        public final Ja7 b(String str, Bi bi) {
            Wg6.c(bi, "listener");
            Ja7 ja7 = new Ja7(str);
            ja7.b = bi;
            return ja7;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str);

        @DexIgnore
        Object onCancel();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ja7 b;

        @DexIgnore
        public Ci(Ja7 ja7) {
            this.b = ja7;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            Ja7 ja7 = this.b;
            String valueOf = String.valueOf(editable);
            int length = valueOf.length() - 1;
            boolean z = false;
            int i = 0;
            while (i <= length) {
                boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            ja7.e = valueOf.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.b.e);
            ImageView imageView = Ja7.x6(this.b).f;
            Wg6.b(imageView, "binding.ivClearName");
            imageView.setVisibility(z3 ? 0 : 8);
            if (!z3 || !(!Wg6.a(this.b.d, this.b.e))) {
                this.b.E6(false);
            } else {
                this.b.E6(true);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore
    public Ja7(String str) {
        this.e = str;
        this.d = str;
    }

    @DexIgnore
    public static final /* synthetic */ M25 x6(Ja7 ja7) {
        M25 m25 = ja7.c;
        if (m25 != null) {
            return m25;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void D6(boolean z) {
        Bi bi = this.b;
        if (bi != null) {
            if (z) {
                bi.onCancel();
            } else {
                String str = this.e;
                if (str != null) {
                    bi.a(str);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public final void E6(boolean z) {
        M25 m25 = this.c;
        if (m25 != null) {
            FlexibleTextView flexibleTextView = m25.c;
            Wg6.b(flexibleTextView, "binding.ftvAdd");
            flexibleTextView.setEnabled(z);
            if (z) {
                M25 m252 = this.c;
                if (m252 != null) {
                    FlexibleTextView flexibleTextView2 = m252.c;
                    if (m252 != null) {
                        ScrollView b2 = m252.b();
                        Wg6.b(b2, "binding.root");
                        flexibleTextView2.setTextColor(W6.d(b2.getContext(), 2131099968));
                        M25 m253 = this.c;
                        if (m253 != null) {
                            m253.c.setBackgroundResource(2131230824);
                        } else {
                            Wg6.n("binding");
                            throw null;
                        }
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                } else {
                    Wg6.n("binding");
                    throw null;
                }
            } else {
                M25 m254 = this.c;
                if (m254 != null) {
                    FlexibleTextView flexibleTextView3 = m254.c;
                    if (m254 != null) {
                        ScrollView b3 = m254.b();
                        Wg6.b(b3, "binding.root");
                        flexibleTextView3.setTextColor(W6.d(b3.getContext(), 2131099844));
                        M25 m255 = this.c;
                        if (m255 != null) {
                            m255.c.setBackgroundResource(2131230825);
                        } else {
                            Wg6.n("binding");
                            throw null;
                        }
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                } else {
                    Wg6.n("binding");
                    throw null;
                }
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id != 2131362358) {
                if (id == 2131362378) {
                    D6(true);
                } else if (id == 2131362684) {
                    M25 m25 = this.c;
                    if (m25 != null) {
                        m25.b.setText("");
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
            } else if (!TextUtils.isEmpty(this.e)) {
                D6(false);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 2131951890);
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        Wg6.b(onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        boolean z = true;
        Wg6.c(layoutInflater, "inflater");
        M25 c2 = M25.c(layoutInflater, viewGroup, false);
        Wg6.b(c2, "FragmentAddTextElementBi\u2026flater, container, false)");
        this.c = c2;
        if (c2 != null) {
            FlexibleTextView flexibleTextView = c2.e;
            Wg6.b(flexibleTextView, "binding.ftvContent");
            flexibleTextView.setText(Um5.c(requireContext(), 2131886596));
            M25 m25 = this.c;
            if (m25 != null) {
                FlexibleEditText flexibleEditText = m25.b;
                Wg6.b(flexibleEditText, "binding.fetText");
                flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(23)});
                String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                if (d2 != null) {
                    M25 m252 = this.c;
                    if (m252 != null) {
                        m252.h.setBackgroundColor(Color.parseColor(d2));
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
                String str = this.e;
                if (str != null) {
                    M25 m253 = this.c;
                    if (m253 != null) {
                        FlexibleEditText flexibleEditText2 = m253.b;
                        flexibleEditText2.setText(str);
                        flexibleEditText2.setSelection(str.length());
                        M25 m254 = this.c;
                        if (m254 != null) {
                            ImageView imageView = m254.f;
                            Wg6.b(imageView, "binding.ivClearName");
                            if (str.length() <= 0) {
                                z = false;
                            }
                            imageView.setVisibility(z ? 0 : 8);
                        } else {
                            Wg6.n("binding");
                            throw null;
                        }
                    } else {
                        Wg6.n("binding");
                        throw null;
                    }
                }
                M25 m255 = this.c;
                if (m255 != null) {
                    FlexibleEditText flexibleEditText3 = m255.b;
                    Wg6.b(flexibleEditText3, "binding.fetText");
                    flexibleEditText3.addTextChangedListener(new Ci(this));
                    M25 m256 = this.c;
                    if (m256 != null) {
                        m256.f.setOnClickListener(this);
                        M25 m257 = this.c;
                        if (m257 != null) {
                            m257.c.setOnClickListener(this);
                            M25 m258 = this.c;
                            if (m258 != null) {
                                m258.d.setOnClickListener(this);
                                E6(false);
                                M25 m259 = this.c;
                                if (m259 != null) {
                                    return m259.b();
                                }
                                Wg6.n("binding");
                                throw null;
                            }
                            Wg6.n("binding");
                            throw null;
                        }
                        Wg6.n("binding");
                        throw null;
                    }
                    Wg6.n("binding");
                    throw null;
                }
                Wg6.n("binding");
                throw null;
            }
            Wg6.n("binding");
            throw null;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Object systemService = requireActivity().getSystemService("input_method");
        if (systemService != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) systemService;
            M25 m25 = this.c;
            if (m25 != null) {
                FlexibleEditText flexibleEditText = m25.b;
                Wg6.b(flexibleEditText, "binding.fetText");
                Gj5.b(flexibleEditText, inputMethodManager);
                return;
            }
            Wg6.n("binding");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
