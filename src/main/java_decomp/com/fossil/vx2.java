package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vx2<E> extends Ax2<E> {
    @DexIgnore
    public /* final */ Sx2<E> d;

    @DexIgnore
    public Vx2(Sx2<E> sx2, int i) {
        super(sx2.size(), i);
        this.d = sx2;
    }

    @DexIgnore
    @Override // com.fossil.Ax2
    public final E a(int i) {
        return this.d.get(i);
    }
}
