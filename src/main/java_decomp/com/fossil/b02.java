package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B02 {
    @DexIgnore
    public /* final */ Ty1 a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public B02(Ty1 ty1, byte[] bArr) {
        if (ty1 == null) {
            throw new NullPointerException("encoding is null");
        } else if (bArr != null) {
            this.a = ty1;
            this.b = bArr;
        } else {
            throw new NullPointerException("bytes is null");
        }
    }

    @DexIgnore
    public byte[] a() {
        return this.b;
    }

    @DexIgnore
    public Ty1 b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof B02)) {
            return false;
        }
        B02 b02 = (B02) obj;
        if (this.a.equals(b02.a)) {
            return Arrays.equals(this.b, b02.b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "EncodedPayload{encoding=" + this.a + ", bytes=[...]}";
    }
}
