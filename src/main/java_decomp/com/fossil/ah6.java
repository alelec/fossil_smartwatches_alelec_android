package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ah6 implements Factory<CaloriesOverviewWeekPresenter> {
    @DexIgnore
    public static CaloriesOverviewWeekPresenter a(Xg6 xg6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new CaloriesOverviewWeekPresenter(xg6, userRepository, summariesRepository, portfolioApp);
    }
}
