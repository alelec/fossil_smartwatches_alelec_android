package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xw4 extends RecyclerView.g<Ai> {
    @DexIgnore
    public List<Xs4> a; // = new ArrayList();
    @DexIgnore
    public Bi b;
    @DexIgnore
    public /* final */ F57.Bi c;
    @DexIgnore
    public /* final */ Uy4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Pe5 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Xw4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ Xs4 c;

            @DexIgnore
            public Aii(Ai ai, Xs4 xs4, F57.Bi bi, Uy4 uy4) {
                this.b = ai;
                this.c = xs4;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                if (this.b.getAdapterPosition() != -1 && (bi = this.b.c.b) != null) {
                    bi.a(this.c, this.b.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Xw4 xw4, Pe5 pe5, View view) {
            super(view);
            Wg6.c(pe5, "binding");
            Wg6.c(view, "root");
            this.c = xw4;
            this.a = pe5;
            this.b = view;
        }

        @DexIgnore
        public void a(Xs4 xs4, F57.Bi bi, Uy4 uy4) {
            Wg6.c(xs4, "item");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Pe5 pe5 = this.a;
            String b2 = Hz4.a.b(xs4.b(), xs4.e(), xs4.i());
            FlexibleTextView flexibleTextView = pe5.u;
            Wg6.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleTextView flexibleTextView2 = pe5.v;
            Wg6.b(flexibleTextView2, "tvSocial");
            flexibleTextView2.setText(xs4.i());
            ImageView imageView = pe5.s;
            Wg6.b(imageView, "ivAvatar");
            Ty4.b(imageView, xs4.h(), b2, bi, uy4);
            pe5.r.setOnClickListener(new Aii(this, xs4, bi, uy4));
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(Xs4 xs4, int i);
    }

    @DexIgnore
    public Xw4() {
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.c = f;
        this.d = Uy4.d.b();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void h() {
        if (this.a.size() != 0) {
            this.a.clear();
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void i(Ai ai, int i) {
        Wg6.c(ai, "holder");
        ai.a(this.a.get(i), this.c, this.d);
    }

    @DexIgnore
    public Ai j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Pe5 z = Pe5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemFriendSearchBinding.\u2026tInflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "itemFriendSearchBinding.root");
        return new Ai(this, z, n);
    }

    @DexIgnore
    public final void k(Bi bi) {
        Wg6.c(bi, "listener");
        this.b = bi;
    }

    @DexIgnore
    public final void l(List<Xs4> list) {
        Wg6.c(list, "friendList");
        if (!Wg6.a(this.a, list)) {
            this.a.clear();
            this.a.addAll(list);
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void m(int i) {
        if (i != -1) {
            this.a.remove(i);
            notifyItemRemoved(i);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        i(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
