package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju5 extends iq4<a, iq4.d, iq4.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Date f1808a;

        @DexIgnore
        public a(Date date) {
            pq7.c(date, "date");
            this.f1808a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.f1808a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries", f = "FetchDailyHeartRateSummaries.kt", l = {27, 55, 57}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ju5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ju5 ju5, qn7 qn7) {
            super(qn7);
            this.this$0 = ju5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = ju5.class.getSimpleName();
        pq7.b(simpleName, "FetchDailyHeartRateSumma\u2026es::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public ju5(HeartRateSummaryRepository heartRateSummaryRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        pq7.c(heartRateSummaryRepository, "mHeartRateSummariesRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        this.d = heartRateSummaryRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ju5.a r22, com.fossil.qn7<? super com.fossil.tl7> r23) {
        /*
        // Method dump skipped, instructions count: 515
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ju5.k(com.fossil.ju5$a, com.fossil.qn7):java.lang.Object");
    }
}
