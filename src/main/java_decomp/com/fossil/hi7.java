package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hi7 {
    @DexIgnore
    public static int a; // = -1;

    @DexIgnore
    public static boolean a() {
        int i = a;
        if (i == 1) {
            return true;
        }
        if (i == 0) {
            return false;
        }
        for (int i2 = 0; i2 < 6; i2++) {
            try {
                if (new File(new String[]{"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"}[i2] + "su").exists()) {
                    a = 1;
                    return true;
                }
            } catch (Exception e) {
            }
        }
        a = 0;
        return false;
    }
}
