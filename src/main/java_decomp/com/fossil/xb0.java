package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xb0 implements Parcelable.Creator<Yb0> {
    @DexIgnore
    public /* synthetic */ Xb0(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Yb0 createFromParcel(Parcel parcel) {
        Jw1 jw1 = Jw1.values()[parcel.readInt()];
        Parcelable readParcelable = parcel.readParcelable(Ry1.class.getClassLoader());
        if (readParcelable != null) {
            Ry1 ry1 = (Ry1) readParcelable;
            boolean z = parcel.readByte() != ((byte) 0);
            byte[] createByteArray = parcel.createByteArray();
            return new Yb0(jw1, ry1, z, createByteArray != null ? createByteArray : new byte[0]);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Yb0[] newArray(int i) {
        return new Yb0[i];
    }
}
