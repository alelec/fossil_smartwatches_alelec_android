package com.fossil;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Te1 implements Mb1 {
    @DexIgnore
    public /* final */ Ue1 b;
    @DexIgnore
    public /* final */ URL c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public URL f;
    @DexIgnore
    public volatile byte[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public Te1(String str) {
        this(str, Ue1.a);
    }

    @DexIgnore
    public Te1(String str, Ue1 ue1) {
        this.c = null;
        Ik1.b(str);
        this.d = str;
        Ik1.d(ue1);
        this.b = ue1;
    }

    @DexIgnore
    public Te1(URL url) {
        this(url, Ue1.a);
    }

    @DexIgnore
    public Te1(URL url, Ue1 ue1) {
        Ik1.d(url);
        this.c = url;
        this.d = null;
        Ik1.d(ue1);
        this.b = ue1;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        messageDigest.update(d());
    }

    @DexIgnore
    public String c() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        Ik1.d(url);
        return url.toString();
    }

    @DexIgnore
    public final byte[] d() {
        if (this.g == null) {
            this.g = c().getBytes(Mb1.a);
        }
        return this.g;
    }

    @DexIgnore
    public Map<String, String> e() {
        return this.b.a();
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof Te1)) {
            return false;
        }
        Te1 te1 = (Te1) obj;
        return c().equals(te1.c()) && this.b.equals(te1.b);
    }

    @DexIgnore
    public final String f() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                Ik1.d(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    @DexIgnore
    public final URL g() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(f());
        }
        return this.f;
    }

    @DexIgnore
    public URL h() throws MalformedURLException {
        return g();
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        if (this.h == 0) {
            int hashCode = c().hashCode();
            this.h = hashCode;
            this.h = (hashCode * 31) + this.b.hashCode();
        }
        return this.h;
    }

    @DexIgnore
    public String toString() {
        return c();
    }
}
