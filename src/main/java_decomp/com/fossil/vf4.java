package com.fossil;

import android.util.Log;
import android.util.Pair;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vf4 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Map<Pair<String, String>, Nt3<If4>> b; // = new Zi0();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Nt3<If4> start();
    }

    @DexIgnore
    public Vf4(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public Nt3<If4> a(String str, String str2, Ai ai) {
        Nt3<If4> nt3;
        synchronized (this) {
            Pair<String, String> pair = new Pair<>(str, str2);
            nt3 = this.b.get(pair);
            if (nt3 == null) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(pair);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                    sb.append("Making new request for: ");
                    sb.append(valueOf);
                    Log.d("FirebaseInstanceId", sb.toString());
                }
                nt3 = ai.start().k(this.a, new Uf4(this, pair));
                this.b.put(pair, nt3);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf2 = String.valueOf(pair);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 29);
                sb2.append("Joining ongoing request for: ");
                sb2.append(valueOf2);
                Log.d("FirebaseInstanceId", sb2.toString());
            }
        }
        return nt3;
    }

    @DexIgnore
    public final /* synthetic */ Nt3 b(Pair pair, Nt3 nt3) throws Exception {
        synchronized (this) {
            this.b.remove(pair);
        }
        return nt3;
    }
}
