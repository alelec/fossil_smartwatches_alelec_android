package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N33 implements Iterator<Object> {
    @DexIgnore
    public final boolean hasNext() {
        return false;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final Object next() {
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
