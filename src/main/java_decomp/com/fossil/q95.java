package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q95 extends P95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G;
    @DexIgnore
    public long E;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        G = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        G.put(2131362968, 2);
        G.put(2131363410, 3);
        G.put(2131363316, 4);
        G.put(2131363086, 5);
        G.put(2131362449, 6);
        G.put(2131363220, 7);
        G.put(2131363065, 8);
        G.put(2131362559, 9);
        G.put(2131363224, 10);
        G.put(2131363067, 11);
        G.put(2131363399, 12);
        G.put(2131361941, 13);
    }
    */

    @DexIgnore
    public Q95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 14, F, G));
    }

    @DexIgnore
    public Q95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[13], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[9], (ImageView) objArr[1], (DashBar) objArr[2], (ConstraintLayout) objArr[0], (RulerValuePicker) objArr[8], (RulerValuePicker) objArr[11], (ScrollView) objArr[5], (TabLayout) objArr[7], (TabLayout) objArr[10], (FlexibleTextView) objArr[4], (FlexibleButton) objArr[12], (FlexibleTextView) objArr[3]);
        this.E = -1;
        this.v.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.E != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.E = 1;
        }
        w();
    }
}
