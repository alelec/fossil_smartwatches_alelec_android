package com.fossil;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.Af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ne1<Data> implements Af1<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;
    @DexIgnore
    public /* final */ Ai<Data> b;

    @DexIgnore
    public interface Ai<Data> {
        @DexIgnore
        Wb1<Data> a(AssetManager assetManager, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Bf1<Uri, ParcelFileDescriptor>, Ai<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public Bi(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.Ne1.Ai
        public Wb1<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new Ac1(assetManager, str);
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, ParcelFileDescriptor> b(Ef1 ef1) {
            return new Ne1(this.a, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Bf1<Uri, InputStream>, Ai<InputStream> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public Ci(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        @Override // com.fossil.Ne1.Ai
        public Wb1<InputStream> a(AssetManager assetManager, String str) {
            return new Gc1(assetManager, str);
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, InputStream> b(Ef1 ef1) {
            return new Ne1(this.a, this);
        }
    }

    @DexIgnore
    public Ne1(AssetManager assetManager, Ai<Data> ai) {
        this.a = assetManager;
        this.b = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(Uri uri, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(uri), this.b.a(this.a, uri.toString().substring(c)));
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }
}
