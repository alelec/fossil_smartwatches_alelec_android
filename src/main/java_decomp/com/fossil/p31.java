package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.O31;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnknownNullness"})
public interface P31 {
    @DexIgnore
    int a(F11 f11, String... strArr);

    @DexIgnore
    void b(String str);

    @DexIgnore
    List<O31> c();

    @DexIgnore
    int d(String str, long j);

    @DexIgnore
    List<O31.Ai> e(String str);

    @DexIgnore
    List<O31> f(long j);

    @DexIgnore
    List<O31> g(int i);

    @DexIgnore
    void h(O31 o31);

    @DexIgnore
    List<O31> i();

    @DexIgnore
    void j(String str, R01 r01);

    @DexIgnore
    List<O31> k();

    @DexIgnore
    List<String> l();

    @DexIgnore
    List<String> m(String str);

    @DexIgnore
    F11 n(String str);

    @DexIgnore
    O31 o(String str);

    @DexIgnore
    int p(String str);

    @DexIgnore
    List<R01> q(String str);

    @DexIgnore
    int r(String str);

    @DexIgnore
    void s(String str, long j);

    @DexIgnore
    int t();
}
