package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.Gc2;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class He2 extends Gc2 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<Gc2.Ai, Ge2> d; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ Ve2 g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ long i;

    @DexIgnore
    public He2(Context context) {
        this.e = context.getApplicationContext();
        this.f = new Xl2(context.getMainLooper(), this);
        this.g = Ve2.b();
        this.h = 5000;
        this.i = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    @Override // com.fossil.Gc2
    public final boolean d(Gc2.Ai ai, ServiceConnection serviceConnection, String str) {
        boolean d2;
        Rc2.l(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            Ge2 ge2 = this.d.get(ai);
            if (ge2 == null) {
                ge2 = new Ge2(this, ai);
                ge2.e(serviceConnection, serviceConnection, str);
                ge2.h(str);
                this.d.put(ai, ge2);
            } else {
                this.f.removeMessages(0, ai);
                if (!ge2.g(serviceConnection)) {
                    ge2.e(serviceConnection, serviceConnection, str);
                    int c = ge2.c();
                    if (c == 1) {
                        serviceConnection.onServiceConnected(ge2.b(), ge2.a());
                    } else if (c == 2) {
                        ge2.h(str);
                    }
                } else {
                    String valueOf = String.valueOf(ai);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = ge2.d();
        }
        return d2;
    }

    @DexIgnore
    @Override // com.fossil.Gc2
    public final void e(Gc2.Ai ai, ServiceConnection serviceConnection, String str) {
        Rc2.l(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.d) {
            Ge2 ge2 = this.d.get(ai);
            if (ge2 == null) {
                String valueOf = String.valueOf(ai);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (ge2.g(serviceConnection)) {
                ge2.f(serviceConnection, str);
                if (ge2.j()) {
                    this.f.sendMessageDelayed(this.f.obtainMessage(0, ai), this.h);
                }
            } else {
                String valueOf2 = String.valueOf(ai);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 0) {
            synchronized (this.d) {
                Gc2.Ai ai = (Gc2.Ai) message.obj;
                Ge2 ge2 = this.d.get(ai);
                if (ge2 != null && ge2.j()) {
                    if (ge2.d()) {
                        ge2.i("GmsClientSupervisor");
                    }
                    this.d.remove(ai);
                }
            }
            return true;
        } else if (i2 != 1) {
            return false;
        } else {
            synchronized (this.d) {
                Gc2.Ai ai2 = (Gc2.Ai) message.obj;
                Ge2 ge22 = this.d.get(ai2);
                if (ge22 != null && ge22.c() == 3) {
                    String valueOf = String.valueOf(ai2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = ge22.b();
                    if (b == null) {
                        b = ai2.a();
                    }
                    ge22.onServiceDisconnected(b == null ? new ComponentName(ai2.b(), "unknown") : b);
                }
            }
            return true;
        }
    }
}
