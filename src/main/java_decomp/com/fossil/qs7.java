package com.fossil;

import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qs7<T> implements Ts7<T> {
    @DexIgnore
    public /* final */ Ts7<T> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ Hg6<T, Boolean> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterator<T>, Jr7 {
        @DexIgnore
        public /* final */ Iterator<T> b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public T d;
        @DexIgnore
        public /* final */ /* synthetic */ Qs7 e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Qs7 qs7) {
            this.e = qs7;
            this.b = qs7.a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.b.hasNext()) {
                T next = this.b.next();
                if (((Boolean) this.e.c.invoke(next)).booleanValue() == this.e.b) {
                    this.d = next;
                    this.c = 1;
                    return;
                }
            }
            this.c = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c == -1) {
                a();
            }
            return this.c == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.c == -1) {
                a();
            }
            if (this.c != 0) {
                T t = this.d;
                this.d = null;
                this.c = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Ts7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Hg6<? super T, java.lang.Boolean> */
    /* JADX WARN: Multi-variable type inference failed */
    public Qs7(Ts7<? extends T> ts7, boolean z, Hg6<? super T, Boolean> hg6) {
        Wg6.c(ts7, "sequence");
        Wg6.c(hg6, "predicate");
        this.a = ts7;
        this.b = z;
        this.c = hg6;
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<T> iterator() {
        return new Ai(this);
    }
}
