package com.fossil;

import com.mapped.Cd6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eb extends Vx1<Cd6, Zk1> {
    @DexIgnore
    public static /* final */ Qx1<Cd6>[] b; // = new Qx1[0];
    @DexIgnore
    public static /* final */ Rx1<Zk1>[] c; // = {new Ab(Ob.o.c), new Cb(Ob.o.c)};
    @DexIgnore
    public static /* final */ Eb d; // = new Eb();

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<Cd6>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<Zk1>[] c() {
        return c;
    }

    @DexIgnore
    public final Zk1 h(byte[] bArr) {
        return Zk1.u.c(Dm7.k(bArr, 12, bArr.length - 4));
    }
}
