package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bf7 {
    @DexIgnore
    public static Object a(int i, String str) {
        switch (i) {
            case 1:
                return Integer.valueOf(str);
            case 2:
                return Long.valueOf(str);
            case 3:
                return str;
            case 4:
                return Boolean.valueOf(str);
            case 5:
                return Float.valueOf(str);
            case 6:
                return Double.valueOf(str);
            default:
                try {
                    Ye7.b("MicroMsg.SDK.PluginProvider.Resolver", "unknown type");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
        }
    }
}
