package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A92 extends Q92 {
    @DexIgnore
    public WeakReference<T82> a;

    @DexIgnore
    public A92(T82 t82) {
        this.a = new WeakReference<>(t82);
    }

    @DexIgnore
    @Override // com.fossil.Q92
    public final void a() {
        T82 t82 = this.a.get();
        if (t82 != null) {
            t82.w();
        }
    }
}
