package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.M47;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Io6 extends BaseFragment implements Ho6, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public G37<O15> g;
    @DexIgnore
    public Go6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Io6 a() {
            return new Io6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Io6 b;

        @DexIgnore
        public Bi(Io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Io6 b;

        @DexIgnore
        public Ci(Io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = M47.a(M47.Ci.PRIVACY, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + a2);
            Io6 io6 = this.b;
            Wg6.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Io6 b;

        @DexIgnore
        public Di(Io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = M47.a(M47.Ci.TERMS, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + a2);
            Io6 io6 = this.b;
            Wg6.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Io6 b;

        @DexIgnore
        public Ei(Io6 io6) {
            this.b = io6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String a2 = M47.a(M47.Ci.SOURCE_LICENSES, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + a2);
            Io6 io6 = this.b;
            Wg6.b(a2, "url");
            io6.M6(a2);
        }
    }

    @DexIgnore
    public void L6(Go6 go6) {
        Wg6.c(go6, "presenter");
        I14.l(go6);
        Wg6.b(go6, "Preconditions.checkNotNull(presenter)");
        this.h = go6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Go6 go6) {
        L6(go6);
    }

    @DexIgnore
    public final void M6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), Pp6.k.a());
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        O15 o15 = (O15) Aq0.f(LayoutInflater.from(getContext()), 2131558490, null, false, A6());
        this.g = new G37<>(this, o15);
        Wg6.b(o15, "binding");
        return o15.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Go6 go6 = this.h;
        if (go6 != null) {
            go6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Go6 go6 = this.h;
        if (go6 != null) {
            go6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<O15> g37 = this.g;
        if (g37 != null) {
            O15 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new Bi(this));
                a2.s.setOnClickListener(new Ci(this));
                a2.t.setOnClickListener(new Di(this));
                a2.r.setOnClickListener(new Ei(this));
                if (!Wr4.a.a().j()) {
                    RelativeLayout relativeLayout = a2.r;
                    Wg6.b(relativeLayout, "binding.btOpenSourceLicense");
                    relativeLayout.setVisibility(4);
                    View view2 = a2.C;
                    Wg6.b(view2, "binding.vLine2");
                    view2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
