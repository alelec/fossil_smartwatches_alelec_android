package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Vd4 {
    @DexIgnore
    Vd4 d(String str) throws IOException;

    @DexIgnore
    Vd4 e(boolean z) throws IOException;
}
