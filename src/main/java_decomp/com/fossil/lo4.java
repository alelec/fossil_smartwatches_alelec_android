package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.fossil.R62;
import com.google.android.gms.location.LocationRequest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lo4 implements R62.Bi, R62.Ci, Ga3 {
    @DexIgnore
    public static /* final */ String i; // = "lo4";
    @DexIgnore
    public static Lo4 j;
    @DexIgnore
    public R62 b;
    @DexIgnore
    public CopyOnWriteArrayList<Bi> c; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Context d;
    @DexIgnore
    public Timer e;
    @DexIgnore
    public TimerTask f;
    @DexIgnore
    public double g;
    @DexIgnore
    public double h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends TimerTask {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            SecureRandom secureRandom = new SecureRandom();
            Lo4.this.g += (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Lo4.this.h += (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Location location = new Location("");
            location.setLatitude(Lo4.this.g + 10.7604877d);
            location.setLongitude(Lo4.this.h + 106.698541d);
            Iterator it = Lo4.this.c.iterator();
            while (it.hasNext()) {
                ((Bi) it.next()).a(location, 1);
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(Location location, int i);
    }

    @DexIgnore
    public static Lo4 h(Context context) {
        Lo4 lo4;
        synchronized (Lo4.class) {
            try {
                if (j == null) {
                    j = new Lo4();
                }
                j.d = context.getApplicationContext();
                lo4 = j;
            } catch (Throwable th) {
                throw th;
            }
        }
        return lo4;
    }

    @DexIgnore
    @Override // com.fossil.K72
    public void d(int i2) {
        String str = i;
        Log.i(str, "MFLocationService is suspended - i=" + i2);
    }

    @DexIgnore
    @Override // com.fossil.K72
    public void e(Bundle bundle) {
        Log.i(i, "MFLocationService is connected");
        m();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0024 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int j() {
        /*
            r4 = this;
            r2 = 0
            android.content.Context r0 = r4.d
            int r0 = com.fossil.G62.g(r0)
            if (r0 == 0) goto L_0x000b
            r2 = -2
        L_0x000a:
            return r2
        L_0x000b:
            android.content.Context r0 = r4.d
            java.lang.String r1 = "location"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.location.LocationManager r0 = (android.location.LocationManager) r0
            java.lang.String r1 = "gps"
            boolean r1 = r0.isProviderEnabled(r1)     // Catch:{ Exception -> 0x0028 }
            java.lang.String r3 = "network"
            boolean r0 = r0.isProviderEnabled(r3)     // Catch:{ Exception -> 0x002d }
            r3 = r0
        L_0x0022:
            if (r1 != 0) goto L_0x000a
            if (r3 != 0) goto L_0x000a
            r2 = -1
            goto L_0x000a
        L_0x0028:
            r0 = move-exception
            r0 = r2
        L_0x002a:
            r3 = r2
            r1 = r0
            goto L_0x0022
        L_0x002d:
            r0 = move-exception
            r0 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lo4.j():int");
    }

    @DexIgnore
    public void k(Bi bi) {
        if (!Ko4.a(this.d)) {
            bi.a(null, -1);
            return;
        }
        String str = i;
        Log.i(str, "Register Location Service - callback=" + bi + ", size=" + this.c.size());
        this.c.add(bi);
        if (Jo4.b(this.d, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            l();
            return;
        }
        if (this.b == null) {
            R62.Ai ai = new R62.Ai(this.d);
            ai.a(Ha3.c);
            ai.d(this);
            ai.e(this);
            this.b = ai.g();
        }
        int j2 = j();
        if (j2 != 0) {
            bi.a(null, j2);
        } else {
            this.b.f();
        }
    }

    @DexIgnore
    public final void l() {
        o();
        this.e = new Timer();
        Ai ai = new Ai();
        this.f = ai;
        this.e.schedule(ai, 0, 1000);
    }

    @DexIgnore
    public final void m() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.k(0);
        locationRequest.A(1000);
        locationRequest.F(100);
        Ha3.d.b(this.b, locationRequest, this);
    }

    @DexIgnore
    @Override // com.fossil.R72
    public void n(Z52 z52) {
        Log.e(i, "MFLocationService is failed to connect");
    }

    @DexIgnore
    public final void o() {
        Timer timer = this.e;
        if (timer != null) {
            timer.cancel();
        }
        TimerTask timerTask = this.f;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ga3
    public void onLocationChanged(Location location) {
        String str = i;
        Log.d(str, "Inside " + i + ".onLocationUpdated - location=" + location);
        CopyOnWriteArrayList<Bi> copyOnWriteArrayList = this.c;
        if (copyOnWriteArrayList != null) {
            Iterator<Bi> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().a(location, 1);
            }
        }
    }

    @DexIgnore
    public void p(Bi bi) {
        this.c.remove(bi);
        String str = i;
        Log.i(str, "Unregister Location Service - callback=" + bi + ", size=" + this.c.size());
        if (Jo4.b(this.d, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            o();
            return;
        }
        R62 r62 = this.b;
        if (r62 != null && r62.n()) {
            Ha3.d.a(this.b, this);
            this.b.g();
        }
    }
}
