package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.FragmentManager;
import com.fossil.W47;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.AlertDialogFragment;
import com.mapped.AppWrapper;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.NumberPickerLarge;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S37 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ String b; // = b;
    @DexIgnore
    public static /* final */ S37 c; // = new S37();

    /*
    static {
        String simpleName = S37.class.getSimpleName();
        Wg6.b(simpleName, "DialogUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void A(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886879));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886878));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886877));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886876));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "FEEDBACK_CONFIRM");
    }

    @DexIgnore
    public final void A0(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "description");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
        fi.e(2131363317, str);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "LIMIT_WARNING");
    }

    @DexIgnore
    public final void B(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886735));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886734));
        fi.b(2131363373);
        fi.k(fragmentManager, "GOAL_TRACKING_ADD_FUTURE_ERROR");
    }

    @DexIgnore
    public final void B0(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "message");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363317, str);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886582));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.k(fragmentManager, "WATCH_FACE_CANNOT_OPEN");
    }

    @DexIgnore
    public final void C(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886901));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "GENERAL_ERROR");
    }

    @DexIgnore
    public final void C0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558483);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886338));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886336));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886337));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "FINDING_FRIEND");
    }

    @DexIgnore
    public final void D(FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886832));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.k(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void E(FragmentManager fragmentManager, String str, String str2) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "title");
        Wg6.c(str2, "description");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, str);
        fi.e(2131363317, str2);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "GENERAL_WARNING");
    }

    @DexIgnore
    public final void F(FragmentManager fragmentManager, int i, int i2, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "emailAddress");
        DashbarData dashbarData = new DashbarData(2131362968, i, i2);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558557);
        fi.e(2131362423, str);
        fi.a(dashbarData);
        fi.b(2131362686);
        fi.b(2131361942);
        fi.j(true);
        fi.h(false);
        fi.i(R.color.transparent);
        Wg6.b(fi, "AlertDialogFragment.Buil\u2026olor(R.color.transparent)");
        AlertDialogFragment f = fi.f("EMAIL_OTP_VERIFICATION");
        Wg6.b(f, "builder.create(EMAIL_OTP_VERIFICATION)");
        f.setStyle(0, 2131951629);
        f.show(fragmentManager, "EMAIL_OTP_VERIFICATION");
    }

    @DexIgnore
    public final void G(FragmentManager fragmentManager, int i, int i2, int i3, String[] strArr) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(strArr, "displayedValues");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558478);
        fi.b(2131362260);
        fi.b(2131363009);
        fi.c(2131362892, 1, 12, i);
        fi.c(2131362893, 0, 59, i2);
        fi.d(2131362896, 0, 1, i3, NumberPickerLarge.getTwoDigitFormatter(), strArr);
        fi.k(fragmentManager, "GOAL_TRACKING_ADD");
    }

    @DexIgnore
    public final void H(FragmentManager fragmentManager, GoalTrackingData goalTrackingData) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(goalTrackingData, "goalTrackingData");
        Bundle bundle = new Bundle();
        bundle.putSerializable("GOAL_TRACKING_DELETE_BUNDLE", goalTrackingData);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886739));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886738));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886737));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886736));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "GOAL_TRACKING_DELETE", bundle);
    }

    @DexIgnore
    public final void I(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886875));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886872));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886871));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886869));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "HAPPINESS_CONFIRM");
    }

    @DexIgnore
    public final void J(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886517));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886365));
        fi.b(2131363373);
        fi.k(fragmentManager, "COMMUTE_TIME_LOAD_LOCATION_FAIL");
    }

    @DexIgnore
    public final void K(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "description");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, str);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886392));
        fi.b(2131363373);
        fi.k(fragmentManager, "LOCATION_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void L(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886367));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886365));
        fi.b(2131363373);
        fi.k(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void M(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886366));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886365));
        fi.b(2131363373);
        fi.k(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void N(FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558488);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886794));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.h(false);
        fi.k(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void O(FragmentManager fragmentManager, int i, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                D(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        N(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            D(fragmentManager);
                            return;
                        } else if (str != null) {
                            x(str, fragmentManager);
                            return;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
                o0(fragmentManager);
            } else {
                p0(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void P(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887110));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887109));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887108));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "CONFIRM_LOGOUT_ACCOUNT");
    }

    @DexIgnore
    public final void Q(FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363373);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886794));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886796));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886797));
        fi.b(2131363291);
        fi.k(fragmentManager, "NO_INTERNET_CONNECTION");
    }

    @DexIgnore
    public final void R(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance().getApplicationContext(), 2131886468));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886831));
        fi.b(2131363373);
        fi.k(fragmentManager, "");
    }

    @DexIgnore
    public final void S(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886899);
        Wg6.b(c2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886928);
        Wg6.b(c3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        U(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void T(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886913);
        Wg6.b(c2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886806);
        Wg6.b(c3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        U(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void U(FragmentManager fragmentManager, String str, String str2) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363410, str);
        fi.e(2131363317, str2);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887075));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.k(fragmentManager, "REQUEST_OPEN_LOCATION_SERVICE");
    }

    @DexIgnore
    public final void V(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886885));
        fi.b(2131363373);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.h(false);
        fi.k(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void W(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, Constants.ACTIVITY);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886995));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "WRONG_FORMAT_PASSWORD");
    }

    @DexIgnore
    public final void X(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886901));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "PROCESS_IMAGE_ERROR");
    }

    @DexIgnore
    public final void Y(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886126));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886880));
        fi.b(2131363373);
        fi.k(fragmentManager, "QUICK_RESPONSE_WARNING");
    }

    @DexIgnore
    public final void Z(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886083));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886392));
        fi.b(2131363373);
        fi.k(fragmentManager, "MAX_NUMBER_OF_ALARMS");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.b(2131363373);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886159));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886158));
        fi.k(fragmentManager, "NOTIFICATION_WARNING");
    }

    @DexIgnore
    public final void a0(FragmentManager fragmentManager, String str, int i, int i2, AppWrapper appWrapper) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "name");
        Wg6.c(appWrapper, "appWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_APPWRAPPER", appWrapper);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        Hr7 hr7 = Hr7.a;
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886153);
        Wg6.b(c2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{str, String.valueOf(i), String.valueOf(i2)}, 3));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        fi.e(2131363317, format);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886152));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886151));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "CONFIRM_REASSIGN_APP", bundle);
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886174));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886173));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886172));
        fi.k(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void b0(FragmentManager fragmentManager, J06 j06, int i, int i2) {
        String displayName;
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(j06, "contactWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER", j06);
        Contact contact = j06.getContact();
        if (contact == null || contact.getContactId() != -100) {
            Contact contact2 = j06.getContact();
            if (contact2 == null || contact2.getContactId() != -200) {
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    displayName = contact3.getDisplayName();
                    Wg6.b(displayName, "contactWrapper.contact!!.displayName");
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                displayName = Um5.c(PortfolioApp.get.instance(), 2131886155);
                Wg6.b(displayName, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
            }
        } else {
            displayName = Um5.c(PortfolioApp.get.instance(), 2131886154);
            Wg6.b(displayName, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
        }
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        Hr7 hr7 = Hr7.a;
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886153);
        Wg6.b(c2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{displayName, Integer.valueOf(i), Integer.valueOf(i2)}, 3));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        fi.e(2131363317, format);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886152));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886151));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "CONFIRM_REASSIGN_CONTACT", bundle);
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886828));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886826));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886821));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886819));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.h(false);
        AlertDialogFragment k = fi.k(fragmentManager, "ASK_TO_CANCEL_WORKOUT");
        Wg6.b(k, "AlertDialogFragment.Buil\u2026r, ASK_TO_CANCEL_WORKOUT)");
        k.setCancelable(false);
    }

    @DexIgnore
    public final void c0(FragmentManager fragmentManager, ContactGroup contactGroup) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(contactGroup, "contactGroup");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE", contactGroup);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887545));
        Hr7 hr7 = Hr7.a;
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131887546);
        Wg6.b(c2, "LanguageHelper.getString\u2026move_contact_description)");
        Contact contact = contactGroup.getContacts().get(0);
        Wg6.b(contact, "contactGroup.contacts[0]");
        String format = String.format(c2, Arrays.copyOf(new Object[]{contact.getDisplayName()}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        fi.e(2131363317, format);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887181));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886545));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "CONFIRM_REMOVE_CONTACT", bundle);
    }

    @DexIgnore
    public final void d(FragmentManager fragmentManager, Bundle bundle, String str, String str2) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(bundle, "bundle");
        Wg6.c(str, "type");
        Wg6.c(str2, "message");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558483);
        fi.e(2131363317, str2);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886324));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886869));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, str, bundle);
    }

    @DexIgnore
    public final void d0(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "deviceName");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887183));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887182));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887181));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887180));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "CONFIRM_REMOVE_DEVICE");
    }

    @DexIgnore
    public final void e(FragmentManager fragmentManager, Bundle bundle) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(bundle, "bundle");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886567));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886566));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886563));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886562));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "DELETE_PHOTO_BACKGROUND", bundle);
    }

    @DexIgnore
    public final void e0(String str, FragmentManager fragmentManager) {
        Wg6.c(str, "serial");
        Wg6.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886824));
        Hr7 hr7 = Hr7.a;
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886825);
        Wg6.b(c2, "LanguageHelper.getString\u2026AnActiveDeviceIsDisabled)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{str}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        fi.e(2131363317, format);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886822));
        fi.b(2131363373);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886820));
        fi.b(2131363291);
        fi.m(fragmentManager, "REMOVE_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final String f() {
        return b;
    }

    @DexIgnore
    public final void f0(String str, FragmentManager fragmentManager) {
        Wg6.c(str, "serial");
        Wg6.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886818));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886817));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886816));
        fi.b(2131363373);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886815));
        fi.b(2131363291);
        fi.m(fragmentManager, "REMOVE_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void g(Integer num, String str, FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        if (num == null) {
            C(fragmentManager);
        } else {
            n0(num.intValue(), str, fragmentManager);
        }
    }

    @DexIgnore
    public final void g0(FragmentManager fragmentManager, String str) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363410, str);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886802));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886803));
        fi.k(fragmentManager, b);
    }

    @DexIgnore
    public final void h(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886320));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886325));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886324));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886323));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "LEAVE_CHALLENGE");
    }

    @DexIgnore
    public final void h0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887319));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887584));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887075));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.k(fragmentManager, "REQUEST_CONTACT_PHONE_SMS_PERMISSION");
    }

    @DexIgnore
    public final void i(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, "Sorry, you are required to log out as you are using unauthorized app");
        fi.h(false);
        fi.k(fragmentManager, "AA_WARNING");
    }

    @DexIgnore
    public final void i0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886830);
        Wg6.b(c2, "LanguageHelper.getString\u2026ourSmartwatchsFindDevice)");
        g0(fragmentManager, c2);
    }

    @DexIgnore
    public final void j(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886874));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886873));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886870));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886868));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "APP_RATING_CONFIRM");
    }

    @DexIgnore
    public final void j0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886927);
        Wg6.b(c2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886928);
        Wg6.b(c3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        l0(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void k(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, Constants.ACTIVITY);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363291);
        fi.b(2131363373);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886898));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886929));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887075));
        fi.h(false);
        fi.k(fragmentManager, "BLUETOOTH_OFF");
    }

    @DexIgnore
    public final void k0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886927);
        Wg6.b(c2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886806);
        Wg6.b(c3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        l0(fragmentManager, c2, c3);
    }

    @DexIgnore
    public final void l(FragmentManager fragmentManager, int i) {
        Wg6.c(fragmentManager, "fragmentManager");
        W47.Ki D6 = W47.D6();
        D6.e(0);
        D6.b(false);
        D6.d(i);
        D6.c(-65536);
        D6.f(false);
        D6.a().show(fragmentManager, "COLOR_PICKER_DIALOG");
    }

    @DexIgnore
    public final void l0(FragmentManager fragmentManager, String str, String str2) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, str2);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886798));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886799));
        fi.k(fragmentManager, "REQUEST_LOCATION_SERVICE_PERMISSION");
    }

    @DexIgnore
    public final void m(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "watchFaceId");
        Bundle bundle = new Bundle();
        bundle.putString("WATCH_FACE_ID", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886565));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886563));
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886564));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886562));
        fi.m(fragmentManager, "DELETE_MY_FACE", bundle);
    }

    @DexIgnore
    public final void m0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886981));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "RESET_PASS_SUCCESS");
    }

    @DexIgnore
    public final void n(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887392));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887499));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887320));
        fi.k(fragmentManager, "DELETE_THEME");
    }

    @DexIgnore
    public final void n0(int i, String str, FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
            return;
        }
        switch (i) {
            case 401:
                return;
            case MFNetworkReturnCode.CLIENT_TIMEOUT /* 408 */:
                D(fragmentManager);
                return;
            case MFNetworkReturnCode.RATE_LIMIT_EXEEDED /* 429 */:
                p0(fragmentManager);
                return;
            case 500:
            case 503:
            case 504:
                o0(fragmentManager);
                return;
            case 601:
                Q(fragmentManager);
                return;
            case 400002:
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                Wg6.b(c2, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c3 = Um5.c(PortfolioApp.get.instance(), 2131886232);
                Wg6.b(c3, "LanguageHelper.getString\u2026tTimeShouldBeGreaterThan)");
                E(fragmentManager, c2, c3);
                return;
            case 400605:
            case 400611:
                String c4 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                Wg6.b(c4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c5 = Um5.c(PortfolioApp.get.instance(), 2131886227);
                Wg6.b(c5, "LanguageHelper.getString\u2026eIsNotAvailableOrAlready)");
                E(fragmentManager, c4, c5);
                return;
            case 400609:
                String c6 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                Wg6.b(c6, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String c7 = Um5.c(PortfolioApp.get.instance(), 2131886230);
                Wg6.b(c7, "LanguageHelper.getString\u2026ayersPerChallengeReached)");
                E(fragmentManager, c6, c7);
                return;
            default:
                if (TextUtils.isEmpty(str)) {
                    D(fragmentManager);
                    return;
                } else if (str != null) {
                    x(str, fragmentManager);
                    return;
                } else {
                    Wg6.i();
                    throw null;
                }
        }
    }

    @DexIgnore
    public final void o(FragmentManager fragmentManager, String str, String str2, Ps4 ps4) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "title");
        Wg6.c(str2, "description");
        Bundle bundle = new Bundle();
        bundle.putParcelable("CHALLENGE", ps4);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, str);
        fi.e(2131363317, str2);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886324));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887108));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(fragmentManager, "LEAVE_CHALLENGE", bundle);
    }

    @DexIgnore
    public final void o0(FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886881));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886882));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886880));
        fi.b(2131363373);
        fi.k(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void p(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887393));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887499));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887320));
        fi.k(fragmentManager, "APPLY_NEW_COLOR_THEME");
    }

    @DexIgnore
    public final void p0(FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886987));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void q(FragmentManager fragmentManager, String str) {
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(str, "description");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, "");
        fi.e(2131363317, str);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886114));
        fi.b(2131363373);
        fi.k(fragmentManager, "DND_SCHEDULED_TIME_ERROR");
    }

    @DexIgnore
    public final void q0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragment");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887398));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887567));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.b(2131363373);
        fi.k(fragmentManager, "CONFIRM_SET_ALARM_FAILED");
    }

    @DexIgnore
    public final void r(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887096));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887095));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887091));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887090));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "CONFIRM_DELETE_ACCOUNT");
    }

    @DexIgnore
    public final void r0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558630);
        fi.b(2131362686);
        fi.b(2131362686);
        fi.k(fragmentManager, "DEVICE_SET_DATA_FAILED");
    }

    @DexIgnore
    public final void s(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886101));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886100));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886099));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "CONFIRM_DELETE_ALARM");
    }

    @DexIgnore
    public final void s0(FragmentManager fragmentManager, Rh5 rh5, int i) {
        String format;
        Wg6.c(fragmentManager, "fragmentManager");
        Wg6.c(rh5, "type");
        int i2 = R37.a[rh5.ordinal()];
        if (i2 == 1) {
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131887120);
            Wg6.b(c2, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 2) {
            Hr7 hr72 = Hr7.a;
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131887111);
            Wg6.b(c3, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c3, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 3) {
            Hr7 hr73 = Hr7.a;
            String c4 = Um5.c(PortfolioApp.get.instance(), 2131887113);
            Wg6.b(c4, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c4, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 == 4) {
            Hr7 hr74 = Hr7.a;
            String c5 = Um5.c(PortfolioApp.get.instance(), 2131887115);
            Wg6.b(c5, "LanguageHelper.getString\u2026t__PleaseSetASleepGoalOf)");
            format = String.format(c5, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
        } else if (i2 != 5) {
            format = "";
        } else {
            Hr7 hr75 = Hr7.a;
            String c6 = Um5.c(PortfolioApp.get.instance(), 2131887128);
            Wg6.b(c6, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            format = String.format(c6, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
        }
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.b(2131363373);
        fi.e(2131363317, format);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887119));
        fi.k(fragmentManager, "GOAL_EXCEED_VALUE");
    }

    @DexIgnore
    public final void t(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131887096));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131887095));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131887091));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131887090));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "DELETE_WORKOUT");
    }

    @DexIgnore
    public final void t0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886129));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886134));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886130));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886952));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.k(fragmentManager, "SET TO WATCH FAIL");
    }

    @DexIgnore
    public final void u(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886582));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886579));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.k(fragmentManager, "DIANA_REQUIRE");
    }

    @DexIgnore
    public final void u0(String str, FragmentManager fragmentManager) {
        Wg6.c(str, "serial");
        Wg6.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886818));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886817));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886816));
        fi.b(2131363373);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886815));
        fi.b(2131363291);
        fi.m(fragmentManager, "SWITCH_DEVICE_ERASE_FAIL", bundle);
    }

    @DexIgnore
    public final void v(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886835));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886834));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886833));
        fi.k(fragmentManager, "DOWNLOAD");
    }

    @DexIgnore
    public final void v0(String str, FragmentManager fragmentManager) {
        Wg6.c(str, "serial");
        Wg6.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886823));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886827));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886822));
        fi.b(2131363373);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886820));
        fi.b(2131363291);
        fi.m(fragmentManager, "SWITCH_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void w(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886849));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886848));
        fi.b(2131363373);
        fi.k(fragmentManager, "SEARCH_ON_STORE");
    }

    @DexIgnore
    public final void w0(String str, FragmentManager fragmentManager) {
        Wg6.c(str, "serial");
        Wg6.c(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886818));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886817));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886816));
        fi.b(2131363373);
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886815));
        fi.b(2131363291);
        fi.m(fragmentManager, "SWITCH_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void x(String str, FragmentManager fragmentManager) {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
        fi.e(2131363317, str);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.k(fragmentManager, "SERVER_ERROR");
    }

    @DexIgnore
    public final void x0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886885));
        fi.b(2131363373);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
        fi.k(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void y(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886884));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886883));
        fi.b(2131363373);
        fi.k(fragmentManager, "FAIL_DUE_TO_DEVICE_DISCONNECTED");
    }

    @DexIgnore
    public final void y0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.b(2131363373);
        fi.b(2131363291);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886174));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886173));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886172));
        fi.k(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void z(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragment");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886235));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886571));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886793));
        fi.b(2131363373);
        fi.k(fragmentManager, "FAILED_LOADING_WATCH_FACE");
    }

    @DexIgnore
    public final void z0(FragmentManager fragmentManager) {
        Wg6.c(fragmentManager, "fragmentManager");
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
        fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886791));
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886790));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886787));
        fi.h(false);
        fi.b(2131363373);
        fi.k(fragmentManager, "FIRMWARE_UPDATE_FAIL");
    }
}
