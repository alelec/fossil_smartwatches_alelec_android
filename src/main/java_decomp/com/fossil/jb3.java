package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jb3 extends Gr2 implements Ib3 {
    @DexIgnore
    public Jb3() {
        super("com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    public static Ib3 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
        return queryLocalInterface instanceof Ib3 ? (Ib3) queryLocalInterface : new Kb3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.Gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            S1((LocationResult) Qr2.a(parcel, LocationResult.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            j1((LocationAvailability) Qr2.a(parcel, LocationAvailability.CREATOR));
        }
        return true;
    }
}
