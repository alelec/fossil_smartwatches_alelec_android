package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O65 extends N65 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362968, 1);
        A.put(2131363410, 2);
        A.put(2131362712, 3);
        A.put(2131362424, 4);
        A.put(2131362423, 5);
        A.put(2131362427, 6);
        A.put(2131361942, 7);
    }
    */

    @DexIgnore
    public O65(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public O65(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (RTLImageView) objArr[3], (DashBar) objArr[1], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2]);
        this.y = -1;
        this.w.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
