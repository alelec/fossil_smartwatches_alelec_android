package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rs5 implements MembersInjector<DebugActivity> {
    @DexIgnore
    public static void a(DebugActivity debugActivity, Cj4 cj4) {
        debugActivity.G = cj4;
    }

    @DexIgnore
    public static void b(DebugActivity debugActivity, DianaPresetRepository dianaPresetRepository) {
        debugActivity.E = dianaPresetRepository;
    }

    @DexIgnore
    public static void c(DebugActivity debugActivity, FirmwareFileRepository firmwareFileRepository) {
        debugActivity.C = firmwareFileRepository;
    }

    @DexIgnore
    public static void d(DebugActivity debugActivity, GuestApiService guestApiService) {
        debugActivity.D = guestApiService;
    }

    @DexIgnore
    public static void e(DebugActivity debugActivity, UpdateDeviceToSpecificFirmwareUsecase updateDeviceToSpecificFirmwareUsecase) {
        debugActivity.B = updateDeviceToSpecificFirmwareUsecase;
    }

    @DexIgnore
    public static void f(DebugActivity debugActivity, ShakeFeedbackService shakeFeedbackService) {
        debugActivity.F = shakeFeedbackService;
    }

    @DexIgnore
    public static void g(DebugActivity debugActivity, An4 an4) {
        debugActivity.A = an4;
    }
}
