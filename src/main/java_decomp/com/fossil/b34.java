package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B34<K, V> extends V24<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> extends Bi<K, V> {
        @DexIgnore
        public /* final */ transient B34<K, V> c;

        @DexIgnore
        public Ai(K k, V v, B34<K, V> b34, B34<K, V> b342) {
            super(k, v, b34);
            this.c = b342;
        }

        @DexIgnore
        @Override // com.fossil.B34
        public B34<K, V> getNextInValueBucket() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K, V> extends B34<K, V> {
        @DexIgnore
        public /* final */ transient B34<K, V> b;

        @DexIgnore
        public Bi(K k, V v, B34<K, V> b34) {
            super(k, v);
            this.b = b34;
        }

        @DexIgnore
        @Override // com.fossil.B34
        public final B34<K, V> getNextInKeyBucket() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.B34
        public final boolean isReusable() {
            return false;
        }
    }

    @DexIgnore
    public B34(B34<K, V> b34) {
        super(b34.getKey(), b34.getValue());
    }

    @DexIgnore
    public B34(K k, V v) {
        super(k, v);
        A24.a(k, v);
    }

    @DexIgnore
    public static <K, V> B34<K, V>[] createEntryArray(int i) {
        return new B34[i];
    }

    @DexIgnore
    public B34<K, V> getNextInKeyBucket() {
        return null;
    }

    @DexIgnore
    public B34<K, V> getNextInValueBucket() {
        return null;
    }

    @DexIgnore
    public boolean isReusable() {
        return true;
    }
}
