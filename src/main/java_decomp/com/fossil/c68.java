package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C68 extends OutputStream {
    @DexIgnore
    public static /* final */ byte[] g; // = new byte[0];
    @DexIgnore
    public /* final */ List<byte[]> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public byte[] e;
    @DexIgnore
    public int f;

    @DexIgnore
    public C68() {
        this(1024);
    }

    @DexIgnore
    public C68(int i) {
        this.b = new ArrayList();
        if (i >= 0) {
            synchronized (this) {
                a(i);
            }
            return;
        }
        throw new IllegalArgumentException("Negative initial size: " + i);
    }

    @DexIgnore
    public final void a(int i) {
        if (this.c < this.b.size() - 1) {
            this.d += this.e.length;
            int i2 = this.c + 1;
            this.c = i2;
            this.e = this.b.get(i2);
            return;
        }
        byte[] bArr = this.e;
        if (bArr == null) {
            this.d = 0;
        } else {
            i = Math.max(bArr.length << 1, i - this.d);
            this.d += this.e.length;
        }
        this.c++;
        byte[] bArr2 = new byte[i];
        this.e = bArr2;
        this.b.add(bArr2);
    }

    @DexIgnore
    public byte[] b() {
        int i = 0;
        synchronized (this) {
            int i2 = this.f;
            if (i2 == 0) {
                return g;
            }
            byte[] bArr = new byte[i2];
            for (byte[] bArr2 : this.b) {
                int min = Math.min(bArr2.length, i2);
                System.arraycopy(bArr2, 0, bArr, i, min);
                i += min;
                i2 -= min;
                if (i2 == 0) {
                    break;
                }
            }
            return bArr;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
    }

    @DexIgnore
    public String toString() {
        return new String(b());
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(int i) {
        synchronized (this) {
            int i2 = this.f - this.d;
            if (i2 == this.e.length) {
                a(this.f + 1);
                i2 = 0;
            }
            this.e[i2] = (byte) ((byte) i);
            this.f++;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i < 0 || i > bArr.length || i2 < 0 || (i3 = i + i2) > bArr.length || i3 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            synchronized (this) {
                int i4 = this.f + i2;
                int i5 = this.f - this.d;
                while (i2 > 0) {
                    int min = Math.min(i2, this.e.length - i5);
                    System.arraycopy(bArr, i3 - i2, this.e, i5, min);
                    i2 -= min;
                    if (i2 > 0) {
                        a(i4);
                        i5 = 0;
                    }
                }
                this.f = i4;
            }
        }
    }
}
