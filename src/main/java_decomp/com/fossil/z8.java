package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class Z8 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Zm1.values().length];
        a = iArr;
        iArr[Zm1.BIOMETRIC_PROFILE.ordinal()] = 1;
        a[Zm1.DAILY_STEP.ordinal()] = 2;
        a[Zm1.DAILY_STEP_GOAL.ordinal()] = 3;
        a[Zm1.DAILY_CALORIE.ordinal()] = 4;
        a[Zm1.DAILY_CALORIE_GOAL.ordinal()] = 5;
        a[Zm1.DAILY_TOTAL_ACTIVE_MINUTE.ordinal()] = 6;
        a[Zm1.DAILY_ACTIVE_MINUTE_GOAL.ordinal()] = 7;
        a[Zm1.DAILY_DISTANCE.ordinal()] = 8;
        a[Zm1.INACTIVE_NUDGE.ordinal()] = 9;
        a[Zm1.VIBE_STRENGTH.ordinal()] = 10;
        a[Zm1.DO_NOT_DISTURB_SCHEDULE.ordinal()] = 11;
        a[Zm1.TIME.ordinal()] = 12;
        a[Zm1.BATTERY.ordinal()] = 13;
        a[Zm1.HEART_RATE_MODE.ordinal()] = 14;
        a[Zm1.DAILY_SLEEP.ordinal()] = 15;
        a[Zm1.DISPLAY_UNIT.ordinal()] = 16;
        a[Zm1.SECOND_TIMEZONE_OFFSET.ordinal()] = 17;
        a[Zm1.CURRENT_HEART_RATE.ordinal()] = 18;
        a[Zm1.HELLAS_BATTERY.ordinal()] = 19;
        a[Zm1.AUTO_WORKOUT_DETECTION.ordinal()] = 20;
        a[Zm1.CYCLING_CADENCE.ordinal()] = 21;
    }
    */
}
