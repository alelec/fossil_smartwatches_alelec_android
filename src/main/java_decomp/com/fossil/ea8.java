package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import com.fossil.Qa8;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ea8 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<byte[], Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ya8 ya8, Bitmap bitmap) {
            super(1);
            this.$resultHandler = ya8;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(byte[] bArr) {
            invoke(bArr);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(byte[] bArr) {
            this.$resultHandler.c(bArr);
            Bitmap bitmap = this.$bitmap;
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
    }

    @DexIgnore
    public Ea8(Context context) {
        Wg6.c(context, "context");
        this.b = context;
    }

    @DexIgnore
    public final void a(String str, Ya8 ya8) {
        Wg6.c(str, "id");
        Wg6.c(ya8, "resultHandler");
        ya8.c(Boolean.valueOf(g().b(this.b, str)));
    }

    @DexIgnore
    public final void b() {
        g().l();
    }

    @DexIgnore
    public final List<String> c(List<String> list) {
        Wg6.c(list, "ids");
        return g().d(this.b, list);
    }

    @DexIgnore
    public final boolean d() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @DexIgnore
    public final List<Ka8> e(String str, int i, int i2, int i3, long j, La8 la8) {
        Wg6.c(str, "galleryId");
        Wg6.c(la8, "option");
        return Qa8.Bi.d(g(), this.b, Wg6.a(str, "isAll") ? "" : str, i, i2, i3, j, la8, null, 128, null);
    }

    @DexIgnore
    public final List<Ka8> f(String str, int i, int i2, int i3, long j, La8 la8) {
        Wg6.c(str, "galleryId");
        Wg6.c(la8, "option");
        return g().e(this.b, Wg6.a(str, "isAll") ? "" : str, i2, i3, i, j, la8);
    }

    @DexIgnore
    public final Qa8 g() {
        return (this.a || Build.VERSION.SDK_INT < 29) ? Pa8.f : Na8.e;
    }

    @DexIgnore
    public final void h(String str, boolean z, Ya8 ya8) {
        Wg6.c(str, "id");
        Wg6.c(ya8, "resultHandler");
        ya8.c(g().h(this.b, str, z));
    }

    @DexIgnore
    public final List<Ma8> i(int i, long j, boolean z, La8 la8) {
        Wg6.c(la8, "option");
        List<Ma8> c = g().c(this.b, i, j, la8);
        if (!z) {
            return c;
        }
        int i2 = 0;
        for (Ma8 ma8 : c) {
            i2 += ma8.b();
        }
        return Pm7.V(Gm7.b(new Ma8("isAll", "Recent", i2, i, true)), c);
    }

    @DexIgnore
    public final Map<String, Double> j(String str) {
        Wg6.c(str, "id");
        Eq0 m = g().m(this.b, str);
        double[] k = m != null ? m.k() : null;
        if (k == null) {
            return Zm7.j(Hl7.a(Constants.LAT, Double.valueOf(0.0d)), Hl7.a("lng", Double.valueOf(0.0d)));
        }
        return Zm7.j(Hl7.a(Constants.LAT, Double.valueOf(k[0])), Hl7.a("lng", Double.valueOf(k[1])));
    }

    @DexIgnore
    public final void k(String str, boolean z, boolean z2, Ya8 ya8) {
        Wg6.c(str, "id");
        Wg6.c(ya8, "resultHandler");
        Ka8 j = g().j(this.b, str);
        if (j == null) {
            Ya8.e(ya8, "The asset not found", null, null, 6, null);
        } else if (!Qa8.a.e()) {
            ya8.c(Ap7.a(new File(j.i())));
        } else {
            byte[] f = g().f(this.b, j, z2);
            ya8.c(f);
            if (z) {
                g().a(this.b, j, f);
            }
        }
    }

    @DexIgnore
    public final Ma8 l(String str, int i, long j, La8 la8) {
        Wg6.c(str, "id");
        Wg6.c(la8, "option");
        if (!Wg6.a(str, "isAll")) {
            return g().o(this.b, str, i, j, la8);
        }
        List<Ma8> c = g().c(this.b, i, j, la8);
        if (c.isEmpty()) {
            return null;
        }
        int i2 = 0;
        for (Ma8 ma8 : c) {
            i2 += ma8.b();
        }
        return new Ma8("isAll", "Recent", i2, i, true);
    }

    @DexIgnore
    public final void m(String str, int i, int i2, int i3, Ya8 ya8) {
        Integer num = null;
        Wg6.c(str, "id");
        Wg6.c(ya8, "resultHandler");
        try {
            if (!d()) {
                Ka8 j = g().j(this.b, str);
                if (j == null) {
                    Ya8.e(ya8, "The asset not found!", null, null, 6, null);
                } else {
                    Wa8.a.b(this.b, j.i(), i, i2, i3, ya8.a());
                }
            } else {
                Ka8 j2 = g().j(this.b, str);
                if (j2 != null) {
                    num = Integer.valueOf(j2.j());
                }
                Bitmap g = g().g(this.b, str, i, i2, num);
                Wa8.a.a(this.b, g, i, i2, i3, new Ai(ya8, g));
            }
        } catch (Exception e) {
            Log.e("PhotoManagerPlugin", "get thumb error", e);
            ya8.d("201", "get thumb error", e);
        }
    }

    @DexIgnore
    public final Ka8 n(byte[] bArr, String str, String str2) {
        Wg6.c(bArr, "image");
        Wg6.c(str, "title");
        Wg6.c(str2, "description");
        return g().i(this.b, bArr, str, str2);
    }

    @DexIgnore
    public final Ka8 o(String str, String str2, String str3) {
        Wg6.c(str, "path");
        Wg6.c(str2, "title");
        Wg6.c(str3, Constants.DESC);
        if (!new File(str).exists()) {
            return null;
        }
        return g().k(this.b, new FileInputStream(str), str2, str3);
    }

    @DexIgnore
    public final void p(boolean z) {
        this.a = z;
    }
}
