package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.NotificationIcon;
import com.mapped.Rc6;
import com.mapped.Zd0;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pr extends Lp {
    @DexIgnore
    public /* final */ Zd0[] C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Pr(K5 k5, I60 i60, Zd0[] zd0Arr, String str, int i) {
        super(k5, i60, Yp.D0, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 16);
        this.C = zd0Arr;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        boolean z = false;
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (Zd0 zd0 : this.C) {
            Io1[] replyMessages = zd0.getReplyMessageGroup().getReplyMessages();
            for (Io1 io1 : replyMessages) {
                Oo1 messageIcon = io1.getMessageIcon();
                if (!messageIcon.d()) {
                    copyOnWriteArrayList.addIfAbsent(messageIcon);
                }
            }
        }
        Object[] array = copyOnWriteArrayList.toArray(new NotificationIcon[0]);
        if (array != null) {
            NotificationIcon[] notificationIconArr = (NotificationIcon[]) array;
            if (notificationIconArr.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    Xa xa = Xa.d;
                    Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.m.b));
                    if (ry1 == null) {
                        ry1 = Hd0.y.d();
                    }
                    Lp.h(this, new Zj(this.w, this.x, Yp.E0, true, 1795, xa.h(notificationIconArr, 1795, ry1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Zo(this), new Mp(this), new Zp(this), null, null, 48, null);
                } catch (Sx1 e) {
                    D90.i.i(e);
                    l(Nr.a(this.v, null, Zq.t, null, null, 13));
                }
            } else {
                H();
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.h5, Px1.a(this.C));
    }

    @DexIgnore
    public final void H() {
        short b = Ke.b.b(this.w.x, Ob.v);
        try {
            Ka ka = Ka.d;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.v.b));
            if (ry1 == null) {
                ry1 = new Ry1(2, 0);
            }
            Lp.h(this, new Zj(this.w, this.x, Yp.F0, true, b, ka.a(b, ry1, this.C), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), new Nq(this), new Br(this), null, null, null, 56, null);
        } catch (Sx1 e) {
            D90.i.i(e);
            l(Nr.a(this.v, null, Zq.q, null, null, 13));
        }
    }
}
