package com.fossil;

import com.fossil.Z62;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T62<R extends Z62> {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Status status);
    }

    @DexIgnore
    public abstract void b(Ai ai);

    @DexIgnore
    public abstract R c(long j, TimeUnit timeUnit);

    @DexIgnore
    public abstract void d(A72<? super R> a72);
}
