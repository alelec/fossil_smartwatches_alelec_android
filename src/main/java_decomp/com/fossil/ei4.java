package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ei4 extends Exception {
    @DexIgnore
    public static /* final */ int ERROR_INVALID_PARAMETERS; // = 1;
    @DexIgnore
    public static /* final */ int ERROR_SIZE; // = 2;
    @DexIgnore
    public static /* final */ int ERROR_TOO_MANY_MESSAGES; // = 4;
    @DexIgnore
    public static /* final */ int ERROR_TTL_EXCEEDED; // = 3;
    @DexIgnore
    public static /* final */ int ERROR_UNKNOWN; // = 0;
    @DexIgnore
    public /* final */ int errorCode;

    @DexIgnore
    public Ei4(String str) {
        super(str);
        this.errorCode = a(str);
    }

    @DexIgnore
    public final int a(String str) {
        if (str == null) {
            return 0;
        }
        String lowerCase = str.toLowerCase(Locale.US);
        char c = '\uffff';
        switch (lowerCase.hashCode()) {
            case -1743242157:
                if (lowerCase.equals("service_not_available")) {
                    c = 3;
                    break;
                }
                break;
            case -1290953729:
                if (lowerCase.equals("toomanymessages")) {
                    c = 4;
                    break;
                }
                break;
            case -920906446:
                if (lowerCase.equals("invalid_parameters")) {
                    c = 0;
                    break;
                }
                break;
            case -617027085:
                if (lowerCase.equals("messagetoobig")) {
                    c = 2;
                    break;
                }
                break;
            case -95047692:
                if (lowerCase.equals("missing_to")) {
                    c = 1;
                    break;
                }
                break;
        }
        if (c == 0 || c == 1) {
            return 1;
        }
        if (c == 2) {
            return 2;
        }
        if (c != 3) {
            return c == 4 ? 4 : 0;
        }
        return 3;
    }

    @DexIgnore
    public final int getErrorCode() {
        return this.errorCode;
    }
}
