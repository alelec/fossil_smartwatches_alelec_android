package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.X37;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv5 extends BaseFragment implements Iw6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Hw6 g;
    @DexIgnore
    public G37<P95> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Vv5.j;
        }

        @DexIgnore
        public final Vv5 b() {
            return new Vv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements K67 {
        @DexIgnore
        public /* final */ /* synthetic */ P95 a;
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 b;

        @DexIgnore
        public Bi(P95 p95, Vv5 vv5) {
            this.a = p95;
            this.b = vv5;
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void a(int i) {
            if (this.a.w.getUnit() == Ai5.METRIC) {
                Vv5.K6(this.b).o(i);
            } else {
                Vv5.K6(this.b).o(Math.round(Jk5.d((float) (i / 12), ((float) i) % 12.0f)));
            }
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void c(boolean z) {
            TabLayout tabLayout = this.a.z;
            Wg6.b(tabLayout, "it.tlHeightUnit");
            H57.c(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 a;

        @DexIgnore
        public Ci(Vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
            CharSequence h = gVar.h();
            Ai5 ai5 = Ai5.METRIC;
            if (Wg6.a(h, PortfolioApp.get.instance().getString(2131886954))) {
                ai5 = Ai5.IMPERIAL;
            }
            Vv5.K6(this.a).p(ai5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements K67 {
        @DexIgnore
        public /* final */ /* synthetic */ P95 a;
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 b;

        @DexIgnore
        public Di(P95 p95, Vv5 vv5) {
            this.a = p95;
            this.b = vv5;
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void a(int i) {
            if (this.a.x.getUnit() == Ai5.METRIC) {
                Vv5.K6(this.b).r(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            Vv5.K6(this.b).r(Math.round(Jk5.m(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.K67
        public void c(boolean z) {
            TabLayout tabLayout = this.a.A;
            Wg6.b(tabLayout, "it.tlWeightUnit");
            H57.c(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 a;

        @DexIgnore
        public Ei(Vv5 vv5) {
            this.a = vv5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
            CharSequence h = gVar.h();
            Ai5 ai5 = Ai5.METRIC;
            if (Wg6.a(h, PortfolioApp.get.instance().getString(2131886956))) {
                ai5 = Ai5.IMPERIAL;
            }
            Vv5.K6(this.a).q(ai5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            Wg6.c(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 b;

        @DexIgnore
        public Fi(Vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Vv5.K6(this.b).n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 b;

        @DexIgnore
        public Gi(Vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Vv5.K6(this.b).n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vv5 b;

        @DexIgnore
        public Hi(Vv5 vv5) {
            this.b = vv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Vv5.K6(this.b).n(false);
        }
    }

    /*
    static {
        String simpleName = Vv5.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Hw6 K6(Vv5 vv5) {
        Hw6 hw6 = vv5.g;
        if (hw6 != null) {
            return hw6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void H1(int i2, Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        int i3 = Wv5.a[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "updateData weight=" + i2 + " metric");
            G37<P95> g37 = this.h;
            if (g37 != null) {
                P95 a2 = g37.a();
                if (a2 != null) {
                    TabLayout.g v = a2.A.v(0);
                    if (v != null) {
                        v.k();
                    }
                    a2.x.setUnit(Ai5.METRIC);
                    a2.x.setFormatter(new ProfileFormatter(4));
                    a2.x.l(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            G37<P95> g372 = this.h;
            if (g372 != null) {
                P95 a3 = g372.a();
                if (a3 != null) {
                    TabLayout.g v2 = a3.A.v(1);
                    if (v2 != null) {
                        v2.k();
                    }
                    float h2 = Jk5.h((float) i2);
                    a3.x.setUnit(Ai5.IMPERIAL);
                    a3.x.setFormatter(new ProfileFormatter(4));
                    a3.x.l(780, 4401, Math.round(h2 * ((float) 10)));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Hw6 hw6) {
        M6(hw6);
    }

    @DexIgnore
    public void M6(Hw6 hw6) {
        Wg6.c(hw6, "presenter");
        this.g = hw6;
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void R0(int i2, Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        int i3 = Wv5.b[ai5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = j;
            local.d(str, "updateData height=" + i2 + " metric");
            G37<P95> g37 = this.h;
            if (g37 != null) {
                P95 a2 = g37.a();
                if (a2 != null) {
                    TabLayout.g v = a2.z.v(0);
                    if (v != null) {
                        v.k();
                    }
                    a2.w.setUnit(Ai5.METRIC);
                    a2.w.setFormatter(new ProfileFormatter(-1));
                    a2.w.l(100, 251, i2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            G37<P95> g372 = this.h;
            if (g372 != null) {
                P95 a3 = g372.a();
                if (a3 != null) {
                    TabLayout.g v2 = a3.z.v(1);
                    if (v2 != null) {
                        v2.k();
                    }
                    Lc6<Integer, Integer> b = Jk5.b((float) i2);
                    a3.w.setUnit(Ai5.IMPERIAL);
                    a3.w.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.w;
                    Integer first = b.getFirst();
                    Wg6.b(first, "currentHeightInFeetAndInches.first");
                    int e = Jk5.e(first.intValue());
                    Integer second = b.getSecond();
                    Wg6.b(second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.l(40, 99, second.intValue() + e);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void f() {
        DashBar dashBar;
        G37<P95> g37 = this.h;
        if (g37 != null) {
            P95 a2 = g37.a();
            if (a2 != null && (dashBar = a2.u) != null) {
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.e(dashBar, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void h() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void i() {
        if (isActive()) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886961);
            Wg6.b(c, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            H6(c);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<P95> g37 = new G37<>(this, (P95) Aq0.f(layoutInflater, 2131558598, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            P95 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Hw6 hw6 = this.g;
        if (hw6 != null) {
            hw6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Hw6 hw6 = this.g;
        if (hw6 != null) {
            hw6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<P95> g37 = this.h;
        if (g37 != null) {
            P95 a2 = g37.a();
            if (a2 != null) {
                a2.w.setValuePickerListener(new Bi(a2, this));
                a2.z.c(new Ci(this));
                a2.x.setValuePickerListener(new Di(a2, this));
                a2.A.c(new Ei(this));
                a2.t.setOnClickListener(new Fi(this));
                a2.C.setOnClickListener(new Gi(this));
                a2.q.setOnClickListener(new Hi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Iw6
    public void u2() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            Wg6.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, true, false, 4, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
