package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface W13 extends List {
    @DexIgnore
    void V(Xz2 xz2);

    @DexIgnore
    Object zzb(int i);

    @DexIgnore
    List<?> zzd();

    @DexIgnore
    W13 zze();
}
