package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class I11 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("WorkerFactory");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends I11 {
        @DexIgnore
        @Override // com.fossil.I11
        public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
            return null;
        }
    }

    @DexIgnore
    public static I11 c() {
        return new Ai();
    }

    @DexIgnore
    public abstract ListenableWorker a(Context context, String str, WorkerParameters workerParameters);

    @DexIgnore
    public final ListenableWorker b(Context context, String str, WorkerParameters workerParameters) {
        ListenableWorker listenableWorker;
        Class<? extends U> cls;
        ListenableWorker a2 = a(context, str, workerParameters);
        if (a2 == null) {
            try {
                cls = Class.forName(str).asSubclass(ListenableWorker.class);
            } catch (Throwable th) {
                X01 c = X01.c();
                String str2 = a;
                c.b(str2, "Invalid class: " + str, th);
                cls = null;
            }
            if (cls != null) {
                try {
                    listenableWorker = (ListenableWorker) cls.getDeclaredConstructor(Context.class, WorkerParameters.class).newInstance(context, workerParameters);
                } catch (Throwable th2) {
                    X01 c2 = X01.c();
                    String str3 = a;
                    c2.b(str3, "Could not instantiate " + str, th2);
                    listenableWorker = a2;
                }
                if (listenableWorker != null || !listenableWorker.j()) {
                    return listenableWorker;
                }
                throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", getClass().getName(), str));
            }
        }
        listenableWorker = a2;
        if (listenableWorker != null) {
        }
        return listenableWorker;
    }
}
