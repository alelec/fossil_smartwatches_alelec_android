package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qt4 {
    @DexIgnore
    public Ps4 a;
    @DexIgnore
    public Ms4 b;
    @DexIgnore
    public Ms4 c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public Qt4(Ps4 ps4, Ms4 ms4, Ms4 ms42, boolean z) {
        Wg6.c(ps4, "challenge");
        this.a = ps4;
        this.b = ms4;
        this.c = ms42;
        this.d = z;
    }

    @DexIgnore
    public final Ps4 a() {
        return this.a;
    }

    @DexIgnore
    public final Ms4 b() {
        return this.c;
    }

    @DexIgnore
    public final Ms4 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Qt4) {
                Qt4 qt4 = (Qt4) obj;
                if (!Wg6.a(this.a, qt4.a) || !Wg6.a(this.b, qt4.b) || !Wg6.a(this.c, qt4.c) || this.d != qt4.d) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Ps4 ps4 = this.a;
        int hashCode = ps4 != null ? ps4.hashCode() : 0;
        Ms4 ms4 = this.b;
        int hashCode2 = ms4 != null ? ms4.hashCode() : 0;
        Ms4 ms42 = this.c;
        if (ms42 != null) {
            i = ms42.hashCode();
        }
        boolean z = this.d;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "UIHistory(challenge=" + this.a + ", topPlayer=" + this.b + ", currentPlayer=" + this.c + ", isFirst=" + this.d + ")";
    }
}
