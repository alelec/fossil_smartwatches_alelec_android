package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.m62;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ea3 extends q62<m62.d.C0151d> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sq2 {
        @DexIgnore
        public /* final */ ot3<Void> b;

        @DexIgnore
        public a(ot3<Void> ot3) {
            this.b = ot3;
        }

        @DexIgnore
        @Override // com.fossil.rq2
        public final void M(pq2 pq2) {
            x72.a(pq2.a(), this.b);
        }
    }

    @DexIgnore
    public ea3(Context context) {
        super(context, ha3.c, (m62.d) null, new f72());
    }

    @DexIgnore
    public nt3<Location> s() {
        return e(new bb3(this));
    }

    @DexIgnore
    public nt3<Void> t(fa3 fa3) {
        return x72.c(g(q72.b(fa3, fa3.class.getSimpleName())));
    }

    @DexIgnore
    public nt3<Void> u(LocationRequest locationRequest, fa3 fa3, Looper looper) {
        ir2 c = ir2.c(locationRequest);
        p72 a2 = q72.a(fa3, pr2.a(looper), fa3.class.getSimpleName());
        return f(new cb3(this, a2, c, a2), new db3(this, a2.b()));
    }

    @DexIgnore
    public final rq2 w(ot3<Boolean> ot3) {
        return new eb3(this, ot3);
    }
}
