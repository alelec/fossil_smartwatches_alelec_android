package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fm extends Ro {
    @DexIgnore
    public /* final */ boolean S;
    @DexIgnore
    public Zm1[] T;
    @DexIgnore
    public /* final */ R60[] U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Fm(K5 k5, I60 i60, R60[] r60Arr, short s, String str, int i) {
        super(k5, i60, Yp.v, true, (i & 8) != 0 ? Ke.b.b(k5.x, Ob.l) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 160);
        this.U = r60Arr;
        this.S = true;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.R, Px1.a(this.U));
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro
    public JSONObject E() {
        JSONObject E = super.E();
        Jd0 jd0 = Jd0.S;
        Zm1[] zm1Arr = this.T;
        return G80.k(E, jd0, zm1Arr != null ? G80.h(zm1Arr) : null);
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public byte[] M() {
        try {
            Ya ya = Ya.f;
            short s = this.D;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.l.b));
            if (ry1 == null) {
                ry1 = Hd0.y.d();
            }
            return ya.a(s, ry1, this.U);
        } catch (Sx1 e) {
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public void P() {
        R60[] r60Arr = this.U;
        ArrayList arrayList = new ArrayList(r60Arr.length);
        for (R60 r60 : r60Arr) {
            arrayList.add(r60.getKey());
        }
        Object[] array = arrayList.toArray(new Zm1[0]);
        if (array != null) {
            Zm1[] zm1Arr = (Zm1[]) array;
            this.T = zm1Arr;
            Ky1 ky1 = Ky1.DEBUG;
            if (zm1Arr != null) {
                Wg6.b(Arrays.toString(zm1Arr), "java.util.Arrays.toString(this)");
            }
            super.P();
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean t() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        Zm1[] zm1Arr = this.T;
        return zm1Arr != null ? zm1Arr : new Zm1[0];
    }
}
