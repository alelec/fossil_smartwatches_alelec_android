package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hi1 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Bi i; // = new Ai();
    @DexIgnore
    public volatile Wa1 a;
    @DexIgnore
    public /* final */ Map<FragmentManager, Gi1> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<androidx.fragment.app.FragmentManager, Ki1> c; // = new HashMap();
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ Bi e;
    @DexIgnore
    public /* final */ Zi0<View, Fragment> f; // = new Zi0<>();
    @DexIgnore
    public /* final */ Zi0<View, android.app.Fragment> g; // = new Zi0<>();
    @DexIgnore
    public /* final */ Bundle h; // = new Bundle();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Bi {
        @DexIgnore
        @Override // com.fossil.Hi1.Bi
        public Wa1 a(Oa1 oa1, Di1 di1, Ii1 ii1, Context context) {
            return new Wa1(oa1, di1, ii1, context);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Wa1 a(Oa1 oa1, Di1 di1, Ii1 ii1, Context context);
    }

    @DexIgnore
    public Hi1(Bi bi) {
        this.e = bi == null ? i : bi;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    @DexIgnore
    @TargetApi(17)
    public static void a(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @DexIgnore
    public static Activity b(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return b(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @DexIgnore
    public static void e(Collection<Fragment> collection, Map<View, Fragment> map) {
        if (collection != null) {
            for (Fragment fragment : collection) {
                if (!(fragment == null || fragment.getView() == null)) {
                    map.put(fragment.getView(), fragment);
                    e(fragment.getChildFragmentManager().i0(), map);
                }
            }
        }
    }

    @DexIgnore
    public static boolean t(Context context) {
        Activity b2 = b(context);
        return b2 == null || !b2.isFinishing();
    }

    @DexIgnore
    @TargetApi(26)
    @Deprecated
    public final void c(FragmentManager fragmentManager, Zi0<View, android.app.Fragment> zi0) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment fragment : fragmentManager.getFragments()) {
                if (fragment.getView() != null) {
                    zi0.put(fragment.getView(), fragment);
                    c(fragment.getChildFragmentManager(), zi0);
                }
            }
            return;
        }
        d(fragmentManager, zi0);
    }

    @DexIgnore
    @Deprecated
    public final void d(FragmentManager fragmentManager, Zi0<View, android.app.Fragment> zi0) {
        int i2 = 0;
        while (true) {
            this.h.putInt("key", i2);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.h, "key");
            } catch (Exception e2) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    zi0.put(fragment.getView(), fragment);
                    if (Build.VERSION.SDK_INT >= 17) {
                        c(fragment.getChildFragmentManager(), zi0);
                    }
                }
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    @Deprecated
    public final android.app.Fragment f(View view, Activity activity) {
        this.g.clear();
        c(activity.getFragmentManager(), this.g);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.g.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.g.clear();
        return fragment;
    }

    @DexIgnore
    public final Fragment g(View view, FragmentActivity fragmentActivity) {
        this.f.clear();
        e(fragmentActivity.getSupportFragmentManager().i0(), this.f);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.f.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.f.clear();
        return fragment;
    }

    @DexIgnore
    @Deprecated
    public final Wa1 h(Context context, FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        Gi1 q = q(fragmentManager, fragment, z);
        Wa1 e2 = q.e();
        if (e2 != null) {
            return e2;
        }
        Wa1 a2 = this.e.a(Oa1.c(context), q.c(), q.f(), context);
        q.k(a2);
        return a2;
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        ComponentCallbacks remove;
        Object obj;
        boolean z = true;
        int i2 = message.what;
        if (i2 == 1) {
            Object obj2 = (FragmentManager) message.obj;
            remove = this.b.remove(obj2);
            obj = obj2;
        } else if (i2 != 2) {
            z = false;
            remove = null;
            obj = null;
        } else {
            Object obj3 = (androidx.fragment.app.FragmentManager) message.obj;
            remove = this.c.remove(obj3);
            obj = obj3;
        }
        if (z && remove == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj);
        }
        return z;
    }

    @DexIgnore
    public Wa1 i(Activity activity) {
        if (Jk1.p()) {
            return k(activity.getApplicationContext());
        }
        a(activity);
        return h(activity, activity.getFragmentManager(), null, t(activity));
    }

    @DexIgnore
    @TargetApi(17)
    @Deprecated
    public Wa1 j(android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (Jk1.p() || Build.VERSION.SDK_INT < 17) {
            return k(fragment.getActivity().getApplicationContext());
        } else {
            return h(fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @DexIgnore
    public Wa1 k(Context context) {
        if (context != null) {
            if (Jk1.q() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return n((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return i((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return k(contextWrapper.getBaseContext());
                    }
                }
            }
            return o(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    @DexIgnore
    public Wa1 l(View view) {
        if (Jk1.p()) {
            return k(view.getContext().getApplicationContext());
        }
        Ik1.d(view);
        Ik1.e(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity b2 = b(view.getContext());
        if (b2 == null) {
            return k(view.getContext().getApplicationContext());
        }
        if (b2 instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) b2;
            Fragment g2 = g(view, fragmentActivity);
            return g2 != null ? m(g2) : n(fragmentActivity);
        }
        android.app.Fragment f2 = f(view, b2);
        return f2 == null ? i(b2) : j(f2);
    }

    @DexIgnore
    public Wa1 m(Fragment fragment) {
        Ik1.e(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (Jk1.p()) {
            return k(fragment.getContext().getApplicationContext());
        }
        return u(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @DexIgnore
    public Wa1 n(FragmentActivity fragmentActivity) {
        if (Jk1.p()) {
            return k(fragmentActivity.getApplicationContext());
        }
        a(fragmentActivity);
        return u(fragmentActivity, fragmentActivity.getSupportFragmentManager(), null, t(fragmentActivity));
    }

    @DexIgnore
    public final Wa1 o(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = this.e.a(Oa1.c(context.getApplicationContext()), new Xh1(), new Ci1(), context.getApplicationContext());
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    @Deprecated
    public Gi1 p(Activity activity) {
        return q(activity.getFragmentManager(), null, t(activity));
    }

    @DexIgnore
    public final Gi1 q(FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        Gi1 gi1 = (Gi1) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (gi1 == null && (gi1 = this.b.get(fragmentManager)) == null) {
            gi1 = new Gi1();
            gi1.j(fragment);
            if (z) {
                gi1.c().d();
            }
            this.b.put(fragmentManager, gi1);
            fragmentManager.beginTransaction().add(gi1, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.d.obtainMessage(1, fragmentManager).sendToTarget();
        }
        return gi1;
    }

    @DexIgnore
    public Ki1 r(Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        return s(fragmentManager, null, t(context));
    }

    @DexIgnore
    public final Ki1 s(androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        Ki1 ki1 = (Ki1) fragmentManager.Z("com.bumptech.glide.manager");
        if (ki1 == null && (ki1 = this.c.get(fragmentManager)) == null) {
            ki1 = new Ki1();
            ki1.F6(fragment);
            if (z) {
                ki1.x6().d();
            }
            this.c.put(fragmentManager, ki1);
            Xq0 j = fragmentManager.j();
            j.d(ki1, "com.bumptech.glide.manager");
            j.i();
            this.d.obtainMessage(2, fragmentManager).sendToTarget();
        }
        return ki1;
    }

    @DexIgnore
    public final Wa1 u(Context context, androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        Ki1 s = s(fragmentManager, fragment, z);
        Wa1 z6 = s.z6();
        if (z6 != null) {
            return z6;
        }
        Wa1 a2 = this.e.a(Oa1.c(context), s.x6(), s.A6(), context);
        s.G6(a2);
        return a2;
    }
}
