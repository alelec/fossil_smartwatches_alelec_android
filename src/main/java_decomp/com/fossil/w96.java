package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface W96 extends Gq4<V96> {
    @DexIgnore
    void S(List<Category> list);

    @DexIgnore
    void S5(List<WatchApp> list);

    @DexIgnore
    void U4(String str);

    @DexIgnore
    void a0(String str, String str2, String str3);

    @DexIgnore
    void c1(boolean z);

    @DexIgnore
    void d6(String str);

    @DexIgnore
    void f0(String str);

    @DexIgnore
    void g0(boolean z, String str, String str2, String str3);

    @DexIgnore
    void i0(String str);

    @DexIgnore
    void o5(WatchApp watchApp);

    @DexIgnore
    void u0(String str);
}
