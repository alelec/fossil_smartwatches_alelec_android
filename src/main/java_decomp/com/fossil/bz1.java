package com.fossil;

import com.fossil.Dz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Ai a(Integer num);

        @DexIgnore
        public abstract Ai b(String str);

        @DexIgnore
        public abstract Bz1 c();

        @DexIgnore
        public abstract Ai d(String str);

        @DexIgnore
        public abstract Ai e(String str);

        @DexIgnore
        public abstract Ai f(String str);

        @DexIgnore
        public abstract Ai g(String str);

        @DexIgnore
        public abstract Ai h(String str);

        @DexIgnore
        public abstract Ai i(String str);
    }

    @DexIgnore
    public static Ai a() {
        return new Dz1.Bi();
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract String g();

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public abstract Integer i();
}
