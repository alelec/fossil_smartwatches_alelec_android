package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ci2 implements Parcelable.Creator<Zh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Zh2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Long l = null;
        Ii2 ii2 = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 2:
                    j = Ad2.y(parcel, t);
                    break;
                case 3:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 4:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 5:
                    str = Ad2.f(parcel, t);
                    break;
                case 6:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 7:
                    i = Ad2.v(parcel, t);
                    break;
                case 8:
                    ii2 = (Ii2) Ad2.e(parcel, t, Ii2.CREATOR);
                    break;
                case 9:
                    l = Ad2.z(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Zh2(j2, j, str3, str2, str, i, ii2, l);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Zh2[] newArray(int i) {
        return new Zh2[i];
    }
}
