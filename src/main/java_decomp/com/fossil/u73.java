package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U73 implements Xw2<T73> {
    @DexIgnore
    public static U73 c; // = new U73();
    @DexIgnore
    public /* final */ Xw2<T73> b;

    @DexIgnore
    public U73() {
        this(Ww2.b(new W73()));
    }

    @DexIgnore
    public U73(Xw2<T73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((T73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((T73) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((T73) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ T73 zza() {
        return this.b.zza();
    }
}
