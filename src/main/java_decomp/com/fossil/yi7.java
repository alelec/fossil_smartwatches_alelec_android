package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Context c;
    @DexIgnore
    public /* final */ /* synthetic */ Jg7 d;

    @DexIgnore
    public Yi7(String str, Context context, Jg7 jg7) {
        this.b = str;
        this.c = context;
        this.d = jg7;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (Ig7.k) {
                if (Ig7.k.size() >= Fg7.A()) {
                    Th7 th7 = Ig7.m;
                    th7.f("The number of page events exceeds the maximum value " + Integer.toString(Fg7.A()));
                    return;
                }
                String unused = Ig7.i = this.b;
                if (Ig7.k.containsKey(Ig7.i)) {
                    Th7 th72 = Ig7.m;
                    th72.d("Duplicate PageID : " + Ig7.i + ", onResume() repeated?");
                    return;
                }
                Ig7.k.put(Ig7.i, Long.valueOf(System.currentTimeMillis()));
                Ig7.a(this.c, true, this.d);
            }
        } catch (Throwable th) {
            Ig7.m.e(th);
            Ig7.f(this.c, th);
        }
    }
}
