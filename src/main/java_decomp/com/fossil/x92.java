package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.M62;
import com.fossil.R62;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X92 extends Ls3 implements R62.Bi, R62.Ci {
    @DexIgnore
    public static M62.Ai<? extends Ys3, Gs3> i; // = Vs3.c;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ M62.Ai<? extends Ys3, Gs3> d;
    @DexIgnore
    public Set<Scope> e;
    @DexIgnore
    public Ac2 f;
    @DexIgnore
    public Ys3 g;
    @DexIgnore
    public Aa2 h;

    @DexIgnore
    public X92(Context context, Handler handler, Ac2 ac2) {
        this(context, handler, ac2, i);
    }

    @DexIgnore
    public X92(Context context, Handler handler, Ac2 ac2, M62.Ai<? extends Ys3, Gs3> ai) {
        this.b = context;
        this.c = handler;
        Rc2.l(ac2, "ClientSettings must not be null");
        this.f = ac2;
        this.e = ac2.j();
        this.d = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ks3
    public final void Q2(Us3 us3) {
        this.c.post(new Y92(this, us3));
    }

    @DexIgnore
    public final void a3(Aa2 aa2) {
        Ys3 ys3 = this.g;
        if (ys3 != null) {
            ys3.a();
        }
        this.f.m(Integer.valueOf(System.identityHashCode(this)));
        M62.Ai<? extends Ys3, Gs3> ai = this.d;
        Context context = this.b;
        Looper looper = this.c.getLooper();
        Ac2 ac2 = this.f;
        this.g = (Ys3) ai.c(context, looper, ac2, ac2.k(), this, this);
        this.h = aa2;
        Set<Scope> set = this.e;
        if (set == null || set.isEmpty()) {
            this.c.post(new Z92(this));
        } else {
            this.g.b();
        }
    }

    @DexIgnore
    public final Ys3 b3() {
        return this.g;
    }

    @DexIgnore
    public final void c3() {
        Ys3 ys3 = this.g;
        if (ys3 != null) {
            ys3.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void d(int i2) {
        this.g.a();
    }

    @DexIgnore
    public final void d3(Us3 us3) {
        Z52 c2 = us3.c();
        if (c2.A()) {
            Tc2 f2 = us3.f();
            Z52 f3 = f2.f();
            if (!f3.A()) {
                String valueOf = String.valueOf(f3);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.h.c(f3);
                this.g.a();
                return;
            }
            this.h.b(f2.c(), this.e);
        } else {
            this.h.c(c2);
        }
        this.g.a();
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void e(Bundle bundle) {
        this.g.e(this);
    }

    @DexIgnore
    @Override // com.fossil.R72
    public final void n(Z52 z52) {
        this.h.c(z52);
    }
}
