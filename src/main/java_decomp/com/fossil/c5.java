package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ R4 d;
    @DexIgnore
    public /* final */ /* synthetic */ R4 e;

    @DexIgnore
    public C5(K5 k5, G7 g7, R4 r4, R4 r42) {
        this.b = k5;
        this.c = g7;
        this.d = r4;
        this.e = r42;
    }

    @DexIgnore
    public final void run() {
        this.b.z.j.f();
        this.b.z.j.c(new A7(this.c, this.d, this.e));
    }
}
