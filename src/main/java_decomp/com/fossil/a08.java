package com.fossil;

import com.fossil.B08;
import com.mapped.Wg6;
import java.lang.Comparable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A08<T extends B08 & Comparable<? super T>> {
    @DexIgnore
    public volatile int _size; // = 0;
    @DexIgnore
    public T[] a;

    @DexIgnore
    public final void a(T t) {
        if (Nv7.a()) {
            if (!(t.e() == null)) {
                throw new AssertionError();
            }
        }
        t.b(this);
        T[] f = f();
        int c = c();
        j(c + 1);
        f[c] = t;
        t.f(c);
        l(c);
    }

    @DexIgnore
    public final T b() {
        T[] tArr = this.a;
        if (tArr != null) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public final int c() {
        return this._size;
    }

    @DexIgnore
    public final boolean d() {
        return c() == 0;
    }

    @DexIgnore
    public final T e() {
        T b;
        synchronized (this) {
            b = b();
        }
        return b;
    }

    @DexIgnore
    public final T[] f() {
        T[] tArr = this.a;
        if (tArr == null) {
            T[] tArr2 = (T[]) new B08[4];
            this.a = tArr2;
            return tArr2;
        } else if (c() < tArr.length) {
            return tArr;
        } else {
            Object[] copyOf = Arrays.copyOf(tArr, c() * 2);
            Wg6.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            T[] tArr3 = (T[]) ((B08[]) copyOf);
            this.a = tArr3;
            return tArr3;
        }
    }

    @DexIgnore
    public final boolean g(T t) {
        boolean z = false;
        synchronized (this) {
            if (t.e() != null) {
                int a2 = t.a();
                if (Nv7.a()) {
                    if (a2 >= 0) {
                        z = true;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
                h(a2);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public final T h(int i) {
        if (Nv7.a()) {
            if (!(c() > 0)) {
                throw new AssertionError();
            }
        }
        T[] tArr = this.a;
        if (tArr != null) {
            j(c() - 1);
            if (i < c()) {
                m(i, c());
                int i2 = (i - 1) / 2;
                if (i > 0) {
                    T t = tArr[i];
                    if (t != null) {
                        Comparable comparable = (Comparable) t;
                        T t2 = tArr[i2];
                        if (t2 == null) {
                            Wg6.i();
                            throw null;
                        } else if (comparable.compareTo(t2) < 0) {
                            m(i, i2);
                            l(i2);
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                k(i);
            }
            T t3 = tArr[c()];
            if (t3 != null) {
                if (Nv7.a()) {
                    if (!(t3.e() == this)) {
                        throw new AssertionError();
                    }
                }
                t3.b(null);
                t3.f(-1);
                tArr[c()] = null;
                return t3;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final T i() {
        T h;
        synchronized (this) {
            h = c() > 0 ? h(0) : null;
        }
        return h;
    }

    @DexIgnore
    public final void j(int i) {
        this._size = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        if (r0.compareTo(r4) < 0) goto L_0x0028;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void k(int r7) {
        /*
            r6 = this;
            r5 = 0
        L_0x0001:
            int r0 = r7 * 2
            int r2 = r0 + 1
            int r0 = r6.c()
            if (r2 < r0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            T extends com.fossil.B08 & java.lang.Comparable<? super T>[] r3 = r6.a
            if (r3 == 0) goto L_0x004d
            int r1 = r2 + 1
            int r0 = r6.c()
            if (r1 >= r0) goto L_0x0051
            r0 = r3[r1]
            if (r0 == 0) goto L_0x0041
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r4 = r3[r2]
            if (r4 == 0) goto L_0x003d
            int r0 = r0.compareTo(r4)
            if (r0 >= 0) goto L_0x0051
        L_0x0028:
            r0 = r3[r7]
            if (r0 == 0) goto L_0x0049
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r2 = r3[r1]
            if (r2 == 0) goto L_0x0045
            int r0 = r0.compareTo(r2)
            if (r0 <= 0) goto L_0x000b
            r6.m(r7, r1)
            r7 = r1
            goto L_0x0001
        L_0x003d:
            com.mapped.Wg6.i()
            throw r5
        L_0x0041:
            com.mapped.Wg6.i()
            throw r5
        L_0x0045:
            com.mapped.Wg6.i()
            throw r5
        L_0x0049:
            com.mapped.Wg6.i()
            throw r5
        L_0x004d:
            com.mapped.Wg6.i()
            throw r5
        L_0x0051:
            r1 = r2
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.A08.k(int):void");
    }

    @DexIgnore
    public final void l(int i) {
        while (i > 0) {
            T[] tArr = this.a;
            if (tArr != null) {
                int i2 = (i - 1) / 2;
                T t = tArr[i2];
                if (t != null) {
                    Comparable comparable = (Comparable) t;
                    T t2 = tArr[i];
                    if (t2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (comparable.compareTo(t2) > 0) {
                        m(i, i2);
                        i = i2;
                    } else {
                        return;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void m(int i, int i2) {
        T[] tArr = this.a;
        if (tArr != null) {
            T t = tArr[i2];
            if (t != null) {
                T t2 = tArr[i];
                if (t2 != null) {
                    tArr[i] = t;
                    tArr[i2] = t2;
                    t.f(i);
                    t2.f(i2);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }
}
