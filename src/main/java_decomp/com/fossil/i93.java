package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I93 implements J93 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true);

    @DexIgnore
    @Override // com.fossil.J93
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
