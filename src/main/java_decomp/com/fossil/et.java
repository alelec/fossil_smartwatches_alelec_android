package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Et extends Bt {
    @DexIgnore
    public /* final */ Rt K;
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public Et(K5 k5, Rt rt, byte[] bArr) {
        super(k5, Ut.g, Hs.I, 0, 8);
        this.K = rt;
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.L.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.K.b).put(this.L).array();
        Wg6.b(array, "ByteBuffer.allocate(KEY_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.u2, Ey1.a(this.K)), Jd0.q2, Dy1.e(this.L, null, 1, null));
    }
}
