package com.fossil;

import com.mapped.Cd6;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pq extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.f));
    @DexIgnore
    public /* final */ long D;

    @DexIgnore
    public Pq(K5 k5, I60 i60, long j) {
        super(k5, i60, Yp.B0, null, false, 24);
        this.D = j;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        long j = this.D;
        if (j < 0 || j > Hy1.b(Oq7.a)) {
            k(Zq.y);
        } else {
            Lp.i(this, new Ct(this.w, this.D), new En(this), new Qn(this), null, Co.b, Oo.b, 8, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.K4, Long.valueOf(this.D));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean r(Fs fs) {
        return fs instanceof Gt;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
