package com.fossil;

import com.fossil.A18;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I61 extends G61<Q18> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public I61(A18.Ai ai) {
        super(ai);
        Wg6.c(ai, "callFactory");
    }

    @DexIgnore
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Object obj) {
        return g((Q18) obj);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.G61
    public /* bridge */ /* synthetic */ Q18 f(Q18 q18) {
        Q18 q182 = q18;
        h(q182);
        return q182;
    }

    @DexIgnore
    public String g(Q18 q18) {
        Wg6.c(q18, "data");
        String q182 = q18.toString();
        Wg6.b(q182, "data.toString()");
        return q182;
    }

    @DexIgnore
    public Q18 h(Q18 q18) {
        Wg6.c(q18, "$this$toHttpUrl");
        return q18;
    }
}
