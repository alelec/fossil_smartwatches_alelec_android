package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ge6 implements Factory<Oe6> {
    @DexIgnore
    public static Oe6 a(De6 de6) {
        Oe6 c = de6.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
