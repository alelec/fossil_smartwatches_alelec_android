package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.mapped.Zf;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vu0<T> {
    @DexIgnore
    public static /* final */ Executor h; // = new Ci();
    @DexIgnore
    public /* final */ Jv0 a;
    @DexIgnore
    public /* final */ Uu0<T> b;
    @DexIgnore
    public Executor c;
    @DexIgnore
    public /* final */ List<Bi<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<T> e;
    @DexIgnore
    public List<T> f; // = Collections.emptyList();
    @DexIgnore
    public int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Zf.Bi {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.mapped.Zf.Bi
            public boolean a(int i, int i2) {
                Object obj = Ai.this.b.get(i);
                Object obj2 = Ai.this.c.get(i2);
                if (obj != null && obj2 != null) {
                    return Vu0.this.b.b().areContentsTheSame(obj, obj2);
                }
                if (obj == null && obj2 == null) {
                    return true;
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.mapped.Zf.Bi
            public boolean b(int i, int i2) {
                Object obj = Ai.this.b.get(i);
                Object obj2 = Ai.this.c.get(i2);
                return (obj == null || obj2 == null) ? obj == null && obj2 == null : Vu0.this.b.b().areItemsTheSame(obj, obj2);
            }

            @DexIgnore
            @Override // com.mapped.Zf.Bi
            public Object c(int i, int i2) {
                Object obj = Ai.this.b.get(i);
                Object obj2 = Ai.this.c.get(i2);
                if (obj != null && obj2 != null) {
                    return Vu0.this.b.b().getChangePayload(obj, obj2);
                }
                throw new AssertionError();
            }

            @DexIgnore
            @Override // com.mapped.Zf.Bi
            public int d() {
                return Ai.this.c.size();
            }

            @DexIgnore
            @Override // com.mapped.Zf.Bi
            public int e() {
                return Ai.this.b.size();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Zf.Ci b;

            @DexIgnore
            public Bii(Zf.Ci ci) {
                this.b = ci;
            }

            @DexIgnore
            public void run() {
                Ai ai = Ai.this;
                Vu0 vu0 = Vu0.this;
                if (vu0.g == ai.d) {
                    vu0.c(ai.c, this.b, ai.e);
                }
            }
        }

        @DexIgnore
        public Ai(List list, List list2, int i, Runnable runnable) {
            this.b = list;
            this.c = list2;
            this.d = i;
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            Vu0.this.c.execute(new Bii(Zf.a(new Aii())));
        }
    }

    @DexIgnore
    public interface Bi<T> {
        @DexIgnore
        void a(List<T> list, List<T> list2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore
    public Vu0(Jv0 jv0, Uu0<T> uu0) {
        this.a = jv0;
        this.b = uu0;
        if (uu0.c() != null) {
            this.c = uu0.c();
        } else {
            this.c = h;
        }
    }

    @DexIgnore
    public void a(Bi<T> bi) {
        this.d.add(bi);
    }

    @DexIgnore
    public List<T> b() {
        return this.f;
    }

    @DexIgnore
    public void c(List<T> list, Zf.Ci ci, Runnable runnable) {
        List<T> list2 = this.f;
        this.e = list;
        this.f = Collections.unmodifiableList(list);
        ci.e(this.a);
        d(list2, runnable);
    }

    @DexIgnore
    public final void d(List<T> list, Runnable runnable) {
        for (Bi<T> bi : this.d) {
            bi.a(list, this.f);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void e(List<T> list) {
        f(list, null);
    }

    @DexIgnore
    public void f(List<T> list, Runnable runnable) {
        int i = this.g + 1;
        this.g = i;
        List<T> list2 = this.e;
        if (list != list2) {
            List<T> list3 = this.f;
            if (list == null) {
                int size = list2.size();
                this.e = null;
                this.f = Collections.emptyList();
                this.a.c(0, size);
                d(list3, runnable);
            } else if (list2 == null) {
                this.e = list;
                this.f = Collections.unmodifiableList(list);
                this.a.b(0, list.size());
                d(list3, runnable);
            } else {
                this.b.a().execute(new Ai(list2, list, i, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }
}
