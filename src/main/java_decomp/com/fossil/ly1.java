package com.fossil;

import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ly1 {
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<My1> a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public Ky1[] b; // = new Ky1[0];

    @DexIgnore
    public final void a(String str, String str2, Object... objArr) {
        Wg6.c(str, "tag");
        Wg6.c(str2, "logContent");
        Wg6.c(objArr, "logParams");
        c(3, str, str2, Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public final void b(String str, String str2, Object... objArr) {
        Wg6.c(str, "tag");
        Wg6.c(str2, "logContent");
        Wg6.c(objArr, "logParams");
        c(6, str, str2, Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public final void c(int i, String str, String str2, Object... objArr) {
        Wg6.c(str, "tag");
        Wg6.c(str2, "logContent");
        Wg6.c(objArr, "logParams");
        long currentTimeMillis = System.currentTimeMillis();
        Ky1 a2 = Ky1.Companion.a(i);
        if (a2 != null && Em7.B(this.b, a2)) {
            Iterator<T> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().a(currentTimeMillis, a2, str, str2, Arrays.copyOf(objArr, objArr.length));
            }
        }
    }
}
