package com.fossil;

import com.mapped.Dx6;
import java.io.File;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S80 implements Dx6<String> {
    @DexIgnore
    public /* final */ /* synthetic */ U80 a;
    @DexIgnore
    public /* final */ /* synthetic */ File b;

    @DexIgnore
    public S80(U80 u80, File file) {
        this.a = u80;
        this.b = file;
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onFailure(Call<String> call, Throwable th) {
        M80.c.a("LogUploader", "Upload Log Fail: throw=%s.", th.getMessage());
        this.a.d();
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onResponse(Call<String> call, Q88<String> q88) {
        M80 m80 = M80.c;
        String f = q88.f();
        int b2 = q88.b();
        String a2 = q88.a();
        W18 d = q88.d();
        m80.a("LogUploader", "Upload Log Response: message=%s, code=%s, body=%s, error body=%s.", f, Integer.valueOf(b2), a2, d != null ? d.string() : null);
        int b3 = q88.b();
        if ((200 <= b3 && 299 >= b3) || b3 == 400 || b3 == 413) {
            this.a.c.remove(this.b);
            this.b.delete();
            this.a.e();
            return;
        }
        this.a.d();
    }
}
