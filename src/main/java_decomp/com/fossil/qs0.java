package com.fossil;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.SavedStateHandleController;
import androidx.lifecycle.ViewModelProvider;
import androidx.savedstate.SavedStateRegistry;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qs0 extends ViewModelProvider.b {
    @DexIgnore
    public static /* final */ Class<?>[] f; // = {Application.class, Ps0.class};
    @DexIgnore
    public static /* final */ Class<?>[] g; // = {Ps0.class};
    @DexIgnore
    public /* final */ Application a;
    @DexIgnore
    public /* final */ ViewModelProvider.a b;
    @DexIgnore
    public /* final */ Bundle c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ SavedStateRegistry e;

    @DexIgnore
    @SuppressLint({"LambdaLast"})
    public Qs0(Application application, Jx0 jx0, Bundle bundle) {
        this.e = jx0.getSavedStateRegistry();
        this.d = jx0.getLifecycle();
        this.c = bundle;
        this.a = application;
        this.b = ViewModelProvider.a.b(application);
    }

    @DexIgnore
    public static <T> Constructor<T> c(Class<T> cls, Class<?>[] clsArr) {
        for (Constructor<?> constructor : cls.getConstructors()) {
            Constructor<T> constructor2 = (Constructor<T>) constructor;
            if (Arrays.equals(clsArr, constructor2.getParameterTypes())) {
                return constructor2;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.d
    public void a(Ts0 ts0) {
        SavedStateHandleController.a(ts0, this.e, this.d);
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.b
    public <T extends Ts0> T b(String str, Class<T> cls) {
        T t;
        boolean isAssignableFrom = Ir0.class.isAssignableFrom(cls);
        Constructor c2 = isAssignableFrom ? c(cls, f) : c(cls, g);
        if (c2 == null) {
            return (T) this.b.create(cls);
        }
        SavedStateHandleController d2 = SavedStateHandleController.d(this.e, this.d, str, this.c);
        if (isAssignableFrom) {
            try {
                t = (T) ((Ts0) c2.newInstance(this.a, d2.e()));
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Failed to access " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("A " + cls + " cannot be instantiated.", e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("An exception happened in constructor of " + cls, e4.getCause());
            }
        } else {
            t = (T) ((Ts0) c2.newInstance(d2.e()));
        }
        t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", d2);
        return t;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.b, androidx.lifecycle.ViewModelProvider.Factory
    public <T extends Ts0> T create(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) b(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }
}
