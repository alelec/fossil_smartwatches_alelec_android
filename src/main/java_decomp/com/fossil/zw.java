package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.mapped.Q40;
import com.mapped.Wg6;
import java.util.concurrent.CopyOnWriteArraySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zw {
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<E60> a; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<E60> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<E60> c; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<E60> d; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ Handler e;
    @DexIgnore
    public static /* final */ BroadcastReceiver f; // = new M1();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new Be();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new Hd();
    @DexIgnore
    public static /* final */ Zw i; // = new Zw();

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            e = new Handler(myLooper);
            BroadcastReceiver broadcastReceiver = f;
            Context a2 = Id0.i.a();
            if (a2 != null) {
                IntentFilter intentFilter = new IntentFilter();
                for (int i2 = 0; i2 < 1; i2++) {
                    intentFilter.addAction(new String[]{"com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED"}[i2]);
                }
                Ct0.b(a2).c(broadcastReceiver, intentFilter);
            }
            BroadcastReceiver broadcastReceiver2 = g;
            Context a3 = Id0.i.a();
            if (a3 != null) {
                IntentFilter intentFilter2 = new IntentFilter();
                for (int i3 = 0; i3 < 1; i3++) {
                    intentFilter2.addAction(new String[]{"com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED"}[i3]);
                }
                Ct0.b(a3).c(broadcastReceiver2, intentFilter2);
            }
            BroadcastReceiver broadcastReceiver3 = h;
            Context a4 = Id0.i.a();
            if (a4 != null) {
                IntentFilter intentFilter3 = new IntentFilter();
                for (int i4 = 0; i4 < 1; i4++) {
                    intentFilter3.addAction(new String[]{"com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED"}[i4]);
                }
                Ct0.b(a4).c(broadcastReceiver3, intentFilter3);
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static /* synthetic */ void a(Zw zw, E60 e60, long j, int i2) {
        if ((i2 & 2) != 0) {
            j = 0;
        }
        zw.c(e60, j);
    }

    @DexIgnore
    public final void b(E60 e60) {
        if (a.contains(e60) && e60.v == Q40.Ci.DISCONNECTED) {
            e.postDelayed(new H3(e60), 200);
        }
    }

    @DexIgnore
    public final void c(E60 e60, long j) {
        e.postDelayed(new Hc(e60), j);
    }

    @DexIgnore
    public final void d(E60 e60, Q40.Di di) {
        int i2 = T0.b[di.ordinal()];
        if (i2 == 2) {
            c(e60, 0);
        } else if (i2 == 3) {
            Qd0.b.b("HID_EXPONENT_BACK_OFF_TAG");
            c(e60, 0);
        }
    }

    @DexIgnore
    public final void e(BluetoothLeAdapter.Ci ci) {
        if (T0.a[ci.ordinal()] == 1) {
            for (T t : a) {
                Zw zw = i;
                Wg6.b(t, "it");
                zw.b(t);
            }
            for (T t2 : b) {
                Zw zw2 = i;
                Wg6.b(t2, "it");
                a(zw2, t2, 0, 2);
            }
        }
    }

    @DexIgnore
    public final boolean f(E60 e60) {
        return a.contains(e60);
    }

    @DexIgnore
    public final boolean g(E60 e60) {
        return b.contains(e60);
    }

    @DexIgnore
    public final void h(E60 e60) {
        b.remove(e60);
    }

    @DexIgnore
    public final boolean i(E60 e60) {
        a.add(e60);
        b(e60);
        return true;
    }

    @DexIgnore
    public final boolean j(E60 e60) {
        a.remove(e60);
        return true;
    }
}
