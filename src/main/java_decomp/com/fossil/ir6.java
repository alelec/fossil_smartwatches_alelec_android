package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir6 extends ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f1657a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a(null, null, 3, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f1658a;
        @DexIgnore
        public Integer b;

        @DexIgnore
        public a() {
            this(null, null, 3, null);
        }

        @DexIgnore
        public a(Integer num, Integer num2) {
            this.f1658a = num;
            this.b = num2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Integer num, Integer num2, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2);
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.f1658a;
        }

        @DexIgnore
        public final void c(Integer num, Integer num2) {
            this.f1658a = num;
            this.b = num2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel$saveColor$1", f = "CustomizeActiveCaloriesChartViewModel.kt", l = {45, 46, 57}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $colorChart;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ir6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ir6 ir6, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ir6;
            this.$id = str;
            this.$colorChart = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, this.$colorChart, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0044  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ad  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00d5  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0114  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0117  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 283
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ir6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = ir6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeActiveCaloriesC\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public ir6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.f1657a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<a> e() {
        return this.f1657a;
    }

    @DexIgnore
    public final void f(String str, String str2) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = d;
        local.d(str3, "saveColor colorChart=" + str2);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = fr6.m.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        this.b.c(Integer.valueOf(parseColor), Integer.valueOf(parseColor));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        if (i == 503) {
            this.b.c(Integer.valueOf(i2), Integer.valueOf(i2));
            d();
        }
    }
}
