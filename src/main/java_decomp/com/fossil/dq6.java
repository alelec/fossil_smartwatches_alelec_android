package com.fossil;

import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dq6 implements MembersInjector<DeleteAccountPresenter> {
    @DexIgnore
    public static void a(DeleteAccountPresenter deleteAccountPresenter) {
        deleteAccountPresenter.v();
    }
}
