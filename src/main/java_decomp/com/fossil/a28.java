package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class A28 implements Runnable {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public A28(String str, Object... objArr) {
        this.b = B28.r(str, objArr);
    }

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.b);
        try {
            k();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
