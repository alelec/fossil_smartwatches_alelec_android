package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hc2 {
    @DexIgnore
    public Hc2(String str) {
        this(str, null);
    }

    @DexIgnore
    public Hc2(String str, String str2) {
        Rc2.l(str, "log tag cannot be null");
        Rc2.c(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        if (str2 == null || str2.length() <= 0) {
        }
    }
}
