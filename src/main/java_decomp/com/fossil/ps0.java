package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.savedstate.SavedStateRegistry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ps0 {
    @DexIgnore
    public /* final */ Map<String, Object> a;
    @DexIgnore
    public /* final */ SavedStateRegistry.b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements SavedStateRegistry.b {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // androidx.savedstate.SavedStateRegistry.b
        public Bundle c() {
            Set<String> keySet = Ps0.this.a.keySet();
            ArrayList<? extends Parcelable> arrayList = new ArrayList<>(keySet.size());
            ArrayList<? extends Parcelable> arrayList2 = new ArrayList<>(arrayList.size());
            for (String str : keySet) {
                arrayList.add(str);
                arrayList2.add(Ps0.this.a.get(str));
            }
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("keys", arrayList);
            bundle.putParcelableArrayList("values", arrayList2);
            return bundle;
        }
    }

    /*
    static {
        Class cls = Double.TYPE;
        Class cls2 = Integer.TYPE;
        Class cls3 = Long.TYPE;
        Class cls4 = Byte.TYPE;
        Class cls5 = Character.TYPE;
        Class cls6 = Float.TYPE;
        Class cls7 = Short.TYPE;
        if (Build.VERSION.SDK_INT < 21) {
            Class cls8 = Integer.TYPE;
        }
        if (Build.VERSION.SDK_INT < 21) {
            Class cls9 = Integer.TYPE;
        }
    }
    */

    @DexIgnore
    public Ps0() {
        new HashMap();
        this.b = new Ai();
        this.a = new HashMap();
    }

    @DexIgnore
    public Ps0(Map<String, Object> map) {
        new HashMap();
        this.b = new Ai();
        this.a = new HashMap(map);
    }

    @DexIgnore
    public static Ps0 a(Bundle bundle, Bundle bundle2) {
        if (bundle == null && bundle2 == null) {
            return new Ps0();
        }
        HashMap hashMap = new HashMap();
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                hashMap.put(str, bundle2.get(str));
            }
        }
        if (bundle == null) {
            return new Ps0(hashMap);
        }
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("keys");
        ArrayList parcelableArrayList2 = bundle.getParcelableArrayList("values");
        if (parcelableArrayList == null || parcelableArrayList2 == null || parcelableArrayList.size() != parcelableArrayList2.size()) {
            throw new IllegalStateException("Invalid bundle passed as restored state");
        }
        for (int i = 0; i < parcelableArrayList.size(); i++) {
            hashMap.put((String) parcelableArrayList.get(i), parcelableArrayList2.get(i));
        }
        return new Ps0(hashMap);
    }

    @DexIgnore
    public SavedStateRegistry.b b() {
        return this.b;
    }
}
