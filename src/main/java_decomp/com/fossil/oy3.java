package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dy3;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oy3 extends RecyclerView.g<Bi> {
    @DexIgnore
    public /* final */ Dy3<?> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Ai(int i) {
            this.b = i;
        }

        @DexIgnore
        public void onClick(View view) {
            Oy3.this.a.O6(Hy3.b(this.b, Oy3.this.a.I6().d));
            Oy3.this.a.P6(Dy3.Ki.DAY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;

        @DexIgnore
        public Bi(TextView textView) {
            super(textView);
            this.a = textView;
        }
    }

    @DexIgnore
    public Oy3(Dy3<?> dy3) {
        this.a = dy3;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.G6().k();
    }

    @DexIgnore
    public final View.OnClickListener h(int i) {
        return new Ai(i);
    }

    @DexIgnore
    public int i(int i) {
        return i - this.a.G6().i().e;
    }

    @DexIgnore
    public int j(int i) {
        return this.a.G6().i().e + i;
    }

    @DexIgnore
    public void k(Bi bi, int i) {
        int j = j(i);
        String string = bi.a.getContext().getString(Rw3.mtrl_picker_navigate_to_year_description);
        bi.a.setText(String.format(Locale.getDefault(), "%d", Integer.valueOf(j)));
        bi.a.setContentDescription(String.format(string, Integer.valueOf(j)));
        Yx3 H6 = this.a.H6();
        Calendar i2 = Ny3.i();
        Xx3 xx3 = i2.get(1) == j ? H6.f : H6.d;
        Xx3 xx32 = xx3;
        for (Long l : this.a.J6().X()) {
            i2.setTimeInMillis(l.longValue());
            if (i2.get(1) == j) {
                xx32 = H6.e;
            }
        }
        xx32.d(bi.a);
        bi.a.setOnClickListener(h(j));
    }

    @DexIgnore
    public Bi l(ViewGroup viewGroup, int i) {
        return new Bi((TextView) LayoutInflater.from(viewGroup.getContext()).inflate(Pw3.mtrl_calendar_year, viewGroup, false));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        k(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }
}
