package com.fossil;

import com.fossil.hq4;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lv6 extends hq4 {
    @DexIgnore
    public /* final */ do5 h;
    @DexIgnore
    public /* final */ mj5 i;
    @DexIgnore
    public /* final */ on5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.migration.MigrationViewModel$migrate$1", f = "MigrationViewModel.kt", l = {25}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lv6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lv6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.migration.MigrationViewModel$migrate$1$resultCode$1", f = "MigrationViewModel.kt", l = {26}, m = "invokeSuspend")
        /* renamed from: com.fossil.lv6$a$a  reason: collision with other inner class name */
        public static final class C0143a extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0143a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0143a aVar = new C0143a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                throw null;
                //return ((C0143a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    do5 do5 = this.this$0.this$0.h;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object g = do5.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(lv6 lv6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = lv6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (o37.b(PortfolioApp.h0.c())) {
                    this.this$0.j().l(new hq4.b(true, false, null, 6, null));
                    dv7 b = bw7.b();
                    C0143a aVar = new C0143a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    this.this$0.j().l(new hq4.b(false, false, null, 6, null));
                    this.this$0.a(1, "");
                    return tl7.f3441a;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            int intValue = ((Number) g).intValue();
            this.this$0.j().l(new hq4.b(false, false, null, 6, null));
            if (intValue == 0) {
                PortfolioApp.h0.c().S1(this.this$0.i, false, 13);
                this.this$0.j.G1(ao7.a(true));
            }
            this.this$0.a(intValue, "");
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public lv6(do5 do5, mj5 mj5, on5 on5) {
        pq7.c(do5, "migrationHelper");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(on5, "mSharedPreferencesManager");
        this.h = do5;
        this.i = mj5;
        this.j = on5;
    }

    @DexIgnore
    public final void r() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }
}
