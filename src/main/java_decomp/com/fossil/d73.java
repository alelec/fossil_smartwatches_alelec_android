package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D73 implements Xw2<G73> {
    @DexIgnore
    public static D73 c; // = new D73();
    @DexIgnore
    public /* final */ Xw2<G73> b;

    @DexIgnore
    public D73() {
        this(Ww2.b(new F73()));
    }

    @DexIgnore
    public D73(Xw2<G73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((G73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ G73 zza() {
        return this.b.zza();
    }
}
