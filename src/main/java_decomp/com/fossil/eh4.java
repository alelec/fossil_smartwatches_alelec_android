package com.fossil;

import com.fossil.Fh4;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Eh4 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ J64 b;

    @DexIgnore
    public enum Ai {
        ATTEMPT_MIGRATION,
        NOT_GENERATED,
        UNREGISTERED,
        REGISTERED,
        REGISTER_ERROR
    }

    @DexIgnore
    public Eh4(J64 j64) {
        File filesDir = j64.g().getFilesDir();
        this.a = new File(filesDir, "PersistedInstallation." + j64.k() + ".json");
        this.b = j64;
    }

    @DexIgnore
    public Fh4 a(Fh4 fh4) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("Fid", fh4.d());
            jSONObject.put("Status", fh4.g().ordinal());
            jSONObject.put("AuthToken", fh4.b());
            jSONObject.put("RefreshToken", fh4.f());
            jSONObject.put("TokenCreationEpochInSecs", fh4.h());
            jSONObject.put("ExpiresInSecs", fh4.c());
            jSONObject.put("FisError", fh4.e());
            File createTempFile = File.createTempFile("PersistedInstallation", "tmp", this.b.g().getFilesDir());
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(jSONObject.toString().getBytes("UTF-8"));
            fileOutputStream.close();
            if (!createTempFile.renameTo(this.a)) {
                throw new IOException("unable to rename the tmpfile to PersistedInstallation");
            }
        } catch (IOException | JSONException e) {
        }
        return fh4;
    }

    @DexIgnore
    public final JSONObject b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[16384];
        try {
            FileInputStream fileInputStream = new FileInputStream(this.a);
            while (true) {
                try {
                    int read = fileInputStream.read(bArr, 0, 16384);
                    if (read < 0) {
                        JSONObject jSONObject = new JSONObject(byteArrayOutputStream.toString());
                        fileInputStream.close();
                        return jSONObject;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                } catch (Throwable th) {
                }
            }
            throw th;
        } catch (IOException | JSONException e) {
            return new JSONObject();
        }
    }

    @DexIgnore
    public Fh4 c() {
        JSONObject b2 = b();
        String optString = b2.optString("Fid", null);
        int optInt = b2.optInt("Status", Ai.ATTEMPT_MIGRATION.ordinal());
        String optString2 = b2.optString("AuthToken", null);
        String optString3 = b2.optString("RefreshToken", null);
        long optLong = b2.optLong("TokenCreationEpochInSecs", 0);
        long optLong2 = b2.optLong("ExpiresInSecs", 0);
        String optString4 = b2.optString("FisError", null);
        Fh4.Ai a2 = Fh4.a();
        a2.d(optString);
        a2.g(Ai.values()[optInt]);
        a2.b(optString2);
        a2.f(optString3);
        a2.h(optLong);
        a2.c(optLong2);
        a2.e(optString4);
        return a2.a();
    }
}
