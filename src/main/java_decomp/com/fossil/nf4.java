package com.fossil;

import android.os.IBinder;
import com.fossil.Kf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Nf4 implements Runnable {
    @DexIgnore
    public /* final */ Kf4.Bi b;
    @DexIgnore
    public /* final */ IBinder c;

    @DexIgnore
    public Nf4(Kf4.Bi bi, IBinder iBinder) {
        this.b = bi;
        this.c = iBinder;
    }

    @DexIgnore
    public final void run() {
        this.b.d(this.c);
    }
}
