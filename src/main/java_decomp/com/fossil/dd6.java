package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dd6 {
    @DexIgnore
    public /* final */ Ue6 a;
    @DexIgnore
    public /* final */ Qd6 b;
    @DexIgnore
    public /* final */ Zf6 c;
    @DexIgnore
    public /* final */ Hi6 d;
    @DexIgnore
    public /* final */ Lj6 e;
    @DexIgnore
    public /* final */ Dh6 f;

    @DexIgnore
    public Dd6(Ue6 ue6, Qd6 qd6, Zf6 zf6, Hi6 hi6, Lj6 lj6, Dh6 dh6) {
        Wg6.c(ue6, "mActivityView");
        Wg6.c(qd6, "mActiveTimeView");
        Wg6.c(zf6, "mCaloriesView");
        Wg6.c(hi6, "mHeartrateView");
        Wg6.c(lj6, "mSleepView");
        Wg6.c(dh6, "mGoalTrackingView");
        this.a = ue6;
        this.b = qd6;
        this.c = zf6;
        this.d = hi6;
        this.e = lj6;
        this.f = dh6;
    }

    @DexIgnore
    public final Qd6 a() {
        return this.b;
    }

    @DexIgnore
    public final Ue6 b() {
        return this.a;
    }

    @DexIgnore
    public final Zf6 c() {
        return this.c;
    }

    @DexIgnore
    public final Dh6 d() {
        return this.f;
    }

    @DexIgnore
    public final Hi6 e() {
        return this.d;
    }

    @DexIgnore
    public final Lj6 f() {
        return this.e;
    }
}
