package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oi extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.d));
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public /* final */ long G; // = Hy1.o(this.H.f.length);
    @DexIgnore
    public /* final */ J0 H;

    @DexIgnore
    public Oi(K5 k5, I60 i60, J0 j0, String str) {
        super(k5, i60, Yp.N, str, false, 16);
        this.H = j0;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        this.E = 0;
        long o = Hy1.o(this.H.f.length);
        this.F = o;
        if (this.E == o) {
            l(Nr.a(this.v, null, Zq.b, null, null, 13));
        } else {
            Lp.j(this, Hs.p, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.i2, this.H.a(false));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.h2, Long.valueOf(this.D));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Fs b(Hs hs) {
        if (Nh.a[hs.ordinal()] != 1) {
            return null;
        }
        long j = this.E;
        Lv lv = new Lv(j, this.F - j, this.H.b(), this.w, 0, 16);
        lv.o(new Ai(this));
        return lv;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return Long.valueOf(this.D);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
