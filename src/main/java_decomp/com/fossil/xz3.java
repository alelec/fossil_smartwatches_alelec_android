package com.fossil;

import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xz3 implements Yz3 {
    @DexIgnore
    public /* final */ Yz3 a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public Xz3(float f, Yz3 yz3) {
        Yz3 yz32 = yz3;
        while (yz32 instanceof Xz3) {
            Yz3 yz33 = ((Xz3) yz32).a;
            f += ((Xz3) yz33).b;
            yz32 = yz33;
        }
        this.a = yz32;
        this.b = f;
    }

    @DexIgnore
    @Override // com.fossil.Yz3
    public float a(RectF rectF) {
        return Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.a.a(rectF) + this.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Xz3)) {
            return false;
        }
        Xz3 xz3 = (Xz3) obj;
        return this.a.equals(xz3.a) && this.b == xz3.b;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, Float.valueOf(this.b)});
    }
}
