package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.jl6;
import com.fossil.nk5;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ql6 extends pv5 implements pl6, View.OnClickListener, jl6.a {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public jl6 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public /* final */ Calendar i; // = Calendar.getInstance();
    @DexIgnore
    public g37<v25> j;
    @DexIgnore
    public ol6 k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public HashMap z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ql6 a(Date date) {
            pq7.c(date, "date");
            ql6 ql6 = new ql6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            ql6.setArguments(bundle);
            return ql6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ boolean f2996a;
        @DexIgnore
        public /* final */ /* synthetic */ cu0 b;

        @DexIgnore
        public b(ql6 ql6, boolean z, cu0 cu0, ai5 ai5) {
            this.f2996a = z;
            this.b = cu0;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            pq7.c(appBarLayout, "appBarLayout");
            return this.f2996a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public ql6() {
        String d = qn5.l.a().d("nonBrandSurface");
        this.t = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = qn5.l.a().d("backgroundDashboard");
        this.u = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("secondaryText");
        this.v = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("primaryText");
        this.w = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandDisableCalendarDay");
        this.x = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("nonBrandNonReachGoal");
        this.y = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
    }

    @DexIgnore
    @Override // com.fossil.jl6.a
    public void B2(WorkoutSession workoutSession) {
        String name;
        ai5 o;
        pq7.c(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ol6 ol6 = this.k;
            if (ol6 == null || (o = ol6.o()) == null || (name = o.name()) == null) {
                name = ai5.METRIC.name();
            }
            WorkoutDetailActivity.a aVar = WorkoutDetailActivity.f;
            pq7.b(activity, "it");
            aVar.a(activity, workoutSession.getId(), name);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "CaloriesDetailFragment";
    }

    @DexIgnore
    @Override // com.fossil.pl6
    public void J(ai5 ai5, ActivitySummary activitySummary) {
        v25 a2;
        int i2;
        int i3;
        pq7.c(ai5, "distanceUnit");
        FLogger.INSTANCE.getLocal().d("CaloriesDetailFragment", "showDayDetail - distanceUnit=" + ai5 + ", activitySummary=" + activitySummary);
        g37<v25> g37 = this.j;
        if (g37 != null && (a2 = g37.a()) != null) {
            pq7.b(a2, "binding");
            View n = a2.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            if (activitySummary != null) {
                i3 = lr7.b((float) activitySummary.getCalories());
                i2 = activitySummary.getCaloriesGoal();
            } else {
                i2 = 0;
                i3 = 0;
            }
            if (i3 > 0) {
                FlexibleTextView flexibleTextView = a2.y;
                pq7.b(flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(ml5.f2399a.b(Float.valueOf((float) i3)));
                FlexibleTextView flexibleTextView2 = a2.x;
                pq7.b(flexibleTextView2, "binding.ftvDailyUnit");
                String c = um5.c(context, 2131886623);
                pq7.b(c, "LanguageHelper.getString\u2026aloriesMonth_Label__Cals)");
                if (c != null) {
                    String lowerCase = c.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView3 = a2.y;
                pq7.b(flexibleTextView3, "binding.ftvDailyValue");
                flexibleTextView3.setText("");
                FlexibleTextView flexibleTextView4 = a2.x;
                pq7.b(flexibleTextView4, "binding.ftvDailyUnit");
                String c2 = um5.c(context, 2131886627);
                pq7.b(c2, "LanguageHelper.getString\u2026riesToday_Text__NoRecord)");
                if (c2 != null) {
                    String upperCase = c2.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView4.setText(upperCase);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = i2 > 0 ? (i3 * 100) / i2 : -1;
            if (i3 >= i2 && i2 > 0) {
                a2.A.setTextColor(this.t);
                a2.z.setTextColor(this.t);
                a2.x.setTextColor(this.t);
                a2.y.setTextColor(this.t);
                a2.B.setTextColor(this.t);
                RTLImageView rTLImageView = a2.H;
                pq7.b(rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.G;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                pq7.b(constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.A;
                pq7.b(flexibleTextView5, "binding.ftvDayOfWeek");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.z;
                pq7.b(flexibleTextView6, "binding.ftvDayOfMonth");
                flexibleTextView6.setSelected(true);
                View view = a2.I;
                pq7.b(view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.y;
                pq7.b(flexibleTextView7, "binding.ftvDailyValue");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = a2.x;
                pq7.b(flexibleTextView8, "binding.ftvDailyUnit");
                flexibleTextView8.setSelected(true);
                FlexibleTextView flexibleTextView9 = a2.B;
                pq7.b(flexibleTextView9, "binding.ftvEst");
                flexibleTextView9.setSelected(true);
                String str = this.s;
                if (str != null) {
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.I.setBackgroundColor(Color.parseColor(str));
                    a2.H.setColorFilter(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.l;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i3 > 0) {
                a2.z.setTextColor(this.w);
                a2.A.setTextColor(this.v);
                a2.x.setTextColor(this.y);
                a2.y.setTextColor(this.w);
                a2.B.setTextColor(this.w);
                View view2 = a2.I;
                pq7.b(view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.H;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.G;
                pq7.b(rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i5 = this.y;
                a2.I.setBackgroundColor(i5);
                a2.H.setColorFilter(i5);
                a2.G.setColorFilter(i5);
                String str3 = this.m;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.z.setTextColor(this.w);
                a2.A.setTextColor(this.v);
                a2.y.setTextColor(this.x);
                a2.x.setTextColor(this.x);
                RTLImageView rTLImageView5 = a2.H;
                pq7.b(rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.G;
                pq7.b(rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                pq7.b(constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.A;
                pq7.b(flexibleTextView10, "binding.ftvDayOfWeek");
                flexibleTextView10.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.z;
                pq7.b(flexibleTextView11, "binding.ftvDayOfMonth");
                flexibleTextView11.setSelected(false);
                View view3 = a2.I;
                pq7.b(view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.y;
                pq7.b(flexibleTextView12, "binding.ftvDailyValue");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.x;
                pq7.b(flexibleTextView13, "binding.ftvDailyUnit");
                flexibleTextView13.setSelected(false);
                FlexibleTextView flexibleTextView14 = a2.B;
                pq7.b(flexibleTextView14, "binding.ftvEst");
                flexibleTextView14.setSelected(false);
                int i6 = this.y;
                a2.I.setBackgroundColor(i6);
                a2.H.setColorFilter(i6);
                a2.G.setColorFilter(i6);
                String str4 = this.m;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.K;
                pq7.b(flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView15 = a2.E;
                pq7.b(flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(um5.c(context, 2131887328));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.K;
                pq7.b(flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView16 = a2.E;
                pq7.b(flexibleTextView16, "binding.ftvProgressValue");
                flexibleTextView16.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView17 = a2.C;
            pq7.b(flexibleTextView17, "binding.ftvGoalValue");
            hr7 hr7 = hr7.f1520a;
            String c3 = um5.c(context, 2131886607);
            pq7.b(c3, "LanguageHelper.getString\u2026Page_Title__OfNumberCals)");
            String format = String.format(c3, Arrays.copyOf(new Object[]{ml5.f2399a.b(Float.valueOf((float) i2))}, 1));
            pq7.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView17.setText(format);
        }
    }

    @DexIgnore
    public final void K6(v25 v25) {
        v25.F.setOnClickListener(this);
        v25.G.setOnClickListener(this);
        v25.H.setOnClickListener(this);
        v25.J.setBackgroundColor(this.u);
        v25.r.setBackgroundColor(this.t);
        nk5.a aVar = nk5.o;
        ol6 ol6 = this.k;
        this.l = aVar.w(ol6 != null ? ol6.n() : null) ? qn5.l.a().d("dianaActiveCaloriesTab") : qn5.l.a().d("hybridActiveCaloriesTab");
        this.m = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.s = qn5.l.a().d("onDianaActiveCaloriesTab");
        jl6 jl6 = new jl6(jl6.c.CALORIES, ai5.IMPERIAL, new WorkoutSessionDifference(), this.l);
        this.g = jl6;
        if (jl6 != null) {
            jl6.u(this);
        }
        RecyclerView recyclerView = v25.M;
        pq7.b(recyclerView, "it");
        recyclerView.setAdapter(this.g);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable f = gl0.f(recyclerView.getContext(), 2131230857);
        if (f != null) {
            ok6 ok6 = new ok6(linearLayoutManager.q2(), false, false, 6, null);
            pq7.b(f, ResourceManager.DRAWABLE);
            ok6.h(f);
            recyclerView.addItemDecoration(ok6);
        }
    }

    @DexIgnore
    public final void L6() {
        v25 a2;
        OverviewDayChart overviewDayChart;
        g37<v25> g37 = this.j;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.w) != null) {
            nk5.a aVar = nk5.o;
            ol6 ol6 = this.k;
            if (aVar.w(ol6 != null ? ol6.n() : null)) {
                overviewDayChart.D("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(ol6 ol6) {
        pq7.c(ol6, "presenter");
        this.k = ol6;
        Lifecycle lifecycle = getLifecycle();
        ol6 ol62 = this.k;
        if (ol62 != null) {
            lifecycle.a(ol62.p());
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pl6
    public void j(Date date, boolean z2, boolean z3, boolean z4) {
        v25 a2;
        pq7.c(date, "date");
        this.h = date;
        Calendar calendar = this.i;
        pq7.b(calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.i.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4 + " - calendar: " + this.i);
        g37<v25> g37 = this.j;
        if (g37 != null && (a2 = g37.a()) != null) {
            a2.q.r(true, true);
            FlexibleTextView flexibleTextView = a2.z;
            pq7.b(flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(this.i.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.G;
                pq7.b(rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.G;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.H;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.A;
                    pq7.b(flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(um5.c(getContext(), 2131886629));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.A;
                pq7.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(jl5.b.i(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.H;
            pq7.b(rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.A;
            pq7.b(flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(jl5.b.i(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.pl6
    public void n(mv5 mv5, ArrayList<String> arrayList) {
        v25 a2;
        OverviewDayChart overviewDayChart;
        pq7.c(mv5, "baseModel");
        pq7.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesDetailFragment", "showDayDetailChart - baseModel=" + mv5);
        g37<v25> g37 = this.j;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.w) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(mv5.f2426a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("CaloriesDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    ol6 ol6 = this.k;
                    if (ol6 != null) {
                        ol6.v();
                        return;
                    }
                    return;
                case 2131362735:
                    ol6 ol62 = this.k;
                    if (ol62 != null) {
                        ol62.u();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        v25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        v25 v25 = (v25) aq0.f(layoutInflater, 2131558507, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.h = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        pq7.b(v25, "binding");
        K6(v25);
        ol6 ol6 = this.k;
        if (ol6 != null) {
            ol6.q(this.h);
        }
        this.j = new g37<>(this, v25);
        L6();
        g37<v25> g37 = this.j;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        ol6 ol6 = this.k;
        if (ol6 != null) {
            ol6.r();
        }
        Lifecycle lifecycle = getLifecycle();
        ol6 ol62 = this.k;
        if (ol62 != null) {
            lifecycle.c(ol62.p());
            super.onDestroyView();
            v6();
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        ol6 ol6 = this.k;
        if (ol6 != null) {
            ol6.m();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        L6();
        ol6 ol6 = this.k;
        if (ol6 != null) {
            ol6.t(this.h);
        }
        ol6 ol62 = this.k;
        if (ol62 != null) {
            ol62.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        ol6 ol6 = this.k;
        if (ol6 != null) {
            ol6.s(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.fossil.pl6
    public void s(boolean z2, ai5 ai5, cu0<WorkoutSession> cu0) {
        v25 a2;
        pq7.c(ai5, "distanceUnit");
        pq7.c(cu0, "workoutSessions");
        g37<v25> g37 = this.j;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z2) {
                LinearLayout linearLayout = a2.J;
                pq7.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!cu0.isEmpty()) {
                    FlexibleTextView flexibleTextView = a2.D;
                    pq7.b(flexibleTextView, "it.ftvNoWorkoutRecorded");
                    flexibleTextView.setVisibility(8);
                    RecyclerView recyclerView = a2.M;
                    pq7.b(recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    jl6 jl6 = this.g;
                    if (jl6 != null) {
                        jl6.t(ai5, cu0);
                    }
                } else {
                    FlexibleTextView flexibleTextView2 = a2.D;
                    pq7.b(flexibleTextView2, "it.ftvNoWorkoutRecorded");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a2.M;
                    pq7.b(recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    jl6 jl62 = this.g;
                    if (jl62 != null) {
                        jl62.t(ai5, cu0);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.J;
                pq7.b(linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            pq7.b(appBarLayout, "it.appBarLayout");
            ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.f();
                if (behavior == null) {
                    behavior = new AppBarLayout.Behavior();
                }
                behavior.setDragCallback(new b(this, z2, cu0, ai5));
                eVar.o(behavior);
                return;
            }
            throw new il7("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }

    @DexIgnore
    @Override // com.fossil.jl6.a
    public void t4(WorkoutSession workoutSession) {
        pq7.c(workoutSession, "workoutSession");
        nn6 b2 = nn6.v.b(workoutSession.getId());
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, nn6.v.a());
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.z;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
