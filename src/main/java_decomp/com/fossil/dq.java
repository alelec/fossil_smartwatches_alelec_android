package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dq extends Qq7 implements Coroutine<Lp, Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Rq b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Dq(Rq rq) {
        super(2);
        this.b = rq;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(Lp lp, Float f) {
        float floatValue = f.floatValue();
        Rq rq = this.b;
        rq.d(floatValue * rq.I);
        return Cd6.a;
    }
}
