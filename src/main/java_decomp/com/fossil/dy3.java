package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dy3<S> extends Ly3<S> {
    @DexIgnore
    public static /* final */ Object m; // = "MONTHS_VIEW_GROUP_TAG";
    @DexIgnore
    public static /* final */ Object s; // = "NAVIGATION_PREV_TAG";
    @DexIgnore
    public static /* final */ Object t; // = "NAVIGATION_NEXT_TAG";
    @DexIgnore
    public static /* final */ Object u; // = "SELECTOR_TOGGLE_TAG";
    @DexIgnore
    public int c;
    @DexIgnore
    public Zx3<S> d;
    @DexIgnore
    public Wx3 e;
    @DexIgnore
    public Hy3 f;
    @DexIgnore
    public Ki g;
    @DexIgnore
    public Yx3 h;
    @DexIgnore
    public RecyclerView i;
    @DexIgnore
    public RecyclerView j;
    @DexIgnore
    public View k;
    @DexIgnore
    public View l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Ai(int i) {
            this.b = i;
        }

        @DexIgnore
        public void run() {
            Dy3.this.j.smoothScrollToPosition(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Rn0 {
        @DexIgnore
        public Bi(Dy3 dy3) {
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void g(View view, Yo0 yo0) {
            super.g(view, yo0);
            yo0.e0(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends My3 {
        @DexIgnore
        public /* final */ /* synthetic */ int I;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Context context, int i, boolean z, int i2) {
            super(context, i, z);
            this.I = i2;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.LinearLayoutManager
        public void N1(RecyclerView.State state, int[] iArr) {
            if (this.I == 0) {
                iArr[0] = Dy3.this.j.getWidth();
                iArr[1] = Dy3.this.j.getWidth();
                return;
            }
            iArr[0] = Dy3.this.j.getHeight();
            iArr[1] = Dy3.this.j.getHeight();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Li {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        @Override // com.fossil.Dy3.Li
        public void a(long j) {
            if (Dy3.this.e.e().S(j)) {
                Dy3.this.d.i0(j);
                Iterator<Ky3<S>> it = Dy3.this.b.iterator();
                while (it.hasNext()) {
                    it.next().a((S) Dy3.this.d.b0());
                }
                Dy3.this.j.getAdapter().notifyDataSetChanged();
                if (Dy3.this.i != null) {
                    Dy3.this.i.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends RecyclerView.l {
        @DexIgnore
        public /* final */ Calendar a; // = Ny3.k();
        @DexIgnore
        public /* final */ Calendar b; // = Ny3.k();

        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.l
        public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
            if ((recyclerView.getAdapter() instanceof Oy3) && (recyclerView.getLayoutManager() instanceof GridLayoutManager)) {
                Oy3 oy3 = (Oy3) recyclerView.getAdapter();
                GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                for (Ln0<Long, Long> ln0 : Dy3.this.d.o()) {
                    F f = ln0.a;
                    if (!(f == null || ln0.b == null)) {
                        this.a.setTimeInMillis(f.longValue());
                        this.b.setTimeInMillis(ln0.b.longValue());
                        int i = oy3.i(this.a.get(1));
                        int i2 = oy3.i(this.b.get(1));
                        View D = gridLayoutManager.D(i);
                        View D2 = gridLayoutManager.D(i2);
                        int Z2 = i / gridLayoutManager.Z2();
                        int Z22 = i2 / gridLayoutManager.Z2();
                        int i3 = Z2;
                        while (i3 <= Z22) {
                            View D3 = gridLayoutManager.D(gridLayoutManager.Z2() * i3);
                            if (D3 != null) {
                                canvas.drawRect((float) (i3 == Z2 ? D.getLeft() + (D.getWidth() / 2) : 0), (float) (D3.getTop() + Dy3.this.h.d.c()), (float) (i3 == Z22 ? D2.getLeft() + (D2.getWidth() / 2) : recyclerView.getWidth()), (float) (D3.getBottom() - Dy3.this.h.d.b()), Dy3.this.h.h);
                            }
                            i3++;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Rn0 {
        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void g(View view, Yo0 yo0) {
            super.g(view, yo0);
            yo0.o0(Dy3.this.l.getVisibility() == 0 ? Dy3.this.getString(Rw3.mtrl_picker_toggle_to_year_selection) : Dy3.this.getString(Rw3.mtrl_picker_toggle_to_day_selection));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ Jy3 a;
        @DexIgnore
        public /* final */ /* synthetic */ MaterialButton b;

        @DexIgnore
        public Gi(Jy3 jy3, MaterialButton materialButton) {
            this.a = jy3;
            this.b = materialButton;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 0) {
                CharSequence text = this.b.getText();
                if (Build.VERSION.SDK_INT >= 16) {
                    recyclerView.announceForAccessibility(text);
                } else {
                    recyclerView.sendAccessibilityEvent(2048);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            int a2 = i < 0 ? Dy3.this.L6().a2() : Dy3.this.L6().d2();
            Dy3.this.f = this.a.h(a2);
            this.b.setText(this.a.i(a2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements View.OnClickListener {
        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        public void onClick(View view) {
            Dy3.this.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Jy3 b;

        @DexIgnore
        public Ii(Jy3 jy3) {
            this.b = jy3;
        }

        @DexIgnore
        public void onClick(View view) {
            int a2 = Dy3.this.L6().a2() + 1;
            if (a2 < Dy3.this.j.getAdapter().getItemCount()) {
                Dy3.this.O6(this.b.h(a2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Jy3 b;

        @DexIgnore
        public Ji(Jy3 jy3) {
            this.b = jy3;
        }

        @DexIgnore
        public void onClick(View view) {
            int d2 = Dy3.this.L6().d2() - 1;
            if (d2 >= 0) {
                Dy3.this.O6(this.b.h(d2));
            }
        }
    }

    @DexIgnore
    public enum Ki {
        DAY,
        YEAR
    }

    @DexIgnore
    public interface Li {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public static int K6(Context context) {
        return context.getResources().getDimensionPixelSize(Lw3.mtrl_calendar_day_height);
    }

    @DexIgnore
    public static <T> Dy3<T> M6(Zx3<T> zx3, int i2, Wx3 wx3) {
        Dy3<T> dy3 = new Dy3<>();
        Bundle bundle = new Bundle();
        bundle.putInt("THEME_RES_ID_KEY", i2);
        bundle.putParcelable("GRID_SELECTOR_KEY", zx3);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", wx3);
        bundle.putParcelable("CURRENT_MONTH_KEY", wx3.h());
        dy3.setArguments(bundle);
        return dy3;
    }

    @DexIgnore
    public final void E6(View view, Jy3 jy3) {
        MaterialButton materialButton = (MaterialButton) view.findViewById(Nw3.month_navigation_fragment_toggle);
        materialButton.setTag(u);
        Mo0.l0(materialButton, new Fi());
        MaterialButton materialButton2 = (MaterialButton) view.findViewById(Nw3.month_navigation_previous);
        materialButton2.setTag(s);
        MaterialButton materialButton3 = (MaterialButton) view.findViewById(Nw3.month_navigation_next);
        materialButton3.setTag(t);
        this.k = view.findViewById(Nw3.mtrl_calendar_year_selector_frame);
        this.l = view.findViewById(Nw3.mtrl_calendar_day_selector_frame);
        P6(Ki.DAY);
        materialButton.setText(this.f.h());
        this.j.addOnScrollListener(new Gi(jy3, materialButton));
        materialButton.setOnClickListener(new Hi());
        materialButton3.setOnClickListener(new Ii(jy3));
        materialButton2.setOnClickListener(new Ji(jy3));
    }

    @DexIgnore
    public final RecyclerView.l F6() {
        return new Ei();
    }

    @DexIgnore
    public Wx3 G6() {
        return this.e;
    }

    @DexIgnore
    public Yx3 H6() {
        return this.h;
    }

    @DexIgnore
    public Hy3 I6() {
        return this.f;
    }

    @DexIgnore
    public Zx3<S> J6() {
        return this.d;
    }

    @DexIgnore
    public LinearLayoutManager L6() {
        return (LinearLayoutManager) this.j.getLayoutManager();
    }

    @DexIgnore
    public final void N6(int i2) {
        this.j.post(new Ai(i2));
    }

    @DexIgnore
    public void O6(Hy3 hy3) {
        Jy3 jy3 = (Jy3) this.j.getAdapter();
        int j2 = jy3.j(hy3);
        int j3 = j2 - jy3.j(this.f);
        boolean z = Math.abs(j3) > 3;
        boolean z2 = j3 > 0;
        this.f = hy3;
        if (z && z2) {
            this.j.scrollToPosition(j2 - 3);
            N6(j2);
        } else if (z) {
            this.j.scrollToPosition(j2 + 3);
            N6(j2);
        } else {
            N6(j2);
        }
    }

    @DexIgnore
    public void P6(Ki ki) {
        this.g = ki;
        if (ki == Ki.YEAR) {
            this.i.getLayoutManager().y1(((Oy3) this.i.getAdapter()).i(this.f.e));
            this.k.setVisibility(0);
            this.l.setVisibility(8);
        } else if (ki == Ki.DAY) {
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            O6(this.f);
        }
    }

    @DexIgnore
    public void Q6() {
        Ki ki = this.g;
        if (ki == Ki.YEAR) {
            P6(Ki.DAY);
        } else if (ki == Ki.DAY) {
            P6(Ki.YEAR);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.c = bundle.getInt("THEME_RES_ID_KEY");
        this.d = (Zx3) bundle.getParcelable("GRID_SELECTOR_KEY");
        this.e = (Wx3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.f = (Hy3) bundle.getParcelable("CURRENT_MONTH_KEY");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i2;
        int i3;
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), this.c);
        this.h = new Yx3(contextThemeWrapper);
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(contextThemeWrapper);
        Hy3 i4 = this.e.i();
        if (Ey3.K6(contextThemeWrapper)) {
            i2 = Pw3.mtrl_calendar_vertical;
            i3 = 1;
        } else {
            i2 = Pw3.mtrl_calendar_horizontal;
            i3 = 0;
        }
        View inflate = cloneInContext.inflate(i2, viewGroup, false);
        GridView gridView = (GridView) inflate.findViewById(Nw3.mtrl_calendar_days_of_week);
        Mo0.l0(gridView, new Bi(this));
        gridView.setAdapter((ListAdapter) new Cy3());
        gridView.setNumColumns(i4.f);
        gridView.setEnabled(false);
        this.j = (RecyclerView) inflate.findViewById(Nw3.mtrl_calendar_months);
        this.j.setLayoutManager(new Ci(getContext(), i3, false, i3));
        this.j.setTag(m);
        Jy3 jy3 = new Jy3(contextThemeWrapper, this.d, this.e, new Di());
        this.j.setAdapter(jy3);
        int integer = contextThemeWrapper.getResources().getInteger(Ow3.mtrl_calendar_year_selector_span);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(Nw3.mtrl_calendar_year_selector_frame);
        this.i = recyclerView;
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            this.i.setLayoutManager(new GridLayoutManager((Context) contextThemeWrapper, integer, 1, false));
            this.i.setAdapter(new Oy3(this));
            this.i.addItemDecoration(F6());
        }
        if (inflate.findViewById(Nw3.month_navigation_fragment_toggle) != null) {
            E6(inflate, jy3);
        }
        if (!Ey3.K6(contextThemeWrapper)) {
            new Hv0().b(this.j);
        }
        this.j.scrollToPosition(jy3.j(this.f));
        return inflate;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("THEME_RES_ID_KEY", this.c);
        bundle.putParcelable("GRID_SELECTOR_KEY", this.d);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.e);
        bundle.putParcelable("CURRENT_MONTH_KEY", this.f);
    }
}
