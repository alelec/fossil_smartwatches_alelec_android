package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class No2 extends Tn2 implements Mo2 {
    @DexIgnore
    public No2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    @Override // com.fossil.Mo2
    public final void m0(Status status) throws RemoteException {
        Parcel d = d();
        Qo2.b(d, status);
        i(1, d);
    }
}
