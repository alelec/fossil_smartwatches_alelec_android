package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hg2 extends Eg2 {
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    public Hg2(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.c = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Eg2
    public final byte[] i() {
        return this.c;
    }
}
