package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lb0 implements Parcelable.Creator<Mb0> {
    @DexIgnore
    public /* synthetic */ Lb0(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Mb0 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (Wg6.a(readString, Ib0.class.getName())) {
            return Ib0.CREATOR.a(parcel);
        }
        if (Wg6.a(readString, Ob0.class.getName())) {
            return Ob0.CREATOR.a(parcel);
        }
        if (Wg6.a(readString, Kb0.class.getName())) {
            return Kb0.CREATOR.a(parcel);
        }
        return null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Mb0[] newArray(int i) {
        return new Mb0[i];
    }
}
