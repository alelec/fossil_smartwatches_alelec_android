package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Bp2 extends IInterface {
    @DexIgnore
    void R1(Zo2 zo2, Mo2 mo2) throws RemoteException;

    @DexIgnore
    void h1(Gj2 gj2, Mo2 mo2) throws RemoteException;

    @DexIgnore
    void j0(Yo2 yo2, Vn2 vn2) throws RemoteException;
}
