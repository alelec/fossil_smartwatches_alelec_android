package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G84 implements B84 {
    @DexIgnore
    @Override // com.fossil.B84
    public void a(String str, Bundle bundle) {
        X74.f().b("Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
    }
}
