package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class K54 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends L54 {
        @DexIgnore
        public /* final */ Charset a;

        @DexIgnore
        public Ai(Charset charset) {
            I14.l(charset);
            this.a = charset;
        }

        @DexIgnore
        @Override // com.fossil.L54
        public Reader a() throws IOException {
            return new InputStreamReader(K54.this.b(), this.a);
        }

        @DexIgnore
        public String toString() {
            return K54.this.toString() + ".asCharSource(" + this.a + ")";
        }
    }

    @DexIgnore
    public L54 a(Charset charset) {
        return new Ai(charset);
    }

    @DexIgnore
    public abstract InputStream b() throws IOException;
}
