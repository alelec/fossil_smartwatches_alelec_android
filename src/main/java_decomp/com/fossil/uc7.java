package com.fossil;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Uc7 {
    @DexIgnore
    public static final Uc7 a = new Ai();
    @DexIgnore
    public static final Uc7 b = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Uc7 {
        @DexIgnore
        @Override // com.fossil.Uc7
        public void a(Nc7 nc7) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Uc7 {
        @DexIgnore
        @Override // com.fossil.Uc7
        public void a(Nc7 nc7) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + nc7 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(Nc7 nc7);
}
