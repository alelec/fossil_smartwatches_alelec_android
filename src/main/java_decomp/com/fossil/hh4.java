package com.fossil;

import com.fossil.Kh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hh4 extends Kh4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Kh4.Bi c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Kh4.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Kh4.Bi c;

        @DexIgnore
        @Override // com.fossil.Kh4.Ai
        public Kh4 a() {
            String str = "";
            if (this.b == null) {
                str = " tokenExpirationTimestamp";
            }
            if (str.isEmpty()) {
                return new Hh4(this.a, this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Kh4.Ai
        public Kh4.Ai b(Kh4.Bi bi) {
            this.c = bi;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Kh4.Ai
        public Kh4.Ai c(String str) {
            this.a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Kh4.Ai
        public Kh4.Ai d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public Hh4(String str, long j, Kh4.Bi bi) {
        this.a = str;
        this.b = j;
        this.c = bi;
    }

    @DexIgnore
    @Override // com.fossil.Kh4
    public Kh4.Bi b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Kh4
    public String c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Kh4
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Kh4)) {
            return false;
        }
        Kh4 kh4 = (Kh4) obj;
        String str = this.a;
        if (str != null ? str.equals(kh4.c()) : kh4.c() == null) {
            if (this.b == kh4.d()) {
                Kh4.Bi bi = this.c;
                if (bi == null) {
                    if (kh4.b() == null) {
                        return true;
                    }
                } else if (bi.equals(kh4.b())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str == null ? 0 : str.hashCode();
        long j = this.b;
        int i2 = (int) (j ^ (j >>> 32));
        Kh4.Bi bi = this.c;
        if (bi != null) {
            i = bi.hashCode();
        }
        return ((((hashCode ^ 1000003) * 1000003) ^ i2) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "TokenResult{token=" + this.a + ", tokenExpirationTimestamp=" + this.b + ", responseCode=" + this.c + "}";
    }
}
