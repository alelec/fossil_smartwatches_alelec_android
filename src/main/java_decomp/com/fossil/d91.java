package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D91 implements Q91 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public D91() {
        this(2500, 1, 1.0f);
    }

    @DexIgnore
    public D91(int i, int i2, float f) {
        this.a = i;
        this.c = i2;
        this.d = f;
    }

    @DexIgnore
    @Override // com.fossil.Q91
    public void a(T91 t91) throws T91 {
        this.b++;
        int i = this.a;
        this.a = i + ((int) (((float) i) * this.d));
        if (!d()) {
            throw t91;
        }
    }

    @DexIgnore
    @Override // com.fossil.Q91
    public int b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Q91
    public int c() {
        return this.b;
    }

    @DexIgnore
    public boolean d() {
        return this.b <= this.c;
    }
}
