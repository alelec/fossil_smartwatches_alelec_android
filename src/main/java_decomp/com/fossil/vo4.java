package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vo4 implements Factory<AlarmHelper> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> d;

    @DexIgnore
    public Vo4(Uo4 uo4, Provider<An4> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static Vo4 a(Uo4 uo4, Provider<An4> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        return new Vo4(uo4, provider, provider2, provider3);
    }

    @DexIgnore
    public static AlarmHelper c(Uo4 uo4, An4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        AlarmHelper b2 = uo4.b(an4, userRepository, alarmsRepository);
        Lk7.c(b2, "Cannot return null from a non-@Nullable @Provides method");
        return b2;
    }

    @DexIgnore
    public AlarmHelper b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
