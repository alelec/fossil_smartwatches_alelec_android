package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dm2 extends Handler {
    @DexIgnore
    public static Fm2 a;

    @DexIgnore
    public Dm2(Looper looper) {
        super(looper);
    }

    @DexIgnore
    public Dm2(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }

    @DexIgnore
    public void a(Message message) {
        super.dispatchMessage(message);
    }

    @DexIgnore
    public final void b(Message message, long j) {
        Fm2 fm2 = a;
        if (fm2 != null) {
            fm2.d(this, message, j);
        }
    }

    @DexIgnore
    public final void dispatchMessage(Message message) {
        Fm2 fm2 = a;
        if (fm2 == null) {
            a(message);
            return;
        }
        Object b = fm2.b(this, message);
        try {
            a(message);
            fm2.c(this, message, b);
        } catch (Throwable th) {
            fm2.c(this, message, b);
            throw th;
        }
    }

    @DexIgnore
    public boolean sendMessageAtTime(Message message, long j) {
        b(message, j);
        return super.sendMessageAtTime(message, j);
    }
}
