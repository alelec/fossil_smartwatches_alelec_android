package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nb6 implements Factory<HomeHybridCustomizePresenter> {
    @DexIgnore
    public static HomeHybridCustomizePresenter a(PortfolioApp portfolioApp, Lb6 lb6, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, An4 an4) {
        return new HomeHybridCustomizePresenter(portfolioApp, lb6, microAppRepository, hybridPresetRepository, setHybridPresetToWatchUseCase, an4);
    }
}
