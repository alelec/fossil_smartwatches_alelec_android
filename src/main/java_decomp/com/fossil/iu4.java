package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iu4 implements Factory<ProfileRepository> {
    @DexIgnore
    public /* final */ Provider<Fu4> a;
    @DexIgnore
    public /* final */ Provider<ProfileRemoteDataSource> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Iu4(Provider<Fu4> provider, Provider<ProfileRemoteDataSource> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Iu4 a(Provider<Fu4> provider, Provider<ProfileRemoteDataSource> provider2, Provider<An4> provider3) {
        return new Iu4(provider, provider2, provider3);
    }

    @DexIgnore
    public static ProfileRepository c(Fu4 fu4, ProfileRemoteDataSource profileRemoteDataSource, An4 an4) {
        return new ProfileRepository(fu4, profileRemoteDataSource, an4);
    }

    @DexIgnore
    public ProfileRepository b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
