package com.fossil;

import com.fossil.fitness.SyncMode;
import com.mapped.Cd6;
import com.mapped.H60;
import com.mapped.Hg6;
import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qc extends Qq7 implements Hg6<Cd6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ C2 b;
    @DexIgnore
    public /* final */ /* synthetic */ E60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Qc(C2 c2, E60 e60) {
        super(1);
        this.b = c2;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Cd6 cd6) {
        H60 a2;
        V8[] v8Arr = ((G2) this.b).e;
        for (V8 v8 : v8Arr) {
            Ob b2 = Ob.A.b(v8.c);
            if (b2 != null) {
                switch (Nc.a[b2.ordinal()]) {
                    case 1:
                        if (v8.b == T8.c) {
                            E60.x0(this.c, false, true, null, 4);
                            break;
                        } else {
                            continue;
                        }
                    case 2:
                        E60 e60 = this.c;
                        Pp1 pp1 = new Pp1(this.b.c, v8.b);
                        Q40.Bi bi = e60.w;
                        if (bi != null) {
                            bi.onEventReceived(e60, pp1);
                            break;
                        } else {
                            continue;
                        }
                    case 3:
                        if (v8.b == T8.c && ((a2 = Cx1.f.a()) == null || this.c.l0(a2, SyncMode.AUTO_SYNC).s(new Pc(this)) == null)) {
                            this.c.F0();
                            break;
                        }
                    case 4:
                        E60 e602 = this.c;
                        Yp1 yp1 = new Yp1(this.b.c, v8.b);
                        Q40.Bi bi2 = e602.w;
                        if (bi2 != null) {
                            bi2.onEventReceived(e602, yp1);
                            break;
                        } else {
                            continue;
                        }
                    case 5:
                        E60 e603 = this.c;
                        Up1 up1 = new Up1(this.b.c, v8.b);
                        Q40.Bi bi3 = e603.w;
                        if (bi3 != null) {
                            bi3.onEventReceived(e603, up1);
                            break;
                        } else {
                            continue;
                        }
                    case 6:
                        if (v8.b == T8.c) {
                            E60.e0(this.c, false, true, null, 4);
                            break;
                        } else {
                            continue;
                        }
                }
            }
        }
        return Cd6.a;
    }
}
