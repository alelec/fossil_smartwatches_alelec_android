package com.fossil;

import android.graphics.PointF;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hv0 extends Qv0 {
    @DexIgnore
    public Lv0 d;
    @DexIgnore
    public Lv0 e;

    @DexIgnore
    @Override // com.fossil.Qv0
    public int[] c(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.l()) {
            iArr[0] = n(mVar, view, q(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.m()) {
            iArr[1] = n(mVar, view, r(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    @Override // com.fossil.Qv0
    public View h(RecyclerView.m mVar) {
        if (mVar.m()) {
            return p(mVar, r(mVar));
        }
        if (mVar.l()) {
            return p(mVar, q(mVar));
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Qv0
    public int i(RecyclerView.m mVar, int i, int i2) {
        int i3;
        int i4;
        if (!(mVar instanceof RecyclerView.v.b)) {
            return -1;
        }
        int Z = mVar.Z();
        if (Z == 0) {
            return -1;
        }
        View h = h(mVar);
        if (h == null) {
            return -1;
        }
        int i0 = mVar.i0(h);
        if (i0 == -1) {
            return -1;
        }
        int i5 = Z - 1;
        PointF a2 = ((RecyclerView.v.b) mVar).a(i5);
        if (a2 == null) {
            return -1;
        }
        if (mVar.l()) {
            i3 = o(mVar, q(mVar), i, 0);
            if (a2.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = -i3;
            }
        } else {
            i3 = 0;
        }
        if (mVar.m()) {
            i4 = o(mVar, r(mVar), 0, i2);
            if (a2.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (!mVar.m()) {
            i4 = i3;
        }
        if (i4 == 0) {
            return -1;
        }
        int i6 = i0 + i4;
        if (i6 < 0) {
            i6 = 0;
        }
        return i6 >= Z ? i5 : i6;
    }

    @DexIgnore
    public final float m(RecyclerView.m mVar, Lv0 lv0) {
        int K = mVar.K();
        if (K == 0) {
            return 1.0f;
        }
        int i = Integer.MAX_VALUE;
        int i2 = RecyclerView.UNDEFINED_DURATION;
        View view = null;
        View view2 = null;
        for (int i3 = 0; i3 < K; i3++) {
            View J = mVar.J(i3);
            int i0 = mVar.i0(J);
            if (i0 != -1) {
                if (i0 < i) {
                    i = i0;
                    view2 = J;
                }
                if (i0 > i2) {
                    i2 = i0;
                    view = J;
                }
            }
        }
        if (view2 == null || view == null) {
            return 1.0f;
        }
        int max = Math.max(lv0.d(view2), lv0.d(view)) - Math.min(lv0.g(view2), lv0.g(view));
        if (max == 0) {
            return 1.0f;
        }
        return (((float) max) * 1.0f) / ((float) ((i2 - i) + 1));
    }

    @DexIgnore
    public final int n(RecyclerView.m mVar, View view, Lv0 lv0) {
        return (lv0.g(view) + (lv0.e(view) / 2)) - (lv0.m() + (lv0.n() / 2));
    }

    @DexIgnore
    public final int o(RecyclerView.m mVar, Lv0 lv0, int i, int i2) {
        int[] d2 = d(i, i2);
        float m = m(mVar, lv0);
        if (m <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return 0;
        }
        return Math.round(((float) (Math.abs(d2[0]) > Math.abs(d2[1]) ? d2[0] : d2[1])) / m);
    }

    @DexIgnore
    public final View p(RecyclerView.m mVar, Lv0 lv0) {
        View view = null;
        int K = mVar.K();
        if (K != 0) {
            int m = lv0.m();
            int n = lv0.n() / 2;
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            while (i2 < K) {
                View J = mVar.J(i2);
                int abs = Math.abs((lv0.g(J) + (lv0.e(J) / 2)) - (m + n));
                if (abs >= i) {
                    abs = i;
                    J = view;
                }
                i2++;
                i = abs;
                view = J;
            }
        }
        return view;
    }

    @DexIgnore
    public final Lv0 q(RecyclerView.m mVar) {
        Lv0 lv0 = this.e;
        if (lv0 == null || lv0.a != mVar) {
            this.e = Lv0.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final Lv0 r(RecyclerView.m mVar) {
        Lv0 lv0 = this.d;
        if (lv0 == null || lv0.a != mVar) {
            this.d = Lv0.c(mVar);
        }
        return this.d;
    }
}
