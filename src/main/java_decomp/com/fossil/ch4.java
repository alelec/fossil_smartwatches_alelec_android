package com.fossil;

import com.fossil.Eh4;
import com.fossil.Fh4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ch4 extends Fh4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Eh4.Ai b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Fh4.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public Eh4.Ai b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Long f;
        @DexIgnore
        public String g;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(Fh4 fh4) {
            this.a = fh4.d();
            this.b = fh4.g();
            this.c = fh4.b();
            this.d = fh4.f();
            this.e = Long.valueOf(fh4.c());
            this.f = Long.valueOf(fh4.h());
            this.g = fh4.e();
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4 a() {
            String str = "";
            if (this.b == null) {
                str = " registrationStatus";
            }
            if (this.e == null) {
                str = str + " expiresInSecs";
            }
            if (this.f == null) {
                str = str + " tokenCreationEpochInSecs";
            }
            if (str.isEmpty()) {
                return new Ch4(this.a, this.b, this.c, this.d, this.e.longValue(), this.f.longValue(), this.g);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai c(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai d(String str) {
            this.a = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai e(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai f(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai g(Eh4.Ai ai) {
            if (ai != null) {
                this.b = ai;
                return this;
            }
            throw new NullPointerException("Null registrationStatus");
        }

        @DexIgnore
        @Override // com.fossil.Fh4.Ai
        public Fh4.Ai h(long j) {
            this.f = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public Ch4(String str, Eh4.Ai ai, String str2, String str3, long j, long j2, String str4) {
        this.a = str;
        this.b = ai;
        this.c = str2;
        this.d = str3;
        this.e = j;
        this.f = j2;
        this.g = str4;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public long c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public String d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        String str2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Fh4)) {
            return false;
        }
        Fh4 fh4 = (Fh4) obj;
        String str3 = this.a;
        if (str3 != null ? str3.equals(fh4.d()) : fh4.d() == null) {
            if (this.b.equals(fh4.g()) && ((str = this.c) != null ? str.equals(fh4.b()) : fh4.b() == null) && ((str2 = this.d) != null ? str2.equals(fh4.f()) : fh4.f() == null) && this.e == fh4.c() && this.f == fh4.h()) {
                String str4 = this.g;
                if (str4 == null) {
                    if (fh4.e() == null) {
                        return true;
                    }
                } else if (str4.equals(fh4.e())) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public String f() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public Eh4.Ai g() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public long h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str == null ? 0 : str.hashCode();
        int hashCode2 = this.b.hashCode();
        String str2 = this.c;
        int hashCode3 = str2 == null ? 0 : str2.hashCode();
        String str3 = this.d;
        int hashCode4 = str3 == null ? 0 : str3.hashCode();
        long j = this.e;
        int i2 = (int) (j ^ (j >>> 32));
        long j2 = this.f;
        int i3 = (int) (j2 ^ (j2 >>> 32));
        String str4 = this.g;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return ((((((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ i2) * 1000003) ^ i3) * 1000003) ^ i;
    }

    @DexIgnore
    @Override // com.fossil.Fh4
    public Fh4.Ai n() {
        return new Bi(this);
    }

    @DexIgnore
    public String toString() {
        return "PersistedInstallationEntry{firebaseInstallationId=" + this.a + ", registrationStatus=" + this.b + ", authToken=" + this.c + ", refreshToken=" + this.d + ", expiresInSecs=" + this.e + ", tokenCreationEpochInSecs=" + this.f + ", fisError=" + this.g + "}";
    }
}
