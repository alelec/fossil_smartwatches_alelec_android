package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hh;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C0 implements Callable<Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ K0 a;
    @DexIgnore
    public /* final */ /* synthetic */ G0 b;

    @DexIgnore
    public C0(G0 g0, K0 k0) {
        this.b = g0;
        this.a = k0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public Cd6 call() throws Exception {
        this.b.a.beginTransaction();
        try {
            this.b.b.insert((Hh<K0>) this.a);
            this.b.a.setTransactionSuccessful();
            return Cd6.a;
        } finally {
            this.b.a.endTransaction();
        }
    }
}
