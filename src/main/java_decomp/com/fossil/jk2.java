package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jk2 {
    @DexIgnore
    public /* final */ Intent a;
    @DexIgnore
    public /* final */ BroadcastReceiver.PendingResult b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ ScheduledFuture<?> d;

    @DexIgnore
    public Jk2(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.a = intent;
        this.b = pendingResult;
        this.d = scheduledExecutorService.schedule(new Kk2(this, intent), 9500, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            if (!this.c) {
                this.b.finish();
                this.d.cancel(false);
                this.c = true;
            }
        }
    }
}
