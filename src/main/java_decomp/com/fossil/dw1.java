package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dw1 extends Mv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Vb0 e;
    @DexIgnore
    public Gw1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Dw1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dw1 createFromParcel(Parcel parcel) {
            return new Dw1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Dw1[] newArray(int i) {
            return new Dw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Dw1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = Vb0.b;
        Parcelable readParcelable = parcel.readParcelable(Gw1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (Gw1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Dw1(Jv1 jv1, Kv1 kv1, byte[] bArr) {
        this(G80.e(0, 1), jv1, kv1, bArr);
    }

    @DexIgnore
    public Dw1(String str, Jv1 jv1, Kv1 kv1, byte[] bArr) {
        super(str, jv1, kv1);
        this.e = Vb0.b;
        this.f = new Gw1(str, bArr);
    }

    @DexIgnore
    public Dw1(JSONObject jSONObject, Cc0[] cc0Arr, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        Cc0 cc0;
        this.e = Vb0.b;
        String optString = jSONObject.optString("name");
        int length = cc0Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = cc0Arr[i];
            if (Wg6.a(optString, cc0.b)) {
                break;
            }
            i++;
        }
        byte[] bArr = cc0 != null ? cc0.c : null;
        if (bArr != null) {
            Bitmap decode = EInkImageFactory.decode(bArr, Format.RLE);
            Wg6.b(decode, "EInkImageFactory.decode(\u2026iceImageData, Format.RLE)");
            this.f = new Gw1(getName(), Cy1.b(decode, null, 1, null));
            decode.recycle();
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    @DexIgnore
    public final Cc0 a(String str) {
        return this.f.a(c().toActualSize(240, 240), str);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = d().put("name", str);
        Wg6.b(put, "getUIScript()\n          \u2026onstant.NAME, customName)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public Dw1 clone() {
        String name = getName();
        Jv1 clone = b().clone();
        Kv1 clone2 = c().clone();
        byte[] bitmapImageData = this.f.getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Dw1(name, clone, clone2, copyOf);
    }

    @DexIgnore
    public final Gw1 e() {
        return this.f;
    }

    @DexIgnore
    public final byte[] getImageData() {
        return this.f.getBitmapImageData();
    }

    @DexIgnore
    public final Dw1 setImageData(byte[] bArr) {
        this.f = new Gw1(getName(), bArr);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledHeight(float f2) {
        Mv1 scaledHeight = super.setScaledHeight(f2);
        if (scaledHeight != null) {
            return (Dw1) scaledHeight;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledPosition(Jv1 jv1) {
        Mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (Dw1) scaledPosition;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledSize(Kv1 kv1) {
        Mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (Dw1) scaledSize;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledWidth(float f2) {
        Mv1 scaledWidth = super.setScaledWidth(f2);
        if (scaledWidth != null) {
            return (Dw1) scaledWidth;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledX(float f2) {
        Mv1 scaledX = super.setScaledX(f2);
        if (scaledX != null) {
            return (Dw1) scaledX;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Dw1 setScaledY(float f2) {
        Mv1 scaledY = super.setScaledY(f2);
        if (scaledY != null) {
            return (Dw1) scaledY;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.image.ImageElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.f, i);
        }
    }
}
