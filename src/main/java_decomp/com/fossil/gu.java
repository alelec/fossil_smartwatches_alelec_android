package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gu {
    @DexIgnore
    public /* synthetic */ Gu(Qg6 qg6) {
    }

    @DexIgnore
    public final Hu a(byte b) {
        Hu hu;
        Hu[] values = Hu.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                hu = null;
                break;
            }
            hu = values[i];
            if (hu.c == b) {
                break;
            }
            i++;
        }
        return hu != null ? hu : Hu.e;
    }
}
