package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bt3 {
    @DexIgnore
    public static ScheduledExecutorService l;
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ PowerManager.WakeLock b;
    @DexIgnore
    public WorkSource c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ Map<String, Integer[]> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public AtomicInteger k;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Bt3(Context context, int i2, String str) {
        this(context, i2, str, null, context == null ? null : context.getPackageName());
    }

    @DexIgnore
    public Bt3(Context context, int i2, String str, String str2, String str3) {
        this(context, i2, str, null, str3, null);
    }

    @DexIgnore
    @SuppressLint({"UnwrappedWakeLock"})
    public Bt3(Context context, int i2, String str, String str2, String str3, String str4) {
        this.a = this;
        this.h = true;
        this.i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.k = new AtomicInteger(0);
        Rc2.l(context, "WakeLock: context must not be null");
        Rc2.h(str, "WakeLock: wakeLockName must not be empty");
        this.d = i2;
        this.f = null;
        this.g = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.e = str;
        }
        this.b = ((PowerManager) context.getSystemService("power")).newWakeLock(i2, str);
        if (Qf2.c(context)) {
            WorkSource a2 = Qf2.a(context, Of2.a(str3) ? context.getPackageName() : str3);
            this.c = a2;
            if (a2 != null && Qf2.c(this.g)) {
                WorkSource workSource = this.c;
                if (workSource != null) {
                    workSource.add(a2);
                } else {
                    this.c = a2;
                }
                try {
                    this.b.setWorkSource(this.c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e2) {
                    Log.wtf("WakeLock", e2.toString());
                }
            }
        }
        if (l == null) {
            l = Te2.a().a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004a, code lost:
        if (r0 == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0052, code lost:
        if (r13.j == 0) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        com.fossil.Ye2.a().c(r13.g, com.fossil.Xe2.a(r13.b, r6), 7, r13.e, r6, null, r13.d, e(), r14);
        r13.j++;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(long r14) {
        /*
            r13 = this;
            r3 = 0
            r1 = 1
            r2 = 0
            java.util.concurrent.atomic.AtomicInteger r0 = r13.k
            r0.incrementAndGet()
            java.lang.String r6 = r13.d(r3)
            java.lang.Object r12 = r13.a
            monitor-enter(r12)
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r13.i     // Catch:{ all -> 0x009f }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x001b
            int r0 = r13.j     // Catch:{ all -> 0x009f }
            if (r0 <= 0) goto L_0x002b
        L_0x001b:
            android.os.PowerManager$WakeLock r0 = r13.b     // Catch:{ all -> 0x009f }
            boolean r0 = r0.isHeld()     // Catch:{ all -> 0x009f }
            if (r0 != 0) goto L_0x002b
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r13.i     // Catch:{ all -> 0x009f }
            r0.clear()     // Catch:{ all -> 0x009f }
            r0 = 0
            r13.j = r0     // Catch:{ all -> 0x009f }
        L_0x002b:
            boolean r0 = r13.h     // Catch:{ all -> 0x009f }
            if (r0 == 0) goto L_0x004c
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r13.i     // Catch:{ all -> 0x009f }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x009f }
            java.lang.Integer[] r0 = (java.lang.Integer[]) r0     // Catch:{ all -> 0x009f }
            if (r0 != 0) goto L_0x008d
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r13.i     // Catch:{ all -> 0x009f }
            r2 = 1
            java.lang.Integer[] r2 = new java.lang.Integer[r2]     // Catch:{ all -> 0x009f }
            r3 = 0
            r4 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x009f }
            r2[r3] = r4     // Catch:{ all -> 0x009f }
            r0.put(r6, r2)     // Catch:{ all -> 0x009f }
            r0 = r1
        L_0x004a:
            if (r0 != 0) goto L_0x0054
        L_0x004c:
            boolean r0 = r13.h     // Catch:{ all -> 0x009f }
            if (r0 != 0) goto L_0x0074
            int r0 = r13.j     // Catch:{ all -> 0x009f }
            if (r0 != 0) goto L_0x0074
        L_0x0054:
            com.fossil.Ye2 r1 = com.fossil.Ye2.a()     // Catch:{ all -> 0x009f }
            android.content.Context r2 = r13.g     // Catch:{ all -> 0x009f }
            android.os.PowerManager$WakeLock r0 = r13.b     // Catch:{ all -> 0x009f }
            java.lang.String r3 = com.fossil.Xe2.a(r0, r6)     // Catch:{ all -> 0x009f }
            r4 = 7
            java.lang.String r5 = r13.e     // Catch:{ all -> 0x009f }
            r7 = 0
            int r8 = r13.d     // Catch:{ all -> 0x009f }
            java.util.List r9 = r13.e()     // Catch:{ all -> 0x009f }
            r10 = r14
            r1.c(r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x009f }
            int r0 = r13.j     // Catch:{ all -> 0x009f }
            int r0 = r0 + 1
            r13.j = r0     // Catch:{ all -> 0x009f }
        L_0x0074:
            monitor-exit(r12)     // Catch:{ all -> 0x009f }
            android.os.PowerManager$WakeLock r0 = r13.b
            r0.acquire()
            r0 = 0
            int r0 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x008c
            java.util.concurrent.ScheduledExecutorService r0 = com.fossil.Bt3.l
            com.fossil.Ct3 r1 = new com.fossil.Ct3
            r1.<init>(r13)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.schedule(r1, r14, r2)
        L_0x008c:
            return
        L_0x008d:
            r1 = 0
            r3 = 0
            r3 = r0[r3]
            int r3 = r3.intValue()
            int r3 = r3 + 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0[r1] = r3
            r0 = r2
            goto L_0x004a
        L_0x009f:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Bt3.a(long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r0 != false) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        if (r11.j == 1) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        com.fossil.Ye2.a().b(r11.g, com.fossil.Xe2.a(r11.b, r5), 8, r11.e, r5, null, r11.d, e());
        r11.j--;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r11 = this;
            r4 = 0
            r1 = 1
            r9 = 0
            java.util.concurrent.atomic.AtomicInteger r0 = r11.k
            int r0 = r0.decrementAndGet()
            if (r0 >= 0) goto L_0x001c
            java.lang.String r0 = "WakeLock"
            java.lang.String r2 = r11.e
            java.lang.String r2 = java.lang.String.valueOf(r2)
            java.lang.String r3 = " release without a matched acquire!"
            java.lang.String r2 = r2.concat(r3)
            android.util.Log.e(r0, r2)
        L_0x001c:
            java.lang.String r5 = r11.d(r4)
            java.lang.Object r10 = r11.a
            monitor-enter(r10)
            boolean r0 = r11.h     // Catch:{ all -> 0x0082 }
            if (r0 == 0) goto L_0x0034
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r11.i     // Catch:{ all -> 0x0082 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0082 }
            java.lang.Integer[] r0 = (java.lang.Integer[]) r0     // Catch:{ all -> 0x0082 }
            if (r0 != 0) goto L_0x0061
        L_0x0031:
            r0 = r9
        L_0x0032:
            if (r0 != 0) goto L_0x003c
        L_0x0034:
            boolean r0 = r11.h     // Catch:{ all -> 0x0082 }
            if (r0 != 0) goto L_0x005c
            int r0 = r11.j     // Catch:{ all -> 0x0082 }
            if (r0 != r1) goto L_0x005c
        L_0x003c:
            com.fossil.Ye2 r0 = com.fossil.Ye2.a()     // Catch:{ all -> 0x0082 }
            android.content.Context r1 = r11.g     // Catch:{ all -> 0x0082 }
            android.os.PowerManager$WakeLock r2 = r11.b     // Catch:{ all -> 0x0082 }
            java.lang.String r2 = com.fossil.Xe2.a(r2, r5)     // Catch:{ all -> 0x0082 }
            r3 = 8
            java.lang.String r4 = r11.e     // Catch:{ all -> 0x0082 }
            r6 = 0
            int r7 = r11.d     // Catch:{ all -> 0x0082 }
            java.util.List r8 = r11.e()     // Catch:{ all -> 0x0082 }
            r0.b(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0082 }
            int r0 = r11.j     // Catch:{ all -> 0x0082 }
            int r0 = r0 + -1
            r11.j = r0     // Catch:{ all -> 0x0082 }
        L_0x005c:
            monitor-exit(r10)     // Catch:{ all -> 0x0082 }
            r11.f(r9)
            return
        L_0x0061:
            r2 = 0
            r2 = r0[r2]
            int r2 = r2.intValue()
            if (r2 != r1) goto L_0x0071
            java.util.Map<java.lang.String, java.lang.Integer[]> r0 = r11.i
            r0.remove(r5)
            r0 = r1
            goto L_0x0032
        L_0x0071:
            r2 = 0
            r3 = 0
            r3 = r0[r3]
            int r3 = r3.intValue()
            int r3 = r3 + -1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0[r2] = r3
            goto L_0x0031
        L_0x0082:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Bt3.b():void");
    }

    @DexIgnore
    public void c(boolean z) {
        this.b.setReferenceCounted(z);
        this.h = z;
    }

    @DexIgnore
    public final String d(String str) {
        return this.h ? !TextUtils.isEmpty(str) ? str : this.f : this.f;
    }

    @DexIgnore
    public final List<String> e() {
        return Qf2.b(this.c);
    }

    @DexIgnore
    public final void f(int i2) {
        if (this.b.isHeld()) {
            try {
                this.b.release();
            } catch (RuntimeException e2) {
                if (e2.getClass().equals(RuntimeException.class)) {
                    Log.e("WakeLock", String.valueOf(this.e).concat(" was already released!"), e2);
                } else {
                    throw e2;
                }
            }
            this.b.isHeld();
        }
    }
}
