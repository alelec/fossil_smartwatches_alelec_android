package com.fossil;

import com.fossil.E88;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class L88 extends E88.Ai {
    @DexIgnore
    public static /* final */ E88.Ai a; // = new L88();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class Ai<T> implements E88<W18, Optional<T>> {
        @DexIgnore
        public /* final */ E88<W18, T> a;

        @DexIgnore
        public Ai(E88<W18, T> e88) {
            this.a = e88;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.E88
        public /* bridge */ /* synthetic */ Object a(W18 w18) throws IOException {
            return b(w18);
        }

        @DexIgnore
        public Optional<T> b(W18 w18) throws IOException {
            return Optional.ofNullable(this.a.a(w18));
        }
    }

    @DexIgnore
    @Override // com.fossil.E88.Ai
    public E88<W18, ?> d(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (E88.Ai.b(type) != Optional.class) {
            return null;
        }
        return new Ai(retrofit3.i(E88.Ai.a(0, (ParameterizedType) type), annotationArr));
    }
}
