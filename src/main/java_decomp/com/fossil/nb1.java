package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nb1<T> {
    @DexIgnore
    public static /* final */ Bi<Object> e; // = new Ai();
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ Bi<T> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public volatile byte[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Bi<Object> {
        @DexIgnore
        @Override // com.fossil.Nb1.Bi
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    }

    @DexIgnore
    public interface Bi<T> {
        @DexIgnore
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    @DexIgnore
    public Nb1(String str, T t, Bi<T> bi) {
        Ik1.b(str);
        this.c = str;
        this.a = t;
        Ik1.d(bi);
        this.b = bi;
    }

    @DexIgnore
    public static <T> Nb1<T> a(String str, T t, Bi<T> bi) {
        return new Nb1<>(str, t, bi);
    }

    @DexIgnore
    public static <T> Bi<T> b() {
        return (Bi<T>) e;
    }

    @DexIgnore
    public static <T> Nb1<T> e(String str) {
        return new Nb1<>(str, null, b());
    }

    @DexIgnore
    public static <T> Nb1<T> f(String str, T t) {
        return new Nb1<>(str, t, b());
    }

    @DexIgnore
    public T c() {
        return this.a;
    }

    @DexIgnore
    public final byte[] d() {
        if (this.d == null) {
            this.d = this.c.getBytes(Mb1.a);
        }
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Nb1) {
            return this.c.equals(((Nb1) obj).c);
        }
        return false;
    }

    @DexIgnore
    public void g(T t, MessageDigest messageDigest) {
        this.b.a(d(), t, messageDigest);
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Option{key='" + this.c + "'}";
    }
}
