package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xs4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    @Vu3("id")
    public String b;
    @DexIgnore
    @Vu3("socialId")
    public String c;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_FIRST_NAME)
    public String d;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_LAST_NAME)
    public String e;
    @DexIgnore
    @Vu3("points")
    public Integer f;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_PROFILE_PIC)
    public String g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xs4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Xs4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Xs4(parcel);
        }

        @DexIgnore
        public Xs4[] b(int i) {
            return new Xs4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Xs4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Xs4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Xs4(android.os.Parcel r11) {
        /*
            r10 = this;
            r7 = 0
            r5 = 0
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r11, r0)
            java.lang.String r1 = r11.readString()
            if (r1 == 0) goto L_0x0054
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r1, r0)
            java.lang.String r2 = r11.readString()
            if (r2 == 0) goto L_0x0050
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r2, r0)
            java.lang.String r3 = r11.readString()
            java.lang.String r4 = r11.readString()
            java.lang.Class r0 = java.lang.Integer.TYPE
            java.lang.ClassLoader r0 = r0.getClassLoader()
            java.lang.Object r0 = r11.readValue(r0)
            boolean r6 = r0 instanceof java.lang.Integer
            if (r6 != 0) goto L_0x004e
        L_0x0033:
            java.lang.Integer r5 = (java.lang.Integer) r5
            java.lang.String r6 = r11.readString()
            byte r0 = r11.readByte()
            byte r8 = (byte) r7
            if (r0 == r8) goto L_0x0041
            r7 = 1
        L_0x0041:
            int r8 = r11.readInt()
            int r9 = r11.readInt()
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            return
        L_0x004e:
            r5 = r0
            goto L_0x0033
        L_0x0050:
            com.mapped.Wg6.i()
            throw r5
        L_0x0054:
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Xs4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Xs4(String str, String str2, String str3, String str4, Integer num, String str5, boolean z, int i2, int i3) {
        Wg6.c(str, "id");
        Wg6.c(str2, "socialId");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = num;
        this.g = str5;
        this.h = z;
        this.i = i2;
        this.j = i3;
    }

    @DexIgnore
    public final boolean a() {
        return this.h;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final int c() {
        return this.j;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Xs4) {
                Xs4 xs4 = (Xs4) obj;
                if (!Wg6.a(this.b, xs4.b) || !Wg6.a(this.c, xs4.c) || !Wg6.a(this.d, xs4.d) || !Wg6.a(this.e, xs4.e) || !Wg6.a(this.f, xs4.f) || !Wg6.a(this.g, xs4.g) || this.h != xs4.h || this.i != xs4.i || this.j != xs4.j) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int f() {
        return this.i;
    }

    @DexIgnore
    public final Integer g() {
        return this.f;
    }

    @DexIgnore
    public final String h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        Integer num = this.f;
        int hashCode5 = num != null ? num.hashCode() : 0;
        String str5 = this.g;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        boolean z = this.h;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i2) * 31) + i3) * 31) + this.i) * 31) + this.j;
    }

    @DexIgnore
    public final String i() {
        return this.c;
    }

    @DexIgnore
    public final void k(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void m(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public String toString() {
        return "Friend(id=" + this.b + ", socialId=" + this.c + ", firstName=" + this.d + ", lastName=" + this.e + ", points=" + this.f + ", profilePicture=" + this.g + ", confirmation=" + this.h + ", pin=" + this.i + ", friendType=" + this.j + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeValue(this.f);
        parcel.writeString(this.g);
        parcel.writeByte(this.h ? (byte) 1 : 0);
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
    }
}
