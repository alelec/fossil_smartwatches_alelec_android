package com.fossil;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xh7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public DisplayMetrics c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public Context o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;

    @DexIgnore
    public Xh7(Context context) {
        this.b = "2.0.3";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        Context applicationContext = context.getApplicationContext();
        this.o = applicationContext;
        this.c = Ei7.u(applicationContext);
        this.a = Ei7.F(this.o);
        this.h = Fg7.v(this.o);
        this.i = Ei7.E(this.o);
        this.j = TimeZone.getDefault().getID();
        this.l = Ei7.K(this.o);
        this.k = Ei7.L(this.o);
        this.m = this.o.getPackageName();
        if (this.d >= 14) {
            this.p = Ei7.R(this.o);
        }
        this.q = Ei7.Q(this.o).toString();
        this.r = Ei7.P(this.o);
        this.s = Ei7.v();
        this.n = Ei7.b(this.o);
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String w;
        String str;
        if (thread == null) {
            if (this.c != null) {
                jSONObject.put("sr", this.c.widthPixels + G78.ANY_MARKER + this.c.heightPixels);
                jSONObject.put("dpi", this.c.xdpi + G78.ANY_MARKER + this.c.ydpi);
            }
            if (Tg7.a(this.o).i()) {
                JSONObject jSONObject2 = new JSONObject();
                Ji7.d(jSONObject2, "bs", Ji7.i(this.o));
                Ji7.d(jSONObject2, "ss", Ji7.j(this.o));
                if (jSONObject2.length() > 0) {
                    Ji7.d(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray c2 = Ji7.c(this.o, 10);
            if (c2 != null && c2.length() > 0) {
                Ji7.d(jSONObject, "wflist", c2.toString());
            }
            w = this.p;
            str = "sen";
        } else {
            Ji7.d(jSONObject, "thn", thread.getName());
            Ji7.d(jSONObject, "qq", Fg7.E(this.o));
            Ji7.d(jSONObject, "cui", Fg7.u(this.o));
            if (Ei7.t(this.r) && this.r.split("/").length == 2) {
                Ji7.d(jSONObject, "fram", this.r.split("/")[0]);
            }
            if (Ei7.t(this.s) && this.s.split("/").length == 2) {
                Ji7.d(jSONObject, "from", this.s.split("/")[0]);
            }
            if (Gh7.b(this.o).v(this.o) != null) {
                jSONObject.put("ui", Gh7.b(this.o).v(this.o).c());
            }
            w = Fg7.w(this.o);
            str = "mid";
        }
        Ji7.d(jSONObject, str, w);
        Ji7.d(jSONObject, "pcn", Ei7.M(this.o));
        Ji7.d(jSONObject, "osn", Build.VERSION.RELEASE);
        Ji7.d(jSONObject, "av", this.a);
        Ji7.d(jSONObject, "ch", this.h);
        Ji7.d(jSONObject, "mf", this.f);
        Ji7.d(jSONObject, "sv", this.b);
        Ji7.d(jSONObject, "osd", Build.DISPLAY);
        Ji7.d(jSONObject, "prod", Build.PRODUCT);
        Ji7.d(jSONObject, "tags", Build.TAGS);
        Ji7.d(jSONObject, "id", Build.ID);
        Ji7.d(jSONObject, "fng", Build.FINGERPRINT);
        Ji7.d(jSONObject, "lch", this.n);
        Ji7.d(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        Ji7.d(jSONObject, "op", this.i);
        Ji7.d(jSONObject, "lg", this.g);
        Ji7.d(jSONObject, "md", this.e);
        Ji7.d(jSONObject, "tz", this.j);
        int i2 = this.l;
        if (i2 != 0) {
            jSONObject.put("jb", i2);
        }
        Ji7.d(jSONObject, "sd", this.k);
        Ji7.d(jSONObject, "apn", this.m);
        Ji7.d(jSONObject, "cpu", this.q);
        Ji7.d(jSONObject, "abi", Build.CPU_ABI);
        Ji7.d(jSONObject, "abi2", Build.CPU_ABI2);
        Ji7.d(jSONObject, "ram", this.r);
        Ji7.d(jSONObject, "rom", this.s);
    }
}
