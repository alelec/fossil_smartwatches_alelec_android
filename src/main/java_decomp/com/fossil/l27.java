package com.fossil;

import com.portfolio.platform.uirenew.welcome.WelcomePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L27 implements Factory<WelcomePresenter> {
    @DexIgnore
    public static WelcomePresenter a(G27 g27) {
        return new WelcomePresenter(g27);
    }
}
