package com.fossil;

import android.content.Context;
import com.fossil.Be1;
import com.fossil.Hi1;
import com.fossil.Je1;
import com.fossil.Oa1;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa1 {
    @DexIgnore
    public /* final */ Map<Class<?>, Xa1<?, ?>> a; // = new Zi0();
    @DexIgnore
    public Xc1 b;
    @DexIgnore
    public Rd1 c;
    @DexIgnore
    public Od1 d;
    @DexIgnore
    public Ie1 e;
    @DexIgnore
    public Le1 f;
    @DexIgnore
    public Le1 g;
    @DexIgnore
    public Be1.Ai h;
    @DexIgnore
    public Je1 i;
    @DexIgnore
    public Zh1 j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public Oa1.Ai l; // = new Ai(this);
    @DexIgnore
    public Hi1.Bi m;
    @DexIgnore
    public Le1 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<Ej1<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Oa1.Ai {
        @DexIgnore
        public Ai(Pa1 pa1) {
        }

        @DexIgnore
        @Override // com.fossil.Oa1.Ai
        public Fj1 build() {
            return new Fj1();
        }
    }

    @DexIgnore
    public Oa1 a(Context context) {
        if (this.f == null) {
            this.f = Le1.g();
        }
        if (this.g == null) {
            this.g = Le1.e();
        }
        if (this.n == null) {
            this.n = Le1.c();
        }
        if (this.i == null) {
            this.i = new Je1.Ai(context).a();
        }
        if (this.j == null) {
            this.j = new Bi1();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new Xd1((long) b2);
            } else {
                this.c = new Sd1();
            }
        }
        if (this.d == null) {
            this.d = new Wd1(this.i.a());
        }
        if (this.e == null) {
            this.e = new He1((long) this.i.d());
        }
        if (this.h == null) {
            this.h = new Ge1(context);
        }
        if (this.b == null) {
            this.b = new Xc1(this.e, this.h, this.g, this.f, Le1.h(), this.n, this.o);
        }
        List<Ej1<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new Oa1(context, this.b, this.e, this.c, this.d, new Hi1(this.m), this.j, this.k, this.l, this.a, this.p, this.q, this.r);
    }

    @DexIgnore
    public void b(Hi1.Bi bi) {
        this.m = bi;
    }
}
