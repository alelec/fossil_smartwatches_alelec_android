package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xp4 implements Factory<N47> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Xp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Xp4 a(Uo4 uo4) {
        return new Xp4(uo4);
    }

    @DexIgnore
    public static N47 c(Uo4 uo4) {
        N47 E = uo4.E();
        Lk7.c(E, "Cannot return null from a non-@Nullable @Provides method");
        return E;
    }

    @DexIgnore
    public N47 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
