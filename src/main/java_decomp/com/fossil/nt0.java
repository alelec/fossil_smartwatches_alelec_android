package com.fossil;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nt0 implements Mt0 {
    @DexIgnore
    public /* final */ MediaSessionManager.RemoteUserInfo a;

    @DexIgnore
    public Nt0(String str, int i, int i2) {
        this.a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Nt0)) {
            return false;
        }
        return this.a.equals(((Nt0) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return Kn0.b(this.a);
    }
}
