package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.fossil.At0;
import com.mapped.Yd;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zs0 extends Yd<Cursor> {
    @DexIgnore
    public /* final */ At0<Cursor>.a a; // = new At0.Ai();
    @DexIgnore
    public Uri b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String[] e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Cursor g;
    @DexIgnore
    public Om0 h;

    @DexIgnore
    public Zs0(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        super(context);
        this.b = uri;
        this.c = strArr;
        this.d = str;
        this.e = strArr2;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.mapped.Yd
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (this.h != null) {
                this.h.a();
            }
        }
    }

    @DexIgnore
    public void deliverResult(Cursor cursor) {
        if (!isReset()) {
            Cursor cursor2 = this.g;
            this.g = cursor;
            if (isStarted()) {
                super.deliverResult((Zs0) cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.At0
    public /* bridge */ /* synthetic */ void deliverResult(Object obj) {
        deliverResult((Cursor) obj);
    }

    @DexIgnore
    @Override // com.mapped.Yd, com.fossil.At0
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.b);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(Arrays.toString(this.c));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.d);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(Arrays.toString(this.e));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.f);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.g);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(this.mContentChanged);
    }

    @DexIgnore
    @Override // com.mapped.Yd
    public Cursor loadInBackground() {
        synchronized (this) {
            if (!isLoadInBackgroundCanceled()) {
                this.h = new Om0();
            } else {
                throw new Vm0();
            }
        }
        try {
            Cursor a2 = Fl0.a(getContext().getContentResolver(), this.b, this.c, this.d, this.e, this.f, this.h);
            if (a2 != null) {
                try {
                    a2.getCount();
                    a2.registerContentObserver(this.a);
                } catch (RuntimeException e2) {
                    a2.close();
                    throw e2;
                }
            }
            synchronized (this) {
                this.h = null;
            }
            return a2;
        } catch (Throwable th) {
            synchronized (this) {
                this.h = null;
                throw th;
            }
        }
    }

    @DexIgnore
    public void onCanceled(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Yd
    public /* bridge */ /* synthetic */ void onCanceled(Cursor cursor) {
        onCanceled(cursor);
    }

    @DexIgnore
    @Override // com.fossil.At0
    public void onReset() {
        super.onReset();
        onStopLoading();
        Cursor cursor = this.g;
        if (cursor != null && !cursor.isClosed()) {
            this.g.close();
        }
        this.g = null;
    }

    @DexIgnore
    @Override // com.fossil.At0
    public void onStartLoading() {
        Cursor cursor = this.g;
        if (cursor != null) {
            deliverResult(cursor);
        }
        if (takeContentChanged() || this.g == null) {
            forceLoad();
        }
    }

    @DexIgnore
    @Override // com.fossil.At0
    public void onStopLoading() {
        cancelLoad();
    }
}
