package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.GraphRequest;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.Bz1;
import com.fossil.C02;
import com.fossil.Lz1;
import com.fossil.Mz1;
import com.fossil.Nz1;
import com.fossil.Pz1;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.zendesk.sdk.network.Constants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tz1 implements B12 {
    @DexIgnore
    public /* final */ Pd4 a;
    @DexIgnore
    public /* final */ ConnectivityManager b;
    @DexIgnore
    public /* final */ URL c; // = f(Az1.c);
    @DexIgnore
    public /* final */ T32 d;
    @DexIgnore
    public /* final */ T32 e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ URL a;
        @DexIgnore
        public /* final */ Kz1 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ai(URL url, Kz1 kz1, String str) {
            this.a = url;
            this.b = kz1;
            this.c = str;
        }

        @DexIgnore
        public Ai a(URL url) {
            return new Ai(url, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ URL b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public Bi(int i, URL url, long j) {
            this.a = i;
            this.b = url;
            this.c = j;
        }
    }

    @DexIgnore
    public Tz1(Context context, T32 t32, T32 t322) {
        Be4 be4 = new Be4();
        be4.g(Cz1.a);
        be4.h(true);
        this.a = be4.f();
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.d = t322;
        this.e = t32;
        this.f = 40000;
    }

    @DexIgnore
    public static /* synthetic */ Ai c(Ai ai, Bi bi) {
        URL url = bi.b;
        if (url == null) {
            return null;
        }
        C12.a("CctTransportBackend", "Following redirect to: %s", url);
        return ai.a(bi.b);
    }

    @DexIgnore
    public static URL f(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e2) {
            throw new IllegalArgumentException("Invalid url: " + str, e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.B12
    public V02 a(U02 u02) {
        String str;
        Mz1.Ai a2;
        HashMap hashMap = new HashMap();
        for (C02 c02 : u02.b()) {
            String j = c02.j();
            if (!hashMap.containsKey(j)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(c02);
                hashMap.put(j, arrayList);
            } else {
                ((List) hashMap.get(j)).add(c02);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry entry : hashMap.entrySet()) {
            C02 c022 = (C02) ((List) entry.getValue()).get(0);
            Nz1.Ai a3 = Nz1.a();
            a3.d(Qz1.zza);
            a3.b(this.e.a());
            a3.i(this.d.a());
            Lz1.Ai a4 = Lz1.a();
            a4.b(Lz1.Bi.zzb);
            Bz1.Ai a5 = Bz1.a();
            a5.a(Integer.valueOf(c022.g("sdk-version")));
            a5.g(c022.b(DeviceRequestsHelper.DEVICE_INFO_MODEL));
            a5.e(c022.b("hardware"));
            a5.b(c022.b("device"));
            a5.i(c022.b("product"));
            a5.h(c022.b("os-uild"));
            a5.f(c022.b("manufacturer"));
            a5.d(c022.b("fingerprint"));
            a4.a(a5.c());
            a3.c(a4.c());
            try {
                a3.a(Integer.parseInt((String) entry.getKey()));
            } catch (NumberFormatException e2) {
                a3.j((String) entry.getKey());
            }
            ArrayList arrayList3 = new ArrayList();
            for (C02 c023 : (List) entry.getValue()) {
                B02 e3 = c023.e();
                Ty1 b2 = e3.b();
                if (b2.equals(Ty1.b("proto"))) {
                    a2 = Mz1.b(e3.a());
                } else if (b2.equals(Ty1.b("json"))) {
                    a2 = Mz1.a(new String(e3.a(), Charset.forName("UTF-8")));
                } else {
                    C12.f("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", b2);
                }
                a2.a(c023.f());
                a2.e(c023.k());
                a2.f(c023.h("tz-offset"));
                Pz1.Ai a6 = Pz1.a();
                a6.b(Pz1.Ci.zza(c023.g("net-type")));
                a6.a(Pz1.Bi.zza(c023.g("mobile-subtype")));
                a2.b(a6.c());
                if (c023.d() != null) {
                    a2.c(c023.d());
                }
                arrayList3.add(a2.d());
            }
            a3.g(arrayList3);
            arrayList2.add(a3.h());
        }
        Kz1 a7 = Kz1.a(arrayList2);
        URL url = this.c;
        if (u02.c() != null) {
            try {
                Az1 c2 = Az1.c(u02.c());
                str = c2.d() != null ? c2.d() : null;
                if (c2.e() != null) {
                    url = f(c2.e());
                }
            } catch (IllegalArgumentException e4) {
                return V02.a();
            }
        } else {
            str = null;
        }
        try {
            Bi bi = (Bi) E12.a(5, new Ai(url, a7, str), Rz1.a(this), Sz1.b());
            if (bi.a == 200) {
                return V02.d(bi.c);
            }
            int i = bi.a;
            return (i >= 500 || i == 404) ? V02.e() : V02.a();
        } catch (IOException e5) {
            C12.c("CctTransportBackend", "Could not make request to the backend", e5);
            return V02.e();
        }
    }

    @DexIgnore
    @Override // com.fossil.B12
    public C02 b(C02 c02) {
        int subtype;
        NetworkInfo activeNetworkInfo = this.b.getActiveNetworkInfo();
        C02.Ai l = c02.l();
        l.a("sdk-version", Build.VERSION.SDK_INT);
        l.c(DeviceRequestsHelper.DEVICE_INFO_MODEL, Build.MODEL);
        l.c("hardware", Build.HARDWARE);
        l.c("device", Build.DEVICE);
        l.c("product", Build.PRODUCT);
        l.c("os-uild", Build.ID);
        l.c("manufacturer", Build.MANUFACTURER);
        l.c("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        l.b("tz-offset", (long) (TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        l.a("net-type", activeNetworkInfo == null ? Pz1.Ci.zzs.zza() : activeNetworkInfo.getType());
        if (activeNetworkInfo == null) {
            subtype = Pz1.Bi.zza.zza();
        } else {
            subtype = activeNetworkInfo.getSubtype();
            if (subtype == -1) {
                subtype = Pz1.Bi.zzu.zza();
            } else if (Pz1.Bi.zza(subtype) == null) {
                subtype = 0;
            }
        }
        l.a("mobile-subtype", subtype);
        return l.d();
    }

    @DexIgnore
    public final Bi d(Ai ai) throws IOException {
        C12.a("CctTransportBackend", "Making request to: %s", ai.a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) ai.a.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.f);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("User-Agent", String.format("datatransport/%s android/", "2.3.0"));
        httpURLConnection.setRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
        httpURLConnection.setRequestProperty("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        String str = ai.c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        try {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                try {
                    this.a.a(ai.b, new BufferedWriter(new OutputStreamWriter(gZIPOutputStream)));
                    gZIPOutputStream.close();
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    int responseCode = httpURLConnection.getResponseCode();
                    C12.e("CctTransportBackend", "Status Code: " + responseCode);
                    C12.e("CctTransportBackend", "Content-Type: " + httpURLConnection.getHeaderField("Content-Type"));
                    C12.e("CctTransportBackend", "Content-Encoding: " + httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER));
                    if (responseCode == 302 || responseCode == 301 || responseCode == 307) {
                        return new Bi(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0);
                    }
                    if (responseCode != 200) {
                        return new Bi(responseCode, null, 0);
                    }
                    InputStream inputStream = httpURLConnection.getInputStream();
                    try {
                        GZIPInputStream gZIPInputStream = DecompressionHelper.GZIP_ENCODING.equals(httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER)) ? new GZIPInputStream(inputStream) : inputStream;
                        try {
                            Bi bi = new Bi(responseCode, null, Oz1.b(new BufferedReader(new InputStreamReader(gZIPInputStream))).a());
                            if (gZIPInputStream != null) {
                                gZIPInputStream.close();
                            }
                            if (inputStream == null) {
                                return bi;
                            }
                            inputStream.close();
                            return bi;
                        } catch (Throwable th) {
                        }
                    } catch (Throwable th2) {
                    }
                } catch (Throwable th3) {
                }
                throw th;
                throw th;
                throw th;
                throw th;
            } catch (Throwable th4) {
            }
        } catch (ConnectException | UnknownHostException e2) {
            C12.c("CctTransportBackend", "Couldn't open connection, returning with 500", e2);
            return new Bi(500, null, 0);
        } catch (Rd4 | IOException e3) {
            C12.c("CctTransportBackend", "Couldn't encode request, returning with 400", e3);
            return new Bi(MFNetworkReturnCode.BAD_REQUEST, null, 0);
        }
    }
}
