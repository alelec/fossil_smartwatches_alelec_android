package com.fossil;

import com.facebook.internal.ServerProtocol;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I58 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public List args; // = new LinkedList();
    @DexIgnore
    public List options; // = new ArrayList();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.P58 a(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r1 = com.fossil.Y58.b(r5)
            java.util.List r0 = r4.options
            java.util.Iterator r2 = r0.iterator()
        L_0x000a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x002c
            java.lang.Object r0 = r2.next()
            com.fossil.P58 r0 = (com.fossil.P58) r0
            java.lang.String r3 = r0.getOpt()
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0021
        L_0x0020:
            return r0
        L_0x0021:
            java.lang.String r3 = r0.getLongOpt()
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x000a
            goto L_0x0020
        L_0x002c:
            r0 = 0
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.I58.a(java.lang.String):com.fossil.P58");
    }

    @DexIgnore
    public void addArg(String str) {
        this.args.add(str);
    }

    @DexIgnore
    public void addOption(P58 p58) {
        this.options.add(p58);
    }

    @DexIgnore
    public List getArgList() {
        return this.args;
    }

    @DexIgnore
    public String[] getArgs() {
        String[] strArr = new String[this.args.size()];
        this.args.toArray(strArr);
        return strArr;
    }

    @DexIgnore
    public Object getOptionObject(char c) {
        return getOptionObject(String.valueOf(c));
    }

    @DexIgnore
    public Object getOptionObject(String str) {
        try {
            return getParsedOptionValue(str);
        } catch (T58 e) {
            PrintStream printStream = System.err;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Exception found converting ");
            stringBuffer.append(str);
            stringBuffer.append(" to desired type: ");
            stringBuffer.append(e.getMessage());
            printStream.println(stringBuffer.toString());
            return null;
        }
    }

    @DexIgnore
    public Properties getOptionProperties(String str) {
        Properties properties = new Properties();
        for (P58 p58 : this.options) {
            if (str.equals(p58.getOpt()) || str.equals(p58.getLongOpt())) {
                List valuesList = p58.getValuesList();
                if (valuesList.size() >= 2) {
                    properties.put(valuesList.get(0), valuesList.get(1));
                } else if (valuesList.size() == 1) {
                    properties.put(valuesList.get(0), ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                }
            }
        }
        return properties;
    }

    @DexIgnore
    public String getOptionValue(char c) {
        return getOptionValue(String.valueOf(c));
    }

    @DexIgnore
    public String getOptionValue(char c, String str) {
        return getOptionValue(String.valueOf(c), str);
    }

    @DexIgnore
    public String getOptionValue(String str) {
        String[] optionValues = getOptionValues(str);
        if (optionValues == null) {
            return null;
        }
        return optionValues[0];
    }

    @DexIgnore
    public String getOptionValue(String str, String str2) {
        String optionValue = getOptionValue(str);
        return optionValue != null ? optionValue : str2;
    }

    @DexIgnore
    public String[] getOptionValues(char c) {
        return getOptionValues(String.valueOf(c));
    }

    @DexIgnore
    public String[] getOptionValues(String str) {
        ArrayList arrayList = new ArrayList();
        for (P58 p58 : this.options) {
            if (str.equals(p58.getOpt()) || str.equals(p58.getLongOpt())) {
                arrayList.addAll(p58.getValuesList());
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    @DexIgnore
    public P58[] getOptions() {
        List list = this.options;
        return (P58[]) list.toArray(new P58[list.size()]);
    }

    @DexIgnore
    public Object getParsedOptionValue(String str) throws T58 {
        String optionValue = getOptionValue(str);
        P58 a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Object type = a2.getType();
        if (optionValue != null) {
            return W58.i(optionValue, type);
        }
        return null;
    }

    @DexIgnore
    public boolean hasOption(char c) {
        return hasOption(String.valueOf(c));
    }

    @DexIgnore
    public boolean hasOption(String str) {
        return this.options.contains(a(str));
    }

    @DexIgnore
    public Iterator iterator() {
        return this.options.iterator();
    }
}
