package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J34<E> extends M44<E> implements B54<E> {
    @DexIgnore
    public J34(M34<E> m34, Y24<E> y24) {
        super(m34, y24);
    }

    @DexIgnore
    @Override // com.fossil.B54
    public Comparator<? super E> comparator() {
        return delegateCollection().comparator();
    }

    @DexIgnore
    @Override // com.fossil.S24, com.fossil.U24, com.fossil.Y24
    public boolean contains(Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    @Override // com.fossil.M44, com.fossil.S24
    public M34<E> delegateCollection() {
        return (M34) super.delegateCollection();
    }

    @DexIgnore
    @Override // com.fossil.Y24
    public int indexOf(Object obj) {
        int indexOf = delegateCollection().indexOf(obj);
        if (indexOf < 0 || !get(indexOf).equals(obj)) {
            return -1;
        }
        return indexOf;
    }

    @DexIgnore
    @Override // com.fossil.Y24
    public int lastIndexOf(Object obj) {
        return indexOf(obj);
    }

    @DexIgnore
    @Override // com.fossil.Y24
    public Y24<E> subListUnchecked(int i, int i2) {
        return new S44(super.subListUnchecked(i, i2), comparator()).asList();
    }
}
