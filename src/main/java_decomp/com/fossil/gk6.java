package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gk6 implements Factory<SleepOverviewMonthPresenter> {
    @DexIgnore
    public static SleepOverviewMonthPresenter a(Dk6 dk6, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        return new SleepOverviewMonthPresenter(dk6, userRepository, sleepSummariesRepository, portfolioApp);
    }
}
