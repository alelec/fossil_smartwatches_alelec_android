package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);
    @DexIgnore
    public b d;
    @DexIgnore
    public Device e;
    @DexIgnore
    public c f;
    @DexIgnore
    public /* final */ wq5.b g; // = new i(this);
    @DexIgnore
    public /* final */ UserRepository h;
    @DexIgnore
    public /* final */ DeviceRepository i;
    @DexIgnore
    public /* final */ mj5 j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ on5 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return qt5.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3024a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            pq7.c(str, "newActiveSerial");
            this.f3024a = str;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.f3024a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3025a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(int i, ArrayList<Integer> arrayList, String str) {
            this.f3025a = i;
            this.b = arrayList;
            this.c = str;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.f3025a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Device f3026a;

        @DexIgnore
        public d(Device device) {
            this.f3026a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.f3026a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$doSwitchDevice$1", f = "SwitchActiveDeviceUseCase.kt", l = {155}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qt5 qt5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object X1;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = qt5.n.a();
                StringBuilder sb = new StringBuilder();
                sb.append("doSwitchDevice serial ");
                b v = this.this$0.v();
                if (v != null) {
                    sb.append(v.b());
                    local.d(a2, sb.toString());
                    PortfolioApp c = PortfolioApp.h0.c();
                    b v2 = this.this$0.v();
                    if (v2 != null) {
                        String b = v2.b();
                        this.L$0 = iv7;
                        this.label = 1;
                        X1 = c.X1(b, this);
                        if (X1 == d) {
                            return d;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                X1 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (!((Boolean) X1).booleanValue()) {
                this.this$0.C();
                this.this$0.i(new c(116, null, ""));
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$forceSwitchWithoutEraseData$1", f = "SwitchActiveDeviceUseCase.kt", l = {181, 184}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $newActiveDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qt5 qt5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qt5;
            this.$newActiveDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$newActiveDeviceSerial, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00da  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 274
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qt5.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements iq4.e<uy6, sy6> {
        @DexIgnore
        /* renamed from: b */
        public void a(sy6 sy6) {
            pq7.c(sy6, "errorValue");
            FLogger.INSTANCE.getLocal().d(qt5.n.a(), "getDeviceSetting fail");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(uy6 uy6) {
            pq7.c(uy6, "responseValue");
            FLogger.INSTANCE.getLocal().d(qt5.n.a(), "getDeviceSetting success");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$linkServer$2", f = "SwitchActiveDeviceUseCase.kt", l = {234, 237}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Boolean, ? extends Integer>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(qt5 qt5, MisfitDeviceProfile misfitDeviceProfile, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qt5;
            this.$currentDeviceProfile = misfitDeviceProfile;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$currentDeviceProfile, this.$serial, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Boolean, ? extends Integer>> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00b8, code lost:
            if (r0 != null) goto L_0x00ba;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ba, code lost:
            if (r0 != null) goto L_0x00bc;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0170  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 514
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qt5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qt5 f3027a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$1", f = "SwitchActiveDeviceUseCase.kt", l = {75}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, String str, MisfitDeviceProfile misfitDeviceProfile, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
                this.$serial = str;
                this.$currentDeviceProfile = misfitDeviceProfile;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$serial, this.$currentDeviceProfile, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object x;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    qt5 qt5 = this.this$0.f3027a;
                    String str = this.$serial;
                    pq7.b(str, "serial");
                    MisfitDeviceProfile misfitDeviceProfile = this.$currentDeviceProfile;
                    if (misfitDeviceProfile != null) {
                        this.L$0 = iv7;
                        this.label = 1;
                        x = qt5.x(str, misfitDeviceProfile, this);
                        if (x == d) {
                            return d;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    x = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                cl7 cl7 = (cl7) x;
                boolean booleanValue = ((Boolean) cl7.component1()).booleanValue();
                int intValue = ((Number) cl7.component2()).intValue();
                PortfolioApp c = PortfolioApp.h0.c();
                String str2 = this.$serial;
                pq7.b(str2, "serial");
                c.Y1(str2, booleanValue, intValue);
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase$mSwitchDeviceReceiver$1$receive$2", f = "SwitchActiveDeviceUseCase.kt", l = {95, 99}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MisfitDeviceProfile $currentDeviceProfile;
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(i iVar, MisfitDeviceProfile misfitDeviceProfile, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = iVar;
                this.$currentDeviceProfile = misfitDeviceProfile;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$currentDeviceProfile, this.$serial, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0067  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x0069
                    if (r0 == r6) goto L_0x0034
                    if (r0 != r7) goto L_0x002c
                    java.lang.Object r0 = r8.L$1
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r9)
                L_0x0019:
                    com.fossil.qt5$i r0 = r8.this$0
                    com.fossil.qt5 r0 = r0.f3027a
                    com.fossil.qt5$d r1 = new com.fossil.qt5$d
                    com.portfolio.platform.data.model.Device r2 = r0.u()
                    r1.<init>(r2)
                    r0.j(r1)
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x002b:
                    return r0
                L_0x002c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0034:
                    java.lang.Object r0 = r8.L$1
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    int r2 = r8.I$0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r9)
                    r3 = r0
                L_0x0042:
                    r0 = r2
                L_0x0043:
                    com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.h0
                    com.portfolio.platform.PortfolioApp r2 = r2.c()
                    java.lang.String r5 = r8.$serial
                    java.lang.String r6 = "serial"
                    com.fossil.pq7.b(r5, r6)
                    r2.J0(r5)
                    com.fossil.p37$a r2 = com.fossil.p37.h
                    com.fossil.p37 r2 = r2.a()
                    r8.L$0 = r1
                    r8.I$0 = r0
                    r8.L$1 = r3
                    r8.label = r7
                    java.lang.Object r0 = r2.d(r8)
                    if (r0 != r4) goto L_0x0019
                    r0 = r4
                    goto L_0x002b
                L_0x0069:
                    com.fossil.el7.b(r9)
                    com.fossil.iv7 r1 = r8.p$
                    com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile r0 = r8.$currentDeviceProfile
                    com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r0 = r0.getVibrationStrength()
                    int r0 = r0.getVibrationStrengthLevel()
                    int r0 = com.fossil.tk5.c(r0)
                    com.fossil.qt5$i r2 = r8.this$0
                    com.fossil.qt5 r2 = r2.f3027a
                    com.portfolio.platform.data.source.DeviceRepository r2 = com.fossil.qt5.n(r2)
                    java.lang.String r3 = r8.$serial
                    java.lang.String r5 = "serial"
                    com.fossil.pq7.b(r3, r5)
                    com.portfolio.platform.data.model.Device r3 = r2.getDeviceBySerial(r3)
                    if (r3 == 0) goto L_0x0043
                    java.lang.Integer r2 = r3.getVibrationStrength()
                    if (r2 != 0) goto L_0x00b8
                L_0x0097:
                    java.lang.Integer r2 = com.fossil.ao7.e(r0)
                    r3.setVibrationStrength(r2)
                    com.fossil.qt5$i r2 = r8.this$0
                    com.fossil.qt5 r2 = r2.f3027a
                    com.portfolio.platform.data.source.DeviceRepository r2 = com.fossil.qt5.n(r2)
                    r8.L$0 = r1
                    r8.I$0 = r0
                    r8.L$1 = r3
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r2.updateDevice(r3, r5, r8)
                    if (r2 != r4) goto L_0x00bf
                    r0 = r4
                    goto L_0x002b
                L_0x00b8:
                    int r2 = r2.intValue()
                    if (r2 == r0) goto L_0x0043
                    goto L_0x0097
                L_0x00bf:
                    r2 = r0
                    goto L_0x0042
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.qt5.i.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(qt5 qt5) {
            this.f3027a = qt5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (communicateMode == CommunicateMode.SWITCH_DEVICE) {
                b v = this.f3027a.v();
                if (!pq7.a(stringExtra, v != null ? v.b() : null)) {
                    return;
                }
                if (intExtra == ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()) {
                    Bundle extras = intent.getExtras();
                    if (extras != null) {
                        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(this, stringExtra, (MisfitDeviceProfile) extras.getParcelable("device"), null), 3, null);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.f3027a.C();
                    FLogger.INSTANCE.getLocal().d(qt5.n.a(), "Switch device  success");
                    Bundle extras2 = intent.getExtras();
                    if (extras2 != null) {
                        MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                        if (misfitDeviceProfile != null) {
                            if (misfitDeviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                                this.f3027a.l.k1(misfitDeviceProfile.getHeartRateMode());
                            }
                            qt5 qt5 = this.f3027a;
                            pq7.b(stringExtra, "serial");
                            qt5.t(stringExtra);
                            xw7 unused2 = gu7.d(jv7.a(bw7.b()), null, null, new b(this, misfitDeviceProfile, stringExtra, null), 3, null);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    this.f3027a.C();
                    int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>();
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = qt5.n.a();
                    local.d(a2, "stop current workout fail due to " + intExtra2);
                    if (intExtra2 != 1101) {
                        if (intExtra2 == 1928) {
                            c w = this.f3027a.w();
                            if (w == null || this.f3027a.i(w) == null) {
                                this.f3027a.i(new c(116, null, ""));
                                return;
                            }
                            return;
                        } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                            this.f3027a.i(new c(117, null, ""));
                            return;
                        }
                    }
                    this.f3027a.i(new c(113, integerArrayListExtra, ""));
                }
            }
        }
    }

    /*
    static {
        String simpleName = qt5.class.getSimpleName();
        pq7.b(simpleName, "SwitchActiveDeviceUseCase::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public qt5(UserRepository userRepository, DeviceRepository deviceRepository, mj5 mj5, PortfolioApp portfolioApp, on5 on5) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(portfolioApp, "mApp");
        pq7.c(on5, "mSharePrefs");
        this.h = userRepository;
        this.i = deviceRepository;
        this.j = mj5;
        this.k = portfolioApp;
        this.l = on5;
    }

    @DexIgnore
    public final void A(Device device) {
        this.e = device;
    }

    @DexIgnore
    public final void B(c cVar) {
        this.f = cVar;
    }

    @DexIgnore
    public final void C() {
        FLogger.INSTANCE.getLocal().d(m, "unregisterReceiver ");
        wq5.d.j(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return m;
    }

    @DexIgnore
    public final xw7 r() {
        return gu7.d(g(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final xw7 s(String str) {
        return gu7.d(g(), null, null, new f(this, str, null), 3, null);
    }

    @DexIgnore
    public final void t(String str) {
        pq7.c(str, "serial");
        FLogger.INSTANCE.getLocal().d(m, "getDeviceSetting start");
        this.j.a(str).e(new ty6(str, ry6.SWITCH), new g());
    }

    @DexIgnore
    public final Device u() {
        return this.e;
    }

    @DexIgnore
    public final b v() {
        return this.d;
    }

    @DexIgnore
    public final c w() {
        return this.f;
    }

    @DexIgnore
    public final /* synthetic */ Object x(String str, MisfitDeviceProfile misfitDeviceProfile, qn7<? super cl7<Boolean, Integer>> qn7) {
        return eu7.g(bw7.b(), new h(this, misfitDeviceProfile, str, null), qn7);
    }

    @DexIgnore
    public final void y() {
        FLogger.INSTANCE.getLocal().d(m, "registerReceiver ");
        wq5.d.j(this.g, CommunicateMode.SWITCH_DEVICE);
        wq5.d.e(this.g, CommunicateMode.SWITCH_DEVICE);
    }

    @DexIgnore
    /* renamed from: z */
    public Object k(b bVar, qn7<Object> qn7) {
        if (bVar == null) {
            return new c(600, null, "");
        }
        this.d = bVar;
        String J = this.k.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "run with mode " + bVar.a() + " currentActive " + J);
        if (bVar.a() != 4) {
            r();
        } else {
            s(bVar.b());
        }
        return new Object();
    }
}
