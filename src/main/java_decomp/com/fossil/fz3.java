package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fz3 extends SparseArray<Parcelable> implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Fz3> CREATOR; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.ClassLoaderCreator<Fz3> {
        @DexIgnore
        public Fz3 a(Parcel parcel) {
            return new Fz3(parcel, null);
        }

        @DexIgnore
        public Fz3 b(Parcel parcel, ClassLoader classLoader) {
            return new Fz3(parcel, classLoader);
        }

        @DexIgnore
        public Fz3[] c(int i) {
            return new Fz3[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.ClassLoaderCreator
        public /* bridge */ /* synthetic */ Fz3 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return b(parcel, classLoader);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return c(i);
        }
    }

    @DexIgnore
    public Fz3() {
    }

    @DexIgnore
    public Fz3(Parcel parcel, ClassLoader classLoader) {
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = keyAt(i2);
            parcelableArr[i2] = (Parcelable) valueAt(i2);
        }
        parcel.writeInt(size);
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }
}
