package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M97 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public /* final */ C77 c;
    @DexIgnore
    public /* final */ F77 d;

    @DexIgnore
    public M97(D77 d77) {
        Wg6.c(d77, "photoListener");
        this.c = new C77(1, d77);
        this.d = new F77(2, d77);
    }

    @DexIgnore
    public final void g(int i) {
        this.d.a(i);
        notifyItemChanged(this.b);
        notifyItemChanged(i);
        this.b = i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.c.b(this.a, i)) {
            return this.c.a();
        }
        if (this.d.d(this.a, i)) {
            return this.d.b();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    public final void h(List<? extends Object> list, int i) {
        Wg6.c(list, "data");
        if (this.b != i) {
            this.b = i;
            this.d.a(i);
        }
        if (!Wg6.a(list, this.a)) {
            this.d.c(false);
            this.a.clear();
            this.a.addAll(list);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void i(boolean z) {
        this.d.c(z);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == this.c.a()) {
            this.c.c(this.a, i, viewHolder);
        } else if (itemViewType == this.d.b()) {
            this.d.e(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == this.c.a()) {
            return this.c.d(viewGroup);
        }
        if (i == this.d.b()) {
            return this.d.f(viewGroup);
        }
        throw new IllegalArgumentException("No delegate for this viewtype : " + i);
    }
}
