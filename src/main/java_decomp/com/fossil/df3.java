package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Df3 implements Parcelable.Creator<Le3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Le3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f4 = 0.5f;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f6 = 1.0f;
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        IBinder iBinder = null;
        String str = null;
        String str2 = null;
        LatLng latLng = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    latLng = (LatLng) Ad2.e(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 4:
                    str = Ad2.f(parcel, t);
                    break;
                case 5:
                    iBinder = Ad2.u(parcel, t);
                    break;
                case 6:
                    f = Ad2.r(parcel, t);
                    break;
                case 7:
                    f2 = Ad2.r(parcel, t);
                    break;
                case 8:
                    z = Ad2.m(parcel, t);
                    break;
                case 9:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 10:
                    z3 = Ad2.m(parcel, t);
                    break;
                case 11:
                    f3 = Ad2.r(parcel, t);
                    break;
                case 12:
                    f4 = Ad2.r(parcel, t);
                    break;
                case 13:
                    f5 = Ad2.r(parcel, t);
                    break;
                case 14:
                    f6 = Ad2.r(parcel, t);
                    break;
                case 15:
                    f7 = Ad2.r(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Le3(latLng, str2, str, iBinder, f, f2, z, z2, z3, f3, f4, f5, f6, f7);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Le3[] newArray(int i) {
        return new Le3[i];
    }
}
