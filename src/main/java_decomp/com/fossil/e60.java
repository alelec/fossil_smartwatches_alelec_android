package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import com.fossil.Ar1;
import com.fossil.Ix1;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.DeviceImplementation$applyTheme$Anon1;
import com.fossil.blesdk.device.DeviceImplementation$clearCache$Anon1;
import com.fossil.blesdk.device.DeviceImplementation$previewTheme$Anon4;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.fossil.common.task.Promise;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.SyncMode;
import com.fossil.fitness.WorkoutSessionManager;
import com.mapped.C90;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.H60;
import com.mapped.HandMovingConfig;
import com.mapped.Hg6;
import com.mapped.Jh6;
import com.mapped.Md0;
import com.mapped.N70;
import com.mapped.Na0;
import com.mapped.Nd0;
import com.mapped.Q40;
import com.mapped.Qb0;
import com.mapped.Qg6;
import com.mapped.R50;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Ta0;
import com.mapped.Tc0;
import com.mapped.U40;
import com.mapped.Wa0;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.mapped.Z40;
import com.mapped.Zb0;
import com.mapped.Zd0;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E60 implements Q40, Bs1, Or1, Tr1, Vr1, Na0, Xr1, Ds1, Ar1, Br1, Cr1, Er1, Gs1, Gr1, Hr1, Jr1, Mr1, Nr1, Od, Pd, Qd, Ir1, Hs1, Yq1, Qr1, Rr1, Js1, Sr1, Ps1, Ts1, Ls1, Ta0, Dr1, Wr1, Zr1, As1, Is1, Cs1, Es1, Qs1, Rs1, Ns1, Kr1, Os1, Zq1, Pr1, Vq1, Ur1, Wq1, Qb0, Yr1, Ms1, Fr1, Fs1, Wa0 {
    @DexIgnore
    public static /* final */ Uz CREATOR; // = new Uz(null);
    @DexIgnore
    public /* final */ I60 b; // = new I60(this);
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ K5 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ Qe g;
    @DexIgnore
    public /* final */ Uc h;
    @DexIgnore
    public /* final */ P50 i;
    @DexIgnore
    public /* final */ Hg6<C2, Cd6> j;
    @DexIgnore
    public /* final */ Coroutine<byte[], N6, Cd6> k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ Object m;
    @DexIgnore
    public Ar1.Ai s;
    @DexIgnore
    public WorkoutSessionManager t;
    @DexIgnore
    public Zk1 u;
    @DexIgnore
    public Q40.Ci v;
    @DexIgnore
    public Q40.Bi w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public /* final */ BluetoothDevice y;
    @DexIgnore
    public /* final */ String z;

    @DexIgnore
    public /* synthetic */ E60(BluetoothDevice bluetoothDevice, String str, Qg6 qg6) {
        this.y = bluetoothDevice;
        this.z = str;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.c = new Handler(myLooper);
            this.d = X4.b.a(this.y);
            this.g = new Qe(new Nw(new Hashtable(), null));
            this.h = new Uc();
            this.i = new P50(this);
            this.j = new Yw(this);
            this.k = new Gd(this);
            this.l = E.a("UUID.randomUUID().toString()");
            this.m = new Object();
            this.u = new Zk1(this.d.C(), this.d.x, this.z, "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
            this.v = Q40.Ci.DISCONNECTED;
            K5 k5 = this.d;
            P50 p50 = this.i;
            if (!k5.k.contains(p50)) {
                k5.k.add(p50);
            }
            Zk1 zk1 = this.u;
            C90.e.d(zk1.getMacAddress(), zk1);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Yb0 e0(E60 e60, boolean z2, boolean z3, A9 a9, int i2) {
        if ((i2 & 2) != 0) {
            z3 = false;
        }
        if ((i2 & 4) != 0) {
            a9 = z3 ? A9.b : A9.c;
        }
        return e60.h0(z2, z3, a9);
    }

    @DexIgnore
    public static final /* synthetic */ void p0(E60 e60, Ky1 ky1, String str, String str2, Object... objArr) {
    }

    @DexIgnore
    public static final /* synthetic */ boolean w0(E60 e60) {
        T t2;
        boolean z2;
        if (e60.d.w == F5.d) {
            Iterator<T> it = e60.g.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it.next();
                if (t2.y == Yp.g) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            if (t2 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static /* synthetic */ Yb0 x0(E60 e60, boolean z2, boolean z3, A9 a9, int i2) {
        if ((i2 & 2) != 0) {
            z3 = false;
        }
        if ((i2 & 4) != 0) {
            a9 = z3 ? A9.b : A9.c;
        }
        return e60.y0(z2, z3, a9);
    }

    @DexIgnore
    @Override // com.mapped.Ta0
    public byte[] A() {
        return Ix.b.a(this.d.x).a;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public void B(Q40.Bi bi) {
        this.w = bi;
    }

    @DexIgnore
    public final boolean B0() {
        boolean z2;
        synchronized (Boolean.valueOf(this.f)) {
            z2 = this.f;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.Yq1
    public Yb0<Cd6> C(Nd0[] nd0Arr) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(nd0Arr).toString(2);
        Sm sm = new Sm(this.d, this.b, nd0Arr);
        synchronized (this.m) {
            U40 d0 = d0(sm);
            yb0 = new Yb0<>();
            if (d0 != null) {
                sm.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Z50(this, sm));
                R60 r60 = new R60(this, sm);
                if (!sm.t) {
                    sm.d.add(r60);
                }
                A80 a80 = new A80(yb0, this, sm);
                if (!sm.t) {
                    sm.h.add(a80);
                }
                sm.a(new Wy(yb0, this, sm));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                sm.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void C0() {
        Ky1 ky1 = Ky1.DEBUG;
        this.g.c(Zq.G, new Yp[]{Yp.g});
        if (!(this.v == Q40.Ci.DISCONNECTED && this.d.w == F5.b) && !this.e) {
            Km km = new Km(this.d, this.b);
            km.a(new O50(this));
            km.F();
        }
    }

    @DexIgnore
    @Override // com.fossil.Is1
    public Yb0<Cd6> D(NotificationFilter[] notificationFilterArr) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(notificationFilterArr).toString(2);
        Ig ig = new Ig(this.d, this.b, notificationFilterArr, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(ig);
            yb0 = new Yb0<>();
            if (d0 != null) {
                ig.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Y70(this, ig));
                V0 v0 = new V0(this, ig);
                if (!ig.t) {
                    ig.d.add(v0);
                }
                I3 i3 = new I3(yb0, this, ig);
                if (!ig.t) {
                    ig.h.add(i3);
                }
                ig.a(new R0(yb0, this, ig));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ig.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void D0() {
        this.u = new Zk1(this.u.getName(), this.u.getMacAddress(), this.u.getSerialNumber(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
    }

    @DexIgnore
    @Override // com.fossil.Rr1
    public Yb0<String> E(byte[] bArr, String str) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        int length = bArr.length;
        Ko ko = new Ko(this.d, this.b, bArr, str);
        Y20 y20 = new Y20(this);
        if (!ko.t) {
            ko.d.add(y20);
        }
        ko.v(new P30(this));
        ko.s(new G40(this));
        synchronized (this.m) {
            U40 d0 = d0(ko);
            yb0 = new Yb0();
            if (d0 != null) {
                ko.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Z0(this, ko));
                S1 s1 = new S1(this, ko);
                if (!ko.t) {
                    ko.d.add(s1);
                }
                D4 d4 = new D4(yb0, this, ko);
                if (!ko.t) {
                    ko.h.add(d4);
                }
                ko.a(new I20(yb0, this, ko));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ko.F();
            }
        }
        return yb0.t(X40.b);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v2. Raw type applied. Possible types: com.mapped.Coroutine<byte[], com.fossil.N6, com.mapped.Cd6>, com.mapped.Coroutine<? super byte[], ? super com.fossil.N6, com.mapped.Cd6> */
    public final void E0() {
        Kk kk = new Kk(this.d, this.b);
        kk.C = this.j;
        kk.D = this.k;
        kk.v(S0.b);
        kk.s(new L1(this));
        kk.F();
    }

    @DexIgnore
    @Override // com.fossil.Cr1
    public boolean F() {
        Ky1 ky1 = Ky1.DEBUG;
        if (!isActive()) {
            Zw.i.i(this);
        }
        D90.i.d(new A90(Ey1.a(I00.b), V80.e, this.d.x, Ey1.a(I00.b), E.a("UUID.randomUUID().toString()"), true, this.l, null, null, null, 896));
        I0(true);
        return true;
    }

    @DexIgnore
    public final Zb0<Cd6> F0() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Tr tr = new Tr(this.d, this.b, null, 4);
        synchronized (this.m) {
            U40 d0 = d0(tr);
            yb0 = new Yb0();
            if (d0 != null) {
                tr.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new V20(this, tr));
                Id id = new Id(this, tr);
                if (!tr.t) {
                    tr.d.add(id);
                }
                Vz vz = new Vz(yb0, this, tr);
                if (!tr.t) {
                    tr.h.add(vz);
                }
                tr.a(new V50(yb0, this, tr));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                tr.F();
            }
        }
        return yb0.t(N60.b);
    }

    @DexIgnore
    public boolean G0() {
        boolean z2;
        synchronized (Boolean.valueOf(this.x)) {
            z2 = this.x;
        }
        return z2;
    }

    @DexIgnore
    @Override // com.fossil.Sr1
    public Zb0<Integer> H() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Uj uj = new Uj(this.d, this.b);
        synchronized (this.m) {
            U40 d0 = d0(uj);
            yb0 = new Yb0();
            if (d0 != null) {
                uj.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new A1(this, uj));
                T1 t1 = new T1(this, uj);
                if (!uj.t) {
                    uj.d.add(t1);
                }
                E4 e4 = new E4(yb0, this, uj);
                if (!uj.t) {
                    uj.h.add(e4);
                }
                uj.a(new H40(yb0, this, uj));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                uj.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public Q40.Di H0() {
        return Q40.Di.g.a(this.d.D());
    }

    @DexIgnore
    @Override // com.fossil.Dr1
    public Zb0<byte[]> I(byte[] bArr) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Dy1.e(bArr, null, 1, null);
        Fq fq = new Fq(this.d, this.b, bArr);
        fq.v(new X60(this));
        synchronized (this.m) {
            U40 d0 = d0(fq);
            yb0 = new Yb0();
            if (d0 != null) {
                fq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Ic(this, fq));
                Jd jd = new Jd(this, fq);
                if (!fq.t) {
                    fq.d.add(jd);
                }
                Cx cx = new Cx(yb0, this, fq);
                if (!fq.t) {
                    fq.h.add(cx);
                }
                fq.a(new F60(yb0, this, fq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                fq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public void I0(boolean z2) {
        D90.i.d(new A90(Ey1.a(I00.g), V80.i, this.d.x, "", "", true, null, null, null, G80.k(new JSONObject(), Jd0.z0, Boolean.valueOf(z2)), 448));
        synchronized (Boolean.valueOf(this.x)) {
            this.x = z2;
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.Es1
    public Yb0<Zm1[]> J(R60[] r60Arr) {
        Yb0<Zm1[]> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(r60Arr).toString(2);
        ArrayList arrayList = new ArrayList();
        for (R60 r60 : r60Arr) {
            if (Em7.B(this.u.g(), r60.getKey())) {
                arrayList.add(r60);
            }
        }
        if (arrayList.isEmpty()) {
            Yb0<Zm1[]> yb02 = new Yb0<>();
            yb02.o(new Zm1[0]);
            return yb02;
        }
        K5 k5 = this.d;
        I60 i60 = this.b;
        Object[] array = arrayList.toArray(new R60[0]);
        if (array != null) {
            Fm fm = new Fm(k5, i60, (R60[]) array, 0, null, 24);
            synchronized (this.m) {
                U40 d0 = d0(fm);
                yb0 = new Yb0<>();
                if (d0 != null) {
                    fm.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                    yb0.n(new Bl1(d0, null, 2));
                } else {
                    yb0.u(new U20(this, fm));
                    K30 k30 = new K30(this, fm);
                    if (!fm.t) {
                        fm.d.add(k30);
                    }
                    R40 r40 = new R40(yb0, this, fm);
                    if (!fm.t) {
                        fm.h.add(r40);
                    }
                    fm.a(new D8(yb0, this, fm));
                    if (this.v != Q40.Ci.CONNECTED && !this.e) {
                        z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                    }
                    fm.F();
                }
            }
            return yb0;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Wq1
    public Yb0<Cd6> K(long j2) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        String str = "setBuddyChallengeMinimumStepThreshold invoked: minimumStepThreshold=" + j2 + '.';
        Wi wi = new Wi(this.d, this.b, j2, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(wi);
            yb0 = new Yb0<>();
            if (d0 != null) {
                wi.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Ld(this, wi));
                Fe fe = new Fe(this, wi);
                if (!wi.t) {
                    wi.d.add(fe);
                }
                Zx zx = new Zx(yb0, this, wi);
                if (!wi.t) {
                    wi.h.add(zx);
                }
                wi.a(new T50(yb0, this, wi));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                wi.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Tr1
    public Zb0<Cd6> L() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        K5 k5 = this.d;
        Cq cq = new Cq(k5, this.b, Yp.s, new Pu(k5));
        synchronized (this.m) {
            U40 d0 = d0(cq);
            yb0 = new Yb0();
            if (d0 != null) {
                cq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Dy(this, cq));
                Vy vy = new Vy(this, cq);
                if (!cq.t) {
                    cq.d.add(vy);
                }
                B00 b00 = new B00(yb0, this, cq);
                if (!cq.t) {
                    cq.h.add(b00);
                }
                cq.a(new A70(yb0, this, cq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public Zk1 M() {
        return this.u;
    }

    @DexIgnore
    @Override // com.mapped.Wa0
    public Yb0<Cd6> N(Lw1 lw1) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        lw1.toJSONString(2);
        Yb0<Cd6> yb02 = new Yb0<>();
        Bg bg = new Bg(this.d, this.b, lw1.j());
        synchronized (this.m) {
            U40 d0 = d0(bg);
            yb0 = new Yb0();
            if (d0 != null) {
                bg.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Zz(this, bg));
                N00 n00 = new N00(this, bg);
                if (!bg.t) {
                    bg.d.add(n00);
                }
                R10 r10 = new R10(yb0, this, bg);
                if (!bg.t) {
                    bg.h.add(r10);
                }
                bg.a(new Bc(yb0, this, bg));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                bg.F();
            }
        }
        yb0.x(new Bd(yb02)).v(new Vd(yb02)).w(new Tw(yb02));
        return yb02;
    }

    @DexIgnore
    @Override // com.fossil.Vr1
    public Zb0<Cd6> O() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        K5 k5 = this.d;
        Cq cq = new Cq(k5, this.b, Yp.r, new Qu(k5));
        synchronized (this.m) {
            U40 d0 = d0(cq);
            yb0 = new Yb0();
            if (d0 != null) {
                cq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new U1(this, cq));
                O3 o3 = new O3(this, cq);
                if (!cq.t) {
                    cq.d.add(o3);
                }
                K8 k8 = new K8(yb0, this, cq);
                if (!cq.t) {
                    cq.h.add(k8);
                }
                cq.a(new C3(yb0, this, cq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public Q40.Ai P() {
        return Q40.Ai.c.b(this.d.H());
    }

    @DexIgnore
    @Override // com.fossil.Hr1
    public Yb0<HashMap<Zm1, R60>> R() {
        Yb0<HashMap<Zm1, R60>> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        K5 k5 = this.d;
        Lm lm = new Lm(k5, this.b, Ke.b.a(k5.x, Ob.l));
        synchronized (this.m) {
            U40 d0 = d0(lm);
            yb0 = new Yb0<>();
            if (d0 != null) {
                lm.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new J50(this, lm));
                C60 c60 = new C60(this, lm);
                if (!lm.t) {
                    lm.d.add(c60);
                }
                M70 m70 = new M70(yb0, this, lm);
                if (!lm.t) {
                    lm.h.add(m70);
                }
                lm.a(new Fy(yb0, this, lm));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                lm.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Wa0
    public Yb0<Cd6> U(ThemeEditor themeEditor) {
        Ky1 ky1 = Ky1.DEBUG;
        themeEditor.d();
        Yb0<Cd6> yb0 = new Yb0<>();
        Rm6 unused = Gu7.d(Id0.i.e(), null, null, new DeviceImplementation$previewTheme$Anon4(this, themeEditor, yb0, null), 3, null);
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Mr1
    public Zb0<C90> V() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Zk zk = new Zk(this.d, this.b);
        synchronized (this.m) {
            U40 d0 = d0(zk);
            yb0 = new Yb0();
            if (d0 != null) {
                zk.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Bx(this, zk));
                Xx xx = new Xx(this, zk);
                if (!zk.t) {
                    zk.d.add(xx);
                }
                Gz gz = new Gz(yb0, this, zk);
                if (!zk.t) {
                    zk.h.add(gz);
                }
                zk.a(new W20(yb0, this, zk));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                zk.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Cs1
    public Zb0<Cd6> W() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        K5 k5 = this.d;
        Cq cq = new Cq(k5, this.b, Yp.u, new Ru(k5));
        synchronized (this.m) {
            U40 d0 = d0(cq);
            yb0 = new Yb0();
            if (d0 != null) {
                cq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new P00(this, cq));
                E10 e10 = new E10(this, cq);
                if (!cq.t) {
                    cq.d.add(e10);
                }
                G20 g20 = new G20(yb0, this, cq);
                if (!cq.t) {
                    cq.h.add(g20);
                }
                cq.a(new S50(yb0, this, cq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Zr1
    public Yb0<Cd6> Y(Yn1 yn1) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        yn1.toJSONString(2);
        Ao ao = new Ao(this.d, this.b, yn1, 0, null, 24);
        synchronized (this.m) {
            U40 d0 = d0(ao);
            yb0 = new Yb0<>();
            if (d0 != null) {
                ao.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new J00(this, ao));
                X00 x00 = new X00(this, ao);
                if (!ao.t) {
                    ao.d.add(x00);
                }
                A20 a20 = new A20(yb0, this, ao);
                if (!ao.t) {
                    ao.h.add(a20);
                }
                ao.a(new Jy(yb0, this, ao));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ao.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Ls1
    public void Z(byte[] bArr) {
        Ky1 ky1 = Ky1.DEBUG;
        Dy1.e(bArr, null, 1, null);
        D90.i.d(new A90(Ey1.a(I00.d), V80.e, this.d.x, Ey1.a(I00.d), E.a("UUID.randomUUID().toString()"), true, this.l, null, null, G80.k(new JSONObject(), Jd0.t2, Long.valueOf(Ix1.a.b(bArr, Ix1.Ai.CRC32))), 384));
        Ix.b.b(this.d.x, bArr);
    }

    @DexIgnore
    @Override // com.fossil.Br1
    public boolean a() {
        Ky1 ky1 = Ky1.DEBUG;
        if (isActive()) {
            Zw.i.j(this);
        }
        D90.i.d(new A90(Ey1.a(I00.c), V80.e, this.d.x, Ey1.a(I00.c), E.a("UUID.randomUUID().toString()"), true, this.l, null, null, null, 896));
        C0();
        G0();
        I0(false);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Os1
    public Yb0<Cd6> a0(Md0 md0) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        md0.toJSONString(2);
        Ef ef = new Ef(this.d, this.b, md0);
        ef.v(new C50(this));
        synchronized (this.m) {
            U40 d0 = d0(ef);
            yb0 = new Yb0<>();
            if (d0 != null) {
                ef.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new X0(this, ef));
                Q1 q1 = new Q1(this, ef);
                if (!ef.t) {
                    ef.d.add(q1);
                }
                B4 b4 = new B4(yb0, this, ef);
                if (!ef.t) {
                    ef.h.add(b4);
                }
                ef.a(new K40(yb0, this, ef));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ef.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Ps1
    public Zb0<byte[]> b(byte[] bArr) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Dy1.e(bArr, null, 1, null);
        Hf hf = new Hf(this.d, this.b, Rt.c, bArr);
        synchronized (this.m) {
            U40 d0 = d0(hf);
            yb0 = new Yb0();
            if (d0 != null) {
                hf.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new P1(this, hf));
                J3 j3 = new J3(this, hf);
                if (!hf.t) {
                    hf.d.add(j3);
                }
                H8 h8 = new H8(yb0, this, hf);
                if (!hf.t) {
                    hf.h.add(h8);
                }
                hf.a(new V70(yb0, this, hf));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                hf.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Nr1
    public Zb0<Cd6> c() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Mq mq = new Mq(this.d, this.b);
        synchronized (this.m) {
            U40 d0 = d0(mq);
            yb0 = new Yb0();
            if (d0 != null) {
                mq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Lc(this, mq));
                Md md = new Md(this, mq);
                if (!mq.t) {
                    mq.d.add(md);
                }
                Fx fx = new Fx(yb0, this, mq);
                if (!mq.t) {
                    mq.h.add(fx);
                }
                mq.a(new G1(yb0, this, mq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                mq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Qb0
    public Yb0<Cd6> c0(Zd0[] zd0Arr) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(zd0Arr).toString(2);
        Pr pr = new Pr(this.d, this.b, zd0Arr, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(pr);
            yb0 = new Yb0<>();
            if (d0 != null) {
                pr.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Ry(this, pr));
                Iz iz = new Iz(this, pr);
                if (!pr.t) {
                    pr.d.add(iz);
                }
                M00 m00 = new M00(yb0, this, pr);
                if (!pr.t) {
                    pr.h.add(m00);
                }
                pr.a(new Xw(yb0, this, pr));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                pr.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Na0
    public Yb0<Cd6> cleanUp() {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Gm gm = new Gm(this.d, this.b);
        synchronized (this.m) {
            U40 d0 = d0(gm);
            yb0 = new Yb0<>();
            if (d0 != null) {
                gm.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new By(this, gm));
                Ty ty = new Ty(this, gm);
                if (!gm.t) {
                    gm.d.add(ty);
                }
                A00 a00 = new A00(yb0, this, gm);
                if (!gm.t) {
                    gm.h.add(a00);
                }
                gm.a(new Yc(yb0, this, gm));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                gm.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Vq1
    public Zb0<Cd6> d(long j2) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Pq pq = new Pq(this.d, this.b, j2);
        synchronized (this.m) {
            U40 d0 = d0(pq);
            yb0 = new Yb0();
            if (d0 != null) {
                pq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new P40(this, pq));
                H50 h50 = new H50(this, pq);
                if (!pq.t) {
                    pq.d.add(h50);
                }
                S60 s60 = new S60(yb0, this, pq);
                if (!pq.t) {
                    pq.h.add(s60);
                }
                pq.a(new F10(yb0, this, pq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                pq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final U40 d0(Lp lp) {
        T t2;
        boolean z2;
        if (BluetoothLeAdapter.k.w() != BluetoothLeAdapter.Ci.ENABLED) {
            return U40.BLUETOOTH_OFF;
        }
        int i2 = L10.e[lp.y.ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3 || i2 == 4) {
            return null;
        }
        if (i2 != 5) {
            return this.v != Q40.Ci.UPGRADING_FIRMWARE ? null : U40.DEVICE_BUSY;
        }
        ArrayList<Lp> a2 = this.g.a();
        if (a2.isEmpty()) {
            return null;
        }
        Iterator<T> it = a2.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            T t3 = t2;
            Yp yp = t3.y;
            if (yp == Yp.g || yp == Yp.c || t3.y().compareTo(A9.b) <= 0) {
                z2 = false;
                continue;
            } else {
                z2 = true;
                continue;
            }
            if (z2) {
                break;
            }
        }
        if (t2 != null) {
            return U40.DEVICE_BUSY;
        }
        return null;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Hs1
    public Yb0<String> e(Pu1 pu1) {
        Yb0<String> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        pu1.toJSONString(2);
        Or or = new Or(this.d, this.b, pu1);
        or.v(new A30(this));
        synchronized (this.m) {
            U40 d0 = d0(or);
            yb0 = new Yb0<>();
            if (d0 != null) {
                or.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new L00(this, or));
                Z00 z00 = new Z00(this, or);
                if (!or.t) {
                    or.d.add(z00);
                }
                B20 b20 = new B20(yb0, this, or);
                if (!or.t) {
                    or.h.add(b20);
                }
                or.a(new K20(yb0, this, or));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                or.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Gs1
    public Yb0<Cd6> f(boolean z2) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Aq aq = new Aq(this.d, this.b, z2, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(aq);
            yb0 = new Yb0<>();
            if (d0 != null) {
                aq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new F50(this, aq));
                X50 x50 = new X50(this, aq);
                if (!aq.t) {
                    aq.d.add(x50);
                }
                G70 g70 = new G70(yb0, this, aq);
                if (!aq.t) {
                    aq.h.add(g70);
                }
                aq.a(new G00(yb0, this, aq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                aq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Yb0<Cd6> f0(Tc0 tc0) {
        Ob ob;
        Yb0<Cd6> yb0;
        if ((tc0 instanceof It1) || (tc0 instanceof Xt1) || (tc0 instanceof Tt1) || (tc0 instanceof Yt1) || (tc0 instanceof Lt1) || (tc0 instanceof Gt1) || (tc0 instanceof Ht1) || (tc0 instanceof Ot1) || (tc0 instanceof Ut1) || (tc0 instanceof Rt1) || (tc0 instanceof Qt1) || (tc0 instanceof Vt1) || (tc0 instanceof Wt1) || (tc0 instanceof Zt1) || (tc0 instanceof Au1)) {
            ob = Ob.i;
        } else if (!(tc0 instanceof St1) && !(tc0 instanceof Kt1) && !(tc0 instanceof Jt1) && !(tc0 instanceof Pt1)) {
            Yb0<Cd6> yb02 = new Yb0<>();
            yb02.n(new Bl1(U40.INVALID_PARAMETERS, null, 2));
            return yb02;
        } else {
            ob = Ob.j;
        }
        Cn cn = new Cn(this.d, this.b, tc0, ob, null, 16);
        synchronized (this.m) {
            U40 d0 = d0(cn);
            yb0 = new Yb0<>();
            if (d0 != null) {
                cn.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new V60(this, cn));
                N70 n70 = new N70(this, cn);
                if (!cn.t) {
                    cn.d.add(n70);
                }
                B1 b1 = new B1(yb0, this, cn);
                if (!cn.t) {
                    cn.h.add(b1);
                }
                cn.a(new D3(yb0, this, cn));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cn.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Rs1
    public Yb0<FitnessData[]> g(H60 h60) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        h60.toJSONString(2);
        Rq rq = new Rq(this.d, this.b, h60, new HashMap());
        synchronized (this.m) {
            U40 d0 = d0(rq);
            yb0 = new Yb0();
            if (d0 != null) {
                rq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new O00(this, rq));
                D10 d10 = new D10(this, rq);
                if (!rq.t) {
                    rq.d.add(d10);
                }
                F20 f20 = new F20(yb0, this, rq);
                if (!rq.t) {
                    rq.h.add(f20);
                }
                rq.a(new Y10(yb0, this, rq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                rq.F();
            }
        }
        return yb0.t(C30.b);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.E60 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.mapped.Q40
    public <T> T g0(Class<T> cls) {
        if (Em7.B(this.u.getDeviceType().a(), cls)) {
            Ky1 ky1 = Ky1.DEBUG;
            cls.getSimpleName();
            return this;
        }
        Ky1 ky12 = Ky1.DEBUG;
        cls.getSimpleName();
        return null;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public Q40.Ci getState() {
        return this.v;
    }

    @DexIgnore
    @Override // com.fossil.Js1
    public Yb0<Cd6> h(Gv1 gv1) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        gv1.toJSONString(2);
        Np np = new Np(this.d, this.b, gv1, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(np);
            yb0 = new Yb0<>();
            if (d0 != null) {
                np.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Q20(this, np));
                G30 g30 = new G30(this, np);
                if (!np.t) {
                    np.d.add(g30);
                }
                O40 o40 = new O40(yb0, this, np);
                if (!np.t) {
                    np.h.add(o40);
                }
                np.a(new E8(yb0, this, np));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                np.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Yb0<byte[][]> h0(boolean z2, boolean z3, A9 a9) {
        Lp yl;
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        if (z3) {
            yl = new Ml(this.d, this.b, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.FALSE), Hl7.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.TRUE)));
        } else {
            yl = new Yl(this.d, this.b, Yp.q0, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.valueOf(z2)), Hl7.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z2))), E.a("UUID.randomUUID().toString()"));
            yl.f(a9);
        }
        synchronized (this.m) {
            U40 d0 = d0(yl);
            yb0 = new Yb0();
            if (d0 != null) {
                yl.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new G50(this, yl));
                Jc jc = new Jc(this, yl);
                if (!yl.t) {
                    yl.d.add(jc);
                }
                H30 h30 = new H30(yb0, this, yl);
                if (!yl.t) {
                    yl.h.add(h30);
                }
                yl.a(new N50(yb0, this, yl));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                yl.F();
            }
        }
        return yb0.t(G60.b);
    }

    @DexIgnore
    @Override // com.fossil.Qr1
    public Zb0<Cd6> i(N70 n70) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        n70.toJSONString(2);
        Fi fi = new Fi(this.d, this.b, n70);
        synchronized (this.m) {
            U40 d0 = d0(fi);
            yb0 = new Yb0();
            if (d0 != null) {
                fi.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new O20(this, fi));
                E30 e30 = new E30(this, fi);
                if (!fi.t) {
                    fi.d.add(e30);
                }
                N40 n40 = new N40(yb0, this, fi);
                if (!fi.t) {
                    fi.h.add(n40);
                }
                fi.a(new D00(yb0, this, fi));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                fi.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public boolean isActive() {
        return Zw.i.f(this);
    }

    @DexIgnore
    public final Zb0<Cd6> j0() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        K5 k5 = this.d;
        Ji ji = new Ji(k5, this.b, Rt.d, Ix.b.a(k5.x).c(), E.a("UUID.randomUUID().toString()"));
        ji.v(new Y2(this));
        synchronized (this.m) {
            U40 d0 = d0(ji);
            yb0 = new Yb0();
            if (d0 != null) {
                ji.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Y50(this, ji));
                Q60 q60 = new Q60(this, ji);
                if (!ji.t) {
                    ji.d.add(q60);
                }
                Z70 z70 = new Z70(yb0, this, ji);
                if (!ji.t) {
                    ji.h.add(z70);
                }
                ji.a(new D1(yb0, this, ji));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ji.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Wa0
    public Yb0<Lw1> k(Lw1 lw1) {
        Yb0<Lw1> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        lw1.toJSONString(2);
        Bg bg = new Bg(this.d, this.b, lw1);
        synchronized (this.m) {
            U40 d0 = d0(bg);
            yb0 = new Yb0<>();
            if (d0 != null) {
                bg.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new E20(this, bg));
                T20 t20 = new T20(this, bg);
                if (!bg.t) {
                    bg.d.add(t20);
                }
                Z30 z30 = new Z30(yb0, this, bg);
                if (!bg.t) {
                    bg.h.add(z30);
                }
                bg.a(new V30(yb0, this, bg));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                bg.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Zb0<Cd6> k0(C2 c2) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        c2.toJSONString(2);
        Em em = new Em(this.d, this.b, c2);
        synchronized (this.m) {
            U40 d0 = d0(em);
            yb0 = new Yb0();
            if (d0 != null) {
                em.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Y30(this, em));
                Q40 q40 = new Q40(this, em);
                if (!em.t) {
                    em.d.add(q40);
                }
                J8 j8 = new J8(yb0, this, em);
                if (!em.t) {
                    em.h.add(j8);
                }
                em.a(new W10(yb0, this, em));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                em.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.mapped.Q40
    public Zb0<Cd6> l() {
        Ky1 ky1 = Ky1.DEBUG;
        D90.i.d(new A90(Ey1.a(I00.f), V80.i, this.d.x, "", "", true, null, null, null, null, 960));
        Zb0<Cd6> zb0 = new Zb0<>();
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new DeviceImplementation$clearCache$Anon1(this, zb0, null), 3, null);
        return zb0;
    }

    @DexIgnore
    public final Zb0<FitnessData[]> l0(H60 h60, SyncMode syncMode) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        syncMode.name();
        h60.toJSONString(2);
        Mi mi = new Mi(this.d, this.b, h60, null, null, syncMode, 24);
        synchronized (this.m) {
            U40 d0 = d0(mi);
            yb0 = new Yb0();
            if (d0 != null) {
                mi.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new De(this, mi));
                P20 p20 = new P20(this, mi);
                if (!mi.t) {
                    mi.d.add(p20);
                }
                W0 w0 = new W0(yb0, this, mi);
                if (!mi.t) {
                    mi.h.add(w0);
                }
                mi.a(new Tz(yb0, this, mi));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                mi.F();
            }
        }
        return yb0.t(M20.b);
    }

    @DexIgnore
    @Override // com.fossil.Er1
    public Yb0<Zk1> m() {
        Yb0<Zk1> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        if (this.v == Q40.Ci.CONNECTED) {
            Yb0<Zk1> yb02 = new Yb0<>();
            yb02.y(1.0f);
            yb02.o(this.u);
            return yb02;
        }
        Ah ah = new Ah(this.d, this.b, E.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            U40 d0 = d0(ah);
            yb0 = new Yb0<>();
            if (d0 != null) {
                ah.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new B60(this, ah));
                T60 t60 = new T60(this, ah);
                if (!ah.t) {
                    ah.d.add(t60);
                }
                D80 d80 = new D80(yb0, this, ah);
                if (!ah.t) {
                    ah.h.add(d80);
                }
                ah.a(new Z2(yb0, this, ah));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ah.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Zb0<Cd6> m0(HashMap<Dr, Object> hashMap) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Ni ni = new Ni(this.d, this.b, hashMap);
        synchronized (this.m) {
            U40 d0 = d0(ni);
            yb0 = new Yb0();
            if (d0 != null) {
                ni.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new B10(this, ni));
                K70 k70 = new K70(this, ni);
                if (!ni.t) {
                    ni.d.add(k70);
                }
                Jz jz = new Jz(yb0, this, ni);
                if (!ni.t) {
                    ni.h.add(jz);
                }
                ni.a(new W40(yb0, this, ni));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ni.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.As1
    public Yb0<Cd6> n(Z40[] z40Arr) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(z40Arr).toString(2);
        Mo mo = new Mo(this.d, this.b, z40Arr, 0, null, 24);
        synchronized (this.m) {
            U40 d0 = d0(mo);
            yb0 = new Yb0<>();
            if (d0 != null) {
                mo.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new E70(this, mo));
                W70 w70 = new W70(this, mo);
                if (!mo.t) {
                    mo.d.add(w70);
                }
                N1 n1 = new N1(yb0, this, mo);
                if (!mo.t) {
                    mo.h.add(n1);
                }
                mo.a(new T00(yb0, this, mo));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                mo.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void n0(com.fossil.Nr r5) {
        /*
            r4 = this;
            r1 = 1
            com.fossil.Zq r0 = r5.c
            com.fossil.Zq r2 = com.fossil.Zq.d
            if (r0 != r2) goto L_0x002a
            com.fossil.Mw r0 = r5.d
            com.fossil.Lw r2 = r0.d
            com.fossil.Lw r3 = com.fossil.Lw.d
            if (r2 != r3) goto L_0x002a
            com.fossil.S5 r0 = r0.e
            com.fossil.R5 r2 = r0.c
            com.fossil.R5 r3 = com.fossil.R5.d
            if (r2 != r3) goto L_0x002a
            com.fossil.G7 r0 = r0.d
            com.fossil.F7 r0 = r0.b
            com.fossil.F7 r2 = com.fossil.F7.c
            if (r0 == r2) goto L_0x0023
            com.fossil.F7 r2 = com.fossil.F7.f
            if (r0 != r2) goto L_0x002a
        L_0x0023:
            r0 = r1
        L_0x0024:
            if (r1 != r0) goto L_0x0029
            r4.C0()
        L_0x0029:
            return
        L_0x002a:
            r0 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.E60.n0(com.fossil.Nr):void");
    }

    @DexIgnore
    @Override // com.fossil.Or1
    public Zb0<Cd6> p(R50 r50, HandMovingConfig[] handMovingConfigArr) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(handMovingConfigArr).toString(2);
        K5 k5 = this.d;
        Cq cq = new Cq(k5, this.b, Yp.t, new Ou(k5, r50, handMovingConfigArr));
        synchronized (this.m) {
            U40 d0 = d0(cq);
            yb0 = new Yb0();
            if (d0 != null) {
                cq.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Mc(this, cq));
                Nd nd = new Nd(this, cq);
                if (!cq.t) {
                    cq.d.add(nd);
                }
                Gx gx = new Gx(yb0, this, cq);
                if (!cq.t) {
                    cq.h.add(gx);
                }
                cq.a(new Z7(yb0, this, cq));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cq.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Fs1
    public Yb0<Ry1> q(Ou1 ou1) {
        Yb0<Ry1> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        ou1.toJSONString(2);
        Ar ar = new Ar(this.d, this.b, ou1);
        ar.v(new Sx(this));
        synchronized (this.m) {
            U40 d0 = d0(ar);
            yb0 = new Yb0<>();
            if (d0 != null) {
                ar.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Ge(this, ar));
                Ex ex = new Ex(this, ar);
                if (!ar.t) {
                    ar.d.add(ex);
                }
                Sy sy = new Sy(yb0, this, ar);
                if (!ar.t) {
                    ar.h.add(sy);
                }
                ar.a(new Ww(yb0, this, ar));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                ar.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void q0(Q40.Ci ci) {
        Q40.Ci ci2;
        synchronized (this.v) {
            ci2 = this.v;
            this.v = ci;
            Cd6 cd6 = Cd6.a;
        }
        if (ci2 != ci) {
            D90.i.d(new A90(Ey1.a(I00.e), V80.i, this.d.x, "", "", true, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.C0, Ey1.a(ci2)), Jd0.B0, Ey1.a(ci)), 448));
            Q40.Ci ci3 = this.v;
            Ky1 ky1 = Ky1.DEBUG;
            Intent intent = new Intent();
            intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE", ci2);
            intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE", ci3);
            Context a2 = Id0.i.a();
            if (a2 != null) {
                Ct0.b(a2).d(intent);
            }
            Looper myLooper = Looper.myLooper();
            if (myLooper == null) {
                myLooper = Looper.getMainLooper();
            }
            if (myLooper != null) {
                new Handler(myLooper).post(new Ox(this, ci2, ci3));
                int i2 = L10.c[ci.ordinal()];
                if (i2 == 1) {
                    D0();
                    this.g.e(Zq.k, new Yp[]{Yp.g});
                    this.g.d(new Nw(new Hashtable(), null));
                    this.e = false;
                } else if (i2 == 2) {
                    D0();
                } else if (i2 == 3) {
                    Zk1 zk1 = this.u;
                    C90.e.d(zk1.getMacAddress(), zk1);
                    this.g.g();
                } else if (i2 == 5) {
                    D0();
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Wa0
    public Yb0<Lw1> r(ThemeEditor themeEditor) {
        Ky1 ky1 = Ky1.DEBUG;
        themeEditor.d();
        Yb0<Lw1> yb0 = new Yb0<>();
        Rm6 unused = Gu7.d(Id0.i.e(), null, null, new DeviceImplementation$applyTheme$Anon1(this, themeEditor, yb0, null), 3, null);
        return yb0;
    }

    @DexIgnore
    public final void r0(Q40.Di di, Q40.Di di2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE", this);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE", di);
        intent.putExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE", di2);
        Context a2 = Id0.i.a();
        if (a2 != null) {
            Ct0.b(a2).d(intent);
        }
    }

    @DexIgnore
    @Override // com.fossil.Wr1
    public Yb0<Cd6> s(Zn1 zn1) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        zn1.toJSONString(2);
        Gk gk = new Gk(this.d, this.b, zn1, 0, null, 24);
        synchronized (this.m) {
            U40 d0 = d0(gk);
            yb0 = new Yb0<>();
            if (d0 != null) {
                gk.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Ax(this, gk));
                Wx wx = new Wx(this, gk);
                if (!gk.t) {
                    gk.d.add(wx);
                }
                Fz fz = new Fz(yb0, this, gk);
                if (!gk.t) {
                    gk.h.add(fz);
                }
                gk.a(new Pz(yb0, this, gk));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                gk.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void s0(Mp1 mp1) {
        Q40.Bi bi = this.w;
        if (bi != null) {
            bi.onEventReceived(this, mp1);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs1
    public Zb0<Cd6> t() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Kh kh = new Kh(this.d, this.b, E.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            U40 d0 = d0(kh);
            yb0 = new Yb0();
            if (d0 != null) {
                kh.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Wz(this, kh));
                K00 k00 = new K00(this, kh);
                if (!kh.t) {
                    kh.d.add(k00);
                }
                N10 n10 = new N10(yb0, this, kh);
                if (!kh.t) {
                    kh.h.add(n10);
                }
                kh.a(new Gc(yb0, this, kh));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                kh.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void t0(Yx1 yx1) {
    }

    @DexIgnore
    @Override // com.fossil.Ur1
    public Zb0<Cd6> u() {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Zb0<Cd6> zb0 = new Zb0<>();
        Mk mk = new Mk(this.d, this.b, E.a("UUID.randomUUID().toString()"));
        synchronized (this.m) {
            U40 d0 = d0(mk);
            yb0 = new Yb0();
            if (d0 != null) {
                mk.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new J70(this, mk));
                B80 b80 = new B80(this, mk);
                if (!mk.t) {
                    mk.d.add(b80);
                }
                R1 r1 = new R1(yb0, this, mk);
                if (!mk.t) {
                    mk.h.add(r1);
                }
                mk.a(new Cd(yb0, this, mk));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                mk.F();
            }
        }
        yb0.x(new Wd(zb0)).v(new Uw(zb0));
        return zb0;
    }

    @DexIgnore
    public final void u0(WorkoutSessionManager workoutSessionManager) {
        this.t = workoutSessionManager;
    }

    @DexIgnore
    @Override // com.fossil.Ms1
    public Yb0<Ru1[]> v(Ru1[] ru1Arr) {
        Yb0<Ru1[]> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Px1.a(ru1Arr).toString(2);
        Il il = new Il(this.d, this.b, ru1Arr);
        synchronized (this.m) {
            U40 d0 = d0(il);
            yb0 = new Yb0<>();
            if (d0 != null) {
                il.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Q10(this, il));
                D20 d20 = new D20(this, il);
                if (!il.t) {
                    il.d.add(d20);
                }
                I30 i30 = new I30(yb0, this, il);
                if (!il.t) {
                    il.h.add(i30);
                }
                il.a(new Sz(yb0, this, il));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                il.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final void v0(boolean z2) {
        synchronized (Boolean.valueOf(this.f)) {
            this.f = z2;
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ts1
    public Zb0<Boolean> w(byte[] bArr) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        Ix1.a.b(bArr, Ix1.Ai.CRC32);
        Lg lg = new Lg(this.d, this.b, bArr);
        synchronized (this.m) {
            U40 d0 = d0(lg);
            yb0 = new Yb0();
            if (d0 != null) {
                lg.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new Yx(this, lg));
                Qy qy = new Qy(this, lg);
                if (!lg.t) {
                    lg.d.add(qy);
                }
                Xz xz = new Xz(yb0, this, lg);
                if (!lg.t) {
                    lg.h.add(xz);
                }
                lg.a(new L50(yb0, this, lg));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                lg.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.y, i2);
        }
        if (parcel != null) {
            parcel.writeString(this.z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Yr1
    public Yb0<Cd6> x(byte[] bArr) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        String str = "sendEncryptedData invoked: encryptedData=" + Dy1.e(bArr, null, 1, null) + '.';
        On on = new On(this.d, this.b, bArr, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(on);
            yb0 = new Yb0<>();
            if (d0 != null) {
                on.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new L3(this, on));
                C4 c4 = new C4(this, on);
                if (!on.t) {
                    on.d.add(c4);
                }
                Kc kc = new Kc(yb0, this, on);
                if (!on.t) {
                    on.h.add(kc);
                }
                on.a(new Dd(yb0, this, on));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                on.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    @Override // com.fossil.Xr1
    public Yb0<Cd6> y(Tc0 tc0) {
        Yb0<Cd6> yb0;
        Ky1 ky1 = Ky1.DEBUG;
        tc0.toJSONString(2);
        if (!(tc0 instanceof Bu1)) {
            return f0(tc0);
        }
        Do r0 = new Do(this.d, this.b, (Bu1) tc0, null, 8);
        synchronized (this.m) {
            U40 d0 = d0(r0);
            yb0 = new Yb0<>();
            if (d0 != null) {
                r0.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new L30(this, r0));
                B40 b40 = new B40(this, r0);
                if (!r0.t) {
                    r0.d.add(b40);
                }
                K50 k50 = new K50(yb0, this, r0);
                if (!r0.t) {
                    r0.h.add(k50);
                }
                r0.a(new B70(yb0, this, r0));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                r0.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Yb0<byte[][]> y0(boolean z2, boolean z3, A9 a9) {
        Lp cj;
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        if (z3) {
            cj = new Pi(this.d, this.b, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.FALSE), Hl7.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.FALSE)));
        } else {
            cj = new Cj(this.d, this.b, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.valueOf(z2)), Hl7.a(Hu1.SKIP_ERASE_CACHE_AFTER_SUCCESS, Boolean.valueOf(z2))), (String) null, 8);
            cj.f(a9);
        }
        synchronized (this.m) {
            U40 d0 = d0(cj);
            yb0 = new Yb0();
            if (d0 != null) {
                cj.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new E80(this, cj));
                Cy cy = new Cy(this, cj);
                if (!cj.t) {
                    cj.d.add(cy);
                }
                D60 d60 = new D60(yb0, this, cj);
                if (!cj.t) {
                    cj.h.add(d60);
                }
                cj.a(new Rw(yb0, this, cj));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                cj.F();
            }
        }
        return yb0.t(Nx.b);
    }

    @DexIgnore
    @Override // com.fossil.Pr1
    public Zb0<Cd6> z(Ho1 ho1) {
        Yb0 yb0;
        Ky1 ky1 = Ky1.DEBUG;
        ho1.toJSONString(2);
        Sh sh = new Sh(this.d, this.b, ho1);
        synchronized (this.m) {
            U40 d0 = d0(sh);
            yb0 = new Yb0();
            if (d0 != null) {
                sh.k(L10.d[d0.ordinal()] != 1 ? Zq.B : Zq.w);
                yb0.n(new Bl1(d0, null, 2));
            } else {
                yb0.u(new A10(this, sh));
                P10 p10 = new P10(this, sh);
                if (!sh.t) {
                    sh.d.add(p10);
                }
                R20 r20 = new R20(yb0, this, sh);
                if (!sh.t) {
                    sh.h.add(r20);
                }
                sh.a(new Sw(yb0, this, sh));
                if (this.v != Q40.Ci.CONNECTED && !this.e) {
                    z0(Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L)));
                }
                sh.F();
            }
        }
        return yb0;
    }

    @DexIgnore
    public final Promise<Zk1, Nr> z0(HashMap<Ym, Object> hashMap) {
        Ky1 ky1 = Ky1.DEBUG;
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        Promise<Zk1, Nr> promise = new Promise<>();
        promise.k(new N0(jh6));
        if (this.v == Q40.Ci.CONNECTED) {
            promise.o(this.u);
        } else if (!this.e) {
            this.l = E.a("UUID.randomUUID().toString()");
            C90.e.e(this.u.getMacAddress(), this.l);
            T t2 = (T) new Fh(this.d, this.b, hashMap, E.a("UUID.randomUUID().toString()"));
            jh6.element = t2;
            T t3 = t2;
            H60 h60 = new H60(this);
            if (!t3.t) {
                t3.d.add(h60);
            }
            t3.v(new Y60(this, promise));
            t3.s(new Q70(this, promise));
            jh6.element.F();
        } else {
            Ky1 ky12 = Ky1.DEBUG;
            promise.n(new Nr(Yp.c, Zq.B, null, null, 12));
        }
        return promise;
    }
}
