package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv2 {
    @DexIgnore
    public static volatile Tw2<Boolean> a; // = Tw2.zzc();
    @DexIgnore
    public static /* final */ Object b; // = new Object();

    @DexIgnore
    public static boolean a(Context context) {
        try {
            return (context.getPackageManager().getApplicationInfo("com.google.android.gms", 0).flags & 129) != 0;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @DexIgnore
    public static boolean b(Context context, Uri uri) {
        ProviderInfo resolveContentProvider;
        boolean z = false;
        String authority = uri.getAuthority();
        if (!"com.google.android.gms.phenotype".equals(authority)) {
            StringBuilder sb = new StringBuilder(String.valueOf(authority).length() + 91);
            sb.append(authority);
            sb.append(" is an unsupported authority. Only com.google.android.gms.phenotype authority is supported.");
            Log.e("PhenotypeClientHelper", sb.toString());
            return false;
        } else if (a.zza()) {
            return a.zzb().booleanValue();
        } else {
            synchronized (b) {
                if (a.zza()) {
                    return a.zzb().booleanValue();
                }
                if (("com.google.android.gms".equals(context.getPackageName()) || ((resolveContentProvider = context.getPackageManager().resolveContentProvider("com.google.android.gms.phenotype", 0)) != null && "com.google.android.gms".equals(resolveContentProvider.packageName))) && a(context)) {
                    z = true;
                }
                a = Tw2.zza(Boolean.valueOf(z));
                return a.zzb().booleanValue();
            }
        }
    }
}
