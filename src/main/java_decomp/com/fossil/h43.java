package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H43 extends I43 {
    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 160
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    @DexIgnore
    @Override // com.fossil.I43
    public final int a(int r10, byte[] r11, int r12, int r13) {
        /*
            r9 = this;
            r1 = 0
            r8 = -32
            r7 = -96
            r3 = -1
            r6 = -65
            r0 = r12
        L_0x0009:
            if (r0 >= r13) goto L_0x0012
            byte r2 = r11[r0]
            if (r2 < 0) goto L_0x0012
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0012:
            if (r0 < r13) goto L_0x007e
            r0 = r1
        L_0x0015:
            return r0
        L_0x0016:
            r2 = r0
        L_0x0017:
            if (r2 < r13) goto L_0x001b
            r0 = r1
            goto L_0x0015
        L_0x001b:
            int r0 = r2 + 1
            byte r2 = r11[r2]
            if (r2 >= 0) goto L_0x0016
            if (r2 >= r8) goto L_0x0033
            if (r0 < r13) goto L_0x0027
            r0 = r2
            goto L_0x0015
        L_0x0027:
            r4 = -62
            if (r2 < r4) goto L_0x0031
            int r2 = r0 + 1
            byte r0 = r11[r0]
            if (r0 <= r6) goto L_0x0017
        L_0x0031:
            r0 = r3
            goto L_0x0015
        L_0x0033:
            r4 = -16
            if (r2 >= r4) goto L_0x0058
            int r4 = r13 + -1
            if (r0 < r4) goto L_0x0040
            int r0 = com.fossil.G43.l(r11, r0, r13)
            goto L_0x0015
        L_0x0040:
            int r4 = r0 + 1
            byte r0 = r11[r0]
            if (r0 > r6) goto L_0x0056
            if (r2 != r8) goto L_0x004a
            if (r0 < r7) goto L_0x0056
        L_0x004a:
            r5 = -19
            if (r2 != r5) goto L_0x0050
            if (r0 >= r7) goto L_0x0056
        L_0x0050:
            int r0 = r4 + 1
            byte r2 = r11[r4]
            if (r2 <= r6) goto L_0x007e
        L_0x0056:
            r0 = r3
            goto L_0x0015
        L_0x0058:
            int r4 = r13 + -2
            if (r0 < r4) goto L_0x0061
            int r0 = com.fossil.G43.l(r11, r0, r13)
            goto L_0x0015
        L_0x0061:
            int r4 = r0 + 1
            byte r0 = r11[r0]
            if (r0 > r6) goto L_0x007c
            int r2 = r2 << 28
            int r0 = r0 + 112
            int r0 = r0 + r2
            int r0 = r0 >> 30
            if (r0 != 0) goto L_0x007c
            int r2 = r4 + 1
            byte r0 = r11[r4]
            if (r0 > r6) goto L_0x007c
            int r0 = r2 + 1
            byte r2 = r11[r2]
            if (r2 <= r6) goto L_0x0016
        L_0x007c:
            r0 = r3
            goto L_0x0015
        L_0x007e:
            r2 = r0
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H43.a(int, byte[], int, int):int");
    }

    @DexIgnore
    @Override // com.fossil.I43
    public final int b(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        char charAt;
        int length = charSequence.length();
        int i5 = i2 + i;
        int i6 = 0;
        while (i6 < length) {
            int i7 = i6 + i;
            if (i7 >= i5 || (charAt = charSequence.charAt(i6)) >= '\u0080') {
                break;
            }
            bArr[i7] = (byte) ((byte) charAt);
            i6++;
        }
        if (i6 == length) {
            return i + length;
        }
        int i8 = i + i6;
        while (i6 < length) {
            char charAt2 = charSequence.charAt(i6);
            if (charAt2 < '\u0080' && i8 < i5) {
                i4 = i8 + 1;
                bArr[i8] = (byte) ((byte) charAt2);
            } else if (charAt2 < '\u0800' && i8 <= i5 - 2) {
                int i9 = i8 + 1;
                bArr[i8] = (byte) ((byte) ((charAt2 >>> 6) | 960));
                i4 = i9 + 1;
                bArr[i9] = (byte) ((byte) ((charAt2 & '?') | 128));
            } else if ((charAt2 < '\ud800' || '\udfff' < charAt2) && i8 <= i5 - 3) {
                int i10 = i8 + 1;
                bArr[i8] = (byte) ((byte) ((charAt2 >>> '\f') | 480));
                int i11 = i10 + 1;
                bArr[i10] = (byte) ((byte) (((charAt2 >>> 6) & 63) | 128));
                i4 = i11 + 1;
                bArr[i11] = (byte) ((byte) ((charAt2 & '?') | 128));
            } else if (i8 <= i5 - 4) {
                int i12 = i6 + 1;
                if (i12 != charSequence.length()) {
                    char charAt3 = charSequence.charAt(i12);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        int i13 = i8 + 1;
                        bArr[i8] = (byte) ((byte) ((codePoint >>> 18) | 240));
                        int i14 = i13 + 1;
                        bArr[i13] = (byte) ((byte) (((codePoint >>> 12) & 63) | 128));
                        int i15 = i14 + 1;
                        bArr[i14] = (byte) ((byte) (((codePoint >>> 6) & 63) | 128));
                        i4 = i15 + 1;
                        bArr[i15] = (byte) ((byte) ((codePoint & 63) | 128));
                        i6 = i12;
                    }
                } else {
                    i12 = i6;
                }
                throw new K43(i12 - 1, length);
            } else if ('\ud800' > charAt2 || charAt2 > '\udfff' || ((i3 = i6 + 1) != charSequence.length() && Character.isSurrogatePair(charAt2, charSequence.charAt(i3)))) {
                StringBuilder sb = new StringBuilder(37);
                sb.append("Failed writing ");
                sb.append(charAt2);
                sb.append(" at index ");
                sb.append(i8);
                throw new ArrayIndexOutOfBoundsException(sb.toString());
            } else {
                throw new K43(i6, length);
            }
            i6++;
            i8 = i4;
        }
        return i8;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: CFG modification limit reached, blocks count: 143
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.processBlocksTree(BlockProcessor.java:72)
        	at jadx.core.dex.visitors.blocksmaker.BlockProcessor.visit(BlockProcessor.java:46)
        */
    @DexIgnore
    @Override // com.fossil.I43
    public final java.lang.String c(byte[] r10, int r11, int r12) throws com.fossil.L13 {
        /*
            r9 = this;
            r6 = 0
            r0 = r11 | r12
            int r1 = r10.length
            int r1 = r1 - r11
            int r1 = r1 - r12
            r0 = r0 | r1
            if (r0 < 0) goto L_0x009e
            int r7 = r11 + r12
            char[] r4 = new char[r12]
            r5 = r6
            r0 = r11
        L_0x000f:
            if (r0 >= r7) goto L_0x0022
            byte r1 = r10[r0]
            boolean r2 = com.fossil.F43.e(r1)
            if (r2 == 0) goto L_0x0022
            int r0 = r0 + 1
            com.fossil.F43.d(r1, r4, r5)
            int r5 = r5 + 1
            goto L_0x000f
        L_0x0021:
            r5 = r2
        L_0x0022:
            if (r0 >= r7) goto L_0x0098
            int r1 = r0 + 1
            byte r0 = r10[r0]
            boolean r2 = com.fossil.F43.e(r0)
            if (r2 == 0) goto L_0x0047
            int r2 = r5 + 1
            com.fossil.F43.d(r0, r4, r5)
            r0 = r1
        L_0x0034:
            if (r0 >= r7) goto L_0x0021
            byte r1 = r10[r0]
            boolean r3 = com.fossil.F43.e(r1)
            if (r3 == 0) goto L_0x0021
            int r0 = r0 + 1
            com.fossil.F43.d(r1, r4, r2)
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0034
        L_0x0047:
            boolean r2 = com.fossil.F43.j(r0)
            if (r2 == 0) goto L_0x005e
            if (r1 >= r7) goto L_0x0059
            byte r2 = r10[r1]
            com.fossil.F43.c(r0, r2, r4, r5)
            int r0 = r1 + 1
            int r5 = r5 + 1
            goto L_0x0022
        L_0x0059:
            com.fossil.L13 r0 = com.fossil.L13.zzh()
            throw r0
        L_0x005e:
            boolean r2 = com.fossil.F43.k(r0)
            if (r2 == 0) goto L_0x007b
            int r2 = r7 + -1
            if (r1 >= r2) goto L_0x0076
            int r2 = r1 + 1
            byte r1 = r10[r1]
            byte r3 = r10[r2]
            com.fossil.F43.b(r0, r1, r3, r4, r5)
            int r0 = r2 + 1
            int r5 = r5 + 1
            goto L_0x0022
        L_0x0076:
            com.fossil.L13 r0 = com.fossil.L13.zzh()
            throw r0
        L_0x007b:
            int r2 = r7 + -2
            if (r1 >= r2) goto L_0x0093
            int r2 = r1 + 1
            byte r1 = r10[r1]
            int r8 = r2 + 1
            byte r2 = r10[r2]
            byte r3 = r10[r8]
            com.fossil.F43.a(r0, r1, r2, r3, r4, r5)
            int r0 = r8 + 1
            int r1 = r5 + 1
            int r5 = r1 + 1
            goto L_0x0022
        L_0x0093:
            com.fossil.L13 r0 = com.fossil.L13.zzh()
            throw r0
        L_0x0098:
            java.lang.String r0 = new java.lang.String
            r0.<init>(r4, r6, r5)
            return r0
        L_0x009e:
            java.lang.ArrayIndexOutOfBoundsException r0 = new java.lang.ArrayIndexOutOfBoundsException
            java.lang.String r1 = "buffer length=%d, index=%d, size=%d"
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]
            int r3 = r10.length
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r6] = r3
            r3 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r11)
            r2[r3] = r4
            r3 = 2
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)
            r2[r3] = r4
            java.lang.String r1 = java.lang.String.format(r1, r2)
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H43.c(byte[], int, int):java.lang.String");
    }
}
