package com.fossil;

import com.google.firebase.crashlytics.CrashlyticsRegistrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class U74 implements D74 {
    @DexIgnore
    public /* final */ CrashlyticsRegistrar a;

    @DexIgnore
    public U74(CrashlyticsRegistrar crashlyticsRegistrar) {
        this.a = crashlyticsRegistrar;
    }

    @DexIgnore
    public static D74 b(CrashlyticsRegistrar crashlyticsRegistrar) {
        return new U74(crashlyticsRegistrar);
    }

    @DexIgnore
    @Override // com.fossil.D74
    public Object a(B74 b74) {
        return this.a.b(b74);
    }
}
