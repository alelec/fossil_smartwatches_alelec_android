package com.fossil;

import com.fossil.Yg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ng4 extends Yg4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Yg4.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.Yg4.Ai
        public Yg4 a() {
            String str = "";
            if (this.a == null) {
                str = " token";
            }
            if (this.b == null) {
                str = str + " tokenExpirationTimestamp";
            }
            if (this.c == null) {
                str = str + " tokenCreationTimestamp";
            }
            if (str.isEmpty()) {
                return new Ng4(this.a, this.b.longValue(), this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Yg4.Ai
        public Yg4.Ai b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null token");
        }

        @DexIgnore
        @Override // com.fossil.Yg4.Ai
        public Yg4.Ai c(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Yg4.Ai
        public Yg4.Ai d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public Ng4(String str, long j, long j2) {
        this.a = str;
        this.b = j;
        this.c = j2;
    }

    @DexIgnore
    @Override // com.fossil.Yg4
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Yg4
    public long c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Yg4
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Yg4)) {
            return false;
        }
        Yg4 yg4 = (Yg4) obj;
        return this.a.equals(yg4.b()) && this.b == yg4.d() && this.c == yg4.c();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        long j = this.b;
        int i = (int) (j ^ (j >>> 32));
        long j2 = this.c;
        return ((((hashCode ^ 1000003) * 1000003) ^ i) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "InstallationTokenResult{token=" + this.a + ", tokenExpirationTimestamp=" + this.b + ", tokenCreationTimestamp=" + this.c + "}";
    }
}
