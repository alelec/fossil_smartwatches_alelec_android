package com.fossil;

import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qn0 {
    @DexIgnore
    public static /* final */ Object a; // = new Object();
    @DexIgnore
    public static char[] b; // = new char[24];

    @DexIgnore
    public static int a(int i, int i2, boolean z, int i3) {
        if (i > 99 || (z && i3 >= 3)) {
            return i2 + 3;
        }
        if (i > 9 || (z && i3 >= 2)) {
            return i2 + 2;
        }
        if (z || i > 0) {
            return i2 + 1;
        }
        return 0;
    }

    @DexIgnore
    public static void b(long j, long j2, PrintWriter printWriter) {
        if (j == 0) {
            printWriter.print("--");
        } else {
            d(j - j2, printWriter, 0);
        }
    }

    @DexIgnore
    public static void c(long j, PrintWriter printWriter) {
        d(j, printWriter, 0);
    }

    @DexIgnore
    public static void d(long j, PrintWriter printWriter, int i) {
        synchronized (a) {
            printWriter.print(new String(b, 0, e(j, i)));
        }
    }

    @DexIgnore
    public static int e(long j, int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        if (b.length < i) {
            b = new char[i];
        }
        char[] cArr = b;
        int i8 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i8 == 0) {
            while (i - 1 > 0) {
                cArr[0] = (char) 32;
            }
            cArr[0] = (char) 48;
            return 1;
        }
        if (i8 > 0) {
            i2 = 43;
        } else {
            j = -j;
            i2 = 45;
        }
        int i9 = (int) (j % 1000);
        int floor = (int) Math.floor((double) (j / 1000));
        if (floor > 86400) {
            i3 = floor / 86400;
            floor -= 86400 * i3;
        } else {
            i3 = 0;
        }
        if (floor > 3600) {
            int i10 = floor / 3600;
            floor -= i10 * 3600;
            i4 = i10;
        } else {
            i4 = 0;
        }
        if (floor > 60) {
            int i11 = floor / 60;
            i5 = floor - (i11 * 60);
            i6 = i11;
        } else {
            i5 = floor;
            i6 = 0;
        }
        if (i != 0) {
            int a2 = a(i3, 1, false, 0);
            int a3 = a2 + a(i4, 1, a2 > 0, 2);
            int a4 = a3 + a(i6, 1, a3 > 0, 2);
            int a5 = a4 + a(i5, 1, a4 > 0, 2);
            i7 = 0;
            for (int a6 = a5 + a(i9, 2, true, a5 > 0 ? 3 : 0) + 1; a6 < i; a6++) {
                cArr[i7] = (char) 32;
                i7++;
            }
        } else {
            i7 = 0;
        }
        cArr[i7] = (char) i2;
        int i12 = i7 + 1;
        boolean z = i != 0;
        int f = f(cArr, i3, 'd', i12, false, 0);
        int f2 = f(cArr, i4, 'h', f, f != i12, z ? 2 : 0);
        int f3 = f(cArr, i6, 'm', f2, f2 != i12, z ? 2 : 0);
        int f4 = f(cArr, i5, 's', f3, f3 != i12, z ? 2 : 0);
        int f5 = f(cArr, i9, 'm', f4, true, (!z || f4 == i12) ? 0 : 3);
        cArr[f5] = (char) 115;
        return f5 + 1;
    }

    @DexIgnore
    public static int f(char[] cArr, int i, char c, int i2, boolean z, int i3) {
        int i4;
        int i5;
        if (!z && i <= 0) {
            return i2;
        }
        if ((!z || i3 < 3) && i <= 99) {
            i5 = i2;
            i4 = i;
        } else {
            int i6 = i / 100;
            cArr[i2] = (char) ((char) (i6 + 48));
            i5 = i2 + 1;
            i4 = i - (i6 * 100);
        }
        if ((z && i3 >= 2) || i4 > 9 || i2 != i5) {
            int i7 = i4 / 10;
            cArr[i5] = (char) ((char) (i7 + 48));
            i5++;
            i4 -= i7 * 10;
        }
        cArr[i5] = (char) ((char) (i4 + 48));
        int i8 = i5 + 1;
        cArr[i8] = (char) c;
        return i8 + 1;
    }
}
