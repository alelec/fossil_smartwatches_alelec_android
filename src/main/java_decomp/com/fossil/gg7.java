package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Gg7 {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    
    @DexIgnore
    public int a;

    @DexIgnore
    public Gg7(int i) {
        this.a = i;
    }

    @DexIgnore
    public static Gg7 getStatReportStrategy(int i) {
        Gg7[] values = values();
        for (Gg7 gg7 : values) {
            if (i == gg7.a()) {
                return gg7;
            }
        }
        return null;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
