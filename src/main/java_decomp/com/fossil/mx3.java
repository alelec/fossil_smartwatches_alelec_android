package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mx3 extends Ye0 {
    @DexIgnore
    public BottomSheetBehavior<FrameLayout> d;
    @DexIgnore
    public FrameLayout e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public BottomSheetBehavior.e j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnClickListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onClick(View view) {
            Mx3 mx3 = Mx3.this;
            if (mx3.g && mx3.isShowing() && Mx3.this.i()) {
                Mx3.this.cancel();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Rn0 {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void g(View view, Yo0 yo0) {
            super.g(view, yo0);
            if (Mx3.this.g) {
                yo0.a(1048576);
                yo0.i0(true);
                return;
            }
            yo0.i0(false);
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public boolean j(View view, int i, Bundle bundle) {
            if (i == 1048576) {
                Mx3 mx3 = Mx3.this;
                if (mx3.g) {
                    mx3.cancel();
                    return true;
                }
            }
            return super.j(view, i, bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements View.OnTouchListener {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends BottomSheetBehavior.e {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, float f) {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void b(View view, int i) {
            if (i == 5) {
                Mx3.this.cancel();
            }
        }
    }

    @DexIgnore
    public Mx3(Context context) {
        this(context, 0);
    }

    @DexIgnore
    public Mx3(Context context, int i2) {
        super(context, b(context, i2));
        this.g = true;
        this.h = true;
        this.j = new Di();
        d(1);
    }

    @DexIgnore
    public static int b(Context context, int i2) {
        if (i2 != 0) {
            return i2;
        }
        TypedValue typedValue = new TypedValue();
        return context.getTheme().resolveAttribute(Jw3.bottomSheetDialogTheme, typedValue, true) ? typedValue.resourceId : Sw3.Theme_Design_Light_BottomSheetDialog;
    }

    @DexIgnore
    public void cancel() {
        BottomSheetBehavior<FrameLayout> f2 = f();
        if (!this.f || f2.q() == 5) {
            super.cancel();
        } else {
            f2.E(5);
        }
    }

    @DexIgnore
    public final FrameLayout e() {
        if (this.e == null) {
            FrameLayout frameLayout = (FrameLayout) View.inflate(getContext(), Pw3.design_bottom_sheet_dialog, null);
            this.e = frameLayout;
            BottomSheetBehavior<FrameLayout> o = BottomSheetBehavior.o((FrameLayout) frameLayout.findViewById(Nw3.design_bottom_sheet));
            this.d = o;
            o.g(this.j);
            this.d.z(this.g);
        }
        return this.e;
    }

    @DexIgnore
    public BottomSheetBehavior<FrameLayout> f() {
        if (this.d == null) {
            e();
        }
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.f;
    }

    @DexIgnore
    public void h() {
        this.d.t(this.j);
    }

    @DexIgnore
    public boolean i() {
        if (!this.i) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(new int[]{16843611});
            this.h = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
            this.i = true;
        }
        return this.h;
    }

    @DexIgnore
    public final View j(int i2, View view, ViewGroup.LayoutParams layoutParams) {
        e();
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) this.e.findViewById(Nw3.coordinator);
        if (i2 != 0 && view == null) {
            view = getLayoutInflater().inflate(i2, (ViewGroup) coordinatorLayout, false);
        }
        FrameLayout frameLayout = (FrameLayout) this.e.findViewById(Nw3.design_bottom_sheet);
        if (layoutParams == null) {
            frameLayout.addView(view);
        } else {
            frameLayout.addView(view, layoutParams);
        }
        coordinatorLayout.findViewById(Nw3.touch_outside).setOnClickListener(new Ai());
        Mo0.l0(frameLayout, new Bi());
        frameLayout.setOnTouchListener(new Ci());
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ye0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Window window = getWindow();
        if (window != null) {
            if (Build.VERSION.SDK_INT >= 21) {
                window.clearFlags(67108864);
                window.addFlags(RecyclerView.UNDEFINED_DURATION);
            }
            window.setLayout(-1, -1);
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.d;
        if (bottomSheetBehavior != null && bottomSheetBehavior.q() == 5) {
            this.d.E(4);
        }
    }

    @DexIgnore
    public void setCancelable(boolean z) {
        super.setCancelable(z);
        if (this.g != z) {
            this.g = z;
            BottomSheetBehavior<FrameLayout> bottomSheetBehavior = this.d;
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.z(z);
            }
        }
    }

    @DexIgnore
    public void setCanceledOnTouchOutside(boolean z) {
        super.setCanceledOnTouchOutside(z);
        if (z && !this.g) {
            this.g = true;
        }
        this.h = z;
        this.i = true;
    }

    @DexIgnore
    @Override // com.fossil.Ye0, android.app.Dialog
    public void setContentView(int i2) {
        super.setContentView(j(i2, null, null));
    }

    @DexIgnore
    @Override // com.fossil.Ye0, android.app.Dialog
    public void setContentView(View view) {
        super.setContentView(j(0, view, null));
    }

    @DexIgnore
    @Override // com.fossil.Ye0
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        super.setContentView(j(0, view, layoutParams));
    }
}
