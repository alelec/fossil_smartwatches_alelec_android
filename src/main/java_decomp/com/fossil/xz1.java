package com.fossil;

import com.fossil.H02;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xz1 extends H02 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Vy1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends H02.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public byte[] b;
        @DexIgnore
        public Vy1 c;

        @DexIgnore
        @Override // com.fossil.H02.Ai
        public H02 a() {
            String str = "";
            if (this.a == null) {
                str = " backendName";
            }
            if (this.c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new Xz1(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.H02.Ai
        public H02.Ai b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null backendName");
        }

        @DexIgnore
        @Override // com.fossil.H02.Ai
        public H02.Ai c(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.H02.Ai
        public H02.Ai d(Vy1 vy1) {
            if (vy1 != null) {
                this.c = vy1;
                return this;
            }
            throw new NullPointerException("Null priority");
        }
    }

    @DexIgnore
    public Xz1(String str, byte[] bArr, Vy1 vy1) {
        this.a = str;
        this.b = bArr;
        this.c = vy1;
    }

    @DexIgnore
    @Override // com.fossil.H02
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.H02
    public byte[] c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.H02
    public Vy1 d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof H02)) {
            return false;
        }
        H02 h02 = (H02) obj;
        if (this.a.equals(h02.b())) {
            if (Arrays.equals(this.b, h02 instanceof Xz1 ? ((Xz1) h02).b : h02.c()) && this.c.equals(h02.d())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b)) * 1000003) ^ this.c.hashCode();
    }
}
