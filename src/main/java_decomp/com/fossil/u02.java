package com.fossil;

import com.fossil.P02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract U02 a();

        @DexIgnore
        public abstract Ai b(Iterable<C02> iterable);

        @DexIgnore
        public abstract Ai c(byte[] bArr);
    }

    @DexIgnore
    public static Ai a() {
        return new P02.Bi();
    }

    @DexIgnore
    public abstract Iterable<C02> b();

    @DexIgnore
    public abstract byte[] c();
}
