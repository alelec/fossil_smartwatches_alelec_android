package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oj5 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public Oj5(String str) {
        this.a = str.substring(0, 3);
    }

    @DexIgnore
    public List<ButtonMapping> a() {
        List<ButtonMapping> arrayList = new ArrayList<>();
        try {
            InputStream openRawResource = PortfolioApp.d0.getResources().openRawResource(2131820546);
            arrayList = b(openRawResource);
            openRawResource.close();
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public final List b(InputStream inputStream) throws XmlPullParserException, IOException {
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, null);
            return d(newPullParser);
        } finally {
            inputStream.close();
        }
    }

    @DexIgnore
    public final List c(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            String name = xmlPullParser.getName();
            if (eventType != 2) {
                if (eventType == 3 && name.equals(LegacyDeviceModel.COLUMN_DEVICE_MODEL)) {
                    break;
                }
            } else if (name.equals("microappmapping")) {
                arrayList.add(new ButtonMapping(xmlPullParser.getAttributeValue(null, "button"), xmlPullParser.getAttributeValue(null, "appid")));
            }
            eventType = xmlPullParser.next();
        }
        return arrayList;
    }

    @DexIgnore
    public final List d(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        String attributeValue;
        ArrayList arrayList = new ArrayList();
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType == 1) {
                break;
            }
            String name = xmlPullParser.getName();
            if (eventType == 2 && name.equals(LegacyDeviceModel.COLUMN_DEVICE_MODEL) && (attributeValue = xmlPullParser.getAttributeValue(null, "name")) != null && this.a.contains(attributeValue)) {
                arrayList.addAll(c(xmlPullParser));
                break;
            }
            eventType = xmlPullParser.next();
        }
        return arrayList;
    }
}
