package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nf7 extends Ef7 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public Nf7() {
    }

    @DexIgnore
    public Nf7(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Ef7
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxapi_sendauth_resp_token");
        this.d = bundle.getString("_wxapi_sendauth_resp_state");
        bundle.getString("_wxapi_sendauth_resp_url");
        bundle.getString("_wxapi_sendauth_resp_lang");
        bundle.getString("_wxapi_sendauth_resp_country");
    }

    @DexIgnore
    @Override // com.fossil.Ef7
    public int b() {
        return 1;
    }
}
