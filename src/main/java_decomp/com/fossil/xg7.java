package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Jg7 d;

    @DexIgnore
    public Xg7(Context context, String str, Jg7 jg7) {
        this.b = context;
        this.c = str;
        this.d = jg7;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            Ig7.u(this.b);
            synchronized (Ig7.k) {
                l = (Long) Ig7.k.remove(this.c);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                String str = Ig7.j;
                if (str != null && str.equals(this.c)) {
                    str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                Rg7 rg7 = new Rg7(this.b, str, this.c, Ig7.a(this.b, false, this.d), valueOf, this.d);
                if (!this.c.equals(Ig7.i)) {
                    Ig7.m.m("Invalid invocation since previous onResume on diff page.");
                }
                new Ch7(rg7).b();
                String unused = Ig7.j = this.c;
                return;
            }
            Th7 th7 = Ig7.m;
            th7.d("Starttime for PageID:" + this.c + " not found, lost onResume()?");
        } catch (Throwable th) {
            Ig7.m.e(th);
            Ig7.f(this.b, th);
        }
    }
}
