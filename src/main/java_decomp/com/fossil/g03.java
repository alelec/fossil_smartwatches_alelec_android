package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G03 implements C03 {
    @DexIgnore
    public G03() {
    }

    @DexIgnore
    public /* synthetic */ G03(Wz2 wz2) {
        this();
    }

    @DexIgnore
    @Override // com.fossil.C03
    public final byte[] a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
