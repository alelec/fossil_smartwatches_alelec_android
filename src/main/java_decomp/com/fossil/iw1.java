package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Wg6;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Iw1 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public /* final */ Ry1 c;
    @DexIgnore
    public /* final */ Yb0 d;
    @DexIgnore
    public /* final */ Cc0[] e;
    @DexIgnore
    public /* final */ Cc0[] f;
    @DexIgnore
    public /* final */ Cc0[] g;
    @DexIgnore
    public /* final */ Cc0[] h;
    @DexIgnore
    public /* final */ Cc0[] i;
    @DexIgnore
    public /* final */ Cc0[] j;
    @DexIgnore
    public /* final */ Cc0[] k;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Iw1(android.os.Parcel r12) {
        /*
            r11 = this;
            r10 = 0
            java.lang.Class<com.fossil.Ry1> r0 = com.fossil.Ry1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r1 = r12.readParcelable(r0)
            if (r1 == 0) goto L_0x00ab
            com.fossil.Ry1 r1 = (com.fossil.Ry1) r1
            java.lang.Class<com.fossil.Yb0> r0 = com.fossil.Yb0.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r2 = r12.readParcelable(r0)
            if (r2 == 0) goto L_0x00a7
            com.fossil.Yb0 r2 = (com.fossil.Yb0) r2
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r3 = r12.createTypedArray(r0)
            if (r3 == 0) goto L_0x00a3
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r3, r0)
            com.fossil.Cc0[] r3 = (com.fossil.Cc0[]) r3
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r4 = r12.createTypedArray(r0)
            if (r4 == 0) goto L_0x009f
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r4, r0)
            com.fossil.Cc0[] r4 = (com.fossil.Cc0[]) r4
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r5 = r12.createTypedArray(r0)
            if (r5 == 0) goto L_0x009b
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r5, r0)
            com.fossil.Cc0[] r5 = (com.fossil.Cc0[]) r5
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r6 = r12.createTypedArray(r0)
            if (r6 == 0) goto L_0x0097
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r6, r0)
            com.fossil.Cc0[] r6 = (com.fossil.Cc0[]) r6
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r7 = r12.createTypedArray(r0)
            if (r7 == 0) goto L_0x0093
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r7, r0)
            com.fossil.Cc0[] r7 = (com.fossil.Cc0[]) r7
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r8 = r12.createTypedArray(r0)
            if (r8 == 0) goto L_0x008f
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r8, r0)
            com.fossil.Cc0[] r8 = (com.fossil.Cc0[]) r8
            com.fossil.Bc0 r0 = com.fossil.Cc0.CREATOR
            java.lang.Object[] r9 = r12.createTypedArray(r0)
            if (r9 == 0) goto L_0x008b
            java.lang.String r0 = "parcel.createTypedArray(UIPackageNode.CREATOR)!!"
            com.mapped.Wg6.b(r9, r0)
            com.fossil.Cc0[] r9 = (com.fossil.Cc0[]) r9
            r0 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            return
        L_0x008b:
            com.mapped.Wg6.i()
            throw r10
        L_0x008f:
            com.mapped.Wg6.i()
            throw r10
        L_0x0093:
            com.mapped.Wg6.i()
            throw r10
        L_0x0097:
            com.mapped.Wg6.i()
            throw r10
        L_0x009b:
            com.mapped.Wg6.i()
            throw r10
        L_0x009f:
            com.mapped.Wg6.i()
            throw r10
        L_0x00a3:
            com.mapped.Wg6.i()
            throw r10
        L_0x00a7:
            com.mapped.Wg6.i()
            throw r10
        L_0x00ab:
            com.mapped.Wg6.i()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iw1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Iw1(Ry1 ry1, Yb0 yb0, Cc0[] cc0Arr, Cc0[] cc0Arr2, Cc0[] cc0Arr3, Cc0[] cc0Arr4, Cc0[] cc0Arr5, Cc0[] cc0Arr6, Cc0[] cc0Arr7) {
        this.c = ry1;
        this.d = yb0;
        this.e = cc0Arr;
        this.f = cc0Arr2;
        this.g = cc0Arr3;
        this.h = cc0Arr4;
        this.i = cc0Arr5;
        this.j = cc0Arr6;
        this.k = cc0Arr7;
    }

    @DexIgnore
    public final Cc0[] a() {
        return this.j;
    }

    @DexIgnore
    public final Cc0[] b() {
        return this.f;
    }

    @DexIgnore
    public final Cc0[] c() {
        return this.i;
    }

    @DexIgnore
    @Override // java.lang.Object
    public abstract Iw1 clone();

    @DexIgnore
    @Override // java.lang.Object
    public abstract /* synthetic */ Nx1 clone();

    @DexIgnore
    public final Cc0[] d() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Cc0[] e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Iw1)) {
            return false;
        }
        Iw1 iw1 = (Iw1) obj;
        if (!Wg6.a(this.c, iw1.c)) {
            return false;
        }
        if (!Wg6.a(this.d, iw1.d)) {
            return false;
        }
        if (!Arrays.equals(this.e, iw1.e)) {
            return false;
        }
        if (!Arrays.equals(this.f, iw1.f)) {
            return false;
        }
        if (!Arrays.equals(this.g, iw1.g)) {
            return false;
        }
        if (!Arrays.equals(this.h, iw1.h)) {
            return false;
        }
        if (!Arrays.equals(this.i, iw1.i)) {
            return false;
        }
        if (!Arrays.equals(this.j, iw1.j)) {
            return false;
        }
        return Arrays.equals(this.k, iw1.k);
    }

    @DexIgnore
    public final Cc0[] f() {
        return this.e;
    }

    @DexIgnore
    public final Yb0 g() {
        return this.d;
    }

    @DexIgnore
    public final String getBundleId() {
        return this.e[0].b;
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] bArr;
        if (this.b == null) {
            try {
                bArr = Ga.d.a(5630, this.c, this);
            } catch (Sx1 e2) {
                bArr = null;
            }
            this.b = bArr;
        }
        byte[] bArr2 = this.b;
        return bArr2 != null ? bArr2 : new byte[0];
    }

    @DexIgnore
    public final String getDisplayName() {
        Cc0 cc0;
        Cc0[] cc0Arr = this.i;
        int length = cc0Arr.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                cc0 = null;
                break;
            }
            Cc0 cc02 = cc0Arr[i2];
            if (Wg6.a(cc02.b, "display_name")) {
                cc0 = cc02;
                break;
            }
            i2++;
        }
        if (cc0 == null) {
            return null;
        }
        if (!(!(cc0.c.length == 0))) {
            return null;
        }
        byte[] bArr = cc0.c;
        return new String(Dm7.k(bArr, 0, bArr.length - 1), Hd0.y.c());
    }

    @DexIgnore
    public final long getPackageCrc() {
        if (getData().length < 4) {
            return 0;
        }
        ByteBuffer order = ByteBuffer.wrap(Dm7.k(getData(), getData().length - 4, getData().length)).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
        return Hy1.o(order.getInt());
    }

    @DexIgnore
    public final Jw1 getPackageType() {
        return this.d.b;
    }

    @DexIgnore
    public final Ry1 getPackageVersion() {
        return this.d.c;
    }

    @DexIgnore
    public final Ry1 h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + Arrays.hashCode(this.e)) * 31) + Arrays.hashCode(this.f)) * 31) + Arrays.hashCode(this.g)) * 31) + Arrays.hashCode(this.h)) * 31) + Arrays.hashCode(this.i)) * 31) + Arrays.hashCode(this.j)) * 31) + Arrays.hashCode(this.k);
    }

    @DexIgnore
    public final Cc0[] i() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.d(Gy1.d(Gy1.d(Gy1.d(Gy1.d(Gy1.d(Gy1.d(Gy1.d(new JSONObject(), Jd0.Q4, this.d.toJSONObject()), Jd0.R4, Px1.a(this.e)), Jd0.S4, Px1.a(this.f)), Jd0.T4, Px1.a(this.g)), Jd0.U4, Px1.a(this.h)), Jd0.V4, Px1.a(this.e)), Jd0.W4, Px1.a(this.e)), Jd0.X4, Px1.a(this.k));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.c, i2);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.e, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.f, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.g, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.h, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.i, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.j, i2);
        }
        if (parcel != null) {
            parcel.writeParcelableArray(this.k, i2);
        }
    }
}
