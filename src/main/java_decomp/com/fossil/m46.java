package com.fossil;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.iq4;
import com.fossil.k66;
import com.fossil.l56;
import com.fossil.lt5;
import com.fossil.tq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y56;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m46 extends g46 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public /* final */ List<Object> f; // = new ArrayList();
    @DexIgnore
    public List<Object> g; // = new ArrayList();
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public /* final */ List<j06> i; // = new ArrayList();
    @DexIgnore
    public /* final */ List<i06> j; // = new ArrayList();
    @DexIgnore
    public /* final */ List<j06> k; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> m; // = new ArrayList();
    @DexIgnore
    public /* final */ LoaderManager n;
    @DexIgnore
    public /* final */ h46 o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ uq4 q;
    @DexIgnore
    public /* final */ y56 r;
    @DexIgnore
    public /* final */ l56 s;
    @DexIgnore
    public /* final */ k66 t;
    @DexIgnore
    public /* final */ lt5 u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return m46.v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tq4.d<l56.a, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m46 f2297a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(m46 m46) {
            this.f2297a = m46;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            pq7.c(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetApps onError");
            this.f2297a.o.k();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(l56.a aVar) {
            pq7.c(aVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetApps onSuccess");
            ArrayList arrayList = new ArrayList();
            for (i06 i06 : aVar.a()) {
                InstalledApp installedApp = i06.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    pq7.i();
                    throw null;
                } else if (isSelected.booleanValue() && i06.getCurrentHandGroup() == this.f2297a.p) {
                    arrayList.add(i06);
                }
            }
            this.f2297a.I().addAll(arrayList);
            this.f2297a.H().addAll(arrayList);
            List<Object> J = this.f2297a.J();
            Object[] array = arrayList.toArray(new i06[0]);
            if (array != null) {
                Serializable a2 = h68.a((Serializable) array);
                pq7.b(a2, "SerializationUtils.clone\u2026sSelected.toTypedArray())");
                mm7.t(J, (Object[]) a2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = m46.w.a();
                local.d(a3, "mContactAndAppDataFirstLoad.size=" + this.f2297a.J().size());
                this.f2297a.n.d(1, new Bundle(), this.f2297a);
                this.f2297a.o.F0(this.f2297a.I());
                this.f2297a.o.k();
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.d<y56.c, y56.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m46 f2298a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ y56.c $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m46$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.m46$c$a$a  reason: collision with other inner class name */
            public static final class C0150a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $contactWrapperList;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0150a(a aVar, List list, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$contactWrapperList = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0150a aVar = new C0150a(this.this$0, this.$contactWrapperList, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0150a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Contact contact;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        for (ContactGroup contactGroup : this.this$0.$responseValue.a()) {
                            for (Contact contact2 : contactGroup.getContacts()) {
                                if (contactGroup.getHour() == this.this$0.this$0.f2298a.p) {
                                    j06 j06 = new j06(contact2, "");
                                    j06.setAdded(true);
                                    pq7.b(contact2, "contact");
                                    ContactGroup contactGroup2 = contact2.getContactGroup();
                                    pq7.b(contactGroup2, "contact.contactGroup");
                                    j06.setCurrentHandGroup(contactGroup2.getHour());
                                    Contact contact3 = j06.getContact();
                                    if (contact3 != null) {
                                        contact3.setDbRowId(contact2.getDbRowId());
                                        contact3.setUseSms(contact2.isUseSms());
                                        contact3.setUseCall(contact2.isUseCall());
                                    }
                                    List<PhoneNumber> phoneNumbers = contact2.getPhoneNumbers();
                                    pq7.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact2.getPhoneNumbers().get(0);
                                        pq7.b(phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            j06.setHasPhoneNumber(true);
                                            j06.setPhoneNumber(number);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a2 = m46.w.a();
                                            local.d(a2, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = m46.w.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                                    ContactGroup contactGroup3 = contact2.getContactGroup();
                                    pq7.b(contactGroup3, "contact.contactGroup");
                                    sb.append(contactGroup3.getHour());
                                    sb.append(" ,rowId=");
                                    sb.append(contact2.getDbRowId());
                                    sb.append(" ,isUseText=");
                                    sb.append(contact2.isUseSms());
                                    sb.append(" ,isUseCall=");
                                    sb.append(contact2.isUseCall());
                                    local2.d(a3, sb.toString());
                                    this.$contactWrapperList.add(j06);
                                    Contact contact4 = j06.getContact();
                                    if ((contact4 != null && contact4.getContactId() == -100) || ((contact = j06.getContact()) != null && contact.getContactId() == -200)) {
                                        this.this$0.this$0.f2298a.L().add(j06);
                                    }
                                }
                            }
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, y56.c cVar2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$responseValue = cVar2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetAllHybridContactGroups onSuccess");
                    ArrayList arrayList = new ArrayList();
                    rv7 rv7 = gu7.b(iv7, this.this$0.f2298a.h(), null, new C0150a(this, arrayList, null), 2, null);
                    this.L$0 = iv7;
                    this.L$1 = arrayList;
                    this.L$2 = rv7;
                    this.label = 1;
                    if (rv7.l(this) == d) {
                        return d;
                    }
                    list = arrayList;
                } else if (i == 1) {
                    rv7 rv72 = (rv7) this.L$2;
                    list = (List) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.f2298a.I().addAll(list);
                this.this$0.f2298a.K().addAll(list);
                List<Object> J = this.this$0.f2298a.J();
                Object[] array = list.toArray(new j06[0]);
                if (array != null) {
                    Serializable a2 = h68.a((Serializable) array);
                    pq7.b(a2, "SerializationUtils.clone\u2026apperList.toTypedArray())");
                    mm7.t(J, (Object[]) a2);
                    this.this$0.f2298a.N();
                    return tl7.f3441a;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(m46 m46) {
            this.f2298a = m46;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(y56.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetAllHybridContactGroups onError");
            this.f2298a.o.k();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(y56.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.f2298a.k(), null, null, new a(this, cVar, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements tq4.d<l56.a, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m46 f2299a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public d(m46 m46, ArrayList arrayList, List list) {
            this.f2299a = m46;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            pq7.c(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetApps onError");
            this.f2299a.o.k();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(l56.a aVar) {
            pq7.c(aVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "mGetHybridApp onSuccess");
            ArrayList arrayList = new ArrayList();
            for (String str : this.b) {
                Iterator<T> it = aVar.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        T next = it.next();
                        if (pq7.a(String.valueOf(next.getUri()), str)) {
                            InstalledApp installedApp = next.getInstalledApp();
                            if (installedApp != null) {
                                installedApp.setSelected(true);
                            }
                            next.setCurrentHandGroup(this.f2299a.p);
                            arrayList.add(next);
                            List list = this.c;
                            Uri uri = next.getUri();
                            if (uri != null) {
                                list.add(uri);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    }
                }
            }
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "queryAppsString: appsSelected=" + arrayList);
            this.f2299a.I().addAll(arrayList);
            m46 m46 = this.f2299a;
            m46.w(m46.M());
            this.f2299a.o.B3(this.f2299a.q());
            int size = this.f2299a.H().size();
            for (int i = 0; i < size; i++) {
                if (!pm7.B(this.c, this.f2299a.H().get(i).getUri())) {
                    i06 i06 = this.f2299a.H().get(i);
                    InstalledApp installedApp2 = i06.getInstalledApp();
                    if (installedApp2 != null) {
                        installedApp2.setSelected(false);
                        this.f2299a.I().add(i06);
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
            this.f2299a.o.F0(this.f2299a.I());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<lt5.c, lt5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ m46 f2300a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements tq4.d<k66.b, tq4.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ e f2301a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(e eVar) {
                this.f2301a = eVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(tq4.a aVar) {
                pq7.c(aVar, "errorResponse");
                FLogger.INSTANCE.getLocal().d(m46.w.a(), ".Inside mSaveAllNotification onError");
                this.f2301a.f2300a.o.k();
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(k66.b bVar) {
                pq7.c(bVar, "successResponse");
                FLogger.INSTANCE.getLocal().d(m46.w.a(), ".Inside mSaveAllNotification onSuccess");
                this.f2301a.f2300a.o.k();
                e eVar = this.f2301a;
                if (eVar.d) {
                    eVar.f2300a.o.close();
                }
            }
        }

        @DexIgnore
        public e(m46 m46, List list, List list2, boolean z) {
            this.f2300a = m46;
            this.b = list;
            this.c = list2;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(lt5.b bVar) {
            pq7.c(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = m46.w.a();
            local.d(a2, "saveNotification fail!! errorValue=" + bVar.a());
            this.f2300a.o.k();
            if (bVar.c() != 1101) {
                this.f2300a.o.u();
                return;
            }
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "Bluetooth is disabled");
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.b());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            h46 h46 = this.f2300a.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                h46.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(lt5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(m46.w.a(), "saveNotification success!!");
            this.f2300a.q.a(this.f2300a.t, new k66.a(this.b, this.c), new a(this));
        }
    }

    /*
    static {
        String simpleName = m46.class.getSimpleName();
        pq7.b(simpleName, "NotificationContactsAndA\u2026er::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public m46(LoaderManager loaderManager, h46 h46, int i2, uq4 uq4, y56 y56, l56 l56, k66 k66, lt5 lt5) {
        pq7.c(loaderManager, "mLoaderManager");
        pq7.c(h46, "mView");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(y56, "mGetAllHybridContactGroups");
        pq7.c(l56, "mGetHybridApp");
        pq7.c(k66, "mSaveAllHybridNotification");
        pq7.c(lt5, "mSetNotificationFiltersUserCase");
        this.n = loaderManager;
        this.o = h46;
        this.p = i2;
        this.q = uq4;
        this.r = y56;
        this.s = l56;
        this.t = k66;
        this.u = lt5;
    }

    @DexIgnore
    public final List<i06> H() {
        return this.j;
    }

    @DexIgnore
    public final List<Object> I() {
        return this.f;
    }

    @DexIgnore
    public final List<Object> J() {
        return this.g;
    }

    @DexIgnore
    public final List<j06> K() {
        return this.i;
    }

    @DexIgnore
    public final List<j06> L() {
        return this.k;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0065 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x012e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean M() {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.m46.M():boolean");
    }

    @DexIgnore
    public final void N() {
        this.q.a(this.s, null, new b(this));
    }

    @DexIgnore
    public final void O() {
        this.o.m();
        this.q.a(this.r, null, new c(this));
    }

    @DexIgnore
    /* renamed from: P */
    public void a(at0<Cursor> at0, Cursor cursor) {
        pq7.c(at0, "loader");
        if (cursor != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = v;
            local.d(str, "onLoadFinished, loader id = " + at0.getId() + "cursor is closed = " + cursor.isClosed());
            if (cursor.isClosed()) {
                this.n.f(at0.getId(), new Bundle(), this);
                return;
            }
            if (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex("display_name"));
                    String string2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                    int i2 = cursor.getInt(cursor.getColumnIndex("contact_id"));
                    this.l.add(Integer.valueOf(i2));
                    T(i2, string, string2);
                } while (cursor.moveToNext());
            }
            Q();
            this.o.F0(this.f);
            cursor.close();
        }
    }

    @DexIgnore
    public final void Q() {
        if (!this.m.isEmpty()) {
            Collection a2 = b78.a(this.m, this.l);
            pq7.b(a2, "CollectionUtils.subtract\u2026actIds, mPhoneContactIds)");
            List h0 = pm7.h0(a2);
            if (!h0.isEmpty()) {
                List<j06> arrayList = new ArrayList<>();
                for (int size = this.f.size() - 1; size >= 0; size--) {
                    Object obj = this.f.get(size);
                    if (obj instanceof j06) {
                        j06 j06 = (j06) obj;
                        Contact contact = j06.getContact();
                        Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                        int size2 = h0.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                break;
                            } else if (pq7.a((Integer) h0.get(i2), valueOf)) {
                                j06.setAdded(false);
                                arrayList.add(obj);
                                this.f.remove(size);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.o.m();
                    R(arrayList, new ArrayList<>(), false);
                }
            }
        }
    }

    @DexIgnore
    public final void R(List<j06> list, List<i06> list2, boolean z) {
        this.u.e(new lt5.a(list, list2, this.p), new e(this, list, list2, z));
    }

    @DexIgnore
    public void S() {
        this.o.M5(this);
    }

    @DexIgnore
    public final void T(int i2, String str, String str2) {
        j06 j06;
        Contact contact;
        for (Object obj : this.f) {
            if ((obj instanceof j06) && (contact = (j06 = (j06) obj).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = j06.getContact();
                if (contact2 != null) {
                    contact2.setFirstName(str);
                }
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setPhotoThumbUri(str2);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public at0<Cursor> d(int i2, Bundle bundle) {
        boolean z;
        this.m.clear();
        this.l.clear();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("has_phone_number != 0 AND mimetype =? ");
        ArrayList arrayList = new ArrayList();
        arrayList.add("vnd.android.cursor.item/phone_v2");
        if (!this.f.isEmpty()) {
            z = false;
            for (T t2 : this.f) {
                if (t2 instanceof j06) {
                    T t3 = t2;
                    if (t3.getContact() == null) {
                        continue;
                    } else {
                        Contact contact = t3.getContact();
                        if (contact == null) {
                            pq7.i();
                            throw null;
                        } else if (contact.getContactId() >= 0) {
                            if (!z) {
                                sb2.append("AND contact_id IN (");
                                z = true;
                            }
                            sb2.append("?, ");
                            Contact contact2 = t3.getContact();
                            Integer valueOf = contact2 != null ? Integer.valueOf(contact2.getContactId()) : null;
                            if (valueOf != null) {
                                this.m.add(valueOf);
                            }
                            arrayList.add(String.valueOf(valueOf));
                        }
                    }
                }
            }
        } else {
            z = false;
        }
        if (z) {
            sb.append((CharSequence) new StringBuilder(sb2.substring(0, sb2.length() - 2)));
            sb.append(")");
        } else {
            sb.append((CharSequence) sb2);
        }
        FLogger.INSTANCE.getLocal().d(v, ".Inside onCreateLoader, selectionQuery = " + ((Object) sb));
        PortfolioApp c2 = PortfolioApp.h0.c();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String sb3 = sb.toString();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new zs0(c2, uri, new String[]{"contact_id", "display_name", "photo_thumb_uri"}, sb3, (String[]) array, null);
        }
        throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(at0<Cursor> at0) {
        pq7.c(at0, "loader");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(v, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.u.q();
        wq5.d.g(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
        h46 h46 = this.o;
        if (h46 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedFragment");
        } else if (((i46) h46).getContext() != null) {
            this.o.w5(this.p);
            if (!this.f.isEmpty() || !this.h) {
                pq7.b(this.n.d(1, new Bundle(), this), "mLoaderManager.initLoade\u2026AndAppsAssignedPresenter)");
                return;
            }
            this.h = false;
            O();
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(v, "stop");
        this.u.q();
        this.n.a(1);
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void n(ArrayList<j06> arrayList) {
        pq7.c(arrayList, "contactWrappersSelected");
        FLogger.INSTANCE.getLocal().d(v, "addContactWrapperList: contactWrappersSelected = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof j06) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact = t2.getContact();
            if (contact != null) {
                arrayList2.add(Integer.valueOf(contact.getContactId()));
            } else {
                pq7.i();
                throw null;
            }
        }
        w(M());
        this.o.B3(q());
        int size2 = this.i.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact2 = this.i.get(i2).getContact();
            if (!pm7.B(arrayList2, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                j06 j06 = this.i.get(i2);
                j06.setAdded(false);
                this.f.add(j06);
            }
        }
        this.o.F0(this.f);
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void o(ArrayList<j06> arrayList) {
        j06 j06;
        Contact contact;
        Contact contact2;
        pq7.c(arrayList, "contactWrappersSelected");
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof j06) && (((contact = (j06 = (j06) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = j06.getContact()) != null && contact2.getContactId() == -200))) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact3 = t2.getContact();
            if (contact3 != null) {
                arrayList2.add(Integer.valueOf(contact3.getContactId()));
            } else {
                pq7.i();
                throw null;
            }
        }
        w(M());
        this.o.B3(q());
        int size2 = this.k.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact4 = this.k.get(i2).getContact();
            if (!pm7.B(arrayList2, contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                j06 j062 = this.k.get(i2);
                j062.setAdded(false);
                this.f.add(j062);
            }
        }
        this.o.F0(this.f);
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void p() {
        w(M());
        this.o.B3(q());
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void r() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if (obj instanceof i06) {
                    i06 i06 = (i06) obj;
                    InstalledApp installedApp = i06.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        pq7.i();
                        throw null;
                    } else if (isSelected.booleanValue()) {
                        Uri uri = i06.getUri();
                        if (uri != null) {
                            arrayList.add(uri.toString());
                        }
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.N4(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.h46 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.g46
    public void s() {
        j06 j06;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof j06) && ((contact = (j06 = (j06) obj).getContact()) == null || contact.getContactId() != -100 || (contact2 = j06.getContact()) == null || contact2.getContactId() != -200)) {
                    if (j06.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.m6(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.h46 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.g46
    public void t() {
        j06 j06;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof j06) && (((contact = (j06 = (j06) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = j06.getContact()) != null && contact2.getContactId() == -200))) {
                    if (j06.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.X1(this.p, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void u(ArrayList<String> arrayList) {
        pq7.c(arrayList, "stringAppsSelected");
        FLogger.INSTANCE.getLocal().d(v, "queryAppsString = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof i06) {
                    this.f.remove(size);
                }
            }
        }
        this.q.a(this.s, null, new d(this, arrayList, new ArrayList()));
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void v() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "mContactAndAppData.size=" + this.f.size() + " mContactAndAppDataFirstLoad.size=" + this.g.size());
        if (q()) {
            this.o.m();
            List<j06> arrayList = new ArrayList<>();
            List<i06> arrayList2 = new ArrayList<>();
            for (j06 j06 : this.f) {
                if (j06 instanceof j06) {
                    arrayList.add(j06);
                } else if (j06 != null) {
                    arrayList2.add((i06) j06);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                }
            }
            R(arrayList, arrayList2, true);
        }
    }

    @DexIgnore
    @Override // com.fossil.g46
    public void x(int i2, boolean z, boolean z2) {
        j06 j06;
        Contact contact;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "updateContactWrapper: contactId=" + i2 + ", useCall=" + z + ", useText=" + z2);
        Iterator<Object> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if ((next instanceof j06) && (contact = (j06 = (j06) next).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = j06.getContact();
                if (contact2 != null) {
                    contact2.setUseCall(z);
                }
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setUseSms(z2);
                }
            }
        }
        w(M());
        this.o.B3(q());
        this.o.F0(this.f);
    }
}
