package com.fossil;

import com.mapped.CitizenBrandLogic;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wr4 {
    @DexIgnore
    public static /* final */ Wr4 a; // = new Wr4();

    @DexIgnore
    public final Vr4 a() {
        String Q = PortfolioApp.get.instance().Q();
        return (!Wg6.a(Q, Ph5.CITIZEN.getName()) && !Wg6.a(Q, Ph5.UNIVERSAL.getName())) ? new Ur4() : new CitizenBrandLogic();
    }
}
