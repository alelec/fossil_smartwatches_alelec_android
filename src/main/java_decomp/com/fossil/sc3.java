package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sc3 extends Ds2 implements Rc3 {
    @DexIgnore
    public Sc3() {
        super("com.google.android.gms.maps.internal.IOnMarkerClickListener");
    }

    @DexIgnore
    @Override // com.fossil.Ds2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        boolean v1 = v1(Ms2.e(parcel.readStrongBinder()));
        parcel2.writeNoException();
        Es2.a(parcel2, v1);
        return true;
    }
}
