package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SparseArray<b> f4359a; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public a c; // = new a(0, null, 0, 7, null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<b> f; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ String[] g;
    @DexIgnore
    public String h; // = "date DESC LIMIT 1";
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Runnable j; // = new c(this);
    @DexIgnore
    public /* final */ Runnable k; // = new d(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ long f4360a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public long c;

        @DexIgnore
        public a() {
            this(0, null, 0, 7, null);
        }

        @DexIgnore
        public a(long j, String str, long j2) {
            pq7.c(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.f4360a = j;
            this.b = str;
            this.c = j2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(long j, String str, long j2, int i, kq7 kq7) {
            this((i & 1) != 0 ? 0 : j, (i & 2) != 0 ? "" : str, (i & 4) == 0 ? j2 : 0);
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final long c() {
            return this.c;
        }

        @DexIgnore
        public final void d(long j) {
            this.c = j;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f4360a == aVar.f4360a && pq7.a(this.b, aVar.b) && this.c == aVar.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = c.a(this.f4360a);
            String str = this.b;
            return (((str != null ? str.hashCode() : 0) + (a2 * 31)) * 31) + c.a(this.c);
        }

        @DexIgnore
        public String toString() {
            return "InboxMessage(id=" + this.f4360a + ", sender=" + this.b + ", dateSent=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4361a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            pq7.c(str, "packageName");
            pq7.c(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.f4361a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.f4361a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!pq7.a(this.f4361a, bVar.f4361a) || !pq7.a(this.b, bVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.f4361a;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "MessageNotification(packageName=" + this.f4361a + ", sender=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yr5 b;

        @DexIgnore
        public c(yr5 yr5) {
            this.b = yr5;
        }

        @DexIgnore
        public final void run() {
            if (this.b.f4359a.size() == 0) {
                this.b.q();
                return;
            }
            this.b.f4359a.clear();
            this.b.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yr5 b;

        @DexIgnore
        public d(yr5 yr5) {
            this.b = yr5;
        }

        @DexIgnore
        public final void run() {
            if (this.b.f.isEmpty()) {
                this.b.r();
                return;
            }
            this.b.m();
            this.b.p();
        }
    }

    @DexIgnore
    public yr5() {
        String str = TextUtils.equals(Build.MANUFACTURER, "samsung") ? "display_recipient_ids" : "recipient_ids";
        this.d = str;
        this.g = new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "date", str};
        this.c.d(System.currentTimeMillis());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004b, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004c, code lost:
        com.fossil.so7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> h(android.content.Context r9, long r10) {
        /*
            r8 = this;
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.String r0 = "content://mms-sms/canonical-address"
            android.net.Uri r0 = android.net.Uri.parse(r0)
            android.net.Uri r1 = android.content.ContentUris.withAppendedId(r0, r10)
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ Exception -> 0x0050 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0050 }
            if (r1 == 0) goto L_0x0048
            boolean r0 = r1.moveToFirst()     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = "address"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x0049 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0049 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0049 }
            if (r2 != 0) goto L_0x003f
            java.lang.String r2 = "address"
            com.fossil.pq7.b(r0, r2)     // Catch:{ all -> 0x0049 }
            java.util.List r0 = r8.i(r9, r0)     // Catch:{ all -> 0x0049 }
            r6.addAll(r0)     // Catch:{ all -> 0x0049 }
        L_0x003f:
            r1.close()     // Catch:{ all -> 0x0049 }
        L_0x0042:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x0049 }
            r0 = 0
            com.fossil.so7.a(r1, r0)
        L_0x0048:
            return r6
        L_0x0049:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x004b }
        L_0x004b:
            r2 = move-exception
            com.fossil.so7.a(r1, r0)
            throw r2
        L_0x0050:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "HybridMessageNotificationComponent"
            java.lang.String r0 = r0.getMessage()
            r1.e(r2, r0)
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yr5.h(android.content.Context, long):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0063, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0064, code lost:
        com.fossil.so7.a(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0067, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> i(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = this;
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            android.net.Uri r0 = android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI
            java.lang.String r1 = android.net.Uri.encode(r9)
            android.net.Uri r1 = android.net.Uri.withAppendedPath(r0, r1)
            android.content.ContentResolver r0 = r8.getContentResolver()     // Catch:{ Exception -> 0x0068 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0068 }
            r3 = 0
            java.lang.String r4 = "display_name"
            r2[r3] = r4     // Catch:{ Exception -> 0x0068 }
            r3 = 1
            java.lang.String r4 = "normalized_number"
            r2[r3] = r4     // Catch:{ Exception -> 0x0068 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0068 }
            if (r2 == 0) goto L_0x005c
            boolean r0 = r2.moveToFirst()     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x005d
        L_0x002f:
            java.lang.String r0 = "normalized_number"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ all -> 0x0061 }
            java.lang.String r1 = r2.getString(r0)     // Catch:{ all -> 0x0061 }
            java.lang.String r0 = "display_name"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ all -> 0x0061 }
            java.lang.String r0 = r2.getString(r0)     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x0079
        L_0x0045:
            java.lang.String r1 = "name"
            com.fossil.pq7.b(r0, r1)     // Catch:{ all -> 0x0061 }
            r6.add(r0)     // Catch:{ all -> 0x0061 }
            boolean r0 = r2.moveToNext()     // Catch:{ all -> 0x0061 }
            if (r0 != 0) goto L_0x002f
        L_0x0053:
            r2.close()     // Catch:{ all -> 0x0061 }
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a     // Catch:{ all -> 0x0061 }
            r0 = 0
            com.fossil.so7.a(r2, r0)
        L_0x005c:
            return r6
        L_0x005d:
            r6.add(r9)
            goto L_0x0053
        L_0x0061:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r1 = move-exception
            com.fossil.so7.a(r2, r0)
            throw r1
        L_0x0068:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "HybridMessageNotificationComponent"
            java.lang.String r0 = r0.getMessage()
            r1.e(r2, r0)
            goto L_0x005c
        L_0x0079:
            r0 = r1
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yr5.i(android.content.Context, java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0058, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0059, code lost:
        com.fossil.so7.a(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005c, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.yr5.a j(android.content.Context r10, java.lang.String r11) {
        /*
            r9 = this;
            r6 = 0
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Exception -> 0x005d }
            java.lang.String r1 = "content://mms-sms/conversations?simple=true"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x005d }
            java.lang.String[] r2 = r9.g     // Catch:{ Exception -> 0x005d }
            r3 = 0
            r4 = 0
            java.lang.String r5 = r9.h     // Catch:{ Exception -> 0x005d }
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x005d }
            if (r7 == 0) goto L_0x0054
            boolean r0 = r7.moveToFirst()     // Catch:{ all -> 0x0056 }
            if (r0 == 0) goto L_0x004b
            int r0 = r7.getColumnIndex(r11)     // Catch:{ all -> 0x0056 }
            java.lang.String r3 = r7.getString(r0)     // Catch:{ all -> 0x0056 }
            java.lang.String r0 = "_id"
            int r0 = r7.getColumnIndex(r0)     // Catch:{ all -> 0x0056 }
            long r1 = r7.getLong(r0)     // Catch:{ all -> 0x0056 }
            java.lang.String r0 = "date"
            int r0 = r7.getColumnIndex(r0)     // Catch:{ all -> 0x0056 }
            long r4 = r7.getLong(r0)     // Catch:{ all -> 0x0056 }
            r7.close()     // Catch:{ all -> 0x0056 }
            com.fossil.yr5$a r0 = new com.fossil.yr5$a     // Catch:{ all -> 0x0056 }
            java.lang.String r8 = "recipientIds"
            com.fossil.pq7.b(r3, r8)     // Catch:{ all -> 0x0056 }
            r0.<init>(r1, r3, r4)     // Catch:{ all -> 0x0056 }
            r1 = 0
            com.fossil.so7.a(r7, r1)
        L_0x004a:
            return r0
        L_0x004b:
            r7.close()
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            r0 = 0
            com.fossil.so7.a(r7, r0)
        L_0x0054:
            r0 = r6
            goto L_0x004a
        L_0x0056:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r1 = move-exception
            com.fossil.so7.a(r7, r0)
            throw r1
        L_0x005d:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "HybridMessageNotificationComponent"
            java.lang.String r0 = r0.getMessage()
            r1.e(r2, r0)
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yr5.j(android.content.Context, java.lang.String):com.fossil.yr5$a");
    }

    @DexIgnore
    public final boolean k(StatusBarNotification statusBarNotification) {
        pq7.c(statusBarNotification, "sbn");
        if (!l(statusBarNotification)) {
            return false;
        }
        Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
        pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
        a j2 = j(applicationContext, this.d);
        if (j2 != null) {
            String a2 = j2.a();
            long b2 = j2.b();
            if (!TextUtils.isEmpty(a2) && this.c.c() < b2) {
                for (String str : wt7.Y(a2, new String[]{" "}, false, 0, 6, null)) {
                    if (str != null) {
                        Long e2 = ut7.e(wt7.u0(str).toString());
                        if (e2 != null) {
                            long longValue = e2.longValue();
                            Context applicationContext2 = PortfolioApp.h0.c().getApplicationContext();
                            pq7.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                            for (T t : h(applicationContext2, longValue)) {
                                if (!TextUtils.isEmpty(t)) {
                                    this.c = j2;
                                    String packageName = statusBarNotification.getPackageName();
                                    pq7.b(packageName, "sbn.packageName");
                                    n(t, packageName);
                                    return true;
                                }
                            }
                            continue;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }
        String tag = statusBarNotification.getTag();
        if (tag == null || !wt7.u(tag, "one_to_one", true)) {
            return false;
        }
        String packageName2 = statusBarNotification.getPackageName();
        CharSequence charSequence = statusBarNotification.getNotification().tickerText;
        if (charSequence != null) {
            try {
                int length = charSequence.length();
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        i2 = -1;
                        break;
                    } else if (pq7.a(String.valueOf(charSequence.charAt(i2)), ":")) {
                        break;
                    } else {
                        i2++;
                    }
                }
                if (i2 != -1) {
                    String obj = charSequence.subSequence(0, i2).toString();
                    if (obj != null) {
                        packageName2 = wt7.u0(obj).toString();
                    } else {
                        throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            } catch (Exception e3) {
                FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", e3.getMessage());
            }
        }
        pq7.b(packageName2, "contact");
        String packageName3 = statusBarNotification.getPackageName();
        pq7.b(packageName3, "sbn.packageName");
        n(packageName2, packageName3);
        return true;
    }

    @DexIgnore
    public final boolean l(StatusBarNotification statusBarNotification) {
        String tag = statusBarNotification.getTag();
        if (tag == null || (!wt7.u(tag, "sms", true) && !wt7.u(tag, "mms", true) && !wt7.u(tag, "rcs", true))) {
            return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
        }
        return true;
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", "processMessage()");
        b poll = this.f.poll();
        if (poll != null) {
            en5.i.a().i(new NotificationInfo(NotificationSource.TEXT, poll.b(), "", poll.a()));
            this.f4359a.put(poll.hashCode(), poll);
        }
    }

    @DexIgnore
    public final void n(String str, String str2) {
        if (this.f4359a.size() == 0) {
            o();
        }
        b bVar = new b(str2, str);
        if (this.f4359a.indexOfKey(bVar.hashCode()) < 0) {
            this.f.offer(bVar);
            if (!this.i) {
                p();
            }
        }
    }

    @DexIgnore
    public final void o() {
        q();
        this.b.postDelayed(this.j, ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public final void p() {
        this.i = true;
        this.e.postDelayed(this.k, 500);
    }

    @DexIgnore
    public final void q() {
        this.b.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void r() {
        this.i = false;
        this.e.removeCallbacksAndMessages(null);
    }
}
