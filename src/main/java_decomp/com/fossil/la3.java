package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<La3> CREATOR; // = new Wa3();
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public La3(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.b = z;
        this.c = z2;
        this.d = z3;
        this.e = z4;
        this.f = z5;
        this.g = z6;
    }

    @DexIgnore
    public final boolean A() {
        return this.f;
    }

    @DexIgnore
    public final boolean D() {
        return this.c;
    }

    @DexIgnore
    public final boolean c() {
        return this.g;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final boolean h() {
        return this.e;
    }

    @DexIgnore
    public final boolean k() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.c(parcel, 1, k());
        Bd2.c(parcel, 2, D());
        Bd2.c(parcel, 3, f());
        Bd2.c(parcel, 4, h());
        Bd2.c(parcel, 5, A());
        Bd2.c(parcel, 6, c());
        Bd2.b(parcel, a2);
    }
}
