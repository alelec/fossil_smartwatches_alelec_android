package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rg3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ Long h;
    @DexIgnore
    public /* final */ Long i;
    @DexIgnore
    public /* final */ Long j;
    @DexIgnore
    public /* final */ Boolean k;

    @DexIgnore
    public Rg3(String str, String str2, long j2, long j3, long j4, long j5, long j6, Long l, Long l2, Long l3, Boolean bool) {
        Rc2.g(str);
        Rc2.g(str2);
        Rc2.a(j2 >= 0);
        Rc2.a(j3 >= 0);
        Rc2.a(j4 >= 0);
        Rc2.a(j6 >= 0);
        this.a = str;
        this.b = str2;
        this.c = j2;
        this.d = j3;
        this.e = j4;
        this.f = j5;
        this.g = j6;
        this.h = l;
        this.i = l2;
        this.j = l3;
        this.k = bool;
    }

    @DexIgnore
    public Rg3(String str, String str2, long j2, long j3, long j4, long j5, Long l, Long l2, Long l3, Boolean bool) {
        this(str, str2, 0, 0, 0, j4, 0, null, null, null, null);
    }

    @DexIgnore
    public final Rg3 a(long j2) {
        return new Rg3(this.a, this.b, this.c, this.d, this.e, j2, this.g, this.h, this.i, this.j, this.k);
    }

    @DexIgnore
    public final Rg3 b(long j2, long j3) {
        return new Rg3(this.a, this.b, this.c, this.d, this.e, this.f, j2, Long.valueOf(j3), this.i, this.j, this.k);
    }

    @DexIgnore
    public final Rg3 c(Long l, Long l2, Boolean bool) {
        return new Rg3(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }
}
