package com.fossil;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Rn0;
import com.fossil.Yo0;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mo0 {
    @DexIgnore
    public static /* final */ AtomicInteger a; // = new AtomicInteger(1);
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Field d;
    @DexIgnore
    public static boolean e;
    @DexIgnore
    public static WeakHashMap<View, String> f;
    @DexIgnore
    public static WeakHashMap<View, Ro0> g; // = null;
    @DexIgnore
    public static Field h;
    @DexIgnore
    public static boolean i; // = false;
    @DexIgnore
    public static ThreadLocal<Rect> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnApplyWindowInsetsListener {
        @DexIgnore
        public /* final */ /* synthetic */ Io0 a;

        @DexIgnore
        public Ai(Io0 io0) {
            this.a = io0;
        }

        @DexIgnore
        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            return this.a.a(view, Vo0.o(windowInsets)).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Fi<Boolean> {
        @DexIgnore
        public Bi(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ Boolean d(View view) {
            return i(view);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.view.View, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ void e(View view, Boolean bool) {
            j(view, bool);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ boolean h(Boolean bool, Boolean bool2) {
            return k(bool, bool2);
        }

        @DexIgnore
        public Boolean i(View view) {
            return Boolean.valueOf(view.isScreenReaderFocusable());
        }

        @DexIgnore
        public void j(View view, Boolean bool) {
            view.setScreenReaderFocusable(bool.booleanValue());
        }

        @DexIgnore
        public boolean k(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Fi<CharSequence> {
        @DexIgnore
        public Ci(int i, Class cls, int i2, int i3) {
            super(i, cls, i2, i3);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ CharSequence d(View view) {
            return i(view);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.view.View, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ void e(View view, CharSequence charSequence) {
            j(view, charSequence);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ boolean h(CharSequence charSequence, CharSequence charSequence2) {
            return k(charSequence, charSequence2);
        }

        @DexIgnore
        public CharSequence i(View view) {
            return view.getAccessibilityPaneTitle();
        }

        @DexIgnore
        public void j(View view, CharSequence charSequence) {
            view.setAccessibilityPaneTitle(charSequence);
        }

        @DexIgnore
        public boolean k(CharSequence charSequence, CharSequence charSequence2) {
            return !TextUtils.equals(charSequence, charSequence2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Fi<Boolean> {
        @DexIgnore
        public Di(int i, Class cls, int i2) {
            super(i, cls, i2);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ Boolean d(View view) {
            return i(view);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.view.View, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ void e(View view, Boolean bool) {
            j(view, bool);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Mo0.Fi
        public /* bridge */ /* synthetic */ boolean h(Boolean bool, Boolean bool2) {
            return k(bool, bool2);
        }

        @DexIgnore
        public Boolean i(View view) {
            return Boolean.valueOf(view.isAccessibilityHeading());
        }

        @DexIgnore
        public void j(View view, Boolean bool) {
            view.setAccessibilityHeading(bool.booleanValue());
        }

        @DexIgnore
        public boolean k(Boolean bool, Boolean bool2) {
            return !a(bool, bool2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei implements ViewTreeObserver.OnGlobalLayoutListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public WeakHashMap<View, Boolean> b; // = new WeakHashMap<>();

        @DexIgnore
        public final void a(View view, boolean z) {
            boolean z2 = view.getVisibility() == 0;
            if (z != z2) {
                if (z2) {
                    Mo0.U(view, 16);
                }
                this.b.put(view, Boolean.valueOf(z2));
            }
        }

        @DexIgnore
        public final void b(View view) {
            view.getViewTreeObserver().addOnGlobalLayoutListener(this);
        }

        @DexIgnore
        public void onGlobalLayout() {
            for (Map.Entry<View, Boolean> entry : this.b.entrySet()) {
                a(entry.getKey(), entry.getValue().booleanValue());
            }
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            b(view);
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Fi<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Class<T> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Fi(int i, Class<T> cls, int i2) {
            this(i, cls, 0, i2);
        }

        @DexIgnore
        public Fi(int i, Class<T> cls, int i2, int i3) {
            this.a = i;
            this.b = cls;
            this.c = i3;
        }

        @DexIgnore
        public boolean a(Boolean bool, Boolean bool2) {
            return (bool == null ? false : bool.booleanValue()) == (bool2 == null ? false : bool2.booleanValue());
        }

        @DexIgnore
        public final boolean b() {
            return Build.VERSION.SDK_INT >= 19;
        }

        @DexIgnore
        public final boolean c() {
            return Build.VERSION.SDK_INT >= this.c;
        }

        @DexIgnore
        public abstract T d(View view);

        @DexIgnore
        public abstract void e(View view, T t);

        @DexIgnore
        public T f(View view) {
            if (c()) {
                return d(view);
            }
            if (b()) {
                T t = (T) view.getTag(this.a);
                if (!this.b.isInstance(t)) {
                    return null;
                }
                return t;
            }
            return null;
        }

        @DexIgnore
        public void g(View view, T t) {
            if (c()) {
                e(view, t);
            } else if (b() && h(f(view), t)) {
                Mo0.C(view);
                view.setTag(this.a, t);
                Mo0.U(view, 0);
            }
        }

        @DexIgnore
        public abstract boolean h(T t, T t2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi {
        @DexIgnore
        public static Vo0 a(View view, Vo0 vo0, Rect rect) {
            WindowInsets n = vo0.n();
            if (n != null) {
                return Vo0.o(view.computeSystemWindowInsets(n, rect));
            }
            rect.setEmpty();
            return vo0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Hi {
        @DexIgnore
        public static WindowInsets a(View view) {
            return view.getRootWindowInsets();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ii {
        @DexIgnore
        public static void a(View view, Context context, int[] iArr, AttributeSet attributeSet, TypedArray typedArray, int i, int i2) {
            view.saveAttributeDataForStyleable(context, iArr, attributeSet, typedArray, i, i2);
        }
    }

    @DexIgnore
    public interface Ji {
        @DexIgnore
        boolean a(View view, KeyEvent keyEvent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ki {
        @DexIgnore
        public static /* final */ ArrayList<WeakReference<View>> d; // = new ArrayList<>();
        @DexIgnore
        public WeakHashMap<View, Boolean> a; // = null;
        @DexIgnore
        public SparseArray<WeakReference<View>> b; // = null;
        @DexIgnore
        public WeakReference<KeyEvent> c; // = null;

        @DexIgnore
        public static Ki a(View view) {
            Ki ki = (Ki) view.getTag(Pk0.tag_unhandled_key_event_manager);
            if (ki != null) {
                return ki;
            }
            Ki ki2 = new Ki();
            view.setTag(Pk0.tag_unhandled_key_event_manager, ki2);
            return ki2;
        }

        @DexIgnore
        public boolean b(View view, KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                g();
            }
            View c2 = c(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (c2 != null && !KeyEvent.isModifierKey(keyCode)) {
                    d().put(keyCode, new WeakReference<>(c2));
                }
            }
            return c2 != null;
        }

        @DexIgnore
        public final View c(View view, KeyEvent keyEvent) {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        View c2 = c(viewGroup.getChildAt(childCount), keyEvent);
                        if (c2 != null) {
                            return c2;
                        }
                    }
                }
                if (e(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        @DexIgnore
        public final SparseArray<WeakReference<View>> d() {
            if (this.b == null) {
                this.b = new SparseArray<>();
            }
            return this.b;
        }

        @DexIgnore
        public final boolean e(View view, KeyEvent keyEvent) {
            ArrayList arrayList = (ArrayList) view.getTag(Pk0.tag_unhandled_key_listeners);
            if (arrayList != null) {
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    if (((Ji) arrayList.get(size)).a(view, keyEvent)) {
                        return true;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        public boolean f(KeyEvent keyEvent) {
            int indexOfKey;
            WeakReference<KeyEvent> weakReference = this.c;
            if (weakReference != null && weakReference.get() == keyEvent) {
                return false;
            }
            this.c = new WeakReference<>(keyEvent);
            WeakReference<View> weakReference2 = null;
            SparseArray<WeakReference<View>> d2 = d();
            if (keyEvent.getAction() == 1 && (indexOfKey = d2.indexOfKey(keyEvent.getKeyCode())) >= 0) {
                weakReference2 = d2.valueAt(indexOfKey);
                d2.removeAt(indexOfKey);
            }
            if (weakReference2 == null) {
                weakReference2 = d2.get(keyEvent.getKeyCode());
            }
            if (weakReference2 == null) {
                return false;
            }
            View view = weakReference2.get();
            if (view != null && Mo0.P(view)) {
                e(view, keyEvent);
            }
            return true;
        }

        @DexIgnore
        public final void g() {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            if (!d.isEmpty()) {
                synchronized (d) {
                    if (this.a == null) {
                        this.a = new WeakHashMap<>();
                    }
                    for (int size = d.size() - 1; size >= 0; size--) {
                        View view = d.get(size).get();
                        if (view == null) {
                            d.remove(size);
                        } else {
                            this.a.put(view, Boolean.TRUE);
                            for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                                this.a.put((View) parent, Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        new Ei();
    }
    */

    @DexIgnore
    public static int A(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        if (!e) {
            try {
                Field declaredField = View.class.getDeclaredField("mMinHeight");
                d = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e2) {
            }
            e = true;
        }
        Field field = d;
        if (field != null) {
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception e3) {
            }
        }
        return 0;
    }

    @DexIgnore
    public static void A0(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setPaddingRelative(i2, i3, i4, i5);
        } else {
            view.setPadding(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public static int B(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        if (!c) {
            try {
                Field declaredField = View.class.getDeclaredField("mMinWidth");
                b = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e2) {
            }
            c = true;
        }
        Field field = b;
        if (field != null) {
            try {
                return ((Integer) field.get(view)).intValue();
            } catch (Exception e3) {
            }
        }
        return 0;
    }

    @DexIgnore
    public static void B0(View view, Ko0 ko0) {
        if (Build.VERSION.SDK_INT >= 24) {
            view.setPointerIcon((PointerIcon) (ko0 != null ? ko0.a() : null));
        }
    }

    @DexIgnore
    public static Rn0 C(View view) {
        Rn0 k = k(view);
        if (k == null) {
            k = new Rn0();
        }
        l0(view, k);
        return k;
    }

    @DexIgnore
    public static void C0(View view, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(i2, i3);
        }
    }

    @DexIgnore
    public static int D(View view) {
        return Build.VERSION.SDK_INT >= 17 ? view.getPaddingEnd() : view.getPaddingRight();
    }

    @DexIgnore
    public static void D0(View view, String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTransitionName(str);
            return;
        }
        if (f == null) {
            f = new WeakHashMap<>();
        }
        f.put(view, str);
    }

    @DexIgnore
    public static int E(View view) {
        return Build.VERSION.SDK_INT >= 17 ? view.getPaddingStart() : view.getPaddingLeft();
    }

    @DexIgnore
    public static void E0(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setTranslationZ(f2);
        }
    }

    @DexIgnore
    public static ViewParent F(View view) {
        return Build.VERSION.SDK_INT >= 16 ? view.getParentForAccessibility() : view.getParent();
    }

    @DexIgnore
    public static void F0(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.stopNestedScroll();
        } else if (view instanceof Co0) {
            ((Co0) view).stopNestedScroll();
        }
    }

    @DexIgnore
    public static Vo0 G(View view) {
        if (Build.VERSION.SDK_INT >= 23) {
            return Vo0.o(Hi.a(view));
        }
        return null;
    }

    @DexIgnore
    public static void G0(View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    @DexIgnore
    public static String H(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getTransitionName();
        }
        WeakHashMap<View, String> weakHashMap = f;
        if (weakHashMap == null) {
            return null;
        }
        return weakHashMap.get(view);
    }

    @DexIgnore
    public static float I(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getTranslationZ() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static int J(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getWindowSystemUiVisibility();
        }
        return 0;
    }

    @DexIgnore
    public static float K(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getZ() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static boolean L(View view) {
        if (Build.VERSION.SDK_INT >= 15) {
            return view.hasOnClickListeners();
        }
        return false;
    }

    @DexIgnore
    public static boolean M(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.hasOverlappingRendering();
        }
        return true;
    }

    @DexIgnore
    public static boolean N(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.hasTransientState();
        }
        return false;
    }

    @DexIgnore
    public static boolean O(View view) {
        Boolean f2 = a().f(view);
        if (f2 == null) {
            return false;
        }
        return f2.booleanValue();
    }

    @DexIgnore
    public static boolean P(View view) {
        return Build.VERSION.SDK_INT >= 19 ? view.isAttachedToWindow() : view.getWindowToken() != null;
    }

    @DexIgnore
    public static boolean Q(View view) {
        return Build.VERSION.SDK_INT >= 19 ? view.isLaidOut() : view.getWidth() > 0 && view.getHeight() > 0;
    }

    @DexIgnore
    public static boolean R(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.isNestedScrollingEnabled();
        }
        if (view instanceof Co0) {
            return ((Co0) view).isNestedScrollingEnabled();
        }
        return false;
    }

    @DexIgnore
    public static boolean S(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.isPaddingRelative();
        }
        return false;
    }

    @DexIgnore
    public static boolean T(View view) {
        Boolean f2 = k0().f(view);
        if (f2 == null) {
            return false;
        }
        return f2.booleanValue();
    }

    @DexIgnore
    public static void U(View view, int i2) {
        if (((AccessibilityManager) view.getContext().getSystemService("accessibility")).isEnabled()) {
            boolean z = o(view) != null;
            if (n(view) != 0 || (z && view.getVisibility() == 0)) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(z ? 32 : 2048);
                obtain.setContentChangeTypes(i2);
                view.sendAccessibilityEventUnchecked(obtain);
            } else if (view.getParent() != null) {
                try {
                    view.getParent().notifySubtreeAccessibilityStateChanged(view, view, i2);
                } catch (AbstractMethodError e2) {
                    Log.e("ViewCompat", view.getParent().getClass().getSimpleName() + " does not fully implement ViewParent", e2);
                }
            }
        }
    }

    @DexIgnore
    public static void V(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetLeftAndRight(i2);
        } else if (i3 >= 21) {
            Rect v = v();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                v.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !v.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            d(view, i2);
            if (z && v.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(v);
            }
        } else {
            d(view, i2);
        }
    }

    @DexIgnore
    public static void W(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 23) {
            view.offsetTopAndBottom(i2);
        } else if (i3 >= 21) {
            Rect v = v();
            boolean z = false;
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                View view2 = (View) parent;
                v.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !v.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            e(view, i2);
            if (z && v.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((View) parent).invalidate(v);
            }
        } else {
            e(view, i2);
        }
    }

    @DexIgnore
    public static Vo0 X(View view, Vo0 vo0) {
        WindowInsets n;
        if (Build.VERSION.SDK_INT < 21 || (n = vo0.n()) == null) {
            return vo0;
        }
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(n);
        return !onApplyWindowInsets.equals(n) ? Vo0.o(onApplyWindowInsets) : vo0;
    }

    @DexIgnore
    public static void Y(View view, Yo0 yo0) {
        view.onInitializeAccessibilityNodeInfo(yo0.D0());
    }

    @DexIgnore
    public static Fi<CharSequence> Z() {
        return new Ci(Pk0.tag_accessibility_pane_title, CharSequence.class, 8, 28);
    }

    @DexIgnore
    public static Fi<Boolean> a() {
        return new Di(Pk0.tag_accessibility_heading, Boolean.class, 28);
    }

    @DexIgnore
    public static boolean a0(View view, int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.performAccessibilityAction(i2, bundle);
        }
        return false;
    }

    @DexIgnore
    public static void b(View view, Yo0.Ai ai) {
        if (Build.VERSION.SDK_INT >= 21) {
            C(view);
            g0(ai.b(), view);
            p(view).add(ai);
            U(view, 0);
        }
    }

    @DexIgnore
    public static void b0(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        } else {
            view.postInvalidate();
        }
    }

    @DexIgnore
    public static Ro0 c(View view) {
        if (g == null) {
            g = new WeakHashMap<>();
        }
        Ro0 ro0 = g.get(view);
        if (ro0 != null) {
            return ro0;
        }
        Ro0 ro02 = new Ro0(view);
        g.put(view, ro02);
        return ro02;
    }

    @DexIgnore
    public static void c0(View view, int i2, int i3, int i4, int i5) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation(i2, i3, i4, i5);
        } else {
            view.postInvalidate(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public static void d(View view, int i2) {
        view.offsetLeftAndRight(i2);
        if (view.getVisibility() == 0) {
            G0(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                G0((View) parent);
            }
        }
    }

    @DexIgnore
    public static void d0(View view, Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay());
        }
    }

    @DexIgnore
    public static void e(View view, int i2) {
        view.offsetTopAndBottom(i2);
        if (view.getVisibility() == 0) {
            G0(view);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                G0((View) parent);
            }
        }
    }

    @DexIgnore
    public static void e0(View view, Runnable runnable, long j2) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimationDelayed(runnable, j2);
        } else {
            view.postDelayed(runnable, ValueAnimator.getFrameDelay() + j2);
        }
    }

    @DexIgnore
    public static Vo0 f(View view, Vo0 vo0, Rect rect) {
        return Build.VERSION.SDK_INT >= 21 ? Gi.a(view, vo0, rect) : vo0;
    }

    @DexIgnore
    public static void f0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 21) {
            g0(i2, view);
            U(view, 0);
        }
    }

    @DexIgnore
    public static Vo0 g(View view, Vo0 vo0) {
        WindowInsets n;
        return (Build.VERSION.SDK_INT < 21 || (n = vo0.n()) == null || view.dispatchApplyWindowInsets(n).equals(n)) ? vo0 : Vo0.o(n);
    }

    @DexIgnore
    public static void g0(int i2, View view) {
        List<Yo0.Ai> p = p(view);
        for (int i3 = 0; i3 < p.size(); i3++) {
            if (p.get(i3).b() == i2) {
                p.remove(i3);
                return;
            }
        }
    }

    @DexIgnore
    public static boolean h(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return Ki.a(view).b(view, keyEvent);
    }

    @DexIgnore
    public static void h0(View view, Yo0.Ai ai, CharSequence charSequence, Bp0 bp0) {
        if (bp0 == null && charSequence == null) {
            f0(view, ai.b());
        } else {
            b(view, ai.a(charSequence, bp0));
        }
    }

    @DexIgnore
    public static boolean i(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return Ki.a(view).f(keyEvent);
    }

    @DexIgnore
    public static void i0(View view) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 20) {
            view.requestApplyInsets();
        } else if (i2 >= 16) {
            view.requestFitSystemWindows();
        }
    }

    @DexIgnore
    public static int j() {
        int i2;
        int i3;
        if (Build.VERSION.SDK_INT >= 17) {
            return View.generateViewId();
        }
        do {
            i2 = a.get();
            i3 = i2 + 1;
            if (i3 > 16777215) {
                i3 = 1;
            }
        } while (!a.compareAndSet(i2, i3));
        return i2;
    }

    @DexIgnore
    public static void j0(View view, @SuppressLint({"ContextFirst"}) Context context, int[] iArr, AttributeSet attributeSet, TypedArray typedArray, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 29) {
            Ii.a(view, context, iArr, attributeSet, typedArray, i2, i3);
        }
    }

    @DexIgnore
    public static Rn0 k(View view) {
        View.AccessibilityDelegate l = l(view);
        if (l == null) {
            return null;
        }
        return l instanceof Rn0.Ai ? ((Rn0.Ai) l).a : new Rn0(l);
    }

    @DexIgnore
    public static Fi<Boolean> k0() {
        return new Bi(Pk0.tag_screen_reader_focusable, Boolean.class, 28);
    }

    @DexIgnore
    public static View.AccessibilityDelegate l(View view) {
        return Build.VERSION.SDK_INT >= 29 ? view.getAccessibilityDelegate() : m(view);
    }

    @DexIgnore
    public static void l0(View view, Rn0 rn0) {
        if (rn0 == null && (l(view) instanceof Rn0.Ai)) {
            rn0 = new Rn0();
        }
        view.setAccessibilityDelegate(rn0 == null ? null : rn0.d());
    }

    @DexIgnore
    public static View.AccessibilityDelegate m(View view) {
        if (i) {
            return null;
        }
        if (h == null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAccessibilityDelegate");
                h = declaredField;
                declaredField.setAccessible(true);
            } catch (Throwable th) {
                i = true;
                return null;
            }
        }
        try {
            Object obj = h.get(view);
            if (obj instanceof View.AccessibilityDelegate) {
                return (View.AccessibilityDelegate) obj;
            }
            return null;
        } catch (Throwable th2) {
            i = true;
            return null;
        }
    }

    @DexIgnore
    public static void m0(View view, boolean z) {
        a().g(view, Boolean.valueOf(z));
    }

    @DexIgnore
    public static int n(View view) {
        if (Build.VERSION.SDK_INT >= 19) {
            return view.getAccessibilityLiveRegion();
        }
        return 0;
    }

    @DexIgnore
    public static void n0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 19) {
            view.setAccessibilityLiveRegion(i2);
        }
    }

    @DexIgnore
    public static CharSequence o(View view) {
        return Z().f(view);
    }

    @DexIgnore
    public static void o0(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    @DexIgnore
    public static List<Yo0.Ai> p(View view) {
        ArrayList arrayList = (ArrayList) view.getTag(Pk0.tag_accessibility_actions);
        if (arrayList != null) {
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList();
        view.setTag(Pk0.tag_accessibility_actions, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static void p0(View view, ColorStateList colorStateList) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintList(colorStateList);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof Lo0) {
            ((Lo0) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    @DexIgnore
    public static ColorStateList q(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintList();
        }
        if (view instanceof Lo0) {
            return ((Lo0) view).getSupportBackgroundTintList();
        }
        return null;
    }

    @DexIgnore
    public static void q0(View view, PorterDuff.Mode mode) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintMode(mode);
            if (Build.VERSION.SDK_INT == 21) {
                Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof Lo0) {
            ((Lo0) view).setSupportBackgroundTintMode(mode);
        }
    }

    @DexIgnore
    public static PorterDuff.Mode r(View view) {
        if (Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintMode();
        }
        if (view instanceof Lo0) {
            return ((Lo0) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    @DexIgnore
    public static void r0(View view, Rect rect) {
        if (Build.VERSION.SDK_INT >= 18) {
            view.setClipBounds(rect);
        }
    }

    @DexIgnore
    public static Rect s(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return view.getClipBounds();
        }
        return null;
    }

    @DexIgnore
    public static void s0(View view, float f2) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setElevation(f2);
        }
    }

    @DexIgnore
    public static Display t(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getDisplay();
        }
        if (P(view)) {
            return ((WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    @DexIgnore
    @Deprecated
    public static void t0(View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    @DexIgnore
    public static float u(View view) {
        return Build.VERSION.SDK_INT >= 21 ? view.getElevation() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public static void u0(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setHasTransientState(z);
        }
    }

    @DexIgnore
    public static Rect v() {
        if (j == null) {
            j = new ThreadLocal<>();
        }
        Rect rect = j.get();
        if (rect == null) {
            rect = new Rect();
            j.set(rect);
        }
        rect.setEmpty();
        return rect;
    }

    @DexIgnore
    public static void v0(View view, int i2) {
        int i3 = Build.VERSION.SDK_INT;
        if (i3 >= 19) {
            view.setImportantForAccessibility(i2);
        } else if (i3 >= 16) {
            if (i2 == 4) {
                i2 = 2;
            }
            view.setImportantForAccessibility(i2);
        }
    }

    @DexIgnore
    public static boolean w(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getFitsSystemWindows();
        }
        return false;
    }

    @DexIgnore
    public static void w0(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 26) {
            view.setImportantForAutofill(i2);
        }
    }

    @DexIgnore
    public static int x(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            return view.getImportantForAccessibility();
        }
        return 0;
    }

    @DexIgnore
    public static void x0(View view, Paint paint) {
        if (Build.VERSION.SDK_INT >= 17) {
            view.setLayerPaint(paint);
            return;
        }
        view.setLayerType(view.getLayerType(), paint);
        view.invalidate();
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public static int y(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }

    @DexIgnore
    public static void y0(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 21) {
            view.setNestedScrollingEnabled(z);
        } else if (view instanceof Co0) {
            ((Co0) view).setNestedScrollingEnabled(z);
        }
    }

    @DexIgnore
    public static int z(View view) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view.getLayoutDirection();
        }
        return 0;
    }

    @DexIgnore
    public static void z0(View view, Io0 io0) {
        if (Build.VERSION.SDK_INT < 21) {
            return;
        }
        if (io0 == null) {
            view.setOnApplyWindowInsetsListener(null);
        } else {
            view.setOnApplyWindowInsetsListener(new Ai(io0));
        }
    }
}
