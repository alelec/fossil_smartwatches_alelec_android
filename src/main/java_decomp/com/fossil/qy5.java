package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.fossil.Ry5;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qy5 extends AsyncTask<Void, Void, Ai> {
    @DexIgnore
    public /* final */ WeakReference<CropImageView> a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public FilterType f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Exception e;
        @DexIgnore
        public /* final */ Bitmap f;

        @DexIgnore
        public Ai(Uri uri, Bitmap bitmap, int i, int i2, Bitmap bitmap2) {
            this.a = uri;
            this.b = bitmap;
            this.c = i;
            this.d = i2;
            this.e = null;
            this.f = bitmap2;
        }

        @DexIgnore
        public Ai(Uri uri, Exception exc) {
            this.a = uri;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = exc;
            this.f = null;
        }
    }

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public Qy5(CropImageView cropImageView, Uri uri, FilterType filterType) {
        this.b = uri;
        this.f = filterType;
        this.a = new WeakReference<>(cropImageView);
        this.c = cropImageView.getContext();
        DisplayMetrics displayMetrics = cropImageView.getResources().getDisplayMetrics();
        float f2 = displayMetrics.density;
        double d2 = f2 > 1.0f ? (double) (1.0f / f2) : 1.0d;
        this.d = (int) (((double) displayMetrics.widthPixels) * d2);
        this.e = (int) (d2 * ((double) displayMetrics.heightPixels));
    }

    @DexIgnore
    public Ai a(Void... voidArr) {
        Ry5.Bi G;
        try {
            if (!isCancelled()) {
                Ry5.Ai o = Ry5.o(this.c, this.b, this.d, this.e);
                if (!isCancelled()) {
                    if (!TextUtils.equals(this.b.getLastPathSegment(), "pickerImage.jpg")) {
                        G = Ry5.F(o.a, this.c, this.b);
                    } else {
                        G = Ry5.G(o.a, new Eq0(this.b.getPath()));
                    }
                    return new Ai(this.b, G.a, o.b, G.b, EInkImageFilter.create().apply(G.a, this.f, false, false, new OutputSettings(G.a.getWidth(), G.a.getHeight(), Format.RAW, false, false)).getPreview());
                }
            }
            return null;
        } catch (Exception e2) {
            return new Ai(this.b, e2);
        }
    }

    @DexIgnore
    public Uri b() {
        return this.b;
    }

    @DexIgnore
    public void c(Ai ai) {
        Bitmap bitmap;
        CropImageView cropImageView;
        if (ai != null) {
            boolean z = false;
            if (!isCancelled() && (cropImageView = this.a.get()) != null) {
                z = true;
                cropImageView.p(ai);
            }
            if (!z && (bitmap = ai.b) != null) {
                bitmap.recycle();
            }
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ Ai doInBackground(Void[] voidArr) {
        return a(voidArr);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Ai ai) {
        c(ai);
    }
}
