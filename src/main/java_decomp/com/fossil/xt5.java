package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt5 extends iq4<a, c, b> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ on5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4174a;
        @DexIgnore
        public /* final */ FirmwareData b;

        @DexIgnore
        public a(String str, FirmwareData firmwareData) {
            pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            pq7.c(firmwareData, "firmwareData");
            this.f4174a = str;
            this.b = firmwareData;
        }

        @DexIgnore
        public final String a() {
            return this.f4174a;
        }

        @DexIgnore
        public final FirmwareData b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateDeviceToSpecificFirmwareUsecase", f = "UpdateDeviceToSpecificFirmwareUsecase.kt", l = {47}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xt5 xt5, qn7 qn7) {
            super(qn7);
            this.this$0 = xt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = yt5.class.getSimpleName();
        pq7.b(simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public xt5(DeviceRepository deviceRepository, on5 on5) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044 A[Catch:{ Exception -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0172  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.xt5.a r10, com.fossil.qn7<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 375
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xt5.k(com.fossil.xt5$a, com.fossil.qn7):java.lang.Object");
    }
}
