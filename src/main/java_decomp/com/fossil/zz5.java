package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zz5 extends Fq4 {
    @DexIgnore
    public abstract void n();

    @DexIgnore
    public abstract void o(Alarm alarm, boolean z);

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q(Alarm alarm);
}
