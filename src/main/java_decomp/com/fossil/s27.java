package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.mapped.An4;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.nio.charset.Charset;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S27 extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ An4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Wh5 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ai(String str, Wh5 wh5, String str2) {
            Wg6.c(str, "aliasName");
            Wg6.c(wh5, "scope");
            Wg6.c(str2, "value");
            this.a = str;
            this.b = wh5;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final Wh5 b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public Bi(int i, String str) {
            Wg6.c(str, "errorMessage");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore
    public S27(An4 an4) {
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = an4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "EncryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    public Object m(Ai ai, Xe6<Object> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Start encrypt ");
        sb.append(ai != null ? ai.c() : null);
        sb.append(" with alias ");
        sb.append(ai != null ? ai.a() : null);
        local.d("EncryptValueKeyStoreUseCase", sb.toString());
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                Z37 z37 = Z37.b;
                if (ai != null) {
                    SecretKey e = z37.e(ai.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(1, e);
                    String c = ai.c();
                    Charset charset = Et7.a;
                    if (c != null) {
                        byte[] bytes = c.getBytes(charset);
                        Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                        byte[] doFinal = instance.doFinal(bytes);
                        Wg6.b(instance, "cipher");
                        String encodeToString = Base64.encodeToString(instance.getIV(), 0);
                        String encodeToString2 = Base64.encodeToString(doFinal, 0);
                        this.d.c1(ai.a(), ai.b(), encodeToString);
                        this.d.d1(ai.a(), ai.b(), encodeToString2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("EncryptValueKeyStoreUseCase", "Encryption done iv " + encodeToString + " value " + encodeToString2);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Z37 z372 = Z37.b;
                if (ai != null) {
                    KeyPair d2 = z372.d(ai.a());
                    if (d2 == null) {
                        d2 = Z37.b.b(ai.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(1, d2.getPublic());
                    String c2 = ai.c();
                    Charset charset2 = Et7.a;
                    if (c2 != null) {
                        byte[] bytes2 = c2.getBytes(charset2);
                        Wg6.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                        String encodeToString3 = Base64.encodeToString(instance2.doFinal(bytes2), 0);
                        this.d.d1(ai.a(), ai.b(), encodeToString3);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("EncryptValueKeyStoreUseCase", "Encryption done value " + encodeToString3);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            j(new Ci());
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("EncryptValueKeyStoreUseCase", "Exception when encrypt values " + e2);
            i(new Bi(600, ""));
        }
        return new Object();
    }
}
