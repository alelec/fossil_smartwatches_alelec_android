package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A74<T> {
    @DexIgnore
    public /* final */ Set<Class<? super T>> a;
    @DexIgnore
    public /* final */ Set<K74> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ D74<T> e;
    @DexIgnore
    public /* final */ Set<Class<?>> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<T> {
        @DexIgnore
        public /* final */ Set<Class<? super T>> a;
        @DexIgnore
        public /* final */ Set<K74> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public D74<T> e;
        @DexIgnore
        public Set<Class<?>> f;

        @DexIgnore
        @SafeVarargs
        public Bi(Class<T> cls, Class<? super T>... clsArr) {
            this.a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.d = 0;
            this.f = new HashSet();
            R74.c(cls, "Null interface");
            this.a.add(cls);
            for (Class<? super T> cls2 : clsArr) {
                R74.c(cls2, "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        @DexIgnore
        public Bi<T> b(K74 k74) {
            R74.c(k74, "Null dependency");
            i(k74.a());
            this.b.add(k74);
            return this;
        }

        @DexIgnore
        public Bi<T> c() {
            h(1);
            return this;
        }

        @DexIgnore
        public A74<T> d() {
            R74.d(this.e != null, "Missing required property: factory.");
            return new A74<>(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, this.f);
        }

        @DexIgnore
        public Bi<T> e() {
            h(2);
            return this;
        }

        @DexIgnore
        public Bi<T> f(D74<T> d74) {
            R74.c(d74, "Null factory");
            this.e = d74;
            return this;
        }

        @DexIgnore
        public final Bi<T> g() {
            this.d = 1;
            return this;
        }

        @DexIgnore
        public final Bi<T> h(int i) {
            R74.d(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public final void i(Class<?> cls) {
            R74.a(!this.a.contains(cls), "Components are not allowed to depend on interfaces they themselves provide.");
        }
    }

    @DexIgnore
    public A74(Set<Class<? super T>> set, Set<K74> set2, int i, int i2, D74<T> d74, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = d74;
        this.f = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> Bi<T> a(Class<T> cls) {
        return new Bi<>(cls, new Class[0]);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> Bi<T> b(Class<T> cls, Class<? super T>... clsArr) {
        return new Bi<>(cls, clsArr);
    }

    @DexIgnore
    public static <T> A74<T> g(T t, Class<T> cls) {
        Bi h = h(cls);
        h.f(Z64.b(t));
        return h.d();
    }

    @DexIgnore
    public static <T> Bi<T> h(Class<T> cls) {
        Bi<T> a2 = a(cls);
        a2.g();
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ Object l(Object obj, B74 b74) {
        return obj;
    }

    @DexIgnore
    public static /* synthetic */ Object m(Object obj, B74 b74) {
        return obj;
    }

    @DexIgnore
    @SafeVarargs
    public static <T> A74<T> n(T t, Class<T> cls, Class<? super T>... clsArr) {
        Bi b2 = b(cls, clsArr);
        b2.f(Y64.b(t));
        return b2.d();
    }

    @DexIgnore
    public Set<K74> c() {
        return this.b;
    }

    @DexIgnore
    public D74<T> d() {
        return this.e;
    }

    @DexIgnore
    public Set<Class<? super T>> e() {
        return this.a;
    }

    @DexIgnore
    public Set<Class<?>> f() {
        return this.f;
    }

    @DexIgnore
    public boolean i() {
        return this.c == 1;
    }

    @DexIgnore
    public boolean j() {
        return this.c == 2;
    }

    @DexIgnore
    public boolean k() {
        return this.d == 0;
    }

    @DexIgnore
    public String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }
}
