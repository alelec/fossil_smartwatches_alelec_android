package com.fossil;

import com.mapped.NotificationVibePattern;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D47 {
    @DexIgnore
    public static final AppNotificationFilter a() {
        AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getAppName(), DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getPackageName(), "", NotificationBaseObj.ANotificationType.NOTIFICATION));
        appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
        return appNotificationFilter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter> b(java.util.List<com.mapped.AppWrapper> r9, boolean r10) {
        /*
        // Method dump skipped, instructions count: 346
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.D47.b(java.util.List, boolean):java.util.List");
    }

    @DexIgnore
    public static /* synthetic */ List c(List list, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return b(list, z);
    }
}
