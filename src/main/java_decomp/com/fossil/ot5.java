package com.fossil;

import android.content.Intent;
import com.fossil.iq4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ e g; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ot5.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2722a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.f2722a = str;
            this.b = i;
        }

        @DexIgnore
        public final String a() {
            return this.f2722a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2723a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "errorCodes");
            this.f2723a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.f2723a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object updateDevice;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    Device deviceBySerial = ot5.this.h.getDeviceBySerial(ot5.this.q());
                    if (deviceBySerial != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ot5.j.a();
                        local.d(a2, "Update vibration stregnth " + ot5.this.p() + " to db");
                        deviceBySerial.setVibrationStrength(ao7.e(ot5.this.p()));
                        DeviceRepository deviceRepository = ot5.this.h;
                        this.L$0 = iv7;
                        this.L$1 = deviceBySerial;
                        this.L$2 = deviceBySerial;
                        this.label = 1;
                        updateDevice = deviceRepository.updateDevice(deviceBySerial, false, this);
                        if (updateDevice == d) {
                            return d;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d(ot5.j.a(), "onReceive #getDeviceBySerial success");
                    ot5.this.j(new d());
                    return tl7.f3441a;
                } else if (i == 1) {
                    Device device = (Device) this.L$2;
                    Device device2 = (Device) this.L$1;
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    updateDevice = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                iq5 iq5 = (iq5) updateDevice;
                FLogger.INSTANCE.getLocal().d(ot5.j.a(), "onReceive #getDeviceBySerial success");
                ot5.this.j(new d());
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ot5.j.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + ot5.this.r() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.SET_VIBRATION_STRENGTH && ot5.this.r()) {
                ot5.this.u(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    xw7 unused = gu7.d(ot5.this.g(), null, null, new a(this, null), 3, null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d(ot5.j.a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                ot5.this.i(new c(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        String simpleName = ot5.class.getSimpleName();
        pq7.b(simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public ot5(DeviceRepository deviceRepository, on5 on5) {
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.h = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return i;
    }

    @DexIgnore
    public final int p() {
        return this.e;
    }

    @DexIgnore
    public final String q() {
        return this.f;
    }

    @DexIgnore
    public final boolean r() {
        return this.d;
    }

    @DexIgnore
    public final void s() {
        wq5.d.e(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    /* renamed from: t */
    public Object k(b bVar, qn7<Object> qn7) {
        try {
            FLogger.INSTANCE.getLocal().d(i, "running UseCase");
            this.d = true;
            Integer e2 = bVar != null ? ao7.e(bVar.b()) : null;
            if (e2 != null) {
                this.e = e2.intValue();
                this.f = bVar.a();
                PortfolioApp.h0.c().L1(bVar.a(), new VibrationStrengthObj(tk5.b(bVar.b()), false, 2, null));
                return new Object();
            }
            pq7.i();
            throw null;
        } catch (Exception e3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.e(str, "Error inside " + i + ".connectDevice - e=" + e3);
            return new c(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void v() {
        wq5.d.j(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }
}
