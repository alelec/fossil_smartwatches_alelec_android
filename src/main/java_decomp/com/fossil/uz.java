package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uz implements Parcelable.Creator<E60> {
    @DexIgnore
    public /* synthetic */ Uz(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public E60 createFromParcel(Parcel parcel) {
        W00 w00 = W00.c;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice) readParcelable;
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                return w00.a(bluetoothDevice, readString);
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public E60[] newArray(int i) {
        return new E60[i];
    }
}
