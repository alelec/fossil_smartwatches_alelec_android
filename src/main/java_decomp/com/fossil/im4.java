package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Im4 implements Nm4 {
    @DexIgnore
    public static char c(char c, int i) {
        int i2 = ((i * 149) % 255) + 1 + c;
        return i2 <= 255 ? (char) i2 : (char) (i2 - 256);
    }

    @DexIgnore
    @Override // com.fossil.Nm4
    public void a(Om4 om4) {
        StringBuilder sb = new StringBuilder();
        sb.append((char) 0);
        while (true) {
            if (!om4.i()) {
                break;
            }
            sb.append(om4.c());
            om4.f++;
            int n = Qm4.n(om4.d(), om4.f, b());
            if (n != b()) {
                om4.o(n);
                break;
            }
        }
        int length = sb.length() - 1;
        int a2 = om4.a() + length + 1;
        om4.q(a2);
        boolean z = om4.g().a() - a2 > 0;
        if (om4.i() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / 250) + 249));
                sb.insert(1, (char) (length % 250));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: " + length);
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            om4.r(c(sb.charAt(i), om4.a() + 1));
        }
    }

    @DexIgnore
    public int b() {
        return 5;
    }
}
