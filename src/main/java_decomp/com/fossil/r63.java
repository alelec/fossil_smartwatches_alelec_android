package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R63 implements Xw2<U63> {
    @DexIgnore
    public static R63 c; // = new R63();
    @DexIgnore
    public /* final */ Xw2<U63> b;

    @DexIgnore
    public R63() {
        this(Ww2.b(new T63()));
    }

    @DexIgnore
    public R63(Xw2<U63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((U63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((U63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ U63 zza() {
        return this.b.zza();
    }
}
