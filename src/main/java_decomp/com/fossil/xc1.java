package com.fossil;

import android.util.Log;
import com.fossil.Be1;
import com.fossil.Cd1;
import com.fossil.Ie1;
import com.fossil.Kk1;
import com.fossil.Uc1;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xc1 implements Zc1, Ie1.Ai, Cd1.Ai {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);
    @DexIgnore
    public /* final */ Fd1 a;
    @DexIgnore
    public /* final */ Bd1 b;
    @DexIgnore
    public /* final */ Ie1 c;
    @DexIgnore
    public /* final */ Bi d;
    @DexIgnore
    public /* final */ Ld1 e;
    @DexIgnore
    public /* final */ Ci f;
    @DexIgnore
    public /* final */ Ai g;
    @DexIgnore
    public /* final */ Nc1 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Uc1.Ei a;
        @DexIgnore
        public /* final */ Mn0<Uc1<?>> b; // = Kk1.d(150, new Aii());
        @DexIgnore
        public int c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Kk1.Di<Uc1<?>> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public Uc1<?> a() {
                Ai ai = Ai.this;
                return new Uc1<>(ai.a, ai.b);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Kk1.Di
            public /* bridge */ /* synthetic */ Uc1<?> create() {
                return a();
            }
        }

        @DexIgnore
        public Ai(Uc1.Ei ei) {
            this.a = ei;
        }

        @DexIgnore
        public <R> Uc1<R> a(Qa1 qa1, Object obj, Ad1 ad1, Mb1 mb1, int i, int i2, Class<?> cls, Class<R> cls2, Sa1 sa1, Wc1 wc1, Map<Class<?>, Sb1<?>> map, boolean z, boolean z2, boolean z3, Ob1 ob1, Uc1.Bi<R> bi) {
            Uc1<?> b2 = this.b.b();
            Ik1.d(b2);
            Uc1<R> uc1 = (Uc1<R>) b2;
            int i3 = this.c;
            this.c = i3 + 1;
            uc1.p(qa1, obj, ad1, mb1, i, i2, cls, cls2, sa1, wc1, map, z, z2, z3, ob1, bi, i3);
            return uc1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Le1 a;
        @DexIgnore
        public /* final */ Le1 b;
        @DexIgnore
        public /* final */ Le1 c;
        @DexIgnore
        public /* final */ Le1 d;
        @DexIgnore
        public /* final */ Zc1 e;
        @DexIgnore
        public /* final */ Cd1.Ai f;
        @DexIgnore
        public /* final */ Mn0<Yc1<?>> g; // = Kk1.d(150, new Aii());

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Kk1.Di<Yc1<?>> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public Yc1<?> a() {
                Bi bi = Bi.this;
                return new Yc1<>(bi.a, bi.b, bi.c, bi.d, bi.e, bi.f, bi.g);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Kk1.Di
            public /* bridge */ /* synthetic */ Yc1<?> create() {
                return a();
            }
        }

        @DexIgnore
        public Bi(Le1 le1, Le1 le12, Le1 le13, Le1 le14, Zc1 zc1, Cd1.Ai ai) {
            this.a = le1;
            this.b = le12;
            this.c = le13;
            this.d = le14;
            this.e = zc1;
            this.f = ai;
        }

        @DexIgnore
        public <R> Yc1<R> a(Mb1 mb1, boolean z, boolean z2, boolean z3, boolean z4) {
            Yc1<?> b2 = this.g.b();
            Ik1.d(b2);
            Yc1<R> yc1 = (Yc1<R>) b2;
            yc1.l(mb1, z, z2, z3, z4);
            return yc1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Uc1.Ei {
        @DexIgnore
        public /* final */ Be1.Ai a;
        @DexIgnore
        public volatile Be1 b;

        @DexIgnore
        public Ci(Be1.Ai ai) {
            this.a = ai;
        }

        @DexIgnore
        @Override // com.fossil.Uc1.Ei
        public Be1 a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new Ce1();
                    }
                }
            }
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di {
        @DexIgnore
        public /* final */ Yc1<?> a;
        @DexIgnore
        public /* final */ Gj1 b;

        @DexIgnore
        public Di(Gj1 gj1, Yc1<?> yc1) {
            this.b = gj1;
            this.a = yc1;
        }

        @DexIgnore
        public void a() {
            synchronized (Xc1.this) {
                this.a.r(this.b);
            }
        }
    }

    @DexIgnore
    public Xc1(Ie1 ie1, Be1.Ai ai, Le1 le1, Le1 le12, Le1 le13, Le1 le14, Fd1 fd1, Bd1 bd1, Nc1 nc1, Bi bi, Ai ai2, Ld1 ld1, boolean z) {
        this.c = ie1;
        this.f = new Ci(ai);
        nc1 = nc1 == null ? new Nc1(z) : nc1;
        this.h = nc1;
        nc1.f(this);
        this.b = bd1 == null ? new Bd1() : bd1;
        this.a = fd1 == null ? new Fd1() : fd1;
        this.d = bi == null ? new Bi(le1, le12, le13, le14, this, this) : bi;
        this.g = ai2 == null ? new Ai(this.f) : ai2;
        this.e = ld1 == null ? new Ld1() : ld1;
        ie1.e(this);
    }

    @DexIgnore
    public Xc1(Ie1 ie1, Be1.Ai ai, Le1 le1, Le1 le12, Le1 le13, Le1 le14, boolean z) {
        this(ie1, ai, le1, le12, le13, le14, null, null, null, null, null, null, z);
    }

    @DexIgnore
    public static void j(String str, long j, Mb1 mb1) {
        Log.v("Engine", str + " in " + Ek1.a(j) + "ms, key: " + mb1);
    }

    @DexIgnore
    @Override // com.fossil.Ie1.Ai
    public void a(Id1<?> id1) {
        this.e.a(id1, true);
    }

    @DexIgnore
    @Override // com.fossil.Zc1
    public void b(Yc1<?> yc1, Mb1 mb1, Cd1<?> cd1) {
        synchronized (this) {
            if (cd1 != null) {
                if (cd1.f()) {
                    this.h.a(mb1, cd1);
                }
            }
            this.a.d(mb1, yc1);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zc1
    public void c(Yc1<?> yc1, Mb1 mb1) {
        synchronized (this) {
            this.a.d(mb1, yc1);
        }
    }

    @DexIgnore
    @Override // com.fossil.Cd1.Ai
    public void d(Mb1 mb1, Cd1<?> cd1) {
        this.h.d(mb1);
        if (cd1.f()) {
            this.c.b(mb1, cd1);
        } else {
            this.e.a(cd1, false);
        }
    }

    @DexIgnore
    public final Cd1<?> e(Mb1 mb1) {
        Id1<?> c2 = this.c.c(mb1);
        if (c2 == null) {
            return null;
        }
        return c2 instanceof Cd1 ? (Cd1) c2 : new Cd1<>(c2, true, true, mb1, this);
    }

    @DexIgnore
    public <R> Di f(Qa1 qa1, Object obj, Mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, Sa1 sa1, Wc1 wc1, Map<Class<?>, Sb1<?>> map, boolean z, boolean z2, Ob1 ob1, boolean z3, boolean z4, boolean z5, boolean z6, Gj1 gj1, Executor executor) {
        long b2 = i ? Ek1.b() : 0;
        Ad1 a2 = this.b.a(obj, mb1, i2, i3, map, cls, cls2, ob1);
        synchronized (this) {
            Cd1<?> i4 = i(a2, z3, b2);
            if (i4 == null) {
                return l(qa1, obj, mb1, i2, i3, cls, cls2, sa1, wc1, map, z, z2, ob1, z3, z4, z5, z6, gj1, executor, a2, b2);
            }
            gj1.c(i4, Gb1.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final Cd1<?> g(Mb1 mb1) {
        Cd1<?> e2 = this.h.e(mb1);
        if (e2 != null) {
            e2.a();
        }
        return e2;
    }

    @DexIgnore
    public final Cd1<?> h(Mb1 mb1) {
        Cd1<?> e2 = e(mb1);
        if (e2 != null) {
            e2.a();
            this.h.a(mb1, e2);
        }
        return e2;
    }

    @DexIgnore
    public final Cd1<?> i(Ad1 ad1, boolean z, long j) {
        if (!z) {
            return null;
        }
        Cd1<?> g2 = g(ad1);
        if (g2 != null) {
            if (i) {
                j("Loaded resource from active resources", j, ad1);
            }
            return g2;
        }
        Cd1<?> h2 = h(ad1);
        if (h2 == null) {
            return null;
        }
        if (i) {
            j("Loaded resource from cache", j, ad1);
        }
        return h2;
    }

    @DexIgnore
    public void k(Id1<?> id1) {
        if (id1 instanceof Cd1) {
            ((Cd1) id1).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> Di l(Qa1 qa1, Object obj, Mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, Sa1 sa1, Wc1 wc1, Map<Class<?>, Sb1<?>> map, boolean z, boolean z2, Ob1 ob1, boolean z3, boolean z4, boolean z5, boolean z6, Gj1 gj1, Executor executor, Ad1 ad1, long j) {
        Yc1<?> a2 = this.a.a(ad1, z6);
        if (a2 != null) {
            a2.b(gj1, executor);
            if (i) {
                j("Added to existing load", j, ad1);
            }
            return new Di(gj1, a2);
        }
        Yc1<R> a3 = this.d.a(ad1, z3, z4, z5, z6);
        Uc1<R> a4 = this.g.a(qa1, obj, ad1, mb1, i2, i3, cls, cls2, sa1, wc1, map, z, z2, z6, ob1, a3);
        this.a.c(ad1, a3);
        a3.b(gj1, executor);
        a3.s(a4);
        if (i) {
            j("Started new load", j, ad1);
        }
        return new Di(gj1, a3);
    }
}
