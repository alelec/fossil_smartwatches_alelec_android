package com.fossil;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Nh4;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ci4 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ci4> CREATOR; // = new Di4();
    @DexIgnore
    public Bundle b;
    @DexIgnore
    public Map<String, String> c;
    @DexIgnore
    public Bi d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Uri a;

        @DexIgnore
        public Bi(Bi4 bi4) {
            bi4.p("gcm.n.title");
            bi4.h("gcm.n.title");
            a(bi4, "gcm.n.title");
            bi4.p("gcm.n.body");
            bi4.h("gcm.n.body");
            a(bi4, "gcm.n.body");
            bi4.p("gcm.n.icon");
            bi4.o();
            bi4.p("gcm.n.tag");
            bi4.p("gcm.n.color");
            bi4.p("gcm.n.click_action");
            bi4.p("gcm.n.android_channel_id");
            this.a = bi4.f();
            bi4.p("gcm.n.image");
            bi4.p("gcm.n.ticker");
            bi4.b("gcm.n.notification_priority");
            bi4.b("gcm.n.visibility");
            bi4.b("gcm.n.notification_count");
            bi4.a("gcm.n.sticky");
            bi4.a("gcm.n.local_only");
            bi4.a("gcm.n.default_sound");
            bi4.a("gcm.n.default_vibrate_timings");
            bi4.a("gcm.n.default_light_settings");
            bi4.j("gcm.n.event_time");
            bi4.e();
            bi4.q();
        }

        @DexIgnore
        public static String[] a(Bi4 bi4, String str) {
            Object[] g = bi4.g(str);
            if (g == null) {
                return null;
            }
            String[] strArr = new String[g.length];
            for (int i = 0; i < g.length; i++) {
                strArr[i] = String.valueOf(g[i]);
            }
            return strArr;
        }
    }

    @DexIgnore
    public Ci4(Bundle bundle) {
        this.b = bundle;
    }

    @DexIgnore
    public final int A() {
        String string = this.b.getString("google.delivered_priority");
        if (string == null) {
            if ("1".equals(this.b.getString("google.priority_reduced"))) {
                return 2;
            }
            string = this.b.getString("google.priority");
        }
        return h(string);
    }

    @DexIgnore
    public final Map<String, String> c() {
        if (this.c == null) {
            this.c = Nh4.Ai.a(this.b);
        }
        return this.c;
    }

    @DexIgnore
    public final String f() {
        return this.b.getString("from");
    }

    @DexIgnore
    public final int h(String str) {
        if ("high".equals(str)) {
            return 1;
        }
        return "normal".equals(str) ? 2 : 0;
    }

    @DexIgnore
    public final Bi k() {
        if (this.d == null && Bi4.t(this.b)) {
            this.d = new Bi(new Bi4(this.b));
        }
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        Di4.c(this, parcel, i);
    }
}
