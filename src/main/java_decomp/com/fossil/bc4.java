package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bc4 {
    @DexIgnore
    public static /* final */ short[] h; // = {10, 20, 30, 60, 120, 300};
    @DexIgnore
    public /* final */ Hc4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ D94 d;
    @DexIgnore
    public /* final */ Ac4 e;
    @DexIgnore
    public /* final */ Ai f;
    @DexIgnore
    public Thread g;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        boolean a();
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Bc4 a(Wc4 wc4);
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        File[] a();

        @DexIgnore
        File[] b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends N84 {
        @DexIgnore
        public /* final */ List<Ec4> b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ float d;

        @DexIgnore
        public Di(List<Ec4> list, boolean z, float f) {
            this.b = list;
            this.c = z;
            this.d = f;
        }

        @DexIgnore
        @Override // com.fossil.N84
        public void a() {
            try {
                b(this.b, this.c);
            } catch (Exception e2) {
                X74.f().e("An unexpected error occurred while attempting to upload crash reports.", e2);
            }
            Bc4.this.g = null;
        }

        @DexIgnore
        public final void b(List<Ec4> list, boolean z) {
            int i;
            X74 f = X74.f();
            f.b("Starting report processing in " + this.d + " second(s)...");
            float f2 = this.d;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    Thread.sleep((long) (f2 * 1000.0f));
                } catch (InterruptedException e2) {
                    Thread.currentThread().interrupt();
                    return;
                }
            }
            if (!Bc4.this.f.a()) {
                int i2 = 0;
                while (list.size() > 0 && !Bc4.this.f.a()) {
                    X74 f3 = X74.f();
                    f3.b("Attempting to send " + list.size() + " report(s)");
                    ArrayList arrayList = new ArrayList();
                    for (Ec4 ec4 : list) {
                        if (!Bc4.this.d(ec4, z)) {
                            arrayList.add(ec4);
                        }
                    }
                    if (arrayList.size() > 0) {
                        long j = (long) Bc4.h[Math.min(i2, Bc4.h.length - 1)];
                        X74 f4 = X74.f();
                        f4.b("Report submission: scheduling delayed retry in " + j + " seconds");
                        try {
                            Thread.sleep(j * 1000);
                            i = i2 + 1;
                        } catch (InterruptedException e3) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    } else {
                        i = i2;
                    }
                    i2 = i;
                    list = arrayList;
                }
            }
        }
    }

    @DexIgnore
    public Bc4(String str, String str2, D94 d94, Ac4 ac4, Hc4 hc4, Ai ai) {
        if (hc4 != null) {
            this.a = hc4;
            this.b = str;
            this.c = str2;
            this.d = d94;
            this.e = ac4;
            this.f = ai;
            return;
        }
        throw new IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d A[Catch:{ Exception -> 0x003b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean d(com.fossil.Ec4 r7, boolean r8) {
        /*
            r6 = this;
            r0 = 1
            r1 = 0
            com.fossil.Cc4 r2 = new com.fossil.Cc4     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = r6.b     // Catch:{ Exception -> 0x003b }
            java.lang.String r4 = r6.c     // Catch:{ Exception -> 0x003b }
            r2.<init>(r3, r4, r7)     // Catch:{ Exception -> 0x003b }
            com.fossil.D94 r3 = r6.d     // Catch:{ Exception -> 0x003b }
            com.fossil.D94 r4 = com.fossil.D94.ALL     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0023
            com.fossil.X74 r2 = com.fossil.X74.f()     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = "Send to Reports Endpoint disabled. Removing Reports Endpoint report."
            r2.b(r3)     // Catch:{ Exception -> 0x003b }
        L_0x001a:
            r2 = r0
        L_0x001b:
            if (r2 == 0) goto L_0x0083
            com.fossil.Ac4 r2 = r6.e     // Catch:{ Exception -> 0x003b }
            r2.b(r7)     // Catch:{ Exception -> 0x003b }
        L_0x0022:
            return r0
        L_0x0023:
            com.fossil.D94 r3 = r6.d     // Catch:{ Exception -> 0x003b }
            com.fossil.D94 r4 = com.fossil.D94.JAVA_ONLY     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0056
            com.fossil.Ec4$Ai r3 = r7.getType()     // Catch:{ Exception -> 0x003b }
            com.fossil.Ec4$Ai r4 = com.fossil.Ec4.Ai.JAVA     // Catch:{ Exception -> 0x003b }
            if (r3 != r4) goto L_0x0056
            com.fossil.X74 r2 = com.fossil.X74.f()     // Catch:{ Exception -> 0x003b }
            java.lang.String r3 = "Send to Reports Endpoint for non-native reports disabled. Removing Reports Uploader report."
            r2.b(r3)     // Catch:{ Exception -> 0x003b }
            goto L_0x001a
        L_0x003b:
            r0 = move-exception
            com.fossil.X74 r2 = com.fossil.X74.f()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error occurred sending report "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r3 = r3.toString()
            r2.e(r3, r0)
            r0 = r1
            goto L_0x0022
        L_0x0056:
            com.fossil.Hc4 r3 = r6.a
            boolean r2 = r3.b(r2, r8)
            com.fossil.X74 r4 = com.fossil.X74.f()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = "Crashlytics Reports Endpoint upload "
            r5.append(r3)
            if (r2 == 0) goto L_0x0080
            java.lang.String r3 = "complete: "
        L_0x006e:
            r5.append(r3)
            java.lang.String r3 = r7.b()
            r5.append(r3)
            java.lang.String r3 = r5.toString()
            r4.g(r3)
            goto L_0x001b
        L_0x0080:
            java.lang.String r3 = "FAILED: "
            goto L_0x006e
        L_0x0083:
            r0 = r1
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Bc4.d(com.fossil.Ec4, boolean):boolean");
    }

    @DexIgnore
    public void e(List<Ec4> list, boolean z, float f2) {
        synchronized (this) {
            if (this.g != null) {
                X74.f().b("Report upload has already been started.");
                return;
            }
            Thread thread = new Thread(new Di(list, z, f2), "Crashlytics Report Uploader");
            this.g = thread;
            thread.start();
        }
    }
}
