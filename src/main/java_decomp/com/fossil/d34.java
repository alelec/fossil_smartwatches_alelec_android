package com.fossil;

import com.fossil.H34;
import com.google.j2objc.annotations.Weak;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D34<K, V> extends H34.Bi<K> {
    @DexIgnore
    @Weak
    public /* final */ A34<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ A34<K, ?> map;

        @DexIgnore
        public Ai(A34<K, ?> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.keySet();
        }
    }

    @DexIgnore
    public D34(A34<K, V> a34) {
        this.map = a34;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.H34.Bi
    public K get(int i) {
        return this.map.entrySet().asList().get(i).getKey();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.H34.Bi, com.fossil.H34.Bi, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
    public H54<K> iterator() {
        return this.map.keyIterator();
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.H34
    public Object writeReplace() {
        return new Ai(this.map);
    }
}
