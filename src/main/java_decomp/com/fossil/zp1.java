package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Zp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zp1 createFromParcel(Parcel parcel) {
            return new Zp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zp1[] newArray(int i) {
            return new Zp1[i];
        }
    }

    @DexIgnore
    public Zp1(byte b, long j, int i) {
        super(E90.WORKOUT_PAUSE, b);
        this.d = j;
        this.e = i;
    }

    @DexIgnore
    public /* synthetic */ Zp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.d = parcel.readLong();
        this.e = parcel.readInt();
    }

    @DexIgnore
    public final int b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Zp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Zp1 zp1 = (Zp1) obj;
            if (this.d != zp1.d) {
                return false;
            }
            return this.e == zp1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.PauseWorkoutNotification");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + Long.valueOf(this.d).hashCode()) * 31) + Integer.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.H5, Long.valueOf(this.d)), Jd0.q, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
