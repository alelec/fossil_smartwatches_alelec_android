package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C73 implements Xw2<B73> {
    @DexIgnore
    public static C73 c; // = new C73();
    @DexIgnore
    public /* final */ Xw2<B73> b;

    @DexIgnore
    public C73() {
        this(Ww2.b(new E73()));
    }

    @DexIgnore
    public C73(Xw2<B73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((B73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((B73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ B73 zza() {
        return this.b.zza();
    }
}
