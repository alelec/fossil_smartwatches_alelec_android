package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Hk2 extends Service {
    @DexIgnore
    public /* final */ ExecutorService b; // = Xp2.a().b(new Sf2("EnhancedIntentService"), 9);
    @DexIgnore
    public Binder c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public int e;
    @DexIgnore
    public int f; // = 0;

    @DexIgnore
    public final void b(Intent intent) {
        if (intent != null) {
            Hr0.b(intent);
        }
        synchronized (this.d) {
            int i = this.f - 1;
            this.f = i;
            if (i == 0) {
                stopSelfResult(this.e);
            }
        }
    }

    @DexIgnore
    public abstract void handleIntent(Intent intent);

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        Binder binder;
        synchronized (this) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "Service received bind request");
            }
            if (this.c == null) {
                this.c = new Lk2(this);
            }
            binder = this.c;
        }
        return binder;
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.d) {
            this.e = i2;
            this.f++;
        }
        if (intent == null) {
            b(intent);
            return 2;
        }
        this.b.execute(new Ik2(this, intent, intent));
        return 3;
    }
}
