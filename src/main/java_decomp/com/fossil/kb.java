package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kb extends Ox1 {
    @DexIgnore
    public static /* final */ Ib e; // = new Ib(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public Kb(byte b2, byte b3) {
        this(ByteBuffer.allocate(2).put(b2).put(b3).getShort(0));
    }

    @DexIgnore
    public Kb(short s) {
        this.d = (short) s;
        ByteBuffer putShort = ByteBuffer.allocate(2).putShort(this.d);
        Wg6.b(putShort, "ByteBuffer.allocate(2).putShort(fileHandle)");
        this.b = putShort.get(0);
        this.c = putShort.get(1);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.J2, Hy1.j(this.b, null, 1, null)), Jd0.t4, Hy1.j(this.c, null, 1, null)), Jd0.A0, Hy1.l(this.d, null, 1, null)), Jd0.g4, e.a(this.d));
    }
}
