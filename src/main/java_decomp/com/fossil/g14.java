package com.fossil;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class G14<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterable<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends X04<T> {
            @DexIgnore
            public /* final */ Iterator<? extends G14<? extends T>> d;

            @DexIgnore
            public Aii() {
                Iterator<T> it = Ai.this.b.iterator();
                I14.l(it);
                this.d = it;
            }

            @DexIgnore
            @Override // com.fossil.X04
            public T a() {
                while (this.d.hasNext()) {
                    G14 g14 = (G14) this.d.next();
                    if (g14.isPresent()) {
                        return (T) g14.get();
                    }
                }
                return (T) b();
            }
        }

        @DexIgnore
        public Ai(Iterable iterable) {
            this.b = iterable;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return new Aii();
        }
    }

    @DexIgnore
    public static <T> G14<T> absent() {
        return W04.withType();
    }

    @DexIgnore
    public static <T> G14<T> fromNullable(T t) {
        return t == null ? absent() : new L14(t);
    }

    @DexIgnore
    public static <T> G14<T> of(T t) {
        I14.l(t);
        return new L14(t);
    }

    @DexIgnore
    public static <T> Iterable<T> presentInstances(Iterable<? extends G14<? extends T>> iterable) {
        I14.l(iterable);
        return new Ai(iterable);
    }

    @DexIgnore
    public abstract Set<T> asSet();

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public abstract T get();

    @DexIgnore
    public abstract int hashCode();

    @DexIgnore
    public abstract boolean isPresent();

    @DexIgnore
    public abstract G14<T> or(G14<? extends T> g14);

    @DexIgnore
    public abstract T or(M14<? extends T> m14);

    @DexIgnore
    public abstract T or(T t);

    @DexIgnore
    public abstract T orNull();

    @DexIgnore
    public abstract String toString();

    @DexIgnore
    public abstract <V> G14<V> transform(B14<? super T, V> b14);
}
