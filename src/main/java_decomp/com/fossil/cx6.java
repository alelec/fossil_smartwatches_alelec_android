package com.fossil;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import com.fossil.iq4;
import com.fossil.jv5;
import com.fossil.kv5;
import com.fossil.m47;
import com.fossil.u27;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx6 extends yw6 {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public /* final */ DeviceRepository A;
    @DexIgnore
    public /* final */ ServerSettingRepository B;
    @DexIgnore
    public /* final */ pr4 C;
    @DexIgnore
    public /* final */ on5 D;
    @DexIgnore
    public jv5 e;
    @DexIgnore
    public kv5 f;
    @DexIgnore
    public vu5 g;
    @DexIgnore
    public ck5 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public MFUser j;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public Calendar u; // = Calendar.getInstance();
    @DexIgnore
    public SignUpSocialAuth v;
    @DexIgnore
    public SignUpEmailAuth w;
    @DexIgnore
    public /* final */ zw6 x;
    @DexIgnore
    public /* final */ u27 y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cx6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$a$a")
        /* renamed from: com.fossil.cx6$a$a  reason: collision with other inner class name */
        public static final class C0035a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$a$a$a")
            /* renamed from: com.fossil.cx6$a$a$a  reason: collision with other inner class name */
            public static final class C0036a implements ServerSettingDataSource.OnGetServerSettingList {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ C0035a f684a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$a$a$a$a")
                /* renamed from: com.fossil.cx6$a$a$a$a  reason: collision with other inner class name */
                public static final class C0037a implements iq4.e<kv5.c, kv5.b> {

                    @DexIgnore
                    /* renamed from: a  reason: collision with root package name */
                    public /* final */ /* synthetic */ C0036a f685a;

                    @DexIgnore
                    public C0037a(C0036a aVar) {
                        this.f685a = aVar;
                    }

                    @DexIgnore
                    /* renamed from: b */
                    public void a(kv5.b bVar) {
                        pq7.c(bVar, "errorValue");
                        this.f685a.f684a.this$0.this$0.x.h();
                        this.f685a.f684a.this$0.this$0.x.T5(bVar.a(), bVar.b());
                    }

                    @DexIgnore
                    /* renamed from: c */
                    public void onSuccess(kv5.c cVar) {
                        pq7.c(cVar, "responseValue");
                        PortfolioApp.h0.c().M().c0(this.f685a.f684a.this$0.this$0);
                        a aVar = this.f685a.f684a.this$0;
                        aVar.this$0.Q(aVar.$it.getService());
                    }
                }

                @DexIgnore
                public C0036a(C0035a aVar) {
                    this.f684a = aVar;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = cx6.E;
                    local.e(str, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.f684a.this$0.this$0.x.h();
                    this.f684a.this$0.this$0.x.T5(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(cx6.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (pq7.a(t.getObjectId(), "dataLocationSharingPrivacyVersionLatest")) {
                                    this.f684a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (pq7.a(t.getObjectId(), "privacyVersionLatest")) {
                                    this.f684a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (pq7.a(t.getObjectId(), "tosVersionLatest")) {
                                    this.f684a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = cx6.E;
                    local.d(str, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.f684a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.f684a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.f684a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.f684a.this$0.this$0.t) {
                        arrayList.add(this.f684a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.f684a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.f684a.this$0.this$0.s) {
                        arrayList2.add(this.f684a.this$0.$privacyVersionLatest.element);
                    }
                    this.f684a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.f684a.this$0.this$0.r) {
                        arrayList3.add(this.f684a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.f684a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.f684a.this$0.this$0.W().e(new kv5.a(this.f684a.this$0.$it), new C0037a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0035a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0035a aVar = new C0035a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0035a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.B.getServerSettingList(new C0036a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dr7 dr7, dr7 dr72, dr7 dr73, SignUpSocialAuth signUpSocialAuth, qn7 qn7, cx6 cx6) {
            super(2, qn7);
            this.$dataLocationSharingPrivacyVersionLatest = dr7;
            this.$privacyVersionLatest = dr72;
            this.$termOfUseVersionLatest = dr73;
            this.$it = signUpSocialAuth;
            this.this$0 = cx6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, qn7, this.this$0);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                C0035a aVar = new C0035a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(h, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $dataLocationSharingPrivacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth $it;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $privacyVersionLatest;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $termOfUseVersionLatest;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cx6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$b$a$a")
            /* renamed from: com.fossil.cx6$b$a$a  reason: collision with other inner class name */
            public static final class C0038a implements ServerSettingDataSource.OnGetServerSettingList {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f686a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$b$a$a$a")
                /* renamed from: com.fossil.cx6$b$a$a$a  reason: collision with other inner class name */
                public static final class C0039a implements iq4.e<jv5.c, jv5.b> {

                    @DexIgnore
                    /* renamed from: a  reason: collision with root package name */
                    public /* final */ /* synthetic */ C0038a f687a;

                    @DexIgnore
                    public C0039a(C0038a aVar) {
                        this.f687a = aVar;
                    }

                    @DexIgnore
                    /* renamed from: b */
                    public void a(jv5.b bVar) {
                        pq7.c(bVar, "errorValue");
                        this.f687a.f686a.this$0.this$0.x.h();
                        this.f687a.f686a.this$0.this$0.x.T5(bVar.a(), bVar.b());
                    }

                    @DexIgnore
                    /* renamed from: c */
                    public void onSuccess(jv5.c cVar) {
                        pq7.c(cVar, "responseValue");
                        PortfolioApp.h0.c().M().c0(this.f687a.f686a.this$0.this$0);
                        cx6 cx6 = this.f687a.f686a.this$0.this$0;
                        String lowerCase = "Email".toLowerCase();
                        pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                        cx6.Q(lowerCase);
                    }
                }

                @DexIgnore
                public C0038a(a aVar) {
                    this.f686a = aVar;
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onFailed(int i) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = cx6.E;
                    local.e(str, "getServerSettingList - onFailed. ErrorCode = " + i);
                    this.f686a.this$0.this$0.x.h();
                    this.f686a.this$0.this$0.x.T5(i, "");
                }

                @DexIgnore
                @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
                public void onSuccess(ServerSettingList serverSettingList) {
                    List<ServerSetting> serverSettings;
                    FLogger.INSTANCE.getLocal().d(cx6.E, "getServerSettingList - onSuccess");
                    if (!(serverSettingList == null || (serverSettings = serverSettingList.getServerSettings()) == null)) {
                        for (T t : serverSettings) {
                            if (t != null) {
                                if (pq7.a(t.getObjectId(), "dataLocationSharingPrivacyVersionLatest")) {
                                    this.f686a.this$0.$dataLocationSharingPrivacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (pq7.a(t.getObjectId(), "privacyVersionLatest")) {
                                    this.f686a.this$0.$privacyVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                                if (pq7.a(t.getObjectId(), "tosVersionLatest")) {
                                    this.f686a.this$0.$termOfUseVersionLatest.element = (T) String.valueOf(t.getValue());
                                }
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = cx6.E;
                    local.d(str, "dataLocationSharingPrivacyVersionLatest=" + ((String) this.f686a.this$0.$dataLocationSharingPrivacyVersionLatest.element) + " privacyVersionLatest=" + ((String) this.f686a.this$0.$privacyVersionLatest.element) + " termOfUseVersionLatest=" + ((String) this.f686a.this$0.$termOfUseVersionLatest.element));
                    ArrayList<String> arrayList = new ArrayList<>();
                    if (this.f686a.this$0.this$0.t) {
                        arrayList.add(this.f686a.this$0.$dataLocationSharingPrivacyVersionLatest.element);
                    }
                    this.f686a.this$0.$it.setAcceptedLocationDataSharing(arrayList);
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    if (this.f686a.this$0.this$0.s) {
                        arrayList2.add(this.f686a.this$0.$privacyVersionLatest.element);
                    }
                    this.f686a.this$0.$it.setAcceptedPrivacies(arrayList2);
                    ArrayList<String> arrayList3 = new ArrayList<>();
                    if (this.f686a.this$0.this$0.r) {
                        arrayList3.add(this.f686a.this$0.$termOfUseVersionLatest.element);
                    }
                    this.f686a.this$0.$it.setAcceptedTermsOfService(arrayList3);
                    this.f686a.this$0.this$0.V().e(new jv5.a(this.f686a.this$0.$it), new C0039a(this));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.B.getServerSettingList(new C0038a(this));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dr7 dr7, dr7 dr72, dr7 dr73, SignUpEmailAuth signUpEmailAuth, qn7 qn7, cx6 cx6) {
            super(2, qn7);
            this.$dataLocationSharingPrivacyVersionLatest = dr7;
            this.$privacyVersionLatest = dr72;
            this.$termOfUseVersionLatest = dr73;
            this.$it = signUpEmailAuth;
            this.this$0 = cx6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$dataLocationSharingPrivacyVersionLatest, this.$privacyVersionLatest, this.$termOfUseVersionLatest, this.$it, qn7, this.this$0);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(h, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1", f = "ProfileSetupPresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED, 419, 427}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $service;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cx6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$1", f = "ProfileSetupPresenter.kt", l = {410, 411, 416, 417}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<UserSettings>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<UserSettings>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00af  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00d2  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0109  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x010c  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 274
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$downloadRecommendedGoals$1$currentUser$1", f = "ProfileSetupPresenter.kt", l = {419}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cx6$c$c")
        /* renamed from: com.fossil.cx6$c$c  reason: collision with other inner class name */
        public static final class C0040c implements iq4.e<u27.c, u27.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f688a;

            @DexIgnore
            public C0040c(c cVar) {
                this.f688a = cVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(u27.a aVar) {
                pq7.c(aVar, "errorValue");
                c cVar = this.f688a;
                cVar.this$0.g0(cVar.$service);
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(u27.c cVar) {
                pq7.c(cVar, "responseValue");
                c cVar2 = this.f688a;
                cVar2.this$0.g0(cVar2.$service);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(cx6 cx6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cx6;
            this.$service = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$service, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0062  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00f0  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 243
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.cx6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cx6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$start$3$1", f = "ProfileSetupPresenter.kt", l = {157}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(cx6 cx6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cx6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            cx6 cx6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                cx6 cx62 = this.this$0;
                dv7 i2 = cx62.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = cx62;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
                cx6 = cx62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
                cx6 = (cx6) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            cx6.j = (MFUser) g;
            MFUser mFUser = this.this$0.j;
            if (mFUser != null) {
                this.this$0.x.n2(mFUser);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ cx6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter$updateAccount$1$response$1", f = "ProfileSetupPresenter.kt", l = {578}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super iq5<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super iq5<? extends MFUser>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.z;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(cx6 cx6, MFUser mFUser, qn7 qn7) {
            super(2, qn7);
            this.this$0 = cx6;
            this.$user = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$user, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.x.c6();
                this.this$0.x.i();
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            iq5 iq5 = (iq5) g;
            if (iq5 instanceof kq5) {
                FLogger.INSTANCE.getLocal().d(cx6.E, "updateAccount successfully");
                PortfolioApp.h0.c().M().c0(this.this$0);
                cx6 cx6 = this.this$0;
                String authType = this.$user.getAuthType();
                if (authType != null) {
                    cx6.Q(authType);
                } else {
                    pq7.i();
                    throw null;
                }
            } else if (iq5 instanceof hq5) {
                FLogger.INSTANCE.getLocal().d(cx6.E, "updateAccount failed");
                zw6 zw6 = this.this$0.x;
                hq5 hq5 = (hq5) iq5;
                int a2 = hq5.a();
                ServerError c = hq5.c();
                if (c == null || (str = c.getMessage()) == null) {
                    str = "";
                }
                zw6.T5(a2, str);
                this.this$0.x.h();
                this.this$0.x.k3();
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = cx6.class.getSimpleName();
        pq7.b(simpleName, "ProfileSetupPresenter::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    public cx6(zw6 zw6, u27 u27, UserRepository userRepository, DeviceRepository deviceRepository, ServerSettingRepository serverSettingRepository, pr4 pr4, on5 on5) {
        pq7.c(zw6, "mView");
        pq7.c(u27, "mGetRecommendedGoalUseCase");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(serverSettingRepository, "mServerSettingRepository");
        pq7.c(pr4, "flagRepository");
        pq7.c(on5, "shared");
        this.x = zw6;
        this.y = u27;
        this.z = userRepository;
        this.A = deviceRepository;
        this.B = serverSettingRepository;
        this.C = pr4;
        this.D = on5;
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void A() {
        Bundle bundle = new Bundle();
        Calendar instance = Calendar.getInstance();
        bundle.putInt("DAY", instance.get(5));
        bundle.putInt("MONTH", instance.get(2) + 1);
        bundle.putInt("YEAR", instance.get(1) - 32);
        this.x.z0(bundle);
    }

    @DexIgnore
    public final xw7 Q(String str) {
        pq7.c(str, Constants.SERVICE);
        return gu7.d(k(), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public SignUpEmailAuth R() {
        return this.w;
    }

    @DexIgnore
    public final List<String> S() {
        ArrayList arrayList = new ArrayList();
        if (this.p) {
            arrayList.add("Agree terms of use and privacy");
        }
        if (this.q) {
            arrayList.add("Allow Gather data usage");
        }
        if (this.r) {
            arrayList.add("Agree term of use");
        }
        if (this.s) {
            arrayList.add("Agree privacy");
        }
        if (this.t) {
            arrayList.add("Allow location data processing");
        }
        return arrayList;
    }

    @DexIgnore
    public final String T() {
        String a2 = m47.a(m47.c.PRIVACY, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886962).toString();
        pq7.b(a2, "privacyPolicyUrl");
        return vt7.q(str, "privacy_policy", a2, false, 4, null);
    }

    @DexIgnore
    public final ck5 U() {
        ck5 ck5 = this.h;
        if (ck5 != null) {
            return ck5;
        }
        pq7.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final jv5 V() {
        jv5 jv5 = this.e;
        if (jv5 != null) {
            return jv5;
        }
        pq7.n("mSignUpEmailUseCase");
        throw null;
    }

    @DexIgnore
    public final kv5 W() {
        kv5 kv5 = this.f;
        if (kv5 != null) {
            return kv5;
        }
        pq7.n("mSignUpSocialUseCase");
        throw null;
    }

    @DexIgnore
    public final String X() {
        String a2 = m47.a(m47.c.PRIVACY, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886963).toString();
        pq7.b(a2, "privacyPolicyUrl");
        return vt7.q(str, "privacy_policy", a2, false, 4, null);
    }

    @DexIgnore
    public SignUpSocialAuth Y() {
        return this.v;
    }

    @DexIgnore
    public final String Z() {
        String a2 = m47.a(m47.c.TERMS, null);
        String a3 = m47.a(m47.c.PRIVACY, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886977).toString();
        pq7.b(a2, "termOfUseUrl");
        String q2 = vt7.q(str, "term_of_use_url", a2, false, 4, null);
        pq7.b(a3, "privacyPolicyUrl");
        return vt7.q(q2, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public final String a0() {
        String a2 = m47.a(m47.c.TERMS, null);
        String str = um5.c(PortfolioApp.h0.c(), 2131886964).toString();
        pq7.b(a2, "termOfUseUrl");
        return vt7.q(str, "term_of_use_url", a2, false, 4, null);
    }

    @DexIgnore
    public final boolean b0() {
        Calendar calendar = this.u;
        if (calendar == null) {
            return false;
        }
        if (calendar != null) {
            Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(calendar), LocalDate.now());
            pq7.b(yearsBetween, "age");
            return yearsBetween.getYears() >= 16;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final boolean c0() {
        if (this.k.length() == 0) {
            this.x.n0(false, false, "");
            return false;
        } else if (!b47.a(this.k)) {
            zw6 zw6 = this.x;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887009);
            pq7.b(c2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            zw6.n0(false, true, c2);
            return false;
        } else {
            this.x.n0(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean d0() {
        String str = this.l;
        if (!(str == null || vt7.l(str))) {
            this.x.L3(true);
            return true;
        }
        this.x.L3(false);
        return false;
    }

    @DexIgnore
    public final boolean e0() {
        String str = this.m;
        if (!(str == null || vt7.l(str))) {
            this.x.p3(true);
            return true;
        }
        this.x.p3(false);
        return false;
    }

    @DexIgnore
    public boolean f0() {
        return this.i;
    }

    @DexIgnore
    public final void g0(String str) {
        pq7.c(str, Constants.SERVICE);
        sl5 b2 = ck5.f.b("user_signup");
        String lowerCase = "Source".toLowerCase();
        pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
        b2.a(lowerCase, str);
        b2.b();
        this.x.h();
        this.x.Q1();
    }

    @DexIgnore
    public void h0(SignUpEmailAuth signUpEmailAuth) {
        pq7.c(signUpEmailAuth, "auth");
        this.w = signUpEmailAuth;
    }

    @DexIgnore
    public void i0(boolean z2) {
        this.i = z2;
    }

    @DexIgnore
    public void j0(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        this.v = signUpSocialAuth;
    }

    @DexIgnore
    public void k0() {
        this.x.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        zw6 zw6 = this.x;
        Spanned fromHtml = Html.fromHtml(Z());
        pq7.b(fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        zw6.w0(fromHtml);
        zw6 zw62 = this.x;
        Spanned fromHtml2 = Html.fromHtml(a0());
        pq7.b(fromHtml2, "Html.fromHtml(getTermsOfUseString())");
        zw62.H4(fromHtml2);
        zw6 zw63 = this.x;
        Spanned fromHtml3 = Html.fromHtml(X());
        pq7.b(fromHtml3, "Html.fromHtml(getPrivacyPolicyString())");
        zw63.Y2(fromHtml3);
        zw6 zw64 = this.x;
        Spanned fromHtml4 = Html.fromHtml(T());
        pq7.b(fromHtml4, "Html.fromHtml(getLocationDataString())");
        zw64.Q3(fromHtml4);
        this.x.f();
        if (!this.i) {
            SignUpSocialAuth signUpSocialAuth = this.v;
            if (signUpSocialAuth != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = E;
                local.d(str, "start - mSocialAuth=" + signUpSocialAuth);
                this.x.e3(signUpSocialAuth);
            }
            SignUpEmailAuth signUpEmailAuth = this.w;
            if (signUpEmailAuth != null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = E;
                local2.d(str2, "start - mEmailAuth=" + signUpEmailAuth);
                this.x.I3(signUpEmailAuth);
            }
        } else if (this.j == null) {
            xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
        }
    }

    @DexIgnore
    public final void l0(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = E;
        local.d(str, "updateAccount - userId = " + mFUser.getUserId());
        xw7 unused = gu7.d(k(), null, null, new e(this, mFUser, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    public final void m0() {
        if (!c0()) {
            this.x.c6();
        } else if (!d0()) {
            this.x.c6();
        } else if (!e0()) {
            this.x.c6();
        } else if (!b0()) {
            this.x.c6();
        } else if (!wr4.f3989a.a().m(S())) {
            this.x.c6();
        } else {
            this.x.k3();
        }
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void n() {
        if (this.i) {
            MFUser mFUser = this.j;
            if (mFUser != null) {
                mFUser.setEmail(this.k);
                String str = this.l;
                if (str != null) {
                    mFUser.setFirstName(str);
                    String str2 = this.m;
                    if (str2 != null) {
                        mFUser.setLastName(str2);
                        String str3 = this.o;
                        if (str3 != null) {
                            mFUser.setBirthday(str3);
                            mFUser.setDiagnosticEnabled(this.q);
                            String str4 = this.n;
                            if (str4 == null) {
                                str4 = qh5.OTHER.toString();
                            }
                            mFUser.setGender(str4);
                            l0(mFUser);
                            return;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = E;
        local.d(str5, "create account socialAuth=" + this.v + " emailAuth=" + this.w);
        this.x.i();
        SignUpSocialAuth signUpSocialAuth = this.v;
        if (signUpSocialAuth != null) {
            signUpSocialAuth.setEmail(this.k);
            String str6 = this.n;
            if (str6 == null) {
                str6 = qh5.OTHER.toString();
            }
            signUpSocialAuth.setGender(str6);
            String str7 = this.o;
            if (str7 != null) {
                signUpSocialAuth.setBirthday(str7);
                signUpSocialAuth.setClientId(dk5.g.a(""));
                String str8 = this.l;
                if (str8 != null) {
                    signUpSocialAuth.setFirstName(str8);
                    String str9 = this.m;
                    if (str9 != null) {
                        signUpSocialAuth.setLastName(str9);
                        signUpSocialAuth.setDiagnosticEnabled(this.q);
                        dr7 dr7 = new dr7();
                        dr7.element = "";
                        dr7 dr72 = new dr7();
                        dr72.element = "";
                        dr7 dr73 = new dr7();
                        dr73.element = "";
                        xw7 unused = gu7.d(k(), null, null, new a(dr7, dr72, dr73, signUpSocialAuth, null, this), 3, null);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        SignUpEmailAuth signUpEmailAuth = this.w;
        if (signUpEmailAuth != null) {
            String str10 = this.n;
            if (str10 == null) {
                str10 = qh5.OTHER.toString();
            }
            signUpEmailAuth.setGender(str10);
            String str11 = this.o;
            if (str11 != null) {
                signUpEmailAuth.setBirthday(str11);
                signUpEmailAuth.setClientId(dk5.g.a(""));
                String str12 = this.l;
                if (str12 != null) {
                    signUpEmailAuth.setFirstName(str12);
                    String str13 = this.m;
                    if (str13 != null) {
                        signUpEmailAuth.setLastName(str13);
                        signUpEmailAuth.setDiagnosticEnabled(this.q);
                        dr7 dr74 = new dr7();
                        dr74.element = "";
                        dr7 dr75 = new dr7();
                        dr75.element = "";
                        dr7 dr76 = new dr7();
                        dr76.element = "";
                        xw7 unused2 = gu7.d(k(), null, null, new b(dr74, dr75, dr76, signUpEmailAuth, null, this), 3, null);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public Calendar o() {
        Calendar calendar = this.u;
        pq7.b(calendar, "mBirthdayCalendar");
        return calendar;
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void p(Date date, Calendar calendar) {
        pq7.c(date, "data");
        pq7.c(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d(E, "onBirthDayChanged");
        this.o = lk5.k(date);
        this.u = calendar;
        zw6 zw6 = this.x;
        String i2 = lk5.i(date);
        pq7.b(i2, "DateHelper.formatLocalDateMonth(data)");
        zw6.Z5(i2);
        if (!b0()) {
            zw6 zw62 = this.x;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886965);
            pq7.b(c2, "LanguageHelper.getString\u2026Text__YouCantUseTheAppIf)");
            zw62.K4(false, c2);
        } else {
            this.x.K4(true, "");
        }
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void q(boolean z2) {
        this.q = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void r(String str) {
        pq7.c(str, Constants.EMAIL);
        this.k = str;
        c0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void s(String str) {
        pq7.c(str, Constants.PROFILE_KEY_FIRST_NAME);
        this.l = str;
        d0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void t(qh5 qh5) {
        pq7.c(qh5, "gender");
        this.n = qh5.toString();
        this.x.p4(qh5);
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void u(String str) {
        pq7.c(str, Constants.PROFILE_KEY_LAST_NAME);
        this.m = str;
        e0();
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void v(boolean z2) {
        this.t = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void w(boolean z2) {
        this.s = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void x(boolean z2) {
        this.r = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void y(boolean z2) {
        this.p = z2;
        m0();
    }

    @DexIgnore
    @Override // com.fossil.yw6
    public void z() {
        Bundle bundle = new Bundle();
        bundle.putInt("DAY", this.u.get(5));
        bundle.putInt("MONTH", this.u.get(2) + 1);
        bundle.putInt("YEAR", this.u.get(1));
        this.x.z0(bundle);
    }
}
