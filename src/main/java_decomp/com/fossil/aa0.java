package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Aa0 {
    c((byte) 1),
    d((byte) 2),
    e((byte) 3),
    f((byte) 4),
    g((byte) 5),
    h((byte) 6),
    i((byte) 7),
    j((byte) 8),
    k((byte) 9),
    l(DateTimeFieldType.MINUTE_OF_HOUR),
    m((byte) 11),
    n((byte) 12);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Aa0(byte b2) {
        this.b = (byte) b2;
    }
}
