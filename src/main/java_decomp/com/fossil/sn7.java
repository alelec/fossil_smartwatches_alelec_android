package com.fossil;

import com.fossil.Dl7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sn7 {
    @DexIgnore
    public static final <T> void a(Hg6<? super Xe6<? super T>, ? extends Object> hg6, Xe6<? super T> xe6) {
        Wg6.c(hg6, "$this$startCoroutine");
        Wg6.c(xe6, "completion");
        Xe6 c = Xn7.c(Xn7.a(hg6, xe6));
        Cd6 cd6 = Cd6.a;
        Dl7.Ai ai = Dl7.Companion;
        c.resumeWith(Dl7.constructor-impl(cd6));
    }

    @DexIgnore
    public static final <R, T> void b(Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine, R r, Xe6<? super T> xe6) {
        Wg6.c(coroutine, "$this$startCoroutine");
        Wg6.c(xe6, "completion");
        Xe6 c = Xn7.c(Xn7.b(coroutine, r, xe6));
        Cd6 cd6 = Cd6.a;
        Dl7.Ai ai = Dl7.Companion;
        c.resumeWith(Dl7.constructor-impl(cd6));
    }
}
