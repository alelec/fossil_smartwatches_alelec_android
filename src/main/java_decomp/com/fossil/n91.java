package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N91 {
    @DexIgnore
    public /* final */ AtomicInteger a;
    @DexIgnore
    public /* final */ Set<M91<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<M91<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<M91<?>> d;
    @DexIgnore
    public /* final */ A91 e;
    @DexIgnore
    public /* final */ G91 f;
    @DexIgnore
    public /* final */ P91 g;
    @DexIgnore
    public /* final */ H91[] h;
    @DexIgnore
    public B91 i;
    @DexIgnore
    public /* final */ List<Ai> j;

    @DexIgnore
    public interface Ai<T> {
        @DexIgnore
        void a(M91<T> m91);
    }

    @DexIgnore
    public N91(A91 a91, G91 g91) {
        this(a91, g91, 4);
    }

    @DexIgnore
    public N91(A91 a91, G91 g91, int i2) {
        this(a91, g91, i2, new E91(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public N91(A91 a91, G91 g91, int i2, P91 p91) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = a91;
        this.f = g91;
        this.h = new H91[i2];
        this.g = p91;
    }

    @DexIgnore
    public <T> M91<T> a(M91<T> m91) {
        m91.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(m91);
        }
        m91.setSequence(c());
        m91.addMarker("add-to-queue");
        if (!m91.shouldCache()) {
            this.d.add(m91);
        } else {
            this.c.add(m91);
        }
        return m91;
    }

    @DexIgnore
    public <T> void b(M91<T> m91) {
        synchronized (this.b) {
            this.b.remove(m91);
        }
        synchronized (this.j) {
            for (Ai ai : this.j) {
                ai.a(m91);
            }
        }
    }

    @DexIgnore
    public int c() {
        return this.a.incrementAndGet();
    }

    @DexIgnore
    public void d() {
        e();
        B91 b91 = new B91(this.c, this.d, this.e, this.g);
        this.i = b91;
        b91.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            H91 h91 = new H91(this.d, this.f, this.e, this.g);
            this.h[i2] = h91;
            h91.start();
        }
    }

    @DexIgnore
    public void e() {
        B91 b91 = this.i;
        if (b91 != null) {
            b91.e();
        }
        H91[] h91Arr = this.h;
        for (H91 h91 : h91Arr) {
            if (h91 != null) {
                h91.e();
            }
        }
    }
}
