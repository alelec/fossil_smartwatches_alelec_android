package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pq0 {
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Ai> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ FragmentManager b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ FragmentManager.f a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ai(FragmentManager.f fVar, boolean z) {
            this.a = fVar;
            this.b = z;
        }
    }

    @DexIgnore
    public Pq0(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    @DexIgnore
    public void a(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().a(fragment, bundle, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void b(Fragment fragment, Context context, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().b(fragment, context, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.b(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void c(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().c(fragment, bundle, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.c(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void d(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().d(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.d(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void e(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().e(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.e(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void f(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().f(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.f(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void g(Fragment fragment, Context context, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().g(fragment, context, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.g(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void h(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().h(fragment, bundle, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.h(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void i(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().i(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.i(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void j(Fragment fragment, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().j(fragment, bundle, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.j(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void k(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().k(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.k(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void l(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().l(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.l(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void m(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().m(fragment, view, bundle, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.m(this.b, fragment, view, bundle);
            }
        }
    }

    @DexIgnore
    public void n(Fragment fragment, boolean z) {
        Fragment l0 = this.b.l0();
        if (l0 != null) {
            l0.getParentFragmentManager().k0().n(fragment, true);
        }
        Iterator<Ai> it = this.a.iterator();
        while (it.hasNext()) {
            Ai next = it.next();
            if (!z || next.b) {
                next.a.n(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void o(FragmentManager.f fVar, boolean z) {
        this.a.add(new Ai(fVar, z));
    }

    @DexIgnore
    public void p(FragmentManager.f fVar) {
        synchronized (this.a) {
            int size = this.a.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.a.get(i).a == fVar) {
                    this.a.remove(i);
                    break;
                } else {
                    i++;
                }
            }
        }
    }
}
