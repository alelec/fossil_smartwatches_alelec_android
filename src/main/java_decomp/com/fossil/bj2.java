package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bj2 implements Parcelable.Creator<Cj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Cj2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        IBinder iBinder = null;
        DataSet dataSet = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                dataSet = (DataSet) Ad2.e(parcel, t, DataSet.CREATOR);
            } else if (l == 2) {
                iBinder = Ad2.u(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                z = Ad2.m(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Cj2(dataSet, iBinder, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Cj2[] newArray(int i) {
        return new Cj2[i];
    }
}
