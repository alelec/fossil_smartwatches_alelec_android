package com.fossil;

import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I17 implements MembersInjector<CalibrationActivity> {
    @DexIgnore
    public static void a(CalibrationActivity calibrationActivity, CalibrationPresenter calibrationPresenter) {
        calibrationActivity.A = calibrationPresenter;
    }
}
