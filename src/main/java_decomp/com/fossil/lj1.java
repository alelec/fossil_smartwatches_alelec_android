package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lj1<T> implements Qj1<T> {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public Bj1 d;

    @DexIgnore
    public Lj1() {
        this(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    public Lj1(int i, int i2) {
        if (Jk1.s(i, i2)) {
            this.b = i;
            this.c = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public final void a(Pj1 pj1) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public final void d(Bj1 bj1) {
        this.d = bj1;
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void f(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void h(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public final Bj1 i() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public final void k(Pj1 pj1) {
        pj1.d(this.b, this.c);
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStop() {
    }
}
