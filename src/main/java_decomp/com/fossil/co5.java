package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.manager.validation.DataValidationManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Co5 implements Factory<DataValidationManager> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<An4> b;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> c;

    @DexIgnore
    public Co5(Provider<DianaPresetRepository> provider, Provider<An4> provider2, Provider<WatchFaceRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Co5 a(Provider<DianaPresetRepository> provider, Provider<An4> provider2, Provider<WatchFaceRepository> provider3) {
        return new Co5(provider, provider2, provider3);
    }

    @DexIgnore
    public static DataValidationManager c(DianaPresetRepository dianaPresetRepository, An4 an4, WatchFaceRepository watchFaceRepository) {
        return new DataValidationManager(dianaPresetRepository, an4, watchFaceRepository);
    }

    @DexIgnore
    public DataValidationManager b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
