package com.fossil;

import android.location.Location;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherInfoWatchAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn5 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static sn5 l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public PortfolioApp f3282a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public CustomizeRealDataRepository e;
    @DexIgnore
    public DianaAppSettingRepository f;
    @DexIgnore
    public GoogleApiService g;
    @DexIgnore
    public String h;
    @DexIgnore
    public Weather i;
    @DexIgnore
    public u08 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final sn5 a() {
            sn5 sn5;
            synchronized (this) {
                if (sn5.l == null) {
                    sn5.l = new sn5(null);
                }
                sn5 = sn5.l;
                if (sn5 == null) {
                    pq7.i();
                    throw null;
                }
            }
            return sn5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {259}, m = "getAddressBaseOnLocation")
    public static final class b extends co7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sn5 sn5, qn7 qn7) {
            super(qn7);
            this.this$0 = sn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(0.0d, 0.0d, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1", f = "WeatherManager.kt", l = {259}, m = "invokeSuspend")
    public static final class c extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sn5 sn5, double d, double d2, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = sn5;
            this.$lat = d;
            this.$lng = d2;
            this.$type = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new c(this.this$0, this.$lat, this.$lng, this.$type, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            throw null;
            //return ((c) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: double : 0x0025: IGET  (r4v0 double) = (r7v0 'this' com.fossil.sn5$c A[IMMUTABLE_TYPE, THIS]) com.fossil.sn5.c.$lat double), (',' char), (wrap: double : 0x002f: IGET  (r4v1 double) = (r7v0 'this' com.fossil.sn5$c A[IMMUTABLE_TYPE, THIS]) com.fossil.sn5.c.$lng double)] */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                GoogleApiService m = this.this$0.m();
                StringBuilder sb = new StringBuilder();
                sb.append(this.$lat);
                sb.append(',');
                sb.append(this.$lng);
                String sb2 = sb.toString();
                String str = this.$type;
                this.label = 1;
                Object addressWithType = m.getAddressWithType(sb2, str, this);
                return addressWithType == d ? d : addressWithType;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {287}, m = "getWeather")
    public static final class d extends co7 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(sn5 sn5, qn7 qn7) {
            super(qn7);
            this.this$0 = sn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(0.0d, 0.0d, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$getWeather$repoResponse$1", f = "WeatherManager.kt", l = {287}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(sn5 sn5, double d, double d2, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = sn5;
            this.$lat = d;
            this.$lng = d2;
            this.$tempUnit = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$lat, this.$lng, this.$tempUnit, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            throw null;
            //return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 j = this.this$0.j();
                double d2 = this.$lat;
                double d3 = this.$lng;
                String str = this.$tempUnit;
                this.label = 1;
                Object weather = j.getWeather(String.valueOf(d2), String.valueOf(d3), str, this);
                return weather == d ? d : weather;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $currentLocation;
        @DexIgnore
        public /* final */ /* synthetic */ zq7 $isFromCaches;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit$inlined;
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, double d, double d2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$currentLat, this.$currentLong, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    f fVar = this.this$0;
                    sn5 sn5 = fVar.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    String str = fVar.$tempUnit$inlined;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object p = sn5.p(d2, d3, str, this);
                    return p == d ? d : p;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, double d, double d2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$currentLat, this.$currentLong, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    sn5 sn5 = this.this$0.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object i2 = sn5.i(d2, d3, this);
                    return i2 == d ? d : i2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLat;
            @DexIgnore
            public /* final */ /* synthetic */ double $currentLong;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(f fVar, double d, double d2, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$currentLat = d;
                this.$currentLong = d2;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, this.$currentLat, this.$currentLong, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    sn5 sn5 = this.this$0.this$0;
                    double d2 = this.$currentLat;
                    double d3 = this.$currentLong;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object i2 = sn5.i(d2, d3, this);
                    return i2 == d ? d : i2;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(Location location, zq7 zq7, qn7 qn7, sn5 sn5, qn7 qn72, String str) {
            super(2, qn7);
            this.$currentLocation = location;
            this.$isFromCaches = zq7;
            this.this$0 = sn5;
            this.$continuation$inlined = qn72;
            this.$tempUnit$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.$currentLocation, this.$isFromCaches, qn7, this.this$0, this.$continuation$inlined, this.$tempUnit$inlined);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005f  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x006a  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x019b  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 457
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {374, 196, 201}, m = "getWeatherBaseOnLocation")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(sn5 sn5, qn7 qn7) {
            super(qn7);
            this.this$0 = sn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForChanceOfRain$2", f = "WeatherManager.kt", l = {156, 162, 166}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, qn7 qn7, h hVar) {
                super(2, qn7);
                this.$tempUnit = str;
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$tempUnit, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    sn5 sn5 = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object q = sn5.q("chance-of-rain", str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(sn5 sn5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sn5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$serial, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 264
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$2", f = "WeatherManager.kt", l = {175, 181, 185}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, qn7 qn7, i iVar) {
                super(2, qn7);
                this.$tempUnit = str;
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$tempUnit, qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    sn5 sn5 = this.this$0.this$0;
                    String str = this.$tempUnit;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object q = sn5.q("weather", str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(sn5 sn5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sn5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, this.$serial, qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 264
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$2", f = "WeatherManager.kt", l = {91, 92, 129, 137, 141}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ WeatherLocationWrapper $location;
            @DexIgnore
            public /* final */ /* synthetic */ String $tempUnit;
            @DexIgnore
            public /* final */ /* synthetic */ iv7 $this_withContext$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user$inlined;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ j this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(WeatherLocationWrapper weatherLocationWrapper, String str, qn7 qn7, MFUser mFUser, j jVar, iv7 iv7) {
                super(2, qn7);
                this.$location = weatherLocationWrapper;
                this.$tempUnit = str;
                this.$user$inlined = mFUser;
                this.this$0 = jVar;
                this.$this_withContext$inlined = iv7;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$location, this.$tempUnit, qn7, this.$user$inlined, this.this$0, this.$this_withContext$inlined);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Weather, ? extends Boolean>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object p;
                Object q;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    WeatherLocationWrapper weatherLocationWrapper = this.$location;
                    Boolean a2 = weatherLocationWrapper != null ? ao7.a(weatherLocationWrapper.isUseCurrentLocation()) : null;
                    if (a2 == null) {
                        pq7.i();
                        throw null;
                    } else if (a2.booleanValue()) {
                        sn5 sn5 = this.this$0.this$0;
                        String str = this.$tempUnit;
                        this.L$0 = iv7;
                        this.label = 1;
                        q = sn5.q("weather", str, this);
                        if (q == d) {
                            return d;
                        }
                        return (cl7) q;
                    } else {
                        sn5 sn52 = this.this$0.this$0;
                        double lat = this.$location.getLat();
                        double lng = this.$location.getLng();
                        String str2 = this.$tempUnit;
                        this.L$0 = iv7;
                        this.label = 2;
                        p = sn52.p(lat, lng, str2, this);
                        if (p == d) {
                            return d;
                        }
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    q = obj;
                    return (cl7) q;
                } else if (i == 2) {
                    iv7 iv73 = (iv7) this.L$0;
                    el7.b(obj);
                    p = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (cl7) p;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(sn5 sn5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sn5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, this.$serial, qn7);
            jVar.p$ = (iv7) obj;
            throw null;
            //return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r4v44, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r4v46, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r4v48, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:111:0x04d6  */
        /* JADX WARNING: Removed duplicated region for block: B:112:0x04d9  */
        /* JADX WARNING: Removed duplicated region for block: B:115:0x04f4  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00b3  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x012d  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x01af  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x01b9  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01cf  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x0219  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x02fe  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r25) {
            /*
            // Method dump skipped, instructions count: 1278
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.j.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {330}, m = "showChanceOfRain")
    public static final class k extends co7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(sn5 sn5, qn7 qn7) {
            super(qn7);
            this.this$0 = sn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$showChanceOfRain$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(sn5 sn5, int i, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sn5;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.this$0, this.$rainProbabilityPercent, qn7);
            lVar.p$ = (iv7) obj;
            throw null;
            //return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.k().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {316}, m = "showTemperature")
    public static final class m extends co7 {
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(sn5 sn5, qn7 qn7) {
            super(qn7);
            this.this$0 = sn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.WeatherManager$showTemperature$3", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class n extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $tempInCelsius;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(sn5 sn5, float f, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sn5;
            this.$tempInCelsius = f;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            n nVar = new n(this.this$0, this.$tempInCelsius, qn7);
            nVar.p$ = (iv7) obj;
            throw null;
            //return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((n) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.k().upsertCustomizeRealData(new CustomizeRealData("temperature", String.valueOf(this.$tempInCelsius)));
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = sn5.class.getSimpleName();
        pq7.b(simpleName, "WeatherManager::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public sn5() {
        this.j = w08.b(false, 1, null);
        PortfolioApp.h0.c().M().I1(this);
    }

    @DexIgnore
    public /* synthetic */ sn5(kq7 kq7) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(double r14, double r16, com.fossil.qn7<? super java.lang.String> r18) {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.i(double, double, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final ApiServiceV2 j() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        pq7.n("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository k() {
        CustomizeRealDataRepository customizeRealDataRepository = this.e;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        pq7.n("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final DianaAppSettingRepository l() {
        DianaAppSettingRepository dianaAppSettingRepository = this.f;
        if (dianaAppSettingRepository != null) {
            return dianaAppSettingRepository;
        }
        pq7.n("mDianaAppSettingRepository");
        throw null;
    }

    @DexIgnore
    public final GoogleApiService m() {
        GoogleApiService googleApiService = this.g;
        if (googleApiService != null) {
            return googleApiService;
        }
        pq7.n("mGoogleApiService");
        throw null;
    }

    @DexIgnore
    public final LocationSource n() {
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            return locationSource;
        }
        pq7.n("mLocationSource");
        throw null;
    }

    @DexIgnore
    public final UserRepository o() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(double r14, double r16, java.lang.String r18, com.fossil.qn7<? super com.fossil.cl7<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r19) {
        /*
            r13 = this;
            r0 = r19
            boolean r2 = r0 instanceof com.fossil.sn5.d
            if (r2 == 0) goto L_0x007b
            r2 = r19
            com.fossil.sn5$d r2 = (com.fossil.sn5.d) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r4 & r3
            if (r4 == 0) goto L_0x007b
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = r3 + r4
            r2.label = r3
            r10 = r2
        L_0x0017:
            java.lang.Object r3 = r10.result
            java.lang.Object r11 = com.fossil.yn7.d()
            int r2 = r10.label
            if (r2 == 0) goto L_0x008c
            r4 = 1
            if (r2 != r4) goto L_0x0084
            java.lang.Object r2 = r10.L$1
            java.lang.String r2 = (java.lang.String) r2
            double r4 = r10.D$1
            double r4 = r10.D$0
            java.lang.Object r2 = r10.L$0
            com.fossil.sn5 r2 = (com.fossil.sn5) r2
            com.fossil.el7.b(r3)
            r2 = r3
        L_0x0034:
            com.fossil.iq5 r2 = (com.fossil.iq5) r2
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.fossil.sn5.k
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "getWeather onResponse: response = "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            boolean r3 = r2 instanceof com.fossil.kq5
            if (r3 == 0) goto L_0x00be
            com.fossil.kq5 r2 = (com.fossil.kq5) r2
            java.lang.Object r3 = r2.a()
            if (r3 == 0) goto L_0x00b2
            com.fossil.oq5 r3 = new com.fossil.oq5
            r3.<init>()
            java.lang.Object r2 = r2.a()
            com.fossil.gj4 r2 = (com.fossil.gj4) r2
            r3.b(r2)
            com.fossil.cl7 r2 = new com.fossil.cl7
            com.portfolio.platform.data.model.microapp.weather.Weather r3 = r3.a()
            r4 = 0
            java.lang.Boolean r4 = com.fossil.ao7.a(r4)
            r2.<init>(r3, r4)
        L_0x007a:
            return r2
        L_0x007b:
            com.fossil.sn5$d r2 = new com.fossil.sn5$d
            r0 = r19
            r2.<init>(r13, r0)
            r10 = r2
            goto L_0x0017
        L_0x0084:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "call to 'resume' before 'invoke' with coroutine"
            r2.<init>(r3)
            throw r2
        L_0x008c:
            com.fossil.el7.b(r3)
            com.fossil.sn5$e r2 = new com.fossil.sn5$e
            r9 = 0
            r3 = r13
            r4 = r14
            r6 = r16
            r8 = r18
            r2.<init>(r3, r4, r6, r8, r9)
            r10.L$0 = r13
            r10.D$0 = r14
            r0 = r16
            r10.D$1 = r0
            r0 = r18
            r10.L$1 = r0
            r3 = 1
            r10.label = r3
            java.lang.Object r2 = com.fossil.jq5.d(r2, r10)
            if (r2 != r11) goto L_0x0034
            r2 = r11
            goto L_0x007a
        L_0x00b2:
            com.fossil.cl7 r2 = new com.fossil.cl7
            r3 = 0
            r4 = 0
            java.lang.Boolean r4 = com.fossil.ao7.a(r4)
            r2.<init>(r3, r4)
            goto L_0x007a
        L_0x00be:
            boolean r2 = r2 instanceof com.fossil.hq5
            if (r2 == 0) goto L_0x00ce
            com.fossil.cl7 r2 = new com.fossil.cl7
            r3 = 0
            r4 = 0
            java.lang.Boolean r4 = com.fossil.ao7.a(r4)
            r2.<init>(r3, r4)
            goto L_0x007a
        L_0x00ce:
            com.fossil.al7 r2 = new com.fossil.al7
            r2.<init>()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.p(double, double, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[Catch:{ all -> 0x01ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f2 A[SYNTHETIC, Splitter:B:44:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0158 A[Catch:{ all -> 0x01d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x017f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(java.lang.String r17, java.lang.String r18, com.fossil.qn7<? super com.fossil.cl7<com.portfolio.platform.data.model.microapp.weather.Weather, java.lang.Boolean>> r19) {
        /*
        // Method dump skipped, instructions count: 501
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.q(java.lang.String, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final Object r(String str, qn7<? super tl7> qn7) {
        return eu7.g(bw7.a(), new h(this, str, null), qn7);
    }

    @DexIgnore
    public final Object s(String str, qn7<? super tl7> qn7) {
        return eu7.g(bw7.a(), new i(this, str, null), qn7);
    }

    @DexIgnore
    public final Object t(String str, qn7<? super tl7> qn7) {
        return eu7.g(bw7.b(), new j(this, str, null), qn7);
    }

    @DexIgnore
    public final boolean u(AddressOfWeather addressOfWeather, double d2, double d3) {
        Location location = new Location("LocationA");
        location.setLatitude(addressOfWeather.getLat());
        location.setLongitude(addressOfWeather.getLng());
        Location location2 = new Location("LocationB");
        location2.setLatitude(d2);
        location2.setLongitude(d3);
        float distanceTo = location.distanceTo(location2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "distance between two address=" + distanceTo);
        return distanceTo < ((float) VideoUploader.RETRY_DELAY_UNIT_MS);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object v(com.portfolio.platform.data.model.microapp.weather.Weather r9, boolean r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
            r8 = this;
            r7 = 0
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.fossil.sn5.k
            if (r0 == 0) goto L_0x0051
            r0 = r11
            com.fossil.sn5$k r0 = (com.fossil.sn5.k) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0051
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0060
            if (r3 != r6) goto L_0x0058
            java.lang.Object r0 = r1.L$2
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r0 = (com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo) r0
            int r0 = r1.I$0
            boolean r10 = r1.Z$0
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.microapp.weather.Weather r0 = (com.portfolio.platform.data.model.microapp.weather.Weather) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.sn5 r0 = (com.fossil.sn5) r0
            com.fossil.el7.b(r2)
        L_0x0034:
            com.fossil.ck5$a r1 = com.fossil.ck5.f
            java.lang.String r2 = "chance-of-rain"
            com.fossil.ul5 r1 = r1.f(r2)
            if (r1 == 0) goto L_0x0047
            java.lang.String r0 = r0.h
            if (r0 == 0) goto L_0x00c3
            java.lang.String r2 = ""
            r1.d(r0, r10, r2)
        L_0x0047:
            com.fossil.ck5$a r0 = com.fossil.ck5.f
            java.lang.String r1 = "chance-of-rain"
            r0.k(r1)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0050:
            return r0
        L_0x0051:
            com.fossil.sn5$k r0 = new com.fossil.sn5$k
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0060:
            com.fossil.el7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.sn5.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "showChanceOfRain - probability="
            r4.append(r5)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r5 = r9.getCurrently()
            float r5 = r5.getRainProbability()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.portfolio.platform.data.model.microapp.weather.Weather$Currently r2 = r9.getCurrently()
            float r2 = r2.getRainProbability()
            r3 = 100
            float r3 = (float) r3
            float r2 = r2 * r3
            int r2 = (int) r2
            com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo r3 = r9.toChanceOfRainComplicationAppInfo()
            java.lang.String r4 = r8.h
            if (r4 == 0) goto L_0x00a5
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            r5.g1(r3, r4)
        L_0x00a5:
            com.fossil.dv7 r4 = com.fossil.bw7.a()
            com.fossil.sn5$l r5 = new com.fossil.sn5$l
            r5.<init>(r8, r2, r7)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.Z$0 = r10
            r1.I$0 = r2
            r1.L$2 = r3
            r1.label = r6
            java.lang.Object r1 = com.fossil.eu7.g(r4, r5, r1)
            if (r1 == r0) goto L_0x0050
            r0 = r8
            goto L_0x0034
        L_0x00c3:
            com.fossil.pq7.i()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.v(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(com.portfolio.platform.data.model.microapp.weather.Weather r9, boolean r10, com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.sn5.w(com.portfolio.platform.data.model.microapp.weather.Weather, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void x(cl7<Weather, Boolean> cl7, cl7<Weather, Boolean> cl72, cl7<Weather, Boolean> cl73) {
        Weather first;
        Weather first2;
        Weather first3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "showWeatherWatchApp - firstWeather=" + cl7 + ", secondWeather=" + cl72 + ", thirdWeather=" + cl73);
        WeatherWatchAppInfo weatherWatchAppInfo = (cl7 == null || (first3 = cl7.getFirst()) == null) ? null : first3.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo2 = (cl72 == null || (first2 = cl72.getFirst()) == null) ? null : first2.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo3 = (cl73 == null || (first = cl73.getFirst()) == null) ? null : first.toWeatherWatchAppInfo();
        if (weatherWatchAppInfo == null) {
            return;
        }
        if (weatherWatchAppInfo != null) {
            PortfolioApp.h0.c().g1(new WeatherInfoWatchAppResponse(weatherWatchAppInfo, weatherWatchAppInfo2, weatherWatchAppInfo3), PortfolioApp.h0.c().J());
            ul5 f2 = ck5.f.f("weather");
            if (f2 != null) {
                String str2 = this.h;
                if (str2 != null) {
                    Boolean second = cl7 != null ? cl7.getSecond() : null;
                    if (second != null) {
                        f2.d(str2, second.booleanValue(), "");
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            ck5.f.k("weather");
            return;
        }
        pq7.i();
        throw null;
    }
}
