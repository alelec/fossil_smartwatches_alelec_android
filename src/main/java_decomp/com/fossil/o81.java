package com.fossil;

import android.graphics.drawable.Drawable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class O81 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends O81 {
        @DexIgnore
        public Ai(Drawable drawable) {
            super(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends O81 {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Drawable drawable, boolean z) {
            super(null);
            Wg6.c(drawable, ResourceManager.DRAWABLE);
        }
    }

    @DexIgnore
    public O81() {
    }

    @DexIgnore
    public /* synthetic */ O81(Qg6 qg6) {
        this();
    }
}
