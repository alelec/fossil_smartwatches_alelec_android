package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cg2 implements Parcelable.Creator<B62> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ B62 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                str = Ad2.f(parcel, t);
            } else if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                j = Ad2.y(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new B62(str, i, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ B62[] newArray(int i) {
        return new B62[i];
    }
}
