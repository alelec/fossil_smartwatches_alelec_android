package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B81 implements G81 {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public B81(Context context) {
        Wg6.c(context, "context");
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.G81
    public Object b(Xe6<? super F81> xe6) {
        Resources resources = this.b.getResources();
        Wg6.b(resources, "context.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return new C81(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
}
