package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Aq3 b;

    @DexIgnore
    public Eq3(Aq3 aq3) {
        this.b = aq3;
    }

    @DexIgnore
    public final void run() {
        Fp3 fp3 = this.b.c;
        Context e = this.b.c.e();
        this.b.c.b();
        fp3.E(new ComponentName(e, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
