package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jf0 extends Drawable implements Drawable.Callback {
    @DexIgnore
    public Drawable b;

    @DexIgnore
    public Jf0(Drawable drawable) {
        b(drawable);
    }

    @DexIgnore
    public Drawable a() {
        return this.b;
    }

    @DexIgnore
    public void b(Drawable drawable) {
        Drawable drawable2 = this.b;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.b = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.b.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return this.b.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.b.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.b.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.b.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.b.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.b.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.b.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.b.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return Am0.h(this.b);
    }

    @DexIgnore
    public boolean isStateful() {
        return this.b.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        this.b.jumpToCurrentState();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.b.setBounds(rect);
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.b.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.b.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Am0.j(this.b, z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.b.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.b.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.b.setFilterBitmap(z);
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        Am0.k(this.b, f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        Am0.l(this.b, i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return this.b.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        Am0.n(this.b, i);
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Am0.o(this.b, colorStateList);
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Am0.p(this.b, mode);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.b.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
