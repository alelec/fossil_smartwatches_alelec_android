package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnknownNullness"})
public abstract class Ar0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList c;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;

        @DexIgnore
        public Ai(Ar0 ar0, int i, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4) {
            this.b = i;
            this.c = arrayList;
            this.d = arrayList2;
            this.e = arrayList3;
            this.f = arrayList4;
        }

        @DexIgnore
        public void run() {
            for (int i = 0; i < this.b; i++) {
                Mo0.D0((View) this.c.get(i), (String) this.d.get(i));
                Mo0.D0((View) this.e.get(i), (String) this.f.get(i));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ Map c;

        @DexIgnore
        public Bi(Ar0 ar0, ArrayList arrayList, Map map) {
            this.b = arrayList;
            this.c = map;
        }

        @DexIgnore
        public void run() {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                View view = (View) this.b.get(i);
                String H = Mo0.H(view);
                if (H != null) {
                    Mo0.D0(view, Ar0.i(this.c, H));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ Map c;

        @DexIgnore
        public Ci(Ar0 ar0, ArrayList arrayList, Map map) {
            this.b = arrayList;
            this.c = map;
        }

        @DexIgnore
        public void run() {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                View view = (View) this.b.get(i);
                Mo0.D0(view, (String) this.c.get(Mo0.H(view)));
            }
        }
    }

    @DexIgnore
    public static void d(List<View> list, View view) {
        int size = list.size();
        if (!h(list, view, size)) {
            list.add(view);
            for (int i = size; i < list.size(); i++) {
                View view2 = list.get(i);
                if (view2 instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view2;
                    int childCount = viewGroup.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        View childAt = viewGroup.getChildAt(i2);
                        if (!h(list, childAt, size)) {
                            list.add(childAt);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public static boolean h(List<View> list, View view, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (list.get(i2) == view) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static String i(Map<String, String> map, String str) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (str.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    @DexIgnore
    public static boolean l(List list) {
        return list == null || list.isEmpty();
    }

    @DexIgnore
    public abstract void A(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    @DexIgnore
    public abstract Object B(Object obj);

    @DexIgnore
    public abstract void a(Object obj, View view);

    @DexIgnore
    public abstract void b(Object obj, ArrayList<View> arrayList);

    @DexIgnore
    public abstract void c(ViewGroup viewGroup, Object obj);

    @DexIgnore
    public abstract boolean e(Object obj);

    @DexIgnore
    public void f(ArrayList<View> arrayList, View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (Oo0.a(viewGroup)) {
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                f(arrayList, viewGroup.getChildAt(i));
            }
            return;
        }
        arrayList.add(view);
    }

    @DexIgnore
    public abstract Object g(Object obj);

    @DexIgnore
    public void j(Map<String, View> map, View view) {
        if (view.getVisibility() == 0) {
            String H = Mo0.H(view);
            if (H != null) {
                map.put(H, view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    j(map, viewGroup.getChildAt(i));
                }
            }
        }
    }

    @DexIgnore
    public void k(View view, Rect rect) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
    }

    @DexIgnore
    public abstract Object m(Object obj, Object obj2, Object obj3);

    @DexIgnore
    public abstract Object n(Object obj, Object obj2, Object obj3);

    @DexIgnore
    public ArrayList<String> o(ArrayList<View> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            View view = arrayList.get(i);
            arrayList2.add(Mo0.H(view));
            Mo0.D0(view, null);
        }
        return arrayList2;
    }

    @DexIgnore
    public abstract void p(Object obj, View view);

    @DexIgnore
    public abstract void q(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    @DexIgnore
    public abstract void r(Object obj, View view, ArrayList<View> arrayList);

    @DexIgnore
    public void s(ViewGroup viewGroup, ArrayList<View> arrayList, Map<String, String> map) {
        Jo0.a(viewGroup, new Ci(this, arrayList, map));
    }

    @DexIgnore
    public abstract void t(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3);

    @DexIgnore
    public abstract void u(Object obj, Rect rect);

    @DexIgnore
    public abstract void v(Object obj, View view);

    @DexIgnore
    public void w(Fragment fragment, Object obj, Om0 om0, Runnable runnable) {
        runnable.run();
    }

    @DexIgnore
    public void x(View view, ArrayList<View> arrayList, Map<String, String> map) {
        Jo0.a(view, new Bi(this, arrayList, map));
    }

    @DexIgnore
    public void y(View view, ArrayList<View> arrayList, ArrayList<View> arrayList2, ArrayList<String> arrayList3, Map<String, String> map) {
        int size = arrayList2.size();
        ArrayList arrayList4 = new ArrayList();
        for (int i = 0; i < size; i++) {
            View view2 = arrayList.get(i);
            String H = Mo0.H(view2);
            arrayList4.add(H);
            if (H != null) {
                Mo0.D0(view2, null);
                String str = map.get(H);
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    } else if (str.equals(arrayList3.get(i2))) {
                        Mo0.D0(arrayList2.get(i2), H);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
        Jo0.a(view, new Ai(this, size, arrayList2, arrayList3, arrayList, arrayList4));
    }

    @DexIgnore
    public abstract void z(Object obj, View view, ArrayList<View> arrayList);
}
