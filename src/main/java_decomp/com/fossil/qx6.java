package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Nw5;
import com.fossil.X37;
import com.mapped.AlertDialogFragment;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qx6 extends Ey6 implements Gq4, Nw5.Ai, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<X95> h;
    @DexIgnore
    public Hy6 i;
    @DexIgnore
    public Nw5 j;
    @DexIgnore
    public Ew5 k;
    @DexIgnore
    public List<Lc6<ShineDevice, String>> l; // = new ArrayList();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Qx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            Qx6 qx6 = new Qx6();
            qx6.setArguments(bundle);
            return qx6;
        }

        @DexIgnore
        public final String b() {
            String simpleName = Qx6.class.getSimpleName();
            Wg6.b(simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ X95 b;
        @DexIgnore
        public /* final */ /* synthetic */ Qx6 c;

        @DexIgnore
        public Bi(X95 x95, Qx6 qx6, Jh6 jh6) {
            this.b = x95;
            this.c = qx6;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getActivity(), 2130772008);
            this.b.s.startAnimation(loadAnimation);
            this.b.w.startAnimation(loadAnimation);
            this.b.q.startAnimation(loadAnimation);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Qx6 b;

        @DexIgnore
        public Ci(Qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Qx6.L6(this.b).r(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Qx6 b;

        @DexIgnore
        public Di(Qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Qx6.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Qx6 b;

        @DexIgnore
        public Ei(Qx6 qx6) {
            this.b = qx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (Qx6.M6(this.b).w() < this.b.l.size()) {
                Qx6 qx6 = this.b;
                qx6.O6((ShineDevice) ((Lc6) qx6.l.get(Qx6.M6(this.b).w())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ Hy6 L6(Qx6 qx6) {
        Hy6 hy6 = qx6.i;
        if (hy6 != null) {
            return hy6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Ew5 M6(Qx6 qx6) {
        Ew5 ew5 = qx6.k;
        if (ew5 != null) {
            return ew5;
        }
        Wg6.n("mSnapHelper");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        P6((Hy6) obj);
    }

    @DexIgnore
    public final void O6(ShineDevice shineDevice) {
        String serial = shineDevice.getSerial();
        Wg6.b(serial, "device.serial");
        if (serial.length() > 0) {
            Hy6 hy6 = this.i;
            if (hy6 != null) {
                hy6.A(shineDevice);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.v(childFragmentManager);
        }
    }

    @DexIgnore
    public void P6(Hy6 hy6) {
        Wg6.c(hy6, "presenter");
        this.i = hy6;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() == -2084521848 && str.equals("DOWNLOAD") && i2 == 2131363373) {
            Hy6 hy6 = this.i;
            if (hy6 != null) {
                List<Lc6<ShineDevice, String>> list = this.l;
                Ew5 ew5 = this.k;
                if (ew5 != null) {
                    hy6.u(list.get(ew5.w()).getFirst());
                } else {
                    Wg6.n("mSnapHelper");
                    throw null;
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public Animation onCreateAnimation(int i2, boolean z, int i3) {
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        if (z) {
            G37<X95> g37 = this.h;
            if (g37 != null) {
                X95 a2 = g37.a();
                if (!(a2 == null || getActivity() == null)) {
                    T t = (T) AnimationUtils.loadAnimation(getActivity(), 2130771999);
                    jh6.element = t;
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAnimationListener(new Bi(a2, this, jh6));
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        return jh6.element;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        X95 x95 = (X95) Aq0.f(layoutInflater, 2131558603, viewGroup, false, A6());
        this.h = new G37<>(this, x95);
        Wg6.b(x95, "binding");
        return x95.n();
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DashBar dashBar;
        FlexibleButton flexibleButton;
        FlexibleTextView flexibleTextView;
        RecyclerView recyclerView;
        FlexibleTextView flexibleTextView2;
        RTLImageView rTLImageView;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<X95> g37 = this.h;
        if (g37 != null) {
            X95 a2 = g37.a();
            if (!(a2 == null || (rTLImageView = a2.t) == null)) {
                rTLImageView.setOnClickListener(new Ci(this));
            }
            G37<X95> g372 = this.h;
            if (g372 != null) {
                X95 a3 = g372.a();
                if (!(a3 == null || (flexibleTextView2 = a3.s) == null)) {
                    Hr7 hr7 = Hr7.a;
                    Locale locale = Locale.US;
                    Wg6.b(locale, "Locale.US");
                    String c = Um5.c(getContext(), 2131886943);
                    Wg6.b(c, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(this.l.size())}, 1));
                    Wg6.b(format, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView2.setText(format);
                }
                Wa1 v = Oa1.v(this);
                Wg6.b(v, "Glide.with(this)");
                Nw5 nw5 = new Nw5(v, this);
                this.j = nw5;
                nw5.k(this.l);
                G37<X95> g373 = this.h;
                if (g373 != null) {
                    X95 a4 = g373.a();
                    if (!(a4 == null || (recyclerView = a4.w) == null)) {
                        Wg6.b(recyclerView, "it");
                        recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.get.instance().getApplicationContext(), 0, false));
                        recyclerView.addItemDecoration(new H67());
                        recyclerView.setAdapter(this.j);
                        Ew5 ew5 = new Ew5(null, 1, null);
                        this.k = ew5;
                        ew5.b(recyclerView);
                    }
                    G37<X95> g374 = this.h;
                    if (g374 != null) {
                        X95 a5 = g374.a();
                        if (!(a5 == null || (flexibleTextView = a5.r) == null)) {
                            flexibleTextView.setOnClickListener(new Di(this));
                        }
                        G37<X95> g375 = this.h;
                        if (g375 != null) {
                            X95 a6 = g375.a();
                            if (!(a6 == null || (flexibleButton = a6.q) == null)) {
                                flexibleButton.setOnClickListener(new Ei(this));
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                G37<X95> g376 = this.h;
                                if (g376 != null) {
                                    X95 a7 = g376.a();
                                    if (a7 != null && (dashBar = a7.u) != null) {
                                        X37.Ai ai = X37.a;
                                        Wg6.b(dashBar, "this");
                                        ai.b(dashBar, z, 500);
                                        return;
                                    }
                                    return;
                                }
                                Wg6.n("mBinding");
                                throw null;
                            }
                            return;
                        }
                        Wg6.n("mBinding");
                        throw null;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Nw5.Ai
    public void p1(View view, Nw5.Bi bi, int i2) {
        RecyclerView recyclerView;
        Wg6.c(view, "view");
        Wg6.c(bi, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String b = s.b();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i2);
        sb.append(", mSnappedPos=");
        Ew5 ew5 = this.k;
        if (ew5 != null) {
            sb.append(ew5.w());
            local.d(b, sb.toString());
            Ew5 ew52 = this.k;
            if (ew52 == null) {
                Wg6.n("mSnapHelper");
                throw null;
            } else if (ew52.w() != i2) {
                G37<X95> g37 = this.h;
                if (g37 != null) {
                    X95 a2 = g37.a();
                    if (a2 != null && (recyclerView = a2.w) != null) {
                        Ew5 ew53 = this.k;
                        if (ew53 != null) {
                            recyclerView.smoothScrollBy((i2 - ew53.w()) * view.getWidth(), view.getHeight());
                        } else {
                            Wg6.n("mSnapHelper");
                            throw null;
                        }
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                List<Lc6<ShineDevice, String>> list = this.l;
                Ew5 ew54 = this.k;
                if (ew54 != null) {
                    O6(list.get(ew54.w()).getFirst());
                } else {
                    Wg6.n("mSnapHelper");
                    throw null;
                }
            }
        } else {
            Wg6.n("mSnapHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void t2(List<Lc6<ShineDevice, String>> list) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        Wg6.c(list, "shineDeviceList");
        this.l = list;
        Nw5 nw5 = this.j;
        if (nw5 != null) {
            nw5.k(list);
            int size = this.l.size();
            if (size <= 1) {
                G37<X95> g37 = this.h;
                if (g37 != null) {
                    X95 a2 = g37.a();
                    if (a2 != null && (flexibleTextView2 = a2.s) != null) {
                        Hr7 hr7 = Hr7.a;
                        Locale locale = Locale.US;
                        Wg6.b(locale, "Locale.US");
                        String c = Um5.c(getActivity(), 2131886943);
                        Wg6.b(c, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        String format = String.format(locale, c, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                        Wg6.b(format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView2.setText(format);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            G37<X95> g372 = this.h;
            if (g372 != null) {
                X95 a3 = g372.a();
                if (a3 != null && (flexibleTextView = a3.s) != null) {
                    Hr7 hr72 = Hr7.a;
                    Locale locale2 = Locale.US;
                    Wg6.b(locale2, "Locale.US");
                    String c2 = Um5.c(getActivity(), 2131886943);
                    Wg6.b(c2, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                    String format2 = String.format(locale2, c2, Arrays.copyOf(new Object[]{Integer.valueOf(size)}, 1));
                    Wg6.b(format2, "java.lang.String.format(locale, format, *args)");
                    flexibleTextView.setText(format2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
