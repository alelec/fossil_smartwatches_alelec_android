package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y42 extends A52<Status> {
    @DexIgnore
    public Y42(R62 r62) {
        super(r62);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ Z62 f(Status status) {
        return status;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(U42 u42) throws RemoteException {
        U42 u422 = u42;
        ((G52) u422.I()).d1(new Z42(this), u422.t0());
    }
}
