package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X43 implements Y43 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.androidId.delete_feature", true);
        hw2.d("measurement.log_androidId_enabled", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.Y43
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
