package com.fossil;

import com.mapped.Wg6;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T48 implements A58 {
    @DexIgnore
    public /* final */ OutputStream b;
    @DexIgnore
    public /* final */ D58 c;

    @DexIgnore
    public T48(OutputStream outputStream, D58 d58) {
        Wg6.c(outputStream, "out");
        Wg6.c(d58, "timeout");
        this.b = outputStream;
        this.c = d58;
    }

    @DexIgnore
    @Override // com.fossil.A58
    public void K(I48 i48, long j) {
        Wg6.c(i48, "source");
        F48.b(i48.p0(), 0, j);
        while (j > 0) {
            this.c.f();
            X48 x48 = i48.b;
            if (x48 != null) {
                int min = (int) Math.min(j, (long) (x48.c - x48.b));
                this.b.write(x48.a, x48.b, min);
                x48.b += min;
                long j2 = (long) min;
                j -= j2;
                i48.o0(i48.p0() - j2);
                if (x48.b == x48.c) {
                    i48.b = x48.b();
                    Y48.c.a(x48);
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
    public void close() {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.A58
    public D58 e() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.A58, java.io.Flushable
    public void flush() {
        this.b.flush();
    }

    @DexIgnore
    public String toString() {
        return "sink(" + this.b + ')';
    }
}
