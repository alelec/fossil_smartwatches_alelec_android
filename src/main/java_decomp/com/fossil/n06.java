package com.fossil;

import com.mapped.AppWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface N06 extends Rv5<M06> {
    @DexIgnore
    void Q0(boolean z);

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Object close();  // void declaration

    @DexIgnore
    void s2(List<AppWrapper> list);

    @DexIgnore
    Object u();  // void declaration
}
