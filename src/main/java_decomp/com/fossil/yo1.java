package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yo1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ap1 b;
    @DexIgnore
    public vw1 c;
    @DexIgnore
    public /* final */ xw1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<yo1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public yo1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                ap1 valueOf = ap1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (ub.f3558a[valueOf.ordinal()]) {
                    case 1:
                        return so1.CREATOR.a(parcel);
                    case 2:
                        return cp1.CREATOR.a(parcel);
                    case 3:
                        return dp1.CREATOR.a(parcel);
                    case 4:
                        return uo1.CREATOR.a(parcel);
                    case 5:
                        return vo1.CREATOR.a(parcel);
                    case 6:
                        return to1.CREATOR.a(parcel);
                    case 7:
                        return wo1.CREATOR.a(parcel);
                    case 8:
                        return po1.CREATOR.a(parcel);
                    case 9:
                        return xo1.CREATOR.a(parcel);
                    case 10:
                        return bp1.CREATOR.a(parcel);
                    case 11:
                        return ro1.CREATOR.a(parcel);
                    case 12:
                        return qo1.CREATOR.a(parcel);
                    default:
                        throw new al7();
                }
            } else {
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public yo1[] newArray(int i) {
            return new yo1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public yo1(android.os.Parcel r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.String r0 = r5.readString()
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.pq7.b(r0, r1)
            com.fossil.ap1 r1 = com.fossil.ap1.valueOf(r0)
            com.fossil.vw1[] r0 = com.fossil.vw1.values()
            int r2 = r5.readInt()
            r2 = r0[r2]
            java.lang.Class<com.fossil.xw1> r0 = com.fossil.xw1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r5.readParcelable(r0)
            if (r0 == 0) goto L_0x002c
            com.fossil.xw1 r0 = (com.fossil.xw1) r0
            r4.<init>(r1, r2, r0)
            return
        L_0x002c:
            com.fossil.pq7.i()
            throw r3
        L_0x0030:
            com.fossil.pq7.i()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yo1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public yo1(ap1 ap1, vw1 vw1, xw1 xw1) {
        this.b = ap1;
        this.c = vw1;
        this.d = xw1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ yo1(ap1 ap1, vw1 vw1, xw1 xw1, int i) {
        this(ap1, (i & 2) != 0 ? vw1.TOP_SHORT_PRESS_RELEASE : vw1, (i & 4) != 0 ? new tc0() : xw1);
    }

    @DexIgnore
    public JSONObject a() {
        return new JSONObject();
    }

    @DexIgnore
    public final void a(vw1 vw1) {
        this.c = vw1;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            yo1 yo1 = (yo1) obj;
            return this.b == yo1.b && this.c == yo1.c && !(pq7.a(this.d, yo1.d) ^ true);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchApp");
    }

    @DexIgnore
    public final vw1 getButtonEvent() {
        return this.c;
    }

    @DexIgnore
    public final ap1 getId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return (((hashCode * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.b.a()).put("button_evt", ey1.a(this.c));
        } catch (JSONException e) {
            d90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
