package com.fossil;

import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface E61<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static <T> boolean a(E61<T> e61, T t) {
            Wg6.c(t, "data");
            return true;
        }
    }

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    String b(T t);

    @DexIgnore
    Object c(G51 g51, T t, F81 f81, X51 x51, Xe6<? super D61> xe6);
}
