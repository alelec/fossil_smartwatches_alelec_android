package com.fossil;

import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eq6 implements MembersInjector<ProfileOptInActivity> {
    @DexIgnore
    public static void a(ProfileOptInActivity profileOptInActivity, ProfileOptInPresenter profileOptInPresenter) {
        profileOptInActivity.A = profileOptInPresenter;
    }
}
