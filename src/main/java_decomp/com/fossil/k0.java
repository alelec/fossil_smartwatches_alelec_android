package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Firmware;
import java.util.Arrays;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K0 {
    @DexIgnore
    @Vu3("id")
    public String a;
    @DexIgnore
    @Vu3("themeClass")
    public Ec0 b;
    @DexIgnore
    @Vu3("firmwareOSVersion")
    public Ry1 c;
    @DexIgnore
    @Vu3("checksum")
    public String d;
    @DexIgnore
    @Vu3(Firmware.COLUMN_DOWNLOAD_URL)
    public String e;
    @DexIgnore
    @Vu3("updatedAt")
    public Date f;
    @DexIgnore
    @Vu3("createdAt")
    public Date g;
    @DexIgnore
    @Vu3("data")
    public byte[] h;

    @DexIgnore
    public K0(String str, Ec0 ec0, Ry1 ry1, String str2, String str3, Date date, Date date2, byte[] bArr) {
        this.a = str;
        this.b = ec0;
        this.c = ry1;
        this.d = str2;
        this.e = str3;
        this.f = date;
        this.g = date2;
        this.h = bArr;
    }

    @DexIgnore
    public final Date a() {
        return this.g;
    }

    @DexIgnore
    public final Date b() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof K0)) {
            return false;
        }
        K0 k0 = (K0) obj;
        if (!Wg6.a(this.a, k0.a)) {
            return false;
        }
        if (this.b != k0.b) {
            return false;
        }
        if (!Wg6.a(this.c, k0.c)) {
            return false;
        }
        return !(Wg6.a(this.d, k0.d) ^ true);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder e2 = E.e("ThemeTemplateEntity(id=");
        e2.append(this.a);
        e2.append(", classifier=");
        e2.append(this.b);
        e2.append(", packageOSVersion=");
        e2.append(this.c);
        e2.append(", checksum=");
        e2.append(this.d);
        e2.append(", downloadUrl=");
        e2.append(this.e);
        e2.append(", updatedAt=");
        e2.append(this.f);
        e2.append(", createdAt=");
        e2.append(this.g);
        e2.append(", data=");
        e2.append(Arrays.toString(this.h));
        e2.append(")");
        return e2.toString();
    }
}
