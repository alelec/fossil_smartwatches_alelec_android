package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hf6 implements MembersInjector<ActivityOverviewFragment> {
    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewDayPresenter activityOverviewDayPresenter) {
        activityOverviewFragment.h = activityOverviewDayPresenter;
    }

    @DexIgnore
    public static void b(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
        activityOverviewFragment.j = activityOverviewMonthPresenter;
    }

    @DexIgnore
    public static void c(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
        activityOverviewFragment.i = activityOverviewWeekPresenter;
    }
}
