package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.migration.MigrationViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mv6 implements Factory<MigrationViewModel> {
    @DexIgnore
    public /* final */ Provider<MigrationHelper> a;
    @DexIgnore
    public /* final */ Provider<Cj4> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Mv6(Provider<MigrationHelper> provider, Provider<Cj4> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Mv6 a(Provider<MigrationHelper> provider, Provider<Cj4> provider2, Provider<An4> provider3) {
        return new Mv6(provider, provider2, provider3);
    }

    @DexIgnore
    public static MigrationViewModel c(MigrationHelper migrationHelper, Cj4 cj4, An4 an4) {
        return new MigrationViewModel(migrationHelper, cj4, an4);
    }

    @DexIgnore
    public MigrationViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
