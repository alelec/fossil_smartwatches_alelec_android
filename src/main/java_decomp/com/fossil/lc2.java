package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Lc2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Ql2 implements Lc2 {
        @DexIgnore
        public Ai() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        @DexIgnore
        @Override // com.fossil.Ql2
        public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                i1(parcel.readInt(), parcel.readStrongBinder(), (Bundle) Sl2.b(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                W0(parcel.readInt(), (Bundle) Sl2.b(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                d2(parcel.readInt(), parcel.readStrongBinder(), (Ee2) Sl2.b(parcel, Ee2.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    @DexIgnore
    void W0(int i, Bundle bundle) throws RemoteException;

    @DexIgnore
    void d2(int i, IBinder iBinder, Ee2 ee2) throws RemoteException;

    @DexIgnore
    void i1(int i, IBinder iBinder, Bundle bundle) throws RemoteException;
}
