package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W08 {
    @DexIgnore
    public static /* final */ Vz7 a; // = new Vz7("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ Vz7 b; // = new Vz7("LOCKED");
    @DexIgnore
    public static /* final */ Vz7 c; // = new Vz7("UNLOCKED");
    @DexIgnore
    public static /* final */ T08 d; // = new T08(b);
    @DexIgnore
    public static /* final */ T08 e; // = new T08(c);

    @DexIgnore
    public static final U08 a(boolean z) {
        return new V08(z);
    }

    @DexIgnore
    public static /* synthetic */ U08 b(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }
}
