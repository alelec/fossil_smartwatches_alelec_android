package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fd7 extends Vc7<ImageView> {
    @DexIgnore
    public Zc7 m;

    @DexIgnore
    public Fd7(Picasso picasso, ImageView imageView, Pd7 pd7, int i, int i2, int i3, Drawable drawable, String str, Object obj, Zc7 zc7, boolean z) {
        super(picasso, imageView, pd7, i, i2, i3, drawable, str, obj, z);
        this.m = zc7;
    }

    @DexIgnore
    @Override // com.fossil.Vc7
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Vc7
    public void b(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            T t = this.c.get();
            if (t != null) {
                Picasso picasso = this.a;
                Nd7.c(t, picasso.e, bitmap, loadedFrom, this.d, picasso.m);
                Zc7 zc7 = this.m;
                if (zc7 != null) {
                    zc7.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", this));
    }

    @DexIgnore
    @Override // com.fossil.Vc7
    public void c() {
        T t = this.c.get();
        if (t != null) {
            int i = this.g;
            if (i != 0) {
                t.setImageResource(i);
            } else {
                Drawable drawable = this.h;
                if (drawable != null) {
                    t.setImageDrawable(drawable);
                }
            }
            Zc7 zc7 = this.m;
            if (zc7 != null) {
                zc7.onError();
            }
        }
    }
}
