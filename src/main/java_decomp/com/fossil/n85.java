package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ View s;
    @DexIgnore
    public /* final */ RecyclerView t;
    @DexIgnore
    public /* final */ SwipeRefreshLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public N85(Object obj, View view, int i, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, View view2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = rTLImageView;
        this.s = view2;
        this.t = recyclerView;
        this.u = swipeRefreshLayout;
        this.v = flexibleTextView2;
    }
}
