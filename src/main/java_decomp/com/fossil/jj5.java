package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jj5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<T> {
    }

    @DexIgnore
    public static final <T> String a(T t) {
        String u = new Gson().u(t, new Ai().getType());
        Wg6.b(u, "Gson().toJson(this, type)");
        return u;
    }
}
