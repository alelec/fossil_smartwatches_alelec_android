package com.fossil;

import com.google.j2objc.annotations.Weak;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M44<E> extends S24<E> {
    @DexIgnore
    @Weak
    public /* final */ U24<E> delegate;
    @DexIgnore
    public /* final */ Y24<? extends E> delegateList;

    @DexIgnore
    public M44(U24<E> u24, Y24<? extends E> y24) {
        this.delegate = u24;
        this.delegateList = y24;
    }

    @DexIgnore
    public M44(U24<E> u24, Object[] objArr) {
        this(u24, Y24.asImmutableList(objArr));
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.Y24
    public int copyIntoArray(Object[] objArr, int i) {
        return this.delegateList.copyIntoArray(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.S24
    public U24<E> delegateCollection() {
        return this.delegate;
    }

    @DexIgnore
    public Y24<? extends E> delegateList() {
        return this.delegateList;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.delegateList.get(i);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.fossil.I54<? extends E>, com.fossil.I54<E> */
    @Override // java.util.List, com.fossil.Y24, com.fossil.Y24
    public I54<E> listIterator(int i) {
        return (I54<? extends E>) this.delegateList.listIterator(i);
    }
}
