package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.G04;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class R04 extends S04 {
    @DexIgnore
    public static /* final */ boolean o; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ TextWatcher d; // = new Ai();
    @DexIgnore
    public /* final */ TextInputLayout.e e; // = new Bi(this.a);
    @DexIgnore
    public /* final */ TextInputLayout.f f; // = new Ci();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public long i; // = Long.MAX_VALUE;
    @DexIgnore
    public StateListDrawable j;
    @DexIgnore
    public C04 k;
    @DexIgnore
    public AccessibilityManager l;
    @DexIgnore
    public ValueAnimator m;
    @DexIgnore
    public ValueAnimator n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements TextWatcher {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ AutoCompleteTextView b;

            @DexIgnore
            public Aii(AutoCompleteTextView autoCompleteTextView) {
                this.b = autoCompleteTextView;
            }

            @DexIgnore
            public void run() {
                boolean isPopupShowing = this.b.isPopupShowing();
                R04.this.z(isPopupShowing);
                R04.this.g = isPopupShowing;
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            R04 r04 = R04.this;
            AutoCompleteTextView u = r04.u(r04.a.getEditText());
            u.post(new Aii(u));
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends TextInputLayout.e {
        @DexIgnore
        public Bi(TextInputLayout textInputLayout) {
            super(textInputLayout);
        }

        @DexIgnore
        @Override // com.fossil.Rn0, com.google.android.material.textfield.TextInputLayout.e
        public void g(View view, Yo0 yo0) {
            super.g(view, yo0);
            yo0.c0(Spinner.class.getName());
            if (yo0.M()) {
                yo0.o0(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void h(View view, AccessibilityEvent accessibilityEvent) {
            super.h(view, accessibilityEvent);
            R04 r04 = R04.this;
            AutoCompleteTextView u = r04.u(r04.a.getEditText());
            if (accessibilityEvent.getEventType() == 1 && R04.this.l.isTouchExplorationEnabled()) {
                R04.this.C(u);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements TextInputLayout.f {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            AutoCompleteTextView u = R04.this.u(textInputLayout.getEditText());
            R04.this.A(u);
            R04.this.r(u);
            R04.this.B(u);
            u.setThreshold(0);
            u.removeTextChangedListener(R04.this.d);
            u.addTextChangedListener(R04.this.d);
            textInputLayout.setErrorIconDrawable((Drawable) null);
            textInputLayout.setTextInputAccessibilityDelegate(R04.this.e);
            textInputLayout.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements View.OnClickListener {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void onClick(View view) {
            R04.this.C((AutoCompleteTextView) R04.this.a.getEditText());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ AutoCompleteTextView b;

        @DexIgnore
        public Ei(AutoCompleteTextView autoCompleteTextView) {
            this.b = autoCompleteTextView;
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                if (R04.this.y()) {
                    R04.this.g = false;
                }
                R04.this.C(this.b);
                view.performClick();
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements View.OnFocusChangeListener {
        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            R04.this.a.setEndIconActivated(z);
            if (!z) {
                R04.this.z(false);
                R04.this.g = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements AutoCompleteTextView.OnDismissListener {
        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        public void onDismiss() {
            R04.this.g = true;
            R04.this.i = System.currentTimeMillis();
            R04.this.z(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends AnimatorListenerAdapter {
        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            R04 r04 = R04.this;
            r04.c.setChecked(r04.h);
            R04.this.n.start();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public Ii() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            R04.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore
    public R04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public final void A(AutoCompleteTextView autoCompleteTextView) {
        if (o) {
            int boxBackgroundMode = this.a.getBoxBackgroundMode();
            if (boxBackgroundMode == 2) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.k);
            } else if (boxBackgroundMode == 1) {
                autoCompleteTextView.setDropDownBackgroundDrawable(this.j);
            }
        }
    }

    @DexIgnore
    public final void B(AutoCompleteTextView autoCompleteTextView) {
        autoCompleteTextView.setOnTouchListener(new Ei(autoCompleteTextView));
        autoCompleteTextView.setOnFocusChangeListener(new Fi());
        if (o) {
            autoCompleteTextView.setOnDismissListener(new Gi());
        }
    }

    @DexIgnore
    public final void C(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView != null) {
            if (y()) {
                this.g = false;
            }
            if (!this.g) {
                if (o) {
                    z(!this.h);
                } else {
                    this.h = !this.h;
                    this.c.toggle();
                }
                if (this.h) {
                    autoCompleteTextView.requestFocus();
                    autoCompleteTextView.showDropDown();
                    return;
                }
                autoCompleteTextView.dismissDropDown();
                return;
            }
            this.g = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.S04
    public void a() {
        float dimensionPixelOffset = (float) this.b.getResources().getDimensionPixelOffset(Lw3.mtrl_shape_corner_size_small_component);
        float dimensionPixelOffset2 = (float) this.b.getResources().getDimensionPixelOffset(Lw3.mtrl_exposed_dropdown_menu_popup_elevation);
        int dimensionPixelOffset3 = this.b.getResources().getDimensionPixelOffset(Lw3.mtrl_exposed_dropdown_menu_popup_vertical_padding);
        C04 w = w(dimensionPixelOffset, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        C04 w2 = w(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, dimensionPixelOffset, dimensionPixelOffset2, dimensionPixelOffset3);
        this.k = w;
        StateListDrawable stateListDrawable = new StateListDrawable();
        this.j = stateListDrawable;
        stateListDrawable.addState(new int[]{16842922}, w);
        this.j.addState(new int[0], w2);
        this.a.setEndIconDrawable(Gf0.d(this.b, o ? Mw3.mtrl_dropdown_arrow : Mw3.mtrl_ic_arrow_drop_down));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(Rw3.exposed_dropdown_menu_content_description));
        this.a.setEndIconOnClickListener(new Di());
        this.a.c(this.f);
        x();
        Mo0.v0(this.c, 2);
        this.l = (AccessibilityManager) this.b.getSystemService("accessibility");
    }

    @DexIgnore
    @Override // com.fossil.S04
    public boolean b(int i2) {
        return i2 != 0;
    }

    @DexIgnore
    @Override // com.fossil.S04
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final void r(AutoCompleteTextView autoCompleteTextView) {
        if (autoCompleteTextView.getKeyListener() == null) {
            int boxBackgroundMode = this.a.getBoxBackgroundMode();
            C04 boxBackground = this.a.getBoxBackground();
            int c = Vx3.c(autoCompleteTextView, Jw3.colorControlHighlight);
            int[][] iArr = {new int[]{16842919}, new int[0]};
            if (boxBackgroundMode == 2) {
                t(autoCompleteTextView, c, iArr, boxBackground);
            } else if (boxBackgroundMode == 1) {
                s(autoCompleteTextView, c, iArr, boxBackground);
            }
        }
    }

    @DexIgnore
    public final void s(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, C04 c04) {
        int boxBackgroundColor = this.a.getBoxBackgroundColor();
        int[] iArr2 = {Vx3.f(i2, boxBackgroundColor, 0.1f), boxBackgroundColor};
        if (o) {
            Mo0.o0(autoCompleteTextView, new RippleDrawable(new ColorStateList(iArr, iArr2), c04, c04));
            return;
        }
        C04 c042 = new C04(c04.C());
        c042.V(new ColorStateList(iArr, iArr2));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{c04, c042});
        int E = Mo0.E(autoCompleteTextView);
        int paddingTop = autoCompleteTextView.getPaddingTop();
        int D = Mo0.D(autoCompleteTextView);
        int paddingBottom = autoCompleteTextView.getPaddingBottom();
        Mo0.o0(autoCompleteTextView, layerDrawable);
        Mo0.A0(autoCompleteTextView, E, paddingTop, D, paddingBottom);
    }

    @DexIgnore
    public final void t(AutoCompleteTextView autoCompleteTextView, int i2, int[][] iArr, C04 c04) {
        LayerDrawable layerDrawable;
        int c = Vx3.c(autoCompleteTextView, Jw3.colorSurface);
        C04 c042 = new C04(c04.C());
        int f2 = Vx3.f(i2, c, 0.1f);
        c042.V(new ColorStateList(iArr, new int[]{f2, 0}));
        if (o) {
            c042.setTint(c);
            ColorStateList colorStateList = new ColorStateList(iArr, new int[]{f2, c});
            C04 c043 = new C04(c04.C());
            c043.setTint(-1);
            layerDrawable = new LayerDrawable(new Drawable[]{new RippleDrawable(colorStateList, c042, c043), c04});
        } else {
            layerDrawable = new LayerDrawable(new Drawable[]{c042, c04});
        }
        Mo0.o0(autoCompleteTextView, layerDrawable);
    }

    @DexIgnore
    public final AutoCompleteTextView u(EditText editText) {
        if (editText instanceof AutoCompleteTextView) {
            return (AutoCompleteTextView) editText;
        }
        throw new RuntimeException("EditText needs to be an AutoCompleteTextView if an Exposed Dropdown Menu is being used.");
    }

    @DexIgnore
    public final ValueAnimator v(int i2, float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(Uw3.a);
        ofFloat.setDuration((long) i2);
        ofFloat.addUpdateListener(new Ii());
        return ofFloat;
    }

    @DexIgnore
    public final C04 w(float f2, float f3, float f4, int i2) {
        G04.Bi a2 = G04.a();
        a2.A(f2);
        a2.E(f2);
        a2.r(f3);
        a2.v(f3);
        G04 m2 = a2.m();
        C04 l2 = C04.l(this.b, f4);
        l2.setShapeAppearanceModel(m2);
        l2.X(0, i2, 0, i2);
        return l2;
    }

    @DexIgnore
    public final void x() {
        this.n = v(67, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        ValueAnimator v = v(50, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.m = v;
        v.addListener(new Hi());
    }

    @DexIgnore
    public final boolean y() {
        long currentTimeMillis = System.currentTimeMillis() - this.i;
        return currentTimeMillis < 0 || currentTimeMillis > 300;
    }

    @DexIgnore
    public final void z(boolean z) {
        if (this.h != z) {
            this.h = z;
            this.n.cancel();
            this.m.start();
        }
    }
}
