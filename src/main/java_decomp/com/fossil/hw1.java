package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.Format;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hw1 extends Fw1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hw1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hw1 createFromParcel(Parcel parcel) {
            return new Hw1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hw1[] newArray(int i) {
            return new Hw1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Hw1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Hw1(String str, byte[] bArr) {
        super(str, bArr, Format.RAW);
    }

    @DexIgnore
    @Override // com.fossil.Fw1, com.fossil.Fw1, java.lang.Object
    public Hw1 clone() {
        String name = getName();
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Hw1(name, copyOf);
    }
}
