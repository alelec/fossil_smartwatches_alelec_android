package com.fossil;

import com.fossil.F38;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J38 implements Closeable {
    @DexIgnore
    public static /* final */ Logger f; // = Logger.getLogger(G38.class.getName());
    @DexIgnore
    public /* final */ K48 b;
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ F38.Ai e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements C58 {
        @DexIgnore
        public /* final */ K48 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public byte d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public short g;

        @DexIgnore
        public Ai(K48 k48) {
            this.b = k48;
        }

        @DexIgnore
        public final void a() throws IOException {
            int i = this.e;
            int l = J38.l(this.b);
            this.f = l;
            this.c = l;
            byte readByte = (byte) (this.b.readByte() & 255);
            this.d = (byte) ((byte) (this.b.readByte() & 255));
            if (J38.f.isLoggable(Level.FINE)) {
                J38.f.fine(G38.b(true, this.e, this.c, readByte, this.d));
            }
            int readInt = this.b.readInt() & Integer.MAX_VALUE;
            this.e = readInt;
            if (readByte != 9) {
                G38.d("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (readInt != i) {
                G38.d("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
        }

        @DexIgnore
        @Override // com.fossil.C58
        public long d0(I48 i48, long j) throws IOException {
            while (true) {
                int i = this.f;
                if (i == 0) {
                    this.b.skip((long) this.g);
                    this.g = (short) 0;
                    if ((this.d & 4) != 0) {
                        return -1;
                    }
                    a();
                } else {
                    long d0 = this.b.d0(i48, Math.min(j, (long) i));
                    if (d0 == -1) {
                        return -1;
                    }
                    this.f = (int) (((long) this.f) - d0);
                    return d0;
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public D58 e() {
            return this.b.e();
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(boolean z, O38 o38);

        @DexIgnore
        void c(boolean z, int i, int i2, List<E38> list);

        @DexIgnore
        void d(int i, long j);

        @DexIgnore
        void e(boolean z, int i, K48 k48, int i2) throws IOException;

        @DexIgnore
        void f(boolean z, int i, int i2);

        @DexIgnore
        void g(int i, int i2, int i3, boolean z);

        @DexIgnore
        void h(int i, D38 d38);

        @DexIgnore
        void i(int i, int i2, List<E38> list) throws IOException;

        @DexIgnore
        void j(int i, D38 d38, L48 l48);
    }

    @DexIgnore
    public J38(K48 k48, boolean z) {
        this.b = k48;
        this.d = z;
        Ai ai = new Ai(k48);
        this.c = ai;
        this.e = new F38.Ai(4096, ai);
    }

    @DexIgnore
    public static int a(int i, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i--;
        }
        if (s <= i) {
            return (short) (i - s);
        }
        G38.d("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public static int l(K48 k48) throws IOException {
        return ((k48.readByte() & 255) << 16) | ((k48.readByte() & 255) << 8) | (k48.readByte() & 255);
    }

    @DexIgnore
    public final void A(Bi bi, int i, byte b2, int i2) throws IOException {
        if (i != 5) {
            G38.d("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            o(bi, i2);
        } else {
            G38.d("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void B(Bi bi, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            if ((b2 & 8) != 0) {
                s = (short) (this.b.readByte() & 255);
            }
            bi.i(i2, this.b.readInt() & Integer.MAX_VALUE, j(a(i - 4, b2, s), s, b2, i2));
            return;
        }
        G38.d("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void C(Bi bi, int i, byte b2, int i2) throws IOException {
        if (i != 4) {
            G38.d("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i));
            throw null;
        } else if (i2 != 0) {
            int readInt = this.b.readInt();
            D38 fromHttp2 = D38.fromHttp2(readInt);
            if (fromHttp2 != null) {
                bi.h(i2, fromHttp2);
                return;
            }
            G38.d("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            G38.d("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void D(Bi bi, int i, byte b2, int i2) throws IOException {
        if (i2 != 0) {
            G38.d("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b2 & 1) != 0) {
            if (i == 0) {
                bi.a();
            } else {
                G38.d("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i % 6 == 0) {
            O38 o38 = new O38();
            for (int i3 = 0; i3 < i; i3 += 6) {
                int readShort = this.b.readShort() & 65535;
                int readInt = this.b.readInt();
                if (readShort != 2) {
                    if (readShort == 3) {
                        readShort = 4;
                    } else if (readShort == 4) {
                        readShort = 7;
                        if (readInt < 0) {
                            G38.d("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    } else if (readShort == 5 && (readInt < 16384 || readInt > 16777215)) {
                        G38.d("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                        throw null;
                    }
                } else if (!(readInt == 0 || readInt == 1)) {
                    G38.d("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                    throw null;
                }
                o38.i(readShort, readInt);
            }
            bi.b(false, o38);
        } else {
            G38.d("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i));
            throw null;
        }
    }

    @DexIgnore
    public final void F(Bi bi, int i, byte b2, int i2) throws IOException {
        if (i == 4) {
            long readInt = ((long) this.b.readInt()) & 2147483647L;
            if (readInt != 0) {
                bi.d(i2, readInt);
                return;
            }
            G38.d("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        G38.d("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i));
        throw null;
    }

    @DexIgnore
    public boolean b(boolean z, Bi bi) throws IOException {
        try {
            this.b.j0(9);
            int l = l(this.b);
            if (l < 0 || l > 16384) {
                G38.d("FRAME_SIZE_ERROR: %s", Integer.valueOf(l));
                throw null;
            }
            byte readByte = (byte) (this.b.readByte() & 255);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.b.readByte() & 255);
                int readInt = this.b.readInt() & Integer.MAX_VALUE;
                if (f.isLoggable(Level.FINE)) {
                    f.fine(G38.b(true, readInt, l, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        f(bi, l, readByte2, readInt);
                        return true;
                    case 1:
                        k(bi, l, readByte2, readInt);
                        return true;
                    case 2:
                        A(bi, l, readByte2, readInt);
                        return true;
                    case 3:
                        C(bi, l, readByte2, readInt);
                        return true;
                    case 4:
                        D(bi, l, readByte2, readInt);
                        return true;
                    case 5:
                        B(bi, l, readByte2, readInt);
                        return true;
                    case 6:
                        m(bi, l, readByte2, readInt);
                        return true;
                    case 7:
                        h(bi, l, readByte2, readInt);
                        return true;
                    case 8:
                        F(bi, l, readByte2, readInt);
                        return true;
                    default:
                        this.b.skip((long) l);
                        return true;
                }
            } else {
                G38.d("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
                throw null;
            }
        } catch (IOException e2) {
            return false;
        }
    }

    @DexIgnore
    public void c(Bi bi) throws IOException {
        if (!this.d) {
            L48 i = this.b.i((long) G38.a.size());
            if (f.isLoggable(Level.FINE)) {
                f.fine(B28.r("<< CONNECTION %s", i.hex()));
            }
            if (!G38.a.equals(i)) {
                G38.d("Expected a connection header but was %s", i.utf8());
                throw null;
            }
        } else if (!b(true, bi)) {
            G38.d("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    public final void f(Bi bi, int i, byte b2, int i2) throws IOException {
        boolean z = true;
        short s = 0;
        if (i2 != 0) {
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b2 & 8) != 0) {
                    s = (short) (this.b.readByte() & 255);
                }
                bi.e(z2, i2, this.b, a(i, b2, s));
                this.b.skip((long) s);
                return;
            }
            G38.d("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        G38.d("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void h(Bi bi, int i, byte b2, int i2) throws IOException {
        if (i < 8) {
            G38.d("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.b.readInt();
            int readInt2 = this.b.readInt();
            int i3 = i - 8;
            D38 fromHttp2 = D38.fromHttp2(readInt2);
            if (fromHttp2 != null) {
                L48 l48 = L48.EMPTY;
                if (i3 > 0) {
                    l48 = this.b.i((long) i3);
                }
                bi.j(readInt, fromHttp2, l48);
                return;
            }
            G38.d("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            G38.d("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final List<E38> j(int i, short s, byte b2, int i2) throws IOException {
        Ai ai = this.c;
        ai.f = i;
        ai.c = i;
        ai.g = (short) s;
        ai.d = (byte) b2;
        ai.e = i2;
        this.e.k();
        return this.e.e();
    }

    @DexIgnore
    public final void k(Bi bi, int i, byte b2, int i2) throws IOException {
        short s = 0;
        if (i2 != 0) {
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.b.readByte() & 255);
            }
            if ((b2 & 32) != 0) {
                o(bi, i2);
                i -= 5;
            }
            bi.c(z, i2, -1, j(a(i, b2, s), s, b2, i2));
            return;
        }
        G38.d("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void m(Bi bi, int i, byte b2, int i2) throws IOException {
        boolean z = true;
        if (i != 8) {
            G38.d("TYPE_PING length != 8: %s", Integer.valueOf(i));
            throw null;
        } else if (i2 == 0) {
            int readInt = this.b.readInt();
            int readInt2 = this.b.readInt();
            if ((b2 & 1) == 0) {
                z = false;
            }
            bi.f(z, readInt, readInt2);
        } else {
            G38.d("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void o(Bi bi, int i) throws IOException {
        int readInt = this.b.readInt();
        bi.g(i, readInt & Integer.MAX_VALUE, (this.b.readByte() & 255) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }
}
