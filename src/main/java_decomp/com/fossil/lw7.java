package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lw7 {
    @DexIgnore
    public static /* final */ Vz7 a; // = new Vz7("REMOVED_TASK");
    @DexIgnore
    public static /* final */ Vz7 b; // = new Vz7("CLOSED_EMPTY");

    @DexIgnore
    public static final long c(long j) {
        if (j <= 0) {
            return 0;
        }
        if (j >= 9223372036854L) {
            return Long.MAX_VALUE;
        }
        return 1000000 * j;
    }
}
