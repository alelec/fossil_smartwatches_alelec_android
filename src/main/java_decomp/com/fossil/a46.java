package com.fossil;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a46 extends x36 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.h0.c().K();
    @DexIgnore
    public ArrayList<Alarm> f; // = new ArrayList<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public /* final */ y36 i;
    @DexIgnore
    public /* final */ bk5 j;
    @DexIgnore
    public /* final */ yx5 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ on5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return a46.n;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<yx5.d, yx5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a46 f196a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(a46 a46, Alarm alarm) {
            this.f196a = a46;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yx5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f196a.i.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a46.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.f196a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.f196a.i.v0();
                }
                this.f196a.D(bVar.a(), false);
                return;
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.b());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            y36 y36 = this.f196a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                y36.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.f196a.D(bVar.a(), false);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yx5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = a46.o.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.f196a.i.a();
            this.f196a.D(this.b, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a46 f197a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", f = "HomeAlertsHybridPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $deviceId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.a46$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1", f = "HomeAlertsHybridPresenter.kt", l = {63, 68}, m = "invokeSuspend")
            /* renamed from: com.fossil.a46$c$a$a  reason: collision with other inner class name */
            public static final class C0008a extends ko7 implements vp7<iv7, qn7<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0008a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0008a aVar = new C0008a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<Alarm>> qn7) {
                    throw null;
                    //return ((C0008a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    iv7 iv7;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 = this.p$;
                        bk5 bk5 = this.this$0.this$0.f197a.j;
                        this.L$0 = iv7;
                        this.label = 1;
                        if (bk5.j(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 = (iv7) this.L$0;
                        el7.b(obj);
                    } else if (i == 2) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.f197a.h) {
                        this.this$0.this$0.f197a.h = true;
                        bk5 bk52 = this.this$0.this$0.f197a.j;
                        Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                        pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        bk52.g(applicationContext);
                    }
                    AlarmsRepository alarmsRepository = this.this$0.this$0.f197a.l;
                    this.L$0 = iv7;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = alarmsRepository.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == d ? d : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return mn7.c(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$deviceId = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$deviceId, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    boolean o = nk5.o.j().o(this.$deviceId);
                    this.this$0.f197a.i.j4(o);
                    if (o) {
                        dv7 i2 = this.this$0.f197a.i();
                        C0008a aVar = new C0008a(this, null);
                        this.L$0 = iv7;
                        this.Z$0 = o;
                        this.label = 1;
                        g = eu7.g(i2, aVar, this);
                        if (g == d) {
                            return d;
                        }
                    }
                    a46 a46 = this.this$0.f197a;
                    a46.g = a46.m.Z();
                    this.this$0.f197a.i.N1(this.this$0.f197a.g);
                    this.this$0.f197a.i.u3(this.this$0.f197a.g);
                    return tl7.f3441a;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<Alarm> list = (List) g;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a46.o.a();
                local.d(a2, "GetAlarms onSuccess: size = " + list.size());
                this.this$0.f197a.f.clear();
                for (Alarm alarm : list) {
                    this.this$0.f197a.f.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                }
                ArrayList arrayList = this.this$0.f197a.f;
                if (arrayList.size() > 1) {
                    lm7.r(arrayList, new b());
                }
                this.this$0.f197a.i.p0(this.this$0.f197a.f);
                a46 a462 = this.this$0.f197a;
                a462.g = a462.m.Z();
                this.this$0.f197a.i.N1(this.this$0.f197a.g);
                this.this$0.f197a.i.u3(this.this$0.f197a.g);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(a46 a46) {
            this.f197a = a46;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.f197a.i.r(true);
            } else {
                xw7 unused = gu7.d(this.f197a.k(), null, null, new a(this, str, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ a46 f198a;

        @DexIgnore
        public d(a46 a46) {
            this.f198a = a46;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            y36 unused = this.f198a.i;
        }
    }

    /*
    static {
        String simpleName = a46.class.getSimpleName();
        pq7.b(simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public a46(y36 y36, bk5 bk5, yx5 yx5, AlarmsRepository alarmsRepository, on5 on5) {
        pq7.c(y36, "mView");
        pq7.c(bk5, "mAlarmHelper");
        pq7.c(yx5, "mSetAlarms");
        pq7.c(alarmsRepository, "mAlarmRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.i = y36;
        this.j = bk5;
        this.k = yx5;
        this.l = alarmsRepository;
        this.m = on5;
    }

    @DexIgnore
    public final void D(Alarm alarm, boolean z) {
        pq7.c(alarm, "editAlarm");
        Iterator<Alarm> it = this.f.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (pq7.a(next.getUri(), alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.f;
                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                if (!z) {
                    break;
                }
                E();
            }
        }
        this.i.p0(this.f);
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.g(PortfolioApp.h0.c());
        PortfolioApp c2 = PortfolioApp.h0.c();
        String e2 = this.e.e();
        if (e2 != null) {
            pq7.b(e2, "mActiveSerial.value!!");
            c2.P0(e2);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public void F() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(n, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.k.s();
        PortfolioApp.h0.h(this);
        LiveData<String> liveData = this.e;
        y36 y36 = this.i;
        if (y36 != null) {
            liveData.h((z36) y36, new c(this));
            wq5.d.g(CommunicateMode.SET_LIST_ALARM);
            y36 y362 = this.i;
            y362.F(!jn5.c(jn5.b, ((z36) y362).getContext(), jn5.a.NOTIFICATION_HYBRID, false, false, false, null, 56, null));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.e.m(new d(this));
        this.k.w();
        PortfolioApp.h0.l(this);
    }

    @DexIgnore
    @Override // com.fossil.x36
    public void n() {
        jn5 jn5 = jn5.b;
        y36 y36 = this.i;
        if (y36 != null) {
            jn5.c(jn5, ((z36) y36).getContext(), jn5.a.NOTIFICATION_HYBRID, false, false, false, null, 60, null);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.x36
    public void o(Alarm alarm, boolean z) {
        pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String e2 = this.e.e();
        if (!(e2 == null || e2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            yx5 yx5 = this.k;
            String e3 = this.e.e();
            if (e3 != null) {
                pq7.b(e3, "mActiveSerial.value!!");
                yx5.e(new yx5.c(e3, this.f, alarm), new b(this, alarm));
                return;
            }
            pq7.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    @tc7
    public final void onSetAlarmEventEndComplete(qi5 qi5) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (qi5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + qi5);
            if (qi5.b()) {
                String a2 = qi5.a();
                Iterator<Alarm> it = this.f.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (pq7.a(next.getUri(), a2)) {
                        next.setActive(false);
                    }
                }
                this.i.W();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.x36
    public void p() {
        boolean z = !this.g;
        this.g = z;
        this.m.V0(z);
        this.i.N1(this.g);
        this.i.u3(this.g);
    }

    @DexIgnore
    @Override // com.fossil.x36
    public void q(Alarm alarm) {
        String e2 = this.e.e();
        if (e2 == null || e2.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.f.size() < 32) {
            y36 y36 = this.i;
            String e3 = this.e.e();
            if (e3 != null) {
                pq7.b(e3, "mActiveSerial.value!!");
                y36.l0(e3, this.f, alarm);
                return;
            }
            pq7.i();
            throw null;
        } else {
            this.i.U();
        }
    }
}
