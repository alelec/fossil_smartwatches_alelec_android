package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ke3 {
    @DexIgnore
    public /* final */ Ls2 a;

    @DexIgnore
    public Ke3(Ls2 ls2) {
        Rc2.k(ls2);
        this.a = ls2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final LatLng b() {
        try {
            return this.a.getPosition();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void c() {
        try {
            this.a.K0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.a.y1();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void e() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Ke3)) {
            return false;
        }
        try {
            return this.a.I1(((Ke3) obj).a);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void f(float f) {
        try {
            this.a.setAlpha(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void g(float f, float f2) {
        try {
            this.a.setAnchor(f, f2);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void h(boolean z) {
        try {
            this.a.setDraggable(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.a.setFlat(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void j(Zd3 zd3) {
        if (zd3 == null) {
            try {
                this.a.X0(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.X0(zd3.a());
        }
    }

    @DexIgnore
    public final void k(float f, float f2) {
        try {
            this.a.setInfoWindowAnchor(f, f2);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void l(LatLng latLng) {
        if (latLng != null) {
            try {
                this.a.setPosition(latLng);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            throw new IllegalArgumentException("latlng cannot be null - a position is required.");
        }
    }

    @DexIgnore
    public final void m(float f) {
        try {
            this.a.setRotation(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void n(String str) {
        try {
            this.a.e2(str);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void o(String str) {
        try {
            this.a.setTitle(str);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void p(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void q(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void r() {
        try {
            this.a.t();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
