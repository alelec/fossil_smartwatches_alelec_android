package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Z40;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jl1 extends Z40 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Jl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jl1 createFromParcel(Parcel parcel) {
            Z40 b = Z40.CREATOR.b(parcel);
            if (b != null) {
                return (Jl1) b;
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotAlarm");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jl1[] newArray(int i) {
            return new Jl1[i];
        }
    }

    @DexIgnore
    public Jl1(Kl1 kl1, Ql1 ql1, Il1 il1) throws IllegalArgumentException {
        super(kl1, ql1, il1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Jl1(Kl1 kl1, Ql1 ql1, Il1 il1, int i, Qg6 qg6) throws IllegalArgumentException {
        this(kl1, (i & 2) != 0 ? null : ql1, (i & 4) != 0 ? null : il1);
    }

    @DexIgnore
    @Override // com.fossil.El1
    public Kl1 getFireTime() {
        Gl1[] b = b();
        for (Gl1 gl1 : b) {
            if (gl1 instanceof Kl1) {
                if (gl1 != null) {
                    return (Kl1) gl1;
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.OneShotFireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
