package com.fossil;

import com.fossil.Be1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rc1<DataType> implements Be1.Bi {
    @DexIgnore
    public /* final */ Jb1<DataType> a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ Ob1 c;

    @DexIgnore
    public Rc1(Jb1<DataType> jb1, DataType datatype, Ob1 ob1) {
        this.a = jb1;
        this.b = datatype;
        this.c = ob1;
    }

    @DexIgnore
    @Override // com.fossil.Be1.Bi
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
