package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X80 extends Z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ X80 i; // = new X80();

    @DexIgnore
    public X80() {
        super("raw_hardware_log", 102400, 20971520, "raw_hardware_log", "raw_hardware_log", new Zw1("", "", ""), 1800, new B90(), Ld0.e, false);
    }

    @DexIgnore
    @Override // com.fossil.Z80
    public long e() {
        return h;
    }
}
