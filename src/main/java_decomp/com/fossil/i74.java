package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I74 extends X64 {
    @DexIgnore
    public static /* final */ Mg4<Set<Object>> e; // = H74.a();
    @DexIgnore
    public /* final */ Map<A74<?>, P74<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, P74<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, P74<Set<?>>> c; // = new HashMap();
    @DexIgnore
    public /* final */ O74 d;

    @DexIgnore
    public I74(Executor executor, Iterable<E74> iterable, A74<?>... a74Arr) {
        this.d = new O74(executor);
        ArrayList<A74<?>> arrayList = new ArrayList();
        arrayList.add(A74.n(this.d, O74.class, Ge4.class, Fe4.class));
        for (E74 e74 : iterable) {
            arrayList.addAll(e74.getComponents());
        }
        for (A74<?> a74 : a74Arr) {
            if (a74 != null) {
                arrayList.add(a74);
            }
        }
        J74.a(arrayList);
        for (A74<?> a742 : arrayList) {
            this.a.put(a742, new P74<>(F74.a(this, a742)));
        }
        g();
        h();
    }

    @DexIgnore
    public static /* synthetic */ Set f(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((P74) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    @Override // com.fossil.B74
    public <T> Mg4<T> a(Class<T> cls) {
        R74.c(cls, "Null interface requested.");
        return this.b.get(cls);
    }

    @DexIgnore
    @Override // com.fossil.B74
    public <T> Mg4<Set<T>> b(Class<T> cls) {
        P74<Set<?>> p74 = this.c.get(cls);
        return p74 != null ? p74 : (Mg4<Set<T>>) e;
    }

    @DexIgnore
    public void d(boolean z) {
        for (Map.Entry<A74<?>, P74<?>> entry : this.a.entrySet()) {
            A74<?> key = entry.getKey();
            P74<?> value = entry.getValue();
            if (key.i() || (key.j() && z)) {
                value.get();
            }
        }
        this.d.c();
    }

    @DexIgnore
    public final void g() {
        for (Map.Entry<A74<?>, P74<?>> entry : this.a.entrySet()) {
            A74<?> key = entry.getKey();
            if (key.k()) {
                P74<?> value = entry.getValue();
                for (Class<? super Object> cls : key.e()) {
                    this.b.put(cls, value);
                }
            }
        }
        i();
    }

    @DexIgnore
    public final void h() {
        HashMap hashMap = new HashMap();
        for (Map.Entry<A74<?>, P74<?>> entry : this.a.entrySet()) {
            A74<?> key = entry.getKey();
            if (!key.k()) {
                P74<?> value = entry.getValue();
                for (Class<? super Object> cls : key.e()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(value);
                }
            }
        }
        for (Map.Entry entry2 : hashMap.entrySet()) {
            this.c.put((Class) entry2.getKey(), new P74<>(G74.a((Set) entry2.getValue())));
        }
    }

    @DexIgnore
    public final void i() {
        for (A74<?> a74 : this.a.keySet()) {
            Iterator<K74> it = a74.c().iterator();
            while (true) {
                if (it.hasNext()) {
                    K74 next = it.next();
                    if (next.c() && !this.b.containsKey(next.a())) {
                        throw new Q74(String.format("Unsatisfied dependency for component %s: %s", a74, next.a()));
                    }
                }
            }
        }
    }
}
