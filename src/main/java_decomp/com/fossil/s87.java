package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import com.mapped.Qg6;
import com.mapped.WatchFaceStickerStorage;
import com.mapped.Wg6;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S87 {
    @DexIgnore
    public W87 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends S87 {
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ Cb7 e;
        @DexIgnore
        public /* final */ Bitmap f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public /* final */ O87 j;
        @DexIgnore
        public W87 k;
        @DexIgnore
        public int l;
        @DexIgnore
        public int m;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(String str, Cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, O87 o87, W87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            Wg6.c(str, "complicationId");
            Wg6.c(o87, "colorSpace");
            this.d = str;
            this.e = cb7;
            this.f = bitmap;
            this.g = z;
            this.h = str2;
            this.i = str3;
            this.j = o87;
            this.k = w87;
            this.l = i2;
            this.m = i3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(String str, Cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, O87 o87, W87 w87, int i2, int i3, int i4, Qg6 qg6) {
            this(str, cb7, bitmap, z, (i4 & 16) != 0 ? null : str2, (i4 & 32) != 0 ? null : str3, o87, (i4 & 128) != 0 ? null : w87, (i4 & 256) != 0 ? 0 : i2, (i4 & 512) != 0 ? 0 : i3);
        }

        @DexIgnore
        public static /* synthetic */ Ai f(Ai ai, String str, Cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, O87 o87, W87 w87, int i2, int i3, int i4, Object obj) {
            return ai.e((i4 & 1) != 0 ? ai.d : str, (i4 & 2) != 0 ? ai.e : cb7, (i4 & 4) != 0 ? ai.f : bitmap, (i4 & 8) != 0 ? ai.g : z, (i4 & 16) != 0 ? ai.h : str2, (i4 & 32) != 0 ? ai.i : str3, (i4 & 64) != 0 ? ai.j : o87, (i4 & 128) != 0 ? ai.b() : w87, (i4 & 256) != 0 ? ai.a() : i2, (i4 & 512) != 0 ? ai.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int a() {
            return this.l;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public W87 b() {
            return this.k;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int c() {
            return this.m;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public void d(W87 w87) {
            this.k = w87;
        }

        @DexIgnore
        public final Ai e(String str, Cb7 cb7, Bitmap bitmap, boolean z, String str2, String str3, O87 o87, W87 w87, int i2, int i3) {
            Wg6.c(str, "complicationId");
            Wg6.c(o87, "colorSpace");
            return new Ai(str, cb7, bitmap, z, str2, str3, o87, w87, i2, i3);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.d, ai.d) || !Wg6.a(this.e, ai.e) || !Wg6.a(this.f, ai.f) || this.g != ai.g || !Wg6.a(this.h, ai.h) || !Wg6.a(this.i, ai.i) || !Wg6.a(this.j, ai.j) || !Wg6.a(b(), ai.b()) || a() != ai.a() || c() != ai.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Bitmap g() {
            return this.f;
        }

        @DexIgnore
        public final O87 h() {
            return this.j;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            String str = this.d;
            int hashCode = str != null ? str.hashCode() : 0;
            Cb7 cb7 = this.e;
            int hashCode2 = cb7 != null ? cb7.hashCode() : 0;
            Bitmap bitmap = this.f;
            int hashCode3 = bitmap != null ? bitmap.hashCode() : 0;
            boolean z = this.g;
            if (z) {
                z = true;
            }
            String str2 = this.h;
            int hashCode4 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.i;
            int hashCode5 = str3 != null ? str3.hashCode() : 0;
            O87 o87 = this.j;
            int hashCode6 = o87 != null ? o87.hashCode() : 0;
            W87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final String i() {
            return this.d;
        }

        @DexIgnore
        public final String j() {
            return this.i;
        }

        @DexIgnore
        public final Cb7 k() {
            return this.e;
        }

        @DexIgnore
        public final String l() {
            return this.h;
        }

        @DexIgnore
        public final boolean m() {
            return this.g;
        }

        @DexIgnore
        public String toString() {
            return "ComplicationConfig(complicationId=" + this.d + ", ring=" + this.e + ", bitmap=" + this.f + ", isGoalRingEnable=" + this.g + ", setting=" + this.h + ", data=" + this.i + ", colorSpace=" + this.j + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends S87 {
        @DexIgnore
        public /* final */ X87 d;
        @DexIgnore
        public O87 e;
        @DexIgnore
        public List<X87> f;
        @DexIgnore
        public Bitmap g;
        @DexIgnore
        public W87 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(O87 o87, List<X87> list, Bitmap bitmap, W87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            Wg6.c(o87, "colorSpace");
            Wg6.c(list, "stickerInfoList");
            this.e = o87;
            this.f = list;
            this.g = bitmap;
            this.h = w87;
            this.i = i2;
            this.j = i3;
            X87 x87 = (X87) Pm7.I(list, o87.ordinal());
            this.d = x87 == null ? (X87) Pm7.H(this.f) : x87;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Bi(O87 o87, List list, Bitmap bitmap, W87 w87, int i2, int i3, int i4, Qg6 qg6) {
            this(o87, list, (i4 & 4) != 0 ? null : bitmap, (i4 & 8) == 0 ? w87 : null, (i4 & 16) != 0 ? 0 : i2, (i4 & 32) == 0 ? i3 : 0);
        }

        @DexIgnore
        public static /* synthetic */ Bi g(Bi bi, O87 o87, List list, Bitmap bitmap, W87 w87, int i2, int i3, int i4, Object obj) {
            return bi.f((i4 & 1) != 0 ? bi.e : o87, (i4 & 2) != 0 ? bi.f : list, (i4 & 4) != 0 ? bi.g : bitmap, (i4 & 8) != 0 ? bi.b() : w87, (i4 & 16) != 0 ? bi.a() : i2, (i4 & 32) != 0 ? bi.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int a() {
            return this.i;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public W87 b() {
            return this.h;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int c() {
            return this.j;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public void d(W87 w87) {
            this.h = w87;
        }

        @DexIgnore
        public final boolean e() {
            boolean z;
            List<X87> list = this.f;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    if (WatchFaceStickerStorage.c.c(it.next().b()) != null) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (!z) {
                        return false;
                    }
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!Wg6.a(this.e, bi.e) || !Wg6.a(this.f, bi.f) || !Wg6.a(this.g, bi.g) || !Wg6.a(b(), bi.b()) || a() != bi.a() || c() != bi.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Bi f(O87 o87, List<X87> list, Bitmap bitmap, W87 w87, int i2, int i3) {
            Wg6.c(o87, "colorSpace");
            Wg6.c(list, "stickerInfoList");
            return new Bi(o87, list, bitmap, w87, i2, i3);
        }

        @DexIgnore
        public final O87 h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            O87 o87 = this.e;
            int hashCode = o87 != null ? o87.hashCode() : 0;
            List<X87> list = this.f;
            int hashCode2 = list != null ? list.hashCode() : 0;
            Bitmap bitmap = this.g;
            int hashCode3 = bitmap != null ? bitmap.hashCode() : 0;
            W87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final Bitmap i() {
            X87 x87 = (X87) Pm7.I(this.f, this.e.ordinal());
            if (x87 != null) {
                return x87.a();
            }
            return null;
        }

        @DexIgnore
        public final X87 j() {
            return this.d;
        }

        @DexIgnore
        public final Bitmap k() {
            X87 x87 = (X87) Pm7.H(this.f);
            if (x87 != null) {
                return x87.a();
            }
            return null;
        }

        @DexIgnore
        public final Bitmap l() {
            return this.g;
        }

        @DexIgnore
        public final List<X87> m() {
            return this.f;
        }

        @DexIgnore
        public final void n(O87 o87) {
            Wg6.c(o87, "<set-?>");
            this.e = o87;
        }

        @DexIgnore
        public void o(int i2) {
            this.i = i2;
        }

        @DexIgnore
        public void p(int i2) {
            this.j = i2;
        }

        @DexIgnore
        public final void q(Bitmap bitmap) {
            this.g = bitmap;
        }

        @DexIgnore
        public String toString() {
            return "StickerConfig(colorSpace=" + this.e + ", stickerInfoList=" + this.f + ", rawBitmap=" + this.g + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends S87 {
        @DexIgnore
        public String d;
        @DexIgnore
        public Typeface e;
        @DexIgnore
        public float f;
        @DexIgnore
        public O87 g;
        @DexIgnore
        public W87 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(String str, Typeface typeface, float f2, O87 o87, W87 w87, int i2, int i3) {
            super(w87, i2, i3, null);
            Wg6.c(str, "text");
            Wg6.c(o87, "colorSpace");
            this.d = str;
            this.e = typeface;
            this.f = f2;
            this.g = o87;
            this.h = w87;
            this.i = i2;
            this.j = i3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(String str, Typeface typeface, float f2, O87 o87, W87 w87, int i2, int i3, int i4, Qg6 qg6) {
            this(str, typeface, f2, o87, (i4 & 16) != 0 ? null : w87, (i4 & 32) != 0 ? 0 : i2, (i4 & 64) != 0 ? 0 : i3);
        }

        @DexIgnore
        public static /* synthetic */ Ci f(Ci ci, String str, Typeface typeface, float f2, O87 o87, W87 w87, int i2, int i3, int i4, Object obj) {
            return ci.e((i4 & 1) != 0 ? ci.d : str, (i4 & 2) != 0 ? ci.e : typeface, (i4 & 4) != 0 ? ci.f : f2, (i4 & 8) != 0 ? ci.g : o87, (i4 & 16) != 0 ? ci.b() : w87, (i4 & 32) != 0 ? ci.a() : i2, (i4 & 64) != 0 ? ci.c() : i3);
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int a() {
            return this.i;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public W87 b() {
            return this.h;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public int c() {
            return this.j;
        }

        @DexIgnore
        @Override // com.fossil.S87
        public void d(W87 w87) {
            this.h = w87;
        }

        @DexIgnore
        public final Ci e(String str, Typeface typeface, float f2, O87 o87, W87 w87, int i2, int i3) {
            Wg6.c(str, "text");
            Wg6.c(o87, "colorSpace");
            return new Ci(str, typeface, f2, o87, w87, i2, i3);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ci) {
                    Ci ci = (Ci) obj;
                    if (!Wg6.a(this.d, ci.d) || !Wg6.a(this.e, ci.e) || Float.compare(this.f, ci.f) != 0 || !Wg6.a(this.g, ci.g) || !Wg6.a(b(), ci.b()) || a() != ci.a() || c() != ci.c()) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final O87 g() {
            return this.g;
        }

        @DexIgnore
        public final float h() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            String str = this.d;
            int hashCode = str != null ? str.hashCode() : 0;
            Typeface typeface = this.e;
            int hashCode2 = typeface != null ? typeface.hashCode() : 0;
            int floatToIntBits = Float.floatToIntBits(this.f);
            O87 o87 = this.g;
            int hashCode3 = o87 != null ? o87.hashCode() : 0;
            W87 b = b();
            if (b != null) {
                i2 = b.hashCode();
            }
            return (((((((((((hashCode * 31) + hashCode2) * 31) + floatToIntBits) * 31) + hashCode3) * 31) + i2) * 31) + a()) * 31) + c();
        }

        @DexIgnore
        public final String i() {
            return this.d;
        }

        @DexIgnore
        public final Typeface j() {
            return this.e;
        }

        @DexIgnore
        public final void k(O87 o87) {
            Wg6.c(o87, "<set-?>");
            this.g = o87;
        }

        @DexIgnore
        public final void l(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public void m(int i2) {
            this.i = i2;
        }

        @DexIgnore
        public void n(int i2) {
            this.j = i2;
        }

        @DexIgnore
        public final void o(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void p(Typeface typeface) {
            this.e = typeface;
        }

        @DexIgnore
        public String toString() {
            return "TextConfig(text=" + this.d + ", typeface=" + this.e + ", fontSize=" + this.f + ", colorSpace=" + this.g + ", metric=" + b() + ", index=" + a() + ", previousIndex=" + c() + ")";
        }
    }

    @DexIgnore
    public S87(W87 w87, int i, int i2) {
        this.a = w87;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public /* synthetic */ S87(W87 w87, int i, int i2, Qg6 qg6) {
        this(w87, i, i2);
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public W87 b() {
        return this.a;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public void d(W87 w87) {
        this.a = w87;
    }
}
