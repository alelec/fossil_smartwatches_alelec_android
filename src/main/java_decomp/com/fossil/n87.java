package com.fossil;

import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N87 implements Factory<WatchFaceRingViewModel> {
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> a;

    @DexIgnore
    public N87(Provider<WFAssetRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static N87 a(Provider<WFAssetRepository> provider) {
        return new N87(provider);
    }

    @DexIgnore
    public static WatchFaceRingViewModel c(WFAssetRepository wFAssetRepository) {
        return new WatchFaceRingViewModel(wFAssetRepository);
    }

    @DexIgnore
    public WatchFaceRingViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
