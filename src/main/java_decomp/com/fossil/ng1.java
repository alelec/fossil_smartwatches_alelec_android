package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ng1 implements Qb1<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ Xf1 a; // = new Xf1();

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(InputStream inputStream, Ob1 ob1) throws IOException {
        return d(inputStream, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        return c(inputStream, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(InputStream inputStream, int i, int i2, Ob1 ob1) throws IOException {
        return this.a.d(ImageDecoder.createSource(Zj1.b(inputStream)), i, i2, ob1);
    }

    @DexIgnore
    public boolean d(InputStream inputStream, Ob1 ob1) throws IOException {
        return true;
    }
}
