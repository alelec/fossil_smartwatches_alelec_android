package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Su2 extends E13<Su2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Su2 zzf;
    @DexIgnore
    public static volatile Z23<Su2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Su2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Su2.zzf);
        }

        @DexIgnore
        public /* synthetic */ Ai(Ou2 ou2) {
            this();
        }
    }

    /*
    static {
        Su2 su2 = new Su2();
        zzf = su2;
        E13.u(Su2.class, su2);
    }
    */

    @DexIgnore
    public final String C() {
        return this.zzd;
    }

    @DexIgnore
    public final String D() {
        return this.zze;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Ou2.a[i - 1]) {
            case 1:
                return new Su2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1008\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                Z23<Su2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Su2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
