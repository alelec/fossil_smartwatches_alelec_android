package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mf6 extends Fq4 {
    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE n();

    @DexIgnore
    public abstract void o(Date date);
}
