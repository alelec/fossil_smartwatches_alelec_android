package com.fossil;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xh2 {
    @DexIgnore
    public static /* final */ DataType a; // = new DataType("com.google.blood_pressure", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", Yh2.a, Yh2.e, Yh2.i, Yh2.j);
    @DexIgnore
    public static /* final */ DataType b; // = new DataType("com.google.blood_glucose", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", Yh2.k, Yh2.l, Wh2.M, Yh2.m, Yh2.n);
    @DexIgnore
    public static /* final */ DataType c; // = new DataType("com.google.oxygen_saturation", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", Yh2.o, Yh2.s, Yh2.w, Yh2.x, Yh2.y);
    @DexIgnore
    public static /* final */ DataType d; // = new DataType("com.google.body.temperature", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", Yh2.z, Yh2.A);
    @DexIgnore
    public static /* final */ DataType e; // = new DataType("com.google.body.temperature.basal", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Yh2.z, Yh2.A);
    @DexIgnore
    public static /* final */ DataType f; // = new DataType("com.google.cervical_mucus", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Yh2.B, Yh2.C);
    @DexIgnore
    public static /* final */ DataType g; // = new DataType("com.google.cervical_position", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Yh2.D, Yh2.E, Yh2.F);
    @DexIgnore
    public static /* final */ DataType h; // = new DataType("com.google.menstruation", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Yh2.G);
    @DexIgnore
    public static /* final */ DataType i; // = new DataType("com.google.ovulation_test", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Yh2.H);
    @DexIgnore
    public static /* final */ DataType j; // = new DataType("com.google.vaginal_spotting", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Wh2.h0);
    @DexIgnore
    public static /* final */ DataType k; // = new DataType("com.google.blood_pressure.summary", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", Yh2.b, Yh2.d, Yh2.c, Yh2.f, Yh2.h, Yh2.g, Yh2.i, Yh2.j);
    @DexIgnore
    public static /* final */ DataType l; // = new DataType("com.google.blood_glucose.summary", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", Wh2.a0, Wh2.b0, Wh2.c0, Yh2.l, Wh2.M, Yh2.m, Yh2.n);
    @DexIgnore
    public static /* final */ DataType m; // = new DataType("com.google.oxygen_saturation.summary", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", Yh2.p, Yh2.r, Yh2.q, Yh2.t, Yh2.v, Yh2.u, Yh2.w, Yh2.x, Yh2.y);
    @DexIgnore
    public static /* final */ DataType n; // = new DataType("com.google.body.temperature.summary", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", Wh2.a0, Wh2.b0, Wh2.c0, Yh2.A);
    @DexIgnore
    public static /* final */ DataType o; // = new DataType("com.google.body.temperature.basal.summary", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", Wh2.a0, Wh2.b0, Wh2.c0, Yh2.A);
}
