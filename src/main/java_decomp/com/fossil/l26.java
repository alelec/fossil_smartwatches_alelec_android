package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.H26;
import com.fossil.Hq4;
import com.fossil.Jn5;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L26 extends Qv5 implements H26.Ai {
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public G37<Ra5> i;
    @DexIgnore
    public QuickResponseViewModel j;
    @DexIgnore
    public H26 k;
    @DexIgnore
    public ArrayList<QuickResponseMessage> l; // = new ArrayList<>();
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final L26 a() {
            return new L26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<QuickResponseViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ L26 a;

        @DexIgnore
        public Bi(L26 l26) {
            this.a = l26;
        }

        @DexIgnore
        public final void a(QuickResponseViewModel.Ai ai) {
            if (ai != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("QuickResponseFragment", "data changed " + ai);
                this.a.N6(ai);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(QuickResponseViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ L26 a;

        @DexIgnore
        public Ci(L26 l26) {
            this.a = l26;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi != null) {
                if (bi.a()) {
                    this.a.b();
                }
                if (bi.b()) {
                    this.a.a();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Hq4.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ L26 a;

        @DexIgnore
        public Di(L26 l26) {
            this.a = l26;
        }

        @DexIgnore
        public final void a(Hq4.Ci ci) {
            if (ci != null && (!ci.a().isEmpty())) {
                Jn5.c(Jn5.b, this.a.getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, true, 113, 12, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ra5 b;
        @DexIgnore
        public /* final */ /* synthetic */ L26 c;

        @DexIgnore
        public Ei(Ra5 ra5, L26 l26) {
            this.b = ra5;
            this.c = l26;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleEditText flexibleEditText = this.b.s;
            Wg6.b(flexibleEditText, "etMessage");
            this.c.P6().x(String.valueOf(flexibleEditText.getText()), 50);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ L26 b;

        @DexIgnore
        public Fi(L26 l26) {
            this.b = l26;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().s(this.b.l);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ra5 b;
        @DexIgnore
        public /* final */ /* synthetic */ L26 c;

        @DexIgnore
        public Gi(Ra5 ra5, L26 l26) {
            this.b = ra5;
            this.c = l26;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseViewModel P6 = this.c.P6();
            FlexibleEditText flexibleEditText = this.b.s;
            Wg6.b(flexibleEditText, "etMessage");
            P6.r(String.valueOf(flexibleEditText.getText()), this.c.l.size(), 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<List<? extends QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ L26 a;

        @DexIgnore
        public Hi(L26 l26) {
            this.a = l26;
        }

        @DexIgnore
        public final void a(List<QuickResponseMessage> list) {
            Ra5 a2;
            FlexibleEditText flexibleEditText;
            if (list.isEmpty()) {
                this.a.P6().z();
                return;
            }
            if (!(list.size() <= this.a.l.size() || (a2 = this.a.O6().a()) == null || (flexibleEditText = a2.s) == null)) {
                flexibleEditText.setText("");
            }
            this.a.l.clear();
            this.a.l.addAll(list);
            H26 L6 = L26.L6(this.a);
            Wg6.b(list, "qrs");
            L6.k(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends QuickResponseMessage> list) {
            a(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ H26 L6(L26 l26) {
        H26 h26 = l26.k;
        if (h26 != null) {
            return h26;
        }
        Wg6.n("mResponseAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        QuickResponseViewModel quickResponseViewModel = this.j;
        if (quickResponseViewModel != null) {
            quickResponseViewModel.s(this.l);
            return false;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void N6(QuickResponseViewModel.Ai ai) {
        FragmentActivity activity;
        Boolean a2 = ai.a();
        if (a2 != null && a2.booleanValue()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
        }
        Boolean f = ai.f();
        if (f != null && f.booleanValue()) {
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.t0(childFragmentManager2);
        }
        Boolean c = ai.c();
        if (!(c == null || !c.booleanValue() || (activity = getActivity()) == null)) {
            activity.finish();
        }
        Boolean b = ai.b();
        if (b != null && b.booleanValue()) {
            S37 s373 = S37.c;
            FragmentManager childFragmentManager3 = getChildFragmentManager();
            Wg6.b(childFragmentManager3, "childFragmentManager");
            s373.Y(childFragmentManager3);
        }
        Boolean d = ai.d();
        if (d != null) {
            if (d.booleanValue()) {
                G37<Ra5> g37 = this.i;
                if (g37 != null) {
                    Ra5 a3 = g37.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.m)) {
                            FlexibleEditText flexibleEditText = a3.s;
                            Wg6.b(flexibleEditText, "etMessage");
                            flexibleEditText.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.m)));
                        } else {
                            FlexibleEditText flexibleEditText2 = a3.s;
                            Wg6.b(flexibleEditText2, "etMessage");
                            flexibleEditText2.setBackgroundTintList(ColorStateList.valueOf(-65536));
                        }
                        FlexibleTextView flexibleTextView = a3.v;
                        Wg6.b(flexibleTextView, "ftvLimitedWarning");
                        flexibleTextView.setVisibility(0);
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                G37<Ra5> g372 = this.i;
                if (g372 != null) {
                    Ra5 a4 = g372.a();
                    if (a4 != null) {
                        FlexibleTextView flexibleTextView2 = a4.v;
                        Wg6.b(flexibleTextView2, "ftvLimitedWarning");
                        if (flexibleTextView2.getVisibility() == 0) {
                            FlexibleTextView flexibleTextView3 = a4.v;
                            Wg6.b(flexibleTextView3, "ftvLimitedWarning");
                            flexibleTextView3.setVisibility(8);
                            if (!TextUtils.isEmpty(this.s)) {
                                FlexibleEditText flexibleEditText3 = a4.s;
                                Wg6.b(flexibleEditText3, "etMessage");
                                flexibleEditText3.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                            } else {
                                FlexibleEditText flexibleEditText4 = a4.s;
                                Wg6.b(flexibleEditText4, "etMessage");
                                flexibleEditText4.setBackgroundTintList(ColorStateList.valueOf(-1));
                            }
                        }
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            }
        }
        Boolean e = ai.e();
        if (e != null && e.booleanValue()) {
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(getContext(), 2131887551);
            Wg6.b(c2, "LanguageHelper.getString\u2026response_limited_warning)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{String.valueOf(10)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            Toast.makeText(getContext(), format, 1).show();
        }
        Integer g = ai.g();
        if (g != null) {
            int intValue = g.intValue();
            G37<Ra5> g373 = this.i;
            if (g373 != null) {
                Ra5 a5 = g373.a();
                if (a5 != null) {
                    FlexibleTextView flexibleTextView4 = a5.u;
                    Wg6.b(flexibleTextView4, "ftvLimitValue");
                    flexibleTextView4.setText(String.valueOf(intValue));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final G37<Ra5> O6() {
        G37<Ra5> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final QuickResponseViewModel P6() {
        QuickResponseViewModel quickResponseViewModel = this.j;
        if (quickResponseViewModel != null) {
            return quickResponseViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != -4398250 || !str.equals("SET TO WATCH FAIL")) {
                return;
            }
            if (i2 == 2131363291) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363373) {
                QuickResponseViewModel quickResponseViewModel = this.j;
                if (quickResponseViewModel != null) {
                    Context requireContext = requireContext();
                    Wg6.b(requireContext, "requireContext()");
                    quickResponseViewModel.A(requireContext, this.l, true);
                    return;
                }
                Wg6.n("mViewModel");
                throw null;
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363373) {
                QuickResponseViewModel quickResponseViewModel2 = this.j;
                if (quickResponseViewModel2 != null) {
                    Context requireContext2 = requireContext();
                    Wg6.b(requireContext2, "requireContext()");
                    quickResponseViewModel2.A(requireContext2, this.l, true);
                    return;
                }
                Wg6.n("mViewModel");
                throw null;
            } else if (i2 == 2131363291 && (activity = getActivity()) != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.H26.Ai
    public void T2() {
        View n;
        G37<Ra5> g37 = this.i;
        if (g37 != null) {
            Ra5 a2 = g37.a();
            if (a2 != null && (n = a2.n()) != null) {
                n.requestFocus();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.H26.Ai
    public void a1(QuickResponseMessage quickResponseMessage) {
        Wg6.c(quickResponseMessage, "qr");
        QuickResponseViewModel quickResponseViewModel = this.j;
        if (quickResponseViewModel != null) {
            quickResponseViewModel.y(quickResponseMessage);
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "onActivityResult result code is " + i3);
        if (i3 != 1) {
            return;
        }
        if (i2 == 112) {
            QuickResponseViewModel quickResponseViewModel = this.j;
            if (quickResponseViewModel != null) {
                Context requireContext = requireContext();
                Wg6.b(requireContext, "requireContext()");
                quickResponseViewModel.A(requireContext, this.l, false);
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        } else if (i2 == 113 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Ra5 ra5 = (Ra5) Aq0.f(LayoutInflater.from(getContext()), 2131558613, null, false, A6());
        PortfolioApp.get.instance().getIface().y1().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(QuickResponseViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026nseViewModel::class.java)");
            QuickResponseViewModel quickResponseViewModel = (QuickResponseViewModel) a2;
            this.j = quickResponseViewModel;
            if (quickResponseViewModel != null) {
                quickResponseViewModel.v().h(getViewLifecycleOwner(), new Bi(this));
                QuickResponseViewModel quickResponseViewModel2 = this.j;
                if (quickResponseViewModel2 != null) {
                    quickResponseViewModel2.j().h(getViewLifecycleOwner(), new Ci(this));
                    QuickResponseViewModel quickResponseViewModel3 = this.j;
                    if (quickResponseViewModel3 != null) {
                        quickResponseViewModel3.l().h(getViewLifecycleOwner(), new Di(this));
                        this.m = ThemeManager.l.a().d("error");
                        this.s = ThemeManager.l.a().d("nonBrandSurface");
                        this.i = new G37<>(this, ra5);
                        Wg6.b(ra5, "binding");
                        return ra5.n();
                    }
                    Wg6.n("mViewModel");
                    throw null;
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Ra5> g37 = this.i;
        if (g37 != null) {
            Ra5 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "ftvLimitValue");
                flexibleTextView.setText("0");
                FlexibleEditText flexibleEditText = a2.s;
                Wg6.b(flexibleEditText, "etMessage");
                flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(50)});
                FlexibleEditText flexibleEditText2 = a2.s;
                Wg6.b(flexibleEditText2, "etMessage");
                flexibleEditText2.addTextChangedListener(new Ei(a2, this));
                this.k = new H26(new ArrayList(), this, 50);
                RecyclerView recyclerView = a2.x;
                Wg6.b(recyclerView, "rvMessage");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.x;
                Wg6.b(recyclerView2, "rvMessage");
                H26 h26 = this.k;
                if (h26 != null) {
                    recyclerView2.setAdapter(h26);
                    a2.r.setOnClickListener(new Fi(this));
                    a2.q.setOnClickListener(new Gi(a2, this));
                } else {
                    Wg6.n("mResponseAdapter");
                    throw null;
                }
            }
            QuickResponseViewModel quickResponseViewModel = this.j;
            if (quickResponseViewModel != null) {
                LiveData<List<QuickResponseMessage>> u2 = quickResponseViewModel.u();
                if (u2 != null) {
                    u2.h(getViewLifecycleOwner(), new Hi(this));
                    return;
                }
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.H26.Ai
    public void x5(int i2, String str) {
        T t2;
        boolean z;
        Wg6.c(str, "message");
        Iterator<T> it = this.l.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (next.getId() == i2) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "changed message " + ((Object) t3) + ", new message " + str);
        if (t3 != null) {
            t3.setResponse(str);
        }
    }
}
