package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iv1 extends Ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Iv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Iv1 createFromParcel(Parcel parcel) {
            return new Iv1(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Iv1[] newArray(int i) {
            return new Iv1[i];
        }
    }

    @DexIgnore
    public Iv1(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("x", this.b).put("y", this.c);
        Wg6.b(put, "JSONObject()\n           \u2026ut(UIScriptConstant.Y, y)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Iv1) {
                Iv1 iv1 = (Iv1) obj;
                if (!(this.b == iv1.b && this.c == iv1.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getX() {
        return this.b;
    }

    @DexIgnore
    public final int getY() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b * 31) + this.c;
    }

    @DexIgnore
    public final void setX(int i) {
        this.b = i;
    }

    @DexIgnore
    public final void setY(int i) {
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public final Jv1 toScaledPosition(int i, int i2) {
        return new Jv1((((float) this.b) * 1.0f) / ((float) i), (((float) this.c) * 1.0f) / ((float) i2));
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e = E.e("CartesianCoordinatePoint(x=");
        e.append(this.b);
        e.append(", y=");
        return E.b(e, this.c, ")");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
    }
}
