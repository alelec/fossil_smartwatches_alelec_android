package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ix1;
import com.mapped.Nd0;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sm extends Zj {
    @DexIgnore
    public /* final */ Nd0[] T;

    @DexIgnore
    public Sm(K5 k5, I60 i60, Nd0[] nd0Arr) {
        super(k5, i60, Yp.h0, true, Ke.b.b(k5.x, Ob.j), new byte[0], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = nd0Arr;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.a4, Px1.a(this.T));
    }

    @DexIgnore
    @Override // com.fossil.Zj, com.fossil.Ro
    public byte[] M() {
        Ry1 microAppVersion = this.x.a().getMicroAppVersion();
        Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.j.b));
        if (ry1 == null) {
            ry1 = Hd0.y.d();
        }
        Wg6.b(ry1, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
        T90 t90 = new T90(this.T, microAppVersion);
        byte a2 = X90.c.a();
        byte[] a3 = t90.a(t90.f);
        byte[] a4 = Dy1.a(Dy1.a(Dy1.a(new byte[]{(byte) t90.c.getMajor(), (byte) t90.c.getMinor(), a2}, a3), t90.a(t90.d)), t90.a(t90.e));
        ByteBuffer order = ByteBuffer.allocate(a4.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        order.put(a4);
        order.putInt((int) Ix1.a.b(a4, Ix1.Ai.CRC32));
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        try {
            return I9.d.a(this.D, ry1, array);
        } catch (Sx1 e) {
            return new byte[0];
        }
    }
}
