package com.fossil;

import androidx.loader.app.LoaderManager;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J46 {
    @DexIgnore
    public /* final */ H46 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public J46(H46 h46, int i, LoaderManager loaderManager) {
        Wg6.c(h46, "mView");
        Wg6.c(loaderManager, "mLoaderManager");
        this.a = h46;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final H46 c() {
        return this.a;
    }
}
