package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G7 extends Ox1 {
    @DexIgnore
    public static /* final */ D7 d; // = new D7(null);
    @DexIgnore
    public /* final */ F7 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public G7(F7 f7, int i) {
        this.b = f7;
        this.c = i;
    }

    @DexIgnore
    public /* synthetic */ G7(F7 f7, int i, int i2) {
        i = (i2 & 2) != 0 ? 0 : i;
        this.b = f7;
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.O0, Ey1.a(this.b));
            if (this.c != 0) {
                G80.k(jSONObject, Jd0.k4, Integer.valueOf(this.c));
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }
}
