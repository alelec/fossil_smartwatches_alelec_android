package com.fossil;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AlertController;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ve0 extends Ye0 implements DialogInterface {
    @DexIgnore
    public /* final */ AlertController d; // = new AlertController(getContext(), this, getWindow());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ AlertController.f a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ai(Context context) {
            this(context, Ve0.g(context, 0));
        }

        @DexIgnore
        public Ai(Context context, int i) {
            this.a = new AlertController.f(new ContextThemeWrapper(context, Ve0.g(context, i)));
            this.b = i;
        }

        @DexIgnore
        public Ve0 a() {
            Ve0 ve0 = new Ve0(this.a.a, this.b);
            this.a.a(ve0.d);
            ve0.setCancelable(this.a.r);
            if (this.a.r) {
                ve0.setCanceledOnTouchOutside(true);
            }
            ve0.setOnCancelListener(this.a.s);
            ve0.setOnDismissListener(this.a.t);
            DialogInterface.OnKeyListener onKeyListener = this.a.u;
            if (onKeyListener != null) {
                ve0.setOnKeyListener(onKeyListener);
            }
            return ve0;
        }

        @DexIgnore
        public Context b() {
            return this.a.a;
        }

        @DexIgnore
        public Ai c(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            return this;
        }

        @DexIgnore
        public Ai d(boolean z) {
            this.a.r = z;
            return this;
        }

        @DexIgnore
        public Ai e(View view) {
            this.a.g = view;
            return this;
        }

        @DexIgnore
        public Ai f(Drawable drawable) {
            this.a.d = drawable;
            return this;
        }

        @DexIgnore
        public Ai g(CharSequence charSequence) {
            this.a.h = charSequence;
            return this;
        }

        @DexIgnore
        public Ai h(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.l = charSequence;
            fVar.n = onClickListener;
            return this;
        }

        @DexIgnore
        public Ai i(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.o = fVar.a.getText(i);
            this.a.q = onClickListener;
            return this;
        }

        @DexIgnore
        public Ai j(DialogInterface.OnKeyListener onKeyListener) {
            this.a.u = onKeyListener;
            return this;
        }

        @DexIgnore
        public Ai k(int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.i = fVar.a.getText(i);
            this.a.k = onClickListener;
            return this;
        }

        @DexIgnore
        public Ai l(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.i = charSequence;
            fVar.k = onClickListener;
            return this;
        }

        @DexIgnore
        public Ai m(ListAdapter listAdapter, int i, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            fVar.I = i;
            fVar.H = true;
            return this;
        }

        @DexIgnore
        public Ai n(int i) {
            AlertController.f fVar = this.a;
            fVar.f = fVar.a.getText(i);
            return this;
        }

        @DexIgnore
        public Ai o(CharSequence charSequence) {
            this.a.f = charSequence;
            return this;
        }

        @DexIgnore
        public Ai p(View view) {
            AlertController.f fVar = this.a;
            fVar.z = view;
            fVar.y = 0;
            fVar.E = false;
            return this;
        }

        @DexIgnore
        public Ve0 q() {
            Ve0 a2 = a();
            a2.show();
            return a2;
        }
    }

    @DexIgnore
    public Ve0(Context context, int i) {
        super(context, g(context, i));
    }

    @DexIgnore
    public static int g(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(Le0.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public Button e(int i) {
        return this.d.c(i);
    }

    @DexIgnore
    public ListView f() {
        return this.d.e();
    }

    @DexIgnore
    @Override // com.fossil.Ye0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d.f();
    }

    @DexIgnore
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.d.h(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @DexIgnore
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.d.i(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @DexIgnore
    @Override // com.fossil.Ye0, android.app.Dialog
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.d.r(charSequence);
    }
}
