package com.fossil;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sw1 extends ThemeEditor {
    @DexIgnore
    public /* final */ Ec0 g;
    @DexIgnore
    public Rw1 h;
    @DexIgnore
    public Rw1 i;
    @DexIgnore
    public Rw1 j;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Sw1(com.fossil.Tw1 r8, com.fossil.Ry1 r9) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r2 = r8.getBundleId()
            java.lang.String r0 = r8.getDisplayName()
            if (r0 == 0) goto L_0x008f
        L_0x000b:
            r7.<init>(r9, r2, r0)
            com.fossil.Ec0 r0 = com.fossil.Ec0.e
            r7.g = r0
            com.fossil.Rw1 r0 = com.fossil.Rw1.STEP
            r7.h = r0
            com.fossil.Rw1 r0 = com.fossil.Rw1.CALORIES
            r7.i = r0
            com.fossil.Rw1 r0 = com.fossil.Rw1.ACTIVE_MINUTE
            r7.j = r0
            com.fossil.Cc0[] r3 = r8.a()
            int r4 = r3.length
            r0 = 0
            r2 = r0
        L_0x0025:
            if (r2 >= r4) goto L_0x0099
            r0 = r3[r2]
            java.lang.String r5 = r0.b
            java.lang.String r6 = "customWatchFace"
            boolean r5 = com.mapped.Wg6.a(r5, r6)
            if (r5 == 0) goto L_0x0095
        L_0x0033:
            if (r0 == 0) goto L_0x008e
            byte[] r0 = r0.c
            if (r0 == 0) goto L_0x008e
            java.lang.String r2 = new java.lang.String
            java.nio.charset.Charset r3 = com.fossil.Et7.a
            r2.<init>(r0, r3)
            java.lang.String r0 = com.fossil.Iy1.c(r2)
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x00a7 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r0 = "barPos"
            org.json.JSONObject r0 = r2.getJSONObject(r0)     // Catch:{ Exception -> 0x00a7 }
            com.fossil.Rw1$Ai r2 = com.fossil.Rw1.d     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r3 = "top"
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r4 = "barPos.getString(UIScriptConstant.TOP)"
            com.mapped.Wg6.b(r3, r4)     // Catch:{ Exception -> 0x00a7 }
            com.fossil.Rw1 r2 = r2.a(r3)     // Catch:{ Exception -> 0x00a7 }
            if (r2 == 0) goto L_0x00a3
            com.fossil.Rw1$Ai r3 = com.fossil.Rw1.d     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r4 = "middle"
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r5 = "barPos.getString(UIScriptConstant.MIDDLE)"
            com.mapped.Wg6.b(r4, r5)     // Catch:{ Exception -> 0x00a7 }
            com.fossil.Rw1 r3 = r3.a(r4)     // Catch:{ Exception -> 0x00a7 }
            if (r3 == 0) goto L_0x009f
            com.fossil.Rw1$Ai r4 = com.fossil.Rw1.d     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r5 = "bot"
            java.lang.String r0 = r0.getString(r5)     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r5 = "barPos.getString(UIScriptConstant.BOT)"
            com.mapped.Wg6.b(r0, r5)     // Catch:{ Exception -> 0x00a7 }
            com.fossil.Rw1 r0 = r4.a(r0)     // Catch:{ Exception -> 0x00a7 }
            if (r0 == 0) goto L_0x009b
            r7.h = r2     // Catch:{ Exception -> 0x00a7 }
            r7.i = r3     // Catch:{ Exception -> 0x00a7 }
            r7.j = r0     // Catch:{ Exception -> 0x00a7 }
        L_0x008e:
            return
        L_0x008f:
            java.lang.String r0 = r8.getBundleId()
            goto L_0x000b
        L_0x0095:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0025
        L_0x0099:
            r0 = r1
            goto L_0x0033
        L_0x009b:
            com.mapped.Wg6.i()
            throw r1
        L_0x009f:
            com.mapped.Wg6.i()
            throw r1
        L_0x00a3:
            com.mapped.Wg6.i()
            throw r1
        L_0x00a7:
            r0 = move-exception
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sw1.<init>(com.fossil.Tw1, com.fossil.Ry1):void");
    }

    @DexIgnore
    @Override // com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor
    public Lw1 a(Hc0 hc0) {
        Cc0 a2;
        if (!(hc0 instanceof Uw1)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Uw1 uw1 = (Uw1) hc0;
        arrayList.add(new Cc0(d(), uw1.f()[0].c));
        Mw1 f = f();
        if (!(f == null || (a2 = Fw1.a(f, new Lv1(192, 192), null, 2, null)) == null)) {
            arrayList2.add(a2);
        }
        Cc0[] b = uw1.b();
        ArrayList arrayList5 = new ArrayList();
        for (Cc0 cc0 : b) {
            if (!Wg6.a(cc0.b, "!preview.rle")) {
                arrayList5.add(cc0);
            }
        }
        arrayList2.addAll(arrayList5);
        Cc0[] c = uw1.c();
        ArrayList arrayList6 = new ArrayList();
        for (Cc0 cc02 : c) {
            if (!Wg6.a(cc02.b, "display_name")) {
                arrayList6.add(cc02);
            }
        }
        arrayList3.addAll(arrayList6);
        String a3 = Iy1.a(e());
        Charset charset = Et7.a;
        if (a3 != null) {
            byte[] bytes = a3.getBytes(charset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            arrayList3.add(new Cc0("display_name", bytes));
            int length = uw1.f().length;
            for (int i2 = 1; i2 < length; i2++) {
                arrayList.add(uw1.f()[i2]);
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("barPos", new JSONObject().put(ViewHierarchy.DIMENSION_TOP_KEY, this.h.a()).put("middle", this.i.a()).put("bot", this.j.a()));
            String jSONObject2 = jSONObject.toString();
            Wg6.b(jSONObject2, "configNodeJSON.toString()");
            String a4 = Iy1.a(jSONObject2);
            Charset c2 = Hd0.y.c();
            if (a4 != null) {
                byte[] bytes2 = a4.getBytes(c2);
                Wg6.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                arrayList4.add(new Cc0("customWatchFace", bytes2));
                Ry1 h2 = uw1.h();
                Jw1 jw1 = uw1.g().b;
                Ry1 ry1 = new Ry1(uw1.g().c.getMajor(), b().getMinor());
                boolean z = uw1.g().d;
                byte[] bArr = uw1.g().e;
                byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
                Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                Yb0 yb0 = new Yb0(jw1, ry1, z, copyOf);
                Object[] array = arrayList.toArray(new Cc0[0]);
                if (array != null) {
                    Cc0[] cc0Arr = (Cc0[]) array;
                    Object[] array2 = arrayList2.toArray(new Cc0[0]);
                    if (array2 != null) {
                        Cc0[] cc0Arr2 = (Cc0[]) array2;
                        Cc0[] d = uw1.d();
                        Cc0[] e = uw1.e();
                        Object[] array3 = arrayList3.toArray(new Cc0[0]);
                        if (array3 != null) {
                            Cc0[] cc0Arr3 = (Cc0[]) array3;
                            Object[] array4 = arrayList4.toArray(new Cc0[0]);
                            if (array4 != null) {
                                return new Tw1(h2, yb0, cc0Arr, cc0Arr2, d, e, cc0Arr3, (Cc0[]) array4, uw1.i());
                            }
                            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor
    public Ec0 g() {
        return this.g;
    }
}
