package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.stetho.dumpapp.Framer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn1 extends ym1 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b(null);
    @DexIgnore
    public /* final */ a c;

    @DexIgnore
    public enum a {
        OFF((byte) 0),
        LOW(Framer.STDERR_FRAME_PREFIX),
        MEDIUM((byte) 75),
        HIGH((byte) 100);
        
        @DexIgnore
        public static /* final */ C0111a d; // = new C0111a(null);
        @DexIgnore
        public /* final */ byte b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hn1$a$a")
        /* renamed from: com.fossil.hn1$a$a  reason: collision with other inner class name */
        public static final class C0111a {
            @DexIgnore
            public /* synthetic */ C0111a(kq7 kq7) {
            }

            @DexIgnore
            public final a a(byte b) {
                boolean z = false;
                if (true == (b <= 25)) {
                    return a.OFF;
                }
                if (true == (b <= 50)) {
                    return a.LOW;
                }
                if (b <= 75) {
                    z = true;
                }
                return true == z ? a.MEDIUM : a.HIGH;
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<hn1> {
        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
        }

        @DexIgnore
        public final hn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new hn1(a.d.a(bArr[0]));
            }
            throw new IllegalArgumentException(e.b(e.e("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public hn1 b(Parcel parcel) {
            return new hn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hn1 createFromParcel(Parcel parcel) {
            return new hn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hn1[] newArray(int i) {
            return new hn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ hn1(Parcel parcel, kq7 kq7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            pq7.b(readString, "parcel.readString()!!");
            this.c = a.valueOf(readString);
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public hn1(a aVar) {
        super(zm1.VIBE_STRENGTH);
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).array();
        pq7.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public String c() {
        return ey1.a(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(hn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((hn1) obj).c;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
    }

    @DexIgnore
    public final a getVibeStrengthLevel() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ym1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
