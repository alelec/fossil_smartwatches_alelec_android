package com.fossil;

import com.fossil.E13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hu2 extends E13<Hu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Hu2 zzl;
    @DexIgnore
    public static volatile Z23<Hu2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public M13<Iu2> zzf; // = E13.B();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public Ju2 zzh;
    @DexIgnore
    public boolean zzi;
    @DexIgnore
    public boolean zzj;
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Hu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Hu2.zzl);
        }

        @DexIgnore
        public /* synthetic */ Ai(Fu2 fu2) {
            this();
        }

        @DexIgnore
        public final String B() {
            return ((Hu2) this.c).K();
        }

        @DexIgnore
        public final int C() {
            return ((Hu2) this.c).M();
        }

        @DexIgnore
        public final Ai x(int i, Iu2 iu2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Hu2) this.c).D(i, iu2);
            return this;
        }

        @DexIgnore
        public final Ai y(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Hu2) this.c).H(str);
            return this;
        }

        @DexIgnore
        public final Iu2 z(int i) {
            return ((Hu2) this.c).C(i);
        }
    }

    /*
    static {
        Hu2 hu2 = new Hu2();
        zzl = hu2;
        E13.u(Hu2.class, hu2);
    }
    */

    @DexIgnore
    public static Ai T() {
        return (Ai) zzl.w();
    }

    @DexIgnore
    public final Iu2 C(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final void D(int i, Iu2 iu2) {
        iu2.getClass();
        M13<Iu2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = E13.q(m13);
        }
        this.zzf.set(i, iu2);
    }

    @DexIgnore
    public final void H(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final boolean I() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int J() {
        return this.zzd;
    }

    @DexIgnore
    public final String K() {
        return this.zze;
    }

    @DexIgnore
    public final List<Iu2> L() {
        return this.zzf;
    }

    @DexIgnore
    public final int M() {
        return this.zzf.size();
    }

    @DexIgnore
    public final boolean N() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final Ju2 O() {
        Ju2 ju2 = this.zzh;
        return ju2 == null ? Ju2.N() : ju2;
    }

    @DexIgnore
    public final boolean P() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean Q() {
        return this.zzj;
    }

    @DexIgnore
    public final boolean R() {
        return (this.zzc & 64) != 0;
    }

    @DexIgnore
    public final boolean S() {
        return this.zzk;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Hu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001\u1004\u0000\u0002\u1008\u0001\u0003\u001b\u0004\u1007\u0002\u0005\u1009\u0003\u0006\u1007\u0004\u0007\u1007\u0005\b\u1007\u0006", new Object[]{"zzc", "zzd", "zze", "zzf", Iu2.class, "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                Z23<Hu2> z232 = zzm;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Hu2.class) {
                    try {
                        z23 = zzm;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzl);
                            zzm = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
