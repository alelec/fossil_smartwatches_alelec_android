package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mp4 implements Factory<LinkStreamingManager> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<BuddyChallengeManager> c;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> d;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> e;
    @DexIgnore
    public /* final */ Provider<An4> f;
    @DexIgnore
    public /* final */ Provider<SetNotificationUseCase> g;
    @DexIgnore
    public /* final */ Provider<MusicControlComponent> h;
    @DexIgnore
    public /* final */ Provider<WorkoutTetherGpsManager> i;

    @DexIgnore
    public Mp4(Uo4 uo4, Provider<HybridPresetRepository> provider, Provider<BuddyChallengeManager> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<An4> provider5, Provider<SetNotificationUseCase> provider6, Provider<MusicControlComponent> provider7, Provider<WorkoutTetherGpsManager> provider8) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
    }

    @DexIgnore
    public static Mp4 a(Uo4 uo4, Provider<HybridPresetRepository> provider, Provider<BuddyChallengeManager> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<An4> provider5, Provider<SetNotificationUseCase> provider6, Provider<MusicControlComponent> provider7, Provider<WorkoutTetherGpsManager> provider8) {
        return new Mp4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static LinkStreamingManager c(Uo4 uo4, HybridPresetRepository hybridPresetRepository, BuddyChallengeManager buddyChallengeManager, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, An4 an4, SetNotificationUseCase setNotificationUseCase, MusicControlComponent musicControlComponent, WorkoutTetherGpsManager workoutTetherGpsManager) {
        LinkStreamingManager t = uo4.t(hybridPresetRepository, buddyChallengeManager, quickResponseRepository, alarmsRepository, an4, setNotificationUseCase, musicControlComponent, workoutTetherGpsManager);
        Lk7.c(t, "Cannot return null from a non-@Nullable @Provides method");
        return t;
    }

    @DexIgnore
    public LinkStreamingManager b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
