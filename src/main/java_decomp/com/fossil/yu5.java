package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yu5 implements Factory<UpdateUser> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public Yu5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Yu5 a(Provider<UserRepository> provider) {
        return new Yu5(provider);
    }

    @DexIgnore
    public static UpdateUser c(UserRepository userRepository) {
        return new UpdateUser(userRepository);
    }

    @DexIgnore
    public UpdateUser b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
