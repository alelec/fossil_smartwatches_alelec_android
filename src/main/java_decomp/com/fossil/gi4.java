package com.fossil;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gi4 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ ArrayDeque<String> d; // = new ArrayDeque<>();
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public boolean f; // = false;

    @DexIgnore
    public Gi4(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        this.a = sharedPreferences;
        this.b = str;
        this.c = str2;
        this.e = executor;
    }

    @DexIgnore
    public static Gi4 c(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        Gi4 gi4 = new Gi4(sharedPreferences, str, str2, executor);
        gi4.d();
        return gi4;
    }

    @DexIgnore
    public final /* synthetic */ void a() {
        h();
    }

    @DexIgnore
    public final boolean b(boolean z) {
        if (z && !this.f) {
            i();
        }
        return z;
    }

    @DexIgnore
    public final void d() {
        synchronized (this.d) {
            this.d.clear();
            String string = this.a.getString(this.b, "");
            if (!TextUtils.isEmpty(string) && string.contains(this.c)) {
                String[] split = string.split(this.c, -1);
                if (split.length == 0) {
                    Log.e("FirebaseMessaging", "Corrupted queue. Please check the queue contents and item separator provided");
                }
                for (String str : split) {
                    if (!TextUtils.isEmpty(str)) {
                        this.d.add(str);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final String e() {
        String peek;
        synchronized (this.d) {
            peek = this.d.peek();
        }
        return peek;
    }

    @DexIgnore
    public final boolean f(Object obj) {
        boolean remove;
        synchronized (this.d) {
            remove = this.d.remove(obj);
            b(remove);
        }
        return remove;
    }

    @DexIgnore
    public final String g() {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = this.d.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            sb.append(this.c);
        }
        return sb.toString();
    }

    @DexIgnore
    public final void h() {
        synchronized (this.d) {
            this.a.edit().putString(this.b, g()).commit();
        }
    }

    @DexIgnore
    public final void i() {
        this.e.execute(new Fi4(this));
    }
}
