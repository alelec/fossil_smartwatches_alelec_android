package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qb5 extends Pb5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public long t;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        v = sparseIntArray;
        sparseIntArray.put(2131362731, 1);
        v.put(2131362672, 2);
    }
    */

    @DexIgnore
    public Qb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 3, u, v));
    }

    @DexIgnore
    public Qb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ImageView) objArr[2], (ImageView) objArr[1]);
        this.t = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.s = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.t != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.t = 1;
        }
        w();
    }
}
