package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ix1;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Gu1 d;
    @DexIgnore
    public /* final */ Fu1 e;
    @DexIgnore
    public /* final */ Iu1 f;
    @DexIgnore
    public /* final */ byte[] g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Wp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wp1 createFromParcel(Parcel parcel) {
            return new Wp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wp1[] newArray(int i) {
            return new Wp1[i];
        }
    }

    @DexIgnore
    public Wp1(byte b, Gu1 gu1, Fu1 fu1, Iu1 iu1, byte[] bArr, int i2, int i3) {
        super(E90.ENCRYPTED_DATA, b);
        this.d = gu1;
        this.e = fu1;
        this.f = iu1;
        this.g = bArr;
        this.h = i2;
        this.i = i3;
    }

    @DexIgnore
    public /* synthetic */ Wp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Gu1 a2 = Gu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.d = a2;
            Fu1 a3 = Fu1.d.a(parcel.readByte());
            if (a3 != null) {
                this.e = a3;
                Iu1 a4 = Iu1.d.a(parcel.readByte());
                if (a4 != null) {
                    this.f = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.g = createByteArray == null ? new byte[0] : createByteArray;
                    this.h = parcel.readInt();
                    this.i = parcel.readInt();
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Wp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Wp1 wp1 = (Wp1) obj;
            if (this.d != wp1.d) {
                return false;
            }
            if (this.e != wp1.e) {
                return false;
            }
            if (this.f != wp1.f) {
                return false;
            }
            if (!Arrays.equals(this.g, wp1.g)) {
                return false;
            }
            if (this.h != wp1.h) {
                return false;
            }
            return this.i == wp1.i;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.EncryptedDataNotification");
    }

    @DexIgnore
    public final Fu1 getEncryptMethod() {
        return this.e;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.g;
    }

    @DexIgnore
    public final Gu1 getEncryptedDataType() {
        return this.d;
    }

    @DexIgnore
    public final Iu1 getKeyType() {
        return this.f;
    }

    @DexIgnore
    public final short getSequence() {
        return Hy1.p(a());
    }

    @DexIgnore
    public final int getXorKeyFirstOffset() {
        return this.h;
    }

    @DexIgnore
    public final int getXorKeySecondOffset() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + Integer.valueOf(this.h).hashCode()) * 31) + Integer.valueOf(this.i).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(super.toJSONObject(), Jd0.j0, Ey1.a(this.d)), Jd0.k5, Ey1.a(this.e)), Jd0.l5, Ey1.a(this.f)), Jd0.U0, Long.valueOf(Ix1.a.b(this.g, Ix1.Ai.CRC32))), Jd0.z5, Integer.valueOf(this.h)), Jd0.A5, Integer.valueOf(this.i));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.g);
        }
        if (parcel != null) {
            parcel.writeInt(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(this.i);
        }
    }
}
