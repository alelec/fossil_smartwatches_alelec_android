package com.fossil;

import android.os.Parcelable;
import com.fossil.Sx1;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V9 extends Vx1<El1[], El1[]> {
    @DexIgnore
    public static /* final */ Qx1<El1[]>[] b; // = {new D9(), new G9(), new J9(new Ry1(3, 0))};
    @DexIgnore
    public static /* final */ Rx1<El1[]>[] c; // = {new M9(Ob.n.c), new P9(Ob.n.c), new S9(Ob.n.c, (byte) 255, new Ry1(3, 0))};
    @DexIgnore
    public static /* final */ V9 d; // = new V9();

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<El1[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<El1[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(El1[] el1Arr) {
        Gl1 gl1;
        ByteBuffer allocate = ByteBuffer.allocate(el1Arr.length * 3);
        for (El1 el1 : el1Arr) {
            Gl1[] b2 = el1.b();
            int length = b2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    gl1 = null;
                    break;
                }
                gl1 = b2[i];
                if (gl1 instanceof Hl1) {
                    break;
                }
                i++;
            }
            if (gl1 != null) {
                allocate.put(gl1.b());
            }
        }
        byte[] array = allocate.array();
        Wg6.b(array, "entryDataBuffer.array()");
        return array;
    }

    @DexIgnore
    public final El1[] j(byte[] bArr) throws Sx1 {
        byte[] a2 = Vx1.a.a(bArr);
        if (a2.length % 3 == 0) {
            ArrayList arrayList = new ArrayList();
            Ur7 l = Bs7.l(Bs7.m(0, a2.length), 3);
            int a3 = l.a();
            int b2 = l.b();
            int c2 = l.c();
            if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                while (true) {
                    Hl1 a4 = Hl1.CREATOR.a(Dm7.k(a2, a3, a3 + 3));
                    Parcelable jl1 = a4 instanceof Kl1 ? new Jl1((Kl1) a4, null, null, 6, null) : a4 instanceof Ol1 ? new Nl1((Ol1) a4, null, null, 6, null) : null;
                    if (jl1 != null) {
                        arrayList.add(jl1);
                    }
                    if (a3 == b2) {
                        break;
                    }
                    a3 += c2;
                }
            }
            Object[] array = arrayList.toArray(new El1[0]);
            if (array != null) {
                return (El1[]) array;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new Sx1(Sx1.Ai.INVALID_FILE_DATA, E.b(E.e("Size("), a2.length, ") not divide to 3."), null, 4, null);
    }

    @DexIgnore
    public final byte[] k(El1[] el1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (El1 el1 : el1Arr) {
            byteArrayOutputStream.write(el1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final El1[] l(byte[] bArr) throws Sx1 {
        try {
            return El1.CREATOR.a(Vx1.a.a(bArr));
        } catch (IllegalArgumentException e) {
            throw new Sx1(Sx1.Ai.INVALID_FILE_DATA, e.getLocalizedMessage(), e);
        }
    }
}
