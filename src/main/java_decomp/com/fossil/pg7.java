package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pg7 extends Ng7 {
    @DexIgnore
    public static /* final */ Jg7 m;

    /*
    static {
        Jg7 jg7 = new Jg7();
        m = jg7;
        jg7.f("A9VH9B8L4GX4");
    }
    */

    @DexIgnore
    public Pg7(Context context) {
        super(context, 0, m);
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.i;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        Ji7.d(jSONObject, "actky", Fg7.s(this.j));
        return true;
    }
}
