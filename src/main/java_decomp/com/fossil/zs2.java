package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zs2 {
    @DexIgnore
    public static volatile Zs2 i; // = null;
    @DexIgnore
    public static Boolean j; // = null;
    @DexIgnore
    public static String k; // = "allow_remote_dynamite";
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Ef2 b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Fg3 d;
    @DexIgnore
    public List<Pair<Sn3, Ci>> e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public T93 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Ai implements Runnable {
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public Ai(Zs2 zs2) {
            this(true);
        }

        @DexIgnore
        public Ai(boolean z) {
            this.b = Zs2.this.b.b();
            this.c = Zs2.this.b.c();
            this.d = z;
        }

        @DexIgnore
        public abstract void a() throws RemoteException;

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void run() {
            if (Zs2.this.g) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e2) {
                Zs2.this.o(e2, false, this.d);
                b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
            Zs2.this.k(new Zt2(this, activity, bundle));
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
            Zs2.this.k(new Eu2(this, activity));
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            Zs2.this.k(new Au2(this, activity));
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            Zs2.this.k(new Bu2(this, activity));
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            R93 r93 = new R93();
            Zs2.this.k(new Cu2(this, activity, r93));
            Bundle n = r93.n(50);
            if (n != null) {
                bundle.putAll(n);
            }
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
            Zs2.this.k(new Yt2(this, activity));
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
            Zs2.this.k(new Du2(this, activity));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ts2 {
        @DexIgnore
        public /* final */ Sn3 b;

        @DexIgnore
        public Ci(Sn3 sn3) {
            this.b = sn3;
        }

        @DexIgnore
        @Override // com.fossil.Us2
        public final void C1(String str, String str2, Bundle bundle, long j) {
            this.b.a(str, str2, bundle, j);
        }

        @DexIgnore
        @Override // com.fossil.Us2
        public final int zza() {
            return System.identityHashCode(this.b);
        }
    }

    @DexIgnore
    public Zs2(Context context, String str, String str2, String str3, Bundle bundle) {
        boolean z = true;
        if (str == null || !K(str2, str3)) {
            this.a = "FA";
        } else {
            this.a = str;
        }
        this.b = Hf2.d();
        this.c = C13.a().a(new It2(this), Q93.a);
        this.d = new Fg3(this);
        if (!(!Q(context) || X())) {
            this.g = true;
            Log.w(this.a, "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection.");
            return;
        }
        if (!K(str2, str3)) {
            if (str2 == null || str3 == null) {
                if ((str3 != null ? false : z) ^ (str2 == null)) {
                    Log.w(this.a, "Specified origin or custom app id is null. Both parameters will be ignored.");
                }
            } else {
                Log.v(this.a, "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy");
            }
        }
        k(new Ct2(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            Log.w(this.a, "Unable to register lifecycle notifications. Application null.");
        } else {
            application.registerActivityLifecycleCallbacks(new Bi());
        }
    }

    @DexIgnore
    public static boolean K(String str, String str2) {
        return (str2 == null || str == null || X()) ? false : true;
    }

    @DexIgnore
    public static boolean Q(Context context) {
        try {
            return Yo3.a(context, "google_app_id") != null;
        } catch (IllegalStateException e2) {
        }
    }

    @DexIgnore
    public static int R(Context context) {
        return DynamiteModule.c(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static int T(Context context) {
        return DynamiteModule.a(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static void V(Context context) {
        synchronized (Zs2.class) {
            try {
                if (j != null) {
                    try {
                    } catch (Throwable th) {
                        throw th;
                    }
                } else if (x(context, "app_measurement_internal_disable_startup_flags")) {
                    j = Boolean.FALSE;
                } else {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
                    j = Boolean.valueOf(sharedPreferences.getBoolean(k, false));
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.remove(k);
                    edit.apply();
                }
            } catch (Exception e2) {
                Log.e("FA", "Exception reading flag from SharedPreferences.", e2);
                j = Boolean.FALSE;
            }
        }
    }

    @DexIgnore
    public static boolean X() {
        try {
            Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public static Zs2 a(Context context) {
        return b(context, null, null, null, null);
    }

    @DexIgnore
    public static Zs2 b(Context context, String str, String str2, String str3, Bundle bundle) {
        Rc2.k(context);
        if (i == null) {
            synchronized (Zs2.class) {
                try {
                    if (i == null) {
                        i = new Zs2(context, str, str2, str3, bundle);
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return i;
    }

    @DexIgnore
    public static boolean x(Context context, String str) {
        Rc2.g(str);
        try {
            ApplicationInfo c2 = Ag2.a(context).c(context.getPackageName(), 128);
            if (!(c2 == null || c2.metaData == null)) {
                return c2.metaData.getBoolean(str);
            }
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return false;
    }

    @DexIgnore
    public final List<Bundle> B(String str, String str2) {
        R93 r93 = new R93();
        k(new Dt2(this, str, str2, r93));
        List<Bundle> list = (List) R93.e(r93.n(5000), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public final void D(String str) {
        k(new Jt2(this, str));
    }

    @DexIgnore
    public final void E(String str, String str2, Bundle bundle) {
        k(new Et2(this, str, str2, bundle));
    }

    @DexIgnore
    public final void F(boolean z) {
        k(new Ut2(this, z));
    }

    @DexIgnore
    public final String I() {
        R93 r93 = new R93();
        k(new Kt2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final void J(String str) {
        k(new Lt2(this, str));
    }

    @DexIgnore
    public final int M(String str) {
        R93 r93 = new R93();
        k(new Tt2(this, str, r93));
        Integer num = (Integer) R93.e(r93.n(ButtonService.CONNECT_TIMEOUT), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    @DexIgnore
    public final String N() {
        R93 r93 = new R93();
        k(new Nt2(this, r93));
        return r93.i(50);
    }

    @DexIgnore
    public final long P() {
        R93 r93 = new R93();
        k(new Mt2(this, r93));
        Long l = (Long) R93.e(r93.n(500), Long.class);
        if (l != null) {
            return l.longValue();
        }
        long nextLong = new Random(System.nanoTime() ^ this.b.b()).nextLong();
        int i2 = this.f + 1;
        this.f = i2;
        return nextLong + ((long) i2);
    }

    @DexIgnore
    public final String S() {
        R93 r93 = new R93();
        k(new Pt2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final String U() {
        R93 r93 = new R93();
        k(new Ot2(this, r93));
        return r93.i(500);
    }

    @DexIgnore
    public final T93 c(Context context, boolean z) {
        DynamiteModule.b bVar;
        if (z) {
            try {
                bVar = DynamiteModule.l;
            } catch (DynamiteModule.a e2) {
                o(e2, true, false);
                return null;
            }
        } else {
            bVar = DynamiteModule.j;
        }
        return S93.asInterface(DynamiteModule.e(context, bVar, ModuleDescriptor.MODULE_ID).d("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
    }

    @DexIgnore
    public final Fg3 e() {
        return this.d;
    }

    @DexIgnore
    public final Map<String, Object> g(String str, String str2, boolean z) {
        R93 r93 = new R93();
        k(new St2(this, str, str2, z, r93));
        Bundle n = r93.n(5000);
        if (n == null || n.size() == 0) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(n.size());
        for (String str3 : n.keySet()) {
            Object obj = n.get(str3);
            if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                hashMap.put(str3, obj);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void h(int i2, String str, Object obj, Object obj2, Object obj3) {
        k(new Rt2(this, false, 5, str, obj, null, null));
    }

    @DexIgnore
    public final void i(Activity activity, String str, String str2) {
        k(new Ft2(this, activity, str, str2));
    }

    @DexIgnore
    public final void j(Bundle bundle) {
        k(new Bt2(this, bundle));
    }

    @DexIgnore
    public final void k(Ai ai) {
        this.c.execute(ai);
    }

    @DexIgnore
    public final void n(Sn3 sn3) {
        Rc2.k(sn3);
        k(new Vt2(this, sn3));
    }

    @DexIgnore
    public final void o(Exception exc, boolean z, boolean z2) {
        this.g |= z;
        if (z) {
            Log.w(this.a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            h(5, "Error with data collection. Data lost.", exc, null, null);
        }
        Log.w(this.a, "Error with data collection. Data lost.", exc);
    }

    @DexIgnore
    public final void p(String str) {
        k(new Gt2(this, str));
    }

    @DexIgnore
    public final void q(String str, Bundle bundle) {
        t(null, str, bundle, false, true, null);
    }

    @DexIgnore
    public final void r(String str, String str2) {
        v(null, str, str2, false);
    }

    @DexIgnore
    public final void s(String str, String str2, Bundle bundle) {
        t(str, str2, bundle, true, true, null);
    }

    @DexIgnore
    public final void t(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l) {
        k(new Xt2(this, l, str, str2, bundle, z, z2));
    }

    @DexIgnore
    public final void u(String str, String str2, Object obj) {
        v(str, str2, obj, true);
    }

    @DexIgnore
    public final void v(String str, String str2, Object obj, boolean z) {
        k(new Wt2(this, str, str2, obj, z));
    }

    @DexIgnore
    public final void w(boolean z) {
        k(new Ht2(this, z));
    }
}
