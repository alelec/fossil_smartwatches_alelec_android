package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.L72;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V92<T> extends Ja2 {
    @DexIgnore
    public /* final */ Ot3<T> b;

    @DexIgnore
    public V92(int i, Ot3<T> ot3) {
        super(i);
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public void b(Status status) {
        this.b.d(new N62(status));
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void c(L72.Ai<?> ai) throws DeadObjectException {
        try {
            i(ai);
        } catch (DeadObjectException e) {
            b(Z82.a(e));
            throw e;
        } catch (RemoteException e2) {
            b(Z82.a(e2));
        } catch (RuntimeException e3) {
            e(e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public void e(Exception exc) {
        this.b.d(exc);
    }

    @DexIgnore
    public abstract void i(L72.Ai<?> ai) throws RemoteException;
}
