package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O35 extends N35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d N;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public /* final */ ConstraintLayout L;
    @DexIgnore
    public long M;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(22);
        N = dVar;
        dVar.a(1, new String[]{"item_default_place_commute_time", "item_default_place_commute_time"}, new int[]{2, 3}, new int[]{2131558680, 2131558680});
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131362593, 4);
        O.put(2131362546, 5);
        O.put(2131362052, 6);
        O.put(2131362761, 7);
        O.put(2131362662, 8);
        O.put(2131363433, 9);
        O.put(2131361903, 10);
        O.put(2131362775, 11);
        O.put(2131362133, 12);
        O.put(2131362787, 13);
        O.put(2131362741, 14);
        O.put(2131362783, 15);
        O.put(2131362359, 16);
        O.put(2131362820, 17);
        O.put(2131363050, 18);
        O.put(2131361921, 19);
        O.put(2131362801, 20);
        O.put(2131363072, 21);
    }
    */

    @DexIgnore
    public O35(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 22, N, O));
    }

    @DexIgnore
    public O35(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 2, (FlexibleAutoCompleteTextView) objArr[10], (View) objArr[19], (ConstraintLayout) objArr[6], (RTLImageView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[5], (RTLImageView) objArr[4], (Ee5) objArr[2], (Ee5) objArr[3], (RTLImageView) objArr[8], (RTLImageView) objArr[14], (RTLImageView) objArr[7], (ConstraintLayout) objArr[11], (View) objArr[15], (ImageView) objArr[13], (LinearLayout) objArr[20], (LinearLayout) objArr[17], (ConstraintLayout) objArr[0], (RecyclerView) objArr[18], (FlexibleSwitchCompat) objArr[21], (View) objArr[9]);
        this.M = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[1];
        this.L = constraintLayout;
        constraintLayout.setTag(null);
        this.H.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.M = 0;
        }
        ViewDataBinding.i(this.x);
        ViewDataBinding.i(this.y);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r6.y.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.x.o() != false) goto L_?;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.M     // Catch:{ all -> 0x001f }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
            com.fossil.Ee5 r1 = r6.x
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            com.fossil.Ee5 r1 = r6.y
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x001f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.O35.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.M = 4;
        }
        this.x.q();
        this.y.q();
        w();
    }
}
