package com.fossil;

import com.fossil.E13;
import java.io.IOException;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N03 implements R43 {
    @DexIgnore
    public /* final */ L03 a;

    @DexIgnore
    public N03(L03 l03) {
        H13.f(l03, "output");
        L03 l032 = l03;
        this.a = l032;
        l032.a = this;
    }

    @DexIgnore
    public static N03 g(L03 l03) {
        N03 n03 = l03.a;
        return n03 != null ? n03 : new N03(l03);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void a(int i, List<?> list, F33 f33) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            d(i, list.get(i2), f33);
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void b(int i, Object obj, F33 f33) throws IOException {
        this.a.q(i, (M23) obj, f33);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final <K, V> void c(int i, H23<K, V> h23, Map<K, V> map) throws IOException {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            this.a.m(i, 2);
            this.a.O(E23.a(h23, entry.getKey(), entry.getValue()));
            E23.b(this.a, h23, entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void d(int i, Object obj, F33 f33) throws IOException {
        L03 l03 = this.a;
        l03.m(i, 3);
        f33.b((M23) obj, l03.a);
        l03.m(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void e(int i, List<?> list, F33 f33) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, list.get(i2), f33);
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void f(int i, Xz2 xz2) throws IOException {
        this.a.o(i, xz2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final int zza() {
        return E13.Fi.k;
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i) throws IOException {
        this.a.m(i, 3);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, double d) throws IOException {
        this.a.k(i, d);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, float f) throws IOException {
        this.a.l(i, f);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, int i2) throws IOException {
        this.a.k0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, long j) throws IOException {
        this.a.n(i, j);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof Xz2) {
            this.a.R(i, (Xz2) obj);
        } else {
            this.a.p(i, (M23) obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, String str) throws IOException {
        this.a.r(i, str);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof W13) {
            W13 w13 = (W13) list;
            while (i2 < list.size()) {
                Object zzb = w13.zzb(i2);
                if (zzb instanceof String) {
                    this.a.r(i, (String) zzb);
                } else {
                    this.a.o(i, (Xz2) zzb);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.r(i, list.get(i2));
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.l0(list.get(i4).intValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zza(int i, boolean z) throws IOException {
        this.a.s(i, z);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzb(int i) throws IOException {
        this.a.m(i, 4);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzb(int i, int i2) throws IOException {
        this.a.P(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzb(int i, long j) throws IOException {
        this.a.Z(i, j);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzb(int i, List<Xz2> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.o(i, list.get(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.x0(list.get(i4).intValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.f0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzc(int i, int i2) throws IOException {
        this.a.P(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzc(int i, long j) throws IOException {
        this.a.n(i, j);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.e0(list.get(i4).longValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzd(int i, int i2) throws IOException {
        this.a.k0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzd(int i, long j) throws IOException {
        this.a.Z(i, j);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.j0(list.get(i4).longValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.t(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.n(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zze(int i, int i2) throws IOException {
        this.a.Y(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zze(int i, long j) throws IOException {
        this.a.Q(i, j);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += L03.s0(list.get(i4).longValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.a0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Z(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzf(int i, int i2) throws IOException {
        this.a.g0(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += L03.A(list.get(i4).floatValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.i(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.l(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += L03.z(list.get(i4).doubleValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.h(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.C0(list.get(i4).intValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.j(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.P(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.L(list.get(i4).booleanValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.y(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.s(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += L03.p0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.O(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Y(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += L03.A0(list.get(i4).intValue());
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.f0(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.k0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.w0(list.get(i4).longValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.a0(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Z(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.t0(list.get(i4).intValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.X(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.g0(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.R43
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.m(i, 2);
            int i3 = 0;
            int i4 = 0;
            while (i4 < list.size()) {
                i4++;
                i3 = L03.o0(list.get(i4).longValue()) + i3;
            }
            this.a.O(i3);
            while (i2 < list.size()) {
                this.a.S(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.Q(i, list.get(i2).longValue());
            i2++;
        }
    }
}
