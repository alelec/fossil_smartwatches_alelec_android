package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E72 extends UnsupportedOperationException {
    @DexIgnore
    public /* final */ B62 zzbe;

    @DexIgnore
    public E72(B62 b62) {
        this.zzbe = b62;
    }

    @DexIgnore
    public final String getMessage() {
        String valueOf = String.valueOf(this.zzbe);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
        sb.append("Missing ");
        sb.append(valueOf);
        return sb.toString();
    }
}
