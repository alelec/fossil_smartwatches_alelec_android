package com.fossil;

import com.fossil.Rn7;
import com.mapped.Af6;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Ve6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Dv7 extends Ve6 implements Rn7 {
    @DexIgnore
    public static /* final */ Ai b; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends On7<Rn7, Dv7> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<Af6.Bi, Dv7> {
            @DexIgnore
            public static /* final */ Aii INSTANCE; // = new Aii();

            @DexIgnore
            public Aii() {
                super(1);
            }

            @DexIgnore
            public final Dv7 invoke(Af6.Bi bi) {
                throw null;
                //return (Dv7) (!(bi instanceof Dv7) ? null : bi);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Dv7 invoke(Af6.Bi bi) {
                return invoke(bi);
            }
        }

        @DexIgnore
        public Ai() {
            super(Rn7.p, Aii.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public Dv7() {
        super(Rn7.p);
    }

    @DexIgnore
    public abstract void M(Af6 af6, Runnable runnable);

    @DexIgnore
    public void P(Af6 af6, Runnable runnable) {
        M(af6, runnable);
    }

    @DexIgnore
    public boolean Q(Af6 af6) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Rn7
    public void a(Xe6<?> xe6) {
        if (xe6 != null) {
            Lu7<?> n = ((Vv7) xe6).n();
            if (n != null) {
                n.o();
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<*>");
    }

    @DexIgnore
    @Override // com.fossil.Rn7
    public final <T> Xe6<T> c(Xe6<? super T> xe6) {
        return new Vv7(this, xe6);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6, com.mapped.Af6.Bi
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        return (E) Rn7.Ai.a(this, ci);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Ve6
    public Af6 minusKey(Af6.Ci<?> ci) {
        return Rn7.Ai.b(this, ci);
    }

    @DexIgnore
    public String toString() {
        return Ov7.a(this) + '@' + Ov7.b(this);
    }
}
