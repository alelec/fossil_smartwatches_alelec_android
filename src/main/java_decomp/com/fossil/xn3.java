package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ String i;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 j;

    @DexIgnore
    public Xn3(Un3 un3, String str, String str2, long j2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.j = un3;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.e = bundle;
        this.f = z;
        this.g = z2;
        this.h = z3;
        this.i = str3;
    }

    @DexIgnore
    public final void run() {
        this.j.O(this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
    }
}
