package com.fossil;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Kx5;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q56 extends BaseFragment implements P56, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public O56 g;
    @DexIgnore
    public G37<H95> h;
    @DexIgnore
    public Kx5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Q56.k;
        }

        @DexIgnore
        public final Q56 b() {
            return new Q56();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Kx5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Q56 a;

        @DexIgnore
        public Bi(Q56 q56) {
            this.a = q56;
        }

        @DexIgnore
        @Override // com.fossil.Kx5.Ai
        public void a(J06 j06) {
            Wg6.c(j06, "contactWrapper");
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.b0(childFragmentManager, j06, j06.getCurrentHandGroup(), Q56.L6(this.a).n());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Q56 b;

        @DexIgnore
        public Ci(Q56 q56) {
            this.b = q56;
        }

        @DexIgnore
        public final void onClick(View view) {
            Q56.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Q56 b;
        @DexIgnore
        public /* final */ /* synthetic */ H95 c;

        @DexIgnore
        public Di(Q56 q56, H95 h95) {
            this.b = q56;
            this.c = h95;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.c.t;
            Wg6.b(rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 > 0 ? 0 : 4);
            Kx5 kx5 = this.b.i;
            if (kx5 != null) {
                kx5.w(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.c.w;
            Wg6.b(alphabetFastScrollRecyclerView, "binding.rvContacts");
            RecyclerView.m layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((LinearLayoutManager) layoutManager).D2(0, 0);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ H95 b;

        @DexIgnore
        public Ei(H95 h95) {
            this.b = h95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                H95 h95 = this.b;
                Wg6.b(h95, "binding");
                h95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ H95 b;

        @DexIgnore
        public Fi(H95 h95) {
            this.b = h95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.q.setText("");
        }
    }

    /*
    static {
        String simpleName = Q56.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ O56 L6(Q56 q56) {
        O56 o56 = q56.g;
        if (o56 != null) {
            return o56;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.P56
    public void A3() {
        Kx5 kx5 = this.i;
        if (kx5 != null) {
            kx5.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        O56 o56 = this.g;
        if (o56 != null) {
            o56.o();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.P56
    public void I(ArrayList<J06> arrayList) {
        Wg6.c(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(O56 o56) {
        N6(o56);
    }

    @DexIgnore
    public void N6(O56 o56) {
        Wg6.c(o56, "presenter");
        this.g = o56;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        J06 j06;
        Wg6.c(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i2 != 2131363373) {
            Kx5 kx5 = this.i;
            if (kx5 != null) {
                kx5.notifyDataSetChanged();
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (j06 = (J06) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                O56 o56 = this.g;
                if (o56 != null) {
                    o56.p(j06);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.P56
    public void T(Cursor cursor) {
        Kx5 kx5 = this.i;
        if (kx5 != null) {
            kx5.l(cursor);
        }
    }

    @DexIgnore
    @Override // com.fossil.P56
    public void V() {
    }

    @DexIgnore
    @Override // com.fossil.P56
    public void n1(List<J06> list, FilterQueryProvider filterQueryProvider, int i2) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        Wg6.c(list, "listContactWrapper");
        Wg6.c(filterQueryProvider, "filterQueryProvider");
        Kx5 kx5 = new Kx5(null, list, i2);
        kx5.z(new Bi(this));
        this.i = kx5;
        if (kx5 != null) {
            kx5.k(filterQueryProvider);
            G37<H95> g37 = this.h;
            if (g37 != null) {
                H95 a2 = g37.a();
                if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                    alphabetFastScrollRecyclerView.setAdapter(this.i);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        H95 h95 = (H95) Aq0.f(layoutInflater, 2131558594, viewGroup, false, A6());
        h95.s.setOnClickListener(new Ci(this));
        h95.q.addTextChangedListener(new Di(this, h95));
        h95.q.setOnFocusChangeListener(new Ei(h95));
        h95.t.setOnClickListener(new Fi(h95));
        h95.w.setIndexBarVisibility(true);
        h95.w.setIndexBarHighLateTextVisibility(true);
        h95.w.setIndexbarHighLateTextColor(2131099703);
        h95.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        h95.w.setIndexTextSize(9);
        Typeface b = Nl0.b(requireContext(), 2131296256);
        String d = ThemeManager.l.a().d("primaryText");
        Typeface f = ThemeManager.l.a().f("nonBrandTextStyle5");
        if (f == null) {
            f = b;
        }
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            h95.w.setIndexBarTextColor(parseColor);
            h95.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (f != null) {
            h95.w.setTypeface(f);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = h95.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext()));
        alphabetFastScrollRecyclerView.setAdapter(this.i);
        this.h = new G37<>(this, h95);
        Wg6.b(h95, "binding");
        return h95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        O56 o56 = this.g;
        if (o56 != null) {
            o56.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        O56 o56 = this.g;
        if (o56 != null) {
            o56.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
