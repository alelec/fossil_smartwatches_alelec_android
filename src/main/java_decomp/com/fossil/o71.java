package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Log;
import com.mapped.W6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface O71 {
    @DexIgnore
    public static final Ai a = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ Ai a; // = new Ai();

        @DexIgnore
        public final O71 a(Context context, Bi bi) {
            Wg6.c(context, "context");
            Wg6.c(bi, "listener");
            ConnectivityManager connectivityManager = (ConnectivityManager) W6.j(context, ConnectivityManager.class);
            if (connectivityManager != null) {
                if (W6.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                    try {
                        return Build.VERSION.SDK_INT >= 21 ? new Q71(connectivityManager, bi) : new P71(context, connectivityManager, bi);
                    } catch (Exception e) {
                        X81.a("NetworkObserverStrategy", new RuntimeException("Failed to register network observer.", e));
                        return L71.b;
                    }
                }
            }
            if (Q81.c.a() && Q81.c.b() <= 5) {
                Log.println(5, "NetworkObserverStrategy", "Unable to register network observer.");
            }
            return L71.b;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    Object start();  // void declaration

    @DexIgnore
    Object stop();  // void declaration
}
