package com.fossil;

import com.crashlytics.android.answers.CustomEvent;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pk1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new HashMap();

    @DexIgnore
    public Pk1(String str) {
        this.a = str;
    }

    @DexIgnore
    public Pk1 a(String str, String str2) {
        this.b.put(str, str2);
        return this;
    }

    @DexIgnore
    public CustomEvent b() {
        CustomEvent customEvent = new CustomEvent(this.a);
        for (String str : this.b.keySet()) {
            Object obj = this.b.get(str);
            if (obj instanceof String) {
                customEvent.putCustomAttribute(str, (String) obj);
            } else if (obj instanceof Number) {
                customEvent.putCustomAttribute(str, (Number) obj);
            }
        }
        return customEvent;
    }
}
