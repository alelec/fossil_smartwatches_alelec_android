package com.fossil;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wy2<E> extends Ay2<E> {
    @DexIgnore
    public static /* final */ Wy2<Object> zza; // = new Wy2<>(new Object[0], 0, null, 0, 0);
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient Object[] e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public /* final */ transient int g;
    @DexIgnore
    public /* final */ transient int h;

    @DexIgnore
    public Wy2(Object[] objArr, int i, Object[] objArr2, int i2, int i3) {
        this.d = objArr;
        this.e = objArr2;
        this.f = i2;
        this.g = i;
        this.h = i3;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean contains(@NullableDecl Object obj) {
        Object[] objArr = this.e;
        if (obj == null || objArr == null) {
            return false;
        }
        int b = Qx2.b(obj);
        while (true) {
            int i = b & this.f;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            b = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final int hashCode() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public final int size() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.d, 0, objArr, i, this.h);
        return this.h + i;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<E> zzb() {
        return (Cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final Sx2<E> zzd() {
        return Sx2.zza(this.d, this.h);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Object[] zze() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzg() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return false;
    }
}
