package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gj2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Gj2> CREATOR; // = new Ij2();
    @DexIgnore
    public /* final */ Uh2 b;
    @DexIgnore
    public /* final */ Si2 c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public Gj2(Uh2 uh2, IBinder iBinder, long j, long j2) {
        this.b = uh2;
        this.c = Ri2.e(iBinder);
        this.d = j;
        this.e = j2;
    }

    @DexIgnore
    public Uh2 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Gj2)) {
            return false;
        }
        Gj2 gj2 = (Gj2) obj;
        return Pc2.a(this.b, gj2.b) && this.d == gj2.d && this.e == gj2.e;
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(this.b, Long.valueOf(this.d), Long.valueOf(this.e));
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, c(), i, false);
        Bd2.m(parcel, 2, this.c.asBinder(), false);
        Bd2.r(parcel, 3, this.d);
        Bd2.r(parcel, 4, this.e);
        Bd2.b(parcel, a2);
    }
}
