package com.fossil;

import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ik7<T> implements Provider<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Provider<T> a;
    @DexIgnore
    public volatile Object b; // = c;

    @DexIgnore
    public Ik7(Provider<T> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static <P extends Provider<T>, T> Provider<T> a(P p) {
        Lk7.b(p);
        return p instanceof Ik7 ? p : new Ik7(p);
    }

    @DexIgnore
    public static Object b(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public T get() {
        T t = (T) this.b;
        if (t == c) {
            synchronized (this) {
                t = this.b;
                if (t == c) {
                    t = this.a.get();
                    b(this.b, t);
                    this.b = t;
                    this.a = null;
                }
            }
        }
        return t;
    }
}
