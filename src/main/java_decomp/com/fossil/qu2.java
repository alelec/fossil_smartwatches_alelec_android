package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qu2 extends E13<Qu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Qu2 zzh;
    @DexIgnore
    public static volatile Z23<Qu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public boolean zzf;
    @DexIgnore
    public int zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Qu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Qu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ Ai(Ou2 ou2) {
            this();
        }

        @DexIgnore
        public final boolean B() {
            return ((Qu2) this.c).H();
        }

        @DexIgnore
        public final boolean C() {
            return ((Qu2) this.c).I();
        }

        @DexIgnore
        public final int E() {
            return ((Qu2) this.c).J();
        }

        @DexIgnore
        public final Ai x(String str) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Qu2) this.c).E(str);
            return this;
        }

        @DexIgnore
        public final String y() {
            return ((Qu2) this.c).C();
        }

        @DexIgnore
        public final boolean z() {
            return ((Qu2) this.c).G();
        }
    }

    /*
    static {
        Qu2 qu2 = new Qu2();
        zzh = qu2;
        E13.u(Qu2.class, qu2);
    }
    */

    @DexIgnore
    public final String C() {
        return this.zzd;
    }

    @DexIgnore
    public final void E(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    @DexIgnore
    public final boolean G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean I() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final int J() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Ou2.a[i - 1]) {
            case 1:
                return new Qu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1008\u0000\u0002\u1007\u0001\u0003\u1007\u0002\u0004\u1004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                Z23<Qu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Qu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
