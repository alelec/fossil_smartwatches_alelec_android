package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Os3 extends Zc2 implements Z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Os3> CREATOR; // = new Qs3();
    @DexIgnore
    public /* final */ List<String> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Os3(List<String> list, String str) {
        this.b = list;
        this.c = str;
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public final Status a() {
        return this.c != null ? Status.f : Status.j;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.w(parcel, 1, this.b, false);
        Bd2.u(parcel, 2, this.c, false);
        Bd2.b(parcel, a2);
    }
}
