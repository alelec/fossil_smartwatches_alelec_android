package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.Af1;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hf1<Data> implements Af1<String, Data> {
    @DexIgnore
    public /* final */ Af1<Uri, Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Bf1<String, AssetFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<String, AssetFileDescriptor> b(Ef1 ef1) {
            return new Hf1(ef1.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Bf1<String, ParcelFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<String, ParcelFileDescriptor> b(Ef1 ef1) {
            return new Hf1(ef1.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Bf1<String, InputStream> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<String, InputStream> b(Ef1 ef1) {
            return new Hf1(ef1.d(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public Hf1(Af1<Uri, Data> af1) {
        this.a = af1;
    }

    @DexIgnore
    public static Uri e(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return f(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? f(str) : parse;
    }

    @DexIgnore
    public static Uri f(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(String str) {
        return d(str);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(String str, int i, int i2, Ob1 ob1) {
        return c(str, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(String str, int i, int i2, Ob1 ob1) {
        Uri e = e(str);
        if (e == null || !this.a.a(e)) {
            return null;
        }
        return this.a.b(e, i, i2, ob1);
    }

    @DexIgnore
    public boolean d(String str) {
        return true;
    }
}
