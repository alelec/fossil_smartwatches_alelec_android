package com.fossil;

import android.os.Build;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.model.request.CustomField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ou5 extends iq4<b, c, a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ long h; // = (pq7.a("release", "release") ? 25044663 : 360000146526L);
    @DexIgnore
    public static /* final */ long i; // = (pq7.a("release", "release") ? 23451689 : 24435966);
    @DexIgnore
    public static /* final */ long j; // = (pq7.a("release", "release") ? 360000024083L : 360000148503L);
    @DexIgnore
    public static /* final */ long k; // = (pq7.a("release", "release") ? 24416029 : 24436006);
    @DexIgnore
    public static /* final */ long l; // = (pq7.a("release", "release") ? 24504683 : 24881463);
    @DexIgnore
    public static /* final */ long m; // = (pq7.a("release", "release") ? 23777683 : 24435986);
    @DexIgnore
    public static /* final */ long n; // = (pq7.a("release", "release") ? 24545186 : 24881503);
    @DexIgnore
    public static /* final */ long o; // = (pq7.a("release", "release") ? 24545246 : 24945726);
    @DexIgnore
    public static /* final */ long p; // = (pq7.a("release", "release") ? 23780847 : 24436086);
    @DexIgnore
    public static /* final */ long q; // = (pq7.a("release", "release") ? 24935086 : 24881543);
    @DexIgnore
    public static /* final */ long r; // = (pq7.a("release", "release") ? 24506223 : 24881523);
    @DexIgnore
    public static /* final */ long s; // = (pq7.a("release", "release") ? 360000156783L : 360000156723L);
    @DexIgnore
    public static /* final */ long t; // = (pq7.a("release", "release") ? 360000156803L : 360000156743L);
    @DexIgnore
    public static /* final */ long u; // = (pq7.a("release", "release") ? 360000156823L : 360000156763L);
    @DexIgnore
    public static /* final */ long v; // = (pq7.a("release", "release") ? 60000157103L : 360000148563L);
    @DexIgnore
    public static /* final */ long w; // = (pq7.a("release", "release") ? 360050695374L : 360053812213L);
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ on5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2726a;

        @DexIgnore
        public b(String str) {
            pq7.c(str, "subject");
            this.f2726a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f2726a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<CustomField> f2727a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public c(List<CustomField> list, List<String> list2, String str, String str2, String str3, String str4) {
            pq7.c(list, "customFieldList");
            pq7.c(list2, "tags");
            pq7.c(str, Constants.EMAIL);
            pq7.c(str2, "userName");
            pq7.c(str3, "subject");
            pq7.c(str4, "additionalInfo");
            this.f2727a = list;
            this.b = list2;
            this.c = str;
            this.d = str2;
            this.e = str3;
            this.f = str4;
        }

        @DexIgnore
        public final String a() {
            return this.f;
        }

        @DexIgnore
        public final List<CustomField> b() {
            return this.f2727a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final List<String> e() {
            return this.b;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.profile.usecase.GetZendeskInformation", f = "GetZendeskInformation.kt", l = {26}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ou5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ou5 ou5, qn7 qn7) {
            super(qn7);
            this.this$0 = ou5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = ou5.class.getSimpleName();
        pq7.b(simpleName, "GetZendeskInformation::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public ou5(UserRepository userRepository, DeviceRepository deviceRepository, on5 on5) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = deviceRepository;
        this.f = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return g;
    }

    @DexIgnore
    public final StringBuilder m(String str, String str2, String str3, String str4, String str5, String str6) {
        StringBuilder sb = new StringBuilder();
        String str7 = Build.DEVICE + " - " + Build.MODEL;
        String str8 = Build.VERSION.RELEASE;
        StringBuilder sb2 = new StringBuilder();
        Locale locale = Locale.getDefault();
        pq7.b(locale, "Locale.getDefault()");
        sb2.append(locale.getLanguage());
        sb2.append("_t");
        String sb3 = sb2.toString();
        String str9 = "";
        String str10 = "";
        String str11 = "";
        for (T t2 : this.e.getAllDevice()) {
            str9 = str9 + " " + t2.getDeviceId();
            if (t2.isActive()) {
                long C = this.f.C(t2.getDeviceId());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str10);
                hr7 hr7 = hr7.f1520a;
                String format = String.format(" %d (%s)", Arrays.copyOf(new Object[]{Long.valueOf(C / ((long) 1000)), lk5.g(new Date(C))}, 2));
                pq7.b(format, "java.lang.String.format(format, *args)");
                sb4.append(format);
                str10 = sb4.toString();
                if (!TextUtils.isEmpty(t2.getActivationDate())) {
                    String k2 = lk5.k(lk5.r0(t2.getActivationDate()));
                    pq7.b(k2, "DateHelper.formatShortDa\u2026e(device.activationDate))");
                    str11 = k2;
                } else {
                    str11 = "";
                }
            }
        }
        sb.append("App Name: " + str);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("App Version: " + str2);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Build number: 27171-2020-11-21");
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Phone Info: " + str7);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("System version: " + str8);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Host Maker: " + str4);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Host Model: " + str5);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Carrier: " + str6);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Language code: " + sb3);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("_______________");
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Serial ");
        if (str3 == null) {
            str3 = "";
        }
        sb5.append(str3);
        sb.append(sb5.toString());
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Last successful sync: " + str10);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("List of paired device: " + str9);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        sb.append("Activation date: " + str11);
        pq7.b(sb, "append(value)");
        rt7.b(sb);
        return sb;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0301  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0309  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0328  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e9 A[SYNTHETIC, Splitter:B:37:0x00e9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x02e6 A[SYNTHETIC, Splitter:B:94:0x02e6] */
    /* renamed from: n */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.ou5.b r31, com.fossil.qn7<? super com.fossil.tl7> r32) {
        /*
        // Method dump skipped, instructions count: 820
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ou5.k(com.fossil.ou5$b, com.fossil.qn7):java.lang.Object");
    }
}
