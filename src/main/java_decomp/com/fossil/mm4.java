package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mm4 implements Nm4 {
    @DexIgnore
    public static void b(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            Qm4.e(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    @DexIgnore
    public static String c(CharSequence charSequence, int i) {
        char c = 0;
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = c + (charAt3 << 6) + (charAt2 << '\f') + (charAt << 18);
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }

    @DexIgnore
    public static void e(Om4 om4, CharSequence charSequence) {
        boolean z = true;
        try {
            int length = charSequence.length();
            if (length != 0) {
                if (length == 1) {
                    om4.p();
                    int a2 = om4.g().a();
                    int a3 = om4.a();
                    if (om4.f() == 0 && a2 - a3 <= 2) {
                        om4.o(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String c = c(charSequence, 0);
                    if (!(!om4.i()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        om4.q(om4.a() + i);
                        if (om4.g().a() - om4.a() >= 3) {
                            om4.q(om4.a() + c.length());
                            z = false;
                        }
                    }
                    if (z) {
                        om4.k();
                        om4.f -= i;
                    } else {
                        om4.s(c);
                    }
                    om4.o(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            om4.o(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Nm4
    public void a(Om4 om4) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!om4.i()) {
                break;
            }
            b(om4.c(), sb);
            om4.f++;
            if (sb.length() >= 4) {
                om4.s(c(sb, 0));
                sb.delete(0, 4);
                if (Qm4.n(om4.d(), om4.f, d()) != d()) {
                    om4.o(0);
                    break;
                }
            }
        }
        sb.append((char) 31);
        e(om4, sb);
    }

    @DexIgnore
    public int d() {
        return 4;
    }
}
