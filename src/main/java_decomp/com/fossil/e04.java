package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E04 implements Yz3 {
    @DexIgnore
    public /* final */ float a;

    @DexIgnore
    public E04(float f) {
        this.a = f;
    }

    @DexIgnore
    @Override // com.fossil.Yz3
    public float a(RectF rectF) {
        return this.a * rectF.height();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof E04)) {
            return false;
        }
        return this.a == ((E04) obj).a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
