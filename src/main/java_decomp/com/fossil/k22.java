package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface K22 extends Closeable {
    @DexIgnore
    Q22 Y(H02 h02, C02 c02);

    @DexIgnore
    long c0(H02 h02);

    @DexIgnore
    int cleanUp();

    @DexIgnore
    boolean f0(H02 h02);

    @DexIgnore
    void g(Iterable<Q22> iterable);

    @DexIgnore
    void h0(Iterable<Q22> iterable);

    @DexIgnore
    Iterable<Q22> q(H02 h02);

    @DexIgnore
    void s(H02 h02, long j);

    @DexIgnore
    Iterable<H02> w();
}
