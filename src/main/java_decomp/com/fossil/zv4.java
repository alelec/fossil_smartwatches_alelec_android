package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zv4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public G37<L35> g;
    @DexIgnore
    public BCNotificationViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public Dw4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Zv4.l;
        }

        @DexIgnore
        public final Zv4 b() {
            return new Zv4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 b;

        @DexIgnore
        public Bi(Zv4 zv4) {
            this.b = zv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ L35 a;
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 b;

        @DexIgnore
        public Ci(L35 l35, Zv4 zv4) {
            this.a = l35;
            this.b = zv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            Zv4.N6(this.b).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 a;

        @DexIgnore
        public Di(Zv4 zv4) {
            this.a = zv4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            L35 l35 = (L35) Zv4.K6(this.a).a();
            if (l35 != null) {
                SwipeRefreshLayout swipeRefreshLayout = l35.w;
                Wg6.b(swipeRefreshLayout, "swipe");
                Wg6.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 a;

        @DexIgnore
        public Ei(Zv4 zv4) {
            this.a = zv4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            L35 l35 = (L35) Zv4.K6(this.a).a();
            if (l35 != null) {
                Wg6.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = l35.q;
                    Wg6.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = l35.u;
                    Wg6.b(recyclerView, "rcvNotification");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = l35.q;
                Wg6.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = l35.u;
                Wg6.b(recyclerView2, "rcvNotification");
                recyclerView2.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<List<? extends Dt4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 a;

        @DexIgnore
        public Fi(Zv4 zv4) {
            this.a = zv4;
        }

        @DexIgnore
        public final void a(List<Dt4> list) {
            Dw4 dw4 = this.a.j;
            if (dw4 != null) {
                Wg6.b(list, "it");
                dw4.i(list);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Dt4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Zv4 a;

        @DexIgnore
        public Gi(Zv4 zv4) {
            this.a = zv4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            boolean booleanValue = lc6.getFirst().booleanValue();
            ServerError serverError = (ServerError) lc6.getSecond();
            L35 l35 = (L35) Zv4.K6(this.a).a();
            if (l35 != null) {
                FlexibleTextView flexibleTextView = l35.q;
                Wg6.b(flexibleTextView, "ftvEmpty");
                flexibleTextView.setVisibility(8);
                if (booleanValue) {
                    FlexibleTextView flexibleTextView2 = l35.r;
                    Wg6.b(flexibleTextView2, "ftvError");
                    flexibleTextView2.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView3 = l35.r;
                Wg6.b(flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(8);
                FlexibleTextView flexibleTextView4 = l35.r;
                Wg6.b(flexibleTextView4, "ftvError");
                String c = Um5.c(flexibleTextView4.getContext(), 2131886231);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    /*
    static {
        String simpleName = Zv4.class.getSimpleName();
        Wg6.b(simpleName, "BCNotificationFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Zv4 zv4) {
        G37<L35> g37 = zv4.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCNotificationViewModel N6(Zv4 zv4) {
        BCNotificationViewModel bCNotificationViewModel = zv4.h;
        if (bCNotificationViewModel != null) {
            return bCNotificationViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        G37<L35> g37 = this.g;
        if (g37 != null) {
            L35 a2 = g37.a();
            if (a2 != null) {
                this.j = new Dw4();
                a2.s.setOnClickListener(new Bi(this));
                a2.w.setOnRefreshListener(new Ci(a2, this));
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.hasFixedSize();
                recyclerView.setAdapter(this.j);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        BCNotificationViewModel bCNotificationViewModel = this.h;
        if (bCNotificationViewModel != null) {
            bCNotificationViewModel.k().h(getViewLifecycleOwner(), new Di(this));
            BCNotificationViewModel bCNotificationViewModel2 = this.h;
            if (bCNotificationViewModel2 != null) {
                bCNotificationViewModel2.i().h(getViewLifecycleOwner(), new Ei(this));
                BCNotificationViewModel bCNotificationViewModel3 = this.h;
                if (bCNotificationViewModel3 != null) {
                    bCNotificationViewModel3.l().h(getViewLifecycleOwner(), new Fi(this));
                    BCNotificationViewModel bCNotificationViewModel4 = this.h;
                    if (bCNotificationViewModel4 != null) {
                        bCNotificationViewModel4.j().h(getViewLifecycleOwner(), new Gi(this));
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().W0().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCNotificationViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.h = (BCNotificationViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        L35 l35 = (L35) Aq0.f(layoutInflater, 2131558515, viewGroup, false, A6());
        this.g = new G37<>(this, l35);
        Wg6.b(l35, "binding");
        return l35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        O6();
        P6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
