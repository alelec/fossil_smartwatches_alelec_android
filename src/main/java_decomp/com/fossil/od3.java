package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Od3 extends As2 implements Zb3 {
    @DexIgnore
    public Od3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void B(Vd3 vd3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, vd3);
        i(96, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void D2(Rc3 rc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rc3);
        i(30, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Fc3 F1() throws RemoteException {
        Fc3 hd3;
        Parcel e = e(25, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            hd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            hd3 = queryLocalInterface instanceof Fc3 ? (Fc3) queryLocalInterface : new Hd3(readStrongBinder);
        }
        e.recycle();
        return hd3;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void H(Jc3 jc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, jc3);
        i(28, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void H2(Nc3 nc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, nc3);
        i(29, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void I0(Dd3 dd3, Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, dd3);
        Es2.c(d, rg2);
        i(38, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void J(LatLngBounds latLngBounds) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLngBounds);
        i(95, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void J0(Hc3 hc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, hc3);
        i(32, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final boolean N1() throws RemoteException {
        Parcel e = e(40, d());
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Ls2 N2(Le3 le3) throws RemoteException {
        Parcel d = d();
        Es2.d(d, le3);
        Parcel e = e(11, d);
        Ls2 e2 = Ms2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Is2 Q(Ee3 ee3) throws RemoteException {
        Parcel d = d();
        Es2.d(d, ee3);
        Parcel e = e(35, d);
        Is2 e2 = Js2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void R(Yc3 yc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, yc3);
        i(87, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final boolean R0() throws RemoteException {
        Parcel e = e(17, d());
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void R2(Tc3 tc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, tc3);
        i(31, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void S2(Wc3 wc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, wc3);
        i(85, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void U0(Rg2 rg2, Kd3 kd3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        Es2.c(d, kd3);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void X(Lc3 lc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, lc3);
        i(42, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void Y0(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(93, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void a0(int i, int i2, int i3, int i4) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        d.writeInt(i2);
        d.writeInt(i3);
        d.writeInt(i4);
        i(39, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void a2(Td3 td3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, td3);
        i(97, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Cc3 b2() throws RemoteException {
        Cc3 cd3;
        Parcel e = e(26, d());
        IBinder readStrongBinder = e.readStrongBinder();
        if (readStrongBinder == null) {
            cd3 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IProjectionDelegate");
            cd3 = queryLocalInterface instanceof Cc3 ? (Cc3) queryLocalInterface : new Cd3(readStrongBinder);
        }
        e.recycle();
        return cd3;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void clear() throws RemoteException {
        i(14, d());
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void e1(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        i(92, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void h2(Xd3 xd3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, xd3);
        i(89, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void j2(Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void o0(Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final CameraPosition p0() throws RemoteException {
        Parcel e = e(1, d());
        CameraPosition cameraPosition = (CameraPosition) Es2.b(e, CameraPosition.CREATOR);
        e.recycle();
        return cameraPosition;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void setBuildingsEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(41, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final boolean setIndoorEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        Parcel e = e(20, d);
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void setMapType(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(16, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void setMyLocationEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(22, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void setTrafficEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        Es2.a(d, z);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void u1() throws RemoteException {
        i(94, d());
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final boolean v0(Je3 je3) throws RemoteException {
        Parcel d = d();
        Es2.d(d, je3);
        Parcel e = e(91, d);
        boolean e2 = Es2.e(e);
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Os2 w1(Oe3 oe3) throws RemoteException {
        Parcel d = d();
        Es2.d(d, oe3);
        Parcel e = e(10, d);
        Os2 e2 = Ps2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final void x1(Rd3 rd3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rd3);
        i(99, d);
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final Rs2 x2(Qe3 qe3) throws RemoteException {
        Parcel d = d();
        Es2.d(d, qe3);
        Parcel e = e(9, d);
        Rs2 e2 = Bs2.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final float z() throws RemoteException {
        Parcel e = e(3, d());
        float readFloat = e.readFloat();
        e.recycle();
        return readFloat;
    }

    @DexIgnore
    @Override // com.fossil.Zb3
    public final float z2() throws RemoteException {
        Parcel e = e(2, d());
        float readFloat = e.readFloat();
        e.recycle();
        return readFloat;
    }
}
