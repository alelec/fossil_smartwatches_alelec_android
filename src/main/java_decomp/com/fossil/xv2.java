package com.fossil;

import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xv2<T> {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static volatile Gw2 h;
    @DexIgnore
    public static Lw2 i; // = new Lw2(Zv2.a);
    @DexIgnore
    public static /* final */ AtomicInteger j; // = new AtomicInteger();
    @DexIgnore
    public /* final */ Hw2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;
    @DexIgnore
    public /* final */ boolean f;

    /*
    static {
        new AtomicReference();
    }
    */

    @DexIgnore
    public Xv2(Hw2 hw2, String str, T t, boolean z) {
        this.d = -1;
        if (hw2.a != null) {
            this.a = hw2;
            this.b = str;
            this.c = t;
            this.f = z;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    public /* synthetic */ Xv2(Hw2 hw2, String str, Object obj, boolean z, Dw2 dw2) {
        this(hw2, str, obj, z);
    }

    @DexIgnore
    public static void g() {
        j.incrementAndGet();
    }

    @DexIgnore
    public static void h(Context context) {
        synchronized (g) {
            Gw2 gw2 = h;
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (gw2 == null || gw2.a() != context) {
                Jv2.d();
                Jw2.b();
                Sv2.b();
                h = new Kv2(context, Ww2.a(new Aw2(context)));
                j.incrementAndGet();
            }
        }
    }

    @DexIgnore
    public static Xv2<Double> i(Hw2 hw2, String str, double d2, boolean z) {
        return new Fw2(hw2, str, Double.valueOf(d2), true);
    }

    @DexIgnore
    public static Xv2<Long> j(Hw2 hw2, String str, long j2, boolean z) {
        return new Dw2(hw2, str, Long.valueOf(j2), true);
    }

    @DexIgnore
    public static Xv2<String> k(Hw2 hw2, String str, String str2, boolean z) {
        return new Ew2(hw2, str, str2, true);
    }

    @DexIgnore
    public static Xv2<Boolean> l(Hw2 hw2, String str, boolean z, boolean z2) {
        return new Cw2(hw2, str, Boolean.valueOf(z), true);
    }

    @DexIgnore
    public static final /* synthetic */ boolean p() {
        return true;
    }

    @DexIgnore
    public abstract T e(Object obj);

    @DexIgnore
    public final String f(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final String n() {
        return f(this.a.c);
    }

    @DexIgnore
    public final T o() {
        T t;
        T t2;
        Object zza;
        if (!this.f) {
            Sw2.h(i.a(this.b), "Attempt to access PhenotypeFlag not via codegen. All new PhenotypeFlags must be accessed through codegen APIs. If you believe you are seeing this error by mistake, you can add your flag to the exemption list located at //java/com/google/android/libraries/phenotype/client/lockdown/flags.textproto. Send the addition CL to ph-reviews@. See go/phenotype-android-codegen for information about generated code. See go/ph-lockdown for more information about this error.");
        }
        int i2 = j.get();
        if (this.d < i2) {
            synchronized (this) {
                if (this.d < i2) {
                    Gw2 gw2 = h;
                    Sw2.h(gw2 != null, "Must call PhenotypeFlag.init() first");
                    String str = (String) Sv2.a(gw2.a()).zza("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
                    if (!(str != null && Gv2.c.matcher(str).matches())) {
                        Nv2 a2 = this.a.a != null ? Vv2.b(gw2.a(), this.a.a) ? Jv2.a(gw2.a().getContentResolver(), this.a.a) : null : Jw2.a(gw2.a(), null);
                        if (!(a2 == null || (zza = a2.zza(n())) == null)) {
                            t = e(zza);
                        }
                        t = null;
                    } else {
                        if (Log.isLoggable("PhenotypeFlag", 3)) {
                            String valueOf = String.valueOf(n());
                            Log.d("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
                        }
                        t = null;
                    }
                    if (t == null) {
                        Object zza2 = Sv2.a(gw2.a()).zza(f(this.a.b));
                        t = zza2 != null ? e(zza2) : null;
                        if (t == null) {
                            t = this.c;
                        }
                    }
                    Tw2<Tv2> zza3 = gw2.b().zza();
                    if (zza3.zza()) {
                        String a3 = zza3.zzb().a(this.a.a, null, this.a.c, this.b);
                        t2 = a3 == null ? this.c : e(a3);
                    } else {
                        t2 = t;
                    }
                    this.e = t2;
                    this.d = i2;
                }
            }
        }
        return this.e;
    }
}
