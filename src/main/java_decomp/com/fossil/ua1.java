package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.Xb1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ua1 {
    @DexIgnore
    public /* final */ Cf1 a;
    @DexIgnore
    public /* final */ Si1 b;
    @DexIgnore
    public /* final */ Wi1 c;
    @DexIgnore
    public /* final */ Xi1 d;
    @DexIgnore
    public /* final */ Yb1 e;
    @DexIgnore
    public /* final */ Uh1 f;
    @DexIgnore
    public /* final */ Ti1 g;
    @DexIgnore
    public /* final */ Vi1 h; // = new Vi1();
    @DexIgnore
    public /* final */ Ui1 i; // = new Ui1();
    @DexIgnore
    public /* final */ Mn0<List<Throwable>> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends RuntimeException {
        @DexIgnore
        public Ai(String str) {
            super(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ai {
        @DexIgnore
        public Bi() {
            super("Failed to find image header parser.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Ai {
        @DexIgnore
        public Ci(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }

        @DexIgnore
        public Ci(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        @DexIgnore
        public <M> Ci(M m, List<Af1<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + ((Object) m));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends Ai {
        @DexIgnore
        public Di(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Ai {
        @DexIgnore
        public Ei(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    @DexIgnore
    public Ua1() {
        Mn0<List<Throwable>> e2 = Kk1.e();
        this.j = e2;
        this.a = new Cf1(e2);
        this.b = new Si1();
        this.c = new Wi1();
        this.d = new Xi1();
        this.e = new Yb1();
        this.f = new Uh1();
        this.g = new Ti1();
        r(Arrays.asList("Gif", "Bitmap", "BitmapDrawable"));
    }

    @DexIgnore
    public <Data> Ua1 a(Class<Data> cls, Jb1<Data> jb1) {
        this.b.a(cls, jb1);
        return this;
    }

    @DexIgnore
    public <TResource> Ua1 b(Class<TResource> cls, Rb1<TResource> rb1) {
        this.d.a(cls, rb1);
        return this;
    }

    @DexIgnore
    public <Data, TResource> Ua1 c(Class<Data> cls, Class<TResource> cls2, Qb1<Data, TResource> qb1) {
        e("legacy_append", cls, cls2, qb1);
        return this;
    }

    @DexIgnore
    public <Model, Data> Ua1 d(Class<Model> cls, Class<Data> cls2, Bf1<Model, Data> bf1) {
        this.a.a(cls, cls2, bf1);
        return this;
    }

    @DexIgnore
    public <Data, TResource> Ua1 e(String str, Class<Data> cls, Class<TResource> cls2, Qb1<Data, TResource> qb1) {
        this.c.a(str, qb1, cls, cls2);
        return this;
    }

    @DexIgnore
    public final <Data, TResource, Transcode> List<Vc1<Data, TResource, Transcode>> f(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.c.d(cls, cls2)) {
            for (Class cls5 : this.f.b(cls4, cls3)) {
                arrayList.add(new Vc1(cls, cls4, cls5, this.c.b(cls, cls4), this.f.a(cls4, cls5), this.j));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public List<ImageHeaderParser> g() {
        List<ImageHeaderParser> b2 = this.g.b();
        if (!b2.isEmpty()) {
            return b2;
        }
        throw new Bi();
    }

    @DexIgnore
    public <Data, TResource, Transcode> Gd1<Data, TResource, Transcode> h(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        Gd1<Data, TResource, Transcode> gd1 = null;
        Gd1<Data, TResource, Transcode> a2 = this.i.a(cls, cls2, cls3);
        if (this.i.c(a2)) {
            return null;
        }
        if (a2 != null) {
            return a2;
        }
        List<Vc1<Data, TResource, Transcode>> f2 = f(cls, cls2, cls3);
        if (!f2.isEmpty()) {
            gd1 = new Gd1<>(cls, cls2, cls3, f2, this.j);
        }
        this.i.d(cls, cls2, cls3, gd1);
        return gd1;
    }

    @DexIgnore
    public <Model> List<Af1<Model, ?>> i(Model model) {
        return this.a.d(model);
    }

    @DexIgnore
    public <Model, TResource, Transcode> List<Class<?>> j(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2, cls3);
        if (a2 != null) {
            return a2;
        }
        ArrayList arrayList = new ArrayList();
        for (Class<?> cls4 : this.a.c(cls)) {
            for (Class cls5 : this.c.d(cls4, cls2)) {
                if (!this.f.b(cls5, cls3).isEmpty() && !arrayList.contains(cls5)) {
                    arrayList.add(cls5);
                }
            }
        }
        this.h.b(cls, cls2, cls3, Collections.unmodifiableList(arrayList));
        return arrayList;
    }

    @DexIgnore
    public <X> Rb1<X> k(Id1<X> id1) throws Di {
        Rb1<X> b2 = this.d.b(id1.d());
        if (b2 != null) {
            return b2;
        }
        throw new Di(id1.d());
    }

    @DexIgnore
    public <X> Xb1<X> l(X x) {
        return this.e.a(x);
    }

    @DexIgnore
    public <X> Jb1<X> m(X x) throws Ei {
        Jb1<X> b2 = this.b.b(x.getClass());
        if (b2 != null) {
            return b2;
        }
        throw new Ei(x.getClass());
    }

    @DexIgnore
    public boolean n(Id1<?> id1) {
        return this.d.b(id1.d()) != null;
    }

    @DexIgnore
    public Ua1 o(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    @DexIgnore
    public Ua1 p(Xb1.Ai<?> ai) {
        this.e.b(ai);
        return this;
    }

    @DexIgnore
    public <TResource, Transcode> Ua1 q(Class<TResource> cls, Class<Transcode> cls2, Th1<TResource, Transcode> th1) {
        this.f.c(cls, cls2, th1);
        return this;
    }

    @DexIgnore
    public final Ua1 r(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.e(arrayList);
        return this;
    }
}
