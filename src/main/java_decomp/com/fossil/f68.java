package com.fossil;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F68 {
    @DexIgnore
    public static /* final */ Random a; // = new Random();

    @DexIgnore
    public static String a(int i, int i2, int i3, boolean z, boolean z2) {
        return b(i, i2, i3, z, z2, null, a);
    }

    @DexIgnore
    public static String b(int i, int i2, int i3, boolean z, boolean z2, char[] cArr, Random random) {
        if (i == 0) {
            return "";
        }
        if (i < 0) {
            throw new IllegalArgumentException("Requested random string length " + i + " is less than 0.");
        } else if (cArr == null || cArr.length != 0) {
            if (i2 == 0 && i3 == 0) {
                if (cArr != null) {
                    i3 = cArr.length;
                } else if (z || z2) {
                    i3 = 123;
                    i2 = 32;
                } else {
                    i3 = Integer.MAX_VALUE;
                }
            } else if (i3 <= i2) {
                throw new IllegalArgumentException("Parameter end (" + i3 + ") must be greater than start (" + i2 + ")");
            }
            char[] cArr2 = new char[i];
            int i4 = i3 - i2;
            while (true) {
                int i5 = i - 1;
                if (i == 0) {
                    return new String(cArr2);
                }
                char nextInt = cArr == null ? (char) (random.nextInt(i4) + i2) : cArr[random.nextInt(i4) + i2];
                if ((z && Character.isLetter(nextInt)) || ((z2 && Character.isDigit(nextInt)) || (!z && !z2))) {
                    if (nextInt < '\udc00' || nextInt > '\udfff') {
                        if (nextInt < '\ud800' || nextInt > '\udb7f') {
                            if (nextInt < '\udb80' || nextInt > '\udbff') {
                                cArr2[i5] = nextInt;
                                i = i5;
                            }
                        } else if (i5 != 0) {
                            cArr2[i5] = (char) ((char) (random.nextInt(128) + 56320));
                            i5--;
                            cArr2[i5] = nextInt;
                            i = i5;
                        }
                    } else if (i5 != 0) {
                        cArr2[i5] = nextInt;
                        i5--;
                        cArr2[i5] = (char) ((char) (random.nextInt(128) + 55296));
                        i = i5;
                    }
                }
                i5++;
                i = i5;
            }
        } else {
            throw new IllegalArgumentException("The chars array must not be empty");
        }
    }

    @DexIgnore
    public static String c(int i, String str) {
        return str == null ? b(i, 0, 0, false, false, null, a) : e(i, str.toCharArray());
    }

    @DexIgnore
    public static String d(int i, boolean z, boolean z2) {
        return a(i, 0, 0, z, z2);
    }

    @DexIgnore
    public static String e(int i, char... cArr) {
        return cArr == null ? b(i, 0, 0, false, false, null, a) : b(i, 0, cArr.length, false, false, cArr, a);
    }

    @DexIgnore
    public static String f(int i) {
        return d(i, true, true);
    }
}
