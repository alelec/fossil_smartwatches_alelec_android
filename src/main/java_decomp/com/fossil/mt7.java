package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Mt7 implements Gt7 {
    IGNORE_CASE(2, 0, 2, null),
    MULTILINE(8, 0, 2, null),
    LITERAL(16, 0, 2, null),
    UNIX_LINES(1, 0, 2, null),
    COMMENTS(4, 0, 2, null),
    DOT_MATCHES_ALL(32, 0, 2, null),
    CANON_EQ(128, 0, 2, null);
    
    @DexIgnore
    public /* final */ int mask;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public Mt7(int i, int i2) {
        this.value = i;
        this.mask = i2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Mt7(int i, int i2, int i3, Qg6 qg6) {
        this(i, (i3 & 2) != 0 ? i : i2);
    }

    @DexIgnore
    @Override // com.fossil.Gt7
    public int getMask() {
        return this.mask;
    }

    @DexIgnore
    @Override // com.fossil.Gt7
    public int getValue() {
        return this.value;
    }
}
