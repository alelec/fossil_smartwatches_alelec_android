package com.fossil;

import com.facebook.internal.ServerProtocol;
import java.io.PrintStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R78 {
    @DexIgnore
    public static final void a(String str) {
        PrintStream printStream = System.err;
        printStream.println("SLF4J: " + str);
    }

    @DexIgnore
    public static final void b(String str, Throwable th) {
        System.err.println(str);
        System.err.println("Reported exception:");
        th.printStackTrace();
    }

    @DexIgnore
    public static boolean c(String str) {
        String d = d(str);
        if (d == null) {
            return false;
        }
        return d.equalsIgnoreCase(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
    }

    @DexIgnore
    public static String d(String str) {
        if (str != null) {
            try {
                return System.getProperty(str);
            } catch (SecurityException e) {
                return null;
            }
        } else {
            throw new IllegalArgumentException("null input");
        }
    }
}
