package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import com.facebook.places.PlaceManager;
import com.fossil.common.cipher.TextEncryption;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W00 {
    @DexIgnore
    public static /* final */ Hashtable<String, E60> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ W00 c;

    /*
    static {
        String string;
        W00 w00 = new W00();
        c = w00;
        SharedPreferences b2 = G80.b(Ld0.b);
        if (b2 != null && (string = b2.getString("com.fossil.blesdk.device.cache", null)) != null) {
            Wg6.b(string, "getSharedPreferences(Sha\u2026ENCE_KEY, null) ?: return");
            try {
                JSONArray jSONArray = new JSONArray(TextEncryption.j.n(string));
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string2 = jSONObject.getString(PlaceManager.PARAM_MAC_ADDRESS);
                    String string3 = jSONObject.getString(Constants.SERIAL_NUMBER);
                    Wg6.b(string3, "serialNumber");
                    Wg6.b(string2, "macAddress");
                    w00.d(string3, string2);
                }
            } catch (Exception e) {
                D90.i.i(e);
            }
        }
    }
    */

    @DexIgnore
    public final E60 a(BluetoothDevice bluetoothDevice, String str) {
        E60 e60;
        M80.c.a("DeviceImplementation.Factory", "getDevice: MAC=%s, serial number=%s.", bluetoothDevice.getAddress(), str);
        synchronized (a) {
            e60 = a.get(bluetoothDevice.getAddress());
            if (e60 == null) {
                e60 = new E60(bluetoothDevice, str, null);
                a.put(e60.u.getMacAddress(), e60);
                if (Zk1.u.d(str)) {
                    c.d(str, e60.u.getMacAddress());
                }
            } else if (!Zk1.u.d(e60.u.getSerialNumber()) && Zk1.u.d(str)) {
                Zk1 a2 = Zk1.a(e60.u, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                e60.u = a2;
                a.put(a2.getMacAddress(), e60);
                c.d(str, e60.u.getMacAddress());
            }
        }
        return e60;
    }

    @DexIgnore
    public final String b(String str) {
        String str2;
        synchronized (b) {
            str2 = b.get(str);
        }
        return str2;
    }

    @DexIgnore
    public final void c() {
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(Constants.SERIAL_NUMBER, entry.getKey());
                    jSONObject.put(PlaceManager.PARAM_MAC_ADDRESS, entry.getValue());
                } catch (Exception e) {
                    D90.i.i(e);
                }
                jSONArray.put(jSONObject);
            }
            Cd6 cd6 = Cd6.a;
        }
        String jSONArray2 = jSONArray.toString();
        Wg6.b(jSONArray2, "array.toString()");
        String o = TextEncryption.j.o(jSONArray2);
        SharedPreferences b2 = G80.b(Ld0.b);
        if (b2 != null) {
            b2.edit().putString("com.fossil.blesdk.device.cache", o).apply();
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        if (Zk1.u.d(str) && BluetoothAdapter.checkBluetoothAddress(str2)) {
            synchronized (b) {
                b.put(str, str2);
                Cd6 cd6 = Cd6.a;
            }
        }
    }

    @DexIgnore
    public final String e(String str) {
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                String key = entry.getKey();
                if (Vt7.j(entry.getValue(), str, true)) {
                    return key;
                }
            }
            return null;
        }
    }
}
