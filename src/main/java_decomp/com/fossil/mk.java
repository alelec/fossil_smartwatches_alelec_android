package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mk extends Lp {
    @DexIgnore
    public R4 C;

    @DexIgnore
    public Mk(K5 k5, I60 i60, String str) {
        super(k5, i60, Yp.g0, str, false, 16);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Cs(this.w), new Bj(this), Nj.b, null, new Ak(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.o1, Ey1.a(this.w.H()));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E = super.E();
        Jd0 jd0 = Jd0.D0;
        R4 r4 = this.C;
        return G80.k(E, jd0, r4 != null ? Ey1.a(r4) : null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        R4 r4 = this.C;
        return r4 != null ? r4 : R4.b;
    }
}
