package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.manager.ThemeManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rn5 implements MembersInjector<ThemeManager> {
    @DexIgnore
    public static void a(ThemeManager themeManager, ThemeRepository themeRepository) {
        themeManager.i = themeRepository;
    }
}
