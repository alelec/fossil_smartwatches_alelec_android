package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pi4 {
    @DexIgnore
    public static volatile Pi4 b;
    @DexIgnore
    public /* final */ Set<Ri4> a; // = new HashSet();

    @DexIgnore
    public static Pi4 a() {
        Pi4 pi4 = b;
        if (pi4 == null) {
            synchronized (Pi4.class) {
                try {
                    pi4 = b;
                    if (pi4 == null) {
                        pi4 = new Pi4();
                        b = pi4;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return pi4;
    }

    @DexIgnore
    public Set<Ri4> b() {
        Set<Ri4> unmodifiableSet;
        synchronized (this.a) {
            unmodifiableSet = Collections.unmodifiableSet(this.a);
        }
        return unmodifiableSet;
    }
}
