package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.StateSet;
import android.util.Xml;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Il0 {
    @DexIgnore
    public static ColorStateList a(Resources resources, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        AttributeSet asAttributeSet = Xml.asAttributeSet(xmlPullParser);
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return b(resources, xmlPullParser, asAttributeSet, theme);
        }
        throw new XmlPullParserException("No start tag found");
    }

    @DexIgnore
    public static ColorStateList b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        String name = xmlPullParser.getName();
        if (name.equals("selector")) {
            return d(resources, xmlPullParser, attributeSet, theme);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid color state list tag " + name);
    }

    @DexIgnore
    public static ColorStateList c(Resources resources, int i, Resources.Theme theme) {
        try {
            return a(resources, resources.getXml(i), theme);
        } catch (Exception e) {
            Log.e("CSLCompat", "Failed to inflate ColorStateList.", e);
            return null;
        }
    }

    @DexIgnore
    public static ColorStateList d(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth;
        int[][] iArr;
        int i;
        int depth2 = xmlPullParser.getDepth() + 1;
        int[][] iArr2 = new int[20][];
        int[] iArr3 = new int[20];
        int i2 = 0;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 1 || ((depth = xmlPullParser.getDepth()) < depth2 && next == 3)) {
                int[] iArr4 = new int[i2];
                int[][] iArr5 = new int[i2][];
                System.arraycopy(iArr3, 0, iArr4, 0, i2);
                System.arraycopy(iArr2, 0, iArr5, 0, i2);
            } else {
                if (next != 2 || depth > depth2) {
                    iArr = iArr2;
                } else if (!xmlPullParser.getName().equals("item")) {
                    iArr = iArr2;
                } else {
                    TypedArray f = f(resources, theme, attributeSet, Qk0.ColorStateListItem);
                    int color = f.getColor(Qk0.ColorStateListItem_android_color, -65281);
                    float f2 = 1.0f;
                    if (f.hasValue(Qk0.ColorStateListItem_android_alpha)) {
                        f2 = f.getFloat(Qk0.ColorStateListItem_android_alpha, 1.0f);
                    } else if (f.hasValue(Qk0.ColorStateListItem_alpha)) {
                        f2 = f.getFloat(Qk0.ColorStateListItem_alpha, 1.0f);
                    }
                    f.recycle();
                    int attributeCount = attributeSet.getAttributeCount();
                    int[] iArr6 = new int[attributeCount];
                    int i3 = 0;
                    int i4 = 0;
                    while (i4 < attributeCount) {
                        int attributeNameResource = attributeSet.getAttributeNameResource(i4);
                        if (attributeNameResource == 16843173 || attributeNameResource == 16843551 || attributeNameResource == Nk0.alpha) {
                            i = i3;
                        } else {
                            if (!attributeSet.getAttributeBooleanValue(i4, false)) {
                                attributeNameResource = -attributeNameResource;
                            }
                            iArr6[i3] = attributeNameResource;
                            i = i3 + 1;
                        }
                        i4++;
                        i3 = i;
                    }
                    int[] trimStateSet = StateSet.trimStateSet(iArr6, i3);
                    iArr3 = Ml0.a(iArr3, i2, e(color, f2));
                    iArr = (int[][]) Ml0.b(iArr2, i2, trimStateSet);
                    i2++;
                }
                iArr2 = iArr;
            }
        }
        int[] iArr42 = new int[i2];
        int[][] iArr52 = new int[i2][];
        System.arraycopy(iArr3, 0, iArr42, 0, i2);
        System.arraycopy(iArr2, 0, iArr52, 0, i2);
        return new ColorStateList(iArr52, iArr42);
    }

    @DexIgnore
    public static int e(int i, float f) {
        return (16777215 & i) | (Math.round(((float) Color.alpha(i)) * f) << 24);
    }

    @DexIgnore
    public static TypedArray f(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        return theme == null ? resources.obtainAttributes(attributeSet, iArr) : theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }
}
