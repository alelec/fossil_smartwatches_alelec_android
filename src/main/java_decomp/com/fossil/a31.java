package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface A31 {
    @DexIgnore
    void a(Z21 z21);

    @DexIgnore
    List<String> b(String str);

    @DexIgnore
    boolean c(String str);

    @DexIgnore
    boolean d(String str);
}
