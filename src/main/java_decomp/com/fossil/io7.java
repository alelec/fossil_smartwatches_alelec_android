package com.fossil;

import com.mapped.Af6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Io7 extends Zn7 {
    @DexIgnore
    public Io7(Xe6<Object> xe6) {
        super(xe6);
        if (xe6 != null) {
            if (!(xe6.getContext() == Un7.INSTANCE)) {
                throw new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString());
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Zn7, com.mapped.Xe6
    public Af6 getContext() {
        return Un7.INSTANCE;
    }
}
