package com.fossil;

import com.portfolio.platform.uirenew.welcome.WelcomePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M27 implements MembersInjector<WelcomePresenter> {
    @DexIgnore
    public static void a(WelcomePresenter welcomePresenter) {
        welcomePresenter.r();
    }
}
