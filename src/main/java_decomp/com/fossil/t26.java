package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T26 implements Factory<InactivityNudgeTimePresenter> {
    @DexIgnore
    public static InactivityNudgeTimePresenter a(Q26 q26, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new InactivityNudgeTimePresenter(q26, remindersSettingsDatabase);
    }
}
