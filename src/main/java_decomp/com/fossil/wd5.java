package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wd5 extends Vd5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362773, 1);
        x.put(2131362562, 2);
        x.put(2131362563, 3);
        x.put(2131362561, 4);
    }
    */

    @DexIgnore
    public Wd5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 5, w, x));
    }

    @DexIgnore
    public Wd5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[3], (AppCompatImageView) objArr[1]);
        this.v = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.u = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.v != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.v = 1;
        }
        w();
    }
}
