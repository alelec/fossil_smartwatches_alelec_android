package com.fossil;

import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qm6 implements MembersInjector<SleepDetailActivity> {
    @DexIgnore
    public static void a(SleepDetailActivity sleepDetailActivity, SleepDetailPresenter sleepDetailPresenter) {
        sleepDetailActivity.A = sleepDetailPresenter;
    }
}
