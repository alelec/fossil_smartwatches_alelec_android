package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class P64 implements D74 {
    @DexIgnore
    public static /* final */ D74 a; // = new P64();

    @DexIgnore
    @Override // com.fossil.D74
    public final Object a(B74 b74) {
        return N64.d((J64) b74.get(J64.class), (Context) b74.get(Context.class), (Ge4) b74.get(Ge4.class));
    }
}
