package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mc5 extends Lc5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362056, 1);
        D.put(2131363025, 2);
        D.put(2131363403, 3);
        D.put(2131363404, 4);
        D.put(2131363112, 5);
        D.put(2131363321, 6);
        D.put(2131363323, 7);
        D.put(2131362172, 8);
        D.put(2131362666, 9);
        D.put(2131362285, 10);
    }
    */

    @DexIgnore
    public Mc5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 11, C, D));
    }

    @DexIgnore
    public Mc5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[8], (FlexibleButton) objArr[10], (RTLImageView) objArr[9], (ConstraintLayout) objArr[0], (RecyclerView) objArr[2], (View) objArr[5], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4]);
        this.B = -1;
        this.u.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.B = 1;
        }
        w();
    }
}
