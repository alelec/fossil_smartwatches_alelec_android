package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rf4 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = 0;

    @DexIgnore
    public Rf4(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static String c(J64 j64) {
        String d2 = j64.j().d();
        if (d2 != null) {
            return d2;
        }
        String c2 = j64.j().c();
        if (!c2.startsWith("1:")) {
            return c2;
        }
        String[] split = c2.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public String a() {
        String str;
        synchronized (this) {
            if (this.b == null) {
                h();
            }
            str = this.b;
        }
        return str;
    }

    @DexIgnore
    public String b() {
        String str;
        synchronized (this) {
            if (this.c == null) {
                h();
            }
            str = this.c;
        }
        return str;
    }

    @DexIgnore
    public int d() {
        int i;
        PackageInfo f;
        synchronized (this) {
            if (this.d == 0 && (f = f("com.google.android.gms")) != null) {
                this.d = f.versionCode;
            }
            i = this.d;
        }
        return i;
    }

    @DexIgnore
    public int e() {
        synchronized (this) {
            if (this.e != 0) {
                return this.e;
            }
            PackageManager packageManager = this.a.getPackageManager();
            if (packageManager.checkPermission("com.google.android.c2dm.permission.SEND", "com.google.android.gms") == -1) {
                Log.e("FirebaseInstanceId", "Google Play services missing or without correct permission.");
                return 0;
            }
            if (!Mf2.j()) {
                Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
                intent.setPackage("com.google.android.gms");
                List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    this.e = 1;
                    return 1;
                }
            }
            Intent intent2 = new Intent("com.google.iid.TOKEN_REQUEST");
            intent2.setPackage("com.google.android.gms");
            List<ResolveInfo> queryBroadcastReceivers = packageManager.queryBroadcastReceivers(intent2, 0);
            if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() <= 0) {
                Log.w("FirebaseInstanceId", "Failed to resolve IID implementation package, falling back");
                if (Mf2.j()) {
                    this.e = 2;
                } else {
                    this.e = 1;
                }
                return this.e;
            }
            this.e = 2;
            return 2;
        }
    }

    @DexIgnore
    public final PackageInfo f(String str) {
        try {
            return this.a.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            String valueOf = String.valueOf(e2);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("Failed to find package ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public boolean g() {
        return e() != 0;
    }

    @DexIgnore
    public final void h() {
        synchronized (this) {
            PackageInfo f = f(this.a.getPackageName());
            if (f != null) {
                this.b = Integer.toString(f.versionCode);
                this.c = f.versionName;
            }
        }
    }
}
