package com.fossil;

import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B41 implements Runnable {
    @DexIgnore
    public S11 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public WorkerParameters.a d;

    @DexIgnore
    public B41(S11 s11, String str, WorkerParameters.a aVar) {
        this.b = s11;
        this.c = str;
        this.d = aVar;
    }

    @DexIgnore
    public void run() {
        this.b.n().j(this.c, this.d);
    }
}
