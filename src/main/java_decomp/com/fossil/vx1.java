package com.fossil;

import com.fossil.Sx1;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vx1<K, V> {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final byte[] a(byte[] bArr) {
            Wg6.c(bArr, "fileData");
            return bArr.length > 16 ? Dm7.k(bArr, 12, bArr.length - 4) : new byte[0];
        }
    }

    @DexIgnore
    public final byte[] a(short s, Ry1 ry1, K k) throws Sx1 {
        Wg6.c(ry1, "version");
        Wg6.c(k, "entries");
        Qx1<K> d = d(ry1);
        if (d != null) {
            return d.a(s, k);
        }
        Sx1.Ai ai = Sx1.Ai.UNSUPPORTED_VERSION;
        throw new Sx1(ai, "Not support version " + ry1 + '.', null, 4, null);
    }

    @DexIgnore
    public abstract Qx1<K>[] b();

    @DexIgnore
    public abstract Rx1<V>[] c();

    @DexIgnore
    public final Qx1<K> d(Ry1 ry1) {
        Ry1 c;
        Qx1<K>[] b = b();
        int length = b.length;
        Qx1<K> qx1 = null;
        int i = 0;
        while (i < length) {
            Qx1<K> qx12 = b[i];
            if (Wg6.a(qx12.c(), ry1)) {
                return qx12;
            }
            if (qx12.c().getMajor() == ry1.getMajor()) {
                if (qx12.c().getMinor() >= ((qx1 == null || (c = qx1.c()) == null) ? 0 : c.getMinor())) {
                    i++;
                    qx1 = qx12;
                }
            }
            qx12 = qx1;
            i++;
            qx1 = qx12;
        }
        return qx1;
    }

    @DexIgnore
    public final Rx1<V> e(Ry1 ry1) {
        Ry1 a2;
        Rx1<V>[] c = c();
        int length = c.length;
        Rx1<V> rx1 = null;
        int i = 0;
        while (i < length) {
            Rx1<V> rx12 = c[i];
            if (Wg6.a(rx12.a(), ry1)) {
                return rx12;
            }
            if (rx12.a().getMajor() == ry1.getMajor()) {
                if (rx12.a().getMinor() >= ((rx1 == null || (a2 = rx1.a()) == null) ? 0 : a2.getMinor())) {
                    i++;
                    rx1 = rx12;
                }
            }
            rx12 = rx1;
            i++;
            rx1 = rx12;
        }
        return rx1;
    }

    @DexIgnore
    public final V f(byte[] bArr) throws Sx1 {
        Wg6.c(bArr, "data");
        try {
            Ry1 ry1 = new Ry1(bArr[2], bArr[3]);
            Rx1<V> e = e(ry1);
            if (e != null) {
                return e.b(bArr);
            }
            Sx1.Ai ai = Sx1.Ai.UNSUPPORTED_VERSION;
            throw new Sx1(ai, "Not support version " + ry1 + '.', null, 4, null);
        } catch (Exception e2) {
            throw new Sx1(Sx1.Ai.INVALID_FILE_DATA, "Invalid file data.", e2);
        }
    }
}
