package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fn3 implements Callable<List<Hr3>> {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 b;

    @DexIgnore
    public Fn3(Qm3 qm3, Or3 or3) {
        this.b = qm3;
        this.a = or3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<Hr3> call() throws Exception {
        this.b.b.d0();
        return this.b.b.U().G(this.a.b);
    }
}
