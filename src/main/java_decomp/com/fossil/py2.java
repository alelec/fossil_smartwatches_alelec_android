package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Py2<K, V> extends Wx2<K, V> {
    @DexIgnore
    public static /* final */ Wx2<Object, Object> zza; // = new Py2(null, new Object[0], 0);
    @DexIgnore
    public /* final */ transient Object e;
    @DexIgnore
    public /* final */ transient Object[] f;
    @DexIgnore
    public /* final */ transient int g;

    @DexIgnore
    public Py2(Object obj, Object[] objArr, int i) {
        this.e = obj;
        this.f = objArr;
        this.g = i;
    }

    @DexIgnore
    public static IllegalArgumentException a(Object obj, Object obj2, Object[] objArr, int i) {
        String valueOf = String.valueOf(obj);
        String valueOf2 = String.valueOf(obj2);
        String valueOf3 = String.valueOf(objArr[i]);
        String valueOf4 = String.valueOf(objArr[i ^ 1]);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39 + String.valueOf(valueOf2).length() + String.valueOf(valueOf3).length() + String.valueOf(valueOf4).length());
        sb.append("Multiple entries with same key: ");
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        sb.append(" and ");
        sb.append(valueOf3);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf4);
        return new IllegalArgumentException(sb.toString());
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:53:0x0031 */
    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: int */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: short[] */
    /* JADX DEBUG: Multi-variable search result rejected for r7v1, resolved type: short */
    /* JADX DEBUG: Multi-variable search result rejected for r0v15, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v3, types: [int[]] */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        r1[r0] = (byte) ((byte) r4);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009d, code lost:
        r0[r1] = (short) ((short) r4);
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00d3, code lost:
        r0[r1] = r4;
        r2 = r2 + 1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <K, V> com.fossil.Py2<K, V> zza(int r11, java.lang.Object[] r12) {
        /*
        // Method dump skipped, instructions count: 236
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Py2.zza(int, java.lang.Object[]):com.fossil.Py2");
    }

    @DexIgnore
    @Override // com.fossil.Wx2, java.util.Map
    @NullableDecl
    public final V get(@NullableDecl Object obj) {
        Object obj2 = this.e;
        Object[] objArr = this.f;
        int i = this.g;
        if (obj == null) {
            return null;
        }
        if (i == 1) {
            if (objArr[0].equals(obj)) {
                return (V) objArr[1];
            }
            return null;
        } else if (obj2 == null) {
            return null;
        } else {
            if (obj2 instanceof byte[]) {
                byte[] bArr = (byte[]) obj2;
                int length = bArr.length;
                int a2 = Qx2.a(obj.hashCode());
                while (true) {
                    int i2 = a2 & (length - 1);
                    int i3 = bArr[i2] & 255;
                    if (i3 == 255) {
                        return null;
                    }
                    if (objArr[i3].equals(obj)) {
                        return (V) objArr[i3 ^ 1];
                    }
                    a2 = i2 + 1;
                }
            } else if (obj2 instanceof short[]) {
                short[] sArr = (short[]) obj2;
                int length2 = sArr.length;
                int a3 = Qx2.a(obj.hashCode());
                while (true) {
                    int i4 = a3 & (length2 - 1);
                    int i5 = sArr[i4] & 65535;
                    if (i5 == 65535) {
                        return null;
                    }
                    if (objArr[i5].equals(obj)) {
                        return (V) objArr[i5 ^ 1];
                    }
                    a3 = i4 + 1;
                }
            } else {
                int[] iArr = (int[]) obj2;
                int length3 = iArr.length;
                int a4 = Qx2.a(obj.hashCode());
                while (true) {
                    int i6 = a4 & (length3 - 1);
                    int i7 = iArr[i6];
                    if (i7 == -1) {
                        return null;
                    }
                    if (objArr[i7].equals(obj)) {
                        return (V) objArr[i7 ^ 1];
                    }
                    a4 = i6 + 1;
                }
            }
        }
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Wx2
    public final Ay2<Map.Entry<K, V>> zza() {
        return new Sy2(this, this.f, 0, this.g);
    }

    @DexIgnore
    @Override // com.fossil.Wx2
    public final Ay2<K> zzb() {
        return new Uy2(this, new Ty2(this.f, 0, this.g));
    }

    @DexIgnore
    @Override // com.fossil.Wx2
    public final Tx2<V> zzc() {
        return new Ty2(this.f, 1, this.g);
    }
}
