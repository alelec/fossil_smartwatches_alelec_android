package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Om4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public Sm4 b;
    @DexIgnore
    public Ll4 c;
    @DexIgnore
    public Ll4 d;
    @DexIgnore
    public /* final */ StringBuilder e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Rm4 h;
    @DexIgnore
    public int i;

    @DexIgnore
    public Om4(String str) {
        byte[] bytes = str.getBytes(Charset.forName("ISO-8859-1"));
        StringBuilder sb = new StringBuilder(bytes.length);
        int length = bytes.length;
        for (int i2 = 0; i2 < length; i2++) {
            char c2 = (char) (bytes[i2] & 255);
            if (c2 != '?' || str.charAt(i2) == '?') {
                sb.append(c2);
            } else {
                throw new IllegalArgumentException("Message contains characters outside ISO-8859-1 encoding.");
            }
        }
        this.a = sb.toString();
        this.b = Sm4.FORCE_NONE;
        this.e = new StringBuilder(str.length());
        this.g = -1;
    }

    @DexIgnore
    public int a() {
        return this.e.length();
    }

    @DexIgnore
    public StringBuilder b() {
        return this.e;
    }

    @DexIgnore
    public char c() {
        return this.a.charAt(this.f);
    }

    @DexIgnore
    public String d() {
        return this.a;
    }

    @DexIgnore
    public int e() {
        return this.g;
    }

    @DexIgnore
    public int f() {
        return h() - this.f;
    }

    @DexIgnore
    public Rm4 g() {
        return this.h;
    }

    @DexIgnore
    public final int h() {
        return this.a.length() - this.i;
    }

    @DexIgnore
    public boolean i() {
        return this.f < h();
    }

    @DexIgnore
    public void j() {
        this.g = -1;
    }

    @DexIgnore
    public void k() {
        this.h = null;
    }

    @DexIgnore
    public void l(Ll4 ll4, Ll4 ll42) {
        this.c = ll4;
        this.d = ll42;
    }

    @DexIgnore
    public void m(int i2) {
        this.i = i2;
    }

    @DexIgnore
    public void n(Sm4 sm4) {
        this.b = sm4;
    }

    @DexIgnore
    public void o(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void p() {
        q(a());
    }

    @DexIgnore
    public void q(int i2) {
        Rm4 rm4 = this.h;
        if (rm4 == null || i2 > rm4.a()) {
            this.h = Rm4.l(i2, this.b, this.c, this.d, true);
        }
    }

    @DexIgnore
    public void r(char c2) {
        this.e.append(c2);
    }

    @DexIgnore
    public void s(String str) {
        this.e.append(str);
    }
}
