package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w85 extends v85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        B.put(2131362546, 2);
        B.put(2131362303, 3);
        B.put(2131362683, 4);
        B.put(2131362035, 5);
        B.put(2131362362, 6);
        B.put(2131363159, 7);
        B.put(2131363046, 8);
    }
    */

    @DexIgnore
    public w85(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public w85(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[5], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (RTLImageView) objArr[4], (RTLImageView) objArr[1], (ConstraintLayout) objArr[0], (RecyclerView) objArr[8], (FlexibleSwitchCompat) objArr[7]);
        this.z = -1;
        this.w.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
