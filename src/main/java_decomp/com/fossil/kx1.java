package com.fossil;

import com.facebook.internal.Utility;
import com.mapped.Wg6;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kx1 {
    @DexIgnore
    public static /* final */ Kx1 a; // = new Kx1();

    @DexIgnore
    public final String a(byte[] bArr) {
        Wg6.c(bArr, "data");
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            Wg6.b(instance, "MessageDigest.getInstance(\"MD5\")");
            instance.update(bArr, 0, bArr.length);
            String bigInteger = new BigInteger(1, instance.digest()).toString(16);
            Hr7 hr7 = Hr7.a;
            String format = String.format("%32s", Arrays.copyOf(new Object[]{bigInteger}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            return Vt7.p(format, ' ', '0', false, 4, null);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }
}
