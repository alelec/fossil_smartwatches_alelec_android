package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mm1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ Cu1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Mm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Mm1 a(byte[] bArr) throws IllegalArgumentException {
            Cu1 cu1;
            if (bArr.length >= 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                short s = order.getShort(0);
                byte b = order.get(2);
                if (bArr.length > 3) {
                    cu1 = Cu1.d.a(order.get(3));
                    if (cu1 == null) {
                        StringBuilder e = E.e("Invalid state: ");
                        e.append((int) order.get(3));
                        throw new IllegalArgumentException(e.toString());
                    }
                } else {
                    cu1 = Cu1.NORMAL;
                }
                return new Mm1(s, b, cu1);
            }
            throw new IllegalArgumentException(E.c(E.e("Invalid data size: "), bArr.length, ", require as ", "least: 3"));
        }

        @DexIgnore
        public Mm1 b(Parcel parcel) {
            return new Mm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Mm1 createFromParcel(Parcel parcel) {
            return new Mm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Mm1[] newArray(int i) {
            return new Mm1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Mm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = (short) ((short) parcel.readInt());
        this.d = parcel.readByte();
        Cu1 a2 = Cu1.d.a(parcel.readByte());
        if (a2 != null) {
            this.e = a2;
            d();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Mm1(short s, byte b, Cu1 cu1) throws IllegalArgumentException {
        super(Zm1.BATTERY);
        this.c = (short) s;
        this.d = (byte) b;
        this.e = cu1;
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.c).put(this.d).put(this.e.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(MIN_\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.c5, Short.valueOf(this.c));
            G80.k(jSONObject, Jd0.b5, Byte.valueOf(this.d));
            G80.k(jSONObject, Jd0.n1, this.e);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        short s = this.c;
        if (s >= 0 && 4400 >= s) {
            byte b = this.d;
            if (b < 0 || 100 < b) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(E.c(E.e("percentage("), this.d, ") is out of range ", "[0, 100]."));
            }
            return;
        }
        throw new IllegalArgumentException(E.c(E.e("voltage("), this.c, ") is out of range ", "[0, 4400]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Mm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Mm1 mm1 = (Mm1) obj;
            if (this.c != mm1.c) {
                return false;
            }
            if (this.d != mm1.d) {
                return false;
            }
            return this.e == mm1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public final byte getPercentage() {
        return this.d;
    }

    @DexIgnore
    public final Cu1 getState() {
        return this.e;
    }

    @DexIgnore
    public final short getVoltage() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        short s = this.c;
        return (((s * 31) + Byte.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.c));
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
    }
}
