package com.fossil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T13 extends Rz2<String> implements W13, RandomAccess {
    @DexIgnore
    public static /* final */ T13 d;
    @DexIgnore
    public /* final */ List<Object> c;

    /*
    static {
        T13 t13 = new T13();
        d = t13;
        t13.zzb();
    }
    */

    @DexIgnore
    public T13() {
        this(10);
    }

    @DexIgnore
    public T13(int i) {
        this(new ArrayList(i));
    }

    @DexIgnore
    public T13(ArrayList<Object> arrayList) {
        this.c = arrayList;
    }

    @DexIgnore
    public static String b(Object obj) {
        return obj instanceof String ? (String) obj : obj instanceof Xz2 ? ((Xz2) obj).zzb() : H13.i((byte[]) obj);
    }

    @DexIgnore
    @Override // com.fossil.W13
    public final void V(Xz2 xz2) {
        a();
        this.c.add(xz2);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.c.add(i, (String) obj);
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList, com.fossil.Rz2
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof W13) {
            collection = ((W13) collection).zzd();
        }
        boolean addAll = this.c.addAll(i, collection);
        ((AbstractList) this).modCount++;
        return addAll;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.Rz2
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    @Override // com.fossil.Rz2
    public final void clear() {
        a();
        this.c.clear();
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        Object obj = this.c.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Xz2) {
            Xz2 xz2 = (Xz2) obj;
            String zzb = xz2.zzb();
            if (xz2.zzc()) {
                this.c.set(i, zzb);
            }
            return zzb;
        }
        byte[] bArr = (byte[]) obj;
        String i2 = H13.i(bArr);
        if (H13.h(bArr)) {
            this.c.set(i, i2);
        }
        return i2;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.c.remove(i);
        ((AbstractList) this).modCount++;
        return b(remove);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return b(this.c.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.c.size();
    }

    @DexIgnore
    @Override // com.fossil.M13
    public final /* synthetic */ M13 zza(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.c);
            return new T13(arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    @Override // com.fossil.W13
    public final Object zzb(int i) {
        return this.c.get(i);
    }

    @DexIgnore
    @Override // com.fossil.W13
    public final List<?> zzd() {
        return Collections.unmodifiableList(this.c);
    }

    @DexIgnore
    @Override // com.fossil.W13
    public final W13 zze() {
        return zza() ? new Y33(this) : this;
    }
}
