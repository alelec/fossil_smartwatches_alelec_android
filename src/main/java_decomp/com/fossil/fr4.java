package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fr4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public ArrayList<Style> a; // = new ArrayList<>();
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public /* final */ ArrayList<Theme> c;
    @DexIgnore
    public Ai d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void D2(String str);

        @DexIgnore
        void L4(String str);

        @DexIgnore
        void g1(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ AppCompatCheckBox a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ Fr4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai = this.b.d.d;
                if (ai != null) {
                    ai.D2(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
                Fr4 fr4 = this.b.d;
                fr4.b = ((Theme) fr4.c.get(this.b.getAdapterPosition())).getId();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai = this.b.d.d;
                if (ai != null) {
                    ai.g1(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Cii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai = this.b.d.d;
                if (ai != null) {
                    ai.L4(((Theme) this.b.d.c.get(this.b.getAdapterPosition())).getId());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Fr4 fr4, View view) {
            super(view);
            Wg6.c(view, "view");
            this.d = fr4;
            this.a = (AppCompatCheckBox) view.findViewById(2131361998);
            this.b = (ImageView) view.findViewById(2131362695);
            this.c = (ImageView) view.findViewById(2131362690);
            this.a.setOnClickListener(new Aii(this));
            this.b.setOnClickListener(new Bii(this));
            this.c.setOnClickListener(new Cii(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Theme theme) {
            Wg6.c(theme, "theme");
            if (Wg6.a(theme.getType(), "fixed")) {
                ImageView imageView = this.b;
                Wg6.b(imageView, "ivEdit");
                imageView.setVisibility(4);
                ImageView imageView2 = this.c;
                Wg6.b(imageView2, "ivDelete");
                imageView2.setVisibility(4);
            }
            AppCompatCheckBox appCompatCheckBox = this.a;
            Wg6.b(appCompatCheckBox, "cbCurrentTheme");
            appCompatCheckBox.setChecked(Wg6.a(this.d.b, theme.getId()));
            AppCompatCheckBox appCompatCheckBox2 = this.a;
            Wg6.b(appCompatCheckBox2, "cbCurrentTheme");
            appCompatCheckBox2.setText(theme.getName());
            String e = ThemeManager.l.a().e("primaryText", this.d.a);
            String e2 = ThemeManager.l.a().e("nonBrandSurface", this.d.a);
            Typeface g = ThemeManager.l.a().g("headline2", this.d.a);
            if (e != null) {
                this.a.setTextColor(Color.parseColor(e));
                Ep0.c(this.a, new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(e), Color.parseColor(e2), Color.parseColor(e2)}));
                this.c.setColorFilter(Color.parseColor(e));
                this.b.setColorFilter(Color.parseColor(e));
            }
            if (g != null) {
                AppCompatCheckBox appCompatCheckBox3 = this.a;
                Wg6.b(appCompatCheckBox3, "cbCurrentTheme");
                appCompatCheckBox3.setTypeface(g);
            }
        }
    }

    @DexIgnore
    public Fr4(ArrayList<Theme> arrayList, Ai ai) {
        Wg6.c(arrayList, "mData");
        this.c = arrayList;
        this.d = ai;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void l(List<Style> list, String str) {
        Wg6.c(list, "allStyles");
        Wg6.c(str, "selectedThemeId");
        this.a.clear();
        this.a.addAll(list);
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public void m(Bi bi, int i) {
        Wg6.c(bi, "holder");
        if (getItemCount() > i && i != -1) {
            Theme theme = this.c.get(i);
            Wg6.b(theme, "mData[position]");
            bi.a(theme);
        }
    }

    @DexIgnore
    public Bi n(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558721, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026tem_theme, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void o(ArrayList<Theme> arrayList) {
        Wg6.c(arrayList, "ids");
        this.c.clear();
        this.c.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        m(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return n(viewGroup, i);
    }
}
