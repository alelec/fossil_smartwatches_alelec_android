package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ac5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleEditText t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport v;
    @DexIgnore
    public /* final */ FlexibleTextView w;

    @DexIgnore
    public Ac5(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, ConstraintLayout constraintLayout2, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = constraintLayout;
        this.t = flexibleEditText;
        this.u = constraintLayout2;
        this.v = recyclerViewEmptySupport;
        this.w = flexibleTextView2;
    }
}
