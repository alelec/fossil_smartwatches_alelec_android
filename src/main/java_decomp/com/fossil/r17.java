package com.fossil;

import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R17 implements MembersInjector<CalibrationPresenter> {
    @DexIgnore
    public static void a(CalibrationPresenter calibrationPresenter) {
        calibrationPresenter.L();
    }
}
