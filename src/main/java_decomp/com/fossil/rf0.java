package com.fossil;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import com.fossil.Cg0;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rf0 extends ActionMode implements Cg0.Ai {
    @DexIgnore
    public Context d;
    @DexIgnore
    public ActionBarContextView e;
    @DexIgnore
    public ActionMode.Callback f;
    @DexIgnore
    public WeakReference<View> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Cg0 i;

    @DexIgnore
    public Rf0(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.d = context;
        this.e = actionBarContextView;
        this.f = callback;
        Cg0 cg0 = new Cg0(actionBarContextView.getContext());
        cg0.W(1);
        this.i = cg0;
        cg0.V(this);
    }

    @DexIgnore
    @Override // com.fossil.Cg0.Ai
    public boolean a(Cg0 cg0, MenuItem menuItem) {
        return this.f.d(this, menuItem);
    }

    @DexIgnore
    @Override // com.fossil.Cg0.Ai
    public void b(Cg0 cg0) {
        k();
        this.e.l();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void c() {
        if (!this.h) {
            this.h = true;
            this.e.sendAccessibilityEvent(32);
            this.f.a(this);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public View d() {
        WeakReference<View> weakReference = this.g;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public Menu e() {
        return this.i;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public MenuInflater f() {
        return new Tf0(this.e.getContext());
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public CharSequence g() {
        return this.e.getSubtitle();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public CharSequence i() {
        return this.e.getTitle();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void k() {
        this.f.c(this, this.i);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public boolean l() {
        return this.e.j();
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void m(View view) {
        this.e.setCustomView(view);
        this.g = view != null ? new WeakReference<>(view) : null;
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void n(int i2) {
        o(this.d.getString(i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void o(CharSequence charSequence) {
        this.e.setSubtitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void q(int i2) {
        r(this.d.getString(i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void r(CharSequence charSequence) {
        this.e.setTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.view.ActionMode
    public void s(boolean z) {
        super.s(z);
        this.e.setTitleOptional(z);
    }
}
