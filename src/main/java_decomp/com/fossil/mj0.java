package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Mj0<T> {
    @DexIgnore
    boolean a(T t);

    @DexIgnore
    T b();

    @DexIgnore
    void c(T[] tArr, int i);
}
