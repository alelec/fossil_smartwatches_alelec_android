package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya6 implements MembersInjector<WatchAppSearchActivity> {
    @DexIgnore
    public static void a(WatchAppSearchActivity watchAppSearchActivity, WatchAppSearchPresenter watchAppSearchPresenter) {
        watchAppSearchActivity.A = watchAppSearchPresenter;
    }
}
