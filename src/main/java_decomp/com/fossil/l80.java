package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.mapped.Rc6;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L80 {
    @DexIgnore
    public static /* final */ K80 a; // = new K80();
    @DexIgnore
    public static BluetoothProfile b;
    @DexIgnore
    public static /* final */ BroadcastReceiver c; // = new J80();
    @DexIgnore
    public static /* final */ L80 d; // = new L80();

    @DexIgnore
    public final int a(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return X6.g.b;
        }
        try {
            int b2 = d.b(bluetoothProfile, bluetoothDevice);
            M80.c.a("HIDProfile", "getPriority: device=%s, priority=%d.", bluetoothDevice.getAddress(), Integer.valueOf(b2));
            if (b2 == 0) {
                return X6.i.b;
            }
            Method method = bluetoothProfile.getClass().getMethod("connect", BluetoothDevice.class);
            if (method == null) {
                M80.c.a("HIDProfile", "connect: localMethod NOT found.", new Object[0]);
                return X6.h.b;
            }
            Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                M80.c.a("HIDProfile", "connectDevice: success=%b.", Boolean.valueOf(booleanValue));
                return booleanValue ? X6.c.b : X6.j.b;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            M80 m80 = M80.c;
            StringBuilder e2 = E.e("connectDevice got exception: ");
            e2.append(Fy1.a(e));
            m80.a("HIDProfile", e2.toString(), new Object[0]);
            D90.i.i(e);
            return X6.h.b;
        } catch (Exception e3) {
            M80 m802 = M80.c;
            StringBuilder e4 = E.e("connectDevice got exception: ");
            e4.append(Fy1.a(e3));
            m802.a("HIDProfile", e4.toString(), new Object[0]);
            e3.printStackTrace();
            D90.i.i(e3);
            return X6.j.b;
        }
    }

    @DexIgnore
    public final int b(BluetoothProfile bluetoothProfile, BluetoothDevice bluetoothDevice) throws Exception {
        Method method = bluetoothProfile.getClass().getMethod("getPriority", BluetoothDevice.class);
        if (method == null) {
            M80.c.a("HIDProfile", "getPriority: localMethod NOT found!", new Object[0]);
            return -1;
        }
        Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
        if (invoke != null) {
            int intValue = ((Integer) invoke).intValue();
            M80.c.a("HIDProfile", "BluetoothHidHost.getPriority: device=%s, priority=%d.", bluetoothDevice.getAddress(), Integer.valueOf(intValue));
            return intValue;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Int");
    }

    @DexIgnore
    public final String c(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "DISCONNECTING" : "CONNECTED" : "CONNECTING" : "DISCONNECTED";
    }

    @DexIgnore
    public final List<BluetoothDevice> d() {
        List<BluetoothDevice> connectedDevices;
        BluetoothProfile bluetoothProfile = b;
        return (bluetoothProfile == null || (connectedDevices = bluetoothProfile.getConnectedDevices()) == null) ? new ArrayList() : connectedDevices;
    }

    @DexIgnore
    public final void e(BluetoothDevice bluetoothDevice, int i, int i2) {
        M80.c.a("HIDProfile", "onHIDDeviceStateChanged: device=%s, %s(%d) to %s(%d).", bluetoothDevice.getAddress(), c(i), Integer.valueOf(i), c(i2), Integer.valueOf(i2));
        D90 d90 = D90.i;
        String a2 = Ey1.a(I80.b);
        V80 v80 = V80.i;
        String address = bluetoothDevice.getAddress();
        if (address == null) {
            address = "";
        }
        JSONObject jSONObject = new JSONObject();
        Jd0 jd0 = Jd0.k0;
        String address2 = bluetoothDevice.getAddress();
        d90.d(new A90(a2, v80, address, "", "", true, null, null, null, G80.k(G80.k(G80.k(jSONObject, jd0, address2 != null ? address2 : ""), Jd0.C0, c(i)), Jd0.B0, c(i2)), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", i);
        intent.putExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", i2);
        Context a3 = Id0.i.a();
        if (a3 != null) {
            Ct0.b(a3).d(intent);
        }
    }

    @DexIgnore
    public final void f(Context context) {
        int profileConnectionState = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(4);
        boolean profileProxy = BluetoothAdapter.getDefaultAdapter().getProfileProxy(context, a, 4);
        M80.c.a("HIDProfile", "setUp: profileConnectionState=%d, getProxy=%b.", Integer.valueOf(profileConnectionState), Boolean.valueOf(profileProxy));
        context.registerReceiver(c, new IntentFilter("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"));
    }

    @DexIgnore
    public final int g(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile == null) {
            return X6.g.b;
        }
        try {
            Method method = bluetoothProfile.getClass().getMethod("disconnect", BluetoothDevice.class);
            if (method == null) {
                M80.c.a("HIDProfile", "disconnect: localMethod NOT found!", new Object[0]);
                return X6.j.b;
            }
            Object invoke = method.invoke(bluetoothProfile, bluetoothDevice);
            if (invoke != null) {
                boolean booleanValue = ((Boolean) invoke).booleanValue();
                M80.c.a("HIDProfile", "disconnectDevice: success=%b.", Boolean.valueOf(booleanValue));
                return booleanValue ? X6.c.b : X6.j.b;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Boolean");
        } catch (NoSuchMethodException e) {
            M80 m80 = M80.c;
            StringBuilder e2 = E.e("disconnectDevice got exception: ");
            e2.append(Fy1.a(e));
            m80.a("HIDProfile", e2.toString(), new Object[0]);
            D90.i.i(e);
            return X6.h.b;
        } catch (Exception e3) {
            M80 m802 = M80.c;
            StringBuilder e4 = E.e("disconnectDevice got exception: ");
            e4.append(Fy1.a(e3));
            m802.a("HIDProfile", e4.toString(), new Object[0]);
            D90.i.i(e3);
            return X6.j.b;
        }
    }

    @DexIgnore
    public final int h(BluetoothDevice bluetoothDevice) {
        BluetoothProfile bluetoothProfile = b;
        if (bluetoothProfile != null) {
            return bluetoothProfile.getConnectionState(bluetoothDevice);
        }
        return 0;
    }
}
