package com.fossil;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B68 {
    /*
    static {
        char c = File.separatorChar;
        D68 d68 = new D68(4);
        PrintWriter printWriter = new PrintWriter(d68);
        printWriter.println();
        d68.toString();
        printWriter.close();
    }
    */

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public static void b(InputStream inputStream) {
        a(inputStream);
    }

    @DexIgnore
    public static void c(OutputStream outputStream) {
        a(outputStream);
    }

    @DexIgnore
    public static int d(InputStream inputStream, OutputStream outputStream) throws IOException {
        long e = e(inputStream, outputStream);
        if (e > 2147483647L) {
            return -1;
        }
        return (int) e;
    }

    @DexIgnore
    public static long e(InputStream inputStream, OutputStream outputStream) throws IOException {
        return f(inputStream, outputStream, new byte[4096]);
    }

    @DexIgnore
    public static long f(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    @DexIgnore
    public static byte[] g(InputStream inputStream) throws IOException {
        C68 c68 = new C68();
        d(inputStream, c68);
        return c68.b();
    }

    @DexIgnore
    public static byte[] h(InputStream inputStream, int i) throws IOException {
        int i2 = 0;
        if (i < 0) {
            throw new IllegalArgumentException("Size must be equal or greater than zero: " + i);
        } else if (i == 0) {
            return new byte[0];
        } else {
            byte[] bArr = new byte[i];
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            }
            if (i2 == i) {
                return bArr;
            }
            throw new IOException("Unexpected readed size. current: " + i2 + ", excepted: " + i);
        }
    }

    @DexIgnore
    public static byte[] i(InputStream inputStream, long j) throws IOException {
        if (j <= 2147483647L) {
            return h(inputStream, (int) j);
        }
        throw new IllegalArgumentException("Size cannot be greater than Integer max value: " + j);
    }
}
