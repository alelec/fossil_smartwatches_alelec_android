package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import com.mapped.Wg6;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fa8 {
    @DexIgnore
    public /* final */ Bi a;
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public /* final */ MethodChannel c; // = new MethodChannel(this.d.messenger(), "top.kikt/photo_manager/notify");
    @DexIgnore
    public /* final */ PluginRegistry.Registrar d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends ContentObserver {
        @DexIgnore
        public /* final */ /* synthetic */ Fa8 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Fa8 fa8, Handler handler) {
            super(handler);
            Wg6.c(handler, "handler");
            this.a = fa8;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.a.b(z, uri);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends ContentObserver {
        @DexIgnore
        public /* final */ /* synthetic */ Fa8 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Fa8 fa8, Handler handler) {
            super(handler);
            Wg6.c(handler, "handler");
            this.a = fa8;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.a.b(z, uri);
        }
    }

    @DexIgnore
    public Fa8(PluginRegistry.Registrar registrar, Handler handler) {
        Wg6.c(registrar, "registry");
        Wg6.c(handler, "handler");
        this.d = registrar;
        this.a = new Bi(this, handler);
        this.b = new Ai(this, handler);
    }

    @DexIgnore
    public final Context a() {
        Context context = this.d.context();
        Wg6.b(context, "registry.context()");
        return context.getApplicationContext();
    }

    @DexIgnore
    public final void b(boolean z, Uri uri) {
        this.c.invokeMethod("change", Zm7.j(Hl7.a("android-self", Boolean.valueOf(z)), Hl7.a("android-uri", String.valueOf(uri))));
    }

    @DexIgnore
    public final void c(boolean z) {
        this.c.invokeMethod("setAndroidQExperimental", Ym7.c(Hl7.a("open", Boolean.valueOf(z))));
    }

    @DexIgnore
    public final void d() {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Uri uri2 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Context a2 = a();
        Wg6.b(a2, "context");
        a2.getContentResolver().registerContentObserver(uri, false, this.b);
        Context a3 = a();
        Wg6.b(a3, "context");
        a3.getContentResolver().registerContentObserver(uri2, false, this.a);
    }

    @DexIgnore
    public final void e() {
        Context a2 = a();
        Wg6.b(a2, "context");
        a2.getContentResolver().unregisterContentObserver(this.b);
        Context a3 = a();
        Wg6.b(a3, "context");
        a3.getContentResolver().unregisterContentObserver(this.a);
    }
}
