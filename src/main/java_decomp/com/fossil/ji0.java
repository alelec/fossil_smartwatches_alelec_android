package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.Sd0;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ji0 extends Service {
    @DexIgnore
    public /* final */ Map<IBinder, IBinder.DeathRecipient> b; // = new Zi0();
    @DexIgnore
    public Sd0.Ai c; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Sd0.Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements IBinder.DeathRecipient {
            @DexIgnore
            public /* final */ /* synthetic */ Li0 a;

            @DexIgnore
            public Aii(Li0 li0) {
                this.a = li0;
            }

            @DexIgnore
            public void binderDied() {
                Ji0.this.a(this.a);
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public int B1(Rd0 rd0, String str, Bundle bundle) {
            return Ji0.this.e(new Li0(rd0), str, bundle);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean D(Rd0 rd0, Uri uri, Bundle bundle, List<Bundle> list) {
            return Ji0.this.c(new Li0(rd0), uri, bundle, list);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean F(Rd0 rd0, int i, Uri uri, Bundle bundle) {
            return Ji0.this.h(new Li0(rd0), i, uri, bundle);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean G1(Rd0 rd0) {
            Li0 li0 = new Li0(rd0);
            try {
                Aii aii = new Aii(li0);
                synchronized (Ji0.this.b) {
                    rd0.asBinder().linkToDeath(aii, 0);
                    Ji0.this.b.put(rd0.asBinder(), aii);
                }
                return Ji0.this.d(li0);
            } catch (RemoteException e) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean Q0(long j) {
            return Ji0.this.i(j);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean Z1(Rd0 rd0, Bundle bundle) {
            return Ji0.this.g(new Li0(rd0), bundle);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public boolean i2(Rd0 rd0, Uri uri) {
            return Ji0.this.f(new Li0(rd0), uri);
        }

        @DexIgnore
        @Override // com.fossil.Sd0
        public Bundle r0(String str, Bundle bundle) {
            return Ji0.this.b(str, bundle);
        }
    }

    @DexIgnore
    public boolean a(Li0 li0) {
        try {
            synchronized (this.b) {
                IBinder a2 = li0.a();
                a2.unlinkToDeath(this.b.get(a2), 0);
                this.b.remove(a2);
            }
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    @DexIgnore
    public abstract Bundle b(String str, Bundle bundle);

    @DexIgnore
    public abstract boolean c(Li0 li0, Uri uri, Bundle bundle, List<Bundle> list);

    @DexIgnore
    public abstract boolean d(Li0 li0);

    @DexIgnore
    public abstract int e(Li0 li0, String str, Bundle bundle);

    @DexIgnore
    public abstract boolean f(Li0 li0, Uri uri);

    @DexIgnore
    public abstract boolean g(Li0 li0, Bundle bundle);

    @DexIgnore
    public abstract boolean h(Li0 li0, int i, Uri uri, Bundle bundle);

    @DexIgnore
    public abstract boolean i(long j);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.c;
    }
}
