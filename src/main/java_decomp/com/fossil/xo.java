package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Xo extends Enum<Xo> {
    @DexIgnore
    public static /* final */ Xo b;
    @DexIgnore
    public static /* final */ /* synthetic */ Xo[] c;

    /*
    static {
        Xo xo = new Xo("NOT_AUTHENTICATED", 0);
        b = xo;
        c = new Xo[]{xo, new Xo("AUTHENTICATING", 1), new Xo("AUTHENTICATED", 2)};
    }
    */

    @DexIgnore
    public Xo(String str, int i) {
    }

    @DexIgnore
    public static Xo valueOf(String str) {
        return (Xo) Enum.valueOf(Xo.class, str);
    }

    @DexIgnore
    public static Xo[] values() {
        return (Xo[]) c.clone();
    }
}
