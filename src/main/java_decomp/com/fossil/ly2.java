package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ly2 extends Ny2<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ Ly2 zza; // = new Ly2();

    @DexIgnore
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        Comparable comparable = (Comparable) obj;
        Comparable comparable2 = (Comparable) obj2;
        Sw2.b(comparable);
        Sw2.b(comparable2);
        return comparable.compareTo(comparable2);
    }

    @DexIgnore
    public final String toString() {
        return "Ordering.natural()";
    }
}
