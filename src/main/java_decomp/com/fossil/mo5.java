package com.fossil;

import com.mapped.Pj4;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mo5 {
    @DexIgnore
    @Vu3("createdAt")
    public String a; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3("updatedAt")
    public String b; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Pj4
    public int c; // = 1;
    @DexIgnore
    @Vu3("id")
    public String d;
    @DexIgnore
    @Vu3("buttons")
    public List<Oo5> e;
    @DexIgnore
    @Vu3("checksumFace")
    public String f;
    @DexIgnore
    @Vu3("downloadFaceUrl")
    public String g;
    @DexIgnore
    @Vu3("previewFaceUrl")
    public String h;
    @DexIgnore
    @Vu3("isActive")
    public boolean i;
    @DexIgnore
    @Vu3("name")
    public String j;
    @DexIgnore
    @Vu3("serialNumber")
    public String k;
    @DexIgnore
    @Vu3("uid")
    public String l;
    @DexIgnore
    @Vu3("originalItemIdInStore")
    public String m;
    @DexIgnore
    @Vu3("_isNew")
    public boolean n;

    @DexIgnore
    public Mo5(String str, List<Oo5> list, String str2, String str3, String str4, boolean z, String str5, String str6, String str7, String str8, boolean z2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "checksumFace");
        Wg6.c(str3, "faceUrl");
        Wg6.c(str5, "name");
        Wg6.c(str6, "serialNumber");
        Wg6.c(str7, "uid");
        this.d = str;
        this.e = list;
        this.f = str2;
        this.g = str3;
        this.h = str4;
        this.i = z;
        this.j = str5;
        this.k = str6;
        this.l = str7;
        this.m = str8;
        this.n = z2;
    }

    @DexIgnore
    public final List<Oo5> a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.f;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.g;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Mo5) {
                Mo5 mo5 = (Mo5) obj;
                if (!Wg6.a(this.d, mo5.d) || !Wg6.a(this.e, mo5.e) || !Wg6.a(this.f, mo5.f) || !Wg6.a(this.g, mo5.g) || !Wg6.a(this.h, mo5.h) || this.i != mo5.i || !Wg6.a(this.j, mo5.j) || !Wg6.a(this.k, mo5.k) || !Wg6.a(this.l, mo5.l) || !Wg6.a(this.m, mo5.m) || this.n != mo5.n) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.j;
    }

    @DexIgnore
    public final String g() {
        return this.m;
    }

    @DexIgnore
    public final int h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 1;
        int i3 = 0;
        String str = this.d;
        int hashCode = str != null ? str.hashCode() : 0;
        List<Oo5> list = this.e;
        int hashCode2 = list != null ? list.hashCode() : 0;
        String str2 = this.f;
        int hashCode3 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.g;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.h;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        boolean z = this.i;
        if (z) {
            z = true;
        }
        String str5 = this.j;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.k;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.l;
        int hashCode8 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.m;
        if (str8 != null) {
            i3 = str8.hashCode();
        }
        boolean z2 = this.n;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i4) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i3) * 31) + i2;
    }

    @DexIgnore
    public final String i() {
        return this.h;
    }

    @DexIgnore
    public final String j() {
        return this.k;
    }

    @DexIgnore
    public final String k() {
        return this.l;
    }

    @DexIgnore
    public final String l() {
        return this.b;
    }

    @DexIgnore
    public final boolean m() {
        return this.i;
    }

    @DexIgnore
    public final boolean n() {
        return this.n;
    }

    @DexIgnore
    public final void o(boolean z) {
        this.i = z;
    }

    @DexIgnore
    public final void p(List<Oo5> list) {
        this.e = list;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, "<set-?>");
        this.a = str;
    }

    @DexIgnore
    public final void r(String str) {
        Wg6.c(str, "<set-?>");
        this.j = str;
    }

    @DexIgnore
    public final void s(int i2) {
        this.c = i2;
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "<set-?>");
        this.b = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.d + ", buttons=" + this.e + ", checksumFace=" + this.f + ", faceUrl=" + this.g + ", previewFaceUrl=" + this.h + ", isActive=" + this.i + ", name=" + this.j + ", serialNumber=" + this.k + ", uid=" + this.l + ", originalItemIdInStore=" + this.m + ", isNew=" + this.n + ")";
    }
}
