package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.nk5;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class of6 extends pv5 implements nf6 {
    @DexIgnore
    public g37<g25> g;
    @DexIgnore
    public mf6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements RecyclerViewCalendar.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ of6 f2676a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(of6 of6) {
            this.f2676a = of6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            pq7.c(calendar, "calendar");
            mf6 mf6 = this.f2676a.h;
            if (mf6 != null) {
                Date time = calendar.getTime();
                pq7.b(time, "calendar.time");
                mf6.o(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.a {
        @DexIgnore
        public /* final */ /* synthetic */ of6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(of6 of6) {
            this.b = of6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
        public void k0(int i, Calendar calendar) {
            pq7.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ActivityDetailActivity.a aVar = ActivityDetailActivity.C;
                Date time = calendar.getTime();
                pq7.b(time, "it.time");
                pq7.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "ActivityOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        g25 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        g37<g25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            nk5.a aVar = nk5.o;
            mf6 mf6 = this.h;
            if (aVar.w(mf6 != null ? mf6.n() : null)) {
                recyclerViewCalendar.J("dianaStepsTab");
            } else {
                recyclerViewCalendar.J("hybridStepsTab");
            }
        }
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(mf6 mf6) {
        pq7.c(mf6, "presenter");
        this.h = mf6;
    }

    @DexIgnore
    @Override // com.fossil.nf6
    public void e(TreeMap<Long, Float> treeMap) {
        g25 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        pq7.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        g37<g25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.nf6
    public void g(Date date, Date date2) {
        g25 a2;
        pq7.c(date, "selectDate");
        pq7.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        g37<g25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            pq7.b(instance, "selectCalendar");
            instance.setTime(date);
            pq7.b(instance2, "startCalendar");
            instance2.setTime(lk5.V(date2));
            pq7.b(instance3, "endCalendar");
            instance3.setTime(lk5.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        g25 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onCreateView");
        g25 g25 = (g25) aq0.f(layoutInflater, 2131558499, viewGroup, false, A6());
        RecyclerViewCalendar recyclerViewCalendar = g25.q;
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        g25.q.setOnCalendarMonthChanged(new a(this));
        g25.q.setOnCalendarItemClickListener(new b(this));
        this.g = new g37<>(this, g25);
        L6();
        g37<g25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onResume");
        L6();
        mf6 mf6 = this.h;
        if (mf6 != null) {
            mf6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onStop");
        mf6 mf6 = this.h;
        if (mf6 != null) {
            mf6.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
