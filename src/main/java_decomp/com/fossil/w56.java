package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W56 implements Factory<NotificationHybridContactPresenter> {
    @DexIgnore
    public static NotificationHybridContactPresenter a(P56 p56, int i, ArrayList<J06> arrayList, LoaderManager loaderManager, Uq4 uq4, Y56 y56) {
        return new NotificationHybridContactPresenter(p56, i, arrayList, loaderManager, uq4, y56);
    }
}
