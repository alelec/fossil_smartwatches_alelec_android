package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zc7 {
    @DexIgnore
    Object onError();  // void declaration

    @DexIgnore
    Object onSuccess();  // void declaration
}
