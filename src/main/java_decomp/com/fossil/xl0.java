package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;
import android.graphics.fonts.FontStyle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import com.fossil.Kl0;
import com.fossil.Zm0;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xl0 extends Yl0 {
    @DexIgnore
    @Override // com.fossil.Yl0
    public Typeface b(Context context, Kl0.Bi bi, Resources resources, int i) {
        FontFamily.Builder builder;
        int i2 = 1;
        Kl0.Ci[] a2 = bi.a();
        int length = a2.length;
        int i3 = 0;
        FontFamily.Builder builder2 = null;
        while (i3 < length) {
            Kl0.Ci ci = a2[i3];
            try {
                Font build = new Font.Builder(resources, ci.b()).setWeight(ci.e()).setSlant(ci.f() ? 1 : 0).setTtcIndex(ci.c()).setFontVariationSettings(ci.d()).build();
                if (builder2 == null) {
                    builder = new FontFamily.Builder(build);
                } else {
                    builder2.addFont(build);
                    builder = builder2;
                }
            } catch (IOException e) {
                builder = builder2;
            }
            i3++;
            builder2 = builder;
        }
        if (builder2 == null) {
            return null;
        }
        int i4 = (i & 1) != 0 ? 700 : MFNetworkReturnCode.BAD_REQUEST;
        if ((i & 2) == 0) {
            i2 = 0;
        }
        return new Typeface.CustomFallbackBuilder(builder2.build()).setStyle(new FontStyle(i4, i2)).build();
    }

    @DexIgnore
    @Override // com.fossil.Yl0
    public Typeface c(Context context, CancellationSignal cancellationSignal, Zm0.Fi[] fiArr, int i) {
        int i2 = 1;
        ContentResolver contentResolver = context.getContentResolver();
        FontFamily.Builder builder = null;
        for (Zm0.Fi fi : fiArr) {
            try {
                ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(fi.c(), "r", cancellationSignal);
                if (openFileDescriptor != null) {
                    try {
                        Font build = new Font.Builder(openFileDescriptor).setWeight(fi.d()).setSlant(fi.e() ? 1 : 0).setTtcIndex(fi.b()).build();
                        if (builder == null) {
                            builder = new FontFamily.Builder(build);
                        } else {
                            builder.addFont(build);
                        }
                        if (openFileDescriptor == null) {
                        }
                    } catch (Throwable th) {
                        th.addSuppressed(th);
                    }
                } else if (openFileDescriptor == null) {
                }
                openFileDescriptor.close();
            } catch (IOException e) {
            }
        }
        if (builder == null) {
            return null;
        }
        int i3 = (i & 1) != 0 ? 700 : MFNetworkReturnCode.BAD_REQUEST;
        if ((i & 2) == 0) {
            i2 = 0;
        }
        return new Typeface.CustomFallbackBuilder(builder.build()).setStyle(new FontStyle(i3, i2)).build();
        throw th;
    }

    @DexIgnore
    @Override // com.fossil.Yl0
    public Typeface d(Context context, InputStream inputStream) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }

    @DexIgnore
    @Override // com.fossil.Yl0
    public Typeface e(Context context, Resources resources, int i, String str, int i2) {
        try {
            Font build = new Font.Builder(resources, i).build();
            return new Typeface.CustomFallbackBuilder(new FontFamily.Builder(build).build()).setStyle(build.getStyle()).build();
        } catch (IOException e) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yl0
    public Zm0.Fi h(Zm0.Fi[] fiArr, int i) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }
}
