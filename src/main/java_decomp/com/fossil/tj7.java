package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tj7 {
    @DexIgnore
    public static Map<Character, String> a;
    @DexIgnore
    public static /* final */ String b; // = System.getProperty("line.separator");

    /*
    static {
        HashMap hashMap = new HashMap();
        a = hashMap;
        hashMap.put('\'', "\\'");
        a.put('\"', "\\\"");
        a.put('\\', "\\\\");
        a.put('/', "\\/");
        a.put('\b', "\\b");
        a.put('\n', "\\n");
        a.put('\t', "\\t");
        a.put('\f', "\\f");
        a.put('\r', "\\r");
    }
    */

    @DexIgnore
    public static List<String> a(String str) {
        if (!b(str)) {
            return Qj7.i(new ArrayList(0));
        }
        String[] split = str.split(",");
        ArrayList arrayList = new ArrayList();
        for (String str2 : split) {
            if (b(str2)) {
                arrayList.add(str2);
            }
        }
        return Qj7.i(arrayList);
    }

    @DexIgnore
    public static boolean b(String str) {
        return str != null && str.trim().length() > 0;
    }

    @DexIgnore
    public static boolean c(String... strArr) {
        if (strArr == null || strArr.length == 0) {
            return false;
        }
        for (String str : strArr) {
            if (d(str)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean d(String str) {
        return !b(str);
    }

    @DexIgnore
    public static boolean e(String str) {
        if (d(str)) {
            return false;
        }
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static String f(List<String> list) {
        if (list == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (b(list.get(i))) {
                sb.append(list.get(i));
                if (i < list.size() - 1) {
                    sb.append(",");
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static String g(String... strArr) {
        return f(strArr == null ? null : Arrays.asList(strArr));
    }

    @DexIgnore
    public static String h(List<? extends Number> list) {
        ArrayList arrayList;
        if (list != null) {
            ArrayList arrayList2 = new ArrayList();
            for (Number number : list) {
                if (number != null) {
                    arrayList2.add(number.toString());
                }
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        return f(arrayList);
    }
}
