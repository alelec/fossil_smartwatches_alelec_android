package com.fossil;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G82 implements D92 {
    @DexIgnore
    public /* final */ C92 a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public G82(C92 c92) {
        this.a = c92;
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.a.t.E()) {
            this.b = true;
            for (Da2 da2 : this.a.t.x) {
                da2.d();
            }
            return false;
        }
        this.a.u(null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void b() {
        if (this.b) {
            this.b = false;
            this.a.p(new I82(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void d(int i) {
        this.a.u(null);
        this.a.u.c(i, this.b);
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void f() {
    }

    @DexIgnore
    public final void g() {
        if (this.b) {
            this.b = false;
            this.a.t.y.a();
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void i(Z52 z52, M62<?> m62, boolean z) {
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: T extends com.fossil.I72<? extends com.fossil.Z62, A> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.D92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t) {
        try {
            this.a.t.y.b(t);
            T82 t82 = this.a.t;
            M62.Fi fi = t82.p.get(t.w());
            Rc2.l(fi, "Appropriate Api was not requested.");
            if (fi.c() || !this.a.h.containsKey(t.w())) {
                boolean z = fi instanceof Wc2;
                M62.Bi bi = fi;
                if (z) {
                    bi = ((Wc2) fi).t0();
                }
                t.y(bi == 1 ? 1 : 0);
                return t;
            }
            t.A(new Status(17));
            return t;
        } catch (DeadObjectException e) {
            this.a.p(new F82(this, this));
        }
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t) {
        j(t);
        return t;
    }
}
