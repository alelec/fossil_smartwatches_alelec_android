package com.fossil;

import com.mapped.Lc3;
import com.mapped.Mc3;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fu3<TResult, TContinuationResult> implements Gt3, Lc3, Mc3<TContinuationResult>, Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Mt3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ Lu3<TContinuationResult> c;

    @DexIgnore
    public Fu3(Executor executor, Mt3<TResult, TContinuationResult> mt3, Lu3<TContinuationResult> lu3) {
        this.a = executor;
        this.b = mt3;
        this.c = lu3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        this.a.execute(new Gu3(this, nt3));
    }

    @DexIgnore
    @Override // com.fossil.Gt3
    public final void onCanceled() {
        this.c.v();
    }

    @DexIgnore
    @Override // com.mapped.Lc3
    public final void onFailure(Exception exc) {
        this.c.t(exc);
    }

    @DexIgnore
    @Override // com.mapped.Mc3
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.u(tcontinuationresult);
    }
}
