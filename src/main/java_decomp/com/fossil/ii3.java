package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ii3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;
    @DexIgnore
    public /* final */ /* synthetic */ Gg3 d;

    @DexIgnore
    public Ii3(Gg3 gg3, String str, long j) {
        this.d = gg3;
        this.b = str;
        this.c = j;
    }

    @DexIgnore
    public final void run() {
        this.d.E(this.b, this.c);
    }
}
