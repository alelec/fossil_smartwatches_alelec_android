package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Es5 implements MembersInjector<WatchAppCommuteTimeManager> {
    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, ApiServiceV2 apiServiceV2) {
        watchAppCommuteTimeManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void b(WatchAppCommuteTimeManager watchAppCommuteTimeManager, DianaAppSettingRepository dianaAppSettingRepository) {
        watchAppCommuteTimeManager.e = dianaAppSettingRepository;
    }

    @DexIgnore
    public static void c(WatchAppCommuteTimeManager watchAppCommuteTimeManager, LocationSource locationSource) {
        watchAppCommuteTimeManager.c = locationSource;
    }

    @DexIgnore
    public static void d(WatchAppCommuteTimeManager watchAppCommuteTimeManager, PortfolioApp portfolioApp) {
        watchAppCommuteTimeManager.a = portfolioApp;
    }

    @DexIgnore
    public static void e(WatchAppCommuteTimeManager watchAppCommuteTimeManager, An4 an4) {
        watchAppCommuteTimeManager.f = an4;
    }

    @DexIgnore
    public static void f(WatchAppCommuteTimeManager watchAppCommuteTimeManager, UserRepository userRepository) {
        watchAppCommuteTimeManager.d = userRepository;
    }
}
