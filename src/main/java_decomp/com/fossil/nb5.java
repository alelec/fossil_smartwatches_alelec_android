package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nb5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewSleepWeekChart q;

    @DexIgnore
    public Nb5(Object obj, View view, int i, OverviewSleepWeekChart overviewSleepWeekChart) {
        super(obj, view, i);
        this.q = overviewSleepWeekChart;
    }
}
