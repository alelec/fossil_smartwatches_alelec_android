package com.fossil;

import com.google.gson.Gson;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lq5 {
    @DexIgnore
    public List<MicroAppSetting> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public List<MicroAppSetting> a() {
        return this.a;
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public void c(Ku3 ku3) {
        this.a = new ArrayList();
        if (ku3.s(CloudLogWriter.ITEMS_PARAM)) {
            try {
                Fu3 q = ku3.q(CloudLogWriter.ITEMS_PARAM);
                if (q.size() > 0) {
                    for (int i = 0; i < q.size(); i++) {
                        Ku3 d = q.m(i).d();
                        MicroAppSetting microAppSetting = new MicroAppSetting();
                        if (d.s("appId")) {
                            microAppSetting.setMicroAppId(d.p("appId").f());
                        }
                        if (d.s(MicroAppSetting.LIKE)) {
                            microAppSetting.setLike(d.p(MicroAppSetting.LIKE).a());
                        }
                        if (d.s(MicroAppSetting.SETTING)) {
                            if (d.r(MicroAppSetting.SETTING).size() == 0) {
                                microAppSetting.setSetting("");
                            } else {
                                microAppSetting.setSetting(d.r(MicroAppSetting.SETTING).toString());
                            }
                        }
                        this.a.add(microAppSetting);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (ku3.s("_range")) {
            try {
                this.b = (Range) new Gson().k(ku3.r("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
