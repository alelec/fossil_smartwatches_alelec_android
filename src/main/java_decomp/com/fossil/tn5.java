package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.manager.WeatherManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tn5 implements MembersInjector<WeatherManager> {
    @DexIgnore
    public static void a(WeatherManager weatherManager, ApiServiceV2 apiServiceV2) {
        weatherManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void b(WeatherManager weatherManager, CustomizeRealDataRepository customizeRealDataRepository) {
        weatherManager.e = customizeRealDataRepository;
    }

    @DexIgnore
    public static void c(WeatherManager weatherManager, DianaAppSettingRepository dianaAppSettingRepository) {
        weatherManager.f = dianaAppSettingRepository;
    }

    @DexIgnore
    public static void d(WeatherManager weatherManager, GoogleApiService googleApiService) {
        weatherManager.g = googleApiService;
    }

    @DexIgnore
    public static void e(WeatherManager weatherManager, LocationSource locationSource) {
        weatherManager.c = locationSource;
    }

    @DexIgnore
    public static void f(WeatherManager weatherManager, PortfolioApp portfolioApp) {
        weatherManager.a = portfolioApp;
    }

    @DexIgnore
    public static void g(WeatherManager weatherManager, UserRepository userRepository) {
        weatherManager.d = userRepository;
    }
}
