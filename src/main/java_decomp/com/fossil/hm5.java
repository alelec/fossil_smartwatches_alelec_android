package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hm5 {
    @DexIgnore
    public Vector<Lm5> a;
    @DexIgnore
    public Map<String, Lm5> b;
    @DexIgnore
    public Map<String, Lm5> c;
    @DexIgnore
    public Map<String, Lm5> d;
    @DexIgnore
    public Map<String, Lm5> e;

    @DexIgnore
    public Hm5() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public void a(int i, Lm5 lm5) {
        if (lm5 != null) {
            d(lm5);
            this.a.add(i, lm5);
            return;
        }
        throw null;
    }

    @DexIgnore
    public boolean b(Lm5 lm5) {
        if (lm5 != null) {
            d(lm5);
            return this.a.add(lm5);
        }
        throw null;
    }

    @DexIgnore
    public Lm5 c(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public final void d(Lm5 lm5) {
        byte[] a2 = lm5.a();
        if (a2 != null) {
            this.b.put(new String(a2), lm5);
        }
        byte[] b2 = lm5.b();
        if (b2 != null) {
            this.c.put(new String(b2), lm5);
        }
        byte[] f = lm5.f();
        if (f != null) {
            this.d.put(new String(f), lm5);
        }
        byte[] e2 = lm5.e();
        if (e2 != null) {
            this.e.put(new String(e2), lm5);
        }
    }

    @DexIgnore
    public void e() {
        this.a.clear();
    }
}
