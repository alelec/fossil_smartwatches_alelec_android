package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M65 extends L65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K;
    @DexIgnore
    public long I;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        K = sparseIntArray;
        sparseIntArray.put(2131362100, 1);
        K.put(2131362668, 2);
        K.put(2131363085, 3);
        K.put(2131362546, 4);
        K.put(2131362405, 5);
        K.put(2131362638, 6);
        K.put(2131362233, 7);
        K.put(2131362212, 8);
        K.put(2131362530, 9);
        K.put(2131362287, 10);
        K.put(2131362101, 11);
        K.put(2131362669, 12);
        K.put(2131363410, 13);
        K.put(2131362677, 14);
        K.put(2131362526, 15);
        K.put(2131362496, 16);
        K.put(2131362276, 17);
    }
    */

    @DexIgnore
    public M65(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 18, J, K));
    }

    @DexIgnore
    public M65(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[11], (View) objArr[8], (FlexibleTextInputEditText) objArr[7], (FlexibleButton) objArr[17], (FlexibleButton) objArr[10], (FlexibleTextView) objArr[5], (FlexibleButton) objArr[16], (FlexibleTextView) objArr[15], (FlexibleButton) objArr[9], (FlexibleTextView) objArr[4], (FlexibleTextInputLayout) objArr[6], (RTLImageView) objArr[2], (RTLImageView) objArr[12], (RTLImageView) objArr[14], (FrameLayout) objArr[0], (ScrollView) objArr[3], (FlexibleTextView) objArr[13]);
        this.I = -1;
        this.F.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.I != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.I = 1;
        }
        w();
    }
}
