package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kc5 extends Jc5 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362386, 1);
        A.put(2131362007, 2);
        A.put(2131362540, 3);
        A.put(2131363116, 4);
        A.put(2131362497, 5);
        A.put(2131362738, 6);
        A.put(2131362274, 7);
    }
    */

    @DexIgnore
    public Kc5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public Kc5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleCheckBox) objArr[2], (FlexibleButton) objArr[7], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (ImageView) objArr[6], (ShimmerFrameLayout) objArr[4]);
        this.y = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.x = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
