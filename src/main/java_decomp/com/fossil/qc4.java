package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qc4 implements Rc4 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Bd4 b;
    @DexIgnore
    public /* final */ Sc4 c;
    @DexIgnore
    public /* final */ B94 d;
    @DexIgnore
    public /* final */ Nc4 e;
    @DexIgnore
    public /* final */ Fd4 f;
    @DexIgnore
    public /* final */ C94 g;
    @DexIgnore
    public /* final */ AtomicReference<Zc4> h; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ AtomicReference<Ot3<Wc4>> i; // = new AtomicReference<>(new Ot3());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Mt3<Void, Void> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public Nt3<Void> a(Void r7) throws Exception {
            JSONObject a2 = Qc4.this.f.a(Qc4.this.b, true);
            if (a2 != null) {
                Ad4 b = Qc4.this.c.b(a2);
                Qc4.this.e.c(b.d(), a2);
                Qc4.this.q(a2, "Loaded settings: ");
                Qc4 qc4 = Qc4.this;
                qc4.r(qc4.b.f);
                Qc4.this.h.set(b);
                ((Ot3) Qc4.this.i.get()).e(b.c());
                Ot3 ot3 = new Ot3();
                ot3.e(b.c());
                Qc4.this.i.set(ot3);
            }
            return Qt3.f(null);
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Nt3' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Mt3
        public /* bridge */ /* synthetic */ Nt3<Void> then(Void r2) throws Exception {
            return a(r2);
        }
    }

    @DexIgnore
    public Qc4(Context context, Bd4 bd4, B94 b94, Sc4 sc4, Nc4 nc4, Fd4 fd4, C94 c94) {
        this.a = context;
        this.b = bd4;
        this.d = b94;
        this.c = sc4;
        this.e = nc4;
        this.f = fd4;
        this.g = c94;
        this.h.set(Oc4.e(b94));
    }

    @DexIgnore
    public static Qc4 l(Context context, String str, H94 h94, Kb4 kb4, String str2, String str3, String str4, C94 c94) {
        String e2 = h94.e();
        R94 r94 = new R94();
        return new Qc4(context, new Bd4(str, h94.f(), h94.g(), h94.h(), h94, R84.h(R84.p(context), str, str3, str2), str3, str2, E94.determineFrom(e2).getId()), r94, new Sc4(r94), new Nc4(context), new Ed4(str4, String.format(Locale.US, "https://firebase-settings.crashlytics.com/spi/v2/platforms/android/gmp/%s/settings", str), kb4), c94);
    }

    @DexIgnore
    @Override // com.fossil.Rc4
    public Nt3<Wc4> a() {
        return this.i.get().a();
    }

    @DexIgnore
    @Override // com.fossil.Rc4
    public Zc4 b() {
        return this.h.get();
    }

    @DexIgnore
    public boolean k() {
        return !n().equals(this.b.f);
    }

    @DexIgnore
    public final Ad4 m(Pc4 pc4) {
        Exception e2;
        Ad4 ad4;
        try {
            if (Pc4.SKIP_CACHE_LOOKUP.equals(pc4)) {
                return null;
            }
            JSONObject b2 = this.e.b();
            if (b2 != null) {
                ad4 = this.c.b(b2);
                if (ad4 != null) {
                    q(b2, "Loaded cached settings: ");
                    long a2 = this.d.a();
                    if (Pc4.IGNORE_CACHE_EXPIRATION.equals(pc4) || !ad4.e(a2)) {
                        try {
                            X74.f().b("Returning cached settings.");
                            return ad4;
                        } catch (Exception e3) {
                            e2 = e3;
                            X74.f().e("Failed to get cached settings", e2);
                            return ad4;
                        }
                    } else {
                        X74.f().b("Cached settings have expired.");
                        return null;
                    }
                } else {
                    X74.f().e("Failed to parse cached settings data.", null);
                    return null;
                }
            } else {
                X74.f().b("No cached settings data found.");
                return null;
            }
        } catch (Exception e4) {
            e2 = e4;
            ad4 = null;
            X74.f().e("Failed to get cached settings", e2);
            return ad4;
        }
    }

    @DexIgnore
    public final String n() {
        return R84.t(this.a).getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public Nt3<Void> o(Pc4 pc4, Executor executor) {
        Ad4 m;
        if (k() || (m = m(pc4)) == null) {
            Ad4 m2 = m(Pc4.IGNORE_CACHE_EXPIRATION);
            if (m2 != null) {
                this.h.set(m2);
                this.i.get().e(m2.c());
            }
            return this.g.d().s(executor, new Ai());
        }
        this.h.set(m);
        this.i.get().e(m.c());
        return Qt3.f(null);
    }

    @DexIgnore
    public Nt3<Void> p(Executor executor) {
        return o(Pc4.USE_CACHE, executor);
    }

    @DexIgnore
    public final void q(JSONObject jSONObject, String str) throws JSONException {
        X74 f2 = X74.f();
        f2.b(str + jSONObject.toString());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final boolean r(String str) {
        SharedPreferences.Editor edit = R84.t(this.a).edit();
        edit.putString("existing_instance_identifier", str);
        edit.apply();
        return true;
    }
}
