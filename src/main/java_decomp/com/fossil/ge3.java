package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ge3 extends Me3 {
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public Ge3(float f) {
        super(0, Float.valueOf(Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
        this.d = Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    @Override // com.fossil.Me3
    public final String toString() {
        float f = this.d;
        StringBuilder sb = new StringBuilder(30);
        sb.append("[Dash: length=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }
}
