package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ TimerTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ TimerTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ RecyclerView y;
    @DexIgnore
    public /* final */ SwipeRefreshLayout z;

    @DexIgnore
    public T95(Object obj, View view, int i, ConstraintLayout constraintLayout, TimerTextView timerTextView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, TimerTextView timerTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, RTLImageView rTLImageView, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = timerTextView;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = timerTextView2;
        this.v = flexibleTextView3;
        this.w = imageView;
        this.x = rTLImageView;
        this.y = recyclerView;
        this.z = swipeRefreshLayout;
        this.A = flexibleTextView4;
    }
}
