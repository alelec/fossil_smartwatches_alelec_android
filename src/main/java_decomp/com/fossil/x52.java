package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X52 {
    @DexIgnore
    public static /* final */ int[] LoadingImageView; // = {2130968916, 2130969269, 2130969270};
    @DexIgnore
    public static /* final */ int LoadingImageView_circleCrop; // = 0;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatio; // = 1;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatioAdjust; // = 2;
    @DexIgnore
    public static /* final */ int[] SignInButton; // = {2130968850, 2130968948, 2130969576};
    @DexIgnore
    public static /* final */ int SignInButton_buttonSize; // = 0;
    @DexIgnore
    public static /* final */ int SignInButton_colorScheme; // = 1;
    @DexIgnore
    public static /* final */ int SignInButton_scopeUris; // = 2;
}
