package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Da extends Ux1<Iw1> {
    @DexIgnore
    public static /* final */ Da b; // = new Da();

    @DexIgnore
    public Da() {
        super(new Ry1(3, 0));
    }

    @DexIgnore
    @Override // com.fossil.Qx1
    public /* bridge */ /* synthetic */ byte[] b(Object obj) {
        return d((Iw1) obj);
    }

    @DexIgnore
    public byte[] d(Iw1 iw1) {
        ByteBuffer order = ByteBuffer.allocate(64).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(PACK\u2026(ByteOrder.LITTLE_ENDIAN)");
        Cc0[] f = iw1.f();
        Cc0[] b2 = iw1.b();
        Cc0[] d = iw1.d();
        Cc0[] e = iw1.e();
        Cc0[] c = iw1.c();
        Cc0[] a2 = iw1.a();
        Cc0[] i = iw1.i();
        byte[] bArr = new byte[0];
        int i2 = 88;
        for (int i3 = 0; i3 < 7; i3++) {
            Cc0[] cc0Arr = new Cc0[][]{f, b2, d, e, c, a2, i}[i3];
            order.putInt(i2);
            for (Cc0 cc0 : cc0Arr) {
                String a3 = Iy1.a(cc0.b);
                Charset c2 = Hd0.y.c();
                if (a3 != null) {
                    byte[] bytes = a3.getBytes(c2);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] array = ByteBuffer.allocate(bytes.length + 1 + 2 + cc0.c.length).order(ByteOrder.LITTLE_ENDIAN).put((byte) bytes.length).put(bytes).putShort((short) cc0.c.length).put(cc0.c).array();
                    Wg6.b(array, "ByteBuffer.allocate(\n   \u2026\n                .array()");
                    bArr = Dm7.q(bArr, array);
                    i2 += array.length;
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        byte[] a4 = iw1.g().a();
        byte[] array2 = order.array();
        Wg6.b(array2, "metaData.array()");
        return Dm7.q(Dm7.q(a4, array2), bArr);
    }
}
