package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W88<T> implements E88<W18, T> {
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public W88(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ Object a(W18 w18) throws IOException {
        return b(w18);
    }

    @DexIgnore
    public T b(W18 w18) throws IOException {
        JsonReader q = this.a.q(w18.charStream());
        try {
            T read = this.b.read(q);
            if (q.V() == Nk4.END_DOCUMENT) {
                return read;
            }
            throw new Ej4("JSON document was not fully consumed.");
        } finally {
            w18.close();
        }
    }
}
