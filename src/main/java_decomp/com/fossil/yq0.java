package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.SharedElementCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yq0 {
    @DexIgnore
    public static /* final */ int[] a; // = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    @DexIgnore
    public static /* final */ Ar0 b; // = (Build.VERSION.SDK_INT >= 21 ? new Zq0() : null);
    @DexIgnore
    public static /* final */ Ar0 c; // = x();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Gi b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Om0 d;

        @DexIgnore
        public Ai(Gi gi, Fragment fragment, Om0 om0) {
            this.b = gi;
            this.c = fragment;
            this.d = om0;
        }

        @DexIgnore
        public void run() {
            this.b.a(this.c, this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public Bi(ArrayList arrayList) {
            this.b = arrayList;
        }

        @DexIgnore
        public void run() {
            Yq0.B(this.b, 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Gi b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Om0 d;

        @DexIgnore
        public Ci(Gi gi, Fragment fragment, Om0 om0) {
            this.b = gi;
            this.c = fragment;
            this.d = om0;
        }

        @DexIgnore
        public void run() {
            this.b.a(this.c, this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object b;
        @DexIgnore
        public /* final */ /* synthetic */ Ar0 c;
        @DexIgnore
        public /* final */ /* synthetic */ View d;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList g;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList h;
        @DexIgnore
        public /* final */ /* synthetic */ Object i;

        @DexIgnore
        public Di(Object obj, Ar0 ar0, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.b = obj;
            this.c = ar0;
            this.d = view;
            this.e = fragment;
            this.f = arrayList;
            this.g = arrayList2;
            this.h = arrayList3;
            this.i = obj2;
        }

        @DexIgnore
        public void run() {
            Object obj = this.b;
            if (obj != null) {
                this.c.p(obj, this.d);
                this.g.addAll(Yq0.k(this.c, this.b, this.e, this.f, this.d));
            }
            if (this.h != null) {
                if (this.i != null) {
                    ArrayList<View> arrayList = new ArrayList<>();
                    arrayList.add(this.d);
                    this.c.q(this.i, this.h, arrayList);
                }
                this.h.clear();
                this.h.add(this.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ Zi0 e;
        @DexIgnore
        public /* final */ /* synthetic */ View f;
        @DexIgnore
        public /* final */ /* synthetic */ Ar0 g;
        @DexIgnore
        public /* final */ /* synthetic */ Rect h;

        @DexIgnore
        public Ei(Fragment fragment, Fragment fragment2, boolean z, Zi0 zi0, View view, Ar0 ar0, Rect rect) {
            this.b = fragment;
            this.c = fragment2;
            this.d = z;
            this.e = zi0;
            this.f = view;
            this.g = ar0;
            this.h = rect;
        }

        @DexIgnore
        public void run() {
            Yq0.f(this.b, this.c, this.d, this.e, false);
            View view = this.f;
            if (view != null) {
                this.g.k(view, this.h);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Ar0 b;
        @DexIgnore
        public /* final */ /* synthetic */ Zi0 c;
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ Hi e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;
        @DexIgnore
        public /* final */ /* synthetic */ View g;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment h;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment i;
        @DexIgnore
        public /* final */ /* synthetic */ boolean j;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList k;
        @DexIgnore
        public /* final */ /* synthetic */ Object l;
        @DexIgnore
        public /* final */ /* synthetic */ Rect m;

        @DexIgnore
        public Fi(Ar0 ar0, Zi0 zi0, Object obj, Hi hi, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.b = ar0;
            this.c = zi0;
            this.d = obj;
            this.e = hi;
            this.f = arrayList;
            this.g = view;
            this.h = fragment;
            this.i = fragment2;
            this.j = z;
            this.k = arrayList2;
            this.l = obj2;
            this.m = rect;
        }

        @DexIgnore
        public void run() {
            Zi0<String, View> h2 = Yq0.h(this.b, this.c, this.d, this.e);
            if (h2 != null) {
                this.f.addAll(h2.values());
                this.f.add(this.g);
            }
            Yq0.f(this.h, this.i, this.j, h2, false);
            Object obj = this.d;
            if (obj != null) {
                this.b.A(obj, this.k, this.f);
                View t = Yq0.t(h2, this.e, this.l, this.j);
                if (t != null) {
                    this.b.k(t, this.m);
                }
            }
        }
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        void a(Fragment fragment, Om0 om0);

        @DexIgnore
        void b(Fragment fragment, Om0 om0);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Hi {
        @DexIgnore
        public Fragment a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public Iq0 c;
        @DexIgnore
        public Fragment d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Iq0 f;
    }

    @DexIgnore
    public static void A(Ar0 ar0, Object obj, Object obj2, Zi0<String, View> zi0, boolean z, Iq0 iq0) {
        ArrayList<String> arrayList = iq0.n;
        if (arrayList != null && !arrayList.isEmpty()) {
            View view = zi0.get(z ? iq0.o.get(0) : iq0.n.get(0));
            ar0.v(obj, view);
            if (obj2 != null) {
                ar0.v(obj2, view);
            }
        }
    }

    @DexIgnore
    public static void B(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    @DexIgnore
    public static void C(FragmentManager fragmentManager, ArrayList<Iq0> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z, Gi gi) {
        if (fragmentManager.n >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                Iq0 iq0 = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    e(iq0, sparseArray, z);
                } else {
                    c(iq0, sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(fragmentManager.o.e());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    Zi0<String, String> d = d(keyAt, arrayList, arrayList2, i, i2);
                    Hi hi = (Hi) sparseArray.valueAt(i4);
                    if (z) {
                        o(fragmentManager, keyAt, hi, view, d, gi);
                    } else {
                        n(fragmentManager, keyAt, hi, view, d, gi);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, Zi0<String, View> zi0, Collection<String> collection) {
        for (int size = zi0.size() - 1; size >= 0; size--) {
            View n = zi0.n(size);
            if (collection.contains(Mo0.H(n))) {
                arrayList.add(n);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x008a, code lost:
        if (r7.mAdded != false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x008c, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00a0, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x00dc, code lost:
        if (r7.mHidden == false) goto L_0x008c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0070 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:93:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(com.fossil.Iq0 r10, com.fossil.Xq0.Ai r11, android.util.SparseArray<com.fossil.Yq0.Hi> r12, boolean r13, boolean r14) {
        /*
        // Method dump skipped, instructions count: 224
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yq0.b(com.fossil.Iq0, com.fossil.Xq0$Ai, android.util.SparseArray, boolean, boolean):void");
    }

    @DexIgnore
    public static void c(Iq0 iq0, SparseArray<Hi> sparseArray, boolean z) {
        int size = iq0.a.size();
        for (int i = 0; i < size; i++) {
            b(iq0, iq0.a.get(i), sparseArray, false, z);
        }
    }

    @DexIgnore
    public static Zi0<String, String> d(int i, ArrayList<Iq0> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        Zi0<String, String> zi0 = new Zi0<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            Iq0 iq0 = arrayList.get(i4);
            if (iq0.E(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = iq0.n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = iq0.n;
                        arrayList4 = iq0.o;
                    } else {
                        ArrayList<String> arrayList6 = iq0.n;
                        arrayList3 = iq0.o;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = zi0.remove(str2);
                        if (remove != null) {
                            zi0.put(str, remove);
                        } else {
                            zi0.put(str, str2);
                        }
                    }
                }
            }
        }
        return zi0;
    }

    @DexIgnore
    public static void e(Iq0 iq0, SparseArray<Hi> sparseArray, boolean z) {
        if (iq0.r.p.c()) {
            for (int size = iq0.a.size() - 1; size >= 0; size--) {
                b(iq0, iq0.a.get(size), sparseArray, true, z);
            }
        }
    }

    @DexIgnore
    public static void f(Fragment fragment, Fragment fragment2, boolean z, Zi0<String, View> zi0, boolean z2) {
        int i;
        int i2;
        SharedElementCallback enterTransitionCallback = z ? fragment2.getEnterTransitionCallback() : fragment.getEnterTransitionCallback();
        if (enterTransitionCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (zi0 == null) {
                i2 = 0;
                i = 0;
            } else {
                i2 = zi0.size();
                i = 0;
            }
            while (i < i2) {
                arrayList2.add(zi0.j(i));
                arrayList.add(zi0.n(i));
                i++;
            }
            if (z2) {
                enterTransitionCallback.g(arrayList2, arrayList, null);
            } else {
                enterTransitionCallback.f(arrayList2, arrayList, null);
            }
        }
    }

    @DexIgnore
    public static boolean g(Ar0 ar0, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!ar0.e(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static Zi0<String, View> h(Ar0 ar0, Zi0<String, String> zi0, Object obj, Hi hi) {
        SharedElementCallback enterTransitionCallback;
        ArrayList<String> arrayList;
        String q;
        Fragment fragment = hi.a;
        View view = fragment.getView();
        if (zi0.isEmpty() || obj == null || view == null) {
            zi0.clear();
            return null;
        }
        Zi0<String, View> zi02 = new Zi0<>();
        ar0.j(zi02, view);
        Iq0 iq0 = hi.c;
        if (hi.b) {
            enterTransitionCallback = fragment.getExitTransitionCallback();
            arrayList = iq0.n;
        } else {
            enterTransitionCallback = fragment.getEnterTransitionCallback();
            arrayList = iq0.o;
        }
        if (arrayList != null) {
            zi02.p(arrayList);
            zi02.p(zi0.values());
        }
        if (enterTransitionCallback != null) {
            enterTransitionCallback.d(arrayList, zi02);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = zi02.get(str);
                if (view2 == null) {
                    String q2 = q(zi0, str);
                    if (q2 != null) {
                        zi0.remove(q2);
                    }
                } else if (!str.equals(Mo0.H(view2)) && (q = q(zi0, str)) != null) {
                    zi0.put(q, Mo0.H(view2));
                }
            }
        } else {
            y(zi0, zi02);
        }
        return zi02;
    }

    @DexIgnore
    public static Zi0<String, View> i(Ar0 ar0, Zi0<String, String> zi0, Object obj, Hi hi) {
        SharedElementCallback exitTransitionCallback;
        ArrayList<String> arrayList;
        if (zi0.isEmpty() || obj == null) {
            zi0.clear();
            return null;
        }
        Fragment fragment = hi.d;
        Zi0<String, View> zi02 = new Zi0<>();
        ar0.j(zi02, fragment.requireView());
        Iq0 iq0 = hi.f;
        if (hi.e) {
            exitTransitionCallback = fragment.getEnterTransitionCallback();
            arrayList = iq0.o;
        } else {
            exitTransitionCallback = fragment.getExitTransitionCallback();
            arrayList = iq0.n;
        }
        if (arrayList != null) {
            zi02.p(arrayList);
        }
        if (exitTransitionCallback != null) {
            exitTransitionCallback.d(arrayList, zi02);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = zi02.get(str);
                if (view == null) {
                    zi0.remove(str);
                } else if (!str.equals(Mo0.H(view))) {
                    zi0.put(Mo0.H(view), zi0.remove(str));
                }
            }
        } else {
            zi0.p(zi02.keySet());
        }
        return zi02;
    }

    @DexIgnore
    public static Ar0 j(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        Ar0 ar0 = b;
        if (ar0 != null && g(ar0, arrayList)) {
            return b;
        }
        Ar0 ar02 = c;
        if (ar02 != null && g(ar02, arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    @DexIgnore
    public static ArrayList<View> k(Ar0 ar0, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            ar0.f(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        ar0.b(obj, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static Object l(Ar0 ar0, ViewGroup viewGroup, View view, Zi0<String, String> zi0, Hi hi, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Rect rect;
        Fragment fragment = hi.a;
        Fragment fragment2 = hi.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hi.b;
        Object u = zi0.isEmpty() ? null : u(ar0, fragment, fragment2, z);
        Zi0<String, View> i = i(ar0, zi0, u, hi);
        if (zi0.isEmpty()) {
            obj3 = null;
        } else {
            arrayList.addAll(i.values());
            obj3 = u;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        f(fragment, fragment2, z, i, true);
        if (obj3 != null) {
            rect = new Rect();
            ar0.z(obj3, view, arrayList);
            A(ar0, obj3, obj2, i, hi.e, hi.f);
            if (obj != null) {
                ar0.u(obj, rect);
            }
        } else {
            rect = null;
        }
        Jo0.a(viewGroup, new Fi(ar0, zi0, obj3, hi, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect));
        return obj3;
    }

    @DexIgnore
    public static Object m(Ar0 ar0, ViewGroup viewGroup, View view, Zi0<String, String> zi0, Hi hi, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        View view2;
        Rect rect;
        Fragment fragment = hi.a;
        Fragment fragment2 = hi.d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hi.b;
        Object u = zi0.isEmpty() ? null : u(ar0, fragment, fragment2, z);
        Zi0<String, View> i = i(ar0, zi0, u, hi);
        Zi0<String, View> h = h(ar0, zi0, u, hi);
        if (zi0.isEmpty()) {
            if (i != null) {
                i.clear();
            }
            if (h != null) {
                h.clear();
            }
            obj3 = null;
        } else {
            a(arrayList, i, zi0.keySet());
            a(arrayList2, h, zi0.values());
            obj3 = u;
        }
        if (obj == null && obj2 == null && obj3 == null) {
            return null;
        }
        f(fragment, fragment2, z, i, true);
        if (obj3 != null) {
            arrayList2.add(view);
            ar0.z(obj3, view, arrayList);
            A(ar0, obj3, obj2, i, hi.e, hi.f);
            rect = new Rect();
            view2 = t(h, hi, obj, z);
            if (view2 != null) {
                ar0.u(obj, rect);
            }
        } else {
            view2 = null;
            rect = null;
        }
        Jo0.a(viewGroup, new Ei(fragment, fragment2, z, h, view2, ar0, rect));
        return obj3;
    }

    @DexIgnore
    public static void n(FragmentManager fragmentManager, int i, Hi hi, View view, Zi0<String, String> zi0, Gi gi) {
        ViewGroup viewGroup = fragmentManager.p.c() ? (ViewGroup) fragmentManager.p.b(i) : null;
        if (viewGroup != null) {
            Fragment fragment = hi.a;
            Fragment fragment2 = hi.d;
            Ar0 j = j(fragment2, fragment);
            if (j != null) {
                boolean z = hi.b;
                boolean z2 = hi.e;
                Object r = r(j, fragment, z);
                Object s = s(j, fragment2, z2);
                ArrayList arrayList = new ArrayList();
                ArrayList<View> arrayList2 = new ArrayList<>();
                Object l = l(j, viewGroup, view, zi0, hi, arrayList, arrayList2, r, s);
                if (r != null || l != null || s != null) {
                    ArrayList<View> k = k(j, s, fragment2, arrayList, view);
                    Object obj = (k == null || k.isEmpty()) ? null : s;
                    j.a(r, view);
                    Object v = v(j, r, obj, l, fragment, hi.b);
                    if (!(fragment2 == null || k == null || (k.size() <= 0 && arrayList.size() <= 0))) {
                        Om0 om0 = new Om0();
                        gi.b(fragment2, om0);
                        j.w(fragment2, v, om0, new Ci(gi, fragment2, om0));
                    }
                    if (v != null) {
                        ArrayList<View> arrayList3 = new ArrayList<>();
                        j.t(v, r, arrayList3, obj, k, l, arrayList2);
                        z(j, viewGroup, fragment, view, arrayList2, r, arrayList3, obj, k);
                        j.x(viewGroup, arrayList2, zi0);
                        j.c(viewGroup, v);
                        j.s(viewGroup, arrayList2, zi0);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void o(FragmentManager fragmentManager, int i, Hi hi, View view, Zi0<String, String> zi0, Gi gi) {
        ViewGroup viewGroup = fragmentManager.p.c() ? (ViewGroup) fragmentManager.p.b(i) : null;
        if (viewGroup != null) {
            Fragment fragment = hi.a;
            Fragment fragment2 = hi.d;
            Ar0 j = j(fragment2, fragment);
            if (j != null) {
                boolean z = hi.b;
                boolean z2 = hi.e;
                ArrayList<View> arrayList = new ArrayList<>();
                ArrayList<View> arrayList2 = new ArrayList<>();
                Object r = r(j, fragment, z);
                Object s = s(j, fragment2, z2);
                Object m = m(j, viewGroup, view, zi0, hi, arrayList2, arrayList, r, s);
                if (r != null || m != null || s != null) {
                    ArrayList<View> k = k(j, s, fragment2, arrayList2, view);
                    ArrayList<View> k2 = k(j, r, fragment, arrayList, view);
                    B(k2, 4);
                    Object v = v(j, r, s, m, fragment, z);
                    if (!(fragment2 == null || k == null || (k.size() <= 0 && arrayList2.size() <= 0))) {
                        Om0 om0 = new Om0();
                        gi.b(fragment2, om0);
                        j.w(fragment2, v, om0, new Ai(gi, fragment2, om0));
                    }
                    if (v != null) {
                        w(j, s, fragment2, k);
                        ArrayList<String> o = j.o(arrayList);
                        j.t(v, r, k2, s, k, m, arrayList);
                        j.c(viewGroup, v);
                        j.y(viewGroup, arrayList2, arrayList, o, zi0);
                        B(k2, 0);
                        j.A(m, arrayList2, arrayList);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static Hi p(Hi hi, SparseArray<Hi> sparseArray, int i) {
        if (hi != null) {
            return hi;
        }
        Hi hi2 = new Hi();
        sparseArray.put(i, hi2);
        return hi2;
    }

    @DexIgnore
    public static String q(Zi0<String, String> zi0, String str) {
        int size = zi0.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(zi0.n(i))) {
                return zi0.j(i);
            }
        }
        return null;
    }

    @DexIgnore
    public static Object r(Ar0 ar0, Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return ar0.g(z ? fragment.getReenterTransition() : fragment.getEnterTransition());
    }

    @DexIgnore
    public static Object s(Ar0 ar0, Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return ar0.g(z ? fragment.getReturnTransition() : fragment.getExitTransition());
    }

    @DexIgnore
    public static View t(Zi0<String, View> zi0, Hi hi, Object obj, boolean z) {
        ArrayList<String> arrayList;
        Iq0 iq0 = hi.c;
        if (obj == null || zi0 == null || (arrayList = iq0.n) == null || arrayList.isEmpty()) {
            return null;
        }
        return zi0.get(z ? iq0.n.get(0) : iq0.o.get(0));
    }

    @DexIgnore
    public static Object u(Ar0 ar0, Fragment fragment, Fragment fragment2, boolean z) {
        if (fragment == null || fragment2 == null) {
            return null;
        }
        return ar0.B(ar0.g(z ? fragment2.getSharedElementReturnTransition() : fragment.getSharedElementEnterTransition()));
    }

    @DexIgnore
    public static Object v(Ar0 ar0, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        return (obj == null || obj2 == null || fragment == null) ? true : z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap() ? ar0.n(obj2, obj, obj3) : ar0.m(obj2, obj, obj3);
    }

    @DexIgnore
    public static void w(Ar0 ar0, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            ar0.r(obj, fragment.getView(), arrayList);
            Jo0.a(fragment.mContainer, new Bi(arrayList));
        }
    }

    @DexIgnore
    public static Ar0 x() {
        try {
            return (Ar0) Class.forName("com.fossil.cy0").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public static void y(Zi0<String, String> zi0, Zi0<String, View> zi02) {
        for (int size = zi0.size() - 1; size >= 0; size--) {
            if (!zi02.containsKey(zi0.n(size))) {
                zi0.l(size);
            }
        }
    }

    @DexIgnore
    public static void z(Ar0 ar0, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        Jo0.a(viewGroup, new Di(obj, ar0, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }
}
