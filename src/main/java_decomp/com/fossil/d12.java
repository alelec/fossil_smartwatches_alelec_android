package com.fossil;

import java.lang.Throwable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface D12<TInput, TResult, TException extends Throwable> {
    @DexIgnore
    TResult apply(TInput tinput) throws Throwable;
}
