package com.fossil;

import com.fossil.Tq4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv5 extends Tq4<Ai.Bii, Ai.Cii, Ai.Aii> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Tq4.Ai {
            @DexIgnore
            public Aii(int i) {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements Tq4.Bi {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements Tq4.Ci {
            @DexIgnore
            public Cii(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Bv5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ Bv5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(Bv5 bv5) {
            this.a = bv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Bv5.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.a.b().a(new Ai.Aii(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(Bv5.f.a(), "executeUseCase - onSuccess");
            this.a.b().onSuccess(new Ai.Cii(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = Bv5.class.getSimpleName();
        Wg6.b(simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public Bv5(ServerSettingRepository serverSettingRepository) {
        Wg6.c(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Ai.Bii bii) {
        h(bii);
    }

    @DexIgnore
    public void h(Ai.Bii bii) {
        this.d.getServerSettingList(new Bi(this));
    }
}
