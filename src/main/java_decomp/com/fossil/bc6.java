package com.fossil;

import com.mapped.DianaCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bc6 implements MembersInjector<DianaCustomizeEditFragment> {
    @DexIgnore
    public static void a(DianaCustomizeEditFragment dianaCustomizeEditFragment, MicroAppPresenter microAppPresenter) {
        dianaCustomizeEditFragment.l = microAppPresenter;
    }

    @DexIgnore
    public static void b(DianaCustomizeEditFragment dianaCustomizeEditFragment, Po4 po4) {
        dianaCustomizeEditFragment.m = po4;
    }
}
