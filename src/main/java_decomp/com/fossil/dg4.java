package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.mapped.W6;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dg4 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Map<String, Long> c; // = new Zi0();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static /* final */ long d; // = TimeUnit.DAYS.toMillis(7);
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public Ai(String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        public static String a(String str, String str2, long j) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("token", str);
                jSONObject.put("appVersion", str2);
                jSONObject.put("timestamp", j);
                return jSONObject.toString();
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb.append("Failed to encode token: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                return null;
            }
        }

        @DexIgnore
        public static Ai c(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            if (!str.startsWith("{")) {
                return new Ai(str, null, 0);
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                return new Ai(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
                sb.append("Failed to parse token: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                return null;
            }
        }

        @DexIgnore
        public boolean b(String str) {
            return System.currentTimeMillis() > this.c + d || !str.equals(this.b);
        }
    }

    @DexIgnore
    public Dg4(Context context) {
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        a("com.google.android.gms.appid-no-backup");
    }

    @DexIgnore
    public static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public final void a(String str) {
        File file = new File(W6.i(this.b), str);
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !i()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    d();
                    FirebaseInstanceId.m().F();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public final String c(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    @DexIgnore
    public void d() {
        synchronized (this) {
            this.c.clear();
            this.a.edit().clear().commit();
        }
    }

    @DexIgnore
    public final void e(String str) {
        SharedPreferences.Editor edit = this.a.edit();
        for (String str2 : this.a.getAll().keySet()) {
            if (str2.startsWith(str)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    @DexIgnore
    public void f(String str) {
        synchronized (this) {
            e(String.valueOf(str).concat("|T|"));
        }
    }

    @DexIgnore
    public final long g(String str) {
        String string = this.a.getString(b(str, "cre"), null);
        if (string != null) {
            try {
                return Long.parseLong(string);
            } catch (NumberFormatException e) {
            }
        }
        return 0;
    }

    @DexIgnore
    public Ai h(String str, String str2, String str3) {
        Ai c2;
        synchronized (this) {
            c2 = Ai.c(this.a.getString(c(str, str2, str3), null));
        }
        return c2;
    }

    @DexIgnore
    public boolean i() {
        boolean isEmpty;
        synchronized (this) {
            isEmpty = this.a.getAll().isEmpty();
        }
        return isEmpty;
    }

    @DexIgnore
    public void j(String str, String str2, String str3, String str4, String str5) {
        synchronized (this) {
            String a2 = Ai.a(str4, str5, System.currentTimeMillis());
            if (a2 != null) {
                SharedPreferences.Editor edit = this.a.edit();
                edit.putString(c(str, str2, str3), a2);
                edit.commit();
            }
        }
    }

    @DexIgnore
    public long k(String str) {
        long l;
        synchronized (this) {
            l = l(str);
            this.c.put(str, Long.valueOf(l));
        }
        return l;
    }

    @DexIgnore
    public final long l(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.a.contains(b(str, "cre"))) {
            return g(str);
        }
        SharedPreferences.Editor edit = this.a.edit();
        edit.putString(b(str, "cre"), String.valueOf(currentTimeMillis));
        edit.commit();
        return currentTimeMillis;
    }
}
