package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ts2 extends Pu2 implements Us2 {
    @DexIgnore
    public Ts2() {
        super("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    @DexIgnore
    @Override // com.fossil.Pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            C1(parcel.readString(), parcel.readString(), (Bundle) Qt2.a(parcel, Bundle.CREATOR), parcel.readLong());
            parcel2.writeNoException();
        } else if (i != 2) {
            return false;
        } else {
            int zza = zza();
            parcel2.writeNoException();
            parcel2.writeInt(zza);
        }
        return true;
    }
}
