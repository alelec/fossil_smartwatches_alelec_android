package com.fossil;

import com.fossil.Yg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xg4 implements Ah4 {
    @DexIgnore
    public /* final */ Bh4 a;
    @DexIgnore
    public /* final */ Ot3<Yg4> b;

    @DexIgnore
    public Xg4(Bh4 bh4, Ot3<Yg4> ot3) {
        this.a = bh4;
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.Ah4
    public boolean a(Fh4 fh4, Exception exc) {
        if (!fh4.i() && !fh4.j() && !fh4.l()) {
            return false;
        }
        this.b.d(exc);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Ah4
    public boolean b(Fh4 fh4) {
        if (!fh4.k() || this.a.b(fh4)) {
            return false;
        }
        Ot3<Yg4> ot3 = this.b;
        Yg4.Ai a2 = Yg4.a();
        a2.b(fh4.b());
        a2.d(fh4.c());
        a2.c(fh4.h());
        ot3.c(a2.a());
        return true;
    }
}
