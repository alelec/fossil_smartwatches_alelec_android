package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tr0 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
    }
}
