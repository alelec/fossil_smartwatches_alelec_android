package com.fossil;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.Nb1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vg1<T> implements Qb1<T, Bitmap> {
    @DexIgnore
    public static /* final */ Nb1<Long> d; // = Nb1.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new Ai());
    @DexIgnore
    public static /* final */ Nb1<Integer> e; // = Nb1.a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new Bi());
    @DexIgnore
    public static /* final */ Ei f; // = new Ei();
    @DexIgnore
    public /* final */ Fi<T> a;
    @DexIgnore
    public /* final */ Rd1 b;
    @DexIgnore
    public /* final */ Ei c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Nb1.Bi<Long> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(8);

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [byte[], java.lang.Object, java.security.MessageDigest] */
        @Override // com.fossil.Nb1.Bi
        public /* bridge */ /* synthetic */ void a(byte[] bArr, Long l, MessageDigest messageDigest) {
            b(bArr, l, messageDigest);
        }

        @DexIgnore
        public void b(byte[] bArr, Long l, MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.a) {
                this.a.position(0);
                messageDigest.update(this.a.putLong(l.longValue()).array());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Nb1.Bi<Integer> {
        @DexIgnore
        public /* final */ ByteBuffer a; // = ByteBuffer.allocate(4);

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [byte[], java.lang.Object, java.security.MessageDigest] */
        @Override // com.fossil.Nb1.Bi
        public /* bridge */ /* synthetic */ void a(byte[] bArr, Integer num, MessageDigest messageDigest) {
            b(bArr, num, messageDigest);
        }

        @DexIgnore
        public void b(byte[] bArr, Integer num, MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.a) {
                    this.a.position(0);
                    messageDigest.update(this.a.putInt(num.intValue()).array());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Fi<AssetFileDescriptor> {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public /* synthetic */ Ci(Ai ai) {
            this();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.media.MediaMetadataRetriever, java.lang.Object] */
        @Override // com.fossil.Vg1.Fi
        public /* bridge */ /* synthetic */ void a(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            b(mediaMetadataRetriever, assetFileDescriptor);
        }

        @DexIgnore
        public void b(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Fi<ByteBuffer> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends MediaDataSource {
            @DexIgnore
            public /* final */ /* synthetic */ ByteBuffer b;

            @DexIgnore
            public Aii(Di di, ByteBuffer byteBuffer) {
                this.b = byteBuffer;
            }

            @DexIgnore
            @Override // java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public long getSize() {
                return (long) this.b.limit();
            }

            @DexIgnore
            @Override // android.media.MediaDataSource
            public int readAt(long j, byte[] bArr, int i, int i2) {
                if (j >= ((long) this.b.limit())) {
                    return -1;
                }
                this.b.position((int) j);
                int min = Math.min(i2, this.b.remaining());
                this.b.get(bArr, i, min);
                return min;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.media.MediaMetadataRetriever, java.lang.Object] */
        @Override // com.fossil.Vg1.Fi
        public /* bridge */ /* synthetic */ void a(MediaMetadataRetriever mediaMetadataRetriever, ByteBuffer byteBuffer) {
            b(mediaMetadataRetriever, byteBuffer);
        }

        @DexIgnore
        public void b(MediaMetadataRetriever mediaMetadataRetriever, ByteBuffer byteBuffer) {
            mediaMetadataRetriever.setDataSource(new Aii(this, byteBuffer));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public MediaMetadataRetriever a() {
            return new MediaMetadataRetriever();
        }
    }

    @DexIgnore
    public interface Fi<T> {
        @DexIgnore
        void a(MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Fi<ParcelFileDescriptor> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [android.media.MediaMetadataRetriever, java.lang.Object] */
        @Override // com.fossil.Vg1.Fi
        public /* bridge */ /* synthetic */ void a(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            b(mediaMetadataRetriever, parcelFileDescriptor);
        }

        @DexIgnore
        public void b(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    @DexIgnore
    public Vg1(Rd1 rd1, Fi<T> fi) {
        this(rd1, fi, f);
    }

    @DexIgnore
    public Vg1(Rd1 rd1, Fi<T> fi, Ei ei) {
        this.b = rd1;
        this.a = fi;
        this.c = ei;
    }

    @DexIgnore
    public static Qb1<AssetFileDescriptor, Bitmap> c(Rd1 rd1) {
        return new Vg1(rd1, new Ci(null));
    }

    @DexIgnore
    public static Qb1<ByteBuffer, Bitmap> d(Rd1 rd1) {
        return new Vg1(rd1, new Di());
    }

    @DexIgnore
    public static Bitmap e(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, Fg1 fg1) {
        Bitmap g = (Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || fg1 == Fg1.d) ? null : g(mediaMetadataRetriever, j, i, i2, i3, fg1);
        return g == null ? f(mediaMetadataRetriever, j, i) : g;
    }

    @DexIgnore
    public static Bitmap f(MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }

    @DexIgnore
    @TargetApi(27)
    public static Bitmap g(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, Fg1 fg1) {
        int i4;
        int i5;
        try {
            int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                i5 = parseInt;
                i4 = parseInt2;
            } else {
                i5 = parseInt2;
                i4 = parseInt;
            }
            float b2 = fg1.b(i4, i5, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, Math.round(((float) i4) * b2), Math.round(((float) i5) * b2));
        } catch (Throwable th) {
            if (Log.isLoggable("VideoDecoder", 3)) {
                Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            }
            return null;
        }
    }

    @DexIgnore
    public static Qb1<ParcelFileDescriptor, Bitmap> h(Rd1 rd1) {
        return new Vg1(rd1, new Gi());
    }

    @DexIgnore
    @Override // com.fossil.Qb1
    public boolean a(T t, Ob1 ob1) {
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: com.fossil.Ob1 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Qb1
    public Id1<Bitmap> b(T t, int i, int i2, Ob1 ob1) throws IOException {
        long longValue = ((Long) ob1.c(d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) ob1.c(e);
            Integer num2 = num == null ? 2 : num;
            Fg1 fg1 = (Fg1) ob1.c(Fg1.f);
            Fg1 fg12 = fg1 == null ? Fg1.e : fg1;
            MediaMetadataRetriever a2 = this.c.a();
            try {
                this.a.a(a2, t);
                Bitmap e2 = e(a2, longValue, num2.intValue(), i, i2, fg12);
                a2.release();
                return Yf1.f(e2, this.b);
            } catch (RuntimeException e3) {
                throw new IOException(e3);
            } catch (Throwable th) {
                a2.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }
}
