package com.fossil;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.fossil.Yh1;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wa1 implements ComponentCallbacks2, Ei1 {
    @DexIgnore
    public static /* final */ Fj1 s; // = ((Fj1) Fj1.t0(Bitmap.class).V());
    @DexIgnore
    public /* final */ Oa1 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ Di1 d;
    @DexIgnore
    public /* final */ Ji1 e;
    @DexIgnore
    public /* final */ Ii1 f;
    @DexIgnore
    public /* final */ Li1 g;
    @DexIgnore
    public /* final */ Runnable h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Yh1 j;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Ej1<Object>> k;
    @DexIgnore
    public Fj1 l;
    @DexIgnore
    public boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            Wa1 wa1 = Wa1.this;
            wa1.d.a(wa1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Yh1.Ai {
        @DexIgnore
        public /* final */ Ji1 a;

        @DexIgnore
        public Bi(Ji1 ji1) {
            this.a = ji1;
        }

        @DexIgnore
        @Override // com.fossil.Yh1.Ai
        public void a(boolean z) {
            if (z) {
                synchronized (Wa1.this) {
                    this.a.e();
                }
            }
        }
    }

    /*
    static {
        Fj1 fj1 = (Fj1) Fj1.t0(Hh1.class).V();
        Fj1 fj12 = (Fj1) ((Fj1) Fj1.u0(Wc1.b).e0(Sa1.LOW)).m0(true);
    }
    */

    @DexIgnore
    public Wa1(Oa1 oa1, Di1 di1, Ii1 ii1, Context context) {
        this(oa1, di1, ii1, new Ji1(), oa1.g(), context);
    }

    @DexIgnore
    public Wa1(Oa1 oa1, Di1 di1, Ii1 ii1, Ji1 ji1, Zh1 zh1, Context context) {
        this.g = new Li1();
        this.h = new Ai();
        this.i = new Handler(Looper.getMainLooper());
        this.b = oa1;
        this.d = di1;
        this.f = ii1;
        this.e = ji1;
        this.c = context;
        this.j = zh1.a(context.getApplicationContext(), new Bi(ji1));
        if (Jk1.p()) {
            this.i.post(this.h);
        } else {
            di1.a(this);
        }
        di1.a(this.j);
        this.k = new CopyOnWriteArrayList<>(oa1.i().c());
        y(oa1.i().d());
        oa1.o(this);
    }

    @DexIgnore
    public boolean A(Qj1<?> qj1) {
        synchronized (this) {
            Bj1 i2 = qj1.i();
            if (i2 == null) {
                return true;
            }
            if (!this.e.a(i2)) {
                return false;
            }
            this.g.l(qj1);
            qj1.d(null);
            return true;
        }
    }

    @DexIgnore
    public final void B(Qj1<?> qj1) {
        boolean A = A(qj1);
        Bj1 i2 = qj1.i();
        if (!A && !this.b.p(qj1) && i2 != null) {
            qj1.d(null);
            i2.clear();
        }
    }

    @DexIgnore
    public <ResourceType> Va1<ResourceType> c(Class<ResourceType> cls) {
        return new Va1<>(this.b, this, cls, this.c);
    }

    @DexIgnore
    public Va1<Bitmap> e() {
        return c(Bitmap.class).u0(s);
    }

    @DexIgnore
    public Va1<Drawable> g() {
        return c(Drawable.class);
    }

    @DexIgnore
    public void l(Qj1<?> qj1) {
        if (qj1 != null) {
            B(qj1);
        }
    }

    @DexIgnore
    public List<Ej1<Object>> m() {
        return this.k;
    }

    @DexIgnore
    public Fj1 n() {
        Fj1 fj1;
        synchronized (this) {
            fj1 = this.l;
        }
        return fj1;
    }

    @DexIgnore
    public <T> Xa1<?, T> o(Class<T> cls) {
        return this.b.i().e(cls);
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onDestroy() {
        synchronized (this) {
            this.g.onDestroy();
            for (Qj1<?> qj1 : this.g.e()) {
                l(qj1);
            }
            this.g.c();
            this.e.b();
            this.d.b(this);
            this.d.b(this.j);
            this.i.removeCallbacks(this.h);
            this.b.s(this);
        }
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStart() {
        synchronized (this) {
            x();
            this.g.onStart();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStop() {
        synchronized (this) {
            w();
            this.g.onStop();
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.m) {
            v();
        }
    }

    @DexIgnore
    public Va1<Drawable> p(Bitmap bitmap) {
        return g().I0(bitmap);
    }

    @DexIgnore
    public Va1<Drawable> q(Uri uri) {
        return g().J0(uri);
    }

    @DexIgnore
    public Va1<Drawable> r(Integer num) {
        return g().L0(num);
    }

    @DexIgnore
    public Va1<Drawable> s(Object obj) {
        return g().M0(obj);
    }

    @DexIgnore
    public Va1<Drawable> t(String str) {
        return g().N0(str);
    }

    @DexIgnore
    public String toString() {
        String str;
        synchronized (this) {
            str = super.toString() + "{tracker=" + this.e + ", treeNode=" + this.f + "}";
        }
        return str;
    }

    @DexIgnore
    public void u() {
        synchronized (this) {
            this.e.c();
        }
    }

    @DexIgnore
    public void v() {
        synchronized (this) {
            u();
            for (Wa1 wa1 : this.f.a()) {
                wa1.u();
            }
        }
    }

    @DexIgnore
    public void w() {
        synchronized (this) {
            this.e.d();
        }
    }

    @DexIgnore
    public void x() {
        synchronized (this) {
            this.e.f();
        }
    }

    @DexIgnore
    public void y(Fj1 fj1) {
        synchronized (this) {
            this.l = (Fj1) ((Fj1) fj1.i()).e();
        }
    }

    @DexIgnore
    public void z(Qj1<?> qj1, Bj1 bj1) {
        synchronized (this) {
            this.g.g(qj1);
            this.e.g(bj1);
        }
    }
}
