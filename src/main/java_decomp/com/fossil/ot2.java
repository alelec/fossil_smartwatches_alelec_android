package com.fossil;

import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ot2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ R93 f;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ot2(Zs2 zs2, R93 r93) {
        super(zs2);
        this.g = zs2;
        this.f = r93;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.g.h.getCurrentScreenClass(this.f);
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void b() {
        this.f.c(null);
    }
}
