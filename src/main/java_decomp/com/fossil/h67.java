package com.fossil;

import android.content.res.Resources;
import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H67 extends RecyclerView.l {
    @DexIgnore
    public int a;

    @DexIgnore
    public H67() {
        Resources system = Resources.getSystem();
        Wg6.b(system, "Resources.getSystem()");
        this.a = system.getDisplayMetrics().widthPixels;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.l
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Wg6.c(rect, "outRect");
        Wg6.c(view, "view");
        Wg6.c(recyclerView, "parent");
        Wg6.c(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        RecyclerView.g adapter = recyclerView.getAdapter();
        if (adapter != null) {
            Wg6.b(adapter, "parent.adapter!!");
            int itemCount = adapter.getItemCount();
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            if (itemCount > 0) {
                int width = recyclerView.getWidth();
                int width2 = view.getWidth();
                if (width == 0) {
                    width = this.a;
                }
                if (width2 == 0) {
                    width2 = this.a / 2;
                }
                if (itemCount == 1 || childAdapterPosition == 0) {
                    rect.set(Math.max(0, (width - width2) / 2), 0, 0, 0);
                } else if (childAdapterPosition == itemCount - 1) {
                    rect.set(0, 0, Math.max(0, (width - width2) / 2), 0);
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }
}
