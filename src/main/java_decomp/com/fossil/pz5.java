package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pz5 implements Factory<Y36> {
    @DexIgnore
    public static Y36 a(Oz5 oz5) {
        Y36 a2 = oz5.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
