package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class V84 implements H84 {
    @DexIgnore
    public /* final */ W84 a;

    @DexIgnore
    public V84(W84 w84) {
        this.a = w84;
    }

    @DexIgnore
    public static H84 b(W84 w84) {
        return new V84(w84);
    }

    @DexIgnore
    @Override // com.fossil.H84
    public void a(String str) {
        this.a.k(str);
    }
}
