package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y55 extends X55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        U.put(2131363291, 2);
        U.put(2131363376, 3);
        U.put(2131362528, 4);
        U.put(2131362054, 5);
        U.put(2131362790, 6);
        U.put(2131362791, 7);
        U.put(2131362789, 8);
        U.put(2131362670, 9);
        U.put(2131362028, 10);
        U.put(2131363550, 11);
        U.put(2131363542, 12);
        U.put(2131363549, 13);
        U.put(2131363546, 14);
        U.put(2131362573, 15);
        U.put(2131362688, 16);
        U.put(2131362578, 17);
        U.put(2131362126, 18);
        U.put(2131363531, 19);
        U.put(2131363423, 20);
        U.put(2131363530, 21);
        U.put(2131363422, 22);
        U.put(2131363529, 23);
        U.put(2131363421, 24);
        U.put(2131363447, 25);
        U.put(2131362176, 26);
        U.put(2131363049, 27);
    }
    */

    @DexIgnore
    public Y55(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 28, T, U));
    }

    @DexIgnore
    public Y55(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (View) objArr[10], (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[18], (CardView) objArr[26], (FlexibleTextView) objArr[4], (View) objArr[15], (View) objArr[17], (ImageView) objArr[9], (ImageView) objArr[16], (View) objArr[8], (View) objArr[6], (View) objArr[7], (ConstraintLayout) objArr[0], (ViewPager2) objArr[27], (RTLImageView) objArr[2], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[24], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[20], (View) objArr[25], (CustomizeWidget) objArr[23], (CustomizeWidget) objArr[21], (CustomizeWidget) objArr[19], (CustomizeWidget) objArr[12], (CustomizeWidget) objArr[14], (CustomizeWidget) objArr[13], (CustomizeWidget) objArr[11]);
        this.S = -1;
        this.D.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.S != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.S = 1;
        }
        w();
    }
}
