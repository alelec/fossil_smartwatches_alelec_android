package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pd0 {
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;

    @DexIgnore
    public Pd0(int i, long j) {
        this.a = i;
        this.b = j;
    }

    @DexIgnore
    public final Pd0 a(int i, long j) {
        return new Pd0(i, j);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Pd0) {
                Pd0 pd0 = (Pd0) obj;
                if (!(this.a == pd0.a && this.b == pd0.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a;
        long j = this.b;
        return (i * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        StringBuilder e = E.e("ExponentBackOffInfo(currentExponent=");
        e.append(this.a);
        e.append(", maxRate=");
        e.append(this.b);
        e.append(")");
        return e.toString();
    }
}
