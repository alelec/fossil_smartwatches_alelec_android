package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I2 extends C2 {
    @DexIgnore
    public static /* final */ H2 CREATOR; // = new H2(null);
    @DexIgnore
    public /* final */ Cu1 e;
    @DexIgnore
    public /* final */ byte f;

    @DexIgnore
    public I2(byte b, Cu1 cu1, byte b2) {
        super(Lt.m, b, false, 4);
        this.e = cu1;
        this.f = (byte) b2;
    }

    @DexIgnore
    public I2(Parcel parcel) {
        super(parcel);
        Cu1 a2 = Cu1.d.a(parcel.readByte());
        this.e = a2 == null ? Cu1.LOW : a2;
        this.f = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.n1, Ey1.a(this.e)), Jd0.b5, Byte.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.f);
        }
    }
}
