package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.helper.AppHelper;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xn5 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public SsoHandler a;
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Xn5.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Xn5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 c;

        @DexIgnore
        public Bi(Xn5 xn5, Yn5 yn5) {
            this.b = xn5;
            this.c = yn5;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.c()) {
                this.c.b(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements WbAuthListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xn5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 c;

        @DexIgnore
        public Ci(Xn5 xn5, Handler handler, Yn5 yn5) {
            this.a = xn5;
            this.b = handler;
            this.c = yn5;
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void cancel() {
            FLogger.INSTANCE.getLocal().d(Xn5.d.a(), "Weibo loginWithEmail cancel");
            this.a.e(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.b(2, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onFailure(WbConnectErrorMessage wbConnectErrorMessage) {
            Wg6.c(wbConnectErrorMessage, "wbConnectErrorMessage");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Xn5.d.a();
            local.d(a2, "Weibo loginWithEmail failed - message: " + wbConnectErrorMessage.getErrorMessage() + "- code: " + wbConnectErrorMessage.getErrorCode());
            this.a.e(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.b(600, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onSuccess(Oauth2AccessToken oauth2AccessToken) {
            Wg6.c(oauth2AccessToken, "oauth2AccessToken");
            this.a.e(true);
            this.b.removeCallbacksAndMessages(null);
            String token = oauth2AccessToken.getToken();
            String format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).format(new Date(oauth2AccessToken.getExpiresTime()));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Xn5.d.a();
            local.d(a2, "Get access token from weibo success: " + token + " ,expire date: " + format);
            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
            signUpSocialAuth.setClientId(AppHelper.g.a(""));
            Wg6.b(token, "authToken");
            signUpSocialAuth.setToken(token);
            signUpSocialAuth.setService("weibo");
            this.c.a(signUpSocialAuth);
        }
    }

    /*
    static {
        String simpleName = Xn5.class.getSimpleName();
        Wg6.b(simpleName, "MFLoginWeiboManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final void b(int i, int i2, Intent intent) {
        Wg6.c(intent, "data");
        SsoHandler ssoHandler = this.a;
        if (ssoHandler == null) {
            return;
        }
        if (ssoHandler != null) {
            ssoHandler.authorizeCallBack(i, i2, intent);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final boolean c() {
        return this.b;
    }

    @DexIgnore
    public final void d(Activity activity, Yn5 yn5) {
        Wg6.c(activity, Constants.ACTIVITY);
        Wg6.c(yn5, Constants.CALLBACK);
        try {
            this.b = false;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new Bi(this, yn5), 60000);
            SsoHandler ssoHandler = new SsoHandler(activity);
            this.a = ssoHandler;
            if (ssoHandler != null) {
                ssoHandler.authorize(new Ci(this, handler, yn5));
            } else {
                Wg6.i();
                throw null;
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "Weibo loginWithEmail failed - ex=" + e);
            yn5.b(600, null, "");
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3) {
        Wg6.c(str, "apiKey");
        Wg6.c(str2, "redirectUrl");
        Wg6.c(str3, "scope");
        try {
            WbSdk.install(PortfolioApp.get.instance(), new AuthInfo(PortfolioApp.get.instance(), str, str2, str3));
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(c, "exception when install WBSdk");
        }
    }
}
