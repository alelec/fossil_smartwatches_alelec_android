package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yq4 extends RecyclerView.g<Ai> {
    @DexIgnore
    public ArrayList<String> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(View view) {
            super(view);
            Wg6.c(view, "itemView");
            View findViewById = view.findViewById(2131362406);
            Wg6.b(findViewById, "itemView.findViewById(R.id.ftv_description)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362691);
            Wg6.b(findViewById2, "itemView.findViewById(R.id.iv_device)");
            this.b = (ImageView) findViewById2;
        }

        @DexIgnore
        public final void a(String str) {
            if (str != null) {
                this.a.setText(str);
            }
        }
    }

    @DexIgnore
    public void g(Ai ai, int i) {
        Wg6.c(ai, "viewHolder");
        ai.a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public Ai h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558705, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(inflate);
    }

    @DexIgnore
    public final void i(List<String> list) {
        Wg6.c(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        g(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }
}
