package com.fossil;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P72<L> {
    @DexIgnore
    public /* final */ Ci a;
    @DexIgnore
    public volatile L b;
    @DexIgnore
    public volatile Ai<L> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<L> {
        @DexIgnore
        public /* final */ L a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Ai(L l, String str) {
            this.a = l;
            this.b = str;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            return this.a == ai.a && this.b.equals(ai.b);
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 31) + this.b.hashCode();
        }
    }

    @DexIgnore
    public interface Bi<L> {
        @DexIgnore
        void a(L l);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends Ol2 {
        @DexIgnore
        public Ci(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            Rc2.a(z);
            P72.this.d((Bi) message.obj);
        }
    }

    @DexIgnore
    public P72(Looper looper, L l, String str) {
        this.a = new Ci(looper);
        Rc2.l(l, "Listener must not be null");
        this.b = l;
        Rc2.g(str);
        this.c = new Ai<>(l, str);
    }

    @DexIgnore
    public final void a() {
        this.b = null;
        this.c = null;
    }

    @DexIgnore
    public final Ai<L> b() {
        return this.c;
    }

    @DexIgnore
    public final void c(Bi<? super L> bi) {
        Rc2.l(bi, "Notifier must not be null");
        this.a.sendMessage(this.a.obtainMessage(1, bi));
    }

    @DexIgnore
    public final void d(Bi<? super L> bi) {
        L l = this.b;
        if (l == null) {
            bi.b();
            return;
        }
        try {
            bi.a(l);
        } catch (RuntimeException e) {
            bi.b();
            throw e;
        }
    }
}
