package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I23 implements J23 {
    @DexIgnore
    @Override // com.fossil.J23
    public final Object a(Object obj) {
        return G23.zza().zzb();
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final Object b(Object obj) {
        ((G23) obj).zzc();
        return obj;
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final int zza(int i, Object obj, Object obj2) {
        G23 g23 = (G23) obj;
        E23 e23 = (E23) obj2;
        if (!g23.isEmpty()) {
            Iterator it = g23.entrySet().iterator();
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                entry.getKey();
                entry.getValue();
                throw new NoSuchMethodError();
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final Object zza(Object obj, Object obj2) {
        G23 g23 = (G23) obj;
        G23 g232 = (G23) obj2;
        if (!g232.isEmpty()) {
            if (!g23.zzd()) {
                g23 = g23.zzb();
            }
            g23.zza(g232);
        }
        return g23;
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final Map<?, ?> zza(Object obj) {
        return (G23) obj;
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final H23<?, ?> zzb(Object obj) {
        E23 e23 = (E23) obj;
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final Map<?, ?> zzc(Object obj) {
        return (G23) obj;
    }

    @DexIgnore
    @Override // com.fossil.J23
    public final boolean zzd(Object obj) {
        return !((G23) obj).zzd();
    }
}
