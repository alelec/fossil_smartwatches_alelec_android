package com.fossil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cx0 {
    @DexIgnore
    public static /* final */ Map<String, Lock> e; // = new HashMap();
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public FileChannel d;

    @DexIgnore
    public Cx0(String str, File file, boolean z) {
        File file2 = new File(file, str + ".lck");
        this.a = file2;
        this.b = a(file2.getAbsolutePath());
        this.c = z;
    }

    @DexIgnore
    public static Lock a(String str) {
        Lock lock;
        synchronized (e) {
            lock = e.get(str);
            if (lock == null) {
                lock = new ReentrantLock();
                e.put(str, lock);
            }
        }
        return lock;
    }

    @DexIgnore
    public void b() {
        this.b.lock();
        if (this.c) {
            try {
                FileChannel channel = new FileOutputStream(this.a).getChannel();
                this.d = channel;
                channel.lock();
            } catch (IOException e2) {
                throw new IllegalStateException("Unable to grab copy lock.", e2);
            }
        }
    }

    @DexIgnore
    public void c() {
        FileChannel fileChannel = this.d;
        if (fileChannel != null) {
            try {
                fileChannel.close();
            } catch (IOException e2) {
            }
        }
        this.b.unlock();
    }
}
