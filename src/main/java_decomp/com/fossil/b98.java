package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B98 implements E88<W18, Double> {
    @DexIgnore
    public static /* final */ B98 a; // = new B98();

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ Double a(W18 w18) throws IOException {
        return b(w18);
    }

    @DexIgnore
    public Double b(W18 w18) throws IOException {
        return Double.valueOf(w18.string());
    }
}
