package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Cf;
import com.mapped.DashboardGoalTrackingAdapter;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eh6 extends BaseFragment implements Dh6, Zw5, Aw5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<P55> g;
    @DexIgnore
    public Ch6 h;
    @DexIgnore
    public DashboardGoalTrackingAdapter i;
    @DexIgnore
    public GoalTrackingOverviewFragment j;
    @DexIgnore
    public F67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Eh6.m;
        }

        @DexIgnore
        public final Eh6 b() {
            return new Eh6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends F67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ Eh6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, Eh6 eh6, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = eh6;
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void b(int i) {
            Eh6.K6(this.f).p();
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void c(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = Eh6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "DashboardGoalTrackingFra\u2026::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Ch6 K6(Eh6 eh6) {
        Ch6 ch6 = eh6.h;
        if (ch6 != null) {
            return ch6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return m;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ch6 ch6) {
        N6(ch6);
    }

    @DexIgnore
    public final P55 M6() {
        G37<P55> g37 = this.g;
        if (g37 != null) {
            return g37.a();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void N6(Ch6 ch6) {
        Wg6.c(ch6, "presenter");
        this.h = ch6;
    }

    @DexIgnore
    @Override // com.fossil.Zw5
    public void Q(Date date) {
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.C;
            Wg6.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z) {
        P55 M6;
        RecyclerView recyclerView;
        View view;
        if (isVisible() && this.g != null && (M6 = M6()) != null && (recyclerView = M6.q) != null) {
            RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
            if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                recyclerView.smoothScrollToPosition(0);
                F67 f67 = this.k;
                if (f67 != null) {
                    f67.d();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Dh6
    public void d() {
        F67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<P55> g37 = new G37<>(this, (P55) Aq0.f(layoutInflater, 2131558545, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            P55 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        Ch6 ch6 = this.h;
        if (ch6 == null) {
            return;
        }
        if (ch6 != null) {
            ch6.o();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        Ch6 ch6 = this.h;
        if (ch6 != null) {
            ch6.o();
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Ch6 ch6 = this.h;
        if (ch6 != null) {
            ch6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        Ch6 ch6 = this.h;
        if (ch6 != null) {
            ch6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        GoalTrackingOverviewFragment goalTrackingOverviewFragment = (GoalTrackingOverviewFragment) getChildFragmentManager().Z("GoalTrackingOverviewFragment");
        this.j = goalTrackingOverviewFragment;
        if (goalTrackingOverviewFragment == null) {
            this.j = new GoalTrackingOverviewFragment();
        }
        Yw5 yw5 = new Yw5();
        PortfolioApp instance = PortfolioApp.get.instance();
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        GoalTrackingOverviewFragment goalTrackingOverviewFragment2 = this.j;
        if (goalTrackingOverviewFragment2 != null) {
            this.i = new DashboardGoalTrackingAdapter(yw5, instance, this, childFragmentManager, goalTrackingOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            P55 M6 = M6();
            if (!(M6 == null || (recyclerView2 = M6.q) == null)) {
                Wg6.b(recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                DashboardGoalTrackingAdapter dashboardGoalTrackingAdapter = this.i;
                if (dashboardGoalTrackingAdapter != null) {
                    recyclerView2.setAdapter(dashboardGoalTrackingAdapter);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        Bi bi = new Bi(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.k = bi;
                        if (bi != null) {
                            recyclerView2.addOnScrollListener(bi);
                            recyclerView2.setItemViewCacheSize(0);
                            Bd6 bd6 = new Bd6(linearLayoutManager.q2());
                            Drawable f = W6.f(recyclerView2.getContext(), 2131230856);
                            if (f != null) {
                                Wg6.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                bd6.h(f);
                                recyclerView2.addItemDecoration(bd6);
                                Ch6 ch6 = this.h;
                                if (ch6 != null) {
                                    ch6.n();
                                } else {
                                    Wg6.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    Wg6.n("mDashboardGoalTrackingAdapter");
                    throw null;
                }
            }
            P55 M62 = M6();
            if (M62 != null) {
                RecyclerView recyclerView3 = M62.q;
            }
            P55 M63 = M6();
            if (!(M63 == null || (recyclerView = M63.q) == null)) {
                Wg6.b(recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof Pv0) {
                    ((Pv0) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Ts0 a2 = Vs0.e(activity).a(Y67.class);
                Wg6.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                Y67 y67 = (Y67) a2;
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Dh6
    public void q(Cf<GoalTrackingSummary> cf) {
        DashboardGoalTrackingAdapter dashboardGoalTrackingAdapter = this.i;
        if (dashboardGoalTrackingAdapter != null) {
            dashboardGoalTrackingAdapter.x(cf);
        } else {
            Wg6.n("mDashboardGoalTrackingAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw5
    public void q0(Date date, Date date2) {
        Wg6.c(date, "startWeekDate");
        Wg6.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
