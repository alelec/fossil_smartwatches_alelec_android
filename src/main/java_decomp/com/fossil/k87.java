package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.RingAdapter;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.edit.complication.ring_selector.WatchFaceRingViewModel;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K87 extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public WatchFaceRingViewModel h;
    @DexIgnore
    public Yg5 i;
    @DexIgnore
    public RingAdapter j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final K87 a() {
            return new K87();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements RingAdapter.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ K87 a;

        @DexIgnore
        public Bi(K87 k87) {
            this.a = k87;
        }

        @DexIgnore
        @Override // com.mapped.RingAdapter.Ai
        public void a(P77 p77) {
            Wg6.c(p77, "ring");
            Cb7 cb7 = new Cb7(p77.d(), p77.f(), p77.c().a());
            Gc7 d = Hc7.c.d(this.a);
            if (d != null) {
                d.A(cb7);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ K87 a;

        @DexIgnore
        public Ci(K87 k87) {
            this.a = k87;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Gc7 d = Hc7.c.d(this.a);
            if (d != null) {
                d.w(z);
            }
            K87.L6(this.a).o(!z);
            if (z) {
                this.a.S6("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<? extends P77>> {
        @DexIgnore
        public /* final */ /* synthetic */ K87 a;

        @DexIgnore
        public Di(K87 k87) {
            this.a = k87;
        }

        @DexIgnore
        public final void a(List<P77> list) {
            RingAdapter L6 = K87.L6(this.a);
            Wg6.b(list, "it");
            L6.n(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends P77> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Eb7> {
        @DexIgnore
        public /* final */ /* synthetic */ K87 a;

        @DexIgnore
        public Ei(K87 k87) {
            this.a = k87;
        }

        @DexIgnore
        public final void a(Eb7 eb7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceRingFragment", "watchFaceComplicationsLive, value = " + eb7);
            String a2 = eb7.b().a();
            if (eb7.a() == Ab7.REMOVED || a2 == null) {
                K87.L6(this.a).q(null);
                this.a.P6();
                return;
            }
            if (!Ik5.d.e(a2)) {
                this.a.P6();
            } else {
                this.a.T6();
            }
            this.a.S6(eb7.b().c());
            if (eb7.a() == Ab7.ADDED) {
                FlexibleSwitchCompat flexibleSwitchCompat = K87.K6(this.a).e;
                Wg6.b(flexibleSwitchCompat, "mBinding.switchRing");
                flexibleSwitchCompat.setChecked(false);
                K87.L6(this.a).o(true);
            } else if (eb7.a() == Ab7.SELECTED) {
                boolean d = eb7.b().d();
                FlexibleSwitchCompat flexibleSwitchCompat2 = K87.K6(this.a).e;
                Wg6.b(flexibleSwitchCompat2, "mBinding.switchRing");
                flexibleSwitchCompat2.setChecked(d);
                K87.L6(this.a).o(!d);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Eb7 eb7) {
            a(eb7);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Yg5 K6(K87 k87) {
        Yg5 yg5 = k87.i;
        if (yg5 != null) {
            return yg5;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ RingAdapter L6(K87 k87) {
        RingAdapter ringAdapter = k87.j;
        if (ringAdapter != null) {
            return ringAdapter;
        }
        Wg6.n("mRingAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceRingFragment";
    }

    @DexIgnore
    public final void P6() {
        Yg5 yg5 = this.i;
        if (yg5 != null) {
            View view = yg5.h;
            Wg6.b(view, "mBinding.vSeparator");
            view.setVisibility(4);
            Yg5 yg52 = this.i;
            if (yg52 != null) {
                FlexibleTextView flexibleTextView = yg52.g;
                Wg6.b(flexibleTextView, "mBinding.tvSelectedComplication");
                flexibleTextView.setVisibility(4);
                Yg5 yg53 = this.i;
                if (yg53 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = yg53.e;
                    Wg6.b(flexibleSwitchCompat, "mBinding.switchRing");
                    flexibleSwitchCompat.setVisibility(4);
                    Yg5 yg54 = this.i;
                    if (yg54 != null) {
                        FlexibleTextView flexibleTextView2 = yg54.f;
                        Wg6.b(flexibleTextView2, "mBinding.tvComplicationDetail");
                        flexibleTextView2.setVisibility(4);
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        RingAdapter ringAdapter = new RingAdapter();
        ringAdapter.p(new Bi(this));
        this.j = ringAdapter;
        Yg5 yg5 = this.i;
        if (yg5 != null) {
            RecyclerView recyclerView = yg5.d;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            RingAdapter ringAdapter2 = this.j;
            if (ringAdapter2 != null) {
                recyclerView.setAdapter(ringAdapter2);
                Yg5 yg52 = this.i;
                if (yg52 != null) {
                    yg52.e.setOnCheckedChangeListener(new Ci(this));
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.n("mRingAdapter");
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void R6() {
        LiveData<Eb7> q;
        WatchFaceRingViewModel watchFaceRingViewModel = this.h;
        if (watchFaceRingViewModel != null) {
            watchFaceRingViewModel.o().h(getViewLifecycleOwner(), new Di(this));
            Gc7 d = Hc7.c.d(this);
            if (d != null && (q = d.q()) != null) {
                q.h(getViewLifecycleOwner(), new Ei(this));
                return;
            }
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void S6(String str) {
        RingAdapter ringAdapter = this.j;
        if (ringAdapter != null) {
            ringAdapter.q(str);
            RingAdapter ringAdapter2 = this.j;
            if (ringAdapter2 != null) {
                int k2 = ringAdapter2.k(str);
                if (k2 >= 0) {
                    Yg5 yg5 = this.i;
                    if (yg5 != null) {
                        yg5.d.scrollToPosition(k2);
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                }
            } else {
                Wg6.n("mRingAdapter");
                throw null;
            }
        } else {
            Wg6.n("mRingAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6() {
        Yg5 yg5 = this.i;
        if (yg5 != null) {
            View view = yg5.h;
            Wg6.b(view, "mBinding.vSeparator");
            view.setVisibility(0);
            Yg5 yg52 = this.i;
            if (yg52 != null) {
                FlexibleTextView flexibleTextView = yg52.g;
                Wg6.b(flexibleTextView, "mBinding.tvSelectedComplication");
                flexibleTextView.setVisibility(0);
                Yg5 yg53 = this.i;
                if (yg53 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = yg53.e;
                    Wg6.b(flexibleSwitchCompat, "mBinding.switchRing");
                    flexibleSwitchCompat.setVisibility(0);
                    Yg5 yg54 = this.i;
                    if (yg54 != null) {
                        FlexibleTextView flexibleTextView2 = yg54.f;
                        Wg6.b(flexibleTextView2, "mBinding.tvComplicationDetail");
                        flexibleTextView2.setVisibility(0);
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Yg5 c = Yg5.c(layoutInflater);
        Wg6.b(c, "WatchFaceRingFragmentBinding.inflate(inflater)");
        this.i = c;
        if (c != null) {
            return c.b();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.get.instance().getIface().e0().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            Po4 po4 = this.g;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(watchFaceEditActivity, po4).a(WatchFaceRingViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                this.h = (WatchFaceRingViewModel) a2;
                Q6();
                R6();
                return;
            }
            Wg6.n("mViewModelFactory");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
