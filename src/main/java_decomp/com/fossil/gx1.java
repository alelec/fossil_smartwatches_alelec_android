package com.fossil;

import android.content.Context;
import com.mapped.W6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gx1 {
    @DexIgnore
    public static /* final */ Gx1 a; // = new Gx1();

    @DexIgnore
    public final boolean a(Context context, String[] strArr) {
        Wg6.c(context, "context");
        Wg6.c(strArr, "permissions");
        boolean z = true;
        for (String str : strArr) {
            z = z && W6.a(context, str) == 0;
        }
        return z;
    }
}
