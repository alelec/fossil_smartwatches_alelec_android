package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gu3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Fu3 c;

    @DexIgnore
    public Gu3(Fu3 fu3, Nt3 nt3) {
        this.c = fu3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        try {
            Nt3 then = Fu3.b(this.c).then(this.b.m());
            if (then == null) {
                this.c.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            then.g(Pt3.b, this.c);
            then.e(Pt3.b, this.c);
            then.a(Pt3.b, this.c);
        } catch (Lt3 e) {
            if (e.getCause() instanceof Exception) {
                this.c.onFailure((Exception) e.getCause());
            } else {
                this.c.onFailure(e);
            }
        } catch (CancellationException e2) {
            this.c.onCanceled();
        } catch (Exception e3) {
            this.c.onFailure(e3);
        }
    }
}
