package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E12 {
    @DexIgnore
    public static <TInput, TResult, TException extends Throwable> TResult a(int i, TInput tinput, D12<TInput, TResult, TException> d12, F12<TInput, TResult> f12) throws Throwable {
        TResult apply;
        if (i < 1) {
            return d12.apply(tinput);
        }
        do {
            apply = d12.apply(tinput);
            tinput = f12.a(tinput, apply);
            if (tinput == null) {
                return apply;
            }
            i--;
        } while (i >= 1);
        return apply;
    }
}
