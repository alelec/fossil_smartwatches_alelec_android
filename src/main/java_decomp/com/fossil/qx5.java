package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qx5 {
    @DexIgnore
    public /* final */ Ox5 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public Qx5(Ox5 ox5, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        Wg6.c(ox5, "mView");
        Wg6.c(str, "mDeviceId");
        Wg6.c(arrayList, "mAlarms");
        this.a = ox5;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final Ox5 d() {
        return this.a;
    }
}
