package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hv extends Dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Hv(short s, K5 k5, int i, int i2) {
        super(Iu.g, s, Hs.o, k5, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(G80.k(super.A(), Jd0.I0, Long.valueOf(this.M)), Jd0.J0, Long.valueOf(this.N));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = Hy1.o(order.getInt(0));
            this.N = Hy1.o(order.getInt(4));
            G80.k(G80.k(jSONObject, Jd0.I0, Long.valueOf(this.M)), Jd0.J0, Long.valueOf(this.N));
        }
        return jSONObject;
    }
}
