package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Tj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uj0 {
    @DexIgnore
    public static float j0; // = 0.5f;
    @DexIgnore
    public Tj0[] A;
    @DexIgnore
    public ArrayList<Tj0> B;
    @DexIgnore
    public Bi[] C;
    @DexIgnore
    public Uj0 D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public float G;
    @DexIgnore
    public int H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public int P;
    @DexIgnore
    public int Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public int U;
    @DexIgnore
    public float V;
    @DexIgnore
    public float W;
    @DexIgnore
    public Object X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public String Z;
    @DexIgnore
    public int a; // = -1;
    @DexIgnore
    public String a0;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public boolean b0;
    @DexIgnore
    public Bk0 c;
    @DexIgnore
    public boolean c0;
    @DexIgnore
    public Bk0 d;
    @DexIgnore
    public boolean d0;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int e0;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public int f0;
    @DexIgnore
    public int[] g; // = new int[2];
    @DexIgnore
    public float[] g0;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public Uj0[] h0;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public Uj0[] i0;
    @DexIgnore
    public float j; // = 1.0f;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public float m; // = 1.0f;
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public float o; // = 1.0f;
    @DexIgnore
    public Wj0 p; // = null;
    @DexIgnore
    public int[] q; // = {Integer.MAX_VALUE, Integer.MAX_VALUE};
    @DexIgnore
    public float r; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public Tj0 s; // = new Tj0(this, Tj0.Di.LEFT);
    @DexIgnore
    public Tj0 t; // = new Tj0(this, Tj0.Di.TOP);
    @DexIgnore
    public Tj0 u; // = new Tj0(this, Tj0.Di.RIGHT);
    @DexIgnore
    public Tj0 v; // = new Tj0(this, Tj0.Di.BOTTOM);
    @DexIgnore
    public Tj0 w; // = new Tj0(this, Tj0.Di.BASELINE);
    @DexIgnore
    public Tj0 x; // = new Tj0(this, Tj0.Di.CENTER_X);
    @DexIgnore
    public Tj0 y; // = new Tj0(this, Tj0.Di.CENTER_Y);
    @DexIgnore
    public Tj0 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;

        /*
        static {
            int[] iArr = new int[Bi.values().length];
            b = iArr;
            try {
                iArr[Bi.FIXED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[Bi.WRAP_CONTENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[Bi.MATCH_PARENT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[Bi.MATCH_CONSTRAINT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            int[] iArr2 = new int[Tj0.Di.values().length];
            a = iArr2;
            try {
                iArr2[Tj0.Di.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Tj0.Di.TOP.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Tj0.Di.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[Tj0.Di.BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Tj0.Di.BASELINE.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[Tj0.Di.CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[Tj0.Di.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[Tj0.Di.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[Tj0.Di.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
        }
        */
    }

    @DexIgnore
    public enum Bi {
        FIXED,
        WRAP_CONTENT,
        MATCH_CONSTRAINT,
        MATCH_PARENT
    }

    @DexIgnore
    public Uj0() {
        Tj0 tj0 = new Tj0(this, Tj0.Di.CENTER);
        this.z = tj0;
        this.A = new Tj0[]{this.s, this.u, this.t, this.v, this.w, tj0};
        this.B = new ArrayList<>();
        Bi bi = Bi.FIXED;
        this.C = new Bi[]{bi, bi};
        this.D = null;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.K = 0;
        this.L = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        this.Y = 0;
        this.Z = null;
        this.a0 = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
        this.e0 = 0;
        this.f0 = 0;
        this.g0 = new float[]{-1.0f, -1.0f};
        this.h0 = new Uj0[]{null, null};
        this.i0 = new Uj0[]{null, null};
        a();
    }

    @DexIgnore
    public int A() {
        return this.J + this.P;
    }

    @DexIgnore
    public void A0(int i2) {
        this.U = i2;
    }

    @DexIgnore
    public Bi B() {
        return this.C[1];
    }

    @DexIgnore
    public void B0(int i2) {
        this.T = i2;
    }

    @DexIgnore
    public int C() {
        return this.Y;
    }

    @DexIgnore
    public void C0(int i2) {
        this.I = i2;
    }

    @DexIgnore
    public int D() {
        if (this.Y == 8) {
            return 0;
        }
        return this.E;
    }

    @DexIgnore
    public void D0(int i2) {
        this.J = i2;
    }

    @DexIgnore
    public int E() {
        return this.U;
    }

    @DexIgnore
    public void E0(boolean z2, boolean z3, boolean z4, boolean z5) {
        if (this.n == -1) {
            if (z4 && !z5) {
                this.n = 0;
            } else if (!z4 && z5) {
                this.n = 1;
                if (this.H == -1) {
                    this.o = 1.0f / this.o;
                }
            }
        }
        if (this.n == 0 && (!this.t.k() || !this.v.k())) {
            this.n = 1;
        } else if (this.n == 1 && (!this.s.k() || !this.u.k())) {
            this.n = 0;
        }
        if (this.n == -1 && (!this.t.k() || !this.v.k() || !this.s.k() || !this.u.k())) {
            if (this.t.k() && this.v.k()) {
                this.n = 0;
            } else if (this.s.k() && this.u.k()) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (z2 && !z3) {
                this.n = 0;
            } else if (!z2 && z3) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1) {
            if (this.h > 0 && this.k == 0) {
                this.n = 0;
            } else if (this.h == 0 && this.k > 0) {
                this.o = 1.0f / this.o;
                this.n = 1;
            }
        }
        if (this.n == -1 && z2 && z3) {
            this.o = 1.0f / this.o;
            this.n = 1;
        }
    }

    @DexIgnore
    public int F() {
        return this.T;
    }

    @DexIgnore
    public void F0() {
        int i2 = this.I;
        int i3 = this.J;
        this.M = i2;
        this.N = i3;
    }

    @DexIgnore
    public int G() {
        return this.I;
    }

    @DexIgnore
    public void G0(Kj0 kj0) {
        int y2 = kj0.y(this.s);
        int y3 = kj0.y(this.t);
        int y4 = kj0.y(this.u);
        int y5 = kj0.y(this.v);
        if (y4 - y2 < 0 || y5 - y3 < 0 || y2 == Integer.MIN_VALUE || y2 == Integer.MAX_VALUE || y3 == Integer.MIN_VALUE || y3 == Integer.MAX_VALUE || y4 == Integer.MIN_VALUE || y4 == Integer.MAX_VALUE || y5 == Integer.MIN_VALUE || y5 == Integer.MAX_VALUE) {
            y5 = 0;
            y4 = 0;
            y3 = 0;
            y2 = 0;
        }
        a0(y2, y3, y4, y5);
    }

    @DexIgnore
    public int H() {
        return this.J;
    }

    @DexIgnore
    public void H0() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].f().q();
        }
    }

    @DexIgnore
    public boolean I() {
        return this.Q > 0;
    }

    @DexIgnore
    public void J(Tj0.Di di, Uj0 uj0, Tj0.Di di2, int i2, int i3) {
        h(di).a(uj0.h(di2), i2, i3, Tj0.Ci.STRONG, 0, true);
    }

    @DexIgnore
    public final boolean K(int i2) {
        int i3 = i2 * 2;
        Tj0[] tj0Arr = this.A;
        if (!(tj0Arr[i3].d == null || tj0Arr[i3].d.d == tj0Arr[i3])) {
            int i4 = i3 + 1;
            return tj0Arr[i4].d != null && tj0Arr[i4].d.d == tj0Arr[i4];
        }
    }

    @DexIgnore
    public boolean L() {
        return this.s.f().b == 1 && this.u.f().b == 1 && this.t.f().b == 1 && this.v.f().b == 1;
    }

    @DexIgnore
    public boolean M() {
        Tj0 tj0;
        Tj0 tj02;
        Tj0 tj03 = this.s;
        Tj0 tj04 = tj03.d;
        return (tj04 != null && tj04.d == tj03) || ((tj02 = (tj0 = this.u).d) != null && tj02.d == tj0);
    }

    @DexIgnore
    public boolean N() {
        Tj0 tj0;
        Tj0 tj02;
        Tj0 tj03 = this.t;
        Tj0 tj04 = tj03.d;
        return (tj04 != null && tj04.d == tj03) || ((tj02 = (tj0 = this.v).d) != null && tj02.d == tj0);
    }

    @DexIgnore
    public boolean O() {
        return this.f == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.k == 0 && this.l == 0 && this.C[1] == Bi.MATCH_CONSTRAINT;
    }

    @DexIgnore
    public boolean P() {
        return this.e == 0 && this.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.h == 0 && this.i == 0 && this.C[0] == Bi.MATCH_CONSTRAINT;
    }

    @DexIgnore
    public void Q() {
        this.s.m();
        this.t.m();
        this.u.m();
        this.v.m();
        this.w.m();
        this.x.m();
        this.y.m();
        this.z.m();
        this.D = null;
        this.r = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.E = 0;
        this.F = 0;
        this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.H = -1;
        this.I = 0;
        this.J = 0;
        this.M = 0;
        this.N = 0;
        this.O = 0;
        this.P = 0;
        this.Q = 0;
        this.R = 0;
        this.S = 0;
        this.T = 0;
        this.U = 0;
        float f2 = j0;
        this.V = f2;
        this.W = f2;
        Bi[] biArr = this.C;
        Bi bi = Bi.FIXED;
        biArr[0] = bi;
        biArr[1] = bi;
        this.X = null;
        this.Y = 0;
        this.a0 = null;
        this.e0 = 0;
        this.f0 = 0;
        float[] fArr = this.g0;
        fArr[0] = -1.0f;
        fArr[1] = -1.0f;
        this.a = -1;
        this.b = -1;
        int[] iArr = this.q;
        iArr[0] = Integer.MAX_VALUE;
        iArr[1] = Integer.MAX_VALUE;
        this.e = 0;
        this.f = 0;
        this.j = 1.0f;
        this.m = 1.0f;
        this.i = Integer.MAX_VALUE;
        this.l = Integer.MAX_VALUE;
        this.h = 0;
        this.k = 0;
        this.n = -1;
        this.o = 1.0f;
        Bk0 bk0 = this.c;
        if (bk0 != null) {
            bk0.e();
        }
        Bk0 bk02 = this.d;
        if (bk02 != null) {
            bk02.e();
        }
        this.p = null;
        this.b0 = false;
        this.c0 = false;
        this.d0 = false;
    }

    @DexIgnore
    public void R() {
        Uj0 u2 = u();
        if (u2 == null || !(u2 instanceof Vj0) || !((Vj0) u()).S0()) {
            int size = this.B.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.B.get(i2).m();
            }
        }
    }

    @DexIgnore
    public void S() {
        for (int i2 = 0; i2 < 6; i2++) {
            this.A[i2].f().e();
        }
    }

    @DexIgnore
    public void T(Ij0 ij0) {
        this.s.n(ij0);
        this.t.n(ij0);
        this.u.n(ij0);
        this.v.n(ij0);
        this.w.n(ij0);
        this.z.n(ij0);
        this.x.n(ij0);
        this.y.n(ij0);
    }

    @DexIgnore
    public void U() {
    }

    @DexIgnore
    public void V(int i2) {
        this.Q = i2;
    }

    @DexIgnore
    public void W(Object obj) {
        this.X = obj;
    }

    @DexIgnore
    public void X(String str) {
        this.Z = str;
    }

    @DexIgnore
    public void Y(String str) {
        int i2;
        int i3;
        float f2;
        int i4 = 0;
        if (str == null || str.length() == 0) {
            this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            return;
        }
        int length = str.length();
        int indexOf = str.indexOf(44);
        if (indexOf <= 0 || indexOf >= length - 1) {
            i2 = 0;
            i3 = -1;
        } else {
            String substring = str.substring(0, indexOf);
            if (!substring.equalsIgnoreCase("W")) {
                i4 = substring.equalsIgnoreCase("H") ? 1 : -1;
            }
            i2 = indexOf + 1;
            i3 = i4;
        }
        int indexOf2 = str.indexOf(58);
        if (indexOf2 < 0 || indexOf2 >= length - 1) {
            String substring2 = str.substring(i2);
            if (substring2.length() > 0) {
                f2 = Float.parseFloat(substring2);
            }
            f2 = 0.0f;
        } else {
            String substring3 = str.substring(i2, indexOf2);
            String substring4 = str.substring(indexOf2 + 1);
            if (substring3.length() > 0 && substring4.length() > 0) {
                try {
                    float parseFloat = Float.parseFloat(substring3);
                    float parseFloat2 = Float.parseFloat(substring4);
                    if (parseFloat > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && parseFloat2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        f2 = i3 == 1 ? Math.abs(parseFloat2 / parseFloat) : Math.abs(parseFloat / parseFloat2);
                    }
                } catch (NumberFormatException e2) {
                }
            }
            f2 = 0.0f;
        }
        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.G = f2;
            this.H = i3;
        }
    }

    @DexIgnore
    public void Z(int i2, int i3, int i4) {
        if (i4 == 0) {
            f0(i2, i3);
        } else if (i4 == 1) {
            t0(i2, i3);
        }
        this.c0 = true;
    }

    @DexIgnore
    public final void a() {
        this.B.add(this.s);
        this.B.add(this.t);
        this.B.add(this.u);
        this.B.add(this.v);
        this.B.add(this.x);
        this.B.add(this.y);
        this.B.add(this.z);
        this.B.add(this.w);
    }

    @DexIgnore
    public void a0(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8 = i4 - i2;
        int i9 = i5 - i3;
        this.I = i2;
        this.J = i3;
        if (this.Y == 8) {
            this.E = 0;
            this.F = 0;
            return;
        }
        if (this.C[0] != Bi.FIXED || i8 >= (i6 = this.E)) {
            i6 = i8;
        }
        if (this.C[1] != Bi.FIXED || i9 >= (i7 = this.F)) {
            i7 = i9;
        }
        this.E = i6;
        this.F = i7;
        int i10 = this.S;
        if (i7 < i10) {
            this.F = i10;
        }
        int i11 = this.E;
        int i12 = this.R;
        if (i11 < i12) {
            this.E = i12;
        }
        this.c0 = true;
    }

    @DexIgnore
    public void b(Kj0 kj0) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z6;
        int i6;
        int i7;
        boolean M2;
        boolean N2;
        Oj0 r2 = kj0.r(this.s);
        Oj0 r3 = kj0.r(this.u);
        Oj0 r4 = kj0.r(this.t);
        Oj0 r5 = kj0.r(this.v);
        Oj0 r6 = kj0.r(this.w);
        Uj0 uj0 = this.D;
        if (uj0 != null) {
            boolean z7 = uj0 != null && uj0.C[0] == Bi.WRAP_CONTENT;
            Uj0 uj02 = this.D;
            boolean z8 = uj02 != null && uj02.C[1] == Bi.WRAP_CONTENT;
            if (K(0)) {
                ((Vj0) this.D).N0(this, 0);
                M2 = true;
            } else {
                M2 = M();
            }
            if (K(1)) {
                ((Vj0) this.D).N0(this, 1);
                N2 = true;
            } else {
                N2 = N();
            }
            if (z7 && this.Y != 8 && this.s.d == null && this.u.d == null) {
                kj0.i(kj0.r(this.D.u), r3, 0, 1);
            }
            if (z8 && this.Y != 8 && this.t.d == null && this.v.d == null && this.w == null) {
                kj0.i(kj0.r(this.D.v), r5, 0, 1);
            }
            z3 = M2;
            z2 = z7;
            z4 = N2;
            z5 = z8;
        } else {
            z2 = false;
            z3 = false;
            z4 = false;
            z5 = false;
        }
        int i8 = this.E;
        int i9 = this.R;
        if (i8 >= i9) {
            i9 = i8;
        }
        int i10 = this.F;
        int i11 = this.S;
        if (i10 >= i11) {
            i11 = i10;
        }
        boolean z9 = this.C[0] != Bi.MATCH_CONSTRAINT;
        boolean z10 = this.C[1] != Bi.MATCH_CONSTRAINT;
        this.n = this.H;
        float f2 = this.G;
        this.o = f2;
        int i12 = this.e;
        int i13 = this.f;
        if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || this.Y == 8) {
            i2 = i13;
            i3 = i12;
            i4 = i9;
            i5 = i11;
            z6 = false;
        } else {
            if (this.C[0] == Bi.MATCH_CONSTRAINT && i12 == 0) {
                i12 = 3;
            }
            if (this.C[1] == Bi.MATCH_CONSTRAINT && i13 == 0) {
                i13 = 3;
            }
            Bi[] biArr = this.C;
            Bi bi = biArr[0];
            Bi bi2 = Bi.MATCH_CONSTRAINT;
            if (bi == bi2 && biArr[1] == bi2 && i12 == 3 && i13 == 3) {
                E0(z2, z5, z9, z10);
            } else {
                Bi[] biArr2 = this.C;
                Bi bi3 = biArr2[0];
                Bi bi4 = Bi.MATCH_CONSTRAINT;
                if (bi3 == bi4 && i12 == 3) {
                    this.n = 0;
                    i9 = (int) (this.o * ((float) this.F));
                    if (biArr2[1] != bi4) {
                        i2 = i13;
                        i3 = 4;
                        i4 = i9;
                        i5 = i11;
                        z6 = false;
                    }
                } else if (this.C[1] == Bi.MATCH_CONSTRAINT && i13 == 3) {
                    this.n = 1;
                    if (this.H == -1) {
                        this.o = 1.0f / this.o;
                    }
                    i11 = (int) (this.o * ((float) this.E));
                    if (this.C[0] != Bi.MATCH_CONSTRAINT) {
                        i2 = 4;
                        i3 = i12;
                        i4 = i9;
                        i5 = i11;
                        z6 = false;
                    }
                }
            }
            i2 = i13;
            i3 = i12;
            i4 = i9;
            i5 = i11;
            z6 = true;
        }
        int[] iArr = this.g;
        iArr[0] = i3;
        iArr[1] = i2;
        boolean z11 = z6 && ((i7 = this.n) == 0 || i7 == -1);
        boolean z12 = this.C[0] == Bi.WRAP_CONTENT && (this instanceof Vj0);
        boolean z13 = !this.z.k();
        if (this.a != 2) {
            Uj0 uj03 = this.D;
            Oj0 r7 = uj03 != null ? kj0.r(uj03.u) : null;
            Uj0 uj04 = this.D;
            e(kj0, z2, uj04 != null ? kj0.r(uj04.s) : null, r7, this.C[0], z12, this.s, this.u, this.I, i4, this.R, this.q[0], this.V, z11, z3, i3, this.h, this.i, this.j, z13);
        }
        if (this.b != 2) {
            boolean z14 = this.C[1] == Bi.WRAP_CONTENT && (this instanceof Vj0);
            boolean z15 = z6 && ((i6 = this.n) == 1 || i6 == -1);
            if (this.Q > 0) {
                if (this.w.f().b == 1) {
                    this.w.f().g(kj0);
                } else {
                    kj0.e(r6, r4, j(), 6);
                    Tj0 tj0 = this.w.d;
                    if (tj0 != null) {
                        kj0.e(r6, kj0.r(tj0), 0, 6);
                        z13 = false;
                    }
                }
            }
            Uj0 uj05 = this.D;
            Oj0 r8 = uj05 != null ? kj0.r(uj05.v) : null;
            Uj0 uj06 = this.D;
            e(kj0, z5, uj06 != null ? kj0.r(uj06.t) : null, r8, this.C[1], z14, this.t, this.v, this.J, i5, this.S, this.q[1], this.W, z15, z4, i2, this.k, this.l, this.m, z13);
            if (z6) {
                if (this.n == 1) {
                    kj0.l(r5, r4, r3, r2, this.o, 6);
                } else {
                    kj0.l(r3, r2, r5, r4, this.o, 6);
                }
            }
            if (this.z.k()) {
                kj0.b(this, this.z.i().e(), (float) Math.toRadians((double) (this.r + 90.0f)), this.z.d());
            }
        }
    }

    @DexIgnore
    public void b0(int i2) {
        this.F = i2;
        int i3 = this.S;
        if (i2 < i3) {
            this.F = i3;
        }
    }

    @DexIgnore
    public boolean c() {
        return this.Y != 8;
    }

    @DexIgnore
    public void c0(boolean z2) {
    }

    @DexIgnore
    public void d(int i2) {
        Zj0.a(i2, this);
    }

    @DexIgnore
    public void d0(float f2) {
        this.V = f2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:138:0x02b2  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02d7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e(com.fossil.Kj0 r18, boolean r19, com.fossil.Oj0 r20, com.fossil.Oj0 r21, com.fossil.Uj0.Bi r22, boolean r23, com.fossil.Tj0 r24, com.fossil.Tj0 r25, int r26, int r27, int r28, int r29, float r30, boolean r31, boolean r32, int r33, int r34, int r35, float r36, boolean r37) {
        /*
        // Method dump skipped, instructions count: 834
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Uj0.e(com.fossil.Kj0, boolean, com.fossil.Oj0, com.fossil.Oj0, com.fossil.Uj0$Bi, boolean, com.fossil.Tj0, com.fossil.Tj0, int, int, int, int, float, boolean, boolean, int, int, int, float, boolean):void");
    }

    @DexIgnore
    public void e0(int i2) {
        this.e0 = i2;
    }

    @DexIgnore
    public void f(Uj0 uj0, float f2, int i2) {
        Tj0.Di di = Tj0.Di.CENTER;
        J(di, uj0, di, i2, 0);
        this.r = f2;
    }

    @DexIgnore
    public void f0(int i2, int i3) {
        this.I = i2;
        int i4 = i3 - i2;
        this.E = i4;
        int i5 = this.R;
        if (i4 < i5) {
            this.E = i5;
        }
    }

    @DexIgnore
    public void g(Kj0 kj0) {
        kj0.r(this.s);
        kj0.r(this.t);
        kj0.r(this.u);
        kj0.r(this.v);
        if (this.Q > 0) {
            kj0.r(this.w);
        }
    }

    @DexIgnore
    public void g0(Bi bi) {
        this.C[0] = bi;
        if (bi == Bi.WRAP_CONTENT) {
            y0(this.T);
        }
    }

    @DexIgnore
    public Tj0 h(Tj0.Di di) {
        switch (Ai.a[di.ordinal()]) {
            case 1:
                return this.s;
            case 2:
                return this.t;
            case 3:
                return this.u;
            case 4:
                return this.v;
            case 5:
                return this.w;
            case 6:
                return this.z;
            case 7:
                return this.x;
            case 8:
                return this.y;
            case 9:
                return null;
            default:
                throw new AssertionError(di.name());
        }
    }

    @DexIgnore
    public void h0(int i2, int i3, int i4, float f2) {
        this.e = i2;
        this.h = i3;
        this.i = i4;
        this.j = f2;
        if (f2 < 1.0f && i2 == 0) {
            this.e = 2;
        }
    }

    @DexIgnore
    public ArrayList<Tj0> i() {
        return this.B;
    }

    @DexIgnore
    public void i0(float f2) {
        this.g0[0] = f2;
    }

    @DexIgnore
    public int j() {
        return this.Q;
    }

    @DexIgnore
    public void j0(int i2) {
        this.q[1] = i2;
    }

    @DexIgnore
    public float k(int i2) {
        if (i2 == 0) {
            return this.V;
        }
        if (i2 == 1) {
            return this.W;
        }
        return -1.0f;
    }

    @DexIgnore
    public void k0(int i2) {
        this.q[0] = i2;
    }

    @DexIgnore
    public int l() {
        return H() + this.F;
    }

    @DexIgnore
    public void l0(int i2) {
        if (i2 < 0) {
            this.S = 0;
        } else {
            this.S = i2;
        }
    }

    @DexIgnore
    public Object m() {
        return this.X;
    }

    @DexIgnore
    public void m0(int i2) {
        if (i2 < 0) {
            this.R = 0;
        } else {
            this.R = i2;
        }
    }

    @DexIgnore
    public String n() {
        return this.Z;
    }

    @DexIgnore
    public void n0(int i2, int i3) {
        this.O = i2;
        this.P = i3;
    }

    @DexIgnore
    public Bi o(int i2) {
        if (i2 == 0) {
            return s();
        }
        if (i2 == 1) {
            return B();
        }
        return null;
    }

    @DexIgnore
    public void o0(int i2, int i3) {
        this.I = i2;
        this.J = i3;
    }

    @DexIgnore
    public int p() {
        return this.M + this.O;
    }

    @DexIgnore
    public void p0(Uj0 uj0) {
        this.D = uj0;
    }

    @DexIgnore
    public int q() {
        return this.N + this.P;
    }

    @DexIgnore
    public void q0(int i2, int i3) {
        if (i3 == 0) {
            this.K = i2;
        } else if (i3 == 1) {
            this.L = i2;
        }
    }

    @DexIgnore
    public int r() {
        if (this.Y == 8) {
            return 0;
        }
        return this.F;
    }

    @DexIgnore
    public void r0(float f2) {
        this.W = f2;
    }

    @DexIgnore
    public Bi s() {
        return this.C[0];
    }

    @DexIgnore
    public void s0(int i2) {
        this.f0 = i2;
    }

    @DexIgnore
    public int t(int i2) {
        if (i2 == 0) {
            return D();
        }
        if (i2 == 1) {
            return r();
        }
        return 0;
    }

    @DexIgnore
    public void t0(int i2, int i3) {
        this.J = i2;
        int i4 = i3 - i2;
        this.F = i4;
        int i5 = this.S;
        if (i4 < i5) {
            this.F = i5;
        }
    }

    @DexIgnore
    public String toString() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        if (this.a0 != null) {
            str = "type: " + this.a0 + " ";
        } else {
            str = "";
        }
        sb.append(str);
        if (this.Z != null) {
            str2 = "id: " + this.Z + " ";
        } else {
            str2 = "";
        }
        sb.append(str2);
        sb.append("(");
        sb.append(this.I);
        sb.append(", ");
        sb.append(this.J);
        sb.append(") - (");
        sb.append(this.E);
        sb.append(" x ");
        sb.append(this.F);
        sb.append(") wrap: (");
        sb.append(this.T);
        sb.append(" x ");
        sb.append(this.U);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public Uj0 u() {
        return this.D;
    }

    @DexIgnore
    public void u0(Bi bi) {
        this.C[1] = bi;
        if (bi == Bi.WRAP_CONTENT) {
            b0(this.U);
        }
    }

    @DexIgnore
    public int v(int i2) {
        if (i2 == 0) {
            return this.K;
        }
        if (i2 == 1) {
            return this.L;
        }
        return 0;
    }

    @DexIgnore
    public void v0(int i2, int i3, int i4, float f2) {
        this.f = i2;
        this.k = i3;
        this.l = i4;
        this.m = f2;
        if (f2 < 1.0f && i2 == 0) {
            this.f = 2;
        }
    }

    @DexIgnore
    public Bk0 w() {
        if (this.d == null) {
            this.d = new Bk0();
        }
        return this.d;
    }

    @DexIgnore
    public void w0(float f2) {
        this.g0[1] = f2;
    }

    @DexIgnore
    public Bk0 x() {
        if (this.c == null) {
            this.c = new Bk0();
        }
        return this.c;
    }

    @DexIgnore
    public void x0(int i2) {
        this.Y = i2;
    }

    @DexIgnore
    public int y() {
        return G() + this.E;
    }

    @DexIgnore
    public void y0(int i2) {
        this.E = i2;
        int i3 = this.R;
        if (i2 < i3) {
            this.E = i3;
        }
    }

    @DexIgnore
    public int z() {
        return this.I + this.O;
    }

    @DexIgnore
    public void z0(boolean z2) {
    }
}
