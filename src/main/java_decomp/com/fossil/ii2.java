package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ii2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ii2> CREATOR; // = new Hi2();
    @DexIgnore
    public static /* final */ Ii2 c; // = new Ii2("com.google.android.gms");
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Ii2(String str) {
        Rc2.k(str);
        this.b = str;
    }

    @DexIgnore
    public static Ii2 f(String str) {
        return "com.google.android.gms".equals(str) ? c : new Ii2(str);
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ii2)) {
            return false;
        }
        return this.b.equals(((Ii2) obj).b);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s}", this.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, this.b, false);
        Bd2.b(parcel, a2);
    }
}
