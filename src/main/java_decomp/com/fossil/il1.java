package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Il1 extends Gl1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Il1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Il1 a(byte[] bArr) {
            return new Il1(new String(Dm7.k(bArr, 0, bArr.length - 1), Et7.a));
        }

        @DexIgnore
        public Il1 b(Parcel parcel) {
            parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                return new Il1(readString);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Il1 createFromParcel(Parcel parcel) {
            return b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Il1[] newArray(int i) {
            return new Il1[i];
        }
    }

    @DexIgnore
    public Il1(String str) {
        super(R8.e);
        this.c = str;
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public byte[] b() {
        String str = this.c;
        Charset c2 = Hd0.y.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return Dm7.p(bytes, (byte) 0);
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Il1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.c, ((Il1) obj).c) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.MessageEntry");
    }

    @DexIgnore
    public final String getMessage() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Gl1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.k, this.c);
    }

    @DexIgnore
    @Override // com.fossil.Gl1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c);
        }
    }
}
