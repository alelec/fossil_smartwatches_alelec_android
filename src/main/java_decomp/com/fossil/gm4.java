package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gm4 implements Ql4 {
    @DexIgnore
    public static Bm4 b(Co4 co4) {
        int e = co4.e();
        int d = co4.d();
        Bm4 bm4 = new Bm4(e, d);
        bm4.e();
        for (int i = 0; i < e; i++) {
            for (int i2 = 0; i2 < d; i2++) {
                if (co4.b(i, i2) == 1) {
                    bm4.n(i, i2);
                }
            }
        }
        return bm4;
    }

    @DexIgnore
    public static Bm4 c(Lm4 lm4, Rm4 rm4) {
        int i;
        int i2;
        int h = rm4.h();
        int g = rm4.g();
        Co4 co4 = new Co4(rm4.j(), rm4.i());
        int i3 = 0;
        for (int i4 = 0; i4 < g; i4++) {
            if (i4 % rm4.e == 0) {
                int i5 = 0;
                for (int i6 = 0; i6 < rm4.j(); i6++) {
                    co4.g(i5, i3, i6 % 2 == 0);
                    i5++;
                }
                i = i3 + 1;
            } else {
                i = i3;
            }
            int i7 = 0;
            for (int i8 = 0; i8 < h; i8++) {
                if (i8 % rm4.d == 0) {
                    co4.g(i7, i, true);
                    i7++;
                }
                co4.g(i7, i, lm4.e(i8, i4));
                int i9 = i7 + 1;
                int i10 = rm4.d;
                if (i8 % i10 == i10 - 1) {
                    co4.g(i9, i, i4 % 2 == 0);
                    i7 = i9 + 1;
                } else {
                    i7 = i9;
                }
            }
            int i11 = i + 1;
            int i12 = rm4.e;
            if (i4 % i12 == i12 - 1) {
                int i13 = 0;
                for (int i14 = 0; i14 < rm4.j(); i14++) {
                    co4.g(i13, i11, true);
                    i13++;
                }
                i2 = i11 + 1;
            } else {
                i2 = i11;
            }
            i3 = i2;
        }
        return b(co4);
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) {
        Ll4 ll4;
        Ll4 ll42;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (kl4 != Kl4.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + kl4);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            Sm4 sm4 = Sm4.FORCE_NONE;
            if (map != null) {
                Sm4 sm42 = (Sm4) map.get(Ml4.DATA_MATRIX_SHAPE);
                if (sm42 != null) {
                    sm4 = sm42;
                }
                Ll4 ll43 = (Ll4) map.get(Ml4.MIN_SIZE);
                ll4 = ll43 != null ? ll43 : null;
                ll42 = (Ll4) map.get(Ml4.MAX_SIZE);
                if (ll42 == null) {
                    ll42 = null;
                }
            } else {
                ll4 = null;
                ll42 = null;
            }
            String b = Qm4.b(str, sm4, ll4, ll42);
            Rm4 l = Rm4.l(b.length(), sm4, ll4, ll42, true);
            Lm4 lm4 = new Lm4(Pm4.c(b, l), l.h(), l.g());
            lm4.h();
            return c(lm4, l);
        }
    }
}
