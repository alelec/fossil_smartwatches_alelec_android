package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hr2 extends Xq2 {
    @DexIgnore
    public J72<Ka3> b;

    @DexIgnore
    public Hr2(J72<Ka3> j72) {
        Rc2.b(j72 != null, "listener can't be null.");
        this.b = j72;
    }

    @DexIgnore
    @Override // com.fossil.Wq2
    public final void E0(Ka3 ka3) throws RemoteException {
        this.b.a(ka3);
        this.b = null;
    }
}
