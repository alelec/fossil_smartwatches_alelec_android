package com.fossil;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface O72 {
    @DexIgnore
    <T extends LifecycleCallback> T S2(String str, Class<T> cls);

    @DexIgnore
    Activity X2();

    @DexIgnore
    void b1(String str, LifecycleCallback lifecycleCallback);

    @DexIgnore
    void startActivityForResult(Intent intent, int i);
}
