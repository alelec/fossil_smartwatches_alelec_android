package com.fossil;

import com.mapped.An4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.util.DeviceUtils;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q37 implements MembersInjector<DeviceUtils> {
    @DexIgnore
    public static void a(DeviceUtils deviceUtils, DeviceRepository deviceRepository) {
        deviceUtils.a = deviceRepository;
    }

    @DexIgnore
    public static void b(DeviceUtils deviceUtils, FirmwareFileRepository firmwareFileRepository) {
        deviceUtils.e = firmwareFileRepository;
    }

    @DexIgnore
    public static void c(DeviceUtils deviceUtils, GuestApiService guestApiService) {
        deviceUtils.d = guestApiService;
    }

    @DexIgnore
    public static void d(DeviceUtils deviceUtils, An4 an4) {
        deviceUtils.b = an4;
    }

    @DexIgnore
    public static void e(DeviceUtils deviceUtils, UserRepository userRepository) {
        deviceUtils.c = userRepository;
    }
}
