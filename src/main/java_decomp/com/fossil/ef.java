package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Md0;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ef extends Zj {
    @DexIgnore
    public /* final */ Md0 T;

    @DexIgnore
    public Ef(K5 k5, I60 i60, Md0 md0) {
        super(k5, i60, Yp.k0, true, md0.a(), md0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = md0;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.z3, this.T.toJSONObject());
    }
}
