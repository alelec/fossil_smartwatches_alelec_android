package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hh2 implements DynamiteModule.b {
    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b
    public final DynamiteModule.b.a a(Context context, String str, DynamiteModule.b.b bVar) throws DynamiteModule.a {
        DynamiteModule.b.a aVar = new DynamiteModule.b.a();
        aVar.a = bVar.a(context, str);
        int b = bVar.b(context, str, true);
        aVar.b = b;
        if (aVar.a == 0 && b == 0) {
            aVar.c = 0;
        } else if (aVar.b >= aVar.a) {
            aVar.c = 1;
        } else {
            aVar.c = -1;
        }
        return aVar;
    }
}
