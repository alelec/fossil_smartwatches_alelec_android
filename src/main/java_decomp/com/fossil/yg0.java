package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yg0 extends Xg0 {
    @DexIgnore
    public /* final */ SeekBar d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public ColorStateList f; // = null;
    @DexIgnore
    public PorterDuff.Mode g; // = null;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public boolean i; // = false;

    @DexIgnore
    public Yg0(SeekBar seekBar) {
        super(seekBar);
        this.d = seekBar;
    }

    @DexIgnore
    @Override // com.fossil.Xg0
    public void c(AttributeSet attributeSet, int i2) {
        super.c(attributeSet, i2);
        Th0 v = Th0.v(this.d.getContext(), attributeSet, Ue0.AppCompatSeekBar, i2, 0);
        SeekBar seekBar = this.d;
        Mo0.j0(seekBar, seekBar.getContext(), Ue0.AppCompatSeekBar, attributeSet, v.r(), i2, 0);
        Drawable h2 = v.h(Ue0.AppCompatSeekBar_android_thumb);
        if (h2 != null) {
            this.d.setThumb(h2);
        }
        j(v.g(Ue0.AppCompatSeekBar_tickMark));
        if (v.s(Ue0.AppCompatSeekBar_tickMarkTintMode)) {
            this.g = Dh0.e(v.k(Ue0.AppCompatSeekBar_tickMarkTintMode, -1), this.g);
            this.i = true;
        }
        if (v.s(Ue0.AppCompatSeekBar_tickMarkTint)) {
            this.f = v.c(Ue0.AppCompatSeekBar_tickMarkTint);
            this.h = true;
        }
        v.w();
        f();
    }

    @DexIgnore
    public final void f() {
        if (this.e == null) {
            return;
        }
        if (this.h || this.i) {
            Drawable r = Am0.r(this.e.mutate());
            this.e = r;
            if (this.h) {
                Am0.o(r, this.f);
            }
            if (this.i) {
                Am0.p(this.e, this.g);
            }
            if (this.e.isStateful()) {
                this.e.setState(this.d.getDrawableState());
            }
        }
    }

    @DexIgnore
    public void g(Canvas canvas) {
        int max;
        int i2 = 1;
        if (this.e != null && (max = this.d.getMax()) > 1) {
            int intrinsicWidth = this.e.getIntrinsicWidth();
            int intrinsicHeight = this.e.getIntrinsicHeight();
            int i3 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
            if (intrinsicHeight >= 0) {
                i2 = intrinsicHeight / 2;
            }
            this.e.setBounds(-i3, -i2, i3, i2);
            float width = ((float) ((this.d.getWidth() - this.d.getPaddingLeft()) - this.d.getPaddingRight())) / ((float) max);
            int save = canvas.save();
            canvas.translate((float) this.d.getPaddingLeft(), (float) (this.d.getHeight() / 2));
            for (int i4 = 0; i4 <= max; i4++) {
                this.e.draw(canvas);
                canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            canvas.restoreToCount(save);
        }
    }

    @DexIgnore
    public void h() {
        Drawable drawable = this.e;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.d.getDrawableState())) {
            this.d.invalidateDrawable(drawable);
        }
    }

    @DexIgnore
    public void i() {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @DexIgnore
    public void j(Drawable drawable) {
        Drawable drawable2 = this.e;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.e = drawable;
        if (drawable != null) {
            drawable.setCallback(this.d);
            Am0.m(drawable, Mo0.z(this.d));
            if (drawable.isStateful()) {
                drawable.setState(this.d.getDrawableState());
            }
            f();
        }
        this.d.invalidate();
    }
}
