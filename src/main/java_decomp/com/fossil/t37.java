package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class T37 {
    @DexIgnore
    public static /* final */ J47 a; // = new J47("GLOBAL");

    @DexIgnore
    public static void a() {
        a.b();
    }

    @DexIgnore
    public static Xx0 b() {
        return a;
    }

    @DexIgnore
    public static void c() {
        a.c();
    }
}
