package com.fossil;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.S87;
import com.fossil.W67;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T67 extends R67 {
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public S87.Ci l;
    @DexIgnore
    public TextView m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Lc6<T67, ViewGroup.LayoutParams> a(WatchFaceEditorView watchFaceEditorView, S87.Ci ci, int i) {
            Wg6.c(watchFaceEditorView, "editorView");
            Wg6.c(ci, "textConfig");
            W87 b = ci.b();
            float b2 = b != null ? b.b() : 2.0f;
            TextView textView = new TextView(watchFaceEditorView.getContext());
            textView.setText(ci.i());
            textView.setTextColor(ci.g().getValue());
            textView.setTextSize(ci.h() / U67.b.a());
            textView.setTypeface(ci.j());
            textView.setSingleLine(true);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            Context context = watchFaceEditorView.getContext();
            Wg6.b(context, "editorView.context");
            T67 t67 = new T67(context, null);
            t67.C(textView, ci, layoutParams, i);
            t67.setScaleX(b2);
            t67.setScaleY(b2);
            t67.setElementEventHandler(watchFaceEditorView);
            return Hl7.a(t67, new FrameLayout.LayoutParams(-2, -2));
        }

        @DexIgnore
        public final Lc6<T67, ViewGroup.LayoutParams> b(Context context, S87.Ci ci) {
            Wg6.c(context, "context");
            Wg6.c(ci, "textConfig");
            TextView textView = new TextView(context);
            textView.setText(ci.i());
            textView.setTextColor(ci.g().getValue());
            textView.setTextSize(ci.h() / U67.b.a());
            textView.setTypeface(ci.j());
            textView.setSingleLine(true);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            T67 t67 = new T67(context, null);
            T67.D(t67, textView, ci, layoutParams, 0, 8, null);
            return Hl7.a(t67, new FrameLayout.LayoutParams(-2, -2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Gg6 b;

        @DexIgnore
        public Bi(Gg6 gg6) {
            this.b = gg6;
        }

        @DexIgnore
        public final void run() {
            Gg6 gg6 = this.b;
            if (gg6 != null) {
                Cd6 cd6 = (Cd6) gg6.invoke();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ T67 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Gg6<Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci) {
                super(0);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.mapped.Gg6
            public final void invoke() {
                this.this$0.b.m();
            }
        }

        @DexIgnore
        public Ci(T67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            if (this.b.A()) {
                this.b.y(new Aii(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ T67 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Gg6<Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di) {
                super(0);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.mapped.Gg6
            public final void invoke() {
                this.this$0.b.m();
            }
        }

        @DexIgnore
        public Di(T67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            if (this.b.A()) {
                this.b.y(new Aii(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ T67 b;

        @DexIgnore
        public Ei(T67 t67) {
            this.b = t67;
        }

        @DexIgnore
        public final void run() {
            this.b.m();
        }
    }

    @DexIgnore
    public T67(Context context) {
        super(context);
    }

    @DexIgnore
    public /* synthetic */ T67(Context context, Qg6 qg6) {
        this(context);
    }

    @DexIgnore
    public static /* synthetic */ T67 D(T67 t67, TextView textView, S87.Ci ci, ViewGroup.LayoutParams layoutParams, int i, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            i = 0;
        }
        t67.C(textView, ci, layoutParams, i);
        return t67;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.T67 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void z(T67 t67, Gg6 gg6, int i, Object obj) {
        if ((i & 1) != 0) {
            gg6 = null;
        }
        t67.y(gg6);
    }

    @DexIgnore
    public final boolean A() {
        float width = (float) getWidth();
        float scaleX = getScaleX();
        float height = (float) getHeight();
        float scaleX2 = getScaleX();
        W67.Ai handler = getHandler();
        return handler != null && W67.Ai.Aii.a(handler, width * scaleX, height * scaleX2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 4, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x007e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ad  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void B(java.lang.String r7, android.graphics.Typeface r8, com.fossil.O87 r9) {
        /*
            r6 = this;
            r2 = 1
            r3 = 0
            r1 = 0
            if (r7 == 0) goto L_0x0095
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = r0.i()
        L_0x000d:
            boolean r0 = com.mapped.Wg6.a(r0, r7)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x0095
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x0091
            r0.setText(r7)
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x0023
            r0.o(r7)
        L_0x0023:
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x008d
            com.fossil.T67$Ci r4 = new com.fossil.T67$Ci
            r4.<init>(r6)
            r0.post(r4)
            r5 = r2
        L_0x0030:
            if (r8 == 0) goto L_0x00a1
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x0097
            android.graphics.Typeface r0 = r0.j()
        L_0x003a:
            boolean r0 = com.mapped.Wg6.a(r0, r8)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x00a1
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x009d
            r0.setTypeface(r8)
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x0050
            r0.p(r8)
        L_0x0050:
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x0099
            com.fossil.T67$Di r4 = new com.fossil.T67$Di
            r4.<init>(r6)
            r0.post(r4)
            r4 = r2
        L_0x005d:
            if (r9 == 0) goto L_0x00ad
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x00a3
            com.fossil.O87 r0 = r0.g()
        L_0x0067:
            if (r0 == r9) goto L_0x00a9
            android.widget.TextView r0 = r6.m
            if (r0 == 0) goto L_0x00a5
            int r1 = r9.getValue()
            r0.setTextColor(r1)
            com.fossil.S87$Ci r0 = r6.l
            if (r0 == 0) goto L_0x00ab
            r0.k(r9)
            r0 = r2
        L_0x007c:
            if (r5 != 0) goto L_0x0082
            if (r4 != 0) goto L_0x0082
            if (r0 == 0) goto L_0x008a
        L_0x0082:
            com.fossil.T67$Ei r0 = new com.fossil.T67$Ei
            r0.<init>(r6)
            r6.post(r0)
        L_0x008a:
            return
        L_0x008b:
            r0 = r1
            goto L_0x000d
        L_0x008d:
            com.mapped.Wg6.i()
            throw r1
        L_0x0091:
            com.mapped.Wg6.i()
            throw r1
        L_0x0095:
            r5 = r3
            goto L_0x0030
        L_0x0097:
            r0 = r1
            goto L_0x003a
        L_0x0099:
            com.mapped.Wg6.i()
            throw r1
        L_0x009d:
            com.mapped.Wg6.i()
            throw r1
        L_0x00a1:
            r4 = r3
            goto L_0x005d
        L_0x00a3:
            r0 = r1
            goto L_0x0067
        L_0x00a5:
            com.mapped.Wg6.i()
            throw r1
        L_0x00a9:
            r0 = r3
            goto L_0x007c
        L_0x00ab:
            r0 = r2
            goto L_0x007c
        L_0x00ad:
            r0 = r3
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.T67.B(java.lang.String, android.graphics.Typeface, com.fossil.O87):void");
    }

    @DexIgnore
    public final T67 C(TextView textView, S87.Ci ci, ViewGroup.LayoutParams layoutParams, int i) {
        addView(textView, layoutParams);
        this.m = textView;
        ci.m(i);
        ci.n(i);
        this.l = ci;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public S87 c(boolean z) {
        int i = 0;
        S87.Ci ci = this.l;
        if (ci != null) {
            ci.d(getMetric());
        }
        if (z) {
            W67.Ai handler = getHandler();
            int c = handler != null ? handler.c() : 0;
            S87.Ci ci2 = this.l;
            if (ci2 != null) {
                if (ci2 != null) {
                    i = ci2.a();
                }
                ci2.n(i);
            }
            S87.Ci ci3 = this.l;
            if (ci3 != null) {
                ci3.m(c);
            }
        }
        S87.Ci ci4 = this.l;
        if (ci4 != null) {
            return ci4;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public W67.Ci getType() {
        return W67.Ci.TEXT;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public void o() {
        W67.Ai handler;
        if (getTouchCount$app_fossilRelease() > 1 && (handler = getHandler()) != null) {
            handler.m(this);
        }
    }

    @DexIgnore
    public final void y(Gg6<Cd6> gg6) {
        W67.Ai handler = getHandler();
        WatchFaceEditorView.a g = handler != null ? handler.g() : null;
        if (g != null) {
            float a2 = g.a();
            float max = (float) Math.max(getWidth(), getHeight());
            float scaleX = getScaleX();
            TextView textView = this.m;
            if (textView != null) {
                float textSize = ((a2 * 0.9f) / (max * scaleX)) * textView.getTextSize();
                TextView textView2 = this.m;
                if (textView2 != null) {
                    textView2.setTextSize(textSize / U67.b.a());
                    S87.Ci ci = this.l;
                    if (ci != null) {
                        ci.l(textSize);
                        post(new Bi(gg6));
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }
}
