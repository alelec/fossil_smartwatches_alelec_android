package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K28 {
    @DexIgnore
    public /* final */ List<G18> a;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public K28(List<G18> list) {
        this.a = list;
    }

    @DexIgnore
    public G18 a(SSLSocket sSLSocket) throws IOException {
        G18 g18;
        int i = this.b;
        int size = this.a.size();
        int i2 = i;
        while (true) {
            if (i2 >= size) {
                g18 = null;
                break;
            }
            g18 = this.a.get(i2);
            if (g18.c(sSLSocket)) {
                this.b = i2 + 1;
                break;
            }
            i2++;
        }
        if (g18 != null) {
            this.c = c(sSLSocket);
            Z18.a.c(g18, sSLSocket, this.d);
            return g18;
        }
        throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
    }

    @DexIgnore
    public boolean b(IOException iOException) {
        boolean z = true;
        this.d = true;
        if (!this.c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        boolean z2 = iOException instanceof SSLHandshakeException;
        if ((z2 && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if (!z2 && !(iOException instanceof SSLProtocolException) && !(iOException instanceof SSLException)) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public final boolean c(SSLSocket sSLSocket) {
        for (int i = this.b; i < this.a.size(); i++) {
            if (this.a.get(i).c(sSLSocket)) {
                return true;
            }
        }
        return false;
    }
}
