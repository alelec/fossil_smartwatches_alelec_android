package com.fossil;

import com.fossil.C44;
import com.fossil.X44;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends I44<C44.Ai<?>> {
        @DexIgnore
        public int a(C44.Ai<?> ai, C44.Ai<?> ai2) {
            return V54.a(ai2.getCount(), ai.getCount());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.I44, java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(C44.Ai<?> ai, C44.Ai<?> ai2) {
            return a(ai, ai2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<E> implements C44.Ai<E> {
        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof C44.Ai)) {
                return false;
            }
            C44.Ai ai = (C44.Ai) obj;
            return getCount() == ai.getCount() && F14.a(getElement(), ai.getElement());
        }

        @DexIgnore
        public int hashCode() {
            E element = getElement();
            return (element == null ? 0 : element.hashCode()) ^ getCount();
        }

        @DexIgnore
        public String toString() {
            String valueOf = String.valueOf(getElement());
            int count = getCount();
            if (count == 1) {
                return valueOf;
            }
            return valueOf + " x " + count;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<E> extends X44.Ai<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends G54<C44.Ai<E>, E> {
            @DexIgnore
            public Aii(Ci ci, Iterator it) {
                super(it);
            }

            @DexIgnore
            @Override // com.fossil.G54
            public /* bridge */ /* synthetic */ Object a(Object obj) {
                return b((C44.Ai) obj);
            }

            @DexIgnore
            public E b(C44.Ai<E> ai) {
                return ai.getElement();
            }
        }

        @DexIgnore
        public abstract C44<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().containsAll(collection);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<E> iterator() {
            return new Aii(this, a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return a().remove(obj, Integer.MAX_VALUE) > 0;
        }

        @DexIgnore
        public int size() {
            return a().entrySet().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di<E> extends X44.Ai<C44.Ai<E>> {
        @DexIgnore
        public abstract C44<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof C44.Ai)) {
                return false;
            }
            C44.Ai ai = (C44.Ai) obj;
            return ai.getCount() > 0 && a().count(ai.getElement()) == ai.getCount();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof C44.Ai)) {
                return false;
            }
            C44.Ai ai = (C44.Ai) obj;
            E e = (E) ai.getElement();
            int count = ai.getCount();
            if (count != 0) {
                return a().setCount(e, count, 0);
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<E> extends Bi<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int count;
        @DexIgnore
        public /* final */ E element;

        @DexIgnore
        public Ei(E e, int i) {
            this.element = e;
            this.count = i;
            A24.b(i, "count");
        }

        @DexIgnore
        @Override // com.fossil.C44.Ai
        public final int getCount() {
            return this.count;
        }

        @DexIgnore
        @Override // com.fossil.C44.Ai
        public final E getElement() {
            return this.element;
        }

        @DexIgnore
        public Ei<E> nextInBucket() {
            return null;
        }
    }

    /*
    static {
        new Ai();
    }
    */

    @DexIgnore
    public static <E> boolean a(C44<E> c44, Collection<? extends E> collection) {
        if (collection.isEmpty()) {
            return false;
        }
        if (collection instanceof C44) {
            for (C44.Ai<E> ai : b(collection).entrySet()) {
                c44.add(ai.getElement(), ai.getCount());
            }
        } else {
            P34.a(c44, collection.iterator());
        }
        return true;
    }

    @DexIgnore
    public static <T> C44<T> b(Iterable<T> iterable) {
        return (C44) iterable;
    }

    @DexIgnore
    public static boolean c(C44<?> c44, Object obj) {
        if (obj == c44) {
            return true;
        }
        if (obj instanceof C44) {
            C44 c442 = (C44) obj;
            if (c44.size() == c442.size() && c44.entrySet().size() == c442.entrySet().size()) {
                for (C44.Ai ai : c442.entrySet()) {
                    if (c44.count(ai.getElement()) != ai.getCount()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <E> C44.Ai<E> d(E e, int i) {
        return new Ei(e, i);
    }

    @DexIgnore
    public static int e(Iterable<?> iterable) {
        if (iterable instanceof C44) {
            return ((C44) iterable).elementSet().size();
        }
        return 11;
    }

    @DexIgnore
    public static boolean f(C44<?> c44, Collection<?> collection) {
        if (collection instanceof C44) {
            collection = ((C44) collection).elementSet();
        }
        return c44.elementSet().removeAll(collection);
    }

    @DexIgnore
    public static boolean g(C44<?> c44, Collection<?> collection) {
        I14.l(collection);
        if (collection instanceof C44) {
            collection = ((C44) collection).elementSet();
        }
        return c44.elementSet().retainAll(collection);
    }

    @DexIgnore
    public static <E> int h(C44<E> c44, E e, int i) {
        A24.b(i, "count");
        int count = c44.count(e);
        int i2 = i - count;
        if (i2 > 0) {
            c44.add(e, i2);
        } else if (i2 < 0) {
            c44.remove(e, -i2);
        }
        return count;
    }

    @DexIgnore
    public static <E> boolean i(C44<E> c44, E e, int i, int i2) {
        A24.b(i, "oldCount");
        A24.b(i2, "newCount");
        if (c44.count(e) != i) {
            return false;
        }
        c44.setCount(e, i2);
        return true;
    }

    @DexIgnore
    public static int j(C44<?> c44) {
        long j = 0;
        for (C44.Ai<?> ai : c44.entrySet()) {
            j = ((long) ai.getCount()) + j;
        }
        return V54.b(j);
    }
}
