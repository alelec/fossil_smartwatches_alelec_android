package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xv4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public Zy4 b; // = new Zy4(1);
    @DexIgnore
    public Wv4 c; // = new Wv4(2);

    @DexIgnore
    public final void g(List<? extends Object> list) {
        Wg6.c(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.a, i)) {
            return 1;
        }
        if (this.c.a(this.a, i)) {
            return 2;
        }
        throw new IllegalArgumentException("wrong type");
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            this.b.c(this.a, i, viewHolder);
        } else if (itemViewType == 2) {
            this.c.b(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == 1) {
            return this.b.d(viewGroup);
        }
        if (i == 2) {
            return this.c.c(viewGroup);
        }
        throw new IllegalArgumentException("wrong type: " + i);
    }
}
