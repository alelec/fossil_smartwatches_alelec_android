package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A95 extends Z85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J;
    @DexIgnore
    public long H;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        J = sparseIntArray;
        sparseIntArray.put(2131362666, 1);
        J.put(2131362546, 2);
        J.put(2131362694, 3);
        J.put(2131362371, 4);
        J.put(2131363028, 5);
        J.put(2131362106, 6);
        J.put(2131362525, 7);
        J.put(2131362815, 8);
        J.put(2131362486, 9);
        J.put(2131363459, 10);
        J.put(2131362814, 11);
        J.put(2131362485, 12);
        J.put(2131363460, 13);
        J.put(2131362807, 14);
        J.put(2131362431, 15);
        J.put(2131363461, 16);
    }
    */

    @DexIgnore
    public A95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 17, I, J));
    }

    @DexIgnore
    public A95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[6], (FlexibleTextView) objArr[4], (ImageView) objArr[15], (ImageView) objArr[12], (ImageView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[2], (ImageView) objArr[1], (ImageView) objArr[3], (LinearLayout) objArr[14], (LinearLayout) objArr[11], (LinearLayout) objArr[8], (ConstraintLayout) objArr[0], (RecyclerView) objArr[5], (View) objArr[10], (View) objArr[13], (View) objArr[16]);
        this.H = -1;
        this.C.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.H != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.H = 1;
        }
        w();
    }
}
