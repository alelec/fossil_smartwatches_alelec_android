package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Cg0;
import com.fossil.Ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uh0 implements Ch0 {
    @DexIgnore
    public Toolbar a;
    @DexIgnore
    public int b;
    @DexIgnore
    public View c;
    @DexIgnore
    public View d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public CharSequence i;
    @DexIgnore
    public CharSequence j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public Window.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public Rg0 n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public Drawable q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ Wf0 b; // = new Wf0(Uh0.this.a.getContext(), 0, 16908332, 0, 0, Uh0.this.i);

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onClick(View view) {
            Uh0 uh0 = Uh0.this;
            Window.Callback callback = uh0.l;
            if (callback != null && uh0.m) {
                callback.onMenuItemSelected(0, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends To0 {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Bi(int i) {
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.So0, com.fossil.To0
        public void a(View view) {
            this.a = true;
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void b(View view) {
            if (!this.a) {
                Uh0.this.a.setVisibility(this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.So0, com.fossil.To0
        public void c(View view) {
            Uh0.this.a.setVisibility(0);
        }
    }

    @DexIgnore
    public Uh0(Toolbar toolbar, boolean z) {
        this(toolbar, z, Se0.abc_action_bar_up_description, Pe0.abc_ic_ab_back_material);
    }

    @DexIgnore
    public Uh0(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        Th0 v = Th0.v(toolbar.getContext(), null, Ue0.ActionBar, Le0.actionBarStyle, 0);
        this.q = v.g(Ue0.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence p2 = v.p(Ue0.ActionBar_title);
            if (!TextUtils.isEmpty(p2)) {
                setTitle(p2);
            }
            CharSequence p3 = v.p(Ue0.ActionBar_subtitle);
            if (!TextUtils.isEmpty(p3)) {
                D(p3);
            }
            Drawable g2 = v.g(Ue0.ActionBar_logo);
            if (g2 != null) {
                z(g2);
            }
            Drawable g3 = v.g(Ue0.ActionBar_icon);
            if (g3 != null) {
                setIcon(g3);
            }
            if (this.g == null && (drawable = this.q) != null) {
                C(drawable);
            }
            k(v.k(Ue0.ActionBar_displayOptions, 0));
            int n2 = v.n(Ue0.ActionBar_customNavigationLayout, 0);
            if (n2 != 0) {
                x(LayoutInflater.from(this.a.getContext()).inflate(n2, (ViewGroup) this.a, false));
                k(this.b | 16);
            }
            int m2 = v.m(Ue0.ActionBar_height, 0);
            if (m2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = m2;
                this.a.setLayoutParams(layoutParams);
            }
            int e2 = v.e(Ue0.ActionBar_contentInsetStart, -1);
            int e3 = v.e(Ue0.ActionBar_contentInsetEnd, -1);
            if (e2 >= 0 || e3 >= 0) {
                this.a.I(Math.max(e2, 0), Math.max(e3, 0));
            }
            int n3 = v.n(Ue0.ActionBar_titleTextStyle, 0);
            if (n3 != 0) {
                Toolbar toolbar2 = this.a;
                toolbar2.N(toolbar2.getContext(), n3);
            }
            int n4 = v.n(Ue0.ActionBar_subtitleTextStyle, 0);
            if (n4 != 0) {
                Toolbar toolbar3 = this.a;
                toolbar3.L(toolbar3.getContext(), n4);
            }
            int n5 = v.n(Ue0.ActionBar_popupTheme, 0);
            if (n5 != 0) {
                this.a.setPopupTheme(n5);
            }
        } else {
            this.b = w();
        }
        v.w();
        y(i2);
        this.k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener(new Ai());
    }

    @DexIgnore
    public void A(int i2) {
        B(i2 == 0 ? null : getContext().getString(i2));
    }

    @DexIgnore
    public void B(CharSequence charSequence) {
        this.k = charSequence;
        F();
    }

    @DexIgnore
    public void C(Drawable drawable) {
        this.g = drawable;
        G();
    }

    @DexIgnore
    public void D(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setSubtitle(charSequence);
        }
    }

    @DexIgnore
    public final void E(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setTitle(charSequence);
        }
    }

    @DexIgnore
    public final void F() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.a.setNavigationContentDescription(this.p);
        } else {
            this.a.setNavigationContentDescription(this.k);
        }
    }

    @DexIgnore
    public final void G() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.a.setNavigationIcon((Drawable) null);
    }

    @DexIgnore
    public final void H() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.a.setLogo(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void a(Menu menu, Ig0.Ai ai) {
        if (this.n == null) {
            Rg0 rg0 = new Rg0(this.a.getContext());
            this.n = rg0;
            rg0.s(Qe0.action_menu_presenter);
        }
        this.n.g(ai);
        this.a.J((Cg0) menu, this.n);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean b() {
        return this.a.A();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void c() {
        this.m = true;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void collapseActionView() {
        this.a.e();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean d() {
        return this.a.d();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean e() {
        return this.a.z();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean f() {
        return this.a.w();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean g() {
        return this.a.Q();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public Context getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public CharSequence getTitle() {
        return this.a.getTitle();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void h() {
        this.a.f();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void i(Mh0 mh0) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.a)) {
            toolbar.removeView(this.c);
        }
        this.c = mh0;
        if (mh0 != null && this.o == 2) {
            this.a.addView(mh0, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            ((ViewGroup.MarginLayoutParams) layoutParams).width = -2;
            ((ViewGroup.MarginLayoutParams) layoutParams).height = -2;
            layoutParams.a = 8388691;
            mh0.setAllowCollapse(true);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public boolean j() {
        return this.a.v();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void k(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    F();
                }
                G();
            }
            if ((i3 & 3) != 0) {
                H();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.a.setTitle(this.i);
                    this.a.setSubtitle(this.j);
                } else {
                    this.a.setTitle((CharSequence) null);
                    this.a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.a.addView(view);
                } else {
                    this.a.removeView(view);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public Menu l() {
        return this.a.getMenu();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void m(int i2) {
        z(i2 != 0 ? Gf0.d(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public int n() {
        return this.o;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public Ro0 o(int i2, long j2) {
        Ro0 c2 = Mo0.c(this.a);
        c2.a(i2 == 0 ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        c2.d(j2);
        c2.f(new Bi(i2));
        return c2;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void p(Ig0.Ai ai, Cg0.Ai ai2) {
        this.a.K(ai, ai2);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public ViewGroup q() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void r(boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public int s() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setIcon(int i2) {
        setIcon(i2 != 0 ? Gf0.d(getContext(), i2) : null);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setIcon(Drawable drawable) {
        this.e = drawable;
        H();
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setTitle(CharSequence charSequence) {
        this.h = true;
        E(charSequence);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setVisibility(int i2) {
        this.a.setVisibility(i2);
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            E(charSequence);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void t() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void u() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    @Override // com.fossil.Ch0
    public void v(boolean z) {
        this.a.setCollapsible(z);
    }

    @DexIgnore
    public final int w() {
        if (this.a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.a.getNavigationIcon();
        return 15;
    }

    @DexIgnore
    public void x(View view) {
        View view2 = this.d;
        if (!(view2 == null || (this.b & 16) == 0)) {
            this.a.removeView(view2);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.a.addView(view);
        }
    }

    @DexIgnore
    public void y(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                A(this.p);
            }
        }
    }

    @DexIgnore
    public void z(Drawable drawable) {
        this.f = drawable;
        H();
    }
}
