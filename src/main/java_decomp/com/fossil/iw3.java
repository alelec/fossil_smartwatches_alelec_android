package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iw3 {
    @DexIgnore
    public static /* final */ int design_appbar_state_list_animator; // = 2130837504;
    @DexIgnore
    public static /* final */ int design_fab_hide_motion_spec; // = 2130837505;
    @DexIgnore
    public static /* final */ int design_fab_show_motion_spec; // = 2130837506;
    @DexIgnore
    public static /* final */ int mtrl_btn_state_list_anim; // = 2130837507;
    @DexIgnore
    public static /* final */ int mtrl_btn_unelevated_state_list_anim; // = 2130837508;
    @DexIgnore
    public static /* final */ int mtrl_card_state_list_anim; // = 2130837509;
    @DexIgnore
    public static /* final */ int mtrl_chip_state_list_anim; // = 2130837510;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_change_size_motion_spec; // = 2130837511;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_hide_motion_spec; // = 2130837512;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_show_motion_spec; // = 2130837513;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_state_list_animator; // = 2130837514;
    @DexIgnore
    public static /* final */ int mtrl_fab_hide_motion_spec; // = 2130837515;
    @DexIgnore
    public static /* final */ int mtrl_fab_show_motion_spec; // = 2130837516;
    @DexIgnore
    public static /* final */ int mtrl_fab_transformation_sheet_collapse_spec; // = 2130837517;
    @DexIgnore
    public static /* final */ int mtrl_fab_transformation_sheet_expand_spec; // = 2130837518;
}
