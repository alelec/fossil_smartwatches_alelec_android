package com.fossil;

import android.content.Context;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wk5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static String b;
    @DexIgnore
    public static /* final */ Wk5 c; // = new Wk5();

    /*
    static {
        String simpleName = Wk5.class.getSimpleName();
        Wg6.b(simpleName, "InstallationUUID::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(Context context) {
        String str;
        synchronized (this) {
            Wg6.c(context, "context");
            if (b == null) {
                File file = new File(context.getFilesDir(), "INSTALLATION");
                try {
                    if (!file.exists()) {
                        c(file);
                    }
                    b = b(file);
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = a;
                    local.d(str2, ".id(), error=" + e);
                }
            }
            str = b;
            if (str == null) {
                str = "";
            }
        }
        return str;
    }

    @DexIgnore
    public final String b(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        byte[] bArr = new byte[((int) randomAccessFile.length())];
        randomAccessFile.readFully(bArr);
        randomAccessFile.close();
        return new String(bArr, Et7.a);
    }

    @DexIgnore
    public final void c(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        String uuid = UUID.randomUUID().toString();
        Wg6.b(uuid, "UUID.randomUUID().toString()");
        Charset charset = Et7.a;
        if (uuid != null) {
            byte[] bytes = uuid.getBytes(charset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            fileOutputStream.write(bytes);
            fileOutputStream.close();
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }
}
