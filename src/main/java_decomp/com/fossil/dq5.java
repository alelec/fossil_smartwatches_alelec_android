package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dq5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public vr5 f821a;
    @DexIgnore
    public on5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $notificationInfo;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dr7 dr7, qn7 qn7) {
            super(2, qn7);
            this.$notificationInfo = dr7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$notificationInfo, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.h0.c());
                if (defaultSmsPackage == null || e47.b.i(defaultSmsPackage)) {
                    FLogger.INSTANCE.getLocal().d(dq5.c, "onReceive() - SMS app filter is assigned - ignore");
                } else {
                    en5.i.a().h(this.$notificationInfo.element);
                    FLogger.INSTANCE.getLocal().d(dq5.c, "onReceive) - SMS app filter is not assigned - add to Queue");
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = dq5.class.getSimpleName();
        pq7.b(simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        dm5 h;
        pq7.c(context, "context");
        pq7.c(intent, "intent");
        dr7 dr7 = new dr7();
        dr7.element = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        StringBuilder sb = new StringBuilder();
        sb.append("SmsMmsReceiver : ");
        String action = intent.getAction();
        if (action != null) {
            sb.append(action);
            local.d(str, sb.toString());
            on5 on5 = this.b;
            if (on5 == null) {
                pq7.n("mSharePref");
                throw null;
            } else if (on5.q0()) {
                FLogger.INSTANCE.getLocal().d(c, "SmsMmsReceiver user new solution");
            } else {
                String str2 = "";
                if (TextUtils.isEmpty(intent.getAction()) || !pq7.a(intent.getAction(), "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
                    try {
                        SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        if (messagesFromIntent != null) {
                            if (!(messagesFromIntent.length == 0)) {
                                for (SmsMessage smsMessage : messagesFromIntent) {
                                    pq7.b(smsMessage, "message");
                                    String messageBody = smsMessage.getMessageBody();
                                    String originatingAddress = smsMessage.getOriginatingAddress();
                                    dr7.element = (T) new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, "");
                                    if (!(TextUtils.isEmpty(messageBody) || TextUtils.isEmpty(originatingAddress))) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
                    }
                } else {
                    try {
                        if (intent.hasExtra("data") && (h = new km5(intent.getByteArrayExtra("data")).h()) != null && h.a() != null) {
                            cm5 a2 = h.a();
                            pq7.b(a2, "pdu.from");
                            String d = a2.d();
                            FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + d);
                            dr7.element = (T) new NotificationInfo(NotificationSource.TEXT, d, "", "");
                        }
                    } catch (Exception e2) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
                    }
                }
                if (dr7.element != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onReceive() - SMSMMS - sender info ");
                    String senderInfo = dr7.element.getSenderInfo();
                    if (senderInfo != null) {
                        sb2.append(senderInfo);
                        local2.d(str3, sb2.toString());
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.h0.c().J())) {
                            on5 on52 = this.b;
                            if (on52 == null) {
                                pq7.n("mSharePref");
                                throw null;
                            } else if (on52.Z()) {
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
                            } else {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str4 = c;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("onReceive() - SMS-MMS - sender info ");
                                String senderInfo2 = dr7.element.getSenderInfo();
                                if (senderInfo2 != null) {
                                    sb3.append(senderInfo2);
                                    local3.d(str4, sb3.toString());
                                    xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(dr7, null), 3, null);
                                    return;
                                }
                                pq7.i();
                                throw null;
                            }
                        } else {
                            if (dr7.element.getSenderInfo() != null) {
                                str2 = dr7.element.getSenderInfo();
                            }
                            vr5 vr5 = this.f821a;
                            if (vr5 == null) {
                                pq7.n("mDianaNotificationComponent");
                                throw null;
                            } else if (str2 != null) {
                                String body = dr7.element.getBody();
                                pq7.b(body, "notificationInfo.body");
                                Calendar instance = Calendar.getInstance();
                                pq7.b(instance, "Calendar.getInstance()");
                                vr5.V(str2, body, instance.getTimeInMillis());
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        } else {
            pq7.i();
            throw null;
        }
    }
}
