package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.Deque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nm2 {
    /*
    static {
        new Mm2();
    }
    */

    @DexIgnore
    public static InputStream a(InputStream inputStream, long j) {
        return new Pm2(inputStream, 1048577);
    }

    @DexIgnore
    public static byte[] b(InputStream inputStream) throws IOException {
        Km2.a(inputStream);
        ArrayDeque arrayDeque = new ArrayDeque(20);
        int i = 0;
        int i2 = 8192;
        while (i < 2147483639) {
            int min = Math.min(i2, 2147483639 - i);
            byte[] bArr = new byte[min];
            arrayDeque.add(bArr);
            int i3 = 0;
            while (i3 < min) {
                int read = inputStream.read(bArr, i3, min - i3);
                if (read == -1) {
                    return c(arrayDeque, i);
                }
                i3 += read;
                i += read;
            }
            i2 = Rm2.a(i2, 2);
        }
        if (inputStream.read() == -1) {
            return c(arrayDeque, 2147483639);
        }
        throw new OutOfMemoryError("input is too large to fit in a byte array");
    }

    @DexIgnore
    public static byte[] c(Deque<byte[]> deque, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        while (i2 > 0) {
            byte[] removeFirst = deque.removeFirst();
            int min = Math.min(i2, removeFirst.length);
            System.arraycopy(removeFirst, 0, bArr, i - i2, min);
            i2 -= min;
        }
        return bArr;
    }
}
