package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N68 {
    @DexIgnore
    public static /* final */ ByteArrayBuffer e; // = c(P68.a, ": ");
    @DexIgnore
    public static /* final */ ByteArrayBuffer f; // = c(P68.a, "\r\n");
    @DexIgnore
    public static /* final */ ByteArrayBuffer g; // = c(P68.a, "--");
    @DexIgnore
    public /* final */ Charset a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ List<L68> c;
    @DexIgnore
    public /* final */ O68 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[O68.values().length];
            a = iArr;
            try {
                iArr[O68.STRICT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[O68.BROWSER_COMPATIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public N68(String str, Charset charset, String str2, O68 o68) {
        if (str == null) {
            throw new IllegalArgumentException("Multipart subtype may not be null");
        } else if (str2 != null) {
            this.a = charset == null ? P68.a : charset;
            this.b = str2;
            this.c = new ArrayList();
            this.d = o68;
        } else {
            throw new IllegalArgumentException("Multipart boundary may not be null");
        }
    }

    @DexIgnore
    public static ByteArrayBuffer c(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(encode.remaining());
        byteArrayBuffer.append(encode.array(), encode.position(), encode.remaining());
        return byteArrayBuffer;
    }

    @DexIgnore
    public static void g(String str, OutputStream outputStream) throws IOException {
        i(c(P68.a, str), outputStream);
    }

    @DexIgnore
    public static void h(String str, Charset charset, OutputStream outputStream) throws IOException {
        i(c(charset, str), outputStream);
    }

    @DexIgnore
    public static void i(ByteArrayBuffer byteArrayBuffer, OutputStream outputStream) throws IOException {
        outputStream.write(byteArrayBuffer.buffer(), 0, byteArrayBuffer.length());
    }

    @DexIgnore
    public static void j(Q68 q68, OutputStream outputStream) throws IOException {
        g(q68.b(), outputStream);
        i(e, outputStream);
        g(q68.a(), outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public static void k(Q68 q68, Charset charset, OutputStream outputStream) throws IOException {
        h(q68.b(), charset, outputStream);
        i(e, outputStream);
        h(q68.a(), charset, outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public void a(L68 l68) {
        if (l68 != null) {
            this.c.add(l68);
        }
    }

    @DexIgnore
    public final void b(O68 o68, OutputStream outputStream, boolean z) throws IOException {
        ByteArrayBuffer c2 = c(this.a, e());
        for (L68 l68 : this.c) {
            i(g, outputStream);
            i(c2, outputStream);
            i(f, outputStream);
            M68 f2 = l68.f();
            int i = Ai.a[o68.ordinal()];
            if (i == 1) {
                Iterator<Q68> it = f2.iterator();
                while (it.hasNext()) {
                    j(it.next(), outputStream);
                }
            } else if (i == 2) {
                k(l68.f().b("Content-Disposition"), this.a, outputStream);
                if (l68.e().d() != null) {
                    k(l68.f().b("Content-Type"), this.a, outputStream);
                }
            }
            i(f, outputStream);
            if (z) {
                l68.e().writeTo(outputStream);
            }
            i(f, outputStream);
        }
        i(g, outputStream);
        i(c2, outputStream);
        i(g, outputStream);
        i(f, outputStream);
    }

    @DexIgnore
    public List<L68> d() {
        return this.c;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public long f() {
        long j = 0;
        for (L68 l68 : this.c) {
            long contentLength = l68.e().getContentLength();
            if (contentLength < 0) {
                return -1;
            }
            j = contentLength + j;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            b(this.d, byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e2) {
            return -1;
        }
    }

    @DexIgnore
    public void l(OutputStream outputStream) throws IOException {
        b(this.d, outputStream, true);
    }
}
