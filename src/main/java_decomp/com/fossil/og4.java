package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Og4 {
    @DexIgnore
    public /* final */ FileChannel a;
    @DexIgnore
    public /* final */ FileLock b;

    @DexIgnore
    public Og4(FileChannel fileChannel, FileLock fileLock) {
        this.a = fileChannel;
        this.b = fileLock;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002b A[SYNTHETIC, Splitter:B:12:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030 A[SYNTHETIC, Splitter:B:15:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Og4 a(android.content.Context r6, java.lang.String r7) {
        /*
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            java.io.File r1 = r6.getFilesDir()     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            r0.<init>(r1, r7)     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            java.lang.String r3 = "rw"
            r1.<init>(r0, r3)     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            java.nio.channels.FileChannel r1 = r1.getChannel()     // Catch:{ IOException -> 0x003d, Error -> 0x0046 }
            java.nio.channels.FileLock r0 = r1.lock()     // Catch:{ IOException -> 0x0039, Error -> 0x0042 }
            com.fossil.Og4 r3 = new com.fossil.Og4     // Catch:{ IOException -> 0x0036, Error -> 0x0020 }
            r3.<init>(r1, r0)     // Catch:{ IOException -> 0x0036, Error -> 0x0020 }
            r2 = r3
        L_0x001f:
            return r2
        L_0x0020:
            r4 = move-exception
            r3 = r0
        L_0x0022:
            java.lang.String r0 = "CrossProcessLock"
            java.lang.String r5 = "encountered error while creating and acquiring the lock, ignoring"
            android.util.Log.e(r0, r5, r4)
            if (r3 == 0) goto L_0x002e
            r3.release()     // Catch:{ IOException -> 0x0044 }
        L_0x002e:
            if (r1 == 0) goto L_0x001f
            r1.close()     // Catch:{ IOException -> 0x0034 }
            goto L_0x001f
        L_0x0034:
            r0 = move-exception
            goto L_0x001f
        L_0x0036:
            r4 = move-exception
            r3 = r0
            goto L_0x0022
        L_0x0039:
            r0 = move-exception
        L_0x003a:
            r3 = r2
            r4 = r0
            goto L_0x0022
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            r1 = r2
            r3 = r2
            r4 = r0
            goto L_0x0022
        L_0x0042:
            r0 = move-exception
            goto L_0x003a
        L_0x0044:
            r0 = move-exception
            goto L_0x002e
        L_0x0046:
            r0 = move-exception
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Og4.a(android.content.Context, java.lang.String):com.fossil.Og4");
    }

    @DexIgnore
    public void b() {
        try {
            this.b.release();
            this.a.close();
        } catch (IOException e) {
            Log.e("CrossProcessLock", "encountered error while releasing, ignoring", e);
        }
    }
}
