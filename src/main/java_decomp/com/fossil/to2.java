package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class To2 implements Qh2 {
    @DexIgnore
    @Override // com.fossil.Qh2
    public final T62<Status> a(R62 r62, DataSet dataSet) {
        Rc2.l(dataSet, "Must set the data set");
        Rc2.o(!dataSet.k().isEmpty(), "Cannot use an empty data set");
        Rc2.l(dataSet.A().p0(), "Must set the app package name for the data source");
        return r62.i(new So2(this, r62, dataSet, false));
    }
}
