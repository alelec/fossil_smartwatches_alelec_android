package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu4 extends ts0 {
    @DexIgnore
    public static /* final */ String t;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<ts4> f2420a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<gs4>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<a> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<Boolean, ServerError, ts4>> m; // = new MutableLiveData<>();
    @DexIgnore
    public ts4 n;
    @DexIgnore
    public ts4 o;
    @DexIgnore
    public List<gs4> p; // = qy4.f3046a.g();
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ tt4 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f2421a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(boolean z, int i) {
            this.f2421a = z;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.f2421a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(this.f2421a == aVar.f2421a && this.b == aVar.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.f2421a;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (i * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "NameRule(isValid=" + this.f2421a + ", length=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ ts4 f2422a;

        @DexIgnore
        public b(ts4 ts4) {
            pq7.c(ts4, "challengeDraft");
            this.f2422a = ts4;
        }

        @DexIgnore
        public final ts4 a() {
            return this.f2422a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof b) && pq7.a(this.f2422a, ((b) obj).f2422a));
        }

        @DexIgnore
        public int hashCode() {
            ts4 ts4 = this.f2422a;
            if (ts4 != null) {
                return ts4.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "NextState(challengeDraft=" + this.f2422a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1", f = "BCCreateChallengeInputViewModel.kt", l = {259}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ mu4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1$result$1", f = "BCCreateChallengeInputViewModel.kt", l = {261, 263}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object l;
                Object l2;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    if (pq7.a(mu4.b(this.this$0.this$0).k(), "activity_best_result")) {
                        tt4 t = this.this$0.this$0.t();
                        ts4 ts4 = this.this$0.this$0.n;
                        String c = ts4 != null ? ts4.c() : null;
                        if (c != null) {
                            ts4 ts42 = this.this$0.this$0.n;
                            String e = ts42 != null ? ts42.e() : null;
                            ts4 ts43 = this.this$0.this$0.n;
                            String a2 = ts43 != null ? ts43.a() : null;
                            this.L$0 = iv7;
                            this.label = 1;
                            l2 = tt4.l(t, c, e, a2, null, this, 8, null);
                            if (l2 == d) {
                                return d;
                            }
                            return (kz4) l2;
                        }
                        pq7.i();
                        throw null;
                    }
                    tt4 t2 = this.this$0.this$0.t();
                    ts4 ts44 = this.this$0.this$0.n;
                    String c2 = ts44 != null ? ts44.c() : null;
                    if (c2 != null) {
                        ts4 ts45 = this.this$0.this$0.n;
                        String e2 = ts45 != null ? ts45.e() : null;
                        ts4 ts46 = this.this$0.this$0.n;
                        String a3 = ts46 != null ? ts46.a() : null;
                        this.L$0 = iv7;
                        this.label = 2;
                        l = tt4.l(t2, c2, e2, a3, null, this, 8, null);
                        if (l == d) {
                            return d;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    l2 = obj;
                    return (kz4) l2;
                } else if (i == 2) {
                    iv7 iv73 = (iv7) this.L$0;
                    el7.b(obj);
                    l = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (kz4) l;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mu4 mu4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = mu4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.l.l(ao7.a(true));
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) g;
            this.this$0.l.l(ao7.a(false));
            if (kz4.c() != null) {
                this.this$0.m.l(new gl7(ao7.a(true), null, this.this$0.n));
            } else {
                this.this$0.m.l(new gl7(ao7.a(false), kz4.a(), null));
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = ey4.class.getSimpleName();
        pq7.b(simpleName, "BCFriendTabViewModel::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public mu4(on5 on5, tt4 tt4) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(tt4, "mChallengeRepository");
        this.s = tt4;
    }

    @DexIgnore
    public static final /* synthetic */ ts4 b(mu4 mu4) {
        ts4 ts4 = mu4.o;
        if (ts4 != null) {
            return ts4;
        }
        pq7.n("originalDraft");
        throw null;
    }

    @DexIgnore
    public final LiveData<Boolean> A() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Boolean> B() {
        return this.e;
    }

    @DexIgnore
    public final void C(vs4 vs4) {
        if (vs4 != null) {
            String d2 = vs4.d();
            this.n = new ts4(null, null, null, vs4.g(), d2, vs4.e(), vs4.b(), null, null, false, null, FailureCode.FAILED_TO_CLEAR_DATA, null);
            e();
        }
        k();
    }

    @DexIgnore
    public final void D(ts4 ts4) {
        pq7.c(ts4, "draft");
        this.n = ts4;
        this.o = new ts4(null, ts4.e(), ts4.a(), null, null, ts4.i(), ts4.b(), null, null, false, null, 1945, null);
        e();
    }

    @DexIgnore
    public final void E(String str) {
        pq7.c(str, "newDes");
        ts4 ts4 = this.n;
        if (ts4 != null) {
            ts4.n(str);
        }
        if (str.length() > 0) {
            if (str.length() > 500) {
                this.h.l(Boolean.FALSE);
            } else {
                this.h.l(Boolean.TRUE);
            }
        }
        j();
    }

    @DexIgnore
    public final void F() {
        ts4 ts4 = this.o;
        if (ts4 != null) {
            ts4 ts42 = this.n;
            if (ts42 == null) {
                pq7.i();
                throw null;
            } else if (py4.c(ts4, ts42)) {
                this.m.l(new gl7<>(Boolean.TRUE, null, null));
            } else {
                xw7 unused = gu7.d(us0.a(this), null, null, new c(this, null), 3, null);
            }
        } else {
            pq7.n("originalDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void G(CharSequence charSequence) {
        if (charSequence != null) {
            if (!(charSequence.length() > 0)) {
                this.q = 0;
                f(0);
                ts4 ts4 = this.n;
                if (ts4 != null) {
                    ts4.p((this.q * 60 * 60) + (this.r * 60));
                }
                j();
            } else if (charSequence.length() > 3) {
                f(this.q);
                return;
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.q != parseInt) {
                    this.q = parseInt;
                    ts4 ts42 = this.n;
                    if (ts42 != null) {
                        ts42.p((parseInt * 60 * 60) + (this.r * 60));
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        f(this.q);
                    }
                    j();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    f(0);
                }
            }
            n();
        }
    }

    @DexIgnore
    public final void H(CharSequence charSequence) {
        if (charSequence != null) {
            if (charSequence.length() > 0) {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.r != parseInt) {
                    this.r = parseInt;
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        g(parseInt);
                    } else if (parseInt > 59) {
                        this.r = 59;
                        g(59);
                    }
                    ts4 ts4 = this.n;
                    if (ts4 != null) {
                        ts4.p((this.q * 60 * 60) + (this.r * 60));
                    }
                    j();
                    n();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    g(parseInt);
                }
            } else {
                this.r = 0;
                g(0);
                ts4 ts42 = this.n;
                if (ts42 != null) {
                    ts42.p((this.q * 60 * 60) + (this.r * 60));
                }
                j();
                n();
            }
        }
    }

    @DexIgnore
    public final void I(String str) {
        pq7.c(str, "newName");
        M(str.length());
        ts4 ts4 = this.n;
        if (ts4 != null) {
            ts4.s(str);
            j();
        }
    }

    @DexIgnore
    public final void J() {
        ts4 ts4 = this.n;
        if (ts4 != null) {
            i(ts4);
        }
    }

    @DexIgnore
    public final void K(gs4 gs4) {
        pq7.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        ts4 ts4 = this.n;
        if (ts4 != null) {
            Object a2 = gs4.a();
            if (!(a2 instanceof String)) {
                a2 = null;
            }
            String str = (String) a2;
            if (str == null) {
                str = "public_with_friend";
            }
            ts4.t(str);
        }
        k();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.e(str2, "onPrivacyChanged - privacyModels: " + this.p);
    }

    @DexIgnore
    public final void L(CharSequence charSequence) {
        int i2 = 0;
        if (charSequence != null) {
            if (!(charSequence.length() > 0)) {
                ts4 ts4 = this.n;
                if (ts4 != null) {
                    ts4.v(0);
                }
                m(0);
                j();
                l(0);
            } else if (charSequence.length() > 7) {
                ts4 ts42 = this.n;
                if (ts42 != null) {
                    i2 = ts42.i();
                }
                m(i2);
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                ts4 ts43 = this.n;
                if (ts43 == null || parseInt != ts43.i()) {
                    ts4 ts44 = this.n;
                    if (ts44 != null) {
                        ts44.v(parseInt);
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        m(parseInt);
                    }
                    l(parseInt);
                    j();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    m(0);
                }
            }
        }
    }

    @DexIgnore
    public final void M(int i2) {
        if (1 <= i2 && 32 >= i2) {
            a e2 = this.g.e();
            if (e2 == null || !e2.b()) {
                h(true, i2);
                return;
            }
            return;
        }
        a e3 = this.g.e();
        if (e3 == null || e3.b()) {
            h(false, i2);
        }
    }

    @DexIgnore
    public final void e() {
        this.f2420a.l(this.n);
    }

    @DexIgnore
    public final void f(int i2) {
        this.i.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void g(int i2) {
        this.j.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void h(boolean z, int i2) {
        this.g.l(new a(z, i2));
    }

    @DexIgnore
    public final void i(ts4 ts4) {
        this.c.l(new b(ts4));
    }

    @DexIgnore
    public final void j() {
        String str;
        ts4 ts4 = this.n;
        if (ts4 == null || (str = ts4.a()) == null) {
            str = "";
        }
        if (str.length() > 0) {
            MutableLiveData<Boolean> mutableLiveData = this.d;
            ts4 ts42 = this.n;
            mutableLiveData.l(Boolean.valueOf((ts42 != null ? us4.a(ts42) : false) && str.length() <= 500));
            return;
        }
        MutableLiveData<Boolean> mutableLiveData2 = this.d;
        ts4 ts43 = this.n;
        mutableLiveData2.l(ts43 != null ? Boolean.valueOf(us4.a(ts43)) : null);
    }

    @DexIgnore
    public final void k() {
        this.b.l(this.p);
    }

    @DexIgnore
    public final void l(int i2) {
        if (1000 <= i2 && 300000 >= i2) {
            if (!pq7.a(this.f.e(), Boolean.TRUE)) {
                this.f.l(Boolean.TRUE);
            }
        } else if (!pq7.a(this.f.e(), Boolean.FALSE)) {
            this.f.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final void m(int i2) {
        this.k.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void n() {
        int i2 = (this.q * 60 * 60) + (this.r * 60);
        if (3600 <= i2 && 259200 >= i2) {
            if (!pq7.a(this.e.e(), Boolean.TRUE)) {
                this.e.l(Boolean.TRUE);
            }
        } else if (!pq7.a(this.e.e(), Boolean.FALSE)) {
            this.e.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<ts4> p() {
        return this.f2420a;
    }

    @DexIgnore
    public final LiveData<gl7<Boolean, ServerError, ts4>> q() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<Integer> r() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Boolean> s() {
        return this.l;
    }

    @DexIgnore
    public final tt4 t() {
        return this.s;
    }

    @DexIgnore
    public final LiveData<Integer> u() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<a> v() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<b> w() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> x() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<gs4>> y() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Integer> z() {
        return this.k;
    }
}
