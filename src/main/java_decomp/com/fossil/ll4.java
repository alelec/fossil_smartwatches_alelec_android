package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ll4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Ll4) {
            Ll4 ll4 = (Ll4) obj;
            return this.a == ll4.a && this.b == ll4.b;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.a + "x" + this.b;
    }
}
