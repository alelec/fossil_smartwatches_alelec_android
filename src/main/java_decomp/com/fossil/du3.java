package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Du3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Eu3 c;

    @DexIgnore
    public Du3(Eu3 eu3, Nt3 nt3) {
        this.c = eu3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.c.b) {
            if (this.c.c != null) {
                this.c.c.onSuccess(this.b.m());
            }
        }
    }
}
