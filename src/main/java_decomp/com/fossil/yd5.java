package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yd5 extends Xd5 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131363227, 1);
        A.put(2131362056, 2);
        A.put(2131362758, 3);
        A.put(2131362268, 4);
        A.put(2131363316, 5);
        A.put(2131363370, 6);
        A.put(2131361920, 7);
    }
    */

    @DexIgnore
    public Yd5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public Yd5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (View) objArr[7], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[0], (FlexibleButton) objArr[4], (ImageView) objArr[3], (View) objArr[1], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[6]);
        this.y = -1;
        this.s.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
