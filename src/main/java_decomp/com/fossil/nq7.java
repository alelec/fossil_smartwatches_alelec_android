package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nq7 extends Gq7 implements Mq7, Gs7 {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public Nq7(int i) {
        this.arity = i;
    }

    @DexIgnore
    public Nq7(int i, Object obj) {
        super(obj);
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ds7 computeReflected() {
        Er7.a(this);
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Nq7) {
            Nq7 nq7 = (Nq7) obj;
            if (getOwner() != null ? getOwner().equals(nq7.getOwner()) : nq7.getOwner() == null) {
                if (getName().equals(nq7.getName()) && getSignature().equals(nq7.getSignature()) && Wg6.a(getBoundReceiver(), nq7.getBoundReceiver())) {
                    return true;
                }
            }
            return false;
        } else if (obj instanceof Gs7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.Mq7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Gs7 getReflected() {
        return (Gs7) super.getReflected();
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner() == null ? 0 : getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Gs7
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @DexIgnore
    @Override // com.fossil.Gs7
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @DexIgnore
    @Override // com.fossil.Gs7
    public boolean isInline() {
        return getReflected().isInline();
    }

    @DexIgnore
    @Override // com.fossil.Gs7
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7, com.fossil.Gs7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public String toString() {
        Ds7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }
}
