package com.fossil;

import android.os.Build;
import androidx.fragment.app.Fragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Y78<T> {
    @DexIgnore
    public T a;

    @DexIgnore
    public Y78(T t) {
        this.a = t;
    }

    @DexIgnore
    public static Y78<Fragment> b(Fragment fragment) {
        return Build.VERSION.SDK_INT < 23 ? new X78(fragment) : new Z78(fragment);
    }

    @DexIgnore
    public T a() {
        return this.a;
    }

    @DexIgnore
    public boolean c(String str) {
        return !d(str);
    }

    @DexIgnore
    public abstract boolean d(String str);

    @DexIgnore
    public boolean e(List<String> list) {
        for (String str : list) {
            if (c(str)) {
                return true;
            }
        }
        return false;
    }
}
