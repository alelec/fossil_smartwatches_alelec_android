package com.fossil;

import com.mapped.EditPhotoFragment;
import com.portfolio.platform.data.source.scope.ActivityScoped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@ActivityScoped
public interface L96 {
    @DexIgnore
    void a(EditPhotoFragment editPhotoFragment);
}
