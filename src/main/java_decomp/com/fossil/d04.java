package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D04 {
    @DexIgnore
    public static Zz3 a(int i) {
        return i != 0 ? i != 1 ? b() : new A04() : new F04();
    }

    @DexIgnore
    public static Zz3 b() {
        return new F04();
    }

    @DexIgnore
    public static B04 c() {
        return new B04();
    }

    @DexIgnore
    public static void d(View view, float f) {
        Drawable background = view.getBackground();
        if (background instanceof C04) {
            ((C04) background).U(f);
        }
    }

    @DexIgnore
    public static void e(View view) {
        Drawable background = view.getBackground();
        if (background instanceof C04) {
            f(view, (C04) background);
        }
    }

    @DexIgnore
    public static void f(View view, C04 c04) {
        if (c04.O()) {
            c04.Z(Kz3.c(view));
        }
    }
}
