package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.Fg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S64 implements Fg3.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ T64 a;

    @DexIgnore
    public S64(T64 t64) {
        this.a = t64;
    }

    @DexIgnore
    @Override // com.fossil.Sn3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (str != null && !str.equals(CrashDumperPlugin.NAME) && O64.f(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("name", str2);
            bundle2.putLong("timestampInMillis", j);
            bundle2.putBundle(NativeProtocol.WEB_DIALOG_PARAMS, bundle);
            this.a.a.a(3, bundle2);
        }
    }
}
