package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleEditText t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport v;
    @DexIgnore
    public /* final */ FlexibleTextView w;

    @DexIgnore
    public V35(Object obj, View view, int i, FlexibleButton flexibleButton, ImageView imageView, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, ConstraintLayout constraintLayout2, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = constraintLayout;
        this.t = flexibleEditText;
        this.u = constraintLayout2;
        this.v = recyclerViewEmptySupport;
        this.w = flexibleTextView;
    }
}
