package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L23 {
    @DexIgnore
    public static /* final */ J23 a; // = c();
    @DexIgnore
    public static /* final */ J23 b; // = new I23();

    @DexIgnore
    public static J23 a() {
        return a;
    }

    @DexIgnore
    public static J23 b() {
        return b;
    }

    @DexIgnore
    public static J23 c() {
        try {
            return (J23) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception e) {
            return null;
        }
    }
}
