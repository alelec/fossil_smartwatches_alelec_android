package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z56 implements MembersInjector<NotificationHybridEveryoneActivity> {
    @DexIgnore
    public static void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity, NotificationHybridEveryonePresenter notificationHybridEveryonePresenter) {
        notificationHybridEveryoneActivity.A = notificationHybridEveryonePresenter;
    }
}
