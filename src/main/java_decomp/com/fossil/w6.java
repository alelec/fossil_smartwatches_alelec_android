package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W6 {
    @DexIgnore
    public /* synthetic */ W6(Qg6 qg6) {
    }

    @DexIgnore
    public final X6 a(int i) {
        X6 x6;
        X6[] values = X6.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                x6 = null;
                break;
            }
            x6 = values[i2];
            if (x6.b == i) {
                break;
            }
            i2++;
        }
        return x6 != null ? x6 : X6.l;
    }
}
