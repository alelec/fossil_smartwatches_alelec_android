package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Tu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gv3 extends Zc2 implements Ru3, Tu3.Ai {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Gv3> CREATOR; // = new Hv3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public Gv3(String str, String str2, String str3) {
        Rc2.k(str);
        this.b = str;
        Rc2.k(str2);
        this.c = str2;
        Rc2.k(str3);
        this.d = str3;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Gv3)) {
            return false;
        }
        Gv3 gv3 = (Gv3) obj;
        return this.b.equals(gv3.b) && Pc2.a(gv3.c, this.c) && Pc2.a(gv3.d, this.d);
    }

    @DexIgnore
    public final String getPath() {
        return this.d;
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v12, types: [int] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String toString() {
        /*
            r6 = this;
            r2 = 0
            java.lang.String r0 = r6.b
            char[] r4 = r0.toCharArray()
            int r5 = r4.length
            r1 = r2
            r3 = r2
        L_0x000a:
            if (r3 >= r5) goto L_0x0013
            char r0 = r4[r3]
            int r0 = r0 + r1
            int r3 = r3 + 1
            r1 = r0
            goto L_0x000a
        L_0x0013:
            java.lang.String r0 = r6.b
            java.lang.String r0 = r0.trim()
            int r3 = r0.length()
            r4 = 25
            if (r3 <= r4) goto L_0x005c
            r4 = 10
            java.lang.String r2 = r0.substring(r2, r4)
            int r4 = r3 + -10
            java.lang.String r0 = r0.substring(r4, r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r2)
            int r4 = r4.length()
            int r4 = r4 + 16
            java.lang.String r5 = java.lang.String.valueOf(r0)
            int r5 = r5.length()
            int r4 = r4 + r5
            r3.<init>(r4)
            r3.append(r2)
            java.lang.String r2 = "..."
            r3.append(r2)
            r3.append(r0)
            java.lang.String r0 = "::"
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = r3.toString()
        L_0x005c:
            java.lang.String r1 = r6.c
            java.lang.String r2 = r6.d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r0)
            int r4 = r4.length()
            int r4 = r4 + 31
            java.lang.String r5 = java.lang.String.valueOf(r1)
            int r5 = r5.length()
            int r4 = r4 + r5
            java.lang.String r5 = java.lang.String.valueOf(r2)
            int r5 = r5.length()
            int r4 = r4 + r5
            r3.<init>(r4)
            java.lang.String r4 = "Channel{token="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = ", nodeId="
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = ", path="
            r3.append(r0)
            r3.append(r2)
            java.lang.String r0 = "}"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gv3.toString():java.lang.String");
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, this.b, false);
        Bd2.u(parcel, 3, c(), false);
        Bd2.u(parcel, 4, getPath(), false);
        Bd2.b(parcel, a2);
    }
}
