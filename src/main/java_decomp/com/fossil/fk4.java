package com.fossil;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fk4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Fk4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public Ai(Method method, Object obj) {
            this.a = method;
            this.b = obj;
        }

        @DexIgnore
        @Override // com.fossil.Fk4
        public <T> T c(Class<T> cls) throws Exception {
            Fk4.a(cls);
            return (T) this.a.invoke(this.b, cls);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Fk4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public Bi(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.Fk4
        public <T> T c(Class<T> cls) throws Exception {
            Fk4.a(cls);
            return (T) this.a.invoke(null, cls, Integer.valueOf(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Fk4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;

        @DexIgnore
        public Ci(Method method) {
            this.a = method;
        }

        @DexIgnore
        @Override // com.fossil.Fk4
        public <T> T c(Class<T> cls) throws Exception {
            Fk4.a(cls);
            return (T) this.a.invoke(null, cls, Object.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Fk4 {
        @DexIgnore
        @Override // com.fossil.Fk4
        public <T> T c(Class<T> cls) {
            throw new UnsupportedOperationException("Cannot allocate " + cls);
        }
    }

    @DexIgnore
    public static void a(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + cls.getName());
        } else if (Modifier.isAbstract(modifiers)) {
            throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + cls.getName());
        }
    }

    @DexIgnore
    public static Fk4 b() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new Ai(cls.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception e) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new Bi(declaredMethod2, intValue);
            } catch (Exception e2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new Ci(declaredMethod3);
                } catch (Exception e3) {
                    return new Di();
                }
            }
        }
    }

    @DexIgnore
    public abstract <T> T c(Class<T> cls) throws Exception;
}
