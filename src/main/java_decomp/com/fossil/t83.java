package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T83 implements Xw2<W83> {
    @DexIgnore
    public static T83 c; // = new T83();
    @DexIgnore
    public /* final */ Xw2<W83> b;

    @DexIgnore
    public T83() {
        this(Ww2.b(new V83()));
    }

    @DexIgnore
    public T83(Xw2<W83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((W83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ W83 zza() {
        return this.b.zza();
    }
}
