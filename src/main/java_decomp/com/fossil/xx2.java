package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xx2 extends Sx2<E> {
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ /* synthetic */ Sx2 zzc;

    @DexIgnore
    public Xx2(Sx2 sx2, int i, int i2) {
        this.zzc = sx2;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        Sw2.a(i, this.e);
        return (E) this.zzc.get(this.d + i);
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Sx2, java.util.List
    public final /* synthetic */ List subList(int i, int i2) {
        return zza(i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Sx2
    public final Sx2<E> zza(int i, int i2) {
        Sw2.e(i, i2, this.e);
        Sx2 sx2 = this.zzc;
        int i3 = this.d;
        return (Sx2) sx2.subList(i + i3, i3 + i2);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Object[] zze() {
        return this.zzc.zze();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzf() {
        return this.zzc.zzf() + this.d;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzg() {
        return this.zzc.zzf() + this.d + this.e;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return true;
    }
}
