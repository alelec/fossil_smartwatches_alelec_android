package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ia5 extends ha5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        D.put(2131363282, 2);
        D.put(2131363257, 3);
        D.put(2131362054, 4);
        D.put(2131362670, 5);
        D.put(2131362028, 6);
        D.put(2131363550, 7);
        D.put(2131363542, 8);
        D.put(2131363549, 9);
        D.put(2131363546, 10);
    }
    */

    @DexIgnore
    public ia5(zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 11, C, D));
    }

    @DexIgnore
    public ia5(zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (View) objArr[6], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[0], (ImageView) objArr[5], (TextView) objArr[3], (TextView) objArr[2], (TextView) objArr[1], (CustomizeWidget) objArr[8], (CustomizeWidget) objArr[10], (CustomizeWidget) objArr[9], (CustomizeWidget) objArr[7]);
        this.B = -1;
        this.s.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.B = 1;
        }
        w();
    }
}
