package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O02 implements Factory<M02> {
    @DexIgnore
    public /* final */ Provider<T32> a;
    @DexIgnore
    public /* final */ Provider<T32> b;
    @DexIgnore
    public /* final */ Provider<K12> c;
    @DexIgnore
    public /* final */ Provider<B22> d;
    @DexIgnore
    public /* final */ Provider<F22> e;

    @DexIgnore
    public O02(Provider<T32> provider, Provider<T32> provider2, Provider<K12> provider3, Provider<B22> provider4, Provider<F22> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static O02 a(Provider<T32> provider, Provider<T32> provider2, Provider<K12> provider3, Provider<B22> provider4, Provider<F22> provider5) {
        return new O02(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public M02 b() {
        return new M02(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
