package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ne3 {
    @DexIgnore
    public /* final */ Os2 a;

    @DexIgnore
    public Ne3(Os2 os2) {
        Rc2.k(os2);
        this.a = os2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.b(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void d(int i) {
        try {
            this.a.setFillColor(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void e(boolean z) {
        try {
            this.a.setGeodesic(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Ne3)) {
            return false;
        }
        try {
            return this.a.L2(((Ne3) obj).a);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void f(List<LatLng> list) {
        try {
            this.a.setPoints(list);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.a.setStrokeColor(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void h(float f) {
        try {
            this.a.setStrokeWidth(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void i(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void j(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
