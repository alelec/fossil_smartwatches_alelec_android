package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Du2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2.Bi g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Du2(Zs2.Bi bi, Activity activity) {
        super(Zs2.this);
        this.g = bi;
        this.f = activity;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        Zs2.this.h.onActivityStopped(Tg2.n(this.f), this.c);
    }
}
