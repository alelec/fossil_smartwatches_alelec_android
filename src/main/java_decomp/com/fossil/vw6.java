package com.fossil;

import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vw6 implements MembersInjector<UpdateFirmwarePresenter> {
    @DexIgnore
    public static void a(UpdateFirmwarePresenter updateFirmwarePresenter) {
        updateFirmwarePresenter.M();
    }
}
