package com.fossil;

import android.os.Build;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ay3 {
    @DexIgnore
    public static String a(long j) {
        return b(j, Locale.getDefault());
    }

    @DexIgnore
    public static String b(long j, Locale locale) {
        return Build.VERSION.SDK_INT >= 24 ? Ny3.b(locale).format(new Date(j)) : Ny3.f(locale).format(new Date(j));
    }

    @DexIgnore
    public static String c(long j) {
        return d(j, Locale.getDefault());
    }

    @DexIgnore
    public static String d(long j, Locale locale) {
        return Build.VERSION.SDK_INT >= 24 ? Ny3.m(locale).format(new Date(j)) : Ny3.f(locale).format(new Date(j));
    }
}
