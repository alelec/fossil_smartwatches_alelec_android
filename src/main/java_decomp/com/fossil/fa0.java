package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Fa0 extends Enum<Fa0> {
    @DexIgnore
    public static /* final */ Fa0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ Fa0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Fa0 fa0 = new Fa0("NONE", 0, (byte) 0);
        Fa0 fa02 = new Fa0("OPEN", 1, (byte) 1);
        Fa0 fa03 = new Fa0("ERROR", 2, (byte) 2);
        c = fa03;
        d = new Fa0[]{fa0, fa02, fa03, new Fa0("READY", 3, (byte) 3), new Fa0("START", 4, (byte) 4), new Fa0("STOP", 5, (byte) 5), new Fa0("RESET", 6, (byte) 6)};
    }
    */

    @DexIgnore
    public Fa0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Fa0 valueOf(String str) {
        return (Fa0) Enum.valueOf(Fa0.class, str);
    }

    @DexIgnore
    public static Fa0[] values() {
        return (Fa0[]) d.clone();
    }
}
