package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Be1 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Be1 build();
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        boolean a(File file);
    }

    @DexIgnore
    void a(Mb1 mb1, Bi bi);

    @DexIgnore
    File b(Mb1 mb1);
}
