package com.fossil;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N84 implements Runnable {
    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(10);
        a();
    }
}
