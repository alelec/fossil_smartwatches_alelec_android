package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hp4 implements Factory<FirmwareFileRepository> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Hp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Hp4 a(Uo4 uo4) {
        return new Hp4(uo4);
    }

    @DexIgnore
    public static FirmwareFileRepository c(Uo4 uo4) {
        FirmwareFileRepository o = uo4.o();
        Lk7.c(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    public FirmwareFileRepository b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
