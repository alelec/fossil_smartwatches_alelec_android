package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ ViewPager2 B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ CustomizeWidget I;
    @DexIgnore
    public /* final */ CustomizeWidget J;
    @DexIgnore
    public /* final */ CustomizeWidget K;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ CardView t;
    @DexIgnore
    public /* final */ ImageButton u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    public H85(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CardView cardView, ImageButton imageButton, View view3, ImageView imageView, View view4, View view5, View view6, ConstraintLayout constraintLayout3, ViewPager2 viewPager2, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view7, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = cardView;
        this.u = imageButton;
        this.v = view3;
        this.w = imageView;
        this.x = view4;
        this.y = view5;
        this.z = view6;
        this.A = constraintLayout3;
        this.B = viewPager2;
        this.C = rTLImageView;
        this.D = flexibleTextView;
        this.E = flexibleTextView2;
        this.F = flexibleTextView3;
        this.G = flexibleTextView4;
        this.H = view7;
        this.I = customizeWidget;
        this.J = customizeWidget2;
        this.K = customizeWidget3;
    }
}
