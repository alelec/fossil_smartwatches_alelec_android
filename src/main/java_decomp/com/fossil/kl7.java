package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Wg6;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kl7 implements Collection<Jl7>, Jr7 {
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Gn7 {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ byte[] c;

        @DexIgnore
        public Ai(byte[] bArr) {
            Wg6.c(bArr, "array");
            this.c = bArr;
        }

        @DexIgnore
        @Override // com.fossil.Gn7
        public byte b() {
            int i = this.b;
            byte[] bArr = this.c;
            if (i < bArr.length) {
                this.b = i + 1;
                byte b2 = bArr[i];
                Jl7.e(b2);
                return b2;
            }
            throw new NoSuchElementException(String.valueOf(this.b));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.length;
        }
    }

    @DexIgnore
    public static boolean b(byte[] bArr, byte b2) {
        return Em7.x(bArr, b2);
    }

    @DexIgnore
    public static boolean c(byte[] bArr, Collection<Jl7> collection) {
        boolean z;
        Wg6.c(collection, MessengerShareContentUtility.ELEMENTS);
        if (!collection.isEmpty()) {
            for (T t : collection) {
                if (!(t instanceof Jl7) || !Em7.x(bArr, t.j())) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (!z) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean e(byte[] bArr, Object obj) {
        return (obj instanceof Kl7) && Wg6.a(bArr, ((Kl7) obj).m());
    }

    @DexIgnore
    public static int g(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public static int h(byte[] bArr) {
        if (bArr != null) {
            return Arrays.hashCode(bArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean i(byte[] bArr) {
        return bArr.length == 0;
    }

    @DexIgnore
    public static Gn7 k(byte[] bArr) {
        return new Ai(bArr);
    }

    @DexIgnore
    public static String l(byte[] bArr) {
        return "UByteArray(storage=" + Arrays.toString(bArr) + ")";
    }

    @DexIgnore
    public boolean a(byte b2) {
        return b(this.b, b2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.Collection
    public /* synthetic */ boolean add(Jl7 jl7) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean addAll(Collection<? extends Jl7> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Jl7) {
            return a(((Jl7) obj).j());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        return c(this.b, collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return e(this.b, obj);
    }

    @DexIgnore
    public int f() {
        return g(this.b);
    }

    @DexIgnore
    public int hashCode() {
        return h(this.b);
    }

    @DexIgnore
    public boolean isEmpty() {
        return i(this.b);
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // java.util.Collection, java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator<Jl7> iterator() {
        return j();
    }

    @DexIgnore
    public Gn7 j() {
        return k(this.b);
    }

    @DexIgnore
    public final /* synthetic */ byte[] m() {
        return this.b;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return f();
    }

    @DexIgnore
    public Object[] toArray() {
        return Jq7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) Jq7.b(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return l(this.b);
    }
}
