package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.P18;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S18 extends RequestBody {
    @DexIgnore
    public static /* final */ R18 e; // = R18.c("multipart/mixed");
    @DexIgnore
    public static /* final */ R18 f; // = R18.c("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {13, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {Framer.STDIN_FRAME_PREFIX, Framer.STDIN_FRAME_PREFIX};
    @DexIgnore
    public /* final */ L48 a;
    @DexIgnore
    public /* final */ R18 b;
    @DexIgnore
    public /* final */ List<Bi> c;
    @DexIgnore
    public long d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ L48 a;
        @DexIgnore
        public R18 b;
        @DexIgnore
        public /* final */ List<Bi> c;

        @DexIgnore
        public Ai() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public Ai(String str) {
            this.b = S18.e;
            this.c = new ArrayList();
            this.a = L48.encodeUtf8(str);
        }

        @DexIgnore
        public Ai a(String str, String str2) {
            d(Bi.b(str, str2));
            return this;
        }

        @DexIgnore
        public Ai b(String str, String str2, RequestBody requestBody) {
            d(Bi.c(str, str2, requestBody));
            return this;
        }

        @DexIgnore
        public Ai c(P18 p18, RequestBody requestBody) {
            d(Bi.a(p18, requestBody));
            return this;
        }

        @DexIgnore
        public Ai d(Bi bi) {
            if (bi != null) {
                this.c.add(bi);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public S18 e() {
            if (!this.c.isEmpty()) {
                return new S18(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }

        @DexIgnore
        public Ai f(R18 r18) {
            if (r18 == null) {
                throw new NullPointerException("type == null");
            } else if (r18.f().equals("multipart")) {
                this.b = r18;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + r18);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ P18 a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public Bi(P18 p18, RequestBody requestBody) {
            this.a = p18;
            this.b = requestBody;
        }

        @DexIgnore
        public static Bi a(P18 p18, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (p18 != null && p18.c("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (p18 == null || p18.c(HttpHeaders.CONTENT_LENGTH) == null) {
                return new Bi(p18, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }

        @DexIgnore
        public static Bi b(String str, String str2) {
            return c(str, null, RequestBody.d(null, str2));
        }

        @DexIgnore
        public static Bi c(String str, String str2, RequestBody requestBody) {
            if (str != null) {
                StringBuilder sb = new StringBuilder("form-data; name=");
                S18.i(sb, str);
                if (str2 != null) {
                    sb.append("; filename=");
                    S18.i(sb, str2);
                }
                P18.Ai ai = new P18.Ai();
                ai.d("Content-Disposition", sb.toString());
                return a(ai.e(), requestBody);
            }
            throw new NullPointerException("name == null");
        }
    }

    /*
    static {
        R18.c("multipart/alternative");
        R18.c("multipart/digest");
        R18.c("multipart/parallel");
    }
    */

    @DexIgnore
    public S18(L48 l48, R18 r18, List<Bi> list) {
        this.a = l48;
        this.b = R18.c(r18 + "; boundary=" + l48.utf8());
        this.c = B28.t(list);
    }

    @DexIgnore
    public static StringBuilder i(StringBuilder sb, String str) {
        sb.append('\"');
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '\n') {
                sb.append("%0A");
            } else if (charAt == '\r') {
                sb.append("%0D");
            } else if (charAt != '\"') {
                sb.append(charAt);
            } else {
                sb.append("%22");
            }
        }
        sb.append('\"');
        return sb;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long j2 = j(null, true);
        this.d = j2;
        return j2;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public R18 b() {
        return this.b;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void h(J48 j48) throws IOException {
        j(j48, false);
    }

    @DexIgnore
    public final long j(J48 j48, boolean z) throws IOException {
        I48 i48;
        if (z) {
            I48 i482 = new I48();
            i48 = i482;
            j48 = i482;
        } else {
            i48 = null;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            Bi bi = this.c.get(i2);
            P18 p18 = bi.a;
            RequestBody requestBody = bi.b;
            j48.Z(i);
            j48.a0(this.a);
            j48.Z(h);
            if (p18 != null) {
                int h2 = p18.h();
                for (int i3 = 0; i3 < h2; i3++) {
                    j48.E(p18.e(i3)).Z(g).E(p18.i(i3)).Z(h);
                }
            }
            R18 b2 = requestBody.b();
            if (b2 != null) {
                j48.E("Content-Type: ").E(b2.toString()).Z(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                j48.E("Content-Length: ").k0(a2).Z(h);
            } else if (z) {
                i48.j();
                return -1;
            }
            j48.Z(h);
            if (z) {
                j += a2;
            } else {
                requestBody.h(j48);
            }
            j48.Z(h);
        }
        j48.Z(i);
        j48.a0(this.a);
        j48.Z(i);
        j48.Z(h);
        if (!z) {
            return j;
        }
        long p0 = j + i48.p0();
        i48.j();
        return p0;
    }
}
