package com.fossil;

import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X14 extends I44<Object> implements Serializable {
    @DexIgnore
    public static /* final */ X14 INSTANCE; // = new X14();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.I44, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E> Y24<E> immutableSortedCopy(Iterable<E> iterable) {
        return Y24.copyOf(iterable);
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S> I44<S> reverse() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <E> List<E> sortedCopy(Iterable<E> iterable) {
        return T34.h(iterable);
    }

    @DexIgnore
    public String toString() {
        return "Ordering.allEqual()";
    }
}
