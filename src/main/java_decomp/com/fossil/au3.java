package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Au3<TResult> implements Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public Ht3<TResult> c;

    @DexIgnore
    public Au3(Executor executor, Ht3<TResult> ht3) {
        this.a = executor;
        this.c = ht3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        synchronized (this.b) {
            if (this.c != null) {
                this.a.execute(new Zt3(this, nt3));
            }
        }
    }
}
