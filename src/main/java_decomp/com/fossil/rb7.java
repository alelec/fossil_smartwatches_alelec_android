package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rb7 {
    @DexIgnore
    public /* final */ Hb7 a;
    @DexIgnore
    public /* final */ Bitmap b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public Rb7(Hb7 hb7, Bitmap bitmap, String str, String str2) {
        this.a = hb7;
        this.b = bitmap;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Rb7(Hb7 hb7, Bitmap bitmap, String str, String str2, int i, Qg6 qg6) {
        this(hb7, bitmap, str, (i & 8) != 0 ? null : str2);
    }

    @DexIgnore
    public final Hb7 a() {
        return this.a;
    }

    @DexIgnore
    public final Bitmap b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Rb7) {
                Rb7 rb7 = (Rb7) obj;
                if (!Wg6.a(this.a, rb7.a) || !Wg6.a(this.b, rb7.b) || !Wg6.a(this.c, rb7.c) || !Wg6.a(this.d, rb7.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Hb7 hb7 = this.a;
        int hashCode = hb7 != null ? hb7.hashCode() : 0;
        Bitmap bitmap = this.b;
        int hashCode2 = bitmap != null ? bitmap.hashCode() : 0;
        String str = this.c;
        int hashCode3 = str != null ? str.hashCode() : 0;
        String str2 = this.d;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "UIBackground(backgroundData=" + this.a + ", bitmap=" + this.b + ", previewUrl=" + this.c + ", localImgPath=" + this.d + ")";
    }
}
