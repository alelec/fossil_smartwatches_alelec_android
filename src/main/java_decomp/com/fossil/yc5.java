package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yc5 extends Xc5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d w;
    @DexIgnore
    public static /* final */ SparseIntArray x;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(5);
        w = dVar;
        dVar.a(0, new String[]{"item_active_time_day"}, new int[]{1}, new int[]{2131558650});
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(2131362127, 2);
        x.put(2131362557, 3);
        x.put(2131362558, 4);
    }
    */

    @DexIgnore
    public Yc5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 5, w, x));
    }

    @DexIgnore
    public Yc5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 1, (ConstraintLayout) objArr[2], (Vc5) objArr[1], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4]);
        this.v = -1;
        LinearLayout linearLayout = (LinearLayout) objArr[0];
        this.u = linearLayout;
        linearLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.v = 0;
        }
        ViewDataBinding.i(this.r);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.r.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return false;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.v     // Catch:{ all -> 0x0017 }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x0017 }
            com.fossil.Vc5 r1 = r6.r
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x0017:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yc5.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.v = 2;
        }
        this.r.q();
        w();
    }
}
