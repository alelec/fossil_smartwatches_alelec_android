package com.fossil;

import com.fossil.Jn5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Kn5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] g;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] h;

    /*
    static {
        int[] iArr = new int[Uh5.values().length];
        a = iArr;
        iArr[Uh5.BLUETOOTH_OFF.ordinal()] = 1;
        a[Uh5.BACKGROUND_LOCATION_PERMISSION_OFF.ordinal()] = 2;
        a[Uh5.LOCATION_PERMISSION_OFF.ordinal()] = 3;
        a[Uh5.LOCATION_SERVICE_OFF.ordinal()] = 4;
        int[] iArr2 = new int[Jn5.Ci.values().length];
        b = iArr2;
        iArr2[Jn5.Ci.LOCATION_BACKGROUND.ordinal()] = 1;
        b[Jn5.Ci.LOCATION_FINE.ordinal()] = 2;
        b[Jn5.Ci.READ_CALL_LOG.ordinal()] = 3;
        b[Jn5.Ci.READ_PHONE_STATE.ordinal()] = 4;
        b[Jn5.Ci.ANSWER_PHONE_CALL.ordinal()] = 5;
        b[Jn5.Ci.CALL_PHONE.ordinal()] = 6;
        b[Jn5.Ci.READ_SMS.ordinal()] = 7;
        b[Jn5.Ci.RECEIVE_MMS.ordinal()] = 8;
        b[Jn5.Ci.RECEIVE_SMS.ordinal()] = 9;
        int[] iArr3 = new int[Jn5.Ci.values().length];
        c = iArr3;
        iArr3[Jn5.Ci.BLUETOOTH_CONNECTION.ordinal()] = 1;
        c[Jn5.Ci.LOCATION_FINE.ordinal()] = 2;
        c[Jn5.Ci.LOCATION_BACKGROUND.ordinal()] = 3;
        c[Jn5.Ci.LOCATION_SERVICE.ordinal()] = 4;
        c[Jn5.Ci.READ_CONTACTS.ordinal()] = 5;
        c[Jn5.Ci.READ_PHONE_STATE.ordinal()] = 6;
        c[Jn5.Ci.READ_CALL_LOG.ordinal()] = 7;
        c[Jn5.Ci.CALL_PHONE.ordinal()] = 8;
        c[Jn5.Ci.ANSWER_PHONE_CALL.ordinal()] = 9;
        c[Jn5.Ci.READ_SMS.ordinal()] = 10;
        c[Jn5.Ci.RECEIVE_SMS.ordinal()] = 11;
        c[Jn5.Ci.RECEIVE_MMS.ordinal()] = 12;
        c[Jn5.Ci.SEND_SMS.ordinal()] = 13;
        c[Jn5.Ci.NOTIFICATION_ACCESS.ordinal()] = 14;
        c[Jn5.Ci.CAMERA.ordinal()] = 15;
        c[Jn5.Ci.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr4 = new int[Jn5.Ci.values().length];
        d = iArr4;
        iArr4[Jn5.Ci.LOCATION_FINE.ordinal()] = 1;
        d[Jn5.Ci.LOCATION_BACKGROUND.ordinal()] = 2;
        d[Jn5.Ci.READ_CONTACTS.ordinal()] = 3;
        d[Jn5.Ci.READ_PHONE_STATE.ordinal()] = 4;
        d[Jn5.Ci.READ_CALL_LOG.ordinal()] = 5;
        d[Jn5.Ci.CALL_PHONE.ordinal()] = 6;
        d[Jn5.Ci.ANSWER_PHONE_CALL.ordinal()] = 7;
        d[Jn5.Ci.READ_SMS.ordinal()] = 8;
        d[Jn5.Ci.RECEIVE_SMS.ordinal()] = 9;
        d[Jn5.Ci.RECEIVE_MMS.ordinal()] = 10;
        d[Jn5.Ci.SEND_SMS.ordinal()] = 11;
        d[Jn5.Ci.CAMERA.ordinal()] = 12;
        d[Jn5.Ci.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        int[] iArr5 = new int[Jn5.Ai.values().length];
        e = iArr5;
        iArr5[Jn5.Ai.SET_COMPLICATION_WATCH_APP_WEATHER.ordinal()] = 1;
        e[Jn5.Ai.SET_MICRO_APP_COMMUTE_TIME.ordinal()] = 2;
        e[Jn5.Ai.SET_WATCH_APP_COMMUTE_TIME.ordinal()] = 3;
        e[Jn5.Ai.SET_COMPLICATION_CHANCE_OF_RAIN.ordinal()] = 4;
        e[Jn5.Ai.FIND_DEVICE.ordinal()] = 5;
        e[Jn5.Ai.PAIR_DEVICE.ordinal()] = 6;
        e[Jn5.Ai.SET_WATCH_APP_MUSIC.ordinal()] = 7;
        e[Jn5.Ai.SET_MICRO_APP_MUSIC.ordinal()] = 8;
        int[] iArr6 = new int[Jn5.Ci.values().length];
        f = iArr6;
        iArr6[Jn5.Ci.BLUETOOTH_CONNECTION.ordinal()] = 1;
        f[Jn5.Ci.LOCATION_FINE.ordinal()] = 2;
        f[Jn5.Ci.LOCATION_BACKGROUND.ordinal()] = 3;
        f[Jn5.Ci.LOCATION_SERVICE.ordinal()] = 4;
        f[Jn5.Ci.READ_CONTACTS.ordinal()] = 5;
        f[Jn5.Ci.READ_PHONE_STATE.ordinal()] = 6;
        f[Jn5.Ci.READ_CALL_LOG.ordinal()] = 7;
        f[Jn5.Ci.CALL_PHONE.ordinal()] = 8;
        f[Jn5.Ci.ANSWER_PHONE_CALL.ordinal()] = 9;
        f[Jn5.Ci.READ_SMS.ordinal()] = 10;
        f[Jn5.Ci.RECEIVE_MMS.ordinal()] = 11;
        f[Jn5.Ci.RECEIVE_SMS.ordinal()] = 12;
        f[Jn5.Ci.SEND_SMS.ordinal()] = 13;
        f[Jn5.Ci.NOTIFICATION_ACCESS.ordinal()] = 14;
        f[Jn5.Ci.CAMERA.ordinal()] = 15;
        f[Jn5.Ci.READ_EXTERNAL_STORAGE.ordinal()] = 16;
        int[] iArr7 = new int[Jn5.Ci.values().length];
        g = iArr7;
        iArr7[Jn5.Ci.NOTIFICATION_ACCESS.ordinal()] = 1;
        g[Jn5.Ci.BLUETOOTH_CONNECTION.ordinal()] = 2;
        g[Jn5.Ci.LOCATION_SERVICE.ordinal()] = 3;
        int[] iArr8 = new int[Jn5.Ci.values().length];
        h = iArr8;
        iArr8[Jn5.Ci.BLUETOOTH_CONNECTION.ordinal()] = 1;
        h[Jn5.Ci.LOCATION_FINE.ordinal()] = 2;
        h[Jn5.Ci.LOCATION_SERVICE.ordinal()] = 3;
        h[Jn5.Ci.READ_CONTACTS.ordinal()] = 4;
        h[Jn5.Ci.READ_PHONE_STATE.ordinal()] = 5;
        h[Jn5.Ci.READ_CALL_LOG.ordinal()] = 6;
        h[Jn5.Ci.CALL_PHONE.ordinal()] = 7;
        h[Jn5.Ci.ANSWER_PHONE_CALL.ordinal()] = 8;
        h[Jn5.Ci.READ_SMS.ordinal()] = 9;
        h[Jn5.Ci.RECEIVE_SMS.ordinal()] = 10;
        h[Jn5.Ci.RECEIVE_MMS.ordinal()] = 11;
        h[Jn5.Ci.CAMERA.ordinal()] = 12;
        h[Jn5.Ci.READ_EXTERNAL_STORAGE.ordinal()] = 13;
        h[Jn5.Ci.NOTIFICATION_ACCESS.ordinal()] = 14;
        h[Jn5.Ci.LOCATION_BACKGROUND.ordinal()] = 15;
        h[Jn5.Ci.SEND_SMS.ordinal()] = 16;
    }
    */
}
