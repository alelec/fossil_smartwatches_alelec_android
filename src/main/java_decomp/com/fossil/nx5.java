package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nx5 extends Fq4 {
    @DexIgnore
    public abstract void n();

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public abstract void p();

    @DexIgnore
    public abstract void q(boolean z, int i);

    @DexIgnore
    public abstract void r(String str);

    @DexIgnore
    public abstract void s(boolean z);

    @DexIgnore
    public abstract void t(String str, String str2, boolean z);

    @DexIgnore
    public abstract void u(String str);
}
