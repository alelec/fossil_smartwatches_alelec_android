package com.fossil;

import com.fossil.P18;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.IDN;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B28 {
    @DexIgnore
    public static /* final */ byte[] a;
    @DexIgnore
    public static /* final */ String[] b; // = new String[0];
    @DexIgnore
    public static /* final */ W18 c;
    @DexIgnore
    public static /* final */ L48 d; // = L48.decodeHex("efbbbf");
    @DexIgnore
    public static /* final */ L48 e; // = L48.decodeHex("feff");
    @DexIgnore
    public static /* final */ L48 f; // = L48.decodeHex("fffe");
    @DexIgnore
    public static /* final */ L48 g; // = L48.decodeHex("0000ffff");
    @DexIgnore
    public static /* final */ L48 h; // = L48.decodeHex("ffff0000");
    @DexIgnore
    public static /* final */ Charset i; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ Charset j; // = Charset.forName("ISO-8859-1");
    @DexIgnore
    public static /* final */ Charset k; // = Charset.forName("UTF-16BE");
    @DexIgnore
    public static /* final */ Charset l; // = Charset.forName("UTF-16LE");
    @DexIgnore
    public static /* final */ Charset m; // = Charset.forName("UTF-32BE");
    @DexIgnore
    public static /* final */ Charset n; // = Charset.forName("UTF-32LE");
    @DexIgnore
    public static /* final */ TimeZone o; // = TimeZone.getTimeZone("GMT");
    @DexIgnore
    public static /* final */ Comparator<String> p; // = new Ai();
    @DexIgnore
    public static /* final */ Method q;
    @DexIgnore
    public static /* final */ Pattern r; // = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Comparator<String> {
        @DexIgnore
        public int a(String str, String str2) {
            return str.compareTo(str2);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(String str, String str2) {
            return a(str, str2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public Bi(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, this.a);
            thread.setDaemon(this.b);
            return thread;
        }
    }

    /*
    static {
        Method method = null;
        byte[] bArr = new byte[0];
        a = bArr;
        c = W18.create((R18) null, bArr);
        RequestBody.f(null, a);
        try {
            method = Throwable.class.getDeclaredMethod("addSuppressed", Throwable.class);
        } catch (Exception e2) {
        }
        q = method;
    }
    */

    @DexIgnore
    public static boolean A(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }

    @DexIgnore
    public static boolean B(Comparator<String> comparator, String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length == 0 || strArr2.length == 0) {
            return false;
        }
        for (String str : strArr) {
            for (String str2 : strArr2) {
                if (comparator.compare(str, str2) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public static X509TrustManager C() {
        try {
            TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            instance.init((KeyStore) null);
            TrustManager[] trustManagers = instance.getTrustManagers();
            if (trustManagers.length == 1 && (trustManagers[0] instanceof X509TrustManager)) {
                return (X509TrustManager) trustManagers[0];
            }
            throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
        } catch (GeneralSecurityException e2) {
            throw b("No System TLS", e2);
        }
    }

    @DexIgnore
    public static boolean D(C58 c58, int i2, TimeUnit timeUnit) throws IOException {
        long nanoTime = System.nanoTime();
        long c2 = c58.e().e() ? c58.e().c() - nanoTime : Long.MAX_VALUE;
        c58.e().d(Math.min(c2, timeUnit.toNanos((long) i2)) + nanoTime);
        try {
            I48 i48 = new I48();
            while (c58.d0(i48, 8192) != -1) {
                i48.j();
            }
            if (c2 == Long.MAX_VALUE) {
                c58.e().a();
            } else {
                c58.e().d(c2 + nanoTime);
            }
            return true;
        } catch (InterruptedIOException e2) {
            if (c2 == Long.MAX_VALUE) {
                c58.e().a();
            } else {
                c58.e().d(c2 + nanoTime);
            }
            return false;
        } catch (Throwable th) {
            if (c2 == Long.MAX_VALUE) {
                c58.e().a();
            } else {
                c58.e().d(c2 + nanoTime);
            }
            throw th;
        }
    }

    @DexIgnore
    public static int E(String str, int i2, int i3) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (!(charAt == '\t' || charAt == '\n' || charAt == '\f' || charAt == '\r' || charAt == ' ')) {
                return i4;
            }
        }
        return i3;
    }

    @DexIgnore
    public static int F(String str, int i2, int i3) {
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            char charAt = str.charAt(i4);
            if (!(charAt == '\t' || charAt == '\n' || charAt == '\f' || charAt == '\r' || charAt == ' ')) {
                return i4 + 1;
            }
        }
        return i2;
    }

    @DexIgnore
    public static ThreadFactory G(String str, boolean z) {
        return new Bi(str, z);
    }

    @DexIgnore
    public static P18 H(List<E38> list) {
        P18.Ai ai = new P18.Ai();
        for (E38 e38 : list) {
            Z18.a.b(ai, e38.a.utf8(), e38.b.utf8());
        }
        return ai.e();
    }

    @DexIgnore
    public static String I(String str, int i2, int i3) {
        int E = E(str, i2, i3);
        return str.substring(E, F(str, E, i3));
    }

    @DexIgnore
    public static boolean J(String str) {
        return r.matcher(str).matches();
    }

    @DexIgnore
    public static void a(Throwable th, Throwable th2) {
        Method method = q;
        if (method != null) {
            try {
                method.invoke(th, th2);
            } catch (IllegalAccessException | InvocationTargetException e2) {
            }
        }
    }

    @DexIgnore
    public static AssertionError b(String str, Exception exc) {
        AssertionError assertionError = new AssertionError(str);
        try {
            assertionError.initCause(exc);
        } catch (IllegalStateException e2) {
        }
        return assertionError;
    }

    @DexIgnore
    public static Charset c(K48 k48, Charset charset) throws IOException {
        if (k48.H(0, d)) {
            k48.skip((long) d.size());
            return i;
        } else if (k48.H(0, e)) {
            k48.skip((long) e.size());
            return k;
        } else if (k48.H(0, f)) {
            k48.skip((long) f.size());
            return l;
        } else if (k48.H(0, g)) {
            k48.skip((long) g.size());
            return m;
        } else if (!k48.H(0, h)) {
            return charset;
        } else {
            k48.skip((long) h.size());
            return n;
        }
    }

    @DexIgnore
    public static String d(String str) {
        if (str.contains(":")) {
            InetAddress m2 = (!str.startsWith("[") || !str.endsWith("]")) ? m(str, 0, str.length()) : m(str, 1, str.length() - 1);
            if (m2 == null) {
                return null;
            }
            byte[] address = m2.getAddress();
            if (address.length == 16) {
                return y(address);
            }
            throw new AssertionError("Invalid IPv6 address: '" + str + "'");
        }
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (lowerCase.isEmpty() || j(lowerCase)) {
                return null;
            }
            return lowerCase;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    @DexIgnore
    public static int e(String str, long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException(str + " < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException(str + " too large.");
            } else if (millis != 0 || i2 <= 0) {
                return (int) millis;
            } else {
                throw new IllegalArgumentException(str + " too small.");
            }
        } else {
            throw new NullPointerException("unit == null");
        }
    }

    @DexIgnore
    public static void f(long j2, long j3, long j4) {
        if ((j3 | j4) < 0 || j3 > j2 || j2 - j3 < j4) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @DexIgnore
    public static void g(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    @DexIgnore
    public static void h(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e2) {
                if (!A(e2)) {
                    throw e2;
                }
            } catch (RuntimeException e3) {
                throw e3;
            } catch (Exception e4) {
            }
        }
    }

    @DexIgnore
    public static String[] i(String[] strArr, String str) {
        int length = strArr.length + 1;
        String[] strArr2 = new String[length];
        System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
        strArr2[length - 1] = str;
        return strArr2;
    }

    @DexIgnore
    public static boolean j(String str) {
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt <= 31 || charAt >= '\u007f') {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static int k(char c2) {
        char c3 = 'a';
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 < 'a' || c2 > 'f') {
            if (c2 < 'A' || c2 > 'F') {
                return -1;
            }
            c3 = 'A';
        }
        return (c2 - c3) + 10;
    }

    @DexIgnore
    public static boolean l(String str, int i2, int i3, byte[] bArr, int i4) {
        int i5 = i2;
        int i6 = i4;
        while (i5 < i3) {
            if (i6 == bArr.length) {
                return false;
            }
            if (i6 != i4) {
                if (str.charAt(i5) != '.') {
                    return false;
                }
                i5++;
            }
            int i7 = 0;
            int i8 = i5;
            while (i8 < i3) {
                char charAt = str.charAt(i8);
                if (charAt < '0' || charAt > '9') {
                    break;
                } else if (i7 == 0 && i5 != i8) {
                    return false;
                } else {
                    i7 = ((i7 * 10) + charAt) - 48;
                    if (i7 > 255) {
                        return false;
                    }
                    i8++;
                }
            }
            if (i8 - i5 == 0) {
                return false;
            }
            bArr[i6] = (byte) ((byte) i7);
            i5 = i8;
            i6++;
        }
        return i6 == i4 + 4;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return null;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.InetAddress m(java.lang.String r13, int r14, int r15) {
        /*
            r12 = 1
            r11 = 16
            r7 = -1
            r4 = 0
            r6 = 0
            byte[] r8 = new byte[r11]
            r5 = r7
            r1 = r7
            r2 = r6
            r0 = r14
        L_0x000c:
            if (r0 >= r15) goto L_0x0029
            if (r2 != r11) goto L_0x0012
            r0 = r4
        L_0x0011:
            return r0
        L_0x0012:
            int r3 = r0 + 2
            if (r3 > r15) goto L_0x0048
            java.lang.String r9 = "::"
            r10 = 2
            boolean r9 = r13.regionMatches(r0, r9, r6, r10)
            if (r9 == 0) goto L_0x0048
            if (r1 == r7) goto L_0x0023
            r0 = r4
            goto L_0x0011
        L_0x0023:
            int r0 = r2 + 2
            if (r3 != r15) goto L_0x002f
            r1 = r0
            r2 = r0
        L_0x0029:
            if (r2 == r11) goto L_0x0095
            if (r1 != r7) goto L_0x0088
            r0 = r4
            goto L_0x0011
        L_0x002f:
            r1 = r0
            r2 = r0
        L_0x0031:
            r0 = r3
            r5 = r6
        L_0x0033:
            if (r0 >= r15) goto L_0x003f
            char r9 = r13.charAt(r0)
            int r9 = k(r9)
            if (r9 != r7) goto L_0x006e
        L_0x003f:
            int r9 = r0 - r3
            if (r9 == 0) goto L_0x0046
            r10 = 4
            if (r9 <= r10) goto L_0x0074
        L_0x0046:
            r0 = r4
            goto L_0x0011
        L_0x0048:
            if (r2 == 0) goto L_0x0054
            java.lang.String r3 = ":"
            boolean r3 = r13.regionMatches(r0, r3, r6, r12)
            if (r3 == 0) goto L_0x0056
            int r0 = r0 + 1
        L_0x0054:
            r3 = r0
            goto L_0x0031
        L_0x0056:
            java.lang.String r3 = "."
            boolean r0 = r13.regionMatches(r0, r3, r6, r12)
            if (r0 == 0) goto L_0x006c
            int r0 = r2 + -2
            boolean r0 = l(r13, r5, r15, r8, r0)
            if (r0 != 0) goto L_0x0068
            r0 = r4
            goto L_0x0011
        L_0x0068:
            int r0 = r2 + 2
            r2 = r0
            goto L_0x0029
        L_0x006c:
            r0 = r4
            goto L_0x0011
        L_0x006e:
            int r5 = r5 << 4
            int r5 = r5 + r9
            int r0 = r0 + 1
            goto L_0x0033
        L_0x0074:
            int r9 = r2 + 1
            int r10 = r5 >>> 8
            r10 = r10 & 255(0xff, float:3.57E-43)
            byte r10 = (byte) r10
            byte r10 = (byte) r10
            r8[r2] = r10
            int r2 = r9 + 1
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r5 = (byte) r5
            byte r5 = (byte) r5
            r8[r9] = r5
            r5 = r3
            goto L_0x000c
        L_0x0088:
            int r0 = r2 - r1
            int r3 = 16 - r0
            java.lang.System.arraycopy(r8, r1, r8, r3, r0)
            int r0 = 16 - r2
            int r0 = r0 + r1
            java.util.Arrays.fill(r8, r1, r0, r6)
        L_0x0095:
            java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r8)     // Catch:{ UnknownHostException -> 0x009b }
            goto L_0x0011
        L_0x009b:
            r0 = move-exception
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.B28.m(java.lang.String, int, int):java.net.InetAddress");
    }

    @DexIgnore
    public static int n(String str, int i2, int i3, char c2) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str.charAt(i4) == c2) {
                return i4;
            }
        }
        return i3;
    }

    @DexIgnore
    public static int o(String str, int i2, int i3, String str2) {
        for (int i4 = i2; i4 < i3; i4++) {
            if (str2.indexOf(str.charAt(i4)) != -1) {
                return i4;
            }
        }
        return i3;
    }

    @DexIgnore
    public static boolean p(C58 c58, int i2, TimeUnit timeUnit) {
        try {
            return D(c58, i2, timeUnit);
        } catch (IOException e2) {
            return false;
        }
    }

    @DexIgnore
    public static boolean q(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public static String r(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    @DexIgnore
    public static String s(Q18 q18, boolean z) {
        String m2;
        if (q18.m().contains(":")) {
            m2 = "[" + q18.m() + "]";
        } else {
            m2 = q18.m();
        }
        if (!z && q18.z() == Q18.e(q18.E())) {
            return m2;
        }
        return m2 + ":" + q18.z();
    }

    @DexIgnore
    public static <T> List<T> t(List<T> list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    @DexIgnore
    public static <T> List<T> u(T... tArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) tArr.clone()));
    }

    @DexIgnore
    public static <K, V> Map<K, V> v(Map<K, V> map) {
        return map.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(new LinkedHashMap(map));
    }

    @DexIgnore
    public static int w(Comparator<String> comparator, String[] strArr, String str) {
        int length = strArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (comparator.compare(strArr[i2], str) == 0) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public static int x(String str) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            char charAt = str.charAt(i2);
            if (charAt <= 31 || charAt >= '\u007f') {
                return i2;
            }
            i2++;
        }
        return -1;
    }

    @DexIgnore
    public static String y(byte[] bArr) {
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        int i5 = 0;
        while (i5 < bArr.length) {
            int i6 = i5;
            while (i6 < 16 && bArr[i6] == 0 && bArr[i6 + 1] == 0) {
                i6 += 2;
            }
            int i7 = i6 - i5;
            if (i7 > i4 && i7 >= 4) {
                i4 = i7;
                i3 = i5;
            }
            i5 = i6 + 2;
        }
        I48 i48 = new I48();
        while (i2 < bArr.length) {
            if (i2 == i3) {
                i48.w0(58);
                i2 += i4;
                if (i2 == 16) {
                    i48.w0(58);
                }
            } else {
                if (i2 > 0) {
                    i48.w0(58);
                }
                i48.y0((long) (((bArr[i2] & 255) << 8) | (bArr[i2 + 1] & 255)));
                i2 += 2;
            }
        }
        return i48.b0();
    }

    @DexIgnore
    public static String[] z(Comparator<? super String> comparator, String[] strArr, String[] strArr2) {
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            int length = strArr2.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                } else if (comparator.compare(str, strArr2[i2]) == 0) {
                    arrayList.add(str);
                    break;
                } else {
                    i2++;
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }
}
