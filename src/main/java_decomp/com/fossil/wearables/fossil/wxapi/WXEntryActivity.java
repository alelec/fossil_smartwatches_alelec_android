package com.fossil.wearables.fossil.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Df7;
import com.fossil.Ef7;
import com.fossil.Q47;
import com.fossil.Tf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WXEntryActivity extends Activity implements Tf7 {
    @DexIgnore
    @Override // com.fossil.Tf7
    public void a(Df7 df7) {
        Q47.i().a(df7);
        finish();
    }

    @DexIgnore
    @Override // com.fossil.Tf7
    public void b(Ef7 ef7) {
        Q47.i().b(ef7);
        finish();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Q47.i().c(getIntent(), this);
    }

    @DexIgnore
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Q47.i().c(getIntent(), this);
    }
}
