package com.fossil.wearables.fsl.history;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HistoryItem extends BaseModel {
    @DexIgnore
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    public Serializable data;
    @DexIgnore
    @DatabaseField
    public long timestamp;
    @DexIgnore
    @DatabaseField
    public String timezone;
    @DexIgnore
    @DatabaseField
    public String type;

    @DexIgnore
    public HistoryItem() {
    }

    @DexIgnore
    public HistoryItem(String str, long j, String str2, Serializable serializable) {
        this.type = str;
        this.timestamp = j;
        this.timezone = str2;
        this.data = serializable;
    }

    @DexIgnore
    public Serializable getData() {
        return this.data;
    }

    @DexIgnore
    public long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public String getTimezone() {
        return this.timezone;
    }

    @DexIgnore
    public String getType() {
        return this.type;
    }

    @DexIgnore
    public void setData(Serializable serializable) {
        this.data = serializable;
    }

    @DexIgnore
    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public void setTimezone(String str) {
        this.timezone = str;
    }

    @DexIgnore
    public void setType(String str) {
        this.type = str;
    }
}
