package com.fossil.wearables.fsl.goaltracking;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTrackingProviderImpl extends BaseDbProvider implements GoalTrackingProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "goal_tracking.db";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE goaltrackingevent ADD COLUMN counter INTEGER DEFAULT 1");
                sQLiteDatabase.execSQL("ALTER TABLE goaltrackingevent ADD COLUMN autoDetected INTEGER");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon2_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon2_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE goaltrackingevent ADD COLUMN uri VARCHAR PRIMARY KEY");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon3_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon3_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE goalphase ADD COLUMN startDay VARCHAR");
                sQLiteDatabase.execSQL("ALTER TABLE goalphase ADD COLUMN endDay VARCHAR");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon4_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon4_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE goaltracking ADD COLUMN pinType INTEGER DEFAULT 0");
                sQLiteDatabase.execSQL("ALTER TABLE goaltrackingevent ADD COLUMN pinType INTEGER DEFAULT 0");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
            put(3, new Anon2_Level2());
            put(4, new Anon3_Level2());
            put(5, new Anon4_Level2());
        }
    }

    @DexIgnore
    public GoalTrackingProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public void clearData() {
        try {
            TableUtils.clearTable(this.databaseHelper.getConnectionSource(), GoalTrackingEvent.class);
            TableUtils.clearTable(this.databaseHelper.getConnectionSource(), GoalTrackingSummary.class);
            TableUtils.clearTable(this.databaseHelper.getConnectionSource(), GoalPhase.class);
            TableUtils.clearTable(this.databaseHelper.getConnectionSource(), GoalTracking.class);
        } catch (Exception e) {
            try {
                TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTracking.class);
                TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalPhase.class);
                TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTrackingSummary.class);
                TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTrackingEvent.class);
            } catch (Exception e2) {
            }
            Log.e(this.TAG, "Failed to clear data");
        }
    }

    @DexIgnore
    public void deleteAllGoalPhases(long j) {
        try {
            DeleteBuilder<GoalPhase, Long> deleteBuilder = getGoalPhaseDao().deleteBuilder();
            deleteBuilder.where().eq("goalId", Long.valueOf(j));
            getGoalPhaseDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".delete all GoalPhase of GoalTracking, id=" + j + " - e=" + e);
        }
    }

    @DexIgnore
    public void deleteGoalTrackingSummary(long j) {
        try {
            DeleteBuilder<GoalTrackingSummary, Long> deleteBuilder = getGoalTrackingSummaryDao().deleteBuilder();
            deleteBuilder.where().eq("goalId", Long.valueOf(j));
            getGoalTrackingSummaryDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".deleteGoalTrackingSummary - e=" + e);
        }
    }

    @DexIgnore
    public void disposeGoalTrackingFromDB(long j) {
        if (getGoalTrackingsByIdIncludeDeleted(j) != null) {
            try {
                DeleteBuilder<GoalTracking, Long> deleteBuilder = getGoalTrackingDao().deleteBuilder();
                deleteBuilder.where().eq("id", Long.valueOf(j));
                getGoalTrackingDao().delete(deleteBuilder.prepare());
            } catch (Exception e) {
                String str = this.TAG;
                Log.e(str, "Error inside " + this.TAG + ".dispose GoalTracking (X), goalId=" + j + " - e=" + e);
            }
            try {
                DeleteBuilder<GoalPhase, Long> deleteBuilder2 = getGoalPhaseDao().deleteBuilder();
                deleteBuilder2.where().eq("goalId", Long.valueOf(j));
                getGoalPhaseDao().delete(deleteBuilder2.prepare());
            } catch (Exception e2) {
                String str2 = this.TAG;
                Log.e(str2, "Error inside " + this.TAG + ".dispose GoalTracking Phases (X), goalId = " + j + " - e=" + e2);
            }
            try {
                DeleteBuilder<GoalTrackingEvent, Long> deleteBuilder3 = getGoalTrackingEventDao().deleteBuilder();
                deleteBuilder3.where().eq("goalId", Long.valueOf(j));
                getGoalTrackingEventDao().delete(deleteBuilder3.prepare());
            } catch (Exception e3) {
                String str3 = this.TAG;
                Log.e(str3, "Error inside " + this.TAG + ".dispose GoalTracking Events (X), goalId = " + j + " - e=" + e3);
            }
            try {
                DeleteBuilder<GoalTrackingSummary, Long> deleteBuilder4 = getGoalTrackingSummaryDao().deleteBuilder();
                deleteBuilder4.where().eq("goalId", Long.valueOf(j));
                getGoalTrackingSummaryDao().delete(deleteBuilder4.prepare());
            } catch (Exception e4) {
                String str4 = this.TAG;
                Log.e(str4, "Error inside " + this.TAG + ".dispose GoalTracking Summaries (X), goalId = " + j + " - e=" + e4);
            }
        }
    }

    @DexIgnore
    public void disposeGoalTrackingFromDB(String str) {
        GoalTracking goalTrackingsByServerIdIncludeDeleted = getGoalTrackingsByServerIdIncludeDeleted(str);
        if (goalTrackingsByServerIdIncludeDeleted != null) {
            long id = goalTrackingsByServerIdIncludeDeleted.getId();
            try {
                DeleteBuilder<GoalTracking, Long> deleteBuilder = getGoalTrackingDao().deleteBuilder();
                deleteBuilder.where().eq("serverId", str);
                getGoalTrackingDao().delete(deleteBuilder.prepare());
            } catch (Exception e) {
                String str2 = this.TAG;
                Log.e(str2, "Error inside " + this.TAG + ".dispose GoalTracking (X), serverId=" + str + " - e=" + e);
            }
            try {
                DeleteBuilder<GoalPhase, Long> deleteBuilder2 = getGoalPhaseDao().deleteBuilder();
                deleteBuilder2.where().eq("goalId", Long.valueOf(id));
                getGoalPhaseDao().delete(deleteBuilder2.prepare());
            } catch (Exception e2) {
                String str3 = this.TAG;
                Log.e(str3, "Error inside " + this.TAG + ".dispose GoalTracking Phases (X), serverId=" + str + ", goalId = " + id + " - e=" + e2);
            }
            try {
                DeleteBuilder<GoalTrackingEvent, Long> deleteBuilder3 = getGoalTrackingEventDao().deleteBuilder();
                deleteBuilder3.where().eq("goalId", Long.valueOf(id));
                getGoalTrackingEventDao().delete(deleteBuilder3.prepare());
            } catch (Exception e3) {
                String str4 = this.TAG;
                Log.e(str4, "Error inside " + this.TAG + ".dispose GoalTracking Events (X), serverId=" + str + ", goalId = " + id + " - e=" + e3);
            }
            try {
                DeleteBuilder<GoalTrackingSummary, Long> deleteBuilder4 = getGoalTrackingSummaryDao().deleteBuilder();
                deleteBuilder4.where().eq("goalId", Long.valueOf(id));
                getGoalTrackingSummaryDao().delete(deleteBuilder4.prepare());
            } catch (Exception e4) {
                String str5 = this.TAG;
                Log.e(str5, "Error inside " + this.TAG + ".dispose GoalTracking Summaries (X), serverId=" + str + ", goalId = " + id + " - e=" + e4);
            }
        }
    }

    @DexIgnore
    public void dropThenCreateTables() {
        try {
            this.databaseHelper.dropTables(getDbEntities());
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTracking.class);
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalPhase.class);
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTrackingSummary.class);
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), GoalTrackingEvent.class);
        } catch (Exception e) {
            Log.e(this.TAG, "Failed to drop tables");
        }
    }

    @DexIgnore
    public GoalTracking getActiveGoalTracking() {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(GoalStatus.ACTIVE.getValue()));
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getActiveGoalTracking - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<GoalTracking> getAllGoalTrackings() {
        try {
            return getGoalTrackingDao().queryForAll();
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{GoalTracking.class, GoalPhase.class, GoalTrackingEvent.class, GoalTrackingSummary.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 5;
    }

    @DexIgnore
    public GoalPhase getGoalPhase(long j) {
        Exception e;
        GoalPhase goalPhase;
        GoalPhase goalPhase2 = null;
        try {
            QueryBuilder<GoalPhase, Long> queryBuilder = getGoalPhaseDao().queryBuilder();
            Where<GoalPhase, Long> where = queryBuilder.where();
            where.or(where.ge(GoalPhase.COLUMN_END_DATE, Long.valueOf(j)), where.eq(GoalPhase.COLUMN_END_DATE, 0), new Where[0]);
            where.and().le(GoalPhase.COLUMN_START_DATE, Long.valueOf(j));
            queryBuilder.setWhere(where);
            List<GoalPhase> query = getGoalPhaseDao().query(queryBuilder.prepare());
            if (query == null || query.isEmpty()) {
                return null;
            }
            if (query.size() == 1) {
                return query.get(0);
            }
            Iterator<GoalPhase> it = query.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                GoalPhase next = it.next();
                if (next.getGoalTracking().getStatus() == GoalStatus.ACTIVE) {
                    goalPhase2 = next;
                    break;
                }
            }
            if (goalPhase2 == null) {
                try {
                    if (query.size() > 0) {
                        return query.get(0);
                    }
                } catch (Exception e2) {
                    e = e2;
                    goalPhase = goalPhase2;
                    Log.e(this.TAG, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
                    return goalPhase;
                }
            }
            return goalPhase2;
        } catch (Exception e3) {
            e = e3;
            goalPhase = null;
            Log.e(this.TAG, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
            return goalPhase;
        }
    }

    @DexIgnore
    public Dao<GoalPhase, Long> getGoalPhaseDao() throws SQLException {
        return this.databaseHelper.getDao(GoalPhase.class);
    }

    @DexIgnore
    public List<GoalPhase> getGoalPhases(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalPhase, Long> queryBuilder = getGoalPhaseDao().queryBuilder();
            queryBuilder.where().eq("goalId", Long.valueOf(j));
            queryBuilder.orderBy(GoalPhase.COLUMN_START_DATE, false);
            List<GoalPhase> query = getGoalPhaseDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalPhases - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public List<GoalPhase> getGoalPhases(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalPhase, Long> queryBuilder = getGoalPhaseDao().queryBuilder();
            queryBuilder.where().between(GoalPhase.COLUMN_START_DATE, Long.valueOf(j), Long.valueOf(j2));
            queryBuilder.orderBy(GoalPhase.COLUMN_START_DATE, false);
            List<GoalPhase> query = getGoalPhaseDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalPhases - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public GoalTracking getGoalTracking(long j) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("id", Long.valueOf(j)).and().ne("pinType", 3);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public GoalTracking getGoalTracking(String str) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("uri", str).and().ne("pinType", 3);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public GoalTracking getGoalTracking(String str, Frequency frequency, int i) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().like("name", str.trim()).and().eq("frequency", Integer.valueOf(frequency.getValue())).and().eq("target", Integer.valueOf(i)).and().gt("status", Integer.valueOf(GoalStatus.REMOVED.getValue())).and().ne("pinType", 3);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.orderBy("updatedAt", false);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<GoalTracking> getGoalTracking(GoalStatus goalStatus) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(goalStatus.getValue())).and().ne("pinType", 3);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTracking - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public GoalTracking getGoalTrackingByServerId(String str) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("serverId", str).and().ne("pinType", 3);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getGoalTrackingByServerId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public Dao<GoalTracking, Long> getGoalTrackingDao() throws SQLException {
        return this.databaseHelper.getDao(GoalTracking.class);
    }

    @DexIgnore
    public SparseArray<List<GoalTrackingEvent>> getGoalTrackingEvent(int i, int i2, long j) {
        SparseArray<List<GoalTrackingEvent>> sparseArray = new SparseArray<>();
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().between("date", Integer.valueOf(i), Integer.valueOf(i2)).and().eq("goalId", Long.valueOf(j));
            queryBuilder.orderBy("date", false);
            List<GoalTrackingEvent> query = getGoalTrackingEventDao().query(queryBuilder.prepare());
            if (query != null) {
                for (GoalTrackingEvent goalTrackingEvent : query) {
                    List<GoalTrackingEvent> list = sparseArray.get(goalTrackingEvent.date);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(goalTrackingEvent);
                    sparseArray.put(goalTrackingEvent.date, list);
                }
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingEvent - e=" + e);
        }
        return sparseArray;
    }

    @DexIgnore
    public SparseArray<List<GoalTrackingEvent>> getGoalTrackingEvent(long j) {
        SparseArray<List<GoalTrackingEvent>> sparseArray = new SparseArray<>();
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().eq("goalId", Long.valueOf(j));
            queryBuilder.orderBy("date", false);
            List<GoalTrackingEvent> query = getGoalTrackingEventDao().query(queryBuilder.prepare());
            if (query != null) {
                for (GoalTrackingEvent goalTrackingEvent : query) {
                    List<GoalTrackingEvent> list = sparseArray.get(goalTrackingEvent.date);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(goalTrackingEvent);
                    sparseArray.put(goalTrackingEvent.date, list);
                }
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingEvent - e=" + e);
        }
        return sparseArray;
    }

    @DexIgnore
    public List<GoalTrackingEvent> getGoalTrackingEventByTrackingTimes(List<Long> list) {
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().in(GoalTrackingEvent.COLUMN_TRACKED_AT, list);
            queryBuilder.orderBy(GoalTrackingEvent.COLUMN_TRACKED_AT, true);
            return queryBuilder.query();
        } catch (SQLException e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".get getGoalTrackingEventByTrackingTimes, e = " + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    public Dao<GoalTrackingEvent, Long> getGoalTrackingEventDao() throws SQLException {
        return this.databaseHelper.getDao(GoalTrackingEvent.class);
    }

    @DexIgnore
    public List<Integer> getGoalTrackingEventDates(int i, int i2, long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().between("date", Integer.valueOf(i), Integer.valueOf(i2)).and().eq("goalId", Long.valueOf(j));
            queryBuilder.orderBy("date", false);
            queryBuilder.groupBy("date");
            List<GoalTrackingEvent> query = getGoalTrackingEventDao().query(queryBuilder.prepare());
            if (query != null) {
                for (GoalTrackingEvent goalTrackingEvent : query) {
                    arrayList.add(Integer.valueOf(goalTrackingEvent.getDate()));
                }
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingEvent - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public List<Integer> getGoalTrackingEventDates(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().eq("goalId", Long.valueOf(j));
            queryBuilder.orderBy("date", false);
            queryBuilder.groupBy("date");
            List<GoalTrackingEvent> query = getGoalTrackingEventDao().query(queryBuilder.prepare());
            if (query != null) {
                for (GoalTrackingEvent goalTrackingEvent : query) {
                    arrayList.add(Integer.valueOf(goalTrackingEvent.getDate()));
                }
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingEvent - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummaries(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalTrackingSummary, Long> queryBuilder = getGoalTrackingSummaryDao().queryBuilder();
            queryBuilder.where().between("date", Long.valueOf(j), Long.valueOf(j2));
            queryBuilder.orderBy("date", false);
            List<GoalTrackingSummary> query = getGoalTrackingSummaryDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingSummaries - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public List<GoalTrackingSummary> getGoalTrackingSummariesPaging(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<GoalTrackingSummary, Long> queryBuilder = getGoalTrackingSummaryDao().queryBuilder();
            queryBuilder.orderBy("date", false);
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.offset(Long.valueOf(j));
            queryBuilder.limit(Long.valueOf(j2));
            List<GoalTrackingSummary> query = getGoalTrackingSummaryDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingSummariesFromGoalId - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public GoalTrackingSummary getGoalTrackingSummary(long j) {
        try {
            QueryBuilder<GoalTrackingSummary, Long> queryBuilder = getGoalTrackingSummaryDao().queryBuilder();
            queryBuilder.where().eq("goalId", Long.valueOf(j));
            List<GoalTrackingSummary> query = getGoalTrackingSummaryDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingSummary - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public Dao<GoalTrackingSummary, Long> getGoalTrackingSummaryDao() throws SQLException {
        return this.databaseHelper.getDao(GoalTrackingSummary.class);
    }

    @DexIgnore
    public GoalTracking getGoalTrackingsByIdIncludeDeleted(long j) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("id", Long.valueOf(j));
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getGoalTrackingByServerId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public GoalTracking getGoalTrackingsByServerIdIncludeDeleted(String str) {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().eq("serverId", str);
            List<GoalTracking> query = getGoalTrackingDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getGoalTrackingByServerId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public GoalTracking getLatestEndedGoalTracking() {
        GoalPhase goalPhase;
        try {
            QueryBuilder<GoalPhase, Long> queryBuilder = getGoalPhaseDao().queryBuilder();
            queryBuilder.where().ne("pinType", 3);
            queryBuilder.orderBy(GoalPhase.COLUMN_END_DATE, false);
            goalPhase = queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".get GoalPhase which has latest end.");
            goalPhase = null;
        }
        if (goalPhase != null) {
            return goalPhase.getGoalTracking();
        }
        return null;
    }

    @DexIgnore
    public GoalTracking getOldedGoalTracking() {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().ne("pinType", 3);
            queryBuilder.orderBy("createdAt", true);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getOldedGoalTracking - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<GoalTracking> getPendingGoalTracking() {
        try {
            QueryBuilder<GoalTracking, Long> queryBuilder = getGoalTrackingDao().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            return getGoalTrackingDao().query(queryBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getPendingGoalTracking - e=" + e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public List<GoalTrackingEvent> getPendingGoalTrackingEvents() {
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            return getGoalTrackingEventDao().query(queryBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getPendingGoalTrackingEvents - e=" + e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public void saveGoalPhase(GoalPhase goalPhase) {
        try {
            QueryBuilder<GoalPhase, Long> queryBuilder = getGoalPhaseDao().queryBuilder();
            queryBuilder.where().eq(GoalPhase.COLUMN_START_DATE, Long.valueOf(goalPhase.startDate)).and().eq(GoalPhase.COLUMN_END_DATE, Long.valueOf(goalPhase.endDate)).and().eq("goalId", Long.valueOf(goalPhase.goal.id));
            GoalPhase goalPhase2 = null;
            List<GoalPhase> query = getGoalPhaseDao().query(queryBuilder.prepare());
            if (query != null && query.size() > 0) {
                goalPhase2 = query.get(0);
            }
            if (goalPhase2 != null) {
                goalPhase.setId(goalPhase2.getId());
            }
            getGoalPhaseDao().createOrUpdate(goalPhase);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".saveGoalPhase - e=" + e);
        }
    }

    @DexIgnore
    public void saveGoalTracking(GoalTracking goalTracking) {
        try {
            GoalTracking goalTracking2 = getGoalTracking(goalTracking.uri);
            if (goalTracking2 == null) {
                goalTracking2 = getGoalTrackingByServerId(goalTracking.getServerId());
            }
            if (goalTracking2 != null) {
                if (goalTracking.getId() > 0) {
                    goalTracking.setUpdatedAt(System.currentTimeMillis());
                } else if (TextUtils.isEmpty(goalTracking.getServerId())) {
                    goalTracking.setUpdatedAt(System.currentTimeMillis());
                }
                goalTracking.setId(goalTracking2.getId());
                getGoalTrackingDao().update((Dao<GoalTracking, Long>) goalTracking);
                return;
            }
            if (TextUtils.isEmpty(goalTracking.getServerId())) {
                goalTracking.setCreatedAt(System.currentTimeMillis());
            }
            getGoalTrackingDao().create((Dao<GoalTracking, Long>) goalTracking);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".saveGoalTracking - e=" + e);
        }
    }

    @DexIgnore
    public void saveGoalTrackingEvent(GoalTrackingEvent goalTrackingEvent) {
        try {
            goalTrackingEvent.setTrackedAt((goalTrackingEvent.getTrackedAt() / 1000) * 1000);
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().eq(GoalTrackingEvent.COLUMN_TRACKED_AT, Long.valueOf(goalTrackingEvent.trackedAt)).and().eq("goalId", Long.valueOf(goalTrackingEvent.goalTracking.id));
            GoalTrackingEvent goalTrackingEvent2 = null;
            List<GoalTrackingEvent> query = getGoalTrackingEventDao().query(queryBuilder.prepare());
            if (query != null && query.size() > 0) {
                goalTrackingEvent2 = query.get(0);
            }
            if (goalTrackingEvent2 == null) {
                getGoalTrackingEventDao().create((Dao<GoalTrackingEvent, Long>) goalTrackingEvent);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".saveGoalTrackingEvent - e=" + e);
        }
    }

    @DexIgnore
    public void saveGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        try {
            GoalTrackingSummary goalTrackingSummary2 = getGoalTrackingSummary(goalTrackingSummary.getGoalTracking().id);
            if (goalTrackingSummary2 == null) {
                goalTrackingSummary.setCreatedAt(System.currentTimeMillis());
            } else {
                goalTrackingSummary.setId(goalTrackingSummary2.getId());
                goalTrackingSummary.setGoalTracking(goalTrackingSummary2.getGoalTracking());
                goalTrackingSummary.setCreatedAt(goalTrackingSummary2.getCreatedAt());
                goalTrackingSummary.setUpdatedAt(System.currentTimeMillis());
            }
            getGoalTrackingSummaryDao().createOrUpdate(goalTrackingSummary);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".saveGoalTrackingSummary - e=" + e);
        }
    }

    @DexIgnore
    public void setGoalTrackingStatus(String str, GoalStatus goalStatus) {
        GoalTracking goalTracking = getGoalTracking(str);
        if (goalTracking != null) {
            goalTracking.setStatus(goalStatus);
            saveGoalTracking(goalTracking);
        }
    }

    @DexIgnore
    public void updateGoalTrackingEvent(GoalTrackingEvent goalTrackingEvent) {
        try {
            QueryBuilder<GoalTrackingEvent, Long> queryBuilder = getGoalTrackingEventDao().queryBuilder();
            queryBuilder.where().eq(GoalTrackingEvent.COLUMN_TRACKED_AT, Long.valueOf(goalTrackingEvent.getTrackedAt())).and().eq("goalId", Long.valueOf(goalTrackingEvent.getGoalTracking().getId()));
            if (getGoalTrackingEventDao().queryForFirst(queryBuilder.prepare()) != null) {
                getGoalTrackingEventDao().update((Dao<GoalTrackingEvent, Long>) goalTrackingEvent);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".updateGoalTrackingEvent - e=" + e);
        }
    }
}
