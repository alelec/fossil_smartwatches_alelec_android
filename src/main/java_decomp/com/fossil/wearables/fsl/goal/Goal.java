package com.fossil.wearables.fsl.goal;

import android.text.TextUtils;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Goal extends BaseFeatureModel {
    @DexIgnore
    @DatabaseField
    public String description;
    @DexIgnore
    @DatabaseField
    public long endDate;
    @DexIgnore
    @DatabaseField
    public long intervalDuration;
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<GoalInterval> intervals;
    @DexIgnore
    @DatabaseField
    public double latitude;
    @DexIgnore
    @DatabaseField
    public double longitude;
    @DexIgnore
    @DatabaseField
    public int reminderHour;
    @DexIgnore
    @DatabaseField
    public int reminderMinute;
    @DexIgnore
    @DatabaseField
    public long startDate;
    @DexIgnore
    @DatabaseField
    public int targetValue;
    @DexIgnore
    @DatabaseField
    public String type;

    @DexIgnore
    private long getTotalIntervalValue() {
        ForeignCollection<GoalInterval> foreignCollection = this.intervals;
        long j = 0;
        if (foreignCollection == null || foreignCollection.size() <= 0) {
            return 0;
        }
        Iterator<E> it = this.intervals.iterator();
        while (it.hasNext()) {
            j = ((long) it.next().getValue()) + j;
        }
        return j;
    }

    @DexIgnore
    private long getTotalTargetValue() {
        return ((long) this.targetValue) * getIntervalCount();
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public long getEndDate() {
        return this.endDate;
    }

    @DexIgnore
    public GoalInterval getInterval(int i) {
        GoalInterval goalInterval = new GoalInterval();
        goalInterval.setGoal(this);
        goalInterval.setIndex(i);
        ForeignCollection<GoalInterval> foreignCollection = this.intervals;
        if (foreignCollection != null && foreignCollection.size() > 0) {
            for (E e : this.intervals) {
                if (e.getIndex() == i) {
                    return e;
                }
            }
        }
        return goalInterval;
    }

    @DexIgnore
    public long getIntervalCount() {
        if (this.intervalDuration != 0) {
            return getTotalDuration() / this.intervalDuration;
        }
        return 0;
    }

    @DexIgnore
    public long getIntervalDuration() {
        return this.intervalDuration;
    }

    @DexIgnore
    public int getIntervalIndex(long j) {
        return (int) (Math.floor((double) (getTotalDuration() / (j - this.startDate))) - 1.0d);
    }

    @DexIgnore
    public List<GoalInterval> getIntervals() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<GoalInterval> foreignCollection = this.intervals;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.intervals);
    }

    @DexIgnore
    public double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public double getProgress() {
        return ((double) getTotalIntervalValue()) / ((double) getTotalTargetValue());
    }

    @DexIgnore
    public int getReminderHour() {
        return this.reminderHour;
    }

    @DexIgnore
    public int getReminderMinute() {
        return this.reminderMinute;
    }

    @DexIgnore
    public long getStartDate() {
        return this.startDate;
    }

    @DexIgnore
    public int getTargetValue() {
        return this.targetValue;
    }

    @DexIgnore
    public long getTotalDuration() {
        return this.endDate - this.startDate;
    }

    @DexIgnore
    public GoalType getType() {
        if (!TextUtils.isEmpty(this.type)) {
            return GoalType.valueOf(this.type);
        }
        return null;
    }

    @DexIgnore
    public boolean meetMyGoal() {
        return Math.round(getProgress() * 100.0d) == 100;
    }

    @DexIgnore
    public void printDebugInfo() {
        String str = this.TAG;
        Log.i(str, "Goal name: " + getName() + "\n Total Duration (in days): " + TimeUnit.MILLISECONDS.toDays(getTotalDuration()) + "\n Interval Duration (in days): " + TimeUnit.MILLISECONDS.toDays(getIntervalDuration()) + "\n Interval Count: " + getIntervalCount() + "\n Target Value: " + getTargetValue() + "\n Total Target Value (interval count * target value): " + getTotalTargetValue() + "\n Total Interval Value (all interval values added up): " + getTotalIntervalValue() + "\n Overall Goal Progress: " + getProgress());
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setIntervalDuration(long j) {
        this.intervalDuration = j;
    }

    @DexIgnore
    public void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public void setLongitude(double d) {
        this.longitude = d;
    }

    @DexIgnore
    public void setReminderHour(int i) {
        this.reminderHour = i;
    }

    @DexIgnore
    public void setReminderMinute(int i) {
        this.reminderMinute = i;
    }

    @DexIgnore
    public void setStartDate(long j) {
        this.startDate = j;
    }

    @DexIgnore
    public void setTargetValue(int i) {
        this.targetValue = i;
    }

    @DexIgnore
    public void setTotalDuration(long j) {
        this.endDate = this.startDate + j;
    }

    @DexIgnore
    public void setType(GoalType goalType) {
        this.type = goalType.name();
    }
}
