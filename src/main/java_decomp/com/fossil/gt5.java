package com.fossil;

import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Gt5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[ServiceActionResult.values().length];
        a = iArr;
        iArr[ServiceActionResult.GET_LATEST_FW.ordinal()] = 1;
        a[ServiceActionResult.LATEST_FW.ordinal()] = 2;
        a[ServiceActionResult.UPDATE_FW_SUCCESS.ordinal()] = 3;
        a[ServiceActionResult.UPDATE_FW_FAILED.ordinal()] = 4;
        a[ServiceActionResult.START_TIMER.ordinal()] = 5;
        a[ServiceActionResult.AUTHORIZE_DEVICE_SUCCESS.ordinal()] = 6;
        a[ServiceActionResult.ASK_FOR_LINK_SERVER.ordinal()] = 7;
        a[ServiceActionResult.ASK_FOR_LABEL_FILE.ordinal()] = 8;
        a[ServiceActionResult.SUCCEEDED.ordinal()] = 9;
        a[ServiceActionResult.FAILED.ordinal()] = 10;
    }
    */
}
