package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C31 {
    @DexIgnore
    public String a;
    @DexIgnore
    public Long b;

    @DexIgnore
    public C31(String str, long j) {
        this.a = str;
        this.b = Long.valueOf(j);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C31(String str, boolean z) {
        this(str, z ? 1 : 0);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C31)) {
            return false;
        }
        C31 c31 = (C31) obj;
        if (!this.a.equals(c31.a)) {
            return false;
        }
        Long l = this.b;
        Long l2 = c31.b;
        return l != null ? l.equals(l2) : l2 == null;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        Long l = this.b;
        return (l != null ? l.hashCode() : 0) + (hashCode * 31);
    }
}
