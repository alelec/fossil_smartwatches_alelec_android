package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 c;

    @DexIgnore
    public Jp3(Fp3 fp3, Or3 or3) {
        this.c = fp3;
        this.b = or3;
    }

    @DexIgnore
    public final void run() {
        Cl3 cl3 = this.c.d;
        if (cl3 == null) {
            this.c.d().F().a("Failed to reset data on the service: not connected to service");
            return;
        }
        try {
            cl3.S0(this.b);
        } catch (RemoteException e) {
            this.c.d().F().b("Failed to reset data on the service: remote exception", e);
        }
        this.c.e0();
    }
}
