package com.fossil;

import android.graphics.RectF;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zz3 {
    @DexIgnore
    public abstract void a(I04 i04, float f, float f2, float f3);

    @DexIgnore
    public void b(I04 i04, float f, float f2, RectF rectF, Yz3 yz3) {
        a(i04, f, f2, yz3.a(rectF));
    }
}
