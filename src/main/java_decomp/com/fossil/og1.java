package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Og1 implements Id1<BitmapDrawable>, Ed1 {
    @DexIgnore
    public /* final */ Resources b;
    @DexIgnore
    public /* final */ Id1<Bitmap> c;

    @DexIgnore
    public Og1(Resources resources, Id1<Bitmap> id1) {
        Ik1.d(resources);
        this.b = resources;
        Ik1.d(id1);
        this.c = id1;
    }

    @DexIgnore
    public static Id1<BitmapDrawable> f(Resources resources, Id1<Bitmap> id1) {
        if (id1 == null) {
            return null;
        }
        return new Og1(resources, id1);
    }

    @DexIgnore
    @Override // com.fossil.Ed1
    public void a() {
        Id1<Bitmap> id1 = this.c;
        if (id1 instanceof Ed1) {
            ((Ed1) id1).a();
        }
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
        this.c.b();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return this.c.c();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<BitmapDrawable> d() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    public BitmapDrawable e() {
        return new BitmapDrawable(this.b, this.c.get());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Id1
    public /* bridge */ /* synthetic */ BitmapDrawable get() {
        return e();
    }
}
