package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q67 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public Q67(float f, float f2) {
        this.a = f;
        this.b = f2;
    }

    @DexIgnore
    public final float a(Q67 q67) {
        Wg6.c(q67, "that");
        float f = this.a - q67.a;
        float f2 = this.b - q67.b;
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Q67) {
                Q67 q67 = (Q67) obj;
                if (!(Float.compare(this.a, q67.a) == 0 && Float.compare(this.b, q67.b) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (Float.floatToIntBits(this.a) * 31) + Float.floatToIntBits(this.b);
    }

    @DexIgnore
    public String toString() {
        return "Point(x=" + this.a + ", y=" + this.b + ")";
    }
}
