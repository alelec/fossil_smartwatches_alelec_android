package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tr2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Tr2> CREATOR; // = new Ur2();
    @DexIgnore
    public static /* final */ List<Zb2> e; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ Za3 f; // = new Za3();
    @DexIgnore
    public Za3 b;
    @DexIgnore
    public List<Zb2> c;
    @DexIgnore
    public String d;

    @DexIgnore
    public Tr2(Za3 za3, List<Zb2> list, String str) {
        this.b = za3;
        this.c = list;
        this.d = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Tr2)) {
            return false;
        }
        Tr2 tr2 = (Tr2) obj;
        return Pc2.a(this.b, tr2.b) && Pc2.a(this.c, tr2.c) && Pc2.a(this.d, tr2.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, this.b, i, false);
        Bd2.y(parcel, 2, this.c, false);
        Bd2.u(parcel, 3, this.d, false);
        Bd2.b(parcel, a2);
    }
}
