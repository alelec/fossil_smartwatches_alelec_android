package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.watchface.Background;
import com.misfit.frameworks.buttonservice.model.watchface.ComplicationData;
import com.misfit.frameworks.buttonservice.model.watchface.MetricObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingObject;
import com.misfit.frameworks.buttonservice.model.watchface.MovingType;
import com.misfit.frameworks.buttonservice.model.watchface.TextData;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeColour;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchface.TickerData;
import com.misfit.frameworks.buttonservice.model.watchface.TimeZoneData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cc7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<Lc6<? extends T, ? extends T>, Boolean> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(Object obj) {
            return Boolean.valueOf(invoke((Lc6) obj));
        }

        @DexIgnore
        public final boolean invoke(Lc6<? extends T, ? extends T> lc6) {
            Wg6.c(lc6, "<name for destructuring parameter 0>");
            return Wg6.a(lc6.component1(), lc6.component2());
        }
    }

    @DexIgnore
    public static final <T> boolean a(List<? extends T> list, List<? extends T> list2) {
        Wg6.c(list, "$this$deepEqualTo");
        Wg6.c(list2, FacebookRequestErrorClassification.KEY_OTHER);
        return list == list2 || (list.size() == list2.size() && !At7.f(At7.o(At7.v(Pm7.z(list), Pm7.z(list2)), Ai.INSTANCE), Boolean.FALSE));
    }

    @DexIgnore
    public static final Background b(Hb7 hb7) {
        Wg6.c(hb7, "$this$toButtonBackground");
        return new Background(hb7.a(), hb7.b());
    }

    @DexIgnore
    public static final ThemeColour c(Nb7 nb7) {
        Wg6.c(nb7, "$this$toButtonColour");
        return ThemeColour.valueOf(nb7.name());
    }

    @DexIgnore
    public static final ComplicationData d(Ib7 ib7) {
        Wg6.c(ib7, "$this$toButtonComplicationData");
        if (ib7.e() == null && ib7.g() == null) {
            return new ComplicationData(ib7.h(), ib7.f(), ib7.d(), null, c(ib7.c()), 8, null);
        }
        byte[] h = ib7.h();
        String f = ib7.f();
        boolean d = ib7.d();
        String e = ib7.e();
        String str = e != null ? e : "";
        Integer g = ib7.g();
        return new ComplicationData(h, f, d, new TimeZoneData(str, g != null ? g.intValue() : 1), c(ib7.c()));
    }

    @DexIgnore
    public static final MetricObject e(Jb7 jb7) {
        Wg6.c(jb7, "$this$toButtonMetric");
        return new MetricObject(jb7.c(), jb7.d(), jb7.b(), jb7.a());
    }

    @DexIgnore
    public static final MovingType f(Lb7 lb7) {
        Wg6.c(lb7, "$this$toButtonMovingType");
        return MovingType.valueOf(lb7.name());
    }

    @DexIgnore
    public static final TextData g(Mb7 mb7) {
        Wg6.c(mb7, "$this$toButtonTextData");
        return new TextData(mb7.f(), mb7.e(), mb7.d(), c(mb7.c()));
    }

    @DexIgnore
    public static final ThemeData h(Ob7 ob7) {
        Wg6.c(ob7, "$this$toButtonThemeData");
        Hb7 b = ob7.b();
        Background b2 = b != null ? b(b) : null;
        ArrayList arrayList = new ArrayList();
        for (T t : ob7.a()) {
            MetricObject e = e(t.b());
            MovingType f = f(t.getType());
            if (t instanceof Mb7) {
                arrayList.add(new MovingObject(e, null, null, g(t), f, 6, null));
            } else if (t instanceof Pb7) {
                arrayList.add(new MovingObject(e, null, i(t), null, f, 10, null));
            } else if (t instanceof Ib7) {
                arrayList.add(new MovingObject(e, d(t), null, null, f, 12, null));
            }
        }
        return new ThemeData(b2, arrayList);
    }

    @DexIgnore
    public static final TickerData i(Pb7 pb7) {
        Wg6.c(pb7, "$this$toButtonTicker");
        return new TickerData(pb7.e(), pb7.d(), c(pb7.c()));
    }

    @DexIgnore
    public static final Ub7 j(Ob7 ob7, List<Fb7> list, List<Fb7> list2, List<Fb7> list3) {
        T t;
        T t2;
        T t3;
        byte[] a2;
        Wg6.c(ob7, "$this$toUIThemeData");
        Wg6.c(list, "backgroundsAsset");
        Wg6.c(list2, "tickersAsset");
        Wg6.c(list3, "ringsAsset");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            String b = next.b();
            Hb7 b2 = ob7.b();
            if (Wg6.a(b, b2 != null ? b2.b() : null)) {
                t = next;
                break;
            }
        }
        T t4 = t;
        String f = t4 != null ? t4.f() : null;
        Hb7 b3 = ob7.b();
        Hb7 b4 = ob7.b();
        Rb7 rb7 = new Rb7(b3, (b4 == null || (a2 = b4.a()) == null) ? null : J37.c(a2), f, null, 8, null);
        ArrayList arrayList = new ArrayList();
        for (T t5 : ob7.a()) {
            if (t5 instanceof Pb7) {
                Iterator<T> it2 = list2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (Wg6.a(next2.d(), t5.d())) {
                        t3 = next2;
                        break;
                    }
                }
                T t6 = t3;
                T t7 = t5;
                arrayList.add(new Vb7(t7, t7.e().length == 0 ? null : J37.c(t7.e()), t6 != null ? t6.f() : null));
            } else if (t5 instanceof Ib7) {
                Iterator<T> it3 = list3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (Wg6.a(next3.b(), t5.f())) {
                        t2 = next3;
                        break;
                    }
                }
                T t8 = t2;
                T t9 = t5;
                arrayList.add(new Sb7(t9, t9.h().length == 0 ? null : J37.c(t9.h()), t8 != null ? t8.f() : null));
            } else if (t5 instanceof Mb7) {
                arrayList.add(new Tb7(t5));
            }
        }
        return new Ub7(rb7, arrayList);
    }

    @DexIgnore
    public static final Hb7 k(Background background) {
        Wg6.c(background, "$this$toWFBackground");
        return new Hb7(background.getData(), background.getId());
    }

    @DexIgnore
    public static final Ib7 l(ComplicationData complicationData, MetricObject metricObject, MovingType movingType, int i) {
        Wg6.c(complicationData, "$this$toWFComplicationData");
        Wg6.c(metricObject, "metric");
        Wg6.c(movingType, "type");
        byte[] data = complicationData.getData();
        String name = complicationData.getName();
        boolean enableRingGoal = complicationData.getEnableRingGoal();
        TimeZoneData timeZoneData = complicationData.getTimeZoneData();
        String location = timeZoneData != null ? timeZoneData.getLocation() : null;
        TimeZoneData timeZoneData2 = complicationData.getTimeZoneData();
        return new Ib7(data, name, enableRingGoal, location, timeZoneData2 != null ? Integer.valueOf(timeZoneData2.getOffsetMin()) : null, p(complicationData.getColour()), m(metricObject), n(movingType), i);
    }

    @DexIgnore
    public static final Jb7 m(MetricObject metricObject) {
        Wg6.c(metricObject, "$this$toWFMetric");
        return new Jb7(metricObject.getScaledX(), metricObject.getScaledY(), metricObject.getScaledWidth(), metricObject.getScaledHeight());
    }

    @DexIgnore
    public static final Lb7 n(MovingType movingType) {
        Wg6.c(movingType, "$this$toWFMovingType");
        return Lb7.valueOf(movingType.name());
    }

    @DexIgnore
    public static final Mb7 o(TextData textData, MetricObject metricObject, int i) {
        Wg6.c(textData, "$this$toWFTextData");
        Wg6.c(metricObject, "metric");
        return new Mb7(textData.getText(), textData.getFont(), textData.getFontScaled(), p(textData.getColour()), m(metricObject), null, i, 32, null);
    }

    @DexIgnore
    public static final Nb7 p(ThemeColour themeColour) {
        Wg6.c(themeColour, "$this$toWFThemeColour");
        return Nb7.valueOf(themeColour.name());
    }

    @DexIgnore
    public static final Ob7 q(ThemeData themeData) {
        Wg6.c(themeData, "$this$toWFThemeData");
        Background background = themeData.getBackground();
        Hb7 k = background != null ? k(background) : null;
        ArrayList arrayList = new ArrayList();
        int i = 0;
        for (T t : themeData.getMovingObjects()) {
            if (i >= 0) {
                T t2 = t;
                int i2 = Bc7.a[t2.getType().ordinal()];
                if (i2 == 1) {
                    TextData textData = t2.getTextData();
                    if (textData != null) {
                        arrayList.add(o(textData, t2.getMetric(), i));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i2 != 2) {
                    ComplicationData complicationData = t2.getComplicationData();
                    if (complicationData != null) {
                        arrayList.add(l(complicationData, t2.getMetric(), t2.getType(), i));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    TickerData tickerData = t2.getTickerData();
                    if (tickerData != null) {
                        arrayList.add(r(tickerData, t2.getMetric(), i));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                i++;
            } else {
                Hm7.l();
                throw null;
            }
        }
        return new Ob7(k, arrayList);
    }

    @DexIgnore
    public static final Pb7 r(TickerData tickerData, MetricObject metricObject, int i) {
        Wg6.c(tickerData, "$this$toWFTicker");
        Wg6.c(metricObject, "metric");
        return new Pb7(tickerData.getData(), tickerData.getName(), p(tickerData.getColour()), m(metricObject), Lb7.TICKER, i);
    }
}
