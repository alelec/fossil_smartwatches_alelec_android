package com.fossil;

import com.fossil.Tu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dv3 implements Su3 {
    @DexIgnore
    public /* final */ Tu3.Bi b;

    @DexIgnore
    public Dv3(Tu3.Bi bi) {
        this.b = bi;
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public final void a(Ru3 ru3) {
        this.b.b(Cv3.s(ru3));
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public final void b(Ru3 ru3, int i, int i2) {
        this.b.a(Cv3.s(ru3), i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public final void c(Ru3 ru3, int i, int i2) {
        this.b.c(Cv3.s(ru3), i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public final void d(Ru3 ru3, int i, int i2) {
        this.b.d(Cv3.s(ru3), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Dv3.class != obj.getClass()) {
            return false;
        }
        return this.b.equals(((Dv3) obj).b);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }
}
