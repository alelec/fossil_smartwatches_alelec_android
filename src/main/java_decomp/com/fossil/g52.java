package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface G52 extends IInterface {
    @DexIgnore
    void M0(E52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    @DexIgnore
    void d1(E52 e52, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
