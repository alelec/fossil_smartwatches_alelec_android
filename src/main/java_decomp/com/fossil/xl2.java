package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xl2 extends Handler {
    @DexIgnore
    public Xl2(Looper looper) {
        super(looper);
    }

    @DexIgnore
    public Xl2(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }

    @DexIgnore
    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }
}
