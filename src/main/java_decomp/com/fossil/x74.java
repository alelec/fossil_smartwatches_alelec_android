package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X74 {
    @DexIgnore
    public static /* final */ X74 c; // = new X74("FirebaseCrashlytics");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public int b; // = 4;

    @DexIgnore
    public X74(String str) {
        this.a = str;
    }

    @DexIgnore
    public static X74 f() {
        return c;
    }

    @DexIgnore
    public final boolean a(int i) {
        return this.b <= i || Log.isLoggable(this.a, i);
    }

    @DexIgnore
    public void b(String str) {
        c(str, null);
    }

    @DexIgnore
    public void c(String str, Throwable th) {
        if (a(3)) {
            Log.d(this.a, str, th);
        }
    }

    @DexIgnore
    public void d(String str) {
        e(str, null);
    }

    @DexIgnore
    public void e(String str, Throwable th) {
        if (a(6)) {
            Log.e(this.a, str, th);
        }
    }

    @DexIgnore
    public void g(String str) {
        h(str, null);
    }

    @DexIgnore
    public void h(String str, Throwable th) {
        if (a(4)) {
            Log.i(this.a, str, th);
        }
    }

    @DexIgnore
    public void i(String str) {
        j(str, null);
    }

    @DexIgnore
    public void j(String str, Throwable th) {
        if (a(5)) {
            Log.w(this.a, str, th);
        }
    }
}
