package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vm3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Xr3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 c;

    @DexIgnore
    public Vm3(Qm3 qm3, Xr3 xr3) {
        this.c = qm3;
        this.b = xr3;
    }

    @DexIgnore
    public final void run() {
        this.c.b.d0();
        if (this.b.d.c() == null) {
            this.c.b.N(this.b);
        } else {
            this.c.b.w(this.b);
        }
    }
}
