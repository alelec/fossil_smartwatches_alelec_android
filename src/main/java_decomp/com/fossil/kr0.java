package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.mapped.Md;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kr0 {
    @DexIgnore
    public static Kr0 c; // = new Kr0();
    @DexIgnore
    public /* final */ Map<Class<?>, Ai> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, Boolean> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Map<Lifecycle.a, List<Bi>> a; // = new HashMap();
        @DexIgnore
        public /* final */ Map<Bi, Lifecycle.a> b;

        @DexIgnore
        public Ai(Map<Bi, Lifecycle.a> map) {
            this.b = map;
            for (Map.Entry<Bi, Lifecycle.a> entry : map.entrySet()) {
                Lifecycle.a value = entry.getValue();
                List<Bi> list = this.a.get(value);
                if (list == null) {
                    list = new ArrayList<>();
                    this.a.put(value, list);
                }
                list.add(entry.getKey());
            }
        }

        @DexIgnore
        public static void b(List<Bi> list, LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    list.get(size).a(lifecycleOwner, aVar, obj);
                }
            }
        }

        @DexIgnore
        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            b(this.a.get(aVar), lifecycleOwner, aVar, obj);
            b(this.a.get(Lifecycle.a.ON_ANY), lifecycleOwner, aVar, obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public Bi(int i, Method method) {
            this.a = i;
            this.b = method;
            method.setAccessible(true);
        }

        @DexIgnore
        public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar, Object obj) {
            try {
                int i = this.a;
                if (i == 0) {
                    this.b.invoke(obj, new Object[0]);
                } else if (i == 1) {
                    this.b.invoke(obj, lifecycleOwner);
                } else if (i == 2) {
                    this.b.invoke(obj, lifecycleOwner, aVar);
                }
            } catch (InvocationTargetException e) {
                throw new RuntimeException("Failed to call observer method", e.getCause());
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Bi.class != obj.getClass()) {
                return false;
            }
            Bi bi = (Bi) obj;
            return this.a == bi.a && this.b.getName().equals(bi.b.getName());
        }

        @DexIgnore
        public int hashCode() {
            return (this.a * 31) + this.b.getName().hashCode();
        }
    }

    @DexIgnore
    public final Ai a(Class<?> cls, Method[] methodArr) {
        int i;
        boolean z;
        Ai c2;
        Class<? super Object> superclass = cls.getSuperclass();
        HashMap hashMap = new HashMap();
        if (!(superclass == null || (c2 = c(superclass)) == null)) {
            hashMap.putAll(c2.b);
        }
        for (Class<?> cls2 : cls.getInterfaces()) {
            for (Map.Entry<Bi, Lifecycle.a> entry : c(cls2).b.entrySet()) {
                e(hashMap, entry.getKey(), entry.getValue(), cls);
            }
        }
        if (methodArr == null) {
            methodArr = b(cls);
        }
        int length = methodArr.length;
        boolean z2 = false;
        int i2 = 0;
        while (i2 < length) {
            Method method = methodArr[i2];
            Md md = (Md) method.getAnnotation(Md.class);
            if (md == null) {
                z = z2;
            } else {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i = 0;
                } else if (parameterTypes[0].isAssignableFrom(LifecycleOwner.class)) {
                    i = 1;
                } else {
                    throw new IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                Lifecycle.a value = md.value();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(Lifecycle.a.class)) {
                        throw new IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (value == Lifecycle.a.ON_ANY) {
                        i = 2;
                    } else {
                        throw new IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (parameterTypes.length <= 2) {
                    e(hashMap, new Bi(i, method), value, cls);
                    z = true;
                } else {
                    throw new IllegalArgumentException("cannot have more than 2 params");
                }
            }
            i2++;
            z2 = z;
        }
        Ai ai = new Ai(hashMap);
        this.a.put(cls, ai);
        this.b.put(cls, Boolean.valueOf(z2));
        return ai;
    }

    @DexIgnore
    public final Method[] b(Class<?> cls) {
        try {
            return cls.getDeclaredMethods();
        } catch (NoClassDefFoundError e) {
            throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", e);
        }
    }

    @DexIgnore
    public Ai c(Class<?> cls) {
        Ai ai = this.a.get(cls);
        return ai != null ? ai : a(cls, null);
    }

    @DexIgnore
    public boolean d(Class<?> cls) {
        Boolean bool = this.b.get(cls);
        if (bool != null) {
            return bool.booleanValue();
        }
        Method[] b2 = b(cls);
        for (Method method : b2) {
            if (((Md) method.getAnnotation(Md.class)) != null) {
                a(cls, b2);
                return true;
            }
        }
        this.b.put(cls, Boolean.FALSE);
        return false;
    }

    @DexIgnore
    public final void e(Map<Bi, Lifecycle.a> map, Bi bi, Lifecycle.a aVar, Class<?> cls) {
        Lifecycle.a aVar2 = map.get(bi);
        if (aVar2 != null && aVar != aVar2) {
            Method method = bi.b;
            throw new IllegalArgumentException("Method " + method.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous value " + aVar2 + ", new value " + aVar);
        } else if (aVar2 == null) {
            map.put(bi, aVar);
        }
    }
}
