package com.fossil;

import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S17 implements MembersInjector<FindDeviceActivity> {
    @DexIgnore
    public static void a(FindDeviceActivity findDeviceActivity, FindDevicePresenter findDevicePresenter) {
        findDeviceActivity.A = findDevicePresenter;
    }
}
