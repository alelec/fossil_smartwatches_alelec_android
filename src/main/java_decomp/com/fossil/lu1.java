package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Lu1 {
    SDK_LOG,
    HARDWARE_RAW_LOG,
    ACTIVITY_RAW_LOG,
    GPS_LOG
}
