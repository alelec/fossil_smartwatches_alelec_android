package com.fossil;

import com.fossil.fitness.CadenceUnit;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ei5 {
    UNKNOWN("unknown"),
    SPM("spm"),
    RPM("rpm");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ei5 a(Integer num) {
            int ordinal = CadenceUnit.SPM.ordinal();
            if (num != null && num.intValue() == ordinal) {
                return Ei5.SPM;
            }
            return (num != null && num.intValue() == CadenceUnit.RPM.ordinal()) ? Ei5.RPM : Ei5.UNKNOWN;
        }
    }

    @DexIgnore
    public Ei5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
