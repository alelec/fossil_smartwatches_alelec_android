package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tl4 {
    @DexIgnore
    public Bm4 a;

    @DexIgnore
    public Bm4 a() {
        return this.a;
    }

    @DexIgnore
    public void b(int i) {
    }

    @DexIgnore
    public void c(boolean z) {
    }

    @DexIgnore
    public void d(int i) {
    }

    @DexIgnore
    public void e(Bm4 bm4) {
        this.a = bm4;
    }

    @DexIgnore
    public void f(int i) {
    }
}
