package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hv3 implements Parcelable.Creator<Gv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Gv3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                str3 = Ad2.f(parcel, t);
            } else if (l == 3) {
                str2 = Ad2.f(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                str = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Gv3(str3, str2, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Gv3[] newArray(int i) {
        return new Gv3[i];
    }
}
