package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hq4 extends Ts0 {
    @DexIgnore
    public /* final */ Yk7 a; // = Zk7.a(Fi.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 b; // = Zk7.a(Gi.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 c; // = Zk7.a(Hi.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 d; // = Zk7.a(Ii.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 e; // = Zk7.a(Di.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 f; // = Zk7.a(Ei.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 g; // = Zk7.a(Ji.INSTANCE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;

        @DexIgnore
        public Ai(int i, String str) {
            Wg6.c(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final void c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void d(String str) {
            Wg6.c(str, "<set-?>");
            this.b = str;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (this.a != ai.a || !Wg6.a(this.b, ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a;
            String str = this.b;
            return (str != null ? str.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "DialogErrorState(errorCode=" + this.a + ", errorMessage=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;

        @DexIgnore
        public Bi() {
            this(false, false, null, 7, null);
        }

        @DexIgnore
        public Bi(boolean z, boolean z2, String str) {
            Wg6.c(str, "message");
            this.a = z;
            this.b = z2;
            this.c = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Bi(boolean z, boolean z2, String str, int i, Qg6 qg6) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? "" : str);
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final void d(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void e(String str) {
            Wg6.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(this.a == bi.a && this.b == bi.b && Wg6.a(this.c, bi.c))) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            String str = this.c;
            int hashCode = str != null ? str.hashCode() : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (((i2 * 31) + i) * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "LoadingState(mStartLoading=" + this.a + ", mStopLoading=" + this.b + ", message=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ List<Uh5> a;

        @DexIgnore
        public Ci() {
            this(null, 1, null);
        }

        @DexIgnore
        public Ci(List<Uh5> list) {
            Wg6.c(list, "permissionCodes");
            this.a = list;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(List list, int i, Qg6 qg6) {
            this((i & 1) != 0 ? new ArrayList() : list);
        }

        @DexIgnore
        public final List<Uh5> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Ci) && Wg6.a(this.a, ((Ci) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            List<Uh5> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "PermissionState(permissionCodes=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Gg6<Ai> {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();

        @DexIgnore
        public Di() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Ai invoke() {
            throw null;
            //return new Ai(-1, "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Qq7 implements Gg6<MutableLiveData<Ai>> {
        @DexIgnore
        public static /* final */ Ei INSTANCE; // = new Ei();

        @DexIgnore
        public Ei() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final MutableLiveData<Ai> invoke() {
            throw null;
            //return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Qq7 implements Gg6<Bi> {
        @DexIgnore
        public static /* final */ Fi INSTANCE; // = new Fi();

        @DexIgnore
        public Fi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Bi invoke() {
            throw null;
            //return new Bi(false, false, null, 7, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Qq7 implements Gg6<MutableLiveData<Bi>> {
        @DexIgnore
        public static /* final */ Gi INSTANCE; // = new Gi();

        @DexIgnore
        public Gi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final MutableLiveData<Bi> invoke() {
            throw null;
            //return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Qq7 implements Gg6<Ci> {
        @DexIgnore
        public static /* final */ Hi INSTANCE; // = new Hi();

        @DexIgnore
        public Hi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Ci invoke() {
            throw null;
            //return new Ci(null, 1, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Qq7 implements Gg6<MutableLiveData<Ci>> {
        @DexIgnore
        public static /* final */ Ii INSTANCE; // = new Ii();

        @DexIgnore
        public Ii() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final MutableLiveData<Ci> invoke() {
            throw null;
            //return new MutableLiveData<>();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji extends Qq7 implements Gg6<MutableLiveData<Object>> {
        @DexIgnore
        public static /* final */ Ji INSTANCE; // = new Ji();

        @DexIgnore
        public Ji() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final MutableLiveData<Object> invoke() {
            throw null;
            //return new MutableLiveData<>();
        }
    }

    @DexIgnore
    public static /* synthetic */ void b(Hq4 hq4, int i, String str, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                i = -1;
            }
            if ((i2 & 2) != 0) {
                str = "";
            }
            hq4.a(i, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitDialogErrorState");
    }

    @DexIgnore
    public static /* synthetic */ void d(Hq4 hq4, boolean z, boolean z2, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            if ((i & 2) != 0) {
                z2 = false;
            }
            if ((i & 4) != 0) {
                str = "";
            }
            hq4.c(z, z2, str);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: emitLoadingState");
    }

    @DexIgnore
    public final void a(int i, String str) {
        Wg6.c(str, "errorMessage");
        g().c(i);
        g().d(str);
        h().l(g());
    }

    @DexIgnore
    public final void c(boolean z, boolean z2, String str) {
        Wg6.c(str, "message");
        i().c(z);
        i().d(z2);
        i().e(str);
        j().l(i());
    }

    @DexIgnore
    public final void e(Uh5... uh5Arr) {
        Wg6.c(uh5Arr, "permissionCodes");
        k().a().clear();
        k().a().addAll(Em7.d0(uh5Arr));
        l().l(k());
    }

    @DexIgnore
    public final void f() {
        m().l(new Object());
    }

    @DexIgnore
    public final Ai g() {
        return (Ai) this.e.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Ai> h() {
        return (MutableLiveData) this.f.getValue();
    }

    @DexIgnore
    public final Bi i() {
        return (Bi) this.a.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Bi> j() {
        return (MutableLiveData) this.b.getValue();
    }

    @DexIgnore
    public final Ci k() {
        return (Ci) this.c.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Ci> l() {
        return (MutableLiveData) this.d.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Object> m() {
        return (MutableLiveData) this.g.getValue();
    }
}
