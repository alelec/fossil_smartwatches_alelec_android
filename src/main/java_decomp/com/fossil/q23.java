package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.E13;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.sqlcipher.database.SQLiteDatabase;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q23<T> implements F33<T> {
    @DexIgnore
    public static /* final */ int[] q; // = new int[0];
    @DexIgnore
    public static /* final */ Unsafe r; // = E43.t();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ M23 e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int[] i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ U23 l;
    @DexIgnore
    public /* final */ V13 m;
    @DexIgnore
    public /* final */ X33<?, ?> n;
    @DexIgnore
    public /* final */ S03<?> o;
    @DexIgnore
    public /* final */ J23 p;

    @DexIgnore
    public Q23(int[] iArr, Object[] objArr, int i2, int i3, M23 m23, boolean z, boolean z2, int[] iArr2, int i4, int i5, U23 u23, V13 v13, X33<?, ?> x33, S03<?> s03, J23 j23) {
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        boolean z3 = m23 instanceof E13;
        this.g = z;
        this.f = s03 != null && s03.e(m23);
        this.h = false;
        this.i = iArr2;
        this.j = i4;
        this.k = i5;
        this.l = u23;
        this.m = v13;
        this.n = x33;
        this.o = s03;
        this.e = m23;
        this.p = j23;
    }

    @DexIgnore
    public static <T> float E(T t, long j2) {
        return ((Float) E43.F(t, j2)).floatValue();
    }

    @DexIgnore
    public static <T> int I(T t, long j2) {
        return ((Integer) E43.F(t, j2)).intValue();
    }

    @DexIgnore
    public static <T> long K(T t, long j2) {
        return ((Long) E43.F(t, j2)).longValue();
    }

    @DexIgnore
    public static W33 L(Object obj) {
        E13 e13 = (E13) obj;
        W33 w33 = e13.zzb;
        if (w33 != W33.a()) {
            return w33;
        }
        W33 g2 = W33.g();
        e13.zzb = g2;
        return g2;
    }

    @DexIgnore
    public static <T> boolean M(T t, long j2) {
        return ((Boolean) E43.F(t, j2)).booleanValue();
    }

    @DexIgnore
    public static <UT, UB> int d(X33<UT, UB> x33, T t) {
        return x33.l(x33.f(t));
    }

    @DexIgnore
    public static int i(byte[] bArr, int i2, int i3, L43 l43, Class<?> cls, Sz2 sz2) throws IOException {
        switch (T23.a[l43.ordinal()]) {
            case 1:
                int k2 = Tz2.k(bArr, i2, sz2);
                sz2.c = Boolean.valueOf(sz2.b != 0);
                return k2;
            case 2:
                return Tz2.q(bArr, i2, sz2);
            case 3:
                sz2.c = Double.valueOf(Tz2.m(bArr, i2));
                return i2 + 8;
            case 4:
            case 5:
                sz2.c = Integer.valueOf(Tz2.h(bArr, i2));
                return i2 + 4;
            case 6:
            case 7:
                sz2.c = Long.valueOf(Tz2.l(bArr, i2));
                return i2 + 8;
            case 8:
                sz2.c = Float.valueOf(Tz2.o(bArr, i2));
                return i2 + 4;
            case 9:
            case 10:
            case 11:
                int i4 = Tz2.i(bArr, i2, sz2);
                sz2.c = Integer.valueOf(sz2.a);
                return i4;
            case 12:
            case 13:
                int k3 = Tz2.k(bArr, i2, sz2);
                sz2.c = Long.valueOf(sz2.b);
                return k3;
            case 14:
                return Tz2.g(B33.a().b(cls), bArr, i2, i3, sz2);
            case 15:
                int i5 = Tz2.i(bArr, i2, sz2);
                sz2.c = Integer.valueOf(J03.c(sz2.a));
                return i5;
            case 16:
                int k4 = Tz2.k(bArr, i2, sz2);
                sz2.c = Long.valueOf(J03.a(sz2.b));
                return k4;
            case 17:
                return Tz2.p(bArr, i2, sz2);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    @DexIgnore
    public static <T> Q23<T> j(Class<T> cls, K23 k23, U23 u23, V13 v13, X33<?, ?> x33, S03<?> s03, J23 j23) {
        int i2;
        char c2;
        int i3;
        int i4;
        int i5;
        int i6;
        char c3;
        int i7;
        int i8;
        char c4;
        char c5;
        int i9;
        char c6;
        int i10;
        char c7;
        int[] iArr;
        int i11;
        char c8;
        char c9;
        int i12;
        int i13;
        char charAt;
        int i14;
        char charAt2;
        char charAt3;
        char charAt4;
        int i15;
        char charAt5;
        char charAt6;
        char charAt7;
        char charAt8;
        int i16;
        int i17;
        char c10;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        int i24;
        int i25;
        int i26;
        int i27;
        Field n2;
        char charAt9;
        int i28;
        char c11;
        int i29;
        Field n3;
        Field n4;
        int i30;
        int i31;
        char charAt10;
        int i32;
        char charAt11;
        char charAt12;
        int i33;
        char charAt13;
        if (k23 instanceof D33) {
            D33 d33 = (D33) k23;
            boolean z = d33.zza() == E13.Fi.j;
            String a2 = d33.a();
            int length = a2.length();
            if (a2.charAt(0) >= '\ud800') {
                int i34 = 1;
                while (true) {
                    i2 = i34 + 1;
                    if (a2.charAt(i34) < '\ud800') {
                        break;
                    }
                    i34 = i2;
                }
            } else {
                i2 = 1;
            }
            int i35 = i2 + 1;
            char charAt14 = a2.charAt(i2);
            if (charAt14 >= '\ud800') {
                int i36 = 13;
                int i37 = charAt14 & '\u1fff';
                while (true) {
                    i33 = i35 + 1;
                    charAt13 = a2.charAt(i35);
                    if (charAt13 < '\ud800') {
                        break;
                    }
                    i37 |= (charAt13 & '\u1fff') << i36;
                    i36 += 13;
                    i35 = i33;
                }
                c2 = (charAt13 << i36) | i37;
                i3 = i33;
            } else {
                c2 = charAt14;
                i3 = i35;
            }
            if (c2 == 0) {
                iArr = q;
                i11 = 0;
                i12 = 0;
                c8 = 0;
                c4 = 0;
                c5 = 0;
                c7 = 0;
                c9 = 0;
            } else {
                int i38 = i3 + 1;
                char charAt15 = a2.charAt(i3);
                if (charAt15 >= '\ud800') {
                    int i39 = 13;
                    int i40 = charAt15 & '\u1fff';
                    while (true) {
                        i4 = i38 + 1;
                        charAt8 = a2.charAt(i38);
                        if (charAt8 < '\ud800') {
                            break;
                        }
                        i40 |= (charAt8 & '\u1fff') << i39;
                        i39 += 13;
                        i38 = i4;
                    }
                    charAt15 = (charAt8 << i39) | i40;
                } else {
                    i4 = i38;
                }
                int i41 = i4 + 1;
                int charAt16 = a2.charAt(i4);
                if (charAt16 >= 55296) {
                    int i42 = charAt16 & 8191;
                    int i43 = 13;
                    while (true) {
                        i5 = i41 + 1;
                        charAt7 = a2.charAt(i41);
                        if (charAt7 < '\ud800') {
                            break;
                        }
                        i42 |= (charAt7 & '\u1fff') << i43;
                        i43 += 13;
                        i41 = i5;
                    }
                    charAt16 = i42 | (charAt7 << i43);
                } else {
                    i5 = i41;
                }
                int i44 = i5 + 1;
                int charAt17 = a2.charAt(i5);
                if (charAt17 >= 55296) {
                    int i45 = charAt17 & 8191;
                    int i46 = 13;
                    while (true) {
                        i6 = i44 + 1;
                        charAt6 = a2.charAt(i44);
                        if (charAt6 < '\ud800') {
                            break;
                        }
                        i45 |= (charAt6 & '\u1fff') << i46;
                        i46 += 13;
                        i44 = i6;
                    }
                    charAt17 = i45 | (charAt6 << i46);
                } else {
                    i6 = i44;
                }
                int i47 = i6 + 1;
                char charAt18 = a2.charAt(i6);
                if (charAt18 >= '\ud800') {
                    int i48 = 13;
                    int i49 = charAt18 & '\u1fff';
                    while (true) {
                        i15 = i47 + 1;
                        charAt5 = a2.charAt(i47);
                        if (charAt5 < '\ud800') {
                            break;
                        }
                        i49 |= (charAt5 & '\u1fff') << i48;
                        i48 += 13;
                        i47 = i15;
                    }
                    c3 = (charAt5 << i48) | i49;
                    i7 = i15;
                } else {
                    c3 = charAt18;
                    i7 = i47;
                }
                int i50 = i7 + 1;
                char charAt19 = a2.charAt(i7);
                if (charAt19 >= '\ud800') {
                    int i51 = charAt19 & '\u1fff';
                    int i52 = 13;
                    while (true) {
                        i8 = i50 + 1;
                        charAt4 = a2.charAt(i50);
                        if (charAt4 < '\ud800') {
                            break;
                        }
                        i51 |= (charAt4 & '\u1fff') << i52;
                        i52 += 13;
                        i50 = i8;
                    }
                    c4 = i51 | (charAt4 << i52);
                } else {
                    i8 = i50;
                    c4 = charAt19;
                }
                int i53 = i8 + 1;
                char charAt20 = a2.charAt(i8);
                if (charAt20 >= '\ud800') {
                    int i54 = charAt20 & '\u1fff';
                    int i55 = 13;
                    int i56 = i54;
                    while (true) {
                        i9 = i53 + 1;
                        charAt3 = a2.charAt(i53);
                        if (charAt3 < '\ud800') {
                            break;
                        }
                        i56 |= (charAt3 & '\u1fff') << i55;
                        i55 += 13;
                        i53 = i9;
                    }
                    c5 = (charAt3 << i55) | i56;
                } else {
                    c5 = charAt20;
                    i9 = i53;
                }
                int i57 = i9 + 1;
                char charAt21 = a2.charAt(i9);
                if (charAt21 >= '\ud800') {
                    int i58 = 13;
                    int i59 = charAt21 & '\u1fff';
                    while (true) {
                        i14 = i57 + 1;
                        charAt2 = a2.charAt(i57);
                        if (charAt2 < '\ud800') {
                            break;
                        }
                        i59 |= (charAt2 & '\u1fff') << i58;
                        i58 += 13;
                        i57 = i14;
                    }
                    c6 = (charAt2 << i58) | i59;
                    i10 = i14;
                } else {
                    c6 = charAt21;
                    i10 = i57;
                }
                int i60 = i10 + 1;
                char charAt22 = a2.charAt(i10);
                if (charAt22 >= '\ud800') {
                    int i61 = 13;
                    int i62 = charAt22 & '\u1fff';
                    while (true) {
                        i13 = i60 + 1;
                        charAt = a2.charAt(i60);
                        if (charAt < '\ud800') {
                            break;
                        }
                        i62 |= (charAt & '\u1fff') << i61;
                        i61 += 13;
                        i60 = i13;
                    }
                    c7 = (charAt << i61) | i62;
                    i3 = i13;
                } else {
                    c7 = charAt22;
                    i3 = i60;
                }
                iArr = new int[(c7 + c5 + c6)];
                i11 = charAt16 + (charAt15 << 1);
                c8 = c3;
                c9 = charAt15;
                i12 = charAt17;
            }
            Unsafe unsafe = r;
            Object[] b2 = d33.b();
            Class<?> cls2 = d33.zzc().getClass();
            int[] iArr2 = new int[(c4 * 3)];
            Object[] objArr = new Object[(c4 << 1)];
            int i63 = c5 + c7;
            int i64 = 0;
            int i65 = 0;
            int i66 = i11;
            int i67 = i63;
            int i68 = c7;
            int i69 = c7;
            while (i3 < length) {
                int i70 = i3 + 1;
                char charAt23 = a2.charAt(i3);
                if (charAt23 >= '\ud800') {
                    int i71 = charAt23 & '\u1fff';
                    int i72 = 13;
                    while (true) {
                        i16 = i70 + 1;
                        charAt12 = a2.charAt(i70);
                        if (charAt12 < '\ud800') {
                            break;
                        }
                        i71 |= (charAt12 & '\u1fff') << i72;
                        i72 += 13;
                        i70 = i16;
                    }
                    i17 = i71 | (charAt12 << i72);
                } else {
                    i16 = i70;
                    i17 = charAt23;
                }
                int i73 = i16 + 1;
                char charAt24 = a2.charAt(i16);
                if (charAt24 >= '\ud800') {
                    int i74 = charAt24 & '\u1fff';
                    int i75 = 13;
                    int i76 = i73;
                    while (true) {
                        i32 = i76 + 1;
                        charAt11 = a2.charAt(i76);
                        if (charAt11 < '\ud800') {
                            break;
                        }
                        i74 |= (charAt11 & '\u1fff') << i75;
                        i75 += 13;
                        i76 = i32;
                    }
                    c10 = i74 | (charAt11 << i75);
                    i18 = i32;
                    i19 = i69;
                } else {
                    c10 = charAt24;
                    i18 = i73;
                    i19 = i69;
                }
                int i77 = c10 & '\u00ff';
                if ((c10 & '\u0400') != 0) {
                    iArr[i64] = i65;
                    i64++;
                }
                if (i77 >= 51) {
                    int i78 = i18 + 1;
                    char charAt25 = a2.charAt(i18);
                    if (charAt25 >= '\ud800') {
                        int i79 = charAt25 & '\u1fff';
                        int i80 = 13;
                        while (true) {
                            i31 = i78 + 1;
                            charAt10 = a2.charAt(i78);
                            if (charAt10 < '\ud800') {
                                break;
                            }
                            i79 |= (charAt10 & '\u1fff') << i80;
                            i80 += 13;
                            i78 = i31;
                        }
                        c11 = (charAt10 << i80) | i79;
                        i29 = i31;
                    } else {
                        c11 = charAt25;
                        i29 = i78;
                    }
                    int i81 = i77 - 51;
                    if (i81 == 9 || i81 == 17) {
                        objArr[((i65 / 3) << 1) + 1] = b2[i66];
                        i20 = i66 + 1;
                    } else {
                        if (i81 != 12 || z) {
                            i30 = i66;
                        } else {
                            objArr[((i65 / 3) << 1) + 1] = b2[i66];
                            i30 = i66 + 1;
                        }
                        i20 = i30;
                    }
                    int i82 = c11 << 1;
                    Object obj = b2[i82];
                    if (obj instanceof Field) {
                        n3 = (Field) obj;
                    } else {
                        n3 = n(cls2, (String) obj);
                        b2[i82] = n3;
                    }
                    i25 = (int) unsafe.objectFieldOffset(n3);
                    int i83 = i82 + 1;
                    Object obj2 = b2[i83];
                    if (obj2 instanceof Field) {
                        n4 = (Field) obj2;
                    } else {
                        n4 = n(cls2, (String) obj2);
                        b2[i83] = n4;
                    }
                    i21 = (int) unsafe.objectFieldOffset(n4);
                    i22 = 0;
                    i26 = i29;
                } else {
                    int i84 = i66 + 1;
                    Field n5 = n(cls2, (String) b2[i66]);
                    if (i77 == 9 || i77 == 17) {
                        objArr[((i65 / 3) << 1) + 1] = n5.getType();
                        i20 = i84;
                    } else {
                        if (i77 == 27 || i77 == 49) {
                            i20 = i84 + 1;
                            objArr[((i65 / 3) << 1) + 1] = b2[i84];
                            i28 = i68;
                        } else if (i77 == 12 || i77 == 30 || i77 == 44) {
                            if (!z) {
                                i20 = i84 + 1;
                                objArr[((i65 / 3) << 1) + 1] = b2[i84];
                                i28 = i68;
                            } else {
                                i20 = i84;
                            }
                        } else if (i77 == 50) {
                            i28 = i68 + 1;
                            iArr[i68] = i65;
                            int i85 = (i65 / 3) << 1;
                            i20 = i84 + 1;
                            objArr[i85] = b2[i84];
                            if ((c10 & '\u0800') != 0) {
                                objArr[i85 + 1] = b2[i20];
                                i20++;
                                i68 = i28;
                            }
                        } else {
                            i20 = i84;
                        }
                        i68 = i28;
                    }
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(n5);
                    if ((c10 & '\u1000') != 4096 || i77 > 17) {
                        i21 = 1048575;
                        i22 = 0;
                        i23 = i18;
                    } else {
                        int i86 = i18 + 1;
                        char charAt26 = a2.charAt(i18);
                        if (charAt26 >= '\ud800') {
                            int i87 = charAt26 & '\u1fff';
                            int i88 = 13;
                            while (true) {
                                i27 = i86 + 1;
                                charAt9 = a2.charAt(i86);
                                if (charAt9 < '\ud800') {
                                    break;
                                }
                                i87 |= (charAt9 & '\u1fff') << i88;
                                i88 += 13;
                                i86 = i27;
                            }
                            charAt26 = (charAt9 << i88) | i87;
                        } else {
                            i27 = i86;
                        }
                        int i89 = (charAt26 / ' ') + (c9 << 1);
                        Object obj3 = b2[i89];
                        if (obj3 instanceof Field) {
                            n2 = (Field) obj3;
                        } else {
                            n2 = n(cls2, (String) obj3);
                            b2[i89] = n2;
                        }
                        i21 = (int) unsafe.objectFieldOffset(n2);
                        i22 = charAt26 % ' ';
                        i23 = i27;
                    }
                    if (i77 < 18 || i77 > 49) {
                        i24 = i67;
                    } else {
                        iArr[i67] = objectFieldOffset;
                        i24 = i67 + 1;
                    }
                    i25 = objectFieldOffset;
                    i67 = i24;
                    i26 = i23;
                }
                int i90 = i65 + 1;
                iArr2[i65] = i17;
                int i91 = i90 + 1;
                iArr2[i90] = ((c10 & '\u0100') != 0 ? SQLiteDatabase.CREATE_IF_NECESSARY : 0) | ((c10 & '\u0200') != 0 ? 536870912 : 0) | (i77 << 20) | i25;
                iArr2[i91] = (i22 << 20) | i21;
                i65 = i91 + 1;
                i66 = i20;
                i3 = i26;
                i69 = i19;
            }
            return new Q23<>(iArr2, objArr, i12, c8, d33.zzc(), z, false, iArr, i69, i63, u23, v13, x33, s03, j23);
        }
        ((Q33) k23).zza();
        throw null;
    }

    @DexIgnore
    public static Field n(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException e2) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    @DexIgnore
    public static List<?> o(Object obj, long j2) {
        return (List) E43.F(obj, j2);
    }

    @DexIgnore
    public static void p(int i2, Object obj, R43 r43) throws IOException {
        if (obj instanceof String) {
            r43.zza(i2, (String) obj);
        } else {
            r43.f(i2, (Xz2) obj);
        }
    }

    @DexIgnore
    public static <UT, UB> void q(X33<UT, UB> x33, T t, R43 r43) throws IOException {
        x33.d(x33.f(t), r43);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.fossil.F33 */
    /* JADX WARN: Multi-variable type inference failed */
    public static boolean w(Object obj, int i2, F33 f33) {
        return f33.zzd(E43.F(obj, (long) (1048575 & i2)));
    }

    @DexIgnore
    public static <T> double x(T t, long j2) {
        return ((Double) E43.F(t, j2)).doubleValue();
    }

    @DexIgnore
    public final void A(T t, int i2) {
        int J = J(i2);
        long j2 = (long) (1048575 & J);
        if (j2 != 1048575) {
            E43.h(t, j2, (1 << (J >>> 20)) | E43.b(t, j2));
        }
    }

    @DexIgnore
    public final void B(T t, int i2, int i3) {
        E43.h(t, (long) (J(i3) & 1048575), i2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0644  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0650  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void C(T r17, com.fossil.R43 r18) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1772
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.C(java.lang.Object, com.fossil.R43):void");
    }

    @DexIgnore
    public final void D(T t, T t2, int i2) {
        int H = H(i2);
        int i3 = this.a[i2];
        long j2 = (long) (H & 1048575);
        if (u(t2, i3, i2)) {
            Object F = E43.F(t, j2);
            Object F2 = E43.F(t2, j2);
            if (F != null && F2 != null) {
                E43.j(t, j2, H13.e(F, F2));
                B(t, i3, i2);
            } else if (F2 != null) {
                E43.j(t, j2, F2);
                B(t, i3, i2);
            }
        }
    }

    @DexIgnore
    public final I13 F(int i2) {
        return (I13) this.b[((i2 / 3) << 1) + 1];
    }

    @DexIgnore
    public final boolean G(T t, T t2, int i2) {
        return t(t, i2) == t(t2, i2);
    }

    @DexIgnore
    public final int H(int i2) {
        return this.a[i2 + 1];
    }

    @DexIgnore
    public final int J(int i2) {
        return this.a[i2 + 2];
    }

    @DexIgnore
    public final int N(int i2) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return y(i2, 0);
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    @Override // com.fossil.F33
    public final void a(T t, byte[] bArr, int i2, int i3, Sz2 sz2) throws IOException {
        int i4;
        int e2;
        M13 m13;
        int i5;
        Sz2 sz22;
        int i6;
        byte[] bArr2;
        T t2;
        Q23<T> q23;
        int i7;
        int i8;
        int i9;
        if (this.g) {
            Unsafe unsafe = r;
            int i10 = 0;
            int i11 = 0;
            int i12 = 1048575;
            int i13 = -1;
            Sz2 sz23 = sz2;
            int i14 = i3;
            byte[] bArr3 = bArr;
            T t3 = t;
            Q23<T> q232 = this;
            while (i2 < i14) {
                int i15 = i2 + 1;
                byte b2 = bArr3[i2];
                int i16 = b2;
                if (b2 < 0) {
                    i15 = Tz2.d(b2, bArr3, i15, sz23);
                    i16 = sz23.a;
                }
                int i17 = (i16 == 1 ? 1 : 0) >>> 3;
                int i18 = (i16 == 1 ? 1 : 0) & 7;
                i10 = i17 > i13 ? q232.c(i17, i10 / 3) : q232.N(i17);
                if (i10 == -1) {
                    i10 = 0;
                    i8 = i12;
                    i9 = i11;
                } else {
                    int[] iArr = q232.a;
                    int i19 = iArr[i10 + 1];
                    int i20 = (267386880 & i19) >>> 20;
                    long j2 = (long) (1048575 & i19);
                    if (i20 <= 17) {
                        int i21 = iArr[i10 + 2];
                        int i22 = 1 << (i21 >>> 20);
                        i5 = i21 & 1048575;
                        if (i5 != i12) {
                            if (i12 != 1048575) {
                                unsafe.putInt(t3, (long) i12, i11);
                            }
                            if (i5 != 1048575) {
                                i11 = unsafe.getInt(t3, (long) i5);
                            }
                            i7 = i11;
                        } else {
                            i5 = i12;
                            i7 = i11;
                        }
                        switch (i20) {
                            case 0:
                                if (i18 == 1) {
                                    E43.f(t3, j2, Tz2.m(bArr3, i15));
                                    e2 = i15 + 8;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 1:
                                if (i18 == 5) {
                                    E43.g(t3, j2, Tz2.o(bArr3, i15));
                                    e2 = i15 + 4;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 2:
                            case 3:
                                if (i18 == 0) {
                                    int k2 = Tz2.k(bArr3, i15, sz23);
                                    unsafe.putLong(t, j2, sz23.b);
                                    i11 = i7 | i22;
                                    e2 = k2;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 4:
                            case 11:
                                if (i18 == 0) {
                                    e2 = Tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, sz23.a);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 5:
                            case 14:
                                if (i18 == 1) {
                                    unsafe.putLong(t, j2, Tz2.l(bArr3, i15));
                                    e2 = i15 + 8;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 6:
                            case 13:
                                if (i18 == 5) {
                                    unsafe.putInt(t3, j2, Tz2.h(bArr3, i15));
                                    e2 = i15 + 4;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 7:
                                if (i18 == 0) {
                                    int k3 = Tz2.k(bArr3, i15, sz23);
                                    E43.k(t3, j2, sz23.b != 0);
                                    e2 = k3;
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 8:
                                if (i18 == 2) {
                                    e2 = (536870912 & i19) == 0 ? Tz2.n(bArr3, i15, sz23) : Tz2.p(bArr3, i15, sz23);
                                    unsafe.putObject(t3, j2, sz23.c);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 9:
                                if (i18 == 2) {
                                    e2 = Tz2.g(q232.k(i10), bArr3, i15, i14, sz23);
                                    Object object = unsafe.getObject(t3, j2);
                                    if (object == null) {
                                        unsafe.putObject(t3, j2, sz23.c);
                                    } else {
                                        unsafe.putObject(t3, j2, H13.e(object, sz23.c));
                                    }
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 10:
                                if (i18 == 2) {
                                    e2 = Tz2.q(bArr3, i15, sz23);
                                    unsafe.putObject(t3, j2, sz23.c);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 12:
                                if (i18 == 0) {
                                    e2 = Tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, sz23.a);
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 15:
                                if (i18 == 0) {
                                    e2 = Tz2.i(bArr3, i15, sz23);
                                    unsafe.putInt(t3, j2, J03.c(sz23.a));
                                    i11 = i7 | i22;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            case 16:
                                if (i18 == 0) {
                                    int k4 = Tz2.k(bArr3, i15, sz23);
                                    unsafe.putLong(t, j2, J03.a(sz23.b));
                                    i11 = i7 | i22;
                                    e2 = k4;
                                    break;
                                }
                                i8 = i5;
                                i9 = i7;
                                break;
                            default:
                                i8 = i5;
                                i9 = i7;
                                break;
                        }
                    } else {
                        if (i20 == 27) {
                            if (i18 == 2) {
                                M13 m132 = (M13) unsafe.getObject(t3, j2);
                                if (!m132.zza()) {
                                    int size = m132.size();
                                    m13 = m132.zza(size == 0 ? 10 : size << 1);
                                    unsafe.putObject(t3, j2, m13);
                                } else {
                                    m13 = m132;
                                }
                                e2 = Tz2.e(q232.k(i10), i16, bArr, i15, i3, m13, sz2);
                                i5 = i12;
                            }
                        } else if (i20 <= 49) {
                            e2 = f(t, bArr, i15, i3, i16, i17, i18, i10, (long) i19, i20, j2, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        } else if (i20 != 50) {
                            e2 = e(t, bArr, i15, i3, i16, i17, i18, i19, i20, j2, i10, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        } else if (i18 == 2) {
                            e2 = g(t, bArr, i15, i3, i10, j2, sz2);
                            if (e2 == i15) {
                                i15 = e2;
                            }
                            i5 = i12;
                            sz22 = sz2;
                            i6 = i3;
                            bArr2 = bArr;
                            t2 = t;
                            q23 = this;
                            i12 = i5;
                            i13 = i17;
                            i2 = e2;
                            sz23 = sz22;
                            i14 = i6;
                            bArr3 = bArr2;
                            t3 = t2;
                            q232 = q23;
                        }
                        i4 = i15;
                        e2 = Tz2.c(i16, bArr, i4, i3, L(t), sz2);
                        i5 = i12;
                        sz22 = sz2;
                        i6 = i3;
                        bArr2 = bArr;
                        t2 = t;
                        q23 = this;
                        i12 = i5;
                        i13 = i17;
                        i2 = e2;
                        sz23 = sz22;
                        i14 = i6;
                        bArr3 = bArr2;
                        t3 = t2;
                        q232 = q23;
                    }
                    sz22 = sz23;
                    i6 = i14;
                    bArr2 = bArr3;
                    t2 = t3;
                    q23 = q232;
                    i12 = i5;
                    i13 = i17;
                    i2 = e2;
                    sz23 = sz22;
                    i14 = i6;
                    bArr3 = bArr2;
                    t3 = t2;
                    q232 = q23;
                }
                i4 = i15;
                i12 = i8;
                i11 = i9;
                e2 = Tz2.c(i16, bArr, i4, i3, L(t), sz2);
                i5 = i12;
                sz22 = sz2;
                i6 = i3;
                bArr2 = bArr;
                t2 = t;
                q23 = this;
                i12 = i5;
                i13 = i17;
                i2 = e2;
                sz23 = sz22;
                i14 = i6;
                bArr3 = bArr2;
                t3 = t2;
                q232 = q23;
            }
            if (i12 != 1048575) {
                unsafe.putInt(t, (long) i12, i11);
            }
            if (i2 != i3) {
                throw L13.zzg();
            }
            return;
        }
        h(t, bArr, i2, i3, 0, sz2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x096e  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x0975  */
    /* JADX WARNING: Removed duplicated region for block: B:540:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003a  */
    @Override // com.fossil.F33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(T r14, com.fossil.R43 r15) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 2716
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.b(java.lang.Object, com.fossil.R43):void");
    }

    @DexIgnore
    public final int c(int i2, int i3) {
        if (i2 < this.c || i2 > this.d) {
            return -1;
        }
        return y(i2, i3);
    }

    @DexIgnore
    public final int e(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, Sz2 sz2) throws IOException {
        int k2;
        Unsafe unsafe = r;
        long j3 = (long) (this.a[i9 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Double.valueOf(Tz2.m(bArr, i2)));
                    k2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 52:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Float.valueOf(Tz2.o(bArr, i2)));
                    k2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 53:
            case 54:
                if (i6 == 0) {
                    k2 = Tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Long.valueOf(sz2.b));
                    break;
                } else {
                    return i2;
                }
            case 55:
            case 62:
                if (i6 == 0) {
                    k2 = Tz2.i(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Integer.valueOf(sz2.a));
                    break;
                } else {
                    return i2;
                }
            case 56:
            case 65:
                if (i6 == 1) {
                    unsafe.putObject(t, j2, Long.valueOf(Tz2.l(bArr, i2)));
                    k2 = i2 + 8;
                    break;
                } else {
                    return i2;
                }
            case 57:
            case 64:
                if (i6 == 5) {
                    unsafe.putObject(t, j2, Integer.valueOf(Tz2.h(bArr, i2)));
                    k2 = i2 + 4;
                    break;
                } else {
                    return i2;
                }
            case 58:
                if (i6 == 0) {
                    int k3 = Tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Boolean.valueOf(sz2.b != 0));
                    k2 = k3;
                    break;
                } else {
                    return i2;
                }
            case 59:
                if (i6 != 2) {
                    return i2;
                }
                int i10 = Tz2.i(bArr, i2, sz2);
                int i11 = sz2.a;
                if (i11 == 0) {
                    unsafe.putObject(t, j2, "");
                } else if ((536870912 & i7) == 0 || G43.g(bArr, i10, i10 + i11)) {
                    unsafe.putObject(t, j2, new String(bArr, i10, i11, H13.a));
                    i10 += i11;
                } else {
                    throw L13.zzh();
                }
                unsafe.putInt(t, j3, i5);
                return i10;
            case 60:
                if (i6 != 2) {
                    return i2;
                }
                int g2 = Tz2.g(k(i9), bArr, i2, i3, sz2);
                Object object = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                if (object == null) {
                    unsafe.putObject(t, j2, sz2.c);
                } else {
                    unsafe.putObject(t, j2, H13.e(object, sz2.c));
                }
                unsafe.putInt(t, j3, i5);
                return g2;
            case 61:
                if (i6 == 2) {
                    k2 = Tz2.q(bArr, i2, sz2);
                    unsafe.putObject(t, j2, sz2.c);
                    break;
                } else {
                    return i2;
                }
            case 63:
                if (i6 != 0) {
                    return i2;
                }
                k2 = Tz2.i(bArr, i2, sz2);
                int i12 = sz2.a;
                I13 F = F(i9);
                if (F == null || F.zza(i12)) {
                    unsafe.putObject(t, j2, Integer.valueOf(i12));
                    break;
                } else {
                    L(t).c(i4, Long.valueOf((long) i12));
                    return k2;
                }
                break;
            case 66:
                if (i6 == 0) {
                    k2 = Tz2.i(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Integer.valueOf(J03.c(sz2.a)));
                    break;
                } else {
                    return i2;
                }
            case 67:
                if (i6 == 0) {
                    k2 = Tz2.k(bArr, i2, sz2);
                    unsafe.putObject(t, j2, Long.valueOf(J03.a(sz2.b)));
                    break;
                } else {
                    return i2;
                }
            case 68:
                if (i6 == 3) {
                    k2 = Tz2.f(k(i9), bArr, i2, i3, (i4 & -8) | 4, sz2);
                    Object object2 = unsafe.getInt(t, j3) == i5 ? unsafe.getObject(t, j2) : null;
                    if (object2 != null) {
                        unsafe.putObject(t, j2, H13.e(object2, sz2.c));
                        break;
                    } else {
                        unsafe.putObject(t, j2, sz2.c);
                        break;
                    }
                } else {
                    return i2;
                }
            default:
                return i2;
        }
        unsafe.putInt(t, j3, i5);
        return k2;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0226  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0027 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:267:0x0026 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0199  */
    public final int f(T r13, byte[] r14, int r15, int r16, int r17, int r18, int r19, int r20, long r21, int r23, long r24, com.fossil.Sz2 r26) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.f(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.fossil.Sz2):int");
    }

    @DexIgnore
    public final <K, V> int g(T t, byte[] bArr, int i2, int i3, int i4, long j2, Sz2 sz2) throws IOException {
        Object obj;
        Unsafe unsafe = r;
        Object z = z(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.p.zzd(object)) {
            obj = this.p.a(z);
            this.p.zza(obj, object);
            unsafe.putObject(t, j2, obj);
        } else {
            obj = object;
        }
        H23<?, ?> zzb = this.p.zzb(z);
        Map<?, ?> zza = this.p.zza(obj);
        int i5 = Tz2.i(bArr, i2, sz2);
        int i6 = sz2.a;
        if (i6 < 0 || i6 > i3 - i5) {
            throw L13.zza();
        }
        int i7 = i6 + i5;
        K k2 = zzb.b;
        Object obj2 = zzb.d;
        int i8 = i5;
        Object obj3 = k2;
        while (i8 < i7) {
            int i9 = i8 + 1;
            byte b2 = bArr[i8];
            int i10 = b2;
            if (b2 < 0) {
                i9 = Tz2.d(b2, bArr, i9, sz2);
                i10 = sz2.a;
            }
            int i11 = (i10 == 1 ? 1 : 0) >>> 3;
            int i12 = (i10 == 1 ? 1 : 0) & 7;
            if (i11 != 1) {
                if (i11 == 2 && i12 == zzb.c.zzb()) {
                    int i13 = i(bArr, i9, i3, zzb.c, zzb.d.getClass(), sz2);
                    obj2 = sz2.c;
                    i8 = i13;
                }
            } else if (i12 == zzb.a.zzb()) {
                i8 = i(bArr, i9, i3, zzb.a, null, sz2);
                obj3 = sz2.c;
            }
            i8 = Tz2.a(i10, bArr, i9, i3, sz2);
        }
        if (i8 == i7) {
            zza.put(obj3, obj2);
            return i7;
        }
        throw L13.zzg();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v94, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int h(T r42, byte[] r43, int r44, int r45, int r46, com.fossil.Sz2 r47) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 1170
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.h(java.lang.Object, byte[], int, int, int, com.fossil.Sz2):int");
    }

    @DexIgnore
    public final F33 k(int i2) {
        int i3 = (i2 / 3) << 1;
        F33 f33 = (F33) this.b[i3];
        if (f33 != null) {
            return f33;
        }
        F33<T> b2 = B33.a().b((Class) this.b[i3 + 1]);
        this.b[i3] = b2;
        return b2;
    }

    @DexIgnore
    public final <K, V, UT, UB> UB l(int i2, int i3, Map<K, V> map, I13 i13, UB ub, X33<UT, UB> x33) {
        H23<?, ?> zzb = this.p.zzb(z(i2));
        Iterator<Map.Entry<K, V>> it = map.entrySet().iterator();
        UB ub2 = ub;
        while (it.hasNext()) {
            Map.Entry<K, V> next = it.next();
            if (!i13.zza(next.getValue().intValue())) {
                UB a2 = ub2 == null ? x33.a() : ub2;
                F03 zzc = Xz2.zzc(E23.a(zzb, next.getKey(), next.getValue()));
                try {
                    E23.b(zzc.b(), zzb, next.getKey(), next.getValue());
                    x33.c(a2, i3, zzc.a());
                    it.remove();
                    ub2 = a2;
                } catch (IOException e2) {
                    throw new RuntimeException(e2);
                }
            }
        }
        return ub2;
    }

    @DexIgnore
    public final <UT, UB> UB m(Object obj, int i2, UB ub, X33<UT, UB> x33) {
        I13 F;
        int i3 = this.a[i2];
        Object F2 = E43.F(obj, (long) (H(i2) & 1048575));
        return (F2 == null || (F = F(i2)) == null) ? ub : (UB) l(i2, i3, (Map<K, V>) this.p.zza(F2), F, ub, x33);
    }

    @DexIgnore
    public final <K, V> void r(R43 r43, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            r43.c(i2, this.p.zzb(z(i3)), this.p.zzc(obj));
        }
    }

    @DexIgnore
    public final void s(T t, T t2, int i2) {
        long H = (long) (H(i2) & 1048575);
        if (t(t2, i2)) {
            Object F = E43.F(t, H);
            Object F2 = E43.F(t2, H);
            if (F != null && F2 != null) {
                E43.j(t, H, H13.e(F, F2));
                A(t, i2);
            } else if (F2 != null) {
                E43.j(t, H, F2);
                A(t, i2);
            }
        }
    }

    @DexIgnore
    public final boolean t(T t, int i2) {
        int J = J(i2);
        long j2 = (long) (J & 1048575);
        if (j2 == 1048575) {
            int H = H(i2);
            long j3 = (long) (H & 1048575);
            switch ((H & 267386880) >>> 20) {
                case 0:
                    return E43.C(t, j3) != 0.0d;
                case 1:
                    return E43.x(t, j3) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return E43.o(t, j3) != 0;
                case 3:
                    return E43.o(t, j3) != 0;
                case 4:
                    return E43.b(t, j3) != 0;
                case 5:
                    return E43.o(t, j3) != 0;
                case 6:
                    return E43.b(t, j3) != 0;
                case 7:
                    return E43.w(t, j3);
                case 8:
                    Object F = E43.F(t, j3);
                    if (F instanceof String) {
                        return !((String) F).isEmpty();
                    }
                    if (F instanceof Xz2) {
                        return !Xz2.zza.equals(F);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return E43.F(t, j3) != null;
                case 10:
                    return !Xz2.zza.equals(E43.F(t, j3));
                case 11:
                    return E43.b(t, j3) != 0;
                case 12:
                    return E43.b(t, j3) != 0;
                case 13:
                    return E43.b(t, j3) != 0;
                case 14:
                    return E43.o(t, j3) != 0;
                case 15:
                    return E43.b(t, j3) != 0;
                case 16:
                    return E43.o(t, j3) != 0;
                case 17:
                    return E43.F(t, j3) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            return ((1 << (J >>> 20)) & E43.b(t, j2)) != 0;
        }
    }

    @DexIgnore
    public final boolean u(T t, int i2, int i3) {
        return E43.b(t, (long) (J(i3) & 1048575)) == i2;
    }

    @DexIgnore
    public final boolean v(T t, int i2, int i3, int i4, int i5) {
        return i3 == 1048575 ? t(t, i2) : (i4 & i5) != 0;
    }

    @DexIgnore
    public final int y(int i2, int i3) {
        int length = (this.a.length / 3) - 1;
        while (i3 <= length) {
            int i4 = (length + i3) >>> 1;
            int i5 = i4 * 3;
            int i6 = this.a[i5];
            if (i2 == i6) {
                return i5;
            }
            if (i2 < i6) {
                length = i4 - 1;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }

    @DexIgnore
    public final Object z(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final int zza(T t) {
        int i2;
        int i3;
        int b2;
        int length = this.a.length;
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5 += 3) {
            int H = H(i5);
            int i6 = this.a[i5];
            long j2 = (long) (1048575 & H);
            int i7 = 37;
            switch ((H & 267386880) >>> 20) {
                case 0:
                    i3 = i4 * 53;
                    b2 = H13.b(Double.doubleToLongBits(E43.C(t, j2)));
                    i2 = i3 + b2;
                    break;
                case 1:
                    i3 = i4 * 53;
                    b2 = Float.floatToIntBits(E43.x(t, j2));
                    i2 = i3 + b2;
                    break;
                case 2:
                    i3 = i4 * 53;
                    b2 = H13.b(E43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 3:
                    i3 = i4 * 53;
                    b2 = H13.b(E43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 4:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 5:
                    i3 = i4 * 53;
                    b2 = H13.b(E43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 6:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 7:
                    i3 = i4 * 53;
                    b2 = H13.c(E43.w(t, j2));
                    i2 = i3 + b2;
                    break;
                case 8:
                    b2 = ((String) E43.F(t, j2)).hashCode();
                    i3 = i4 * 53;
                    i2 = i3 + b2;
                    break;
                case 9:
                    Object F = E43.F(t, j2);
                    if (F != null) {
                        i7 = F.hashCode();
                    }
                    i2 = i7 + (i4 * 53);
                    break;
                case 10:
                    i3 = i4 * 53;
                    b2 = E43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 11:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 12:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 13:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 14:
                    i3 = i4 * 53;
                    b2 = H13.b(E43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 15:
                    i3 = i4 * 53;
                    b2 = E43.b(t, j2);
                    i2 = i3 + b2;
                    break;
                case 16:
                    i3 = i4 * 53;
                    b2 = H13.b(E43.o(t, j2));
                    i2 = i3 + b2;
                    break;
                case 17:
                    Object F2 = E43.F(t, j2);
                    if (F2 != null) {
                        i7 = F2.hashCode();
                    }
                    i2 = i7 + (i4 * 53);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i4 * 53;
                    b2 = E43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 50:
                    i3 = i4 * 53;
                    b2 = E43.F(t, j2).hashCode();
                    i2 = i3 + b2;
                    break;
                case 51:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(Double.doubleToLongBits(x(t, j2)));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 52:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = Float.floatToIntBits(E(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 53:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 54:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 55:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 56:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 57:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 58:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.c(M(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 59:
                    if (u(t, i6, i5)) {
                        b2 = ((String) E43.F(t, j2)).hashCode();
                        i3 = i4 * 53;
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 60:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = E43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 61:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = E43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 62:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 63:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 64:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 65:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 66:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = I(t, j2);
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 67:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = H13.b(K(t, j2));
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                case 68:
                    if (u(t, i6, i5)) {
                        i3 = i4 * 53;
                        b2 = E43.F(t, j2).hashCode();
                        i2 = i3 + b2;
                        break;
                    }
                    i2 = i4;
                    break;
                default:
                    i2 = i4;
                    break;
            }
            i4 = i2;
        }
        int hashCode = (i4 * 53) + this.n.f(t).hashCode();
        return this.f ? (hashCode * 53) + this.o.b(t).hashCode() : hashCode;
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final T zza() {
        return (T) this.l.zza(this.e);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (com.fossil.H33.q(com.fossil.E43.F(r12, r6), com.fossil.E43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006c, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007e, code lost:
        if (com.fossil.E43.o(r12, r6) == com.fossil.E43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008f, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a2, code lost:
        if (com.fossil.E43.o(r12, r6) == com.fossil.E43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b3, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c4, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d6, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ec, code lost:
        if (com.fossil.H33.q(com.fossil.E43.F(r12, r6), com.fossil.E43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0102, code lost:
        if (com.fossil.H33.q(com.fossil.E43.F(r12, r6), com.fossil.E43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0118, code lost:
        if (com.fossil.H33.q(com.fossil.E43.F(r12, r6), com.fossil.E43.F(r13, r6)) != false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x012a, code lost:
        if (com.fossil.E43.w(r12, r6) == com.fossil.E43.w(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x013c, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0150, code lost:
        if (com.fossil.E43.o(r12, r6) == com.fossil.E43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0162, code lost:
        if (com.fossil.E43.b(r12, r6) == com.fossil.E43.b(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0176, code lost:
        if (com.fossil.E43.o(r12, r6) == com.fossil.E43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018a, code lost:
        if (com.fossil.E43.o(r12, r6) == com.fossil.E43.o(r13, r6)) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.fossil.E43.x(r12, r6)) == java.lang.Float.floatToIntBits(com.fossil.E43.x(r13, r6))) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01c0, code lost:
        if (java.lang.Double.doubleToLongBits(com.fossil.E43.C(r12, r6)) == java.lang.Double.doubleToLongBits(com.fossil.E43.C(r13, r6))) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x003a, code lost:
        if (com.fossil.H33.q(com.fossil.E43.F(r12, r6), com.fossil.E43.F(r13, r6)) != false) goto L_0x006c;
     */
    @DexIgnore
    @Override // com.fossil.F33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zza(T r12, T r13) {
        /*
        // Method dump skipped, instructions count: 642
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.zza(java.lang.Object, java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final int zzb(T t) {
        int i2;
        int i3;
        int i4;
        int c0;
        int H;
        int B0;
        int V;
        int h0;
        int p0;
        int B;
        int V2;
        int h02;
        int p02;
        if (this.g) {
            Unsafe unsafe = r;
            int i5 = 0;
            int i6 = 0;
            while (true) {
                int i7 = i6;
                if (i5 >= this.a.length) {
                    return d(this.n, t) + i7;
                }
                int H2 = H(i5);
                int i8 = (267386880 & H2) >>> 20;
                int i9 = this.a[i5];
                long j2 = (long) (H2 & 1048575);
                int i10 = (i8 < Y03.zza.zza() || i8 > Y03.zzb.zza()) ? 0 : this.a[i5 + 2] & 1048575;
                switch (i8) {
                    case 0:
                        if (t(t, i5)) {
                            B = L03.B(i9, 0.0d);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 1:
                        if (t(t, i5)) {
                            B = L03.C(i9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 2:
                        if (t(t, i5)) {
                            B = L03.c0(i9, E43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 3:
                        if (t(t, i5)) {
                            B = L03.i0(i9, E43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 4:
                        if (t(t, i5)) {
                            B = L03.m0(i9, E43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 5:
                        if (t(t, i5)) {
                            B = L03.r0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 6:
                        if (t(t, i5)) {
                            B = L03.y0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 7:
                        if (t(t, i5)) {
                            B = L03.H(i9, true);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 8:
                        if (t(t, i5)) {
                            Object F = E43.F(t, j2);
                            if (F instanceof Xz2) {
                                B = L03.U(i9, (Xz2) F);
                                break;
                            } else {
                                B = L03.G(i9, (String) F);
                                break;
                            }
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 9:
                        if (t(t, i5)) {
                            B = H33.a(i9, E43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 10:
                        if (t(t, i5)) {
                            B = L03.U(i9, (Xz2) E43.F(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 11:
                        if (t(t, i5)) {
                            B = L03.q0(i9, E43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 12:
                        if (t(t, i5)) {
                            B = L03.D0(i9, E43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 13:
                        if (t(t, i5)) {
                            B = L03.B0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 14:
                        if (t(t, i5)) {
                            B = L03.v0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 15:
                        if (t(t, i5)) {
                            B = L03.u0(i9, E43.b(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 16:
                        if (t(t, i5)) {
                            B = L03.n0(i9, E43.o(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 17:
                        if (t(t, i5)) {
                            B = L03.V(i9, (M23) E43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 18:
                        B = H33.U(i9, o(t, j2), false);
                        break;
                    case 19:
                        B = H33.R(i9, o(t, j2), false);
                        break;
                    case 20:
                        B = H33.d(i9, o(t, j2), false);
                        break;
                    case 21:
                        B = H33.t(i9, o(t, j2), false);
                        break;
                    case 22:
                        B = H33.H(i9, o(t, j2), false);
                        break;
                    case 23:
                        B = H33.U(i9, o(t, j2), false);
                        break;
                    case 24:
                        B = H33.R(i9, o(t, j2), false);
                        break;
                    case 25:
                        B = H33.X(i9, o(t, j2), false);
                        break;
                    case 26:
                        B = H33.b(i9, o(t, j2));
                        break;
                    case 27:
                        B = H33.c(i9, o(t, j2), k(i5));
                        break;
                    case 28:
                        B = H33.r(i9, o(t, j2));
                        break;
                    case 29:
                        B = H33.L(i9, o(t, j2), false);
                        break;
                    case 30:
                        B = H33.D(i9, o(t, j2), false);
                        break;
                    case 31:
                        B = H33.R(i9, o(t, j2), false);
                        break;
                    case 32:
                        B = H33.U(i9, o(t, j2), false);
                        break;
                    case 33:
                        B = H33.O(i9, o(t, j2), false);
                        break;
                    case 34:
                        B = H33.z(i9, o(t, j2), false);
                        break;
                    case 35:
                        V2 = H33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 36:
                        V2 = H33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 37:
                        V2 = H33.e((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 38:
                        V2 = H33.u((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 39:
                        V2 = H33.I((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 40:
                        V2 = H33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 41:
                        V2 = H33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 42:
                        V2 = H33.Y((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 43:
                        V2 = H33.M((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 44:
                        V2 = H33.E((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 45:
                        V2 = H33.S((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 46:
                        V2 = H33.V((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 47:
                        V2 = H33.P((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 48:
                        V2 = H33.A((List) unsafe.getObject(t, j2));
                        if (V2 > 0) {
                            if (this.h) {
                                unsafe.putInt(t, (long) i10, V2);
                            }
                            h02 = L03.h0(i9);
                            p02 = L03.p0(V2);
                            B = p02 + h02 + V2;
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 49:
                        B = H33.s(i9, o(t, j2), k(i5));
                        break;
                    case 50:
                        B = this.p.zza(i9, E43.F(t, j2), z(i5));
                        break;
                    case 51:
                        if (u(t, i9, i5)) {
                            B = L03.B(i9, 0.0d);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 52:
                        if (u(t, i9, i5)) {
                            B = L03.C(i9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 53:
                        if (u(t, i9, i5)) {
                            B = L03.c0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 54:
                        if (u(t, i9, i5)) {
                            B = L03.i0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 55:
                        if (u(t, i9, i5)) {
                            B = L03.m0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 56:
                        if (u(t, i9, i5)) {
                            B = L03.r0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 57:
                        if (u(t, i9, i5)) {
                            B = L03.y0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 58:
                        if (u(t, i9, i5)) {
                            B = L03.H(i9, true);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 59:
                        if (u(t, i9, i5)) {
                            Object F2 = E43.F(t, j2);
                            if (F2 instanceof Xz2) {
                                B = L03.U(i9, (Xz2) F2);
                                break;
                            } else {
                                B = L03.G(i9, (String) F2);
                                break;
                            }
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 60:
                        if (u(t, i9, i5)) {
                            B = H33.a(i9, E43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 61:
                        if (u(t, i9, i5)) {
                            B = L03.U(i9, (Xz2) E43.F(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 62:
                        if (u(t, i9, i5)) {
                            B = L03.q0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 63:
                        if (u(t, i9, i5)) {
                            B = L03.D0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 64:
                        if (u(t, i9, i5)) {
                            B = L03.B0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 65:
                        if (u(t, i9, i5)) {
                            B = L03.v0(i9, 0);
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 66:
                        if (u(t, i9, i5)) {
                            B = L03.u0(i9, I(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 67:
                        if (u(t, i9, i5)) {
                            B = L03.n0(i9, K(t, j2));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    case 68:
                        if (u(t, i9, i5)) {
                            B = L03.V(i9, (M23) E43.F(t, j2), k(i5));
                            break;
                        } else {
                            continue;
                            i6 = i7;
                            i5 += 3;
                        }
                    default:
                        i6 = i7;
                        i5 += 3;
                }
                i7 += B;
                i6 = i7;
                i5 += 3;
            }
        } else {
            Unsafe unsafe2 = r;
            int i11 = 0;
            int i12 = 0;
            int i13 = 1048575;
            int i14 = 0;
            while (i11 < this.a.length) {
                int H3 = H(i11);
                int[] iArr = this.a;
                int i15 = iArr[i11];
                int i16 = (267386880 & H3) >>> 20;
                if (i16 <= 17) {
                    i4 = iArr[i11 + 2];
                    int i17 = 1048575 & i4;
                    int i18 = 1 << (i4 >>> 20);
                    if (i17 != i13) {
                        i14 = unsafe2.getInt(t, (long) i17);
                        i13 = i17;
                    }
                    i2 = i18;
                    i3 = i13;
                } else {
                    i2 = 0;
                    i3 = i13;
                    i4 = (!this.h || i16 < Y03.zza.zza() || i16 > Y03.zzb.zza()) ? 0 : this.a[i11 + 2] & 1048575;
                }
                long j3 = (long) (1048575 & H3);
                switch (i16) {
                    case 0:
                        if ((i14 & i2) != 0) {
                            i12 += L03.B(i15, 0.0d);
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if ((i14 & i2) != 0) {
                            i12 += L03.C(i15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if ((i14 & i2) != 0) {
                            c0 = L03.c0(i15, unsafe2.getLong(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if ((i14 & i2) != 0) {
                            c0 = L03.i0(i15, unsafe2.getLong(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if ((i14 & i2) != 0) {
                            c0 = L03.m0(i15, unsafe2.getInt(t, j3));
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if ((i14 & i2) != 0) {
                            c0 = L03.r0(i15, 0);
                            i12 += c0;
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if ((i14 & i2) != 0) {
                            i12 += L03.y0(i15, 0);
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if ((i14 & i2) != 0) {
                            H = L03.H(i15, true);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if ((i14 & i2) != 0) {
                            Object object = unsafe2.getObject(t, j3);
                            H = object instanceof Xz2 ? L03.U(i15, (Xz2) object) : L03.G(i15, (String) object);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        if ((i14 & i2) != 0) {
                            H = H33.a(i15, unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 10:
                        if ((i14 & i2) != 0) {
                            H = L03.U(i15, (Xz2) unsafe2.getObject(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if ((i14 & i2) != 0) {
                            H = L03.q0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if ((i14 & i2) != 0) {
                            H = L03.D0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if ((i14 & i2) != 0) {
                            B0 = L03.B0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if ((i14 & i2) != 0) {
                            H = L03.v0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if ((i14 & i2) != 0) {
                            H = L03.u0(i15, unsafe2.getInt(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if ((i14 & i2) != 0) {
                            H = L03.n0(i15, unsafe2.getLong(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        if ((i14 & i2) != 0) {
                            H = L03.V(i15, (M23) unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 18:
                        H = H33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 19:
                        H = H33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 20:
                        H = H33.d(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 21:
                        H = H33.t(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 22:
                        H = H33.H(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 23:
                        H = H33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 24:
                        H = H33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 25:
                        H = H33.X(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 26:
                        H = H33.b(i15, (List) unsafe2.getObject(t, j3));
                        i12 += H;
                        break;
                    case 27:
                        H = H33.c(i15, (List) unsafe2.getObject(t, j3), k(i11));
                        i12 += H;
                        break;
                    case 28:
                        H = H33.r(i15, (List) unsafe2.getObject(t, j3));
                        i12 += H;
                        break;
                    case 29:
                        H = H33.L(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 30:
                        H = H33.D(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 31:
                        H = H33.R(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 32:
                        H = H33.U(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 33:
                        H = H33.O(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 34:
                        H = H33.z(i15, (List) unsafe2.getObject(t, j3), false);
                        i12 += H;
                        break;
                    case 35:
                        V = H33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 36:
                        V = H33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 37:
                        V = H33.e((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 38:
                        V = H33.u((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 39:
                        V = H33.I((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 40:
                        V = H33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 41:
                        V = H33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 42:
                        V = H33.Y((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 43:
                        V = H33.M((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 44:
                        V = H33.E((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 45:
                        V = H33.S((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 46:
                        V = H33.V((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 47:
                        V = H33.P((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 48:
                        V = H33.A((List) unsafe2.getObject(t, j3));
                        if (V <= 0) {
                            break;
                        } else {
                            if (this.h) {
                                unsafe2.putInt(t, (long) i4, V);
                            }
                            h0 = L03.h0(i15);
                            p0 = L03.p0(V);
                            B0 = p0 + h0 + V;
                            i12 += B0;
                            break;
                        }
                    case 49:
                        H = H33.s(i15, (List) unsafe2.getObject(t, j3), k(i11));
                        i12 += H;
                        break;
                    case 50:
                        H = this.p.zza(i15, unsafe2.getObject(t, j3), z(i11));
                        i12 += H;
                        break;
                    case 51:
                        if (u(t, i15, i11)) {
                            H = L03.B(i15, 0.0d);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 52:
                        if (u(t, i15, i11)) {
                            B0 = L03.C(i15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 53:
                        if (u(t, i15, i11)) {
                            H = L03.c0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 54:
                        if (u(t, i15, i11)) {
                            H = L03.i0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 55:
                        if (u(t, i15, i11)) {
                            H = L03.m0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 56:
                        if (u(t, i15, i11)) {
                            H = L03.r0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 57:
                        if (u(t, i15, i11)) {
                            B0 = L03.y0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 58:
                        if (u(t, i15, i11)) {
                            H = L03.H(i15, true);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 59:
                        if (u(t, i15, i11)) {
                            Object object2 = unsafe2.getObject(t, j3);
                            H = object2 instanceof Xz2 ? L03.U(i15, (Xz2) object2) : L03.G(i15, (String) object2);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        if (u(t, i15, i11)) {
                            H = H33.a(i15, unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 61:
                        if (u(t, i15, i11)) {
                            H = L03.U(i15, (Xz2) unsafe2.getObject(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 62:
                        if (u(t, i15, i11)) {
                            H = L03.q0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 63:
                        if (u(t, i15, i11)) {
                            H = L03.D0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 64:
                        if (u(t, i15, i11)) {
                            B0 = L03.B0(i15, 0);
                            i12 += B0;
                            break;
                        } else {
                            break;
                        }
                    case 65:
                        if (u(t, i15, i11)) {
                            H = L03.v0(i15, 0);
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 66:
                        if (u(t, i15, i11)) {
                            H = L03.u0(i15, I(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 67:
                        if (u(t, i15, i11)) {
                            H = L03.n0(i15, K(t, j3));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        if (u(t, i15, i11)) {
                            H = L03.V(i15, (M23) unsafe2.getObject(t, j3), k(i11));
                            i12 += H;
                            break;
                        } else {
                            break;
                        }
                }
                i11 += 3;
                i13 = i3;
            }
            int i19 = 0;
            int d2 = i12 + d(this.n, t);
            if (!this.f) {
                return d2;
            }
            T03<?> b2 = this.o.b(t);
            int i20 = 0;
            while (true) {
                int i21 = i19;
                if (i20 < b2.a.k()) {
                    Map.Entry<T, Object> i22 = b2.a.i(i20);
                    i19 = T03.a(i22.getKey(), i22.getValue()) + i21;
                    i20++;
                } else {
                    for (Map.Entry<T, Object> entry : b2.a.n()) {
                        i21 += T03.a(entry.getKey(), entry.getValue());
                    }
                    return d2 + i21;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void zzb(T t, T t2) {
        if (t2 != null) {
            for (int i2 = 0; i2 < this.a.length; i2 += 3) {
                int H = H(i2);
                long j2 = (long) (1048575 & H);
                int i3 = this.a[i2];
                switch ((H & 267386880) >>> 20) {
                    case 0:
                        if (t(t2, i2)) {
                            E43.f(t, j2, E43.C(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 1:
                        if (t(t2, i2)) {
                            E43.g(t, j2, E43.x(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 2:
                        if (t(t2, i2)) {
                            E43.i(t, j2, E43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 3:
                        if (t(t2, i2)) {
                            E43.i(t, j2, E43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 4:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 5:
                        if (t(t2, i2)) {
                            E43.i(t, j2, E43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 6:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 7:
                        if (t(t2, i2)) {
                            E43.k(t, j2, E43.w(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 8:
                        if (t(t2, i2)) {
                            E43.j(t, j2, E43.F(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 9:
                        s(t, t2, i2);
                        break;
                    case 10:
                        if (t(t2, i2)) {
                            E43.j(t, j2, E43.F(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 11:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 12:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 13:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 14:
                        if (t(t2, i2)) {
                            E43.i(t, j2, E43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 15:
                        if (t(t2, i2)) {
                            E43.h(t, j2, E43.b(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 16:
                        if (t(t2, i2)) {
                            E43.i(t, j2, E43.o(t2, j2));
                            A(t, i2);
                            break;
                        } else {
                            break;
                        }
                    case 17:
                        s(t, t2, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.m.b(t, t2, j2);
                        break;
                    case 50:
                        H33.n(this.p, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (u(t2, i3, i2)) {
                            E43.j(t, j2, E43.F(t2, j2));
                            B(t, i3, i2);
                            break;
                        } else {
                            break;
                        }
                    case 60:
                        D(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (u(t2, i3, i2)) {
                            E43.j(t, j2, E43.F(t2, j2));
                            B(t, i3, i2);
                            break;
                        } else {
                            break;
                        }
                    case 68:
                        D(t, t2, i2);
                        break;
                }
            }
            H33.o(this.n, t, t2);
            if (this.f) {
                H33.m(this.o, t, t2);
                return;
            }
            return;
        }
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void zzc(T t) {
        int i2;
        int i3 = this.j;
        while (true) {
            i2 = this.k;
            if (i3 >= i2) {
                break;
            }
            long H = (long) (H(this.i[i3]) & 1048575);
            Object F = E43.F(t, H);
            if (F != null) {
                E43.j(t, H, this.p.b(F));
            }
            i3++;
        }
        int length = this.i.length;
        for (int i4 = i2; i4 < length; i4++) {
            this.m.d(t, (long) this.i[i4]);
        }
        this.n.j(t);
        if (this.f) {
            this.o.g(t);
        }
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.F33] */
    /* JADX WARN: Type inference failed for: r0v39 */
    /* JADX WARN: Type inference failed for: r0v41, types: [com.fossil.F33] */
    /* JADX WARN: Type inference failed for: r0v48 */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0064 A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.F33
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean zzd(T r15) {
        /*
        // Method dump skipped, instructions count: 291
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q23.zzd(java.lang.Object):boolean");
    }
}
