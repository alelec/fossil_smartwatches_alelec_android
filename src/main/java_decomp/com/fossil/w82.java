package com.fossil;

import android.os.Bundle;
import com.fossil.Fc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W82 implements Fc2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ T82 a;

    @DexIgnore
    public W82(T82 t82) {
        this.a = t82;
    }

    @DexIgnore
    @Override // com.fossil.Fc2.Ai
    public final boolean c() {
        return this.a.n();
    }

    @DexIgnore
    @Override // com.fossil.Fc2.Ai
    public final Bundle y() {
        return null;
    }
}
