package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I96 implements Factory<ComplicationSearchPresenter> {
    @DexIgnore
    public static ComplicationSearchPresenter a(D96 d96, ComplicationRepository complicationRepository, An4 an4) {
        return new ComplicationSearchPresenter(d96, complicationRepository, an4);
    }
}
