package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Cl3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Aq3 c;

    @DexIgnore
    public Zp3(Aq3 aq3, Cl3 cl3) {
        this.c = aq3;
        this.b = cl3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.c) {
            Aq3.c(this.c, false);
            if (!this.c.c.V()) {
                this.c.c.d().N().a("Connected to service");
                this.c.c.L(this.b);
            }
        }
    }
}
