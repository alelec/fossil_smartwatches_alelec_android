package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zs4 implements Ys4 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Xs4> b;
    @DexIgnore
    public /* final */ Vh c;
    @DexIgnore
    public /* final */ Vh d;
    @DexIgnore
    public /* final */ Vh e;
    @DexIgnore
    public /* final */ Vh f;
    @DexIgnore
    public /* final */ Vh g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<Xs4> {
        @DexIgnore
        public Ai(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Xs4 xs4) {
            if (xs4.d() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, xs4.d());
            }
            if (xs4.i() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, xs4.i());
            }
            if (xs4.b() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, xs4.b());
            }
            if (xs4.e() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, xs4.e());
            }
            if (xs4.g() == null) {
                mi.bindNull(5);
            } else {
                mi.bindLong(5, (long) xs4.g().intValue());
            }
            if (xs4.h() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, xs4.h());
            }
            mi.bindLong(7, xs4.a() ? 1 : 0);
            mi.bindLong(8, (long) xs4.f());
            mi.bindLong(9, (long) xs4.c());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Xs4 xs4) {
            a(mi, xs4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `friend` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`confirmation`,`pin`,`friendType`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE friend SET confirmation = ? AND friendType = ? AND pin = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM friend WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Vh {
        @DexIgnore
        public Di(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 2 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Vh {
        @DexIgnore
        public Ei(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = 1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Vh {
        @DexIgnore
        public Fi(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM friend WHERE friendType = -1 AND pin = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends Vh {
        @DexIgnore
        public Gi(Zs4 zs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM friend";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements Callable<List<Xs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Hi(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public List<Xs4> a() throws Exception {
            Cursor b2 = Ex0.b(Zs4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "socialId");
                int c3 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
                int c4 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
                int c5 = Dx0.c(b2, "points");
                int c6 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
                int c7 = Dx0.c(b2, "confirmation");
                int c8 = Dx0.c(b2, "pin");
                int c9 = Dx0.c(b2, "friendType");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new Xs4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.isNull(c5) ? null : Integer.valueOf(b2.getInt(c5)), b2.getString(c6), b2.getInt(c7) != 0, b2.getInt(c8), b2.getInt(c9)));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ List<Xs4> call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore
    public Zs4(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
        new Bi(this, oh);
        this.c = new Ci(this, oh);
        this.d = new Di(this, oh);
        this.e = new Ei(this, oh);
        this.f = new Fi(this, oh);
        this.g = new Gi(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.g.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public int b(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public LiveData<List<Xs4>> c() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType <> 3 ORDER BY friendType DESC, firstName COLLATE NOCASE ASC", 0);
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Hi hi = new Hi(f2);
        return invalidationTracker.d(new String[]{"friend"}, false, hi);
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> d() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType = 2 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public int e() {
        int i = 0;
        Rh f2 = Rh.f("SELECT COUNT(*) FROM friend WHERE friendType = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i = b2.getInt(0);
            }
            return i;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> f() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType = 1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public void g() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.e.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public void h() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.d.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> i() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE pin = 2", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public Long[] insert(List<Xs4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public void j() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.f.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> k() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType = 0 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> l() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType = 0 OR friendType = 1 OR friendType = -1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public int m(String[] strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder b2 = Hx0.b();
        b2.append("DELETE FROM friend WHERE id in (");
        Hx0.a(b2, strArr.length);
        b2.append(")");
        Mi compileStatement = this.a.compileStatement(b2.toString());
        int i = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindString(i, str);
            }
            i++;
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public List<Xs4> n() {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE friendType = -1 AND pin = 0", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Xs4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public long o(Xs4 xs4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(xs4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ys4
    public Xs4 p(String str) {
        Rh f2 = Rh.f("SELECT * FROM friend WHERE id =? LIMIT 1", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Xs4 xs4 = null;
        Integer num = null;
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "confirmation");
            int c9 = Dx0.c(b2, "pin");
            int c10 = Dx0.c(b2, "friendType");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                String string4 = b2.getString(c5);
                if (!b2.isNull(c6)) {
                    num = Integer.valueOf(b2.getInt(c6));
                }
                xs4 = new Xs4(string, string2, string3, string4, num, b2.getString(c7), b2.getInt(c8) != 0, b2.getInt(c9), b2.getInt(c10));
            }
            return xs4;
        } finally {
            b2.close();
            f2.m();
        }
    }
}
