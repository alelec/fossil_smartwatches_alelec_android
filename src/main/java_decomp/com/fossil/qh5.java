package com.fossil;

import android.text.TextUtils;
import com.mapped.Qg6;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Qh5 {
    FEMALE(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX),
    MALE("M"),
    OTHER("O");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public /* final */ String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Qh5 a(String str) {
            if (TextUtils.isEmpty(str)) {
                return Qh5.OTHER;
            }
            Qh5[] values = Qh5.values();
            for (Qh5 qh5 : values) {
                if (Vt7.j(str, qh5.getValue(), true)) {
                    return qh5;
                }
            }
            return Qh5.OTHER;
        }
    }

    @DexIgnore
    public Qh5(String str) {
        this.value = str;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
