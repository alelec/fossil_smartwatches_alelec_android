package com.fossil;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.av5;
import com.fossil.dv5;
import com.fossil.ev5;
import com.fossil.fv5;
import com.fossil.gv5;
import com.fossil.ht5;
import com.fossil.hu5;
import com.fossil.hv5;
import com.fossil.iq4;
import com.fossil.iu5;
import com.fossil.ju5;
import com.fossil.ku5;
import com.fossil.n27;
import com.fossil.o27;
import com.fossil.ru5;
import com.fossil.su5;
import com.fossil.tu5;
import com.fossil.uu5;
import com.fossil.vu5;
import com.fossil.z27;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz6 extends oz6 {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ Pattern T;
    @DexIgnore
    public static /* final */ a U; // = new a(null);
    @DexIgnore
    public ck5 A;
    @DexIgnore
    public vu5 B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public hu5 F;
    @DexIgnore
    public iu5 G;
    @DexIgnore
    public z27 H;
    @DexIgnore
    public v27 I;
    @DexIgnore
    public WatchLocalizationRepository J;
    @DexIgnore
    public on5 K;
    @DexIgnore
    public vt4 L;
    @DexIgnore
    public pr4 M;
    @DexIgnore
    public WorkoutSettingRepository N;
    @DexIgnore
    public String O;
    @DexIgnore
    public String P;
    @DexIgnore
    public /* final */ pz6 Q;
    @DexIgnore
    public /* final */ ls5 R;
    @DexIgnore
    public dv5 e;
    @DexIgnore
    public xn5 f;
    @DexIgnore
    public ev5 g;
    @DexIgnore
    public hv5 h;
    @DexIgnore
    public gv5 i;
    @DexIgnore
    public fv5 j;
    @DexIgnore
    public UserRepository k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public uq4 m;
    @DexIgnore
    public tu5 n;
    @DexIgnore
    public uu5 o;
    @DexIgnore
    public ru5 p;
    @DexIgnore
    public su5 q;
    @DexIgnore
    public ku5 r;
    @DexIgnore
    public ju5 s;
    @DexIgnore
    public AlarmsRepository t;
    @DexIgnore
    public ht5 u;
    @DexIgnore
    public mj5 v;
    @DexIgnore
    public av5 w;
    @DexIgnore
    public on5 x;
    @DexIgnore
    public n27 y;
    @DexIgnore
    public o27 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return uz6.S;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<vu5.a, iq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3678a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(uz6 uz6) {
            this.f3678a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(iq4.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(uz6.U.a(), "Get current user failed");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(vu5.a aVar) {
            pq7.c(aVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Get current user success: " + aVar.a());
            MFUser a3 = aVar.a();
            if (a3 != null) {
                this.f3678a.J().w(a3.getUserId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1", f = "SignUpPresenter.kt", l = {574, 575, 579, 584, 587}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uz6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1$currentUser$1", f = "SignUpPresenter.kt", l = {587}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository S = this.this$0.this$0.S();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = S.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uz6 uz6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = uz6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x012f  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x017f  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0182  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0186  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 393
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uz6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.e<o27.c, o27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3679a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public d(uz6 uz6, SignUpSocialAuth signUpSocialAuth) {
            this.f3679a = uz6;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(o27.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f3679a.Q.h();
            this.f3679a.V(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(o27.c cVar) {
            pq7.c(cVar, "responseValue");
            boolean a2 = cVar.a();
            if (a2) {
                this.f3679a.Y(this.b);
            } else if (!a2) {
                this.f3679a.Q.g4(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1", f = "SignUpPresenter.kt", l = {526, 537}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uz6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1", f = "SignUpPresenter.kt", l = {527, 528, 529, 530, HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 532, 533}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0054  */
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x00c5  */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0038  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                // Method dump skipped, instructions count: 246
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.uz6.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1", f = "SignUpPresenter.kt", l = {537}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<Alarm>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<Alarm>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    AlarmsRepository I = this.this$0.this$0.I();
                    this.L$0 = iv7;
                    this.label = 1;
                    Object activeAlarms = I.getActiveAlarms(this);
                    return activeAlarms == d ? d : activeAlarms;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(uz6 uz6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = uz6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0054  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
                r0 = r8
            L_0x0017:
                java.util.List r0 = (java.util.List) r0
                if (r0 != 0) goto L_0x0020
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
            L_0x0020:
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                java.util.List r0 = com.fossil.dj5.a(r0)
                r1.q1(r0)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x003f:
                com.fossil.uz6 r2 = r7.this$0
                com.fossil.dv7 r2 = com.fossil.uz6.x(r2)
                com.fossil.uz6$e$b r3 = new com.fossil.uz6$e$b
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r7)
                if (r0 != r1) goto L_0x0017
                r0 = r1
                goto L_0x002f
            L_0x0056:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.uz6 r2 = r7.this$0
                com.fossil.dv7 r2 = com.fossil.uz6.y(r2)
                com.fossil.uz6$e$a r3 = new com.fossil.uz6$e$a
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = com.fossil.eu7.g(r2, r3, r7)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uz6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements iq4.e<fv5.c, fv5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3680a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(uz6 uz6) {
            this.f3680a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(fv5.a aVar) {
            pq7.c(aVar, "errorValue");
            this.f3680a.Q.h();
            this.f3680a.V(aVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(fv5.c cVar) {
            pq7.c(cVar, "responseValue");
            PortfolioApp.h0.c().M().z0(this.f3680a);
            this.f3680a.Z();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements iq4.e<av5.c, av5.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3681a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onError$1", f = "SignUpPresenter.kt", l = {514}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ av5.a $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, av5.a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
                this.$errorValue = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$errorValue, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository S = this.this$0.f3681a.S();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (S.clearAllUser(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.f3681a.Q.h();
                this.this$0.f3681a.V(this.$errorValue.a(), this.$errorValue.b());
                return tl7.f3441a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1", f = "SignUpPresenter.kt", l = {427, 436, HelpSearchRecyclerViewAdapter.TYPE_NO_RESULTS, 448, 464, 472, 503}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $currentUser;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$1", f = "SignUpPresenter.kt", l = {427}, m = "invokeSuspend")
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        UserRepository S = this.this$0.this$0.f3681a.S();
                        this.L$0 = iv7;
                        this.label = 1;
                        if (S.clearAllUser(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return tl7.f3441a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uz6$g$b$b")
            @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$2", f = "SignUpPresenter.kt", l = {442, 443}, m = "invokeSuspend")
            /* renamed from: com.fossil.uz6$g$b$b  reason: collision with other inner class name */
            public static final class C0254b extends ko7 implements vp7<iv7, qn7<? super iq5<UserSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0254b(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0254b bVar = new C0254b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<UserSettings>> qn7) {
                    throw null;
                    //return ((C0254b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    iv7 iv7;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 = this.p$;
                        PortfolioApp c = PortfolioApp.h0.c();
                        this.L$0 = iv7;
                        this.label = 1;
                        if (c.g2(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 = (iv7) this.L$0;
                        el7.b(obj);
                    } else if (i == 2) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    UserRepository S = this.this$0.this$0.f3681a.S();
                    this.L$0 = iv7;
                    this.label = 2;
                    Object userSettingFromServer = S.getUserSettingFromServer(this);
                    return userSettingFromServer == d ? d : userSettingFromServer;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class c implements iq4.e<uy6, sy6> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ b f3682a;
                @DexIgnore
                public /* final */ /* synthetic */ dr7 b;
                @DexIgnore
                public /* final */ /* synthetic */ dr7 c;
                @DexIgnore
                public /* final */ /* synthetic */ List d;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1", f = "SignUpPresenter.kt", l = {491}, m = "invokeSuspend")
                public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ sy6 $errorValue;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ c this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uz6$g$b$c$a$a")
                    @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1$1", f = "SignUpPresenter.kt", l = {491}, m = "invokeSuspend")
                    /* renamed from: com.fossil.uz6$g$b$c$a$a  reason: collision with other inner class name */
                    public static final class C0255a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                        @DexIgnore
                        public Object L$0;
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public iv7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0255a(a aVar, qn7 qn7) {
                            super(2, qn7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                            pq7.c(qn7, "completion");
                            C0255a aVar = new C0255a(this.this$0, qn7);
                            aVar.p$ = (iv7) obj;
                            throw null;
                            //return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.vp7
                        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                            throw null;
                            //return ((C0255a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final Object invokeSuspend(Object obj) {
                            Object d = yn7.d();
                            int i = this.label;
                            if (i == 0) {
                                el7.b(obj);
                                iv7 iv7 = this.p$;
                                UserRepository S = this.this$0.this$0.f3682a.this$0.f3681a.S();
                                this.L$0 = iv7;
                                this.label = 1;
                                if (S.clearAllUser(this) == d) {
                                    return d;
                                }
                            } else if (i == 1) {
                                iv7 iv72 = (iv7) this.L$0;
                                el7.b(obj);
                            } else {
                                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                            }
                            return tl7.f3441a;
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public a(c cVar, sy6 sy6, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = cVar;
                        this.$errorValue = sy6;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        a aVar = new a(this.this$0, this.$errorValue, qn7);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        throw null;
                        //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            dv7 h = this.this$0.f3682a.this$0.f3681a.h();
                            C0255a aVar = new C0255a(this, null);
                            this.L$0 = iv7;
                            this.label = 1;
                            if (eu7.g(h, aVar, this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        this.this$0.f3682a.this$0.f3681a.Q.h();
                        this.this$0.f3682a.this$0.f3681a.V(this.$errorValue.a(), this.$errorValue.b());
                        return tl7.f3441a;
                    }
                }

                @DexIgnore
                public c(b bVar, dr7 dr7, dr7 dr72, List list) {
                    this.f3682a = bVar;
                    this.b = dr7;
                    this.c = dr72;
                    this.d = list;
                }

                @DexIgnore
                /* renamed from: b */
                public void a(sy6 sy6) {
                    pq7.c(sy6, "errorValue");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = uz6.U.a();
                    local.d(a2, "onLoginSuccess download device setting fail " + sy6.a());
                    xw7 unused = gu7.d(this.f3682a.this$0.f3681a.k(), null, null, new a(this, sy6, null), 3, null);
                }

                @DexIgnore
                /* renamed from: c */
                public void onSuccess(uy6 uy6) {
                    pq7.c(uy6, "responseValue");
                    PortfolioApp.h0.c().n1(this.b.element, this.c.element);
                    FLogger.INSTANCE.getLocal().d(uz6.U.a(), "onLoginSuccess download device setting success");
                    for (cl7 cl7 : this.d) {
                        PortfolioApp.h0.c().F1((String) cl7.getFirst(), (String) cl7.getSecond());
                    }
                    this.f3682a.this$0.f3681a.a0(this.b.element);
                    PortfolioApp.h0.c().S1(this.f3682a.this$0.f3681a.L(), false, 13);
                    this.f3682a.this$0.f3681a.D();
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$response$1", f = "SignUpPresenter.kt", l = {448}, m = "invokeSuspend")
            public static final class d extends ko7 implements vp7<iv7, qn7<? super iq5<ApiResponse<Device>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(b bVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    d dVar = new d(this.this$0, qn7);
                    dVar.p$ = (iv7) obj;
                    throw null;
                    //return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super iq5<ApiResponse<Device>>> qn7) {
                    throw null;
                    //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        DeviceRepository K = this.this$0.this$0.f3681a.K();
                        this.L$0 = iv7;
                        this.label = 1;
                        Object downloadDeviceList = K.downloadDeviceList(this);
                        return downloadDeviceList == d ? d : downloadDeviceList;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(g gVar, MFUser mFUser, qn7 qn7) {
                super(2, qn7);
                this.this$0 = gVar;
                this.$currentUser = mFUser;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, this.$currentUser, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0126  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x01a9  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01d3  */
            /* JADX WARNING: Removed duplicated region for block: B:55:0x0296  */
            /* JADX WARNING: Removed duplicated region for block: B:63:0x0301  */
            /* JADX WARNING: Removed duplicated region for block: B:71:0x034f  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 870
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.uz6.g.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(uz6 uz6) {
            this.f3681a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(av5.a aVar) {
            pq7.c(aVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + aVar.a());
            xw7 unused = gu7.d(this.f3681a.k(), null, null, new a(this, aVar, null), 3, null);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(av5.c cVar) {
            pq7.c(cVar, "responseValue");
            xw7 unused = gu7.d(this.f3681a.k(), null, null, new b(this, cVar.a(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements iq4.e<ht5.e, ht5.d> {
        @DexIgnore
        /* renamed from: b */
        public void a(ht5.d dVar) {
            pq7.c(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ht5.e eVar) {
            pq7.c(eVar, "responseValue");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements iq4.e<z27.c, z27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3683a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth b;

        @DexIgnore
        public i(uz6 uz6, SignUpEmailAuth signUpEmailAuth) {
            this.f3683a = uz6;
            this.b = signUpEmailAuth;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(z27.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f3683a.Q.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "requestOtpCode errorCode=" + bVar.a() + " message=" + bVar.b());
            this.f3683a.Q.l3(bVar.a(), bVar.b());
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(z27.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f3683a.Q.h();
            this.f3683a.Q.y3(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements iq4.e<n27.c, n27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3684a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public j(uz6 uz6, String str, String str2) {
            this.f3684a = uz6;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(n27.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f3684a.Q.h();
            this.f3684a.V(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(n27.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f3684a.F();
            boolean a2 = cVar.a();
            if (a2) {
                this.f3684a.Q.h();
                pz6 pz6 = this.f3684a.Q;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131887007);
                pq7.b(c2, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
                pz6.g6(c2);
                this.f3684a.C();
            } else if (!a2) {
                SignUpEmailAuth signUpEmailAuth = new SignUpEmailAuth();
                signUpEmailAuth.setEmail(this.b);
                signUpEmailAuth.setPassword(this.c);
                this.f3684a.b0(signUpEmailAuth);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements iq4.e<dv5.d, dv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3685a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(uz6 uz6) {
            this.f3685a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(dv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.f3685a.Q.h();
            if (2 != cVar.a()) {
                this.f3685a.V(cVar.a(), "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(dv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.f3685a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements iq4.e<ev5.d, ev5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3686a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public l(uz6 uz6) {
            this.f3686a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(ev5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.f3686a.Q.h();
            if (2 != cVar.a()) {
                this.f3686a.V(cVar.a(), "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(ev5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.f3686a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements iq4.e<gv5.d, gv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3687a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public m(uz6 uz6) {
            this.f3687a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(gv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.f3687a.Q.h();
            this.f3687a.V(cVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(gv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.f3687a.E(dVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements iq4.e<hv5.d, hv5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ uz6 f3688a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public n(uz6 uz6) {
            this.f3688a = uz6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(hv5.c cVar) {
            pq7.c(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.f3688a.Q.h();
            this.f3688a.V(cVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(hv5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uz6.U.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.f3688a.E(dVar.a());
        }
    }

    /*
    static {
        String simpleName = uz6.class.getSimpleName();
        pq7.b(simpleName, "SignUpPresenter::class.java.simpleName");
        S = simpleName;
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            T = compile;
        } else {
            pq7.i();
            throw null;
        }
    }
    */

    @DexIgnore
    public uz6(pz6 pz6, ls5 ls5) {
        pq7.c(pz6, "mView");
        pq7.c(ls5, "mContext");
        this.Q = pz6;
        this.R = ls5;
    }

    @DexIgnore
    public final void C() {
        vu5 vu5 = this.B;
        if (vu5 != null) {
            vu5.e(null, new b(this));
        } else {
            pq7.n("mGetUser");
            throw null;
        }
    }

    @DexIgnore
    public final xw7 D() {
        return gu7.d(k(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final void E(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "checkSocialAccountIsExisted " + signUpSocialAuth);
        o27 o27 = this.z;
        if (o27 != null) {
            o27.e(new o27.a(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new d(this, signUpSocialAuth));
        } else {
            pq7.n("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(av6.R.a(), "downloadOptionalsResources");
        Date date = new Date();
        xw7 unused = gu7.d(k(), null, null, new e(this, null), 3, null);
        su5 su5 = this.q;
        if (su5 != null) {
            su5.e(new su5.a(date), null);
            ru5 ru5 = this.p;
            if (ru5 != null) {
                ru5.e(new ru5.a(date), null);
                tu5 tu5 = this.n;
                if (tu5 != null) {
                    tu5.e(new tu5.a(date), null);
                    uu5 uu5 = this.o;
                    if (uu5 != null) {
                        uu5.e(new uu5.a(date), null);
                        ku5 ku5 = this.r;
                        if (ku5 != null) {
                            ku5.e(new ku5.a(date), null);
                            ju5 ju5 = this.s;
                            if (ju5 != null) {
                                ju5.e(new ju5.a(date), null);
                                iu5 iu5 = this.G;
                                if (iu5 != null) {
                                    iu5.e(new iu5.a(date), null);
                                    hu5 hu5 = this.F;
                                    if (hu5 != null) {
                                        hu5.e(new hu5.a(date), null);
                                    } else {
                                        pq7.n("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    pq7.n("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                pq7.n("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            pq7.n("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        pq7.n("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    pq7.n("mFetchSleepSessions");
                    throw null;
                }
            } else {
                pq7.n("mFetchActivities");
                throw null;
            }
        } else {
            pq7.n("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final vt4 G() {
        vt4 vt4 = this.L;
        if (vt4 != null) {
            return vt4;
        }
        pq7.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final pr4 H() {
        pr4 pr4 = this.M;
        if (pr4 != null) {
            return pr4;
        }
        pq7.n("flagRepository");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository I() {
        AlarmsRepository alarmsRepository = this.t;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        pq7.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final ck5 J() {
        ck5 ck5 = this.A;
        if (ck5 != null) {
            return ck5;
        }
        pq7.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository K() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final mj5 L() {
        mj5 mj5 = this.v;
        if (mj5 != null) {
            return mj5;
        }
        pq7.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final v27 M() {
        v27 v27 = this.I;
        if (v27 != null) {
            return v27;
        }
        pq7.n("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository N() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        pq7.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final on5 O() {
        on5 on5 = this.K;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final on5 P() {
        on5 on5 = this.x;
        if (on5 != null) {
            return on5;
        }
        pq7.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository Q() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        pq7.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository R() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        pq7.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository S() {
        UserRepository userRepository = this.k;
        if (userRepository != null) {
            return userRepository;
        }
        pq7.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository T() {
        WatchLocalizationRepository watchLocalizationRepository = this.J;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        pq7.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingRepository U() {
        WorkoutSettingRepository workoutSettingRepository = this.N;
        if (workoutSettingRepository != null) {
            return workoutSettingRepository;
        }
        pq7.n("mWorkoutSettingRepository");
        throw null;
    }

    @DexIgnore
    public final void V(int i2, String str) {
        pq7.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local.d(str2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.Q.C(i2, str);
        } else if (!o37.b(PortfolioApp.h0.c())) {
            this.Q.C(601, "");
        } else {
            this.Q.C(i2, "");
        }
    }

    @DexIgnore
    public final boolean W() {
        String str = this.O;
        if (str == null || str.length() == 0) {
            this.Q.n0(false, false, "");
        } else if (!b47.a(this.O)) {
            pz6 pz6 = this.Q;
            String c2 = um5.c(PortfolioApp.h0.c(), 2131887009);
            pq7.b(c2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            pz6.n0(false, true, c2);
        } else {
            this.Q.n0(true, false, "");
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean X() {
        if (!TextUtils.isEmpty(this.P)) {
            String str = this.P;
            if (str != null) {
                boolean z2 = str.length() >= 7;
                boolean matches = T.matcher(this.P).matches();
                this.Q.U3(z2, matches);
                if (z2 && matches) {
                    return true;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            this.Q.U3(false, false);
        }
        return false;
    }

    @DexIgnore
    public final void Y(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        fv5 fv5 = this.j;
        if (fv5 != null) {
            fv5.e(new fv5.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new f(this));
        } else {
            pq7.n("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void Z() {
        FLogger.INSTANCE.getLocal().d(S, "onLoginSuccess download user info");
        av5 av5 = this.w;
        if (av5 != null) {
            av5.e(new av5.b(), new g(this));
        } else {
            pq7.n("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void a0(String str) {
        pq7.c(str, "activeSerial");
        ht5 ht5 = this.u;
        if (ht5 != null) {
            ht5.e(new ht5.c(str), new h());
        } else {
            pq7.n("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void b0(SignUpEmailAuth signUpEmailAuth) {
        pq7.c(signUpEmailAuth, "emailAuth");
        z27 z27 = this.H;
        if (z27 != null) {
            z27.e(new z27.a(signUpEmailAuth.getEmail()), new i(this, signUpEmailAuth));
        } else {
            pq7.n("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public void c0() {
        this.Q.M5(this);
    }

    @DexIgnore
    public final void d0() {
        Locale locale = Locale.getDefault();
        pq7.b(locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            pq7.b(locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                pq7.b(locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append(LocaleConverter.LOCALE_DELIMITER);
                Locale locale4 = Locale.getDefault();
                pq7.b(locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = S;
                local.d(str, "language: " + sb2);
                if (vt7.j(sb2, "zh_CN", true) || vt7.j(sb2, "zh_SG", true) || vt7.j(sb2, "zh_TW", true)) {
                    this.Q.d3(true);
                    return;
                } else {
                    this.Q.d3(false);
                    return;
                }
            }
        }
        this.Q.d3(false);
    }

    @DexIgnore
    public final void e0() {
        boolean z2 = TextUtils.isEmpty(this.O) || TextUtils.isEmpty(this.P);
        boolean W = W();
        boolean X = X();
        if (z2 || !X || !W) {
            this.Q.B4();
        } else {
            this.Q.x2();
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.Q.f();
        e0();
        d0();
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void n() {
        if (PortfolioApp.h0.c().p0()) {
            this.Q.R();
        } else {
            V(601, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void o(boolean z2) {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void p(String str) {
        pq7.c(str, Constants.EMAIL);
        this.O = str;
        W();
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void q(String str) {
        pq7.c(str, "password");
        this.P = str;
        e0();
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void r(SignUpSocialAuth signUpSocialAuth) {
        pq7.c(signUpSocialAuth, "auth");
        this.Q.i();
        E(signUpSocialAuth);
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void s(String str, String str2) {
        pq7.c(str, Constants.EMAIL);
        pq7.c(str2, "password");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = S;
        local.d(str3, "signupEmail " + str + ' ' + str2);
        if (W()) {
            this.Q.i();
            n27 n27 = this.y;
            if (n27 != null) {
                n27.e(new n27.a(str), new j(this, str, str2));
            } else {
                pq7.n("mCheckAuthenticationEmailExisting");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void t() {
        this.Q.i();
        dv5 dv5 = this.e;
        if (dv5 != null) {
            dv5.e(new dv5.b(new WeakReference(this.R)), new k(this));
        } else {
            pq7.n("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void u() {
        this.Q.i();
        ev5 ev5 = this.g;
        if (ev5 != null) {
            ev5.e(new ev5.b(new WeakReference(this.R)), new l(this));
        } else {
            pq7.n("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void v() {
        if (!dk5.g.j(this.R, "com.tencent.mm")) {
            dk5.g.k(this.R, "com.tencent.mm");
            return;
        }
        this.Q.i();
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.e(new gv5.b(new WeakReference(this.R)), new m(this));
        } else {
            pq7.n("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.oz6
    public void w() {
        this.Q.i();
        hv5 hv5 = this.h;
        if (hv5 != null) {
            hv5.e(new hv5.b(new WeakReference(this.R)), new n(this));
        } else {
            pq7.n("mLoginWeiboUseCase");
            throw null;
        }
    }
}
