package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wj5 extends Wa1 {
    @DexIgnore
    public Wj5(Oa1 oa1, Di1 di1, Ii1 ii1, Context context) {
        super(oa1, di1, ii1, context);
    }

    @DexIgnore
    public <ResourceType> Vj5<ResourceType> C(Class<ResourceType> cls) {
        return new Vj5<>(this.b, this, cls, this.c);
    }

    @DexIgnore
    public Vj5<Bitmap> D() {
        return (Vj5) super.e();
    }

    @DexIgnore
    public Vj5<Drawable> E() {
        return (Vj5) super.g();
    }

    @DexIgnore
    public Vj5<Drawable> F(Bitmap bitmap) {
        return (Vj5) super.p(bitmap);
    }

    @DexIgnore
    public Vj5<Drawable> G(Uri uri) {
        return (Vj5) super.q(uri);
    }

    @DexIgnore
    public Vj5<Drawable> H(Integer num) {
        return (Vj5) super.r(num);
    }

    @DexIgnore
    public Vj5<Drawable> I(Object obj) {
        return (Vj5) super.s(obj);
    }

    @DexIgnore
    public Vj5<Drawable> J(String str) {
        return (Vj5) super.t(str);
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public /* bridge */ /* synthetic */ Va1 c(Class cls) {
        return C(cls);
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public /* bridge */ /* synthetic */ Va1 e() {
        return D();
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public /* bridge */ /* synthetic */ Va1 g() {
        return E();
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public /* bridge */ /* synthetic */ Va1 r(Integer num) {
        return H(num);
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public /* bridge */ /* synthetic */ Va1 t(String str) {
        return J(str);
    }

    @DexIgnore
    @Override // com.fossil.Wa1
    public void y(Fj1 fj1) {
        if (fj1 instanceof Uj5) {
            super.y(fj1);
        } else {
            super.y(new Uj5().w0(fj1));
        }
    }
}
