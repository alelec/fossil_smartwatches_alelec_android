package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rh0 {
    @DexIgnore
    public ColorStateList a;
    @DexIgnore
    public PorterDuff.Mode b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public void a() {
        this.a = null;
        this.d = false;
        this.b = null;
        this.c = false;
    }
}
