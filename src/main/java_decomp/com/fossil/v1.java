package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V1 implements Parcelable.Creator<X1> {
    @DexIgnore
    public /* synthetic */ V1(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public X1 createFromParcel(Parcel parcel) {
        return new X1(parcel.readLong());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public X1[] newArray(int i) {
        return new X1[i];
    }
}
