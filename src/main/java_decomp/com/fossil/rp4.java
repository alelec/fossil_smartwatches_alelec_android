package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rp4 implements Factory<MigrationManager> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<GetHybridDeviceSettingUseCase> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> f;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> g;
    @DexIgnore
    public /* final */ Provider<DeviceDao> h;
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> i;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> j;
    @DexIgnore
    public /* final */ Provider<Cj4> k;
    @DexIgnore
    public /* final */ Provider<FlagRepository> l;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> m;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<FileRepository> o;
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> p;
    @DexIgnore
    public /* final */ Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> q;
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> r;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> s;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> t;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> u;

    @DexIgnore
    public Rp4(Uo4 uo4, Provider<An4> provider, Provider<UserRepository> provider2, Provider<GetHybridDeviceSettingUseCase> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<Cj4> provider10, Provider<FlagRepository> provider11, Provider<DianaPresetRepository> provider12, Provider<WatchFaceRepository> provider13, Provider<FileRepository> provider14, Provider<WFBackgroundPhotoRepository> provider15, Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> provider16, Provider<WFAssetRepository> provider17, Provider<DeviceRepository> provider18, Provider<DianaAppSettingRepository> provider19, Provider<DianaWatchFaceRepository> provider20) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
        this.p = provider15;
        this.q = provider16;
        this.r = provider17;
        this.s = provider18;
        this.t = provider19;
        this.u = provider20;
    }

    @DexIgnore
    public static Rp4 a(Uo4 uo4, Provider<An4> provider, Provider<UserRepository> provider2, Provider<GetHybridDeviceSettingUseCase> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<DeviceDao> provider7, Provider<HybridCustomizeDatabase> provider8, Provider<MicroAppLastSettingRepository> provider9, Provider<Cj4> provider10, Provider<FlagRepository> provider11, Provider<DianaPresetRepository> provider12, Provider<WatchFaceRepository> provider13, Provider<FileRepository> provider14, Provider<WFBackgroundPhotoRepository> provider15, Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> provider16, Provider<WFAssetRepository> provider17, Provider<DeviceRepository> provider18, Provider<DianaAppSettingRepository> provider19, Provider<DianaWatchFaceRepository> provider20) {
        return new Rp4(uo4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    public static MigrationManager c(Uo4 uo4, An4 an4, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, Cj4 cj4, FlagRepository flagRepository, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository dianaPresetRepository2, WFAssetRepository wFAssetRepository, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        MigrationManager y = uo4.y(an4, userRepository, getHybridDeviceSettingUseCase, notificationsRepository, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, cj4, flagRepository, dianaPresetRepository, watchFaceRepository, fileRepository, wFBackgroundPhotoRepository, dianaPresetRepository2, wFAssetRepository, deviceRepository, dianaAppSettingRepository, dianaWatchFaceRepository);
        Lk7.c(y, "Cannot return null from a non-@Nullable @Provides method");
        return y;
    }

    @DexIgnore
    public MigrationManager b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get(), this.s.get(), this.t.get(), this.u.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
