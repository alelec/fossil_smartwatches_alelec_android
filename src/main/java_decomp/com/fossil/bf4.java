package com.fossil;

import android.content.BroadcastReceiver;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Bf4 implements Ht3 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ BroadcastReceiver.PendingResult b;

    @DexIgnore
    public Bf4(boolean z, BroadcastReceiver.PendingResult pendingResult) {
        this.a = z;
        this.b = pendingResult;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3 nt3) {
        FirebaseInstanceIdReceiver.d(this.a, this.b, nt3);
    }
}
