package com.fossil;

import com.mapped.Wg6;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xm7 {
    @DexIgnore
    public static final <K, V> V a(Map<K, ? extends V> map, K k) {
        Wg6.c(map, "$this$getOrImplicitDefault");
        if (map instanceof Wm7) {
            return (V) ((Wm7) map).d(k);
        }
        V v = (V) map.get(k);
        if (v != null || map.containsKey(k)) {
            return v;
        }
        throw new NoSuchElementException("Key " + ((Object) k) + " is missing in the map.");
    }
}
