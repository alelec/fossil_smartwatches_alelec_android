package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cq extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C;
    @DexIgnore
    public /* final */ Fs D;

    @DexIgnore
    public Cq(K5 k5, I60 i60, Yp yp, Fs fs) {
        super(k5, i60, yp, null, false, 24);
        this.D = fs;
        ArrayList<Ow> arrayList = this.i;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Fs fs2 = this.D;
        if (!(fs2 instanceof Ht) && !(fs2 instanceof Ts)) {
            if (fs2 instanceof Mu) {
                linkedHashSet.add(Ow.c);
            } else if (fs2 instanceof Fv) {
                linkedHashSet.add(Ow.d);
                linkedHashSet.add(Ow.e);
            } else if (fs2 instanceof Zs) {
                linkedHashSet.add(Ow.e);
            } else if (fs2 instanceof Dv) {
                linkedHashSet.add(Ow.d);
            } else if ((fs2 instanceof Tv) || (fs2 instanceof Pv)) {
                linkedHashSet.add(Ow.d);
                linkedHashSet.add(Ow.e);
            } else if (fs2 instanceof Bt) {
                linkedHashSet.add(Ow.f);
            } else if ((fs2 instanceof Us) || (fs2 instanceof Ws) || (fs2 instanceof Ls)) {
                linkedHashSet.add(Ow.g);
            } else if (fs2 instanceof Ks) {
                linkedHashSet.add(((Ks) fs2).L.a());
            } else if (fs2 instanceof Ps) {
                linkedHashSet.add(((Ps) fs2).K().a());
                linkedHashSet.add(((Ps) this.D).N().a());
            } else {
                linkedHashSet.add(Ow.b);
            }
        }
        this.C = By1.a(arrayList, new ArrayList(linkedHashSet));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public final void B() {
        Lp.i(this, this.D, new Po(this), Cp.b, null, new Pp(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return Gy1.c(super.C(), this.D.z());
    }

    @DexIgnore
    public void G(Fs fs) {
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
