package com.fossil;

import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nr extends Ox1 {
    @DexIgnore
    public /* final */ Yp b;
    @DexIgnore
    public /* final */ Zq c;
    @DexIgnore
    public /* final */ Mw d;
    @DexIgnore
    public /* final */ Ax1 e;

    @DexIgnore
    public Nr(Yp yp, Zq zq, Mw mw, Ax1 ax1) {
        this.b = yp;
        this.c = zq;
        this.d = mw;
        this.e = ax1;
    }

    @DexIgnore
    public /* synthetic */ Nr(Yp yp, Zq zq, Mw mw, Ax1 ax1, int i) {
        Ax1 ax12 = null;
        yp = (i & 1) != 0 ? Yp.b : yp;
        Mw mw2 = (i & 4) != 0 ? new Mw(null, null, Lw.b, null, null, 27) : mw;
        ax12 = (i & 8) == 0 ? ax1 : ax12;
        this.b = yp;
        this.c = zq;
        this.d = mw2;
        this.e = ax12;
    }

    @DexIgnore
    public static /* synthetic */ Nr a(Nr nr, Yp yp, Zq zq, Mw mw, Ax1 ax1, int i) {
        if ((i & 1) != 0) {
            yp = nr.b;
        }
        if ((i & 2) != 0) {
            zq = nr.c;
        }
        if ((i & 4) != 0) {
            mw = nr.d;
        }
        if ((i & 8) != 0) {
            ax1 = nr.e;
        }
        return nr.a(yp, zq, mw, ax1);
    }

    @DexIgnore
    public final Nr a(Yp yp, Zq zq, Mw mw, Ax1 ax1) {
        return new Nr(yp, zq, mw, ax1);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Nr) {
                Nr nr = (Nr) obj;
                if (!Wg6.a(this.b, nr.b) || !Wg6.a(this.c, nr.c) || !Wg6.a(this.d, nr.d) || !Wg6.a(this.e, nr.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Yp yp = this.b;
        int hashCode = yp != null ? yp.hashCode() : 0;
        Zq zq = this.c;
        int hashCode2 = zq != null ? zq.hashCode() : 0;
        Mw mw = this.d;
        int hashCode3 = mw != null ? mw.hashCode() : 0;
        Ax1 ax1 = this.e;
        if (ax1 != null) {
            i = ax1.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(G80.k(jSONObject, Jd0.i4, Ey1.a(this.b)), Jd0.O0, Ey1.a(this.c));
            if (this.d.d != Lw.b) {
                G80.k(jSONObject, Jd0.j4, this.d.toJSONObject());
            }
            Jd0 jd0 = Jd0.e6;
            Ax1 ax1 = this.e;
            G80.k(jSONObject, jd0, ax1 != null ? ax1.toJSONObject() : null);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e2 = E.e("Result(phaseId=");
        e2.append(this.b);
        e2.append(", resultCode=");
        e2.append(this.c);
        e2.append(", requestResult=");
        e2.append(this.d);
        e2.append(", networkError=");
        e2.append(this.e);
        e2.append(")");
        return e2.toString();
    }
}
