package com.fossil;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ba3 extends Binder implements IInterface {
    @DexIgnore
    public Ba3(String str) {
        attachInterface(this, str);
    }

    @DexIgnore
    public IBinder asBinder() {
        return this;
    }

    @DexIgnore
    public abstract boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException;

    @DexIgnore
    @Override // android.os.Binder
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        boolean z;
        if (i > 16777215) {
            z = super.onTransact(i, parcel, parcel2, i2);
        } else {
            parcel.enforceInterface(getInterfaceDescriptor());
            z = false;
        }
        if (z) {
            return true;
        }
        return d(i, parcel, parcel2, i2);
    }
}
