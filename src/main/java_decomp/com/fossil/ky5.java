package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ky5 extends U47 {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public static /* final */ Ai D; // = new Ai(null);
    @DexIgnore
    public String A;
    @DexIgnore
    public HashMap B;
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<I15> l;
    @DexIgnore
    public List<? extends Date> m; // = Hm7.e();
    @DexIgnore
    public Date s; // = Xy4.a.a();
    @DexIgnore
    public String[] t; // = new String[0];
    @DexIgnore
    public Long[] u; // = new Long[0];
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public Ly5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ky5.C;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ I15 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ky5 b;

        @DexIgnore
        public Bi(I15 i15, Ky5 ky5) {
            this.a = i15;
            this.b = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ky5.D.a();
            local.e(a2, "dateStrPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            if (i2 == 0) {
                Ky5 ky5 = this.b;
                ky5.P6(ky5.w);
                Ky5 ky52 = this.b;
                ky52.R6(ky52.v);
                NumberPicker numberPicker2 = this.a.u;
                Wg6.b(numberPicker2, "hourPicker");
                if (numberPicker2.getValue() < this.b.w) {
                    Ky5 ky53 = this.b;
                    ky53.x = ky53.w;
                    NumberPicker numberPicker3 = this.a.u;
                    Wg6.b(numberPicker3, "hourPicker");
                    numberPicker3.setValue(this.b.x);
                }
                NumberPicker numberPicker4 = this.a.v;
                Wg6.b(numberPicker4, "minutePicker");
                if (numberPicker4.getValue() < this.b.v) {
                    Ky5 ky54 = this.b;
                    ky54.y = ky54.v;
                    NumberPicker numberPicker5 = this.a.v;
                    Wg6.b(numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.b.v);
                    return;
                }
                return;
            }
            this.b.P6(0);
            NumberPicker numberPicker6 = this.a.u;
            Wg6.b(numberPicker6, "hourPicker");
            numberPicker6.setValue(this.b.x);
            this.b.R6(0);
            NumberPicker numberPicker7 = this.a.v;
            Wg6.b(numberPicker7, "minutePicker");
            numberPicker7.setValue(this.b.y);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ I15 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ky5 b;

        @DexIgnore
        public Ci(I15 i15, Ky5 ky5) {
            this.a = i15;
            this.b = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ky5.D.a();
            local.e(a2, "hourPicker.onValueChanged: old: " + i + " - newVal: " + i2);
            this.b.x = i2;
            if (i2 == this.b.w) {
                NumberPicker numberPicker2 = this.a.s;
                Wg6.b(numberPicker2, "dateStrPicker");
                if (numberPicker2.getValue() == 0) {
                    Ky5 ky5 = this.b;
                    ky5.R6(ky5.v);
                    NumberPicker numberPicker3 = this.a.v;
                    Wg6.b(numberPicker3, "minutePicker");
                    if (numberPicker3.getValue() < this.b.v) {
                        Ky5 ky52 = this.b;
                        ky52.y = ky52.v;
                        NumberPicker numberPicker4 = this.a.v;
                        Wg6.b(numberPicker4, "minutePicker");
                        numberPicker4.setValue(this.b.v);
                        return;
                    }
                    return;
                }
            }
            this.b.R6(0);
            NumberPicker numberPicker5 = this.a.v;
            Wg6.b(numberPicker5, "minutePicker");
            numberPicker5.setValue(this.b.y);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Ky5 a;

        @DexIgnore
        public Di(Ky5 ky5) {
            this.a = ky5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            this.a.y = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ky5.D.a();
            local.e(a2, "minutePicker.onValueChanged: old: " + i + " - newVal: " + i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ky5 b;

        @DexIgnore
        public Ei(Ky5 ky5) {
            this.b = ky5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Date M6 = this.b.M6();
            Ly5 ly5 = this.b.z;
            if (ly5 != null) {
                ly5.a(M6);
            }
            this.b.dismiss();
        }
    }

    /*
    static {
        String simpleName = Ky5.class.getSimpleName();
        Wg6.b(simpleName, "BottomDateTimeDialog::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    public final void L6() {
        if (!this.m.isEmpty()) {
            G37<I15> g37 = this.l;
            if (g37 != null) {
                I15 a2 = g37.a();
                if (a2 != null) {
                    NumberPicker numberPicker = a2.s;
                    Wg6.b(numberPicker, "dateStrPicker");
                    numberPicker.setMinValue(0);
                    NumberPicker numberPicker2 = a2.s;
                    Wg6.b(numberPicker2, "dateStrPicker");
                    numberPicker2.setMaxValue(this.t.length - 1);
                    a2.s.setDisplayedValues(this.t);
                    P6(0);
                    R6(0);
                    NumberPicker numberPicker3 = a2.s;
                    Wg6.b(numberPicker3, "dateStrPicker");
                    numberPicker3.setValue(1);
                    NumberPicker numberPicker4 = a2.u;
                    Wg6.b(numberPicker4, "hourPicker");
                    numberPicker4.setValue(this.w);
                    NumberPicker numberPicker5 = a2.v;
                    Wg6.b(numberPicker5, "minutePicker");
                    numberPicker5.setValue(this.v);
                    return;
                }
                return;
            }
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final Date M6() {
        G37<I15> g37 = this.l;
        if (g37 != null) {
            I15 a2 = g37.a();
            if (a2 == null) {
                return new Date();
            }
            Long[] lArr = this.u;
            NumberPicker numberPicker = a2.s;
            Wg6.b(numberPicker, "dateStrPicker");
            long longValue = lArr[numberPicker.getValue()].longValue();
            NumberPicker numberPicker2 = a2.u;
            Wg6.b(numberPicker2, "hourPicker");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = a2.v;
            Wg6.b(numberPicker3, "minutePicker");
            return new Date(longValue + ((long) (value * 60 * 60 * 1000)) + ((long) (numberPicker3.getValue() * 60 * 1000)));
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        if (!this.m.isEmpty()) {
            Lc6<String[], Long[]> f = Py4.f(this.m);
            this.t = f.getFirst();
            this.u = f.getSecond();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = C;
            StringBuilder sb = new StringBuilder();
            sb.append("dateStrValues: ");
            String[] strArr = this.t;
            ArrayList arrayList = new ArrayList(strArr.length);
            for (String str2 : strArr) {
                arrayList.add(str2);
            }
            sb.append(arrayList);
            sb.append(" - dateStrMetadata: ");
            Long[] lArr = this.u;
            ArrayList arrayList2 = new ArrayList(lArr.length);
            for (Long l2 : lArr) {
                arrayList2.add(Long.valueOf(l2.longValue()));
            }
            sb.append(arrayList2);
            local.e(str, sb.toString());
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTime(this.s);
            this.w = instance.get(11);
            int i = instance.get(12);
            this.v = i;
            this.x = this.w;
            this.y = i;
        }
    }

    @DexIgnore
    public final void O6(List<? extends Date> list) {
        Wg6.c(list, "newData");
        this.m = list;
        if (!list.isEmpty()) {
            this.s = (Date) list.get(0);
        }
    }

    @DexIgnore
    public final void P6(int i) {
        G37<I15> g37 = this.l;
        if (g37 != null) {
            I15 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.u;
                Wg6.b(numberPicker, "hourPicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.u;
                Wg6.b(numberPicker2, "hourPicker");
                numberPicker2.setMaxValue(23);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6(Ly5 ly5) {
        Wg6.c(ly5, "listener");
        this.z = ly5;
    }

    @DexIgnore
    public final void R6(int i) {
        G37<I15> g37 = this.l;
        if (g37 != null) {
            I15 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                Wg6.b(numberPicker, "minutePicker");
                numberPicker.setMinValue(i);
                NumberPicker numberPicker2 = a2.v;
                Wg6.b(numberPicker2, "minutePicker");
                numberPicker2.setMaxValue(59);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        G37<I15> g37 = this.l;
        if (g37 != null) {
            I15 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                Wg6.b(flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.A);
                a2.s.setOnValueChangedListener(new Bi(a2, this));
                a2.u.setOnValueChangedListener(new Ci(a2, this));
                a2.v.setOnValueChangedListener(new Di(this));
                a2.q.setOnClickListener(new Ei(this));
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        I15 i15 = (I15) Aq0.f(layoutInflater, 2131558442, viewGroup, false, this.k);
        this.l = new G37<>(this, i15);
        Wg6.b(i15, "binding");
        return i15.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        N6();
        L6();
    }

    @DexIgnore
    public final void setTitle(String str) {
        Wg6.c(str, "title");
        this.A = str;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.B;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
