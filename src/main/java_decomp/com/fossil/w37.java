package com.fossil;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.widget.FilterQueryProvider;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W37 {
    @DexIgnore
    public static /* final */ W37 a; // = new W37();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements FilterQueryProvider {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        public final Cursor runQuery(CharSequence charSequence) {
            String str;
            Wg6.b(charSequence, "constraint");
            if (charSequence.length() == 0) {
                str = "has_phone_number!=0 AND mimetype=?";
            } else {
                str = "(display_name LIKE '%" + charSequence + "%' OR display_name LIKE 'N%" + charSequence + "%') AND has_phone_number!=0 AND mimetype=?";
            }
            return PortfolioApp.get.instance().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, str, new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
        }
    }

    @DexIgnore
    public final FilterQueryProvider a() {
        return Ai.a;
    }
}
