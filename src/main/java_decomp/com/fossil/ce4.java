package com.fossil;

import android.util.Base64;
import android.util.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Date;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ce4 implements Td4, Vd4 {
    @DexIgnore
    public Ce4 a; // = null;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public /* final */ JsonWriter c;
    @DexIgnore
    public /* final */ Map<Class<?>, Sd4<?>> d;
    @DexIgnore
    public /* final */ Map<Class<?>, Ud4<?>> e;
    @DexIgnore
    public /* final */ Sd4<Object> f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public Ce4(Writer writer, Map<Class<?>, Sd4<?>> map, Map<Class<?>, Ud4<?>> map2, Sd4<Object> sd4, boolean z) {
        this.c = new JsonWriter(writer);
        this.d = map;
        this.e = map2;
        this.f = sd4;
        this.g = z;
    }

    @DexIgnore
    @Override // com.fossil.Td4
    public /* bridge */ /* synthetic */ Td4 a(String str, boolean z) throws IOException {
        n(str, z);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Td4
    public /* bridge */ /* synthetic */ Td4 b(String str, long j) throws IOException {
        l(str, j);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Td4
    public /* bridge */ /* synthetic */ Td4 c(String str, int i) throws IOException {
        k(str, i);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Vd4
    public /* bridge */ /* synthetic */ Vd4 d(String str) throws IOException {
        j(str);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Vd4
    public /* bridge */ /* synthetic */ Vd4 e(boolean z) throws IOException {
        o(z);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Td4
    public /* bridge */ /* synthetic */ Td4 f(String str, Object obj) throws IOException {
        m(str, obj);
        return this;
    }

    @DexIgnore
    public Ce4 g(int i) throws IOException {
        v();
        this.c.value((long) i);
        return this;
    }

    @DexIgnore
    public Ce4 h(long j) throws IOException {
        v();
        this.c.value(j);
        return this;
    }

    @DexIgnore
    public Ce4 i(Object obj, boolean z) throws IOException {
        int i = 0;
        if (!z || !q(obj)) {
            if (obj == null) {
                this.c.nullValue();
            } else if (obj instanceof Number) {
                this.c.value((Number) obj);
            } else if (obj.getClass().isArray()) {
                if (obj instanceof byte[]) {
                    p((byte[]) obj);
                } else {
                    this.c.beginArray();
                    if (obj instanceof int[]) {
                        int[] iArr = (int[]) obj;
                        int length = iArr.length;
                        while (i < length) {
                            this.c.value((long) iArr[i]);
                            i++;
                        }
                    } else if (obj instanceof long[]) {
                        long[] jArr = (long[]) obj;
                        int length2 = jArr.length;
                        while (i < length2) {
                            h(jArr[i]);
                            i++;
                        }
                    } else if (obj instanceof double[]) {
                        double[] dArr = (double[]) obj;
                        int length3 = dArr.length;
                        while (i < length3) {
                            this.c.value(dArr[i]);
                            i++;
                        }
                    } else if (obj instanceof boolean[]) {
                        boolean[] zArr = (boolean[]) obj;
                        int length4 = zArr.length;
                        while (i < length4) {
                            this.c.value(zArr[i]);
                            i++;
                        }
                    } else if (obj instanceof Number[]) {
                        for (Number number : (Number[]) obj) {
                            i(number, false);
                        }
                    } else {
                        for (Object obj2 : (Object[]) obj) {
                            i(obj2, false);
                        }
                    }
                    this.c.endArray();
                }
            } else if (obj instanceof Collection) {
                this.c.beginArray();
                for (Object obj3 : (Collection) obj) {
                    i(obj3, false);
                }
                this.c.endArray();
            } else if (obj instanceof Map) {
                this.c.beginObject();
                for (Map.Entry entry : ((Map) obj).entrySet()) {
                    Object key = entry.getKey();
                    try {
                        m((String) key, entry.getValue());
                    } catch (ClassCastException e2) {
                        throw new Rd4(String.format("Only String keys are currently supported in maps, got %s of type %s instead.", key, key.getClass()), e2);
                    }
                }
                this.c.endObject();
            } else {
                Sd4<?> sd4 = this.d.get(obj.getClass());
                if (sd4 != null) {
                    s(sd4, obj, z);
                } else {
                    Ud4<?> ud4 = this.e.get(obj.getClass());
                    if (ud4 != null) {
                        ud4.a(obj, this);
                    } else if (obj instanceof Enum) {
                        j(((Enum) obj).name());
                    } else {
                        s(this.f, obj, z);
                    }
                }
            }
            return this;
        }
        throw new Rd4(String.format("%s cannot be encoded inline", obj == null ? null : obj.getClass()));
    }

    @DexIgnore
    public Ce4 j(String str) throws IOException {
        v();
        this.c.value(str);
        return this;
    }

    @DexIgnore
    public Ce4 k(String str, int i) throws IOException {
        v();
        this.c.name(str);
        g(i);
        return this;
    }

    @DexIgnore
    public Ce4 l(String str, long j) throws IOException {
        v();
        this.c.name(str);
        h(j);
        return this;
    }

    @DexIgnore
    public Ce4 m(String str, Object obj) throws IOException {
        if (this.g) {
            u(str, obj);
        } else {
            t(str, obj);
        }
        return this;
    }

    @DexIgnore
    public Ce4 n(String str, boolean z) throws IOException {
        v();
        this.c.name(str);
        o(z);
        return this;
    }

    @DexIgnore
    public Ce4 o(boolean z) throws IOException {
        v();
        this.c.value(z);
        return this;
    }

    @DexIgnore
    public Ce4 p(byte[] bArr) throws IOException {
        v();
        if (bArr == null) {
            this.c.nullValue();
        } else {
            this.c.value(Base64.encodeToString(bArr, 2));
        }
        return this;
    }

    @DexIgnore
    public final boolean q(Object obj) {
        return obj == null || obj.getClass().isArray() || (obj instanceof Collection) || (obj instanceof Date) || (obj instanceof Enum) || (obj instanceof Number);
    }

    @DexIgnore
    public void r() throws IOException {
        v();
        this.c.flush();
    }

    @DexIgnore
    public Ce4 s(Sd4<Object> sd4, Object obj, boolean z) throws IOException {
        if (!z) {
            this.c.beginObject();
        }
        sd4.a(obj, this);
        if (!z) {
            this.c.endObject();
        }
        return this;
    }

    @DexIgnore
    public final Ce4 t(String str, Object obj) throws IOException, Rd4 {
        v();
        this.c.name(str);
        if (obj == null) {
            this.c.nullValue();
        } else {
            i(obj, false);
        }
        return this;
    }

    @DexIgnore
    public final Ce4 u(String str, Object obj) throws IOException, Rd4 {
        if (obj != null) {
            v();
            this.c.name(str);
            i(obj, false);
        }
        return this;
    }

    @DexIgnore
    public final void v() throws IOException {
        if (this.b) {
            Ce4 ce4 = this.a;
            if (ce4 != null) {
                ce4.v();
                this.a.b = false;
                this.a = null;
                this.c.endObject();
                return;
            }
            return;
        }
        throw new IllegalStateException("Parent context used since this context was created. Cannot use this context anymore.");
    }
}
