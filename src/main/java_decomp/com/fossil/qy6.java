package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qy6 {
    @DexIgnore
    public static /* final */ String d; // = "PairingUpdateFWPresenter";
    @DexIgnore
    public /* final */ Ai a; // = new Ai(this);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Ux6 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Qy6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Qy6 qy6) {
            this.a = qy6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && Vt7.j(otaEvent.getSerial(), this.a.c(), true)) {
                this.a.b().Z((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    @DexIgnore
    public Qy6(String str, Ux6 ux6) {
        Wg6.c(str, "serial");
        Wg6.c(ux6, "mView");
        this.b = str;
        this.c = ux6;
    }

    @DexIgnore
    public final void a() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.b);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "serial=" + this.b + ", mCurrentDeviceType=" + deviceBySerial);
        if (DeviceHelper.o.w(deviceBySerial)) {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886916));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886917));
            explore2.setBackground(2131231351);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886914));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886915));
            explore4.setBackground(2131231350);
        } else {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886923));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886922));
            explore2.setBackground(2131231355);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886920));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886921));
            explore4.setBackground(2131231352);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.c.X(arrayList);
    }

    @DexIgnore
    public final Ux6 b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final void d() {
        PortfolioApp instance = PortfolioApp.get.instance();
        Ai ai = this.a;
        instance.registerReceiver(ai, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        a();
        this.c.f();
    }

    @DexIgnore
    public final void e() {
        PortfolioApp.get.instance().unregisterReceiver(this.a);
    }

    @DexIgnore
    public final void f() {
        if (!DeviceIdentityUtils.isDianaDevice(this.b)) {
            this.c.R6();
        }
    }
}
