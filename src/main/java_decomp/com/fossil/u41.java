package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U41<TResult> {
    @DexIgnore
    public /* final */ T41<TResult> a; // = new T41<>();

    @DexIgnore
    public T41<TResult> a() {
        return this.a;
    }

    @DexIgnore
    public void b() {
        if (!e()) {
            throw new IllegalStateException("Cannot cancel a completed task.");
        }
    }

    @DexIgnore
    public void c(Exception exc) {
        if (!f(exc)) {
            throw new IllegalStateException("Cannot set the error on a completed task.");
        }
    }

    @DexIgnore
    public void d(TResult tresult) {
        if (!g(tresult)) {
            throw new IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    @DexIgnore
    public boolean e() {
        return this.a.v();
    }

    @DexIgnore
    public boolean f(Exception exc) {
        return this.a.w(exc);
    }

    @DexIgnore
    public boolean g(TResult tresult) {
        return this.a.x(tresult);
    }
}
