package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bm3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ Xl3 e;

    @DexIgnore
    public Bm3(Xl3 xl3, String str, long j) {
        this.e = xl3;
        Rc2.g(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.B().getLong(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void b(long j) {
        SharedPreferences.Editor edit = this.e.B().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
