package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pd1 implements Yd1 {
    @DexIgnore
    public /* final */ Bi a; // = new Bi();
    @DexIgnore
    public /* final */ Ud1<Ai, Bitmap> b; // = new Ud1<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Zd1 {
        @DexIgnore
        public /* final */ Bi a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public Bitmap.Config d;

        @DexIgnore
        public Ai(Bi bi) {
            this.a = bi;
        }

        @DexIgnore
        @Override // com.fossil.Zd1
        public void a() {
            this.a.c(this);
        }

        @DexIgnore
        public void b(int i, int i2, Bitmap.Config config) {
            this.b = i;
            this.c = i2;
            this.d = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            return this.b == ai.b && this.c == ai.c && this.d == ai.d;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b;
            int i2 = this.c;
            Bitmap.Config config = this.d;
            return (config != null ? config.hashCode() : 0) + (((i * 31) + i2) * 31);
        }

        @DexIgnore
        public String toString() {
            return Pd1.f(this.b, this.c, this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Qd1<Ai> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.Zd1' to match base method */
        @Override // com.fossil.Qd1
        public /* bridge */ /* synthetic */ Ai a() {
            return d();
        }

        @DexIgnore
        public Ai d() {
            return new Ai(this);
        }

        @DexIgnore
        public Ai e(int i, int i2, Bitmap.Config config) {
            Ai ai = (Ai) b();
            ai.b(i, i2, config);
            return ai;
        }
    }

    @DexIgnore
    public static String f(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    @DexIgnore
    public static String g(Bitmap bitmap) {
        return f(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public String a(int i, int i2, Bitmap.Config config) {
        return f(i, i2, config);
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public void b(Bitmap bitmap) {
        this.b.d(this.a.e(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        return this.b.a(this.a.e(i, i2, config));
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public String d(Bitmap bitmap) {
        return g(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public int e(Bitmap bitmap) {
        return Jk1.h(bitmap);
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public Bitmap removeLast() {
        return this.b.f();
    }

    @DexIgnore
    public String toString() {
        return "AttributeStrategy:\n  " + this.b;
    }
}
