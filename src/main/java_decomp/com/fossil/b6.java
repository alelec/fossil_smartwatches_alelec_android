package com.fossil;

import android.bluetooth.BluetoothGatt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B6 extends U5 {
    @DexIgnore
    public /* final */ N5 k; // = N5.d;
    @DexIgnore
    public F5 l; // = F5.b;

    @DexIgnore
    public B6(N4 n4) {
        super(V5.d, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        int i = H5.b[k5.w.ordinal()];
        if (i == 1) {
            k5.b.post(new O4(k5, new G7(F7.b, 0, 2), F5.b));
        } else if (i == 2) {
            k5.c(3, 0);
            BluetoothGatt bluetoothGatt = k5.c;
            if (bluetoothGatt != null) {
                Ky1 ky1 = Ky1.DEBUG;
                bluetoothGatt.disconnect();
            }
            k5.c(0, 0);
        } else if (i == 3) {
            k5.c(3, 0);
            BluetoothGatt bluetoothGatt2 = k5.c;
            if (bluetoothGatt2 != null) {
                Ky1 ky12 = Ky1.DEBUG;
                bluetoothGatt2.disconnect();
            } else {
                Ky1 ky13 = Ky1.DEBUG;
            }
            k5.c(0, 0);
        }
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void f(H7 h7) {
        S5 a2;
        k(h7);
        G7 g7 = h7.a;
        if (g7.b == F7.b) {
            a2 = this.l == F5.b ? S5.a(this.e, null, R5.b, null, 5) : S5.a(this.e, null, R5.e, null, 5);
        } else {
            S5 a3 = S5.e.a(g7);
            a2 = S5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public N5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return (h7 instanceof Z6) && ((Z6) h7).b == F5.b;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.f;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.l = ((Z6) h7).b;
    }
}
