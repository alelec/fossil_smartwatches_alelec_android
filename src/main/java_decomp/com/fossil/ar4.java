package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.AnimationImageView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ar4 extends RecyclerView.g<Ai> {
    @DexIgnore
    public static /* final */ String c; // = "ExploreWatchAdapter";
    @DexIgnore
    public ArrayList<Explore> a; // = new ArrayList<>();
    @DexIgnore
    public Ai b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ AnimationImageView c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(View view) {
            super(view);
            Wg6.c(view, "itemView");
            View findViewById = view.findViewById(2131362546);
            Wg6.b(findViewById, "itemView.findViewById(R.id.ftv_title)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362405);
            Wg6.b(findViewById2, "itemView.findViewById(R.id.ftv_desc)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362691);
            Wg6.b(findViewById3, "itemView.findViewById(R.id.iv_device)");
            this.c = (AnimationImageView) findViewById3;
        }

        @DexIgnore
        public final AnimationImageView a() {
            return this.c;
        }

        @DexIgnore
        public final void b(Explore explore) {
            if (explore != null) {
                this.a.setText(explore.getTitle());
                this.b.setText(explore.getDescription());
                Explore.ExploreType exploreType = explore.getExploreType();
                if (exploreType != null) {
                    int i = Zq4.a[exploreType.ordinal()];
                    if (i == 1) {
                        AnimationImageView animationImageView = this.c;
                        String string = PortfolioApp.get.instance().getString(2131887271);
                        Wg6.b(string, "PortfolioApp.instance.ge\u2026ng.animation_wrist_flick)");
                        animationImageView.q(string, 1, 80, 30, MFNetworkReturnCode.BAD_REQUEST, 2000);
                    } else if (i == 2) {
                        AnimationImageView animationImageView2 = this.c;
                        String string2 = PortfolioApp.get.instance().getString(2131887269);
                        Wg6.b(string2, "PortfolioApp.instance.ge\u2026ing.animation_double_tap)");
                        animationImageView2.q(string2, 1, 50, 30, 600, 2400);
                    } else if (i == 3) {
                        AnimationImageView animationImageView3 = this.c;
                        String string3 = PortfolioApp.get.instance().getString(2131887270);
                        Wg6.b(string3, "PortfolioApp.instance.ge\u2026animation_press_and_hold)");
                        animationImageView3.q(string3, 1, 50, 30, 1000, 3000);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void g(Ai ai, int i) {
        Wg6.c(ai, "welcomeViewHolder");
        ai.b(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public Ai h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558726, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(inflate);
    }

    @DexIgnore
    public final void i() {
        AnimationImageView a2;
        FLogger.INSTANCE.getLocal().d(c, "onStopAnimation");
        Ai ai = this.b;
        if (ai != null && (a2 = ai.a()) != null) {
            a2.r();
        }
    }

    @DexIgnore
    public void j(Ai ai) {
        Wg6.c(ai, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewAttachedToWindow");
        this.b = ai;
        ai.a().n();
        super.onViewAttachedToWindow(ai);
    }

    @DexIgnore
    public void k(Ai ai) {
        Wg6.c(ai, "holder");
        FLogger.INSTANCE.getLocal().d(c, "onViewDetachedFromWindow");
        ai.a().r();
        super.onViewDetachedFromWindow(ai);
    }

    @DexIgnore
    public final void l(List<? extends Explore> list) {
        Wg6.c(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        g(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onViewAttachedToWindow(Ai ai) {
        j(ai);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onViewDetachedFromWindow(Ai ai) {
        k(ai);
    }
}
