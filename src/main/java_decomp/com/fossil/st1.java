package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class St1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ry1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<St1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public St1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Nq1.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<Ri\u2026class.java.classLoader)!!");
                Nq1 nq1 = (Nq1) readParcelable;
                Nt1 nt1 = (Nt1) parcel.readParcelable(Nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(Ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new St1(nq1, nt1, (Ry1) readParcelable2);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public St1[] newArray(int i) {
            return new St1[i];
        }
    }

    @DexIgnore
    public St1(Nq1 nq1, Nt1 nt1, Ry1 ry1) {
        super(nq1, nt1);
        this.d = ry1;
    }

    @DexIgnore
    public St1(Nq1 nq1, Ry1 ry1) {
        super(nq1, null);
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(super.a(), Jd0.t3, this.d.toString());
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        try {
            I9 i9 = I9.d;
            X90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9.a(s, ry1, new Ob0(((Lq1) deviceRequest).c(), new Ry1(this.d.getMajor(), this.d.getMinor())).a());
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (Sx1 e) {
            D90.i.i(e);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(St1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.d, ((St1) obj).d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData");
    }

    @DexIgnore
    public final Ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
