package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jw4 implements Factory<BCOverviewLeaderBoardViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Jw4(Provider<Tt4> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Jw4 a(Provider<Tt4> provider, Provider<An4> provider2) {
        return new Jw4(provider, provider2);
    }

    @DexIgnore
    public static BCOverviewLeaderBoardViewModel c(Tt4 tt4, An4 an4) {
        return new BCOverviewLeaderBoardViewModel(tt4, an4);
    }

    @DexIgnore
    public BCOverviewLeaderBoardViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
