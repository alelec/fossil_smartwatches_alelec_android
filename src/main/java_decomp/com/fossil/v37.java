package com.fossil;

import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V37 {
    @DexIgnore
    public static FlutterEngine a; // = FlutterEngineCache.getInstance().get("screenshot_engine_id");
    @DexIgnore
    public static FlutterEngine b; // = FlutterEngineCache.getInstance().get("map_engine_id");
    @DexIgnore
    public static /* final */ V37 c; // = new V37();

    @DexIgnore
    public final FlutterEngine a() {
        return b;
    }

    @DexIgnore
    public final FlutterEngine b() {
        return a;
    }
}
