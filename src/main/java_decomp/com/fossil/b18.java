package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface B18 {
    @DexIgnore
    void onFailure(A18 a18, IOException iOException);

    @DexIgnore
    void onResponse(A18 a18, Response response) throws IOException;
}
