package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Iz3 extends Transition {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ TextView a;

        @DexIgnore
        public Ai(Iz3 iz3, TextView textView) {
            this.a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void n(Wy0 wy0) {
        r0(wy0);
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public void q(Wy0 wy0) {
        r0(wy0);
    }

    @DexIgnore
    public final void r0(Wy0 wy0) {
        View view = wy0.b;
        if (view instanceof TextView) {
            wy0.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    @Override // androidx.transition.Transition
    public Animator u(ViewGroup viewGroup, Wy0 wy0, Wy0 wy02) {
        ValueAnimator valueAnimator;
        float f = 1.0f;
        if (wy0 == null || wy02 == null || !(wy0.b instanceof TextView)) {
            valueAnimator = null;
        } else {
            View view = wy02.b;
            if (!(view instanceof TextView)) {
                valueAnimator = null;
            } else {
                TextView textView = (TextView) view;
                Map<String, Object> map = wy0.a;
                Map<String, Object> map2 = wy02.a;
                float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
                if (map2.get("android:textscale:scale") != null) {
                    f = ((Float) map2.get("android:textscale:scale")).floatValue();
                }
                if (floatValue == f) {
                    return null;
                }
                ValueAnimator ofFloat = ValueAnimator.ofFloat(floatValue, f);
                ofFloat.addUpdateListener(new Ai(this, textView));
                valueAnimator = ofFloat;
            }
        }
        return valueAnimator;
    }
}
