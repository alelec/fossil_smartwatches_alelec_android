package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sv2 implements Nv2 {
    @DexIgnore
    public static Sv2 c;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ContentObserver b;

    @DexIgnore
    public Sv2() {
        this.a = null;
        this.b = null;
    }

    @DexIgnore
    public Sv2(Context context) {
        this.a = context;
        this.b = new Uv2(this, null);
        context.getContentResolver().registerContentObserver(Gv2.a, true, this.b);
    }

    @DexIgnore
    public static Sv2 a(Context context) {
        Sv2 sv2;
        synchronized (Sv2.class) {
            try {
                if (c == null) {
                    c = Hl0.b(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new Sv2(context) : new Sv2();
                }
                sv2 = c;
            } catch (Throwable th) {
                throw th;
            }
        }
        return sv2;
    }

    @DexIgnore
    public static void b() {
        synchronized (Sv2.class) {
            try {
                if (!(c == null || c.a == null || c.b == null)) {
                    c.a.getContentResolver().unregisterContentObserver(c.b);
                }
                c = null;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ String c(String str) {
        return Gv2.a(this.a.getContentResolver(), str, null);
    }

    @DexIgnore
    public final String d(String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) Qv2.a(new Rv2(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Nv2
    public final /* synthetic */ Object zza(String str) {
        return d(str);
    }
}
