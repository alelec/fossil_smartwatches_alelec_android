package com.fossil;

import android.content.Context;
import com.fossil.Ee1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ge1 extends Ee1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Ee1.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ai(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.Ee1.Ai
        public File a() {
            File cacheDir = this.a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            return this.b != null ? new File(cacheDir, this.b) : cacheDir;
        }
    }

    @DexIgnore
    public Ge1(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public Ge1(Context context, String str, long j) {
        super(new Ai(context, str), j);
    }
}
