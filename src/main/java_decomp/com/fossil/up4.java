package com.fossil;

import android.content.Context;
import com.mapped.An4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Up4 implements Factory<An4> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public Up4(Uo4 uo4, Provider<Context> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Up4 a(Uo4 uo4, Provider<Context> provider) {
        return new Up4(uo4, provider);
    }

    @DexIgnore
    public static An4 c(Uo4 uo4, Context context) {
        An4 B = uo4.B(context);
        Lk7.c(B, "Cannot return null from a non-@Nullable @Provides method");
        return B;
    }

    @DexIgnore
    public An4 b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
