package com.fossil;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Um3 implements Callable<List<Hr3>> {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 d;

    @DexIgnore
    public Um3(Qm3 qm3, Or3 or3, String str, String str2) {
        this.d = qm3;
        this.a = or3;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ List<Hr3> call() throws Exception {
        this.d.b.d0();
        return this.d.b.U().I(this.a.b, this.b, this.c);
    }
}
