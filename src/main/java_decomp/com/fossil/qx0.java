package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;
import android.util.Pair;
import com.mapped.Mi;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qx0 implements Lx0 {
    @DexIgnore
    public static /* final */ String[] c; // = new String[0];
    @DexIgnore
    public /* final */ SQLiteDatabase b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ Ox0 a;

        @DexIgnore
        public Ai(Ox0 ox0) {
            this.a = ox0;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.c(new Tx0(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ Ox0 a;

        @DexIgnore
        public Bi(Ox0 ox0) {
            this.a = ox0;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.c(new Tx0(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    @DexIgnore
    public Qx0(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    @DexIgnore
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.b == sQLiteDatabase;
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public void beginTransaction() {
        this.b.beginTransaction();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public Mi compileStatement(String str) {
        return new Ux0(this.b.compileStatement(str));
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public void endTransaction() {
        this.b.endTransaction();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public void execSQL(String str) throws SQLException {
        this.b.execSQL(str);
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public void execSQL(String str, Object[] objArr) throws SQLException {
        this.b.execSQL(str, objArr);
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public List<Pair<String, String>> getAttachedDbs() {
        return this.b.getAttachedDbs();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public String getPath() {
        return this.b.getPath();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public boolean inTransaction() {
        return this.b.inTransaction();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public boolean isOpen() {
        return this.b.isOpen();
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public Cursor query(Ox0 ox0) {
        return this.b.rawQueryWithFactory(new Ai(ox0), ox0.b(), c, null);
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public Cursor query(Ox0 ox0, CancellationSignal cancellationSignal) {
        return this.b.rawQueryWithFactory(new Bi(ox0), ox0.b(), c, null, cancellationSignal);
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public Cursor query(String str) {
        return query(new Kx0(str));
    }

    @DexIgnore
    @Override // com.fossil.Lx0
    public void setTransactionSuccessful() {
        this.b.setTransactionSuccessful();
    }
}
