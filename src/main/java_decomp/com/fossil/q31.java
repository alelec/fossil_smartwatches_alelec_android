package com.fossil;

import android.database.Cursor;
import com.fossil.O31;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q31 implements P31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<O31> b;
    @DexIgnore
    public /* final */ Vh c;
    @DexIgnore
    public /* final */ Vh d;
    @DexIgnore
    public /* final */ Vh e;
    @DexIgnore
    public /* final */ Vh f;
    @DexIgnore
    public /* final */ Vh g;
    @DexIgnore
    public /* final */ Vh h;
    @DexIgnore
    public /* final */ Vh i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<O31> {
        @DexIgnore
        public Ai(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, O31 o31) {
            String str = o31.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            mi.bindLong(2, (long) U31.h(o31.b));
            String str2 = o31.c;
            if (str2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, str2);
            }
            String str3 = o31.d;
            if (str3 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, str3);
            }
            byte[] k = R01.k(o31.e);
            if (k == null) {
                mi.bindNull(5);
            } else {
                mi.bindBlob(5, k);
            }
            byte[] k2 = R01.k(o31.f);
            if (k2 == null) {
                mi.bindNull(6);
            } else {
                mi.bindBlob(6, k2);
            }
            mi.bindLong(7, o31.g);
            mi.bindLong(8, o31.h);
            mi.bindLong(9, o31.i);
            mi.bindLong(10, (long) o31.k);
            mi.bindLong(11, (long) U31.a(o31.l));
            mi.bindLong(12, o31.m);
            mi.bindLong(13, o31.n);
            mi.bindLong(14, o31.o);
            mi.bindLong(15, o31.p);
            mi.bindLong(16, o31.q ? 1 : 0);
            P01 p01 = o31.j;
            if (p01 != null) {
                mi.bindLong(17, (long) U31.g(p01.b()));
                mi.bindLong(18, p01.g() ? 1 : 0);
                mi.bindLong(19, p01.h() ? 1 : 0);
                mi.bindLong(20, p01.f() ? 1 : 0);
                mi.bindLong(21, p01.i() ? 1 : 0);
                mi.bindLong(22, p01.c());
                mi.bindLong(23, p01.d());
                byte[] c = U31.c(p01.a());
                if (c == null) {
                    mi.bindNull(24);
                } else {
                    mi.bindBlob(24, c);
                }
            } else {
                mi.bindNull(17);
                mi.bindNull(18);
                mi.bindNull(19);
                mi.bindNull(20);
                mi.bindNull(21);
                mi.bindNull(22);
                mi.bindNull(23);
                mi.bindNull(24);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, O31 o31) {
            a(mi, o31);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Vh {
        @DexIgnore
        public Di(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Vh {
        @DexIgnore
        public Ei(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Vh {
        @DexIgnore
        public Fi(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends Vh {
        @DexIgnore
        public Gi(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends Vh {
        @DexIgnore
        public Hi(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii extends Vh {
        @DexIgnore
        public Ii(Q31 q31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public Q31(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
        this.c = new Bi(this, oh);
        this.d = new Ci(this, oh);
        this.e = new Di(this, oh);
        this.f = new Ei(this, oh);
        this.g = new Fi(this, oh);
        this.h = new Gi(this, oh);
        this.i = new Hi(this, oh);
        new Ii(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.P31
    public int a(F11 f11, String... strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder b2 = Hx0.b();
        b2.append("UPDATE workspec SET state=");
        b2.append("?");
        b2.append(" WHERE id IN (");
        Hx0.a(b2, strArr.length);
        b2.append(")");
        Mi compileStatement = this.a.compileStatement(b2.toString());
        compileStatement.bindLong(1, (long) U31.h(f11));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31> c() {
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 ORDER BY period_start_time", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    O31 o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public int d(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.h.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31.Ai> e(String str) {
        Rh f2 = Rh.f("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "state");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                O31.Ai ai = new O31.Ai();
                ai.a = b2.getString(c2);
                ai.b = U31.f(b2.getInt(c3));
                arrayList.add(ai);
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31> f(long j) {
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        f2.bindLong(1, j);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    O31 o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31> g(int i2) {
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        f2.bindLong(1, (long) i2);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    O31 o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public void h(O31 o31) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert((Hh<O31>) o31);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31> i() {
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    O31 o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public void j(String str, R01 r01) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.d.acquire();
        byte[] k = R01.k(r01);
        if (k == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindBlob(1, k);
        }
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<O31> k() {
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    O31 o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                    arrayList.add(o31);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<String> l() {
        Rh f2 = Rh.f("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<String> m(String str) {
        Rh f2 = Rh.f("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public F11 n(String str) {
        F11 f11 = null;
        Rh f2 = Rh.f("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                f11 = U31.f(b2.getInt(0));
            }
            return f11;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public O31 o(String str) {
        Throwable th;
        O31 o31;
        Rh f2 = Rh.f("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "required_network_type");
            int c3 = Dx0.c(b2, "requires_charging");
            int c4 = Dx0.c(b2, "requires_device_idle");
            int c5 = Dx0.c(b2, "requires_battery_not_low");
            int c6 = Dx0.c(b2, "requires_storage_not_low");
            int c7 = Dx0.c(b2, "trigger_content_update_delay");
            int c8 = Dx0.c(b2, "trigger_max_content_delay");
            int c9 = Dx0.c(b2, "content_uri_triggers");
            int c10 = Dx0.c(b2, "id");
            int c11 = Dx0.c(b2, "state");
            int c12 = Dx0.c(b2, "worker_class_name");
            int c13 = Dx0.c(b2, "input_merger_class_name");
            int c14 = Dx0.c(b2, "input");
            int c15 = Dx0.c(b2, "output");
            try {
                int c16 = Dx0.c(b2, "initial_delay");
                int c17 = Dx0.c(b2, "interval_duration");
                int c18 = Dx0.c(b2, "flex_duration");
                int c19 = Dx0.c(b2, "run_attempt_count");
                int c20 = Dx0.c(b2, "backoff_policy");
                int c21 = Dx0.c(b2, "backoff_delay_duration");
                int c22 = Dx0.c(b2, "period_start_time");
                int c23 = Dx0.c(b2, "minimum_retention_duration");
                int c24 = Dx0.c(b2, "schedule_requested_at");
                int c25 = Dx0.c(b2, "run_in_foreground");
                if (b2.moveToFirst()) {
                    String string = b2.getString(c10);
                    String string2 = b2.getString(c12);
                    P01 p01 = new P01();
                    p01.k(U31.e(b2.getInt(c2)));
                    p01.m(b2.getInt(c3) != 0);
                    p01.n(b2.getInt(c4) != 0);
                    p01.l(b2.getInt(c5) != 0);
                    p01.o(b2.getInt(c6) != 0);
                    p01.p(b2.getLong(c7));
                    p01.q(b2.getLong(c8));
                    p01.j(U31.b(b2.getBlob(c9)));
                    o31 = new O31(string, string2);
                    o31.b = U31.f(b2.getInt(c11));
                    o31.d = b2.getString(c13);
                    o31.e = R01.g(b2.getBlob(c14));
                    o31.f = R01.g(b2.getBlob(c15));
                    o31.g = b2.getLong(c16);
                    o31.h = b2.getLong(c17);
                    o31.i = b2.getLong(c18);
                    o31.k = b2.getInt(c19);
                    o31.l = U31.d(b2.getInt(c20));
                    o31.m = b2.getLong(c21);
                    o31.n = b2.getLong(c22);
                    o31.o = b2.getLong(c23);
                    o31.p = b2.getLong(c24);
                    o31.q = b2.getInt(c25) != 0;
                    o31.j = p01;
                } else {
                    o31 = null;
                }
                b2.close();
                f2.m();
                return o31;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public int p(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.g.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public List<R01> q(String str) {
        Rh f2 = Rh.f("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(R01.g(b2.getBlob(0)));
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public int r(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.f.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public void s(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.e.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.P31
    public int t() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.i.acquire();
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.i.release(acquire);
        }
    }
}
