package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ho extends Qq7 implements Coroutine<Fs, Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Af b;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ho(Af af, byte[] bArr, long j) {
        super(2);
        this.b = af;
        this.c = bArr;
        this.d = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(Fs fs, Float f) {
        Af af = this.b;
        long floatValue = ((long) (f.floatValue() * ((float) this.c.length))) + this.d;
        af.E = floatValue;
        float f2 = (((float) floatValue) * 1.0f) / ((float) af.D);
        if (Math.abs(f2 - af.F) > this.b.Q || f2 == 1.0f) {
            Af af2 = this.b;
            af2.F = f2;
            af2.d(f2);
        }
        return Cd6.a;
    }
}
