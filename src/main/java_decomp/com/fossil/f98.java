package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F98 implements E88<W18, Short> {
    @DexIgnore
    public static /* final */ F98 a; // = new F98();

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ Short a(W18 w18) throws IOException {
        return b(w18);
    }

    @DexIgnore
    public Short b(W18 w18) throws IOException {
        return Short.valueOf(w18.string());
    }
}
