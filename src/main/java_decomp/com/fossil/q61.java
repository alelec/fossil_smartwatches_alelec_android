package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q61 implements N61<Uri, Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Q61(Context context) {
        Wg6.c(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return c(uri);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.N61
    public /* bridge */ /* synthetic */ Uri b(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    public boolean c(Uri uri) {
        Wg6.c(uri, "data");
        if (Wg6.a(uri.getScheme(), "android.resource")) {
            String authority = uri.getAuthority();
            if (!(authority == null || Vt7.l(authority))) {
                List<String> pathSegments = uri.getPathSegments();
                Wg6.b(pathSegments, "data.pathSegments");
                if (pathSegments.size() == 2) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public Uri d(Uri uri) {
        Wg6.c(uri, "data");
        String authority = uri.getAuthority();
        String str = authority != null ? authority : "";
        Resources resourcesForApplication = this.a.getPackageManager().getResourcesForApplication(str);
        List<String> pathSegments = uri.getPathSegments();
        int identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), str);
        if (identifier != 0) {
            Uri parse = Uri.parse("android.resource://" + str + '/' + identifier);
            Wg6.b(parse, "Uri.parse(this)");
            return parse;
        }
        throw new IllegalStateException(("Invalid android.resource URI: " + uri).toString());
    }
}
