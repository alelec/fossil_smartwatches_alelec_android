package com.fossil;

import com.fossil.iq4;
import com.fossil.xu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au6 extends du6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ eu6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ xu5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au6 f331a;
        @DexIgnore
        public /* final */ /* synthetic */ ai5 b;

        @DexIgnore
        public a(au6 au6, ai5 ai5) {
            this.f331a = au6;
            this.b = ai5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f331a.f.a();
            this.f331a.f.o(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            UserDisplayUnit a2;
            pq7.c(cVar, "responseValue");
            this.f331a.f.a();
            this.f331a.f.F3(this.b);
            MFUser mFUser = this.f331a.e;
            if (mFUser != null && (a2 = ij5.a(mFUser)) != null) {
                PortfolioApp.h0.c().y1(a2, PortfolioApp.h0.c().J());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au6 f332a;
        @DexIgnore
        public /* final */ /* synthetic */ ai5 b;

        @DexIgnore
        public b(au6 au6, ai5 ai5) {
            this.f332a = au6;
            this.b = ai5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f332a.f.a();
            this.f332a.f.o(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f332a.f.a();
            this.f332a.f.A2(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au6 f333a;
        @DexIgnore
        public /* final */ /* synthetic */ ai5 b;

        @DexIgnore
        public c(au6 au6, ai5 ai5) {
            this.f333a = au6;
            this.b = ai5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f333a.f.a();
            this.f333a.f.o(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            UserDisplayUnit a2;
            pq7.c(cVar, "responseValue");
            this.f333a.f.a();
            this.f333a.f.m1(this.b);
            MFUser mFUser = this.f333a.e;
            if (mFUser != null && (a2 = ij5.a(mFUser)) != null) {
                PortfolioApp.h0.c().y1(a2, PortfolioApp.h0.c().J());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ au6 f334a;
        @DexIgnore
        public /* final */ /* synthetic */ ai5 b;

        @DexIgnore
        public d(au6 au6, ai5 ai5) {
            this.f334a = au6;
            this.b = ai5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f334a.f.a();
            this.f334a.f.o(bVar.a(), "");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            this.f334a.f.a();
            this.f334a.f.R2(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ au6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(au6 au6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = au6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            au6 au6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                au6 au62 = this.this$0;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = au62;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
                au6 = au62;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                au6 = (au6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            au6.e = (MFUser) g;
            eu6 eu6 = this.this$0.f;
            MFUser mFUser = this.this$0.e;
            if (mFUser != null) {
                MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                if (unitGroup != null) {
                    String height = unitGroup.getHeight();
                    if (height == null) {
                        height = ai5.METRIC.getValue();
                    }
                    eu6.A2(ai5.fromString(height));
                    eu6 eu62 = this.this$0.f;
                    MFUser mFUser2 = this.this$0.e;
                    if (mFUser2 != null) {
                        MFUser.UnitGroup unitGroup2 = mFUser2.getUnitGroup();
                        if (unitGroup2 != null) {
                            String weight = unitGroup2.getWeight();
                            if (weight == null) {
                                weight = ai5.METRIC.getValue();
                            }
                            eu62.R2(ai5.fromString(weight));
                            eu6 eu63 = this.this$0.f;
                            MFUser mFUser3 = this.this$0.e;
                            if (mFUser3 != null) {
                                MFUser.UnitGroup unitGroup3 = mFUser3.getUnitGroup();
                                if (unitGroup3 != null) {
                                    String distance = unitGroup3.getDistance();
                                    if (distance == null) {
                                        distance = ai5.METRIC.getValue();
                                    }
                                    eu63.F3(ai5.fromString(distance));
                                    eu6 eu64 = this.this$0.f;
                                    MFUser mFUser4 = this.this$0.e;
                                    if (mFUser4 != null) {
                                        MFUser.UnitGroup unitGroup4 = mFUser4.getUnitGroup();
                                        if (unitGroup4 != null) {
                                            String temperature = unitGroup4.getTemperature();
                                            if (temperature == null) {
                                                temperature = ai5.METRIC.getValue();
                                            }
                                            eu64.m1(ai5.fromString(temperature));
                                            return tl7.f3441a;
                                        }
                                        pq7.i();
                                        throw null;
                                    }
                                    pq7.i();
                                    throw null;
                                }
                                pq7.i();
                                throw null;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }
    }

    /*
    static {
        String simpleName = au6.class.getSimpleName();
        pq7.b(simpleName, "PreferredUnitPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public au6(eu6 eu6, UserRepository userRepository, xu5 xu5) {
        pq7.c(eu6, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(xu5, "mUpdateUser");
        this.f = eu6;
        this.g = userRepository;
        this.h = xu5;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.du6
    public void n(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setDistanceUnit() called with: unit = [" + ai5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setDistanceUnit: unit = " + ai5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setDistance(value);
                this.f.b();
                xu5 xu5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    xu5.e(new xu5.a(mFUser2), new a(this, ai5));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.du6
    public void o(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setHeightUnit() called with: unit = [" + ai5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save height with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setHeight(value);
                this.f.b();
                xu5 xu5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    xu5.e(new xu5.a(mFUser2), new b(this, ai5));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.du6
    public void p(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setTemperatureUnit() called with: unit = [" + ai5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setTemperatureUnit: unit = " + ai5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setTemperature(value);
                this.f.b();
                xu5 xu5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    xu5.e(new xu5.a(mFUser2), new c(this, ai5));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.du6
    public void q(ai5 ai5) {
        pq7.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setWeightUnit() called with: unit = [" + ai5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save weight with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                pq7.b(value, "unit.value");
                unitGroup.setWeight(value);
                this.f.b();
                xu5 xu5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    xu5.e(new xu5.a(mFUser2), new d(this, ai5));
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public void v() {
        this.f.M5(this);
    }
}
