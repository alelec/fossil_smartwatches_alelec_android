package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q {
    @DexIgnore
    public static /* final */ Q a; // = new Q();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.io.File r10, byte[] r11) {
        /*
            r9 = this;
            r6 = 1
            r7 = 0
            r8 = 0
            boolean r0 = r10.exists()
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x002c, all -> 0x0024 }
            r2 = 0
            r3 = 1
            r4 = 0
            r5 = 0
            r1 = r11
            net.sqlcipher.database.SQLiteDatabase r1 = net.sqlcipher.database.SQLiteDatabase.openDatabase(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x002c, all -> 0x0024 }
            java.lang.String r0 = "db"
            com.mapped.Wg6.b(r1, r0)     // Catch:{ Exception -> 0x0034, all -> 0x0032 }
            r1.getVersion()     // Catch:{ Exception -> 0x0034, all -> 0x0032 }
            r0 = r6
        L_0x001f:
            r1.close()
            r7 = r0
        L_0x0023:
            return r7
        L_0x0024:
            r0 = move-exception
            r1 = r8
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()
        L_0x002b:
            throw r0
        L_0x002c:
            r0 = move-exception
            r1 = r8
        L_0x002e:
            if (r1 == 0) goto L_0x0023
            r0 = r7
            goto L_0x001f
        L_0x0032:
            r0 = move-exception
            goto L_0x0026
        L_0x0034:
            r0 = move-exception
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q.a(java.io.File, byte[]):boolean");
    }
}
