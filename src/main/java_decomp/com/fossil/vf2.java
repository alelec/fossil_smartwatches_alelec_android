package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.SystemClock;
import com.zendesk.sdk.support.help.HelpRecyclerViewAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vf2 {
    @DexIgnore
    public static /* final */ IntentFilter a; // = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    @DexIgnore
    public static long b;
    @DexIgnore
    public static float c; // = Float.NaN;

    @DexIgnore
    @TargetApi(20)
    public static int a(Context context) {
        int i = 0;
        if (context == null || context.getApplicationContext() == null) {
            return -1;
        }
        Intent registerReceiver = context.getApplicationContext().registerReceiver(null, a);
        int intExtra = registerReceiver == null ? 0 : registerReceiver.getIntExtra("plugged", 0);
        int i2 = 3;
        if (Mf2.d()) {
            i2 = 7;
        }
        int i3 = (intExtra & i2) != 0 ? 1 : 0;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return -1;
        }
        if (Mf2.g() ? powerManager.isInteractive() : powerManager.isScreenOn()) {
            i = 2;
        }
        return i | i3;
    }

    @DexIgnore
    public static float b(Context context) {
        float f;
        synchronized (Vf2.class) {
            try {
                if (SystemClock.elapsedRealtime() - b >= 60000 || Float.isNaN(c)) {
                    Intent registerReceiver = context.getApplicationContext().registerReceiver(null, a);
                    if (registerReceiver != null) {
                        c = ((float) registerReceiver.getIntExtra(HelpRecyclerViewAdapter.CategoryViewHolder.ROTATION_PROPERTY_NAME, -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
                    }
                    b = SystemClock.elapsedRealtime();
                    f = c;
                } else {
                    f = c;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return f;
    }
}
