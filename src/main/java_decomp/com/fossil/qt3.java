package com.fossil;

import com.mapped.Lc3;
import com.mapped.Mc3;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qt3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ci {
        @DexIgnore
        public /* final */ CountDownLatch a;

        @DexIgnore
        public Ai() {
            this.a = new CountDownLatch(1);
        }

        @DexIgnore
        public /* synthetic */ Ai(Pu3 pu3) {
            this();
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.a.await();
        }

        @DexIgnore
        public final boolean b(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        @DexIgnore
        @Override // com.fossil.Gt3
        public final void onCanceled() {
            this.a.countDown();
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            this.a.countDown();
        }

        @DexIgnore
        @Override // com.mapped.Mc3
        public final void onSuccess(Object obj) {
            this.a.countDown();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ci {
        @DexIgnore
        public /* final */ Object a; // = new Object();
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Lu3<Void> c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public Exception g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public Bi(int i, Lu3<Void> lu3) {
            this.b = i;
            this.c = lu3;
        }

        @DexIgnore
        public final void a() {
            if (this.d + this.e + this.f != this.b) {
                return;
            }
            if (this.g != null) {
                Lu3<Void> lu3 = this.c;
                int i = this.e;
                int i2 = this.b;
                StringBuilder sb = new StringBuilder(54);
                sb.append(i);
                sb.append(" out of ");
                sb.append(i2);
                sb.append(" underlying tasks failed");
                lu3.t(new ExecutionException(sb.toString(), this.g));
            } else if (this.h) {
                this.c.v();
            } else {
                this.c.u(null);
            }
        }

        @DexIgnore
        @Override // com.fossil.Gt3
        public final void onCanceled() {
            synchronized (this.a) {
                this.f++;
                this.h = true;
                a();
            }
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            synchronized (this.a) {
                this.e++;
                this.g = exc;
                a();
            }
        }

        @DexIgnore
        @Override // com.mapped.Mc3
        public final void onSuccess(Object obj) {
            synchronized (this.a) {
                this.d++;
                a();
            }
        }
    }

    @DexIgnore
    public interface Ci extends Gt3, Lc3, Mc3<Object> {
    }

    @DexIgnore
    public static <TResult> TResult a(Nt3<TResult> nt3) throws ExecutionException, InterruptedException {
        Rc2.i();
        Rc2.l(nt3, "Task must not be null");
        if (nt3.p()) {
            return (TResult) i(nt3);
        }
        Ai ai = new Ai(null);
        j(nt3, ai);
        ai.a();
        return (TResult) i(nt3);
    }

    @DexIgnore
    public static <TResult> TResult b(Nt3<TResult> nt3, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        Rc2.i();
        Rc2.l(nt3, "Task must not be null");
        Rc2.l(timeUnit, "TimeUnit must not be null");
        if (nt3.p()) {
            return (TResult) i(nt3);
        }
        Ai ai = new Ai(null);
        j(nt3, ai);
        if (ai.b(j, timeUnit)) {
            return (TResult) i(nt3);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static <TResult> Nt3<TResult> c(Executor executor, Callable<TResult> callable) {
        Rc2.l(executor, "Executor must not be null");
        Rc2.l(callable, "Callback must not be null");
        Lu3 lu3 = new Lu3();
        executor.execute(new Pu3(lu3, callable));
        return lu3;
    }

    @DexIgnore
    public static <TResult> Nt3<TResult> d() {
        Lu3 lu3 = new Lu3();
        lu3.v();
        return lu3;
    }

    @DexIgnore
    public static <TResult> Nt3<TResult> e(Exception exc) {
        Lu3 lu3 = new Lu3();
        lu3.t(exc);
        return lu3;
    }

    @DexIgnore
    public static <TResult> Nt3<TResult> f(TResult tresult) {
        Lu3 lu3 = new Lu3();
        lu3.u(tresult);
        return lu3;
    }

    @DexIgnore
    public static Nt3<Void> g(Collection<? extends Nt3<?>> collection) {
        if (collection == null || collection.isEmpty()) {
            return f(null);
        }
        Iterator<? extends Nt3<?>> it = collection.iterator();
        while (it.hasNext()) {
            if (((Nt3) it.next()) == null) {
                throw new NullPointerException("null tasks are not accepted");
            }
        }
        Lu3 lu3 = new Lu3();
        Bi bi = new Bi(collection.size(), lu3);
        Iterator<? extends Nt3<?>> it2 = collection.iterator();
        while (it2.hasNext()) {
            j((Nt3) it2.next(), bi);
        }
        return lu3;
    }

    @DexIgnore
    public static Nt3<Void> h(Nt3<?>... nt3Arr) {
        return (nt3Arr == null || nt3Arr.length == 0) ? f(null) : g(Arrays.asList(nt3Arr));
    }

    @DexIgnore
    public static <TResult> TResult i(Nt3<TResult> nt3) throws ExecutionException {
        if (nt3.q()) {
            return nt3.m();
        }
        if (nt3.o()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(nt3.l());
    }

    @DexIgnore
    public static void j(Nt3<?> nt3, Ci ci) {
        nt3.g(Pt3.b, ci);
        nt3.e(Pt3.b, ci);
        nt3.a(Pt3.b, ci);
    }
}
