package com.fossil;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import com.fossil.lx1;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx1 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ lx1.b f2434a; // = lx1.b.CBC_PKCS5_PADDING;
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[16];
    @DexIgnore
    public static /* final */ String c; // = lx1.b.GCM_NO_PADDING.getValue();
    @DexIgnore
    public static Context d;
    @DexIgnore
    public static SharedPreferences e;
    @DexIgnore
    public static byte[] f;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    @TargetApi(23)
    public static byte[] h;
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static /* final */ mx1 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.common.cipher.TextEncryption$loadAsymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public a(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            byte[] bArr = null;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                mx1 mx1 = mx1.j;
                synchronized (mx1.g) {
                    mx1 mx12 = mx1.j;
                    if (mx1.f == null) {
                        KeyPair e = ex1.b.e("a");
                        if (e == null) {
                            if (Build.VERSION.SDK_INT < 23) {
                                mx1 mx13 = mx1.j;
                                Context context = mx1.d;
                                if (context != null) {
                                    e = ex1.b.b(context, "a");
                                }
                            }
                            e = null;
                        }
                        if (e != null) {
                            mx1 mx14 = mx1.j;
                            SharedPreferences sharedPreferences = mx1.e;
                            if (sharedPreferences != null) {
                                String string = sharedPreferences.getString("b", null);
                                if (string != null) {
                                    bArr = Base64.decode(string, 0);
                                }
                                if (bArr == null) {
                                    byte[] encoded = lx1.f2275a.c().getEncoded();
                                    Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                    instance.init(1, e.getPublic());
                                    try {
                                        bArr = instance.doFinal(encoded);
                                        sharedPreferences.edit().putString("b", Base64.encodeToString(bArr, 0)).apply();
                                    } catch (Throwable th) {
                                        mx1.j.l();
                                    }
                                }
                                Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                instance2.init(2, e.getPrivate());
                                try {
                                    mx1 mx15 = mx1.j;
                                    mx1.f = instance2.doFinal(bArr);
                                } catch (Throwable th2) {
                                    mx1.j.l();
                                }
                            }
                        }
                    }
                    tl7 tl7 = tl7.f3441a;
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.fossil.common.cipher.TextEncryption$loadSymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public b(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            byte[] bArr = null;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                mx1 mx1 = mx1.j;
                synchronized (mx1.i) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        mx1 mx12 = mx1.j;
                        if (mx1.h == null) {
                            SecretKey f = ex1.b.f("c");
                            mx1 mx13 = mx1.j;
                            SharedPreferences sharedPreferences = mx1.e;
                            if (sharedPreferences != null) {
                                String string = sharedPreferences.getString("d", null);
                                byte[] decode = string != null ? Base64.decode(string, 0) : null;
                                String string2 = sharedPreferences.getString("e", null);
                                if (string2 != null) {
                                    bArr = Base64.decode(string2, 0);
                                }
                                if (decode == null || bArr == null) {
                                    byte[] encoded = lx1.f2275a.c().getEncoded();
                                    mx1 mx14 = mx1.j;
                                    Cipher instance = Cipher.getInstance(mx1.c);
                                    try {
                                        instance.init(1, f);
                                        decode = instance.doFinal(encoded);
                                        pq7.b(instance, "cipher");
                                        bArr = instance.getIV();
                                        sharedPreferences.edit().putString("d", Base64.encodeToString(decode, 0)).putString("e", Base64.encodeToString(bArr, 0)).apply();
                                    } catch (Throwable th) {
                                        mx1.j.m();
                                    }
                                }
                                mx1 mx15 = mx1.j;
                                Cipher instance2 = Cipher.getInstance(mx1.c);
                                instance2.init(2, f, new GCMParameterSpec(128, bArr));
                                try {
                                    mx1 mx16 = mx1.j;
                                    mx1.h = instance2.doFinal(decode);
                                } catch (Throwable th2) {
                                    mx1.j.m();
                                }
                            }
                        }
                    }
                    tl7 tl7 = tl7.f3441a;
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        mx1 mx1 = new mx1();
        j = mx1;
        mx1.q();
    }
    */

    @DexIgnore
    public final void l() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor remove;
        SharedPreferences sharedPreferences = e;
        if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (remove = edit.remove("b")) == null)) {
            remove.apply();
        }
        ex1.b.c("a");
    }

    @DexIgnore
    public final void m() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor remove;
        SharedPreferences.Editor remove2;
        SharedPreferences sharedPreferences = e;
        if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (remove = edit.remove("d")) == null || (remove2 = remove.remove("e")) == null)) {
            remove2.apply();
        }
        ex1.b.c("c");
    }

    @DexIgnore
    public final String n(String str) {
        pq7.c(str, "encryptedText");
        for (byte[] bArr : em7.E(new byte[][]{h, f})) {
            try {
                lx1 lx1 = lx1.f2275a;
                lx1.b bVar = f2434a;
                byte[] bArr2 = b;
                byte[] decode = Base64.decode(str, 2);
                pq7.b(decode, "Base64.decode(encryptedText, Base64.NO_WRAP)");
                return new String(lx1.a(bVar, bArr, bArr2, decode), dx1.a());
            } catch (Exception e2) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String o(String str) {
        byte[] bArr;
        pq7.c(str, "text");
        if (Build.VERSION.SDK_INT >= 23) {
            r();
            bArr = h;
        } else {
            p();
            bArr = f;
        }
        if (bArr == null) {
            return null;
        }
        try {
            lx1 lx1 = lx1.f2275a;
            lx1.b bVar = f2434a;
            byte[] bArr2 = b;
            byte[] bytes = str.getBytes(dx1.a());
            pq7.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return Base64.encodeToString(lx1.b(bVar, bArr, bArr2, bytes), 2);
        } catch (Exception e2) {
            return null;
        }
    }

    @DexIgnore
    public final void p() {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new a(null), 3, null);
    }

    @DexIgnore
    public final void q() {
        p();
        r();
    }

    @DexIgnore
    public final void r() {
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(null), 3, null);
    }

    @DexIgnore
    public final void s(Context context) {
        pq7.c(context, "context");
        d = context;
        e = context.getSharedPreferences("com.fossil.common.cipher.TextEncryption", 0);
    }
}
