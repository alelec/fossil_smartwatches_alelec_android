package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ir3 implements Parcelable.Creator<Fr3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Fr3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = 0;
        int i = 0;
        Double d = null;
        String str = null;
        String str2 = null;
        Float f = null;
        Long l = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    i = Ad2.v(parcel, t);
                    break;
                case 2:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 3:
                    j = Ad2.y(parcel, t);
                    break;
                case 4:
                    l = Ad2.z(parcel, t);
                    break;
                case 5:
                    f = Ad2.s(parcel, t);
                    break;
                case 6:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 7:
                    str = Ad2.f(parcel, t);
                    break;
                case 8:
                    d = Ad2.q(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Fr3(i, str3, j, l, f, str2, str, d);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Fr3[] newArray(int i) {
        return new Fr3[i];
    }
}
