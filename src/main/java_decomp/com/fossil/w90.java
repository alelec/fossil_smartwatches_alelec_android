package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class W90 extends Enum<W90> {
    @DexIgnore
    public static /* final */ W90 c;
    @DexIgnore
    public static /* final */ W90 d;
    @DexIgnore
    public static /* final */ /* synthetic */ W90[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        W90 w90 = new W90("CLOCK_WISE", 0, (byte) 0);
        c = w90;
        W90 w902 = new W90("COUNTER_CLOCK_WISE", 1, (byte) 1);
        W90 w903 = new W90("SHORTEST_PATH", 2, (byte) 2);
        d = w903;
        e = new W90[]{w90, w902, w903};
    }
    */

    @DexIgnore
    public W90(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static W90 valueOf(String str) {
        return (W90) Enum.valueOf(W90.class, str);
    }

    @DexIgnore
    public static W90[] values() {
        return (W90[]) e.clone();
    }
}
