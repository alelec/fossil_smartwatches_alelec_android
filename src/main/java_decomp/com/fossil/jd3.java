package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jd3 {
    @DexIgnore
    public static /* final */ String a; // = "jd3";
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context b;
    @DexIgnore
    public static Md3 c;

    @DexIgnore
    public static Md3 a(Context context) throws E62 {
        Md3 nd3;
        Rc2.k(context);
        Md3 md3 = c;
        if (md3 != null) {
            return md3;
        }
        int h = G62.h(context, 13400000);
        if (h == 0) {
            Log.i(a, "Making Creator dynamically");
            IBinder iBinder = (IBinder) c(d(context).getClassLoader(), "com.google.android.gms.maps.internal.CreatorImpl");
            if (iBinder == null) {
                nd3 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICreator");
                nd3 = queryLocalInterface instanceof Md3 ? (Md3) queryLocalInterface : new Nd3(iBinder);
            }
            c = nd3;
            try {
                nd3.p2(Tg2.n(d(context).getResources()), G62.f);
                return c;
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            throw new E62(h);
        }
    }

    @DexIgnore
    public static <T> T b(Class<?> cls) {
        try {
            return (T) cls.newInstance();
        } catch (InstantiationException e) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to instantiate the dynamic class ".concat(valueOf) : new String("Unable to instantiate the dynamic class "));
        } catch (IllegalAccessException e2) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf2.length() != 0 ? "Unable to call the default constructor of ".concat(valueOf2) : new String("Unable to call the default constructor of "));
        }
    }

    @DexIgnore
    public static <T> T c(ClassLoader classLoader, String str) {
        try {
            Rc2.k(classLoader);
            return (T) b(classLoader.loadClass(str));
        } catch (ClassNotFoundException e) {
            String valueOf = String.valueOf(str);
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to find dynamic class ".concat(valueOf) : new String("Unable to find dynamic class "));
        }
    }

    @DexIgnore
    public static Context d(Context context) {
        Context context2 = b;
        if (context2 != null) {
            return context2;
        }
        Context e = e(context);
        b = e;
        return e;
    }

    @DexIgnore
    public static Context e(Context context) {
        try {
            return DynamiteModule.e(context, DynamiteModule.i, "com.google.android.gms.maps_dynamite").b();
        } catch (Exception e) {
            Log.e(a, "Failed to load maps module, use legacy", e);
            return G62.d(context);
        }
    }
}
