package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bb0 extends Va0 {
    @DexIgnore
    public static /* final */ Ab0 CREATOR; // = new Ab0(null);
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public /* synthetic */ Bb0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c);
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Bb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Bb0) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.StartRepeatInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.U3, Byte.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
    }
}
