package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A35 extends Z25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d x;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(6);
        x = dVar;
        dVar.a(1, new String[]{"item_calories_workout_day", "item_calories_workout_day"}, new int[]{2, 3}, new int[]{2131558666, 2131558666});
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362189, 4);
        y.put(2131362355, 5);
    }
    */

    @DexIgnore
    public A35(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 6, x, y));
    }

    @DexIgnore
    public A35(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 2, (OverviewDayChart) objArr[4], (FlexibleTextView) objArr[5], (Vd5) objArr[2], (Vd5) objArr[3], (LinearLayout) objArr[1]);
        this.w = -1;
        this.u.setTag(null);
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.v = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.w = 0;
        }
        ViewDataBinding.i(this.s);
        ViewDataBinding.i(this.t);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r6.t.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.s.o() != false) goto L_?;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.w     // Catch:{ all -> 0x001f }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
            com.fossil.Vd5 r1 = r6.s
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            com.fossil.Vd5 r1 = r6.t
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x001f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.A35.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.w = 4;
        }
        this.s.q();
        this.t.q();
        w();
    }
}
