package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum DEFAULT can be incorrect */
/* JADX WARN: Init of enum BLACK can be incorrect */
public enum Cw1 {
    DEFAULT(r0),
    WHITE((int) 4294967295L),
    LIGHT_GRAY((int) 4290295992L),
    DARK_GRAY((int) 4285032552L),
    BLACK(r0);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Cw1 a(String str) {
            Cw1[] values = Cw1.values();
            for (Cw1 cw1 : values) {
                if (Vt7.j(cw1.name(), str, true)) {
                    return cw1;
                }
            }
            return null;
        }
    }

    /*
    static {
        int i = (int) 4278190080L;
    }
    */

    @DexIgnore
    public Cw1(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int getColorCode() {
        return this.b;
    }
}
