package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uj extends Cq {
    @DexIgnore
    public int E;

    @DexIgnore
    public Uj(K5 k5, I60 i60) {
        super(k5, i60, Yp.f, new Ht(k5));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.c, Integer.valueOf(this.E));
    }

    @DexIgnore
    @Override // com.fossil.Cq
    public void G(Fs fs) {
        if (fs instanceof Ht) {
            this.E = ((Ht) fs).A;
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return Integer.valueOf(this.E);
    }
}
