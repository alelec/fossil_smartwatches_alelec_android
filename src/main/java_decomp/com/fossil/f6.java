package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F6 extends U5 {
    @DexIgnore
    public int k;

    @DexIgnore
    public F6(N4 n4) {
        super(V5.j, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.F();
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return h7 instanceof J7;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.h;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.k = ((J7) h7).b;
    }
}
