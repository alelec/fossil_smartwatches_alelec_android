package com.fossil;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zo4 implements Factory<Context> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Zo4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Zo4 a(Uo4 uo4) {
        return new Zo4(uo4);
    }

    @DexIgnore
    public static Context c(Uo4 uo4) {
        Context h = uo4.h();
        Lk7.c(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    public Context b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
