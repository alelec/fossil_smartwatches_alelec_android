package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Rm6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fy7 {
    @DexIgnore
    public static final void a(Af6 af6) {
        Rm6 rm6 = (Rm6) af6.get(Rm6.r);
        if (rm6 != null && !rm6.isActive()) {
            throw rm6.k();
        }
    }

    @DexIgnore
    public static final Object b(Xe6<? super Cd6> xe6) {
        Object obj;
        Af6 context = xe6.getContext();
        a(context);
        Xe6 c = Xn7.c(xe6);
        if (!(c instanceof Vv7)) {
            c = null;
        }
        Vv7 vv7 = (Vv7) c;
        if (vv7 != null) {
            if (vv7.h.Q(context)) {
                vv7.m(context, Cd6.a);
            } else {
                Ey7 ey7 = new Ey7();
                vv7.m(context.plus(ey7), Cd6.a);
                if (ey7.b) {
                    obj = Wv7.c(vv7) ? Yn7.d() : Cd6.a;
                }
            }
            obj = Yn7.d();
        } else {
            obj = Cd6.a;
        }
        if (obj == Yn7.d()) {
            Go7.c(xe6);
        }
        return obj == Yn7.d() ? obj : Cd6.a;
    }
}
