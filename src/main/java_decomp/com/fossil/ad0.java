package com.fossil;

import android.content.Context;
import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ad0 {
    @DexIgnore
    public static Cd0 a;
    @DexIgnore
    public static Bd0 b;
    @DexIgnore
    public static Dd0 c;
    @DexIgnore
    public static /* final */ Ad0 d; // = new Ad0();

    @DexIgnore
    public final Bd0 a() {
        if (b == null) {
            String c2 = Cx1.f.c();
            Context a2 = Id0.i.a();
            Ft1 i = Cx1.f.i();
            M80 m80 = M80.c;
            m80.a("ApiServiceManager", "build apiServiceV3, baseUrl=" + c2 + ", context=" + a2 + ", userCredential=" + String.valueOf(i), new Object[0]);
            if (!(c2 == null || a2 == null || i == null)) {
                Ed0 ed0 = new Ed0();
                ed0.b(c2 + "/v2/");
                File cacheDir = a2.getCacheDir();
                Wg6.b(cacheDir, "context.cacheDir");
                ed0.a(cacheDir);
                ed0.c(new Wc0(i));
                ed0.c(new Yc0());
                ed0.c(new Xc0());
                b = (Bd0) ed0.d(Bd0.class);
            }
        }
        return b;
    }

    @DexIgnore
    public final Cd0 b() {
        if (a == null) {
            String c2 = Cx1.f.c();
            Context a2 = Id0.i.a();
            M80 m80 = M80.c;
            m80.a("ApiServiceManager", "build authApiService, baseUrl=" + c2 + ", context=" + a2, new Object[0]);
            if (!(c2 == null || a2 == null)) {
                Ed0 ed0 = new Ed0();
                ed0.b(c2 + "/v2.1/");
                File cacheDir = a2.getCacheDir();
                Wg6.b(cacheDir, "context.cacheDir");
                ed0.a(cacheDir);
                ed0.c(new Yc0());
                ed0.c(new Xc0());
                a = (Cd0) ed0.d(Cd0.class);
            }
        }
        return a;
    }
}
