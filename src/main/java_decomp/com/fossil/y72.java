package com.fossil;

import android.os.RemoteException;
import com.fossil.M62;
import com.fossil.M62.Bi;
import com.fossil.P72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Y72<A extends M62.Bi, L> {
    @DexIgnore
    public /* final */ P72.Ai<L> a;

    @DexIgnore
    public Y72(P72.Ai<L> ai) {
        this.a = ai;
    }

    @DexIgnore
    public P72.Ai<L> a() {
        return this.a;
    }

    @DexIgnore
    public abstract void b(A a2, Ot3<Boolean> ot3) throws RemoteException;
}
