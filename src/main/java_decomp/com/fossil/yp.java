package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Yp extends Enum<Yp> {
    @DexIgnore
    public static /* final */ Yp A;
    @DexIgnore
    public static /* final */ Yp A0;
    @DexIgnore
    public static /* final */ Yp B;
    @DexIgnore
    public static /* final */ Yp B0;
    @DexIgnore
    public static /* final */ Yp C;
    @DexIgnore
    public static /* final */ Yp C0;
    @DexIgnore
    public static /* final */ Yp D;
    @DexIgnore
    public static /* final */ Yp D0;
    @DexIgnore
    public static /* final */ Yp E;
    @DexIgnore
    public static /* final */ Yp E0;
    @DexIgnore
    public static /* final */ Yp F;
    @DexIgnore
    public static /* final */ Yp F0;
    @DexIgnore
    public static /* final */ Yp G;
    @DexIgnore
    public static /* final */ Yp G0;
    @DexIgnore
    public static /* final */ Yp H;
    @DexIgnore
    public static /* final */ Yp H0;
    @DexIgnore
    public static /* final */ Yp I;
    @DexIgnore
    public static /* final */ Yp I0;
    @DexIgnore
    public static /* final */ Yp J;
    @DexIgnore
    public static /* final */ Yp J0;
    @DexIgnore
    public static /* final */ Yp K;
    @DexIgnore
    public static /* final */ Yp K0;
    @DexIgnore
    public static /* final */ Yp L;
    @DexIgnore
    public static /* final */ Yp L0;
    @DexIgnore
    public static /* final */ Yp M;
    @DexIgnore
    public static /* final */ Yp M0;
    @DexIgnore
    public static /* final */ Yp N;
    @DexIgnore
    public static /* final */ Yp N0;
    @DexIgnore
    public static /* final */ Yp O;
    @DexIgnore
    public static /* final */ /* synthetic */ Yp[] O0;
    @DexIgnore
    public static /* final */ Yp P;
    @DexIgnore
    public static /* final */ Yp Q;
    @DexIgnore
    public static /* final */ Yp R;
    @DexIgnore
    public static /* final */ Yp S;
    @DexIgnore
    public static /* final */ Yp T;
    @DexIgnore
    public static /* final */ Yp U;
    @DexIgnore
    public static /* final */ Yp V;
    @DexIgnore
    public static /* final */ Yp W;
    @DexIgnore
    public static /* final */ Yp X;
    @DexIgnore
    public static /* final */ Yp Y;
    @DexIgnore
    public static /* final */ Yp Z;
    @DexIgnore
    public static /* final */ Yp a0;
    @DexIgnore
    public static /* final */ Yp b;
    @DexIgnore
    public static /* final */ Yp b0;
    @DexIgnore
    public static /* final */ Yp c;
    @DexIgnore
    public static /* final */ Yp c0;
    @DexIgnore
    public static /* final */ Yp d;
    @DexIgnore
    public static /* final */ Yp d0;
    @DexIgnore
    public static /* final */ Yp e;
    @DexIgnore
    public static /* final */ Yp e0;
    @DexIgnore
    public static /* final */ Yp f;
    @DexIgnore
    public static /* final */ Yp f0;
    @DexIgnore
    public static /* final */ Yp g;
    @DexIgnore
    public static /* final */ Yp g0;
    @DexIgnore
    public static /* final */ Yp h;
    @DexIgnore
    public static /* final */ Yp h0;
    @DexIgnore
    public static /* final */ Yp i;
    @DexIgnore
    public static /* final */ Yp i0;
    @DexIgnore
    public static /* final */ Yp j;
    @DexIgnore
    public static /* final */ Yp j0;
    @DexIgnore
    public static /* final */ Yp k;
    @DexIgnore
    public static /* final */ Yp k0;
    @DexIgnore
    public static /* final */ Yp l;
    @DexIgnore
    public static /* final */ Yp l0;
    @DexIgnore
    public static /* final */ Yp m;
    @DexIgnore
    public static /* final */ Yp m0;
    @DexIgnore
    public static /* final */ Yp n;
    @DexIgnore
    public static /* final */ Yp n0;
    @DexIgnore
    public static /* final */ Yp o;
    @DexIgnore
    public static /* final */ Yp o0;
    @DexIgnore
    public static /* final */ Yp p;
    @DexIgnore
    public static /* final */ Yp p0;
    @DexIgnore
    public static /* final */ Yp q;
    @DexIgnore
    public static /* final */ Yp q0;
    @DexIgnore
    public static /* final */ Yp r;
    @DexIgnore
    public static /* final */ Yp r0;
    @DexIgnore
    public static /* final */ Yp s;
    @DexIgnore
    public static /* final */ Yp s0;
    @DexIgnore
    public static /* final */ Yp t;
    @DexIgnore
    public static /* final */ Yp t0;
    @DexIgnore
    public static /* final */ Yp u;
    @DexIgnore
    public static /* final */ Yp u0;
    @DexIgnore
    public static /* final */ Yp v;
    @DexIgnore
    public static /* final */ Yp v0;
    @DexIgnore
    public static /* final */ Yp w;
    @DexIgnore
    public static /* final */ Yp w0;
    @DexIgnore
    public static /* final */ Yp x;
    @DexIgnore
    public static /* final */ Yp x0;
    @DexIgnore
    public static /* final */ Yp y;
    @DexIgnore
    public static /* final */ Yp y0;
    @DexIgnore
    public static /* final */ Yp z;
    @DexIgnore
    public static /* final */ Yp z0;

    /*
    static {
        Yp yp = new Yp("UNKNOWN", 0);
        b = yp;
        Yp yp2 = new Yp("MAKE_DEVICE_READY", 1);
        c = yp2;
        Yp yp3 = new Yp("READ_DEVICE_INFO_FILE", 2);
        d = yp3;
        Yp yp4 = new Yp("READ_DEVICE_INFO_CHARACTERISTICS", 3);
        e = yp4;
        Yp yp5 = new Yp("READ_RSSI", 4);
        f = yp5;
        Yp yp6 = new Yp("STREAMING", 5);
        g = yp6;
        Yp yp7 = new Yp("OTA", 6);
        h = yp7;
        Yp yp8 = new Yp("DISCONNECT", 7);
        i = yp8;
        Yp yp9 = new Yp("CLOSE", 8);
        Yp yp10 = new Yp("PUT_FILE", 9);
        j = yp10;
        Yp yp11 = new Yp("SET_CONNECTION_PRIORITY", 10);
        Yp yp12 = new Yp("SET_CONNECTION_PARAMS", 11);
        k = yp12;
        Yp yp13 = new Yp("GET_CONNECTION_PARAMS", 12);
        Yp yp14 = new Yp("PLAY_ANIMATION", 13);
        l = yp14;
        Yp yp15 = new Yp("GET_FILE", 14);
        m = yp15;
        Yp yp16 = new Yp("SYNC", 15);
        n = yp16;
        Yp yp17 = new Yp("GET_HARDWARE_LOG", 16);
        o = yp17;
        Yp yp18 = new Yp("SET_ALARMS", 17);
        p = yp18;
        Yp yp19 = new Yp("GET_ALARMS", 18);
        q = yp19;
        Yp yp20 = new Yp("REQUEST_HANDS", 19);
        r = yp20;
        Yp yp21 = new Yp("RELEASE_HANDS", 20);
        s = yp21;
        Yp yp22 = new Yp("MOVE_HANDS", 21);
        t = yp22;
        Yp yp23 = new Yp("SET_CALIBRATION_POSITION", 22);
        u = yp23;
        Yp yp24 = new Yp("SET_DEVICE_CONFIGS", 23);
        v = yp24;
        Yp yp25 = new Yp("GET_DEVICE_CONFIGS", 24);
        w = yp25;
        Yp yp26 = new Yp("PUT_JSON_OBJECT", 25);
        Yp yp27 = new Yp("SET_COMPLICATION", 26);
        x = yp27;
        Yp yp28 = new Yp("SET_COMPLICATION_CONFIG", 27);
        Yp yp29 = new Yp("SET_WATCH_APP", 28);
        y = yp29;
        Yp yp30 = new Yp("SEND_DEVICE_RESPONSE", 29);
        Yp yp31 = new Yp("FIND_THE_FASTEST_CONNECTION_INTERVAL", 30);
        Yp yp32 = new Yp("FIND_MAXIMUM_CONNECTION_INTERVAL", 31);
        Yp yp33 = new Yp("FIND_OPTIMAL_CONNECTION_PARAMETERS", 32);
        Yp yp34 = new Yp("SEND_APP_NOTIFICATION", 33);
        z = yp34;
        Yp yp35 = new Yp("SEND_TRACK_INFO", 34);
        A = yp35;
        Yp yp36 = new Yp("NOTIFY_MUSIC_EVENT", 35);
        B = yp36;
        Yp yp37 = new Yp("ERASE_DATA", 36);
        Yp yp38 = new Yp("SET_FRONT_LIGHT_ENABLE", 37);
        C = yp38;
        Yp yp39 = new Yp("SET_BACKGROUND_IMAGE", 38);
        D = yp39;
        Yp yp40 = new Yp("GET_BACKGROUND_IMAGE", 39);
        E = yp40;
        Yp yp41 = new Yp("PUT_BACKGROUND_IMAGE_DATA", 40);
        F = yp41;
        Yp yp42 = new Yp("PUT_BACKGROUND_IMAGE_CONFIG", 41);
        G = yp42;
        Yp yp43 = new Yp("GET_NOTIFICATION_FILTER", 42);
        H = yp43;
        Yp yp44 = new Yp("SET_NOTIFICATION_FILTER", 43);
        I = yp44;
        Yp yp45 = new Yp("GET_CURRENT_WORKOUT_SESSION", 44);
        J = yp45;
        Yp yp46 = new Yp("STOP_CURRENT_WORKOUT_SESSION", 45);
        K = yp46;
        Yp yp47 = new Yp("CLEAN_UP_DEVICE", 46);
        L = yp47;
        Yp yp48 = new Yp("GET_HEARTBEAT_STATISTIC", 47);
        Yp yp49 = new Yp("GET_HEARTBEAT_INTERVAL", 48);
        Yp yp50 = new Yp("SET_HEARTBEAT_INTERVAL", 49);
        Yp yp51 = new Yp("SEND_ASYNC_EVENT_ACK", 50);
        M = yp51;
        Yp yp52 = new Yp("FIND_CORRECT_OFFSET", 51);
        N = yp52;
        Yp yp53 = new Yp("SYNC_IN_BACKGROUND", 52);
        O = yp53;
        Yp yp54 = new Yp("GET_HARDWARE_LOG_IN_BACKGROUND", 53);
        P = yp54;
        Yp yp55 = new Yp("SEND_BACKGROUND_SYNC_ACK", 54);
        Yp yp56 = new Yp("START_AUTHENTICATION", 55);
        Q = yp56;
        Yp yp57 = new Yp("EXCHANGE_SECRET_KEY", 56);
        R = yp57;
        Yp yp58 = new Yp("AUTHENTICATE", 57);
        S = yp58;
        Yp yp59 = new Yp("TRY_SET_CONNECTION_PARAMS", 58);
        T = yp59;
        Yp yp60 = new Yp("VERIFY_SECRET_KEY", 59);
        U = yp60;
        Yp yp61 = new Yp("SEND_DEVICE_DATA", 60);
        V = yp61;
        Yp yp62 = new Yp("PUT_NOTIFICATION_ICON", 61);
        W = yp62;
        Yp yp63 = new Yp("PUT_NOTIFICATION_FILTER_RULE", 62);
        X = yp63;
        Yp yp64 = new Yp("GET_NOTIFICATION_ICON", 63);
        Y = yp64;
        Yp yp65 = new Yp("GET_NOTIFICATION_FILTER_RULE", 64);
        Z = yp65;
        Yp yp66 = new Yp("SET_DEVICE_PRESET", 65);
        a0 = yp66;
        Yp yp67 = new Yp("PUT_PRESET_CONFIG", 66);
        b0 = yp67;
        Yp yp68 = new Yp("GET_DEVICE_PRESET", 67);
        c0 = yp68;
        Yp yp69 = new Yp("GET_PRESET_CONFIG", 68);
        d0 = yp69;
        Yp yp70 = new Yp("GET_CURRENT_PRESET", 69);
        e0 = yp70;
        Yp yp71 = new Yp("PUT_LOCALIZATION_FILE", 70);
        f0 = yp71;
        Yp yp72 = new Yp("CREATE_BOND", 71);
        g0 = yp72;
        Yp yp73 = new Yp("CONFIGURE_MICRO_APP", 72);
        h0 = yp73;
        Yp yp74 = new Yp("CONNECT_HID", 73);
        i0 = yp74;
        Yp yp75 = new Yp("DISCONNECT_HID", 74);
        j0 = yp75;
        Yp yp76 = new Yp("PUT_WATCH_PARAMETERS_FILE", 75);
        k0 = yp76;
        Yp yp77 = new Yp("TROUBLESHOOT_DEVICE_BLE", 76);
        Yp yp78 = new Yp("LEGACY_OTA", 77);
        l0 = yp78;
        Yp yp79 = new Yp("LEGACY_SYNC", 78);
        m0 = yp79;
        Yp yp80 = new Yp("SEND_CUSTOM_COMMAND", 79);
        n0 = yp80;
        Yp yp81 = new Yp("SYNC_FLOW", 80);
        o0 = yp81;
        Yp yp82 = new Yp("NOTIFY_APP_NOTIFICATION_EVENT", 81);
        p0 = yp82;
        Yp yp83 = new Yp("GET_DATA_COLLECTION_FILE", 82);
        q0 = yp83;
        Yp yp84 = new Yp("GET_DATA_COLLECTION_FILE_IN_BACKGROUND", 83);
        r0 = yp84;
        Yp yp85 = new Yp("CDG_GET_BATTERY", 84);
        s0 = yp85;
        Yp yp86 = new Yp("CDG_GET_AVERAGE_RSSI", 85);
        t0 = yp86;
        Yp yp87 = new Yp("CDG_VIBE", 86);
        u0 = yp87;
        Yp yp88 = new Yp("CDG_SUBSCRIBE_HEART_RATE_CHARACTERISTIC", 87);
        Yp yp89 = new Yp("CDG_UNSUBSCRIBE_HEART_RATE_CHARACTERISTIC", 88);
        Yp yp90 = new Yp("CDG_START_STREAMING_ACCEL", 89);
        v0 = yp90;
        Yp yp91 = new Yp("CDG_ABORT_STREAMING_ACCEL", 90);
        w0 = yp91;
        Yp yp92 = new Yp("CDG_GET_CHARGING_STATUS", 91);
        Yp yp93 = new Yp("CDG_SUBSCRIBE_ASYNC_CHARACTERISTIC", 92);
        Yp yp94 = new Yp("CDG_UNSUBSCRIBE_ASYNC_CHARACTERISTIC", 93);
        Yp yp95 = new Yp("GET_INSTALLED_UI_PACKAGE", 94);
        x0 = yp95;
        Yp yp96 = new Yp("INSTALL_UI_PACKAGE", 95);
        y0 = yp96;
        Yp yp97 = new Yp("UNINSTALL_UI_PACKAGE", 96);
        z0 = yp97;
        Yp yp98 = new Yp("FETCH_DEVICE_INFORMATION", 97);
        A0 = yp98;
        Yp yp99 = new Yp("CONFIRM_AUTHORIZATION", 98);
        B0 = yp99;
        Yp yp100 = new Yp("GET_PDK_CONFIG", 99);
        Yp yp101 = new Yp("SEND_ENCRYPTED_DATA", 100);
        C0 = yp101;
        Yp yp102 = new Yp("SET_BUDDY_CHALLENGE_FITNESS_DATA", 101);
        Yp yp103 = new Yp("SET_REPLY_MESSAGE", 102);
        D0 = yp103;
        Yp yp104 = new Yp("PUT_REPLY_MESSAGE_ICON", 103);
        E0 = yp104;
        Yp yp105 = new Yp("PUT_REPLY_MESSAGE_DATA", 104);
        F0 = yp105;
        Yp yp106 = new Yp("SET_BUDDY_CHALLENGE_MINIMUM_STEP_THRESHOLD", 105);
        G0 = yp106;
        Yp yp107 = new Yp("SET_UP_WATCH_APPS", 106);
        H0 = yp107;
        Yp yp108 = new Yp("DELETE_FILE", 107);
        I0 = yp108;
        Yp yp109 = new Yp("SET_WORKOUT_ROUTE_IMAGE", 108);
        J0 = yp109;
        Yp yp110 = new Yp("PUT_WORKOUT_ROUTE_IMAGE_DATA", 109);
        K0 = yp110;
        Yp yp111 = new Yp("PUT_ELABEL_FILE", 110);
        L0 = yp111;
        Yp yp112 = new Yp("INSTALL_THEME_PACKAGE", 111);
        M0 = yp112;
        Yp yp113 = new Yp("SWITCH_THEME", 112);
        N0 = yp113;
        O0 = new Yp[]{yp, yp2, yp3, yp4, yp5, yp6, yp7, yp8, yp9, yp10, yp11, yp12, yp13, yp14, yp15, yp16, yp17, yp18, yp19, yp20, yp21, yp22, yp23, yp24, yp25, yp26, yp27, yp28, yp29, yp30, yp31, yp32, yp33, yp34, yp35, yp36, yp37, yp38, yp39, yp40, yp41, yp42, yp43, yp44, yp45, yp46, yp47, yp48, yp49, yp50, yp51, yp52, yp53, yp54, yp55, yp56, yp57, yp58, yp59, yp60, yp61, yp62, yp63, yp64, yp65, yp66, yp67, yp68, yp69, yp70, yp71, yp72, yp73, yp74, yp75, yp76, yp77, yp78, yp79, yp80, yp81, yp82, yp83, yp84, yp85, yp86, yp87, yp88, yp89, yp90, yp91, yp92, yp93, yp94, yp95, yp96, yp97, yp98, yp99, yp100, yp101, yp102, yp103, yp104, yp105, yp106, yp107, yp108, yp109, yp110, yp111, yp112, yp113};
    }
    */

    @DexIgnore
    public Yp(String str, int i2) {
    }

    @DexIgnore
    public static Yp valueOf(String str) {
        return (Yp) Enum.valueOf(Yp.class, str);
    }

    @DexIgnore
    public static Yp[] values() {
        return (Yp[]) O0.clone();
    }
}
