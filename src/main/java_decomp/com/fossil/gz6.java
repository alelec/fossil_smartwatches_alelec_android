package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.data.model.PermissionData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gz6 {
    @DexIgnore
    public /* final */ Dz6 a;
    @DexIgnore
    public /* final */ List<PermissionData> b;

    @DexIgnore
    public Gz6(Dz6 dz6, List<PermissionData> list) {
        Wg6.c(dz6, "mView");
        Wg6.c(list, "mListPerms");
        this.a = dz6;
        this.b = list;
    }

    @DexIgnore
    public final List<PermissionData> a() {
        return this.b;
    }

    @DexIgnore
    public final Dz6 b() {
        return this.a;
    }
}
