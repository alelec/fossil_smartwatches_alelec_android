package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M53 implements Xw2<L53> {
    @DexIgnore
    public static M53 c; // = new M53();
    @DexIgnore
    public /* final */ Xw2<L53> b;

    @DexIgnore
    public M53() {
        this(Ww2.b(new O53()));
    }

    @DexIgnore
    public M53(Xw2<L53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((L53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((L53) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ L53 zza() {
        return this.b.zza();
    }
}
