package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import com.fossil.Jt0;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kt0 {
    @DexIgnore
    public static Field a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Jt0.Ai {
        @DexIgnore
        public Ai(Context context, Ci ci) {
            super(context, ci);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((Ci) this.b).e(str, new Bi(result), bundle);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public MediaBrowserService.Result a;

        @DexIgnore
        public Bi(MediaBrowserService.Result result) {
            this.a = result;
        }

        @DexIgnore
        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }

        @DexIgnore
        public void b(List<Parcel> list, int i) {
            try {
                Kt0.a.setInt(this.a, i);
            } catch (IllegalAccessException e) {
                Log.w("MBSCompatApi26", e);
            }
            this.a.sendResult(a(list));
        }
    }

    @DexIgnore
    public interface Ci extends Jt0.Bi {
        @DexIgnore
        void e(String str, Bi bi, Bundle bundle);
    }

    /*
    static {
        try {
            Field declaredField = MediaBrowserService.Result.class.getDeclaredField("mFlags");
            a = declaredField;
            declaredField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.w("MBSCompatApi26", e);
        }
    }
    */

    @DexIgnore
    public static Object a(Context context, Ci ci) {
        return new Ai(context, ci);
    }
}
