package com.fossil;

import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ng3 {
    @DexIgnore
    public static volatile Handler d;
    @DexIgnore
    public /* final */ Ln3 a;
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public volatile long c;

    @DexIgnore
    public Ng3(Ln3 ln3) {
        Rc2.k(ln3);
        this.a = ln3;
        this.b = new Qg3(this, ln3);
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public final void c(long j) {
        e();
        if (j >= 0) {
            this.c = this.a.zzm().b();
            if (!f().postDelayed(this.b, j)) {
                this.a.d().F().b("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.c != 0;
    }

    @DexIgnore
    public final void e() {
        this.c = 0;
        f().removeCallbacks(this.b);
    }

    @DexIgnore
    public final Handler f() {
        Handler handler;
        if (d != null) {
            return d;
        }
        synchronized (Ng3.class) {
            try {
                if (d == null) {
                    d = new E93(this.a.e().getMainLooper());
                }
                handler = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return handler;
    }
}
