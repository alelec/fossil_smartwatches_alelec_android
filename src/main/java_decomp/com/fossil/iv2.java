package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iv2 extends ContentObserver {
    @DexIgnore
    public Iv2(Handler handler) {
        super(null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        Gv2.e.set(true);
    }
}
