package com.fossil;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ni2 {
    @DexIgnore
    public static /* final */ DataType[] a; // = {DataType.v, DataType.L, DataType.M, DataType.u, DataType.k, DataType.P, Xh2.e, Xh2.o, Xh2.b, Xh2.l, Xh2.a, Xh2.k, DataType.I, DataType.b0, Xh2.d, Xh2.n, DataType.s, DataType.R, DataType.m, Xh2.f, Xh2.g, DataType.F, DataType.E, DataType.C, DataType.D, DataType.TYPE_DISTANCE_CUMULATIVE, DataType.A, DataType.l, DataType.Q, DataType.h, DataType.i, DataType.V, DataType.W, DataType.x, DataType.X, DataType.G, DataType.d0, DataType.Y, DataType.y, DataType.z, Xh2.h, DataType.J, DataType.K, DataType.e0, Xh2.i, Xh2.c, Xh2.m, DataType.t, DataType.Z, DataType.w, DataType.B, DataType.a0, DataType.g, DataType.j, DataType.TYPE_STEP_COUNT_CUMULATIVE, DataType.f, Xh2.j, DataType.H, DataType.c0, DataType.N, DataType.O};
    @DexIgnore
    public static /* final */ DataType[] b; // = {Xh2.e, Xh2.o, Xh2.b, Xh2.l, Xh2.a, Xh2.k, Xh2.d, Xh2.n, Xh2.f, Xh2.g, Xh2.h, Xh2.i, Xh2.c, Xh2.m, Xh2.j};

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static DataType a(String str) {
        char c;
        switch (str.hashCode()) {
            case -2060095039:
                if (str.equals("com.google.cycling.wheel_revolution.rpm")) {
                    c = 24;
                    break;
                }
                c = '\uffff';
                break;
            case -2023954015:
                if (str.equals("com.google.location.bounding_box")) {
                    c = '%';
                    break;
                }
                c = '\uffff';
                break;
            case -2001464928:
                if (str.equals("com.google.internal.symptom")) {
                    c = 30;
                    break;
                }
                c = '\uffff';
                break;
            case -1999891138:
                if (str.equals("com.google.heart_minutes")) {
                    c = 31;
                    break;
                }
                c = '\uffff';
                break;
            case -1939429191:
                if (str.equals("com.google.blood_glucose.summary")) {
                    c = '\n';
                    break;
                }
                c = '\uffff';
                break;
            case -1783842905:
                if (str.equals("com.google.accelerometer")) {
                    c = 0;
                    break;
                }
                c = '\uffff';
                break;
            case -1757812901:
                if (str.equals("com.google.location.sample")) {
                    c = '&';
                    break;
                }
                c = '\uffff';
                break;
            case -1659958877:
                if (str.equals("com.google.menstruation")) {
                    c = '(';
                    break;
                }
                c = '\uffff';
                break;
            case -1487055015:
                if (str.equals("com.google.body.temperature.basal.summary")) {
                    c = 7;
                    break;
                }
                c = '\uffff';
                break;
            case -1466904157:
                if (str.equals("com.google.floor_change.summary")) {
                    c = 28;
                    break;
                }
                c = '\uffff';
                break;
            case -1465729060:
                if (str.equals("com.google.internal.primary_device")) {
                    c = '>';
                    break;
                }
                c = '\uffff';
                break;
            case -1431431801:
                if (str.equals("com.google.height.summary")) {
                    c = '$';
                    break;
                }
                c = '\uffff';
                break;
            case -1248818137:
                if (str.equals("com.google.distance.delta")) {
                    c = 26;
                    break;
                }
                c = '\uffff';
                break;
            case -1196687875:
                if (str.equals("com.google.internal.session.v2")) {
                    c = '3';
                    break;
                }
                c = '\uffff';
                break;
            case -1196687874:
                if (str.equals("com.google.internal.session.v3")) {
                    c = '4';
                    break;
                }
                c = '\uffff';
                break;
            case -1103712522:
                if (str.equals("com.google.heart_minutes.summary")) {
                    c = ' ';
                    break;
                }
                c = '\uffff';
                break;
            case -1102520626:
                if (str.equals("com.google.step_count.delta")) {
                    c = '8';
                    break;
                }
                c = '\uffff';
                break;
            case -1091068721:
                if (str.equals("com.google.height")) {
                    c = '#';
                    break;
                }
                c = '\uffff';
                break;
            case -922976890:
                if (str.equals("com.google.cycling.pedaling.cumulative")) {
                    c = 22;
                    break;
                }
                c = '\uffff';
                break;
            case -900592674:
                if (str.equals("com.google.cycling.pedaling.cadence")) {
                    c = 21;
                    break;
                }
                c = '\uffff';
                break;
            case -886569606:
                if (str.equals("com.google.location.track")) {
                    c = '\'';
                    break;
                }
                c = '\uffff';
                break;
            case -777285735:
                if (str.equals("com.google.heart_rate.summary")) {
                    c = '\"';
                    break;
                }
                c = '\uffff';
                break;
            case -700668164:
                if (str.equals("com.google.internal.goal")) {
                    c = 29;
                    break;
                }
                c = '\uffff';
                break;
            case -661631456:
                if (str.equals("com.google.weight")) {
                    c = ';';
                    break;
                }
                c = '\uffff';
                break;
            case -424876584:
                if (str.equals("com.google.weight.summary")) {
                    c = '<';
                    break;
                }
                c = '\uffff';
                break;
            case -362418992:
                if (str.equals("com.google.body.temperature")) {
                    c = 14;
                    break;
                }
                c = '\uffff';
                break;
            case -217611775:
                if (str.equals("com.google.blood_glucose")) {
                    c = '\t';
                    break;
                }
                c = '\uffff';
                break;
            case -185830635:
                if (str.equals("com.google.power.summary")) {
                    c = '0';
                    break;
                }
                c = '\uffff';
                break;
            case -177293656:
                if (str.equals("com.google.nutrition.summary")) {
                    c = '+';
                    break;
                }
                c = '\uffff';
                break;
            case -164586193:
                if (str.equals("com.google.activity.exercise")) {
                    c = 2;
                    break;
                }
                c = '\uffff';
                break;
            case -98150574:
                if (str.equals("com.google.heart_rate.bpm")) {
                    c = '!';
                    break;
                }
                c = '\uffff';
                break;
            case -56824761:
                if (str.equals("com.google.calories.bmr")) {
                    c = 16;
                    break;
                }
                c = '\uffff';
                break;
            case 53773386:
                if (str.equals("com.google.blood_pressure.summary")) {
                    c = '\f';
                    break;
                }
                c = '\uffff';
                break;
            case 269180370:
                if (str.equals("com.google.activity.samples")) {
                    c = 3;
                    break;
                }
                c = '\uffff';
                break;
            case 295793957:
                if (str.equals("com.google.sensor.events")) {
                    c = '1';
                    break;
                }
                c = '\uffff';
                break;
            case 296250623:
                if (str.equals("com.google.calories.bmr.summary")) {
                    c = 17;
                    break;
                }
                c = '\uffff';
                break;
            case 324760871:
                if (str.equals("com.google.step_count.cadence")) {
                    c = '6';
                    break;
                }
                c = '\uffff';
                break;
            case 378060028:
                if (str.equals("com.google.activity.segment")) {
                    c = 4;
                    break;
                }
                c = '\uffff';
                break;
            case 529727579:
                if (str.equals("com.google.power.sample")) {
                    c = '/';
                    break;
                }
                c = '\uffff';
                break;
            case 657433501:
                if (str.equals("com.google.step_count.cumulative")) {
                    c = '7';
                    break;
                }
                c = '\uffff';
                break;
            case 682891187:
                if (str.equals("com.google.body.fat.percentage")) {
                    c = '\b';
                    break;
                }
                c = '\uffff';
                break;
            case 841663855:
                if (str.equals("com.google.activity.summary")) {
                    c = 5;
                    break;
                }
                c = '\uffff';
                break;
            case 877955159:
                if (str.equals("com.google.speed.summary")) {
                    c = '5';
                    break;
                }
                c = '\uffff';
                break;
            case 899666941:
                if (str.equals("com.google.calories.expended")) {
                    c = 18;
                    break;
                }
                c = '\uffff';
                break;
            case 936279698:
                if (str.equals("com.google.blood_pressure")) {
                    c = 11;
                    break;
                }
                c = '\uffff';
                break;
            case 946706510:
                if (str.equals("com.google.hydration")) {
                    c = '*';
                    break;
                }
                c = '\uffff';
                break;
            case 946938859:
                if (str.equals("com.google.stride_model")) {
                    c = '9';
                    break;
                }
                c = '\uffff';
                break;
            case 1029221057:
                if (str.equals("com.google.device_on_body")) {
                    c = '=';
                    break;
                }
                c = '\uffff';
                break;
            case 1098265835:
                if (str.equals("com.google.floor_change")) {
                    c = 27;
                    break;
                }
                c = '\uffff';
                break;
            case 1111714923:
                if (str.equals("com.google.body.fat.percentage.summary")) {
                    c = '\r';
                    break;
                }
                c = '\uffff';
                break;
            case 1214093899:
                if (str.equals("com.google.vaginal_spotting")) {
                    c = ':';
                    break;
                }
                c = '\uffff';
                break;
            case 1404118825:
                if (str.equals("com.google.oxygen_saturation")) {
                    c = '-';
                    break;
                }
                c = '\uffff';
                break;
            case 1439932546:
                if (str.equals("com.google.ovulation_test")) {
                    c = ',';
                    break;
                }
                c = '\uffff';
                break;
            case 1483133089:
                if (str.equals("com.google.body.temperature.basal")) {
                    c = 6;
                    break;
                }
                c = '\uffff';
                break;
            case 1524007137:
                if (str.equals("com.google.cycling.wheel_revolution.cumulative")) {
                    c = 23;
                    break;
                }
                c = '\uffff';
                break;
            case 1532018766:
                if (str.equals("com.google.active_minutes")) {
                    c = 1;
                    break;
                }
                c = '\uffff';
                break;
            case 1633152752:
                if (str.equals("com.google.nutrition")) {
                    c = ')';
                    break;
                }
                c = '\uffff';
                break;
            case 1921738212:
                if (str.equals("com.google.distance.cumulative")) {
                    c = 25;
                    break;
                }
                c = '\uffff';
                break;
            case 1925848149:
                if (str.equals("com.google.cervical_position")) {
                    c = 20;
                    break;
                }
                c = '\uffff';
                break;
            case 1975902189:
                if (str.equals("com.google.cervical_mucus")) {
                    c = 19;
                    break;
                }
                c = '\uffff';
                break;
            case 2051843553:
                if (str.equals("com.google.oxygen_saturation.summary")) {
                    c = '.';
                    break;
                }
                c = '\uffff';
                break;
            case 2053496735:
                if (str.equals("com.google.speed")) {
                    c = '2';
                    break;
                }
                c = '\uffff';
                break;
            case 2131809416:
                if (str.equals("com.google.body.temperature.summary")) {
                    c = 15;
                    break;
                }
                c = '\uffff';
                break;
            default:
                c = '\uffff';
                break;
        }
        switch (c) {
            case 0:
                return DataType.v;
            case 1:
                return DataType.M;
            case 2:
                return DataType.L;
            case 3:
                return DataType.u;
            case 4:
                return DataType.k;
            case 5:
                return DataType.P;
            case 6:
                return Xh2.e;
            case 7:
                return Xh2.o;
            case '\b':
                return DataType.I;
            case '\t':
                return Xh2.b;
            case '\n':
                return Xh2.l;
            case 11:
                return Xh2.a;
            case '\f':
                return Xh2.k;
            case '\r':
                return DataType.b0;
            case 14:
                return Xh2.d;
            case 15:
                return Xh2.n;
            case 16:
                return DataType.s;
            case 17:
                return DataType.R;
            case 18:
                return DataType.m;
            case 19:
                return Xh2.f;
            case 20:
                return Xh2.g;
            case 21:
                return DataType.F;
            case 22:
                return DataType.E;
            case 23:
                return DataType.C;
            case 24:
                return DataType.D;
            case 25:
                return DataType.TYPE_DISTANCE_CUMULATIVE;
            case 26:
                return DataType.A;
            case 27:
                return DataType.l;
            case 28:
                return DataType.Q;
            case 29:
                return DataType.h;
            case 30:
                return DataType.i;
            case 31:
                return DataType.V;
            case ' ':
                return DataType.W;
            case '!':
                return DataType.x;
            case '\"':
                return DataType.X;
            case '#':
                return DataType.G;
            case '$':
                return DataType.d0;
            case '%':
                return DataType.Y;
            case '&':
                return DataType.y;
            case '\'':
                return DataType.z;
            case '(':
                return Xh2.h;
            case ')':
                return DataType.J;
            case '*':
                return DataType.K;
            case '+':
                return DataType.e0;
            case ',':
                return Xh2.i;
            case '-':
                return Xh2.c;
            case '.':
                return Xh2.m;
            case '/':
                return DataType.t;
            case '0':
                return DataType.Z;
            case '1':
                return DataType.w;
            case '2':
                return DataType.B;
            case '3':
                return DataType.a.a;
            case '4':
                return DataType.a.b;
            case '5':
                return DataType.a0;
            case '6':
                return DataType.g;
            case '7':
                return DataType.TYPE_STEP_COUNT_CUMULATIVE;
            case '8':
                return DataType.f;
            case '9':
                return DataType.j;
            case ':':
                return Xh2.j;
            case ';':
                return DataType.H;
            case '<':
                return DataType.c0;
            case '=':
                return DataType.N;
            case '>':
                return DataType.O;
            default:
                return null;
        }
    }
}
