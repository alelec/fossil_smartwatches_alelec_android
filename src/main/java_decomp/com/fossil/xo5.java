package com.fossil;

import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xo5 implements Wo5 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<No5> b;
    @DexIgnore
    public /* final */ Po5 c; // = new Po5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<No5> {
        @DexIgnore
        public Ai(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, No5 no5) {
            if (no5.g() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, no5.g());
            }
            if (no5.e() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, no5.e());
            }
            if (no5.f() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, no5.f());
            }
            String a2 = Xo5.this.c.a(no5.a());
            if (a2 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a2);
            }
            mi.bindLong(5, no5.i() ? 1 : 0);
            if (no5.d() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, no5.d());
            }
            if (no5.b() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, no5.b());
            }
            if (no5.c() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, no5.c());
            }
            if (no5.h() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, no5.h());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, No5 no5) {
            a(mi, no5);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `diana_recommended_preset` (`serial`,`id`,`name`,`button`,`isDefault`,`faceUrl`,`checkSum`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore
    public Xo5(Oh oh) {
        this.a = oh;
        this.b = new Ai(oh);
    }

    @DexIgnore
    @Override // com.fossil.Wo5
    public Long[] insert(List<No5> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
