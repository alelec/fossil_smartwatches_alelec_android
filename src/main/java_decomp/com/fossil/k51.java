package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K51 implements J51 {
    @DexIgnore
    public static /* final */ Bitmap.Config[] d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] e;
    @DexIgnore
    public static /* final */ Bitmap.Config[] f; // = {Bitmap.Config.RGB_565};
    @DexIgnore
    public static /* final */ Bitmap.Config[] g; // = {Bitmap.Config.ARGB_4444};
    @DexIgnore
    public static /* final */ Bitmap.Config[] h; // = {Bitmap.Config.ALPHA_8};
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public /* final */ N51<Bi, Bitmap> b; // = new N51<>();
    @DexIgnore
    public /* final */ HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> c; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Bitmap.Config b;

        @DexIgnore
        public Bi(int i, Bitmap.Config config) {
            Wg6.c(config, "config");
            this.a = i;
            this.b = config;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (this.a != bi.a || !Wg6.a(this.b, bi.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a;
            Bitmap.Config config = this.b;
            return (config != null ? config.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            Ai ai = K51.i;
            int i = this.a;
            Bitmap.Config config = this.b;
            return '[' + i + "](" + config + ')';
        }
    }

    /*
    static {
        Bitmap.Config[] configArr = Build.VERSION.SDK_INT >= 26 ? new Bitmap.Config[]{Bitmap.Config.ARGB_8888, Bitmap.Config.RGBA_F16} : new Bitmap.Config[]{Bitmap.Config.ARGB_8888};
        d = configArr;
        e = configArr;
    }
    */

    @DexIgnore
    @Override // com.fossil.J51
    public String a(int i2, int i3, Bitmap.Config config) {
        Wg6.c(config, "config");
        int a2 = Y81.a.a(i2, i3, config);
        return '[' + a2 + "](" + config + ')';
    }

    @DexIgnore
    @Override // com.fossil.J51
    public void b(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int b2 = W81.b(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "bitmap.config");
        Bi bi = new Bi(b2, config);
        this.b.f(bi, bitmap);
        NavigableMap<Integer, Integer> h2 = h(bitmap.getConfig());
        Integer num = (Integer) h2.get(Integer.valueOf(bi.a()));
        h2.put(Integer.valueOf(bi.a()), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap c(int i2, int i3, Bitmap.Config config) {
        Wg6.c(config, "config");
        Bi f2 = f(Y81.a.a(i2, i3, config), config);
        Bitmap a2 = this.b.a(f2);
        if (a2 != null) {
            e(f2.a(), a2);
            a2.reconfigure(i2, i3, config);
        }
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.J51
    public String d(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int b2 = W81.b(bitmap);
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "bitmap.config");
        return '[' + b2 + "](" + config + ')';
    }

    @DexIgnore
    public final void e(int i2, Bitmap bitmap) {
        NavigableMap<Integer, Integer> h2 = h(bitmap.getConfig());
        Object obj = h2.get(Integer.valueOf(i2));
        if (obj != null) {
            int intValue = ((Number) obj).intValue();
            if (intValue == 1) {
                h2.remove(Integer.valueOf(i2));
            } else {
                h2.put(Integer.valueOf(i2), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new IllegalStateException(("Tried to decrement empty size, size: " + i2 + ", removed: " + d(bitmap) + ", this: " + this).toString());
        }
    }

    @DexIgnore
    public final Bi f(int i2, Bitmap.Config config) {
        Bitmap.Config[] g2 = g(config);
        for (Bitmap.Config config2 : g2) {
            Integer ceilingKey = h(config2).ceilingKey(Integer.valueOf(i2));
            if (ceilingKey != null && ceilingKey.intValue() <= i2 * 8) {
                return new Bi(ceilingKey.intValue(), config2);
            }
        }
        return new Bi(i2, config);
    }

    @DexIgnore
    public final Bitmap.Config[] g(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16 == config) {
            return e;
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return d;
        }
        if (config == Bitmap.Config.RGB_565) {
            return f;
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return g;
        }
        if (config == Bitmap.Config.ALPHA_8) {
            return h;
        }
        return new Bitmap.Config[]{config};
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> h(Bitmap.Config config) {
        HashMap<Bitmap.Config, NavigableMap<Integer, Integer>> hashMap = this.c;
        NavigableMap<Integer, Integer> navigableMap = hashMap.get(config);
        if (navigableMap == null) {
            navigableMap = new TreeMap<>();
            hashMap.put(config, navigableMap);
        }
        return navigableMap;
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap removeLast() {
        Bitmap e2 = this.b.e();
        if (e2 != null) {
            e(W81.b(e2), e2);
        }
        return e2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy: groupedMap=");
        sb.append(this.b);
        sb.append(", sortedSizes=(");
        for (Map.Entry<Bitmap.Config, NavigableMap<Integer, Integer>> entry : this.c.entrySet()) {
            sb.append(entry.getKey());
            sb.append("[");
            sb.append(entry.getValue());
            sb.append("], ");
        }
        if (!this.c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")");
        String sb2 = sb.toString();
        Wg6.b(sb2, "StringBuilder().apply(builderAction).toString()");
        return sb2;
    }
}
