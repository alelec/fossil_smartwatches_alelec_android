package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.model.PermissionData;
import dagger.internal.Factory;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kz6 implements Factory<Jz6> {
    @DexIgnore
    public static Jz6 a(Dz6 dz6, List<PermissionData> list, An4 an4) {
        return new Jz6(dz6, list, an4);
    }
}
