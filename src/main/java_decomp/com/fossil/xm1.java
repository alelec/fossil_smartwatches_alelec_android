package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xm1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ int d; // = Hy1.a(Gr7.a);
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Xm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new Xm1(Hy1.n(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 2"));
        }

        @DexIgnore
        public Xm1 b(Parcel parcel) {
            return new Xm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xm1 createFromParcel(Parcel parcel) {
            return new Xm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xm1[] newArray(int i) {
            return new Xm1[i];
        }
    }

    @DexIgnore
    public Xm1(int i) throws IllegalArgumentException {
        super(Zm1.DAILY_TOTAL_ACTIVE_MINUTE);
        this.c = i;
        d();
    }

    @DexIgnore
    public /* synthetic */ Xm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt();
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.c).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public Integer c() {
        return Integer.valueOf(this.c);
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        int i = d;
        int i2 = this.c;
        if (!(i2 >= 0 && i >= i2)) {
            StringBuilder e = E.e("minute(");
            e.append(this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            throw new IllegalArgumentException(E.b(e, d, "]."));
        }
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Xm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Xm1) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
    }

    @DexIgnore
    public final int getMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
    }
}
