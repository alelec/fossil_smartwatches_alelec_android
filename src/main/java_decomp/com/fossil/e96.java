package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Gw5;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E96 extends BaseFragment implements D96 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<V35> g;
    @DexIgnore
    public Gw5 h;
    @DexIgnore
    public C96 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return E96.k;
        }

        @DexIgnore
        public final E96 b() {
            return new E96();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ E96 b;

        @DexIgnore
        public Bi(E96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public final void onClick(View view) {
            V35 a2 = this.b.M6().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ E96 b;

        @DexIgnore
        public Ci(E96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            FlexibleTextView flexibleTextView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                V35 a2 = this.b.M6().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.b.z("");
                this.b.N6().n();
                return;
            }
            V35 a3 = this.b.M6().a();
            if (!(a3 == null || (flexibleTextView = a3.w) == null)) {
                flexibleTextView.setVisibility(8);
            }
            V35 a4 = this.b.M6().a();
            if (!(a4 == null || (imageView = a4.r) == null)) {
                imageView.setVisibility(0);
            }
            this.b.z(String.valueOf(charSequence));
            this.b.N6().p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ E96 b;

        @DexIgnore
        public Di(E96 e96) {
            this.b = e96;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Gw5.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ E96 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(E96 e96) {
            this.a = e96;
        }

        @DexIgnore
        @Override // com.fossil.Gw5.Ci
        public void a(String str) {
            Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            V35 a2 = this.a.M6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "it.tvNotFound");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886813);
                Wg6.b(c, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Gw5.Di {
        @DexIgnore
        public /* final */ /* synthetic */ E96 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(E96 e96) {
            this.a = e96;
        }

        @DexIgnore
        @Override // com.fossil.Gw5.Di
        public void a(Complication complication) {
            Wg6.c(complication, "item");
            this.a.N6().o(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ V35 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public Gi(V35 v35, long j) {
            this.a = v35;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleButton flexibleButton = this.a.q;
            Wg6.b(flexibleButton, "it");
            if (flexibleButton.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                flexibleButton.animate().setDuration(this.b).alpha(1.0f);
            } else {
                flexibleButton.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = E96.class.getSimpleName();
        Wg6.b(simpleName, "ComplicationSearchFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.D96
    public void B(List<Lc6<Complication, String>> list) {
        Wg6.c(list, "results");
        Gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.m(list);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.D96
    public void D() {
        Gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.m(null);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D96
    public void K(List<Lc6<Complication, String>> list) {
        Wg6.c(list, "recentSearchResult");
        Gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.l(list);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void K6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Xk5 xk5 = Xk5.a;
            G37<V35> g37 = this.g;
            if (g37 != null) {
                V35 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    Wg6.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.View");
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(C96 c96) {
        Q6(c96);
    }

    @DexIgnore
    public final G37<V35> M6() {
        G37<V35> g37 = this.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final C96 N6() {
        C96 c96 = this.i;
        if (c96 != null) {
            return c96;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = Is5.a.a(j2);
        Window window = fragmentActivity.getWindow();
        Wg6.b(window, "context.window");
        window.setEnterTransition(a2);
        G37<V35> g37 = this.g;
        if (g37 != null) {
            V35 a3 = g37.a();
            if (a3 != null) {
                Wg6.b(a3, "binding");
                P6(a2, j2, a3);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet P6(TransitionSet transitionSet, long j2, V35 v35) {
        FlexibleButton flexibleButton = v35.q;
        Wg6.b(flexibleButton, "binding.btnCancel");
        flexibleButton.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new Gi(v35, j2));
    }

    @DexIgnore
    public void Q6(C96 c96) {
        Wg6.c(c96, "presenter");
        this.i = c96;
    }

    @DexIgnore
    @Override // com.fossil.D96
    public void b3(Complication complication) {
        Wg6.c(complication, "selectedComplication");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, new Intent().putExtra("SEARCH_COMPLICATION_RESULT_ID", complication.getComplicationId()));
            Xk5 xk5 = Xk5.a;
            G37<V35> g37 = this.g;
            if (g37 != null) {
                V35 a2 = g37.a();
                FlexibleButton flexibleButton = a2 != null ? a2.q : null;
                if (flexibleButton != null) {
                    Wg6.b(activity, "it");
                    xk5.a(flexibleButton, activity);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.view.View");
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<V35> g37 = new G37<>(this, (V35) Aq0.f(layoutInflater, 2131558521, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            V35 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        C96 c96 = this.i;
        if (c96 != null) {
            c96.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        C96 c96 = this.i;
        if (c96 != null) {
            c96.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Wg6.b(activity, "it");
            O6(activity, 550);
        }
        this.h = new Gw5();
        G37<V35> g37 = this.g;
        if (g37 != null) {
            V35 a2 = g37.a();
            if (a2 != null) {
                V35 v35 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = v35.v;
                Wg6.b(recyclerViewEmptySupport, "this.rvResults");
                Gw5 gw5 = this.h;
                if (gw5 != null) {
                    recyclerViewEmptySupport.setAdapter(gw5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = v35.v;
                    Wg6.b(recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = v35.v;
                    FlexibleTextView flexibleTextView = v35.w;
                    Wg6.b(flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = v35.r;
                    Wg6.b(imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    v35.r.setOnClickListener(new Bi(this));
                    v35.t.addTextChangedListener(new Ci(this));
                    v35.q.setOnClickListener(new Di(this));
                    Gw5 gw52 = this.h;
                    if (gw52 != null) {
                        gw52.o(new Ei(this));
                        Gw5 gw53 = this.h;
                        if (gw53 != null) {
                            gw53.p(new Fi(this));
                        } else {
                            Wg6.n("mAdapter");
                            throw null;
                        }
                    } else {
                        Wg6.n("mAdapter");
                        throw null;
                    }
                } else {
                    Wg6.n("mAdapter");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.D96
    public void z(String str) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Gw5 gw5 = this.h;
        if (gw5 != null) {
            gw5.n(str);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }
}
