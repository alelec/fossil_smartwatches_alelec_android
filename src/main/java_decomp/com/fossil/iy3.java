package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Iy3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int f; // = Ny3.k().getMaximum(4);
    @DexIgnore
    public /* final */ Hy3 b;
    @DexIgnore
    public /* final */ Zx3<?> c;
    @DexIgnore
    public Yx3 d;
    @DexIgnore
    public /* final */ Wx3 e;

    @DexIgnore
    public Iy3(Hy3 hy3, Zx3<?> zx3, Wx3 wx3) {
        this.b = hy3;
        this.c = zx3;
        this.e = wx3;
    }

    @DexIgnore
    public int a(int i) {
        return b() + (i - 1);
    }

    @DexIgnore
    public int b() {
        return this.b.e();
    }

    @DexIgnore
    public Long c(int i) {
        if (i < this.b.e() || i > h()) {
            return null;
        }
        return Long.valueOf(this.b.f(i(i)));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.widget.TextView d(int r9, android.view.View r10, android.view.ViewGroup r11) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            android.content.Context r0 = r11.getContext()
            r8.e(r0)
            r0 = r10
            android.widget.TextView r0 = (android.widget.TextView) r0
            if (r10 != 0) goto L_0x00e3
            android.content.Context r0 = r11.getContext()
            android.view.LayoutInflater r0 = android.view.LayoutInflater.from(r0)
            int r1 = com.fossil.Pw3.mtrl_calendar_day
            android.view.View r0 = r0.inflate(r1, r11, r6)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = r0
        L_0x001f:
            int r0 = r8.b()
            int r0 = r9 - r0
            if (r0 < 0) goto L_0x002d
            com.fossil.Hy3 r2 = r8.b
            int r3 = r2.g
            if (r0 < r3) goto L_0x003c
        L_0x002d:
            r0 = 8
            r1.setVisibility(r0)
            r1.setEnabled(r6)
        L_0x0035:
            java.lang.Long r2 = r8.c(r9)
            if (r2 != 0) goto L_0x0070
        L_0x003b:
            return r1
        L_0x003c:
            int r0 = r0 + 1
            r1.setTag(r2)
            java.lang.String r2 = java.lang.String.valueOf(r0)
            r1.setText(r2)
            com.fossil.Hy3 r2 = r8.b
            long r2 = r2.f(r0)
            com.fossil.Hy3 r0 = r8.b
            int r0 = r0.e
            com.fossil.Hy3 r4 = com.fossil.Hy3.n()
            int r4 = r4.e
            if (r0 != r4) goto L_0x0068
            java.lang.String r0 = com.fossil.Ay3.a(r2)
            r1.setContentDescription(r0)
        L_0x0061:
            r1.setVisibility(r6)
            r1.setEnabled(r7)
            goto L_0x0035
        L_0x0068:
            java.lang.String r0 = com.fossil.Ay3.c(r2)
            r1.setContentDescription(r0)
            goto L_0x0061
        L_0x0070:
            com.fossil.Wx3 r0 = r8.e
            com.fossil.Wx3$Ci r0 = r0.e()
            long r4 = r2.longValue()
            boolean r0 = r0.S(r4)
            if (r0 == 0) goto L_0x00d7
            r1.setEnabled(r7)
            com.fossil.Zx3<?> r0 = r8.c
            java.util.Collection r0 = r0.X()
            java.util.Iterator r3 = r0.iterator()
        L_0x008d:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00b5
            java.lang.Object r0 = r3.next()
            java.lang.Long r0 = (java.lang.Long) r0
            long r4 = r0.longValue()
            long r6 = r2.longValue()
            long r6 = com.fossil.Ny3.a(r6)
            long r4 = com.fossil.Ny3.a(r4)
            int r0 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r0 != 0) goto L_0x008d
            com.fossil.Yx3 r0 = r8.d
            com.fossil.Xx3 r0 = r0.b
            r0.d(r1)
            goto L_0x003b
        L_0x00b5:
            java.util.Calendar r0 = com.fossil.Ny3.i()
            long r4 = r0.getTimeInMillis()
            long r2 = r2.longValue()
            int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x00ce
            com.fossil.Yx3 r0 = r8.d
            com.fossil.Xx3 r0 = r0.c
            r0.d(r1)
            goto L_0x003b
        L_0x00ce:
            com.fossil.Yx3 r0 = r8.d
            com.fossil.Xx3 r0 = r0.a
            r0.d(r1)
            goto L_0x003b
        L_0x00d7:
            r1.setEnabled(r6)
            com.fossil.Yx3 r0 = r8.d
            com.fossil.Xx3 r0 = r0.g
            r0.d(r1)
            goto L_0x003b
        L_0x00e3:
            r1 = r0
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iy3.d(int, android.view.View, android.view.ViewGroup):android.widget.TextView");
    }

    @DexIgnore
    public final void e(Context context) {
        if (this.d == null) {
            this.d = new Yx3(context);
        }
    }

    @DexIgnore
    public boolean f(int i) {
        return i % this.b.f == 0;
    }

    @DexIgnore
    public boolean g(int i) {
        return (i + 1) % this.b.f == 0;
    }

    @DexIgnore
    public int getCount() {
        return this.b.g + b();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object getItem(int i) {
        return c(i);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) (i / this.b.f);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ View getView(int i, View view, ViewGroup viewGroup) {
        return d(i, view, viewGroup);
    }

    @DexIgnore
    public int h() {
        return (this.b.e() + this.b.g) - 1;
    }

    @DexIgnore
    public boolean hasStableIds() {
        return true;
    }

    @DexIgnore
    public int i(int i) {
        return (i - this.b.e()) + 1;
    }

    @DexIgnore
    public boolean j(int i) {
        return i >= b() && i <= h();
    }
}
