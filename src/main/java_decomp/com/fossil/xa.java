package com.fossil;

import com.mapped.NotificationIcon;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xa extends Vx1<Nu1[], byte[]> {
    @DexIgnore
    public static /* final */ Qx1<Nu1[]>[] b; // = {new Na(), new Qa()};
    @DexIgnore
    public static /* final */ Rx1<byte[]>[] c; // = {new Ta(Ob.k.c), new Va(Ob.k.c)};
    @DexIgnore
    public static /* final */ Xa d; // = new Xa();

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<Nu1[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<byte[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] g(Nu1[] nu1Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ArrayList arrayList = new ArrayList();
        for (Nu1 nu1 : nu1Arr) {
            byte[] a2 = nu1.a();
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        for (byte[] bArr : Pm7.C(arrayList)) {
            byteArrayOutputStream.write(bArr);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public final byte[] h(NotificationIcon[] notificationIconArr, short s, Ry1 ry1) {
        if (notificationIconArr.length == 0) {
            return new byte[0];
        }
        try {
            return a(s, ry1, notificationIconArr);
        } catch (Sx1 e) {
            return new byte[0];
        }
    }
}
