package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.fl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sd6 extends pd6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public /* final */ qd6 g;
    @DexIgnore
    public /* final */ SummariesRepository h;
    @DexIgnore
    public /* final */ FitnessDataRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ no4 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1", f = "DashboardActiveTimePresenter.kt", l = {58, 66}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sd6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sd6$a$a")
        /* renamed from: com.fossil.sd6$a$a  reason: collision with other inner class name */
        public static final class C0220a<T> implements ls0<cu0<ActivitySummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ qd6 f3242a;

            @DexIgnore
            public C0220a(qd6 qd6) {
                this.f3242a = qd6;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(cu0<ActivitySummary> cu0) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
                local.d("DashboardActiveTimePresenter", sb.toString());
                if (cu0 != null) {
                    this.f3242a.q(cu0);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements fl5.a {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f3243a;

            @DexIgnore
            public b(a aVar) {
                this.f3243a = aVar;
            }

            @DexIgnore
            @Override // com.fossil.fl5.a
            public final void e(fl5.g gVar) {
                pq7.c(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardActiveTimePresenter", "onStatusChange status=" + gVar);
                if (gVar.b()) {
                    this.f3243a.this$0.g.d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$user$1", f = "DashboardActiveTimePresenter.kt", l = {58}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.j;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sd6 sd6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = sd6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0067  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                r9 = 2
                r4 = 1
                java.lang.Object r8 = com.fossil.yn7.d()
                int r0 = r10.label
                if (r0 == 0) goto L_0x00a0
                if (r0 == r4) goto L_0x0059
                if (r0 != r9) goto L_0x0051
                java.lang.Object r0 = r10.L$4
                com.fossil.sd6 r0 = (com.fossil.sd6) r0
                java.lang.Object r1 = r10.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r10.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r11)
                r2 = r0
                r1 = r11
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.fossil.sd6.x(r2, r0)
                com.fossil.sd6 r0 = r10.this$0
                com.fossil.qd6 r1 = com.fossil.sd6.w(r0)
                com.fossil.sd6 r0 = r10.this$0
                com.portfolio.platform.data.Listing r0 = com.fossil.sd6.r(r0)
                if (r0 == 0) goto L_0x004e
                androidx.lifecycle.LiveData r2 = r0.getPagedList()
                if (r2 == 0) goto L_0x004e
                if (r1 == 0) goto L_0x00c0
                r0 = r1
                com.fossil.rd6 r0 = (com.fossil.rd6) r0
                com.fossil.sd6$a$a r3 = new com.fossil.sd6$a$a
                r3.<init>(r1)
                r2.h(r0, r3)
            L_0x004e:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0050:
                return r0
            L_0x0051:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0059:
                java.lang.Object r0 = r10.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r11)
                r6 = r0
                r1 = r11
            L_0x0062:
                r5 = r1
                com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
                if (r5 == 0) goto L_0x004e
                java.lang.String r0 = r5.getCreatedAt()
                java.util.Date r2 = com.fossil.lk5.q0(r0)
                com.fossil.sd6 r7 = r10.this$0
                com.portfolio.platform.data.source.SummariesRepository r0 = com.fossil.sd6.u(r7)
                com.fossil.sd6 r1 = r10.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r1 = com.fossil.sd6.t(r1)
                java.lang.String r3 = "createdDate"
                com.fossil.pq7.b(r2, r3)
                com.fossil.sd6 r3 = r10.this$0
                com.fossil.no4 r3 = com.fossil.sd6.s(r3)
                com.fossil.sd6$a$b r4 = new com.fossil.sd6$a$b
                r4.<init>(r10)
                r10.L$0 = r6
                r10.L$1 = r5
                r10.L$2 = r5
                r10.L$3 = r2
                r10.L$4 = r7
                r10.label = r9
                r5 = r10
                java.lang.Object r1 = r0.getSummariesPaging(r1, r2, r3, r4, r5)
                if (r1 != r8) goto L_0x00bd
                r0 = r8
                goto L_0x0050
            L_0x00a0:
                com.fossil.el7.b(r11)
                com.fossil.iv7 r0 = r10.p$
                com.fossil.sd6 r1 = r10.this$0
                com.fossil.dv7 r1 = com.fossil.sd6.q(r1)
                com.fossil.sd6$a$c r2 = new com.fossil.sd6$a$c
                r3 = 0
                r2.<init>(r10, r3)
                r10.L$0 = r0
                r10.label = r4
                java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r10)
                if (r1 != r8) goto L_0x00c8
                r0 = r8
                goto L_0x0050
            L_0x00bd:
                r2 = r7
                goto L_0x0027
            L_0x00c0:
                com.fossil.il7 r0 = new com.fossil.il7
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment"
                r0.<init>(r1)
                throw r0
            L_0x00c8:
                r6 = r0
                goto L_0x0062
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sd6.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public sd6(qd6 qd6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, no4 no4) {
        pq7.c(qd6, "mView");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(no4, "mAppExecutors");
        this.g = qd6;
        this.h = summariesRepository;
        this.i = fitnessDataRepository;
        this.j = userRepository;
        this.k = no4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        Boolean p0 = lk5.p0(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimePresenter", "start isDateTodayDate " + p0 + " listingPage " + this.f);
        if (!p0.booleanValue()) {
            this.e = new Date();
            Listing<ActivitySummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.pd6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pd6
    public void o() {
        LiveData<cu0<ActivitySummary>> pagedList;
        try {
            qd6 qd6 = this.g;
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (qd6 != null) {
                    pagedList.n((rd6) qd6);
                } else {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(tl7.f3441a);
            local.e("DashboardActiveTimePresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.pd6
    public void p() {
        gp7<tl7> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public void y() {
        this.g.M5(this);
    }
}
