package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J22 extends Q22 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ H02 b;
    @DexIgnore
    public /* final */ C02 c;

    @DexIgnore
    public J22(long j, H02 h02, C02 c02) {
        this.a = j;
        if (h02 != null) {
            this.b = h02;
            if (c02 != null) {
                this.c = c02;
                return;
            }
            throw new NullPointerException("Null event");
        }
        throw new NullPointerException("Null transportContext");
    }

    @DexIgnore
    @Override // com.fossil.Q22
    public C02 b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Q22
    public long c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Q22
    public H02 d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Q22)) {
            return false;
        }
        Q22 q22 = (Q22) obj;
        return this.a == q22.c() && this.b.equals(q22.d()) && this.c.equals(q22.b());
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "PersistedEvent{id=" + this.a + ", transportContext=" + this.b + ", event=" + this.c + "}";
    }
}
