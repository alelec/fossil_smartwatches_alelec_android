package com.fossil;

import com.fossil.P18;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V18 {
    @DexIgnore
    public /* final */ Q18 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ P18 c;
    @DexIgnore
    public /* final */ RequestBody d;
    @DexIgnore
    public /* final */ Map<Class<?>, Object> e;
    @DexIgnore
    public volatile Z08 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Q18 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public P18.Ai c;
        @DexIgnore
        public RequestBody d;
        @DexIgnore
        public Map<Class<?>, Object> e;

        @DexIgnore
        public Ai() {
            this.e = Collections.emptyMap();
            this.b = "GET";
            this.c = new P18.Ai();
        }

        @DexIgnore
        public Ai(V18 v18) {
            this.e = Collections.emptyMap();
            this.a = v18.a;
            this.b = v18.b;
            this.d = v18.d;
            this.e = v18.e.isEmpty() ? Collections.emptyMap() : new LinkedHashMap<>(v18.e);
            this.c = v18.c.f();
        }

        @DexIgnore
        public Ai a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public V18 b() {
            if (this.a != null) {
                return new V18(this);
            }
            throw new IllegalStateException("url == null");
        }

        @DexIgnore
        public Ai c(Z08 z08) {
            String z082 = z08.toString();
            if (z082.isEmpty()) {
                i(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
            } else {
                e(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, z082);
            }
            return this;
        }

        @DexIgnore
        public Ai d() {
            g("GET", null);
            return this;
        }

        @DexIgnore
        public Ai e(String str, String str2) {
            this.c.h(str, str2);
            return this;
        }

        @DexIgnore
        public Ai f(P18 p18) {
            this.c = p18.f();
            return this;
        }

        @DexIgnore
        public Ai g(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !V28.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !V28.e(str)) {
                this.b = str;
                this.d = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public Ai h(RequestBody requestBody) {
            g("POST", requestBody);
            return this;
        }

        @DexIgnore
        public Ai i(String str) {
            this.c.g(str);
            return this;
        }

        @DexIgnore
        public <T> Ai j(Class<? super T> cls, T t) {
            if (cls != null) {
                if (t == null) {
                    this.e.remove(cls);
                } else {
                    if (this.e.isEmpty()) {
                        this.e = new LinkedHashMap();
                    }
                    this.e.put(cls, cls.cast(t));
                }
                return this;
            }
            throw new NullPointerException("type == null");
        }

        @DexIgnore
        public Ai k(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                l(Q18.l(str));
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public Ai l(Q18 q18) {
            if (q18 != null) {
                this.a = q18;
                return this;
            }
            throw new NullPointerException("url == null");
        }
    }

    @DexIgnore
    public V18(Ai ai) {
        this.a = ai.a;
        this.b = ai.b;
        this.c = ai.c.e();
        this.d = ai.d;
        this.e = B28.v(ai.e);
    }

    @DexIgnore
    public RequestBody a() {
        return this.d;
    }

    @DexIgnore
    public Z08 b() {
        Z08 z08 = this.f;
        if (z08 != null) {
            return z08;
        }
        Z08 k = Z08.k(this.c);
        this.f = k;
        return k;
    }

    @DexIgnore
    public String c(String str) {
        return this.c.c(str);
    }

    @DexIgnore
    public List<String> d(String str) {
        return this.c.j(str);
    }

    @DexIgnore
    public P18 e() {
        return this.c;
    }

    @DexIgnore
    public boolean f() {
        return this.a.n();
    }

    @DexIgnore
    public String g() {
        return this.b;
    }

    @DexIgnore
    public Ai h() {
        return new Ai(this);
    }

    @DexIgnore
    public <T> T i(Class<? extends T> cls) {
        return (T) cls.cast(this.e.get(cls));
    }

    @DexIgnore
    public Q18 j() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.a + ", tags=" + this.e + '}';
    }
}
