package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fe3 extends Ce3 {
    @DexIgnore
    public /* final */ Zd3 e;
    @DexIgnore
    public /* final */ float f;

    @DexIgnore
    public Fe3(Zd3 zd3) {
        this(zd3, 10.0f);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Fe3(com.fossil.Zd3 r3, float r4) {
        /*
            r2 = this;
            java.lang.String r0 = "bitmapDescriptor must not be null"
            com.fossil.Rc2.l(r3, r0)
            r0 = r3
            com.fossil.Zd3 r0 = (com.fossil.Zd3) r0
            r1 = 0
            int r1 = (r4 > r1 ? 1 : (r4 == r1 ? 0 : -1))
            if (r1 <= 0) goto L_0x0015
            r2.<init>(r0, r4)
            r2.e = r3
            r2.f = r4
            return
        L_0x0015:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "refWidth must be positive"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fe3.<init>(com.fossil.Zd3, float):void");
    }

    @DexIgnore
    @Override // com.fossil.Ce3
    public final String toString() {
        String valueOf = String.valueOf(this.e);
        float f2 = this.f;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 55);
        sb.append("[CustomCap: bitmapDescriptor=");
        sb.append(valueOf);
        sb.append(" refWidth=");
        sb.append(f2);
        sb.append("]");
        return sb.toString();
    }
}
