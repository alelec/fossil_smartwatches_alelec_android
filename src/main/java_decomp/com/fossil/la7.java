package com.fossil;

import com.mapped.MyFaceFragment;
import com.portfolio.platform.watchface.faces.WatchFaceListFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface La7 {
    @DexIgnore
    void a(MyFaceFragment myFaceFragment);

    @DexIgnore
    void b(WatchFaceListFragment watchFaceListFragment);
}
