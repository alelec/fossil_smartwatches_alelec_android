package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Mb4 implements FilenameFilter {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public Mb4(String str) {
        this.a = str;
    }

    @DexIgnore
    public static FilenameFilter a(String str) {
        return new Mb4(str);
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(this.a);
    }
}
