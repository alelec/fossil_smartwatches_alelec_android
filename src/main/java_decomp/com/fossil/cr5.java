package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.ShakeFeedbackService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cr5 implements Factory<ShakeFeedbackService> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public Cr5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Cr5 a(Provider<UserRepository> provider) {
        return new Cr5(provider);
    }

    @DexIgnore
    public static ShakeFeedbackService c(UserRepository userRepository) {
        return new ShakeFeedbackService(userRepository);
    }

    @DexIgnore
    public ShakeFeedbackService b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
