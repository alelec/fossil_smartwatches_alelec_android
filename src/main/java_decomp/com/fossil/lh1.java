package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lh1 {
    @DexIgnore
    public /* final */ Bb1 a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ List<Bi> c;
    @DexIgnore
    public /* final */ Wa1 d;
    @DexIgnore
    public /* final */ Rd1 e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Va1<Bitmap> i;
    @DexIgnore
    public Ai j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public Ai l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public Ai n;
    @DexIgnore
    public Di o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Lj1<Bitmap> {
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public Bitmap h;

        @DexIgnore
        public Ai(Handler handler, int i, long j) {
            this.e = handler;
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public /* bridge */ /* synthetic */ void b(Object obj, Tj1 tj1) {
            e((Bitmap) obj, tj1);
        }

        @DexIgnore
        public Bitmap c() {
            return this.h;
        }

        @DexIgnore
        public void e(Bitmap bitmap, Tj1<? super Bitmap> tj1) {
            this.h = bitmap;
            this.e.sendMessageAtTime(this.e.obtainMessage(1, this), this.g);
        }

        @DexIgnore
        @Override // com.fossil.Qj1
        public void j(Drawable drawable) {
            this.h = null;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Handler.Callback {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                Lh1.this.m((Ai) message.obj);
                return true;
            }
            if (i == 2) {
                Lh1.this.d.l((Ai) message.obj);
            }
            return false;
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public Lh1(Oa1 oa1, Bb1 bb1, int i2, int i3, Sb1<Bitmap> sb1, Bitmap bitmap) {
        this(oa1.f(), Oa1.t(oa1.h()), bb1, null, i(Oa1.t(oa1.h()), i2, i3), sb1, bitmap);
    }

    @DexIgnore
    public Lh1(Rd1 rd1, Wa1 wa1, Bb1 bb1, Handler handler, Va1<Bitmap> va1, Sb1<Bitmap> sb1, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = wa1;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new Ci()) : handler;
        this.e = rd1;
        this.b = handler;
        this.i = va1;
        this.a = bb1;
        o(sb1, bitmap);
    }

    @DexIgnore
    public static Mb1 g() {
        return new Yj1(Double.valueOf(Math.random()));
    }

    @DexIgnore
    public static Va1<Bitmap> i(Wa1 wa1, int i2, int i3) {
        return wa1.e().u0(((Fj1) ((Fj1) Fj1.u0(Wc1.a).s0(true)).m0(true)).b0(i2, i3));
    }

    @DexIgnore
    public void a() {
        this.c.clear();
        n();
        q();
        Ai ai = this.j;
        if (ai != null) {
            this.d.l(ai);
            this.j = null;
        }
        Ai ai2 = this.l;
        if (ai2 != null) {
            this.d.l(ai2);
            this.l = null;
        }
        Ai ai3 = this.n;
        if (ai3 != null) {
            this.d.l(ai3);
            this.n = null;
        }
        this.a.clear();
        this.k = true;
    }

    @DexIgnore
    public ByteBuffer b() {
        return this.a.f().asReadOnlyBuffer();
    }

    @DexIgnore
    public Bitmap c() {
        Ai ai = this.j;
        return ai != null ? ai.c() : this.m;
    }

    @DexIgnore
    public int d() {
        Ai ai = this.j;
        if (ai != null) {
            return ai.f;
        }
        return -1;
    }

    @DexIgnore
    public Bitmap e() {
        return this.m;
    }

    @DexIgnore
    public int f() {
        return this.a.c();
    }

    @DexIgnore
    public int h() {
        return this.r;
    }

    @DexIgnore
    public int j() {
        return this.a.i() + this.p;
    }

    @DexIgnore
    public int k() {
        return this.q;
    }

    @DexIgnore
    public final void l() {
        if (this.f && !this.g) {
            if (this.h) {
                Ik1.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.a.g();
                this.h = false;
            }
            Ai ai = this.n;
            if (ai != null) {
                this.n = null;
                m(ai);
                return;
            }
            this.g = true;
            int d2 = this.a.d();
            long uptimeMillis = SystemClock.uptimeMillis();
            this.a.b();
            this.l = new Ai(this.b, this.a.h(), ((long) d2) + uptimeMillis);
            this.i.u0(Fj1.v0(g())).M0(this.a).C0(this.l);
        }
    }

    @DexIgnore
    public void m(Ai ai) {
        Di di = this.o;
        if (di != null) {
            di.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, ai).sendToTarget();
        } else if (!this.f) {
            this.n = ai;
        } else {
            if (ai.c() != null) {
                n();
                Ai ai2 = this.j;
                this.j = ai;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (ai2 != null) {
                    this.b.obtainMessage(2, ai2).sendToTarget();
                }
            }
            l();
        }
    }

    @DexIgnore
    public final void n() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.b(bitmap);
            this.m = null;
        }
    }

    @DexIgnore
    public void o(Sb1<Bitmap> sb1, Bitmap bitmap) {
        Ik1.d(sb1);
        Ik1.d(bitmap);
        this.m = bitmap;
        this.i = this.i.u0(new Fj1().o0(sb1));
        this.p = Jk1.h(bitmap);
        this.q = bitmap.getWidth();
        this.r = bitmap.getHeight();
    }

    @DexIgnore
    public final void p() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            l();
        }
    }

    @DexIgnore
    public final void q() {
        this.f = false;
    }

    @DexIgnore
    public void r(Bi bi) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(bi)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(bi);
            if (isEmpty) {
                p();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    public void s(Bi bi) {
        this.c.remove(bi);
        if (this.c.isEmpty()) {
            q();
        }
    }
}
