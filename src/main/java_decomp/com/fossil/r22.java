package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class R22 implements J32.Bi {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ H02 b;

    @DexIgnore
    public R22(long j, H02 h02) {
        this.a = j;
        this.b = h02;
    }

    @DexIgnore
    public static J32.Bi a(long j, H02 h02) {
        return new R22(j, h02);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.g0(this.a, this.b, (SQLiteDatabase) obj);
    }
}
