package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xw1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Yw1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xw1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xw1 createFromParcel(Parcel parcel) {
            Yw1 yw1 = Yw1.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = Rc0.a[yw1.ordinal()];
            if (i == 1) {
                return Tc0.CREATOR.a(parcel);
            }
            if (i == 2) {
                return Ww1.CREATOR.a(parcel);
            }
            throw new Kc6();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xw1[] newArray(int i) {
            return new Xw1[i];
        }
    }

    @DexIgnore
    public Xw1(Yw1 yw1) {
        this.b = yw1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((Xw1) obj).b;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
    }
}
