package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dl7;
import com.mapped.Cd6;
import com.mapped.Dx6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.lang.reflect.Method;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J88 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<Throwable, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            invoke(th);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<Throwable, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_await$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Call call) {
            super(1);
            this.$this_await$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            invoke(th);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_await$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Dx6<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 a;

        @DexIgnore
        public Ci(Lk6 lk6) {
            this.a = lk6;
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onFailure(Call<T> call, Throwable th) {
            Wg6.c(call, "call");
            Wg6.c(th, "t");
            Lk6 lk6 = this.a;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onResponse(Call<T> call, Q88<T> q88) {
            Wg6.c(call, "call");
            Wg6.c(q88, "response");
            if (q88.e()) {
                T a2 = q88.a();
                if (a2 == null) {
                    Object i = call.c().i(I88.class);
                    if (i != null) {
                        Wg6.b(i, "call.request().tag(Invocation::class.java)!!");
                        Method a3 = ((I88) i).a();
                        StringBuilder sb = new StringBuilder();
                        sb.append("Response from ");
                        Wg6.b(a3, "method");
                        Class<?> declaringClass = a3.getDeclaringClass();
                        Wg6.b(declaringClass, "method.declaringClass");
                        sb.append(declaringClass.getName());
                        sb.append('.');
                        sb.append(a3.getName());
                        sb.append(" was null but response body type was declared as non-null");
                        Wk7 wk7 = new Wk7(sb.toString());
                        Lk6 lk6 = this.a;
                        Dl7.Ai ai = Dl7.Companion;
                        lk6.resumeWith(Dl7.constructor-impl(El7.a(wk7)));
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Lk6 lk62 = this.a;
                Dl7.Ai ai2 = Dl7.Companion;
                lk62.resumeWith(Dl7.constructor-impl(a2));
                return;
            }
            Lk6 lk63 = this.a;
            G88 g88 = new G88(q88);
            Dl7.Ai ai3 = Dl7.Companion;
            lk63.resumeWith(Dl7.constructor-impl(El7.a(g88)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Dx6<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 a;

        @DexIgnore
        public Di(Lk6 lk6) {
            this.a = lk6;
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onFailure(Call<T> call, Throwable th) {
            Wg6.c(call, "call");
            Wg6.c(th, "t");
            Lk6 lk6 = this.a;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onResponse(Call<T> call, Q88<T> q88) {
            Wg6.c(call, "call");
            Wg6.c(q88, "response");
            if (q88.e()) {
                Lk6 lk6 = this.a;
                T a2 = q88.a();
                Dl7.Ai ai = Dl7.Companion;
                lk6.resumeWith(Dl7.constructor-impl(a2));
                return;
            }
            Lk6 lk62 = this.a;
            G88 g88 = new G88(q88);
            Dl7.Ai ai2 = Dl7.Companion;
            lk62.resumeWith(Dl7.constructor-impl(El7.a(g88)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Qq7 implements Hg6<Throwable, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Call $this_awaitResponse$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Call call) {
            super(1);
            this.$this_awaitResponse$inlined = call;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            invoke(th);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.$this_awaitResponse$inlined.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Dx6<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 a;

        @DexIgnore
        public Fi(Lk6 lk6) {
            this.a = lk6;
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onFailure(Call<T> call, Throwable th) {
            Wg6.c(call, "call");
            Wg6.c(th, "t");
            Lk6 lk6 = this.a;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(El7.a(th)));
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onResponse(Call<T> call, Q88<T> q88) {
            Wg6.c(call, "call");
            Wg6.c(q88, "response");
            Lk6 lk6 = this.a;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(q88));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "retrofit2/KotlinExtensions", f = "KotlinExtensions.kt", l = {100, 102}, m = "yieldAndThrow")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public Gi(Xe6 xe6) {
            super(xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return J88.d(null, this);
        }
    }

    @DexIgnore
    public static final <T> Object a(Call<T> call, Xe6<? super T> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        lu7.e(new Ai(call));
        call.D(new Ci(lu7));
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public static final <T> Object b(Call<T> call, Xe6<? super T> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        lu7.e(new Bi(call));
        call.D(new Di(lu7));
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public static final <T> Object c(Call<T> call, Xe6<? super Q88<T>> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        lu7.e(new Ei(call));
        call.D(new Fi(lu7));
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Object d(java.lang.Exception r5, com.mapped.Xe6<?> r6) {
        /*
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.J88.Gi
            if (r0 == 0) goto L_0x002d
            r0 = r6
            com.fossil.J88$Gi r0 = (com.fossil.J88.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002d
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003b
            if (r3 != r4) goto L_0x0033
            java.lang.Object r0 = r0.L$0
            java.lang.Exception r0 = (java.lang.Exception) r0
            boolean r2 = r1 instanceof com.fossil.Dl7.Bi
            if (r2 == 0) goto L_0x004b
            r0 = r1
            com.fossil.Dl7$Bi r0 = (com.fossil.Dl7.Bi) r0
            java.lang.Throwable r0 = r0.exception
            throw r0
        L_0x002d:
            com.fossil.J88$Gi r0 = new com.fossil.J88$Gi
            r0.<init>(r6)
            goto L_0x0013
        L_0x0033:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003b:
            boolean r3 = r1 instanceof com.fossil.Dl7.Bi
            if (r3 != 0) goto L_0x004c
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = com.fossil.Fy7.b(r0)
            if (r0 != r2) goto L_0x004a
            return r2
        L_0x004a:
            r0 = r5
        L_0x004b:
            throw r0
        L_0x004c:
            com.fossil.Dl7$Bi r1 = (com.fossil.Dl7.Bi) r1
            java.lang.Throwable r0 = r1.exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.J88.d(java.lang.Exception, com.mapped.Xe6):java.lang.Object");
    }
}
