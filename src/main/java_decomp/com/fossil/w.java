package com.fossil;

import com.mapped.Oh;
import com.mapped.Vh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W extends Vh {
    @DexIgnore
    public W(Z z, Oh oh) {
        super(oh);
    }

    @DexIgnore
    @Override // com.mapped.Vh
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0";
    }
}
