package com.fossil;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yv2 {
    @DexIgnore
    public static /* final */ Zi0<String, Uri> a; // = new Zi0<>();

    @DexIgnore
    public static Uri a(String str) {
        Uri uri;
        synchronized (Yv2.class) {
            try {
                uri = a.get(str);
                if (uri == null) {
                    String valueOf = String.valueOf(Uri.encode(str));
                    uri = Uri.parse(valueOf.length() != 0 ? "content://com.google.android.gms.phenotype/".concat(valueOf) : new String("content://com.google.android.gms.phenotype/"));
                    a.put(str, uri);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return uri;
    }
}
