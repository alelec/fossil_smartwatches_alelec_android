package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yr extends Ss {
    @DexIgnore
    public long A;
    @DexIgnore
    public B5 B; // = B5.b;

    @DexIgnore
    public Yr(K5 k5, long j) {
        super(Hs.K, k5);
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.q1, Ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new Z5(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void g(U5 u5) {
        this.B = ((Z5) u5).l;
        this.g.add(new Hw(0, null, null, G80.k(new JSONObject(), Jd0.q1, Ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void i(P7 p7) {
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.p1, Ey1.a(this.y.D())), Jd0.k0, this.y.x);
    }
}
