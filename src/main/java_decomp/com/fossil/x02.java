package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X02 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ T32 b;
    @DexIgnore
    public /* final */ T32 c;

    @DexIgnore
    public X02(Context context, T32 t32, T32 t322) {
        this.a = context;
        this.b = t32;
        this.c = t322;
    }

    @DexIgnore
    public W02 a(String str) {
        return W02.a(this.a, this.b, this.c, str);
    }
}
