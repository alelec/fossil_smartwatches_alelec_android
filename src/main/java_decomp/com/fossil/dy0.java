package com.fossil;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Dy0 {
    @DexIgnore
    void a(ViewGroup viewGroup, View view);

    @DexIgnore
    void setVisibility(int i);
}
