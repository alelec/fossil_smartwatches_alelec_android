package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dj5 {
    @DexIgnore
    public static final List<Alarm> a(List<com.portfolio.platform.data.source.local.alarm.Alarm> list) {
        Wg6.c(list, "$this$toButtonAlarm");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            Alarm alarm = new Alarm();
            alarm.setAlarmTitle(t.getTitle());
            alarm.setAlarmMessage(t.getMessage());
            alarm.setAlarmMinute(t.getTotalMinutes());
            alarm.setRepeat(t.isRepeated());
            alarm.setDays(t.getDays());
            arrayList.add(alarm);
        }
        return arrayList;
    }
}
