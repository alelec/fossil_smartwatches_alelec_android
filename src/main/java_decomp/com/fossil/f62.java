package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F62 extends L62 {
    @DexIgnore
    public /* final */ int zzas;

    @DexIgnore
    public F62(int i, String str, Intent intent) {
        super(str, intent);
        this.zzas = i;
    }

    @DexIgnore
    public int getConnectionStatusCode() {
        return this.zzas;
    }
}
