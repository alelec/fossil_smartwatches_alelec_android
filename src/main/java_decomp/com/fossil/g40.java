package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G40 extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ E60 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public G40(E60 e60) {
        super(1);
        this.b = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Lp lp) {
        if (((Ko) lp).Z) {
            this.b.q0(Q40.Ci.DISCONNECTED);
        } else {
            this.b.q0(Q40.Ci.CONNECTED);
        }
        return Cd6.a;
    }
}
