package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Wc1 {
    @DexIgnore
    public static /* final */ Wc1 a; // = new Ai();
    @DexIgnore
    public static /* final */ Wc1 b; // = new Bi();
    @DexIgnore
    public static /* final */ Wc1 c; // = new Ci();
    @DexIgnore
    public static /* final */ Wc1 d; // = new Di();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Wc1 {
        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean b() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean c(Gb1 gb1) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean d(boolean z, Gb1 gb1, Ib1 ib1) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Wc1 {
        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean b() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean c(Gb1 gb1) {
            return (gb1 == Gb1.DATA_DISK_CACHE || gb1 == Gb1.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean d(boolean z, Gb1 gb1, Ib1 ib1) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Wc1 {
        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean c(Gb1 gb1) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean d(boolean z, Gb1 gb1, Ib1 ib1) {
            return (gb1 == Gb1.RESOURCE_DISK_CACHE || gb1 == Gb1.MEMORY_CACHE) ? false : true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Wc1 {
        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean a() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean b() {
            return true;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean c(Gb1 gb1) {
            return gb1 == Gb1.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.Wc1
        public boolean d(boolean z, Gb1 gb1, Ib1 ib1) {
            return ((z && gb1 == Gb1.DATA_DISK_CACHE) || gb1 == Gb1.LOCAL) && ib1 == Ib1.TRANSFORMED;
        }
    }

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c(Gb1 gb1);

    @DexIgnore
    public abstract boolean d(boolean z, Gb1 gb1, Ib1 ib1);
}
