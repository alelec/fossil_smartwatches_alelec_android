package com.fossil;

import com.fossil.Dl7;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fz7 {
    @DexIgnore
    public static /* final */ int a; // = d(Throwable.class, -1);
    @DexIgnore
    public static /* final */ ReentrantReadWriteLock b; // = new ReentrantReadWriteLock();
    @DexIgnore
    public static /* final */ WeakHashMap<Class<? extends Throwable>, Hg6<Throwable, Throwable>> c; // = new WeakHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Throwable invoke(Throwable th) {
            return invoke(th);
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                Dl7.Ai ai = Dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage(), th);
                if (newInstance != null) {
                    r0 = Dl7.constructor-impl((Throwable) newInstance);
                    if (Dl7.isFailure-impl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                Dl7.Ai ai2 = Dl7.Companion;
                r0 = Dl7.constructor-impl(El7.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Throwable invoke(Throwable th) {
            return invoke(th);
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                Dl7.Ai ai = Dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th);
                if (newInstance != null) {
                    r0 = Dl7.constructor-impl((Throwable) newInstance);
                    if (Dl7.isFailure-impl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th2) {
                Dl7.Ai ai2 = Dl7.Companion;
                r0 = Dl7.constructor-impl(El7.a(th2));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Throwable invoke(Throwable th) {
            return invoke(th);
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                Dl7.Ai ai = Dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(th.getMessage());
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    r0 = Dl7.constructor-impl(th2);
                    if (Dl7.isFailure-impl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                Dl7.Ai ai2 = Dl7.Companion;
                r0 = Dl7.constructor-impl(El7.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Hg6<Throwable, Throwable> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor $constructor$inlined;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Constructor constructor) {
            super(1);
            this.$constructor$inlined = constructor;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Throwable invoke(Throwable th) {
            return invoke(th);
        }

        @DexIgnore
        public final Throwable invoke(Throwable th) {
            Object r0;
            try {
                Dl7.Ai ai = Dl7.Companion;
                Object newInstance = this.$constructor$inlined.newInstance(new Object[0]);
                if (newInstance != null) {
                    Throwable th2 = (Throwable) newInstance;
                    th2.initCause(th);
                    r0 = Dl7.constructor-impl(th2);
                    if (Dl7.isFailure-impl(r0)) {
                        r0 = null;
                    }
                    return (Throwable) r0;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Throwable");
            } catch (Throwable th3) {
                Dl7.Ai ai2 = Dl7.Companion;
                r0 = Dl7.constructor-impl(El7.a(th3));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Integer.valueOf(t2.getParameterTypes().length), Integer.valueOf(t.getParameterTypes().length));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Qq7 implements Hg6 {
        @DexIgnore
        public static /* final */ Fi INSTANCE; // = new Fi();

        @DexIgnore
        public Fi() {
            super(1);
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return invoke((Throwable) obj);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            throw null;
            //return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Qq7 implements Hg6 {
        @DexIgnore
        public static /* final */ Gi INSTANCE; // = new Gi();

        @DexIgnore
        public Gi() {
            super(1);
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return invoke((Throwable) obj);
        }

        @DexIgnore
        public final Void invoke(Throwable th) {
            throw null;
            //return null;
        }
    }

    @DexIgnore
    public static final Hg6<Throwable, Throwable> a(Constructor<?> constructor) {
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        int length = parameterTypes.length;
        if (length == 0) {
            return new Di(constructor);
        }
        if (length == 1) {
            Class<?> cls = parameterTypes[0];
            if (Wg6.a(cls, Throwable.class)) {
                return new Bi(constructor);
            }
            if (Wg6.a(cls, String.class)) {
                return new Ci(constructor);
            }
            return null;
        } else if (length == 2 && Wg6.a(parameterTypes[0], String.class) && Wg6.a(parameterTypes[1], Throwable.class)) {
            return new Ai(constructor);
        } else {
            return null;
        }
    }

    @DexIgnore
    public static final int b(Class<?> cls, int i) {
        do {
            Field[] declaredFields = cls.getDeclaredFields();
            int length = declaredFields.length;
            int i2 = 0;
            for (int i3 = 0; i3 < length; i3++) {
                if (!Modifier.isStatic(declaredFields[i3].getModifiers())) {
                    i2++;
                }
            }
            i += i2;
            cls = cls.getSuperclass();
        } while (cls != null);
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int c(Class cls, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return b(cls, i);
    }

    @DexIgnore
    public static final int d(Class<?> cls, int i) {
        Object r0;
        Ep7.c(cls);
        try {
            Dl7.Ai ai = Dl7.Companion;
            r0 = Dl7.constructor-impl(Integer.valueOf(c(cls, 0, 1, null)));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            r0 = Dl7.constructor-impl(El7.a(th));
        }
        if (Dl7.isFailure-impl(r0)) {
            r0 = Integer.valueOf(i);
        }
        return ((Number) r0).intValue();
    }

    /*  JADX ERROR: StackOverflowError in pass: MarkFinallyVisitor
        java.lang.StackOverflowError
        	at jadx.core.dex.nodes.InsnNode.isSame(InsnNode.java:303)
        	at jadx.core.dex.instructions.InvokeNode.isSame(InvokeNode.java:77)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.sameInsns(MarkFinallyVisitor.java:451)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.compareBlocks(MarkFinallyVisitor.java:436)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:408)
        	at jadx.core.dex.visitors.MarkFinallyVisitor.checkBlocksTree(MarkFinallyVisitor.java:411)
        */
    @DexIgnore
    public static final <E extends java.lang.Throwable> E e(E r9) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fz7.e(java.lang.Throwable):java.lang.Throwable");
    }
}
