package com.fossil;

import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D26 extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public Ai(String str) {
            Wg6.c(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.contact.ContactGroup> */
        /* JADX WARN: Multi-variable type inference failed */
        public Ci(List<? extends ContactGroup> list) {
            Wg6.c(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = D26.class.getSimpleName();
        Wg6.b(simpleName, "GetAllContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return d;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return m(bi, xe6);
    }

    @DexIgnore
    public Object m(Bi bi, Xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        return allContactGroups != null ? new Ci(allContactGroups) : new Ai("Get all contact group failed");
    }
}
