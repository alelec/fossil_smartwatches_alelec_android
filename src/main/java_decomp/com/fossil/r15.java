package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class R15 extends Q15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        Q.put(2131362666, 2);
        Q.put(2131363410, 3);
        Q.put(2131362089, 4);
        Q.put(2131362667, 5);
        Q.put(2131362402, 6);
        Q.put(2131362401, 7);
        Q.put(2131362783, 8);
        Q.put(2131362399, 9);
        Q.put(2131362397, 10);
        Q.put(2131362429, 11);
        Q.put(2131362735, 12);
        Q.put(2131361890, 13);
        Q.put(2131362051, 14);
        Q.put(2131362091, 15);
        Q.put(2131362925, 16);
        Q.put(2131362505, 17);
        Q.put(2131362445, 18);
        Q.put(2131362189, 19);
        Q.put(2131362796, 20);
        Q.put(2131363455, 21);
        Q.put(2131362494, 22);
        Q.put(2131363023, 23);
    }
    */

    @DexIgnore
    public R15(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public R15(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (AppBarLayout) objArr[13], (ConstraintLayout) objArr[14], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[1], (OverviewDayChart) objArr[19], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (View) objArr[8], (LinearLayout) objArr[20], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (RecyclerView) objArr[23], (FlexibleTextView) objArr[3], (View) objArr[21]);
        this.O = -1;
        this.K.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
