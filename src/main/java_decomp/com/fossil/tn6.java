package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.N04;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tn6 extends BaseFragment implements Sn6 {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public /* final */ String g; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String h; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String i; // = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public int j;
    @DexIgnore
    public Rn6 k;
    @DexIgnore
    public Gr4 l;
    @DexIgnore
    public G37<F85> m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Tn6 a() {
            return new Tn6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ F85 a;
        @DexIgnore
        public /* final */ /* synthetic */ Tn6 b;

        @DexIgnore
        public Bi(F85 f85, Tn6 tn6) {
            this.a = f85;
            this.b = tn6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.h)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwareFragment", "set icon color " + this.b.h);
                int parseColor = Color.parseColor(this.b.h);
                TabLayout.g v = this.a.t.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.g) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.g);
                TabLayout.g v2 = this.a.t.v(this.b.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Tn6 a;

        @DexIgnore
        public Ci(Tn6 tn6) {
            this.a = tn6;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.g) && !TextUtils.isEmpty(this.a.h)) {
                int parseColor = Color.parseColor(this.a.g);
                int parseColor2 = Color.parseColor(this.a.h);
                gVar.o(2131230966);
                if (i == this.a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Rn6 rn6) {
        O6(rn6);
    }

    @DexIgnore
    public void O6(Rn6 rn6) {
        Wg6.c(rn6, "presenter");
        this.k = rn6;
    }

    @DexIgnore
    public final void P6() {
        G37<F85> g37 = this.m;
        if (g37 != null) {
            F85 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.t : null;
            if (tabLayout != null) {
                G37<F85> g372 = this.m;
                if (g372 != null) {
                    F85 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.w : null;
                    if (viewPager2 != null) {
                        new N04(tabLayout, viewPager2, new Ci(this)).a();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Sn6
    public void X(List<? extends Explore> list) {
        Wg6.c(list, "data");
        Gr4 gr4 = this.l;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            Wg6.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Sn6
    public void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        if (isActive()) {
            G37<F85> g37 = this.m;
            if (g37 != null) {
                F85 a2 = g37.a();
                if (a2 != null && (flexibleProgressBar = a2.u) != null) {
                    flexibleProgressBar.setProgress(i2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        F85 f85 = (F85) Aq0.f(layoutInflater, 2131558579, viewGroup, false, A6());
        this.m = new G37<>(this, f85);
        Wg6.b(f85, "binding");
        return f85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Rn6 rn6 = this.k;
        if (rn6 == null) {
            return;
        }
        if (rn6 != null) {
            rn6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Rn6 rn6 = this.k;
        if (rn6 == null) {
            return;
        }
        if (rn6 != null) {
            rn6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        this.l = new Gr4(new ArrayList());
        G37<F85> g37 = this.m;
        if (g37 != null) {
            F85 a2 = g37.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.u;
                Wg6.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleTextView flexibleTextView = a2.s;
                Wg6.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                ViewPager2 viewPager2 = a2.w;
                Wg6.b(viewPager2, "binding.rvpTutorial");
                Gr4 gr4 = this.l;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (!TextUtils.isEmpty(this.i)) {
                        TabLayout tabLayout = a2.t;
                        Wg6.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.i)));
                    }
                    P6();
                    a2.w.g(new Bi(a2, this));
                    return;
                }
                Wg6.n("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
