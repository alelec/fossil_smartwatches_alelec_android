package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Z40;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nl1 extends Z40 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Nl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nl1 createFromParcel(Parcel parcel) {
            Z40 b = Z40.CREATOR.b(parcel);
            if (b != null) {
                return (Nl1) b;
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedAlarm");
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nl1[] newArray(int i) {
            return new Nl1[i];
        }
    }

    @DexIgnore
    public Nl1(Ol1 ol1, Ql1 ql1, Il1 il1) {
        super(ol1, ql1, il1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Nl1(Ol1 ol1, Ql1 ql1, Il1 il1, int i, Qg6 qg6) {
        this(ol1, (i & 2) != 0 ? null : ql1, (i & 4) != 0 ? null : il1);
    }

    @DexIgnore
    @Override // com.fossil.El1
    public Ol1 getFireTime() {
        Gl1[] b = b();
        for (Gl1 gl1 : b) {
            if (gl1 instanceof Ol1) {
                if (gl1 != null) {
                    return (Ol1) gl1;
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.RepeatedFireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }
}
