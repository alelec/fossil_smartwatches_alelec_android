package com.fossil;

import android.content.res.Resources;
import com.mapped.Gg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U67 {
    @DexIgnore
    public static /* final */ Yk7 a; // = Zk7.a(Ai.INSTANCE);
    @DexIgnore
    public static /* final */ U67 b; // = new U67();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<Float> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(0);
        }

        @DexIgnore
        /* Return type fixed from 'float' to match base method */
        @Override // com.mapped.Gg6
        public final Float invoke() {
            Resources system = Resources.getSystem();
            Wg6.b(system, "Resources.getSystem()");
            return system.getDisplayMetrics().density;
        }
    }

    @DexIgnore
    public final float a() {
        return ((Number) a.getValue()).floatValue();
    }
}
