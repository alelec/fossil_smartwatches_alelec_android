package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dy2<K, V> extends By2<K, V> {
    @DexIgnore
    public final Ey2<K, V> a() {
        Set<Map.Entry<K, Collection<V>>> entrySet = this.a.entrySet();
        if (entrySet.isEmpty()) {
            return Rx2.zza;
        }
        Zx2 zx2 = new Zx2(entrySet.size());
        int i = 0;
        for (Map.Entry<K, Collection<V>> entry : entrySet) {
            K key = entry.getKey();
            Ay2 zza = Ay2.zza(entry.getValue());
            if (!zza.isEmpty()) {
                int i2 = (zx2.b + 1) << 1;
                Object[] objArr = zx2.a;
                if (i2 > objArr.length) {
                    int length = objArr.length;
                    if (i2 >= 0) {
                        int i3 = length + (length >> 1) + 1;
                        if (i3 < i2) {
                            i3 = Integer.highestOneBit(i2 - 1) << 1;
                        }
                        if (i3 < 0) {
                            i3 = Integer.MAX_VALUE;
                        }
                        zx2.a = Arrays.copyOf(objArr, i3);
                    } else {
                        throw new AssertionError("cannot store more than MAX_VALUE elements");
                    }
                }
                Ex2.a(key, zza);
                Object[] objArr2 = zx2.a;
                int i4 = zx2.b;
                objArr2[i4 * 2] = key;
                objArr2[(i4 * 2) + 1] = zza;
                zx2.b = i4 + 1;
                i = zza.size() + i;
            }
        }
        return new Ey2<>(Py2.zza(zx2.b, zx2.a), i, null);
    }
}
