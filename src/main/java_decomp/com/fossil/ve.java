package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ve extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ue CREATOR; // = new Ue(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public Ve(int i, int i2, int i3, int i4) {
        int i5 = 3200;
        boolean z = false;
        if (true == (i < 6)) {
            i = 6;
        } else {
            if (true == (i > 3200)) {
                i = 3200;
            }
        }
        this.b = i;
        this.c = (i > i2 || 3200 < i2) ? this.b : i2;
        if (true == (i3 < 0)) {
            i3 = 0;
        } else {
            if (true == (i3 > 499)) {
                i3 = 499;
            }
        }
        this.d = i3;
        if (true == (i4 < 10)) {
            i5 = 10;
        } else {
            if (true != (i4 > 3200 ? true : z)) {
                i5 = i4;
            }
        }
        this.e = i5;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.N, Integer.valueOf(this.b)), Jd0.M, Integer.valueOf(this.c)), Jd0.O, Integer.valueOf(this.d)), Jd0.P, Integer.valueOf(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
    }
}
