package com.fossil;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Fs2 extends IInterface {
    @DexIgnore
    Rg2 O1(float f) throws RemoteException;

    @DexIgnore
    Rg2 g() throws RemoteException;

    @DexIgnore
    Rg2 zza(Bitmap bitmap) throws RemoteException;

    @DexIgnore
    Rg2 zza(String str) throws RemoteException;
}
