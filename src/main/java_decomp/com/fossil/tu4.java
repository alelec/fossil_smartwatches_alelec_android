package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tu4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public List<Vs4> a; // = new ArrayList();
    @DexIgnore
    public Ai b;
    @DexIgnore
    public /* final */ int c; // = W6.d(PortfolioApp.get.instance(), 2131099689);
    @DexIgnore
    public /* final */ int d; // = W6.d(PortfolioApp.get.instance(), 2131099677);

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void k6(Vs4 vs4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Xd5 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Tu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<View, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Vs4 $item$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Vs4 vs4) {
                super(1);
                this.this$0 = bi;
                this.$item$inlined = vs4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
                invoke(view);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                Ai ai;
                if (this.this$0.getAdapterPosition() != -1 && (ai = this.this$0.c.b) != null) {
                    ai.k6(this.$item$inlined);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Tu4 tu4, Xd5 xd5, View view) {
            super(view);
            Wg6.c(xd5, "binding");
            Wg6.c(view, "root");
            this.c = tu4;
            this.a = xd5;
            this.b = view;
        }

        @DexIgnore
        public void a(Vs4 vs4) {
            int i = 8;
            Wg6.c(vs4, "item");
            Xd5 xd5 = this.a;
            View view = xd5.v;
            Wg6.b(view, "topDivider");
            view.setVisibility(getAdapterPosition() == 0 ? 8 : 0);
            View view2 = xd5.q;
            Wg6.b(view2, "bottomDivider");
            if (getAdapterPosition() != this.c.a.size() - 1) {
                i = 0;
            }
            view2.setVisibility(i);
            FlexibleTextView flexibleTextView = xd5.x;
            Wg6.b(flexibleTextView, "tvName");
            flexibleTextView.setText(vs4.c());
            FlexibleTextView flexibleTextView2 = xd5.w;
            Wg6.b(flexibleTextView2, "tvDesc");
            flexibleTextView2.setText(vs4.a());
            ImageView imageView = xd5.u;
            Wg6.b(imageView, "ivThumbnail");
            imageView.setImageDrawable(W6.f(imageView.getContext(), vs4.f()));
            String g = vs4.g();
            int hashCode = g.hashCode();
            if (hashCode != -1348781656) {
                if (hashCode == -637042289 && g.equals("activity_reach_goal")) {
                    xd5.x.setTextColor(this.c.c);
                }
            } else if (g.equals("activity_best_result")) {
                xd5.x.setTextColor(this.c.d);
            }
            FlexibleButton flexibleButton = xd5.t;
            Wg6.b(flexibleButton, "fbCreate");
            Fz4.a(flexibleButton, new Aii(this, vs4));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void k(Bi bi, int i) {
        Wg6.c(bi, "holder");
        bi.a(this.a.get(i));
    }

    @DexIgnore
    public Bi l(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Xd5 z = Xd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemChallengeTemplateLis\u2026tInflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "itemTemplateListBinding.root");
        return new Bi(this, z, n);
    }

    @DexIgnore
    public final void m(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    public final void n(List<Vs4> list) {
        Wg6.c(list, "newList");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        k(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }
}
