package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yl1 extends Ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Yl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yl1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    return new Yl1(readString, readInt, readString2);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yl1[] newArray(int i) {
            return new Yl1[i];
        }
    }

    @DexIgnore
    public Yl1(String str, int i, String str2) throws IllegalArgumentException {
        this.b = str;
        this.c = i;
        this.d = str2;
        if (!(str2.length() <= 10)) {
            StringBuilder e = E.e("traffic(");
            e.append(this.d);
            e.append(") length must be less than or equal to 10");
            throw new IllegalArgumentException(e.toString().toString());
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.c;
    }

    @DexIgnore
    public final String getDestination() {
        return this.b;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.s, this.b), Jd0.x4, Integer.valueOf(this.c)), Jd0.z4, this.d);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
    }
}
