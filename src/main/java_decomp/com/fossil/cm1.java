package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cm1 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Cm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Cm1 a(Parcel parcel) {
            return new Cm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Cm1 createFromParcel(Parcel parcel) {
            return new Cm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Cm1[] newArray(int i) {
            return new Cm1[i];
        }
    }

    @DexIgnore
    public Cm1() {
        super(Fm1.CHANCE_OF_RAIN, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ Cm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Cm1(Dt1 dt1, Et1 et1) {
        super(Fm1.CHANCE_OF_RAIN, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Cm1(Dt1 dt1, Et1 et1, int i, Qg6 qg6) {
        this(dt1, (i & 2) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }
}
