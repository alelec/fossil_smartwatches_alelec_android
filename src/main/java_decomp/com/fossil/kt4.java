package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kt4 implements Jt4 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<It4> b;
    @DexIgnore
    public /* final */ Vh c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<It4> {
        @DexIgnore
        public Ai(Kt4 kt4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, It4 it4) {
            if (it4.c() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, it4.c());
            }
            if (it4.g() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, it4.g());
            }
            if (it4.b() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, it4.b());
            }
            if (it4.d() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, it4.d());
            }
            if (it4.e() == null) {
                mi.bindNull(5);
            } else {
                mi.bindLong(5, (long) it4.e().intValue());
            }
            if (it4.f() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, it4.f());
            }
            if (it4.a() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, it4.a());
            }
            if (it4.h() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, it4.h());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, It4 it4) {
            a(mi, it4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `profile` (`id`,`socialId`,`firstName`,`lastName`,`points`,`profilePicture`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Kt4 kt4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE profile SET socialId = ? WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Kt4 kt4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM profile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Callable<It4> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Di(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public It4 a() throws Exception {
            It4 it4;
            Integer num = null;
            Cursor b2 = Ex0.b(Kt4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "socialId");
                int c3 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
                int c4 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
                int c5 = Dx0.c(b2, "points");
                int c6 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
                int c7 = Dx0.c(b2, "createdAt");
                int c8 = Dx0.c(b2, "updatedAt");
                if (b2.moveToFirst()) {
                    String string = b2.getString(c);
                    String string2 = b2.getString(c2);
                    String string3 = b2.getString(c3);
                    String string4 = b2.getString(c4);
                    if (!b2.isNull(c5)) {
                        num = Integer.valueOf(b2.getInt(c5));
                    }
                    it4 = new It4(string, string2, string3, string4, num, b2.getString(c6), b2.getString(c7), b2.getString(c8));
                } else {
                    it4 = null;
                }
                return it4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ It4 call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore
    public Kt4(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
        new Bi(this, oh);
        this.c = new Ci(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Jt4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.c.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Jt4
    public LiveData<It4> b() {
        Rh f = Rh.f("SELECT*FROM profile  LIMIT 1", 0);
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Di di = new Di(f);
        return invalidationTracker.d(new String[]{"profile"}, false, di);
    }

    @DexIgnore
    @Override // com.fossil.Jt4
    public long c(It4 it4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(it4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Jt4
    public It4 d() {
        It4 it4;
        Integer num = null;
        Rh f = Rh.f("SELECT*FROM profile LIMIT 1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "points");
            int c7 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c8 = Dx0.c(b2, "createdAt");
            int c9 = Dx0.c(b2, "updatedAt");
            if (b2.moveToFirst()) {
                String string = b2.getString(c2);
                String string2 = b2.getString(c3);
                String string3 = b2.getString(c4);
                String string4 = b2.getString(c5);
                if (!b2.isNull(c6)) {
                    num = Integer.valueOf(b2.getInt(c6));
                }
                it4 = new It4(string, string2, string3, string4, num, b2.getString(c7), b2.getString(c8), b2.getString(c9));
            } else {
                it4 = null;
            }
            return it4;
        } finally {
            b2.close();
            f.m();
        }
    }
}
