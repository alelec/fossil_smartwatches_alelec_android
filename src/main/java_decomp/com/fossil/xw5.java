package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw5 extends du0<SleepSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ zw5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ pv5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4195a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public int f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public a(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4) {
            pq7.c(str, "mDayOfWeek");
            pq7.c(str2, "mDayOfMonth");
            pq7.c(str3, "mDailyUnit");
            pq7.c(str4, "mDailyEst");
            this.f4195a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = i;
            this.g = str3;
            this.h = str4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4, int i2, kq7 kq7) {
            this((i2 & 1) != 0 ? null : date, (i2 & 2) != 0 ? false : z, (i2 & 4) != 0 ? false : z2, (i2 & 8) != 0 ? "" : str, (i2 & 16) != 0 ? "" : str2, (i2 & 32) == 0 ? i : 0, (i2 & 64) != 0 ? "" : str3, (i2 & 128) == 0 ? str4 : "");
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final int c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.f4195a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        public final void i(String str) {
            pq7.c(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void j(String str) {
            pq7.c(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void k(int i) {
            this.f = i;
        }

        @DexIgnore
        public final void l(Date date) {
            this.f4195a = date;
        }

        @DexIgnore
        public final void m(String str) {
            pq7.c(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void n(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void o(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final void p(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4196a;
        @DexIgnore
        public /* final */ uf5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ xw5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.f4196a;
                if (date != null) {
                    this.b.d.l.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xw5 xw5, uf5 uf5, View view) {
            super(view);
            pq7.c(uf5, "binding");
            pq7.c(view, "root");
            this.d = xw5;
            this.b = uf5;
            this.c = view;
            uf5.n().setOnClickListener(new a(this));
            this.b.s.setTextColor(xw5.f);
            this.b.v.setTextColor(xw5.f);
        }

        @DexIgnore
        public void b(SleepSummary sleepSummary) {
            a v = this.d.v(sleepSummary);
            this.f4196a = v.d();
            FlexibleTextView flexibleTextView = this.b.u;
            pq7.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(v.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            pq7.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(v.e());
            if (v.c() <= 0) {
                ConstraintLayout constraintLayout = this.b.q;
                pq7.b(constraintLayout, "binding.clDailyValue");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.s;
                pq7.b(flexibleTextView3, "binding.ftvDailyUnit");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                pq7.b(constraintLayout2, "binding.clDailyValue");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.A;
                pq7.b(flexibleTextView4, "binding.tvMin");
                flexibleTextView4.setText(String.valueOf(ll5.b(v.c())));
                FlexibleTextView flexibleTextView5 = this.b.y;
                pq7.b(flexibleTextView5, "binding.tvHour");
                flexibleTextView5.setText(String.valueOf(ll5.a(v.c())));
                FlexibleTextView flexibleTextView6 = this.b.B;
                pq7.b(flexibleTextView6, "binding.tvMinUnit");
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886890);
                pq7.b(c2, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                if (c2 != null) {
                    String lowerCase = c2.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView6.setText(lowerCase);
                    FlexibleTextView flexibleTextView7 = this.b.s;
                    pq7.b(flexibleTextView7, "binding.ftvDailyUnit");
                    flexibleTextView7.setVisibility(8);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            FlexibleTextView flexibleTextView8 = this.b.s;
            pq7.b(flexibleTextView8, "binding.ftvDailyUnit");
            flexibleTextView8.setText(v.b());
            FlexibleTextView flexibleTextView9 = this.b.v;
            pq7.b(flexibleTextView9, "binding.ftvEst");
            flexibleTextView9.setText(v.a());
            if (v.g()) {
                this.b.s.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099938));
                FlexibleTextView flexibleTextView10 = this.b.s;
                pq7.b(flexibleTextView10, "binding.ftvDailyUnit");
                flexibleTextView10.setAllCaps(true);
            } else {
                this.b.s.setTextColor(gl0.d(PortfolioApp.h0.c(), 2131099940));
                FlexibleTextView flexibleTextView11 = this.b.s;
                pq7.b(flexibleTextView11, "binding.ftvDailyUnit");
                flexibleTextView11.setAllCaps(false);
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            pq7.b(constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!v.g());
            FlexibleTextView flexibleTextView12 = this.b.u;
            pq7.b(flexibleTextView12, "binding.ftvDayOfWeek");
            flexibleTextView12.setSelected(v.h());
            FlexibleTextView flexibleTextView13 = this.b.t;
            pq7.b(flexibleTextView13, "binding.ftvDayOfMonth");
            flexibleTextView13.setSelected(v.h());
            if (v.h()) {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (v.g()) {
                this.b.r.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Date f4197a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public c(Date date, Date date2, String str, String str2) {
            pq7.c(str, "mWeekly");
            pq7.c(str2, "mWeeklyValue");
            this.f4197a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Date date, Date date2, String str, String str2, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.f4197a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.f4197a = date;
        }

        @DexIgnore
        public final void g(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends b {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ wf5 g;
        @DexIgnore
        public /* final */ /* synthetic */ xw5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d b;

            @DexIgnore
            public a(d dVar) {
                this.b = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    zw5 zw5 = this.b.h.l;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            zw5.q0(date, date2);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public d(com.fossil.xw5 r4, com.fossil.wf5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.pq7.c(r5, r0)
                r3.h = r4
                com.fossil.uf5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.pq7.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.fossil.pq7.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.fossil.xw5$d$a r1 = new com.fossil.xw5$d$a
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.xw5.d.<init>(com.fossil.xw5, com.fossil.wf5):void");
        }

        @DexIgnore
        @Override // com.fossil.xw5.b
        public void b(SleepSummary sleepSummary) {
            c w = this.h.w(sleepSummary);
            this.f = w.a();
            this.e = w.b();
            FlexibleTextView flexibleTextView = this.g.s;
            pq7.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(w.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            pq7.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(w.d());
            super.b(sleepSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ xw5 b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public e(xw5 xw5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = xw5;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            pq7.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.n.getId() + ", isAdded=" + this.b.n.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.m.Z(this.b.n.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                xq0 j = this.b.m.j();
                j.b(view.getId(), this.b.n, this.b.n.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                xq0 j2 = this.b.m.j();
                j2.q(Z);
                j2.k();
                xq0 j3 = this.b.m.j();
                j3.b(view.getId(), this.b.n, this.b.n.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.n.getId() + ", isAdded2=" + this.b.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            pq7.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FrameLayout f4198a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(FrameLayout frameLayout, View view) {
            super(view);
            this.f4198a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xw5(ax5 ax5, PortfolioApp portfolioApp, zw5 zw5, FragmentManager fragmentManager, pv5 pv5) {
        super(ax5);
        pq7.c(ax5, "sleepDifference");
        pq7.c(portfolioApp, "mApp");
        pq7.c(zw5, "mOnItemClick");
        pq7.c(fragmentManager, "mFragmentManager");
        pq7.c(pv5, "mFragment");
        this.k = portfolioApp;
        this.l = zw5;
        this.m = fragmentManager;
        this.n = pv5;
        String d2 = qn5.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("nonBrandNonReachGoal");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("dianaSleepTab");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("secondaryText");
        this.h = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        String d7 = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(d7 == null ? "#FFFFFF" : d7);
        String d8 = qn5.l.a().d("onDianaSleepTab");
        this.j = Color.parseColor(d8 == null ? "#FFFFFF" : d8);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        SleepSummary sleepSummary = (SleepSummary) getItem(i2);
        if (sleepSummary != null) {
            Calendar calendar = this.c;
            pq7.b(calendar, "mCalendar");
            calendar.setTime(sleepSummary.getDate());
            Calendar calendar2 = this.c;
            pq7.b(calendar2, "mCalendar");
            Boolean p0 = lk5.p0(calendar2.getTime());
            pq7.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        pq7.c(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepsAdapter", "onBindViewHolder - position=" + i2 + ", viewType=" + getItemViewType(i2));
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            pq7.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            pq7.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardSleepsAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            pq7.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new e(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((b) viewHolder).b((SleepSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((b) viewHolder).b((SleepSummary) getItem(i2));
        } else {
            ((d) viewHolder).b((SleepSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        pq7.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new f(frameLayout, frameLayout);
        } else if (i2 == 1) {
            uf5 z = uf5.z(from, viewGroup, false);
            pq7.b(z, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View n2 = z.n();
            pq7.b(n2, "itemSleepDayBinding.root");
            return new b(this, z, n2);
        } else if (i2 != 2) {
            uf5 z2 = uf5.z(from, viewGroup, false);
            pq7.b(z2, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View n3 = z2.n();
            pq7.b(n3, "itemSleepDayBinding.root");
            return new b(this, z2, n3);
        } else {
            wf5 z3 = wf5.z(from, viewGroup, false);
            pq7.b(z3, "ItemSleepWeekBinding.inf\u2026tInflater, parent, false)");
            return new d(this, z3);
        }
    }

    @DexIgnore
    public final a v(SleepSummary sleepSummary) {
        MFSleepDay sleepDay;
        a aVar = new a(null, false, false, null, null, 0, null, null, 255, null);
        if (!(sleepSummary == null || (sleepDay = sleepSummary.getSleepDay()) == null)) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(sleepDay.getDate());
            int i2 = instance.get(7);
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = um5.c(this.k, 2131886659);
                pq7.b(c2, "LanguageHelper.getString\u2026n_SleepToday_Text__Today)");
                aVar.n(c2);
            } else {
                aVar.n(jl5.b.i(i2));
            }
            aVar.l(instance.getTime());
            aVar.m(String.valueOf(instance.get(5)));
            int sleepMinutes = sleepDay.getSleepMinutes();
            if (sleepMinutes > 0) {
                aVar.k(sleepMinutes);
                aVar.j("");
                List<MFSleepSession> sleepSessions = sleepSummary.getSleepSessions();
                if (sleepSessions != null && (!sleepSessions.isEmpty())) {
                    MFSleepSession mFSleepSession = sleepSessions.get(0);
                    int startTime = mFSleepSession.getStartTime();
                    int endTime = mFSleepSession.getEndTime();
                    jl5 jl5 = jl5.b;
                    String o = lk5.o(((long) startTime) * 1000, mFSleepSession.getTimezoneOffset());
                    pq7.b(o, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String k2 = jl5.k(o);
                    jl5 jl52 = jl5.b;
                    String o2 = lk5.o(((long) endTime) * 1000, mFSleepSession.getTimezoneOffset());
                    pq7.b(o2, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String k3 = jl52.k(o2);
                    hr7 hr7 = hr7.f1520a;
                    String c3 = um5.c(this.k, 2131887573);
                    pq7.b(c3, "LanguageHelper.getString\u2026ing.sleep_start_end_time)");
                    String format = String.format(c3, Arrays.copyOf(new Object[]{k2, k3}, 2));
                    pq7.b(format, "java.lang.String.format(format, *args)");
                    int size = sleepSessions.size();
                    if (size > 1) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(format);
                        sb.append("\n");
                        hr7 hr72 = hr7.f1520a;
                        String c4 = um5.c(this.k, 2131886649);
                        pq7.b(c4, "LanguageHelper.getString\u2026ltiple_Label__NumberMore)");
                        String format2 = String.format(c4, Arrays.copyOf(new Object[]{Integer.valueOf(size - 1)}, 1));
                        pq7.b(format2, "java.lang.String.format(format, *args)");
                        sb.append(format2);
                        aVar.i(sb.toString());
                    } else {
                        aVar.i(format);
                    }
                }
                if (sleepDay.getGoalMinutes() > 0) {
                    aVar.p(sleepMinutes >= sleepDay.getGoalMinutes());
                } else {
                    aVar.p(false);
                }
            } else {
                String c5 = um5.c(this.k, 2131886657);
                pq7.b(c5, "LanguageHelper.getString\u2026leepToday_Text__NoRecord)");
                aVar.j(c5);
                aVar.o(true);
            }
        }
        return aVar;
    }

    @DexIgnore
    public final c w(SleepSummary sleepSummary) {
        String str;
        Double averageSleepOfWeek;
        c cVar = new c(null, null, null, null, 15, null);
        if (sleepSummary != null) {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "calendar");
            instance.setTime(sleepSummary.getDate());
            Boolean p0 = lk5.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = lk5.N(i3);
            int i4 = instance.get(1);
            cVar.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = lk5.N(i6);
            int i7 = instance.get(1);
            cVar.f(instance.getTime());
            pq7.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = um5.c(this.k, 2131886661);
                pq7.b(str, "LanguageHelper.getString\u2026eepToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            cVar.g(str);
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            double doubleValue = (sleepDay == null || (averageSleepOfWeek = sleepDay.getAverageSleepOfWeek()) == null) ? 0.0d : averageSleepOfWeek.doubleValue();
            if (doubleValue <= 0.0d) {
                cVar.h("");
            } else {
                double d2 = (double) 60;
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(this.k, 2131886658);
                pq7.b(c2, "LanguageHelper.getString\u2026xt__NumberHrNumberMinAvg)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf((int) (doubleValue / d2)), Integer.valueOf((int) (doubleValue % d2))}, 2));
                pq7.b(format, "java.lang.String.format(format, *args)");
                cVar.h(format);
            }
        }
        return cVar;
    }

    @DexIgnore
    public final void x(cu0<SleepSummary> cu0) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList pagedList=");
        sb.append(cu0 != null ? Integer.valueOf(cu0.size()) : null);
        local.d("DashboardSleepsAdapter", sb.toString());
        super.i(cu0);
    }
}
