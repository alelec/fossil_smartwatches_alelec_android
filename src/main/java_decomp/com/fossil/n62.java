package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N62 extends Exception {
    @DexIgnore
    @Deprecated
    public /* final */ Status mStatus;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public N62(com.google.android.gms.common.api.Status r5) {
        /*
            r4 = this;
            int r1 = r5.f()
            java.lang.String r0 = r5.h()
            if (r0 == 0) goto L_0x0032
            java.lang.String r0 = r5.h()
        L_0x000e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r0)
            int r3 = r3.length()
            int r3 = r3 + 13
            r2.<init>(r3)
            r2.append(r1)
            java.lang.String r1 = ": "
            r2.append(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r4.<init>(r0)
            r4.mStatus = r5
            return
        L_0x0032:
            java.lang.String r0 = ""
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.N62.<init>(com.google.android.gms.common.api.Status):void");
    }

    @DexIgnore
    public Status getStatus() {
        return this.mStatus;
    }

    @DexIgnore
    public int getStatusCode() {
        return this.mStatus.f();
    }

    @DexIgnore
    @Deprecated
    public String getStatusMessage() {
        return this.mStatus.h();
    }
}
