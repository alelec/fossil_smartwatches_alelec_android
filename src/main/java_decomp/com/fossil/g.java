package com.fossil;

import android.bluetooth.BluetoothDevice;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G extends Qq7 implements Hg6<BluetoothDevice, String> {
    @DexIgnore
    public static /* final */ G b; // = new G();

    @DexIgnore
    public G() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public String invoke(BluetoothDevice bluetoothDevice) {
        String address = bluetoothDevice.getAddress();
        Wg6.b(address, "it.address");
        return address;
    }
}
