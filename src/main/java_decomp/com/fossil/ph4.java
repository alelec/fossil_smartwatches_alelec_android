package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Ph4 implements Runnable {
    @DexIgnore
    public /* final */ Sh4 b;
    @DexIgnore
    public /* final */ Intent c;
    @DexIgnore
    public /* final */ Ot3 d;

    @DexIgnore
    public Ph4(Sh4 sh4, Intent intent, Ot3 ot3) {
        this.b = sh4;
        this.c = intent;
        this.d = ot3;
    }

    @DexIgnore
    public final void run() {
        this.b.g(this.c, this.d);
    }
}
