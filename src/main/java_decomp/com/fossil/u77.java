package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface U77 {
    @DexIgnore
    void a(L77 l77);

    @DexIgnore
    List<P77> b(L77 l77);

    @DexIgnore
    P77 c(L77 l77, String str);

    @DexIgnore
    Long[] insert(List<P77> list);
}
