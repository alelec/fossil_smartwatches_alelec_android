package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lq1 extends X90 {
    @DexIgnore
    public /* final */ Bv1 e;

    @DexIgnore
    public Lq1(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(Bv1.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (Bv1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Lq1(E90 e90, byte b, Bv1 bv1) {
        super(e90, b, Hy1.p(bv1.getRequestId()));
        this.e = bv1;
    }

    @DexIgnore
    public final Bv1 c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.e, ((Lq1) obj).e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        try {
            G80.k(jSONObject, Jd0.v3, this.e);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
