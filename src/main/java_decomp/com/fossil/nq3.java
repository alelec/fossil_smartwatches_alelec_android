package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nq3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ U93 b;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService c;

    @DexIgnore
    public Nq3(AppMeasurementDynamiteService appMeasurementDynamiteService, U93 u93) {
        this.c = appMeasurementDynamiteService;
        this.b = u93;
    }

    @DexIgnore
    public final void run() {
        this.c.b.F().S(this.b, this.c.b.n());
    }
}
