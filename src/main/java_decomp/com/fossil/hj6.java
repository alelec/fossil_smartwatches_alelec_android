package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj6 extends ej6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public /* final */ LiveData<h47<List<DailyHeartRateSummary>>> g;
    @DexIgnore
    public List<String> h; // = new ArrayList();
    @DexIgnore
    public /* final */ fj6 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter", f = "HeartRateOverviewWeekPresenter.kt", l = {111}, m = "calculateStartAndEndDate")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ hj6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hj6 hj6, qn7 qn7) {
            super(qn7);
            this.this$0 = hj6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hj6 f1488a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$mHeartRateSummaries$1$1", f = "HeartRateOverviewWeekPresenter.kt", l = {38, 42, 40}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<DailyHeartRateSummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hj6$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$mHeartRateSummaries$1$1$startAndEnd$1", f = "HeartRateOverviewWeekPresenter.kt", l = {38}, m = "invokeSuspend")
            /* renamed from: com.fossil.hj6$b$a$a  reason: collision with other inner class name */
            public static final class C0109a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0109a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0109a aVar = new C0109a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Date, ? extends Date>> qn7) {
                    throw null;
                    //return ((C0109a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        a aVar = this.this$0;
                        hj6 hj6 = aVar.this$0.f1488a;
                        Date date = aVar.$it;
                        pq7.b(date, "it");
                        this.L$0 = iv7;
                        this.label = 1;
                        Object s = hj6.s(date, this);
                        return s == d ? d : s;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<DailyHeartRateSummary>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r6 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ab
                    if (r0 == r4) goto L_0x0049
                    if (r0 == r5) goto L_0x0027
                    if (r0 != r7) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    com.fossil.cl7 r0 = (com.fossil.cl7) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x001c:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001e:
                    return r0
                L_0x001f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0027:
                    java.lang.Object r0 = r8.L$2
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$1
                    com.fossil.cl7 r1 = (com.fossil.cl7) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.hs0 r2 = (com.fossil.hs0) r2
                    com.fossil.el7.b(r9)
                    r5 = r0
                    r3 = r9
                L_0x0038:
                    r0 = r3
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r7
                    java.lang.Object r0 = r5.a(r0, r8)
                    if (r0 != r6) goto L_0x001c
                    r0 = r6
                    goto L_0x001e
                L_0x0049:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                    r4 = r0
                    r1 = r9
                L_0x0052:
                    r0 = r1
                    com.fossil.cl7 r0 = (com.fossil.cl7) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r1 = "mHeartRateSummaries onDateChanged - startDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = ", endDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getSecond()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = "HeartRateOverviewWeekPresenter"
                    java.lang.String r3 = r3.toString()
                    r2.d(r1, r3)
                    com.fossil.hj6$b r1 = r8.this$0
                    com.fossil.hj6 r1 = r1.f1488a
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r3 = com.fossil.hj6.o(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    java.lang.Object r2 = r0.getSecond()
                    java.util.Date r2 = (java.util.Date) r2
                    r8.L$0 = r4
                    r8.L$1 = r0
                    r8.L$2 = r4
                    r8.label = r5
                    r5 = 0
                    java.lang.Object r3 = r3.getHeartRateSummaries(r1, r2, r5, r8)
                    if (r3 != r6) goto L_0x00cb
                    r0 = r6
                    goto L_0x001e
                L_0x00ab:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.hj6$b r1 = r8.this$0
                    com.fossil.hj6 r1 = r1.f1488a
                    com.fossil.dv7 r1 = com.fossil.hj6.n(r1)
                    com.fossil.hj6$b$a$a r2 = new com.fossil.hj6$b$a$a
                    r3 = 0
                    r2.<init>(r8, r3)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
                    if (r1 != r6) goto L_0x00d0
                    r0 = r6
                    goto L_0x001e
                L_0x00cb:
                    r2 = r4
                    r1 = r0
                    r5 = r4
                    goto L_0x0038
                L_0x00d0:
                    r4 = r0
                    goto L_0x0052
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.hj6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(hj6 hj6) {
            this.f1488a = hj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<DailyHeartRateSummary>>> apply(Date date) {
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<DailyHeartRateSummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ hj6 f1489a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$1", f = "HeartRateOverviewWeekPresenter.kt", l = {66}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.hj6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$1$listRestingDataPoint$1", f = "HeartRateOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.hj6$c$a$a  reason: collision with other inner class name */
            public static final class C0110a extends ko7 implements vp7<iv7, qn7<? super List<? extends Integer>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0110a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0110a aVar = new C0110a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends Integer>> qn7) {
                    throw null;
                    //return ((C0110a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        List<T> list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        ArrayList arrayList = new ArrayList(im7.m(list, 10));
                        for (T t : list) {
                            Resting resting = t.getResting();
                            arrayList.add(resting != null ? ao7.e(resting.getValue()) : null);
                        }
                        return arrayList;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 h = this.this$0.f1489a.h();
                    C0110a aVar = new C0110a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (list != null) {
                    this.this$0.f1489a.i.q5(pm7.j0(list), this.this$0.f1489a.h);
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(hj6 hj6) {
            this.f1489a = hj6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<DailyHeartRateSummary>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSummaries -- heartRateSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("HeartRateOverviewWeekPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                xw7 unused = gu7.d(this.f1489a.k(), null, null, new a(this, list, null), 3, null);
            }
        }
    }

    @DexIgnore
    public hj6(fj6 fj6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        pq7.c(fj6, "mView");
        pq7.c(userRepository, "userRepository");
        pq7.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.i = fj6;
        this.j = userRepository;
        this.k = heartRateSummaryRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<h47<List<DailyHeartRateSummary>>> c2 = ss0.c(mutableLiveData, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = c2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !lk5.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewWeekPresenter", "start with date " + this.e);
        t();
        LiveData<h47<List<DailyHeartRateSummary>>> liveData = this.g;
        fj6 fj6 = this.i;
        if (fj6 != null) {
            liveData.h((gj6) fj6, new c(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        MFLogger.d("HeartRateOverviewWeekPresenter", "stop");
        try {
            LiveData<h47<List<DailyHeartRateSummary>>> liveData = this.g;
            fj6 fj6 = this.i;
            if (fj6 != null) {
                liveData.n((gj6) fj6);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
        } catch (Exception e2) {
            MFLogger.d("HeartRateOverviewWeekPresenter", "stop exception " + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(java.util.Date r6, com.fossil.qn7<? super com.fossil.cl7<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.hj6.a
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.fossil.hj6$a r0 = (com.fossil.hj6.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.fossil.dr7 r0 = (com.fossil.dr7) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.fossil.hj6 r2 = (com.fossil.hj6) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.fossil.lk5.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.fossil.lk5.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.fossil.cl7 r2 = new com.fossil.cl7
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.fossil.hj6$a r0 = new com.fossil.hj6$a
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.el7.b(r3)
            com.fossil.dr7 r0 = new com.fossil.dr7
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.fossil.lk5.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r4 = r0
            r1 = r6
            goto L_0x0031
        L_0x008a:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hj6.s(java.util.Date, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void t() {
        this.h.clear();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(this.e);
        instance.add(5, -6);
        for (int i2 = 1; i2 <= 7; i2++) {
            Boolean p0 = lk5.p0(instance.getTime());
            pq7.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                List<String> list = this.h;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886648);
                pq7.b(c2, "LanguageHelper.getString\u2026_Sleep7days_Label__Today)");
                list.add(c2);
            } else {
                switch (instance.get(7)) {
                    case 1:
                        List<String> list2 = this.h;
                        String c3 = um5.c(PortfolioApp.h0.c(), 2131886770);
                        pq7.b(c3, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                        list2.add(c3);
                        continue;
                    case 2:
                        List<String> list3 = this.h;
                        String c4 = um5.c(PortfolioApp.h0.c(), 2131886769);
                        pq7.b(c4, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                        list3.add(c4);
                        continue;
                    case 3:
                        List<String> list4 = this.h;
                        String c5 = um5.c(PortfolioApp.h0.c(), 2131886772);
                        pq7.b(c5, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                        list4.add(c5);
                        continue;
                    case 4:
                        List<String> list5 = this.h;
                        String c6 = um5.c(PortfolioApp.h0.c(), 2131886774);
                        pq7.b(c6, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                        list5.add(c6);
                        continue;
                    case 5:
                        List<String> list6 = this.h;
                        String c7 = um5.c(PortfolioApp.h0.c(), 2131886773);
                        pq7.b(c7, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                        list6.add(c7);
                        continue;
                    case 6:
                        List<String> list7 = this.h;
                        String c8 = um5.c(PortfolioApp.h0.c(), 2131886768);
                        pq7.b(c8, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                        list7.add(c8);
                        continue;
                    case 7:
                        List<String> list8 = this.h;
                        String c9 = um5.c(PortfolioApp.h0.c(), 2131886771);
                        pq7.b(c9, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                        list8.add(c9);
                        continue;
                }
            }
            instance.add(5, 1);
        }
    }

    @DexIgnore
    public void u() {
        this.i.M5(this);
    }
}
