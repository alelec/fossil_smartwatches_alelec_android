package com.fossil;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.fossil.Ig0;
import com.fossil.Ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dg0 implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, Ig0.Ai {
    @DexIgnore
    public Cg0 b;
    @DexIgnore
    public Ve0 c;
    @DexIgnore
    public Ag0 d;
    @DexIgnore
    public Ig0.Ai e;

    @DexIgnore
    public Dg0(Cg0 cg0) {
        this.b = cg0;
    }

    @DexIgnore
    public void a() {
        Ve0 ve0 = this.c;
        if (ve0 != null) {
            ve0.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0.Ai
    public void b(Cg0 cg0, boolean z) {
        if (z || cg0 == this.b) {
            a();
        }
        Ig0.Ai ai = this.e;
        if (ai != null) {
            ai.b(cg0, z);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0.Ai
    public boolean c(Cg0 cg0) {
        Ig0.Ai ai = this.e;
        if (ai != null) {
            return ai.c(cg0);
        }
        return false;
    }

    @DexIgnore
    public void d(IBinder iBinder) {
        Cg0 cg0 = this.b;
        Ve0.Ai ai = new Ve0.Ai(cg0.w());
        Ag0 ag0 = new Ag0(ai.b(), Re0.abc_list_menu_item_layout);
        this.d = ag0;
        ag0.g(this);
        this.b.b(this.d);
        ai.c(this.d.a(), this);
        View A = cg0.A();
        if (A != null) {
            ai.e(A);
        } else {
            ai.f(cg0.y());
            ai.o(cg0.z());
        }
        ai.j(this);
        Ve0 a2 = ai.a();
        this.c = a2;
        a2.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.N((Eg0) this.d.a().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        this.d.b(this.b, true);
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.e(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i, keyEvent, 0);
    }
}
