package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu5 extends iq4<a, iq4.d, iq4.a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ GoalTrackingRepository d;
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Date f1533a;

        @DexIgnore
        public a(Date date) {
            pq7.c(date, "date");
            this.f1533a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.f1533a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries", f = "FetchDailyGoalTrackingSummaries.kt", l = {27, 55, 57}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ hu5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hu5 hu5, qn7 qn7) {
            super(qn7);
            this.this$0 = hu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = hu5.class.getSimpleName();
        pq7.b(simpleName, "FetchDailyGoalTrackingSu\u2026es::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public hu5(GoalTrackingRepository goalTrackingRepository, UserRepository userRepository) {
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(userRepository, "mUserRepository");
        this.d = goalTrackingRepository;
        this.e = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.hu5.a r22, com.fossil.qn7<? super com.fossil.tl7> r23) {
        /*
        // Method dump skipped, instructions count: 517
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hu5.k(com.fossil.hu5$a, com.fossil.qn7):java.lang.Object");
    }
}
