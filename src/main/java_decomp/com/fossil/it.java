package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class It {
    @DexIgnore
    public /* synthetic */ It(Qg6 qg6) {
    }

    @DexIgnore
    public final Kt a(byte b) {
        Kt kt;
        Kt[] values = Kt.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                kt = null;
                break;
            }
            kt = values[i];
            if (kt.c == b) {
                break;
            }
            i++;
        }
        return kt != null ? kt : Kt.i;
    }
}
