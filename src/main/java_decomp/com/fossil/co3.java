package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Co3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public Co3(Un3 un3, Bundle bundle) {
        this.c = un3;
        this.b = bundle;
    }

    @DexIgnore
    public final void run() {
        this.c.v0(this.b);
    }
}
