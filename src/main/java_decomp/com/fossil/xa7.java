package com.fossil;

import com.portfolio.platform.watchface.gallery.WatchFaceGalleryFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xa7 implements MembersInjector<WatchFaceGalleryFragment> {
    @DexIgnore
    public static void a(WatchFaceGalleryFragment watchFaceGalleryFragment, Po4 po4) {
        watchFaceGalleryFragment.h = po4;
    }
}
