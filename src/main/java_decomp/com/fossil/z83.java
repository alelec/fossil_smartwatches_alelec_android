package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z83 implements Xw2<C93> {
    @DexIgnore
    public static Z83 c; // = new Z83();
    @DexIgnore
    public /* final */ Xw2<C93> b;

    @DexIgnore
    public Z83() {
        this(Ww2.b(new B93()));
    }

    @DexIgnore
    public Z83(Xw2<C93> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((C93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ C93 zza() {
        return this.b.zza();
    }
}
