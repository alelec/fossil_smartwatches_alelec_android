package com.fossil;

import com.fossil.Lz7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Wy7<E> {
    @DexIgnore
    Object a();

    @DexIgnore
    void d(E e);

    @DexIgnore
    Vz7 e(E e, Lz7.Ci ci);
}
