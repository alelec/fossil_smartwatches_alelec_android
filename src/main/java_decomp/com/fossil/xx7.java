package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Xx7 {
    @DexIgnore
    long a();

    @DexIgnore
    Runnable b(Runnable runnable);

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    void e(Thread thread);

    @DexIgnore
    void f(Object obj, long j);

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    Object h();  // void declaration
}
