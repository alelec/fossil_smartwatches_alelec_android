package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mm5 {
    @DexIgnore
    public static byte a; // = 61;

    @DexIgnore
    public static final byte[] a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i < bArr.length) {
            byte b = bArr[i];
            if (b == a) {
                int i2 = i + 1;
                if ('\r' == ((char) bArr[i2])) {
                    i += 2;
                    if ('\n' == ((char) bArr[i])) {
                        continue;
                    }
                }
                try {
                    int digit = Character.digit((char) bArr[i2], 16);
                    i = i2 + 1;
                    int digit2 = Character.digit((char) bArr[i], 16);
                    if (!(digit == -1 || digit2 == -1)) {
                        byteArrayOutputStream.write((char) (digit2 + (digit << 4)));
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                }
                return null;
            }
            byteArrayOutputStream.write(b);
            i++;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
