package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vx3 {
    @DexIgnore
    public static int a(Context context, int i, int i2) {
        TypedValue a2 = Nz3.a(context, i);
        return a2 != null ? a2.data : i2;
    }

    @DexIgnore
    public static int b(Context context, int i, String str) {
        return Nz3.c(context, i, str);
    }

    @DexIgnore
    public static int c(View view, int i) {
        return Nz3.d(view, i);
    }

    @DexIgnore
    public static int d(View view, int i, int i2) {
        return a(view.getContext(), i, i2);
    }

    @DexIgnore
    public static int e(int i, int i2) {
        return Pl0.e(i2, i);
    }

    @DexIgnore
    public static int f(int i, int i2, float f) {
        return e(i, Pl0.h(i2, Math.round(((float) Color.alpha(i2)) * f)));
    }

    @DexIgnore
    public static int g(View view, int i, int i2, float f) {
        return f(c(view, i), c(view, i2), f);
    }
}
