package com.fossil;

import android.graphics.Rect;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Fh0 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    void setOnFitSystemWindowsListener(Ai ai);
}
