package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G6 extends U5 {
    @DexIgnore
    public R4 k; // = R4.b;

    @DexIgnore
    public G6(N4 n4) {
        super(V5.l, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.I();
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void f(H7 h7) {
        S5 a2;
        k(h7);
        G7 g7 = h7.a;
        if (g7.b == F7.b) {
            a2 = this.k == R4.b ? S5.a(this.e, null, R5.b, null, 5) : S5.a(this.e, null, R5.e, null, 5);
        } else {
            S5 a3 = S5.e.a(g7);
            a2 = S5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        R4 r4;
        return (h7 instanceof A7) && ((r4 = ((A7) h7).b) == R4.d || r4 == R4.b);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.j;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.k = ((A7) h7).b;
    }
}
