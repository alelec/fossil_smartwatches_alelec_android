package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dj extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Af b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Dj(Af af) {
        super(1);
        this.b = af;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        Af af = this.b;
        long j = ((Wv) fs).L;
        af.I = 0;
        af.H = 0;
        af.J = 0;
        af.G = j;
        int i = (j > af.D ? 1 : (j == af.D ? 0 : -1));
        if (i > 0 || j <= 0) {
            af.G = 0;
            af.T();
        } else if (i == 0) {
            af.U();
        } else {
            af.H = j;
            af.V();
        }
        return Cd6.a;
    }
}
