package com.fossil;

import android.content.Context;
import com.fossil.M62;
import com.fossil.Q62;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rh2 extends Q62<M62.Di.Bii> {
    @DexIgnore
    public static /* final */ Qh2 j; // = new To2();

    @DexIgnore
    public Rh2(Context context, M62.Di.Bii bii) {
        super(context, Zm2.H, bii, Q62.Ai.c);
    }

    @DexIgnore
    public Nt3<Void> s(DataSet dataSet) {
        return Qc2.c(j.a(b(), dataSet));
    }
}
