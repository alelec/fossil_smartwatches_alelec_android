package com.fossil;

import android.os.Bundle;
import com.fossil.Fg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q64 implements Fg3.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ R64 a;

    @DexIgnore
    public Q64(R64 r64) {
        this.a = r64;
    }

    @DexIgnore
    @Override // com.fossil.Sn3
    public final void a(String str, String str2, Bundle bundle, long j) {
        if (this.a.a.contains(str2)) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("events", O64.g(str2));
            this.a.b.a(2, bundle2);
        }
    }
}
