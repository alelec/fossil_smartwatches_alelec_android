package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tp6 implements Factory<HelpPresenter> {
    @DexIgnore
    public static HelpPresenter a(Op6 op6, DeviceRepository deviceRepository, GetZendeskInformation getZendeskInformation, AnalyticsHelper analyticsHelper) {
        return new HelpPresenter(op6, deviceRepository, getZendeskInformation, analyticsHelper);
    }
}
