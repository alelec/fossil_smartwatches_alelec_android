package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R30 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ S50 b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public R30(S50 s50, Object obj) {
        this.b = s50;
        this.c = obj;
    }

    @DexIgnore
    public final void run() {
        this.b.b.o(this.c);
    }
}
