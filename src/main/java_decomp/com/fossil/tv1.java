package com.fossil;

import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tv1 extends Gw1 {
    @DexIgnore
    public Tv1(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    public Tv1(byte[] bArr) {
        this(G80.e(0, 1), bArr);
    }

    @DexIgnore
    @Override // com.fossil.Fw1, com.fossil.Fw1, com.fossil.Gw1, com.fossil.Gw1, com.fossil.Gw1, java.lang.Object
    public Tv1 clone() {
        String name = getName();
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Tv1(name, copyOf);
    }
}
