package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Wg6;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dn7 extends Cn7 {
    @DexIgnore
    public static final <T> Set<T> b() {
        return Tm7.INSTANCE;
    }

    @DexIgnore
    public static final <T> LinkedHashSet<T> c(T... tArr) {
        Wg6.c(tArr, MessengerShareContentUtility.ELEMENTS);
        LinkedHashSet<T> linkedHashSet = new LinkedHashSet<>(Ym7.b(tArr.length));
        Em7.b0(tArr, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Set<T> d(Set<? extends T> set) {
        Wg6.c(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        return size != 0 ? size != 1 ? set : Cn7.a(set.iterator().next()) : b();
    }
}
