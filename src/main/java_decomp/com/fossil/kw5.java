package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.mapped.AppWrapper;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public Bi a;
    @DexIgnore
    public List<? extends Object> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ Fj1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ff5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Kw5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.b;
                    if (list != null) {
                        Object obj = list.get(adapterPosition);
                        if (obj instanceof J06) {
                            J06 j06 = (J06) obj;
                            Contact contact = j06.getContact();
                            if (contact == null || contact.getContactId() != -100) {
                                Contact contact2 = j06.getContact();
                                if ((contact2 == null || contact2.getContactId() != -200) && (bi = this.b.b.a) != null) {
                                    bi.a(j06);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                FLogger.INSTANCE.getLocal().d(Kw5.e, "ivRemove.setOnClickListener");
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.b;
                    if (list != null) {
                        Object obj = list.get(adapterPosition);
                        if (obj instanceof J06) {
                            J06 j06 = (J06) obj;
                            j06.setAdded(!j06.isAdded());
                            FLogger.INSTANCE.getLocal().d(Kw5.e, "isAdded=" + j06.isAdded());
                            if (j06.isAdded()) {
                                this.b.b.c++;
                            } else {
                                Kw5 kw5 = this.b.b;
                                kw5.c--;
                            }
                        } else if (obj != null) {
                            AppWrapper appWrapper = (AppWrapper) obj;
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            if (installedApp != null) {
                                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                                if (isSelected != null) {
                                    installedApp.setSelected(!isSelected.booleanValue());
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str = Kw5.e;
                            StringBuilder sb = new StringBuilder();
                            sb.append("isSelected=");
                            InstalledApp installedApp3 = appWrapper.getInstalledApp();
                            Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                            if (isSelected2 != null) {
                                sb.append(isSelected2.booleanValue());
                                local.d(str, sb.toString());
                                InstalledApp installedApp4 = appWrapper.getInstalledApp();
                                Boolean isSelected3 = installedApp4 != null ? installedApp4.isSelected() : null;
                                if (isSelected3 == null) {
                                    Wg6.i();
                                    throw null;
                                } else if (isSelected3.booleanValue()) {
                                    this.b.b.c++;
                                } else {
                                    Kw5 kw52 = this.b.b;
                                    kw52.c--;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                        }
                        Bi bi = this.b.b.a;
                        if (bi != null) {
                            bi.b();
                        }
                        this.b.b.notifyItemChanged(adapterPosition);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements Ej1<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ J06 c;

            @DexIgnore
            public Cii(Ai ai, J06 j06) {
                this.b = ai;
                this.c = j06;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                FLogger.INSTANCE.getLocal().d(Kw5.e, "renderContactData onResourceReady");
                this.b.a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            @Override // com.fossil.Ej1
            public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
                FLogger.INSTANCE.getLocal().d(Kw5.e, "renderContactData onLoadFailed");
                Contact contact = this.c.getContact();
                if (contact == null) {
                    return false;
                }
                contact.setPhotoThumbUri(null);
                return false;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
            @Override // com.fossil.Ej1
            public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                return a(drawable, obj, qj1, gb1, z);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Kw5 kw5, Ff5 ff5) {
            super(ff5.n());
            Wg6.c(ff5, "binding");
            this.b = kw5;
            this.a = ff5;
            String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d)) {
                this.a.v.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                this.a.q.setBackgroundColor(Color.parseColor(d2));
            }
            this.a.q.setOnClickListener(new Aii(this));
            this.a.u.setOnClickListener(new Bii(this));
        }

        @DexIgnore
        public final void b(AppWrapper appWrapper) {
            Wg6.c(appWrapper, "appWrapper");
            Wj5 b2 = Tj5.b(this.a.t);
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                b2.I(new Qj5(installedApp)).T0(((Fj1) new Fj1().o0(new Hk5())).n()).F0(this.a.t);
                FlexibleTextView flexibleTextView = this.a.s;
                Wg6.b(flexibleTextView, "binding.ftvHybridName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                FlexibleTextView flexibleTextView2 = this.a.r;
                Wg6.b(flexibleTextView2, "binding.ftvHybridFeature");
                flexibleTextView2.setVisibility(8);
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected == null) {
                    Wg6.i();
                    throw null;
                } else if (isSelected.booleanValue()) {
                    this.a.u.setImageResource(2131231155);
                } else {
                    this.a.u.setImageResource(2131231123);
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final void c(J06 j06) {
            Uri uri;
            String str;
            String str2;
            String str3;
            Contact contact;
            String str4;
            Wg6.c(j06, "contactWrapper");
            Contact contact2 = j06.getContact();
            if (!TextUtils.isEmpty(contact2 != null ? contact2.getPhotoThumbUri() : null)) {
                Contact contact3 = j06.getContact();
                uri = Uri.parse(contact3 != null ? contact3.getPhotoThumbUri() : null);
            } else {
                uri = null;
            }
            Contact contact4 = j06.getContact();
            if (contact4 == null || (str = contact4.getFirstName()) == null) {
                str = "";
            }
            Contact contact5 = j06.getContact();
            if (contact5 == null || (str2 = contact5.getLastName()) == null) {
                str2 = "";
            }
            Contact contact6 = j06.getContact();
            if (contact6 == null || contact6.getContactId() != -100) {
                Contact contact7 = j06.getContact();
                if (contact7 == null || contact7.getContactId() != -200) {
                    String str5 = str + " " + str2;
                    Wj5 b2 = Tj5.b(this.a.t);
                    Contact contact8 = j06.getContact();
                    Vj5<Drawable> T0 = b2.I(new Sj5(uri, contact8 != null ? contact8.getDisplayName() : null)).T0(this.b.d);
                    Wj5 b3 = Tj5.b(this.a.t);
                    Contact contact9 = j06.getContact();
                    Wg6.b(T0.a1(b3.I(new Sj5((Uri) null, contact9 != null ? contact9.getDisplayName() : null)).T0(this.b.d)).b1(new Cii(this, j06)).F0(this.a.t), "GlideApp.with(binding.iv\u2026nto(binding.ivHybridIcon)");
                    str3 = str5;
                } else {
                    this.a.t.setImageDrawable(W6.f(PortfolioApp.get.instance(), 2131231113));
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886155);
                    Wg6.b(str3, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
                }
            } else {
                this.a.t.setImageDrawable(W6.f(PortfolioApp.get.instance(), 2131231112));
                str3 = Um5.c(PortfolioApp.get.instance(), 2131886154);
                Wg6.b(str3, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
            }
            FlexibleTextView flexibleTextView = this.a.s;
            Wg6.b(flexibleTextView, "binding.ftvHybridName");
            flexibleTextView.setText(str3);
            Contact contact10 = j06.getContact();
            if ((contact10 == null || contact10.getContactId() != -100) && ((contact = j06.getContact()) == null || contact.getContactId() != -200)) {
                Contact contact11 = j06.getContact();
                Boolean valueOf = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                if (valueOf != null) {
                    if (valueOf.booleanValue()) {
                        Contact contact12 = j06.getContact();
                        Boolean valueOf2 = contact12 != null ? Boolean.valueOf(contact12.isUseCall()) : null;
                        if (valueOf2 == null) {
                            Wg6.i();
                            throw null;
                        } else if (valueOf2.booleanValue()) {
                            str4 = Um5.c(PortfolioApp.get.instance(), 2131886147);
                            Wg6.b(str4, "LanguageHelper.getString\u2026pped_Text__CallsMessages)");
                            FlexibleTextView flexibleTextView2 = this.a.r;
                            Wg6.b(flexibleTextView2, "binding.ftvHybridFeature");
                            flexibleTextView2.setText(str4);
                            FlexibleTextView flexibleTextView3 = this.a.r;
                            Wg6.b(flexibleTextView3, "binding.ftvHybridFeature");
                            flexibleTextView3.setVisibility(0);
                        }
                    }
                    Contact contact13 = j06.getContact();
                    Boolean valueOf3 = contact13 != null ? Boolean.valueOf(contact13.isUseSms()) : null;
                    if (valueOf3 != null) {
                        if (valueOf3.booleanValue()) {
                            str4 = Um5.c(PortfolioApp.get.instance(), 2131886148);
                            Wg6.b(str4, "LanguageHelper.getString\u2026nedTapped_Text__Messages)");
                        } else {
                            str4 = "";
                        }
                        Contact contact14 = j06.getContact();
                        Boolean valueOf4 = contact14 != null ? Boolean.valueOf(contact14.isUseCall()) : null;
                        if (valueOf4 != null) {
                            if (valueOf4.booleanValue()) {
                                str4 = Um5.c(PortfolioApp.get.instance(), 2131886146);
                                Wg6.b(str4, "LanguageHelper.getString\u2026signedTapped_Text__Calls)");
                            }
                            FlexibleTextView flexibleTextView22 = this.a.r;
                            Wg6.b(flexibleTextView22, "binding.ftvHybridFeature");
                            flexibleTextView22.setText(str4);
                            FlexibleTextView flexibleTextView32 = this.a.r;
                            Wg6.b(flexibleTextView32, "binding.ftvHybridFeature");
                            flexibleTextView32.setVisibility(0);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                FlexibleTextView flexibleTextView4 = this.a.r;
                Wg6.b(flexibleTextView4, "binding.ftvHybridFeature");
                flexibleTextView4.setText("");
                FlexibleTextView flexibleTextView5 = this.a.r;
                Wg6.b(flexibleTextView5, "binding.ftvHybridFeature");
                flexibleTextView5.setVisibility(8);
            }
            if (j06.isAdded()) {
                this.a.u.setImageResource(2131231155);
            } else {
                this.a.u.setImageResource(2131231123);
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(J06 j06);

        @DexIgnore
        Object b();  // void declaration
    }

    /*
    static {
        String name = Kw5.class.getName();
        Wg6.b(name, "NotificationContactsAndA\u2026dAdapter::class.java.name");
        e = name;
    }
    */

    @DexIgnore
    public Kw5() {
        Yi1 m0 = ((Fj1) ((Fj1) ((Fj1) new Fj1().o0(new Hk5())).n()).l(Wc1.a)).m0(true);
        Wg6.b(m0, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        this.d = (Fj1) m0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends Object> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final boolean m() {
        return getItemCount() + this.c <= 12;
    }

    @DexIgnore
    public void n(Ai ai, int i) {
        Wg6.c(ai, "holder");
        List<? extends Object> list = this.b;
        if (list != null) {
            Object obj = list.get(i);
            if (obj instanceof J06) {
                ai.c((J06) obj);
            } else if (obj != null) {
                ai.b((AppWrapper) obj);
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ai o(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Ff5 z = Ff5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemNotificationHybridBi\u2026.context), parent, false)");
        return new Ai(this, z);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        n(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return o(viewGroup, i);
    }

    @DexIgnore
    public final void p(List<? extends Object> list) {
        Wg6.c(list, "data");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void q(Bi bi) {
        Wg6.c(bi, "listener");
        this.a = bi;
    }
}
