package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pv3 extends Zc2 implements Yu3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Pv3> CREATOR; // = new Qv3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    public Pv3(String str, String str2, int i, boolean z) {
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = z;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Pv3)) {
            return false;
        }
        return ((Pv3) obj).b.equals(this.b);
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final boolean h() {
        return this.e;
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        String str = this.c;
        String str2 = this.b;
        int i = this.d;
        boolean z = this.e;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 45 + String.valueOf(str2).length());
        sb.append("Node{");
        sb.append(str);
        sb.append(", id=");
        sb.append(str2);
        sb.append(", hops=");
        sb.append(i);
        sb.append(", isNearby=");
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, f(), false);
        Bd2.u(parcel, 3, c(), false);
        Bd2.n(parcel, 4, this.d);
        Bd2.c(parcel, 5, h());
        Bd2.b(parcel, a2);
    }
}
