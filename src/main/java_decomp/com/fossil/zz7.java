package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zz7 {
    @DexIgnore
    public static /* final */ Vz7 a; // = new Vz7("ZERO");
    @DexIgnore
    public static /* final */ Coroutine<Object, Af6.Bi, Object> b; // = Ai.INSTANCE;
    @DexIgnore
    public static /* final */ Coroutine<Vx7<?>, Af6.Bi, Vx7<?>> c; // = Bi.INSTANCE;
    @DexIgnore
    public static /* final */ Coroutine<C08, Af6.Bi, C08> d; // = Di.INSTANCE;
    @DexIgnore
    public static /* final */ Coroutine<C08, Af6.Bi, C08> e; // = Ci.INSTANCE;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Coroutine<Object, Af6.Bi, Object> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(2);
        }

        @DexIgnore
        public final Object invoke(Object obj, Af6.Bi bi) {
            if (!(bi instanceof Vx7)) {
                return obj;
            }
            Integer num = (Integer) (!(obj instanceof Integer) ? null : obj);
            int intValue = num != null ? num.intValue() : 1;
            return intValue == 0 ? bi : Integer.valueOf(intValue + 1);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Object invoke(Object obj, Af6.Bi bi) {
            return invoke(obj, bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Coroutine<Vx7<?>, Af6.Bi, Vx7<?>> {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();

        @DexIgnore
        public Bi() {
            super(2);
        }

        @DexIgnore
        public final Vx7<?> invoke(Vx7<?> vx7, Af6.Bi bi) {
            if (vx7 != null) {
                return vx7;
            }
            return (Vx7) (!(bi instanceof Vx7) ? null : bi);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Vx7<?> invoke(Vx7<?> vx7, Af6.Bi bi) {
            return invoke(vx7, bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Coroutine<C08, Af6.Bi, C08> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(2);
        }

        @DexIgnore
        public final C08 invoke(C08 c08, Af6.Bi bi) {
            if (bi instanceof Vx7) {
                ((Vx7) bi).B(c08.b(), c08.d());
            }
            return c08;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ C08 invoke(C08 c08, Af6.Bi bi) {
            return invoke(c08, bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Coroutine<C08, Af6.Bi, C08> {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();

        @DexIgnore
        public Di() {
            super(2);
        }

        @DexIgnore
        public final C08 invoke(C08 c08, Af6.Bi bi) {
            if (bi instanceof Vx7) {
                c08.a(((Vx7) bi).F(c08.b()));
            }
            return c08;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ C08 invoke(C08 c08, Af6.Bi bi) {
            return invoke(c08, bi);
        }
    }

    @DexIgnore
    public static final void a(Af6 af6, Object obj) {
        if (obj != a) {
            if (obj instanceof C08) {
                ((C08) obj).c();
                af6.fold(obj, e);
                return;
            }
            Object fold = af6.fold(null, c);
            if (fold != null) {
                ((Vx7) fold).B(af6, obj);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final Object b(Af6 af6) {
        Object fold = af6.fold(0, b);
        if (fold != null) {
            return fold;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public static final Object c(Af6 af6, Object obj) {
        Object b2 = obj != null ? obj : b(af6);
        if (b2 == 0) {
            return a;
        }
        if (b2 instanceof Integer) {
            return af6.fold(new C08(af6, ((Number) b2).intValue()), d);
        }
        if (b2 != null) {
            return ((Vx7) b2).F(af6);
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }
}
