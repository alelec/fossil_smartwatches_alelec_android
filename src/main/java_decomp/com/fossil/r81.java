package com.fossil;

import android.content.Context;
import android.view.View;
import com.mapped.Wg6;
import java.io.File;
import okhttp3.Cache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R81 {
    @DexIgnore
    public static final void a(View view) {
        Wg6.c(view, "view");
        W81.i(view).a();
    }

    @DexIgnore
    public static final Cache b(Context context) {
        Wg6.c(context, "context");
        File g = Y81.a.g(context);
        return new Cache(g, Y81.a.c(g));
    }
}
