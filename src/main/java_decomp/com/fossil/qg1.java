package com.fossil;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qg1 extends FilterInputStream {
    @DexIgnore
    public volatile byte[] b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ Od1 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends IOException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = -4338378848813561757L;

        @DexIgnore
        public Ai(String str) {
            super(str);
        }
    }

    @DexIgnore
    public Qg1(InputStream inputStream, Od1 od1) {
        this(inputStream, od1, 65536);
    }

    @DexIgnore
    public Qg1(InputStream inputStream, Od1 od1, int i) {
        super(inputStream);
        this.e = -1;
        this.g = od1;
        this.b = (byte[]) od1.g(i, byte[].class);
    }

    @DexIgnore
    public static IOException f() throws IOException {
        throw new IOException("BufferedInputStream is closed");
    }

    @DexIgnore
    public final int a(InputStream inputStream, byte[] bArr) throws IOException {
        int i = this.e;
        if (i != -1) {
            int i2 = this.f;
            int i3 = this.d;
            if (i2 - i < i3) {
                if (i == 0 && i3 > bArr.length && this.c == bArr.length) {
                    int length = bArr.length * 2;
                    if (length <= i3) {
                        i3 = length;
                    }
                    byte[] bArr2 = (byte[]) this.g.g(i3, byte[].class);
                    System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                    this.b = bArr2;
                    this.g.f(bArr);
                    bArr = bArr2;
                } else {
                    int i4 = this.e;
                    if (i4 > 0) {
                        System.arraycopy(bArr, i4, bArr, 0, bArr.length - i4);
                    }
                }
                int i5 = this.f - this.e;
                this.f = i5;
                this.e = 0;
                this.c = 0;
                int read = inputStream.read(bArr, i5, bArr.length - i5);
                int i6 = this.f;
                if (read > 0) {
                    i6 += read;
                }
                this.c = i6;
                return read;
            }
        }
        int read2 = inputStream.read(bArr);
        if (read2 <= 0) {
            return read2;
        }
        this.e = -1;
        this.f = 0;
        this.c = read2;
        return read2;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int available() throws IOException {
        int i;
        int i2;
        int available;
        synchronized (this) {
            InputStream inputStream = ((FilterInputStream) this).in;
            if (this.b == null || inputStream == null) {
                f();
                throw null;
            }
            i = this.c;
            i2 = this.f;
            available = inputStream.available();
        }
        return available + (i - i2);
    }

    @DexIgnore
    public void b() {
        synchronized (this) {
            this.d = this.b.length;
        }
    }

    @DexIgnore
    public void c() {
        synchronized (this) {
            if (this.b != null) {
                this.g.f(this.b);
                this.b = null;
            }
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
    public void close() throws IOException {
        if (this.b != null) {
            this.g.f(this.b);
            this.b = null;
        }
        InputStream inputStream = ((FilterInputStream) this).in;
        ((FilterInputStream) this).in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    @DexIgnore
    public void mark(int i) {
        synchronized (this) {
            this.d = Math.max(this.d, i);
            this.e = this.f;
        }
    }

    @DexIgnore
    public boolean markSupported() {
        return true;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        synchronized (this) {
            byte[] bArr = this.b;
            InputStream inputStream = ((FilterInputStream) this).in;
            if (bArr == null || inputStream == null) {
                f();
                throw null;
            } else if (this.f >= this.c && a(inputStream, bArr) == -1) {
                return -1;
            } else {
                if (bArr != this.b && (bArr = this.b) == null) {
                    f();
                    throw null;
                } else if (this.c - this.f <= 0) {
                    return -1;
                } else {
                    int i = this.f;
                    this.f = i + 1;
                    return bArr[i] & 255;
                }
            }
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        int i5 = -1;
        synchronized (this) {
            byte[] bArr2 = this.b;
            if (bArr2 == null) {
                f();
                throw null;
            } else if (i2 == 0) {
                return 0;
            } else {
                InputStream inputStream = ((FilterInputStream) this).in;
                if (inputStream != null) {
                    if (this.f < this.c) {
                        int i6 = this.c - this.f >= i2 ? i2 : this.c - this.f;
                        System.arraycopy(bArr2, this.f, bArr, i, i6);
                        this.f += i6;
                        if (i6 == i2 || inputStream.available() == 0) {
                            return i6;
                        }
                        i += i6;
                        i3 = i2 - i6;
                    } else {
                        i3 = i2;
                    }
                    while (true) {
                        if (this.e == -1 && i3 >= bArr2.length) {
                            i4 = inputStream.read(bArr, i, i3);
                            if (i4 == -1) {
                                if (i3 != i2) {
                                    i5 = i2 - i3;
                                }
                                return i5;
                            }
                        } else if (a(inputStream, bArr2) == -1) {
                            if (i3 != i2) {
                                i5 = i2 - i3;
                            }
                            return i5;
                        } else if (bArr2 == this.b || (bArr2 = this.b) != null) {
                            i4 = this.c - this.f >= i3 ? i3 : this.c - this.f;
                            System.arraycopy(bArr2, this.f, bArr, i, i4);
                            this.f += i4;
                        } else {
                            f();
                            throw null;
                        }
                        i3 -= i4;
                        if (i3 == 0) {
                            return i2;
                        }
                        if (inputStream.available() == 0) {
                            return i2 - i3;
                        }
                        i += i4;
                    }
                } else {
                    f();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public void reset() throws IOException {
        synchronized (this) {
            if (this.b == null) {
                throw new IOException("Stream is closed");
            } else if (-1 != this.e) {
                this.f = this.e;
            } else {
                throw new Ai("Mark has been invalidated, pos: " + this.f + " markLimit: " + this.d);
            }
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        synchronized (this) {
            if (j < 1) {
                return 0;
            }
            byte[] bArr = this.b;
            if (bArr != null) {
                InputStream inputStream = ((FilterInputStream) this).in;
                if (inputStream == null) {
                    f();
                    throw null;
                } else if (((long) (this.c - this.f)) >= j) {
                    this.f = (int) (((long) this.f) + j);
                    return j;
                } else {
                    long j2 = ((long) this.c) - ((long) this.f);
                    this.f = this.c;
                    if (this.e == -1 || j > ((long) this.d)) {
                        return j2 + inputStream.skip(j - j2);
                    } else if (a(inputStream, bArr) == -1) {
                        return j2;
                    } else {
                        if (((long) (this.c - this.f)) >= j - j2) {
                            this.f = (int) ((((long) this.f) + j) - j2);
                            return j;
                        }
                        long j3 = (long) this.c;
                        long j4 = (long) this.f;
                        this.f = this.c;
                        return (j2 + j3) - j4;
                    }
                }
            } else {
                f();
                throw null;
            }
        }
    }
}
