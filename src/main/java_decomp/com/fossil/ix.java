package com.fossil;

import com.mapped.Cd6;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ix {
    @DexIgnore
    public static /* final */ Hashtable<String, Hx> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Ix b; // = new Ix();

    @DexIgnore
    public final Hx a(String str) {
        Hx hx;
        synchronized (a) {
            hx = a.get(str);
            if (hx == null) {
                hx = new Hx();
            }
            a.put(str, hx);
        }
        return hx;
    }

    @DexIgnore
    public final void b(String str, byte[] bArr) {
        synchronized (a) {
            b.a(str).a = bArr;
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void c(String str, byte[] bArr, byte[] bArr2) {
        synchronized (a) {
            b.a(str).b(bArr, bArr2);
            Cd6 cd6 = Cd6.a;
        }
    }
}
