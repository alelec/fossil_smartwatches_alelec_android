package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W1 implements Parcelable.Creator<Y1> {
    @DexIgnore
    public /* synthetic */ W1(Qg6 qg6) {
    }

    @DexIgnore
    public Y1 a(Parcel parcel) {
        return new Y1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Y1 createFromParcel(Parcel parcel) {
        return new Y1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Y1[] newArray(int i) {
        return new Y1[i];
    }
}
