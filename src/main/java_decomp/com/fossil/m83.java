package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M83 implements Xw2<L83> {
    @DexIgnore
    public static M83 c; // = new M83();
    @DexIgnore
    public /* final */ Xw2<L83> b;

    @DexIgnore
    public M83() {
        this(Ww2.b(new O83()));
    }

    @DexIgnore
    public M83(Xw2<L83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((L83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ L83 zza() {
        return this.b.zza();
    }
}
