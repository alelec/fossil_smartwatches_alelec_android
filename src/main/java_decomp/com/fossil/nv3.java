package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nv3 extends Zc2 implements Xu3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Nv3> CREATOR; // = new Ov3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public Nv3(int i, String str, byte[] bArr, String str2) {
        this.b = i;
        this.c = str;
        this.d = bArr;
        this.e = str2;
    }

    @DexIgnore
    public final byte[] c() {
        return this.d;
    }

    @DexIgnore
    public final int f() {
        return this.b;
    }

    @DexIgnore
    public final String getPath() {
        return this.c;
    }

    @DexIgnore
    public final String h() {
        return this.e;
    }

    @DexIgnore
    public final String toString() {
        int i = this.b;
        String str = this.c;
        byte[] bArr = this.d;
        String valueOf = String.valueOf(bArr == null ? "null" : Integer.valueOf(bArr.length));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length());
        sb.append("MessageEventParcelable[");
        sb.append(i);
        sb.append(",");
        sb.append(str);
        sb.append(", size=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 2, f());
        Bd2.u(parcel, 3, getPath(), false);
        Bd2.g(parcel, 4, c(), false);
        Bd2.u(parcel, 5, h(), false);
        Bd2.b(parcel, a2);
    }
}
