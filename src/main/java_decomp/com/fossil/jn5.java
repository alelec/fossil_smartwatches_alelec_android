package com.fossil;

import android.content.Context;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jn5 {
    @DexIgnore
    public static /* final */ HashMap<Ai, ArrayList<Ci>> a;
    @DexIgnore
    public static /* final */ Jn5 b; // = new Jn5();

    @DexIgnore
    public enum Ai {
        SET_BLE_COMMAND,
        PAIR_DEVICE,
        NOTIFICATION_HYBRID,
        NOTIFICATION_DIANA,
        NOTIFICATION_GET_CONTACTS,
        NOTIFICATION_APPS,
        SET_COMPLICATION_WATCH_APP_WEATHER,
        SET_COMPLICATION_CHANCE_OF_RAIN,
        SET_WATCH_APP_MUSIC,
        SET_WATCH_APP_COMMUTE_TIME,
        FIND_DEVICE,
        EDIT_AVATAR,
        UPDATE_FIRMWARE,
        SET_MICRO_APP_MUSIC,
        SET_MICRO_APP_COMMUTE_TIME,
        GET_USER_LOCATION,
        CAPTURE_IMAGE,
        QUICK_RESPONSE
    }

    @DexIgnore
    public enum Bi {
        LOCATION_GROUP,
        SMS_GROUP,
        PHONE_GROUP
    }

    @DexIgnore
    public enum Ci {
        BLUETOOTH_CONNECTION,
        NOTIFICATION_ACCESS,
        LOCATION_FINE,
        LOCATION_SERVICE,
        LOCATION_BACKGROUND,
        READ_CONTACTS,
        READ_PHONE_STATE,
        READ_CALL_LOG,
        READ_SMS,
        RECEIVE_SMS,
        RECEIVE_MMS,
        CAMERA,
        READ_EXTERNAL_STORAGE,
        CALL_PHONE,
        ANSWER_PHONE_CALL,
        SEND_SMS
    }

    /*
    static {
        HashMap<Ai, ArrayList<Ci>> hashMap = new HashMap<>();
        hashMap.put(Ai.SET_BLE_COMMAND, Hm7.c(Ci.BLUETOOTH_CONNECTION));
        hashMap.put(Ai.PAIR_DEVICE, Hm7.c(Ci.BLUETOOTH_CONNECTION, Ci.LOCATION_FINE, Ci.LOCATION_SERVICE));
        hashMap.put(Ai.UPDATE_FIRMWARE, Hm7.c(Ci.BLUETOOTH_CONNECTION, Ci.LOCATION_FINE, Ci.LOCATION_SERVICE));
        hashMap.put(Ai.FIND_DEVICE, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE));
        hashMap.put(Ai.NOTIFICATION_DIANA, Hm7.c(Ci.READ_CONTACTS, Ci.READ_PHONE_STATE, Ci.READ_CALL_LOG, Ci.CALL_PHONE, Ci.ANSWER_PHONE_CALL, Ci.READ_SMS, Ci.RECEIVE_SMS, Ci.RECEIVE_MMS, Ci.NOTIFICATION_ACCESS));
        hashMap.put(Ai.NOTIFICATION_HYBRID, Hm7.c(Ci.READ_CONTACTS, Ci.READ_PHONE_STATE, Ci.READ_CALL_LOG, Ci.READ_SMS, Ci.RECEIVE_SMS, Ci.RECEIVE_MMS, Ci.NOTIFICATION_ACCESS));
        hashMap.put(Ai.NOTIFICATION_GET_CONTACTS, Hm7.c(Ci.READ_CONTACTS));
        hashMap.put(Ai.NOTIFICATION_APPS, Hm7.c(Ci.NOTIFICATION_ACCESS));
        hashMap.put(Ai.QUICK_RESPONSE, Hm7.c(Ci.SEND_SMS));
        hashMap.put(Ai.SET_COMPLICATION_WATCH_APP_WEATHER, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE, Ci.LOCATION_BACKGROUND));
        hashMap.put(Ai.SET_COMPLICATION_CHANCE_OF_RAIN, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE, Ci.LOCATION_BACKGROUND));
        hashMap.put(Ai.SET_WATCH_APP_MUSIC, Hm7.c(Ci.NOTIFICATION_ACCESS));
        hashMap.put(Ai.SET_WATCH_APP_COMMUTE_TIME, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE, Ci.LOCATION_BACKGROUND));
        hashMap.put(Ai.SET_MICRO_APP_MUSIC, Hm7.c(Ci.NOTIFICATION_ACCESS));
        hashMap.put(Ai.SET_MICRO_APP_COMMUTE_TIME, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE, Ci.LOCATION_BACKGROUND));
        hashMap.put(Ai.EDIT_AVATAR, Hm7.c(Ci.CAMERA));
        hashMap.put(Ai.GET_USER_LOCATION, Hm7.c(Ci.LOCATION_FINE, Ci.LOCATION_SERVICE));
        hashMap.put(Ai.CAPTURE_IMAGE, Hm7.c(Ci.CAMERA));
        a = hashMap;
    }
    */

    @DexIgnore
    public static /* synthetic */ boolean c(Jn5 jn5, Context context, Ai ai, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = false;
        boolean z5 = (i & 4) != 0 ? true : z;
        boolean z6 = (i & 8) != 0 ? false : z2;
        if ((i & 16) == 0) {
            z4 = z3;
        }
        return jn5.b(context, ai, z5, z6, z4, (i & 32) != 0 ? null : num);
    }

    @DexIgnore
    public static /* synthetic */ boolean f(Jn5 jn5, Context context, List list, List list2, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        return jn5.d(context, (i & 2) != 0 ? null : list, list2, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3, (i & 64) != 0 ? null : num);
    }

    @DexIgnore
    public static /* synthetic */ boolean g(Jn5 jn5, Context context, List list, boolean z, boolean z2, boolean z3, Integer num, int i, Object obj) {
        boolean z4 = false;
        boolean z5 = (i & 4) != 0 ? true : z;
        boolean z6 = (i & 8) != 0 ? false : z2;
        if ((i & 16) == 0) {
            z4 = z3;
        }
        return jn5.e(context, list, z5, z6, z4, (i & 32) != 0 ? null : num);
    }

    @DexIgnore
    public final void a(Context context, ArrayList<Uh5> arrayList, List<Ai> list) {
        Wg6.c(arrayList, "errorCodes");
        Wg6.c(list, "features");
        ArrayList<Ci> arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            int i = Kn5.a[it.next().ordinal()];
            if (i == 1) {
                arrayList2.add(Ci.BLUETOOTH_CONNECTION);
            } else if (i == 2) {
                arrayList2.add(Ci.LOCATION_BACKGROUND);
            } else if (i == 3) {
                arrayList2.add(Ci.LOCATION_FINE);
            } else if (i == 4) {
                arrayList2.add(Ci.LOCATION_SERVICE);
            }
        }
        for (Ci ci : arrayList2) {
            Gl7<String, String, String> l = b.l(ci, list);
            String component1 = l.component1();
            String component2 = l.component2();
            String component3 = l.component3();
            arrayList3.add(new PermissionData("PERMISSION_REQUEST_TYPE", b.j(ci), ci, b.i(ci), component1, component2, component3 != null ? component3 : "", false));
        }
        if ((!arrayList2.isEmpty()) && context != null) {
            PermissionActivity.a.b(PermissionActivity.B, context, arrayList3, false, false, null, 28, null);
        }
    }

    @DexIgnore
    public final boolean b(Context context, Ai ai, boolean z, boolean z2, boolean z3, Integer num) {
        if (ai == null) {
            return true;
        }
        return b.h(context, null, Hm7.i(ai), z, z2, z3, num);
    }

    @DexIgnore
    public final boolean d(Context context, List<String> list, List<? extends Ai> list2, boolean z, boolean z2, boolean z3, Integer num) {
        Wg6.c(list2, "features");
        return h(context, list, list2, z, z2, z3, num);
    }

    @DexIgnore
    public final boolean e(Context context, List<? extends Ai> list, boolean z, boolean z2, boolean z3, Integer num) {
        Wg6.c(list, "features");
        return h(context, null, list, z, z2, z3, num);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: com.portfolio.platform.uirenew.permission.PermissionActivity$a */
    /* JADX WARN: Multi-variable type inference failed */
    public final boolean h(Context context, List<String> list, List<? extends Ai> list2, boolean z, boolean z2, boolean z3, Integer num) {
        WeakReference weakReference = new WeakReference(context);
        HashMap hashMap = new HashMap();
        for (T t : list2) {
            List<Ci> k = b.k(t);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PermissionManager", "feature " + ((Object) t) + " appPermissionIdsNotGranted " + k);
            for (T t2 : k) {
                List list3 = (List) hashMap.get(t2);
                if (list3 != null) {
                    list3.add(t);
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(t);
                    hashMap.put(t2, arrayList);
                    Cd6 cd6 = Cd6.a;
                }
            }
        }
        if (!(!hashMap.isEmpty())) {
            if (list == null || list.isEmpty()) {
                return true;
            }
            Context context2 = (Context) weakReference.get();
            if (context2 != null) {
                ArrayList<PermissionData> arrayList2 = new ArrayList<>();
                Iterator<T> it = list.iterator();
                while (it.hasNext()) {
                    arrayList2.add(new PermissionData("PERMISSION_SETTING_TYPE", new ArrayList(), null, "", it.next(), "", "", false));
                }
                if (!arrayList2.isEmpty()) {
                    PermissionActivity.a aVar = PermissionActivity.B;
                    Wg6.b(context2, "it");
                    aVar.a(context2, arrayList2, z3, z2, num);
                }
                Cd6 cd62 = Cd6.a;
            }
            return false;
        } else if (!z) {
            return false;
        } else {
            ArrayList arrayList3 = new ArrayList();
            Set<Ci> keySet = hashMap.keySet();
            Wg6.b(keySet, "permissionUsedByFeaturesMap.keys");
            for (Ci ci : keySet) {
                Jn5 jn5 = b;
                Object obj = hashMap.get(ci);
                if (obj != null) {
                    Gl7<String, String, String> l = jn5.l(ci, (List) obj);
                    String component1 = l.component1();
                    String component2 = l.component2();
                    String component3 = l.component3();
                    Jn5 jn52 = b;
                    Wg6.b(ci, "appPermissionId");
                    arrayList3.add(new PermissionData("PERMISSION_REQUEST_TYPE", jn52.j(ci), ci, b.i(ci), component1, component2, component3 != null ? component3 : "", false));
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            Context context3 = (Context) weakReference.get();
            if (context3 != null) {
                ArrayList arrayList4 = new ArrayList();
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Object obj2 : arrayList3) {
                    String permissionGroup = ((PermissionData) obj2).getPermissionGroup();
                    Object obj3 = linkedHashMap.get(permissionGroup);
                    if (obj3 == null) {
                        obj3 = new ArrayList();
                        linkedHashMap.put(permissionGroup, obj3);
                    }
                    ((List) obj3).add(obj2);
                }
                Collection<List> values = linkedHashMap.values();
                Wg6.b(values, "permissionDatasGroupByPermissionGroup.values");
                for (List<PermissionData> list4 : values) {
                    Wg6.b(list4, "sameGroupPermissionDataList");
                    if (!list4.isEmpty()) {
                        ArrayList<String> arrayList5 = new ArrayList<>();
                        for (PermissionData permissionData : list4) {
                            arrayList5.addAll(permissionData.getAndroidPermissionSet());
                        }
                        ((PermissionData) list4.get(0)).setAndroidPermissionSet(arrayList5);
                        arrayList4.add(list4.get(0));
                    }
                }
                if (list != null) {
                    Iterator<T> it2 = list.iterator();
                    while (it2.hasNext()) {
                        arrayList4.add(new PermissionData("PERMISSION_SETTING_TYPE", new ArrayList(), null, "", it2.next(), "", "", false));
                    }
                    Cd6 cd63 = Cd6.a;
                }
                PermissionActivity.a aVar2 = PermissionActivity.B;
                Wg6.b(context3, "it");
                aVar2.a(context3, arrayList4, z3, z2, num);
                Cd6 cd64 = Cd6.a;
            }
            return false;
        }
    }

    @DexIgnore
    public final String i(Ci ci) {
        switch (Kn5.b[ci.ordinal()]) {
            case 1:
            case 2:
                return Bi.LOCATION_GROUP.name();
            case 3:
            case 4:
            case 5:
            case 6:
                return Bi.PHONE_GROUP.name();
            case 7:
            case 8:
            case 9:
                return Bi.SMS_GROUP.name();
            default:
                return ci.name();
        }
    }

    @DexIgnore
    public final ArrayList<String> j(Ci ci) {
        ArrayList<String> arrayList = new ArrayList<>();
        switch (Kn5.d[ci.ordinal()]) {
            case 1:
                arrayList.add("android.permission.ACCESS_FINE_LOCATION");
                break;
            case 2:
                if (DeviceHelper.o.s()) {
                    arrayList.add("android.permission.ACCESS_BACKGROUND_LOCATION");
                    break;
                }
                break;
            case 3:
                arrayList.add("android.permission.READ_CONTACTS");
                break;
            case 4:
                arrayList.add("android.permission.READ_PHONE_STATE");
                break;
            case 5:
                arrayList.add("android.permission.READ_CALL_LOG");
                break;
            case 6:
                if (DeviceHelper.o.z() && DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                    arrayList.add("android.permission.CALL_PHONE");
                    break;
                }
            case 7:
                if (DeviceHelper.o.z() && DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                    arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                    break;
                }
            case 8:
                arrayList.add("android.permission.READ_SMS");
                break;
            case 9:
                arrayList.add("android.permission.RECEIVE_SMS");
                break;
            case 10:
                arrayList.add("android.permission.RECEIVE_MMS");
                break;
            case 11:
                if (DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                    arrayList.add("android.permission.SEND_SMS");
                    break;
                }
                break;
            case 12:
                arrayList.add("android.permission.CAMERA");
                break;
            case 13:
                arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                break;
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Ci> k(Ai ai) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "Check permission is granted or not for feature " + ai);
        ArrayList arrayList = new ArrayList();
        PortfolioApp instance = PortfolioApp.get.instance();
        for (Map.Entry<Ai, ArrayList<Ci>> entry : a.entrySet()) {
            if (entry.getKey() == ai) {
                Iterator<T> it = entry.getValue().iterator();
                while (it.hasNext()) {
                    switch (Kn5.c[it.next().ordinal()]) {
                        case 1:
                            if (!BluetoothUtils.isBluetoothEnable()) {
                                arrayList.add(Ci.BLUETOOTH_CONNECTION);
                                break;
                            } else {
                                break;
                            }
                        case 2:
                            if (!LocationUtils.isLocationPermissionGranted(instance)) {
                                arrayList.add(Ci.LOCATION_FINE);
                                break;
                            } else {
                                break;
                            }
                        case 3:
                            if (DeviceHelper.o.s() && !LocationUtils.isBackgroundLocationPermissionGranted(instance)) {
                                arrayList.add(Ci.LOCATION_BACKGROUND);
                                break;
                            }
                        case 4:
                            if (!LocationUtils.isLocationEnable(instance)) {
                                arrayList.add(Ci.LOCATION_SERVICE);
                                break;
                            } else {
                                break;
                            }
                        case 5:
                            if (!V78.a(instance, "android.permission.READ_CONTACTS")) {
                                arrayList.add(Ci.READ_CONTACTS);
                                break;
                            } else {
                                break;
                            }
                        case 6:
                            if (!V78.a(instance, "android.permission.READ_PHONE_STATE")) {
                                arrayList.add(Ci.READ_PHONE_STATE);
                                break;
                            } else {
                                break;
                            }
                        case 7:
                            if (!V78.a(instance, "android.permission.READ_CALL_LOG")) {
                                arrayList.add(Ci.READ_CALL_LOG);
                                break;
                            } else {
                                break;
                            }
                        case 8:
                            if (!V78.a(instance, "android.permission.CALL_PHONE")) {
                                arrayList.add(Ci.CALL_PHONE);
                                break;
                            } else {
                                break;
                            }
                        case 9:
                            if (DeviceHelper.o.z()) {
                                if (!V78.a(instance, "android.permission.ANSWER_PHONE_CALLS")) {
                                    arrayList.add(Ci.ANSWER_PHONE_CALL);
                                    break;
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        case 10:
                            if (!V78.a(instance, "android.permission.READ_SMS")) {
                                arrayList.add(Ci.READ_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 11:
                            if (!V78.a(instance, "android.permission.RECEIVE_SMS")) {
                                arrayList.add(Ci.RECEIVE_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 12:
                            if (!V78.a(instance, "android.permission.RECEIVE_MMS")) {
                                arrayList.add(Ci.RECEIVE_MMS);
                                break;
                            } else {
                                break;
                            }
                        case 13:
                            if (!V78.a(instance, "android.permission.SEND_SMS")) {
                                arrayList.add(Ci.SEND_SMS);
                                break;
                            } else {
                                break;
                            }
                        case 14:
                            if (!PortfolioApp.get.instance().y0()) {
                                arrayList.add(Ci.NOTIFICATION_ACCESS);
                                break;
                            } else {
                                break;
                            }
                        case 15:
                            if (!V78.a(instance, "android.permission.CAMERA")) {
                                arrayList.add(Ci.CAMERA);
                                break;
                            } else {
                                break;
                            }
                        case 16:
                            if (!V78.a(instance, "android.permission.READ_EXTERNAL_STORAGE")) {
                                arrayList.add(Ci.READ_EXTERNAL_STORAGE);
                                break;
                            } else {
                                break;
                            }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Gl7<String, String, String> l(Ci ci, List<Ai> list) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String c;
        if (list != null) {
            int size = list.size();
            str = "";
            z = false;
            int i = 0;
            while (true) {
                if (i < size) {
                    switch (Kn5.e[list.get(i).ordinal()]) {
                        case 1:
                            c = Um5.c(PortfolioApp.get.instance(), 2131886400);
                            Wg6.b(c, "LanguageHelper.getString\u2026naWeather_Title__Weather)");
                            break;
                        case 2:
                        case 3:
                            c = Um5.c(PortfolioApp.get.instance(), 2131886408);
                            Wg6.b(c, "LanguageHelper.getString\u2026eTime_Title__CommuteTime)");
                            break;
                        case 4:
                            c = Um5.c(PortfolioApp.get.instance(), 2131886476);
                            Wg6.b(c, "LanguageHelper.getString\u2026Rain_Title__ChanceOfRain)");
                            break;
                        case 5:
                            c = Um5.c(PortfolioApp.get.instance(), 2131887157);
                            Wg6.b(c, "LanguageHelper.getString\u2026Device_Title__FindDevice)");
                            break;
                        case 6:
                            c = Um5.c(PortfolioApp.get.instance(), 2131887605);
                            Wg6.b(c, "LanguageHelper.getString\u2026sion_feature_pair_device)");
                            break;
                        case 7:
                        case 8:
                            c = "";
                            z = true;
                            break;
                        default:
                            c = "";
                            break;
                    }
                    if (!(str.length() == 0)) {
                        if (!Wt7.v(str, c, false, 2, null)) {
                            c = str + Ym5.b + c;
                        } else {
                            c = str;
                        }
                    }
                    if (i == list.size() - 1) {
                        str = c;
                    } else {
                        i++;
                        str = c;
                    }
                }
            }
        } else {
            z = false;
            str = "";
        }
        if (ci != null) {
            switch (Kn5.f[ci.ordinal()]) {
                case 1:
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886929);
                    Wg6.b(str3, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str4 = Um5.c(PortfolioApp.get.instance(), 2131886929);
                    Wg6.b(str4, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str2 = null;
                    break;
                case 2:
                case 3:
                    if (list == null) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886927);
                        Wg6.b(str3, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886927);
                        Wg6.b(str4, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    } else if (list.get(0) == Ai.PAIR_DEVICE) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886932);
                        Wg6.b(str3, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886932);
                        Wg6.b(str4, "LanguageHelper.getString\u2026PermissionThisTimeToScan)");
                    } else {
                        Hr7 hr7 = Hr7.a;
                        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886926);
                        Wg6.b(c2, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        str3 = String.format(c2, Arrays.copyOf(new Object[]{str}, 1));
                        Wg6.b(str3, "java.lang.String.format(format, *args)");
                        Hr7 hr72 = Hr7.a;
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886926);
                        Wg6.b(c3, "LanguageHelper.getString\u2026wPermissionAllTheTimeFor)");
                        str4 = String.format(c3, Arrays.copyOf(new Object[]{str}, 1));
                        Wg6.b(str4, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a2 = Um5.a();
                    Wg6.b(a2, "LanguageHelper.getLocale()");
                    sb.append(a2.getLanguage());
                    str2 = sb.toString();
                    break;
                case 4:
                    if (list == null) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886808);
                        Wg6.b(str3, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886808);
                        Wg6.b(str4, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                    } else if (list.get(0) == Ai.PAIR_DEVICE) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886930);
                        Wg6.b(str3, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886930);
                        Wg6.b(str4, "LanguageHelper.getString\u2026nLocationServicesInOrder)");
                    } else {
                        Hr7 hr73 = Hr7.a;
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886940);
                        Wg6.b(c4, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str3 = String.format(c4, Arrays.copyOf(new Object[]{str}, 1));
                        Wg6.b(str3, "java.lang.String.format(format, *args)");
                        Hr7 hr74 = Hr7.a;
                        String c5 = Um5.c(PortfolioApp.get.instance(), 2131886940);
                        Wg6.b(c5, "LanguageHelper.getString\u2026rnOnLocationServiceSoThe)");
                        str4 = String.format(c5, Arrays.copyOf(new Object[]{str}, 1));
                        Wg6.b(str4, "java.lang.String.format(format, *args)");
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a3 = Um5.a();
                    Wg6.b(a3, "LanguageHelper.getLocale()");
                    sb2.append(a3.getLanguage());
                    str2 = sb2.toString();
                    break;
                case 5:
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886936);
                    Wg6.b(str3, "LanguageHelper.getString\u2026llowTheAppToReadContacts)");
                    Hr7 hr75 = Hr7.a;
                    String c6 = Um5.c(PortfolioApp.get.instance(), 2131886801);
                    Wg6.b(c6, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    str4 = String.format(c6, Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q()}, 1));
                    Wg6.b(str4, "java.lang.String.format(format, *args)");
                    str2 = null;
                    break;
                case 6:
                case 7:
                case 8:
                case 9:
                    if (!DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886937);
                        Wg6.b(str3, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886937);
                        Wg6.b(str4, "LanguageHelper.getString\u2026__AllowTheAppToReadPhone)");
                        str2 = null;
                        break;
                    } else {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886935);
                        Wg6.b(str3, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886935);
                        Wg6.b(str4, "LanguageHelper.getString\u2026AllowTheAppToAnswerPhone)");
                        str2 = null;
                        break;
                    }
                case 10:
                case 11:
                case 12:
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886938);
                    Wg6.b(str3, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str4 = Um5.c(PortfolioApp.get.instance(), 2131886938);
                    Wg6.b(str4, "LanguageHelper.getString\u2026s___AllowTheAppToReadSms)");
                    str2 = null;
                    break;
                case 13:
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886939);
                    Wg6.b(str3, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str4 = Um5.c(PortfolioApp.get.instance(), 2131886939);
                    Wg6.b(str4, "LanguageHelper.getString\u2026s___AllowTheAppToSendSms)");
                    str2 = null;
                    break;
                case 14:
                    if (!z) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886933);
                        Wg6.b(str3, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886933);
                        Wg6.b(str4, "LanguageHelper.getString\u2026ns___AllowTheAppToAccess)");
                        str2 = null;
                        break;
                    } else {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131886934);
                        Wg6.b(str3, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131886934);
                        Wg6.b(str4, "LanguageHelper.getString\u2026___AllowTheAppToAccess_1)");
                        str2 = null;
                        break;
                    }
                case 15:
                    Hr7 hr76 = Hr7.a;
                    String c7 = Um5.c(PortfolioApp.get.instance(), 2131886800);
                    Wg6.b(c7, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str3 = String.format(c7, Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q()}, 1));
                    Wg6.b(str3, "java.lang.String.format(format, *args)");
                    Hr7 hr77 = Hr7.a;
                    String c8 = Um5.c(PortfolioApp.get.instance(), 2131886800);
                    Wg6.b(c8, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    str4 = String.format(c8, Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q()}, 1));
                    Wg6.b(str4, "java.lang.String.format(format, *args)");
                    str2 = null;
                    break;
                case 16:
                    str3 = Um5.c(PortfolioApp.get.instance(), 2131886865);
                    Wg6.b(str3, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str4 = Um5.c(PortfolioApp.get.instance(), 2131886865);
                    Wg6.b(str4, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str2 = null;
                    break;
            }
            return new Gl7<>(str3, str4, str2);
        }
        str4 = "";
        str2 = null;
        str3 = "";
        return new Gl7<>(str3, str4, str2);
    }

    @DexIgnore
    public final boolean m(Context context, Ci ci) {
        Wg6.c(ci, "permissionId");
        WeakReference weakReference = new WeakReference(context);
        switch (Kn5.h[ci.ordinal()]) {
            case 1:
                return BluetoothUtils.isBluetoothEnable();
            case 2:
                Context context2 = (Context) weakReference.get();
                if (context2 != null) {
                    return LocationUtils.isLocationPermissionGranted(context2);
                }
                break;
            case 3:
                Context context3 = (Context) weakReference.get();
                if (context3 != null) {
                    return LocationUtils.isLocationEnable(context3);
                }
                break;
            case 4:
                Context context4 = (Context) weakReference.get();
                if (context4 != null) {
                    return V78.a(context4, "android.permission.READ_CONTACTS");
                }
                break;
            case 5:
                Context context5 = (Context) weakReference.get();
                if (context5 != null) {
                    return V78.a(context5, "android.permission.READ_PHONE_STATE");
                }
                break;
            case 6:
                Context context6 = (Context) weakReference.get();
                if (context6 != null) {
                    return V78.a(context6, "android.permission.READ_CALL_LOG");
                }
                break;
            case 7:
                Context context7 = (Context) weakReference.get();
                if (context7 != null) {
                    return V78.a(context7, "android.permission.CALL_PHONE");
                }
                break;
            case 8:
                Context context8 = (Context) weakReference.get();
                if (context8 != null) {
                    return V78.a(context8, "android.permission.ANSWER_PHONE_CALLS");
                }
                break;
            case 9:
                Context context9 = (Context) weakReference.get();
                if (context9 != null) {
                    return V78.a(context9, "android.permission.READ_SMS");
                }
                break;
            case 10:
                Context context10 = (Context) weakReference.get();
                if (context10 != null) {
                    return V78.a(context10, "android.permission.RECEIVE_SMS");
                }
                break;
            case 11:
                Context context11 = (Context) weakReference.get();
                if (context11 != null) {
                    return V78.a(context11, "android.permission.RECEIVE_MMS");
                }
                break;
            case 12:
                Context context12 = (Context) weakReference.get();
                if (context12 != null) {
                    return V78.a(context12, "android.permission.CAMERA");
                }
                break;
            case 13:
                Context context13 = (Context) weakReference.get();
                if (context13 != null) {
                    return V78.a(context13, "android.permission.READ_EXTERNAL_STORAGE");
                }
                break;
            case 14:
                return PortfolioApp.get.instance().y0();
            case 15:
                if (DeviceHelper.o.s()) {
                    return LocationUtils.isBackgroundLocationPermissionGranted(context);
                }
                return true;
            case 16:
                Context context14 = (Context) weakReference.get();
                if (context14 != null) {
                    return V78.a(context14, "android.permission.SEND_SMS");
                }
                break;
        }
        return false;
    }

    @DexIgnore
    public final boolean n(Ci ci) {
        Wg6.c(ci, "permission");
        int i = Kn5.g[ci.ordinal()];
        if (i == 1) {
            return PortfolioApp.get.instance().y0();
        }
        if (i == 2) {
            return BluetoothUtils.isBluetoothEnable();
        }
        if (i != 3) {
            return true;
        }
        return LocationUtils.isLocationEnable(PortfolioApp.get.instance());
    }
}
