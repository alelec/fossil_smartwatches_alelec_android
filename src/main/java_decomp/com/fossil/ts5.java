package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Er4;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ts5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public int a;
    @DexIgnore
    public List<? extends Er4> b;
    @DexIgnore
    public Ei c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ Ts5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<Er4> h = this.b.b.h();
                    if (h != null) {
                        Er4 er4 = h.get(adapterPosition);
                        Ei g = this.b.b.g();
                        if (g != null) {
                            g.a(er4.a(), this.b.b.i(), adapterPosition, er4.b(), null);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<Er4> h = this.b.b.h();
                if (h != null) {
                    Er4 er4 = h.get(adapterPosition);
                    Ei g = this.b.b.g();
                    if (g == null) {
                        return true;
                    }
                    g.b(er4.a(), this.b.b.i(), adapterPosition, er4.b(), null);
                    return true;
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ts5 ts5, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.b = ts5;
            view.setOnClickListener(new Aii(this));
            view.setOnLongClickListener(new Bii(this));
            View findViewById = view.findViewById(2131363303);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public List<String> a; // = new ArrayList();
        @DexIgnore
        public TextView b;
        @DexIgnore
        public Spinner c;
        @DexIgnore
        public Button d;
        @DexIgnore
        public /* final */ /* synthetic */ Ts5 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<Er4> h = this.b.e.h();
                    if (h != null) {
                        Er4 er4 = h.get(adapterPosition);
                        Ei g = this.b.e.g();
                        if (g != null) {
                            String a2 = er4.a();
                            int i = this.b.e.i();
                            Object b2 = er4.b();
                            Bundle bundle = new Bundle();
                            bundle.putInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS", this.b.c.getSelectedItemPosition());
                            g.a(a2, i, adapterPosition, b2, bundle);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Ts5 ts5, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.e = ts5;
            View findViewById = view.findViewById(2131363411);
            if (findViewById != null) {
                this.b = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363130);
                if (findViewById2 != null) {
                    this.c = (Spinner) findViewById2;
                    View findViewById3 = view.findViewById(2131361970);
                    if (findViewById3 != null) {
                        Button button = (Button) findViewById3;
                        button.setOnClickListener(new Aii(this));
                        this.d = button;
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.Spinner");
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final Button b() {
            return this.d;
        }

        @DexIgnore
        public final TextView c() {
            return this.b;
        }

        @DexIgnore
        public final void d(List<String> list) {
            Wg6.c(list, "value");
            this.a = list;
            e();
        }

        @DexIgnore
        public final void e() {
            View view = this.itemView;
            Wg6.b(view, "itemView");
            ArrayAdapter arrayAdapter = new ArrayAdapter(view.getContext(), 17367048, this.a);
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public SwitchCompat b;
        @DexIgnore
        public /* final */ /* synthetic */ Ts5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ SwitchCompat b;
            @DexIgnore
            public /* final */ /* synthetic */ Ci c;

            @DexIgnore
            public Aii(SwitchCompat switchCompat, Ci ci) {
                this.b = switchCompat;
                this.c = ci;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.c.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<Er4> h = this.c.c.h();
                    if (h != null) {
                        Er4 er4 = h.get(adapterPosition);
                        if (er4 != null) {
                            ((Ws5) er4).g(this.b.isChecked());
                            Ei g = this.c.c.g();
                            if (g != null) {
                                String a2 = er4.a();
                                int i = this.c.c.i();
                                Object b2 = er4.b();
                                Bundle bundle = new Bundle();
                                List<Er4> h2 = this.c.c.h();
                                if (h2 != null) {
                                    Er4 er42 = h2.get(adapterPosition);
                                    if (er42 != null) {
                                        bundle.putBoolean("DEBUG_BUNDLE_IS_CHECKED", ((Ws5) er42).f());
                                        g.a(a2, i, adapterPosition, b2, bundle);
                                    } else {
                                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                            this.c.c.notifyItemChanged(adapterPosition);
                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Ts5 ts5, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.c = ts5;
            View findViewById = view.findViewById(2131363304);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363074);
                if (findViewById2 != null) {
                    SwitchCompat switchCompat = (SwitchCompat) findViewById2;
                    switchCompat.setOnClickListener(new Aii(switchCompat, this));
                    this.b = switchCompat;
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final SwitchCompat a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ Ts5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;

            @DexIgnore
            public Aii(Di di) {
                this.b = di;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<Er4> h = this.b.c.h();
                    if (h != null) {
                        Er4 er4 = h.get(adapterPosition);
                        Ei g = this.b.c.g();
                        if (g != null) {
                            g.a(er4.a(), this.b.c.i(), adapterPosition, er4.b(), null);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;

            @DexIgnore
            public Bii(Di di) {
                this.b = di;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<Er4> h = this.b.c.h();
                if (h != null) {
                    Er4 er4 = h.get(adapterPosition);
                    Ei g = this.b.c.g();
                    if (g == null) {
                        return true;
                    }
                    g.b(er4.a(), this.b.c.i(), adapterPosition, er4.b(), null);
                    return true;
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Ts5 ts5, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.c = ts5;
            view.setOnClickListener(new Aii(this));
            view.setOnLongClickListener(new Bii(this));
            View findViewById = view.findViewById(2131363305);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363306);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Ei {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore
    public final Ei g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends Er4> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        List<? extends Er4> list = this.b;
        if ((list != null ? (Er4) list.get(i) : null) instanceof Ws5) {
            return Zs5.CHILD_ITEM_WITH_SWITCH.ordinal();
        }
        List<? extends Er4> list2 = this.b;
        if ((list2 != null ? (Er4) list2.get(i) : null) instanceof Xs5) {
            return Zs5.CHILD_ITEM_WITH_TEXT.ordinal();
        }
        List<? extends Er4> list3 = this.b;
        return (list3 != null ? (Er4) list3.get(i) : null) instanceof Vs5 ? Zs5.CHILD_ITEM_WITH_SPINNER.ordinal() : Zs5.CHILD.ordinal();
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.mapped.Er4>, java.util.List<com.mapped.Er4> */
    public final List<Er4> h() {
        return this.b;
    }

    @DexIgnore
    public final int i() {
        return this.a;
    }

    @DexIgnore
    public final void j(List<? extends Er4> list) {
        this.b = list;
    }

    @DexIgnore
    public final void k(Ei ei) {
        Wg6.c(ei, "itemClickListener");
        this.c = ei;
    }

    @DexIgnore
    public final void l(int i) {
        this.a = i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        if (viewHolder instanceof Ai) {
            TextView a2 = ((Ai) viewHolder).a();
            List<? extends Er4> list = this.b;
            if (list != null) {
                a2.setText(((Er4) list.get(i)).d());
            } else {
                Wg6.i();
                throw null;
            }
        } else if (viewHolder instanceof Ci) {
            Ci ci = (Ci) viewHolder;
            TextView b2 = ci.b();
            List<? extends Er4> list2 = this.b;
            if (list2 != null) {
                b2.setText(((Er4) list2.get(i)).d());
                SwitchCompat a3 = ci.a();
                List<? extends Er4> list3 = this.b;
                if (list3 != null) {
                    Object obj = list3.get(i);
                    if (obj != null) {
                        a3.setChecked(((Ws5) obj).f());
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        } else if (viewHolder instanceof Di) {
            Di di = (Di) viewHolder;
            TextView b3 = di.b();
            List<? extends Er4> list4 = this.b;
            if (list4 != null) {
                b3.setText(((Er4) list4.get(i)).d());
                TextView a4 = di.a();
                List<? extends Er4> list5 = this.b;
                if (list5 != null) {
                    Object obj2 = list5.get(i);
                    if (obj2 != null) {
                        a4.setText(((Xs5) obj2).f());
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        } else if (viewHolder instanceof Bi) {
            Bi bi = (Bi) viewHolder;
            TextView c2 = bi.c();
            List<? extends Er4> list6 = this.b;
            if (list6 != null) {
                c2.setText(((Er4) list6.get(i)).d());
                Button b4 = bi.b();
                List<? extends Er4> list7 = this.b;
                if (list7 != null) {
                    Object obj3 = list7.get(i);
                    if (obj3 != null) {
                        b4.setText(((Vs5) obj3).f());
                        List<? extends Er4> list8 = this.b;
                        if (list8 != null) {
                            Object obj4 = list8.get(i);
                            if (obj4 != null) {
                                bi.d(((Vs5) obj4).g());
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                        }
                        Wg6.i();
                        throw null;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == Zs5.CHILD.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558675, viewGroup, false);
            Wg6.b(inflate, "view");
            return new Ai(this, inflate);
        } else if (i == Zs5.CHILD_ITEM_WITH_SWITCH.ordinal()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558677, viewGroup, false);
            Wg6.b(inflate2, "view");
            return new Ci(this, inflate2);
        } else if (i == Zs5.CHILD_ITEM_WITH_TEXT.ordinal()) {
            View inflate3 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558678, viewGroup, false);
            Wg6.b(inflate3, "view");
            return new Di(this, inflate3);
        } else if (i == Zs5.CHILD_ITEM_WITH_SPINNER.ordinal()) {
            View inflate4 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558676, viewGroup, false);
            Wg6.b(inflate4, "view");
            return new Bi(this, inflate4);
        } else {
            throw new IllegalArgumentException("viewType is not appropriate");
        }
    }
}
