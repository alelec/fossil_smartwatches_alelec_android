package com.fossil;

import com.fossil.V02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q02 extends V02 {
    @DexIgnore
    public /* final */ V02.Ai a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public Q02(V02.Ai ai, long j) {
        if (ai != null) {
            this.a = ai;
            this.b = j;
            return;
        }
        throw new NullPointerException("Null status");
    }

    @DexIgnore
    @Override // com.fossil.V02
    public long b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.V02
    public V02.Ai c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof V02)) {
            return false;
        }
        V02 v02 = (V02) obj;
        return this.a.equals(v02.c()) && this.b == v02.b();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        long j = this.b;
        return ((hashCode ^ 1000003) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "BackendResponse{status=" + this.a + ", nextRequestWaitMillis=" + this.b + "}";
    }
}
