package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ql7 implements Comparable<Ql7> {
    @DexIgnore
    public /* final */ short b;

    @DexIgnore
    public /* synthetic */ Ql7(short s) {
        this.b = (short) s;
    }

    @DexIgnore
    public static final /* synthetic */ Ql7 a(short s) {
        return new Ql7(s);
    }

    @DexIgnore
    public static int c(short s, short s2) {
        return Wg6.d(s & 65535, 65535 & s2);
    }

    @DexIgnore
    public static short e(short s) {
        return s;
    }

    @DexIgnore
    public static boolean f(short s, Object obj) {
        return (obj instanceof Ql7) && s == ((Ql7) obj).j();
    }

    @DexIgnore
    public static int h(short s) {
        return s;
    }

    @DexIgnore
    public static String i(short s) {
        return String.valueOf(65535 & s);
    }

    @DexIgnore
    public final int b(short s) {
        return c(this.b, s);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Ql7 ql7) {
        return b(ql7.j());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return f(this.b, obj);
    }

    @DexIgnore
    public int hashCode() {
        short s = this.b;
        h(s);
        return s;
    }

    @DexIgnore
    public final /* synthetic */ short j() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return i(this.b);
    }
}
