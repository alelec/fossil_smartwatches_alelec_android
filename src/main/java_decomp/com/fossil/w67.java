package com.fossil;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class W67 extends FrameLayout {
    @DexIgnore
    public Ai b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;

    @DexIgnore
    public interface Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public static /* synthetic */ boolean a(Ai ai, float f, float f2, float f3, int i, Object obj) {
                if (obj == null) {
                    if ((i & 4) != 0) {
                        f3 = 1.0f;
                    }
                    return ai.d(f, f2, f3);
                }
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: isReachMaximumSize");
            }

            @DexIgnore
            public static /* synthetic */ boolean b(Ai ai, float f, float f2, float f3, int i, Object obj) {
                if (obj == null) {
                    if ((i & 4) != 0) {
                        f3 = 1.0f;
                    }
                    return ai.f(f, f2, f3);
                }
                throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: isReachMinimumSize");
            }
        }

        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        int c();

        @DexIgnore
        boolean d(float f, float f2, float f3);

        @DexIgnore
        boolean e(W67 w67);

        @DexIgnore
        boolean f(float f, float f2, float f3);

        @DexIgnore
        WatchFaceEditorView.a g();

        @DexIgnore
        boolean j(MotionEvent motionEvent);

        @DexIgnore
        void k(S87 s87);

        @DexIgnore
        void l(W67 w67);

        @DexIgnore
        void m(T67 t67);

        @DexIgnore
        boolean n(W67 w67);

        @DexIgnore
        boolean q(W67 w67);
    }

    @DexIgnore
    public enum Bi {
        UPDATED,
        ADDED,
        REMOVED
    }

    @DexIgnore
    public enum Ci {
        TEXT,
        STICKER,
        COMPLICATION,
        NONE
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public W67(Context context) {
        super(context);
        Wg6.c(context, "context");
    }

    @DexIgnore
    public static /* synthetic */ S87 d(W67 w67, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            return w67.c(z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: extractConfig");
    }

    @DexIgnore
    public boolean a() {
        Ai ai = this.b;
        if (ai != null) {
            return ai.n(this);
        }
        return false;
    }

    @DexIgnore
    public final void b() {
        Ai ai = this.b;
        if (ai != null) {
            ai.l(this);
        }
    }

    @DexIgnore
    public abstract S87 c(boolean z);

    @DexIgnore
    public final void e() {
        Ai ai = this.b;
        if (ai != null) {
            ai.b();
        }
    }

    @DexIgnore
    public final void f() {
        Ai ai = this.b;
        if (ai != null) {
            ai.a();
        }
    }

    @DexIgnore
    public void g() {
        setOnTouchListener(new O67(this));
    }

    @DexIgnore
    public final Ai getHandler() {
        return this.b;
    }

    @DexIgnore
    public final float getMPrevTranslateX() {
        return this.g;
    }

    @DexIgnore
    public final float getMPrevTranslateY() {
        return this.h;
    }

    @DexIgnore
    public final W87 getMetric() {
        return new W87(getTranslationX(), getTranslationY(), (float) getWidth(), (float) getHeight(), getScaleX());
    }

    @DexIgnore
    public final int getTouchCount$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public abstract Ci getType();

    @DexIgnore
    public final boolean h() {
        return this.e;
    }

    @DexIgnore
    public final boolean i() {
        Ai ai = this.b;
        if (ai != null) {
            return ai.e(this);
        }
        return false;
    }

    @DexIgnore
    public final boolean j() {
        return this.c;
    }

    @DexIgnore
    public final boolean k(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        Ai ai = this.b;
        if (ai != null) {
            return ai.j(motionEvent);
        }
        return false;
    }

    @DexIgnore
    public final boolean l() {
        return this.d;
    }

    @DexIgnore
    public final void m() {
        S87 c2 = c(true);
        Ai ai = this.b;
        if (ai != null) {
            ai.k(c2);
        }
    }

    @DexIgnore
    public final void n() {
        this.f = 0;
    }

    @DexIgnore
    public abstract void o();

    @DexIgnore
    public final boolean p() {
        Ai ai = this.b;
        if (!Wg6.a(ai != null ? Boolean.valueOf(ai.q(this)) : null, Boolean.TRUE)) {
            return false;
        }
        n();
        return true;
    }

    @DexIgnore
    public final void q() {
        this.g = getTranslationX();
        this.h = getTranslationY();
    }

    @DexIgnore
    public final void r(float f2, float f3) {
        setTranslationX(f2);
        setTranslationY(f3);
    }

    @DexIgnore
    public final void s(float f2, float f3) {
        setTranslationX(f2);
        setTranslationY(f3);
        this.g = f2;
        this.h = f3;
    }

    @DexIgnore
    public final void setAnimating$app_fossilRelease(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void setEditing$app_fossilRelease(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void setElementEventHandler(Ai ai) {
        Wg6.c(ai, "handler");
        this.b = ai;
    }

    @DexIgnore
    public final void setHandler(Ai ai) {
        this.b = ai;
    }

    @DexIgnore
    public final void setMPrevTranslateX(float f2) {
        this.g = f2;
    }

    @DexIgnore
    public final void setMPrevTranslateY(float f2) {
        this.h = f2;
    }

    @DexIgnore
    public final void setTouchCount$app_fossilRelease(int i) {
        this.f = i;
    }

    @DexIgnore
    public final void setTranslating$app_fossilRelease(boolean z) {
        this.d = z;
    }
}
