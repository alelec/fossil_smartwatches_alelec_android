package com.fossil;

public final class Hz2 {
    public static final Gz2 a;

    public static final class Ai extends Gz2 {
        @Override // com.fossil.Gz2
        public final void a(Throwable th, Throwable th2) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /*
    static {
        /*
            java.lang.Integer r1 = a()     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0018
            int r0 = r1.intValue()     // Catch:{ all -> 0x0028 }
            r2 = 19
            if (r0 < r2) goto L_0x0018
            com.fossil.Lz2 r0 = new com.fossil.Lz2     // Catch:{ all -> 0x0028 }
            r0.<init>()     // Catch:{ all -> 0x0028 }
        L_0x0013:
            com.fossil.Hz2.a = r0
            if (r1 != 0) goto L_0x0068
        L_0x0017:
            return
        L_0x0018:
            java.lang.String r0 = "com.google.devtools.build.android.desugar.runtime.twr_disable_mimic"
            boolean r0 = java.lang.Boolean.getBoolean(r0)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x005f
            com.fossil.Kz2 r0 = new com.fossil.Kz2
            r0.<init>()
            goto L_0x0013
        L_0x0028:
            r0 = move-exception
        L_0x0029:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.Class<com.fossil.Hz2$Ai> r3 = com.fossil.Hz2.Ai.class
            java.lang.String r3 = r3.getName()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r3)
            int r5 = r5.length()
            int r5 = r5 + 133
            r4.<init>(r5)
            java.lang.String r5 = "An error has occurred when initializing the try-with-resources desuguring strategy. The default strategy "
            r4.append(r5)
            r4.append(r3)
            java.lang.String r3 = "will be used. The error is: "
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            r2.println(r3)
            java.io.PrintStream r2 = java.lang.System.err
            r0.printStackTrace(r2)
            com.fossil.Hz2$Ai r0 = new com.fossil.Hz2$Ai
            r0.<init>()
            goto L_0x0013
        L_0x005f:
            com.fossil.Hz2$Ai r0 = new com.fossil.Hz2$Ai
            r0.<init>()
            goto L_0x0013
        L_0x0065:
            r0 = move-exception
            r1 = 0
            goto L_0x0029
        L_0x0068:
            r1.intValue()
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Hz2.<clinit>():void");
    }
    */

    public static Integer a() {
        try {
            return (Integer) Class.forName("android.os.Build$VERSION").getField("SDK_INT").get(null);
        } catch (Exception e) {
            System.err.println("Failed to retrieve value from android.os.Build$VERSION.SDK_INT due to the following exception.");
            e.printStackTrace(System.err);
            return null;
        }
    }

    public static void b(Throwable th, Throwable th2) {
        a.a(th, th2);
    }
}
