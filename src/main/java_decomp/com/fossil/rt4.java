package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Wg6;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rt4 {
    @DexIgnore
    public /* final */ Qs4 a;

    @DexIgnore
    public Rt4(Qs4 qs4) {
        Wg6.c(qs4, "dao");
        this.a = qs4;
    }

    @DexIgnore
    public final Ps4 a(String str) {
        Wg6.c(str, "id");
        return this.a.m(str);
    }

    @DexIgnore
    public final LiveData<Ps4> b(String str) {
        Wg6.c(str, "id");
        return this.a.s(str);
    }

    @DexIgnore
    public final int c(String str) {
        Wg6.c(str, "id");
        return this.a.n(str);
    }

    @DexIgnore
    public final void d() {
        this.a.g();
        this.a.w();
        this.a.x();
        this.a.q();
        this.a.o();
    }

    @DexIgnore
    public final void e() {
        this.a.w();
    }

    @DexIgnore
    public final void f() {
        this.a.p();
    }

    @DexIgnore
    public final Ps4 g(String[] strArr, Date date) {
        Wg6.c(strArr, "status");
        Wg6.c(date, "date");
        return this.a.f(strArr, date);
    }

    @DexIgnore
    public final LiveData<Ps4> h(String[] strArr, Date date) {
        Wg6.c(strArr, "status");
        Wg6.c(date, "date");
        return this.a.c(strArr, date);
    }

    @DexIgnore
    public final int i(String str) {
        Wg6.c(str, "id");
        return this.a.b(str);
    }

    @DexIgnore
    public final void j(String[] strArr) {
        Wg6.c(strArr, "status");
        this.a.u(strArr);
    }

    @DexIgnore
    public final Ks4 k(String str) {
        Wg6.c(str, "id");
        return this.a.d(str);
    }

    @DexIgnore
    public final List<Ls4> l() {
        return this.a.j();
    }

    @DexIgnore
    public final List<Ms4> m() {
        return this.a.a();
    }

    @DexIgnore
    public final LiveData<Bt4> n(String str) {
        Wg6.c(str, "challengeId");
        return this.a.l(str);
    }

    @DexIgnore
    public final Bt4 o(String str) {
        Wg6.c(str, "id");
        return this.a.e(str);
    }

    @DexIgnore
    public final LiveData<List<Bt4>> p() {
        return this.a.i();
    }

    @DexIgnore
    public final long q(Ls4 ls4) {
        Wg6.c(ls4, "fitnessData");
        return this.a.h(ls4);
    }

    @DexIgnore
    public final long r(Ps4 ps4) {
        Wg6.c(ps4, "challenge");
        return this.a.k(ps4);
    }

    @DexIgnore
    public final Long[] s(List<Ps4> list) {
        Wg6.c(list, "challenges");
        return this.a.insert(list);
    }

    @DexIgnore
    public final Long[] t(List<Ks4> list) {
        Wg6.c(list, "displayPlayer");
        return this.a.y(list);
    }

    @DexIgnore
    public final Long[] u(List<Bt4> list) {
        Wg6.c(list, "histories");
        return this.a.t(list);
    }

    @DexIgnore
    public final Long[] v(List<Ms4> list) {
        Wg6.c(list, "players");
        return this.a.v(list);
    }

    @DexIgnore
    public final int w(Bt4 bt4) {
        Wg6.c(bt4, "historyChallenge");
        return this.a.r(bt4);
    }
}
