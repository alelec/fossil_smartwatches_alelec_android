package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zj extends Ro {
    @DexIgnore
    public /* final */ byte[] S;

    @DexIgnore
    public Zj(K5 k5, I60 i60, Yp yp, boolean z, short s, byte[] bArr, float f, String str) {
        super(k5, i60, yp, z, s, f, str, false, 128);
        this.S = bArr;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Zj(K5 k5, I60 i60, Yp yp, boolean z, short s, byte[] bArr, float f, String str, int i) {
        super(k5, i60, yp, z, s, (i & 64) != 0 ? 0.001f : f, (i & 128) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 128);
        this.S = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public byte[] M() {
        return this.S;
    }
}
