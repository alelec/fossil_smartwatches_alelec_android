package com.fossil;

import com.fossil.Ta4$d$d$a$b$e;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Na4 extends Ta4$d$d$a$b$e {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Ua4<Ta4$d$d$a$b$e.b> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$e.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Ua4<Ta4$d$d$a$b$e.b> c;

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$e.a
        public Ta4$d$d$a$b$e a() {
            String str = "";
            if (this.a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " importance";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (str.isEmpty()) {
                return new Na4(this.a, this.b.intValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$e.a
        public Ta4$d$d$a$b$e.a b(Ua4<Ta4$d$d$a$b$e.b> ua4) {
            if (ua4 != null) {
                this.c = ua4;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$e.a
        public Ta4$d$d$a$b$e.a c(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$e.a
        public Ta4$d$d$a$b$e.a d(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }
    }

    @DexIgnore
    public Na4(String str, int i, Ua4<Ta4$d$d$a$b$e.b> ua4) {
        this.a = str;
        this.b = i;
        this.c = ua4;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e
    public Ua4<Ta4$d$d$a$b$e.b> b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e
    public int c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e
    public String d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4$d$d$a$b$e)) {
            return false;
        }
        Ta4$d$d$a$b$e ta4$d$d$a$b$e = (Ta4$d$d$a$b$e) obj;
        return this.a.equals(ta4$d$d$a$b$e.d()) && this.b == ta4$d$d$a$b$e.c() && this.c.equals(ta4$d$d$a$b$e.b());
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Thread{name=" + this.a + ", importance=" + this.b + ", frames=" + this.c + "}";
    }
}
