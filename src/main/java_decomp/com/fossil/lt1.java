package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Yl1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Lt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Lt1 createFromParcel(Parcel parcel) {
            return new Lt1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Lt1[] newArray(int i) {
            return new Lt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Lt1(Parcel parcel, Qg6 qg6) {
        super((X90) parcel.readParcelable(Iq1.class.getClassLoader()), (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()));
        this.d = (Yl1) parcel.readParcelable(Yl1.class.getClassLoader());
    }

    @DexIgnore
    public Lt1(Iq1 iq1, Nt1 nt1) throws IllegalArgumentException {
        super(iq1, nt1);
        this.d = null;
    }

    @DexIgnore
    public Lt1(Iq1 iq1, Nt1 nt1, Yl1 yl1) {
        super(iq1, nt1);
        this.d = yl1;
    }

    @DexIgnore
    public Lt1(Iq1 iq1, Yl1 yl1) throws IllegalArgumentException {
        super(iq1, null);
        this.d = yl1;
    }

    @DexIgnore
    public Lt1(Nt1 nt1) throws IllegalArgumentException {
        this(null, nt1, null);
    }

    @DexIgnore
    public Lt1(Yl1 yl1) throws IllegalArgumentException {
        this(null, null, yl1);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        Object obj;
        JSONObject a2 = super.a();
        Jd0 jd0 = Jd0.y4;
        Yl1 yl1 = this.d;
        if (yl1 == null || (obj = yl1.toJSONObject()) == null) {
            obj = JSONObject.NULL;
        }
        return G80.k(a2, jd0, obj);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (this.d != null) {
                jSONObject2.put("dest", this.d.getDestination()).put("commute", this.d.getCommuteTimeInMinute()).put("traffic", this.d.getTraffic());
            } else {
                Nt1 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
            }
            jSONObject.put("commuteApp._.config.commute_info", jSONObject2);
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        Wg6.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Lt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.d, ((Lt1) obj).d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData");
    }

    @DexIgnore
    public final Yl1 getCommuteTimeInfo() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        Yl1 yl1 = this.d;
        return (yl1 != null ? yl1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Yl1 yl1 = this.d;
            if (yl1 != null) {
                parcel.writeParcelable(yl1, i);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.os.Parcelable");
        }
    }
}
