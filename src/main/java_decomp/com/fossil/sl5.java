package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sl5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Map<String, String> c; // = new HashMap();
    @DexIgnore
    public AnalyticsHelper d;

    @DexIgnore
    public Sl5(AnalyticsHelper analyticsHelper, String str) {
        Wg6.c(analyticsHelper, "analyticsHelper");
        Wg6.c(str, "eventName");
        this.d = analyticsHelper;
        this.a = str;
    }

    @DexIgnore
    public final Sl5 a(String str, String str2) {
        Wg6.c(str, "paramName");
        Wg6.c(str2, "paramValue");
        Map<String, String> map = this.c;
        if (map != null) {
            map.put(str, str2);
            return this;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void b() {
        String str;
        Map<String, String> map = this.c;
        if (map == null) {
            Wg6.i();
            throw null;
        } else if (!map.isEmpty() || (str = this.b) == null) {
            AnalyticsHelper analyticsHelper = this.d;
            if (analyticsHelper != null) {
                analyticsHelper.l(this.a, this.c);
            } else {
                Wg6.i();
                throw null;
            }
        } else if (str != null) {
            AnalyticsHelper analyticsHelper2 = this.d;
            if (analyticsHelper2 != null) {
                String str2 = this.a;
                if (str != null) {
                    analyticsHelper2.j(str2, str);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            AnalyticsHelper analyticsHelper3 = this.d;
            if (analyticsHelper3 != null) {
                analyticsHelper3.i(this.a);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }
}
