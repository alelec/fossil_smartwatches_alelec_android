package com.fossil;

import java.net.URL;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sl3 implements Runnable {
    @DexIgnore
    public /* final */ URL b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ Ql3 d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ Map<String, String> f;
    @DexIgnore
    public /* final */ /* synthetic */ Ol3 g;

    @DexIgnore
    public Sl3(Ol3 ol3, String str, URL url, byte[] bArr, Map<String, String> map, Ql3 ql3) {
        this.g = ol3;
        Rc2.g(str);
        Rc2.k(url);
        Rc2.k(ql3);
        this.b = url;
        this.c = bArr;
        this.d = ql3;
        this.e = str;
        this.f = map;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f A[SYNTHETIC, Splitter:B:14:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ca A[SYNTHETIC, Splitter:B:36:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
        // Method dump skipped, instructions count: 312
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sl3.run():void");
    }
}
