package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDaySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bn6 extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public ArrayList<Bi> e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ OverviewSleepDayChart a;
        @DexIgnore
        public /* final */ OverviewSleepDaySummary b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ FlexibleTextView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ FlexibleTextView h;
        @DexIgnore
        public /* final */ FlexibleTextView i;
        @DexIgnore
        public /* final */ FlexibleTextView j;
        @DexIgnore
        public /* final */ FlexibleTextView k;
        @DexIgnore
        public /* final */ FlexibleTextView l;
        @DexIgnore
        public /* final */ FlexibleTextView m;
        @DexIgnore
        public /* final */ FlexibleTextView n;
        @DexIgnore
        public /* final */ FlexibleTextView o;
        @DexIgnore
        public /* final */ FlexibleTextView p;
        @DexIgnore
        public /* final */ FlexibleTextView q;
        @DexIgnore
        public /* final */ ConstraintLayout r;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(View view) {
            super(view);
            Wg6.c(view, "item");
            View findViewById = view.findViewById(2131363122);
            Wg6.b(findViewById, "item.findViewById(R.id.sleep_chart)");
            this.a = (OverviewSleepDayChart) findViewById;
            View findViewById2 = view.findViewById(2131363470);
            Wg6.b(findViewById2, "item.findViewById(R.id.v_summary_chart)");
            this.b = (OverviewSleepDaySummary) findViewById2;
            View findViewById3 = view.findViewById(2131363276);
            Wg6.b(findViewById3, "item.findViewById(R.id.tv_awake)");
            this.c = (FlexibleTextView) findViewById3;
            View findViewById4 = view.findViewById(2131363349);
            Wg6.b(findViewById4, "item.findViewById(R.id.tv_light)");
            this.d = (FlexibleTextView) findViewById4;
            View findViewById5 = view.findViewById(2131363309);
            Wg6.b(findViewById5, "item.findViewById(R.id.tv_deep)");
            this.e = (FlexibleTextView) findViewById5;
            View findViewById6 = view.findViewById(2131363280);
            Wg6.b(findViewById6, "item.findViewById(R.id.tv_awake_min)");
            this.f = (FlexibleTextView) findViewById6;
            View findViewById7 = view.findViewById(2131363353);
            Wg6.b(findViewById7, "item.findViewById(R.id.tv_light_min)");
            this.g = (FlexibleTextView) findViewById7;
            View findViewById8 = view.findViewById(2131363313);
            Wg6.b(findViewById8, "item.findViewById(R.id.tv_deep_min)");
            this.h = (FlexibleTextView) findViewById8;
            View findViewById9 = view.findViewById(2131363278);
            Wg6.b(findViewById9, "item.findViewById(R.id.tv_awake_hour)");
            this.i = (FlexibleTextView) findViewById9;
            View findViewById10 = view.findViewById(2131363351);
            Wg6.b(findViewById10, "item.findViewById(R.id.tv_light_hour)");
            this.j = (FlexibleTextView) findViewById10;
            View findViewById11 = view.findViewById(2131363311);
            Wg6.b(findViewById11, "item.findViewById(R.id.tv_deep_hour)");
            this.k = (FlexibleTextView) findViewById11;
            View findViewById12 = view.findViewById(2131363279);
            Wg6.b(findViewById12, "item.findViewById(R.id.tv_awake_hour_unit)");
            this.l = (FlexibleTextView) findViewById12;
            View findViewById13 = view.findViewById(2131363352);
            Wg6.b(findViewById13, "item.findViewById(R.id.tv_light_hour_unit)");
            this.m = (FlexibleTextView) findViewById13;
            View findViewById14 = view.findViewById(2131363312);
            Wg6.b(findViewById14, "item.findViewById(R.id.tv_deep_hour_unit)");
            this.n = (FlexibleTextView) findViewById14;
            View findViewById15 = view.findViewById(2131363281);
            Wg6.b(findViewById15, "item.findViewById(R.id.tv_awake_min_unit)");
            this.o = (FlexibleTextView) findViewById15;
            View findViewById16 = view.findViewById(2131363354);
            Wg6.b(findViewById16, "item.findViewById(R.id.tv_light_min_unit)");
            this.p = (FlexibleTextView) findViewById16;
            View findViewById17 = view.findViewById(2131363314);
            Wg6.b(findViewById17, "item.findViewById(R.id.tv_deep_min_unit)");
            this.q = (FlexibleTextView) findViewById17;
            View findViewById18 = view.findViewById(2131362108);
            Wg6.b(findViewById18, "item.findViewById(R.id.cl_sleep_chart)");
            this.r = (ConstraintLayout) findViewById18;
        }

        @DexIgnore
        public final ConstraintLayout a() {
            return this.r;
        }

        @DexIgnore
        public final OverviewSleepDayChart b() {
            return this.a;
        }

        @DexIgnore
        public final OverviewSleepDaySummary c() {
            return this.b;
        }

        @DexIgnore
        public final FlexibleTextView d() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView e() {
            return this.i;
        }

        @DexIgnore
        public final FlexibleTextView f() {
            return this.l;
        }

        @DexIgnore
        public final FlexibleTextView g() {
            return this.f;
        }

        @DexIgnore
        public final FlexibleTextView h() {
            return this.o;
        }

        @DexIgnore
        public final FlexibleTextView i() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView j() {
            return this.k;
        }

        @DexIgnore
        public final FlexibleTextView k() {
            return this.n;
        }

        @DexIgnore
        public final FlexibleTextView l() {
            return this.h;
        }

        @DexIgnore
        public final FlexibleTextView m() {
            return this.q;
        }

        @DexIgnore
        public final FlexibleTextView n() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView o() {
            return this.j;
        }

        @DexIgnore
        public final FlexibleTextView p() {
            return this.m;
        }

        @DexIgnore
        public final FlexibleTextView q() {
            return this.g;
        }

        @DexIgnore
        public final FlexibleTextView r() {
            return this.p;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Mv5 a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public Bi(Mv5 mv5, float f2, float f3, float f4, int i, int i2, int i3, int i4) {
            Wg6.c(mv5, "sleepChartModel");
            this.a = mv5;
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = i;
            this.f = i2;
            this.g = i3;
            this.h = i4;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final float b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.g;
        }

        @DexIgnore
        public final float d() {
            return this.d;
        }

        @DexIgnore
        public final int e() {
            return this.f;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(Wg6.a(this.a, bi.a) && Float.compare(this.b, bi.b) == 0 && Float.compare(this.c, bi.c) == 0 && Float.compare(this.d, bi.d) == 0 && this.e == bi.e && this.f == bi.f && this.g == bi.g && this.h == bi.h)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final float f() {
            return this.c;
        }

        @DexIgnore
        public final Mv5 g() {
            return this.a;
        }

        @DexIgnore
        public final int h() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            Mv5 mv5 = this.a;
            return ((((((((((((((mv5 != null ? mv5.hashCode() : 0) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d)) * 31) + this.e) * 31) + this.f) * 31) + this.g) * 31) + this.h;
        }

        @DexIgnore
        public String toString() {
            return "SleepUIData(sleepChartModel=" + this.a + ", awakePer=" + this.b + ", lightPer=" + this.c + ", deepPer=" + this.d + ", awakeMin=" + this.e + ", lightMin=" + this.f + ", deepMin=" + this.g + ", timeZoneOffsetInSecond=" + this.h + ")";
        }
    }

    @DexIgnore
    public Bn6(ArrayList<Bi> arrayList) {
        Wg6.c(arrayList, "data");
        this.e = arrayList;
        String d2 = ThemeManager.l.a().d("awakeSleep");
        this.a = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("lightSleep");
        this.b = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("deepSleep");
        this.c = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("nonBrandSurface");
        this.d = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
    }

    @DexIgnore
    public void g(Ai ai, int i) {
        Wg6.c(ai, "holder");
        Bi bi = this.e.get(i);
        Wg6.b(bi, "data[position]");
        Bi bi2 = bi;
        ai.d().setText(Jl5.b.l(2131886693, bi2.b()));
        ai.n().setText(Jl5.b.l(2131886695, bi2.f()));
        ai.i().setText(Jl5.b.l(2131886694, bi2.d()));
        ai.g().setText(String.valueOf(Ll5.b(bi2.a())));
        ai.q().setText(String.valueOf(Ll5.b(bi2.e())));
        ai.l().setText(String.valueOf(Ll5.b(bi2.c())));
        ai.e().setText(String.valueOf(Ll5.a(bi2.a())));
        ai.o().setText(String.valueOf(Ll5.a(bi2.e())));
        ai.j().setText(String.valueOf(Ll5.a(bi2.c())));
        OverviewSleepDayChart b2 = ai.b();
        BarChart.F(b2, 0, this.a, this.b, this.c, null, null, 49, null);
        b2.setTimeZoneOffsetInSecond(bi2.h());
        b2.r(bi2.g());
        ai.c().c(bi2.b(), bi2.f(), bi2.d(), this.a, this.b, this.c);
        ai.d().setTextColor(this.a);
        ai.n().setTextColor(this.b);
        ai.i().setTextColor(this.c);
        ai.e().setTextColor(this.a);
        ai.g().setTextColor(this.a);
        ai.f().setTextColor(this.a);
        ai.h().setTextColor(this.a);
        ai.o().setTextColor(this.b);
        ai.q().setTextColor(this.b);
        ai.p().setTextColor(this.b);
        ai.r().setTextColor(this.b);
        ai.j().setTextColor(this.c);
        ai.l().setTextColor(this.c);
        ai.k().setTextColor(this.c);
        ai.m().setTextColor(this.c);
        ai.a().setBackgroundColor(this.d);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    public Ai h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558701, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026p_session, parent, false)");
        return new Ai(inflate);
    }

    @DexIgnore
    public final void i(List<Bi> list) {
        Wg6.c(list, "data");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        g(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }
}
