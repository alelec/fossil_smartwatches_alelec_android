package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oe3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Oe3> CREATOR; // = new Ff3();
    @DexIgnore
    public /* final */ List<LatLng> b;
    @DexIgnore
    public /* final */ List<List<LatLng>> c;
    @DexIgnore
    public float d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public List<Me3> l;

    @DexIgnore
    public Oe3() {
        this.d = 10.0f;
        this.e = -16777216;
        this.f = 0;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.h = true;
        this.i = false;
        this.j = false;
        this.k = 0;
        this.l = null;
        this.b = new ArrayList();
        this.c = new ArrayList();
    }

    @DexIgnore
    public Oe3(List<LatLng> list, List list2, float f2, int i2, int i3, float f3, boolean z, boolean z2, boolean z3, int i4, List<Me3> list3) {
        this.d = 10.0f;
        this.e = -16777216;
        this.f = 0;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.h = true;
        this.i = false;
        this.j = false;
        this.k = 0;
        this.l = null;
        this.b = list;
        this.c = list2;
        this.d = f2;
        this.e = i2;
        this.f = i3;
        this.g = f3;
        this.h = z;
        this.i = z2;
        this.j = z3;
        this.k = i4;
        this.l = list3;
    }

    @DexIgnore
    public final int A() {
        return this.f;
    }

    @DexIgnore
    public final List<LatLng> D() {
        return this.b;
    }

    @DexIgnore
    public final int F() {
        return this.e;
    }

    @DexIgnore
    public final int L() {
        return this.k;
    }

    @DexIgnore
    public final Oe3 c(Iterable<LatLng> iterable) {
        for (LatLng latLng : iterable) {
            this.b.add(latLng);
        }
        return this;
    }

    @DexIgnore
    public final Oe3 f(boolean z) {
        this.j = z;
        return this;
    }

    @DexIgnore
    public final Oe3 h(int i2) {
        this.f = i2;
        return this;
    }

    @DexIgnore
    public final Oe3 k(boolean z) {
        this.i = z;
        return this;
    }

    @DexIgnore
    public final List<Me3> o0() {
        return this.l;
    }

    @DexIgnore
    public final float p0() {
        return this.d;
    }

    @DexIgnore
    public final float q0() {
        return this.g;
    }

    @DexIgnore
    public final boolean r0() {
        return this.j;
    }

    @DexIgnore
    public final boolean s0() {
        return this.i;
    }

    @DexIgnore
    public final boolean t0() {
        return this.h;
    }

    @DexIgnore
    public final Oe3 u0(int i2) {
        this.e = i2;
        return this;
    }

    @DexIgnore
    public final Oe3 v0(float f2) {
        this.d = f2;
        return this;
    }

    @DexIgnore
    public final Oe3 w0(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.y(parcel, 2, D(), false);
        Bd2.q(parcel, 3, this.c, false);
        Bd2.j(parcel, 4, p0());
        Bd2.n(parcel, 5, F());
        Bd2.n(parcel, 6, A());
        Bd2.j(parcel, 7, q0());
        Bd2.c(parcel, 8, t0());
        Bd2.c(parcel, 9, s0());
        Bd2.c(parcel, 10, r0());
        Bd2.n(parcel, 11, L());
        Bd2.y(parcel, 12, o0(), false);
        Bd2.b(parcel, a2);
    }

    @DexIgnore
    public final Oe3 x0(float f2) {
        this.g = f2;
        return this;
    }
}
