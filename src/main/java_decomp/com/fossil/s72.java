package com.fossil;

import android.os.RemoteException;
import com.fossil.M62;
import com.fossil.M62.Bi;
import com.fossil.P72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S72<A extends M62.Bi, L> {
    @DexIgnore
    public /* final */ P72<L> a;
    @DexIgnore
    public /* final */ B62[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public S72(P72<L> p72) {
        this.a = p72;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }

    @DexIgnore
    public P72.Ai<L> b() {
        return this.a.b();
    }

    @DexIgnore
    public B62[] c() {
        return this.b;
    }

    @DexIgnore
    public abstract void d(A a2, Ot3<Void> ot3) throws RemoteException;

    @DexIgnore
    public final boolean e() {
        return this.c;
    }
}
