package com.fossil;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vi1 {
    @DexIgnore
    public /* final */ AtomicReference<Hk1> a; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ Zi0<Hk1, List<Class<?>>> b; // = new Zi0<>();

    @DexIgnore
    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        Hk1 hk1;
        List<Class<?>> list;
        Hk1 andSet = this.a.getAndSet(null);
        if (andSet == null) {
            hk1 = new Hk1(cls, cls2, cls3);
        } else {
            andSet.a(cls, cls2, cls3);
            hk1 = andSet;
        }
        synchronized (this.b) {
            list = this.b.get(hk1);
        }
        this.a.set(hk1);
        return list;
    }

    @DexIgnore
    public void b(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new Hk1(cls, cls2, cls3), list);
        }
    }
}
