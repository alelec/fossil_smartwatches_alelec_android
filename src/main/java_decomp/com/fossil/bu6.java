package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bu6 implements Factory<PreferredUnitPresenter> {
    @DexIgnore
    public static PreferredUnitPresenter a(Eu6 eu6, UserRepository userRepository, UpdateUser updateUser) {
        return new PreferredUnitPresenter(eu6, userRepository, updateUser);
    }
}
