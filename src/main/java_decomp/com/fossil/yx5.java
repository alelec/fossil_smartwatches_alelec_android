package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx5 extends iq4<c, d, b> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public c d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ PortfolioApp g;
    @DexIgnore
    public /* final */ AlarmsRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return yx5.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Alarm f4387a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public b(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            pq7.c(arrayList, "mBLEErrorCodes");
            this.f4387a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final Alarm a() {
            return this.f4387a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f4388a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public c(String str, List<Alarm> list, Alarm alarm) {
            pq7.c(str, "deviceId");
            pq7.c(list, "alarms");
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.f4388a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.f4388a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Alarm f4389a;

        @DexIgnore
        public d(Alarm alarm) {
            pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.f4389a = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.f4389a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements wq5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1", f = "SetAlarms.kt", l = {38, 43, 45, 58}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
                this.$intent = intent;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$intent, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0060  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00ab  */
            /* JADX WARNING: Removed duplicated region for block: B:35:0x0166  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r21) {
                /*
                // Method dump skipped, instructions count: 464
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.yx5.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d(yx5.j.a(), "onReceive");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && yx5.this.p()) {
                yx5.this.v(false);
                xw7 unused = gu7.d(yx5.this.g(), null, null, new a(this, intent, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms", f = "SetAlarms.kt", l = {96, 100}, m = "run")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ yx5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(yx5 yx5, qn7 qn7) {
            super(qn7);
            this.this$0 = yx5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = yx5.class.getSimpleName();
        pq7.b(simpleName, "SetAlarms::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public yx5(PortfolioApp portfolioApp, AlarmsRepository alarmsRepository) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(alarmsRepository, "mAlarmsRepository");
        this.g = portfolioApp;
        this.h = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return i;
    }

    @DexIgnore
    public final boolean p() {
        return this.e;
    }

    @DexIgnore
    public final c q() {
        c cVar = this.d;
        if (cVar != null) {
            return cVar;
        }
        pq7.n("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void r(Intent intent, Alarm alarm) {
        pq7.c(intent, "intent");
        pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "processBLEErrorCodes() - errorCode=" + intExtra);
        ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
        if (integerArrayListExtra == null) {
            integerArrayListExtra = new ArrayList<>(intExtra);
        }
        ul5 f2 = ck5.f.f("set_alarms");
        if (f2 != null) {
            f2.c(String.valueOf(intExtra));
        }
        ck5.f.k("set_alarms");
        i(new b(alarm, intExtra, integerArrayListExtra));
    }

    @DexIgnore
    public final void s() {
        wq5.d.e(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: t */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.yx5.c r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.yx5.k(com.fossil.yx5$c, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void u(c cVar) {
        List<Alarm> b2 = cVar.b();
        ArrayList arrayList = new ArrayList();
        for (T t : b2) {
            if (t.isActive()) {
                arrayList.add(t);
            }
        }
        this.g.o1(cVar.c(), dj5.a(arrayList));
    }

    @DexIgnore
    public final void v(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void w() {
        wq5.d.j(this.f, CommunicateMode.SET_LIST_ALARM);
    }
}
