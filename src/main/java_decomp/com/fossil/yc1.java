package com.fossil;

import com.fossil.Cd1;
import com.fossil.Kk1;
import com.fossil.Uc1;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yc1<R> implements Uc1.Bi<R>, Kk1.Fi {
    @DexIgnore
    public static /* final */ Ci E; // = new Ci();
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Cd1<?> B;
    @DexIgnore
    public Uc1<R> C;
    @DexIgnore
    public volatile boolean D;
    @DexIgnore
    public /* final */ Ei b;
    @DexIgnore
    public /* final */ Mk1 c;
    @DexIgnore
    public /* final */ Cd1.Ai d;
    @DexIgnore
    public /* final */ Mn0<Yc1<?>> e;
    @DexIgnore
    public /* final */ Ci f;
    @DexIgnore
    public /* final */ Zc1 g;
    @DexIgnore
    public /* final */ Le1 h;
    @DexIgnore
    public /* final */ Le1 i;
    @DexIgnore
    public /* final */ Le1 j;
    @DexIgnore
    public /* final */ Le1 k;
    @DexIgnore
    public /* final */ AtomicInteger l;
    @DexIgnore
    public Mb1 m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public Id1<?> w;
    @DexIgnore
    public Gb1 x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public Dd1 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ Gj1 b;

        @DexIgnore
        public Ai(Gj1 gj1) {
            this.b = gj1;
        }

        @DexIgnore
        public void run() {
            synchronized (this.b.e()) {
                synchronized (Yc1.this) {
                    if (Yc1.this.b.b(this.b)) {
                        Yc1.this.e(this.b);
                    }
                    Yc1.this.i();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ Gj1 b;

        @DexIgnore
        public Bi(Gj1 gj1) {
            this.b = gj1;
        }

        @DexIgnore
        public void run() {
            synchronized (this.b.e()) {
                synchronized (Yc1.this) {
                    if (Yc1.this.b.b(this.b)) {
                        Yc1.this.B.a();
                        Yc1.this.g(this.b);
                        Yc1.this.r(this.b);
                    }
                    Yc1.this.i();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public <R> Cd1<R> a(Id1<R> id1, boolean z, Mb1 mb1, Cd1.Ai ai) {
            return new Cd1<>(id1, z, true, mb1, ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ Gj1 a;
        @DexIgnore
        public /* final */ Executor b;

        @DexIgnore
        public Di(Gj1 gj1, Executor executor) {
            this.a = gj1;
            this.b = executor;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof Di) {
                return this.a.equals(((Di) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Iterable<Di> {
        @DexIgnore
        public /* final */ List<Di> b;

        @DexIgnore
        public Ei() {
            this(new ArrayList(2));
        }

        @DexIgnore
        public Ei(List<Di> list) {
            this.b = list;
        }

        @DexIgnore
        public static Di d(Gj1 gj1) {
            return new Di(gj1, Dk1.a());
        }

        @DexIgnore
        public void a(Gj1 gj1, Executor executor) {
            this.b.add(new Di(gj1, executor));
        }

        @DexIgnore
        public boolean b(Gj1 gj1) {
            return this.b.contains(d(gj1));
        }

        @DexIgnore
        public Ei c() {
            return new Ei(new ArrayList(this.b));
        }

        @DexIgnore
        public void clear() {
            this.b.clear();
        }

        @DexIgnore
        public void e(Gj1 gj1) {
            this.b.remove(d(gj1));
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.b.isEmpty();
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<Di> iterator() {
            return this.b.iterator();
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }
    }

    @DexIgnore
    public Yc1(Le1 le1, Le1 le12, Le1 le13, Le1 le14, Zc1 zc1, Cd1.Ai ai, Mn0<Yc1<?>> mn0) {
        this(le1, le12, le13, le14, zc1, ai, mn0, E);
    }

    @DexIgnore
    public Yc1(Le1 le1, Le1 le12, Le1 le13, Le1 le14, Zc1 zc1, Cd1.Ai ai, Mn0<Yc1<?>> mn0, Ci ci) {
        this.b = new Ei();
        this.c = Mk1.a();
        this.l = new AtomicInteger();
        this.h = le1;
        this.i = le12;
        this.j = le13;
        this.k = le14;
        this.g = zc1;
        this.d = ai;
        this.e = mn0;
        this.f = ci;
    }

    @DexIgnore
    @Override // com.fossil.Uc1.Bi
    public void a(Dd1 dd1) {
        synchronized (this) {
            this.z = dd1;
        }
        n();
    }

    @DexIgnore
    public void b(Gj1 gj1, Executor executor) {
        boolean z2 = true;
        synchronized (this) {
            this.c.c();
            this.b.a(gj1, executor);
            if (this.y) {
                k(1);
                executor.execute(new Bi(gj1));
            } else if (this.A) {
                k(1);
                executor.execute(new Ai(gj1));
            } else {
                if (this.D) {
                    z2 = false;
                }
                Ik1.a(z2, "Cannot add callbacks to a cancelled EngineJob");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Id1<R> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Uc1.Bi
    public void c(Id1<R> id1, Gb1 gb1) {
        synchronized (this) {
            this.w = id1;
            this.x = gb1;
        }
        o();
    }

    @DexIgnore
    @Override // com.fossil.Uc1.Bi
    public void d(Uc1<?> uc1) {
        j().execute(uc1);
    }

    @DexIgnore
    public void e(Gj1 gj1) {
        try {
            gj1.a(this.z);
        } catch (Throwable th) {
            throw new Oc1(th);
        }
    }

    @DexIgnore
    @Override // com.fossil.Kk1.Fi
    public Mk1 f() {
        return this.c;
    }

    @DexIgnore
    public void g(Gj1 gj1) {
        try {
            gj1.c(this.B, this.x);
        } catch (Throwable th) {
            throw new Oc1(th);
        }
    }

    @DexIgnore
    public void h() {
        if (!m()) {
            this.D = true;
            this.C.c();
            this.g.c(this, this.m);
        }
    }

    @DexIgnore
    public void i() {
        Cd1<?> cd1;
        synchronized (this) {
            this.c.c();
            Ik1.a(m(), "Not yet complete!");
            int decrementAndGet = this.l.decrementAndGet();
            Ik1.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                cd1 = this.B;
                q();
            } else {
                cd1 = null;
            }
        }
        if (cd1 != null) {
            cd1.g();
        }
    }

    @DexIgnore
    public final Le1 j() {
        return this.t ? this.j : this.u ? this.k : this.i;
    }

    @DexIgnore
    public void k(int i2) {
        synchronized (this) {
            Ik1.a(m(), "Not yet complete!");
            if (this.l.getAndAdd(i2) == 0 && this.B != null) {
                this.B.a();
            }
        }
    }

    @DexIgnore
    public Yc1<R> l(Mb1 mb1, boolean z2, boolean z3, boolean z4, boolean z5) {
        synchronized (this) {
            this.m = mb1;
            this.s = z2;
            this.t = z3;
            this.u = z4;
            this.v = z5;
        }
        return this;
    }

    @DexIgnore
    public final boolean m() {
        return this.A || this.y || this.D;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        r4.g.b(r4, r0, null);
        r1 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003e, code lost:
        if (r1.hasNext() == false) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        r0 = r1.next();
        r0.b.execute(new com.fossil.Yc1.Ai(r4, r0.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0053, code lost:
        i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void n() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.fossil.Mk1 r0 = r4.c     // Catch:{ all -> 0x005f }
            r0.c()     // Catch:{ all -> 0x005f }
            boolean r0 = r4.D     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x000f
            r4.q()     // Catch:{ all -> 0x005f }
            monitor-exit(r4)     // Catch:{ all -> 0x005f }
        L_0x000e:
            return
        L_0x000f:
            com.fossil.Yc1$Ei r0 = r4.b     // Catch:{ all -> 0x005f }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0062
            boolean r0 = r4.A     // Catch:{ all -> 0x005f }
            if (r0 != 0) goto L_0x0057
            r0 = 1
            r4.A = r0     // Catch:{ all -> 0x005f }
            com.fossil.Mb1 r0 = r4.m     // Catch:{ all -> 0x005f }
            com.fossil.Yc1$Ei r1 = r4.b     // Catch:{ all -> 0x005f }
            com.fossil.Yc1$Ei r1 = r1.c()     // Catch:{ all -> 0x005f }
            int r2 = r1.size()     // Catch:{ all -> 0x005f }
            int r2 = r2 + 1
            r4.k(r2)     // Catch:{ all -> 0x005f }
            monitor-exit(r4)     // Catch:{ all -> 0x005f }
            com.fossil.Zc1 r2 = r4.g
            r3 = 0
            r2.b(r4, r0, r3)
            java.util.Iterator r1 = r1.iterator()
        L_0x003a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0053
            java.lang.Object r0 = r1.next()
            com.fossil.Yc1$Di r0 = (com.fossil.Yc1.Di) r0
            java.util.concurrent.Executor r2 = r0.b
            com.fossil.Yc1$Ai r3 = new com.fossil.Yc1$Ai
            com.fossil.Gj1 r0 = r0.a
            r3.<init>(r0)
            r2.execute(r3)
            goto L_0x003a
        L_0x0053:
            r4.i()
            goto L_0x000e
        L_0x0057:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Already failed once"
            r0.<init>(r1)
            throw r0
        L_0x005f:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0062:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Received an exception without any callbacks to notify"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yc1.n():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        r5.g.b(r5, r1, r2);
        r1 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0054, code lost:
        if (r1.hasNext() == false) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0056, code lost:
        r0 = r1.next();
        r0.b.execute(new com.fossil.Yc1.Bi(r5, r0.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0069, code lost:
        i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void o() {
        /*
            r5 = this;
            monitor-enter(r5)
            com.fossil.Mk1 r0 = r5.c     // Catch:{ all -> 0x0075 }
            r0.c()     // Catch:{ all -> 0x0075 }
            boolean r0 = r5.D     // Catch:{ all -> 0x0075 }
            if (r0 == 0) goto L_0x0014
            com.fossil.Id1<?> r0 = r5.w     // Catch:{ all -> 0x0075 }
            r0.b()     // Catch:{ all -> 0x0075 }
            r5.q()     // Catch:{ all -> 0x0075 }
            monitor-exit(r5)     // Catch:{ all -> 0x0075 }
        L_0x0013:
            return
        L_0x0014:
            com.fossil.Yc1$Ei r0 = r5.b     // Catch:{ all -> 0x0075 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0075 }
            if (r0 != 0) goto L_0x0078
            boolean r0 = r5.y     // Catch:{ all -> 0x0075 }
            if (r0 != 0) goto L_0x006d
            com.fossil.Yc1$Ci r0 = r5.f     // Catch:{ all -> 0x0075 }
            com.fossil.Id1<?> r1 = r5.w     // Catch:{ all -> 0x0075 }
            boolean r2 = r5.s     // Catch:{ all -> 0x0075 }
            com.fossil.Mb1 r3 = r5.m     // Catch:{ all -> 0x0075 }
            com.fossil.Cd1$Ai r4 = r5.d     // Catch:{ all -> 0x0075 }
            com.fossil.Cd1 r0 = r0.a(r1, r2, r3, r4)     // Catch:{ all -> 0x0075 }
            r5.B = r0     // Catch:{ all -> 0x0075 }
            r0 = 1
            r5.y = r0     // Catch:{ all -> 0x0075 }
            com.fossil.Yc1$Ei r0 = r5.b     // Catch:{ all -> 0x0075 }
            com.fossil.Yc1$Ei r0 = r0.c()     // Catch:{ all -> 0x0075 }
            int r1 = r0.size()     // Catch:{ all -> 0x0075 }
            int r1 = r1 + 1
            r5.k(r1)     // Catch:{ all -> 0x0075 }
            com.fossil.Mb1 r1 = r5.m     // Catch:{ all -> 0x0075 }
            com.fossil.Cd1<?> r2 = r5.B     // Catch:{ all -> 0x0075 }
            monitor-exit(r5)     // Catch:{ all -> 0x0075 }
            com.fossil.Zc1 r3 = r5.g
            r3.b(r5, r1, r2)
            java.util.Iterator r1 = r0.iterator()
        L_0x0050:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r1.next()
            com.fossil.Yc1$Di r0 = (com.fossil.Yc1.Di) r0
            java.util.concurrent.Executor r2 = r0.b
            com.fossil.Yc1$Bi r3 = new com.fossil.Yc1$Bi
            com.fossil.Gj1 r0 = r0.a
            r3.<init>(r0)
            r2.execute(r3)
            goto L_0x0050
        L_0x0069:
            r5.i()
            goto L_0x0013
        L_0x006d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Already have resource"
            r0.<init>(r1)
            throw r0
        L_0x0075:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0078:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Received a resource without any callbacks to notify"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Yc1.o():void");
    }

    @DexIgnore
    public boolean p() {
        return this.v;
    }

    @DexIgnore
    public final void q() {
        synchronized (this) {
            if (this.m != null) {
                this.b.clear();
                this.m = null;
                this.B = null;
                this.w = null;
                this.A = false;
                this.D = false;
                this.y = false;
                this.C.y(false);
                this.C = null;
                this.z = null;
                this.x = null;
                this.e.a(this);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    @DexIgnore
    public void r(Gj1 gj1) {
        synchronized (this) {
            this.c.c();
            this.b.e(gj1);
            if (this.b.isEmpty()) {
                h();
                if ((this.y || this.A) && this.l.get() == 0) {
                    q();
                }
            }
        }
    }

    @DexIgnore
    public void s(Uc1<R> uc1) {
        synchronized (this) {
            this.C = uc1;
            (uc1.E() ? this.h : j()).execute(uc1);
        }
    }
}
