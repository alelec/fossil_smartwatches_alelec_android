package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.TypedValue;
import android.webkit.MimeTypeMap;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J61 implements E61<Uri> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ V51 b;

    @DexIgnore
    public J61(Context context, V51 v51) {
        Wg6.c(context, "context");
        Wg6.c(v51, "drawableDecoder");
        this.a = context;
        this.b = v51;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return e(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Uri uri) {
        return f(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, Uri uri, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, uri, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, Uri uri, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        Integer c;
        Drawable c2;
        String authority = uri.getAuthority();
        if (authority != null) {
            Wg6.b(authority, "it");
            String str = Ao7.a(Vt7.l(authority) ^ true).booleanValue() ? authority : null;
            if (str != null) {
                Wg6.b(str, "data.authority?.takeIf {\u2026InvalidUriException(data)");
                List<String> pathSegments = uri.getPathSegments();
                Wg6.b(pathSegments, "data.pathSegments");
                String str2 = (String) Pm7.R(pathSegments);
                if (str2 == null || (c = Ut7.c(str2)) == null) {
                    g(uri);
                    throw null;
                }
                int intValue = c.intValue();
                Resources resourcesForApplication = this.a.getPackageManager().getResourcesForApplication(str);
                TypedValue typedValue = new TypedValue();
                resourcesForApplication.getValue(intValue, typedValue, true);
                CharSequence charSequence = typedValue.string;
                Wg6.b(charSequence, "path");
                String obj = charSequence.subSequence(Wt7.K(charSequence, '/', 0, false, 6, null), charSequence.length()).toString();
                MimeTypeMap singleton = MimeTypeMap.getSingleton();
                Wg6.b(singleton, "MimeTypeMap.getSingleton()");
                String g = W81.g(singleton, obj);
                if (Wg6.a(g, "text/xml")) {
                    if (Wg6.a(str, this.a.getPackageName())) {
                        c2 = T81.a(this.a, intValue);
                    } else {
                        Context context = this.a;
                        Wg6.b(resourcesForApplication, "resources");
                        c2 = T81.c(context, resourcesForApplication, intValue);
                    }
                    boolean o = W81.o(c2);
                    if (o) {
                        Bitmap a2 = this.b.a(c2, f81, x51.d());
                        Resources resources = this.a.getResources();
                        Wg6.b(resources, "context.resources");
                        c2 = new BitmapDrawable(resources, a2);
                    }
                    return new C61(c2, o, Q51.MEMORY);
                }
                InputStream openRawResource = resourcesForApplication.openRawResource(intValue);
                Wg6.b(openRawResource, "resources.openRawResource(resId)");
                return new K61(S48.d(S48.l(openRawResource)), g, Q51.MEMORY);
            }
        }
        g(uri);
        throw null;
    }

    @DexIgnore
    public boolean e(Uri uri) {
        Wg6.c(uri, "data");
        return Wg6.a(uri.getScheme(), "android.resource");
    }

    @DexIgnore
    public String f(Uri uri) {
        Wg6.c(uri, "data");
        StringBuilder sb = new StringBuilder();
        sb.append(uri);
        sb.append('-');
        Resources resources = this.a.getResources();
        Wg6.b(resources, "context.resources");
        Configuration configuration = resources.getConfiguration();
        Wg6.b(configuration, "context.resources.configuration");
        sb.append(W81.h(configuration));
        return sb.toString();
    }

    @DexIgnore
    public final Void g(Uri uri) {
        throw new IllegalStateException("Invalid android.resource URI: " + uri);
    }
}
