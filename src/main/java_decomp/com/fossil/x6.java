package com.fossil;

import com.facebook.internal.NativeProtocol;
import com.fossil.wearables.fsl.enums.ActivityIntensity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class X6 extends Enum<X6> {
    @DexIgnore
    public static /* final */ X6 c;
    @DexIgnore
    public static /* final */ X6 d;
    @DexIgnore
    public static /* final */ X6 e;
    @DexIgnore
    public static /* final */ X6 f;
    @DexIgnore
    public static /* final */ X6 g;
    @DexIgnore
    public static /* final */ X6 h;
    @DexIgnore
    public static /* final */ X6 i;
    @DexIgnore
    public static /* final */ X6 j;
    @DexIgnore
    public static /* final */ X6 k;
    @DexIgnore
    public static /* final */ X6 l;
    @DexIgnore
    public static /* final */ /* synthetic */ X6[] m;
    @DexIgnore
    public static /* final */ W6 n; // = new W6(null);
    @DexIgnore
    public /* final */ int b;

    /*
    static {
        X6 x6 = new X6("SUCCESS", 0, 0);
        c = x6;
        X6 x62 = new X6("UNSUPPORTED_FILE_HANDLE", 1, 1);
        d = x62;
        X6 x63 = new X6("READ_NOT_PERMITTED", 2, 2);
        X6 x64 = new X6("WRITE_NOT_PERMITTED", 3, 3);
        X6 x65 = new X6("INVALID_PDU", 4, 4);
        X6 x66 = new X6("INSUFFICIENT_AUTHENTICATION", 5, 5);
        X6 x67 = new X6("REQUEST_NOT_SUPPORTED", 6, 6);
        e = x67;
        X6 x68 = new X6("INVALID_OFFSET", 7, 7);
        X6 x69 = new X6("INSUFFICIENT_AUTHORIZATION", 8, 8);
        X6 x610 = new X6("PREPARE_QUEUE_FULL", 9, 9);
        X6 x611 = new X6("ATTRIBUTE_NOT_FOUND", 10, 10);
        X6 x612 = new X6("ATTRIBUTE_NOT_LONG", 11, 11);
        X6 x613 = new X6("INSUFFICIENT_ENCRYPTION_KEY_SIZE", 12, 12);
        X6 x614 = new X6("INVALID_ATTRIBUTE_VALUE_LEN", 13, 13);
        X6 x615 = new X6("OTHER_UNLIKELY_ERROR", 14, 14);
        X6 x616 = new X6("INSUFFICIENT_ENCRYPTION", 15, 15);
        X6 x617 = new X6("UNSUPPORTED_GROUP_TYPE", 16, 16);
        X6 x618 = new X6("INSUFFICIENT_RESOURCES", 17, 17);
        X6 x619 = new X6("OUT_OF_MEMORY", 18, 112);
        X6 x620 = new X6("TRANSACTION_TIMEOUT", 19, 113);
        X6 x621 = new X6("TRANSACTION_OVERFLOW", 20, 114);
        X6 x622 = new X6("INVALID_RESPONSE_PDU", 21, 115);
        X6 x623 = new X6("REQUEST_CANCELLED", 22, 116);
        X6 x624 = new X6("OTHER_UNDEFINED_ERROR", 23, 117);
        X6 x625 = new X6("REQUIRED_CHARACTERISTIC_NOT_FOUND", 24, 118);
        X6 x626 = new X6("ATTRIBUTE_PDU_LENGTH_EXCEEDED_MTU_SIZE", 25, 119);
        X6 x627 = new X6("PROCEDURE_CONTINUING", 26, 120);
        X6 x628 = new X6("NO_RESOURCES", 27, 128);
        X6 x629 = new X6("INTERNAL_ERROR", 28, 129);
        X6 x630 = new X6("WRONG_STATE", 29, 130);
        X6 x631 = new X6("DB_FULL", 30, 131);
        X6 x632 = new X6("BUSY", 31, 132);
        X6 x633 = new X6("ERROR", 32, 133);
        X6 x634 = new X6("CMD_STARTED", 33, 134);
        X6 x635 = new X6("ILLEGAL_PARAMETER", 34, 135);
        X6 x636 = new X6("PENDING", 35, 136);
        X6 x637 = new X6("AUTH_FAIL", 36, 137);
        X6 x638 = new X6("MORE", 37, 138);
        X6 x639 = new X6("INVALID_CFG", 38, 139);
        X6 x640 = new X6("SERVICE_STARTED", 39, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        X6 x641 = new X6("ENCRYPED_NO_MITM", 40, 141);
        X6 x642 = new X6("NOT_ENCRYPTED", 41, 142);
        X6 x643 = new X6("CONGESTED", 42, 143);
        X6 x644 = new X6("CCCD_IMPROPERLY_CONFIGURED", 43, 253);
        X6 x645 = new X6("PROCEDURE_ALREADY_IN_PROGRESS", 44, 254);
        X6 x646 = new X6("VALUE_OUT_OF_RANGE", 45, 255);
        X6 x647 = new X6("GATT_FAILURE", 46, 257);
        X6 x648 = new X6("START_FAIL", 47, 65536);
        f = x648;
        X6 x649 = new X6("HID_PROXY_NOT_CONNECTED", 48, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY);
        g = x649;
        X6 x650 = new X6("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 49, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REQUEST);
        h = x650;
        X6 x651 = new X6("HID_INPUT_DEVICE_DISABLED", 50, NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REPLY);
        i = x651;
        X6 x652 = new X6("HID_UNKNOWN_ERROR", 51, NativeProtocol.MESSAGE_GET_INSTALL_DATA_REQUEST);
        j = x652;
        X6 x653 = new X6("BLUETOOTH_OFF", 52, 16777215);
        k = x653;
        X6 x654 = new X6("UNKNOWN", 53, 16777215);
        l = x654;
        m = new X6[]{x6, x62, x63, x64, x65, x66, x67, x68, x69, x610, x611, x612, x613, x614, x615, x616, x617, x618, x619, x620, x621, x622, x623, x624, x625, x626, x627, x628, x629, x630, x631, x632, x633, x634, x635, x636, x637, x638, x639, x640, x641, x642, x643, x644, x645, x646, x647, x648, x649, x650, x651, x652, x653, x654};
    }
    */

    @DexIgnore
    public X6(String str, int i2, int i3) {
        this.b = i3;
    }

    @DexIgnore
    public static X6 valueOf(String str) {
        return (X6) Enum.valueOf(X6.class, str);
    }

    @DexIgnore
    public static X6[] values() {
        return (X6[]) m.clone();
    }
}
