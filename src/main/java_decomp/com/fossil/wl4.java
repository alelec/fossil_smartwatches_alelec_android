package com.fossil;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wl4 {
    @DexIgnore
    public static /* final */ String[] b; // = {"UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT"};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{0, 327708, 327710, 327709, 656318}, new int[]{590318, 0, 327710, 327709, 656318}, new int[]{262158, 590300, 0, 590301, 932798}, new int[]{327709, 327708, 656318, 0, 327710}, new int[]{327711, 656380, 656382, 656381, 0}};
    @DexIgnore
    public static /* final */ int[][] d;
    @DexIgnore
    public static /* final */ int[][] e;
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Comparator<Yl4> {
        @DexIgnore
        public Ai(Wl4 wl4) {
        }

        @DexIgnore
        public int a(Yl4 yl4, Yl4 yl42) {
            return yl4.d() - yl42.d();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Yl4 yl4, Yl4 yl42) {
            return a(yl4, yl42);
        }
    }

    /*
    static {
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 5, 256);
        d = iArr;
        iArr[0][32] = 1;
        for (int i = 65; i <= 90; i++) {
            d[0][i] = (i - 65) + 2;
        }
        d[1][32] = 1;
        for (int i2 = 97; i2 <= 122; i2++) {
            d[1][i2] = (i2 - 97) + 2;
        }
        d[2][32] = 1;
        for (int i3 = 48; i3 <= 57; i3++) {
            d[2][i3] = (i3 - 48) + 2;
        }
        int[][] iArr2 = d;
        iArr2[2][44] = 12;
        iArr2[2][46] = 13;
        for (int i4 = 0; i4 < 28; i4++) {
            d[3][new int[]{0, 32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 27, 28, 29, 30, 31, 64, 92, 94, 95, 96, 124, 126, 127}[i4]] = i4;
        }
        int[] iArr3 = {0, 13, 0, 0, 0, 0, 33, 39, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 58, 59, 60, 61, 62, 63, 91, 93, 123, 125};
        for (int i5 = 0; i5 < 31; i5++) {
            if (iArr3[i5] > 0) {
                d[4][iArr3[i5]] = i5;
            }
        }
        int[][] iArr4 = (int[][]) Array.newInstance(Integer.TYPE, 6, 6);
        e = iArr4;
        for (int[] iArr5 : iArr4) {
            Arrays.fill(iArr5, -1);
        }
        int[][] iArr6 = e;
        iArr6[0][4] = 0;
        iArr6[1][4] = 0;
        iArr6[1][0] = 28;
        iArr6[3][4] = 0;
        iArr6[2][4] = 0;
        iArr6[2][0] = 15;
    }
    */

    @DexIgnore
    public Wl4(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    public static Collection<Yl4> b(Iterable<Yl4> iterable) {
        boolean z;
        LinkedList linkedList = new LinkedList();
        for (Yl4 yl4 : iterable) {
            Iterator it = linkedList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = true;
                    break;
                }
                Yl4 yl42 = (Yl4) it.next();
                if (yl42.f(yl4)) {
                    z = false;
                    break;
                } else if (yl4.f(yl42)) {
                    it.remove();
                }
            }
            if (z) {
                linkedList.add(yl4);
            }
        }
        return linkedList;
    }

    @DexIgnore
    public static void d(Yl4 yl4, int i, int i2, Collection<Yl4> collection) {
        Yl4 b2 = yl4.b(i);
        collection.add(b2.g(4, i2));
        if (yl4.e() != 4) {
            collection.add(b2.h(4, i2));
        }
        if (i2 == 3 || i2 == 4) {
            collection.add(b2.g(2, 16 - i2).g(2, 1));
        }
        if (yl4.c() > 0) {
            collection.add(yl4.a(i).a(i + 1));
        }
    }

    @DexIgnore
    public static Collection<Yl4> f(Iterable<Yl4> iterable, int i, int i2) {
        LinkedList linkedList = new LinkedList();
        for (Yl4 yl4 : iterable) {
            d(yl4, i, i2, linkedList);
        }
        return b(linkedList);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0049  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.Am4 a() {
        /*
            r8 = this;
            r7 = 32
            r3 = 0
            com.fossil.Yl4 r0 = com.fossil.Yl4.e
            java.util.List r2 = java.util.Collections.singletonList(r0)
            r1 = r3
        L_0x000a:
            byte[] r4 = r8.a
            int r0 = r4.length
            if (r1 >= r0) goto L_0x004f
            int r0 = r1 + 1
            int r5 = r4.length
            if (r0 >= r5) goto L_0x0035
            byte r4 = r4[r0]
        L_0x0016:
            byte[] r5 = r8.a
            byte r5 = r5[r1]
            r6 = 13
            if (r5 == r6) goto L_0x0043
            r6 = 44
            if (r5 == r6) goto L_0x003f
            r6 = 46
            if (r5 == r6) goto L_0x003b
            r6 = 58
            if (r5 == r6) goto L_0x0037
        L_0x002a:
            r4 = r3
        L_0x002b:
            if (r4 <= 0) goto L_0x0049
            java.util.Collection r1 = f(r2, r1, r4)
            r2 = r1
        L_0x0032:
            int r1 = r0 + 1
            goto L_0x000a
        L_0x0035:
            r4 = r3
            goto L_0x0016
        L_0x0037:
            if (r4 != r7) goto L_0x002a
            r4 = 5
            goto L_0x002b
        L_0x003b:
            if (r4 != r7) goto L_0x002a
            r4 = 3
            goto L_0x002b
        L_0x003f:
            if (r4 != r7) goto L_0x002a
            r4 = 4
            goto L_0x002b
        L_0x0043:
            r5 = 10
            if (r4 != r5) goto L_0x002a
            r4 = 2
            goto L_0x002b
        L_0x0049:
            java.util.Collection r2 = r8.e(r2, r1)
            r0 = r1
            goto L_0x0032
        L_0x004f:
            com.fossil.Wl4$Ai r0 = new com.fossil.Wl4$Ai
            r0.<init>(r8)
            java.lang.Object r0 = java.util.Collections.min(r2, r0)
            com.fossil.Yl4 r0 = (com.fossil.Yl4) r0
            byte[] r1 = r8.a
            com.fossil.Am4 r0 = r0.i(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wl4.a():com.fossil.Am4");
    }

    @DexIgnore
    public final void c(Yl4 yl4, int i, Collection<Yl4> collection) {
        Yl4 yl42;
        char c2 = (char) (this.a[i] & 255);
        boolean z = d[yl4.e()][c2] > 0;
        Yl4 yl43 = null;
        int i2 = 0;
        while (i2 <= 4) {
            int i3 = d[i2][c2];
            if (i3 > 0) {
                yl42 = yl43 == null ? yl4.b(i) : yl43;
                if (!z || i2 == yl4.e() || i2 == 2) {
                    collection.add(yl42.g(i2, i3));
                }
                if (!z && e[yl4.e()][i2] >= 0) {
                    collection.add(yl42.h(i2, i3));
                }
            } else {
                yl42 = yl43;
            }
            i2++;
            yl43 = yl42;
        }
        if (yl4.c() > 0 || d[yl4.e()][c2] == 0) {
            collection.add(yl4.a(i));
        }
    }

    @DexIgnore
    public final Collection<Yl4> e(Iterable<Yl4> iterable, int i) {
        LinkedList linkedList = new LinkedList();
        for (Yl4 yl4 : iterable) {
            c(yl4, i, linkedList);
        }
        return b(linkedList);
    }
}
