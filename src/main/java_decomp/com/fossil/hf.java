package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hf extends Lp {
    @DexIgnore
    public byte[] C;
    @DexIgnore
    public /* final */ ArrayList<Ow> D; // = By1.a(this.i, Hm7.c(Ow.f));
    @DexIgnore
    public /* final */ Rt E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public Hf(K5 k5, I60 i60, Rt rt, byte[] bArr) {
        super(k5, i60, Yp.Q, null, false, 24);
        this.E = rt;
        this.F = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        byte[] bArr = this.F;
        if (bArr.length != 8) {
            k(Zq.y);
        } else {
            Lp.i(this, new Ft(this.w, this.E, bArr), new Qq(this), Er.b, null, new Sr(this), null, 40, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(G80.k(super.C(), Jd0.u2, Ey1.a(this.E)), Jd0.o2, Dy1.e(this.F, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        String str = null;
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.q2;
        byte[] bArr = this.C;
        if (bArr != null) {
            str = Dy1.e(bArr, null, 1, null);
        }
        return G80.k(E2, jd0, str);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        byte[] bArr = this.C;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.D;
    }
}
