package com.fossil;

import android.util.Log;
import com.fossil.Z62;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Da2<R extends Z62> extends D72<R> implements A72<R> {
    @DexIgnore
    public C72<? super R, ? extends Z62> a;
    @DexIgnore
    public Da2<? extends Z62> b;
    @DexIgnore
    public volatile B72<? super R> c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Status e;
    @DexIgnore
    public /* final */ WeakReference<R62> f;
    @DexIgnore
    public /* final */ Fa2 g;

    @DexIgnore
    public static void c(Z62 z62) {
        if (z62 instanceof V62) {
            try {
                ((V62) z62).release();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(z62);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.A72
    public final void a(R r) {
        synchronized (this.d) {
            if (!r.a().D()) {
                h(r.a());
                c(r);
            } else if (this.a != null) {
                W92.a().submit(new Ca2(this, r));
            } else if (e()) {
                this.c.c(r);
            }
        }
    }

    @DexIgnore
    public final void d() {
        this.c = null;
    }

    @DexIgnore
    public final boolean e() {
        return (this.c == null || this.f.get() == null) ? false : true;
    }

    @DexIgnore
    public final void h(Status status) {
        synchronized (this.d) {
            this.e = status;
            j(status);
        }
    }

    @DexIgnore
    public final void j(Status status) {
        synchronized (this.d) {
            if (this.a != null) {
                Status a2 = this.a.a(status);
                Rc2.l(a2, "onFailure must not return null");
                this.b.h(a2);
            } else if (e()) {
                this.c.b(status);
            }
        }
    }
}
