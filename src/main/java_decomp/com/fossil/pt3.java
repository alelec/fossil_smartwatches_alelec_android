package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pt3 {
    @DexIgnore
    public static /* final */ Executor a; // = new Ai();
    @DexIgnore
    public static /* final */ Executor b; // = new Ku3();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Aa3(Looper.getMainLooper());

        @DexIgnore
        public final void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }
}
