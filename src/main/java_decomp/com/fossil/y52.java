package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y52 implements ServiceConnection {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ BlockingQueue<IBinder> b; // = new LinkedBlockingQueue();

    @DexIgnore
    public IBinder a(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        Rc2.j("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (!this.a) {
            this.a = true;
            IBinder poll = this.b.poll(j, timeUnit);
            if (poll != null) {
                return poll;
            }
            throw new TimeoutException("Timed out waiting for the service connection");
        }
        throw new IllegalStateException("Cannot call get on this connection more than once");
    }

    @DexIgnore
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.b.add(iBinder);
    }

    @DexIgnore
    public void onServiceDisconnected(ComponentName componentName) {
    }
}
