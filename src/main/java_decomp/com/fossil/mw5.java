package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mw5 extends Du0<GoalTrackingData, Ai> {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public Bi d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ Mw5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                if (this.b.d.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    Ai ai = this.b;
                    GoalTrackingData j = Mw5.j(ai.d, ai.getAdapterPosition());
                    if (j != null && (bi = this.b.d.d) != null) {
                        Wg6.b(j, "it1");
                        bi.Z2(j);
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Mw5 mw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.d = mw5;
            this.a = (FlexibleTextView) view.findViewById(2131362543);
            this.b = (FlexibleTextView) view.findViewById(2131362493);
            FlexibleTextView flexibleTextView = (FlexibleTextView) view.findViewById(2131362403);
            this.c = flexibleTextView;
            flexibleTextView.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            Wg6.c(goalTrackingData, "item");
            if (this.d.c == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + Dl5.b(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = Dl5.b(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                Wg6.b(str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            FlexibleTextView flexibleTextView = this.a;
            Wg6.b(flexibleTextView, "mTvTime");
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131887558);
            Wg6.b(c2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            String format = String.format(c2, Arrays.copyOf(new Object[]{TimeUtils.p(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str}, 2));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView.setText(format);
            FlexibleTextView flexibleTextView2 = this.b;
            Wg6.b(flexibleTextView2, "mTvNoTime");
            flexibleTextView2.setVisibility(8);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void Z2(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Mw5(Bi bi, Nq4 nq4) {
        super(nq4);
        Wg6.c(nq4, "goalTrackingDataDiff");
        this.d = bi;
        TimeZone timeZone = TimeZone.getDefault();
        Wg6.b(timeZone, "TimeZone.getDefault()");
        this.c = TimeUtils.g0(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData j(Mw5 mw5, int i) {
        return (GoalTrackingData) mw5.getItem(i);
    }

    @DexIgnore
    public void m(Ai ai, int i) {
        Wg6.c(ai, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) getItem(i);
        if (goalTrackingData != null) {
            ai.a(goalTrackingData);
        }
    }

    @DexIgnore
    public Ai n(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558710, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new Ai(this, inflate);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        m((Ai) viewHolder, i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return n(viewGroup, i);
    }
}
