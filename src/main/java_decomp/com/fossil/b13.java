package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B13 implements N23 {
    @DexIgnore
    public static /* final */ B13 a; // = new B13();

    @DexIgnore
    public static B13 a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.N23
    public final boolean zza(Class<?> cls) {
        return E13.class.isAssignableFrom(cls);
    }

    @DexIgnore
    @Override // com.fossil.N23
    public final K23 zzb(Class<?> cls) {
        if (!E13.class.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (K23) E13.o(cls.asSubclass(E13.class)).r(E13.Fi.c, null, null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
