package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mq6 implements Factory<ProfileOptInPresenter> {
    @DexIgnore
    public static ProfileOptInPresenter a(Hq6 hq6, UpdateUser updateUser, UserRepository userRepository) {
        return new ProfileOptInPresenter(hq6, updateUser, userRepository);
    }
}
