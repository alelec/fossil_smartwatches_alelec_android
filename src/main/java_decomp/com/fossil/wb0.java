package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class Wb0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Vv1.values().length];
        a = iArr;
        iArr[Vv1.WEATHER.ordinal()] = 1;
        a[Vv1.STEPS.ordinal()] = 2;
        a[Vv1.DATE.ordinal()] = 3;
        a[Vv1.CHANCE_OF_RAIN.ordinal()] = 4;
        a[Vv1.SECOND_TIMEZONE.ordinal()] = 5;
        a[Vv1.ACTIVE_MINUTES.ordinal()] = 6;
        a[Vv1.CALORIES.ordinal()] = 7;
        a[Vv1.BATTERY.ordinal()] = 8;
        a[Vv1.HEART_RATE.ordinal()] = 9;
    }
    */
}
