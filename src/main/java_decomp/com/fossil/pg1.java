package com.fossil;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pg1 implements Qb1<ParcelFileDescriptor, Bitmap> {
    @DexIgnore
    public /* final */ Gg1 a;

    @DexIgnore
    public Pg1(Gg1 gg1) {
        this.a = gg1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(ParcelFileDescriptor parcelFileDescriptor, Ob1 ob1) throws IOException {
        return d(parcelFileDescriptor, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, Ob1 ob1) throws IOException {
        return c(parcelFileDescriptor, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, Ob1 ob1) throws IOException {
        return this.a.d(parcelFileDescriptor, i, i2, ob1);
    }

    @DexIgnore
    public boolean d(ParcelFileDescriptor parcelFileDescriptor, Ob1 ob1) {
        return this.a.o(parcelFileDescriptor);
    }
}
