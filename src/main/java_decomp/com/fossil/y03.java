package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Y03 {
    b(0, A13.zza, N13.zze),
    c(1, A13.zza, N13.zzd),
    d(2, A13.zza, N13.zzc),
    e(3, A13.zza, N13.zzc),
    f(4, A13.zza, N13.zzb),
    g(5, A13.zza, N13.zzc),
    h(6, A13.zza, N13.zzb),
    i(7, A13.zza, N13.zzf),
    j(8, A13.zza, N13.zzg),
    k(9, A13.zza, N13.zzj),
    l(10, A13.zza, N13.zzh),
    m(11, A13.zza, N13.zzb),
    s(12, A13.zza, N13.zzi),
    t(13, A13.zza, N13.zzb),
    u(14, A13.zza, N13.zzc),
    v(15, A13.zza, N13.zzb),
    w(16, A13.zza, N13.zzc),
    x(17, A13.zza, N13.zzj),
    y(18, A13.zzb, N13.zze),
    z(19, A13.zzb, N13.zzd),
    A(20, A13.zzb, N13.zzc),
    B(21, A13.zzb, N13.zzc),
    C(22, A13.zzb, N13.zzb),
    D(23, A13.zzb, N13.zzc),
    E(24, A13.zzb, N13.zzb),
    F(25, A13.zzb, N13.zzf),
    G(26, A13.zzb, N13.zzg),
    H(27, A13.zzb, N13.zzj),
    I(28, A13.zzb, N13.zzh),
    J(29, A13.zzb, N13.zzb),
    K(30, A13.zzb, N13.zzi),
    L(31, A13.zzb, N13.zzb),
    M(32, A13.zzb, N13.zzc),
    N(33, A13.zzb, N13.zzb),
    O(34, A13.zzb, N13.zzc),
    zza(35, A13.zzc, N13.zze),
    P(36, A13.zzc, N13.zzd),
    Q(37, A13.zzc, N13.zzc),
    R(38, A13.zzc, N13.zzc),
    S(39, A13.zzc, N13.zzb),
    T(40, A13.zzc, N13.zzc),
    U(41, A13.zzc, N13.zzb),
    V(42, A13.zzc, N13.zzf),
    W(43, A13.zzc, N13.zzb),
    X(44, A13.zzc, N13.zzi),
    Y(45, A13.zzc, N13.zzb),
    Z(46, A13.zzc, N13.zzc),
    a0(47, A13.zzc, N13.zzb),
    zzb(48, A13.zzc, N13.zzc),
    b0(49, A13.zzb, N13.zzj),
    c0(50, A13.zzd, N13.zza);
    
    @DexIgnore
    public static /* final */ Y03[] d0;
    @DexIgnore
    public /* final */ N13 zzaz;
    @DexIgnore
    public /* final */ int zzba;
    @DexIgnore
    public /* final */ A13 zzbb;
    @DexIgnore
    public /* final */ Class<?> zzbc;
    @DexIgnore
    public /* final */ boolean zzbd;

    /*
    static {
        Y03[] values = values();
        d0 = new Y03[values.length];
        for (Y03 y03 : values) {
            d0[y03.zzba] = y03;
        }
    }
    */

    @DexIgnore
    public Y03(int i2, A13 a13, N13 n13) {
        int i3;
        boolean z2 = true;
        this.zzba = i2;
        this.zzbb = a13;
        this.zzaz = n13;
        int i4 = X03.a[a13.ordinal()];
        if (i4 == 1) {
            this.zzbc = n13.zza();
        } else if (i4 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = n13.zza();
        }
        this.zzbd = (a13 != A13.zza || (i3 = X03.b[n13.ordinal()]) == 1 || i3 == 2 || i3 == 3) ? false : z2;
    }

    @DexIgnore
    public final int zza() {
        return this.zzba;
    }
}
