package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Yh4 implements Callable {
    @DexIgnore
    public /* final */ Zh4 a;

    @DexIgnore
    public Yh4(Zh4 zh4) {
        this.a = zh4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return this.a.a();
    }
}
