package com.fossil;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.iq4;
import com.fossil.jb6;
import com.fossil.jn5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h76 extends d76 {
    @DexIgnore
    public k76 e;
    @DexIgnore
    public MutableLiveData<List<oo5>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<l66> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<l66> h;
    @DexIgnore
    public /* final */ e76 i;
    @DexIgnore
    public /* final */ jb6 j;
    @DexIgnore
    public /* final */ on5 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $it;
        @DexIgnore
        public /* final */ /* synthetic */ List $presetButtons$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h76 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h76$a$a")
        /* renamed from: com.fossil.h76$a$a  reason: collision with other inner class name */
        public static final class C0103a implements iq4.e<jb6.c, jb6.a> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ a f1438a;
            @DexIgnore
            public /* final */ /* synthetic */ ul5 b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h76$a$a$a")
            /* renamed from: com.fossil.h76$a$a$a  reason: collision with other inner class name */
            public static final class C0104a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ C0103a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h76$a$a$a$a")
                /* renamed from: com.fossil.h76$a$a$a$a  reason: collision with other inner class name */
                public static final class C0105a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0104a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0105a(C0104a aVar, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0105a aVar = new C0105a(this.this$0, qn7);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        throw null;
                        //return ((C0105a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            k76 x = h76.x(this.this$0.this$0.f1438a.this$0);
                            this.L$0 = iv7;
                            this.label = 1;
                            if (x.E(this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return tl7.f3441a;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0104a(C0103a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0104a aVar = new C0104a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0104a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        dv7 b = bw7.b();
                        C0105a aVar = new C0105a(this, null);
                        this.L$0 = iv7;
                        this.label = 1;
                        if (eu7.g(b, aVar, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.f1438a.this$0.i.w();
                    this.this$0.f1438a.this$0.i.t0(true);
                    return tl7.f3441a;
                }
            }

            @DexIgnore
            public C0103a(a aVar, ul5 ul5) {
                this.f1438a = aVar;
                this.b = ul5;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(jb6.a aVar) {
                pq7.c(aVar, "errorValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppEditPresenter", "setToWatch onError, errorCode = " + aVar);
                this.f1438a.this$0.i.w();
                int b2 = aVar.b();
                if (b2 != 1101) {
                    if (b2 == 8888) {
                        this.f1438a.this$0.i.c();
                    } else if (!(b2 == 1112 || b2 == 1113)) {
                        this.f1438a.this$0.i.P();
                    }
                    String arrayList = aVar.a().toString();
                    ul5 ul5 = this.b;
                    pq7.b(arrayList, "errorCode");
                    ul5.c(arrayList);
                    return;
                }
                List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(aVar.a());
                pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                e76 e76 = this.f1438a.this$0.i;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
                if (array != null) {
                    uh5[] uh5Arr = (uh5[]) array;
                    e76.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    String arrayList2 = aVar.a().toString();
                    ul5 ul52 = this.b;
                    pq7.b(arrayList2, "errorCode");
                    ul52.c(arrayList2);
                    return;
                }
                throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(jb6.c cVar) {
                pq7.c(cVar, "responseValue");
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "setToWatch success");
                this.b.c("");
                xw7 unused = gu7.d(this.f1438a.this$0.k(), null, null, new C0104a(this, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(List list, qn7 qn7, h76 h76, boolean z, List list2) {
            super(2, qn7);
            this.$it = list;
            this.this$0 = h76;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$presetButtons$inlined = list2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$it, qn7, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$presetButtons$inlined);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v13, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0100  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0191  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 409
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.h76.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<List<? extends oo5>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h76 f1439a;

        @DexIgnore
        public b(h76 h76) {
            this.f1439a = h76;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(List<oo5> list) {
            MutableLiveData mutableLiveData = this.f1439a.f;
            if (list != null) {
                mutableLiveData.l(kj5.a(list));
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h76 f1440a;

        @DexIgnore
        public c(h76 h76) {
            this.f1440a = h76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "start - observe selected watchApp value=" + str);
            e76 e76 = this.f1440a.i;
            if (str != null) {
                e76.W5(str);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Boolean> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h76 f1441a;

        @DexIgnore
        public d(h76 h76) {
            this.f1441a = h76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "update preset status isChanged=" + bool);
            e76 e76 = this.f1441a.i;
            if (bool != null) {
                e76.s0(bool.booleanValue());
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<l66> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h76 f1442a;

        @DexIgnore
        public e(h76 h76) {
            this.f1442a = h76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(l66 l66) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "show preset button" + l66);
            e76 e76 = this.f1442a.i;
            pq7.b(l66, "it");
            e76.a2(l66);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ h76 f1443a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter$wrapperButtonPrestTransformations$1$1", f = "WatchAppEditPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<l66>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $buttons;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$buttons = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$buttons, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<l66> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    hs0 hs0 = this.p$;
                    hl5 hl5 = hl5.f1493a;
                    List<oo5> list = this.$buttons;
                    pq7.b(list, "buttons");
                    List<jn5.a> a2 = hl5.a(list);
                    mo5 p = h76.x(this.this$0.f1443a).p();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppEditPresenter", "receive buttons " + this.$buttons + " currentPreset " + p);
                    ArrayList arrayList = new ArrayList();
                    List<oo5> list2 = this.$buttons;
                    pq7.b(list2, "buttons");
                    for (oo5 oo5 : list2) {
                        WatchApp o = h76.x(this.this$0.f1443a).o(oo5.a());
                        if (o != null) {
                            String watchappId = o.getWatchappId();
                            String icon = o.getIcon();
                            if (icon == null) {
                                icon = "";
                            }
                            ao7.a(arrayList.add(new n66(watchappId, icon, um5.d(PortfolioApp.h0.c(), o.getNameKey(), o.getName()), oo5.b(), null, 16, null)));
                        }
                    }
                    l66 l66 = (l66) this.this$0.f1443a.g.e();
                    if (l66 != null) {
                        this.this$0.f1443a.B(l66.b(), arrayList);
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppEditPresenter", "wrapperPresetTransformations presetChanged watchAppsDetail=" + arrayList);
                    if (p != null) {
                        this.this$0.f1443a.g.l(new l66(p.e(), p.f(), arrayList, a2, p.m()));
                    }
                    MutableLiveData mutableLiveData = this.this$0.f1443a.g;
                    this.L$0 = hs0;
                    this.L$1 = a2;
                    this.L$2 = p;
                    this.L$3 = arrayList;
                    this.label = 1;
                    if (hs0.a(mutableLiveData, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    ArrayList arrayList2 = (ArrayList) this.L$3;
                    mo5 mo5 = (mo5) this.L$2;
                    List list3 = (List) this.L$1;
                    hs0 hs02 = (hs0) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public f(h76 h76) {
            this.f1443a = h76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<l66> apply(List<oo5> list) {
            return or0.c(null, 0, new a(this, list, null), 3, null);
        }
    }

    @DexIgnore
    public h76(e76 e76, jb6 jb6, on5 on5) {
        pq7.c(e76, "mView");
        pq7.c(jb6, "mSetWatchAppUseCase");
        pq7.c(on5, "mSharedPreferencesManager");
        this.i = e76;
        this.j = jb6;
        this.k = on5;
        new Gson();
        LiveData<l66> c2 = ss0.c(this.f, new f(this));
        pq7.b(c2, "Transformations.switchMa\u2026erPreset)\n        }\n    }");
        this.h = c2;
    }

    @DexIgnore
    public static final /* synthetic */ k76 x(h76 h76) {
        k76 k76 = h76.e;
        if (k76 != null) {
            return k76;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public final void A(String str, String str2, String str3) {
        sl5 b2 = ck5.f.b("set_watch_apps_manually");
        b2.a("button", str);
        b2.a("old_app", str2);
        b2.a("new_app", str3);
        b2.b();
    }

    @DexIgnore
    public final void B(List<n66> list, List<n66> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!pq7.a(list.get(i2).a(), list2.get(i2).a())) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                n66 n66 = list.get(i3);
                n66 n662 = list2.get(i3);
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, item at " + i3 + " position: " + n66.c() + ", oldId: " + n66.a() + ", newId: " + n662.a());
                String c2 = n662.c();
                if (c2 != null) {
                    int hashCode = c2.hashCode();
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            A(ViewHierarchy.DIMENSION_TOP_KEY, n66.a(), n662.a());
                        }
                    } else if (c2.equals("middle")) {
                        A("middle", n66.a(), n662.a());
                    }
                }
                A("bottom", n66.a(), n662.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public Bundle C(Bundle bundle) {
        k76 k76 = this.e;
        if (k76 != null) {
            List<oo5> e2 = k76.q().e();
            if (!(e2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", new Gson().t(e2));
            }
            k76 k762 = this.e;
            if (k762 != null) {
                List<oo5> s = k762.s();
                if (!(s == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", new Gson().t(s));
                }
                k76 k763 = this.e;
                if (k763 != null) {
                    String e3 = k763.v().e();
                    if (!(e3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", e3);
                    }
                    return bundle;
                }
                pq7.n("mWatchAppEditViewModel");
                throw null;
            }
            pq7.n("mWatchAppEditViewModel");
            throw null;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public void D() {
        this.i.M5(this);
    }

    @DexIgnore
    public final void E(List<oo5> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditPresenter", "buttons=" + list);
        k76 k76 = this.e;
        if (k76 != null) {
            k76.G(list);
        } else {
            pq7.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.j.o();
        wq5.d.g(CommunicateMode.SET_WATCH_APPS);
        k76 k76 = this.e;
        if (k76 != null) {
            MutableLiveData<List<oo5>> q = k76.q();
            e76 e76 = this.i;
            if (e76 != null) {
                q.h((f76) e76, new b(this));
                k76 k762 = this.e;
                if (k762 != null) {
                    k762.v().h((LifecycleOwner) this.i, new c(this));
                    k76 k763 = this.e;
                    if (k763 != null) {
                        k763.r().h((LifecycleOwner) this.i, new d(this));
                        this.h.h((LifecycleOwner) this.i, new e(this));
                        return;
                    }
                    pq7.n("mWatchAppEditViewModel");
                    throw null;
                }
                pq7.n("mWatchAppEditViewModel");
                throw null;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditFragment");
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.j.r();
        k76 k76 = this.e;
        if (k76 != null) {
            MutableLiveData<String> v = k76.v();
            e76 e76 = this.i;
            if (e76 != null) {
                v.n((f76) e76);
                this.g.n((LifecycleOwner) this.i);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditFragment");
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void n(String str, String str2) {
        T t;
        pq7.c(str, "watchAppId");
        pq7.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "dropWatchApp - watchAppId=" + str + ", toPosition=" + str2);
        k76 k76 = this.e;
        if (k76 != null) {
            if (!k76.D(str)) {
                k76 k762 = this.e;
                if (k762 != null) {
                    List<oo5> e2 = k762.q().e();
                    if (e2 != null) {
                        pq7.b(e2, "presetButtons");
                        Iterator<T> it = e2.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            T next = it.next();
                            if (pq7.a(next.b(), str2)) {
                                t = next;
                                break;
                            }
                        }
                        T t2 = t;
                        if (t2 != null) {
                            t2.d(str);
                        }
                        E(e2);
                    }
                } else {
                    pq7.n("mWatchAppEditViewModel");
                    throw null;
                }
            }
            if (str.hashCode() == -829740640 && str.equals("commute-time") && !this.k.t()) {
                this.k.s1(true);
                this.i.i0("commute-time");
                return;
            }
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void o() {
        k76 k76 = this.e;
        if (k76 != null) {
            boolean C = k76.C();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "isPresetChanged " + C);
            if (C) {
                this.i.L();
            } else {
                this.i.t0(false);
            }
        } else {
            pq7.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void p(boolean z) {
        k76 k76 = this.e;
        if (k76 != null) {
            List<oo5> e2 = k76.q().e();
            if (e2 != null) {
                xw7 unused = gu7.d(k(), null, null, new a(e2, null, this, z, e2), 3, null);
                return;
            }
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void q(k76 k76) {
        pq7.c(k76, "viewModel");
        this.e = k76;
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void r(String str) {
        pq7.c(str, "watchAppPos");
        k76 k76 = this.e;
        if (k76 != null) {
            k76.F(str);
        } else {
            pq7.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d76
    public void s(String str, String str2) {
        T t;
        T t2;
        pq7.c(str, "fromPosition");
        pq7.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "swapWatchApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!pq7.a(str, str2)) {
            k76 k76 = this.e;
            if (k76 != null) {
                List<oo5> e2 = k76.q().e();
                if (e2 != null) {
                    pq7.b(e2, "buttons");
                    Iterator<T> it = e2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (pq7.a(next.b(), str)) {
                            t = next;
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = e2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        T next2 = it2.next();
                        if (pq7.a(next2.b(), str2)) {
                            t2 = next2;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.e(str2);
                    }
                    if (t4 != null) {
                        t4.e(str);
                    }
                    FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "swapWatchApp - update preset");
                    E(e2);
                    return;
                }
                return;
            }
            pq7.n("mWatchAppEditViewModel");
            throw null;
        }
    }
}
