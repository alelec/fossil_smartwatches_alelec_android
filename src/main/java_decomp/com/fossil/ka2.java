package com.fossil;

import android.os.RemoteException;
import com.fossil.L72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ka2 extends V92<Void> {
    @DexIgnore
    public /* final */ U92 c;

    @DexIgnore
    public Ka2(U92 u92, Ot3<Void> ot3) {
        super(3, ot3);
        this.c = u92;
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final /* bridge */ /* synthetic */ void d(A82 a82, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final B62[] g(L72.Ai<?> ai) {
        return this.c.a.c();
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final boolean h(L72.Ai<?> ai) {
        return this.c.a.e();
    }

    @DexIgnore
    @Override // com.fossil.V92
    public final void i(L72.Ai<?> ai) throws RemoteException {
        this.c.a.d(ai.R(), this.b);
        if (this.c.a.b() != null) {
            ai.A().put(this.c.a.b(), this.c);
        }
    }
}
