package com.fossil;

import com.fossil.imagefilters.FilterType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class R96 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[FilterType.values().length];
        a = iArr;
        iArr[FilterType.ATKINSON_DITHERING.ordinal()] = 1;
        a[FilterType.BURKES_DITHERING.ordinal()] = 2;
        a[FilterType.DIRECT_MAPPING.ordinal()] = 3;
        a[FilterType.JAJUNI_DITHERING.ordinal()] = 4;
        a[FilterType.SIERRA_DITHERING.ordinal()] = 5;
        a[FilterType.ORDERED_DITHERING.ordinal()] = 6;
    }
    */
}
