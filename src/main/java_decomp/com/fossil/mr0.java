package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mr0<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new Bi();
    @DexIgnore
    public /* final */ Runnable f; // = new Ci();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends LiveData<T> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void j() {
            Mr0 mr0 = Mr0.this;
            mr0.a.execute(mr0.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v4, resolved type: androidx.lifecycle.LiveData<T> */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            boolean z;
            do {
                if (Mr0.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (Mr0.this.c.compareAndSet(true, false)) {
                        try {
                            obj = Mr0.this.a();
                            z = true;
                        } finally {
                            Mr0.this.d.set(false);
                        }
                    }
                    if (z) {
                        Mr0.this.b.l(obj);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (Mr0.this.c.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void run() {
            boolean g = Mr0.this.b.g();
            if (Mr0.this.c.compareAndSet(false, true) && g) {
                Mr0 mr0 = Mr0.this;
                mr0.a.execute(mr0.e);
            }
        }
    }

    @DexIgnore
    public Mr0(Executor executor) {
        this.a = executor;
        this.b = new Ai();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        Bi0.f().b(this.f);
    }
}
