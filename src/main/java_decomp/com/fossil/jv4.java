package com.fossil;

import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jv4 implements Factory<BCLeaderBoardViewModel> {
    @DexIgnore
    public /* final */ Provider<Tt4> a;

    @DexIgnore
    public Jv4(Provider<Tt4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Jv4 a(Provider<Tt4> provider) {
        return new Jv4(provider);
    }

    @DexIgnore
    public static BCLeaderBoardViewModel c(Tt4 tt4) {
        return new BCLeaderBoardViewModel(tt4);
    }

    @DexIgnore
    public BCLeaderBoardViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
