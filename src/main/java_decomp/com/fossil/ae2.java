package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ae2 implements Parcelable.Creator<Sc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Sc2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        GoogleSignInAccount googleSignInAccount = null;
        Account account = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i2 = Ad2.v(parcel, t);
            } else if (l == 2) {
                account = (Account) Ad2.e(parcel, t, Account.CREATOR);
            } else if (l == 3) {
                i = Ad2.v(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                googleSignInAccount = (GoogleSignInAccount) Ad2.e(parcel, t, GoogleSignInAccount.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Sc2(i2, account, i, googleSignInAccount);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Sc2[] newArray(int i) {
        return new Sc2[i];
    }
}
