package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class An5 implements Factory<CustomizeRealDataManager> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> c;

    @DexIgnore
    public An5(Provider<PortfolioApp> provider, Provider<DeviceRepository> provider2, Provider<DianaAppSettingRepository> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static An5 a(Provider<PortfolioApp> provider, Provider<DeviceRepository> provider2, Provider<DianaAppSettingRepository> provider3) {
        return new An5(provider, provider2, provider3);
    }

    @DexIgnore
    public static CustomizeRealDataManager c(PortfolioApp portfolioApp, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        return new CustomizeRealDataManager(portfolioApp, deviceRepository, dianaAppSettingRepository);
    }

    @DexIgnore
    public CustomizeRealDataManager b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
