package com.fossil;

import android.content.Context;
import com.facebook.appevents.UserDataStore;
import com.mapped.Wg6;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import net.sqlcipher.DatabaseErrorHandler;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;
import net.sqlcipher.database.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nn5 {
    @DexIgnore
    public static /* final */ Nn5 a; // = new Nn5();

    @DexIgnore
    public enum Ai {
        DOES_NOT_EXIST,
        UNENCRYPTED,
        ENCRYPTED
    }

    @DexIgnore
    public final void a(Context context, File file, byte[] bArr) throws IOException {
        Wg6.c(context, "ctxt");
        Wg6.c(file, "originalFile");
        SQLiteDatabase.loadLibs(context);
        if (file.exists()) {
            File createTempFile = File.createTempFile("sqlcipherutils", "tmp", context.getCacheDir());
            SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), "", (SQLiteDatabase.CursorFactory) null, 0);
            Wg6.b(openDatabase, UserDataStore.DATE_OF_BIRTH);
            int version = openDatabase.getVersion();
            openDatabase.close();
            Wg6.b(createTempFile, "newFile");
            SQLiteDatabase openDatabase2 = SQLiteDatabase.openDatabase(createTempFile.getAbsolutePath(), bArr, (SQLiteDatabase.CursorFactory) null, 0, (SQLiteDatabaseHook) null, (DatabaseErrorHandler) null);
            SQLiteStatement compileStatement = openDatabase2.compileStatement("ATTACH DATABASE ? AS plaintext KEY ''");
            compileStatement.bindString(1, file.getAbsolutePath());
            compileStatement.execute();
            openDatabase2.rawExecSQL("SELECT sqlcipher_export('main', 'plaintext')");
            openDatabase2.rawExecSQL("DETACH DATABASE plaintext");
            Wg6.b(openDatabase2, UserDataStore.DATE_OF_BIRTH);
            openDatabase2.setVersion(version);
            compileStatement.close();
            openDatabase2.close();
            file.delete();
            createTempFile.renameTo(file);
            return;
        }
        throw new FileNotFoundException(file.getAbsolutePath() + " not found");
    }

    @DexIgnore
    public final void b(Context context, File file, char[] cArr) throws IOException {
        Wg6.c(context, "ctxt");
        Wg6.c(file, "originalFile");
        a(context, file, SQLiteDatabase.getBytes(cArr));
    }

    @DexIgnore
    public final Ai c(Context context, String str) {
        Wg6.c(context, "ctxt");
        SQLiteDatabase.loadLibs(context);
        File databasePath = context.getDatabasePath(str);
        Wg6.b(databasePath, "ctxt.getDatabasePath(dbName)");
        return d(databasePath);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Nn5.Ai d(java.io.File r6) {
        /*
            r5 = this;
            r2 = 0
            java.lang.String r0 = "dbPath"
            com.mapped.Wg6.c(r6, r0)
            boolean r0 = r6.exists()
            if (r0 == 0) goto L_0x0035
            java.lang.String r0 = r6.getAbsolutePath()     // Catch:{ Exception -> 0x0026, all -> 0x002d }
            java.lang.String r1 = ""
            r3 = 0
            r4 = 1
            net.sqlcipher.database.SQLiteDatabase r0 = net.sqlcipher.database.SQLiteDatabase.openDatabase(r0, r1, r3, r4)     // Catch:{ Exception -> 0x0026, all -> 0x002d }
            java.lang.String r1 = "db"
            com.mapped.Wg6.b(r0, r1)     // Catch:{ Exception -> 0x0038 }
            r0.getVersion()     // Catch:{ Exception -> 0x0038 }
            com.fossil.Nn5$Ai r1 = com.fossil.Nn5.Ai.UNENCRYPTED     // Catch:{ Exception -> 0x0038 }
        L_0x0022:
            r0.close()
        L_0x0025:
            return r1
        L_0x0026:
            r0 = move-exception
            r0 = r2
        L_0x0028:
            com.fossil.Nn5$Ai r1 = com.fossil.Nn5.Ai.ENCRYPTED     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x0025
            goto L_0x0022
        L_0x002d:
            r0 = move-exception
            r1 = r0
        L_0x002f:
            if (r2 == 0) goto L_0x0034
            r2.close()
        L_0x0034:
            throw r1
        L_0x0035:
            com.fossil.Nn5$Ai r1 = com.fossil.Nn5.Ai.DOES_NOT_EXIST
            goto L_0x0025
        L_0x0038:
            r1 = move-exception
            goto L_0x0028
        L_0x003a:
            r1 = move-exception
            r2 = r0
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nn5.d(java.io.File):com.fossil.Nn5$Ai");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(android.content.Context r10, java.lang.String r11, byte[] r12) {
        /*
            r9 = this;
            r6 = 1
            r7 = 0
            r8 = 0
            java.lang.String r0 = "ctxt"
            com.mapped.Wg6.c(r10, r0)
            net.sqlcipher.database.SQLiteDatabase.loadLibs(r10)
            java.io.File r0 = r10.getDatabasePath(r11)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0034
            java.lang.String r1 = "dbPath"
            com.mapped.Wg6.b(r0, r1)     // Catch:{ Exception -> 0x003d, all -> 0x0035 }
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x003d, all -> 0x0035 }
            r2 = 0
            r3 = 1
            r4 = 0
            r5 = 0
            r1 = r12
            net.sqlcipher.database.SQLiteDatabase r1 = net.sqlcipher.database.SQLiteDatabase.openDatabase(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x003d, all -> 0x0035 }
            java.lang.String r0 = "db"
            com.mapped.Wg6.b(r1, r0)     // Catch:{ Exception -> 0x0043, all -> 0x0045 }
            r1.getVersion()     // Catch:{ Exception -> 0x0043, all -> 0x0045 }
            r0 = r6
        L_0x0030:
            r1.close()
            r7 = r0
        L_0x0034:
            return r7
        L_0x0035:
            r0 = move-exception
            r1 = r8
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            throw r0
        L_0x003d:
            r0 = move-exception
            r1 = r8
        L_0x003f:
            if (r1 == 0) goto L_0x0034
            r0 = r7
            goto L_0x0030
        L_0x0043:
            r0 = move-exception
            goto L_0x003f
        L_0x0045:
            r0 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nn5.e(android.content.Context, java.lang.String, byte[]):boolean");
    }
}
