package com.fossil;

import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R9 extends Vx1<NotificationFilter[], NotificationFilter[]> {
    @DexIgnore
    public static /* final */ Qx1<NotificationFilter[]>[] b; // = {new L9(), new O9()};
    @DexIgnore
    public static /* final */ Rx1<NotificationFilter[]>[] c; // = new Rx1[0];
    @DexIgnore
    public static /* final */ R9 d; // = new R9();

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<NotificationFilter[]>[] b() {
        return b;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<NotificationFilter[]>[] c() {
        return c;
    }

    @DexIgnore
    public final byte[] h(NotificationFilter[] notificationFilterArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (NotificationFilter notificationFilter : notificationFilterArr) {
            byteArrayOutputStream.write(notificationFilter.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "entriesData.toByteArray()");
        return byteArray;
    }
}
