package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V58 {
    @DexIgnore
    public static /* final */ Class a;
    @DexIgnore
    public static /* final */ Class b;
    @DexIgnore
    public static /* final */ Class c;
    @DexIgnore
    public static /* final */ Class d;
    @DexIgnore
    public static /* final */ Class e;
    @DexIgnore
    public static /* final */ Class f;
    @DexIgnore
    public static /* final */ Class g;
    @DexIgnore
    public static /* final */ Class h;
    @DexIgnore
    public static /* final */ Class i;
    @DexIgnore
    public static /* synthetic */ Class j;
    @DexIgnore
    public static /* synthetic */ Class k;
    @DexIgnore
    public static /* synthetic */ Class l;
    @DexIgnore
    public static /* synthetic */ Class m;
    @DexIgnore
    public static /* synthetic */ Class n;
    @DexIgnore
    public static /* synthetic */ Class o;
    @DexIgnore
    public static /* synthetic */ Class p;
    @DexIgnore
    public static /* synthetic */ Class q;
    @DexIgnore
    public static /* synthetic */ Class r;

    /*
    static {
        Class cls = j;
        if (cls == null) {
            cls = a("java.lang.String");
            j = cls;
        }
        a = cls;
        Class cls2 = k;
        if (cls2 == null) {
            cls2 = a("java.lang.Object");
            k = cls2;
        }
        b = cls2;
        Class cls3 = l;
        if (cls3 == null) {
            cls3 = a("java.lang.Number");
            l = cls3;
        }
        c = cls3;
        Class cls4 = m;
        if (cls4 == null) {
            cls4 = a("java.util.Date");
            m = cls4;
        }
        d = cls4;
        Class cls5 = n;
        if (cls5 == null) {
            cls5 = a("java.lang.Class");
            n = cls5;
        }
        e = cls5;
        Class cls6 = o;
        if (cls6 == null) {
            cls6 = a("java.io.FileInputStream");
            o = cls6;
        }
        f = cls6;
        Class cls7 = p;
        if (cls7 == null) {
            cls7 = a("java.io.File");
            p = cls7;
        }
        g = cls7;
        Class cls8 = q;
        if (cls8 == null) {
            cls8 = a("[Ljava.io.File;");
            q = cls8;
        }
        h = cls8;
        Class cls9 = r;
        if (cls9 == null) {
            cls9 = a("java.net.URL");
            r = cls9;
        }
        i = cls9;
    }
    */

    @DexIgnore
    public static /* synthetic */ Class a(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e2) {
            throw new NoClassDefFoundError().initCause(e2);
        }
    }
}
