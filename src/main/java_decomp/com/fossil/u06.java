package com.fossil;

import com.fossil.Tq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.mapped.AppWrapper;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U06 extends Tq4<Ai, Bi, Tq4.Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tq4.Bi {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public Ai(List<AppWrapper> list) {
            Wg6.c(list, "appWrapperList");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Ci {
        @DexIgnore
        public Bi(boolean z) {
        }
    }

    /*
    static {
        String simpleName = U06.class.getSimpleName();
        Wg6.b(simpleName, "SaveAppsNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public U06(NotificationsRepository notificationsRepository) {
        Wg6.c(notificationsRepository, "notificationsRepository");
        I14.o(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        Wg6.b(notificationsRepository, "checkNotNull(notificatio\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Ai ai) {
        f(ai);
    }

    @DexIgnore
    public void f(Ai ai) {
        Wg6.c(ai, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (AppWrapper appWrapper : ai.a()) {
            InstalledApp installedApp = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                Wg6.i();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList.add(appWrapper);
            } else {
                arrayList2.add(appWrapper);
            }
        }
        g(arrayList2);
        h(arrayList);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAppsNotification done");
        b().onSuccess(new Bi(true));
    }

    @DexIgnore
    public final void g(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper appWrapper : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = appWrapper.getInstalledApp();
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(appWrapper.getCurrentHandGroup());
                    InstalledApp installedApp2 = appWrapper.getInstalledApp();
                    String identifier = installedApp2 != null ? installedApp2.getIdentifier() : null;
                    if (identifier != null) {
                        appFilter.setType(identifier);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Removed: App name = ");
                        InstalledApp installedApp3 = appWrapper.getInstalledApp();
                        sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                        sb.append(", row id = ");
                        InstalledApp installedApp4 = appWrapper.getInstalledApp();
                        sb.append(installedApp4 != null ? Integer.valueOf(installedApp4.getDbRowId()) : null);
                        sb.append(", hour = ");
                        sb.append(appWrapper.getCurrentHandGroup());
                        local.d(str, sb.toString());
                        arrayList.add(appFilter);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }

    @DexIgnore
    public final void h(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper appWrapper : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(appWrapper.getCurrentHandGroup());
                InstalledApp installedApp = appWrapper.getInstalledApp();
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved: App name = ");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                sb.append(installedApp2 != null ? installedApp2.getIdentifier() : null);
                sb.append(", hour = ");
                sb.append(appWrapper.getCurrentHandGroup());
                local.d(str, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }
}
