package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum wn1 {
    SUCCESS((byte) 0),
    NO_MUSIC_PLAYER((byte) 1),
    FAIL_TO_TRIGGER((byte) 2);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public wn1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
