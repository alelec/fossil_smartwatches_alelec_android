package com.fossil;

import android.content.Intent;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hz4 {
    @DexIgnore
    public static /* final */ Hz4 a; // = new Hz4();

    @DexIgnore
    public final String a(String str, String str2, String str3) {
        boolean z = false;
        Wg6.c(str3, "socialId");
        if (!(str == null || str.length() == 0)) {
            return str;
        }
        if (str2 == null || str2.length() == 0) {
            z = true;
        }
        return !z ? str2 : str3;
    }

    @DexIgnore
    public final String b(String str, String str2, String str3) {
        Wg6.c(str3, "socialId");
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131887310);
        Wg6.b(c, "LanguageHelper.getString\u2026allenge_format_full_name)");
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        String format = String.format(c, Arrays.copyOf(new Object[]{str, str2}, 2));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        return Wg6.a(format, " ") ? str3 : format;
    }

    @DexIgnore
    public final void c(boolean z) {
        Intent intent = new Intent("set_sync_data_event");
        intent.putExtra("set_sync_data_extra", z);
        Ct0.b(PortfolioApp.get.instance()).d(intent);
    }
}
