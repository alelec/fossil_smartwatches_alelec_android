package com.fossil;

import com.fossil.fitness.SleepState;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k47 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qq7 implements rp7<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            pq7.c(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Integer invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Integer.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends qq7 implements rp7<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            pq7.c(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public /* bridge */ /* synthetic */ Long invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Long.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return mn7.c(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore
    public static final ActivitySummary a(ActivitySummary activitySummary, ActivitySample activitySample) {
        List<Integer> j0;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SyncDataExtensions", "calculateSample - currentSummary=" + activitySummary + ", newSample=" + activitySample);
        double steps = activitySample.getSteps();
        double steps2 = activitySummary.getSteps();
        double calories = activitySample.getCalories();
        double calories2 = activitySummary.getCalories();
        double distance = activitySample.getDistance();
        double distance2 = activitySummary.getDistance();
        int activeTime = activitySample.getActiveTime();
        int activeTime2 = activitySummary.getActiveTime();
        List<Integer> intensitiesInArray = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
        List<Integer> intensities = activitySummary.getIntensities();
        if (intensitiesInArray.size() > intensities.size()) {
            ArrayList arrayList = new ArrayList(im7.m(intensitiesInArray, 10));
            int i = 0;
            for (T t : intensitiesInArray) {
                if (i >= 0) {
                    arrayList.add(Integer.valueOf((i < intensities.size() ? intensities.get(i).intValue() : 0) + t.intValue()));
                    i++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            j0 = pm7.j0(arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(im7.m(intensities, 10));
            int i2 = 0;
            for (T t2 : intensities) {
                if (i2 >= 0) {
                    arrayList2.add(Integer.valueOf((i2 < intensitiesInArray.size() ? intensitiesInArray.get(i2).intValue() : 0) + t2.intValue()));
                    i2++;
                } else {
                    hm7.l();
                    throw null;
                }
            }
            j0 = pm7.j0(arrayList2);
        }
        activitySummary.setSteps(steps + steps2);
        activitySummary.setCalories(calories + calories2);
        activitySummary.setDistance(distance + distance2);
        activitySummary.setActiveTime(activeTime + activeTime2);
        activitySummary.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary.setIntensities(j0);
        return activitySummary;
    }

    @DexIgnore
    public static final List<cl7<Long, Long>> b(List<FitnessDataWrapper> list) {
        pq7.c(list, "$this$getActiveTimeList");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        pm7.b0(list, new a());
        for (T t : list) {
            DateTime startTimeTZ = t.getStartTimeTZ();
            int resolutionInSecond = t.getActiveMinute().getResolutionInSecond();
            if (!t.getActiveMinute().getValues().isEmpty()) {
                int i = 0;
                boolean z = false;
                for (T t2 : t.getActiveMinute().getValues()) {
                    if (i >= 0) {
                        z = t2.booleanValue();
                        if (z && z != z) {
                            startTimeTZ = startTimeTZ.plus(((long) (i * resolutionInSecond)) * 1000);
                            pq7.b(startTimeTZ, "startTime.plus(index * resolutionInSecond * 1000L)");
                        }
                        if (!z && z != z) {
                            arrayList.add(new cl7(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf(t.getStartLongTime() + (((long) (i * resolutionInSecond)) * 1000))));
                        }
                        i++;
                    } else {
                        hm7.l();
                        throw null;
                    }
                }
                if (((Boolean) pm7.P(t.getActiveMinute().getValues())).booleanValue()) {
                    arrayList.add(new cl7(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf((((long) (t.getActiveMinute().getValues().size() * resolutionInSecond)) * 1000) + t.getStartLongTime())));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<WrapperTapEventSummary> c(List<FitnessDataWrapper> list) {
        pq7.c(list, "$this$getGoalTrackings");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        pm7.b0(list, new b());
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Iterator<T> it2 = it.next().getGoalTrackings().iterator();
            while (it2.hasNext()) {
                arrayList.add(it2.next());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final gl7<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> d(List<FitnessDataWrapper> list, String str, String str2, long j) {
        String str3;
        int i;
        String str4;
        int i2;
        int i3;
        ArrayList arrayList;
        String str5;
        ActivitySummary activitySummary;
        ArrayList arrayList2;
        String str6;
        ActivitySample activitySample;
        ActivitySummary activitySummary2;
        ArrayList arrayList3;
        ArrayList arrayList4;
        int i4;
        int i5;
        TimeZone timeZone;
        ActivitySummary activitySummary3;
        ActivitySummary activitySummary4;
        pq7.c(list, "$this$getListActivityData");
        pq7.c(str, "serial");
        pq7.c(str2, ButtonService.USER_ID);
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "getListActivityData");
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        ArrayList arrayList7 = new ArrayList();
        if (list.isEmpty()) {
            return new gl7<>(arrayList5, arrayList6, arrayList7);
        }
        pm7.b0(list, mn7.b(c.INSTANCE, d.INSTANCE));
        FitnessDataWrapper fitnessDataWrapper = list.get(0);
        int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
        DateTime withMillisOfSecond = fitnessDataWrapper.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
        DateTime plusMinutes = withMillisOfSecond.plusMinutes(1);
        pq7.b(withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        int hourOfDay = withMillisOfSecond.getHourOfDay();
        pq7.b(withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        int minuteOfHour = withMillisOfSecond.getMinuteOfHour();
        TimeZone t = lk5.t(timezoneOffsetInSecond);
        Date date = withMillisOfSecond.toLocalDateTime().toDate();
        pq7.b(date, "startTime.toLocalDateTime().toDate()");
        pq7.b(withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        pq7.b(plusMinutes, SampleRaw.COLUMN_END_TIME);
        ActivitySample activitySample2 = new ActivitySample(str2, date, withMillisOfSecond, plusMinutes, 0.0d, 0.0d, 0.0d, 0, new ActivityIntensities(), timezoneOffsetInSecond, str, j);
        DateTime startTimeTZ = fitnessDataWrapper.getStartTimeTZ();
        double steps = activitySample2.getSteps();
        double calories = activitySample2.getCalories();
        double distance = activitySample2.getDistance();
        int activeTime = activitySample2.getActiveTime();
        List<Integer> intensitiesInArray = activitySample2.getIntensityDistInSteps().getIntensitiesInArray();
        pq7.b(t, "timeZone");
        ActivitySummary a2 = y37.a(startTimeTZ, steps, calories, distance, activeTime, intensitiesInArray, t.getID());
        int size = list.size();
        int i6 = 0;
        String str7 = "timeZone";
        TimeZone timeZone2 = t;
        int i7 = minuteOfHour;
        int i8 = hourOfDay;
        ActivitySample activitySample3 = activitySample2;
        ArrayList arrayList8 = arrayList5;
        String str8 = "SyncDataExtensions";
        for (T t2 : list) {
            int i9 = i6 + 1;
            if (i6 >= 0) {
                T t3 = t2;
                int timezoneOffsetInSecond2 = t3.getTimezoneOffsetInSecond();
                FitnessDataWrapper fitnessDataWrapper2 = i9 < size ? list.get(i9) : null;
                boolean z = fitnessDataWrapper2 == null || timezoneOffsetInSecond2 != fitnessDataWrapper2.getTimezoneOffsetInSecond();
                int size2 = t3.getStep().getValues().size();
                int size3 = t3.getCalorie().getValues().size();
                int size4 = t3.getDistance().getValues().size();
                int size5 = t3.getActiveMinute().getValues().size();
                if (size2 == size3 && size3 == size4 && size4 == size5) {
                    List<Short> values = t3.getStep().getValues();
                    int resolutionInSecond = t3.getStep().getResolutionInSecond();
                    int i10 = 0;
                    int i11 = i7;
                    for (T t4 : values) {
                        if (i10 >= 0) {
                            short shortValue = t4.shortValue();
                            DateTime withMillisOfSecond2 = t3.getStartTimeTZ().plusMillis(i10 * resolutionInSecond * 1000).withSecondOfMinute(0).withMillisOfSecond(0);
                            DateTime plusSeconds = withMillisOfSecond2.plusSeconds(resolutionInSecond);
                            pq7.b(withMillisOfSecond2, "currentStartTime");
                            int hourOfDay2 = withMillisOfSecond2.getHourOfDay();
                            int minuteOfHour2 = withMillisOfSecond2.getMinuteOfHour();
                            float floatValue = t3.getCalorie().getValues().get(i10).floatValue();
                            double doubleValue = t3.getDistance().getValues().get(i10).doubleValue();
                            ActivityIntensities activityIntensities = new ActivityIntensities();
                            double d2 = (double) shortValue;
                            activityIntensities.setIntensity(d2);
                            int resolutionInSecond2 = t3.getActiveMinute().getValues().get(i10).booleanValue() ? t3.getActiveMinute().getResolutionInSecond() / 60 : 0;
                            if (hourOfDay2 == i8 && minuteOfHour2 / 15 == i11 / 15) {
                                activitySample3.setSteps(activitySample3.getSteps() + d2);
                                activitySample3.setCalories(activitySample3.getCalories() + ((double) floatValue));
                                activitySample3.setDistance(activitySample3.getDistance() + doubleValue);
                                pq7.b(plusSeconds, "currentEndTime");
                                activitySample3.setEndTime(plusSeconds);
                                activitySample3.setActiveTime(activitySample3.getActiveTime() + resolutionInSecond2);
                                ActivityIntensities intensityDistInSteps = activitySample3.getIntensityDistInSteps();
                                intensityDistInSteps.setLight(intensityDistInSteps.getLight() + activityIntensities.getLight());
                                ActivityIntensities intensityDistInSteps2 = activitySample3.getIntensityDistInSteps();
                                intensityDistInSteps2.setModerate(intensityDistInSteps2.getModerate() + activityIntensities.getModerate());
                                ActivityIntensities intensityDistInSteps3 = activitySample3.getIntensityDistInSteps();
                                intensityDistInSteps3.setIntense(intensityDistInSteps3.getIntense() + activityIntensities.getIntense());
                            } else {
                                if (f(activitySample3)) {
                                    arrayList7.add(new GFitSample(lr7.a(activitySample3.getSteps()), (float) activitySample3.getDistance(), (float) activitySample3.getCalories(), activitySample3.getStartTime().getMillis(), activitySample3.getEndTime().getMillis()));
                                    int minuteOfHour3 = activitySample3.getStartTime().getMinuteOfHour();
                                    DateTime withMinuteOfHour = activitySample3.getStartTime().withMinuteOfHour(minuteOfHour3 - (minuteOfHour3 % 15));
                                    pq7.b(withMinuteOfHour, "currentSampleRaw.startTi\u2026nuteOfHour(currentMinute)");
                                    activitySample3.setStartTimeId(withMinuteOfHour);
                                    arrayList8.add(activitySample3);
                                    if (lk5.m0(a2.getDate(), activitySample3.getDate())) {
                                        pq7.b(a2, "currentActivitySummary");
                                        a(a2, activitySample3);
                                        activitySummary4 = a2;
                                    } else {
                                        pq7.b(a2, "currentActivitySummary");
                                        arrayList6.add(a2);
                                        DateTime dateTime = new DateTime(activitySample3.getDate());
                                        double steps2 = activitySample3.getSteps();
                                        double calories2 = activitySample3.getCalories();
                                        double distance2 = activitySample3.getDistance();
                                        int activeTime2 = activitySample3.getActiveTime();
                                        List<Integer> intensitiesInArray2 = activitySample3.getIntensityDistInSteps().getIntensitiesInArray();
                                        pq7.b(timeZone2, str7);
                                        activitySummary4 = y37.a(dateTime, steps2, calories2, distance2, activeTime2, intensitiesInArray2, timeZone2.getID());
                                    }
                                } else {
                                    activitySummary4 = a2;
                                }
                                i8 = withMillisOfSecond2.getHourOfDay();
                                i11 = withMillisOfSecond2.getMinuteOfHour();
                                FLogger.INSTANCE.getLocal().d(str8, "getListActivityData - startTime=" + withMillisOfSecond2 + ", endTime=" + plusSeconds);
                                Date date2 = withMillisOfSecond2.toLocalDateTime().toDate();
                                pq7.b(date2, "currentStartTime.toLocalDateTime().toDate()");
                                pq7.b(plusSeconds, "currentEndTime");
                                activitySample3 = new ActivitySample(str2, date2, withMillisOfSecond2, plusSeconds, d2, (double) floatValue, doubleValue, resolutionInSecond2, activityIntensities, timezoneOffsetInSecond2, str, j);
                                a2 = activitySummary4;
                            }
                            tl7 tl7 = tl7.f3441a;
                            i10++;
                            arrayList7 = arrayList7;
                            arrayList6 = arrayList6;
                            timeZone2 = timeZone2;
                        } else {
                            hm7.l();
                            throw null;
                        }
                    }
                    str3 = "currentSampleRaw.startTi\u2026nuteOfHour(currentMinute)";
                    i = i9;
                    str4 = str8;
                    i2 = i11;
                    i3 = i8;
                    arrayList = arrayList8;
                    str5 = "currentActivitySummary";
                    activitySummary = a2;
                    arrayList2 = arrayList6;
                    str6 = str7;
                } else {
                    FLogger.INSTANCE.getLocal().e(str8, "getSampleRawList(), steps, calories and distances data sizes do not match from SDK");
                    str3 = "currentSampleRaw.startTi\u2026nuteOfHour(currentMinute)";
                    i = i9;
                    str4 = str8;
                    i2 = i7;
                    i3 = i8;
                    arrayList = arrayList8;
                    str5 = "currentActivitySummary";
                    activitySummary = a2;
                    arrayList2 = arrayList6;
                    str6 = str7;
                }
                if (z) {
                    if (f(activitySample3)) {
                        arrayList7.add(new GFitSample(lr7.a(activitySample3.getSteps()), (float) activitySample3.getDistance(), (float) activitySample3.getCalories(), activitySample3.getStartTime().getMillis(), activitySample3.getEndTime().getMillis()));
                        int minuteOfHour4 = activitySample3.getStartTime().getMinuteOfHour();
                        DateTime withMinuteOfHour2 = activitySample3.getStartTime().withMinuteOfHour(minuteOfHour4 - (minuteOfHour4 % 15));
                        pq7.b(withMinuteOfHour2, str3);
                        activitySample3.setStartTimeId(withMinuteOfHour2);
                        arrayList.add(activitySample3);
                        if (lk5.m0(activitySummary.getDate(), activitySample3.getDate())) {
                            pq7.b(activitySummary, str5);
                            a(activitySummary, activitySample3);
                            arrayList2.add(activitySummary);
                        } else {
                            pq7.b(activitySummary, str5);
                            arrayList2.add(activitySummary);
                            DateTime dateTime2 = new DateTime(activitySample3.getDate());
                            double steps3 = activitySample3.getSteps();
                            double calories3 = activitySample3.getCalories();
                            double distance3 = activitySample3.getDistance();
                            int activeTime3 = activitySample3.getActiveTime();
                            List<Integer> intensitiesInArray3 = activitySample3.getIntensityDistInSteps().getIntensitiesInArray();
                            pq7.b(timeZone2, str6);
                            ActivitySummary a3 = y37.a(dateTime2, steps3, calories3, distance3, activeTime3, intensitiesInArray3, timeZone2.getID());
                            pq7.b(a3, "Interpolator.createActiv\u2026esInArray(), timeZone.id)");
                            arrayList2.add(a3);
                        }
                    } else {
                        pq7.b(activitySummary, str5);
                        arrayList2.add(activitySummary);
                    }
                    if (fitnessDataWrapper2 != null) {
                        DateTime withMillisOfSecond3 = fitnessDataWrapper2.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
                        DateTime plusMinutes2 = withMillisOfSecond3.plusMinutes(1);
                        pq7.b(withMillisOfSecond3, SampleRaw.COLUMN_START_TIME);
                        int hourOfDay3 = withMillisOfSecond3.getHourOfDay();
                        pq7.b(withMillisOfSecond3, SampleRaw.COLUMN_START_TIME);
                        int minuteOfHour5 = withMillisOfSecond3.getMinuteOfHour();
                        TimeZone t5 = lk5.t(fitnessDataWrapper2.getTimezoneOffsetInSecond());
                        Date date3 = withMillisOfSecond3.toLocalDateTime().toDate();
                        pq7.b(date3, "startTime.toLocalDateTime().toDate()");
                        pq7.b(withMillisOfSecond3, SampleRaw.COLUMN_START_TIME);
                        pq7.b(plusMinutes2, SampleRaw.COLUMN_END_TIME);
                        activitySample = new ActivitySample(str2, date3, withMillisOfSecond3, plusMinutes2, 0.0d, 0.0d, 0.0d, 0, new ActivityIntensities(), timezoneOffsetInSecond, str, j);
                        Iterator it = arrayList2.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                activitySummary3 = null;
                                break;
                            }
                            Object next = it.next();
                            if (lk5.m0(((ActivitySummary) next).getDate(), g(fitnessDataWrapper2.getStartTimeTZ(), true))) {
                                activitySummary3 = next;
                                break;
                            }
                        }
                        activitySummary2 = activitySummary3;
                        if (activitySummary2 == null) {
                            DateTime startTimeTZ2 = fitnessDataWrapper2.getStartTimeTZ();
                            double steps4 = activitySample.getSteps();
                            double calories4 = activitySample.getCalories();
                            double distance4 = activitySample.getDistance();
                            int activeTime4 = activitySample.getActiveTime();
                            List<Integer> intensitiesInArray4 = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
                            pq7.b(t5, str6);
                            activitySummary2 = y37.a(startTimeTZ2, steps4, calories4, distance4, activeTime4, intensitiesInArray4, t5.getID());
                            tl7 tl72 = tl7.f3441a;
                        } else {
                            arrayList2.remove(activitySummary2);
                        }
                        i4 = minuteOfHour5;
                        i5 = hourOfDay3;
                        timeZone = t5;
                    } else {
                        i4 = i2;
                        i5 = i3;
                        activitySummary2 = activitySummary;
                        activitySample = activitySample3;
                        timeZone = timeZone2;
                    }
                    i2 = i4;
                    i3 = i5;
                    arrayList3 = arrayList;
                    arrayList4 = arrayList2;
                    timeZone2 = timeZone;
                } else {
                    activitySample = activitySample3;
                    activitySummary2 = activitySummary;
                    arrayList3 = arrayList;
                    arrayList4 = arrayList2;
                }
                tl7 tl73 = tl7.f3441a;
                i6 = i;
                str7 = str6;
                i7 = i2;
                i8 = i3;
                activitySample3 = activitySample;
                a2 = activitySummary2;
                arrayList6 = arrayList4;
                arrayList8 = arrayList3;
                str8 = str4;
            } else {
                hm7.l();
                throw null;
            }
        }
        FLogger.INSTANCE.getLocal().d(str8, "sampleRaw " + arrayList8 + " \n summary " + arrayList6 + " \n gFit " + arrayList7);
        return new gl7<>(arrayList8, arrayList6, arrayList7);
    }

    @DexIgnore
    public static final List<MFSleepSession> e(List<FitnessDataWrapper> list, String str) {
        SleepSessionHeartRate sleepSessionHeartRate;
        int i;
        pq7.c(list, "$this$getSleepSessions");
        pq7.c(str, "serial");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        pm7.b0(list, new e());
        Gson gson = new Gson();
        for (T t : list) {
            for (T t2 : t.getSleeps()) {
                long millis = t2.getStartTime().getMillis();
                int normalizedSleepQuality = t2.getNormalizedSleepQuality();
                SleepDistribution sleepDistribution = new SleepDistribution(t2.getAwakeMinutes(), t2.getLightSleepMinutes(), t2.getDeepSleepMinutes());
                ArrayList arrayList2 = new ArrayList();
                for (T t3 : t2.getStateChanges()) {
                    int component1 = t3.component1();
                    arrayList2.add(new WrapperSleepStateChange((component1 == SleepState.AWAKE.ordinal() ? SleepState.AWAKE : component1 == SleepState.SLEEP.ordinal() ? SleepState.SLEEP : component1 == SleepState.DEEP_SLEEP.ordinal() ? SleepState.DEEP_SLEEP : SleepState.AWAKE).ordinal(), (long) t3.component2()));
                }
                String t4 = gson.t(arrayList2);
                if (t2.getHeartRate() != null) {
                    List<Short> values = t2.getHeartRate().getValues();
                    short s = Short.MIN_VALUE;
                    short s2 = Short.MAX_VALUE;
                    int resolutionInSecond = t2.getHeartRate().getResolutionInSecond();
                    int size = values.size();
                    short s3 = 0;
                    short s4 = 0;
                    int i2 = 0;
                    while (i2 < size) {
                        short shortValue = values.get(i2).shortValue();
                        if (shortValue > s) {
                            s = shortValue;
                        } else if (shortValue < s2) {
                            s2 = shortValue;
                        }
                        if (shortValue > 0) {
                            i = shortValue + s4;
                            s3++;
                        } else {
                            i = s4;
                        }
                        i2++;
                        s4 = i;
                    }
                    sleepSessionHeartRate = new SleepSessionHeartRate((float) (s3 > 0 ? s4 / s3 : 0), s, s2, resolutionInSecond, values);
                } else {
                    sleepSessionHeartRate = null;
                }
                long j = (long) 1000;
                int currentTimeMillis = (int) (System.currentTimeMillis() / j);
                long millis2 = t2.getEndTime().getMillis();
                Date date = t2.getEndTime().toDate();
                pq7.b(date, "sleepSession.endTime.toDate()");
                int deepSleepMinutes = t2.getDeepSleepMinutes();
                int lightSleepMinutes = t2.getLightSleepMinutes();
                pq7.b(t4, "sleepStatesJson");
                DateTime now = DateTime.now();
                pq7.b(now, "DateTime.now()");
                arrayList.add(new MFSleepSession(millis2, date, t.getTimezoneOffsetInSecond(), str, currentTimeMillis, currentTimeMillis, (double) normalizedSleepQuality, oh5.Device.ordinal(), (int) (millis / j), (int) (t2.getEndTime().getMillis() / j), deepSleepMinutes + lightSleepMinutes, sleepDistribution, t4, sleepSessionHeartRate, now));
            }
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "sleepSessions " + arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final boolean f(ActivitySample activitySample) {
        pq7.c(activitySample, "$this$isDataAvailable");
        double d2 = (double) 0;
        return activitySample.getSteps() > d2 || activitySample.getCalories() > d2 || activitySample.getDistance() > d2 || activitySample.getActiveTime() > 0;
    }

    @DexIgnore
    public static final Date g(DateTime dateTime, boolean z) {
        pq7.c(dateTime, "$this$toDate");
        if (z) {
            Date date = dateTime.toLocalDateTime().toDate();
            pq7.b(date, "toLocalDateTime().toDate()");
            return date;
        }
        Date date2 = dateTime.toDate();
        pq7.b(date2, "toDate()");
        return date2;
    }
}
