package com.fossil;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.Qu2;
import com.fossil.Ru2;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jm3 extends Zq3 implements Hg3 {
    @DexIgnore
    public static int j; // = 65535;
    @DexIgnore
    public static int k; // = 2;
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> d; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> e; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> f; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, Ru2> g; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, Map<String, Integer>> h; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, String> i; // = new Zi0();

    @DexIgnore
    public Jm3(Yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    public static Map<String, String> w(Ru2 ru2) {
        Zi0 zi0 = new Zi0();
        if (ru2 != null) {
            for (Su2 su2 : ru2.L()) {
                zi0.put(su2.C(), su2.D());
            }
        }
        return zi0;
    }

    @DexIgnore
    public final boolean A(String str, String str2) {
        h();
        J(str);
        if (H(str) && Kr3.B0(str2)) {
            return true;
        }
        if (I(str) && Kr3.c0(str2)) {
            return true;
        }
        Map<String, Boolean> map = this.e.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final void B(String str) {
        h();
        this.i.put(str, null);
    }

    @DexIgnore
    public final boolean C(String str, String str2) {
        h();
        J(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        if (S53.a() && m().s(Xg3.J0) && ("purchase".equals(str2) || "refund".equals(str2))) {
            return true;
        }
        Map<String, Boolean> map = this.f.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final int D(String str, String str2) {
        h();
        J(str);
        Map<String, Integer> map = this.h.get(str);
        if (map == null) {
            return 1;
        }
        Integer num = map.get(str2);
        if (num == null) {
            return 1;
        }
        return num.intValue();
    }

    @DexIgnore
    public final void E(String str) {
        h();
        this.g.remove(str);
    }

    @DexIgnore
    public final boolean F(String str) {
        h();
        Ru2 u = u(str);
        if (u == null) {
            return false;
        }
        return u.O();
    }

    @DexIgnore
    public final long G(String str) {
        String a2 = a(str, "measurement.account.time_zone_offset_minutes");
        if (!TextUtils.isEmpty(a2)) {
            try {
                return Long.parseLong(a2);
            } catch (NumberFormatException e2) {
                d().I().c("Unable to parse timezone offset. appId", Kl3.w(str), e2);
            }
        }
        return 0;
    }

    @DexIgnore
    public final boolean H(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_internal"));
    }

    @DexIgnore
    public final boolean I(String str) {
        return "1".equals(a(str, "measurement.upload.blacklist_public"));
    }

    @DexIgnore
    public final void J(String str) {
        r();
        h();
        Rc2.g(str);
        if (this.g.get(str) == null) {
            byte[] p0 = o().p0(str);
            if (p0 == null) {
                this.d.put(str, null);
                this.e.put(str, null);
                this.f.put(str, null);
                this.g.put(str, null);
                this.i.put(str, null);
                this.h.put(str, null);
                return;
            }
            Ru2.Ai ai = (Ru2.Ai) v(str, p0).x();
            x(str, ai);
            this.d.put(str, w((Ru2) ((E13) ai.h())));
            this.g.put(str, (Ru2) ((E13) ai.h()));
            this.i.put(str, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hg3
    public final String a(String str, String str2) {
        h();
        J(str);
        Map<String, String> map = this.d.get(str);
        if (map != null) {
            return map.get(str2);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final Ru2 u(String str) {
        r();
        h();
        Rc2.g(str);
        J(str);
        return this.g.get(str);
    }

    @DexIgnore
    public final Ru2 v(String str, byte[] bArr) {
        String str2 = null;
        if (bArr == null) {
            return Ru2.Q();
        }
        try {
            Ru2.Ai P = Ru2.P();
            Gr3.z(P, bArr);
            Ru2 ru2 = (Ru2) ((E13) P.h());
            Nl3 N = d().N();
            Long valueOf = ru2.H() ? Long.valueOf(ru2.I()) : null;
            if (ru2.J()) {
                str2 = ru2.K();
            }
            N.c("Parsed config. version, gmp_app_id", valueOf, str2);
            return ru2;
        } catch (L13 e2) {
            d().I().c("Unable to merge remote config. appId", Kl3.w(str), e2);
            return Ru2.Q();
        } catch (RuntimeException e3) {
            d().I().c("Unable to merge remote config. appId", Kl3.w(str), e3);
            return Ru2.Q();
        }
    }

    @DexIgnore
    public final void x(String str, Ru2.Ai ai) {
        Zi0 zi0 = new Zi0();
        Zi0 zi02 = new Zi0();
        Zi0 zi03 = new Zi0();
        if (ai != null) {
            for (int i2 = 0; i2 < ai.x(); i2++) {
                Qu2.Ai ai2 = (Qu2.Ai) ai.y(i2).x();
                if (TextUtils.isEmpty(ai2.y())) {
                    d().I().a("EventConfig contained null event name");
                } else {
                    String b = On3.b(ai2.y());
                    if (!TextUtils.isEmpty(b)) {
                        ai2.x(b);
                        ai.z(i2, ai2);
                    }
                    zi0.put(ai2.y(), Boolean.valueOf(ai2.z()));
                    zi02.put(ai2.y(), Boolean.valueOf(ai2.B()));
                    if (ai2.C()) {
                        if (ai2.E() < k || ai2.E() > j) {
                            d().I().c("Invalid sampling rate. Event name, sample rate", ai2.y(), Integer.valueOf(ai2.E()));
                        } else {
                            zi03.put(ai2.y(), Integer.valueOf(ai2.E()));
                        }
                    }
                }
            }
        }
        this.e.put(str, zi0);
        this.f.put(str, zi02);
        this.h.put(str, zi03);
    }

    @DexIgnore
    public final boolean y(String str, byte[] bArr, String str2) {
        r();
        h();
        Rc2.g(str);
        Ru2.Ai ai = (Ru2.Ai) v(str, bArr).x();
        if (ai == null) {
            return false;
        }
        x(str, ai);
        this.g.put(str, (Ru2) ((E13) ai.h()));
        this.i.put(str, str2);
        this.d.put(str, w((Ru2) ((E13) ai.h())));
        o().N(str, new ArrayList(ai.B()));
        try {
            ai.C();
            bArr = ((Ru2) ((E13) ai.h())).c();
        } catch (RuntimeException e2) {
            d().I().c("Unable to serialize reduced-size config. Storing full config instead. appId", Kl3.w(str), e2);
        }
        Kg3 o = o();
        Rc2.g(str);
        o.h();
        o.r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) o.v().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                o.d().F().b("Failed to update remote config (got 0). appId", Kl3.w(str));
            }
        } catch (SQLiteException e3) {
            o.d().F().c("Error storing remote config. appId", Kl3.w(str), e3);
        }
        this.g.put(str, (Ru2) ((E13) ai.h()));
        return true;
    }

    @DexIgnore
    public final String z(String str) {
        h();
        return this.i.get(str);
    }
}
