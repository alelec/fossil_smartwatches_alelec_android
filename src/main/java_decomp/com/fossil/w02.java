package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class W02 {
    @DexIgnore
    public static W02 a(Context context, T32 t32, T32 t322, String str) {
        return new R02(context, t32, t322, str);
    }

    @DexIgnore
    public abstract Context b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract T32 d();

    @DexIgnore
    public abstract T32 e();
}
