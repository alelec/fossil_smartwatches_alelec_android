package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wg3 implements Parcelable.Creator<Ug3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ug3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 2) {
                Ad2.B(parcel, t);
            } else {
                bundle = Ad2.a(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ug3(bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ug3[] newArray(int i) {
        return new Ug3[i];
    }
}
