package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rg2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Ql2 implements Rg2 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Rl2 implements Rg2 {
            @DexIgnore
            public Aii(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        @DexIgnore
        public Ai() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        @DexIgnore
        public static Rg2 e(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            return queryLocalInterface instanceof Rg2 ? (Rg2) queryLocalInterface : new Aii(iBinder);
        }
    }
}
