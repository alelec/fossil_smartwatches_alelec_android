package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U18 implements A18 {
    @DexIgnore
    public /* final */ OkHttpClient b;
    @DexIgnore
    public /* final */ Z28 c;
    @DexIgnore
    public /* final */ G48 d;
    @DexIgnore
    public M18 e;
    @DexIgnore
    public /* final */ V18 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends G48 {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.G48
        public void x() {
            U18.this.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends A28 {
        @DexIgnore
        public /* final */ B18 c;

        @DexIgnore
        public Bi(B18 b18) {
            super("OkHttp %s", U18.this.l());
            this.c = b18;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x002b A[Catch:{ all -> 0x0049 }] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0080  */
        @Override // com.fossil.A28
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void k() {
            /*
                r4 = this;
                r1 = 1
                r2 = 0
                com.fossil.U18 r0 = com.fossil.U18.this
                com.fossil.G48 r0 = r0.d
                r0.r()
                com.fossil.U18 r0 = com.fossil.U18.this     // Catch:{ IOException -> 0x0056, all -> 0x0022 }
                okhttp3.Response r0 = r0.i()     // Catch:{ IOException -> 0x0056, all -> 0x0022 }
                com.fossil.B18 r2 = r4.c     // Catch:{ IOException -> 0x0093, all -> 0x0095 }
                com.fossil.U18 r3 = com.fossil.U18.this     // Catch:{ IOException -> 0x0093, all -> 0x0095 }
                r2.onResponse(r3, r0)     // Catch:{ IOException -> 0x0093, all -> 0x0095 }
            L_0x0016:
                com.fossil.U18 r0 = com.fossil.U18.this
                okhttp3.OkHttpClient r0 = r0.b
                com.fossil.K18 r0 = r0.p()
                r0.e(r4)
                return
            L_0x0022:
                r0 = move-exception
                r1 = r2
            L_0x0024:
                com.fossil.U18 r2 = com.fossil.U18.this     // Catch:{ all -> 0x0049 }
                r2.cancel()     // Catch:{ all -> 0x0049 }
                if (r1 != 0) goto L_0x0048
                java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0049 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0049 }
                r2.<init>()     // Catch:{ all -> 0x0049 }
                java.lang.String r3 = "canceled due to "
                r2.append(r3)     // Catch:{ all -> 0x0049 }
                r2.append(r0)     // Catch:{ all -> 0x0049 }
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0049 }
                r1.<init>(r2)     // Catch:{ all -> 0x0049 }
                com.fossil.B18 r2 = r4.c     // Catch:{ all -> 0x0049 }
                com.fossil.U18 r3 = com.fossil.U18.this     // Catch:{ all -> 0x0049 }
                r2.onFailure(r3, r1)     // Catch:{ all -> 0x0049 }
            L_0x0048:
                throw r0     // Catch:{ all -> 0x0049 }
            L_0x0049:
                r0 = move-exception
                com.fossil.U18 r1 = com.fossil.U18.this
                okhttp3.OkHttpClient r1 = r1.b
                com.fossil.K18 r1 = r1.p()
                r1.e(r4)
                throw r0
            L_0x0056:
                r0 = move-exception
                r1 = r2
            L_0x0058:
                com.fossil.U18 r2 = com.fossil.U18.this
                java.io.IOException r0 = r2.n(r0)
                if (r1 == 0) goto L_0x0080
                com.fossil.W38 r1 = com.fossil.W38.j()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Callback failure for "
                r2.append(r3)
                com.fossil.U18 r3 = com.fossil.U18.this
                java.lang.String r3 = r3.o()
                r2.append(r3)
                r3 = 4
                java.lang.String r2 = r2.toString()
                r1.q(r3, r2, r0)
                goto L_0x0016
            L_0x0080:
                com.fossil.U18 r1 = com.fossil.U18.this
                com.fossil.M18 r1 = com.fossil.U18.d(r1)
                com.fossil.U18 r2 = com.fossil.U18.this
                r1.b(r2, r0)
                com.fossil.B18 r1 = r4.c
                com.fossil.U18 r2 = com.fossil.U18.this
                r1.onFailure(r2, r0)
                goto L_0x0016
            L_0x0093:
                r0 = move-exception
                goto L_0x0058
            L_0x0095:
                r0 = move-exception
                goto L_0x0024
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.U18.Bi.k():void");
        }

        @DexIgnore
        public void l(ExecutorService executorService) {
            try {
                executorService.execute(this);
            } catch (RejectedExecutionException e) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                interruptedIOException.initCause(e);
                U18.this.e.b(U18.this, interruptedIOException);
                this.c.onFailure(U18.this, interruptedIOException);
                U18.this.b.p().e(this);
            } catch (Throwable th) {
                U18.this.b.p().e(this);
                throw th;
            }
        }

        @DexIgnore
        public U18 m() {
            return U18.this;
        }

        @DexIgnore
        public String n() {
            return U18.this.f.j().m();
        }
    }

    @DexIgnore
    public U18(OkHttpClient okHttpClient, V18 v18, boolean z) {
        this.b = okHttpClient;
        this.f = v18;
        this.g = z;
        this.c = new Z28(okHttpClient, z);
        Ai ai = new Ai();
        this.d = ai;
        ai.g((long) okHttpClient.g(), TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public static U18 j(OkHttpClient okHttpClient, V18 v18, boolean z) {
        U18 u18 = new U18(okHttpClient, v18, z);
        u18.e = okHttpClient.r().a(u18);
        return u18;
    }

    @DexIgnore
    @Override // com.fossil.A18
    public Response a() throws IOException {
        synchronized (this) {
            if (!this.h) {
                this.h = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        e();
        this.d.r();
        this.e.c(this);
        try {
            this.b.p().b(this);
            Response i = i();
            if (i != null) {
                this.b.p().f(this);
                return i;
            }
            throw new IOException("Canceled");
        } catch (IOException e2) {
            IOException n = n(e2);
            this.e.b(this, n);
            throw n;
        } catch (Throwable th) {
            this.b.p().f(this);
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.A18
    public V18 c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.A18
    public void cancel() {
        this.c.a();
    }

    @DexIgnore
    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return g();
    }

    @DexIgnore
    public final void e() {
        this.c.j(W38.j().n("response.body().close()"));
    }

    @DexIgnore
    @Override // com.fossil.A18
    public boolean f() {
        return this.c.d();
    }

    @DexIgnore
    public U18 g() {
        return j(this.b, this.f, this.g);
    }

    @DexIgnore
    public Response i() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.b.v());
        arrayList.add(this.c);
        arrayList.add(new Q28(this.b.o()));
        arrayList.add(new D28(this.b.w()));
        arrayList.add(new J28(this.b));
        if (!this.g) {
            arrayList.addAll(this.b.x());
        }
        arrayList.add(new R28(this.g));
        Response d2 = new W28(arrayList, null, null, null, 0, this.f, this, this.e, this.b.j(), this.b.I(), this.b.N()).d(this.f);
        if (!this.c.d()) {
            return d2;
        }
        B28.g(d2);
        throw new IOException("Canceled");
    }

    @DexIgnore
    public String l() {
        return this.f.j().C();
    }

    @DexIgnore
    @Override // com.fossil.A18
    public void m(B18 b18) {
        synchronized (this) {
            if (!this.h) {
                this.h = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        e();
        this.e.c(this);
        this.b.p().a(new Bi(b18));
    }

    @DexIgnore
    public IOException n(IOException iOException) {
        if (!this.d.s()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public String o() {
        StringBuilder sb = new StringBuilder();
        sb.append(f() ? "canceled " : "");
        sb.append(this.g ? "web socket" : "call");
        sb.append(" to ");
        sb.append(l());
        return sb.toString();
    }
}
