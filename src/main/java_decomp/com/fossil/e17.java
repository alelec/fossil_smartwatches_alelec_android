package com.fossil;

import com.portfolio.platform.uirenew.watchsetting.WatchSettingFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E17 implements MembersInjector<WatchSettingFragment> {
    @DexIgnore
    public static void a(WatchSettingFragment watchSettingFragment, Po4 po4) {
        watchSettingFragment.l = po4;
    }
}
