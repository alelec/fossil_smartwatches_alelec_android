package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum V90 {
    c((byte) 1);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public V90(byte b2) {
        this.b = (byte) b2;
    }
}
