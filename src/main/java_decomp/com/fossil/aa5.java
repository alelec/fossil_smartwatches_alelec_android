package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Aa5 extends Z95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        E.put(2131362968, 2);
        E.put(2131362546, 3);
        E.put(2131362692, 4);
        E.put(2131362405, 5);
        E.put(2131362969, 6);
        E.put(2131361926, 7);
        E.put(2131363004, 8);
        E.put(2131362556, 9);
        E.put(2131362288, 10);
        E.put(2131362273, 11);
    }
    */

    @DexIgnore
    public Aa5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public Aa5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (Barrier) objArr[7], (FlexibleButton) objArr[11], (FlexibleButton) objArr[10], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[9], (ImageView) objArr[1], (ImageView) objArr[4], (View) objArr[6], (DashBar) objArr[2], (RelativeLayout) objArr[8], (ConstraintLayout) objArr[0]);
        this.C = -1;
        this.B.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
