package com.fossil;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.A91;
import com.fossil.O91;
import com.fossil.U91;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M91<T> implements Comparable<M91<T>> {
    @DexIgnore
    public static /* final */ String DEFAULT_PARAMS_ENCODING; // = "UTF-8";
    @DexIgnore
    public A91.Ai mCacheEntry;
    @DexIgnore
    public boolean mCanceled;
    @DexIgnore
    public /* final */ int mDefaultTrafficStatsTag;
    @DexIgnore
    public O91.Ai mErrorListener;
    @DexIgnore
    public /* final */ U91.Ai mEventLog;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ int mMethod;
    @DexIgnore
    public Bi mRequestCompleteListener;
    @DexIgnore
    public N91 mRequestQueue;
    @DexIgnore
    public boolean mResponseDelivered;
    @DexIgnore
    public Q91 mRetryPolicy;
    @DexIgnore
    public Integer mSequence;
    @DexIgnore
    public boolean mShouldCache;
    @DexIgnore
    public boolean mShouldRetryServerErrors;
    @DexIgnore
    public Object mTag;
    @DexIgnore
    public /* final */ String mUrl;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        public Ai(String str, long j) {
            this.b = str;
            this.c = j;
        }

        @DexIgnore
        public void run() {
            M91.this.mEventLog.a(this.b, this.c);
            M91.this.mEventLog.b(M91.this.toString());
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(M91<?> m91);

        @DexIgnore
        void b(M91<?> m91, O91<?> o91);
    }

    @DexIgnore
    public enum Ci {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    @DexIgnore
    public M91(int i, String str, O91.Ai ai) {
        this.mEventLog = U91.Ai.c ? new U91.Ai() : null;
        this.mLock = new Object();
        this.mShouldCache = true;
        this.mCanceled = false;
        this.mResponseDelivered = false;
        this.mShouldRetryServerErrors = false;
        this.mCacheEntry = null;
        this.mMethod = i;
        this.mUrl = str;
        this.mErrorListener = ai;
        setRetryPolicy(new D91());
        this.mDefaultTrafficStatsTag = findDefaultTrafficStatsTag(str);
    }

    @DexIgnore
    @Deprecated
    public M91(String str, O91.Ai ai) {
        this(-1, str, ai);
    }

    @DexIgnore
    private byte[] encodeParameters(Map<String, String> map, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getKey() == null || entry.getValue() == null) {
                    throw new IllegalArgumentException(String.format("Request#getParams() or Request#getPostParams() returned a map containing a null key or value: (%s, %s). All keys and values must be non-null.", entry.getKey(), entry.getValue()));
                }
                sb.append(URLEncoder.encode(entry.getKey(), str));
                sb.append('=');
                sb.append(URLEncoder.encode(entry.getValue(), str));
                sb.append('&');
            }
            return sb.toString().getBytes(str);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not supported: " + str, e);
        }
    }

    @DexIgnore
    public static int findDefaultTrafficStatsTag(String str) {
        Uri parse;
        String host;
        if (TextUtils.isEmpty(str) || (parse = Uri.parse(str)) == null || (host = parse.getHost()) == null) {
            return 0;
        }
        return host.hashCode();
    }

    @DexIgnore
    public void addMarker(String str) {
        if (U91.Ai.c) {
            this.mEventLog.a(str, Thread.currentThread().getId());
        }
    }

    @DexIgnore
    public void cancel() {
        synchronized (this.mLock) {
            this.mCanceled = true;
            this.mErrorListener = null;
        }
    }

    @DexIgnore
    public int compareTo(M91<T> m91) {
        Ci priority = getPriority();
        Ci priority2 = m91.getPriority();
        return priority == priority2 ? this.mSequence.intValue() - m91.mSequence.intValue() : priority2.ordinal() - priority.ordinal();
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return compareTo((M91) ((M91) obj));
    }

    @DexIgnore
    public void deliverError(T91 t91) {
        O91.Ai ai;
        synchronized (this.mLock) {
            ai = this.mErrorListener;
        }
        if (ai != null) {
            ai.onErrorResponse(t91);
        }
    }

    @DexIgnore
    public abstract void deliverResponse(T t);

    @DexIgnore
    public void finish(String str) {
        N91 n91 = this.mRequestQueue;
        if (n91 != null) {
            n91.b(this);
        }
        if (U91.Ai.c) {
            long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new Ai(str, id));
                return;
            }
            this.mEventLog.a(str, id);
            this.mEventLog.b(toString());
        }
    }

    @DexIgnore
    public byte[] getBody() throws Z81 {
        Map<String, String> params = getParams();
        if (params == null || params.size() <= 0) {
            return null;
        }
        return encodeParameters(params, getParamsEncoding());
    }

    @DexIgnore
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded; charset=" + getParamsEncoding();
    }

    @DexIgnore
    public A91.Ai getCacheEntry() {
        return this.mCacheEntry;
    }

    @DexIgnore
    public String getCacheKey() {
        String url = getUrl();
        int method = getMethod();
        if (method == 0 || method == -1) {
            return url;
        }
        return Integer.toString(method) + '-' + url;
    }

    @DexIgnore
    public O91.Ai getErrorListener() {
        O91.Ai ai;
        synchronized (this.mLock) {
            ai = this.mErrorListener;
        }
        return ai;
    }

    @DexIgnore
    public Map<String, String> getHeaders() throws Z81 {
        return Collections.emptyMap();
    }

    @DexIgnore
    public int getMethod() {
        return this.mMethod;
    }

    @DexIgnore
    public Map<String, String> getParams() throws Z81 {
        return null;
    }

    @DexIgnore
    public String getParamsEncoding() {
        return "UTF-8";
    }

    @DexIgnore
    @Deprecated
    public byte[] getPostBody() throws Z81 {
        Map<String, String> postParams = getPostParams();
        if (postParams == null || postParams.size() <= 0) {
            return null;
        }
        return encodeParameters(postParams, getPostParamsEncoding());
    }

    @DexIgnore
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    @Deprecated
    public Map<String, String> getPostParams() throws Z81 {
        return getParams();
    }

    @DexIgnore
    @Deprecated
    public String getPostParamsEncoding() {
        return getParamsEncoding();
    }

    @DexIgnore
    public Ci getPriority() {
        return Ci.NORMAL;
    }

    @DexIgnore
    public Q91 getRetryPolicy() {
        return this.mRetryPolicy;
    }

    @DexIgnore
    public final int getSequence() {
        Integer num = this.mSequence;
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("getSequence called before setSequence");
    }

    @DexIgnore
    public Object getTag() {
        return this.mTag;
    }

    @DexIgnore
    public final int getTimeoutMs() {
        return getRetryPolicy().b();
    }

    @DexIgnore
    public int getTrafficStatsTag() {
        return this.mDefaultTrafficStatsTag;
    }

    @DexIgnore
    public String getUrl() {
        return this.mUrl;
    }

    @DexIgnore
    public boolean hasHadResponseDelivered() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mResponseDelivered;
        }
        return z;
    }

    @DexIgnore
    public boolean isCanceled() {
        boolean z;
        synchronized (this.mLock) {
            z = this.mCanceled;
        }
        return z;
    }

    @DexIgnore
    public void markDelivered() {
        synchronized (this.mLock) {
            this.mResponseDelivered = true;
        }
    }

    @DexIgnore
    public void notifyListenerResponseNotUsable() {
        Bi bi;
        synchronized (this.mLock) {
            bi = this.mRequestCompleteListener;
        }
        if (bi != null) {
            bi.a(this);
        }
    }

    @DexIgnore
    public void notifyListenerResponseReceived(O91<?> o91) {
        Bi bi;
        synchronized (this.mLock) {
            bi = this.mRequestCompleteListener;
        }
        if (bi != null) {
            bi.b(this, o91);
        }
    }

    @DexIgnore
    public T91 parseNetworkError(T91 t91) {
        return t91;
    }

    @DexIgnore
    public abstract O91<T> parseNetworkResponse(J91 j91);

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public M91<?> setCacheEntry(A91.Ai ai) {
        this.mCacheEntry = ai;
        return this;
    }

    @DexIgnore
    public void setNetworkRequestCompleteListener(Bi bi) {
        synchronized (this.mLock) {
            this.mRequestCompleteListener = bi;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public M91<?> setRequestQueue(N91 n91) {
        this.mRequestQueue = n91;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public M91<?> setRetryPolicy(Q91 q91) {
        this.mRetryPolicy = q91;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final M91<?> setSequence(int i) {
        this.mSequence = Integer.valueOf(i);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final M91<?> setShouldCache(boolean z) {
        this.mShouldCache = z;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final M91<?> setShouldRetryServerErrors(boolean z) {
        this.mShouldRetryServerErrors = z;
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.M91<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public M91<?> setTag(Object obj) {
        this.mTag = obj;
        return this;
    }

    @DexIgnore
    public final boolean shouldCache() {
        return this.mShouldCache;
    }

    @DexIgnore
    public final boolean shouldRetryServerErrors() {
        return this.mShouldRetryServerErrors;
    }

    @DexIgnore
    public String toString() {
        String str = "0x" + Integer.toHexString(getTrafficStatsTag());
        StringBuilder sb = new StringBuilder();
        sb.append(isCanceled() ? "[X] " : "[ ] ");
        sb.append(getUrl());
        sb.append(" ");
        sb.append(str);
        sb.append(" ");
        sb.append(getPriority());
        sb.append(" ");
        sb.append(this.mSequence);
        return sb.toString();
    }
}
