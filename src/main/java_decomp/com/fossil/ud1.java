package com.fossil;

import com.fossil.Zd1;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ud1<K extends Zd1, V> {
    @DexIgnore
    public /* final */ Ai<K, V> a; // = new Ai<>();
    @DexIgnore
    public /* final */ Map<K, Ai<K, V>> b; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K, V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public List<V> b;
        @DexIgnore
        public Ai<K, V> c;
        @DexIgnore
        public Ai<K, V> d;

        @DexIgnore
        public Ai() {
            this(null);
        }

        @DexIgnore
        public Ai(K k) {
            this.d = this;
            this.c = this;
            this.a = k;
        }

        @DexIgnore
        public void a(V v) {
            if (this.b == null) {
                this.b = new ArrayList();
            }
            this.b.add(v);
        }

        @DexIgnore
        public V b() {
            int c2 = c();
            if (c2 > 0) {
                return this.b.remove(c2 - 1);
            }
            return null;
        }

        @DexIgnore
        public int c() {
            List<V> list = this.b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }
    }

    @DexIgnore
    public static <K, V> void e(Ai<K, V> ai) {
        Ai<K, V> ai2 = ai.d;
        ai2.c = ai.c;
        ai.c.d = ai2;
    }

    @DexIgnore
    public static <K, V> void g(Ai<K, V> ai) {
        ai.c.d = ai;
        ai.d.c = ai;
    }

    @DexIgnore
    public V a(K k) {
        Ai<K, V> ai = this.b.get(k);
        if (ai == null) {
            ai = new Ai<>(k);
            this.b.put(k, ai);
        } else {
            k.a();
        }
        b(ai);
        return ai.b();
    }

    @DexIgnore
    public final void b(Ai<K, V> ai) {
        e(ai);
        Ai<K, V> ai2 = this.a;
        ai.d = ai2;
        ai.c = ai2.c;
        g(ai);
    }

    @DexIgnore
    public final void c(Ai<K, V> ai) {
        e(ai);
        Ai<K, V> ai2 = this.a;
        ai.d = ai2.d;
        ai.c = ai2;
        g(ai);
    }

    @DexIgnore
    public void d(K k, V v) {
        Ai<K, V> ai = this.b.get(k);
        if (ai == null) {
            ai = new Ai<>(k);
            c(ai);
            this.b.put(k, ai);
        } else {
            k.a();
        }
        ai.a(v);
    }

    @DexIgnore
    public V f() {
        for (Ai<K, V> ai = this.a.d; !ai.equals(this.a); ai = ai.d) {
            V b2 = ai.b();
            if (b2 != null) {
                return b2;
            }
            e(ai);
            this.b.remove(ai.a);
            ai.a.a();
        }
        return null;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (Ai<K, V> ai = this.a.c; !ai.equals(this.a); ai = ai.c) {
            z = true;
            sb.append('{');
            sb.append((Object) ai.a);
            sb.append(':');
            sb.append(ai.c());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }
}
