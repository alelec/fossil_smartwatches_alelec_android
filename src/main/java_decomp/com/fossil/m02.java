package com.fossil;

import android.content.Context;
import com.fossil.C02;
import com.fossil.H02;
import com.fossil.N02;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M02 implements L02 {
    @DexIgnore
    public static volatile N02 e;
    @DexIgnore
    public /* final */ T32 a;
    @DexIgnore
    public /* final */ T32 b;
    @DexIgnore
    public /* final */ K12 c;
    @DexIgnore
    public /* final */ B22 d;

    @DexIgnore
    public M02(T32 t32, T32 t322, K12 k12, B22 b22, F22 f22) {
        this.a = t32;
        this.b = t322;
        this.c = k12;
        this.d = b22;
        f22.a();
    }

    @DexIgnore
    public static M02 c() {
        N02 n02 = e;
        if (n02 != null) {
            return n02.b();
        }
        throw new IllegalStateException("Not initialized!");
    }

    @DexIgnore
    public static Set<Ty1> d(Zz1 zz1) {
        return zz1 instanceof A02 ? Collections.unmodifiableSet(((A02) zz1).a()) : Collections.singleton(Ty1.b("proto"));
    }

    @DexIgnore
    public static void f(Context context) {
        if (e == null) {
            synchronized (M02.class) {
                try {
                    if (e == null) {
                        N02.Ai c2 = Yz1.c();
                        c2.a(context);
                        e = c2.build();
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.L02
    public void a(G02 g02, Zy1 zy1) {
        this.c.a(g02.f().e(g02.c().c()), b(g02), zy1);
    }

    @DexIgnore
    public final C02 b(G02 g02) {
        C02.Ai a2 = C02.a();
        a2.i(this.a.a());
        a2.k(this.b.a());
        a2.j(g02.g());
        a2.h(new B02(g02.b(), g02.d()));
        a2.g(g02.c().a());
        return a2.d();
    }

    @DexIgnore
    public B22 e() {
        return this.d;
    }

    @DexIgnore
    public Yy1 g(Zz1 zz1) {
        Set<Ty1> d2 = d(zz1);
        H02.Ai a2 = H02.a();
        a2.b(zz1.getName());
        a2.c(zz1.getExtras());
        return new I02(d2, a2.a(), this);
    }

    @DexIgnore
    @Deprecated
    public Yy1 h(String str) {
        Set<Ty1> d2 = d(null);
        H02.Ai a2 = H02.a();
        a2.b(str);
        return new I02(d2, a2.a(), this);
    }
}
