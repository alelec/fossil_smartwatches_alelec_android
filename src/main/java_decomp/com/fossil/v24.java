package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V24<K, V> extends T14<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ K key;
    @DexIgnore
    public /* final */ V value;

    @DexIgnore
    public V24(K k, V v) {
        this.key = k;
        this.value = v;
    }

    @DexIgnore
    @Override // java.util.Map.Entry, com.fossil.T14
    public final K getKey() {
        return this.key;
    }

    @DexIgnore
    @Override // java.util.Map.Entry, com.fossil.T14
    public final V getValue() {
        return this.value;
    }

    @DexIgnore
    @Override // java.util.Map.Entry, com.fossil.T14
    public final V setValue(V v) {
        throw new UnsupportedOperationException();
    }
}
