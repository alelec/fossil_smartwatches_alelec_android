package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ow1 extends Lw1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ow1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ow1 createFromParcel(Parcel parcel) {
            return new Ow1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ow1[] newArray(int i) {
            return new Ow1[i];
        }
    }

    @DexIgnore
    public Ow1(Parcel parcel) {
        super(parcel);
        k();
    }

    @DexIgnore
    public Ow1(Ry1 ry1, Yb0 yb0, Cc0[] cc0Arr, Cc0[] cc0Arr2, Cc0[] cc0Arr3, Cc0[] cc0Arr4, Cc0[] cc0Arr5, Cc0[] cc0Arr6, Cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        k();
    }

    @DexIgnore
    @Override // com.fossil.Lw1
    private final void k() {
        if (!(getThemeClassifier() == Ec0.d)) {
            throw new IllegalArgumentException("Incorrect theme classifier.".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Iw1, com.fossil.Iw1, com.fossil.Lw1, java.lang.Object
    public final Ow1 clone() {
        Ry1 clone = h().clone();
        Jw1 jw1 = g().b;
        Ry1 ry1 = new Ry1(g().c.getMajor(), 0);
        boolean z = g().d;
        byte[] bArr = g().e;
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        Ow1 ow1 = new Ow1(clone, new Yb0(jw1, ry1, z, copyOf), (Cc0[]) f().clone(), (Cc0[]) b().clone(), (Cc0[]) d().clone(), (Cc0[]) e().clone(), (Cc0[]) c().clone(), (Cc0[]) a().clone(), (Cc0[]) i().clone());
        ow1.f()[0] = new Cc0(G80.e(0, 1), ow1.f()[0].c);
        return ow1;
    }

    @DexIgnore
    @Override // com.fossil.Lw1
    public Nw1 edit() {
        return new Nw1(this, new Ry1(getPackageVersion().getMajor(), (getPackageVersion().getMinor() + 1) % (Hy1.c(Fq7.a) + 1)));
    }
}
