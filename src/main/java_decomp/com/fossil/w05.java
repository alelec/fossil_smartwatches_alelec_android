package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class w05 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon14 b;
    @DexIgnore
    public /* final */ /* synthetic */ RecommendedPreset c;

    @DexIgnore
    public /* synthetic */ w05(PresetRepository.Anon14 anon14, RecommendedPreset recommendedPreset) {
        this.b = anon14;
        this.c = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
