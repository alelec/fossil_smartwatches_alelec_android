package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pa5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton A;
    @DexIgnore
    public /* final */ FlexibleButton B;
    @DexIgnore
    public /* final */ FlexibleButton C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout J;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout K;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout L;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout M;
    @DexIgnore
    public /* final */ RTLImageView N;
    @DexIgnore
    public /* final */ RTLImageView O;
    @DexIgnore
    public /* final */ RTLImageView P;
    @DexIgnore
    public /* final */ RTLImageView Q;
    @DexIgnore
    public /* final */ ImageView R;
    @DexIgnore
    public /* final */ DashBar S;
    @DexIgnore
    public /* final */ ConstraintLayout T;
    @DexIgnore
    public /* final */ ScrollView U;
    @DexIgnore
    public /* final */ FlexibleTextView V;
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ FlexibleCheckBox r;
    @DexIgnore
    public /* final */ FlexibleCheckBox s;
    @DexIgnore
    public /* final */ FlexibleCheckBox t;
    @DexIgnore
    public /* final */ FlexibleCheckBox u;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText v;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText w;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText x;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText y;
    @DexIgnore
    public /* final */ FlexibleButton z;

    @DexIgnore
    public Pa5(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, FlexibleCheckBox flexibleCheckBox2, FlexibleCheckBox flexibleCheckBox3, FlexibleCheckBox flexibleCheckBox4, FlexibleCheckBox flexibleCheckBox5, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextInputEditText flexibleTextInputEditText4, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, FlexibleTextInputLayout flexibleTextInputLayout4, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, ImageView imageView, DashBar dashBar, ConstraintLayout constraintLayout, ScrollView scrollView, FlexibleTextView flexibleTextView7) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = flexibleCheckBox2;
        this.s = flexibleCheckBox3;
        this.t = flexibleCheckBox4;
        this.u = flexibleCheckBox5;
        this.v = flexibleTextInputEditText;
        this.w = flexibleTextInputEditText2;
        this.x = flexibleTextInputEditText3;
        this.y = flexibleTextInputEditText4;
        this.z = flexibleButton;
        this.A = flexibleButton2;
        this.B = flexibleButton3;
        this.C = flexibleButton4;
        this.D = flexibleTextView;
        this.E = flexibleTextView2;
        this.F = flexibleTextView3;
        this.G = flexibleTextView4;
        this.H = flexibleTextView5;
        this.I = flexibleTextView6;
        this.J = flexibleTextInputLayout;
        this.K = flexibleTextInputLayout2;
        this.L = flexibleTextInputLayout3;
        this.M = flexibleTextInputLayout4;
        this.N = rTLImageView;
        this.O = rTLImageView2;
        this.P = rTLImageView3;
        this.Q = rTLImageView4;
        this.R = imageView;
        this.S = dashBar;
        this.T = constraintLayout;
        this.U = scrollView;
        this.V = flexibleTextView7;
    }
}
