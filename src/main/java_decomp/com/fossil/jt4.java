package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Jt4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    LiveData<It4> b();

    @DexIgnore
    long c(It4 it4);

    @DexIgnore
    It4 d();
}
