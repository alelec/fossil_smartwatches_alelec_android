package com.fossil;

import android.text.TextUtils;
import com.fossil.V18;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uq5 implements Y08 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public AtomicInteger b; // = new AtomicInteger(0);
    @DexIgnore
    public Auth c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ An4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Auth $auth$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Response $response$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Uq5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Xe6 xe6, Uq5 uq5, MFUser mFUser, Auth auth, Response response) {
            super(2, xe6);
            this.this$0 = uq5;
            this.$user$inlined = mFUser;
            this.$auth$inlined = auth;
            this.$response$inlined = response;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(xe6, this.this$0, this.$user$inlined, this.$auth$inlined, this.$response$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                UserDatabase B = EncryptedDatabaseManager.j.B();
                if (!(B == null || (userDao = B.userDao()) == null)) {
                    userDao.updateUser(this.$user$inlined);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = Uq5.class.getSimpleName();
        Wg6.b(simpleName, "TokenAuthenticator::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public Uq5(AuthApiGuestService authApiGuestService, An4 an4) {
        Wg6.c(authApiGuestService, "mAuthApiGuestService");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.d = authApiGuestService;
        this.e = an4;
    }

    @DexIgnore
    public final Q88<Auth> a(MFUser mFUser, Response response, boolean z) {
        Q88<Auth> a2;
        Exception e2;
        Q88<Auth> q88 = null;
        synchronized (this) {
            FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b);
            if (z) {
                if (TextUtils.isEmpty(mFUser.getAuth().getRefreshToken())) {
                    Ku3 ku3 = new Ku3();
                    FLogger.INSTANCE.getLocal().d(f, "Exchange Legacy Token");
                    ku3.n(Constants.PROFILE_KEY_ACCESS_TOKEN, mFUser.getAuth().getAccessToken());
                    ku3.n("clientId", AppHelper.g.a(mFUser.getUserId()));
                    this.c = this.d.tokenExchangeLegacy(ku3).a().a();
                } else {
                    W18 a3 = response.a();
                    if (a3 != null) {
                        StringBuilder sb = new StringBuilder();
                        String readLine = new BufferedReader(new InputStreamReader(a3.byteStream())).readLine();
                        int length = readLine.length();
                        for (int i = 0; i < length; i++) {
                            sb.append(readLine.charAt(i));
                        }
                        String sb2 = sb.toString();
                        Wg6.b(sb2, "sb.toString()");
                        try {
                            JsonElement c2 = new Ij4().c(sb2);
                            Ku3 d2 = c2 != null ? c2.d() : null;
                            if (d2 != null && d2.s("_error")) {
                                JsonElement p = d2.p("_error");
                                Wg6.b(p, "parseResult.get(\"_error\")");
                                int b2 = p.b();
                                if (b2 != 401005) {
                                    if (b2 == 401011) {
                                        FLogger.INSTANCE.getLocal().d(f, "Refresh Token");
                                        Ku3 ku32 = new Ku3();
                                        ku32.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                                        try {
                                            a2 = this.d.tokenRefresh(ku32).a();
                                            try {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str = f;
                                                StringBuilder sb3 = new StringBuilder();
                                                sb3.append("Refresh Token error=");
                                                sb3.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                                                sb3.append(" body=");
                                                sb3.append(a2 != null ? a2.d() : null);
                                                local.d(str, sb3.toString());
                                                this.c = a2 != null ? a2.a() : null;
                                            } catch (Exception e3) {
                                                e = e3;
                                                FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e.getMessage());
                                                e.printStackTrace();
                                                return q88;
                                            }
                                        } catch (Exception e4) {
                                            e = e4;
                                            FLogger.INSTANCE.getLocal().d(f, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e.getMessage());
                                            e.printStackTrace();
                                            return q88;
                                        }
                                    } else if (b2 != 401026) {
                                        FLogger.INSTANCE.getLocal().d(f, "unauthorized: error = " + sb2);
                                        FLogger.INSTANCE.getLocal().d(f, "Refresh Token");
                                        Ku3 ku33 = new Ku3();
                                        ku33.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                                        try {
                                            a2 = this.d.tokenRefresh(ku33).a();
                                            try {
                                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                                String str2 = f;
                                                StringBuilder sb4 = new StringBuilder();
                                                sb4.append("Refresh Token error=");
                                                sb4.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                                                sb4.append(" body=");
                                                sb4.append(a2 != null ? a2.d() : null);
                                                local2.d(str2, sb4.toString());
                                                this.c = a2 != null ? a2.a() : null;
                                            } catch (Exception e5) {
                                                e2 = e5;
                                                try {
                                                    FLogger.INSTANCE.getLocal().d(AuthenticationInterceptor.g.a(), "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                                                    e2.printStackTrace();
                                                } catch (Exception e6) {
                                                    q88 = a2;
                                                    FLogger.INSTANCE.getLocal().d(f, "Crash when trying to parse body " + sb2);
                                                    return q88;
                                                }
                                                return q88;
                                            }
                                        } catch (Exception e7) {
                                            e2 = e7;
                                            a2 = null;
                                            FLogger.INSTANCE.getLocal().d(AuthenticationInterceptor.g.a(), "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                                            e2.printStackTrace();
                                            return q88;
                                        }
                                    } else {
                                        FLogger.INSTANCE.getLocal().d(f, "Exchange Legacy Token failed");
                                    }
                                    q88 = a2;
                                } else {
                                    FLogger.INSTANCE.getLocal().d(f, "Token is in black list");
                                }
                            }
                        } catch (Exception e8) {
                            FLogger.INSTANCE.getLocal().d(f, "Crash when trying to parse body " + sb2);
                            return q88;
                        }
                    }
                }
            }
        }
        return q88;
    }

    @DexIgnore
    @Override // com.fossil.Y08
    public V18 authenticate(X18 x18, Response response) {
        String f2;
        String f3;
        String accessToken;
        MFUser.Auth auth;
        UserDao userDao;
        int i = 0;
        Wg6.c(response, "response");
        UserDatabase B = EncryptedDatabaseManager.j.B();
        MFUser currentUser = (B == null || (userDao = B.userDao()) == null) ? null : userDao.getCurrentUser();
        if (TextUtils.isEmpty((currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken())) {
            FLogger.INSTANCE.getLocal().d(f, "unauthorized: userAccessToken is null or empty");
            return null;
        }
        boolean z = this.b.getAndIncrement() == 0;
        this.c = null;
        if (currentUser != null) {
            Q88<Auth> a2 = a(currentUser, response, z);
            Auth auth2 = this.c;
            if (this.b.decrementAndGet() == 0) {
                this.c = null;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "authenticate decrementAndGet mCount=" + this.b + " address=" + this);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "old headers=" + response.G().e().toString());
            if (auth2 != null && (accessToken = auth2.getAccessToken()) != null) {
                currentUser.getAuth().setAccessToken(accessToken);
                MFUser.Auth auth3 = currentUser.getAuth();
                String refreshToken = auth2.getRefreshToken();
                if (refreshToken != null) {
                    auth3.setRefreshToken(refreshToken);
                    MFUser.Auth auth4 = currentUser.getAuth();
                    String w0 = TimeUtils.w0(auth2.getAccessTokenExpiresAt());
                    Wg6.b(w0, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                    auth4.setAccessTokenExpiresAt(w0);
                    MFUser.Auth auth5 = currentUser.getAuth();
                    Integer accessTokenExpiresIn = auth2.getAccessTokenExpiresIn();
                    if (accessTokenExpiresIn != null) {
                        i = accessTokenExpiresIn.intValue();
                    }
                    auth5.setAccessTokenExpiresIn(i);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    local3.d(str3, "accessToken = " + accessToken + ", refreshToken = " + auth2.getRefreshToken());
                    this.e.V1(accessToken);
                    this.e.H0(System.currentTimeMillis());
                    Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ai(null, this, currentUser, auth2, response), 3, null);
                    V18.Ai h = response.G().h();
                    h.e("Authorization", "Bearer " + accessToken);
                    return h.b();
                }
                Wg6.i();
                throw null;
            } else if (a2 != null && a2.b() >= 500 && a2.b() < 600) {
                ServerError serverError = new ServerError();
                serverError.setCode(Integer.valueOf(a2.b()));
                W18 d2 = a2.d();
                if (d2 == null || (f3 = d2.string()) == null) {
                    f3 = a2.f();
                }
                serverError.setMessage(f3);
                throw new ServerErrorException(serverError);
            } else if (!z || a2 == null) {
                return null;
            } else {
                try {
                    Gson gson = new Gson();
                    W18 a3 = a2.g().a();
                    if (a3 == null || (f2 = a3.string()) == null) {
                        f2 = a2.f();
                    }
                    ServerError serverError2 = (ServerError) gson.k(f2, ServerError.class);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str4 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("forceLogout userMessage=");
                    Wg6.b(serverError2, "serverError");
                    sb.append(serverError2.getUserMessage());
                    local4.d(str4, sb.toString());
                    PortfolioApp.get.instance().F(serverError2);
                    return null;
                } catch (Exception e2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str5 = f;
                    local5.d(str5, "forceLogout ex=" + e2.getMessage());
                    e2.printStackTrace();
                    PortfolioApp.get.instance().F(null);
                    return null;
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }
}
