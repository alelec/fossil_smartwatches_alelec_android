package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.NestedScrollableHost;
import com.portfolio.platform.view.watchface.WatchFaceCoverView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ug5 {
    @DexIgnore
    public /* final */ ConstraintLayout a;
    @DexIgnore
    public /* final */ Barrier b;
    @DexIgnore
    public /* final */ WatchFaceCoverView c;
    @DexIgnore
    public /* final */ ConstraintLayout d;
    @DexIgnore
    public /* final */ RecyclerView e;
    @DexIgnore
    public /* final */ NestedScrollableHost f;
    @DexIgnore
    public /* final */ WatchFacePreviewView g;

    @DexIgnore
    public Ug5(ConstraintLayout constraintLayout, Barrier barrier, WatchFaceCoverView watchFaceCoverView, ConstraintLayout constraintLayout2, RecyclerView recyclerView, NestedScrollableHost nestedScrollableHost, WatchFacePreviewView watchFacePreviewView) {
        this.a = constraintLayout;
        this.b = barrier;
        this.c = watchFaceCoverView;
        this.d = constraintLayout2;
        this.e = recyclerView;
        this.f = nestedScrollableHost;
        this.g = watchFacePreviewView;
    }

    @DexIgnore
    public static Ug5 a(View view) {
        int i = 2131363538;
        Barrier barrier = (Barrier) view.findViewById(2131361911);
        if (barrier != null) {
            WatchFaceCoverView watchFaceCoverView = (WatchFaceCoverView) view.findViewById(2131362768);
            if (watchFaceCoverView != null) {
                ConstraintLayout constraintLayout = (ConstraintLayout) view;
                RecyclerView recyclerView = (RecyclerView) view.findViewById(2131363035);
                if (recyclerView != null) {
                    NestedScrollableHost nestedScrollableHost = (NestedScrollableHost) view.findViewById(2131363036);
                    if (nestedScrollableHost != null) {
                        WatchFacePreviewView watchFacePreviewView = (WatchFacePreviewView) view.findViewById(2131363538);
                        if (watchFacePreviewView != null) {
                            return new Ug5(constraintLayout, barrier, watchFaceCoverView, constraintLayout, recyclerView, nestedScrollableHost, watchFacePreviewView);
                        }
                    } else {
                        i = 2131363036;
                    }
                } else {
                    i = 2131363035;
                }
            } else {
                i = 2131362768;
            }
        } else {
            i = 2131361911;
        }
        throw new NullPointerException("Missing required view with ID: ".concat(view.getResources().getResourceName(i)));
    }

    @DexIgnore
    public static Ug5 c(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        View inflate = layoutInflater.inflate(2131558857, viewGroup, false);
        if (z) {
            viewGroup.addView(inflate);
        }
        return a(inflate);
    }

    @DexIgnore
    public ConstraintLayout b() {
        return this.a;
    }
}
