package com.fossil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I33 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int b;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> c;
    @DexIgnore
    public /* final */ /* synthetic */ G33 d;

    @DexIgnore
    public I33(G33 g33) {
        this.d = g33;
        this.b = this.d.c.size();
    }

    @DexIgnore
    public /* synthetic */ I33(G33 g33, J33 j33) {
        this(g33);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.c == null) {
            this.c = this.d.g.entrySet().iterator();
        }
        return this.c;
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.b;
        return (i > 0 && i <= this.d.c.size()) || a().hasNext();
    }

    @DexIgnore
    @Override // java.util.Iterator
    public final /* synthetic */ Object next() {
        if (a().hasNext()) {
            return (Map.Entry) a().next();
        }
        List list = this.d.c;
        int i = this.b - 1;
        this.b = i;
        return (Map.Entry) list.get(i);
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
