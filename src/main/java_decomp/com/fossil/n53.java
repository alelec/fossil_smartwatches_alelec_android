package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N53 implements Xw2<Q53> {
    @DexIgnore
    public static N53 c; // = new N53();
    @DexIgnore
    public /* final */ Xw2<Q53> b;

    @DexIgnore
    public N53() {
        this(Ww2.b(new P53()));
    }

    @DexIgnore
    public N53(Xw2<Q53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((Q53) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Q53 zza() {
        return this.b.zza();
    }
}
