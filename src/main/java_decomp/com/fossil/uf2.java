package com.fossil;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uf2 implements Runnable {
    @DexIgnore
    public /* final */ Runnable b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public Uf2(Runnable runnable, int i) {
        this.b = runnable;
        this.c = i;
    }

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(this.c);
        this.b.run();
    }
}
