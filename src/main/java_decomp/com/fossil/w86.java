package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W86 implements Factory<U86> {
    @DexIgnore
    public static U86 a(V86 v86) {
        U86 a2 = v86.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
