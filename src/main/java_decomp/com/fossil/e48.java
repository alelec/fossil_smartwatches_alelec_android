package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E48 {
    @DexIgnore
    public static final byte[] a(String str) {
        Wg6.c(str, "$this$asUtf8ToByteArray");
        byte[] bytes = str.getBytes(Et7.a);
        Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }

    @DexIgnore
    public static final String b(byte[] bArr) {
        Wg6.c(bArr, "$this$toUtf8String");
        return new String(bArr, Et7.a);
    }
}
