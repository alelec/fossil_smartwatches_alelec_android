package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ee2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ee2> CREATOR; // = new De2();
    @DexIgnore
    public Bundle b;
    @DexIgnore
    public B62[] c;
    @DexIgnore
    public int d;

    @DexIgnore
    public Ee2() {
    }

    @DexIgnore
    public Ee2(Bundle bundle, B62[] b62Arr, int i) {
        this.b = bundle;
        this.c = b62Arr;
        this.d = i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.e(parcel, 1, this.b, false);
        Bd2.x(parcel, 2, this.c, i, false);
        Bd2.n(parcel, 3, this.d);
        Bd2.b(parcel, a2);
    }
}
