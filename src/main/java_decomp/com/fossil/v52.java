package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V52 {
    @DexIgnore
    public static /* final */ int common_full_open_on_phone; // = 2131230919;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark; // = 2131230920;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_focused; // = 2131230921;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal; // = 2131230922;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal_background; // = 2131230923;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_disabled; // = 2131230924;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light; // = 2131230925;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_focused; // = 2131230926;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal; // = 2131230927;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal_background; // = 2131230928;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark; // = 2131230929;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_focused; // = 2131230930;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal; // = 2131230931;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal_background; // = 2131230932;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_disabled; // = 2131230933;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light; // = 2131230934;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_focused; // = 2131230935;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal; // = 2131230936;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal_background; // = 2131230937;
    @DexIgnore
    public static /* final */ int googleg_disabled_color_18; // = 2131230964;
    @DexIgnore
    public static /* final */ int googleg_standard_color_18; // = 2131230965;
}
