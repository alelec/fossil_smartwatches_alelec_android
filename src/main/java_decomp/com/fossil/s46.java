package com.fossil;

import android.util.SparseArray;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface S46 extends Gq4<R46> {
    @DexIgnore
    void k5(SparseArray<List<BaseFeatureModel>> sparseArray);
}
