package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class En7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "kotlin.collections.SlidingWindowKt$windowedIterator$1", f = "SlidingWindow.kt", l = {34, 40, 49, 55, 58}, m = "invokeSuspend")
    public static final class Ai extends Jo7 implements Coroutine<Vs7<? super List<? extends T>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator $iterator;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $partialWindows;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $reuseBuffer;
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public /* final */ /* synthetic */ int $step;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Vs7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(int i, int i2, Iterator it, boolean z, boolean z2, Xe6 xe6) {
            super(2, xe6);
            this.$size = i;
            this.$step = i2;
            this.$iterator = it;
            this.$reuseBuffer = z;
            this.$partialWindows = z2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$size, this.$step, this.$iterator, this.$reuseBuffer, this.$partialWindows, xe6);
            ai.p$ = (Vs7) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Object obj, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(obj, xe6)).invokeSuspend(Cd6.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0086  */
        /* JADX WARNING: Removed duplicated region for block: B:70:0x017d  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0179 A[SYNTHETIC] */
        @Override // com.fossil.Zn7
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 435
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.En7.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        String str;
        if (!(i > 0 && i2 > 0)) {
            if (i != i2) {
                str = "Both size " + i + " and step " + i2 + " must be greater than zero.";
            } else {
                str = "size " + i + " must be greater than zero.";
            }
            throw new IllegalArgumentException(str.toString());
        }
    }

    @DexIgnore
    public static final <T> Iterator<List<T>> b(Iterator<? extends T> it, int i, int i2, boolean z, boolean z2) {
        Wg6.c(it, "iterator");
        return !it.hasNext() ? Qm7.b : Ws7.a(new Ai(i, i2, it, z2, z, null));
    }
}
