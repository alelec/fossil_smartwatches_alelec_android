package com.fossil.imagefilters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum FilterType {
    DIRECT_MAPPING(0),
    FLOYD_DITHERING(1),
    SIERRA_DITHERING(2),
    SIERRA_TWO_ROW_DITHERING(3),
    SIERRA_LITE_DITHERING(4),
    BURKES_DITHERING(5),
    ORDERED_DITHERING(6),
    ATKINSON_DITHERING(7),
    STUCKI_DITHERING(8),
    JAJUNI_DITHERING(9);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public FilterType(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
