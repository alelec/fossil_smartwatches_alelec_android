package com.fossil.imagefilters;

import android.graphics.Bitmap;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class EInkImageFactory {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends EInkImageFactory {
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native Bitmap decode(byte[] bArr, Format format);

        @DexIgnore
        public static native byte[] encode(Bitmap bitmap, FilterType filterType, OutputSettings outputSettings);

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }
    }

    @DexIgnore
    public static Bitmap decode(byte[] bArr, Format format) {
        return CppProxy.decode(bArr, format);
    }

    @DexIgnore
    public static byte[] encode(Bitmap bitmap, FilterType filterType, OutputSettings outputSettings) {
        return CppProxy.encode(bitmap, filterType, outputSettings);
    }
}
