package com.fossil.imagefilters;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FilterResult {
    @DexIgnore
    public /* final */ byte[] mData;
    @DexIgnore
    public /* final */ Bitmap mPreview;
    @DexIgnore
    public /* final */ FilterType mType;

    @DexIgnore
    public FilterResult(FilterType filterType, byte[] bArr, Bitmap bitmap) {
        this.mType = filterType;
        this.mData = bArr;
        this.mPreview = bitmap;
    }

    @DexIgnore
    public byte[] getData() {
        return this.mData;
    }

    @DexIgnore
    public Bitmap getPreview() {
        return this.mPreview;
    }

    @DexIgnore
    public FilterType getType() {
        return this.mType;
    }

    @DexIgnore
    public String toString() {
        return "FilterResult{mType=" + this.mType + ",mData=" + this.mData + ",mPreview=" + this.mPreview + "}";
    }
}
