package com.fossil;

import com.fossil.J38;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I38 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService E; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), B28.G("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ Socket A;
    @DexIgnore
    public /* final */ L38 B;
    @DexIgnore
    public /* final */ Li C;
    @DexIgnore
    public /* final */ Set<Integer> D; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ Ji c;
    @DexIgnore
    public /* final */ Map<Integer, K38> d; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ ScheduledExecutorService i;
    @DexIgnore
    public /* final */ ExecutorService j;
    @DexIgnore
    public /* final */ N38 k;
    @DexIgnore
    public long l; // = 0;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public long s; // = 0;
    @DexIgnore
    public long t; // = 0;
    @DexIgnore
    public long u; // = 0;
    @DexIgnore
    public long v; // = 0;
    @DexIgnore
    public long w; // = 0;
    @DexIgnore
    public long x;
    @DexIgnore
    public O38 y; // = new O38();
    @DexIgnore
    public /* final */ O38 z; // = new O38();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ D38 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(String str, Object[] objArr, int i, D38 d38) {
            super(str, objArr);
            this.c = i;
            this.d = d38;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            try {
                I38.this.t0(this.c, this.d);
            } catch (IOException e2) {
                I38.this.C();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.c = i;
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            try {
                I38.this.B.D(this.c, this.d);
            } catch (IOException e2) {
                I38.this.C();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends A28 {
        @DexIgnore
        public Ci(String str, Object... objArr) {
            super(str, objArr);
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            I38.this.s0(false, 2, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.c = i;
            this.d = list;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            if (I38.this.k.a(this.c, this.d)) {
                try {
                    I38.this.B.A(this.c, D38.CANCEL);
                    synchronized (I38.this) {
                        I38.this.D.remove(Integer.valueOf(this.c));
                    }
                } catch (IOException e2) {
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(String str, Object[] objArr, int i, List list, boolean z) {
            super(str, objArr);
            this.c = i;
            this.d = list;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            boolean b = I38.this.k.b(this.c, this.d, this.e);
            if (b) {
                try {
                    I38.this.B.A(this.c, D38.CANCEL);
                } catch (IOException e2) {
                    return;
                }
            }
            if (b || this.e) {
                synchronized (I38.this) {
                    I38.this.D.remove(Integer.valueOf(this.c));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ I48 d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(String str, Object[] objArr, int i, I48 i48, int i2, boolean z) {
            super(str, objArr);
            this.c = i;
            this.d = i48;
            this.e = i2;
            this.f = z;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            try {
                boolean d2 = I38.this.k.d(this.c, this.d, this.e, this.f);
                if (d2) {
                    I38.this.B.A(this.c, D38.CANCEL);
                }
                if (d2 || this.f) {
                    synchronized (I38.this) {
                        I38.this.D.remove(Integer.valueOf(this.c));
                    }
                }
            } catch (IOException e2) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends A28 {
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ D38 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(String str, Object[] objArr, int i, D38 d38) {
            super(str, objArr);
            this.c = i;
            this.d = d38;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            I38.this.k.c(this.c, this.d);
            synchronized (I38.this) {
                I38.this.D.remove(Integer.valueOf(this.c));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Hi {
        @DexIgnore
        public Socket a;
        @DexIgnore
        public String b;
        @DexIgnore
        public K48 c;
        @DexIgnore
        public J48 d;
        @DexIgnore
        public Ji e; // = Ji.a;
        @DexIgnore
        public N38 f; // = N38.a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public Hi(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public I38 a() {
            return new I38(this);
        }

        @DexIgnore
        public Hi b(Ji ji) {
            this.e = ji;
            return this;
        }

        @DexIgnore
        public Hi c(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public Hi d(Socket socket, String str, K48 k48, J48 j48) {
            this.a = socket;
            this.b = str;
            this.c = k48;
            this.d = j48;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ii extends A28 {
        @DexIgnore
        public Ii() {
            super("OkHttp %s ping", I38.this.e);
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            boolean z;
            synchronized (I38.this) {
                if (I38.this.m < I38.this.l) {
                    z = true;
                } else {
                    I38.h(I38.this);
                    z = false;
                }
            }
            if (z) {
                I38.this.C();
            } else {
                I38.this.s0(false, 1, 0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ji {
        @DexIgnore
        public static /* final */ Ji a; // = new Aii();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Ji {
            @DexIgnore
            @Override // com.fossil.I38.Ji
            public void c(K38 k38) throws IOException {
                k38.f(D38.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public void b(I38 i38) {
        }

        @DexIgnore
        public abstract void c(K38 k38) throws IOException;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ki extends A28 {
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        public Ki(boolean z, int i, int i2) {
            super("OkHttp %s ping %08x%08x", I38.this.e, Integer.valueOf(i), Integer.valueOf(i2));
            this.c = z;
            this.d = i;
            this.e = i2;
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            I38.this.s0(this.c, this.d, this.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li extends A28 implements J38.Bi {
        @DexIgnore
        public /* final */ J38 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends A28 {
            @DexIgnore
            public /* final */ /* synthetic */ K38 c;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(String str, Object[] objArr, K38 k38) {
                super(str, objArr);
                this.c = k38;
            }

            @DexIgnore
            @Override // com.fossil.A28
            public void k() {
                try {
                    I38.this.c.c(this.c);
                } catch (IOException e) {
                    W38 j = W38.j();
                    j.q(4, "Http2Connection.Listener failure for " + I38.this.e, e);
                    try {
                        this.c.f(D38.PROTOCOL_ERROR);
                    } catch (IOException e2) {
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii extends A28 {
            @DexIgnore
            public /* final */ /* synthetic */ boolean c;
            @DexIgnore
            public /* final */ /* synthetic */ O38 d;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(String str, Object[] objArr, boolean z, O38 o38) {
                super(str, objArr);
                this.c = z;
                this.d = o38;
            }

            @DexIgnore
            @Override // com.fossil.A28
            public void k() {
                Li.this.l(this.c, this.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Cii extends A28 {
            @DexIgnore
            public Cii(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            @Override // com.fossil.A28
            public void k() {
                I38 i38 = I38.this;
                i38.c.b(i38);
            }
        }

        @DexIgnore
        public Li(J38 j38) {
            super("OkHttp %s", I38.this.e);
            this.c = j38;
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void b(boolean z, O38 o38) {
            try {
                I38.this.i.execute(new Bii("OkHttp %s ACK Settings", new Object[]{I38.this.e}, z, o38));
            } catch (RejectedExecutionException e) {
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0079, code lost:
            r0.q(r12);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x007c, code lost:
            if (r9 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x007e, code lost:
            r0.p();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        @DexIgnore
        @Override // com.fossil.J38.Bi
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void c(boolean r9, int r10, int r11, java.util.List<com.fossil.E38> r12) {
            /*
                r8 = this;
                com.fossil.I38 r0 = com.fossil.I38.this
                boolean r0 = r0.X(r10)
                if (r0 == 0) goto L_0x000e
                com.fossil.I38 r0 = com.fossil.I38.this
                r0.S(r10, r12, r9)
            L_0x000d:
                return
            L_0x000e:
                com.fossil.I38 r6 = com.fossil.I38.this
                monitor-enter(r6)
                com.fossil.I38 r0 = com.fossil.I38.this     // Catch:{ all -> 0x0023 }
                com.fossil.K38 r0 = r0.D(r10)     // Catch:{ all -> 0x0023 }
                if (r0 != 0) goto L_0x0078
                com.fossil.I38 r0 = com.fossil.I38.this     // Catch:{ all -> 0x0023 }
                boolean r0 = com.fossil.I38.j(r0)     // Catch:{ all -> 0x0023 }
                if (r0 == 0) goto L_0x0026
                monitor-exit(r6)     // Catch:{ all -> 0x0023 }
                goto L_0x000d
            L_0x0023:
                r0 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0023 }
                throw r0
            L_0x0026:
                com.fossil.I38 r0 = com.fossil.I38.this
                int r0 = r0.f
                if (r10 > r0) goto L_0x002e
                monitor-exit(r6)
                goto L_0x000d
            L_0x002e:
                int r0 = r10 % 2
                com.fossil.I38 r1 = com.fossil.I38.this
                int r1 = r1.g
                int r1 = r1 % 2
                if (r0 != r1) goto L_0x003a
                monitor-exit(r6)
                goto L_0x000d
            L_0x003a:
                com.fossil.P18 r5 = com.fossil.B28.H(r12)
                com.fossil.K38 r0 = new com.fossil.K38
                com.fossil.I38 r2 = com.fossil.I38.this
                r3 = 0
                r1 = r10
                r4 = r9
                r0.<init>(r1, r2, r3, r4, r5)
                com.fossil.I38 r1 = com.fossil.I38.this
                r1.f = r10
                com.fossil.I38 r1 = com.fossil.I38.this
                java.util.Map<java.lang.Integer, com.fossil.K38> r1 = r1.d
                java.lang.Integer r2 = java.lang.Integer.valueOf(r10)
                r1.put(r2, r0)
                java.util.concurrent.ExecutorService r1 = com.fossil.I38.l()
                com.fossil.I38$Li$Aii r2 = new com.fossil.I38$Li$Aii
                java.lang.String r3 = "OkHttp %s stream %d"
                r4 = 2
                java.lang.Object[] r4 = new java.lang.Object[r4]
                r5 = 0
                com.fossil.I38 r7 = com.fossil.I38.this
                java.lang.String r7 = r7.e
                r4[r5] = r7
                r5 = 1
                java.lang.Integer r7 = java.lang.Integer.valueOf(r10)
                r4[r5] = r7
                r2.<init>(r3, r4, r0)
                r1.execute(r2)
                monitor-exit(r6)
                goto L_0x000d
            L_0x0078:
                monitor-exit(r6)
                r0.q(r12)
                if (r9 == 0) goto L_0x000d
                r0.p()
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.I38.Li.c(boolean, int, int, java.util.List):void");
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void d(int i, long j) {
            if (i == 0) {
                synchronized (I38.this) {
                    I38.this.x += j;
                    I38.this.notifyAll();
                }
                return;
            }
            K38 D = I38.this.D(i);
            if (D != null) {
                synchronized (D) {
                    D.c(j);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void e(boolean z, int i, K48 k48, int i2) throws IOException {
            if (I38.this.X(i)) {
                I38.this.P(i, k48, i2, z);
                return;
            }
            K38 D = I38.this.D(i);
            if (D == null) {
                I38.this.u0(i, D38.PROTOCOL_ERROR);
                long j = (long) i2;
                I38.this.q0(j);
                k48.skip(j);
                return;
            }
            D.o(k48, i2);
            if (z) {
                D.p();
            }
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void f(boolean z, int i, int i2) {
            if (z) {
                synchronized (I38.this) {
                    if (i == 1) {
                        I38.c(I38.this);
                    } else if (i == 2) {
                        I38.o(I38.this);
                    } else if (i == 3) {
                        I38.A(I38.this);
                        I38.this.notifyAll();
                    }
                }
                return;
            }
            try {
                I38.this.i.execute(new Ki(true, i, i2));
            } catch (RejectedExecutionException e) {
            }
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void g(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void h(int i, D38 d38) {
            if (I38.this.X(i)) {
                I38.this.V(i, d38);
                return;
            }
            K38 b0 = I38.this.b0(i);
            if (b0 != null) {
                b0.r(d38);
            }
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void i(int i, int i2, List<E38> list) {
            I38.this.T(i2, list);
        }

        @DexIgnore
        @Override // com.fossil.J38.Bi
        public void j(int i, D38 d38, L48 l48) {
            K38[] k38Arr;
            l48.size();
            synchronized (I38.this) {
                k38Arr = (K38[]) I38.this.d.values().toArray(new K38[I38.this.d.size()]);
                I38.this.h = true;
            }
            for (K38 k38 : k38Arr) {
                if (k38.i() > i && k38.l()) {
                    k38.r(D38.REFUSED_STREAM);
                    I38.this.b0(k38.i());
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.A28
        public void k() {
            D38 d38;
            D38 d382;
            I38 i38;
            D38 d383 = D38.INTERNAL_ERROR;
            try {
                this.c.c(this);
                do {
                } while (this.c.b(false, this));
                d38 = D38.NO_ERROR;
                try {
                    d382 = D38.CANCEL;
                    try {
                        i38 = I38.this;
                    } catch (IOException e) {
                    }
                } catch (IOException e2) {
                    try {
                        d38 = D38.PROTOCOL_ERROR;
                        d382 = D38.PROTOCOL_ERROR;
                        i38 = I38.this;
                        i38.B(d38, d382);
                        B28.g(this.c);
                    } catch (Throwable th) {
                        th = th;
                        try {
                            I38.this.B(d38, d383);
                        } catch (IOException e3) {
                        }
                        B28.g(this.c);
                        throw th;
                    }
                }
            } catch (IOException e4) {
                d38 = d383;
                d38 = D38.PROTOCOL_ERROR;
                d382 = D38.PROTOCOL_ERROR;
                i38 = I38.this;
                i38.B(d38, d382);
                B28.g(this.c);
            } catch (Throwable th2) {
                th = th2;
                d38 = d383;
                I38.this.B(d38, d383);
                B28.g(this.c);
                throw th;
            }
            i38.B(d38, d382);
            B28.g(this.c);
        }

        @DexIgnore
        public void l(boolean z, O38 o38) {
            long j;
            K38[] k38Arr;
            synchronized (I38.this.B) {
                synchronized (I38.this) {
                    int d2 = I38.this.z.d();
                    if (z) {
                        I38.this.z.a();
                    }
                    I38.this.z.h(o38);
                    int d3 = I38.this.z.d();
                    if (d3 == -1 || d3 == d2) {
                        j = 0;
                        k38Arr = null;
                    } else {
                        j = (long) (d3 - d2);
                        k38Arr = !I38.this.d.isEmpty() ? (K38[]) I38.this.d.values().toArray(new K38[I38.this.d.size()]) : null;
                    }
                }
                try {
                    I38.this.B.a(I38.this.z);
                } catch (IOException e) {
                    I38.this.C();
                }
            }
            if (k38Arr != null) {
                for (K38 k38 : k38Arr) {
                    synchronized (k38) {
                        k38.c(j);
                    }
                }
            }
            I38.E.execute(new Cii("OkHttp %s settings", I38.this.e));
        }
    }

    @DexIgnore
    public I38(Hi hi) {
        this.k = hi.f;
        boolean z2 = hi.g;
        this.b = z2;
        this.c = hi.e;
        int i2 = z2 ? 1 : 2;
        this.g = i2;
        if (hi.g) {
            this.g = i2 + 2;
        }
        if (hi.g) {
            this.y.i(7, 16777216);
        }
        this.e = hi.b;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, B28.G(B28.r("OkHttp %s Writer", this.e), false));
        this.i = scheduledThreadPoolExecutor;
        if (hi.h != 0) {
            Ii ii = new Ii();
            int i3 = hi.h;
            scheduledThreadPoolExecutor.scheduleAtFixedRate(ii, (long) i3, (long) i3, TimeUnit.MILLISECONDS);
        }
        this.j = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), B28.G(B28.r("OkHttp %s Push Observer", this.e), true));
        this.z.i(7, 65535);
        this.z.i(5, 16384);
        this.x = (long) this.z.d();
        this.A = hi.a;
        this.B = new L38(hi.d, this.b);
        this.C = new Li(new J38(hi.c, this.b));
    }

    @DexIgnore
    public static /* synthetic */ long A(I38 i38) {
        long j2 = i38.u;
        i38.u = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long c(I38 i38) {
        long j2 = i38.m;
        i38.m = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long h(I38 i38) {
        long j2 = i38.l;
        i38.l = 1 + j2;
        return j2;
    }

    @DexIgnore
    public static /* synthetic */ long o(I38 i38) {
        long j2 = i38.t;
        i38.t = 1 + j2;
        return j2;
    }

    @DexIgnore
    public void B(D38 d38, D38 d382) throws IOException {
        K38[] k38Arr;
        try {
            i0(d38);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.d.isEmpty()) {
                this.d.clear();
                k38Arr = (K38[]) this.d.values().toArray(new K38[this.d.size()]);
            } else {
                k38Arr = null;
            }
        }
        if (k38Arr != null) {
            IOException iOException = e;
            for (K38 k38 : k38Arr) {
                try {
                    k38.f(d382);
                } catch (IOException e3) {
                    if (iOException != null) {
                        iOException = e3;
                    }
                }
            }
            e = iOException;
        }
        try {
            this.B.close();
            e = e;
        } catch (IOException e4) {
            e = e4;
            if (e != null) {
                e = e;
            }
        }
        try {
            this.A.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.i.shutdown();
        this.j.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public final void C() {
        try {
            B(D38.PROTOCOL_ERROR, D38.PROTOCOL_ERROR);
        } catch (IOException e2) {
        }
    }

    @DexIgnore
    public K38 D(int i2) {
        K38 k38;
        synchronized (this) {
            k38 = this.d.get(Integer.valueOf(i2));
        }
        return k38;
    }

    @DexIgnore
    public boolean F(long j2) {
        synchronized (this) {
            if (this.h) {
                return false;
            }
            return this.t >= this.s || j2 < this.v;
        }
    }

    @DexIgnore
    public int G() {
        int e2;
        synchronized (this) {
            e2 = this.z.e(Integer.MAX_VALUE);
        }
        return e2;
    }

    @DexIgnore
    public final K38 L(int i2, List<E38> list, boolean z2) throws IOException {
        int i3;
        K38 k38;
        boolean z3;
        boolean z4 = !z2;
        synchronized (this.B) {
            synchronized (this) {
                if (this.g > 1073741823) {
                    i0(D38.REFUSED_STREAM);
                }
                if (!this.h) {
                    i3 = this.g;
                    this.g += 2;
                    k38 = new K38(i3, this, z4, false, null);
                    z3 = !z2 || this.x == 0 || k38.b == 0;
                    if (k38.m()) {
                        this.d.put(Integer.valueOf(i3), k38);
                    }
                } else {
                    throw new C38();
                }
            }
            if (i2 == 0) {
                this.B.C(z4, i3, i2, list);
            } else if (!this.b) {
                this.B.o(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (z3) {
            this.B.flush();
        }
        return k38;
    }

    @DexIgnore
    public K38 M(List<E38> list, boolean z2) throws IOException {
        return L(0, list, z2);
    }

    @DexIgnore
    public void P(int i2, K48 k48, int i3, boolean z2) throws IOException {
        I48 i48 = new I48();
        long j2 = (long) i3;
        k48.j0(j2);
        k48.d0(i48, j2);
        if (i48.p0() == j2) {
            Q(new Fi("OkHttp %s Push Data[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, i48, i3, z2));
            return;
        }
        throw new IOException(i48.p0() + " != " + i3);
    }

    @DexIgnore
    public final void Q(A28 a28) {
        synchronized (this) {
            if (!this.h) {
                this.j.execute(a28);
            }
        }
    }

    @DexIgnore
    public void S(int i2, List<E38> list, boolean z2) {
        try {
            Q(new Ei("OkHttp %s Push Headers[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, list, z2));
        } catch (RejectedExecutionException e2) {
        }
    }

    @DexIgnore
    public void T(int i2, List<E38> list) {
        synchronized (this) {
            if (this.D.contains(Integer.valueOf(i2))) {
                u0(i2, D38.PROTOCOL_ERROR);
                return;
            }
            this.D.add(Integer.valueOf(i2));
            try {
                Q(new Di("OkHttp %s Push Request[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException e2) {
            }
        }
    }

    @DexIgnore
    public void V(int i2, D38 d38) {
        Q(new Gi("OkHttp %s Push Reset[%s]", new Object[]{this.e, Integer.valueOf(i2)}, i2, d38));
    }

    @DexIgnore
    public boolean X(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public K38 b0(int i2) {
        K38 remove;
        synchronized (this) {
            remove = this.d.remove(Integer.valueOf(i2));
            notifyAll();
        }
        return remove;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        B(D38.NO_ERROR, D38.CANCEL);
    }

    @DexIgnore
    public void flush() throws IOException {
        this.B.flush();
    }

    @DexIgnore
    public void g0() {
        synchronized (this) {
            if (this.t >= this.s) {
                this.s++;
                this.v = System.nanoTime() + 1000000000;
                try {
                    this.i.execute(new Ci("OkHttp %s ping", this.e));
                } catch (RejectedExecutionException e2) {
                }
            }
        }
    }

    @DexIgnore
    public void i0(D38 d38) throws IOException {
        synchronized (this.B) {
            synchronized (this) {
                if (!this.h) {
                    this.h = true;
                    this.B.j(this.f, d38, B28.a);
                }
            }
        }
    }

    @DexIgnore
    public void o0() throws IOException {
        p0(true);
    }

    @DexIgnore
    public void p0(boolean z2) throws IOException {
        if (z2) {
            this.B.b();
            this.B.B(this.y);
            int d2 = this.y.d();
            if (d2 != 65535) {
                this.B.D(0, (long) (d2 - 65535));
            }
        }
        new Thread(this.C).start();
    }

    @DexIgnore
    public void q0(long j2) {
        synchronized (this) {
            long j3 = this.w + j2;
            this.w = j3;
            if (j3 >= ((long) (this.y.d() / 2))) {
                v0(0, this.w);
                this.w = 0;
            }
        }
    }

    @DexIgnore
    public void r0(int i2, boolean z2, I48 i48, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.B.c(z2, i2, i48, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (this.x <= 0) {
                    try {
                        if (this.d.containsKey(Integer.valueOf(i2))) {
                            wait();
                        } else {
                            throw new IOException("stream closed");
                        }
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                        throw new InterruptedIOException();
                    }
                }
                min = Math.min((int) Math.min(j2, this.x), this.B.l());
                j3 = (long) min;
                this.x -= j3;
            }
            j2 -= j3;
            this.B.c(z2 && j2 == 0, i2, i48, min);
        }
    }

    @DexIgnore
    public void s0(boolean z2, int i2, int i3) {
        try {
            this.B.m(z2, i2, i3);
        } catch (IOException e2) {
            C();
        }
    }

    @DexIgnore
    public void t0(int i2, D38 d38) throws IOException {
        this.B.A(i2, d38);
    }

    @DexIgnore
    public void u0(int i2, D38 d38) {
        try {
            this.i.execute(new Ai("OkHttp %s stream %d", new Object[]{this.e, Integer.valueOf(i2)}, i2, d38));
        } catch (RejectedExecutionException e2) {
        }
    }

    @DexIgnore
    public void v0(int i2, long j2) {
        try {
            this.i.execute(new Bi("OkHttp Window Update %s stream %d", new Object[]{this.e, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException e2) {
        }
    }
}
