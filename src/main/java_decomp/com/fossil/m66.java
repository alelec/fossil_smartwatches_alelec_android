package com.fossil;

import com.fossil.Jn5;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M66 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<N66> c;
    @DexIgnore
    public List<? extends Jn5.Ai> d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public M66(String str, String str2, ArrayList<N66> arrayList, List<? extends Jn5.Ai> list, boolean z) {
        Wg6.c(str, "mPresetId");
        Wg6.c(str2, "mPresetName");
        Wg6.c(arrayList, "mMicroApps");
        Wg6.c(list, "mPermissionFeatures");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = list;
        this.e = z;
    }

    @DexIgnore
    public final ArrayList<N66> a() {
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.Jn5$Ai>, java.util.List<com.fossil.Jn5$Ai> */
    public final List<Jn5.Ai> b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.e;
    }
}
