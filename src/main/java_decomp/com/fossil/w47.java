package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.V47;
import com.fossil.Ve0;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.ColorPanelView;
import com.portfolio.platform.view.ColorPickerView;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W47 extends Kq0 implements ColorPickerView.c, TextWatcher {
    @DexIgnore
    public static /* final */ int[] A; // = {-769226, -1499549, -54125, -6543440, -10011977, -12627531, -14575885, -16537100, -16728876, -16738680, -11751600, -7617718, -3285959, -5317, -16121, -26624, -8825528, -10453621, -6381922};
    @DexIgnore
    public X47 b;
    @DexIgnore
    public FrameLayout c;
    @DexIgnore
    public int[] d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public V47 j;
    @DexIgnore
    public LinearLayout k;
    @DexIgnore
    public SeekBar l;
    @DexIgnore
    public TextView m;
    @DexIgnore
    public ColorPickerView s;
    @DexIgnore
    public ColorPanelView t;
    @DexIgnore
    public EditText u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public int y;
    @DexIgnore
    public /* final */ View.OnTouchListener z; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements SeekBar.OnSeekBarChangeListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            V47 v47;
            W47.this.m.setText(String.format(Locale.ENGLISH, "%d%%", Integer.valueOf((int) ((((double) i) * 100.0d) / 255.0d))));
            int i2 = 255 - i;
            int i3 = 0;
            while (true) {
                v47 = W47.this.j;
                int[] iArr = v47.c;
                if (i3 >= iArr.length) {
                    break;
                }
                int i4 = iArr[i3];
                W47.this.j.c[i3] = Color.argb(i2, Color.red(i4), Color.green(i4), Color.blue(i4));
                i3++;
            }
            v47.notifyDataSetChanged();
            for (int i5 = 0; i5 < W47.this.k.getChildCount(); i5++) {
                FrameLayout frameLayout = (FrameLayout) W47.this.k.getChildAt(i5);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362166);
                ImageView imageView = (ImageView) frameLayout.findViewById(2131362163);
                if (frameLayout.getTag() == null) {
                    frameLayout.setTag(Integer.valueOf(colorPanelView.getBorderColor()));
                }
                int color = colorPanelView.getColor();
                int argb = Color.argb(i2, Color.red(color), Color.green(color), Color.blue(color));
                if (i2 <= 165) {
                    colorPanelView.setBorderColor(argb | -16777216);
                } else {
                    colorPanelView.setBorderColor(((Integer) frameLayout.getTag()).intValue());
                }
                if (colorPanelView.getTag() != null && ((Boolean) colorPanelView.getTag()).booleanValue()) {
                    if (i2 <= 165) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else if (Pl0.b(argb) >= 0.65d) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else {
                        imageView.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
                    }
                }
                colorPanelView.setColor(argb);
            }
            W47.this.e = Color.argb(i2, Color.red(W47.this.e), Color.green(W47.this.e), Color.blue(W47.this.e));
        }

        @DexIgnore
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @DexIgnore
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnTouchListener {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            EditText editText = W47.this.u;
            if (view == editText || !editText.hasFocus()) {
                return false;
            }
            W47.this.u.clearFocus();
            ((InputMethodManager) W47.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(W47.this.u.getWindowToken(), 0);
            W47.this.u.clearFocus();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements DialogInterface.OnClickListener {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            W47 w47 = W47.this;
            w47.E6(w47.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements View.OnClickListener {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void onClick(View view) {
            W47.this.c.removeAllViews();
            W47 w47 = W47.this;
            int i = w47.f;
            if (i == 0) {
                w47.f = 1;
                ((Button) view).setText(w47.y != 0 ? W47.this.y : 2131887388);
                W47 w472 = W47.this;
                w472.c.addView(w472.A6());
            } else if (i == 1) {
                w47.f = 0;
                ((Button) view).setText(w47.w != 0 ? W47.this.w : 2131887515);
                W47 w473 = W47.this;
                w473.c.addView(w473.z6());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements View.OnClickListener {
        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public void onClick(View view) {
            int color = W47.this.t.getColor();
            W47 w47 = W47.this;
            int i = w47.e;
            if (color == i) {
                w47.E6(i);
                W47.this.dismiss();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements View.OnFocusChangeListener {
        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            if (z) {
                ((InputMethodManager) W47.this.getActivity().getSystemService("input_method")).showSoftInput(W47.this.u, 1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements V47.Ai {
        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        @Override // com.fossil.V47.Ai
        public void a(int i) {
            W47 w47 = W47.this;
            int i2 = w47.e;
            if (i2 == i) {
                w47.E6(i2);
                W47.this.dismiss();
                return;
            }
            w47.e = i;
            if (w47.h) {
                w47.y6(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public Hi(W47 w47, ColorPanelView colorPanelView, int i) {
            this.b = colorPanelView;
            this.c = i;
        }

        @DexIgnore
        public void run() {
            this.b.setColor(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView b;

        @DexIgnore
        public Ii(ColorPanelView colorPanelView) {
            this.b = colorPanelView;
        }

        @DexIgnore
        public void onClick(View view) {
            if (!(view.getTag() instanceof Boolean) || !((Boolean) view.getTag()).booleanValue()) {
                W47.this.e = this.b.getColor();
                W47.this.j.a();
                for (int i = 0; i < W47.this.k.getChildCount(); i++) {
                    FrameLayout frameLayout = (FrameLayout) W47.this.k.getChildAt(i);
                    ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362166);
                    ImageView imageView = (ImageView) frameLayout.findViewById(2131362163);
                    imageView.setImageResource(colorPanelView == view ? 2131230942 : 0);
                    if ((colorPanelView != view || Pl0.b(colorPanelView.getColor()) < 0.65d) && Color.alpha(colorPanelView.getColor()) > 165) {
                        imageView.setColorFilter((ColorFilter) null);
                    } else {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    }
                    colorPanelView.setTag(Boolean.valueOf(colorPanelView == view));
                }
                return;
            }
            W47 w47 = W47.this;
            w47.E6(w47.e);
            W47.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView b;

        @DexIgnore
        public Ji(W47 w47, ColorPanelView colorPanelView) {
            this.b = colorPanelView;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            this.b.d();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki {
        @DexIgnore
        public int a; // = 2131887505;
        @DexIgnore
        public int b; // = 2131887515;
        @DexIgnore
        public int c; // = 2131887388;
        @DexIgnore
        public int d; // = 2131887566;
        @DexIgnore
        public int e; // = 1;
        @DexIgnore
        public int[] f; // = W47.A;
        @DexIgnore
        public int g; // = -16777216;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public boolean j; // = true;
        @DexIgnore
        public boolean k; // = true;
        @DexIgnore
        public boolean l; // = true;
        @DexIgnore
        public int m; // = 1;

        @DexIgnore
        public W47 a() {
            W47 w47 = new W47();
            Bundle bundle = new Bundle();
            bundle.putInt("id", this.h);
            bundle.putInt("dialogType", this.e);
            bundle.putInt(BaseFeatureModel.COLUMN_COLOR, this.g);
            bundle.putIntArray("presets", this.f);
            bundle.putBoolean("alpha", this.i);
            bundle.putBoolean("allowCustom", this.k);
            bundle.putBoolean("allowPresets", this.j);
            bundle.putInt("dialogTitle", this.a);
            bundle.putBoolean("showColorShades", this.l);
            bundle.putInt("colorShape", this.m);
            bundle.putInt("presetsButtonText", this.b);
            bundle.putInt("customButtonText", this.c);
            bundle.putInt("selectedButtonText", this.d);
            w47.setArguments(bundle);
            return w47;
        }

        @DexIgnore
        public Ki b(boolean z) {
            this.j = z;
            return this;
        }

        @DexIgnore
        public Ki c(int i2) {
            this.g = i2;
            return this;
        }

        @DexIgnore
        public Ki d(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        public Ki e(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        public Ki f(boolean z) {
            this.i = z;
            return this;
        }
    }

    @DexIgnore
    public static Ki D6() {
        return new Ki();
    }

    @DexIgnore
    public View A6() {
        View inflate = View.inflate(getActivity(), 2131558458, null);
        this.k = (LinearLayout) inflate.findViewById(2131363114);
        this.l = (SeekBar) inflate.findViewById(2131363235);
        this.m = (TextView) inflate.findViewById(2131363236);
        GridView gridView = (GridView) inflate.findViewById(2131362569);
        C6();
        if (this.h) {
            y6(this.e);
        } else {
            this.k.setVisibility(8);
            inflate.findViewById(2131363113).setVisibility(8);
        }
        V47 v47 = new V47(new Gi(), this.d, getSelectedItemPosition(), this.i);
        this.j = v47;
        gridView.setAdapter((ListAdapter) v47);
        if (this.v) {
            J6();
        } else {
            inflate.findViewById(2131363234).setVisibility(8);
            inflate.findViewById(2131363237).setVisibility(8);
        }
        return inflate;
    }

    @DexIgnore
    public final int[] B6(int i2) {
        return new int[]{K6(i2, 0.9d), K6(i2, 0.7d), K6(i2, 0.5d), K6(i2, 0.333d), K6(i2, 0.166d), K6(i2, -0.125d), K6(i2, -0.25d), K6(i2, -0.375d), K6(i2, -0.5d), K6(i2, -0.675d), K6(i2, -0.7d), K6(i2, -0.775d)};
    }

    @DexIgnore
    public final void C6() {
        int alpha = Color.alpha(this.e);
        int[] intArray = getArguments().getIntArray("presets");
        this.d = intArray;
        if (intArray == null) {
            this.d = A;
        }
        boolean z2 = this.d == A;
        int[] iArr = this.d;
        this.d = Arrays.copyOf(iArr, iArr.length);
        if (alpha != 255) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.d;
                if (i2 >= iArr2.length) {
                    break;
                }
                int i3 = iArr2[i2];
                this.d[i2] = Color.argb(alpha, Color.red(i3), Color.green(i3), Color.blue(i3));
                i2++;
            }
        }
        this.d = L6(this.d, this.e);
        int i4 = getArguments().getInt(BaseFeatureModel.COLUMN_COLOR);
        if (i4 != this.e) {
            this.d = L6(this.d, i4);
        }
        if (z2) {
            int[] iArr3 = this.d;
            if (iArr3.length == 19) {
                this.d = H6(iArr3, Color.argb(alpha, 0, 0, 0));
            }
        }
    }

    @DexIgnore
    public final void E6(int i2) {
        if (this.b != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.b.C3(this.g, i2);
            return;
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof X47) {
            ((X47) activity).C3(this.g, i2);
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager instanceof X47) {
                ((X47) fragmentManager).C3(this.g, i2);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ColorPickerDialog", "onColorSelected fm=" + fragmentManager);
            return;
        }
        throw new IllegalStateException("The activity must implement ColorPickerDialogListener");
    }

    @DexIgnore
    public final void F6() {
        if (this.b != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.b.q3(this.g);
            return;
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof X47) {
            ((X47) activity).q3(this.g);
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager instanceof X47) {
            ((X47) fragmentManager).C3(this.g, this.e);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ColorPickerDialog", "fm=" + fragmentManager);
    }

    @DexIgnore
    public final int G6(String str) throws NumberFormatException {
        int i2;
        int i3;
        int i4;
        int i5;
        int parseInt;
        int parseInt2;
        if (str.startsWith("#")) {
            str = str.substring(1);
        }
        if (str.length() == 0) {
            i2 = 0;
        } else if (str.length() <= 2) {
            i2 = Integer.parseInt(str, 16);
        } else {
            if (str.length() == 3) {
                parseInt = Integer.parseInt(str.substring(0, 1), 16);
                parseInt2 = Integer.parseInt(str.substring(1, 2), 16);
                i2 = Integer.parseInt(str.substring(2, 3), 16);
            } else if (str.length() == 4) {
                parseInt2 = Integer.parseInt(str.substring(0, 2), 16);
                i2 = Integer.parseInt(str.substring(2, 4), 16);
                i4 = 0;
                i5 = 255;
                i3 = parseInt2;
                return Color.argb(i5, i4, i3, i2);
            } else if (str.length() == 5) {
                parseInt = Integer.parseInt(str.substring(0, 1), 16);
                parseInt2 = Integer.parseInt(str.substring(1, 3), 16);
                i2 = Integer.parseInt(str.substring(3, 5), 16);
            } else if (str.length() == 6) {
                parseInt = Integer.parseInt(str.substring(0, 2), 16);
                parseInt2 = Integer.parseInt(str.substring(2, 4), 16);
                i2 = Integer.parseInt(str.substring(4, 6), 16);
            } else {
                if (str.length() == 7) {
                    i5 = Integer.parseInt(str.substring(0, 1), 16);
                    i4 = Integer.parseInt(str.substring(1, 3), 16);
                    int parseInt3 = Integer.parseInt(str.substring(3, 5), 16);
                    i2 = Integer.parseInt(str.substring(5, 7), 16);
                    i3 = parseInt3;
                } else if (str.length() == 8) {
                    i5 = Integer.parseInt(str.substring(0, 2), 16);
                    i4 = Integer.parseInt(str.substring(2, 4), 16);
                    int parseInt4 = Integer.parseInt(str.substring(4, 6), 16);
                    i2 = Integer.parseInt(str.substring(6, 8), 16);
                    i3 = parseInt4;
                } else {
                    i2 = -1;
                    i3 = -1;
                    i4 = -1;
                    i5 = -1;
                }
                return Color.argb(i5, i4, i3, i2);
            }
            i4 = parseInt;
            i5 = 255;
            i3 = parseInt2;
            return Color.argb(i5, i4, i3, i2);
        }
        i4 = 0;
        parseInt2 = 0;
        i5 = 255;
        i3 = parseInt2;
        return Color.argb(i5, i4, i3, i2);
    }

    @DexIgnore
    public final int[] H6(int[] iArr, int i2) {
        boolean z2;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z2 = false;
                break;
            } else if (iArr[i3] == i2) {
                z2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (z2) {
            return iArr;
        }
        int length2 = iArr.length + 1;
        int[] iArr2 = new int[length2];
        int i4 = length2 - 1;
        iArr2[i4] = i2;
        System.arraycopy(iArr, 0, iArr2, 0, i4);
        return iArr2;
    }

    @DexIgnore
    public final void I6(int i2) {
        if (this.v) {
            this.u.setText(String.format("%08X", Integer.valueOf(i2)));
            return;
        }
        this.u.setText(String.format("%06X", Integer.valueOf(16777215 & i2)));
    }

    @DexIgnore
    public final void J6() {
        int alpha = 255 - Color.alpha(this.e);
        this.l.setMax(255);
        this.l.setProgress(alpha);
        this.m.setText(String.format(Locale.ENGLISH, "%d%%", Integer.valueOf((int) ((((double) alpha) * 100.0d) / 255.0d))));
        this.l.setOnSeekBarChangeListener(new Ai());
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.ColorPickerView.c
    public void K5(int i2) {
        this.e = i2;
        ColorPanelView colorPanelView = this.t;
        if (colorPanelView != null) {
            colorPanelView.setColor(i2);
        }
        if (!this.x && this.u != null) {
            I6(i2);
            if (this.u.hasFocus()) {
                ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.u.getWindowToken(), 0);
                this.u.clearFocus();
            }
        }
        this.x = false;
    }

    @DexIgnore
    public final int K6(int i2, double d2) {
        double d3 = 0.0d;
        long parseLong = Long.parseLong(String.format("#%06X", Integer.valueOf(16777215 & i2)).substring(1), 16);
        int i3 = (d2 > 0.0d ? 1 : (d2 == 0.0d ? 0 : -1));
        if (i3 >= 0) {
            d3 = 255.0d;
        }
        if (i3 < 0) {
            d2 *= -1.0d;
        }
        long j2 = parseLong >> 16;
        long j3 = (parseLong >> 8) & 255;
        long j4 = parseLong & 255;
        return Color.argb(Color.alpha(i2), (int) (j2 + Math.round((d3 - ((double) j2)) * d2)), (int) (j3 + Math.round((d3 - ((double) j3)) * d2)), (int) (Math.round((d3 - ((double) j4)) * d2) + j4));
    }

    @DexIgnore
    public final int[] L6(int[] iArr, int i2) {
        boolean z2;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z2 = false;
                break;
            } else if (iArr[i3] == i2) {
                z2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (z2) {
            return iArr;
        }
        int length2 = iArr.length + 1;
        int[] iArr2 = new int[length2];
        iArr2[0] = i2;
        System.arraycopy(iArr, 0, iArr2, 1, length2 - 1);
        return iArr2;
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        int G6;
        if (this.u.isFocused() && (G6 = G6(editable.toString())) != this.s.getColor()) {
            this.x = true;
            this.s.n(G6, true);
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public final int getSelectedItemPosition() {
        int i2 = 0;
        while (true) {
            int[] iArr = this.d;
            if (i2 >= iArr.length) {
                return -1;
            }
            if (iArr[i2] == this.e) {
                return i2;
            }
            i2++;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && (parentFragment instanceof X47)) {
            this.b = (X47) parentFragment;
        }
        if (this.b == null && (context instanceof X47)) {
            this.b = (X47) context;
        }
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        int i2;
        this.g = getArguments().getInt("id");
        this.v = getArguments().getBoolean("alpha");
        this.h = getArguments().getBoolean("showColorShades");
        this.i = getArguments().getInt("colorShape");
        if (bundle == null) {
            this.e = getArguments().getInt(BaseFeatureModel.COLUMN_COLOR);
            this.f = getArguments().getInt("dialogType");
        } else {
            this.e = bundle.getInt(BaseFeatureModel.COLUMN_COLOR);
            this.f = bundle.getInt("dialogType");
        }
        FrameLayout frameLayout = new FrameLayout(requireActivity());
        this.c = frameLayout;
        int i3 = this.f;
        if (i3 == 0) {
            frameLayout.addView(z6());
        } else if (i3 == 1) {
            frameLayout.addView(A6());
        }
        int i4 = getArguments().getInt("selectedButtonText");
        if (i4 == 0) {
            i4 = 2131887566;
        }
        Ve0.Ai ai = new Ve0.Ai(requireActivity());
        ai.p(this.c);
        ai.k(i4, new Ci());
        int i5 = getArguments().getInt("dialogTitle");
        if (i5 != 0) {
            ai.n(i5);
        }
        this.w = getArguments().getInt("presetsButtonText");
        this.y = getArguments().getInt("customButtonText");
        if (this.f == 0 && getArguments().getBoolean("allowPresets")) {
            i2 = this.w;
            if (i2 == 0) {
                i2 = 2131887388;
            }
        } else if (this.f != 1 || !getArguments().getBoolean("allowCustom")) {
            i2 = 0;
        } else {
            i2 = this.y;
            if (i2 == 0) {
                i2 = 2131887515;
            }
        }
        if (i2 != 0) {
            ai.i(i2, null);
        }
        return ai.a();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onDetach() {
        super.onDetach();
        this.b = null;
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        F6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(BaseFeatureModel.COLUMN_COLOR, this.e);
        bundle.putInt("dialogType", this.f);
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onStart() {
        super.onStart();
        Ve0 ve0 = (Ve0) getDialog();
        ve0.getWindow().clearFlags(131080);
        ve0.getWindow().setSoftInputMode(4);
        Button e2 = ve0.e(-3);
        if (e2 != null) {
            e2.setOnClickListener(new Di());
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void y6(int i2) {
        int[] B6 = B6(i2);
        if (this.k.getChildCount() != 0) {
            for (int i3 = 0; i3 < this.k.getChildCount(); i3++) {
                FrameLayout frameLayout = (FrameLayout) this.k.getChildAt(i3);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362166);
                colorPanelView.setColor(B6[i3]);
                colorPanelView.setTag(Boolean.FALSE);
                ((ImageView) frameLayout.findViewById(2131362163)).setImageDrawable(null);
            }
            return;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(2131165314);
        for (int i4 : B6) {
            View inflate = View.inflate(getActivity(), this.i == 0 ? 2131558456 : 2131558455, null);
            ColorPanelView colorPanelView2 = (ColorPanelView) inflate.findViewById(2131362166);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) colorPanelView2.getLayoutParams();
            marginLayoutParams.rightMargin = dimensionPixelSize;
            marginLayoutParams.leftMargin = dimensionPixelSize;
            colorPanelView2.setLayoutParams(marginLayoutParams);
            colorPanelView2.setColor(i4);
            this.k.addView(inflate);
            colorPanelView2.post(new Hi(this, colorPanelView2, i4));
            colorPanelView2.setOnClickListener(new Ii(colorPanelView2));
            colorPanelView2.setOnLongClickListener(new Ji(this, colorPanelView2));
        }
    }

    @DexIgnore
    public View z6() {
        View inflate = View.inflate(getActivity(), 2131558457, null);
        this.s = (ColorPickerView) inflate.findViewById(2131362167);
        this.t = (ColorPanelView) inflate.findViewById(2131362164);
        this.u = (EditText) inflate.findViewById(2131362168);
        try {
            TypedValue typedValue = new TypedValue();
            getActivity().obtainStyledAttributes(typedValue.data, new int[]{16842806}).recycle();
        } catch (Exception e2) {
        }
        this.s.setAlphaSliderVisible(this.v);
        this.s.n(this.e, true);
        this.t.setColor(this.e);
        I6(this.e);
        if (!this.v) {
            this.u.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
        this.t.setOnClickListener(new Ei());
        inflate.setOnTouchListener(this.z);
        this.s.setOnColorChangedListener(this);
        this.u.addTextChangedListener(this);
        this.u.setOnFocusChangeListener(new Fi());
        return inflate;
    }
}
