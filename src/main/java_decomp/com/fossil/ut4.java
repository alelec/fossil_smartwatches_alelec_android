package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ut4 implements Factory<Tt4> {
    @DexIgnore
    public /* final */ Provider<Rt4> a;
    @DexIgnore
    public /* final */ Provider<ChallengeRemoteDataSource> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Ut4(Provider<Rt4> provider, Provider<ChallengeRemoteDataSource> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Ut4 a(Provider<Rt4> provider, Provider<ChallengeRemoteDataSource> provider2, Provider<An4> provider3) {
        return new Ut4(provider, provider2, provider3);
    }

    @DexIgnore
    public static Tt4 c(Rt4 rt4, ChallengeRemoteDataSource challengeRemoteDataSource, An4 an4) {
        return new Tt4(rt4, challengeRemoteDataSource, an4);
    }

    @DexIgnore
    public Tt4 b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
