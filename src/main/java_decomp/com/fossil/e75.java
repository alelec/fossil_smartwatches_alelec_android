package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E75 extends D75 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        ViewDataBinding.d dVar = new ViewDataBinding.d(8);
        z = dVar;
        dVar.a(1, new String[]{"item_heartrate_workout_day", "item_heartrate_workout_day"}, new int[]{2, 3}, new int[]{2131558697, 2131558697});
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131363203, 4);
        A.put(2131362834, 5);
        A.put(2131362355, 6);
        A.put(2131362655, 7);
    }
    */

    @DexIgnore
    public E75(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public E75(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 2, (FlexibleTextView) objArr[6], (Bf5) objArr[2], (Bf5) objArr[3], (ImageView) objArr[7], (LinearLayout) objArr[5], (LinearLayout) objArr[1], (TodayHeartRateChart) objArr[4]);
        this.y = -1;
        this.v.setTag(null);
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.x = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
        ViewDataBinding.i(this.r);
        ViewDataBinding.i(this.s);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        if (r6.s.o() != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.r.o() != false) goto L_?;
     */
    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean o() {
        /*
            r6 = this;
            r0 = 1
            monitor-enter(r6)
            long r2 = r6.y     // Catch:{ all -> 0x001f }
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 == 0) goto L_0x000c
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
        L_0x000b:
            return r0
        L_0x000c:
            monitor-exit(r6)     // Catch:{ all -> 0x001f }
            com.fossil.Bf5 r1 = r6.r
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            com.fossil.Bf5 r1 = r6.s
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x000b
            r0 = 0
            goto L_0x000b
        L_0x001f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.E75.o():boolean");
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 4;
        }
        this.r.q();
        this.s.q();
        w();
    }
}
