package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lp {
    @DexIgnore
    public static /* final */ Kp B; // = new Kp(null);
    @DexIgnore
    public boolean A;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public Fs b;
    @DexIgnore
    public /* final */ D5 c;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Lp, Cd6>> d;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Lp, Cd6>> e;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Lp, Cd6>> f;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Lp, Cd6>> g;
    @DexIgnore
    public CopyOnWriteArrayList<Coroutine<Lp, Float, Cd6>> h;
    @DexIgnore
    public /* final */ ArrayList<Ow> i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public A9 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public Lp n;
    @DexIgnore
    public /* final */ Handler o;
    @DexIgnore
    public /* final */ long p;
    @DexIgnore
    public /* final */ Md0 q;
    @DexIgnore
    public /* final */ El r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public Nr v;
    @DexIgnore
    public /* final */ K5 w;
    @DexIgnore
    public /* final */ I60 x;
    @DexIgnore
    public /* final */ Yp y;
    @DexIgnore
    public String z;

    @DexIgnore
    public Lp(K5 k5, I60 i60, Yp yp, String str, boolean z2) {
        this.w = k5;
        this.x = i60;
        this.y = yp;
        this.z = str;
        this.A = z2;
        this.a = Ey1.a(yp);
        this.c = new Tj(this);
        this.d = new CopyOnWriteArrayList<>();
        this.e = new CopyOnWriteArrayList<>();
        this.f = new CopyOnWriteArrayList<>();
        this.g = new CopyOnWriteArrayList<>();
        this.h = new CopyOnWriteArrayList<>();
        this.i = Hm7.c(Ow.b);
        Xo xo = Xo.b;
        this.k = A9.c;
        this.l = System.currentTimeMillis();
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.o = new Handler(myLooper);
            this.p = 30000;
            this.q = new Md0(new Rl(this));
            this.r = new El(this);
            if (this.A) {
                D90.i.d(new A90(Ey1.a(this.y), V80.b, this.w.x, Ey1.a(this.y), this.z, true, null, null, null, null, 960));
            }
            this.v = new Nr(this.y, Zq.c, null, null, 12);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Lp(K5 k5, I60 i60, Yp yp, String str, boolean z2, int i2) {
        this(k5, i60, yp, (i2 & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, (i2 & 16) != 0 ? true : z2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.Lp */
    /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.mapped.Coroutine<com.fossil.Lp, java.lang.Float, com.mapped.Cd6>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void h(Lp lp, Lp lp2, Hg6 hg6, Hg6 hg62, Coroutine coroutine, Hg6 hg63, Hg6 hg64, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 8) != 0) {
                coroutine = Th.b;
            }
            if ((i2 & 16) != 0) {
                hg63 = Gi.b;
            }
            if ((i2 & 32) != 0) {
                hg64 = Ui.b;
            }
            if (!lp.t) {
                lp.n = lp2;
                if (lp2 != 0) {
                    lp2.v(hg6);
                    lp2.s(new Hj(lp, hg64, hg62));
                    if (!lp2.t) {
                        lp2.h.add(coroutine);
                    }
                    lp2.a(hg63);
                    lp2.F();
                    return;
                }
                return;
            }
            lp.l(lp.v);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeSubPhase");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v30, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.mapped.Hg6<com.fossil.Fs, com.mapped.Cd6>> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v31, resolved type: java.util.concurrent.CopyOnWriteArrayList<com.mapped.Coroutine<com.fossil.Fs, java.lang.Float, com.mapped.Cd6>> */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void i(Lp lp, Fs fs, Hg6 hg6, Hg6 hg62, Coroutine coroutine, Hg6 hg63, Hg6 hg64, int i2, Object obj) {
        boolean z2;
        if (obj == null) {
            if ((i2 & 8) != 0) {
                coroutine = Rf.b;
            }
            if ((i2 & 16) != 0) {
                hg63 = Fg.b;
            }
            if ((i2 & 32) != 0) {
                hg64 = Tg.b;
            }
            if (lp.t || ((z2 = lp.u) && (!z2 || !lp.r(fs)))) {
                lp.l(lp.v);
                return;
            }
            fs.w = fs.w && lp.A;
            String str = lp.z;
            fs.d = str;
            A90 a90 = fs.f;
            if (a90 != null) {
                a90.i = str;
            }
            String a2 = Ey1.a(lp.y);
            fs.c = a2;
            A90 a902 = fs.f;
            if (a902 != null) {
                a902.h = a2;
            }
            lp.b = fs;
            if (fs != null) {
                fs.o(hg6);
                fs.c(new Gh(lp, hg64, hg62));
                if (!fs.t) {
                    fs.o.add(coroutine);
                }
                if (!fs.t) {
                    fs.n.add(hg63);
                } else {
                    hg63.invoke(fs);
                }
                if (!fs.t) {
                    Ky1 ky1 = Ky1.DEBUG;
                    fs.z().toString(2);
                    long currentTimeMillis = System.currentTimeMillis();
                    A90 a903 = fs.f;
                    if (a903 != null) {
                        a903.b = currentTimeMillis;
                    }
                    A90 a904 = fs.f;
                    if (a904 != null) {
                        a904.n = Gy1.c(a904.n, G80.k(G80.k(G80.k(fs.z(), Jd0.C1, Double.valueOf(Hy1.f(fs.e))), Jd0.t1, Double.valueOf(Hy1.f(currentTimeMillis))), Jd0.v1, Long.valueOf(fs.x())));
                    }
                    fs.n(fs.p);
                    K5 k5 = fs.y;
                    Bs bs = fs.q;
                    if (!k5.i.contains(bs)) {
                        k5.i.add(bs);
                    }
                    K5 k52 = fs.y;
                    Ds ds = fs.r;
                    if (!k52.j.contains(ds)) {
                        k52.j.add(ds);
                    }
                    fs.q();
                    return;
                }
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: executeRequest");
    }

    @DexIgnore
    public static /* synthetic */ void j(Lp lp, Hs hs, Hs hs2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                hs2 = hs;
            }
            lp.m(hs, hs2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryExecuteRequest");
    }

    @DexIgnore
    public final long A() {
        return this.l;
    }

    @DexIgnore
    public abstract void B();

    @DexIgnore
    public JSONObject C() {
        return new JSONObject();
    }

    @DexIgnore
    public void D() {
    }

    @DexIgnore
    public JSONObject E() {
        return new JSONObject();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        r0 = com.fossil.Ky1.DEBUG;
        z().toString();
        r0 = com.fossil.Ky1.DEBUG;
        r0 = r6.p;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        if (r0 <= 0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        r6.o.postDelayed(r6.q, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r0 = r6.x.b();
        r1 = r6.r;
        r0.a.b(r6);
        r0.c.put(r6, r1);
        r0.g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void F() {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.fossil.Id0 r0 = com.fossil.Id0.i
            java.lang.Object r0 = r0.m()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            boolean r0 = com.mapped.Wg6.a(r0, r2)
            if (r0 == 0) goto L_0x0058
            boolean r0 = r6.s
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r0)
            monitor-enter(r1)
            boolean r0 = r6.s     // Catch:{ all -> 0x0055 }
            if (r0 == 0) goto L_0x001f
            monitor-exit(r1)
        L_0x001e:
            return
        L_0x001f:
            r0 = 1
            r6.s = r0
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            monitor-exit(r1)
            com.fossil.Ky1 r0 = com.fossil.Ky1.DEBUG
            java.util.ArrayList r0 = r6.z()
            r0.toString()
            com.fossil.Ky1 r0 = com.fossil.Ky1.DEBUG
            long r0 = r6.p
            r2 = 0
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x003f
            android.os.Handler r2 = r6.o
            com.fossil.Md0 r3 = r6.q
            r2.postDelayed(r3, r0)
        L_0x003f:
            com.fossil.I60 r0 = r6.x
            com.fossil.Qe r0 = r0.b()
            com.fossil.El r1 = r6.r
            com.fossil.Nd0<com.fossil.Lp> r2 = r0.a
            r2.b(r6)
            java.util.Hashtable<com.fossil.Lp, com.fossil.El> r2 = r0.c
            r2.put(r6, r1)
            r0.g()
            goto L_0x001e
        L_0x0055:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0058:
            com.fossil.Nr r0 = r6.v
            com.fossil.Zq r2 = com.fossil.Zq.b
            r5 = 13
            r3 = r1
            r4 = r1
            com.fossil.Nr r0 = com.fossil.Nr.a(r0, r1, r2, r3, r4, r5)
            r6.l(r0)
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lp.F():void");
    }

    @DexIgnore
    public final Lp a(Hg6<? super Lp, Cd6> hg6) {
        if (!this.t) {
            this.g.add(hg6);
        } else {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public Fs b(Hs hs) {
        return null;
    }

    @DexIgnore
    public final void c() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            this.w.k.remove(this.c);
            Qe b2 = this.x.b();
            b2.e.b(this);
            b2.a.remove(this);
            b2.b.remove(this);
            b2.c.remove(this);
            JSONObject c2 = Gy1.c(G80.k(G80.k(new JSONObject(), Jd0.k, B.a(this.v)), Jd0.O0, Ey1.a(this.v.c)), E());
            if (this.v.d.d != Lw.b) {
                G80.k(c2, Jd0.P0, G80.k(G80.k(new JSONObject(), Jd0.q, this.v.d.b), Jd0.g2, this.v.d.c));
            }
            A90 a90 = new A90(Ey1.a(this.y), V80.d, this.w.x, Ey1.a(this.y), this.z, this.v.c == Zq.b, null, null, null, c2, 448);
            if (this.A) {
                D90.i.d(a90);
            }
            if (this instanceof Mi) {
                Y80.i.d(a90);
            } else if ((this instanceof Cj) || (this instanceof Pi)) {
                X80.i.d(a90);
            }
            Nr nr = this.v;
            if (nr.c == Zq.b) {
                Ky1 ky1 = Ky1.DEBUG;
                Ox1.toJSONString$default(nr, 0, 1, null);
                E().toString(2);
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                Ky1 ky12 = Ky1.ERROR;
                Ox1.toJSONString$default(nr, 0, 1, null);
                Iterator<T> it2 = this.f.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.g.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public final void d(float f2) {
        Iterator<T> it = this.h.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public void e(F5 f5) {
        if (Df.a[f5.ordinal()] == 1) {
            k(Zq.k);
        }
    }

    @DexIgnore
    public void f(A9 a9) {
        this.k = a9;
    }

    @DexIgnore
    public void k(Zq zq) {
        if (!this.t && !this.u) {
            Ky1 ky1 = Ky1.DEBUG;
            Ey1.a(zq);
            this.o.removeCallbacksAndMessages(null);
            this.u = true;
            this.v = Nr.a(this.v, null, zq, null, null, 13);
            Fs fs = this.b;
            if (fs == null || fs.t) {
                Lp lp = this.n;
                if (lp == null || lp.t) {
                    l(this.v);
                } else if (lp != null) {
                    lp.k(zq);
                }
            } else if (fs != null) {
                fs.l(Lw.t);
            }
        }
    }

    @DexIgnore
    public final void l(Nr nr) {
        this.v = Nr.a(nr, this.y, null, null, null, 14);
        c();
    }

    @DexIgnore
    public final void m(Hs hs, Hs hs2) {
        n(hs, new Dm(this, hs2));
    }

    @DexIgnore
    public final void n(Hs hs, Gg6<Cd6> gg6) {
        Fs b2 = b(hs);
        if (b2 == null) {
            l(Nr.a(this.v, null, Zq.i, null, null, 13));
        } else if (this.m < b2.z) {
            i(this, b2, Qm.b, new Zn(this, gg6), null, new Lo(this), Yo.b, 8, null);
        } else {
            l(this.v);
        }
    }

    @DexIgnore
    public final void o(Mw mw) {
        this.v = Nr.a(this.v, null, Zq.I.a(mw), mw, null, 9);
        c();
    }

    @DexIgnore
    public final void p(Md0 md0) {
        if (!md0.b) {
            md0.b = true;
            this.o.removeCallbacksAndMessages(md0);
        }
    }

    @DexIgnore
    public boolean q(Lp lp) {
        return u(lp) || this.y != lp.y;
    }

    @DexIgnore
    public boolean r(Fs fs) {
        return false;
    }

    @DexIgnore
    public final Lp s(Hg6<? super Lp, Cd6> hg6) {
        if (!this.t) {
            this.f.add(hg6);
        } else if (this.v.c != Zq.b) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public boolean t() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return Ey1.a(this.y) + '(' + this.z + ')';
    }

    @DexIgnore
    public final boolean u(Lp lp) {
        if (Wg6.a(this.n, lp)) {
            return true;
        }
        Lp lp2 = this.n;
        return lp2 != null && lp2.u(lp);
    }

    @DexIgnore
    public final Lp v(Hg6<? super Lp, Cd6> hg6) {
        if (!this.t) {
            this.e.add(hg6);
        } else if (this.v.c == Zq.b) {
            hg6.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final boolean w() {
        return t() && this.x.a().d().compareTo(Hd0.y.k()) >= 0;
    }

    @DexIgnore
    public Object x() {
        return Cd6.a;
    }

    @DexIgnore
    public A9 y() {
        return this.k;
    }

    @DexIgnore
    public ArrayList<Ow> z() {
        return this.i;
    }
}
