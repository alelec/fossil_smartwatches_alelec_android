package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Be2 implements Parcelable.Creator<Tc2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Tc2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        boolean z2 = false;
        int i = 0;
        Z52 z52 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i = Ad2.v(parcel, t);
            } else if (l == 2) {
                iBinder = Ad2.u(parcel, t);
            } else if (l == 3) {
                z52 = (Z52) Ad2.e(parcel, t, Z52.CREATOR);
            } else if (l == 4) {
                z2 = Ad2.m(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                z = Ad2.m(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Tc2(i, iBinder, z52, z2, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Tc2[] newArray(int i) {
        return new Tc2[i];
    }
}
