package com.fossil;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ a T; // = new a(null);
    @DexIgnore
    public /* final */ HeartRateSummaryRepository A;
    @DexIgnore
    public /* final */ WorkoutSessionRepository B;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase C;
    @DexIgnore
    public /* final */ hu4 D;
    @DexIgnore
    public /* final */ zt4 E;
    @DexIgnore
    public /* final */ tt4 F;
    @DexIgnore
    public /* final */ du4 G;
    @DexIgnore
    public /* final */ FileRepository H;
    @DexIgnore
    public /* final */ QuickResponseRepository I;
    @DexIgnore
    public /* final */ WatchFaceRepository J;
    @DexIgnore
    public /* final */ RingStyleRepository K;
    @DexIgnore
    public /* final */ vt4 L;
    @DexIgnore
    public /* final */ WorkoutSettingRepository M;
    @DexIgnore
    public /* final */ DianaAppSettingRepository N;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository O;
    @DexIgnore
    public /* final */ WatchAppDataRepository P;
    @DexIgnore
    public /* final */ uo5 Q;
    @DexIgnore
    public /* final */ k97 R;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;
    @DexIgnore
    public /* final */ on5 f;
    @DexIgnore
    public /* final */ HybridPresetRepository g;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ MicroAppSettingRepository j;
    @DexIgnore
    public /* final */ NotificationsRepository k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;
    @DexIgnore
    public /* final */ vn5 o;
    @DexIgnore
    public /* final */ uk5 p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ DianaPresetRepository r;
    @DexIgnore
    public /* final */ WatchAppRepository s;
    @DexIgnore
    public /* final */ ComplicationRepository t;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;
    @DexIgnore
    public /* final */ MicroAppRepository w;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository x;
    @DexIgnore
    public /* final */ FitnessDataRepository y;
    @DexIgnore
    public /* final */ HeartRateSampleRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return zu5.S;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4540a;
        @DexIgnore
        public /* final */ WeakReference<Activity> b;

        @DexIgnore
        public b(int i, WeakReference<Activity> weakReference) {
            this.f4540a = i;
            this.b = weakReference;
        }

        @DexIgnore
        public final int a() {
            return this.f4540a;
        }

        @DexIgnore
        public final WeakReference<Activity> b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4541a;

        @DexIgnore
        public c(int i) {
            this.f4541a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f4541a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", f = "DeleteLogoutUserUseCase.kt", l = {180, 181, 182, 186, 188, 195, 196, 197, 198, Action.Music.MUSIC_END_ACTION, 200, Action.Selfie.TAKE_BURST, 209, 210, 211, 212, 213, 214, 216, 219, 220, 224, 225, 226}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zu5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(zu5 zu5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a2  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00cb  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00f0  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x010c  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0144  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x0160  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x017c  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01da  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x01ff  */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x021b  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0237  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0253  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x026f  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x028a  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x02c5  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x02e0  */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0304  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x031f  */
        /* JADX WARNING: Removed duplicated region for block: B:97:0x033a  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x003e  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 1014
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zu5.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1", f = "DeleteLogoutUserUseCase.kt", l = {148}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zu5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1$1", f = "DeleteLogoutUserUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.I.removeAll();
                    String c = um5.c(PortfolioApp.h0.c(), 2131886131);
                    pq7.b(c, "LanguageHelper.getString\u2026an_Text__CanICallYouBack)");
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(c);
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886132);
                    pq7.b(c2, "LanguageHelper.getString\u2026_MessageOn_Text__OnMyWay)");
                    QuickResponseMessage quickResponseMessage2 = new QuickResponseMessage(c2);
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886133);
                    pq7.b(c3, "LanguageHelper.getString\u2026y_Text__SorryCantTalkNow)");
                    this.this$0.this$0.I.insertQRs(hm7.h(quickResponseMessage, quickResponseMessage2, new QuickResponseMessage(c3)));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(zu5 zu5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.K1(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {89, 98, 100, 119, 134}, m = "run")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zu5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(zu5 zu5, qn7 qn7) {
            super(qn7);
            this.this$0 = zu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = zu5.class.getSimpleName();
        pq7.b(simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        S = simpleName;
    }
    */

    @DexIgnore
    public zu5(UserRepository userRepository, AlarmsRepository alarmsRepository, on5 on5, HybridPresetRepository hybridPresetRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, MicroAppSettingRepository microAppSettingRepository, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, SleepSessionsRepository sleepSessionsRepository, GoalTrackingRepository goalTrackingRepository, vn5 vn5, uk5 uk5, SleepSummariesRepository sleepSummariesRepository, DianaPresetRepository dianaPresetRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, NotificationSettingsDatabase notificationSettingsDatabase, DNDSettingsDatabase dNDSettingsDatabase, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository, FitnessDataRepository fitnessDataRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, RemindersSettingsDatabase remindersSettingsDatabase, hu4 hu4, zt4 zt4, tt4 tt4, du4 du4, FileRepository fileRepository, QuickResponseRepository quickResponseRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, vt4 vt4, WorkoutSettingRepository workoutSettingRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository, WatchAppDataRepository watchAppDataRepository, uo5 uo5, k97 k97) {
        pq7.c(userRepository, "mUserRepository");
        pq7.c(alarmsRepository, "mAlarmRepository");
        pq7.c(on5, "mSharedPreferences");
        pq7.c(hybridPresetRepository, "mPresetRepository");
        pq7.c(activitiesRepository, "mActivitiesRepository");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(microAppSettingRepository, "mMicroAppSettingRepository");
        pq7.c(notificationsRepository, "mNotificationRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(vn5, "mLoginGoogleManager");
        pq7.c(uk5, "mGoogleFitHelper");
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(dianaPresetRepository, "mDianaPresetRepository");
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        pq7.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        pq7.c(microAppRepository, "mMicroAppRepository");
        pq7.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        pq7.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        pq7.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        pq7.c(hu4, "mSocialProfileRepository");
        pq7.c(zt4, "mSocialFriendRepository");
        pq7.c(tt4, "challengeRepository");
        pq7.c(du4, "notificationRepository");
        pq7.c(fileRepository, "mFileRepository");
        pq7.c(quickResponseRepository, "mQuickResponseRepository");
        pq7.c(watchFaceRepository, "mWatchFaceRepository");
        pq7.c(ringStyleRepository, "mRingStyleRepository");
        pq7.c(vt4, "fcmRepository");
        pq7.c(workoutSettingRepository, "mWorkoutSettingRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        pq7.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        pq7.c(watchAppDataRepository, "mWatchAppDataRepository");
        pq7.c(uo5, "dianaPresetRepository");
        pq7.c(k97, "photoWFBackgroundPhotoRepository");
        this.d = userRepository;
        this.e = alarmsRepository;
        this.f = on5;
        this.g = hybridPresetRepository;
        this.h = activitiesRepository;
        this.i = summariesRepository;
        this.j = microAppSettingRepository;
        this.k = notificationsRepository;
        this.l = deviceRepository;
        this.m = sleepSessionsRepository;
        this.n = goalTrackingRepository;
        this.o = vn5;
        this.p = uk5;
        this.q = sleepSummariesRepository;
        this.r = dianaPresetRepository;
        this.s = watchAppRepository;
        this.t = complicationRepository;
        this.u = notificationSettingsDatabase;
        this.v = dNDSettingsDatabase;
        this.w = microAppRepository;
        this.x = microAppLastSettingRepository;
        this.y = fitnessDataRepository;
        this.z = heartRateSampleRepository;
        this.A = heartRateSummaryRepository;
        this.B = workoutSessionRepository;
        this.C = remindersSettingsDatabase;
        this.D = hu4;
        this.E = zt4;
        this.F = tt4;
        this.G = du4;
        this.H = fileRepository;
        this.I = quickResponseRepository;
        this.J = watchFaceRepository;
        this.K = ringStyleRepository;
        this.L = vt4;
        this.M = workoutSettingRepository;
        this.N = dianaAppSettingRepository;
        this.O = dianaWatchFaceRepository;
        this.P = watchAppDataRepository;
        this.Q = uo5;
        this.R = k97;
    }

    @DexIgnore
    public final void b0(b bVar) {
        this.o.f(bVar.b());
        try {
            IButtonConnectivity b2 = PortfolioApp.h0.b();
            if (b2 != null) {
                b2.deviceUnlink(PortfolioApp.h0.c().J());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        xw7 unused = gu7.d(g(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final void c0() {
        if (this.p.e()) {
            this.p.j();
        }
    }

    @DexIgnore
    public final void d0() {
        xw7 unused = gu7.d(g(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0229  */
    /* renamed from: e0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.zu5.b r11, com.fossil.qn7<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 572
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zu5.k(com.fossil.zu5$b, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return S;
    }
}
