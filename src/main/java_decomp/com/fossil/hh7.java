package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hh7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ Gh7 f;

    @DexIgnore
    public Hh7(Gh7 gh7, List list, int i, boolean z, boolean z2) {
        this.f = gh7;
        this.b = list;
        this.c = i;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public void run() {
        this.f.o(this.b, this.c, this.d);
        if (this.e) {
            this.b.clear();
        }
    }
}
