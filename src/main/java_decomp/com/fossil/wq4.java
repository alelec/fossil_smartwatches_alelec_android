package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.Tq4;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wq4 implements Vq4 {
    @DexIgnore
    public static /* final */ int c;
    @DexIgnore
    public static /* final */ int d;
    @DexIgnore
    public static /* final */ int e; // = ((c * 2) + 1);
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new Ai();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(Integer.MAX_VALUE);
    @DexIgnore
    public /* final */ Handler a; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ ThreadPoolExecutor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Tq4.Di b;
        @DexIgnore
        public /* final */ /* synthetic */ Tq4.Ai c;

        @DexIgnore
        public Bi(Wq4 wq4, Tq4.Di di, Tq4.Ai ai) {
            this.b = di;
            this.c = ai;
        }

        @DexIgnore
        public void run() {
            Tq4.Di di = this.b;
            if (di != null) {
                di.a(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Tq4.Di b;
        @DexIgnore
        public /* final */ /* synthetic */ Tq4.Ci c;

        @DexIgnore
        public Ci(Wq4 wq4, Tq4.Di di, Tq4.Ci ci) {
            this.b = di;
            this.c = ci;
        }

        @DexIgnore
        public void run() {
            Tq4.Di di = this.b;
            if (di != null) {
                di.onSuccess(this.c);
            }
        }
    }

    /*
    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        c = availableProcessors;
        d = Math.max(2, Math.min(availableProcessors - 1, 4));
    }
    */

    @DexIgnore
    public Wq4() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(d, e, 30, TimeUnit.SECONDS, g, f);
        this.b = threadPoolExecutor;
        threadPoolExecutor.allowCoreThreadTimeOut(true);
    }

    @DexIgnore
    @Override // com.fossil.Vq4
    public <P extends Tq4.Ci, E extends Tq4.Ai> void a(P p, Tq4.Di<P, E> di) {
        this.a.post(new Ci(this, di, p));
    }

    @DexIgnore
    @Override // com.fossil.Vq4
    public <P extends Tq4.Ci, E extends Tq4.Ai> void b(E e2, Tq4.Di<P, E> di) {
        this.a.post(new Bi(this, di, e2));
    }

    @DexIgnore
    @Override // com.fossil.Vq4
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }
}
