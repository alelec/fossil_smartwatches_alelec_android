package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Df4 implements Ft3 {
    @DexIgnore
    public /* final */ Ef4 a;

    @DexIgnore
    public Df4(Ef4 ef4) {
        this.a = ef4;
    }

    @DexIgnore
    @Override // com.fossil.Ft3
    public final Object then(Nt3 nt3) {
        return this.a.g(nt3);
    }
}
