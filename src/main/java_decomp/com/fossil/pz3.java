package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Nl0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pz3 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ ColorStateList h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ float j;
    @DexIgnore
    public /* final */ float k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public Typeface n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Nl0.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Rz3 a;

        @DexIgnore
        public Ai(Rz3 rz3) {
            this.a = rz3;
        }

        @DexIgnore
        @Override // com.fossil.Nl0.Ai
        public void c(int i) {
            Pz3.this.m = true;
            this.a.a(i);
        }

        @DexIgnore
        @Override // com.fossil.Nl0.Ai
        public void d(Typeface typeface) {
            Pz3 pz3 = Pz3.this;
            pz3.n = Typeface.create(typeface, pz3.e);
            Pz3.this.m = true;
            this.a.b(Pz3.this.n, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Rz3 {
        @DexIgnore
        public /* final */ /* synthetic */ TextPaint a;
        @DexIgnore
        public /* final */ /* synthetic */ Rz3 b;

        @DexIgnore
        public Bi(TextPaint textPaint, Rz3 rz3) {
            this.a = textPaint;
            this.b = rz3;
        }

        @DexIgnore
        @Override // com.fossil.Rz3
        public void a(int i) {
            this.b.a(i);
        }

        @DexIgnore
        @Override // com.fossil.Rz3
        public void b(Typeface typeface, boolean z) {
            Pz3.this.k(this.a, typeface);
            this.b.b(typeface, z);
        }
    }

    @DexIgnore
    public Pz3(Context context, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, Tw3.TextAppearance);
        this.a = obtainStyledAttributes.getDimension(Tw3.TextAppearance_android_textSize, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b = Oz3.a(context, obtainStyledAttributes, Tw3.TextAppearance_android_textColor);
        this.c = Oz3.a(context, obtainStyledAttributes, Tw3.TextAppearance_android_textColorHint);
        this.d = Oz3.a(context, obtainStyledAttributes, Tw3.TextAppearance_android_textColorLink);
        this.e = obtainStyledAttributes.getInt(Tw3.TextAppearance_android_textStyle, 0);
        this.f = obtainStyledAttributes.getInt(Tw3.TextAppearance_android_typeface, 1);
        int e2 = Oz3.e(obtainStyledAttributes, Tw3.TextAppearance_fontFamily, Tw3.TextAppearance_android_fontFamily);
        this.l = obtainStyledAttributes.getResourceId(e2, 0);
        this.g = obtainStyledAttributes.getString(e2);
        obtainStyledAttributes.getBoolean(Tw3.TextAppearance_textAllCaps, false);
        this.h = Oz3.a(context, obtainStyledAttributes, Tw3.TextAppearance_android_shadowColor);
        this.i = obtainStyledAttributes.getFloat(Tw3.TextAppearance_android_shadowDx, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.j = obtainStyledAttributes.getFloat(Tw3.TextAppearance_android_shadowDy, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.k = obtainStyledAttributes.getFloat(Tw3.TextAppearance_android_shadowRadius, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final void d() {
        String str;
        if (this.n == null && (str = this.g) != null) {
            this.n = Typeface.create(str, this.e);
        }
        if (this.n == null) {
            int i2 = this.f;
            if (i2 == 1) {
                this.n = Typeface.SANS_SERIF;
            } else if (i2 == 2) {
                this.n = Typeface.SERIF;
            } else if (i2 != 3) {
                this.n = Typeface.DEFAULT;
            } else {
                this.n = Typeface.MONOSPACE;
            }
            this.n = Typeface.create(this.n, this.e);
        }
    }

    @DexIgnore
    public Typeface e() {
        d();
        return this.n;
    }

    @DexIgnore
    public Typeface f(Context context) {
        if (this.m) {
            return this.n;
        }
        if (!context.isRestricted()) {
            try {
                Typeface b2 = Nl0.b(context, this.l);
                this.n = b2;
                if (b2 != null) {
                    this.n = Typeface.create(b2, this.e);
                }
            } catch (Resources.NotFoundException | UnsupportedOperationException e2) {
            } catch (Exception e3) {
                Log.d("TextAppearance", "Error loading font " + this.g, e3);
            }
        }
        d();
        this.m = true;
        return this.n;
    }

    @DexIgnore
    public void g(Context context, TextPaint textPaint, Rz3 rz3) {
        k(textPaint, e());
        h(context, new Bi(textPaint, rz3));
    }

    @DexIgnore
    public void h(Context context, Rz3 rz3) {
        if (Qz3.a()) {
            f(context);
        } else {
            d();
        }
        if (this.l == 0) {
            this.m = true;
        }
        if (this.m) {
            rz3.b(this.n, true);
            return;
        }
        try {
            Nl0.d(context, this.l, new Ai(rz3), null);
        } catch (Resources.NotFoundException e2) {
            this.m = true;
            rz3.a(1);
        } catch (Exception e3) {
            Log.d("TextAppearance", "Error loading font " + this.g, e3);
            this.m = true;
            rz3.a(-3);
        }
    }

    @DexIgnore
    public void i(Context context, TextPaint textPaint, Rz3 rz3) {
        j(context, textPaint, rz3);
        ColorStateList colorStateList = this.b;
        textPaint.setColor(colorStateList != null ? colorStateList.getColorForState(textPaint.drawableState, colorStateList.getDefaultColor()) : -16777216);
        float f2 = this.k;
        float f3 = this.i;
        float f4 = this.j;
        ColorStateList colorStateList2 = this.h;
        textPaint.setShadowLayer(f2, f3, f4, colorStateList2 != null ? colorStateList2.getColorForState(textPaint.drawableState, colorStateList2.getDefaultColor()) : 0);
    }

    @DexIgnore
    public void j(Context context, TextPaint textPaint, Rz3 rz3) {
        if (Qz3.a()) {
            k(textPaint, f(context));
        } else {
            g(context, textPaint, rz3);
        }
    }

    @DexIgnore
    public void k(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
        int style = typeface.getStyle() & this.e;
        textPaint.setFakeBoldText((style & 1) != 0);
        textPaint.setTextSkewX((style & 2) != 0 ? -0.25f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        textPaint.setTextSize(this.a);
    }
}
