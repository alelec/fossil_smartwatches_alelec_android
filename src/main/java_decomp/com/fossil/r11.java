package com.fossil;

import android.content.Context;
import android.os.Build;
import java.io.File;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class R11 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("WrkDbPathHelper");
    @DexIgnore
    public static /* final */ String[] b; // = {"-journal", "-shm", "-wal"};

    @DexIgnore
    public static File a(Context context) {
        return Build.VERSION.SDK_INT < 23 ? b(context) : c(context, "androidx.work.workdb");
    }

    @DexIgnore
    public static File b(Context context) {
        return context.getDatabasePath("androidx.work.workdb");
    }

    @DexIgnore
    public static File c(Context context, String str) {
        return new File(context.getNoBackupFilesDir(), str);
    }

    @DexIgnore
    public static String d() {
        return "androidx.work.workdb";
    }

    @DexIgnore
    public static void e(Context context) {
        File b2 = b(context);
        if (Build.VERSION.SDK_INT >= 23 && b2.exists()) {
            X01.c().a(a, "Migrating WorkDatabase to the no-backup directory", new Throwable[0]);
            Map<File, File> f = f(context);
            for (File file : f.keySet()) {
                File file2 = f.get(file);
                if (file.exists() && file2 != null) {
                    if (file2.exists()) {
                        X01.c().h(a, String.format("Over-writing contents of %s", file2), new Throwable[0]);
                    }
                    X01.c().a(a, file.renameTo(file2) ? String.format("Migrated %s to %s", file, file2) : String.format("Renaming %s to %s failed", file, file2), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public static Map<File, File> f(Context context) {
        HashMap hashMap = new HashMap();
        if (Build.VERSION.SDK_INT >= 23) {
            File b2 = b(context);
            File a2 = a(context);
            hashMap.put(b2, a2);
            String[] strArr = b;
            for (String str : strArr) {
                hashMap.put(new File(b2.getPath() + str), new File(a2.getPath() + str));
            }
        }
        return hashMap;
    }
}
