package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class J75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RelativeLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ View J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ CustomizeWidget L;
    @DexIgnore
    public /* final */ CustomizeWidget M;
    @DexIgnore
    public /* final */ CustomizeWidget N;
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ RelativeLayout s;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    public J75(Object obj, View view, int i, RTLImageView rTLImageView, RelativeLayout relativeLayout, RelativeLayout relativeLayout2, RelativeLayout relativeLayout3, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, LinearLayout linearLayout, RelativeLayout relativeLayout4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, View view2, View view3, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = relativeLayout;
        this.s = relativeLayout2;
        this.t = relativeLayout3;
        this.u = constraintLayout;
        this.v = constraintLayout2;
        this.w = rTLImageView2;
        this.x = rTLImageView3;
        this.y = rTLImageView4;
        this.z = linearLayout;
        this.A = relativeLayout4;
        this.B = flexibleTextView;
        this.C = flexibleTextView2;
        this.D = flexibleTextView3;
        this.E = flexibleTextView4;
        this.F = flexibleTextView5;
        this.G = flexibleTextView6;
        this.H = flexibleTextView7;
        this.I = flexibleTextView8;
        this.J = view2;
        this.K = view3;
        this.L = customizeWidget;
        this.M = customizeWidget2;
        this.N = customizeWidget3;
    }
}
