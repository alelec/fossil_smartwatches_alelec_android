package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gj extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Ko b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Gj(Ko ko) {
        super(1);
        this.b = ko;
    }

    @DexIgnore
    public final void a(Lp lp) {
        this.b.U = System.currentTimeMillis();
        Ko ko = this.b;
        Zk1 zk1 = ((Fh) lp).C;
        ko.X = zk1;
        ko.W = zk1.getFirmwareVersion();
        Ko ko2 = this.b;
        String str = ko2.a0;
        if (str == null || Vt7.j(ko2.W, str, true)) {
            Ko ko3 = this.b;
            ko3.Z = false;
            ko3.l(Nr.a(ko3.v, null, Zq.b, null, null, 13));
            return;
        }
        Ko ko4 = this.b;
        ko4.Z = false;
        ko4.l(Nr.a(ko4.v, null, Zq.D, null, null, 13));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Lp lp) {
        a(lp);
        return Cd6.a;
    }
}
