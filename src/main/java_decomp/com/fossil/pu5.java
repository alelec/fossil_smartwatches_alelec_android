package com.fossil;

import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixElementStatus;
import com.google.maps.model.Duration;
import com.google.maps.model.LatLng;
import com.google.maps.model.TrafficModel;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.manager.SoLibraryLoader;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pu5 {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        String simpleName = Pu5.class.getSimpleName();
        Wg6.b(simpleName, "DurationUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final long a(String str, TravelMode travelMode, boolean z, double d, double d2) {
        String str2;
        Wg6.c(str, "destinationAddress");
        Wg6.c(travelMode, "travelMode");
        FLogger.INSTANCE.getLocal().d(a, "executeUseCase");
        GeoApiContext.Builder builder = new GeoApiContext.Builder();
        Access c = SoLibraryLoader.f().c(PortfolioApp.get.instance());
        if (c == null || (str2 = c.getN()) == null) {
            str2 = "";
        }
        DistanceMatrixApiRequest departureTime = DistanceMatrixApi.newRequest(builder.apiKey(str2).build()).origins(new LatLng(d, d2)).destinations(str).mode(travelMode).units(Unit.IMPERIAL).trafficModel(TrafficModel.BEST_GUESS).departureTime(new DateTime(System.currentTimeMillis()));
        if (z) {
            departureTime.avoid(DirectionsApi.RouteRestriction.TOLLS);
        }
        try {
            DistanceMatrix distanceMatrix = (DistanceMatrix) departureTime.await();
            DistanceMatrixElement distanceMatrixElement = distanceMatrix.rows[0].elements[0];
            DistanceMatrixElementStatus distanceMatrixElementStatus = distanceMatrixElement != null ? distanceMatrixElement.status : null;
            if (distanceMatrixElementStatus == null || distanceMatrixElementStatus != DistanceMatrixElementStatus.OK) {
                return -1;
            }
            DistanceMatrixElement distanceMatrixElement2 = distanceMatrix.rows[0].elements[0];
            Duration duration = distanceMatrixElement2 != null ? distanceMatrixElement2.durationInTraffic : null;
            if (duration != null) {
                return duration.inSeconds;
            }
            DistanceMatrixElement distanceMatrixElement3 = distanceMatrix.rows[0].elements[0];
            Duration duration2 = distanceMatrixElement3 != null ? distanceMatrixElement3.duration : null;
            if (duration2 != null) {
                return duration2.inSeconds;
            }
            return -1;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.d(str3, "Exception ex=" + e);
            e.printStackTrace();
        }
    }
}
