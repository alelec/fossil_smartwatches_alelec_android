package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K95 extends J95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G;
    @DexIgnore
    public long E;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        G = sparseIntArray;
        sparseIntArray.put(2131362666, 1);
        G.put(2131362546, 2);
        G.put(2131362047, 3);
        G.put(2131361815, 4);
        G.put(2131362831, 5);
        G.put(2131362932, 6);
        G.put(2131362326, 7);
        G.put(2131362787, 8);
        G.put(2131362114, 9);
        G.put(2131361816, 10);
        G.put(2131362830, 11);
        G.put(2131362931, 12);
        G.put(2131362327, 13);
    }
    */

    @DexIgnore
    public K95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 14, F, G));
    }

    @DexIgnore
    public K95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleCheckBox) objArr[4], (FlexibleCheckBox) objArr[10], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[2], (RTLImageView) objArr[1], (View) objArr[8], (ConstraintLayout) objArr[11], (ConstraintLayout) objArr[5], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[6], (ConstraintLayout) objArr[0]);
        this.E = -1;
        this.D.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.E != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.E = 1;
        }
        w();
    }
}
