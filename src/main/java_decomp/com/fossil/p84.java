package com.fossil;

import com.fossil.Ta4;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P84 implements L94 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public P84(String str, String str2, byte[] bArr) {
        this.b = str;
        this.c = str2;
        this.a = bArr;
    }

    @DexIgnore
    @Override // com.fossil.L94
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.L94
    public InputStream b() {
        if (e()) {
            return null;
        }
        return new ByteArrayInputStream(this.a);
    }

    @DexIgnore
    @Override // com.fossil.L94
    public Ta4.Ci.Bii c() {
        byte[] d = d();
        if (d == null) {
            return null;
        }
        Ta4.Ci.Bii.Aiii a2 = Ta4.Ci.Bii.a();
        a2.b(d);
        a2.c(this.b);
        return a2.a();
    }

    @DexIgnore
    public final byte[] d() {
        if (e()) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                try {
                    gZIPOutputStream.write(this.a);
                    gZIPOutputStream.finish();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    gZIPOutputStream.close();
                    byteArrayOutputStream.close();
                    return byteArray;
                } catch (Throwable th) {
                }
            } catch (Throwable th2) {
            }
        } catch (IOException e) {
            return null;
        }
        throw th;
        throw th;
    }

    @DexIgnore
    public final boolean e() {
        byte[] bArr = this.a;
        return bArr == null || bArr.length == 0;
    }
}
