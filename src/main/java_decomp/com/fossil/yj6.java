package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yj6 {
    @DexIgnore
    public /* final */ Sj6 a;
    @DexIgnore
    public /* final */ Jk6 b;
    @DexIgnore
    public /* final */ Dk6 c;

    @DexIgnore
    public Yj6(Sj6 sj6, Jk6 jk6, Dk6 dk6) {
        Wg6.c(sj6, "mSleepOverviewDayView");
        Wg6.c(jk6, "mSleepOverviewWeekView");
        Wg6.c(dk6, "mSleepOverviewMonthView");
        this.a = sj6;
        this.b = jk6;
        this.c = dk6;
    }

    @DexIgnore
    public final Sj6 a() {
        return this.a;
    }

    @DexIgnore
    public final Dk6 b() {
        return this.c;
    }

    @DexIgnore
    public final Jk6 c() {
        return this.b;
    }
}
