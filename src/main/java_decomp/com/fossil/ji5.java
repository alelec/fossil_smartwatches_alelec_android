package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ji5 {
    DEVICE("device"),
    MANUAL("manual");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ji5 a(String str) {
            Ji5 ji5;
            Wg6.c(str, "value");
            Ji5[] values = Ji5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ji5 = null;
                    break;
                }
                ji5 = values[i];
                String mValue = ji5.getMValue();
                String lowerCase = str.toLowerCase();
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (Wg6.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return ji5 != null ? ji5 : Ji5.DEVICE;
        }
    }

    @DexIgnore
    public Ji5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
