package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.A94;
import com.fossil.Bc4;
import com.fossil.Ec4;
import com.fossil.V94;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U84 {
    @DexIgnore
    public static /* final */ Comparator<File> A; // = new Pi();
    @DexIgnore
    public static /* final */ Comparator<File> B; // = new Qi();
    @DexIgnore
    public static /* final */ Pattern C; // = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    @DexIgnore
    public static /* final */ Map<String, String> D; // = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    @DexIgnore
    public static /* final */ String[] E; // = {"SessionUser", "SessionApp", "SessionOS", "SessionDevice"};
    @DexIgnore
    public static /* final */ FilenameFilter x; // = new Ji("BeginSession");
    @DexIgnore
    public static /* final */ FilenameFilter y; // = T84.a();
    @DexIgnore
    public static /* final */ FilenameFilter z; // = new Oi();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ C94 b;
    @DexIgnore
    public /* final */ X84 c;
    @DexIgnore
    public /* final */ S94 d;
    @DexIgnore
    public /* final */ S84 e;
    @DexIgnore
    public /* final */ Kb4 f;
    @DexIgnore
    public /* final */ H94 g;
    @DexIgnore
    public /* final */ Tb4 h;
    @DexIgnore
    public /* final */ L84 i;
    @DexIgnore
    public /* final */ Bc4.Bi j;
    @DexIgnore
    public /* final */ a0 k;
    @DexIgnore
    public /* final */ V94 l;
    @DexIgnore
    public /* final */ Ac4 m;
    @DexIgnore
    public /* final */ Bc4.Ai n;
    @DexIgnore
    public /* final */ W74 o;
    @DexIgnore
    public /* final */ Kd4 p;
    @DexIgnore
    public /* final */ String q;
    @DexIgnore
    public /* final */ B84 r;
    @DexIgnore
    public /* final */ Q94 s;
    @DexIgnore
    public A94 t;
    @DexIgnore
    public Ot3<Boolean> u; // = new Ot3<>();
    @DexIgnore
    public Ot3<Boolean> v; // = new Ot3<>();
    @DexIgnore
    public Ot3<Void> w; // = new Ot3<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ long a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ai(long j, String str) {
            this.a = j;
            this.b = str;
        }

        @DexIgnore
        public Void a() throws Exception {
            if (U84.this.f0()) {
                return null;
            }
            U84.this.l.i(this.a, this.b);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ S94 a;

        @DexIgnore
        public Bi(S94 s94) {
            this.a = s94;
        }

        @DexIgnore
        public Void a() throws Exception {
            U84.this.s.l();
            new K94(U84.this.W()).i(U84.this.T(), this.a);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Map a;

        @DexIgnore
        public Ci(Map map) {
            this.a = map;
        }

        @DexIgnore
        public Void a() throws Exception {
            new K94(U84.this.W()).h(U84.this.T(), this.a);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Callable<Void> {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public Void a() throws Exception {
            U84.this.L();
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei implements Runnable {
        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public void run() {
            U84 u84 = U84.this;
            u84.I(u84.k0(new Zi()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements FilenameFilter {
        @DexIgnore
        public /* final */ /* synthetic */ Set a;

        @DexIgnore
        public Fi(U84 u84, Set set) {
            this.a = set;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            if (str.length() < 35) {
                return false;
            }
            return this.a.contains(str.substring(0, 35));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;

        @DexIgnore
        public Gi(U84 u84, String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.p(xb4, this.a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public Hi(String str, String str2, String str3, String str4, int i) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
            this.e = i;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.r(xb4, this.a, this.b, this.c, this.d, this.e, U84.this.q);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public Ii(U84 u84, String str, String str2, boolean z) {
            this.a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.B(xb4, this.a, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji extends Yi {
        @DexIgnore
        public Ji(String str) {
            super(str);
        }

        @DexIgnore
        @Override // com.fossil.U84.Yi
        public boolean accept(File file, String str) {
            return super.accept(file, str) && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;
        @DexIgnore
        public /* final */ /* synthetic */ long e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ String h;
        @DexIgnore
        public /* final */ /* synthetic */ String i;

        @DexIgnore
        public Ki(U84 u84, int i2, String str, int i3, long j, long j2, boolean z, int i4, String str2, String str3) {
            this.a = i2;
            this.b = str;
            this.c = i3;
            this.d = j;
            this.e = j2;
            this.f = z;
            this.g = i4;
            this.h = str2;
            this.i = str3;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.t(xb4, this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ S94 a;

        @DexIgnore
        public Li(U84 u84, S94 s94) {
            this.a = s94;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.C(xb4, this.a.b(), null, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Mi implements Xi {
        @DexIgnore
        public /* final */ /* synthetic */ String a;

        @DexIgnore
        public Mi(String str) {
            this.a = str;
        }

        @DexIgnore
        @Override // com.fossil.U84.Xi
        public void a(Xb4 xb4) throws Exception {
            Yb4.s(xb4, this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ni implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ long a;

        @DexIgnore
        public Ni(long j) {
            this.a = j;
        }

        @DexIgnore
        public Void a() throws Exception {
            Bundle bundle = new Bundle();
            bundle.putInt("fatal", 1);
            bundle.putLong("timestamp", this.a);
            U84.this.r.a("_ae", bundle);
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Oi implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.length() == 39 && str.endsWith(".cls");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Pi implements Comparator<File> {
        @DexIgnore
        public int a(File file, File file2) {
            return file2.getName().compareTo(file.getName());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(File file, File file2) {
            return a(file, file2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Qi implements Comparator<File> {
        @DexIgnore
        public int a(File file, File file2) {
            return file.getName().compareTo(file2.getName());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(File file, File file2) {
            return a(file, file2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ri implements A94.Ai {
        @DexIgnore
        public Ri() {
        }

        @DexIgnore
        @Override // com.fossil.A94.Ai
        public void a(Rc4 rc4, Thread thread, Throwable th) {
            U84.this.e0(rc4, thread, th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Si implements Callable<Nt3<Void>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date a;
        @DexIgnore
        public /* final */ /* synthetic */ Throwable b;
        @DexIgnore
        public /* final */ /* synthetic */ Thread c;
        @DexIgnore
        public /* final */ /* synthetic */ Rc4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Mt3<Wc4, Void> {
            @DexIgnore
            public /* final */ /* synthetic */ Executor a;

            @DexIgnore
            public Aii(Executor executor) {
                this.a = executor;
            }

            @DexIgnore
            public Nt3<Void> a(Wc4 wc4) throws Exception {
                if (wc4 == null) {
                    X74.f().i("Received null app settings, cannot send reports at crash time.");
                    return Qt3.f(null);
                }
                U84.this.u0(wc4, true);
                return Qt3.h(U84.this.q0(), U84.this.s.n(this.a, D94.getState(wc4)));
            }

            @DexIgnore
            /* Return type fixed from 'com.fossil.Nt3' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Mt3
            public /* bridge */ /* synthetic */ Nt3<Void> then(Wc4 wc4) throws Exception {
                return a(wc4);
            }
        }

        @DexIgnore
        public Si(Date date, Throwable th, Thread thread, Rc4 rc4) {
            this.a = date;
            this.b = th;
            this.c = thread;
            this.d = rc4;
        }

        @DexIgnore
        public Nt3<Void> a() throws Exception {
            U84.this.c.a();
            long b0 = U84.b0(this.a);
            U84.this.s.k(this.b, this.c, b0);
            U84.this.F0(this.c, this.b, b0);
            U84.this.D0(this.a.getTime());
            Zc4 b2 = this.d.b();
            int i = b2.b().a;
            int i2 = b2.b().b;
            U84.this.J(i);
            U84.this.L();
            U84.this.B0(i2);
            if (!U84.this.b.b()) {
                return Qt3.f(null);
            }
            Executor c2 = U84.this.e.c();
            return this.d.a().s(c2, new Aii(c2));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Nt3<Void> call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ti implements Mt3<Void, Boolean> {
        @DexIgnore
        public Ti(U84 u84) {
        }

        @DexIgnore
        public Nt3<Boolean> a(Void r2) throws Exception {
            return Qt3.f(Boolean.TRUE);
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Nt3' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Mt3
        public /* bridge */ /* synthetic */ Nt3<Boolean> then(Void r2) throws Exception {
            return a(r2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ui implements Mt3<Boolean, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Nt3 a;
        @DexIgnore
        public /* final */ /* synthetic */ float b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Callable<Nt3<Void>> {
            @DexIgnore
            public /* final */ /* synthetic */ Boolean a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Aiii implements Mt3<Wc4, Void> {
                @DexIgnore
                public /* final */ /* synthetic */ List a;
                @DexIgnore
                public /* final */ /* synthetic */ boolean b;
                @DexIgnore
                public /* final */ /* synthetic */ Executor c;

                @DexIgnore
                public Aiii(List list, boolean z, Executor executor) {
                    this.a = list;
                    this.b = z;
                    this.c = executor;
                }

                @DexIgnore
                public Nt3<Void> a(Wc4 wc4) throws Exception {
                    if (wc4 == null) {
                        X74.f().i("Received null app settings, cannot send reports during app startup.");
                        return Qt3.f(null);
                    }
                    for (Ec4 ec4 : this.a) {
                        if (ec4.getType() == Ec4.Ai.JAVA) {
                            U84.x(wc4.e, ec4.c());
                        }
                    }
                    U84.this.q0();
                    U84.this.j.a(wc4).e(this.a, this.b, Ui.this.b);
                    U84.this.s.n(this.c, D94.getState(wc4));
                    U84.this.w.e(null);
                    return Qt3.f(null);
                }

                @DexIgnore
                /* Return type fixed from 'com.fossil.Nt3' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.fossil.Mt3
                public /* bridge */ /* synthetic */ Nt3<Void> then(Wc4 wc4) throws Exception {
                    return a(wc4);
                }
            }

            @DexIgnore
            public Aii(Boolean bool) {
                this.a = bool;
            }

            @DexIgnore
            public Nt3<Void> a() throws Exception {
                List<Ec4> d = U84.this.m.d();
                if (!this.a.booleanValue()) {
                    X74.f().b("Reports are being deleted.");
                    U84.G(U84.this.h0());
                    U84.this.m.c(d);
                    U84.this.s.m();
                    U84.this.w.e(null);
                    return Qt3.f(null);
                }
                X74.f().b("Reports are being sent.");
                boolean booleanValue = this.a.booleanValue();
                U84.this.b.a(booleanValue);
                Executor c = U84.this.e.c();
                return Ui.this.a.s(c, new Aiii(d, booleanValue, c));
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // java.util.concurrent.Callable
            public /* bridge */ /* synthetic */ Nt3<Void> call() throws Exception {
                return a();
            }
        }

        @DexIgnore
        public Ui(Nt3 nt3, float f) {
            this.a = nt3;
            this.b = f;
        }

        @DexIgnore
        public Nt3<Void> a(Boolean bool) throws Exception {
            return U84.this.e.i(new Aii(bool));
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Nt3' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Mt3
        public /* bridge */ /* synthetic */ Nt3<Void> then(Boolean bool) throws Exception {
            return a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Vi implements Bc4.Bi {
        @DexIgnore
        public Vi() {
        }

        @DexIgnore
        @Override // com.fossil.Bc4.Bi
        public Bc4 a(Wc4 wc4) {
            String str = wc4.c;
            String str2 = wc4.d;
            return new Bc4(wc4.e, U84.this.i.a, D94.getState(wc4), U84.this.m, U84.this.S(str, str2), U84.this.n);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Wi implements FilenameFilter {
        @DexIgnore
        public Wi() {
        }

        @DexIgnore
        public /* synthetic */ Wi(Ji ji) {
            this();
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return !U84.z.accept(file, str) && U84.C.matcher(str).matches();
        }
    }

    @DexIgnore
    public interface Xi {
        @DexIgnore
        void a(Xb4 xb4) throws Exception;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Yi implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Yi(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            return str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Zi implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return Wb4.e.accept(file, str) || str.contains("SessionMissingBinaryImages");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 implements V94.Bi {
        @DexIgnore
        public /* final */ Tb4 a;

        @DexIgnore
        public a0(Tb4 tb4) {
            this.a = tb4;
        }

        @DexIgnore
        @Override // com.fossil.V94.Bi
        public File a() {
            File file = new File(this.a.b(), "log-files");
            if (!file.exists()) {
                file.mkdirs();
            }
            return file;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements Bc4.Ci {
        @DexIgnore
        public b0() {
        }

        @DexIgnore
        public /* synthetic */ b0(U84 u84, Ji ji) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.Bc4.Ci
        public File[] a() {
            return U84.this.l0();
        }

        @DexIgnore
        @Override // com.fossil.Bc4.Ci
        public File[] b() {
            return U84.this.i0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements Bc4.Ai {
        @DexIgnore
        public c0() {
        }

        @DexIgnore
        public /* synthetic */ c0(U84 u84, Ji ji) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.Bc4.Ai
        public boolean a() {
            return U84.this.f0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0 implements Runnable {
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ Ec4 c;
        @DexIgnore
        public /* final */ Bc4 d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public d0(Context context, Ec4 ec4, Bc4 bc4, boolean z) {
            this.b = context;
            this.c = ec4;
            this.d = bc4;
            this.e = z;
        }

        @DexIgnore
        public void run() {
            if (R84.c(this.b)) {
                X74.f().b("Attempting to send crash report at time of crash...");
                this.d.d(this.c, this.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e0 implements FilenameFilter {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public e0(String str) {
            this.a = str;
        }

        @DexIgnore
        public boolean accept(File file, String str) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".cls");
            return !str.equals(sb.toString()) && str.contains(this.a) && !str.endsWith(".cls_temp");
        }
    }

    @DexIgnore
    public U84(Context context, S84 s84, Kb4 kb4, H94 h94, C94 c94, Tb4 tb4, X84 x84, L84 l84, Ac4 ac4, Bc4.Bi bi, W74 w74, Nd4 nd4, B84 b84, Rc4 rc4) {
        new AtomicInteger(0);
        new AtomicBoolean(false);
        this.a = context;
        this.e = s84;
        this.f = kb4;
        this.g = h94;
        this.b = c94;
        this.h = tb4;
        this.c = x84;
        this.i = l84;
        if (bi != null) {
            this.j = bi;
        } else {
            this.j = F();
        }
        this.o = w74;
        this.q = nd4.a();
        this.r = b84;
        this.d = new S94();
        this.k = new a0(tb4);
        this.l = new V94(context, this.k);
        this.m = ac4 == null ? new Ac4(new b0(this, null)) : ac4;
        this.n = new c0(this, null);
        Hd4 hd4 = new Hd4(1024, new Jd4(10));
        this.p = hd4;
        this.s = Q94.b(context, h94, tb4, l84, this.l, this.d, hd4, rc4);
    }

    @DexIgnore
    public static void E(InputStream inputStream, Xb4 xb4, int i2) throws IOException {
        byte[] bArr = new byte[i2];
        int i3 = 0;
        while (i3 < i2) {
            int read = inputStream.read(bArr, i3, i2 - i3);
            if (read < 0) {
                break;
            }
            i3 += read;
        }
        xb4.U(bArr);
    }

    @DexIgnore
    public static void G(File[] fileArr) {
        if (fileArr != null) {
            for (File file : fileArr) {
                file.delete();
            }
        }
    }

    @DexIgnore
    public static void H0(Xb4 xb4, File[] fileArr, String str) {
        Arrays.sort(fileArr, R84.c);
        for (File file : fileArr) {
            try {
                X74.f().b(String.format(Locale.US, "Found Non Fatal for session ID %s in %s ", str, file.getName()));
                P0(xb4, file);
            } catch (Exception e2) {
                X74.f().e("Error writting non-fatal to session.", e2);
            }
        }
    }

    @DexIgnore
    public static void P0(Xb4 xb4, File file) throws IOException {
        FileInputStream fileInputStream;
        if (!file.exists()) {
            X74.f().d("Tried to include a file that doesn't exist: " + file.getName());
            return;
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                E(fileInputStream, xb4, (int) file.length());
                R84.e(fileInputStream, "Failed to close file input stream.");
            } catch (Throwable th) {
                th = th;
                R84.e(fileInputStream, "Failed to close file input stream.");
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            R84.e(fileInputStream, "Failed to close file input stream.");
            throw th;
        }
    }

    @DexIgnore
    public static boolean Q() {
        try {
            Class.forName("com.google.firebase.crash.FirebaseCrash");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public static long U() {
        return b0(new Date());
    }

    @DexIgnore
    public static List<L94> X(Z74 z74, String str, Context context, File file, byte[] bArr) {
        byte[] bArr2;
        K94 k94 = new K94(file);
        File b2 = k94.b(str);
        File a2 = k94.a(str);
        try {
            bArr2 = Eb4.a(z74.d(), context);
        } catch (Exception e2) {
            bArr2 = null;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new P84("logs_file", FileLogWriter.LOG_FOLDER, bArr));
        arrayList.add(new P84("binary_images_file", "binaryImages", bArr2));
        arrayList.add(new G94("crash_meta_file", "metadata", z74.g()));
        arrayList.add(new G94("session_meta_file", Constants.SESSION, z74.f()));
        arrayList.add(new G94("app_meta_file", "app", z74.a()));
        arrayList.add(new G94("device_meta_file", "device", z74.c()));
        arrayList.add(new G94("os_meta_file", "os", z74.b()));
        arrayList.add(new G94("minidump_file", "minidump", z74.e()));
        arrayList.add(new G94("user_meta_file", "user", b2));
        arrayList.add(new G94("keys_file", "keys", a2));
        return arrayList;
    }

    @DexIgnore
    public static String a0(File file) {
        return file.getName().substring(0, 35);
    }

    @DexIgnore
    public static long b0(Date date) {
        return date.getTime() / 1000;
    }

    @DexIgnore
    public static String r0(String str) {
        return str.replaceAll(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, "");
    }

    @DexIgnore
    public static void x(String str, File file) throws Exception {
        if (str != null) {
            y(file, new Mi(str));
        }
    }

    @DexIgnore
    public static void y(File file, Xi xi) throws Exception {
        FileOutputStream fileOutputStream;
        Xb4 xb4;
        Xb4 xb42 = null;
        try {
            fileOutputStream = new FileOutputStream(file, true);
            try {
                xb42 = Xb4.z(fileOutputStream);
                xi.a(xb42);
                R84.j(xb42, "Failed to flush to append to " + file.getPath());
                R84.e(fileOutputStream, "Failed to close " + file.getPath());
            } catch (Throwable th) {
                th = th;
                xb4 = xb42;
                R84.j(xb4, "Failed to flush to append to " + file.getPath());
                R84.e(fileOutputStream, "Failed to close " + file.getPath());
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            xb4 = null;
            R84.j(xb4, "Failed to flush to append to " + file.getPath());
            R84.e(fileOutputStream, "Failed to close " + file.getPath());
            throw th;
        }
    }

    @DexIgnore
    public final void A(S94 s94) {
        this.e.h(new Bi(s94));
    }

    @DexIgnore
    public final void A0(String str, int i2) {
        File W = W();
        T94.d(W, new Yi(str + "SessionEvent"), i2, B);
    }

    @DexIgnore
    public void B() {
        this.e.g(new Ei());
    }

    @DexIgnore
    public void B0(int i2) {
        int f2 = i2 - T94.f(Y(), V(), i2, B);
        T94.d(W(), z, f2 - T94.c(Z(), f2, B), B);
    }

    @DexIgnore
    public final void C(File[] fileArr, int i2, int i3) {
        X74.f().b("Closing open sessions.");
        while (i2 < fileArr.length) {
            File file = fileArr[i2];
            String a02 = a0(file);
            X74 f2 = X74.f();
            f2.b("Closing session: " + a02);
            N0(file, a02, i3);
            i2++;
        }
    }

    @DexIgnore
    public final Nt3<Boolean> C0() {
        if (this.b.b()) {
            X74.f().b("Automatic data collection is enabled. Allowing upload.");
            this.u.e(Boolean.FALSE);
            return Qt3.f(Boolean.TRUE);
        }
        X74.f().b("Automatic data collection is disabled.");
        X74.f().b("Notifying that unsent reports are available.");
        this.u.e(Boolean.TRUE);
        Nt3<TContinuationResult> r2 = this.b.c().r(new Ti(this));
        X74.f().b("Waiting for send/deleteUnsentReports to be called.");
        return T94.g(r2, this.v.a());
    }

    @DexIgnore
    public final void D(Wb4 wb4) {
        if (wb4 != null) {
            try {
                wb4.a();
            } catch (IOException e2) {
                X74.f().e("Error closing session file stream in the presence of an exception", e2);
            }
        }
    }

    @DexIgnore
    public final void D0(long j2) {
        try {
            File W = W();
            new File(W, ".ae" + j2).createNewFile();
        } catch (IOException e2) {
            X74.f().b("Could not write app exception marker.");
        }
    }

    @DexIgnore
    public final void E0(String str, long j2) throws Exception {
        String format = String.format(Locale.US, "Crashlytics Android SDK/%s", W84.i());
        M0(str, "BeginSession", new Gi(this, str, format, j2));
        this.o.d(str, format, j2);
    }

    @DexIgnore
    public final Bc4.Bi F() {
        return new Vi();
    }

    @DexIgnore
    public final void F0(Thread thread, Throwable th, long j2) {
        Throwable th2;
        Wb4 wb4;
        Xb4 xb4;
        Throwable th3;
        Wb4 wb42;
        Xb4 xb42;
        Wb4 wb43;
        Xb4 z2;
        Xb4 xb43 = null;
        try {
            String T = T();
            if (T == null) {
                X74.f().d("Tried to write a fatal exception while no session was open.");
                R84.j(null, "Failed to flush to session begin file.");
                R84.e(null, "Failed to close fatal exception file output stream.");
                return;
            }
            wb43 = new Wb4(W(), T + "SessionCrash");
            try {
                z2 = Xb4.z(wb43);
            } catch (Exception e2) {
                e = e2;
                wb42 = wb43;
                xb42 = xb43;
                try {
                    X74.f().e("An error occurred in the fatal exception logger", e);
                    wb43 = wb42;
                    R84.j(xb42, "Failed to flush to session begin file.");
                    R84.e(wb43, "Failed to close fatal exception file output stream.");
                } catch (Throwable th4) {
                    th2 = th4;
                    wb4 = wb42;
                    xb43 = xb42;
                    xb4 = xb43;
                    th3 = th2;
                    R84.j(xb4, "Failed to flush to session begin file.");
                    R84.e(wb4, "Failed to close fatal exception file output stream.");
                    throw th3;
                }
            } catch (Throwable th5) {
                th2 = th5;
                wb4 = wb43;
                xb4 = xb43;
                th3 = th2;
                R84.j(xb4, "Failed to flush to session begin file.");
                R84.e(wb4, "Failed to close fatal exception file output stream.");
                throw th3;
            }
            try {
                K0(z2, thread, th, j2, CrashDumperPlugin.NAME, true);
                xb42 = z2;
            } catch (Exception e3) {
                e = e3;
                xb43 = z2;
                wb42 = wb43;
                xb42 = xb43;
                X74.f().e("An error occurred in the fatal exception logger", e);
                wb43 = wb42;
                R84.j(xb42, "Failed to flush to session begin file.");
                R84.e(wb43, "Failed to close fatal exception file output stream.");
            } catch (Throwable th6) {
                th2 = th6;
                wb4 = wb43;
                xb43 = z2;
                xb4 = xb43;
                th3 = th2;
                R84.j(xb4, "Failed to flush to session begin file.");
                R84.e(wb4, "Failed to close fatal exception file output stream.");
                throw th3;
            }
            R84.j(xb42, "Failed to flush to session begin file.");
            R84.e(wb43, "Failed to close fatal exception file output stream.");
        } catch (Exception e4) {
            e = e4;
            wb42 = null;
            xb42 = null;
            X74.f().e("An error occurred in the fatal exception logger", e);
            wb43 = wb42;
            R84.j(xb42, "Failed to flush to session begin file.");
            R84.e(wb43, "Failed to close fatal exception file output stream.");
        } catch (Throwable th7) {
            th3 = th7;
            wb4 = null;
            xb4 = null;
            R84.j(xb4, "Failed to flush to session begin file.");
            R84.e(wb4, "Failed to close fatal exception file output stream.");
            throw th3;
        }
    }

    @DexIgnore
    public final void G0(Xb4 xb4, String str) throws IOException {
        String[] strArr = E;
        for (String str2 : strArr) {
            File[] k0 = k0(new Yi(str + str2 + ".cls"));
            if (k0.length == 0) {
                X74.f().b("Can't find " + str2 + " data for session ID " + str);
            } else {
                X74.f().b("Collecting " + str2 + " data for session ID " + str);
                P0(xb4, k0[0]);
            }
        }
    }

    @DexIgnore
    public boolean H() {
        if (!this.c.c()) {
            String T = T();
            return T != null && this.o.e(T);
        }
        X74.f().b("Found previous crash marker.");
        this.c.d();
        return true;
    }

    @DexIgnore
    public void I(File[] fileArr) {
        HashSet hashSet = new HashSet();
        for (File file : fileArr) {
            X74.f().b("Found invalid session part file: " + file);
            hashSet.add(a0(file));
        }
        if (!hashSet.isEmpty()) {
            File[] k0 = k0(new Fi(this, hashSet));
            for (File file2 : k0) {
                X74.f().b("Deleting invalid session file: " + file2);
                file2.delete();
            }
        }
    }

    @DexIgnore
    public final void I0(String str) throws Exception {
        String d2 = this.g.d();
        L84 l84 = this.i;
        String str2 = l84.e;
        String str3 = l84.f;
        String a2 = this.g.a();
        int id = E94.determineFrom(this.i.c).getId();
        M0(str, "SessionApp", new Hi(d2, str2, str3, a2, id));
        this.o.f(str, d2, str2, str3, a2, id, this.q);
    }

    @DexIgnore
    public void J(int i2) throws Exception {
        K(i2, true);
    }

    @DexIgnore
    public final void J0(String str) throws Exception {
        Context R = R();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int m2 = R84.m();
        String str2 = Build.MODEL;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long v2 = R84.v();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean C2 = R84.C(R);
        int n2 = R84.n(R);
        String str3 = Build.MANUFACTURER;
        String str4 = Build.PRODUCT;
        M0(str, "SessionDevice", new Ki(this, m2, str2, availableProcessors, v2, blockCount, C2, n2, str3, str4));
        this.o.c(str, m2, str2, availableProcessors, v2, blockCount, C2, n2, str3, str4);
    }

    @DexIgnore
    public final void K(int i2, boolean z2) throws Exception {
        int i3 = !z2 ? 1 : 0;
        z0(i3 + 8);
        File[] o0 = o0();
        if (o0.length <= i3) {
            X74.f().b("No open sessions to be closed.");
            return;
        }
        String a02 = a0(o0[i3]);
        O0(a02);
        if (z2) {
            this.s.h();
        } else if (this.o.e(a02)) {
            O(a02);
            if (!this.o.a(a02)) {
                X74 f2 = X74.f();
                f2.b("Could not finalize native session: " + a02);
            }
        }
        C(o0, i3, i2);
        this.s.d(U());
    }

    @DexIgnore
    public final void K0(Xb4 xb4, Thread thread, Throwable th, long j2, String str, boolean z2) throws Exception {
        Thread[] threadArr;
        Map<String, String> treeMap;
        Ld4 ld4 = new Ld4(th, this.p);
        Context R = R();
        O84 a2 = O84.a(R);
        Float b2 = a2.b();
        int c2 = a2.c();
        boolean q2 = R84.q(R);
        int i2 = R.getResources().getConfiguration().orientation;
        long v2 = R84.v();
        long a3 = R84.a(R);
        long b3 = R84.b(Environment.getDataDirectory().getPath());
        ActivityManager.RunningAppProcessInfo k2 = R84.k(R.getPackageName(), R);
        LinkedList linkedList = new LinkedList();
        StackTraceElement[] stackTraceElementArr = ld4.c;
        String str2 = this.i.b;
        String d2 = this.g.d();
        int i3 = 0;
        if (z2) {
            Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            threadArr = new Thread[allStackTraces.size()];
            for (Map.Entry<Thread, StackTraceElement[]> entry : allStackTraces.entrySet()) {
                threadArr[i3] = entry.getKey();
                linkedList.add(this.p.a(entry.getValue()));
                i3++;
            }
        } else {
            threadArr = new Thread[0];
        }
        if (!R84.l(R, "com.crashlytics.CollectCustomKeys", true)) {
            treeMap = new TreeMap<>();
        } else {
            Map<String, String> a4 = this.d.a();
            treeMap = (a4 == null || a4.size() <= 1) ? a4 : new TreeMap<>(a4);
        }
        Yb4.u(xb4, j2, str, ld4, thread, stackTraceElementArr, threadArr, linkedList, 8, treeMap, this.l.c(), k2, i2, d2, str2, b2, c2, q2, v2 - a3, b3);
        this.l.a();
    }

    @DexIgnore
    public final void L() throws Exception {
        long U = U();
        String q84 = new Q84(this.g).toString();
        X74 f2 = X74.f();
        f2.b("Opening a new session with ID " + q84);
        this.o.h(q84);
        E0(q84, U);
        I0(q84);
        L0(q84);
        J0(q84);
        this.l.g(q84);
        this.s.g(r0(q84), U);
    }

    @DexIgnore
    public final void L0(String str) throws Exception {
        String str2 = Build.VERSION.RELEASE;
        String str3 = Build.VERSION.CODENAME;
        boolean E2 = R84.E(R());
        M0(str, "SessionOS", new Ii(this, str2, str3, E2));
        this.o.g(str, str2, str3, E2);
    }

    @DexIgnore
    public void M(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Rc4 rc4) {
        s0();
        A94 a94 = new A94(new Ri(), rc4, uncaughtExceptionHandler);
        this.t = a94;
        Thread.setDefaultUncaughtExceptionHandler(a94);
    }

    @DexIgnore
    public final void M0(String str, String str2, Xi xi) throws Exception {
        Throwable th;
        Wb4 wb4;
        Xb4 xb4;
        try {
            wb4 = new Wb4(W(), str + str2);
            try {
                xb4 = Xb4.z(wb4);
                try {
                    xi.a(xb4);
                    R84.j(xb4, "Failed to flush to session " + str2 + " file.");
                    R84.e(wb4, "Failed to close session " + str2 + " file.");
                } catch (Throwable th2) {
                    th = th2;
                    R84.j(xb4, "Failed to flush to session " + str2 + " file.");
                    R84.e(wb4, "Failed to close session " + str2 + " file.");
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                xb4 = null;
                R84.j(xb4, "Failed to flush to session " + str2 + " file.");
                R84.e(wb4, "Failed to close session " + str2 + " file.");
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            xb4 = null;
            wb4 = null;
            R84.j(xb4, "Failed to flush to session " + str2 + " file.");
            R84.e(wb4, "Failed to close session " + str2 + " file.");
            throw th;
        }
    }

    @DexIgnore
    public final File[] N(File[] fileArr) {
        return fileArr == null ? new File[0] : fileArr;
    }

    @DexIgnore
    public final void N0(File file, String str, int i2) {
        X74 f2 = X74.f();
        f2.b("Collecting session parts for ID " + str);
        File[] k0 = k0(new Yi(str + "SessionCrash"));
        boolean z2 = k0 != null && k0.length > 0;
        X74.f().b(String.format(Locale.US, "Session %s has fatal exception: %s", str, Boolean.valueOf(z2)));
        File[] k02 = k0(new Yi(str + "SessionEvent"));
        boolean z3 = k02 != null && k02.length > 0;
        X74.f().b(String.format(Locale.US, "Session %s has non-fatal exceptions: %s", str, Boolean.valueOf(z3)));
        if (z2 || z3) {
            y0(file, str, c0(str, k02, i2), z2 ? k0[0] : null);
        } else {
            X74 f3 = X74.f();
            f3.b("No events present for session ID " + str);
        }
        X74 f4 = X74.f();
        f4.b("Removing session part files for ID " + str);
        G(n0(str));
    }

    @DexIgnore
    public final void O(String str) {
        X74 f2 = X74.f();
        f2.b("Finalizing native report for session " + str);
        Z74 b2 = this.o.b(str);
        File e2 = b2.e();
        if (e2 == null || !e2.exists()) {
            X74 f3 = X74.f();
            f3.i("No minidump data found for session " + str);
            return;
        }
        long lastModified = e2.lastModified();
        V94 v94 = new V94(this.a, this.k, str);
        File file = new File(Y(), str);
        if (!file.mkdirs()) {
            X74.f().b("Couldn't create native sessions directory");
            return;
        }
        D0(lastModified);
        List<L94> X = X(b2, str, R(), W(), v94.c());
        M94.b(file, X);
        this.s.c(r0(str), X);
        v94.a();
    }

    @DexIgnore
    public final void O0(String str) throws Exception {
        M0(str, "SessionUser", new Li(this, d0(str)));
    }

    @DexIgnore
    public boolean P(int i2) {
        this.e.b();
        if (f0()) {
            X74.f().b("Skipping session finalization because a crash has already occurred.");
            return false;
        }
        X74.f().b("Finalizing previously open sessions.");
        try {
            K(i2, false);
            X74.f().b("Closed all previously open sessions");
            return true;
        } catch (Exception e2) {
            X74.f().e("Unable to finalize previously open sessions.", e2);
            return false;
        }
    }

    @DexIgnore
    public void Q0(long j2, String str) {
        this.e.h(new Ai(j2, str));
    }

    @DexIgnore
    public final Context R() {
        return this.a;
    }

    @DexIgnore
    public final Hc4 S(String str, String str2) {
        String u2 = R84.u(R(), "com.crashlytics.ApiEndpoint");
        return new Gc4(new Ic4(u2, str, this.f, W84.i()), new Jc4(u2, str2, this.f, W84.i()));
    }

    @DexIgnore
    public final String T() {
        File[] o0 = o0();
        if (o0.length > 0) {
            return a0(o0[0]);
        }
        return null;
    }

    @DexIgnore
    public File V() {
        return new File(W(), "fatal-sessions");
    }

    @DexIgnore
    public File W() {
        return this.h.b();
    }

    @DexIgnore
    public File Y() {
        return new File(W(), "native-sessions");
    }

    @DexIgnore
    public File Z() {
        return new File(W(), "nonfatal-sessions");
    }

    @DexIgnore
    public final File[] c0(String str, File[] fileArr, int i2) {
        if (fileArr.length <= i2) {
            return fileArr;
        }
        X74.f().b(String.format(Locale.US, "Trimming down to %d logged exceptions.", Integer.valueOf(i2)));
        A0(str, i2);
        return k0(new Yi(str + "SessionEvent"));
    }

    @DexIgnore
    public final S94 d0(String str) {
        return f0() ? this.d : new K94(W()).e(str);
    }

    @DexIgnore
    public void e0(Rc4 rc4, Thread thread, Throwable th) {
        synchronized (this) {
            X74 f2 = X74.f();
            f2.b("Crashlytics is handling uncaught exception \"" + th + "\" from thread " + thread.getName());
            try {
                T94.a(this.e.i(new Si(new Date(), th, thread, rc4)));
            } catch (Exception e2) {
            }
        }
    }

    @DexIgnore
    public boolean f0() {
        A94 a94 = this.t;
        return a94 != null && a94.a();
    }

    @DexIgnore
    public File[] h0() {
        return k0(y);
    }

    @DexIgnore
    public File[] i0() {
        LinkedList linkedList = new LinkedList();
        Collections.addAll(linkedList, j0(V(), z));
        Collections.addAll(linkedList, j0(Z(), z));
        Collections.addAll(linkedList, j0(W(), z));
        return (File[]) linkedList.toArray(new File[linkedList.size()]);
    }

    @DexIgnore
    public final File[] j0(File file, FilenameFilter filenameFilter) {
        return N(file.listFiles(filenameFilter));
    }

    @DexIgnore
    public final File[] k0(FilenameFilter filenameFilter) {
        return j0(W(), filenameFilter);
    }

    @DexIgnore
    public File[] l0() {
        return N(Y().listFiles());
    }

    @DexIgnore
    public File[] m0() {
        return k0(x);
    }

    @DexIgnore
    public final File[] n0(String str) {
        return k0(new e0(str));
    }

    @DexIgnore
    public final File[] o0() {
        File[] m0 = m0();
        Arrays.sort(m0, A);
        return m0;
    }

    @DexIgnore
    public final Nt3<Void> p0(long j2) {
        if (!Q()) {
            return Qt3.c(new ScheduledThreadPoolExecutor(1), new Ni(j2));
        }
        X74.f().b("Skipping logging Crashlytics event to Firebase, FirebaseCrash exists");
        return Qt3.f(null);
    }

    @DexIgnore
    public final Nt3<Void> q0() {
        ArrayList arrayList = new ArrayList();
        File[] h0 = h0();
        for (File file : h0) {
            try {
                arrayList.add(p0(Long.parseLong(file.getName().substring(3))));
            } catch (NumberFormatException e2) {
                X74.f().b("Could not parse timestamp from file " + file.getName());
            }
            file.delete();
        }
        return Qt3.g(arrayList);
    }

    @DexIgnore
    public void s0() {
        this.e.h(new Di());
    }

    @DexIgnore
    public final void t0(File[] fileArr, Set<String> set) {
        for (File file : fileArr) {
            String name = file.getName();
            Matcher matcher = C.matcher(name);
            if (!matcher.matches()) {
                X74.f().b("Deleting unknown file: " + name);
                file.delete();
            } else if (!set.contains(matcher.group(1))) {
                X74.f().b("Trimming session file: " + name);
                file.delete();
            }
        }
    }

    @DexIgnore
    public final void u0(Wc4 wc4, boolean z2) throws Exception {
        Context R = R();
        Bc4 a2 = this.j.a(wc4);
        File[] i0 = i0();
        for (File file : i0) {
            x(wc4.e, file);
            this.e.g(new d0(R, new Fc4(file, D), a2, z2));
        }
    }

    @DexIgnore
    public void v0(String str, String str2) {
        try {
            this.d.d(str, str2);
            z(this.d.a());
        } catch (IllegalArgumentException e2) {
            Context context = this.a;
            if (context == null || !R84.A(context)) {
                X74.f().d("Attempting to set custom attribute with null key, ignoring.");
                return;
            }
            throw e2;
        }
    }

    @DexIgnore
    public void w0(String str) {
        this.d.e(str);
        A(this.d);
    }

    @DexIgnore
    public Nt3<Void> x0(float f2, Nt3<Wc4> nt3) {
        if (!this.m.a()) {
            X74.f().b("No reports are available.");
            this.u.e(Boolean.FALSE);
            return Qt3.f(null);
        }
        X74.f().b("Unsent reports are available.");
        return C0().r(new Ui(nt3, f2));
    }

    @DexIgnore
    public final void y0(File file, String str, File[] fileArr, File file2) {
        Throwable th;
        Wb4 wb4;
        Wb4 wb42;
        Xb4 xb4;
        Wb4 wb43;
        Exception e2;
        Xb4 xb42;
        Xb4 xb43 = null;
        boolean z2 = file2 != null;
        File V = z2 ? V() : Z();
        if (!V.exists()) {
            V.mkdirs();
        }
        try {
            wb43 = new Wb4(V, str);
            try {
                xb42 = Xb4.z(wb43);
                try {
                    X74.f().b("Collecting SessionStart data for session ID " + str);
                    P0(xb42, file);
                    xb42.g0(4, U());
                    xb42.C(5, z2);
                    xb42.e0(11, 1);
                    xb42.H(12, 3);
                    G0(xb42, str);
                    H0(xb42, fileArr, str);
                    if (z2) {
                        P0(xb42, file2);
                    }
                    R84.j(xb42, "Error flushing session file stream");
                    R84.e(wb43, "Failed to close CLS file");
                } catch (Exception e3) {
                    e2 = e3;
                    try {
                        X74.f().e("Failed to write session file for session ID: " + str, e2);
                        R84.j(xb42, "Error flushing session file stream");
                        D(wb43);
                    } catch (Throwable th2) {
                        th = th2;
                        wb4 = wb43;
                        xb43 = xb42;
                        wb42 = wb4;
                        xb4 = xb43;
                        R84.j(xb4, "Error flushing session file stream");
                        R84.e(wb42, "Failed to close CLS file");
                        throw th;
                    }
                }
            } catch (Exception e4) {
                e2 = e4;
                xb42 = null;
                X74.f().e("Failed to write session file for session ID: " + str, e2);
                R84.j(xb42, "Error flushing session file stream");
                D(wb43);
            } catch (Throwable th3) {
                th = th3;
                wb4 = wb43;
                wb42 = wb4;
                xb4 = xb43;
                R84.j(xb4, "Error flushing session file stream");
                R84.e(wb42, "Failed to close CLS file");
                throw th;
            }
        } catch (Exception e5) {
            e2 = e5;
            xb42 = null;
            wb43 = null;
            X74.f().e("Failed to write session file for session ID: " + str, e2);
            R84.j(xb42, "Error flushing session file stream");
            D(wb43);
        } catch (Throwable th4) {
            wb42 = null;
            xb4 = null;
            th = th4;
            R84.j(xb4, "Error flushing session file stream");
            R84.e(wb42, "Failed to close CLS file");
            throw th;
        }
    }

    @DexIgnore
    public final void z(Map<String, String> map) {
        this.e.h(new Ci(map));
    }

    @DexIgnore
    public final void z0(int i2) {
        HashSet hashSet = new HashSet();
        File[] o0 = o0();
        int min = Math.min(i2, o0.length);
        for (int i3 = 0; i3 < min; i3++) {
            hashSet.add(a0(o0[i3]));
        }
        this.l.b(hashSet);
        t0(k0(new Wi(null)), hashSet);
    }
}
