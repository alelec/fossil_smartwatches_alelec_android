package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lm4 {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public Lm4(CharSequence charSequence, int i, int i2) {
        this.a = charSequence;
        this.c = i;
        this.b = i2;
        byte[] bArr = new byte[(i * i2)];
        this.d = bArr;
        Arrays.fill(bArr, (byte) -1);
    }

    @DexIgnore
    public final void a(int i) {
        g(this.b - 1, 0, i, 1);
        g(this.b - 1, 1, i, 2);
        g(this.b - 1, 2, i, 3);
        g(0, this.c - 2, i, 4);
        g(0, this.c - 1, i, 5);
        g(1, this.c - 1, i, 6);
        g(2, this.c - 1, i, 7);
        g(3, this.c - 1, i, 8);
    }

    @DexIgnore
    public final void b(int i) {
        g(this.b - 3, 0, i, 1);
        g(this.b - 2, 0, i, 2);
        g(this.b - 1, 0, i, 3);
        g(0, this.c - 4, i, 4);
        g(0, this.c - 3, i, 5);
        g(0, this.c - 2, i, 6);
        g(0, this.c - 1, i, 7);
        g(1, this.c - 1, i, 8);
    }

    @DexIgnore
    public final void c(int i) {
        g(this.b - 3, 0, i, 1);
        g(this.b - 2, 0, i, 2);
        g(this.b - 1, 0, i, 3);
        g(0, this.c - 2, i, 4);
        g(0, this.c - 1, i, 5);
        g(1, this.c - 1, i, 6);
        g(2, this.c - 1, i, 7);
        g(3, this.c - 1, i, 8);
    }

    @DexIgnore
    public final void d(int i) {
        g(this.b - 1, 0, i, 1);
        g(this.b - 1, this.c - 1, i, 2);
        g(0, this.c - 3, i, 3);
        g(0, this.c - 2, i, 4);
        g(0, this.c - 1, i, 5);
        g(1, this.c - 3, i, 6);
        g(1, this.c - 2, i, 7);
        g(1, this.c - 1, i, 8);
    }

    @DexIgnore
    public final boolean e(int i, int i2) {
        return this.d[(this.c * i2) + i] == 1;
    }

    @DexIgnore
    public final boolean f(int i, int i2) {
        return this.d[(this.c * i2) + i] >= 0;
    }

    @DexIgnore
    public final void g(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        boolean z = true;
        if (i < 0) {
            int i7 = this.b;
            i6 = i + i7;
            i5 = (4 - ((i7 + 4) % 8)) + i2;
        } else {
            i5 = i2;
            i6 = i;
        }
        if (i5 < 0) {
            int i8 = this.c;
            i5 += i8;
            i6 += 4 - ((i8 + 4) % 8);
        }
        if ((this.a.charAt(i3) & (1 << (8 - i4))) == 0) {
            z = false;
        }
        i(i5, i6, z);
    }

    @DexIgnore
    public final void h() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int i5 = 4;
        int i6 = 0;
        while (true) {
            int i7 = i4;
            if (i5 == this.b && i7 == 0) {
                a(i6);
                i6++;
            }
            if (i5 == this.b - 2 && i7 == 0 && this.c % 4 != 0) {
                b(i6);
                i6++;
            }
            if (i5 == this.b - 2 && i7 == 0 && this.c % 8 == 4) {
                c(i6);
                i6++;
            }
            if (i5 == this.b + 4 && i7 == 2 && this.c % 8 == 0) {
                d(i6);
                i6++;
            }
            do {
                if (i5 < this.b && i7 >= 0 && !f(i7, i5)) {
                    j(i5, i7, i6);
                    i6++;
                }
                i5 -= 2;
                i7 += 2;
                if (i5 < 0) {
                    break;
                }
            } while (i7 < this.c);
            int i8 = i5 + 1;
            int i9 = i7 + 3;
            int i10 = i8;
            while (true) {
                if (i10 >= 0 && i9 < this.c && !f(i9, i10)) {
                    j(i10, i9, i6);
                    i6++;
                }
                i10 += 2;
                i = i9 - 2;
                if (i10 >= this.b || i < 0) {
                    i5 = i10 + 3;
                    i4 = i + 1;
                    i2 = this.b;
                } else {
                    i9 = i;
                }
            }
            i5 = i10 + 3;
            i4 = i + 1;
            i2 = this.b;
            if (i5 >= i2 && i4 >= (i3 = this.c)) {
                break;
            }
        }
        if (!f(i3 - 1, i2 - 1)) {
            i(this.c - 1, this.b - 1, true);
            i(this.c - 2, this.b - 2, true);
        }
    }

    @DexIgnore
    public final void i(int i, int i2, boolean z) {
        this.d[(this.c * i2) + i] = (byte) (z ? (byte) 1 : 0);
    }

    @DexIgnore
    public final void j(int i, int i2, int i3) {
        int i4 = i - 2;
        int i5 = i2 - 2;
        g(i4, i5, i3, 1);
        int i6 = i2 - 1;
        g(i4, i6, i3, 2);
        int i7 = i - 1;
        g(i7, i5, i3, 3);
        g(i7, i6, i3, 4);
        g(i7, i2, i3, 5);
        g(i, i5, i3, 6);
        g(i, i6, i3, 7);
        g(i, i2, i3, 8);
    }
}
