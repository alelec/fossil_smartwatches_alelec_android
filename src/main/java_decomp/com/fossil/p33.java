package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P33 implements Comparable<P33>, Map.Entry<K, V> {
    @DexIgnore
    public /* final */ K b;
    @DexIgnore
    public V c;
    @DexIgnore
    public /* final */ /* synthetic */ G33 d;

    @DexIgnore
    public P33(G33 g33, K k, V v) {
        this.d = g33;
        this.b = k;
        this.c = v;
    }

    @DexIgnore
    public P33(G33 g33, Map.Entry<K, V> entry) {
        this(g33, entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public final /* synthetic */ int compareTo(P33 p33) {
        return ((Comparable) getKey()).compareTo((Comparable) p33.getKey());
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return a(this.b, entry.getKey()) && a(this.c, entry.getValue());
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final /* synthetic */ Object getKey() {
        return this.b;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V getValue() {
        return this.c;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        K k = this.b;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.c;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    @Override // java.util.Map.Entry
    public final V setValue(V v) {
        G33.g(this.d);
        V v2 = this.c;
        this.c = v;
        return v2;
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.b);
        String valueOf2 = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        return sb.toString();
    }
}
