package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X97 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Fb7> a; // = new ArrayList();
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public /* final */ G77 c;

    @DexIgnore
    public X97(E77 e77) {
        Wg6.c(e77, "templateListener");
        this.c = new G77(1, e77);
    }

    @DexIgnore
    public final void g(int i) {
        if (i != this.b) {
            this.c.a(i);
            notifyItemChanged(this.b);
            notifyItemChanged(i);
            this.b = i;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.c.c(this.a, i)) {
            return this.c.b();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    public final void h(List<Fb7> list) {
        Wg6.c(list, "data");
        if (!Wg6.a(this.a, list)) {
            this.a.clear();
            this.a.addAll(list);
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        if (getItemViewType(i) == this.c.b()) {
            this.c.d(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == this.c.b()) {
            return this.c.e(viewGroup);
        }
        throw new IllegalArgumentException("No delegate for this viewtype : " + i);
    }
}
