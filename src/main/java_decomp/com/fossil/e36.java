package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E36 implements Factory<NotificationWatchRemindersPresenter> {
    @DexIgnore
    public static NotificationWatchRemindersPresenter a(Y26 y26, An4 an4, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new NotificationWatchRemindersPresenter(y26, an4, remindersSettingsDatabase);
    }
}
