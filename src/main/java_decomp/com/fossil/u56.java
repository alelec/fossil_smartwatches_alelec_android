package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U56 implements Factory<P56> {
    @DexIgnore
    public static P56 a(R56 r56) {
        P56 d = r56.d();
        Lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
