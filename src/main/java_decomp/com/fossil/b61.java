package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.fossil.E61;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B61 implements E61<Drawable> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ V51 b;

    @DexIgnore
    public B61(Context context, V51 v51) {
        Wg6.c(context, "context");
        Wg6.c(v51, "drawableDecoder");
        this.a = context;
        this.b = v51;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(Drawable drawable) {
        return e(drawable);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Drawable drawable) {
        return f(drawable);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, Drawable drawable, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, drawable, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, Drawable drawable, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        boolean o = W81.o(drawable);
        if (o) {
            Bitmap a2 = this.b.a(drawable, f81, x51.d());
            Resources resources = this.a.getResources();
            Wg6.b(resources, "context.resources");
            drawable = new BitmapDrawable(resources, a2);
        }
        return new C61(drawable, o, Q51.MEMORY);
    }

    @DexIgnore
    public boolean e(Drawable drawable) {
        Wg6.c(drawable, "data");
        return E61.Ai.a(this, drawable);
    }

    @DexIgnore
    public String f(Drawable drawable) {
        Wg6.c(drawable, "data");
        return null;
    }
}
