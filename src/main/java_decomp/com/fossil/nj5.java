package com.fossil;

import com.mapped.Cj4;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nj5 implements Factory<Cj4> {
    @DexIgnore
    public /* final */ Provider<GetDianaDeviceSettingUseCase> a;
    @DexIgnore
    public /* final */ Provider<GetHybridDeviceSettingUseCase> b;
    @DexIgnore
    public /* final */ Provider<HybridSyncUseCase> c;
    @DexIgnore
    public /* final */ Provider<DianaSyncUseCase> d;

    @DexIgnore
    public Nj5(Provider<GetDianaDeviceSettingUseCase> provider, Provider<GetHybridDeviceSettingUseCase> provider2, Provider<HybridSyncUseCase> provider3, Provider<DianaSyncUseCase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static Nj5 a(Provider<GetDianaDeviceSettingUseCase> provider, Provider<GetHybridDeviceSettingUseCase> provider2, Provider<HybridSyncUseCase> provider3, Provider<DianaSyncUseCase> provider4) {
        return new Nj5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static Cj4 c(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, HybridSyncUseCase hybridSyncUseCase, DianaSyncUseCase dianaSyncUseCase) {
        return new Cj4(getDianaDeviceSettingUseCase, getHybridDeviceSettingUseCase, hybridSyncUseCase, dianaSyncUseCase);
    }

    @DexIgnore
    public Cj4 b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
