package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A23 extends V13 {
    @DexIgnore
    public A23() {
        super();
    }

    @DexIgnore
    public static <E> M13<E> e(Object obj, long j) {
        return (M13) E43.F(obj, j);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.util.List] */
    @Override // com.fossil.V13
    public final <E> void b(Object obj, Object obj2, long j) {
        M13<E> e = e(obj, j);
        M13<E> e2 = e(obj2, j);
        int size = e.size();
        int size2 = e2.size();
        M13<E> m13 = e;
        m13 = e;
        if (size > 0 && size2 > 0) {
            boolean zza = e.zza();
            M13<E> m132 = e;
            if (!zza) {
                m132 = e.zza(size2 + size);
            }
            m132.addAll(e2);
            m13 = m132;
        }
        if (size <= 0) {
            m13 = e2;
        }
        E43.j(obj, j, m13);
    }

    @DexIgnore
    @Override // com.fossil.V13
    public final void d(Object obj, long j) {
        e(obj, j).zzb();
    }
}
