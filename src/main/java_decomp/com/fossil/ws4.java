package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ws4 {
    @DexIgnore
    @Vu3("notificationId")
    public String a;
    @DexIgnore
    @Vu3("title-loc-key")
    public String b;
    @DexIgnore
    @Vu3("createdAt")
    public Date c;
    @DexIgnore
    @Vu3("loc-key")
    public String d;
    @DexIgnore
    @Vu3("challenge")
    public Ss4 e;
    @DexIgnore
    @Vu3("profile")
    public Lt4 f;
    @DexIgnore
    @Vu3("confirm")
    public Boolean g;
    @DexIgnore
    @Vu3("rank")
    public Integer h;

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final Ss4 b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final Lt4 d() {
        return this.f;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ws4) {
                Ws4 ws4 = (Ws4) obj;
                if (!Wg6.a(this.a, ws4.a) || !Wg6.a(this.b, ws4.b) || !Wg6.a(this.c, ws4.c) || !Wg6.a(this.d, ws4.d) || !Wg6.a(this.e, ws4.e) || !Wg6.a(this.f, ws4.f) || !Wg6.a(this.g, ws4.g) || !Wg6.a(this.h, ws4.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        Date date = this.c;
        int hashCode3 = date != null ? date.hashCode() : 0;
        String str3 = this.d;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        Ss4 ss4 = this.e;
        int hashCode5 = ss4 != null ? ss4.hashCode() : 0;
        Lt4 lt4 = this.f;
        int hashCode6 = lt4 != null ? lt4.hashCode() : 0;
        Boolean bool = this.g;
        int hashCode7 = bool != null ? bool.hashCode() : 0;
        Integer num = this.h;
        if (num != null) {
            i = num.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "FCMNotificationData(id=" + this.a + ", titleLoc=" + this.b + ", createdAt=" + this.c + ", bodyLoc=" + this.d + ", challenge=" + this.e + ", profile=" + this.f + ", confirm=" + this.g + ", rank=" + this.h + ")";
    }
}
