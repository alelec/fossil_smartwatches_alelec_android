package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.S87;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleProgressBar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W97 extends RecyclerView.g<Ai> {
    @DexIgnore
    public List<S87.Bi> a; // = Hm7.e();
    @DexIgnore
    public /* final */ Hg6<S87.Bi, Cd6> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Yf5 a;
        @DexIgnore
        public /* final */ /* synthetic */ W97 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ S87.Bi c;

            @DexIgnore
            public Aii(Ai ai, S87.Bi bi) {
                this.b = ai;
                this.c = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Hg6 hg6;
                if (this.c.e() && (hg6 = this.b.b.b) != null) {
                    Cd6 cd6 = (Cd6) hg6.invoke(S87.Bi.g(this.c, null, null, null, null, 0, 0, 63, null));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(W97 w97, Yf5 yf5) {
            super(yf5.b());
            Wg6.c(yf5, "binding");
            this.b = w97;
            this.a = yf5;
        }

        @DexIgnore
        public final void a(S87.Bi bi) {
            Wg6.c(bi, "stickerConfig");
            if (bi.e()) {
                ImageView imageView = this.a.c;
                Wg6.b(imageView, "binding.stickerImage");
                Bitmap k = bi.k();
                A51 b2 = X41.b();
                Context context = imageView.getContext();
                Wg6.b(context, "context");
                U71 u71 = new U71(context, b2.a());
                u71.x(k);
                u71.z(imageView);
                b2.b(u71.w());
                ImageView imageView2 = this.a.c;
                Wg6.b(imageView2, "binding.stickerImage");
                imageView2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = this.a.b;
                Wg6.b(flexibleProgressBar, "binding.progressBar");
                flexibleProgressBar.setVisibility(8);
            } else {
                ImageView imageView3 = this.a.c;
                Wg6.b(imageView3, "binding.stickerImage");
                imageView3.setVisibility(8);
                FlexibleProgressBar flexibleProgressBar2 = this.a.b;
                Wg6.b(flexibleProgressBar2, "binding.progressBar");
                flexibleProgressBar2.setVisibility(0);
            }
            this.a.b().setOnClickListener(new Aii(this, bi));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Hg6<? super com.fossil.S87$Bi, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public W97(Hg6<? super S87.Bi, Cd6> hg6) {
        this.b = hg6;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void h(Ai ai, int i) {
        Wg6.c(ai, "holder");
        ai.a(this.a.get(i));
    }

    @DexIgnore
    public Ai i(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Yf5 c = Yf5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(c, "ItemStickerPreviewBindin\u2026tInflater, parent, false)");
        return new Ai(this, c);
    }

    @DexIgnore
    public final void j(List<S87.Bi> list) {
        Wg6.c(list, "stickerList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        h(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return i(viewGroup, i);
    }
}
