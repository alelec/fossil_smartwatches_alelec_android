package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.Pc2;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zh2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Zh2> CREATOR; // = new Ci2();
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ Ii2 h;
    @DexIgnore
    public /* final */ Long i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public long a; // = 0;
        @DexIgnore
        public long b; // = 0;
        @DexIgnore
        public String c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public String e; // = "";
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public Long g;

        @DexIgnore
        public Zh2 a() {
            boolean z = true;
            Rc2.o(this.a > 0, "Start time should be specified.");
            long j = this.b;
            if (j != 0 && j <= this.a) {
                z = false;
            }
            Rc2.o(z, "End time should be later than start time.");
            if (this.d == null) {
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                long j2 = this.a;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 20);
                sb.append(str);
                sb.append(j2);
                this.d = sb.toString();
            }
            return new Zh2(this);
        }

        @DexIgnore
        public Ai b(String str) {
            this.f = Hp2.a(str);
            return this;
        }

        @DexIgnore
        public Ai c(String str) {
            Rc2.c(str.length() <= 1000, "Session description cannot exceed %d characters", 1000);
            this.e = str;
            return this;
        }

        @DexIgnore
        public Ai d(long j, TimeUnit timeUnit) {
            Rc2.o(j >= 0, "End time should be positive.");
            this.b = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public Ai e(String str) {
            Rc2.a(str != null && TextUtils.getTrimmedLength(str) > 0);
            this.d = str;
            return this;
        }

        @DexIgnore
        public Ai f(String str) {
            Rc2.c(str.length() <= 100, "Session name cannot exceed %d characters", 100);
            this.c = str;
            return this;
        }

        @DexIgnore
        public Ai g(long j, TimeUnit timeUnit) {
            Rc2.o(j > 0, "Start time should be positive.");
            this.a = timeUnit.toMillis(j);
            return this;
        }
    }

    @DexIgnore
    public Zh2(long j, long j2, String str, String str2, String str3, int i2, Ii2 ii2, Long l) {
        this.b = j;
        this.c = j2;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = i2;
        this.h = ii2;
        this.i = l;
    }

    @DexIgnore
    public Zh2(Ai ai) {
        this(ai.a, ai.b, ai.c, ai.d, ai.e, ai.f, null, ai.g);
    }

    @DexIgnore
    public long A(TimeUnit timeUnit) {
        return timeUnit.convert(this.b, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public String c() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Zh2)) {
            return false;
        }
        Zh2 zh2 = (Zh2) obj;
        return this.b == zh2.b && this.c == zh2.c && Pc2.a(this.d, zh2.d) && Pc2.a(this.e, zh2.e) && Pc2.a(this.f, zh2.f) && Pc2.a(this.h, zh2.h) && this.g == zh2.g;
    }

    @DexIgnore
    public long f(TimeUnit timeUnit) {
        return timeUnit.convert(this.c, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public String h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(Long.valueOf(this.b), Long.valueOf(this.c), this.e);
    }

    @DexIgnore
    public String k() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a(SampleRaw.COLUMN_START_TIME, Long.valueOf(this.b));
        c2.a(SampleRaw.COLUMN_END_TIME, Long.valueOf(this.c));
        c2.a("name", this.d);
        c2.a("identifier", this.e);
        c2.a("description", this.f);
        c2.a(Constants.ACTIVITY, Integer.valueOf(this.g));
        c2.a("application", this.h);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.r(parcel, 1, this.b);
        Bd2.r(parcel, 2, this.c);
        Bd2.u(parcel, 3, k(), false);
        Bd2.u(parcel, 4, h(), false);
        Bd2.u(parcel, 5, c(), false);
        Bd2.n(parcel, 7, this.g);
        Bd2.t(parcel, 8, this.h, i2, false);
        Bd2.s(parcel, 9, this.i, false);
        Bd2.b(parcel, a2);
    }
}
