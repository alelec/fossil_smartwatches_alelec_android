package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ RecyclerView v;

    @DexIgnore
    public T35(Object obj, View view, int i, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, RTLImageView rTLImageView2, View view2, ConstraintLayout constraintLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = rTLImageView;
        this.s = rTLImageView2;
        this.t = view2;
        this.u = constraintLayout;
        this.v = recyclerView;
    }
}
