package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W36 implements Factory<V36> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ W36 a; // = new W36();
    }

    @DexIgnore
    public static W36 a() {
        return Ai.a;
    }

    @DexIgnore
    public static V36 c() {
        return new V36();
    }

    @DexIgnore
    public V36 b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
