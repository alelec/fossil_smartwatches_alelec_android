package com.fossil;

import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ww4 implements Factory<BCFindFriendsViewModel> {
    @DexIgnore
    public /* final */ Provider<FriendRepository> a;
    @DexIgnore
    public /* final */ Provider<Tt4> b;

    @DexIgnore
    public Ww4(Provider<FriendRepository> provider, Provider<Tt4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Ww4 a(Provider<FriendRepository> provider, Provider<Tt4> provider2) {
        return new Ww4(provider, provider2);
    }

    @DexIgnore
    public static BCFindFriendsViewModel c(FriendRepository friendRepository, Tt4 tt4) {
        return new BCFindFriendsViewModel(friendRepository, tt4);
    }

    @DexIgnore
    public BCFindFriendsViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
