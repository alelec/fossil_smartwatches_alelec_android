package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.TextView;
import com.facebook.LegacyTokenHelper;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Um5 {
    /*
    static {
        String str = "Localization_" + Um5.class.getSimpleName();
    }
    */

    @DexIgnore
    public static Locale a() {
        return Resources.getSystem().getConfiguration().locale;
    }

    @DexIgnore
    public static String b() {
        return Resources.getSystem().getConfiguration().locale.toString();
    }

    @DexIgnore
    public static String c(Context context, int i) {
        if (context == null || i == 0) {
            return "";
        }
        try {
            String resourceEntryName = context.getResources().getResourceEntryName(i);
            if (TextUtils.isEmpty(resourceEntryName)) {
                return "";
            }
            String f = Tm5.d().f(resourceEntryName);
            return (f == null || f.isEmpty()) ? context.getResources().getString(i) : f;
        } catch (Resources.NotFoundException e) {
            return "";
        }
    }

    @DexIgnore
    public static String d(Context context, String str, String str2) {
        if (context == null || TextUtils.isEmpty(str)) {
            return str2;
        }
        String f = Tm5.d().f(str);
        if (f == null || f.isEmpty()) {
            try {
                int identifier = context.getResources().getIdentifier(str, LegacyTokenHelper.TYPE_STRING, context.getPackageName());
                if (identifier == 0) {
                    return str2;
                }
                f = context.getResources().getString(identifier);
            } catch (Exception e) {
                return str2;
            }
        }
        return f;
    }

    @DexIgnore
    public static void e(TextView textView, int i) {
        if (textView != null && i != 0) {
            textView.setText(c(textView.getContext(), i));
        }
    }
}
