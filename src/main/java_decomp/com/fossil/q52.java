package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q52 implements Parcelable.Creator<SignInAccount> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SignInAccount createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = "";
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 4) {
                str = Ad2.f(parcel, t);
            } else if (l == 7) {
                googleSignInAccount = (GoogleSignInAccount) Ad2.e(parcel, t, GoogleSignInAccount.CREATOR);
            } else if (l != 8) {
                Ad2.B(parcel, t);
            } else {
                str2 = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new SignInAccount(str, googleSignInAccount, str2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ SignInAccount[] newArray(int i) {
        return new SignInAccount[i];
    }
}
