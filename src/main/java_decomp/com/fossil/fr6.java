package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fr6 extends BaseFragment implements X47, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public CustomizeActiveCaloriesChartViewModel h;
    @DexIgnore
    public G37<J45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Fr6.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<CustomizeActiveCaloriesChartViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Fr6 a;

        @DexIgnore
        public Bi(Fr6 fr6) {
            this.a = fr6;
        }

        @DexIgnore
        public final void a(CustomizeActiveCaloriesChartViewModel.Ai ai) {
            if (ai != null) {
                Integer a2 = ai.a();
                if (a2 != null) {
                    this.a.N6(a2.intValue());
                }
                Integer b = ai.b();
                if (b != null) {
                    this.a.M6(b.intValue());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CustomizeActiveCaloriesChartViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fr6 b;

        @DexIgnore
        public Ci(Fr6 fr6) {
            this.b = fr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 503);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fr6 b;

        @DexIgnore
        public Di(Fr6 fr6) {
            this.b = fr6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = Fr6.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeActiveCaloriesC\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.X47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        Hr7 hr7 = Hr7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.h;
        if (customizeActiveCaloriesChartViewModel != null) {
            customizeActiveCaloriesChartViewModel.h(i2, Color.parseColor(format));
            if (i2 == 503) {
                l = format;
                return;
            }
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void L6() {
        OverviewDayChart overviewDayChart;
        G37<J45> g37 = this.i;
        if (g37 != null) {
            J45 a2 = g37.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 10, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c = Hm7.c(arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 214, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c2 = Hm7.c(arrayList2);
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 24, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c3 = Hm7.c(arrayList3);
            ArrayList arrayList4 = new ArrayList();
            arrayList4.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 20, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c4 = Hm7.c(arrayList4);
            ArrayList arrayList5 = new ArrayList();
            arrayList5.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 6, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c5 = Hm7.c(arrayList5);
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 100, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c6 = Hm7.c(arrayList6);
            ArrayList arrayList7 = new ArrayList();
            arrayList7.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 250, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c7 = Hm7.c(arrayList7);
            ArrayList arrayList8 = new ArrayList();
            arrayList8.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 280, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c8 = Hm7.c(arrayList8);
            ArrayList arrayList9 = new ArrayList();
            arrayList9.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 272, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c9 = Hm7.c(arrayList9);
            ArrayList arrayList10 = new ArrayList();
            arrayList10.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 79, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c10 = Hm7.c(arrayList10);
            ArrayList arrayList11 = new ArrayList();
            arrayList11.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 10, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c11 = Hm7.c(arrayList11);
            ArrayList arrayList12 = new ArrayList();
            arrayList12.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 5, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c12 = Hm7.c(arrayList12);
            ArrayList arrayList13 = new ArrayList();
            arrayList13.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 82, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c13 = Hm7.c(arrayList13);
            ArrayList arrayList14 = new ArrayList();
            arrayList14.add(new BarChart.b(-1, BarChart.e.DEFAULT, 0, 5, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c14 = Hm7.c(arrayList14);
            ArrayList arrayList15 = new ArrayList();
            arrayList15.add(new BarChart.a(-1, c, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c2, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c3, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c4, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c5, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c6, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c7, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c8, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c9, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c10, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c11, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c12, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c13, -1, false, 8, null));
            arrayList15.add(new BarChart.a(-1, c14, -1, false, 8, null));
            BarChart.c cVar = new BarChart.c(SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, 150, arrayList15);
            if (a2 != null && (overviewDayChart = a2.r) != null) {
                overviewDayChart.I(cVar);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void M6(int i2) {
        G37<J45> g37 = this.i;
        if (g37 != null) {
            J45 a2 = g37.a();
            if (a2 != null) {
                a2.r.setGraphPreviewColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void N6(int i2) {
        G37<J45> g37 = this.i;
        if (g37 != null) {
            J45 a2 = g37.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.h;
            if (customizeActiveCaloriesChartViewModel != null) {
                customizeActiveCaloriesChartViewModel.f(UserCustomizeThemeFragment.m.a(), l);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        J45 j45 = (J45) Aq0.f(LayoutInflater.from(getContext()), 2131558529, null, false, A6());
        PortfolioApp.get.instance().getIface().Y(new Hr6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CustomizeActiveCaloriesChartViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = (CustomizeActiveCaloriesChartViewModel) a2;
            this.h = customizeActiveCaloriesChartViewModel;
            if (customizeActiveCaloriesChartViewModel != null) {
                customizeActiveCaloriesChartViewModel.e().h(getViewLifecycleOwner(), new Bi(this));
                CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel2 = this.h;
                if (customizeActiveCaloriesChartViewModel2 != null) {
                    customizeActiveCaloriesChartViewModel2.g();
                    this.i = new G37<>(this, j45);
                    L6();
                    Wg6.b(j45, "binding");
                    return j45.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.h;
        if (customizeActiveCaloriesChartViewModel != null) {
            customizeActiveCaloriesChartViewModel.g();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<J45> g37 = this.i;
        if (g37 != null) {
            J45 a2 = g37.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new Ci(this));
                a2.s.setOnClickListener(new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
