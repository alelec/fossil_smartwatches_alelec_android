package com.fossil;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wh1 implements Di1 {
    @DexIgnore
    public /* final */ Set<Ei1> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    @Override // com.fossil.Di1
    public void a(Ei1 ei1) {
        this.a.add(ei1);
        if (this.c) {
            ei1.onDestroy();
        } else if (this.b) {
            ei1.onStart();
        } else {
            ei1.onStop();
        }
    }

    @DexIgnore
    @Override // com.fossil.Di1
    public void b(Ei1 ei1) {
        this.a.remove(ei1);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (Ei1 ei1 : Jk1.j(this.a)) {
            ei1.onDestroy();
        }
    }

    @DexIgnore
    public void d() {
        this.b = true;
        for (Ei1 ei1 : Jk1.j(this.a)) {
            ei1.onStart();
        }
    }

    @DexIgnore
    public void e() {
        this.b = false;
        for (Ei1 ei1 : Jk1.j(this.a)) {
            ei1.onStop();
        }
    }
}
