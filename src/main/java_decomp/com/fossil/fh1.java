package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.Bb1;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fh1 implements Qb1<ByteBuffer, Hh1> {
    @DexIgnore
    public static /* final */ Ai f; // = new Ai();
    @DexIgnore
    public static /* final */ Bi g; // = new Bi();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ Bi c;
    @DexIgnore
    public /* final */ Ai d;
    @DexIgnore
    public /* final */ Gh1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Bb1 a(Bb1.Ai ai, Db1 db1, ByteBuffer byteBuffer, int i) {
            return new Fb1(ai, db1, byteBuffer, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Queue<Eb1> a; // = Jk1.f(0);

        @DexIgnore
        public Eb1 a(ByteBuffer byteBuffer) {
            Eb1 poll;
            synchronized (this) {
                poll = this.a.poll();
                if (poll == null) {
                    poll = new Eb1();
                }
                poll.p(byteBuffer);
            }
            return poll;
        }

        @DexIgnore
        public void b(Eb1 eb1) {
            synchronized (this) {
                eb1.a();
                this.a.offer(eb1);
            }
        }
    }

    @DexIgnore
    public Fh1(Context context, List<ImageHeaderParser> list, Rd1 rd1, Od1 od1) {
        this(context, list, rd1, od1, g, f);
    }

    @DexIgnore
    public Fh1(Context context, List<ImageHeaderParser> list, Rd1 rd1, Od1 od1, Bi bi, Ai ai) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = ai;
        this.e = new Gh1(rd1, od1);
        this.c = bi;
    }

    @DexIgnore
    public static int e(Db1 db1, int i, int i2) {
        int min = Math.min(db1.a() / i2, db1.d() / i);
        int max = Math.max(1, min == 0 ? 0 : Integer.highestOneBit(min));
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + db1.d() + "x" + db1.a() + "]");
        }
        return max;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(ByteBuffer byteBuffer, Ob1 ob1) throws IOException {
        return f(byteBuffer, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Hh1> b(ByteBuffer byteBuffer, int i, int i2, Ob1 ob1) throws IOException {
        return d(byteBuffer, i, i2, ob1);
    }

    @DexIgnore
    public final Jh1 c(ByteBuffer byteBuffer, int i, int i2, Eb1 eb1, Ob1 ob1) {
        long b2 = Ek1.b();
        try {
            Db1 c2 = eb1.c();
            if (c2.b() <= 0 || c2.c() != 0) {
            }
            Bitmap.Config config = ob1.c(Nh1.a) == Hb1.PREFER_RGB_565 ? Bitmap.Config.RGB_565 : Bitmap.Config.ARGB_8888;
            Bb1 a2 = this.d.a(this.e, c2, byteBuffer, e(c2, i, i2));
            a2.e(config);
            a2.b();
            Bitmap a3 = a2.a();
            if (a3 != null) {
                Jh1 jh1 = new Jh1(new Hh1(this.a, a2, Tf1.c(), i, i2, a3));
                if (Log.isLoggable("BufferGifDecoder", 2)) {
                    Log.v("BufferGifDecoder", "Decoded GIF from stream in " + Ek1.a(b2));
                }
                return jh1;
            } else if (!Log.isLoggable("BufferGifDecoder", 2)) {
                return null;
            } else {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + Ek1.a(b2));
                return null;
            }
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + Ek1.a(b2));
            }
        }
    }

    @DexIgnore
    public Jh1 d(ByteBuffer byteBuffer, int i, int i2, Ob1 ob1) {
        Eb1 a2 = this.c.a(byteBuffer);
        try {
            return c(byteBuffer, i, i2, a2, ob1);
        } finally {
            this.c.b(a2);
        }
    }

    @DexIgnore
    public boolean f(ByteBuffer byteBuffer, Ob1 ob1) throws IOException {
        return !((Boolean) ob1.c(Nh1.b)).booleanValue() && Lb1.f(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }
}
