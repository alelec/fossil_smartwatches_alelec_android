package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Vg3 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ U93 d;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 e;

    @DexIgnore
    public Pp3(Fp3 fp3, Vg3 vg3, String str, U93 u93) {
        this.e = fp3;
        this.b = vg3;
        this.c = str;
        this.d = u93;
    }

    @DexIgnore
    public final void run() {
        try {
            Cl3 cl3 = this.e.d;
            if (cl3 == null) {
                this.e.d().F().a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            byte[] w2 = cl3.w2(this.b, this.c);
            this.e.e0();
            this.e.k().T(this.d, w2);
        } catch (RemoteException e2) {
            this.e.d().F().b("Failed to send event to the service to bundle", e2);
        } finally {
            this.e.k().T(this.d, null);
        }
    }
}
