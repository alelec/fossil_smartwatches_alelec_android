package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qq3 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ Ng3 c; // = new Tq3(this, this.d.a);
    @DexIgnore
    public /* final */ /* synthetic */ Jq3 d;

    @DexIgnore
    public Qq3(Jq3 jq3) {
        this.d = jq3;
        long c2 = jq3.zzm().c();
        this.a = c2;
        this.b = c2;
    }

    @DexIgnore
    public final void a() {
        this.c.e();
        this.a = 0;
        this.b = 0;
    }

    @DexIgnore
    public final void b(long j) {
        this.d.h();
        this.c.e();
        this.a = j;
        this.b = j;
    }

    @DexIgnore
    public final boolean d(boolean z, boolean z2, long j) {
        this.d.h();
        this.d.x();
        if (!L63.a() || !this.d.m().s(Xg3.A0)) {
            j = this.d.zzm().c();
        }
        if (!W63.a() || !this.d.m().s(Xg3.w0) || this.d.a.o()) {
            this.d.l().u.b(this.d.zzm().b());
        }
        long j2 = j - this.a;
        if (z || j2 >= 1000) {
            if (this.d.m().s(Xg3.U) && !z2) {
                j2 = (!X63.a() || !this.d.m().s(Xg3.W) || !L63.a() || !this.d.m().s(Xg3.A0)) ? e() : g(j);
            }
            this.d.d().N().b("Recording user engagement, ms", Long.valueOf(j2));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j2);
            Ap3.L(this.d.s().D(!this.d.m().K().booleanValue()), bundle, true);
            if (this.d.m().s(Xg3.U) && !this.d.m().s(Xg3.V) && z2) {
                bundle.putLong("_fr", 1);
            }
            if (!this.d.m().s(Xg3.V) || !z2) {
                this.d.p().Q("auto", "_e", bundle);
            }
            this.a = j;
            this.c.e();
            this.c.c(3600000);
            return true;
        }
        this.d.d().N().b("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
        return false;
    }

    @DexIgnore
    public final long e() {
        long c2 = this.d.zzm().c();
        long j = this.b;
        this.b = c2;
        return c2 - j;
    }

    @DexIgnore
    public final void f(long j) {
        this.c.e();
    }

    @DexIgnore
    public final long g(long j) {
        long j2 = this.b;
        this.b = j;
        return j - j2;
    }

    @DexIgnore
    public final void h() {
        this.d.h();
        d(false, false, this.d.zzm().c());
        this.d.o().v(this.d.zzm().c());
    }
}
