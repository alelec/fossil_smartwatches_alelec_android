package com.fossil;

import android.content.Context;
import com.fossil.K21;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H21 implements K21.Ai {
    @DexIgnore
    public static /* final */ String d; // = X01.f("WorkConstraintsTracker");
    @DexIgnore
    public /* final */ G21 a;
    @DexIgnore
    public /* final */ K21<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public H21(Context context, K41 k41, G21 g21) {
        Context applicationContext = context.getApplicationContext();
        this.a = g21;
        this.b = new K21[]{new I21(applicationContext, k41), new J21(applicationContext, k41), new P21(applicationContext, k41), new L21(applicationContext, k41), new O21(applicationContext, k41), new N21(applicationContext, k41), new M21(applicationContext, k41)};
    }

    @DexIgnore
    @Override // com.fossil.K21.Ai
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String str : list) {
                if (c(str)) {
                    X01.c().a(d, String.format("Constraints met for %s", str), new Throwable[0]);
                    arrayList.add(str);
                }
            }
            if (this.a != null) {
                this.a.f(arrayList);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.K21.Ai
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.a != null) {
                this.a.b(list);
            }
        }
    }

    @DexIgnore
    public boolean c(String str) {
        synchronized (this.c) {
            K21<?>[] k21Arr = this.b;
            for (K21<?> k21 : k21Arr) {
                if (k21.d(str)) {
                    X01.c().a(d, String.format("Work %s constrained by %s", str, k21.getClass().getSimpleName()), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void d(Iterable<O31> iterable) {
        synchronized (this.c) {
            for (K21<?> k21 : this.b) {
                k21.g(null);
            }
            for (K21<?> k212 : this.b) {
                k212.e(iterable);
            }
            for (K21<?> k213 : this.b) {
                k213.g(this);
            }
        }
    }

    @DexIgnore
    public void e() {
        synchronized (this.c) {
            for (K21<?> k21 : this.b) {
                k21.f();
            }
        }
    }
}
