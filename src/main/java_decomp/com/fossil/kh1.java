package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kh1 implements Sb1<Hh1> {
    @DexIgnore
    public /* final */ Sb1<Bitmap> b;

    @DexIgnore
    public Kh1(Sb1<Bitmap> sb1) {
        Ik1.d(sb1);
        this.b = sb1;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.Sb1
    public Id1<Hh1> b(Context context, Id1<Hh1> id1, int i, int i2) {
        Hh1 hh1 = id1.get();
        Id1<Bitmap> yf1 = new Yf1(hh1.e(), Oa1.c(context).f());
        Id1<Bitmap> b2 = this.b.b(context, yf1, i, i2);
        if (!yf1.equals(b2)) {
            yf1.b();
        }
        hh1.m(this.b, b2.get());
        return id1;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (obj instanceof Kh1) {
            return this.b.equals(((Kh1) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        return this.b.hashCode();
    }
}
