package com.fossil;

import com.fossil.Ta4$d$d$a$b$e;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oa4 extends Ta4$d$d$a$b$e.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$e.b.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b a() {
            String str = "";
            if (this.a == null) {
                str = " pc";
            }
            if (this.b == null) {
                str = str + " symbol";
            }
            if (this.d == null) {
                str = str + " offset";
            }
            if (this.e == null) {
                str = str + " importance";
            }
            if (str.isEmpty()) {
                return new Oa4(this.a.longValue(), this.b, this.c, this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b.a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b.a c(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b.a d(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b.a e(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.d.d.a.b.e.b.a
        public Ta4$d$d$a$b$e.b.a f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null symbol");
        }
    }

    @DexIgnore
    public Oa4(long j, String str, String str2, long j2, int i) {
        this.a = j;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e.b
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e.b
    public int c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e.b
    public long d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e.b
    public long e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4$d$d$a$b$e.b)) {
            return false;
        }
        Ta4$d$d$a$b$e.b bVar = (Ta4$d$d$a$b$e.b) obj;
        return this.a == bVar.e() && this.b.equals(bVar.f()) && ((str = this.c) != null ? str.equals(bVar.b()) : bVar.b() == null) && this.d == bVar.d() && this.e == bVar.c();
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$e.b
    public String f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int i = (int) (j ^ (j >>> 32));
        int hashCode = this.b.hashCode();
        String str = this.c;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j2 = this.d;
        return ((((hashCode2 ^ ((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003)) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.e;
    }

    @DexIgnore
    public String toString() {
        return "Frame{pc=" + this.a + ", symbol=" + this.b + ", file=" + this.c + ", offset=" + this.d + ", importance=" + this.e + "}";
    }
}
