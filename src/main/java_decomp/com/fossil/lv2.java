package com.fossil;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lv2 extends ContentObserver {
    @DexIgnore
    public /* final */ /* synthetic */ Jv2 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Lv2(Jv2 jv2, Handler handler) {
        super(null);
        this.a = jv2;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.a.c();
    }
}
