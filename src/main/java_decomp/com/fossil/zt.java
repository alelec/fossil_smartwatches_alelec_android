package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Zt {
    c((byte) 3),
    d((byte) 9),
    e((byte) 10),
    f((byte) 12),
    g(DateTimeFieldType.SECOND_OF_MINUTE),
    h(DateTimeFieldType.MILLIS_OF_SECOND),
    i((byte) 241),
    j((byte) 242);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Zt(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
