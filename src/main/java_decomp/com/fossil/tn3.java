package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Tn3 implements Runnable {
    @DexIgnore
    public /* final */ Un3 b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public Tn3(Un3 un3, Bundle bundle) {
        this.b = un3;
        this.c = bundle;
    }

    @DexIgnore
    public final void run() {
        Un3 un3 = this.b;
        Bundle bundle = this.c;
        if (J73.a() && un3.m().s(Xg3.N0)) {
            if (bundle == null) {
                un3.l().C.b(new Bundle());
                return;
            }
            Bundle a2 = un3.l().C.a();
            for (String str : bundle.keySet()) {
                Object obj = bundle.get(str);
                if (obj != null && !(obj instanceof String) && !(obj instanceof Long) && !(obj instanceof Double)) {
                    un3.k();
                    if (Kr3.b0(obj)) {
                        un3.k().I(27, null, null, 0);
                    }
                    un3.d().K().c("Invalid default event parameter type. Name, value", str, obj);
                } else if (Kr3.B0(str)) {
                    un3.d().K().b("Invalid default event parameter name. Name", str);
                } else if (obj == null) {
                    a2.remove(str);
                } else if (un3.k().g0("param", str, 100, obj)) {
                    un3.k().M(a2, str, obj);
                }
            }
            un3.k();
            if (Kr3.Z(a2, un3.m().z())) {
                un3.k().I(26, null, null, 0);
                un3.d().K().a("Too many default event parameters set. Discarding beyond event parameter limit");
            }
            un3.l().C.b(a2);
            un3.r().F(a2);
        }
    }
}
