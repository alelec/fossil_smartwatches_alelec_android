package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Kr4 {
    BUDDY_CHALLENGE("buddy_challenge"),
    COMMUTE_TIME("commute_time");
    
    @DexIgnore
    public /* final */ String strType;

    @DexIgnore
    public Kr4(String str) {
        this.strType = str;
    }

    @DexIgnore
    public final String getStrType() {
        return this.strType;
    }
}
