package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ze1<A, B> {
    @DexIgnore
    public /* final */ Fk1<Bi<A>, B> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Fk1<Bi<A>, B> {
        @DexIgnore
        public Ai(Ze1 ze1, long j) {
            super(j);
        }

        @DexIgnore
        @Override // com.fossil.Fk1
        public /* bridge */ /* synthetic */ void j(Object obj, Object obj2) {
            n((Bi) obj, obj2);
        }

        @DexIgnore
        public void n(Bi<A> bi, B b) {
            bi.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<A> {
        @DexIgnore
        public static /* final */ Queue<Bi<?>> d; // = Jk1.f(0);
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public A c;

        @DexIgnore
        public static <A> Bi<A> a(A a2, int i, int i2) {
            Bi<A> bi;
            synchronized (d) {
                bi = (Bi<A>) d.poll();
            }
            if (bi == null) {
                bi = new Bi<>();
            }
            bi.b(a2, i, i2);
            return bi;
        }

        @DexIgnore
        public final void b(A a2, int i, int i2) {
            this.c = a2;
            this.b = i;
            this.a = i2;
        }

        @DexIgnore
        public void c() {
            synchronized (d) {
                d.offer(this);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Bi)) {
                return false;
            }
            Bi bi = (Bi) obj;
            return this.b == bi.b && this.a == bi.a && this.c.equals(bi.c);
        }

        @DexIgnore
        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
        }
    }

    @DexIgnore
    public Ze1(long j) {
        this.a = new Ai(this, j);
    }

    @DexIgnore
    public B a(A a2, int i, int i2) {
        Bi<A> a3 = Bi.a(a2, i, i2);
        B g = this.a.g(a3);
        a3.c();
        return g;
    }

    @DexIgnore
    public void b(A a2, int i, int i2, B b) {
        this.a.k(Bi.a(a2, i, i2), b);
    }
}
