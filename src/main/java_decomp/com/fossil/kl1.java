package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.K70;
import com.mapped.Qg6;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kl1 extends Hl1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ K70 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final LinkedHashSet<K70> a(K70 k70) {
            LinkedHashSet<K70> linkedHashSet = new LinkedHashSet<>();
            if (k70 == null) {
                Mm7.t(linkedHashSet, K70.values());
            } else {
                linkedHashSet.add(k70);
            }
            return linkedHashSet;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kl1 createFromParcel(Parcel parcel) {
            return (Kl1) Hl1.CREATOR.b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kl1[] newArray(int i) {
            return new Kl1[i];
        }
    }

    @DexIgnore
    public Kl1(byte b, byte b2, K70 k70) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(k70), false, false, true);
        this.i = k70;
    }

    @DexIgnore
    public final K70 getDayOfWeek() {
        return this.i;
    }
}
