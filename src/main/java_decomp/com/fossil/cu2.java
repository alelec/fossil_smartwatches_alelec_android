package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cu2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ R93 g;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2.Bi h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Cu2(Zs2.Bi bi, Activity activity, R93 r93) {
        super(Zs2.this);
        this.h = bi;
        this.f = activity;
        this.g = r93;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        Zs2.this.h.onActivitySaveInstanceState(Tg2.n(this.f), this.g, this.c);
    }
}
