package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tl extends Lp {
    @DexIgnore
    public Se C;
    @DexIgnore
    public /* final */ ArrayList<Ow> D; // = By1.a(this.i, Hm7.c(Ow.c));
    @DexIgnore
    public /* final */ Ve E;

    @DexIgnore
    public Tl(K5 k5, I60 i60, Ve ve, String str) {
        super(k5, i60, Yp.k, str, false, 16);
        this.E = ve;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Su(this.w), new Vj(this), new Hk(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.m4, this.E.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.n4;
        Se se = this.C;
        return G80.k(E2, jd0, se != null ? se.toJSONObject() : null);
    }

    @DexIgnore
    public final Se I() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        Se se = this.C;
        return se != null ? se : new Se(0, 0, 0);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.D;
    }
}
