package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.watchface.MetaData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P77 {
    @DexIgnore
    @Vu3("id")
    public String a;
    @DexIgnore
    @Vu3("category")
    public String b;
    @DexIgnore
    @Vu3("name")
    public String c;
    @DexIgnore
    @Vu3("data")
    public Q77 d;
    @DexIgnore
    @Vu3("_isNew")
    public boolean e;
    @DexIgnore
    @Vu3("metadata")
    public MetaData f;
    @DexIgnore
    public L77 g;

    @DexIgnore
    public P77(String str, String str2, String str3, Q77 q77, boolean z, MetaData metaData, L77 l77) {
        Wg6.c(str, "id");
        Wg6.c(str2, "category");
        Wg6.c(str3, "name");
        Wg6.c(q77, "data");
        Wg6.c(l77, "assetType");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = q77;
        this.e = z;
        this.f = metaData;
        this.g = l77;
    }

    @DexIgnore
    public final L77 a() {
        return this.g;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final Q77 c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final MetaData e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof P77) {
                P77 p77 = (P77) obj;
                if (!Wg6.a(this.a, p77.a) || !Wg6.a(this.b, p77.b) || !Wg6.a(this.c, p77.c) || !Wg6.a(this.d, p77.d) || this.e != p77.e || !Wg6.a(this.f, p77.f) || !Wg6.a(this.g, p77.g)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final boolean g() {
        return this.e;
    }

    @DexIgnore
    public final void h(L77 l77) {
        Wg6.c(l77, "<set-?>");
        this.g = l77;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Q77 q77 = this.d;
        int hashCode4 = q77 != null ? q77.hashCode() : 0;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        MetaData metaData = this.f;
        int hashCode5 = metaData != null ? metaData.hashCode() : 0;
        L77 l77 = this.g;
        if (l77 != null) {
            i = l77.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WFAsset(id=" + this.a + ", category=" + this.b + ", name=" + this.c + ", data=" + this.d + ", isNew=" + this.e + ", meta=" + this.f + ", assetType=" + this.g + ")";
    }
}
