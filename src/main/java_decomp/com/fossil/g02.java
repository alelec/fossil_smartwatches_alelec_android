package com.fossil;

import com.fossil.Wz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class G02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract G02 a();

        @DexIgnore
        public abstract Ai b(Ty1 ty1);

        @DexIgnore
        public abstract Ai c(Uy1<?> uy1);

        @DexIgnore
        public abstract Ai d(Wy1<?, byte[]> wy1);

        @DexIgnore
        public abstract Ai e(H02 h02);

        @DexIgnore
        public abstract Ai f(String str);
    }

    @DexIgnore
    public static Ai a() {
        return new Wz1.Bi();
    }

    @DexIgnore
    public abstract Ty1 b();

    @DexIgnore
    public abstract Uy1<?> c();

    @DexIgnore
    public byte[] d() {
        return e().apply(c().b());
    }

    @DexIgnore
    public abstract Wy1<?, byte[]> e();

    @DexIgnore
    public abstract H02 f();

    @DexIgnore
    public abstract String g();
}
