package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Au1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Au1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Au1 createFromParcel(Parcel parcel) {
            return new Au1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Au1[] newArray(int i) {
            return new Au1[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Au1(android.os.Parcel r4, com.mapped.Qg6 r5) {
        /*
            r3 = this;
            r2 = 0
            java.lang.Class<com.fossil.Tq1> r0 = com.fossil.Tq1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r4.readParcelable(r0)
            if (r0 == 0) goto L_0x002a
            java.lang.String r1 = "parcel.readParcelable<Wo\u2026class.java.classLoader)!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Tq1 r0 = (com.fossil.Tq1) r0
            java.lang.Class<com.fossil.Nt1> r1 = com.fossil.Nt1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r4.readParcelable(r1)
            if (r1 == 0) goto L_0x0026
            com.fossil.Nt1 r1 = (com.fossil.Nt1) r1
            r3.<init>(r0, r1)
            return
        L_0x0026:
            com.mapped.Wg6.i()
            throw r2
        L_0x002a:
            com.mapped.Wg6.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Au1.<init>(android.os.Parcel, com.mapped.Qg6):void");
    }

    @DexIgnore
    public Au1(Tq1 tq1, Nt1 nt1) {
        super(tq1, nt1);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            Nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    D90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = Hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            Wg6.i();
            throw null;
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
    }
}
