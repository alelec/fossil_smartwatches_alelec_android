package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T08 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public T08(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public String toString() {
        return "Empty[" + this.a + ']';
    }
}
