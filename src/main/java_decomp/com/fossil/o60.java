package com.fossil;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.DeviceImplementation$applyTheme$1", f = "DeviceImplementation.kt", l = {FailureCode.FAILED_TO_SENDING_ENCRYPTED_DATA_SESSION}, m = "invokeSuspend")
public final class o60 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ e60 e;
    @DexIgnore
    public /* final */ /* synthetic */ kw1 f;
    @DexIgnore
    public /* final */ /* synthetic */ oy1 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o60(e60 e60, kw1 kw1, oy1 oy1, qn7 qn7) {
        super(2, qn7);
        this.e = e60;
        this.f = kw1;
        this.g = oy1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        o60 o60 = new o60(this.e, this.f, this.g, qn7);
        o60.b = (iv7) obj;
        throw null;
        //return o60;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        throw null;
        //return ((o60) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        Object d2 = yn7.d();
        int i = this.d;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.b;
            nc0 nc0 = nc0.f2498a;
            ec0 g2 = this.f.g();
            ry1 uiPackageOSVersion = this.e.u.getUiPackageOSVersion();
            this.c = iv7;
            this.d = 1;
            a2 = nc0.a(g2, uiPackageOSVersion, true, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.c;
            el7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        o oVar = (o) a2;
        V v = oVar.f2599a;
        if (v != null) {
            lw1 a3 = this.f.a(v);
            if (a3 != null) {
                this.e.k(a3).s(new m40(this)).r(new e50(this)).w(new w50(this));
            } else {
                this.g.n(new bl1(cl1.UNKNOWN_ERROR, null, 2));
            }
        } else {
            oy1 oy1 = this.g;
            bl1 bl1 = oVar.b;
            if (bl1 == null) {
                bl1 = new bl1(cl1.UNKNOWN_ERROR, null, 2);
            }
            oy1.n(bl1);
        }
        return tl7.f3441a;
    }
}
