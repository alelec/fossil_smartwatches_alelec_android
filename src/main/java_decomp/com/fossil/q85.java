package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q85 extends P85 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362056, 1);
        B.put(2131363030, 2);
        B.put(2131363045, 3);
        B.put(2131363391, 4);
        B.put(2131363365, 5);
        B.put(2131363374, 6);
        B.put(2131363364, 7);
        B.put(2131363363, 8);
    }
    */

    @DexIgnore
    public Q85(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public Q85(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ScrollView) objArr[0], (RecyclerView) objArr[2], (RecyclerView) objArr[3], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4]);
        this.z = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
