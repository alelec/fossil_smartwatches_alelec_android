package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.N04;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Px6 extends Ey6 {
    @DexIgnore
    public static /* final */ Ai z; // = new Ai(null);
    @DexIgnore
    public Hy6 h;
    @DexIgnore
    public Py6 i;
    @DexIgnore
    public G37<V95> j;
    @DexIgnore
    public Yq4 k; // = new Yq4();
    @DexIgnore
    public String l; // = "";
    @DexIgnore
    public List<String> m; // = new ArrayList();
    @DexIgnore
    public int s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public /* final */ String v; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String w; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String x; // = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Px6 a(List<String> list, boolean z) {
            Wg6.c(list, "instructionList");
            Px6 px6 = new Px6();
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            px6.setArguments(bundle);
            px6.m = list;
            px6.s = 0;
            return px6;
        }

        @DexIgnore
        public final Px6 b(String str, boolean z) {
            Wg6.c(str, "serial");
            Px6 px6 = new Px6();
            px6.l = str;
            px6.s = 1;
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            px6.setArguments(bundle);
            return px6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Px6 a;

        @DexIgnore
        public Bi(Px6 px6) {
            this.a = px6;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.v) && !TextUtils.isEmpty(this.a.w)) {
                int parseColor = Color.parseColor(this.a.v);
                int parseColor2 = Color.parseColor(this.a.w);
                gVar.o(2131230966);
                if (i == this.a.u) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ V95 a;
        @DexIgnore
        public /* final */ /* synthetic */ Px6 b;

        @DexIgnore
        public Ci(V95 v95, Px6 px6) {
            this.a = v95;
            this.b = px6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            Drawable e;
            Drawable e2;
            if (i == 0) {
                FlexibleTextView flexibleTextView = this.a.w;
                Wg6.b(flexibleTextView, "binding.ftvTimeout");
                flexibleTextView.setVisibility(0);
            } else {
                FlexibleTextView flexibleTextView2 = this.a.w;
                Wg6.b(flexibleTextView2, "binding.ftvTimeout");
                flexibleTextView2.setVisibility(4);
            }
            if (!TextUtils.isEmpty(this.b.w)) {
                int parseColor = Color.parseColor(this.b.w);
                TabLayout.g v = this.a.z.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.v) && this.b.u != i) {
                int parseColor2 = Color.parseColor(this.b.v);
                TabLayout.g v2 = this.a.z.v(this.b.u);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.u = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Px6 b;

        @DexIgnore
        public Di(Px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Px6.M6(this.b).n();
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Px6 b;

        @DexIgnore
        public Ei(Px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Context context = this.b.getContext();
            if (context != null) {
                Wg6.b(context, "context!!");
                aVar.b(context, this.b.l, true, this.b.t);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Px6 b;

        @DexIgnore
        public Fi(Px6 px6) {
            this.b = px6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                Wg6.b(activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = this.b.getActivity();
                    if (activity2 != null) {
                        Wg6.b(activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = this.b.getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ Hy6 M6(Px6 px6) {
        Hy6 hy6 = px6.h;
        if (hy6 != null) {
            return hy6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        W6((Hy6) obj);
    }

    @DexIgnore
    public final void U6(List<String> list) {
        Wg6.c(list, "instructionList");
        this.k.i(list);
        G37<V95> g37 = this.j;
        if (g37 != null) {
            V95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                Wg6.b(constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.q;
                Wg6.b(constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6(String str) {
        Wg6.c(str, "serial");
        this.l = str;
        G37<V95> g37 = this.j;
        if (g37 != null) {
            V95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                Wg6.b(constraintLayout, "it.clPairingAuthorize");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.q;
                Wg6.b(constraintLayout2, "it.clAuthorizeFailed");
                constraintLayout2.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void W6(Hy6 hy6) {
        Wg6.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    public final void X6(boolean z2) {
        Py6 py6 = this.i;
        if (py6 != null) {
            py6.c(z2);
        } else {
            Wg6.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void Y6(int i2) {
        G37<V95> g37 = this.j;
        if (g37 != null) {
            V95 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "it.ftvTimeout");
                Hr7 hr7 = Hr7.a;
                Context context = getContext();
                if (context != null) {
                    String c = Um5.c(context, 2131886892);
                    Wg6.b(c, "LanguageHelper.getString\u2026_ExpiringInSecondSeconds)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    return;
                }
                Wg6.i();
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        G37<V95> g37 = new G37<>(this, (V95) Aq0.f(layoutInflater, 2131558602, viewGroup, false, A6()));
        this.j = g37;
        if (g37 != null) {
            V95 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        Py6 py6 = this.i;
        if (py6 != null) {
            py6.b();
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.n("mSubPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        this.k.i(this.m);
        this.i = new Py6(this);
        Bundle arguments = getArguments();
        this.t = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        G37<V95> g37 = this.j;
        if (g37 != null) {
            V95 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.C;
                Wg6.b(viewPager2, "binding.vpAuthorizeInstruction");
                viewPager2.setAdapter(this.k);
                if (!TextUtils.isEmpty(this.x)) {
                    TabLayout tabLayout = a2.z;
                    Wg6.b(tabLayout, "binding.indicator");
                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.x)));
                }
                G37<V95> g372 = this.j;
                if (g372 != null) {
                    V95 a3 = g372.a();
                    TabLayout tabLayout2 = a3 != null ? a3.z : null;
                    if (tabLayout2 != null) {
                        G37<V95> g373 = this.j;
                        if (g373 != null) {
                            V95 a4 = g373.a();
                            ViewPager2 viewPager22 = a4 != null ? a4.C : null;
                            if (viewPager22 != null) {
                                new N04(tabLayout2, viewPager22, new Bi(this)).a();
                                a2.C.g(new Ci(a2, this));
                                a2.s.setOnClickListener(new Di(this));
                                a2.v.setOnClickListener(new Ei(this));
                                a2.t.setOnClickListener(new Fi(this));
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.n("mBinding");
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            }
            int i2 = this.s;
            if (i2 == 0) {
                U6(this.m);
            } else if (i2 == 1) {
                V6(this.l);
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
