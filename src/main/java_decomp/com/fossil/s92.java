package com.fossil;

import com.fossil.M62;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface S92 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    boolean c();

    @DexIgnore
    void f(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean g(T72 t72);

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t);

    @DexIgnore
    <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t);

    @DexIgnore
    Object l();  // void declaration

    @DexIgnore
    Z52 m();
}
