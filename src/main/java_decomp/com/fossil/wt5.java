package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wt5 implements Factory<UnlinkDeviceUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> f;

    @DexIgnore
    public Wt5(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5, Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static Wt5 a(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5, Provider<com.portfolio.platform.preset.data.source.DianaPresetRepository> provider6) {
        return new Wt5(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static UnlinkDeviceUseCase c(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository dianaPresetRepository2) {
        return new UnlinkDeviceUseCase(deviceRepository, hybridPresetRepository, dianaPresetRepository, portfolioApp, watchFaceRepository, dianaPresetRepository2);
    }

    @DexIgnore
    public UnlinkDeviceUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
