package com.fossil;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lx2 extends AbstractSet<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ Hx2 b;

    @DexIgnore
    public Lx2(Hx2 hx2) {
        this.b = hx2;
    }

    @DexIgnore
    public final void clear() {
        this.b.clear();
    }

    @DexIgnore
    public final boolean contains(@NullableDecl Object obj) {
        Map zzb = this.b.zzb();
        if (zzb != null) {
            return zzb.entrySet().contains(obj);
        }
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            int i = this.b.b(entry.getKey());
            return i != -1 && Qw2.a(this.b.zzc[i], entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return this.b.zzf();
    }

    @DexIgnore
    public final boolean remove(@NullableDecl Object obj) {
        Map zzb = this.b.zzb();
        if (zzb != null) {
            return zzb.entrySet().remove(obj);
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (this.b.zza()) {
            return false;
        }
        int i = this.b.f();
        Object key = entry.getKey();
        Object value = entry.getValue();
        Object obj2 = this.b.b;
        Hx2 hx2 = this.b;
        int c = Ox2.c(key, value, i, obj2, hx2.zza, hx2.zzb, hx2.zzc);
        if (c == -1) {
            return false;
        }
        this.b.zza(c, i);
        Hx2.zzd(this.b);
        this.b.zzc();
        return true;
    }

    @DexIgnore
    public final int size() {
        return this.b.size();
    }
}
