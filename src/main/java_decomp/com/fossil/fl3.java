package com.fossil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fl3 extends SQLiteOpenHelper {
    @DexIgnore
    public /* final */ /* synthetic */ Gl3 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Fl3(Gl3 gl3, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        this.b = gl3;
    }

    @DexIgnore
    public final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        try {
            return super.getWritableDatabase();
        } catch (SQLiteDatabaseLockedException e) {
            throw e;
        } catch (SQLiteException e2) {
            this.b.d().F().a("Opening the local database failed, dropping and recreating it");
            if (!this.b.e().getDatabasePath("google_app_measurement_local.db").delete()) {
                this.b.d().F().b("Failed to delete corrupted local db file", "google_app_measurement_local.db");
            }
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException e3) {
                this.b.d().F().b("Failed to open local database. Events will bypass local storage", e3);
                return null;
            }
        }
    }

    @DexIgnore
    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        Og3.b(this.b.d(), sQLiteDatabase);
    }

    @DexIgnore
    public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    @DexIgnore
    public final void onOpen(SQLiteDatabase sQLiteDatabase) {
        Og3.c(this.b.d(), sQLiteDatabase, GraphRequest.DEBUG_MESSAGES_KEY, "create table if not exists messages ( type INTEGER NOT NULL, entry BLOB NOT NULL)", "type,entry", null);
    }

    @DexIgnore
    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
