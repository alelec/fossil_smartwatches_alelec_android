package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3649a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ tt4 e;
    @DexIgnore
    public /* final */ zt4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel", f = "BCMemberInChallengeViewModel.kt", l = {82, 82}, m = "loadInvitedPlayers")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(uv4 uv4, qn7 qn7) {
            super(qn7);
            this.this$0 = uv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super rv7<? extends cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uv4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uv4$b$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCMemberInChallengeViewModel.kt", l = {91}, m = "invokeSuspend")
            /* renamed from: com.fossil.uv4$b$a$a  reason: collision with other inner class name */
            public static final class C0252a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0252a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0252a aVar = new C0252a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
                    throw null;
                    //return ((C0252a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        tt4 tt4 = this.this$0.this$0.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = iv7;
                            this.label = 1;
                            Object t = tt4.t(str, new String[]{"waiting", "running", "completed", "left_after_start"}, this);
                            return t == d ? d : t;
                        }
                        pq7.i();
                        throw null;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uv4$b$a$b")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCMemberInChallengeViewModel.kt", l = {86}, m = "invokeSuspend")
            /* renamed from: com.fossil.uv4$b$a$b  reason: collision with other inner class name */
            public static final class C0253b extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0253b(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0253b bVar = new C0253b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
                    throw null;
                    //return ((C0253b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object t;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        b bVar = this.this$0.this$0;
                        if (!bVar.$isWaiting) {
                            return new kz4(hm7.e(), null, 2, null);
                        }
                        tt4 tt4 = bVar.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = iv7;
                            this.label = 1;
                            t = tt4.t(str, new String[]{"invited", "invitation_expired"}, this);
                            if (t == d) {
                                return d;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        t = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (kz4) t;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    return hl7.a(gu7.b(iv7, null, null, new C0253b(this, null), 3, null), gu7.b(iv7, null, null, new C0252a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uv4 uv4, boolean z, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = uv4;
            this.$isWaiting = z;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$isWaiting, this.$challengeId, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super rv7<? extends cl7<? extends rv7<? extends kz4<List<? extends ms4>>>, ? extends rv7<? extends kz4<List<? extends ms4>>>>>> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return gu7.b(this.p$, bw7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1", f = "BCMemberInChallengeViewModel.kt", l = {38, 39, 40, 42, 45}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uv4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $any;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $joined;
            @DexIgnore
            public /* final */ /* synthetic */ List $pending;
            @DexIgnore
            public /* final */ /* synthetic */ kz4 $resultJoined;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, List list2, List list3, List list4, kz4 kz4, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$joined = list;
                this.$friends = list2;
                this.$pending = list3;
                this.$any = list4;
                this.$resultJoined = kz4;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$joined, this.$friends, this.$pending, this.$any, this.$resultJoined, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    List list = this.$joined;
                    if (list != null) {
                        cl7<List<at4>, List<at4>> j = py4.j(this.$friends, list);
                        List<at4> first = j.getFirst();
                        List<at4> second = j.getSecond();
                        List list2 = this.$pending;
                        List<at4> h = list2 != null ? py4.h(list2) : null;
                        if (!first.isEmpty()) {
                            this.$any.add(jl5.b.p(first.size()));
                            this.$any.addAll(first);
                        }
                        if (!second.isEmpty()) {
                            this.$any.add(jl5.b.q(second.size()));
                            this.$any.addAll(second);
                        }
                        if (h != null && (!h.isEmpty())) {
                            this.$any.add(jl5.b.r(h.size()));
                            this.$any.addAll(h);
                        }
                        this.this$0.this$0.c.l(this.$any);
                    } else if (this.this$0.this$0.c.e() == null) {
                        this.this$0.this$0.d.l(hl7.a(ao7.a(true), this.$resultJoined.a()));
                    } else {
                        this.this$0.this$0.d.l(hl7.a(ao7.a(false), this.$resultJoined.a()));
                    }
                    this.this$0.this$0.b.l(ao7.a(false));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$friends$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<xs4>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.f.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uv4 uv4, String str, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = uv4;
            this.$challengeId = str;
            this.$isWaiting = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, this.$isWaiting, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v21, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r1v10, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r2v15, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00fa  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0121  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0154  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x015a  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0160  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 359
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uv4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public uv4(tt4 tt4, zt4 zt4) {
        pq7.c(tt4, "challengeRepository");
        pq7.c(zt4, "friendRepository");
        this.e = tt4;
        this.f = zt4;
        String simpleName = uv4.class.getSimpleName();
        pq7.b(simpleName, "BCMemberInChallengeViewM\u2026el::class.java.simpleName");
        this.f3649a = simpleName;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> f() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> g() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> h() {
        LiveData<List<Object>> a2 = ss0.a(this.c);
        pq7.b(a2, "Transformations.distinctUntilChanged(this)");
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(java.lang.String r8, boolean r9, com.fossil.qn7<? super com.fossil.cl7<? extends com.fossil.rv7<com.fossil.kz4<java.util.List<com.fossil.ms4>>>, ? extends com.fossil.rv7<com.fossil.kz4<java.util.List<com.fossil.ms4>>>>> r10) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.fossil.uv4.a
            if (r0 == 0) goto L_0x0031
            r0 = r10
            com.fossil.uv4$a r0 = (com.fossil.uv4.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0031
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x0061
            if (r0 == r5) goto L_0x0040
            if (r0 != r6) goto L_0x0038
            boolean r0 = r3.Z$0
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.fossil.uv4 r0 = (com.fossil.uv4) r0
            com.fossil.el7.b(r2)
        L_0x0030:
            return r2
        L_0x0031:
            com.fossil.uv4$a r0 = new com.fossil.uv4$a
            r0.<init>(r7, r10)
            r3 = r0
            goto L_0x0015
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            boolean r9 = r3.Z$0
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.fossil.uv4 r1 = (com.fossil.uv4) r1
            com.fossil.el7.b(r2)
            r8 = r0
        L_0x004e:
            r0 = r2
            com.fossil.rv7 r0 = (com.fossil.rv7) r0
            r3.L$0 = r1
            r3.L$1 = r8
            r3.Z$0 = r9
            r3.label = r6
            java.lang.Object r2 = r0.l(r3)
            if (r2 != r4) goto L_0x0030
            r2 = r4
            goto L_0x0030
        L_0x0061:
            com.fossil.el7.b(r2)
            com.fossil.uv4$b r0 = new com.fossil.uv4$b
            r1 = 0
            r0.<init>(r7, r9, r8, r1)
            r3.L$0 = r7
            r3.L$1 = r8
            r3.Z$0 = r9
            r3.label = r5
            java.lang.Object r2 = com.fossil.ux7.c(r0, r3)
            if (r2 != r4) goto L_0x007a
            r2 = r4
            goto L_0x0030
        L_0x007a:
            r1 = r7
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uv4.i(java.lang.String, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void j(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.f3649a;
        local.e(str2, "loadMembers - challengeId: " + str + " - isWaiting: " + z);
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, z, null), 3, null);
    }
}
