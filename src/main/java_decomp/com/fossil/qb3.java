package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qb3 {
    @DexIgnore
    public /* final */ Zb3 a;
    @DexIgnore
    public Xb3 b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object onCancel();  // void declaration

        @DexIgnore
        Object onFinish();  // void declaration
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object onCameraIdle();  // void declaration
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Object onCameraMove();  // void declaration
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void onCameraMoveStarted(int i);
    }

    @DexIgnore
    public interface Ei {
        @DexIgnore
        void onCircleClick(De3 de3);
    }

    @DexIgnore
    public interface Fi {
        @DexIgnore
        void onInfoWindowClick(Ke3 ke3);
    }

    @DexIgnore
    public interface Gi {
        @DexIgnore
        void onMapClick(LatLng latLng);
    }

    @DexIgnore
    public interface Hi {
        @DexIgnore
        Object onMapLoaded();  // void declaration
    }

    @DexIgnore
    public interface Ii {
        @DexIgnore
        void onMapLongClick(LatLng latLng);
    }

    @DexIgnore
    public interface Ji {
        @DexIgnore
        boolean onMarkerClick(Ke3 ke3);
    }

    @DexIgnore
    public interface Ki {
        @DexIgnore
        void onMarkerDrag(Ke3 ke3);

        @DexIgnore
        void onMarkerDragEnd(Ke3 ke3);

        @DexIgnore
        void onMarkerDragStart(Ke3 ke3);
    }

    @DexIgnore
    public interface Li {
        @DexIgnore
        void onPolygonClick(Ne3 ne3);
    }

    @DexIgnore
    public interface Mi {
        @DexIgnore
        void onPolylineClick(Pe3 pe3);
    }

    @DexIgnore
    public interface Ni {
        @DexIgnore
        void onSnapshotReady(Bitmap bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi extends Ld3 {
        @DexIgnore
        public /* final */ Ai b;

        @DexIgnore
        public Oi(Ai ai) {
            this.b = ai;
        }

        @DexIgnore
        @Override // com.fossil.Kd3
        public final void onCancel() {
            this.b.onCancel();
        }

        @DexIgnore
        @Override // com.fossil.Kd3
        public final void onFinish() {
            this.b.onFinish();
        }
    }

    @DexIgnore
    public Qb3(Zb3 zb3) {
        Rc2.k(zb3);
        this.a = zb3;
    }

    @DexIgnore
    public final void A(Di di) {
        if (di == null) {
            try {
                this.a.B(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.B(new Yf3(this, di));
        }
    }

    @DexIgnore
    public final void B(Ei ei) {
        if (ei == null) {
            try {
                this.a.h2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.h2(new Uf3(this, ei));
        }
    }

    @DexIgnore
    public final void C(Fi fi) {
        if (fi == null) {
            try {
                this.a.J0(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.J0(new Sf3(this, fi));
        }
    }

    @DexIgnore
    public final void D(Gi gi) {
        if (gi == null) {
            try {
                this.a.H(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.H(new Bg3(this, gi));
        }
    }

    @DexIgnore
    public final void E(Hi hi) {
        if (hi == null) {
            try {
                this.a.X(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.X(new Tf3(this, hi));
        }
    }

    @DexIgnore
    public final void F(Ii ii) {
        if (ii == null) {
            try {
                this.a.H2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.H2(new Cg3(this, ii));
        }
    }

    @DexIgnore
    public final void G(Ji ji) {
        if (ji == null) {
            try {
                this.a.D2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.D2(new Qf3(this, ji));
        }
    }

    @DexIgnore
    public final void H(Ki ki) {
        if (ki == null) {
            try {
                this.a.R2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.R2(new Rf3(this, ki));
        }
    }

    @DexIgnore
    public final void I(Li li) {
        if (li == null) {
            try {
                this.a.S2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.S2(new Vf3(this, li));
        }
    }

    @DexIgnore
    public final void J(Mi mi) {
        if (mi == null) {
            try {
                this.a.R(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.R(new Wf3(this, mi));
        }
    }

    @DexIgnore
    public final void K(int i, int i2, int i3, int i4) {
        try {
            this.a.a0(i, i2, i3, i4);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void L(boolean z) {
        try {
            this.a.setTrafficEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void M(Ni ni) {
        N(ni, null);
    }

    @DexIgnore
    public final void N(Ni ni, Bitmap bitmap) {
        try {
            this.a.I0(new Xf3(this, ni), (Tg2) (bitmap != null ? Tg2.n(bitmap) : null));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final De3 a(Ee3 ee3) {
        try {
            return new De3(this.a.Q(ee3));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Ke3 b(Le3 le3) {
        try {
            Ls2 N2 = this.a.N2(le3);
            if (N2 != null) {
                return new Ke3(N2);
            }
            return null;
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Ne3 c(Oe3 oe3) {
        try {
            return new Ne3(this.a.w1(oe3));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Pe3 d(Qe3 qe3) {
        try {
            return new Pe3(this.a.x2(qe3));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void e(Ob3 ob3) {
        try {
            this.a.j2(ob3.a());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void f(Ob3 ob3, Ai ai) {
        try {
            this.a.U0(ob3.a(), ai == null ? null : new Oi(ai));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void g() {
        try {
            this.a.clear();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final CameraPosition h() {
        try {
            return this.a.p0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final float i() {
        try {
            return this.a.z2();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final float j() {
        try {
            return this.a.z();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Ub3 k() {
        try {
            return new Ub3(this.a.b2());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Xb3 l() {
        try {
            if (this.b == null) {
                this.b = new Xb3(this.a.F1());
            }
            return this.b;
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean m() {
        try {
            return this.a.N1();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean n() {
        try {
            return this.a.R0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void o(Ob3 ob3) {
        try {
            this.a.o0(ob3.a());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void p() {
        try {
            this.a.u1();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void q(boolean z) {
        try {
            this.a.setBuildingsEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean r(boolean z) {
        try {
            return this.a.setIndoorEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void s(LatLngBounds latLngBounds) {
        try {
            this.a.J(latLngBounds);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean t(Je3 je3) {
        try {
            return this.a.v0(je3);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void u(int i) {
        try {
            this.a.setMapType(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void v(float f) {
        try {
            this.a.Y0(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void w(float f) {
        try {
            this.a.e1(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void x(boolean z) {
        try {
            this.a.setMyLocationEnabled(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void y(Bi bi) {
        if (bi == null) {
            try {
                this.a.x1(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.x1(new Ag3(this, bi));
        }
    }

    @DexIgnore
    public final void z(Ci ci) {
        if (ci == null) {
            try {
                this.a.a2(null);
            } catch (RemoteException e) {
                throw new Se3(e);
            }
        } else {
            this.a.a2(new Zf3(this, ci));
        }
    }
}
