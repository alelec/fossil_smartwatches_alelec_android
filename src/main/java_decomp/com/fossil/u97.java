package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.s87;
import com.portfolio.platform.data.source.FileRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u97 extends hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<List<s87.b>> h;
    @DexIgnore
    public /* final */ LiveData<List<s87.b>> i;
    @DexIgnore
    public /* final */ MutableLiveData<u37<Boolean>> j;
    @DexIgnore
    public /* final */ LiveData<u37<Boolean>> k;
    @DexIgnore
    public /* final */ s77 l;
    @DexIgnore
    public /* final */ FileRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$1", f = "WatchFaceStickerViewModel.kt", l = {36}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ u97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(u97 u97, qn7 qn7) {
            super(2, qn7);
            this.this$0 = u97;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.h.l(pm7.h0(b97.c.f()));
                u97 u97 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (u97.r(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel$checkAndDownloadMissingResources$2", f = "WatchFaceStickerViewModel.kt", l = {43, 57, 60}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ u97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(u97 u97, qn7 qn7) {
            super(2, qn7);
            this.this$0 = u97;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v30, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00ec  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0189  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01a2  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x01a5  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 434
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.u97.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.edit.sticker.WatchFaceStickerViewModel", f = "WatchFaceStickerViewModel.kt", l = {67}, m = "loadMissingBitmap")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ u97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(u97 u97, qn7 qn7) {
            super(qn7);
            this.this$0 = u97;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore
    public u97(s77 s77, FileRepository fileRepository) {
        pq7.c(s77, "wfAssetRepository");
        pq7.c(fileRepository, "fileRepository");
        this.l = s77;
        this.m = fileRepository;
        MutableLiveData<List<s87.b>> mutableLiveData = new MutableLiveData<>();
        this.h = mutableLiveData;
        this.i = mutableLiveData;
        MutableLiveData<u37<Boolean>> mutableLiveData2 = new MutableLiveData<>(new u37(Boolean.FALSE));
        this.j = mutableLiveData2;
        this.k = mutableLiveData2;
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object r(qn7<? super tl7> qn7) {
        Object g = eu7.g(bw7.b(), new b(this, null), qn7);
        return g == yn7.d() ? g : tl7.f3441a;
    }

    @DexIgnore
    public final LiveData<u37<Boolean>> s() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<List<s87.b>> t() {
        return this.i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.u97.c
            if (r0 == 0) goto L_0x0095
            r0 = r7
            com.fossil.u97$c r0 = (com.fossil.u97.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0095
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x00a4
            if (r3 != r4) goto L_0x009c
            java.lang.Object r0 = r0.L$0
            com.fossil.u97 r0 = (com.fossil.u97) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Iterable r0 = (java.lang.Iterable) r0
            java.util.Iterator r2 = r0.iterator()
        L_0x002e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x00b7
            java.lang.Object r0 = r2.next()
            com.fossil.p77 r0 = (com.fossil.p77) r0
            com.fossil.q77 r1 = r0.c()
            java.lang.String r1 = r1.a()
            java.lang.String r1 = com.fossil.ym5.b(r1)
            com.portfolio.platform.data.source.FileRepository r3 = r6.m
            java.lang.String r4 = "fileName"
            com.fossil.pq7.b(r1, r4)
            com.misfit.frameworks.buttonservice.model.FileType r4 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            java.io.File r1 = r3.getFileByName(r1, r4)
            if (r1 == 0) goto L_0x00b5
            java.lang.String r1 = r1.getAbsolutePath()
        L_0x0059:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeFile(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "load missing bitmap, file="
            r4.append(r5)
            com.fossil.q77 r5 = r0.c()
            java.lang.String r5 = r5.a()
            r4.append(r5)
            java.lang.String r5 = ", bitmap="
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = "WatchFaceStickerViewModel"
            java.lang.String r4 = r4.toString()
            r3.d(r5, r4)
            if (r1 == 0) goto L_0x002e
            com.fossil.b97 r3 = com.fossil.b97.c
            java.lang.String r0 = r0.f()
            r3.a(r0, r1)
            goto L_0x002e
        L_0x0095:
            com.fossil.u97$c r0 = new com.fossil.u97$c
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x009c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x00a4:
            com.fossil.el7.b(r1)
            com.fossil.s77 r1 = r6.l
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.g(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
        L_0x00b4:
            return r0
        L_0x00b5:
            r1 = 0
            goto L_0x0059
        L_0x00b7:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u97.u(com.fossil.qn7):java.lang.Object");
    }
}
