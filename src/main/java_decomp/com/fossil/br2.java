package com.fossil;

import com.fossil.P72;
import com.google.android.gms.location.LocationAvailability;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Br2 implements P72.Bi<Yv2> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationAvailability a;

    @DexIgnore
    public Br2(Zq2 zq2, LocationAvailability locationAvailability) {
        this.a = locationAvailability;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.P72.Bi
    public final /* synthetic */ void a(Yv2 yv2) {
        yv2.onLocationAvailability(this.a);
    }

    @DexIgnore
    @Override // com.fossil.P72.Bi
    public final void b() {
    }
}
