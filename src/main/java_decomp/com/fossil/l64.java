package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.Pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L64 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public L64(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        Rc2.o(!Of2.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static L64 a(Context context) {
        Xc2 xc2 = new Xc2(context);
        String a2 = xc2.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new L64(a2, xc2.a("google_api_key"), xc2.a("firebase_database_url"), xc2.a("ga_trackingId"), xc2.a("gcm_defaultSenderId"), xc2.a("google_storage_bucket"), xc2.a("project_id"));
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public String d() {
        return this.e;
    }

    @DexIgnore
    public String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof L64)) {
            return false;
        }
        L64 l64 = (L64) obj;
        return Pc2.a(this.b, l64.b) && Pc2.a(this.a, l64.a) && Pc2.a(this.c, l64.c) && Pc2.a(this.d, l64.d) && Pc2.a(this.e, l64.e) && Pc2.a(this.f, l64.f) && Pc2.a(this.g, l64.g);
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("applicationId", this.b);
        c2.a("apiKey", this.a);
        c2.a("databaseUrl", this.c);
        c2.a("gcmSenderId", this.e);
        c2.a("storageBucket", this.f);
        c2.a("projectId", this.g);
        return c2.toString();
    }
}
