package com.fossil;

import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jk4 {
    @DexIgnore
    public static /* final */ TimeZone a; // = TimeZone.getTimeZone("UTC");

    @DexIgnore
    public static boolean a(String str, int i, char c) {
        return i < str.length() && str.charAt(i) == c;
    }

    @DexIgnore
    public static int b(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return i;
            }
            i++;
        }
        return str.length();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0158  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x021c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Date c(java.lang.String r14, java.text.ParsePosition r15) throws java.text.ParseException {
        /*
        // Method dump skipped, instructions count: 567
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Jk4.c(java.lang.String, java.text.ParsePosition):java.util.Date");
    }

    @DexIgnore
    public static int d(String str, int i, int i2) throws NumberFormatException {
        int i3;
        int i4;
        if (i < 0 || i2 > str.length() || i > i2) {
            throw new NumberFormatException(str);
        }
        if (i < i2) {
            i3 = i + 1;
            int digit = Character.digit(str.charAt(i), 10);
            if (digit >= 0) {
                i4 = -digit;
            } else {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
        } else {
            i4 = 0;
            i3 = i;
        }
        while (i3 < i2) {
            int digit2 = Character.digit(str.charAt(i3), 10);
            if (digit2 >= 0) {
                i4 = (i4 * 10) - digit2;
                i3++;
            } else {
                throw new NumberFormatException("Invalid number: " + str.substring(i, i2));
            }
        }
        return -i4;
    }
}
