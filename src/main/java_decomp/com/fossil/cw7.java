package com.fossil;

import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cw7 implements Dw7 {
    @DexIgnore
    public /* final */ Future<?> b;

    @DexIgnore
    public Cw7(Future<?> future) {
        this.b = future;
    }

    @DexIgnore
    @Override // com.fossil.Dw7
    public void dispose() {
        this.b.cancel(false);
    }

    @DexIgnore
    public String toString() {
        return "DisposableFutureHandle[" + this.b + ']';
    }
}
