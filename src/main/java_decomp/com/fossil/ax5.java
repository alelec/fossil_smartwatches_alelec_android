package com.fossil;

import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ax5 extends Zf.Di<SleepSummary> {
    @DexIgnore
    public boolean a(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        Wg6.c(sleepSummary, "oldItem");
        Wg6.c(sleepSummary2, "newItem");
        return Wg6.a(sleepSummary, sleepSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        return a(sleepSummary, sleepSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        return b(sleepSummary, sleepSummary2);
    }

    @DexIgnore
    public boolean b(SleepSummary sleepSummary, SleepSummary sleepSummary2) {
        Date date = null;
        Wg6.c(sleepSummary, "oldItem");
        Wg6.c(sleepSummary2, "newItem");
        MFSleepDay sleepDay = sleepSummary.getSleepDay();
        Date date2 = sleepDay != null ? sleepDay.getDate() : null;
        MFSleepDay sleepDay2 = sleepSummary2.getSleepDay();
        if (sleepDay2 != null) {
            date = sleepDay2.getDate();
        }
        return TimeUtils.m0(date2, date);
    }
}
