package com.fossil;

import android.net.Uri;
import androidx.lifecycle.MutableLiveData;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.DeviceHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hh5 extends Hq4 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public /* final */ Yk7 h; // = Zk7.a(Di.INSTANCE);
    @DexIgnore
    public /* final */ Yk7 i; // = Zk7.a(Ci.INSTANCE);
    @DexIgnore
    public /* final */ UserRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public Ai() {
            this(false, false, false, false, 15, null);
        }

        @DexIgnore
        public Ai(boolean z, boolean z2, boolean z3, boolean z4) {
            this.a = z;
            this.b = z2;
            this.c = z3;
            this.d = z4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(boolean z, boolean z2, boolean z3, boolean z4, int i, Qg6 qg6) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4);
        }

        @DexIgnore
        public final boolean a() {
            return this.d;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final boolean c() {
            return this.a;
        }

        @DexIgnore
        public final boolean d() {
            return this.c;
        }

        @DexIgnore
        public final void e(boolean z) {
            this.d = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(this.a == ai.a && this.b == ai.b && this.c == ai.c && this.d == ai.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void g(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final void h(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (z2) {
                z2 = true;
            }
            boolean z3 = this.c;
            if (z3) {
                z3 = true;
            }
            boolean z4 = this.d;
            if (!z4) {
                i = z4 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z2 ? 1 : 0;
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z3 ? 1 : 0;
            int i9 = z3 ? 1 : 0;
            int i10 = z3 ? 1 : 0;
            return (((((i2 * 31) + i5) * 31) + i8) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "NavigationState(mNextToWatchFaceList=" + this.a + ", mNextToWatchFaceGallery=" + this.b + ", mNextToWelcome=" + this.c + ", mNextToDashboard=" + this.d + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $it;
        @DexIgnore
        public /* final */ /* synthetic */ Uri $uri$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Hh5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.j;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Uri uri, Xe6 xe6, Hh5 hh5, Uri uri2) {
            super(2, xe6);
            this.$it = uri;
            this.this$0 = hh5;
            this.$uri$inlined = uri2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$it, xe6, this.this$0, this.$uri$inlined);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) g) == null) {
                Hh5.r(this.this$0, false, false, true, false, 11, null);
            } else if (!DeviceHelper.o.x(PortfolioApp.get.instance().J())) {
                Hh5.r(this.this$0, false, false, false, true, 7, null);
                Hq4.d(this.this$0, false, true, null, 5, null);
                return Cd6.a;
            } else {
                boolean contains = this.$it.getPathSegments().contains("customized");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = Hh5.k;
                local.d(str, "checkToGoToNextStep open my faces: " + contains);
                if (contains) {
                    Hh5.r(this.this$0, true, false, false, false, 14, null);
                } else {
                    Hh5.r(this.this$0, false, true, false, false, 13, null);
                }
            }
            Hq4.d(this.this$0, false, true, null, 5, null);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Gg6<Ai> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Ai invoke() {
            throw null;
            //return new Ai(false, false, false, false, 8, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Gg6<MutableLiveData<Ai>> {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();

        @DexIgnore
        public Di() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final MutableLiveData<Ai> invoke() {
            throw null;
            //return new MutableLiveData<>();
        }
    }

    /*
    static {
        String simpleName = Hh5.class.getSimpleName();
        Wg6.b(simpleName, "DeepLinkViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public Hh5(UserRepository userRepository) {
        Wg6.c(userRepository, "mUserRepository");
        this.j = userRepository;
    }

    @DexIgnore
    public static /* synthetic */ void r(Hh5 hh5, boolean z, boolean z2, boolean z3, boolean z4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        if ((i2 & 4) != 0) {
            z3 = false;
        }
        if ((i2 & 8) != 0) {
            z4 = false;
        }
        hh5.q(z, z2, z3, z4);
    }

    @DexIgnore
    public final void p(Uri uri) {
        if (uri != null) {
            Hq4.d(this, true, false, null, 6, null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = k;
            local.d(str, "checkToGoToNextStep uri " + uri);
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(uri, null, this, uri), 3, null);
        }
    }

    @DexIgnore
    public final void q(boolean z, boolean z2, boolean z3, boolean z4) {
        s().f(z2);
        s().g(z);
        s().h(z3);
        s().e(z4);
        t().l(s());
    }

    @DexIgnore
    public final Ai s() {
        return (Ai) this.i.getValue();
    }

    @DexIgnore
    public final MutableLiveData<Ai> t() {
        return (MutableLiveData) this.h.getValue();
    }
}
