package com.fossil;

import android.annotation.TargetApi;
import android.os.Trace;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xj2 implements Closeable {
    @DexIgnore
    public static /* final */ Fk2<Boolean> c; // = Ek2.b().a("nts.enable_tracing", true);
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    @TargetApi(18)
    public Xj2(String str) {
        boolean z = Mf2.e() && c.get().booleanValue();
        this.b = z;
        if (z) {
            Trace.beginSection(str.length() > 127 ? str.substring(0, 127) : str);
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    @TargetApi(18)
    public final void close() {
        if (this.b) {
            Trace.endSection();
        }
    }
}
