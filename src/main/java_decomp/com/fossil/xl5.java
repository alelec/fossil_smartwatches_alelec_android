package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xl5 extends Exception {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7323249827281485390L;

    @DexIgnore
    public Xl5() {
    }

    @DexIgnore
    public Xl5(String str) {
        super(str);
    }

    @DexIgnore
    public Xl5(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public Xl5(Throwable th) {
        super(th);
    }
}
