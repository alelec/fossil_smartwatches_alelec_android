package com.fossil;

import com.fossil.G02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K02<T> implements Xy1<T> {
    @DexIgnore
    public /* final */ H02 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Ty1 c;
    @DexIgnore
    public /* final */ Wy1<T, byte[]> d;
    @DexIgnore
    public /* final */ L02 e;

    @DexIgnore
    public K02(H02 h02, String str, Ty1 ty1, Wy1<T, byte[]> wy1, L02 l02) {
        this.a = h02;
        this.b = str;
        this.c = ty1;
        this.d = wy1;
        this.e = l02;
    }

    @DexIgnore
    public static /* synthetic */ void c(Exception exc) {
    }

    @DexIgnore
    @Override // com.fossil.Xy1
    public void a(Uy1<T> uy1) {
        b(uy1, J02.b());
    }

    @DexIgnore
    @Override // com.fossil.Xy1
    public void b(Uy1<T> uy1, Zy1 zy1) {
        L02 l02 = this.e;
        G02.Ai a2 = G02.a();
        a2.e(this.a);
        a2.c(uy1);
        a2.f(this.b);
        a2.d(this.d);
        a2.b(this.c);
        l02.a(a2.a(), zy1);
    }
}
