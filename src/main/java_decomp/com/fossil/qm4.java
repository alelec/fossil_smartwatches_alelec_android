package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qm4 {
    @DexIgnore
    public static int a(CharSequence charSequence, int i) {
        int i2 = 0;
        int length = charSequence.length();
        if (i < length) {
            char charAt = charSequence.charAt(i);
            while (f(charAt) && i < length) {
                i2++;
                i++;
                if (i < length) {
                    charAt = charSequence.charAt(i);
                }
            }
        }
        return i2;
    }

    @DexIgnore
    public static String b(String str, Sm4 sm4, Ll4 ll4, Ll4 ll42) {
        Hm4 hm4 = new Hm4();
        int i = 0;
        Jm4 jm4 = new Jm4();
        Tm4 tm4 = new Tm4();
        Um4 um4 = new Um4();
        Mm4 mm4 = new Mm4();
        Im4 im4 = new Im4();
        Om4 om4 = new Om4(str);
        om4.n(sm4);
        om4.l(ll4, ll42);
        if (str.startsWith("[)>\u001e05\u001d") && str.endsWith("\u001e\u0004")) {
            om4.r('\u00ec');
            om4.m(2);
            om4.f += 7;
        } else if (str.startsWith("[)>\u001e06\u001d") && str.endsWith("\u001e\u0004")) {
            om4.r('\u00ed');
            om4.m(2);
            om4.f += 7;
        }
        while (om4.i()) {
            new Nm4[]{hm4, jm4, tm4, um4, mm4, im4}[i].a(om4);
            if (om4.e() >= 0) {
                i = om4.e();
                om4.j();
            }
        }
        int a2 = om4.a();
        om4.p();
        int a3 = om4.g().a();
        if (!(a2 >= a3 || i == 0 || i == 5)) {
            om4.r('\u00fe');
        }
        StringBuilder b = om4.b();
        if (b.length() < a3) {
            b.append('\u0081');
        }
        while (b.length() < a3) {
            b.append(o('\u0081', b.length() + 1));
        }
        return om4.b().toString();
    }

    @DexIgnore
    public static int c(float[] fArr, int[] iArr, int i, byte[] bArr) {
        Arrays.fill(bArr, (byte) 0);
        int i2 = i;
        for (int i3 = 0; i3 < 6; i3++) {
            iArr[i3] = (int) Math.ceil((double) fArr[i3]);
            int i4 = iArr[i3];
            if (i2 > i4) {
                Arrays.fill(bArr, (byte) 0);
                i2 = i4;
            }
            if (i2 == i4) {
                bArr[i3] = (byte) ((byte) (bArr[i3] + 1));
            }
        }
        return i2;
    }

    @DexIgnore
    public static int d(byte[] bArr) {
        int i = 0;
        for (int i2 = 0; i2 < 6; i2++) {
            i += bArr[i2];
        }
        return i;
    }

    @DexIgnore
    public static void e(char c) {
        String hexString = Integer.toHexString(c);
        throw new IllegalArgumentException("Illegal character: " + c + " (0x" + ("0000".substring(0, 4 - hexString.length()) + hexString) + ')');
    }

    @DexIgnore
    public static boolean f(char c) {
        return c >= '0' && c <= '9';
    }

    @DexIgnore
    public static boolean g(char c) {
        return c >= '\u0080' && c <= '\u00ff';
    }

    @DexIgnore
    public static boolean h(char c) {
        return c == ' ' || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z');
    }

    @DexIgnore
    public static boolean i(char c) {
        return c >= ' ' && c <= '^';
    }

    @DexIgnore
    public static boolean j(char c) {
        return c == ' ' || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z');
    }

    @DexIgnore
    public static boolean k(char c) {
        return m(c) || c == ' ' || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z');
    }

    @DexIgnore
    public static boolean l(char c) {
        return false;
    }

    @DexIgnore
    public static boolean m(char c) {
        return c == '\r' || c == '*' || c == '>';
    }

    @DexIgnore
    public static int n(CharSequence charSequence, int i, int i2) {
        float[] fArr;
        if (i >= charSequence.length()) {
            return i2;
        }
        if (i2 == 0) {
            fArr = new float[]{0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.25f};
        } else {
            fArr = new float[]{1.0f, 2.0f, 2.0f, 2.0f, 2.0f, 2.25f};
            fArr[i2] = 0.0f;
        }
        int i3 = 0;
        while (true) {
            int i4 = i + i3;
            if (i4 == charSequence.length()) {
                byte[] bArr = new byte[6];
                int[] iArr = new int[6];
                int c = c(fArr, iArr, Integer.MAX_VALUE, bArr);
                int d = d(bArr);
                if (iArr[0] == c) {
                    return 0;
                }
                if (d == 1 && bArr[5] > 0) {
                    return 5;
                }
                if (d == 1 && bArr[4] > 0) {
                    return 4;
                }
                if (d != 1 || bArr[2] <= 0) {
                    return (d != 1 || bArr[3] <= 0) ? 1 : 3;
                }
                return 2;
            }
            char charAt = charSequence.charAt(i4);
            i3++;
            if (f(charAt)) {
                fArr[0] = fArr[0] + 0.5f;
            } else if (g(charAt)) {
                fArr[0] = (float) Math.ceil((double) fArr[0]);
                fArr[0] = fArr[0] + 2.0f;
            } else {
                fArr[0] = (float) Math.ceil((double) fArr[0]);
                fArr[0] = fArr[0] + 1.0f;
            }
            if (h(charAt)) {
                fArr[1] = fArr[1] + 0.6666667f;
            } else if (g(charAt)) {
                fArr[1] = fArr[1] + 2.6666667f;
            } else {
                fArr[1] = fArr[1] + 1.3333334f;
            }
            if (j(charAt)) {
                fArr[2] = fArr[2] + 0.6666667f;
            } else if (g(charAt)) {
                fArr[2] = fArr[2] + 2.6666667f;
            } else {
                fArr[2] = fArr[2] + 1.3333334f;
            }
            if (k(charAt)) {
                fArr[3] = fArr[3] + 0.6666667f;
            } else if (g(charAt)) {
                fArr[3] = fArr[3] + 4.3333335f;
            } else {
                fArr[3] = fArr[3] + 3.3333333f;
            }
            if (i(charAt)) {
                fArr[4] = fArr[4] + 0.75f;
            } else if (g(charAt)) {
                fArr[4] = fArr[4] + 4.25f;
            } else {
                fArr[4] = fArr[4] + 3.25f;
            }
            if (l(charAt)) {
                fArr[5] = fArr[5] + 4.0f;
            } else {
                fArr[5] = fArr[5] + 1.0f;
            }
            if (i3 >= 4) {
                int[] iArr2 = new int[6];
                byte[] bArr2 = new byte[6];
                c(fArr, iArr2, Integer.MAX_VALUE, bArr2);
                int d2 = d(bArr2);
                if (iArr2[0] < iArr2[5] && iArr2[0] < iArr2[1] && iArr2[0] < iArr2[2] && iArr2[0] < iArr2[3] && iArr2[0] < iArr2[4]) {
                    return 0;
                }
                if (iArr2[5] < iArr2[0] || bArr2[1] + bArr2[2] + bArr2[3] + bArr2[4] == 0) {
                    return 5;
                }
                if (d2 == 1 && bArr2[4] > 0) {
                    return 4;
                }
                if (d2 == 1 && bArr2[2] > 0) {
                    return 2;
                }
                if (d2 == 1 && bArr2[3] > 0) {
                    return 3;
                }
                if (iArr2[1] + 1 < iArr2[0] && iArr2[1] + 1 < iArr2[5] && iArr2[1] + 1 < iArr2[4] && iArr2[1] + 1 < iArr2[2]) {
                    if (iArr2[1] < iArr2[3]) {
                        return 1;
                    }
                    if (iArr2[1] == iArr2[3]) {
                        int i5 = i + i3;
                        while (true) {
                            i5++;
                            if (i5 >= charSequence.length()) {
                                break;
                            }
                            char charAt2 = charSequence.charAt(i5);
                            if (!m(charAt2)) {
                                if (!k(charAt2)) {
                                    break;
                                }
                            } else {
                                return 3;
                            }
                        }
                        return 1;
                    }
                }
            }
        }
        return 5;
    }

    @DexIgnore
    public static char o(char c, int i) {
        int i2 = ((i * 149) % 253) + 1 + c;
        if (i2 > 254) {
            i2 -= 254;
        }
        return (char) i2;
    }
}
