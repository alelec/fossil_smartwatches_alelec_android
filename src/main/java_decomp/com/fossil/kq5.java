package com.fossil;

import com.mapped.Ap4;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kq5<T> extends Ap4<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public Kq5(T t, boolean z) {
        super(null);
        this.a = t;
        this.b = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Kq5(Object obj, boolean z, int i, Qg6 qg6) {
        this(obj, (i & 2) != 0 ? false : z);
    }

    @DexIgnore
    public final T a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Kq5) {
                Kq5 kq5 = (Kq5) obj;
                if (!Wg6.a(this.a, kq5.a) || this.b != kq5.b) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        T t = this.a;
        int hashCode = t != null ? t.hashCode() : 0;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Success(response=" + ((Object) this.a) + ", isFromCache=" + this.b + ")";
    }
}
