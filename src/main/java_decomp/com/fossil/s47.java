package com.fossil;

import android.widget.RadioGroup;
import com.mapped.AlertDialogFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class S47 implements RadioGroup.OnCheckedChangeListener {
    @DexIgnore
    public /* final */ /* synthetic */ AlertDialogFragment a;
    @DexIgnore
    public /* final */ /* synthetic */ Integer b;

    @DexIgnore
    public /* synthetic */ S47(AlertDialogFragment alertDialogFragment, Integer num) {
        this.a = alertDialogFragment;
        this.b = num;
    }

    @DexIgnore
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.a.v6(this.b, radioGroup, i);
    }
}
