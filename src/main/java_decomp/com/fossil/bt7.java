package com.fossil;

import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bt7<T, R> implements Ts7<R> {
    @DexIgnore
    public /* final */ Ts7<T> a;
    @DexIgnore
    public /* final */ Hg6<T, R> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterator<R>, Jr7 {
        @DexIgnore
        public /* final */ Iterator<T> b;
        @DexIgnore
        public /* final */ /* synthetic */ Bt7 c;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Bt7 bt7) {
            this.c = bt7;
            this.b = bt7.a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public R next() {
            return (R) this.c.b.invoke(this.b.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Ts7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Hg6<? super T, ? extends R> */
    /* JADX WARN: Multi-variable type inference failed */
    public Bt7(Ts7<? extends T> ts7, Hg6<? super T, ? extends R> hg6) {
        Wg6.c(ts7, "sequence");
        Wg6.c(hg6, "transformer");
        this.a = ts7;
        this.b = hg6;
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<R> iterator() {
        return new Ai(this);
    }
}
