package com.fossil;

import com.mapped.Wg6;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class It7 implements Ht7 {
    @DexIgnore
    public /* final */ Matcher a;
    @DexIgnore
    public /* final */ CharSequence b;

    @DexIgnore
    public It7(Matcher matcher, CharSequence charSequence) {
        Wg6.c(matcher, "matcher");
        Wg6.c(charSequence, "input");
        this.a = matcher;
        this.b = charSequence;
    }

    @DexIgnore
    @Override // com.fossil.Ht7
    public Wr7 a() {
        return Lt7.g(b());
    }

    @DexIgnore
    public final MatchResult b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ht7
    public String getValue() {
        String group = b().group();
        Wg6.b(group, "matchResult.group()");
        return group;
    }

    @DexIgnore
    @Override // com.fossil.Ht7
    public Ht7 next() {
        int end = (b().end() == b().start() ? 1 : 0) + b().end();
        if (end > this.b.length()) {
            return null;
        }
        Matcher matcher = this.a.pattern().matcher(this.b);
        Wg6.b(matcher, "matcher.pattern().matcher(input)");
        return Lt7.e(matcher, end, this.b);
    }
}
