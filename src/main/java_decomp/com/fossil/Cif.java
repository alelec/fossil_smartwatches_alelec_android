package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.device.logic.phase.SyncPhase$onAllFileRead$1", f = "SyncPhase.kt", l = {}, m = "invokeSuspend")
/* renamed from: com.fossil.if  reason: invalid class name */
public final class Cif extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ mi d;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Cif(mi miVar, ArrayList arrayList, qn7 qn7) {
        super(2, qn7);
        this.d = miVar;
        this.e = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        Cif ifVar = new Cif(this.d, this.e, qn7);
        ifVar.b = (iv7) obj;
        throw null;
        //return ifVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        Cif ifVar = new Cif(this.d, this.e, qn7);
        ifVar.b = iv7;
        return ifVar.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        yn7.d();
        if (this.c == 0) {
            el7.b(obj);
            u.f3499a.b(this.d.x.a().getMacAddress(), ob.ACTIVITY_FILE.c);
            this.d.b0(this.e);
            mi miVar = this.d;
            miVar.l(nr.a(miVar.v, null, zq.SUCCESS, null, null, 13));
            return tl7.f3441a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
