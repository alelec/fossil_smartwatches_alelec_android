package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fw1 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ Format d;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Fw1(android.os.Parcel r5) {
        /*
            r4 = this;
            r2 = 0
            java.lang.String r0 = r5.readString()
            if (r0 == 0) goto L_0x0029
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            byte[] r1 = r5.createByteArray()
            if (r1 == 0) goto L_0x0025
            java.lang.String r2 = "parcel.createByteArray()!!"
            com.mapped.Wg6.b(r1, r2)
            int r2 = r5.readInt()
            com.fossil.imagefilters.Format[] r3 = com.fossil.imagefilters.Format.values()
            r2 = r3[r2]
            r4.<init>(r0, r1, r2)
            return
        L_0x0025:
            com.mapped.Wg6.i()
            throw r2
        L_0x0029:
            com.mapped.Wg6.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fw1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Fw1(String str, byte[] bArr, Format format) {
        this.b = str;
        this.c = bArr;
        this.d = format;
    }

    @DexIgnore
    public static /* synthetic */ Cc0 a(Fw1 fw1, Lv1 lv1, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                str = fw1.b;
            }
            return fw1.a(lv1, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getImageNode");
    }

    @DexIgnore
    public final Cc0 a(Lv1 lv1, String str) {
        byte[] a2 = a(lv1);
        if (!(a2.length == 0)) {
            return new Cc0(str, a2);
        }
        return null;
    }

    @DexIgnore
    public final String a() {
        return G80.f(this.d);
    }

    @DexIgnore
    public final void a(String str) {
        this.b = str;
    }

    @DexIgnore
    public final byte[] a(Lv1 lv1) {
        try {
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(this.c, 0, this.c.length);
            Wg6.b(decodeByteArray, "bitmap");
            if (decodeByteArray.getByteCount() <= 0) {
                return new byte[0];
            }
            byte[] encode = EInkImageFactory.encode(decodeByteArray, FilterType.DIRECT_MAPPING, new OutputSettings(lv1.getWidth(), lv1.getHeight(), this.d, false, false));
            Wg6.b(encode, "EInkImageFactory.encode(\u2026 false)\n                )");
            decodeByteArray.recycle();
            return encode;
        } catch (Throwable th) {
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // java.lang.Object
    public abstract Fw1 clone();

    @DexIgnore
    @Override // java.lang.Object
    public abstract /* synthetic */ Nx1 clone();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getBitmapImageData() {
        return this.c;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.d(Gy1.d(Gy1.d(new JSONObject(), Jd0.H, this.b), Jd0.F0, Integer.valueOf(this.c.length)), Jd0.U5, Ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
