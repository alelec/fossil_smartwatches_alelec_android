package com.fossil;

import android.content.Context;
import android.util.SparseIntArray;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.xx5;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux5 extends nx5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public Alarm e;
    @DexIgnore
    public SparseIntArray f; // = new SparseIntArray();
    @DexIgnore
    public MFUser g;
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public /* final */ ox5 j;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ ArrayList<Alarm> l;
    @DexIgnore
    public /* final */ Alarm m;
    @DexIgnore
    public /* final */ yx5 n;
    @DexIgnore
    public /* final */ bk5 o;
    @DexIgnore
    public /* final */ xx5 p;
    @DexIgnore
    public /* final */ UserRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ux5.r;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<xx5.e, xx5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux5 f3668a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ux5 ux5) {
            this.f3668a = ux5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xx5.c cVar) {
            pq7.c(cVar, "errorValue");
            bk5 bk5 = this.f3668a.o;
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            bk5.g(applicationContext);
            this.f3668a.j.a();
            int b = cVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ux5.s.a();
            local.d(a2, "deleteAlarm() - deleteAlarm - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.f3668a.j.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.f3668a.j.s5();
                    return;
                }
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(cVar.a());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            ox5 ox5 = this.f3668a.j;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                ox5.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xx5.e eVar) {
            pq7.c(eVar, "responseValue");
            bk5 bk5 = this.f3668a.o;
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            bk5.g(applicationContext);
            this.f3668a.j.a();
            this.f3668a.j.B0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<yx5.d, yx5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ux5 f3669a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(ux5 ux5) {
            this.f3669a = ux5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yx5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f3669a.h = bVar.a().getUri();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ux5.s.a();
            local.d(a2, "saveAlarm SetAlarms onError - uri: " + this.f3669a.h);
            this.f3669a.j.a();
            int c = bVar.c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = ux5.s.a();
            local2.d(a3, "saveAlarm - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.f3669a.j.c();
                    return;
                } else if (!(c == 1112 || c == 1113)) {
                    if (this.f3669a.m == null) {
                        this.f3669a.j.Y1(bVar.a(), false);
                        return;
                    } else {
                        this.f3669a.j.s5();
                        return;
                    }
                }
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.b());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            ox5 ox5 = this.f3669a.j;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                ox5.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yx5.d dVar) {
            pq7.c(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(ux5.s.a(), "saveAlarm SetAlarms onSuccess");
            this.f3669a.j.a();
            Alarm alarm = this.f3669a.m;
            if (alarm == null) {
                alarm = dVar.a();
            }
            this.f3669a.j.Y1(alarm, true);
            this.f3669a.V();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1", f = "AlarmPresenter.kt", l = {56}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ux5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1", f = "AlarmPresenter.kt", l = {56}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.q;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ux5 ux5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ux5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ux5 ux5;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = ux5.s.a();
                local.d(a2, "start alarm " + this.this$0.m);
                PortfolioApp.h0.h(iv7);
                this.this$0.n.s();
                this.this$0.p.r();
                wq5.d.g(CommunicateMode.SET_LIST_ALARM);
                ux5 ux52 = this.this$0;
                dv7 i2 = ux52.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.L$1 = ux52;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
                ux5 = ux52;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                ux5 = (ux5) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ux5.g = (MFUser) g;
            MFUser mFUser = this.this$0.g;
            if (!(mFUser == null || mFUser.getUserId() == null)) {
                this.this$0.j.Z4(this.this$0.m != null);
                if (this.this$0.m != null) {
                    if (this.this$0.e == null) {
                        ux5 ux53 = this.this$0;
                        ux53.e = Alarm.copy$default(ux53.m, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null);
                    }
                    this.this$0.j.W2();
                } else if (this.this$0.e == null) {
                    this.this$0.e = new Alarm(null, "", "", "", 0, 0, new int[0], true, false, "", "", 0, 2048, null);
                    this.this$0.W();
                }
                this.this$0.Y();
                if (this.this$0.f.size() == 0) {
                    ux5 ux54 = this.this$0;
                    Alarm alarm = ux54.e;
                    ux54.f = ux54.P(alarm != null ? alarm.getDays() : null);
                }
                ox5 ox5 = this.this$0.j;
                Alarm alarm2 = this.this$0.e;
                String title = alarm2 != null ? alarm2.getTitle() : null;
                if (title != null) {
                    ox5.t(title);
                    ox5 ox52 = this.this$0.j;
                    Alarm alarm3 = this.this$0.e;
                    String message = alarm3 != null ? alarm3.getMessage() : null;
                    if (message != null) {
                        ox52.l2(message);
                        ox5 ox53 = this.this$0.j;
                        Alarm alarm4 = this.this$0.e;
                        Integer e = alarm4 != null ? ao7.e(alarm4.getTotalMinutes()) : null;
                        if (e != null) {
                            ox53.E(e.intValue());
                            ox5 ox54 = this.this$0.j;
                            Alarm alarm5 = this.this$0.e;
                            Boolean a3 = alarm5 != null ? ao7.a(alarm5.isRepeated()) : null;
                            if (a3 != null) {
                                ox54.m2(a3.booleanValue());
                                this.this$0.j.b5(this.this$0.f);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = ux5.class.getSimpleName();
        pq7.b(simpleName, "AlarmPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public ux5(ox5 ox5, String str, ArrayList<Alarm> arrayList, Alarm alarm, yx5 yx5, bk5 bk5, xx5 xx5, UserRepository userRepository) {
        pq7.c(ox5, "mView");
        pq7.c(str, "mDeviceId");
        pq7.c(arrayList, "mAlarms");
        pq7.c(yx5, "mSetAlarms");
        pq7.c(bk5, "mAlarmHelper");
        pq7.c(xx5, "mDeleteAlarm");
        pq7.c(userRepository, "mUserRepository");
        this.j = ox5;
        this.k = str;
        this.l = arrayList;
        this.m = alarm;
        this.n = yx5;
        this.o = bk5;
        this.p = xx5;
        this.q = userRepository;
    }

    @DexIgnore
    public static /* synthetic */ void S(ux5 ux5, String str, String str2, Integer num, int[] iArr, Boolean bool, Boolean bool2, String str3, String str4, String str5, int i2, Object obj) {
        ux5.R((i2 & 1) != 0 ? null : str, (i2 & 2) != 0 ? null : str2, (i2 & 4) != 0 ? null : num, (i2 & 8) != 0 ? null : iArr, (i2 & 16) != 0 ? null : bool, (i2 & 32) != 0 ? null : bool2, (i2 & 64) != 0 ? null : str3, (i2 & 128) != 0 ? null : str4, (i2 & 256) != 0 ? null : str5);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0091, code lost:
        if (r0 != 12) goto L_0x0093;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0093, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0097, code lost:
        if (r0 == 12) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x009a, code lost:
        if (r0 != 12) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x009c, code lost:
        r2 = r0 + 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0058, code lost:
        if (r0 == 12) goto L_0x005a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void O(java.lang.String r13, java.lang.String r14, boolean r15) {
        /*
            r12 = this;
            r2 = 0
            r4 = 12
            r1 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r3 = com.fossil.ux5.r
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "updateTime: hourValue = "
            r5.append(r6)
            r5.append(r13)
            java.lang.String r6 = ", minuteValue = "
            r5.append(r6)
            r5.append(r14)
            java.lang.String r6 = ", isPM = "
            r5.append(r6)
            r5.append(r15)
            java.lang.String r5 = r5.toString()
            r0.d(r3, r5)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x0071 }
            java.lang.String r3 = "Integer.valueOf(hourValue)"
            com.fossil.pq7.b(r0, r3)     // Catch:{ Exception -> 0x0071 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0071 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x009f }
            java.lang.String r5 = "Integer.valueOf(minuteValue)"
            com.fossil.pq7.b(r3, r5)     // Catch:{ Exception -> 0x009f }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x009f }
        L_0x004a:
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.h0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            boolean r5 = android.text.format.DateFormat.is24HourFormat(r5)
            if (r5 == 0) goto L_0x0095
            if (r15 == 0) goto L_0x0091
            if (r0 != r4) goto L_0x009c
        L_0x005a:
            r2 = r4
        L_0x005b:
            int r0 = r2 * 60
            int r0 = r0 + r3
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            r10 = 507(0x1fb, float:7.1E-43)
            r0 = r12
            r2 = r1
            r4 = r1
            r5 = r1
            r6 = r1
            r7 = r1
            r8 = r1
            r9 = r1
            r11 = r1
            S(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            return
        L_0x0071:
            r3 = move-exception
            r0 = r2
        L_0x0073:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.ux5.r
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Exception when parse time e="
            r7.append(r8)
            r7.append(r3)
            java.lang.String r3 = r7.toString()
            r5.e(r6, r3)
            r3 = r2
            goto L_0x004a
        L_0x0091:
            if (r0 == r4) goto L_0x005b
        L_0x0093:
            r2 = r0
            goto L_0x005b
        L_0x0095:
            if (r15 != 0) goto L_0x009a
            if (r0 != r4) goto L_0x0093
            goto L_0x005b
        L_0x009a:
            if (r0 == r4) goto L_0x005a
        L_0x009c:
            int r2 = r0 + 12
            goto L_0x005b
        L_0x009f:
            r3 = move-exception
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ux5.O(java.lang.String, java.lang.String, boolean):void");
    }

    @DexIgnore
    public final SparseIntArray P(int[] iArr) {
        int length = iArr != null ? iArr.length : 0;
        if (length <= 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray();
        for (int i2 = 0; i2 < length; i2++) {
            if (iArr != null) {
                int i3 = iArr[i2];
                sparseIntArray.put(i3, i3);
            } else {
                pq7.i();
                throw null;
            }
        }
        return sparseIntArray;
    }

    @DexIgnore
    public final int[] Q(SparseIntArray sparseIntArray) {
        int size = sparseIntArray != null ? sparseIntArray.size() : 0;
        if (size <= 0) {
            return null;
        }
        int i2 = 0;
        while (i2 < size) {
            if (sparseIntArray != null) {
                if (sparseIntArray.keyAt(i2) != sparseIntArray.valueAt(i2)) {
                    sparseIntArray.removeAt(i2);
                    size--;
                    i2--;
                }
                i2++;
            } else {
                pq7.i();
                throw null;
            }
        }
        if (sparseIntArray != null) {
            int size2 = sparseIntArray.size();
            if (size2 <= 0) {
                return null;
            }
            int[] iArr = new int[size2];
            for (int i3 = 0; i3 < size2; i3++) {
                iArr[i3] = sparseIntArray.valueAt(i3);
            }
            return iArr;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final void R(String str, String str2, Integer num, int[] iArr, Boolean bool, Boolean bool2, String str3, String str4, String str5) {
        Alarm alarm;
        Alarm alarm2;
        Alarm alarm3;
        Alarm alarm4;
        Alarm alarm5;
        Alarm alarm6;
        if (!(str == null || (alarm6 = this.e) == null)) {
            alarm6.setTitle(str);
        }
        if (!(str2 == null || (alarm5 = this.e) == null)) {
            alarm5.setMessage(str2);
        }
        if (num != null) {
            int intValue = num.intValue();
            Alarm alarm7 = this.e;
            if (alarm7 != null) {
                alarm7.setTotalMinutes(intValue);
            }
        }
        if (!(iArr == null || (alarm4 = this.e) == null)) {
            alarm4.setDays(iArr);
        }
        if (bool != null) {
            boolean booleanValue = bool.booleanValue();
            Alarm alarm8 = this.e;
            if (alarm8 != null) {
                alarm8.setActive(booleanValue);
            }
        }
        if (bool2 != null) {
            boolean booleanValue2 = bool2.booleanValue();
            Alarm alarm9 = this.e;
            if (alarm9 != null) {
                alarm9.setRepeated(booleanValue2);
            }
        }
        if (!(str3 == null || (alarm3 = this.e) == null)) {
            alarm3.setCreatedAt(str3);
        }
        if (!(str4 == null || (alarm2 = this.e) == null)) {
            alarm2.setUpdatedAt(str4);
        }
        if (str5 != null && (alarm = this.e) != null) {
            alarm.setUri(str5);
        }
    }

    @DexIgnore
    public final boolean T() {
        return !pq7.a(this.m, this.e);
    }

    @DexIgnore
    public final boolean U() {
        return this.m == null;
    }

    @DexIgnore
    public final void V() {
        FLogger.INSTANCE.getLocal().d(r, "onSetAlarmsSuccess()");
        this.o.g(PortfolioApp.h0.c());
        PortfolioApp.h0.c().P0(this.k);
    }

    @DexIgnore
    public final void W() {
        boolean z = true;
        int i2 = Calendar.getInstance().get(10);
        int i3 = Calendar.getInstance().get(12);
        if (Calendar.getInstance().get(9) != 1) {
            z = false;
        }
        O(String.valueOf(i2), String.valueOf(i3), z);
    }

    @DexIgnore
    public void X() {
        this.j.M5(this);
    }

    @DexIgnore
    public final void Y() {
        if (U() || T()) {
            this.i = true;
            this.j.Y(true);
            return;
        }
        this.i = false;
        this.j.Y(false);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.n.w();
        this.p.v();
        PortfolioApp.h0.l(this);
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void n() {
        this.j.b();
        xx5 xx5 = this.p;
        String str = this.k;
        ArrayList<Alarm> arrayList = this.l;
        Alarm alarm = this.m;
        if (alarm != null) {
            xx5.e(new xx5.d(str, arrayList, alarm), new b(this));
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void o() {
        if (this.i) {
            this.j.q4();
        } else {
            this.j.B0();
        }
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void p() {
        jn5 jn5 = jn5.b;
        ox5 ox5 = this.j;
        if (ox5 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmFragment");
        } else if (jn5.c(jn5, ((px5) ox5).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            this.j.b();
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            String w0 = lk5.w0(instance.getTime());
            if (this.m != null) {
                S(this, null, null, null, null, null, null, null, w0, null, 383, null);
                Iterator<Alarm> it = this.l.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Alarm next = it.next();
                    if (pq7.a(next.getUri(), this.m.getUri())) {
                        ArrayList<Alarm> arrayList = this.l;
                        int indexOf = arrayList.indexOf(next);
                        Alarm alarm = this.e;
                        if (alarm != null) {
                            arrayList.set(indexOf, alarm);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                }
            } else {
                String str = this.h;
                if (str == null) {
                    StringBuilder sb = new StringBuilder();
                    MFUser mFUser = this.g;
                    sb.append(mFUser != null ? mFUser.getUserId() : null);
                    sb.append(':');
                    Calendar instance2 = Calendar.getInstance();
                    pq7.b(instance2, "Calendar.getInstance()");
                    sb.append(instance2.getTimeInMillis());
                    str = sb.toString();
                }
                S(this, null, null, null, null, null, null, w0, w0, str, 63, null);
                ArrayList<Alarm> arrayList2 = this.l;
                Alarm alarm2 = this.e;
                if (alarm2 != null) {
                    arrayList2.add(alarm2);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("saveAlarm - uri: ");
            Alarm alarm3 = this.e;
            sb2.append(alarm3 != null ? alarm3.getUri() : null);
            local.d(str2, sb2.toString());
            FLogger.INSTANCE.getLocal().d(r, "saveAlarm SetAlarms");
            yx5 yx5 = this.n;
            String str3 = this.k;
            ArrayList<Alarm> arrayList3 = this.l;
            Alarm alarm4 = this.e;
            if (alarm4 != null) {
                yx5.e(new yx5.c(str3, arrayList3, alarm4), new c(this));
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void q(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "updateDaysRepeat: isStateEnabled = " + z + ", day = " + i2);
        if (z) {
            this.f.put(i2, i2);
        } else {
            this.f.delete(i2);
        }
        S(this, null, null, null, Q(this.f), null, null, null, null, null, 503, null);
        if (this.f.size() == 0) {
            S(this, null, null, null, null, null, Boolean.FALSE, null, null, null, 479, null);
            this.j.m2(false);
        }
        Y();
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void r(String str) {
        pq7.c(str, "message");
        S(this, null, str, null, null, null, null, null, null, null, 509, null);
        Y();
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void s(boolean z) {
        S(this, null, null, null, null, null, Boolean.valueOf(z), null, null, null, 479, null);
        if (!z) {
            S(this, null, null, null, new int[0], null, null, null, null, null, 503, null);
        } else if (this.f.size() != 0) {
            S(this, null, null, null, Q(this.f), null, null, null, null, null, 503, null);
        } else {
            SparseIntArray P = P(new int[]{2, 3, 4, 5, 6, 7, 1});
            this.f = P;
            S(this, null, null, null, Q(P), null, null, null, null, null, 503, null);
            this.j.b5(this.f);
        }
        if (this.m != null) {
            Y();
        }
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void t(String str, String str2, boolean z) {
        pq7.c(str, "hourValue");
        pq7.c(str2, "minuteValue");
        O(str, str2, z);
        if (this.m != null) {
            Y();
        }
    }

    @DexIgnore
    @Override // com.fossil.nx5
    public void u(String str) {
        pq7.c(str, "title");
        S(this, str, null, null, null, null, null, null, null, null, 510, null);
        Y();
    }
}
