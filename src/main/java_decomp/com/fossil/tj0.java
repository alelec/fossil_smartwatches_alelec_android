package com.fossil;

import com.fossil.Oj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tj0 {
    @DexIgnore
    public Ak0 a; // = new Ak0(this);
    @DexIgnore
    public /* final */ Uj0 b;
    @DexIgnore
    public /* final */ Di c;
    @DexIgnore
    public Tj0 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public Ci g; // = Ci.NONE;
    @DexIgnore
    public int h;
    @DexIgnore
    public Oj0 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Di.values().length];
            a = iArr;
            try {
                iArr[Di.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Di.LEFT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Di.RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Di.TOP.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Di.BOTTOM.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Di.BASELINE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Di.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[Di.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Di.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
        */
    }

    @DexIgnore
    public enum Bi {
        RELAXED,
        STRICT
    }

    @DexIgnore
    public enum Ci {
        NONE,
        STRONG,
        WEAK
    }

    @DexIgnore
    public enum Di {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        BASELINE,
        CENTER,
        CENTER_X,
        CENTER_Y
    }

    @DexIgnore
    public Tj0(Uj0 uj0, Di di) {
        Bi bi = Bi.RELAXED;
        this.h = 0;
        this.b = uj0;
        this.c = di;
    }

    @DexIgnore
    public boolean a(Tj0 tj0, int i2, int i3, Ci ci, int i4, boolean z) {
        if (tj0 == null) {
            this.d = null;
            this.e = 0;
            this.f = -1;
            this.g = Ci.NONE;
            this.h = 2;
            return true;
        } else if (!z && !l(tj0)) {
            return false;
        } else {
            this.d = tj0;
            if (i2 > 0) {
                this.e = i2;
            } else {
                this.e = 0;
            }
            this.f = i3;
            this.g = ci;
            this.h = i4;
            return true;
        }
    }

    @DexIgnore
    public boolean b(Tj0 tj0, int i2, Ci ci, int i3) {
        return a(tj0, i2, -1, ci, i3, false);
    }

    @DexIgnore
    public int c() {
        return this.h;
    }

    @DexIgnore
    public int d() {
        Tj0 tj0;
        if (this.b.C() == 8) {
            return 0;
        }
        return (this.f <= -1 || (tj0 = this.d) == null || tj0.b.C() != 8) ? this.e : this.f;
    }

    @DexIgnore
    public Uj0 e() {
        return this.b;
    }

    @DexIgnore
    public Ak0 f() {
        return this.a;
    }

    @DexIgnore
    public Oj0 g() {
        return this.i;
    }

    @DexIgnore
    public Ci h() {
        return this.g;
    }

    @DexIgnore
    public Tj0 i() {
        return this.d;
    }

    @DexIgnore
    public Di j() {
        return this.c;
    }

    @DexIgnore
    public boolean k() {
        return this.d != null;
    }

    @DexIgnore
    public boolean l(Tj0 tj0) {
        boolean z;
        if (tj0 == null) {
            return false;
        }
        Di j = tj0.j();
        Di di = this.c;
        if (j == di) {
            return di != Di.BASELINE || (tj0.e().I() && e().I());
        }
        switch (Ai.a[di.ordinal()]) {
            case 1:
                return (j == Di.BASELINE || j == Di.CENTER_X || j == Di.CENTER_Y) ? false : true;
            case 2:
            case 3:
                z = j == Di.LEFT || j == Di.RIGHT;
                if (tj0.e() instanceof Xj0) {
                    return z || j == Di.CENTER_X;
                }
                break;
            case 4:
            case 5:
                z = j == Di.TOP || j == Di.BOTTOM;
                if (tj0.e() instanceof Xj0) {
                    return z || j == Di.CENTER_Y;
                }
                break;
            case 6:
            case 7:
            case 8:
            case 9:
                return false;
            default:
                throw new AssertionError(this.c.name());
        }
        return z;
    }

    @DexIgnore
    public void m() {
        this.d = null;
        this.e = 0;
        this.f = -1;
        this.g = Ci.STRONG;
        this.h = 0;
        Bi bi = Bi.RELAXED;
        this.a.e();
    }

    @DexIgnore
    public void n(Ij0 ij0) {
        Oj0 oj0 = this.i;
        if (oj0 == null) {
            this.i = new Oj0(Oj0.Ai.UNRESTRICTED, null);
        } else {
            oj0.d();
        }
    }

    @DexIgnore
    public String toString() {
        return this.b.n() + ":" + this.c.toString();
    }
}
