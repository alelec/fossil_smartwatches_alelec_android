package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Kh2 extends IInterface {
    @DexIgnore
    int L1(Rg2 rg2, String str, boolean z) throws RemoteException;

    @DexIgnore
    Rg2 V1(Rg2 rg2, String str, int i) throws RemoteException;

    @DexIgnore
    int f2() throws RemoteException;

    @DexIgnore
    int m1(Rg2 rg2, String str, boolean z) throws RemoteException;

    @DexIgnore
    Rg2 w0(Rg2 rg2, String str, int i) throws RemoteException;
}
