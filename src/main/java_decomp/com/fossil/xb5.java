package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xb5 extends Wb5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131361851, 1);
        y.put(2131363410, 2);
        y.put(2131363471, 3);
        y.put(2131363175, 4);
        y.put(2131363497, 5);
    }
    */

    @DexIgnore
    public Xb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 6, x, y));
    }

    @DexIgnore
    public Xb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ImageView) objArr[1], (TabLayout) objArr[4], (FlexibleTextView) objArr[2], (View) objArr[3], (ViewPager) objArr[5]);
        this.w = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.v = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.w != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.w = 1;
        }
        w();
    }
}
