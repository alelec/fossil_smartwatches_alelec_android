package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Em6 implements Factory<GoalTrackingDetailPresenter> {
    @DexIgnore
    public static GoalTrackingDetailPresenter a(Zl6 zl6, GoalTrackingRepository goalTrackingRepository, U04 u04) {
        return new GoalTrackingDetailPresenter(zl6, goalTrackingRepository, u04);
    }
}
