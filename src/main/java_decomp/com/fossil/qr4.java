package com.fossil;

import com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qr4 implements Factory<FlagRepository> {
    @DexIgnore
    public /* final */ Provider<Nr4> a;
    @DexIgnore
    public /* final */ Provider<FlagRemoteDataSource> b;

    @DexIgnore
    public Qr4(Provider<Nr4> provider, Provider<FlagRemoteDataSource> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Qr4 a(Provider<Nr4> provider, Provider<FlagRemoteDataSource> provider2) {
        return new Qr4(provider, provider2);
    }

    @DexIgnore
    public static FlagRepository c(Nr4 nr4, FlagRemoteDataSource flagRemoteDataSource) {
        return new FlagRepository(nr4, flagRemoteDataSource);
    }

    @DexIgnore
    public FlagRepository b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
