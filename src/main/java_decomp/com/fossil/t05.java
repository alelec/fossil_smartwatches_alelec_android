package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class t05 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon11 b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ List d;
    @DexIgnore
    public /* final */ /* synthetic */ PresetDataSource.GetRecommendedPresetListCallback e;

    @DexIgnore
    public /* synthetic */ t05(PresetRepository.Anon11 anon11, String str, List list, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        this.b = anon11;
        this.c = str;
        this.d = list;
        this.e = getRecommendedPresetListCallback;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d, this.e);
    }
}
