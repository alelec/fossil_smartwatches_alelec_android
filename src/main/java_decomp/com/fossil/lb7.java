package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Lb7 {
    STEP,
    ACTIVE,
    BATTERY,
    CALORIES,
    RAIN,
    DATE,
    SECOND_TIME,
    WEATHER,
    HEART,
    TICKER,
    TEXT;
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Lb7 a(String str) {
            Wg6.c(str, "id");
            switch (str.hashCode()) {
                case -331239923:
                    if (str.equals(Constants.BATTERY)) {
                        return Lb7.BATTERY;
                    }
                    break;
                case -168965370:
                    if (str.equals("calories")) {
                        return Lb7.CALORIES;
                    }
                    break;
                case -85386984:
                    if (str.equals("active-minutes")) {
                        return Lb7.ACTIVE;
                    }
                    break;
                case -48173007:
                    if (str.equals("chance-of-rain")) {
                        return Lb7.RAIN;
                    }
                    break;
                case 3076014:
                    if (str.equals("date")) {
                        return Lb7.DATE;
                    }
                    break;
                case 109761319:
                    if (str.equals("steps")) {
                        return Lb7.STEP;
                    }
                    break;
                case 134170930:
                    if (str.equals("second-timezone")) {
                        return Lb7.SECOND_TIME;
                    }
                    break;
                case 1223440372:
                    if (str.equals("weather")) {
                        return Lb7.WEATHER;
                    }
                    break;
                case 1884273159:
                    if (str.equals("heart-rate")) {
                        return Lb7.HEART;
                    }
                    break;
            }
            return Lb7.STEP;
        }

        @DexIgnore
        public final String b(Lb7 lb7) {
            Wg6.c(lb7, "type");
            switch (Kb7.a[lb7.ordinal()]) {
                case 1:
                    return "steps";
                case 2:
                    return "active-minutes";
                case 3:
                    return Constants.BATTERY;
                case 4:
                    return "calories";
                case 5:
                    return "chance-of-rain";
                case 6:
                    return "date";
                case 7:
                    return "second-timezone";
                case 8:
                    return "weather";
                case 9:
                    return "heart-rate";
                default:
                    return "empty";
            }
        }
    }
}
