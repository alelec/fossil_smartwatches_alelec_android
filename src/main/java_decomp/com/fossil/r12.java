package com.fossil;

import com.fossil.V12;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R12 extends V12 {
    @DexIgnore
    public /* final */ T32 a;
    @DexIgnore
    public /* final */ Map<Vy1, V12.Bi> b;

    @DexIgnore
    public R12(T32 t32, Map<Vy1, V12.Bi> map) {
        if (t32 != null) {
            this.a = t32;
            if (map != null) {
                this.b = map;
                return;
            }
            throw new NullPointerException("Null values");
        }
        throw new NullPointerException("Null clock");
    }

    @DexIgnore
    @Override // com.fossil.V12
    public T32 d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof V12)) {
            return false;
        }
        V12 v12 = (V12) obj;
        return this.a.equals(v12.d()) && this.b.equals(v12.g());
    }

    @DexIgnore
    @Override // com.fossil.V12
    public Map<Vy1, V12.Bi> g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SchedulerConfig{clock=" + this.a + ", values=" + this.b + "}";
    }
}
