package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import io.flutter.plugin.common.StandardMessageCodec;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hy1 {
    @DexIgnore
    public static final int a(Gr7 gr7) {
        Wg6.c(gr7, "$this$MAX_UNSIGNED_VALUE");
        return 65535;
    }

    @DexIgnore
    public static final long b(Oq7 oq7) {
        Wg6.c(oq7, "$this$MAX_UNSIGNED_VALUE");
        return 4294967295L;
    }

    @DexIgnore
    public static final short c(Fq7 fq7) {
        Wg6.c(fq7, "$this$MAX_UNSIGNED_VALUE");
        return 255;
    }

    @DexIgnore
    public static final short d(Fq7 fq7) {
        Wg6.c(fq7, "$this$MIN_UNSIGNED_VALUE");
        return 0;
    }

    @DexIgnore
    public static final float e(float f, int i) {
        float pow = (float) Math.pow((double) 10.0f, (double) i);
        return ((float) Lr7.d(f * pow)) / pow;
    }

    @DexIgnore
    public static final double f(long j) {
        return ((double) j) / ((double) 1000);
    }

    @DexIgnore
    public static final String g(byte b, Locale locale) {
        Wg6.c(locale, "locale");
        Ct7.a(16);
        String num = Integer.toString(b & 255, 16);
        Wg6.b(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        if (num != null) {
            String upperCase = num.toUpperCase(locale);
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Wt7.O(upperCase, 2, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String h(int i, Locale locale) {
        Wg6.c(locale, "locale");
        Ct7.a(16);
        String l = Long.toString(((long) i) & 4294967295L, 16);
        Wg6.b(l, "java.lang.Long.toString(this, checkRadix(radix))");
        if (l != null) {
            String upperCase = l.toUpperCase(locale);
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Wt7.O(upperCase, 8, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String i(short s, Locale locale) {
        Wg6.c(locale, "locale");
        Ct7.a(16);
        String num = Integer.toString(65535 & s, 16);
        Wg6.b(num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        if (num != null) {
            String upperCase = num.toUpperCase(locale);
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return Wt7.O(upperCase, 4, '0');
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static /* synthetic */ String j(byte b, Locale locale, int i, Object obj) {
        if ((i & 1) != 0) {
            locale = Dx1.b();
        }
        return g(b, locale);
    }

    @DexIgnore
    public static /* synthetic */ String k(int i, Locale locale, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            locale = Dx1.b();
        }
        return h(i, locale);
    }

    @DexIgnore
    public static /* synthetic */ String l(short s, Locale locale, int i, Object obj) {
        if ((i & 1) != 0) {
            locale = Dx1.b();
        }
        return i(s, locale);
    }

    @DexIgnore
    public static final String m(long j, boolean z) {
        return z ? String.valueOf(((double) j) / ((double) 1000)) : String.valueOf(j / ((long) 1000));
    }

    @DexIgnore
    public static final int n(short s) {
        return s < 0 ? s + 65536 : s;
    }

    @DexIgnore
    public static final long o(int i) {
        return i < 0 ? ((long) i) + 4294967296L : (long) i;
    }

    @DexIgnore
    public static final short p(byte b) {
        return b < 0 ? (short) (b + StandardMessageCodec.NULL) : (short) b;
    }
}
