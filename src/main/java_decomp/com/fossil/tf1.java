package com.fossil;

import android.content.Context;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tf1<T> implements Sb1<T> {
    @DexIgnore
    public static /* final */ Sb1<?> b; // = new Tf1();

    @DexIgnore
    public static <T> Tf1<T> c() {
        return (Tf1) b;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
    }

    @DexIgnore
    @Override // com.fossil.Sb1
    public Id1<T> b(Context context, Id1<T> id1, int i, int i2) {
        return id1;
    }
}
