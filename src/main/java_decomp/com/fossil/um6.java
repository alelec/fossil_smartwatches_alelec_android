package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.an6;
import com.fossil.bn6;
import com.fossil.n04;
import com.fossil.nk5;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um6 extends pv5 implements tm6, View.OnClickListener {
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public /* final */ String A;
    @DexIgnore
    public /* final */ String B;
    @DexIgnore
    public HashMap C;
    @DexIgnore
    public /* final */ Calendar g; // = Calendar.getInstance();
    @DexIgnore
    public g37<fb5> h;
    @DexIgnore
    public sm6 i;
    @DexIgnore
    public bn6 j;
    @DexIgnore
    public an6 k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final um6 a(Date date) {
            pq7.c(date, "date");
            um6 um6 = new um6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            um6.setArguments(bundle);
            return um6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ um6 f3615a;

        @DexIgnore
        public b(um6 um6, fb5 fb5) {
            this.f3615a = um6;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.f3615a.A) && !TextUtils.isEmpty(this.f3615a.B)) {
                int parseColor = Color.parseColor(this.f3615a.A);
                int parseColor2 = Color.parseColor(this.f3615a.B);
                gVar.o(2131230966);
                if (i == this.f3615a.l) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ um6 f3616a;
        @DexIgnore
        public /* final */ /* synthetic */ fb5 b;

        @DexIgnore
        public c(um6 um6, fb5 fb5) {
            this.f3616a = um6;
            this.b = fb5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "onPageScrolled " + i);
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.f3616a.B)) {
                int parseColor = Color.parseColor(this.f3616a.B);
                TabLayout.g v = this.b.w.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.f3616a.A) && this.f3616a.l != i) {
                int parseColor2 = Color.parseColor(this.f3616a.A);
                TabLayout.g v2 = this.b.w.v(this.f3616a.l);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.f3616a.l = i;
            ViewPager2 viewPager2 = this.b.O;
            pq7.b(viewPager2, "binding.rvHeartRateSleep");
            viewPager2.setCurrentItem(i);
        }
    }

    @DexIgnore
    public um6() {
        String d = qn5.l.a().d("nonBrandSurface");
        this.u = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = qn5.l.a().d("backgroundDashboard");
        this.v = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("secondaryText");
        this.w = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d("primaryText");
        this.x = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = qn5.l.a().d("nonBrandDisableCalendarDay");
        this.y = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = qn5.l.a().d("nonBrandNonReachGoal");
        this.z = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        this.A = qn5.l.a().d("disabledButton");
        this.B = qn5.l.a().d("primaryColor");
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "SleepDetailFragment";
    }

    @DexIgnore
    @Override // com.fossil.tm6
    public void H0() {
        fb5 a2;
        ConstraintLayout constraintLayout;
        g37<fb5> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (constraintLayout = a2.r) != null) {
            constraintLayout.setVisibility(8);
        }
    }

    @DexIgnore
    public final void O6(fb5 fb5) {
        fb5.H.setOnClickListener(this);
        fb5.I.setOnClickListener(this);
        fb5.K.setOnClickListener(this);
        fb5.r.setBackgroundColor(this.v);
        fb5.q.setBackgroundColor(this.u);
        fb5.u.setBackgroundColor(this.u);
        nk5.a aVar = nk5.o;
        sm6 sm6 = this.i;
        this.m = aVar.w(sm6 != null ? sm6.n() : null) ? qn5.l.a().d("dianaSleepTab") : qn5.l.a().d("hybridSleepTab");
        this.s = qn5.l.a().d("nonBrandActivityDetailBackground");
        this.t = qn5.l.a().d("onDianaSleepTab");
        this.j = new bn6(new ArrayList());
        ViewPager2 viewPager2 = fb5.P;
        pq7.b(viewPager2, "rvSleeps");
        viewPager2.setAdapter(this.j);
        TabLayout tabLayout = fb5.w;
        pq7.b(tabLayout, "binding.cpiSleep");
        tabLayout.setBackgroundTintList(ColorStateList.valueOf(this.u));
        new n04(fb5.w, fb5.P, new b(this, fb5)).a();
        viewPager2.g(new c(this, fb5));
        viewPager2.setCurrentItem(this.l);
        this.k = new an6(new ArrayList());
        ViewPager2 viewPager22 = fb5.O;
        pq7.b(viewPager22, "rvHeartRateSleep");
        viewPager22.setAdapter(this.k);
        viewPager22.setCurrentItem(this.l);
        viewPager22.setUserInputEnabled(true);
    }

    @DexIgnore
    @Override // com.fossil.tm6
    public void P5(MFSleepDay mFSleepDay) {
        fb5 a2;
        int i2;
        int i3;
        FLogger.INSTANCE.getLocal().d("SleepDetailFragment", "showDayDetail - sleepDay=" + mFSleepDay);
        g37<fb5> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            pq7.b(a2, "binding");
            View n = a2.n();
            pq7.b(n, "binding.root");
            Context context = n.getContext();
            if (mFSleepDay != null) {
                i3 = mFSleepDay.getSleepMinutes();
                i2 = mFSleepDay.getGoalMinutes();
            } else {
                i2 = 0;
                i3 = 0;
            }
            if (i3 > 0) {
                int i4 = i3 / 60;
                int i5 = i3 - (i4 * 60);
                FlexibleTextView flexibleTextView = a2.z;
                pq7.b(flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(String.valueOf(i4));
                FlexibleTextView flexibleTextView2 = a2.x;
                pq7.b(flexibleTextView2, "binding.ftvDailyUnit");
                String c2 = um5.c(context, 2131886888);
                pq7.b(c2, "LanguageHelper.getString\u2026Abbreviations_Hours__Hrs)");
                if (c2 != null) {
                    String lowerCase = c2.toLowerCase();
                    pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                    if (i5 > 0) {
                        FlexibleTextView flexibleTextView3 = a2.A;
                        pq7.b(flexibleTextView3, "binding.ftvDailyValue2");
                        hr7 hr7 = hr7.f1520a;
                        Locale locale = Locale.US;
                        pq7.b(locale, "Locale.US");
                        String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
                        pq7.b(format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView3.setText(format);
                        FlexibleTextView flexibleTextView4 = a2.y;
                        pq7.b(flexibleTextView4, "binding.ftvDailyUnit2");
                        String c3 = um5.c(context, 2131886890);
                        pq7.b(c3, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                        if (c3 != null) {
                            String lowerCase2 = c3.toLowerCase();
                            pq7.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                            flexibleTextView4.setText(lowerCase2);
                        } else {
                            throw new il7("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        FlexibleTextView flexibleTextView5 = a2.A;
                        pq7.b(flexibleTextView5, "binding.ftvDailyValue2");
                        flexibleTextView5.setText("");
                        FlexibleTextView flexibleTextView6 = a2.y;
                        pq7.b(flexibleTextView6, "binding.ftvDailyUnit2");
                        flexibleTextView6.setText("");
                    }
                    ViewPager2 viewPager2 = a2.P;
                    pq7.b(viewPager2, "binding.rvSleeps");
                    viewPager2.setVisibility(0);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView7 = a2.z;
                pq7.b(flexibleTextView7, "binding.ftvDailyValue");
                flexibleTextView7.setText("");
                FlexibleTextView flexibleTextView8 = a2.x;
                pq7.b(flexibleTextView8, "binding.ftvDailyUnit");
                String c4 = um5.c(context, 2131886717);
                pq7.b(c4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (c4 != null) {
                    String upperCase = c4.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView8.setText(upperCase);
                    FlexibleTextView flexibleTextView9 = a2.A;
                    pq7.b(flexibleTextView9, "binding.ftvDailyValue2");
                    flexibleTextView9.setText("");
                    FlexibleTextView flexibleTextView10 = a2.y;
                    pq7.b(flexibleTextView10, "binding.ftvDailyUnit2");
                    flexibleTextView10.setText("");
                    ViewPager2 viewPager22 = a2.P;
                    pq7.b(viewPager22, "binding.rvSleeps");
                    viewPager22.setVisibility(8);
                    TabLayout tabLayout = a2.w;
                    pq7.b(tabLayout, "binding.cpiSleep");
                    tabLayout.setVisibility(8);
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i6 = i2 > 0 ? (i3 * 100) / i2 : -1;
            if (i3 >= i2 && i2 > 0) {
                a2.C.setTextColor(this.u);
                a2.B.setTextColor(this.u);
                a2.x.setTextColor(this.u);
                a2.z.setTextColor(this.u);
                a2.y.setTextColor(this.u);
                a2.A.setTextColor(this.u);
                RTLImageView rTLImageView = a2.K;
                pq7.b(rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.I;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                pq7.b(constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView11 = a2.C;
                pq7.b(flexibleTextView11, "binding.ftvDayOfWeek");
                flexibleTextView11.setSelected(true);
                FlexibleTextView flexibleTextView12 = a2.B;
                pq7.b(flexibleTextView12, "binding.ftvDayOfMonth");
                flexibleTextView12.setSelected(true);
                View view = a2.L;
                pq7.b(view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView13 = a2.z;
                pq7.b(flexibleTextView13, "binding.ftvDailyValue");
                flexibleTextView13.setSelected(true);
                FlexibleTextView flexibleTextView14 = a2.x;
                pq7.b(flexibleTextView14, "binding.ftvDailyUnit");
                flexibleTextView14.setSelected(true);
                String str = this.t;
                if (str != null) {
                    a2.C.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.L.setBackgroundColor(Color.parseColor(str));
                    a2.K.setColorFilter(Color.parseColor(str));
                    a2.I.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.m;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i3 > 0) {
                a2.B.setTextColor(this.x);
                a2.C.setTextColor(this.w);
                a2.x.setTextColor(this.z);
                a2.z.setTextColor(this.x);
                a2.y.setTextColor(this.z);
                a2.A.setTextColor(this.x);
                View view2 = a2.L;
                pq7.b(view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.K;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.I;
                pq7.b(rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i7 = this.z;
                a2.L.setBackgroundColor(i7);
                a2.K.setColorFilter(i7);
                a2.I.setColorFilter(i7);
                String str3 = this.s;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.B.setTextColor(this.x);
                a2.C.setTextColor(this.w);
                a2.z.setTextColor(this.y);
                a2.x.setTextColor(this.y);
                a2.y.setTextColor(this.y);
                a2.A.setTextColor(this.y);
                RTLImageView rTLImageView5 = a2.K;
                pq7.b(rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.I;
                pq7.b(rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                pq7.b(constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView15 = a2.C;
                pq7.b(flexibleTextView15, "binding.ftvDayOfWeek");
                flexibleTextView15.setSelected(false);
                FlexibleTextView flexibleTextView16 = a2.B;
                pq7.b(flexibleTextView16, "binding.ftvDayOfMonth");
                flexibleTextView16.setSelected(false);
                View view3 = a2.L;
                pq7.b(view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView17 = a2.z;
                pq7.b(flexibleTextView17, "binding.ftvDailyValue");
                flexibleTextView17.setSelected(false);
                FlexibleTextView flexibleTextView18 = a2.x;
                pq7.b(flexibleTextView18, "binding.ftvDailyUnit");
                flexibleTextView18.setSelected(false);
                int i8 = this.z;
                a2.L.setBackgroundColor(i8);
                a2.K.setColorFilter(i8);
                a2.I.setColorFilter(i8);
                String str4 = this.s;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i6 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.M;
                pq7.b(flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView19 = a2.G;
                pq7.b(flexibleTextView19, "binding.ftvProgressValue");
                flexibleTextView19.setText(um5.c(context, 2131887328));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.M;
                pq7.b(flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i6);
                FlexibleTextView flexibleTextView20 = a2.G;
                pq7.b(flexibleTextView20, "binding.ftvProgressValue");
                flexibleTextView20.setText(i6 + "%");
            }
            if (i2 < 60) {
                FlexibleTextView flexibleTextView21 = a2.D;
                pq7.b(flexibleTextView21, "binding.ftvGoalValue");
                hr7 hr72 = hr7.f1520a;
                String c5 = um5.c(context, 2131886614);
                pq7.b(c5, "LanguageHelper.getString\u2026Page_Title__OfNumberMins)");
                String format2 = String.format(c5, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
                pq7.b(format2, "java.lang.String.format(format, *args)");
                flexibleTextView21.setText(format2);
                return;
            }
            FlexibleTextView flexibleTextView22 = a2.D;
            pq7.b(flexibleTextView22, "binding.ftvGoalValue");
            hr7 hr73 = hr7.f1520a;
            String c6 = um5.c(context, 2131886779);
            pq7.b(c6, "LanguageHelper.getString\u2026ecord_Title__OfNumberHrs)");
            String format3 = String.format(c6, Arrays.copyOf(new Object[]{dl5.b(((float) i2) / 60.0f, 1)}, 1));
            pq7.b(format3, "java.lang.String.format(format, *args)");
            flexibleTextView22.setText(format3);
        }
    }

    @DexIgnore
    /* renamed from: P6 */
    public void M5(sm6 sm6) {
        pq7.c(sm6, "presenter");
        this.i = sm6;
    }

    @DexIgnore
    @Override // com.fossil.tm6
    public void j(Date date, boolean z2, boolean z3, boolean z4) {
        fb5 a2;
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDay - date=" + date + ", isCreateAt=" + z2 + ", isToday=" + z3 + ", isDateAfter=" + z4);
        Calendar calendar = this.g;
        pq7.b(calendar, "mCalendar");
        calendar.setTime(date);
        int i2 = this.g.get(7);
        g37<fb5> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            FlexibleTextView flexibleTextView = a2.B;
            pq7.b(flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(this.g.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.I;
                pq7.b(rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.I;
                pq7.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.K;
                pq7.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    pq7.b(flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(um5.c(getContext(), 2131886662));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.C;
                pq7.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(jl5.b.i(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.K;
            pq7.b(rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.C;
            pq7.b(flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(jl5.b.i(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.tm6
    public void o2(List<bn6.b> list) {
        fb5 a2;
        TabLayout tabLayout;
        fb5 a3;
        TabLayout tabLayout2;
        pq7.c(list, "listOfSleepSessionUIData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailFragment", "showDayDetailChart - baseModel=" + list);
        if (list.size() > 1) {
            g37<fb5> g37 = this.h;
            if (!(g37 == null || (a3 = g37.a()) == null || (tabLayout2 = a3.w) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            g37<fb5> g372 = this.h;
            if (!(g372 == null || (a2 = g372.a()) == null || (tabLayout = a2.w) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        bn6 bn6 = this.j;
        if (bn6 != null) {
            bn6.i(list);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("SleepDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    sm6 sm6 = this.i;
                    if (sm6 != null) {
                        sm6.r();
                        return;
                    }
                    return;
                case 2131362735:
                    sm6 sm62 = this.i;
                    if (sm62 != null) {
                        sm62.q();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        fb5 a2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        fb5 fb5 = (fb5) aq0.f(layoutInflater, 2131558623, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            pq7.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        Date date = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            date = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Calendar calendar = this.g;
        pq7.b(calendar, "mCalendar");
        calendar.setTime(date);
        pq7.b(fb5, "binding");
        O6(fb5);
        g37<fb5> g37 = new g37<>(this, fb5);
        this.h = g37;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        sm6 sm6 = this.i;
        if (sm6 != null) {
            sm6.m();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        sm6 sm6 = this.i;
        if (sm6 != null) {
            Calendar calendar = this.g;
            pq7.b(calendar, "mCalendar");
            Date time = calendar.getTime();
            pq7.b(time, "mCalendar.time");
            sm6.p(time);
        }
        sm6 sm62 = this.i;
        if (sm62 != null) {
            sm62.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        pq7.c(bundle, "outState");
        sm6 sm6 = this.i;
        if (sm6 != null) {
            sm6.o(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.fossil.tm6
    public void u5(ArrayList<an6.a> arrayList) {
        fb5 a2;
        ConstraintLayout constraintLayout;
        pq7.c(arrayList, "sleepHeartRateDatumData");
        g37<fb5> g37 = this.h;
        if (!(g37 == null || (a2 = g37.a()) == null || (constraintLayout = a2.r) == null)) {
            constraintLayout.setVisibility(0);
        }
        an6 an6 = this.k;
        if (an6 != null) {
            an6.i(arrayList);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.C;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
