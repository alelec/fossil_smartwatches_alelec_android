package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C11 {
    @DexIgnore
    public static /* final */ int enable_system_alarm_service_default; // = 2131034115;
    @DexIgnore
    public static /* final */ int enable_system_foreground_service_default; // = 2131034116;
    @DexIgnore
    public static /* final */ int enable_system_job_service_default; // = 2131034117;
    @DexIgnore
    public static /* final */ int workmanager_test_configuration; // = 2131034119;
}
