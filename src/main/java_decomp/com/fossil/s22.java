package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class S22 implements J32.Bi {
    @DexIgnore
    public /* final */ J32 a;
    @DexIgnore
    public /* final */ H02 b;

    @DexIgnore
    public S22(J32 j32, H02 h02) {
        this.a = j32;
        this.b = h02;
    }

    @DexIgnore
    public static J32.Bi a(J32 j32, H02 h02) {
        return new S22(j32, h02);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.Q(this.a, this.b, (SQLiteDatabase) obj);
    }
}
