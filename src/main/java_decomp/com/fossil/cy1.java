package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cy1 {
    @DexIgnore
    public static final byte[] a(Bitmap bitmap, Bitmap.CompressFormat compressFormat) {
        Wg6.c(bitmap, "$this$toByteArray");
        Wg6.c(compressFormat, "compressFormat");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "stream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public static /* synthetic */ byte[] b(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int i, Object obj) {
        if ((i & 1) != 0) {
            compressFormat = Bitmap.CompressFormat.PNG;
        }
        return a(bitmap, compressFormat);
    }
}
