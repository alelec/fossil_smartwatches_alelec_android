package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X2 implements Parcelable.Creator<Z1> {
    @DexIgnore
    public /* synthetic */ X2(Qg6 qg6) {
    }

    @DexIgnore
    public Z1 a(Parcel parcel) {
        return new Z1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Z1 createFromParcel(Parcel parcel) {
        return new Z1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Z1[] newArray(int i) {
        return new Z1[i];
    }
}
