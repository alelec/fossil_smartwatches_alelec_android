package com.fossil;

import com.fossil.bu0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xt0<Key, Value> {
    @DexIgnore
    public AtomicBoolean mInvalid; // = new AtomicBoolean(false);
    @DexIgnore
    public CopyOnWriteArrayList<c> mOnInvalidatedCallbacks; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements gi0<List<X>, List<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ gi0 f4170a;

        @DexIgnore
        public a(gi0 gi0) {
            this.f4170a = gi0;
        }

        @DexIgnore
        /* renamed from: a */
        public List<Y> apply(List<X> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.f4170a.apply(list.get(i)));
            }
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<Key, Value> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends b<Key, ToValue> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ gi0 f4171a;

            @DexIgnore
            public a(gi0 gi0) {
                this.f4171a = gi0;
            }

            @DexIgnore
            @Override // com.fossil.xt0.b
            public xt0<Key, ToValue> create() {
                return b.this.create().mapByPage(this.f4171a);
            }
        }

        @DexIgnore
        public abstract xt0<Key, Value> create();

        @DexIgnore
        public <ToValue> b<Key, ToValue> map(gi0<Value, ToValue> gi0) {
            return mapByPage(xt0.createListFunction(gi0));
        }

        @DexIgnore
        public <ToValue> b<Key, ToValue> mapByPage(gi0<List<Value>, List<ToValue>> gi0) {
            return new a(gi0);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f4172a;
        @DexIgnore
        public /* final */ xt0 b;
        @DexIgnore
        public /* final */ bu0.a<T> c;
        @DexIgnore
        public /* final */ Object d; // = new Object();
        @DexIgnore
        public Executor e; // = null;
        @DexIgnore
        public boolean f; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ bu0 b;

            @DexIgnore
            public a(bu0 bu0) {
                this.b = bu0;
            }

            @DexIgnore
            public void run() {
                d dVar = d.this;
                dVar.c.a(dVar.f4172a, this.b);
            }
        }

        @DexIgnore
        public d(xt0 xt0, int i, Executor executor, bu0.a<T> aVar) {
            this.b = xt0;
            this.f4172a = i;
            this.e = executor;
            this.c = aVar;
        }

        @DexIgnore
        public static void d(List<?> list, int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        public boolean a() {
            if (!this.b.isInvalid()) {
                return false;
            }
            b(bu0.b());
            return true;
        }

        @DexIgnore
        public void b(bu0<T> bu0) {
            Executor executor;
            synchronized (this.d) {
                if (!this.f) {
                    this.f = true;
                    executor = this.e;
                } else {
                    throw new IllegalStateException("callback.onResult already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new a(bu0));
            } else {
                this.c.a(this.f4172a, bu0);
            }
        }

        @DexIgnore
        public void c(Executor executor) {
            synchronized (this.d) {
                this.e = executor;
            }
        }
    }

    @DexIgnore
    public static <A, B> List<B> convert(gi0<List<A>, List<B>> gi0, List<A> list) {
        List<B> apply = gi0.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new IllegalStateException("Invalid Function " + gi0 + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> gi0<List<X>, List<Y>> createListFunction(gi0<X, Y> gi0) {
        return new a(gi0);
    }

    @DexIgnore
    public void addInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.add(cVar);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            Iterator<c> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> xt0<Key, ToValue> map(gi0<Value, ToValue> gi0);

    @DexIgnore
    public abstract <ToValue> xt0<Key, ToValue> mapByPage(gi0<List<Value>, List<ToValue>> gi0);

    @DexIgnore
    public void removeInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.remove(cVar);
    }
}
