package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Me3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Me3> CREATOR; // = new Ef3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Float c;

    @DexIgnore
    public Me3(int i, Float f) {
        boolean z = true;
        if (i != 1 && (f == null || f.floatValue() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            z = false;
        }
        String valueOf = String.valueOf(f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 45);
        sb.append("Invalid PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        Rc2.b(z, sb.toString());
        this.b = i;
        this.c = f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Me3)) {
            return false;
        }
        Me3 me3 = (Me3) obj;
        return this.b == me3.b && Pc2.a(this.c, me3.c);
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(Integer.valueOf(this.b), this.c);
    }

    @DexIgnore
    public String toString() {
        int i = this.b;
        String valueOf = String.valueOf(this.c);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 39);
        sb.append("[PatternItem: type=");
        sb.append(i);
        sb.append(" length=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 2, this.b);
        Bd2.l(parcel, 3, this.c, false);
        Bd2.b(parcel, a2);
    }
}
