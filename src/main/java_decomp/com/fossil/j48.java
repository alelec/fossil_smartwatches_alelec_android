package com.fossil;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface J48 extends A58, WritableByteChannel {
    @DexIgnore
    J48 E(String str) throws IOException;

    @DexIgnore
    J48 J(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    long N(C58 c58) throws IOException;

    @DexIgnore
    J48 O(long j) throws IOException;

    @DexIgnore
    J48 Z(byte[] bArr) throws IOException;

    @DexIgnore
    J48 a0(L48 l48) throws IOException;

    @DexIgnore
    I48 d();

    @DexIgnore
    @Override // com.fossil.A58, java.io.Flushable
    void flush() throws IOException;

    @DexIgnore
    J48 k0(long j) throws IOException;

    @DexIgnore
    OutputStream l0();

    @DexIgnore
    J48 n(int i) throws IOException;

    @DexIgnore
    J48 p(int i) throws IOException;

    @DexIgnore
    J48 v(int i) throws IOException;

    @DexIgnore
    J48 x() throws IOException;
}
