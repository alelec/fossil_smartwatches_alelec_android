package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V88<T> implements E88<T, RequestBody> {
    @DexIgnore
    public static /* final */ R18 c; // = R18.c("application/json; charset=UTF-8");
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public V88(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ RequestBody a(Object obj) throws IOException {
        return b(obj);
    }

    @DexIgnore
    public RequestBody b(T t) throws IOException {
        I48 i48 = new I48();
        JsonWriter r = this.a.r(new OutputStreamWriter(i48.l0(), d));
        this.b.write(r, t);
        r.close();
        return RequestBody.e(c, i48.S());
    }
}
