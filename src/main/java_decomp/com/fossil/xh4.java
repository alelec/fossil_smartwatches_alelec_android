package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xh4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Sd4<Xh4> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Xh4) obj, td4);
        }

        @DexIgnore
        public void b(Xh4 xh4, Td4 td4) throws Rd4, IOException {
            Intent b = xh4.b();
            td4.c("ttl", Ai4.q(b));
            td4.f(Constants.EVENT, xh4.a());
            td4.f("instanceId", Ai4.e());
            td4.c("priority", Ai4.n(b));
            td4.f("packageName", Ai4.m());
            td4.f("sdkPlatform", "ANDROID");
            td4.f("messageType", Ai4.k(b));
            String g = Ai4.g(b);
            if (g != null) {
                td4.f("messageId", g);
            }
            String p = Ai4.p(b);
            if (p != null) {
                td4.f("topic", p);
            }
            String b2 = Ai4.b(b);
            if (b2 != null) {
                td4.f("collapseKey", b2);
            }
            if (Ai4.h(b) != null) {
                td4.f("analyticsLabel", Ai4.h(b));
            }
            if (Ai4.d(b) != null) {
                td4.f("composerLabel", Ai4.d(b));
            }
            String o = Ai4.o();
            if (o != null) {
                td4.f("projectNumber", o);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Xh4 a;

        @DexIgnore
        public Bi(Xh4 xh4) {
            Rc2.k(xh4);
            this.a = xh4;
        }

        @DexIgnore
        public final Xh4 a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sd4<Bi> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public final /* bridge */ /* synthetic */ void a(Object obj, Td4 td4) throws IOException {
            b((Bi) obj, td4);
        }

        @DexIgnore
        public final void b(Bi bi, Td4 td4) throws Rd4, IOException {
            td4.f("messaging_client_event", bi.a());
        }
    }

    @DexIgnore
    public Xh4(String str, Intent intent) {
        Rc2.h(str, "evenType must be non-null");
        this.a = str;
        Rc2.l(intent, "intent must be non-null");
        this.b = intent;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final Intent b() {
        return this.b;
    }
}
