package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E55 extends D55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public /* final */ NestedScrollView B;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362095, 1);
        E.put(2131363380, 2);
        E.put(2131363498, 3);
        E.put(2131362708, 4);
        E.put(2131362105, 5);
        E.put(2131363389, 6);
        E.put(2131363519, 7);
        E.put(2131362710, 8);
        E.put(2131363378, 9);
        E.put(2131363379, 10);
        E.put(2131362262, 11);
    }
    */

    @DexIgnore
    public E55(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public E55(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[5], (FlexibleButton) objArr[11], (ImageView) objArr[4], (ImageView) objArr[8], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[7]);
        this.C = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.B = nestedScrollView;
        nestedScrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
