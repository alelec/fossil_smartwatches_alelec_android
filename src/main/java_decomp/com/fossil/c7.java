package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C7 extends H7 {
    @DexIgnore
    public /* final */ UUID[] b;
    @DexIgnore
    public /* final */ N6[] c;

    @DexIgnore
    public C7(G7 g7, UUID[] uuidArr, N6[] n6Arr) {
        super(g7);
        this.b = uuidArr;
        this.c = n6Arr;
    }
}
