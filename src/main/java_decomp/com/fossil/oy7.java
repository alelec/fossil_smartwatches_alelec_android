package com.fossil;

import com.mapped.Rc6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Oy7 {
    @DexIgnore
    public static final <E> Object a(Vy7<? extends E> vy7, Xe6<? super E> xe6) {
        if (vy7 != null) {
            return vy7.a(xe6);
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.channels.ReceiveChannel<E?>");
    }
}
