package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ue implements Parcelable.Creator<Ve> {
    @DexIgnore
    public /* synthetic */ Ue(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ve createFromParcel(Parcel parcel) {
        return new Ve(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Ve[] newArray(int i) {
        return new Ve[i];
    }
}
