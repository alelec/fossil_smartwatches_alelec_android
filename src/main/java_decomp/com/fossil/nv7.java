package com.fossil;

import java.util.concurrent.atomic.AtomicLong;

public final class Nv7 {
    public static final boolean a = Gv7.class.desiredAssertionStatus();
    public static final boolean b;
    public static final boolean c;
    public static final AtomicLong d = new AtomicLong(0);

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0047, code lost:
        if (r0.equals("auto") != false) goto L_0x0012;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0079, code lost:
        if (r0.equals("on") != false) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x007b, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0083, code lost:
        if (r0.equals("") != false) goto L_0x007b;
     */
    /*
    static {
        /*
            r1 = 0
            r2 = 1
            java.lang.Class<com.fossil.Gv7> r0 = com.fossil.Gv7.class
            boolean r0 = r0.desiredAssertionStatus()
            com.fossil.Nv7.a = r0
            java.lang.String r0 = "kotlinx.coroutines.debug"
            java.lang.String r0 = com.fossil.Wz7.d(r0)
            if (r0 != 0) goto L_0x002d
        L_0x0012:
            boolean r0 = com.fossil.Nv7.a
        L_0x0014:
            com.fossil.Nv7.b = r0
            if (r0 == 0) goto L_0x0021
            java.lang.String r0 = "kotlinx.coroutines.stacktrace.recovery"
            boolean r0 = com.fossil.Wz7.e(r0, r2)
            if (r0 == 0) goto L_0x0021
            r1 = r2
        L_0x0021:
            com.fossil.Nv7.c = r1
            java.util.concurrent.atomic.AtomicLong r0 = new java.util.concurrent.atomic.AtomicLong
            r2 = 0
            r0.<init>(r2)
            com.fossil.Nv7.d = r0
            return
        L_0x002d:
            int r3 = r0.hashCode()
            if (r3 == 0) goto L_0x007d
            r4 = 3551(0xddf, float:4.976E-42)
            if (r3 == r4) goto L_0x0073
            r4 = 109935(0x1ad6f, float:1.54052E-40)
            if (r3 == r4) goto L_0x0069
            r4 = 3005871(0x2dddaf, float:4.212122E-39)
            if (r3 != r4) goto L_0x0049
            java.lang.String r3 = "auto"
            boolean r3 = r0.equals(r3)
            if (r3 != 0) goto L_0x0012
        L_0x0049:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "System property 'kotlinx.coroutines.debug' has unrecognized value '"
            r1.append(r2)
            r1.append(r0)
            r0 = 39
            r1.append(r0)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0069:
            java.lang.String r3 = "off"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0049
            r0 = r1
            goto L_0x0014
        L_0x0073:
            java.lang.String r3 = "on"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0049
        L_0x007b:
            r0 = r2
            goto L_0x0014
        L_0x007d:
            java.lang.String r3 = ""
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x0049
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Nv7.<clinit>():void");
    }
    */

    public static final boolean a() {
        return a;
    }

    public static final AtomicLong b() {
        return d;
    }

    public static final boolean c() {
        return b;
    }

    public static final boolean d() {
        return c;
    }
}
