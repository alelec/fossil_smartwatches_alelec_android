package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jn3 implements Ln3 {
    @DexIgnore
    public /* final */ Pm3 a;

    @DexIgnore
    public Jn3(Pm3 pm3) {
        Rc2.k(pm3);
        this.a = pm3;
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public Yr3 b() {
        return this.a.b();
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public Im3 c() {
        return this.a.c();
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public Kl3 d() {
        return this.a.d();
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public Context e() {
        return this.a.e();
    }

    @DexIgnore
    public void f() {
        this.a.q();
    }

    @DexIgnore
    public void g() {
        this.a.c().g();
    }

    @DexIgnore
    public void h() {
        this.a.c().h();
    }

    @DexIgnore
    public Pg3 i() {
        return this.a.P();
    }

    @DexIgnore
    public Il3 j() {
        return this.a.G();
    }

    @DexIgnore
    public Kr3 k() {
        return this.a.F();
    }

    @DexIgnore
    public Xl3 l() {
        return this.a.z();
    }

    @DexIgnore
    public Zr3 m() {
        return this.a.w();
    }

    @DexIgnore
    @Override // com.fossil.Ln3
    public Ef2 zzm() {
        return this.a.zzm();
    }
}
