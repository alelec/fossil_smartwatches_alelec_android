package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S58 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map longOpts; // = new HashMap();
    @DexIgnore
    public Map optionGroups; // = new HashMap();
    @DexIgnore
    public List requiredOpts; // = new ArrayList();
    @DexIgnore
    public Map shortOpts; // = new HashMap();

    @DexIgnore
    public S58 addOption(P58 p58) {
        String key = p58.getKey();
        if (p58.hasLongOpt()) {
            this.longOpts.put(p58.getLongOpt(), p58);
        }
        if (p58.isRequired()) {
            if (this.requiredOpts.contains(key)) {
                List list = this.requiredOpts;
                list.remove(list.indexOf(key));
            }
            this.requiredOpts.add(key);
        }
        this.shortOpts.put(key, p58);
        return this;
    }

    @DexIgnore
    public S58 addOption(String str, String str2, boolean z, String str3) {
        addOption(new P58(str, str2, z, str3));
        return this;
    }

    @DexIgnore
    public S58 addOption(String str, boolean z, String str2) {
        addOption(str, null, z, str2);
        return this;
    }

    @DexIgnore
    public S58 addOptionGroup(Q58 q58) {
        if (q58.isRequired()) {
            this.requiredOpts.add(q58);
        }
        for (P58 p58 : q58.getOptions()) {
            p58.setRequired(false);
            addOption(p58);
            this.optionGroups.put(p58.getKey(), q58);
        }
        return this;
    }

    @DexIgnore
    public P58 getOption(String str) {
        String b = Y58.b(str);
        return this.shortOpts.containsKey(b) ? (P58) this.shortOpts.get(b) : (P58) this.longOpts.get(b);
    }

    @DexIgnore
    public Q58 getOptionGroup(P58 p58) {
        return (Q58) this.optionGroups.get(p58.getKey());
    }

    @DexIgnore
    public Collection getOptionGroups() {
        return new HashSet(this.optionGroups.values());
    }

    @DexIgnore
    public Collection getOptions() {
        return Collections.unmodifiableCollection(helpOptions());
    }

    @DexIgnore
    public List getRequiredOptions() {
        return this.requiredOpts;
    }

    @DexIgnore
    public boolean hasOption(String str) {
        String b = Y58.b(str);
        return this.shortOpts.containsKey(b) || this.longOpts.containsKey(b);
    }

    @DexIgnore
    public List helpOptions() {
        return new ArrayList(this.shortOpts.values());
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[ Options: [ short ");
        stringBuffer.append(this.shortOpts.toString());
        stringBuffer.append(" ] [ long ");
        stringBuffer.append(this.longOpts);
        stringBuffer.append(" ]");
        return stringBuffer.toString();
    }
}
