package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Kd6 extends Fq4 {
    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE n();

    @DexIgnore
    public abstract int o();

    @DexIgnore
    public abstract void p(String str, boolean z);

    @DexIgnore
    public abstract void q(int i);

    @DexIgnore
    public abstract void r();

    @DexIgnore
    public abstract void s(int i);

    @DexIgnore
    public abstract void t(boolean z);
}
