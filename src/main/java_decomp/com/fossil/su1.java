package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ix1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ ry1 c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<su1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public su1 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                pq7.b(createByteArray, "parcel.createByteArray()!!");
                return new su1(createByteArray);
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public su1[] newArray(int i) {
            return new su1[i];
        }
    }

    @DexIgnore
    public su1(byte[] bArr) throws IllegalArgumentException {
        this.b = bArr;
        if (bArr.length >= 16) {
            this.d = ByteBuffer.wrap(dm7.k(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
            this.c = new ry1(bArr[2], bArr[3]);
            return;
        }
        throw new IllegalArgumentException(e.c(e.e("data.size("), this.b.length, ") is not equal or larger ", "than 16"));
    }

    @DexIgnore
    public final short a() {
        return this.d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.b;
    }

    @DexIgnore
    public final ry1 getWatchParameterVersion() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        return g80.k(g80.k(g80.k(g80.k(new JSONObject(), jd0.A0, hy1.l(this.d, null, 1, null)), jd0.j2, this.c.toString()), jd0.f3, Long.valueOf(ix1.f1688a.b(this.b, ix1.a.CRC32C))), jd0.I, Integer.valueOf(this.b.length));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
