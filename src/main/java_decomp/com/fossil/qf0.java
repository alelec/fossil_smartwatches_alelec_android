package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qf0 extends ContextWrapper {
    @DexIgnore
    public int a;
    @DexIgnore
    public Resources.Theme b;
    @DexIgnore
    public LayoutInflater c;
    @DexIgnore
    public Configuration d;
    @DexIgnore
    public Resources e;

    @DexIgnore
    public Qf0() {
        super(null);
    }

    @DexIgnore
    public Qf0(Context context, int i) {
        super(context);
        this.a = i;
    }

    @DexIgnore
    public Qf0(Context context, Resources.Theme theme) {
        super(context);
        this.b = theme;
    }

    @DexIgnore
    public void a(Configuration configuration) {
        if (this.e != null) {
            throw new IllegalStateException("getResources() or getAssets() has already been called");
        } else if (this.d == null) {
            this.d = new Configuration(configuration);
        } else {
            throw new IllegalStateException("Override configuration has already been set");
        }
    }

    @DexIgnore
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @DexIgnore
    public final Resources b() {
        if (this.e == null) {
            Configuration configuration = this.d;
            if (configuration == null) {
                this.e = super.getResources();
            } else if (Build.VERSION.SDK_INT >= 17) {
                this.e = createConfigurationContext(configuration).getResources();
            } else {
                Resources resources = super.getResources();
                Configuration configuration2 = new Configuration(resources.getConfiguration());
                configuration2.updateFrom(this.d);
                this.e = new Resources(resources.getAssets(), resources.getDisplayMetrics(), configuration2);
            }
        }
        return this.e;
    }

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public final void d() {
        boolean z = this.b == null;
        if (z) {
            this.b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.b.setTo(theme);
            }
        }
        e(this.b, this.a, z);
    }

    @DexIgnore
    public void e(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    @DexIgnore
    public AssetManager getAssets() {
        return getResources().getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return b();
    }

    @DexIgnore
    @Override // android.content.Context, android.content.ContextWrapper
    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.c == null) {
            this.c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.c;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        if (theme != null) {
            return theme;
        }
        if (this.a == 0) {
            this.a = Te0.Theme_AppCompat_Light;
        }
        d();
        return this.b;
    }

    @DexIgnore
    public void setTheme(int i) {
        if (this.a != i) {
            this.a = i;
            d();
        }
    }
}
