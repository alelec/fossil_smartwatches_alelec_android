package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H53 implements Xw2<K53> {
    @DexIgnore
    public static H53 c; // = new H53();
    @DexIgnore
    public /* final */ Xw2<K53> b;

    @DexIgnore
    public H53() {
        this(Ww2.b(new J53()));
    }

    @DexIgnore
    public H53(Xw2<K53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((K53) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((K53) c.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((K53) c.zza()).zzc();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ K53 zza() {
        return this.b.zza();
    }
}
