package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ long e;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 f;

    @DexIgnore
    public Hn3(Qm3 qm3, String str, String str2, String str3, long j) {
        this.f = qm3;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = j;
    }

    @DexIgnore
    public final void run() {
        String str = this.b;
        if (str == null) {
            this.f.b.f0().N().R(this.c, null);
            return;
        }
        this.f.b.f0().N().R(this.c, new Xo3(this.d, str, this.e));
    }
}
