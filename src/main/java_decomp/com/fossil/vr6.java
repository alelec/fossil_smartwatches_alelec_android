package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActivityChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vr6 implements Factory<CustomizeActivityChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Vr6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Vr6 a(Provider<ThemeRepository> provider) {
        return new Vr6(provider);
    }

    @DexIgnore
    public static CustomizeActivityChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeActivityChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeActivityChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
