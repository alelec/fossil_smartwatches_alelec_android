package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.jn5;
import com.fossil.r66;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ac6 extends qv5 implements zb6, t47.g, View.OnClickListener {
    @DexIgnore
    public yb6 h;
    @DexIgnore
    public g37<h85> i;
    @DexIgnore
    public /* final */ ArrayList<Fragment> j; // = new ArrayList<>();
    @DexIgnore
    public ub6 k;
    @DexIgnore
    public lc6 l;
    @DexIgnore
    public po4 m;
    @DexIgnore
    public sb6 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ac6 f247a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(ac6 ac6) {
            this.f247a = ac6;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.f247a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                pq7.b(activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.D;
                pq7.b(stringExtra, "presetId");
                pq7.b(stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            h85 a2 = this.f247a.O6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.D;
                pq7.b(flexibleTextView, "binding.tvPresetName");
                flexibleTextView.setEllipsize(TextUtils.TruncateAt.END);
                a2.D.setSingleLine(true);
                FlexibleTextView flexibleTextView2 = a2.D;
                pq7.b(flexibleTextView2, "binding.tvPresetName");
                flexibleTextView2.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            h85 a2 = this.f247a.O6().a();
            if (a2 != null) {
                is5 is5 = is5.f1661a;
                h85 a3 = this.f247a.O6().a();
                if (a3 != null) {
                    CardView cardView = a3.t;
                    pq7.b(cardView, "mBinding.get()!!.cvGroup");
                    is5.d(cardView);
                    ViewPropertyAnimator animate = a2.G.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = a2.F.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = a2.E.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ac6 f248a;

        @DexIgnore
        public b(ac6 ac6) {
            this.f248a = ac6;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f248a.M6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f248a.K6(ViewHierarchy.DIMENSION_TOP_KEY);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f248a.L6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f248a.U6(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f248a.N6(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ac6 f249a;

        @DexIgnore
        public c(ac6 ac6) {
            this.f249a = ac6;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f249a.M6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f249a.K6("middle");
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f249a.L6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f249a.U6(str, "middle");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f249a.N6("middle", view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements r66.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ac6 f250a;

        @DexIgnore
        public d(ac6 ac6) {
            this.f250a = ac6;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void a(String str) {
            pq7.c(str, "label");
            this.f250a.M6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void b() {
            this.f250a.K6("bottom");
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public void c(String str) {
            pq7.c(str, "label");
            this.f250a.L6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean d(String str) {
            pq7.c(str, "fromPos");
            this.f250a.U6(str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.r66.a
        public boolean e(View view, String str) {
            pq7.c(view, "view");
            pq7.c(str, "id");
            return this.f250a.N6("bottom", view, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.o();
            return false;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void K6(String str) {
        pq7.c(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.J();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                ub6 ub6 = this.k;
                if (ub6 != null) {
                    ub6.L6();
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void L() {
        t47.f fVar = new t47.f(2131558482);
        fVar.e(2131363317, um5.c(PortfolioApp.h0.c(), 2131886536));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886534));
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886535));
        fVar.b(2131363291);
        fVar.b(2131363373);
        fVar.k(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public final void L6(String str, String str2) {
        pq7.c(str, "position");
        pq7.c(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.I();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.I();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.I();
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void M6(String str, String str2) {
        pq7.c(str, "position");
        pq7.c(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.J();
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final boolean N6(String str, View view, String str2) {
        pq7.c(str, "position");
        pq7.c(view, "view");
        pq7.c(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.K.J();
                    }
                } else if (str.equals("middle")) {
                    a2.J.J();
                }
            } else if (str.equals("bottom")) {
                a2.I.J();
            }
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            a2.I.setDragMode(false);
            ub6 ub6 = this.k;
            if (ub6 != null) {
                ub6.L6();
            }
            yb6 yb6 = this.h;
            if (yb6 != null) {
                yb6.n(str2, str);
                return true;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final g37<h85> O6() {
        g37<h85> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void P() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            pq7.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.h0.c().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    public final Transition P6(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        pq7.b(window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new a(this));
    }

    @DexIgnore
    public final void Q6(String str) {
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.setSelectedWc(true);
                            a2.J.setSelectedWc(false);
                            a2.I.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.K.setSelectedWc(false);
                        a2.J.setSelectedWc(true);
                        a2.I.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.K.setSelectedWc(false);
                    a2.J.setSelectedWc(false);
                    a2.I.setSelectedWc(true);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i2 == 2131363291) {
                    t0(false);
                    return;
                } else if (i2 == 2131363373) {
                    yb6 yb6 = this.h;
                    if (yb6 != null) {
                        yb6.q(true);
                        return;
                    } else {
                        pq7.n("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i2 == 2131363373 && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (pq7.a(stringExtra2, MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                jn5 jn5 = jn5.b;
                Context context = getContext();
                if (context != null) {
                    jn5.c(jn5, context, hl5.f1493a.d(stringExtra2), false, false, false, null, 60, null);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                yb6 yb62 = this.h;
                if (yb62 != null) {
                    pq7.b(stringExtra, "toPos");
                    yb62.p(stringExtra);
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        ub6 ub6 = (ub6) getChildFragmentManager().Z("MicroAppFragment");
        this.k = ub6;
        if (ub6 == null) {
            this.k = new ub6();
        }
        ub6 ub62 = this.k;
        if (ub62 != null) {
            this.j.add(ub62);
        }
        ro4 M = PortfolioApp.h0.c().M();
        ub6 ub63 = this.k;
        if (ub63 != null) {
            M.O0(new qb6(ub63)).a(this);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    /* renamed from: S6 */
    public void M5(yb6 yb6) {
        pq7.c(yb6, "presenter");
        this.h = yb6;
    }

    @DexIgnore
    public final void T6() {
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.K;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                pq7.b(putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.X(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new r66(new b(this)), null, 8, null);
                CustomizeWidget customizeWidget2 = a2.J;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                pq7.b(putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.X(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new r66(new c(this)), null, 8, null);
                CustomizeWidget customizeWidget3 = a2.I;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                pq7.b(putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.X(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new r66(new d(this)), null, 8, null);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U6(String str, String str2) {
        pq7.c(str, "fromPosition");
        pq7.c(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str2.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str2.equals("bottom")) {
                    a2.I.J();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                yb6 yb6 = this.h;
                if (yb6 != null) {
                    yb6.s(str, str2);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void W6(CustomizeWidget customizeWidget, FlexibleTextView flexibleTextView, n66 n66) {
        if (n66 != null) {
            flexibleTextView.setText(n66.b());
            customizeWidget.S(n66.a());
            customizeWidget.T();
            V6(customizeWidget, n66.a());
            return;
        }
        flexibleTextView.setText("");
        customizeWidget.S("empty");
        customizeWidget.T();
        V6(customizeWidget, "empty");
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void X4(String str, String str2, String str3) {
        pq7.c(str, "message");
        pq7.c(str2, "microAppId");
        pq7.c(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            t47.f fVar = new t47.f(2131558480);
            fVar.e(2131363317, str);
            fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886792));
            fVar.b(2131363373);
            fVar.m(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void e1(m66 m66) {
        Object obj;
        Object obj2;
        Object obj3;
        pq7.c(m66, "data");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + m66.a());
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(m66.a());
                FlexibleTextView flexibleTextView = a2.D;
                pq7.b(flexibleTextView, "it.tvPresetName");
                String d2 = m66.d();
                if (d2 != null) {
                    String upperCase = d2.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        Object next = it.next();
                        if (pq7.a(((n66) next).c(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                            obj = next;
                            break;
                        }
                    }
                    n66 n66 = (n66) obj;
                    Iterator it2 = arrayList.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        Object next2 = it2.next();
                        if (pq7.a(((n66) next2).c(), "middle")) {
                            obj2 = next2;
                            break;
                        }
                    }
                    n66 n662 = (n66) obj2;
                    Iterator it3 = arrayList.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            obj3 = null;
                            break;
                        }
                        Object next3 = it3.next();
                        if (pq7.a(((n66) next3).c(), "bottom")) {
                            obj3 = next3;
                            break;
                        }
                    }
                    CustomizeWidget customizeWidget = a2.K;
                    pq7.b(customizeWidget, "it.waTop");
                    FlexibleTextView flexibleTextView2 = a2.G;
                    pq7.b(flexibleTextView2, "it.tvWaTop");
                    W6(customizeWidget, flexibleTextView2, n66);
                    CustomizeWidget customizeWidget2 = a2.J;
                    pq7.b(customizeWidget2, "it.waMiddle");
                    FlexibleTextView flexibleTextView3 = a2.F;
                    pq7.b(flexibleTextView3, "it.tvWaMiddle");
                    W6(customizeWidget2, flexibleTextView3, n662);
                    CustomizeWidget customizeWidget3 = a2.I;
                    pq7.b(customizeWidget3, "it.waBottom");
                    FlexibleTextView flexibleTextView4 = a2.E;
                    pq7.b(flexibleTextView4, "it.tvWaBottom");
                    W6(customizeWidget3, flexibleTextView4, (n66) obj3);
                    return;
                }
                throw new il7("null cannot be cast to non-null type java.lang.String");
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            po4 po4 = this.m;
            if (po4 != null) {
                ts0 a2 = vs0.f(hybridCustomizeEditActivity, po4).a(sb6.class);
                pq7.b(a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                sb6 sb6 = (sb6) a2;
                this.s = sb6;
                yb6 yb6 = this.h;
                if (yb6 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (sb6 != null) {
                    yb6.r(sb6);
                } else {
                    pq7.n("mShareViewModel");
                    throw null;
                }
            } else {
                pq7.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && jn5.b.m(getContext(), jn5.c.BLUETOOTH_CONNECTION)) {
            yb6 yb6 = this.h;
            if (yb6 != null) {
                yb6.q(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362528) {
                yb6 yb6 = this.h;
                if (yb6 != null) {
                    yb6.q(true);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (id != 2131363291) {
                switch (id) {
                    case 2131363529:
                        yb6 yb62 = this.h;
                        if (yb62 != null) {
                            yb62.p("bottom");
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    case 2131363530:
                        yb6 yb63 = this.h;
                        if (yb63 != null) {
                            yb63.p("middle");
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    case 2131363531:
                        yb6 yb64 = this.h;
                        if (yb64 != null) {
                            yb64.p(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                yb6 yb65 = this.h;
                if (yb65 != null) {
                    yb65.o();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        h85 h85 = (h85) aq0.f(layoutInflater, 2131558580, viewGroup, false, A6());
        R6();
        this.i = new g37<>(this, h85);
        pq7.b(h85, "binding");
        return h85.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            pq7.b(activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            pq7.b(window, "activity.window");
            window.setEnterTransition(is5.f1661a.b());
            Window window2 = activity.getWindow();
            pq7.b(window2, "activity.window");
            window2.setSharedElementEnterTransition(is5.f1661a.c(PortfolioApp.h0.c()));
            Intent intent = activity.getIntent();
            pq7.b(intent, "activity.intent");
            activity.setEnterSharedElementCallback(new hs5(intent, PortfolioApp.h0.c()));
            P6(activity);
        }
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 != null) {
                String d2 = qn5.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d2)) {
                    a2.H.setBackgroundColor(Color.parseColor(d2));
                    a2.t.setBackgroundColor(Color.parseColor(d2));
                }
                a2.K.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.C.setOnClickListener(this);
                a2.u.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.B;
                pq7.b(viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new g67(getChildFragmentManager(), this.j));
                ViewPager2 viewPager22 = a2.B;
                pq7.b(viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                if (a2.B.getChildAt(0) != null) {
                    View childAt = a2.B.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(2);
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
            }
            T6();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void s0(boolean z) {
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.u;
                pq7.b(imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.u;
                pq7.b(imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.u.setBackgroundResource(2131231291);
                return;
            }
            ImageButton imageButton3 = a2.u;
            pq7.b(imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.u;
            pq7.b(imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.u.setBackgroundResource(2131231292);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void t0(boolean z) {
        FlexibleTextView flexibleTextView;
        g37<h85> g37 = this.i;
        if (g37 != null) {
            h85 a2 = g37.a();
            if (!(a2 == null || (flexibleTextView = a2.D) == null)) {
                flexibleTextView.setSingleLine(false);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void u4(String str) {
        pq7.c(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        Q6(str);
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void w() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void y() {
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886814);
        pq7.b(c2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        H6(c2);
    }

    @DexIgnore
    @Override // com.fossil.zb6
    public void y1() {
        ImageView imageView;
        if (isActive()) {
            g37<h85> g37 = this.i;
            if (g37 != null) {
                h85 a2 = g37.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    imageView.setImageResource(2131230975);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }
}
