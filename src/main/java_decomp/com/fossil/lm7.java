package com.fossil;

import com.mapped.Wg6;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lm7 extends Km7 {
    @DexIgnore
    public static final <T extends Comparable<? super T>> void q(List<T> list) {
        Wg6.c(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }

    @DexIgnore
    public static final <T> void r(List<T> list, Comparator<? super T> comparator) {
        Wg6.c(list, "$this$sortWith");
        Wg6.c(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }
}
