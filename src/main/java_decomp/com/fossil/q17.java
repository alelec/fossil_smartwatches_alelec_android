package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q17 implements Factory<CalibrationPresenter> {
    @DexIgnore
    public static CalibrationPresenter a(PortfolioApp portfolioApp, L17 l17, UserRepository userRepository, Ct0 ct0, Pl5 pl5) {
        return new CalibrationPresenter(portfolioApp, l17, userRepository, ct0, pl5);
    }
}
