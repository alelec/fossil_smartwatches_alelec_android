package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ut7 extends Tt7 {
    @DexIgnore
    public static final Integer c(String str) {
        Wg6.c(str, "$this$toIntOrNull");
        return d(str, 10);
    }

    @DexIgnore
    public static final Integer d(String str, int i) {
        int i2;
        boolean z;
        int i3;
        Wg6.c(str, "$this$toIntOrNull");
        Ct7.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        char charAt = str.charAt(0);
        int i4 = -2147483647;
        if (charAt >= '0') {
            i2 = 0;
            z = false;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                i4 = RecyclerView.UNDEFINED_DURATION;
                i2 = 1;
                z = true;
            } else if (charAt != '+') {
                return null;
            } else {
                i2 = 1;
                z = false;
            }
        }
        int i5 = -59652323;
        int i6 = 0;
        for (int i7 = i2; i7 < length; i7++) {
            int b = Ct7.b(str.charAt(i7), i);
            if (b < 0) {
                return null;
            }
            if (i6 >= i5) {
                i3 = i5;
            } else if (i5 != -59652323 || i6 < (i3 = i4 / i)) {
                return null;
            }
            int i8 = i6 * i;
            if (i8 < i4 + b) {
                return null;
            }
            i6 = i8 - b;
            i5 = i3;
        }
        return z ? Integer.valueOf(i6) : Integer.valueOf(-i6);
    }

    @DexIgnore
    public static final Long e(String str) {
        Wg6.c(str, "$this$toLongOrNull");
        return f(str, 10);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0076  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.Long f(java.lang.String r14, int r15) {
        /*
            java.lang.String r0 = "$this$toLongOrNull"
            com.mapped.Wg6.c(r14, r0)
            com.fossil.Ct7.a(r15)
            int r8 = r14.length()
            if (r8 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            r0 = 0
            r1 = 0
            char r4 = r14.charAt(r1)
            r2 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
            r1 = 1
            r5 = 48
            if (r4 >= r5) goto L_0x0046
            r0 = 1
            if (r8 != r0) goto L_0x0025
            r0 = 0
            goto L_0x000f
        L_0x0025:
            r0 = 45
            if (r4 != r0) goto L_0x0041
            r2 = -9223372036854775808
            r0 = 1
        L_0x002c:
            r6 = 0
            r4 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
        L_0x0033:
            if (r0 >= r8) goto L_0x006f
            char r9 = r14.charAt(r0)
            int r9 = com.fossil.Ct7.b(r9, r15)
            if (r9 >= 0) goto L_0x004a
            r0 = 0
            goto L_0x000f
        L_0x0041:
            r0 = 43
            if (r4 != r0) goto L_0x0048
            r0 = 1
        L_0x0046:
            r1 = 0
            goto L_0x002c
        L_0x0048:
            r0 = 0
            goto L_0x000f
        L_0x004a:
            int r10 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r10 >= 0) goto L_0x0060
            r10 = -256204778801521550(0xfc71c71c71c71c72, double:-2.772000429909333E291)
            int r4 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r4 != 0) goto L_0x005e
            long r4 = (long) r15
            long r4 = r2 / r4
            int r10 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r10 >= 0) goto L_0x0060
        L_0x005e:
            r0 = 0
            goto L_0x000f
        L_0x0060:
            long r10 = (long) r15
            long r6 = r6 * r10
            long r10 = (long) r9
            long r12 = r2 + r10
            int r9 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r9 >= 0) goto L_0x006b
            r0 = 0
            goto L_0x000f
        L_0x006b:
            long r6 = r6 - r10
            int r0 = r0 + 1
            goto L_0x0033
        L_0x006f:
            if (r1 == 0) goto L_0x0076
            java.lang.Long r0 = java.lang.Long.valueOf(r6)
            goto L_0x000f
        L_0x0076:
            long r0 = -r6
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ut7.f(java.lang.String, int):java.lang.Long");
    }
}
