package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public List<String> a;
    @DexIgnore
    public Bi b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ Bw5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                int adapterPosition = this.b.getAdapterPosition();
                if (this.b.getAdapterPosition() != -1 && (bi = this.b.b.b) != null) {
                    List list = this.b.b.a;
                    if (list != null) {
                        bi.a((String) list.get(adapterPosition));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Bw5 bw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.b = bw5;
            view.setOnClickListener(new Aii(this));
            View findViewById = view.findViewById(2131362783);
            if (findViewById != null) {
                View findViewById2 = view.findViewById(2131362103);
                if (findViewById2 != null) {
                    String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
                    String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                    if (!TextUtils.isEmpty(d)) {
                        findViewById.setBackgroundColor(Color.parseColor(d));
                    }
                    if (!TextUtils.isEmpty(d2)) {
                        findViewById2.setBackgroundColor(Color.parseColor(d2));
                    }
                    View findViewById3 = view.findViewById(2131363408);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<String> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public void i(Ai ai, int i) {
        Wg6.c(ai, "holder");
        TextView a2 = ai.a();
        List<String> list = this.a;
        if (list != null) {
            a2.setText(list.get(i));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ai j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558657, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void k(List<String> list) {
        Wg6.c(list, "addressSearchList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(Bi bi) {
        Wg6.c(bi, "listener");
        this.b = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        i(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
