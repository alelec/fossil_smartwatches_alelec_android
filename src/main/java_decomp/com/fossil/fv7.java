package com.fossil;

import com.mapped.Af6;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fv7 {
    @DexIgnore
    public static final void a(Af6 af6, Throwable th) {
        try {
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) af6.get(CoroutineExceptionHandler.q);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(af6, th);
            } else {
                Ev7.a(af6, th);
            }
        } catch (Throwable th2) {
            Ev7.a(af6, b(th, th2));
        }
    }

    @DexIgnore
    public static final Throwable b(Throwable th, Throwable th2) {
        if (th == th2) {
            return th;
        }
        RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
        Tk7.a(runtimeException, th);
        return runtimeException;
    }
}
