package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Yd1 {
    @DexIgnore
    String a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String d(Bitmap bitmap);

    @DexIgnore
    int e(Bitmap bitmap);

    @DexIgnore
    Bitmap removeLast();
}
