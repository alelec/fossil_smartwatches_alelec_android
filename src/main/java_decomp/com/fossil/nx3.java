package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nx3 extends Ze0 {
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends BottomSheetBehavior.e {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, float f) {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void b(View view, int i) {
            if (i == 5) {
                Nx3.this.w6();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void dismiss() {
        if (!y6(false)) {
            super.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public void dismissAllowingStateLoss() {
        if (!y6(true)) {
            super.dismissAllowingStateLoss();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ze0, com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        return new Mx3(getContext(), getTheme());
    }

    @DexIgnore
    public final void w6() {
        if (this.b) {
            super.dismissAllowingStateLoss();
        } else {
            super.dismiss();
        }
    }

    @DexIgnore
    public final void x6(BottomSheetBehavior<?> bottomSheetBehavior, boolean z) {
        this.b = z;
        if (bottomSheetBehavior.q() == 5) {
            w6();
            return;
        }
        if (getDialog() instanceof Mx3) {
            ((Mx3) getDialog()).h();
        }
        bottomSheetBehavior.g(new Bi());
        bottomSheetBehavior.E(5);
    }

    @DexIgnore
    public final boolean y6(boolean z) {
        Dialog dialog = getDialog();
        if (dialog instanceof Mx3) {
            Mx3 mx3 = (Mx3) dialog;
            BottomSheetBehavior<FrameLayout> f = mx3.f();
            if (f.s() && mx3.g()) {
                x6(f, z);
                return true;
            }
        }
        return false;
    }
}
