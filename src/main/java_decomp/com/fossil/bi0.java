package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bi0 extends Di0 {
    @DexIgnore
    public static volatile Bi0 c;
    @DexIgnore
    public static /* final */ Executor d; // = new Ai();
    @DexIgnore
    public static /* final */ Executor e; // = new Bi();
    @DexIgnore
    public Di0 a;
    @DexIgnore
    public Di0 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            Bi0.f().d(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            Bi0.f().a(runnable);
        }
    }

    @DexIgnore
    public Bi0() {
        Ci0 ci0 = new Ci0();
        this.b = ci0;
        this.a = ci0;
    }

    @DexIgnore
    public static Executor e() {
        return e;
    }

    @DexIgnore
    public static Bi0 f() {
        if (c != null) {
            return c;
        }
        synchronized (Bi0.class) {
            try {
                if (c == null) {
                    c = new Bi0();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return c;
    }

    @DexIgnore
    public static Executor g() {
        return d;
    }

    @DexIgnore
    @Override // com.fossil.Di0
    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    @DexIgnore
    @Override // com.fossil.Di0
    public boolean c() {
        return this.a.c();
    }

    @DexIgnore
    @Override // com.fossil.Di0
    public void d(Runnable runnable) {
        this.a.d(runnable);
    }
}
