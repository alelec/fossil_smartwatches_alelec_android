package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Vw1 e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kq1 createFromParcel(Parcel parcel) {
            return new Kq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kq1[] newArray(int i) {
            return new Kq1[i];
        }
    }

    @DexIgnore
    public Kq1(byte b, int i, Vw1 vw1, String str) {
        super(E90.IFTTT_APP, b, i);
        this.e = vw1;
        this.f = str;
    }

    @DexIgnore
    public /* synthetic */ Kq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = Vw1.values()[parcel.readInt()];
        String readString = parcel.readString();
        if (readString != null) {
            this.f = readString;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Kq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Kq1 kq1 = (Kq1) obj;
            if (this.e != kq1.e) {
                return false;
            }
            String str = this.f;
            String str2 = kq1.f;
            if (str != null) {
                return !str.contentEquals(str2);
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest");
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.e.hashCode()) * 31) + this.f.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
    }
}
