package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.mapped.Kc6;
import com.mapped.Wg6;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V51 {
    @DexIgnore
    public /* final */ G51 a;

    @DexIgnore
    public V51(G51 g51) {
        Wg6.c(g51, "bitmapPool");
        this.a = g51;
    }

    @DexIgnore
    public final Bitmap a(Drawable drawable, F81 f81, Bitmap.Config config) {
        Wg6.c(drawable, ResourceManager.DRAWABLE);
        Wg6.c(f81, "size");
        Wg6.c(config, "config");
        Bitmap.Config q = W81.q(config);
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Wg6.b(bitmap, "bitmap");
            if (W81.q(bitmap.getConfig()) == q) {
                return bitmap;
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicWidth <= 0) {
            intrinsicWidth = 512;
        }
        if (intrinsicHeight <= 0) {
            intrinsicHeight = 512;
        }
        if (f81 instanceof C81) {
            C81 c81 = (C81) f81;
            double c = T51.c(intrinsicWidth, intrinsicHeight, c81.d(), c81.c(), E81.FIT);
            int a2 = Lr7.a(((double) intrinsicWidth) * c);
            int a3 = Lr7.a(c * ((double) intrinsicHeight));
            Rect bounds = drawable.getBounds();
            int i = bounds.left;
            int i2 = bounds.top;
            int i3 = bounds.right;
            int i4 = bounds.bottom;
            Bitmap c2 = this.a.c(a2, a3, q);
            drawable.setBounds(0, 0, a2, a3);
            drawable.draw(new Canvas(c2));
            drawable.setBounds(i, i2, i3, i4);
            return c2;
        }
        throw new Kc6();
    }
}
