package com.fossil;

import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gj5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ViewTreeObserver.OnWindowFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditText a;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager b;

        @DexIgnore
        public Ai(EditText editText, InputMethodManager inputMethodManager) {
            this.a = editText;
            this.b = inputMethodManager;
        }

        @DexIgnore
        public void onWindowFocusChanged(boolean z) {
            if (z) {
                Gj5.c(this.a, this.b);
                this.a.getViewTreeObserver().removeOnWindowFocusChangeListener(this);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;
        @DexIgnore
        public /* final */ /* synthetic */ InputMethodManager c;

        @DexIgnore
        public Bi(EditText editText, InputMethodManager inputMethodManager) {
            this.b = editText;
            this.c = inputMethodManager;
        }

        @DexIgnore
        public final void run() {
            this.c.showSoftInput(this.b, 1);
        }
    }

    @DexIgnore
    public static final void b(EditText editText, InputMethodManager inputMethodManager) {
        Wg6.c(editText, "$this$focusAndShowKeyboard");
        Wg6.c(inputMethodManager, "imm");
        editText.requestFocus();
        if (editText.hasWindowFocus()) {
            c(editText, inputMethodManager);
        } else {
            editText.getViewTreeObserver().addOnWindowFocusChangeListener(new Ai(editText, inputMethodManager));
        }
    }

    @DexIgnore
    public static final void c(EditText editText, InputMethodManager inputMethodManager) {
        if (editText.isFocused()) {
            editText.post(new Bi(editText, inputMethodManager));
        }
    }
}
