package com.fossil;

import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ec4 {

    @DexIgnore
    public enum Ai {
        JAVA,
        NATIVE
    }

    @DexIgnore
    Map<String, String> a();

    @DexIgnore
    String b();

    @DexIgnore
    File c();

    @DexIgnore
    File[] d();

    @DexIgnore
    String e();

    @DexIgnore
    Ai getType();

    @DexIgnore
    Object remove();  // void declaration
}
