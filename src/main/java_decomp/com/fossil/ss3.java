package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ss3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ss3> CREATOR; // = new Rs3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Sc2 c;

    @DexIgnore
    public Ss3(int i, Sc2 sc2) {
        this.b = i;
        this.c = sc2;
    }

    @DexIgnore
    public Ss3(Sc2 sc2) {
        this(1, sc2);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.t(parcel, 2, this.c, i, false);
        Bd2.b(parcel, a2);
    }
}
