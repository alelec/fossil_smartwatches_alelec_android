package com.fossil;

import android.content.res.AssetManager;
import android.util.Log;
import com.fossil.Wb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ub1<T> implements Wb1<T> {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ AssetManager c;
    @DexIgnore
    public T d;

    @DexIgnore
    public Ub1(AssetManager assetManager, String str) {
        this.c = assetManager;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void a() {
        T t = this.d;
        if (t != null) {
            try {
                b(t);
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public abstract void b(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.Wb1
    public Gb1 c() {
        return Gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void d(Sa1 sa1, Wb1.Ai<? super T> ai) {
        try {
            T e = e(this.c, this.b);
            this.d = e;
            ai.e(e);
        } catch (IOException e2) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e2);
            }
            ai.b(e2);
        }
    }

    @DexIgnore
    public abstract T e(AssetManager assetManager, String str) throws IOException;
}
