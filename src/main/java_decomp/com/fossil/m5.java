package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M5 extends U5 {
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ N6 l;

    @DexIgnore
    public M5(V5 v5, N6 n6, N4 n4) {
        super(v5, n4);
        this.l = n6;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public JSONObject b(boolean z) {
        return G80.k(super.b(z), Jd0.R0, this.l.b);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void f(H7 h7) {
        super.f(h7);
        this.k = false;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean l() {
        return this.e.c == R5.m && this.k;
    }
}
