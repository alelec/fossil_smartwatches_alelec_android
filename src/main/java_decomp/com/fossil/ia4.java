package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ia4 extends Ta4.Di.Dii.Aiii {
    @DexIgnore
    public /* final */ Ta4.Di.Dii.Aiii.Biiii a;
    @DexIgnore
    public /* final */ Ua4<Ta4.Bi> b;
    @DexIgnore
    public /* final */ Boolean c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Dii.Aiii.Aiiii {
        @DexIgnore
        public Ta4.Di.Dii.Aiii.Biiii a;
        @DexIgnore
        public Ua4<Ta4.Bi> b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(Ta4.Di.Dii.Aiii aiii) {
            this.a = aiii.d();
            this.b = aiii.c();
            this.c = aiii.b();
            this.d = Integer.valueOf(aiii.e());
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Aiii.Aiiii
        public Ta4.Di.Dii.Aiii a() {
            String str = "";
            if (this.a == null) {
                str = " execution";
            }
            if (this.d == null) {
                str = str + " uiOrientation";
            }
            if (str.isEmpty()) {
                return new Ia4(this.a, this.b, this.c, this.d.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Aiii.Aiiii
        public Ta4.Di.Dii.Aiii.Aiiii b(Boolean bool) {
            this.c = bool;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Aiii.Aiiii
        public Ta4.Di.Dii.Aiii.Aiiii c(Ua4<Ta4.Bi> ua4) {
            this.b = ua4;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Aiii.Aiiii
        public Ta4.Di.Dii.Aiii.Aiiii d(Ta4.Di.Dii.Aiii.Biiii biiii) {
            if (biiii != null) {
                this.a = biiii;
                return this;
            }
            throw new NullPointerException("Null execution");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Aiii.Aiiii
        public Ta4.Di.Dii.Aiii.Aiiii e(int i) {
            this.d = Integer.valueOf(i);
            return this;
        }
    }

    @DexIgnore
    public Ia4(Ta4.Di.Dii.Aiii.Biiii biiii, Ua4<Ta4.Bi> ua4, Boolean bool, int i) {
        this.a = biiii;
        this.b = ua4;
        this.c = bool;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii
    public Boolean b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii
    public Ua4<Ta4.Bi> c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii
    public Ta4.Di.Dii.Aiii.Biiii d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii
    public int e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Ua4<Ta4.Bi> ua4;
        Boolean bool;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Dii.Aiii)) {
            return false;
        }
        Ta4.Di.Dii.Aiii aiii = (Ta4.Di.Dii.Aiii) obj;
        return this.a.equals(aiii.d()) && ((ua4 = this.b) != null ? ua4.equals(aiii.c()) : aiii.c() == null) && ((bool = this.c) != null ? bool.equals(aiii.b()) : aiii.b() == null) && this.d == aiii.e();
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii.Aiii
    public Ta4.Di.Dii.Aiii.Aiiii f() {
        return new Bi(this);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.a.hashCode();
        Ua4<Ta4.Bi> ua4 = this.b;
        int hashCode2 = ua4 == null ? 0 : ua4.hashCode();
        Boolean bool = this.c;
        if (bool != null) {
            i = bool.hashCode();
        }
        return ((((hashCode2 ^ ((hashCode ^ 1000003) * 1000003)) * 1000003) ^ i) * 1000003) ^ this.d;
    }

    @DexIgnore
    public String toString() {
        return "Application{execution=" + this.a + ", customAttributes=" + this.b + ", background=" + this.c + ", uiOrientation=" + this.d + "}";
    }
}
