package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ha4 extends Ta4.Di.Dii {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Ta4.Di.Dii.Aiii c;
    @DexIgnore
    public /* final */ Ta4.Di.Dii.Ciii d;
    @DexIgnore
    public /* final */ Ta4.Di.Dii.Diii e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Dii.Biii {
        @DexIgnore
        public Long a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Ta4.Di.Dii.Aiii c;
        @DexIgnore
        public Ta4.Di.Dii.Ciii d;
        @DexIgnore
        public Ta4.Di.Dii.Diii e;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(Ta4.Di.Dii dii) {
            this.a = Long.valueOf(dii.e());
            this.b = dii.f();
            this.c = dii.b();
            this.d = dii.c();
            this.e = dii.d();
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii a() {
            String str = "";
            if (this.a == null) {
                str = " timestamp";
            }
            if (this.b == null) {
                str = str + " type";
            }
            if (this.c == null) {
                str = str + " app";
            }
            if (this.d == null) {
                str = str + " device";
            }
            if (str.isEmpty()) {
                return new Ha4(this.a.longValue(), this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii.Biii b(Ta4.Di.Dii.Aiii aiii) {
            if (aiii != null) {
                this.c = aiii;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii.Biii c(Ta4.Di.Dii.Ciii ciii) {
            if (ciii != null) {
                this.d = ciii;
                return this;
            }
            throw new NullPointerException("Null device");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii.Biii d(Ta4.Di.Dii.Diii diii) {
            this.e = diii;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii.Biii e(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Dii.Biii
        public Ta4.Di.Dii.Biii f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }
    }

    @DexIgnore
    public Ha4(long j, String str, Ta4.Di.Dii.Aiii aiii, Ta4.Di.Dii.Ciii ciii, Ta4.Di.Dii.Diii diii) {
        this.a = j;
        this.b = str;
        this.c = aiii;
        this.d = ciii;
        this.e = diii;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public Ta4.Di.Dii.Aiii b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public Ta4.Di.Dii.Ciii c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public Ta4.Di.Dii.Diii d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public long e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Dii)) {
            return false;
        }
        Ta4.Di.Dii dii = (Ta4.Di.Dii) obj;
        if (this.a == dii.e() && this.b.equals(dii.f()) && this.c.equals(dii.b()) && this.d.equals(dii.c())) {
            Ta4.Di.Dii.Diii diii = this.e;
            if (diii == null) {
                if (dii.d() == null) {
                    return true;
                }
            } else if (diii.equals(dii.d())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public String f() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Dii
    public Ta4.Di.Dii.Biii g() {
        return new Bi(this);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int i = (int) (j ^ (j >>> 32));
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        int hashCode3 = this.d.hashCode();
        Ta4.Di.Dii.Diii diii = this.e;
        return (diii == null ? 0 : diii.hashCode()) ^ ((((((((i ^ 1000003) * 1000003) ^ hashCode) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "Event{timestamp=" + this.a + ", type=" + this.b + ", app=" + this.c + ", device=" + this.d + ", log=" + this.e + "}";
    }
}
