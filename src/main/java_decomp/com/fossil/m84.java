package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M84 extends Z84 {
    @DexIgnore
    public /* final */ Ta4 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public M84(Ta4 ta4, String str) {
        if (ta4 != null) {
            this.a = ta4;
            if (str != null) {
                this.b = str;
                return;
            }
            throw new NullPointerException("Null sessionId");
        }
        throw new NullPointerException("Null report");
    }

    @DexIgnore
    @Override // com.fossil.Z84
    public Ta4 b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Z84
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Z84)) {
            return false;
        }
        Z84 z84 = (Z84) obj;
        return this.a.equals(z84.b()) && this.b.equals(z84.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CrashlyticsReportWithSessionId{report=" + this.a + ", sessionId=" + this.b + "}";
    }
}
