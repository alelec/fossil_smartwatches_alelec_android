package com.fossil;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nk2 implements ServiceConnection {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Intent b;
    @DexIgnore
    public /* final */ ScheduledExecutorService c;
    @DexIgnore
    public /* final */ Queue<Jk2> d;
    @DexIgnore
    public Lk2 e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public Nk2(Context context, String str) {
        this(context, str, new ScheduledThreadPoolExecutor(0, new Sf2("EnhancedIntentService")));
    }

    @DexIgnore
    public Nk2(Context context, String str, ScheduledExecutorService scheduledExecutorService) {
        this.d = new ArrayDeque();
        this.f = false;
        this.a = context.getApplicationContext();
        this.b = new Intent(str).setPackage(this.a.getPackageName());
        this.c = scheduledExecutorService;
    }

    @DexIgnore
    public final void a(Intent intent, BroadcastReceiver.PendingResult pendingResult) {
        synchronized (this) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "new intent queued in the bind-strategy delivery");
            }
            this.d.add(new Jk2(intent, pendingResult, this.c));
            b();
        }
    }

    @DexIgnore
    public final void b() {
        synchronized (this) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "flush queue called");
            }
            while (!this.d.isEmpty()) {
                if (Log.isLoggable("EnhancedIntentService", 3)) {
                    Log.d("EnhancedIntentService", "found intent to be delivered");
                }
                if (this.e == null || !this.e.isBinderAlive()) {
                    if (Log.isLoggable("EnhancedIntentService", 3)) {
                        boolean z = !this.f;
                        StringBuilder sb = new StringBuilder(39);
                        sb.append("binder is dead. start connection? ");
                        sb.append(z);
                        Log.d("EnhancedIntentService", sb.toString());
                    }
                    if (!this.f) {
                        this.f = true;
                        try {
                            if (!Ve2.b().a(this.a, this.b, this, 65)) {
                                Log.e("EnhancedIntentService", "binding to the service failed");
                                this.f = false;
                                c();
                            } else {
                                return;
                            }
                        } catch (SecurityException e2) {
                            Log.e("EnhancedIntentService", "Exception while binding the service", e2);
                        }
                    }
                    return;
                }
                if (Log.isLoggable("EnhancedIntentService", 3)) {
                    Log.d("EnhancedIntentService", "binder is alive, sending the intent.");
                }
                this.e.b(this.d.poll());
            }
        }
    }

    @DexIgnore
    public final void c() {
        while (!this.d.isEmpty()) {
            this.d.poll().a();
        }
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this) {
            this.f = false;
            this.e = (Lk2) iBinder;
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                String valueOf = String.valueOf(componentName);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20);
                sb.append("onServiceConnected: ");
                sb.append(valueOf);
                Log.d("EnhancedIntentService", sb.toString());
            }
            if (iBinder == null) {
                Log.e("EnhancedIntentService", "Null service connection");
                c();
            } else {
                b();
            }
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            Log.d("EnhancedIntentService", sb.toString());
        }
        b();
    }
}
