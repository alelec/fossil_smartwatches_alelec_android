package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qd3 extends As2 implements Bc3 {
    @DexIgnore
    public Qd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapViewDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final Rg2 getView() throws RemoteException {
        Parcel e = e(8, d());
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void l(Pc3 pc3) throws RemoteException {
        Parcel d = d();
        Es2.c(d, pc3);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel d = d();
        Es2.d(d, bundle);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onDestroy() throws RemoteException {
        i(5, d());
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onLowMemory() throws RemoteException {
        i(6, d());
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onPause() throws RemoteException {
        i(4, d());
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onResume() throws RemoteException {
        i(3, d());
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel d = d();
        Es2.d(d, bundle);
        Parcel e = e(7, d);
        if (e.readInt() != 0) {
            bundle.readFromParcel(e);
        }
        e.recycle();
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onStart() throws RemoteException {
        i(12, d());
    }

    @DexIgnore
    @Override // com.fossil.Bc3
    public final void onStop() throws RemoteException {
        i(13, d());
    }
}
