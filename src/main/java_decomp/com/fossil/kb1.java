package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kb1 extends IOException {
    @DexIgnore
    public static /* final */ int UNKNOWN; // = -1;
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ int statusCode;

    @DexIgnore
    public Kb1(int i) {
        this("Http request failed with status code: " + i, i);
    }

    @DexIgnore
    public Kb1(String str) {
        this(str, -1);
    }

    @DexIgnore
    public Kb1(String str, int i) {
        this(str, i, null);
    }

    @DexIgnore
    public Kb1(String str, int i, Throwable th) {
        super(str, th);
        this.statusCode = i;
    }

    @DexIgnore
    public int getStatusCode() {
        return this.statusCode;
    }
}
