package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Y34<K, V> {
    @DexIgnore
    Map<K, Collection<V>> asMap();

    @DexIgnore
    Object clear();  // void declaration

    @DexIgnore
    boolean containsEntry(Object obj, Object obj2);

    @DexIgnore
    boolean containsKey(Object obj);

    @DexIgnore
    Collection<Map.Entry<K, V>> entries();

    @DexIgnore
    Collection<V> get(K k);

    @DexIgnore
    boolean isEmpty();

    @DexIgnore
    Set<K> keySet();

    @DexIgnore
    @CanIgnoreReturnValue
    boolean put(K k, V v);

    @DexIgnore
    @CanIgnoreReturnValue
    boolean putAll(K k, Iterable<? extends V> iterable);

    @DexIgnore
    @CanIgnoreReturnValue
    boolean remove(Object obj, Object obj2);

    @DexIgnore
    int size();
}
