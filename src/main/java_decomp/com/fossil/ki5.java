package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.fossil.fitness.WorkoutState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ki5 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop"),
    PAUSE("pause"),
    RESUME("resume"),
    IDLE("idle");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final ki5 a(String str) {
            ki5 ki5;
            pq7.c(str, "value");
            ki5[] values = ki5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ki5 = null;
                    break;
                }
                ki5 = values[i];
                String mValue = ki5.getMValue();
                String lowerCase = str.toLowerCase();
                pq7.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (pq7.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return ki5 != null ? ki5 : ki5.IDLE;
        }

        @DexIgnore
        public final ki5 b(int i) {
            return i == WorkoutState.START.ordinal() ? ki5.START : i == WorkoutState.END.ordinal() ? ki5.STOP : i == WorkoutState.PAUSE.ordinal() ? ki5.PAUSE : i == WorkoutState.RESUME.ordinal() ? ki5.RESUME : ki5.IDLE;
        }
    }

    @DexIgnore
    public ki5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        pq7.c(str, "<set-?>");
        this.mValue = str;
    }
}
