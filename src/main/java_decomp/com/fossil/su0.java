package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Kv0;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Su0 implements Kv0.Ai {
    @DexIgnore
    public Mn0<Bi> a;
    @DexIgnore
    public /* final */ ArrayList<Bi> b;
    @DexIgnore
    public /* final */ ArrayList<Bi> c;
    @DexIgnore
    public /* final */ Ai d;
    @DexIgnore
    public Runnable e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ Kv0 g;
    @DexIgnore
    public int h;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(int i, int i2);

        @DexIgnore
        void b(Bi bi);

        @DexIgnore
        void c(int i, int i2, Object obj);

        @DexIgnore
        void d(Bi bi);

        @DexIgnore
        RecyclerView.ViewHolder e(int i);

        @DexIgnore
        void f(int i, int i2);

        @DexIgnore
        void g(int i, int i2);

        @DexIgnore
        void h(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Object c;
        @DexIgnore
        public int d;

        @DexIgnore
        public Bi(int i, int i2, int i3, Object obj) {
            this.a = i;
            this.b = i2;
            this.d = i3;
            this.c = obj;
        }

        @DexIgnore
        public String a() {
            int i = this.a;
            return i != 1 ? i != 2 ? i != 4 ? i != 8 ? "Object" : "mv" : "up" : "rm" : "add";
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Bi.class != obj.getClass()) {
                return false;
            }
            Bi bi = (Bi) obj;
            int i = this.a;
            if (i != bi.a) {
                return false;
            }
            if (i == 8 && Math.abs(this.d - this.b) == 1 && this.d == bi.b && this.b == bi.d) {
                return true;
            }
            if (this.d != bi.d) {
                return false;
            }
            if (this.b != bi.b) {
                return false;
            }
            Object obj2 = this.c;
            return obj2 != null ? obj2.equals(bi.c) : bi.c == null;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return Integer.toHexString(System.identityHashCode(this)) + "[" + a() + ",s:" + this.b + "c:" + this.d + ",p:" + this.c + "]";
        }
    }

    @DexIgnore
    public Su0(Ai ai) {
        this(ai, false);
    }

    @DexIgnore
    public Su0(Ai ai, boolean z) {
        this.a = new Nn0(30);
        this.b = new ArrayList<>();
        this.c = new ArrayList<>();
        this.h = 0;
        this.d = ai;
        this.f = z;
        this.g = new Kv0(this);
    }

    @DexIgnore
    @Override // com.fossil.Kv0.Ai
    public void a(Bi bi) {
        if (!this.f) {
            bi.c = null;
            this.a.a(bi);
        }
    }

    @DexIgnore
    @Override // com.fossil.Kv0.Ai
    public Bi b(int i, int i2, int i3, Object obj) {
        Bi b2 = this.a.b();
        if (b2 == null) {
            return new Bi(i, i2, i3, obj);
        }
        b2.a = i;
        b2.b = i2;
        b2.d = i3;
        b2.c = obj;
        return b2;
    }

    @DexIgnore
    public final void c(Bi bi) {
        v(bi);
    }

    @DexIgnore
    public final void d(Bi bi) {
        v(bi);
    }

    @DexIgnore
    public int e(int i) {
        int size = this.b.size();
        int i2 = i;
        for (int i3 = 0; i3 < size; i3++) {
            Bi bi = this.b.get(i3);
            int i4 = bi.a;
            if (i4 != 1) {
                if (i4 == 2) {
                    int i5 = bi.b;
                    if (i5 <= i2) {
                        int i6 = bi.d;
                        if (i5 + i6 > i2) {
                            return -1;
                        }
                        i2 -= i6;
                    } else {
                        continue;
                    }
                } else if (i4 == 8) {
                    int i7 = bi.b;
                    if (i7 == i2) {
                        i2 = bi.d;
                    } else {
                        if (i7 < i2) {
                            i2--;
                        }
                        if (bi.d <= i2) {
                            i2++;
                        }
                    }
                }
            } else if (bi.b <= i2) {
                i2 += bi.d;
            }
        }
        return i2;
    }

    @DexIgnore
    public final void f(Bi bi) {
        boolean z;
        int i;
        int i2 = bi.b;
        int i3 = bi.d + i2;
        char c2 = '\uffff';
        int i4 = 0;
        int i5 = i2;
        while (i5 < i3) {
            if (this.d.e(i5) != null || h(i5)) {
                if (c2 == 0) {
                    k(b(2, i2, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    v(b(2, i2, i4, null));
                    z = true;
                } else {
                    z = false;
                }
                c2 = 0;
            }
            if (z) {
                i5 -= i4;
                i3 -= i4;
                i = 1;
            } else {
                i = i4 + 1;
            }
            i5++;
            i4 = i;
        }
        if (i4 != bi.d) {
            a(bi);
            bi = b(2, i2, i4, null);
        }
        if (c2 == 0) {
            k(bi);
        } else {
            v(bi);
        }
    }

    @DexIgnore
    public final void g(Bi bi) {
        int i = bi.b;
        int i2 = bi.d;
        char c2 = '\uffff';
        int i3 = 0;
        int i4 = i;
        for (int i5 = i; i5 < i2 + i; i5++) {
            if (this.d.e(i5) != null || h(i5)) {
                if (c2 == 0) {
                    k(b(4, i4, i3, bi.c));
                    i4 = i5;
                    i3 = 0;
                }
                c2 = 1;
            } else {
                if (c2 == 1) {
                    v(b(4, i4, i3, bi.c));
                    i3 = 0;
                    i4 = i5;
                }
                c2 = 0;
            }
            i3++;
        }
        if (i3 != bi.d) {
            Object obj = bi.c;
            a(bi);
            bi = b(4, i4, i3, obj);
        }
        if (c2 == 0) {
            k(bi);
        } else {
            v(bi);
        }
    }

    @DexIgnore
    public final boolean h(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            Bi bi = this.c.get(i2);
            int i3 = bi.a;
            if (i3 == 8) {
                if (n(bi.d, i2 + 1) == i) {
                    return true;
                }
            } else if (i3 == 1) {
                int i4 = bi.b;
                int i5 = bi.d;
                for (int i6 = i4; i6 < i5 + i4; i6++) {
                    if (n(i6, i2 + 1) == i) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    public void i() {
        int size = this.c.size();
        for (int i = 0; i < size; i++) {
            this.d.d(this.c.get(i));
        }
        x(this.c);
        this.h = 0;
    }

    @DexIgnore
    public void j() {
        i();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            Bi bi = this.b.get(i);
            int i2 = bi.a;
            if (i2 == 1) {
                this.d.d(bi);
                this.d.g(bi.b, bi.d);
            } else if (i2 == 2) {
                this.d.d(bi);
                this.d.h(bi.b, bi.d);
            } else if (i2 == 4) {
                this.d.d(bi);
                this.d.c(bi.b, bi.d, bi.c);
            } else if (i2 == 8) {
                this.d.d(bi);
                this.d.a(bi.b, bi.d);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        x(this.b);
        this.h = 0;
    }

    @DexIgnore
    public final void k(Bi bi) {
        int i;
        int i2 = bi.a;
        if (i2 == 1 || i2 == 8) {
            throw new IllegalArgumentException("should not dispatch add or move for pre layout");
        }
        int z = z(bi.b, i2);
        int i3 = bi.b;
        int i4 = bi.a;
        if (i4 == 2) {
            i = 0;
        } else if (i4 == 4) {
            i = 1;
        } else {
            throw new IllegalArgumentException("op should be remove or update." + bi);
        }
        int i5 = 1;
        for (int i6 = 1; i6 < bi.d; i6++) {
            int z2 = z(bi.b + (i * i6), bi.a);
            int i7 = bi.a;
            if (i7 == 2 ? z2 == z : i7 == 4 && z2 == z + 1) {
                i5++;
            } else {
                Bi b2 = b(bi.a, z, i5, bi.c);
                l(b2, i3);
                a(b2);
                if (bi.a == 4) {
                    i3 += i5;
                }
                i5 = 1;
                z = z2;
            }
        }
        Object obj = bi.c;
        a(bi);
        if (i5 > 0) {
            Bi b3 = b(bi.a, z, i5, obj);
            l(b3, i3);
            a(b3);
        }
    }

    @DexIgnore
    public void l(Bi bi, int i) {
        this.d.b(bi);
        int i2 = bi.a;
        if (i2 == 2) {
            this.d.h(i, bi.d);
        } else if (i2 == 4) {
            this.d.c(i, bi.d, bi.c);
        } else {
            throw new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
        }
    }

    @DexIgnore
    public int m(int i) {
        return n(i, 0);
    }

    @DexIgnore
    public int n(int i, int i2) {
        int size = this.c.size();
        int i3 = i;
        while (i2 < size) {
            Bi bi = this.c.get(i2);
            int i4 = bi.a;
            if (i4 == 8) {
                int i5 = bi.b;
                if (i5 == i3) {
                    i3 = bi.d;
                } else {
                    if (i5 < i3) {
                        i3--;
                    }
                    if (bi.d <= i3) {
                        i3++;
                    }
                }
            } else {
                int i6 = bi.b;
                if (i6 > i3) {
                    continue;
                } else if (i4 == 2) {
                    int i7 = bi.d;
                    if (i3 < i6 + i7) {
                        return -1;
                    }
                    i3 -= i7;
                } else if (i4 == 1) {
                    i3 += bi.d;
                }
            }
            i2++;
        }
        return i3;
    }

    @DexIgnore
    public boolean o(int i) {
        return (this.h & i) != 0;
    }

    @DexIgnore
    public boolean p() {
        return this.b.size() > 0;
    }

    @DexIgnore
    public boolean q() {
        return !this.c.isEmpty() && !this.b.isEmpty();
    }

    @DexIgnore
    public boolean r(int i, int i2, Object obj) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(4, i, i2, obj));
        this.h |= 4;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public boolean s(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(1, i, i2, null));
        this.h |= 1;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public boolean t(int i, int i2, int i3) {
        boolean z = true;
        if (i == i2) {
            return false;
        }
        if (i3 == 1) {
            this.b.add(b(8, i, i2, null));
            this.h |= 8;
            if (this.b.size() != 1) {
                z = false;
            }
            return z;
        }
        throw new IllegalArgumentException("Moving more than 1 item is not supported yet");
    }

    @DexIgnore
    public boolean u(int i, int i2) {
        boolean z = true;
        if (i2 < 1) {
            return false;
        }
        this.b.add(b(2, i, i2, null));
        this.h |= 2;
        if (this.b.size() != 1) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public final void v(Bi bi) {
        this.c.add(bi);
        int i = bi.a;
        if (i == 1) {
            this.d.g(bi.b, bi.d);
        } else if (i == 2) {
            this.d.f(bi.b, bi.d);
        } else if (i == 4) {
            this.d.c(bi.b, bi.d, bi.c);
        } else if (i == 8) {
            this.d.a(bi.b, bi.d);
        } else {
            throw new IllegalArgumentException("Unknown update op type for " + bi);
        }
    }

    @DexIgnore
    public void w() {
        this.g.b(this.b);
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            Bi bi = this.b.get(i);
            int i2 = bi.a;
            if (i2 == 1) {
                c(bi);
            } else if (i2 == 2) {
                f(bi);
            } else if (i2 == 4) {
                g(bi);
            } else if (i2 == 8) {
                d(bi);
            }
            Runnable runnable = this.e;
            if (runnable != null) {
                runnable.run();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public void x(List<Bi> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(list.get(i));
        }
        list.clear();
    }

    @DexIgnore
    public void y() {
        x(this.b);
        x(this.c);
        this.h = 0;
    }

    @DexIgnore
    public final int z(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = i;
        for (int size = this.c.size() - 1; size >= 0; size--) {
            Bi bi = this.c.get(size);
            int i7 = bi.a;
            if (i7 == 8) {
                int i8 = bi.b;
                int i9 = bi.d;
                if (i8 < i9) {
                    i4 = i9;
                    i5 = i8;
                } else {
                    i4 = i8;
                    i5 = i9;
                }
                if (i6 < i5 || i6 > i4) {
                    int i10 = bi.b;
                    if (i6 < i10) {
                        if (i2 == 1) {
                            bi.b = i10 + 1;
                            bi.d++;
                            i3 = i6;
                        } else if (i2 == 2) {
                            bi.b = i10 - 1;
                            bi.d--;
                            i3 = i6;
                        }
                    }
                    i3 = i6;
                } else {
                    int i11 = bi.b;
                    if (i5 == i11) {
                        if (i2 == 1) {
                            bi.d++;
                        } else if (i2 == 2) {
                            bi.d--;
                        }
                        i3 = i6 + 1;
                    } else {
                        if (i2 == 1) {
                            bi.b = i11 + 1;
                        } else if (i2 == 2) {
                            bi.b = i11 - 1;
                        }
                        i3 = i6 - 1;
                    }
                }
            } else {
                int i12 = bi.b;
                if (i12 <= i6) {
                    if (i7 == 1) {
                        i3 = i6 - bi.d;
                    } else {
                        if (i7 == 2) {
                            i3 = bi.d + i6;
                        }
                        i3 = i6;
                    }
                } else if (i2 == 1) {
                    bi.b = i12 + 1;
                    i3 = i6;
                } else {
                    if (i2 == 2) {
                        bi.b = i12 - 1;
                        i3 = i6;
                    }
                    i3 = i6;
                }
            }
            i6 = i3;
        }
        for (int size2 = this.c.size() - 1; size2 >= 0; size2--) {
            Bi bi2 = this.c.get(size2);
            if (bi2.a == 8) {
                int i13 = bi2.d;
                if (i13 == bi2.b || i13 < 0) {
                    this.c.remove(size2);
                    a(bi2);
                }
            } else if (bi2.d <= 0) {
                this.c.remove(size2);
                a(bi2);
            }
        }
        return i6;
    }
}
