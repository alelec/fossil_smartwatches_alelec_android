package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class J95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ FlexibleCheckBox r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    public J95(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, FlexibleCheckBox flexibleCheckBox2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, RTLImageView rTLImageView, View view2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout5) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = flexibleCheckBox2;
        this.s = constraintLayout;
        this.t = constraintLayout2;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = rTLImageView;
        this.y = view2;
        this.z = constraintLayout3;
        this.A = constraintLayout4;
        this.B = flexibleTextView4;
        this.C = flexibleTextView5;
        this.D = constraintLayout5;
    }
}
