package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ei1 {
    @DexIgnore
    Object onDestroy();  // void declaration

    @DexIgnore
    Object onStart();  // void declaration

    @DexIgnore
    Object onStop();  // void declaration
}
