package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vg4 extends K64 {
    @DexIgnore
    public /* final */ Ai status;

    @DexIgnore
    public enum Ai {
        BAD_CONFIG
    }

    @DexIgnore
    public Vg4(Ai ai) {
        this.status = ai;
    }

    @DexIgnore
    public Vg4(String str, Ai ai) {
        super(str);
        this.status = ai;
    }

    @DexIgnore
    public Vg4(String str, Ai ai, Throwable th) {
        super(str, th);
        this.status = ai;
    }

    @DexIgnore
    public Ai getStatus() {
        return this.status;
    }
}
