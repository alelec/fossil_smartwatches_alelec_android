package com.fossil;

import android.text.TextUtils;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e86 extends r76 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public Gson f; // = new Gson();
    @DexIgnore
    public CommuteTimeSetting g;
    @DexIgnore
    public ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public MFUser i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ s76 k;
    @DexIgnore
    public /* final */ on5 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2", f = "CommuteTimeSettingsPresenter.kt", l = {155, 162}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address;
        @DexIgnore
        public /* final */ /* synthetic */ String $displayInfo;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e86 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e86$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.e86$a$a  reason: collision with other inner class name */
        public static final class C0061a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ dr7 $searchedRecent;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0061a(a aVar, dr7 dr7, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$searchedRecent = dr7;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0061a aVar = new C0061a(this.this$0, this.$searchedRecent, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0061a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.l.I0(this.$searchedRecent.element);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$searchedRecent$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<String>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.l.h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(e86 e86, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = e86;
            this.$address = str;
            this.$displayInfo = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$address, this.$displayInfo, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0060  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 218
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.e86.a.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2", f = "CommuteTimeSettingsPresenter.kt", l = {67, 68}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e86 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {67}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.m;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.e86$b$b")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$recentSearchedAddress$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.e86$b$b  reason: collision with other inner class name */
        public static final class C0062b extends ko7 implements vp7<iv7, qn7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0062b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0062b bVar = new C0062b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<String>> qn7) {
                throw null;
                //return ((C0062b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.l.h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(e86 e86, qn7 qn7) {
            super(2, qn7);
            this.this$0 = e86;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x00b7
                if (r0 == r5) goto L_0x008d
                if (r0 != r6) goto L_0x0085
                java.lang.Object r0 = r8.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r9)
                r0 = r9
            L_0x0017:
                java.lang.String r1 = "withContext(IO) { mShare\u2026r.addressSearchedRecent }"
                com.fossil.pq7.b(r0, r1)
                java.util.List r0 = (java.util.List) r0
                com.fossil.e86 r1 = r8.this$0
                java.util.ArrayList r1 = com.fossil.e86.y(r1)
                r1.clear()
                com.fossil.e86 r1 = r8.this$0
                java.util.ArrayList r1 = com.fossil.e86.y(r1)
                r1.addAll(r0)
                com.fossil.e86 r0 = r8.this$0
                com.portfolio.platform.data.model.MFUser r2 = com.fossil.e86.A(r0)
                if (r2 == 0) goto L_0x0064
                com.fossil.e86 r0 = r8.this$0
                com.fossil.s76 r3 = com.fossil.e86.C(r0)
                com.portfolio.platform.data.model.MFUser$Address r0 = r2.getAddresses()
                java.lang.String r1 = ""
                if (r0 == 0) goto L_0x00d5
                java.lang.String r0 = r0.getHome()
                if (r0 == 0) goto L_0x00d5
            L_0x004c:
                r3.k2(r0)
                com.fossil.e86 r0 = r8.this$0
                com.fossil.s76 r3 = com.fossil.e86.C(r0)
                com.portfolio.platform.data.model.MFUser$Address r0 = r2.getAddresses()
                if (r0 == 0) goto L_0x00d9
                java.lang.String r0 = r0.getWork()
                if (r0 == 0) goto L_0x00d9
            L_0x0061:
                r3.Q4(r0)
            L_0x0064:
                com.fossil.e86 r0 = r8.this$0
                com.fossil.s76 r0 = com.fossil.e86.C(r0)
                com.fossil.e86 r1 = r8.this$0
                java.util.ArrayList r1 = com.fossil.e86.y(r1)
                r0.h0(r1)
                com.fossil.e86 r0 = r8.this$0
                com.fossil.s76 r0 = com.fossil.e86.C(r0)
                com.fossil.e86 r1 = r8.this$0
                com.portfolio.platform.data.model.setting.CommuteTimeSetting r1 = com.fossil.e86.x(r1)
                r0.s4(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0084:
                return r0
            L_0x0085:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x008d:
                java.lang.Object r0 = r8.L$1
                com.fossil.e86 r0 = (com.fossil.e86) r0
                java.lang.Object r1 = r8.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r9)
                r3 = r0
                r2 = r9
            L_0x009a:
                r0 = r2
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                com.fossil.e86.D(r3, r0)
                com.fossil.e86 r0 = r8.this$0
                com.fossil.dv7 r0 = com.fossil.e86.w(r0)
                com.fossil.e86$b$b r2 = new com.fossil.e86$b$b
                r2.<init>(r8, r7)
                r8.L$0 = r1
                r8.label = r6
                java.lang.Object r0 = com.fossil.eu7.g(r0, r2, r8)
                if (r0 != r4) goto L_0x0017
                r0 = r4
                goto L_0x0084
            L_0x00b7:
                com.fossil.el7.b(r9)
                com.fossil.iv7 r1 = r8.p$
                com.fossil.e86 r0 = r8.this$0
                com.fossil.dv7 r2 = com.fossil.e86.w(r0)
                com.fossil.e86$b$a r3 = new com.fossil.e86$b$a
                r3.<init>(r8, r7)
                r8.L$0 = r1
                r8.L$1 = r0
                r8.label = r5
                java.lang.Object r2 = com.fossil.eu7.g(r2, r3, r8)
                if (r2 != r4) goto L_0x00db
                r0 = r4
                goto L_0x0084
            L_0x00d5:
                java.lang.String r0 = ""
                goto L_0x004c
            L_0x00d9:
                r0 = r1
                goto L_0x0061
            L_0x00db:
                r3 = r0
                goto L_0x009a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.e86.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = e86.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsPrese\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public e86(s76 s76, on5 on5, UserRepository userRepository) {
        pq7.c(s76, "mView");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(userRepository, "mUserRepository");
        this.k = s76;
        this.l = on5;
        this.m = userRepository;
    }

    @DexIgnore
    public void E(String str) {
        CommuteTimeSetting commuteTimeSetting;
        pq7.c(str, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "initSeting " + str);
        try {
            commuteTimeSetting = (CommuteTimeSetting) this.f.k(str, CommuteTimeSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = n;
            local2.d(str3, "exception when parse commute time setting " + e2);
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        this.g = commuteTimeSetting;
        if (commuteTimeSetting == null) {
            this.g = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
    }

    @DexIgnore
    public void F() {
        this.k.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        PlacesClient createClient = Places.createClient(PortfolioApp.h0.c());
        this.e = createClient;
        this.k.H(createClient);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                this.k.w2(commuteTimeSetting.getAddress());
            }
            this.k.p6(commuteTimeSetting.getAvoidTolls());
        }
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void n() {
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setAddress("");
        }
    }

    @DexIgnore
    @Override // com.fossil.r76
    public CommuteTimeSetting o() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void p() {
        String str;
        MFUser.Address addresses;
        MFUser.Address addresses2;
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty((mFUser == null || (addresses2 = mFUser.getAddresses()) == null) ? null : addresses2.getHome())) {
            this.j = false;
            this.k.c2("Home", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 == null || (addresses = mFUser2.getAddresses()) == null || (str = addresses.getHome()) == null) {
                str = "";
            }
            commuteTimeSetting.setAddress(str);
        }
        this.k.x0(true);
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void q() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            s76 s76 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getHome()) == null) {
                str = "";
            }
            s76.c2("Home", str);
        }
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void r(String str) {
        pq7.c(str, "address");
        if (this.j) {
            this.j = false;
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null && !TextUtils.isEmpty(str)) {
            commuteTimeSetting.setAddress(str);
            this.k.w2(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void s() {
        MFUser.Address addresses;
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty((mFUser == null || (addresses = mFUser.getAddresses()) == null) ? null : addresses.getWork())) {
            this.j = false;
            this.k.c2("Other", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 != null) {
                MFUser.Address addresses2 = mFUser2.getAddresses();
                if (addresses2 != null) {
                    String work = addresses2.getWork();
                    if (work != null) {
                        commuteTimeSetting.setAddress(work);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        this.k.x0(true);
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void t() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            s76 s76 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getWork()) == null) {
                str = "";
            }
            s76.c2("Other", str);
        }
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void u(String str, String str2, mh5 mh5, boolean z, String str3) {
        boolean z2 = true;
        pq7.c(str, "displayInfo");
        pq7.c(str2, "address");
        pq7.c(mh5, "directionBy");
        pq7.c(str3, "format");
        FLogger.INSTANCE.getLocal().d(n, "saveCommuteTimeSetting - displayInfo=" + str + ", address=" + str2 + ", directionBy=" + mh5.getType() + ", avoidTolls=" + z + ", format=" + str3);
        this.k.b();
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (str.length() == 0) {
                if (str2.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    commuteTimeSetting.setAddress(str);
                }
            } else {
                commuteTimeSetting.setAddress(str2);
            }
            commuteTimeSetting.setAvoidTolls(z);
            commuteTimeSetting.setFormat(str3);
        }
        xw7 unused = gu7.d(k(), null, null, new a(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.r76
    public void v(String str) {
        pq7.c(str, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateFormatType, format = " + str);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setFormat(str);
        }
        this.k.s4(this.g);
    }
}
