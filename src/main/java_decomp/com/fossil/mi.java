package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.fitness.BinaryFile;
import com.fossil.fitness.FitnessAlgorithm;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsLogFileManager;
import com.fossil.fitness.Result;
import com.fossil.fitness.StatusCode;
import com.fossil.fitness.SyncMode;
import com.fossil.fitness.UserProfile;
import com.mapped.H60;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mi extends Bi {
    @DexIgnore
    public FitnessData[] V;
    @DexIgnore
    public StatusCode W;
    @DexIgnore
    public /* final */ HashMap<String, Long> X;
    @DexIgnore
    public long Y;
    @DexIgnore
    public /* final */ H60 Z;
    @DexIgnore
    public /* final */ SyncMode a0;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Mi(com.fossil.K5 r13, com.fossil.I60 r14, com.mapped.H60 r15, java.util.HashMap r16, java.lang.String r17, com.fossil.fitness.SyncMode r18, int r19) {
        /*
            r12 = this;
            r2 = r19 & 8
            if (r2 == 0) goto L_0x0044
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
        L_0x0009:
            r2 = r19 & 16
            if (r2 == 0) goto L_0x0041
            java.lang.String r2 = "UUID.randomUUID().toString()"
            java.lang.String r10 = com.fossil.E.a(r2)
        L_0x0013:
            r2 = r19 & 32
            if (r2 == 0) goto L_0x0019
            com.fossil.fitness.SyncMode r18 = com.fossil.fitness.SyncMode.FULL_SYNC
        L_0x0019:
            com.fossil.Yp r5 = com.fossil.Yp.n
            com.fossil.Ke r2 = com.fossil.Ke.b
            java.lang.String r3 = r13.x
            com.fossil.Ob r4 = com.fossil.Ob.e
            short r6 = r2.a(r3, r4)
            r7 = 1
            r9 = 0
            r11 = 64
            r2 = r12
            r3 = r13
            r4 = r14
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r12.Z = r15
            r0 = r18
            r12.a0 = r0
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            r12.X = r2
            r2 = -1
            r12.Y = r2
            return
        L_0x0041:
            r10 = r17
            goto L_0x0013
        L_0x0044:
            r8 = r16
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Mi.<init>(com.fossil.K5, com.fossil.I60, com.mapped.H60, java.util.HashMap, java.lang.String, com.fossil.fitness.SyncMode, int):void");
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public void B() {
        if (Q3.f.f(this.x.a())) {
            Lp.h(this, new Nm(this.w, this.x, this.Z, false, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 32), new Kg(this), new Yg(this), new Lh(this), null, null, 48, null);
        } else {
            super.B();
        }
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp, com.fossil.Mj
    public JSONObject C() {
        JSONObject put = super.C().put(Zm1.BIOMETRIC_PROFILE.b(), this.Z.c());
        Wg6.b(put, "super.optionDescription(\u2026ofile.valueDescription())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public JSONObject E() {
        Object obj;
        String name;
        JSONObject E = super.E();
        Jd0 jd0 = Jd0.W;
        FitnessData[] fitnessDataArr = this.V;
        JSONObject k = G80.k(E, jd0, fitnessDataArr != null ? G80.i(fitnessDataArr) : null);
        Jd0 jd02 = Jd0.n2;
        StatusCode statusCode = this.W;
        if (statusCode == null || (name = statusCode.name()) == null || (obj = Iy1.b(name)) == null) {
            obj = JSONObject.NULL;
        }
        return G80.k(G80.k(G80.k(G80.k(k, jd02, obj), Jd0.P5, Integer.valueOf(this.X.size())), Jd0.Q5, Long.valueOf(this.Y)), Jd0.b6, this.a0.name());
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void M(ArrayList<J0> arrayList) {
        Rm6 unused = Gu7.d(Qw7.b, null, null, new If(this, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void b0(ArrayList<J0> arrayList) {
        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
        for (T t : arrayList) {
            arrayList2.add(new BinaryFile(t.f, (int) (t.i / ((long) 1000))));
        }
        ArrayList<BinaryFile> arrayList3 = new ArrayList<>(arrayList2);
        FitnessAlgorithm create = FitnessAlgorithm.create(new UserProfile((short) this.Z.getAge(), this.Z.getGender().b(), ((float) this.Z.getHeightInCentimeter()) / 100.0f, (float) this.Z.getWeightInKilogram()));
        Wg6.b(create, "FitnessAlgorithm.create(userProfile)");
        ArrayList<String> list = GpsLogFileManager.defaultManager().list();
        Wg6.b(list, "GpsLogFileManager.defaultManager().list()");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String str = list.get(i);
            File file = new File(str);
            HashMap<String, Long> hashMap = this.X;
            Wg6.b(str, "filePath");
            hashMap.put(str, Long.valueOf(file.length()));
            this.o.post(new Yh(this, file, i, list));
        }
        long currentTimeMillis = System.currentTimeMillis();
        Ky1 ky1 = Ky1.DEBUG;
        arrayList3.size();
        this.a0.name();
        Result parseWithBinaries = create.parseWithBinaries(arrayList3, this.a0);
        Wg6.b(parseWithBinaries, "fitnessAlgorithm.parseWi\u2026BinaryFileList, syncMode)");
        this.Y = System.currentTimeMillis() - currentTimeMillis;
        this.W = parseWithBinaries.getStatus();
        if (parseWithBinaries.getStatus() == StatusCode.SUCCESS) {
            ArrayList<FitnessData> fitnessData = parseWithBinaries.getFitnessData();
            Wg6.b(fitnessData, "parsedResult.fitnessData");
            Object[] array = fitnessData.toArray(new FitnessData[0]);
            if (array != null) {
                this.V = (FitnessData[]) array;
                Ky1 ky12 = Ky1.DEBUG;
                FitnessData[] fitnessDataArr = this.V;
                if (fitnessDataArr != null) {
                    G80.i(fitnessDataArr).toString(2);
                    return;
                }
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Ky1 ky13 = Ky1.DEBUG;
        parseWithBinaries.getStatus();
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public Object x() {
        FitnessData[] fitnessDataArr = this.V;
        return fitnessDataArr != null ? fitnessDataArr : new FitnessData[0];
    }
}
