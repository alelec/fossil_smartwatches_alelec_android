package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gr4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ ArrayList<Explore> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleTextView a;
        @DexIgnore
        public ImageView b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Gr4 gr4, View view) {
            super(view);
            Wg6.c(view, "view");
            this.a = (FlexibleTextView) view.findViewById(2131362386);
            this.b = (ImageView) view.findViewById(2131362726);
        }

        @DexIgnore
        public final void a(Explore explore, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareTutorialAdapter", "build - position=" + i);
            if (explore != null) {
                FlexibleTextView flexibleTextView = this.a;
                Wg6.b(flexibleTextView, "ftvContent");
                flexibleTextView.setText(explore.getDescription());
                ImageView imageView = this.b;
                Wg6.b(imageView, "ivIcon");
                imageView.setBackground(W6.f(PortfolioApp.get.instance(), explore.getBackground()));
            }
        }
    }

    @DexIgnore
    public Gr4(ArrayList<Explore> arrayList) {
        Wg6.c(arrayList, "mData");
        this.a = arrayList;
    }

    @DexIgnore
    public Ai g(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558687, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026_tutorial, parent, false)");
        return new Ai(this, inflate);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void h(List<? extends Explore> list) {
        Wg6.c(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        if (getItemCount() > i && i != -1) {
            ((Ai) viewHolder).a(this.a.get(i), i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return g(viewGroup, i);
    }
}
