package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ug3 extends Zc2 implements Iterable<String> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ug3> CREATOR; // = new Wg3();
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public Ug3(Bundle bundle) {
        this.b = bundle;
    }

    @DexIgnore
    public final Long A(String str) {
        return Long.valueOf(this.b.getLong(str));
    }

    @DexIgnore
    public final Double D(String str) {
        return Double.valueOf(this.b.getDouble(str));
    }

    @DexIgnore
    public final String F(String str) {
        return this.b.getString(str);
    }

    @DexIgnore
    public final int c() {
        return this.b.size();
    }

    @DexIgnore
    public final Object h(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public final Iterator<String> iterator() {
        return new Tg3(this);
    }

    @DexIgnore
    public final Bundle k() {
        return new Bundle(this.b);
    }

    @DexIgnore
    public final String toString() {
        return this.b.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.e(parcel, 2, k(), false);
        Bd2.b(parcel, a2);
    }
}
