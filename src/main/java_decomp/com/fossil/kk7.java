package com.fossil;

import com.fossil.Gk7;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kk7<K, V> extends Gk7<K, V, Provider<V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<K, V> extends Gk7.Ai<K, V, Provider<V>> {
        @DexIgnore
        public Bi(int i) {
            super(i);
        }

        @DexIgnore
        public Kk7<K, V> b() {
            return new Kk7<>(this.a);
        }

        @DexIgnore
        public Bi<K, V> c(K k, Provider<V> provider) {
            super.a(k, provider);
            return this;
        }
    }

    @DexIgnore
    public Kk7(Map<K, Provider<V>> map) {
        super(map);
    }

    @DexIgnore
    public static <K, V> Bi<K, V> b(int i) {
        return new Bi<>(i);
    }

    @DexIgnore
    public Map<K, Provider<V>> c() {
        return a();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return c();
    }
}
