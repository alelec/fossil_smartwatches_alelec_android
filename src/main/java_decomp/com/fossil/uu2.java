package com.fossil;

import com.fossil.Cv2;
import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uu2 extends E13<Uu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Uu2 zzh;
    @DexIgnore
    public static volatile Z23<Uu2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public Cv2 zze;
    @DexIgnore
    public Cv2 zzf;
    @DexIgnore
    public boolean zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Uu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Uu2.zzh);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai B(boolean z) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Uu2) this.c).I(z);
            return this;
        }

        @DexIgnore
        public final Ai x(int i) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Uu2) this.c).C(i);
            return this;
        }

        @DexIgnore
        public final Ai y(Cv2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Uu2) this.c).H((Cv2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai z(Cv2 cv2) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Uu2) this.c).M(cv2);
            return this;
        }
    }

    /*
    static {
        Uu2 uu2 = new Uu2();
        zzh = uu2;
        E13.u(Uu2.class, uu2);
    }
    */

    @DexIgnore
    public static Ai S() {
        return (Ai) zzh.w();
    }

    @DexIgnore
    public final void C(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final void H(Cv2 cv2) {
        cv2.getClass();
        this.zze = cv2;
        this.zzc |= 2;
    }

    @DexIgnore
    public final void I(boolean z) {
        this.zzc |= 8;
        this.zzg = z;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int K() {
        return this.zzd;
    }

    @DexIgnore
    public final void M(Cv2 cv2) {
        cv2.getClass();
        this.zzf = cv2;
        this.zzc |= 4;
    }

    @DexIgnore
    public final Cv2 N() {
        Cv2 cv2 = this.zze;
        return cv2 == null ? Cv2.c0() : cv2;
    }

    @DexIgnore
    public final boolean O() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final Cv2 P() {
        Cv2 cv2 = this.zzf;
        return cv2 == null ? Cv2.c0() : cv2;
    }

    @DexIgnore
    public final boolean Q() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final boolean R() {
        return this.zzg;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Uu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u1004\u0000\u0002\u1009\u0001\u0003\u1009\u0002\u0004\u1007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                Z23<Uu2> z232 = zzi;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Uu2.class) {
                    try {
                        z23 = zzi;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzh);
                            zzi = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
