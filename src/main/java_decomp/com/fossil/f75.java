package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class F75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewHeartRateCalendar q;

    @DexIgnore
    public F75(Object obj, View view, int i, RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
        super(obj, view, i);
        this.q = recyclerViewHeartRateCalendar;
    }
}
