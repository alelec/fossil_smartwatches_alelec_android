package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw5;
import com.fossil.Cr4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M76 extends BaseFragment implements W76, Cr4.Ai {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<P35> g;
    @DexIgnore
    public V76 h;
    @DexIgnore
    public Bw5 i;
    @DexIgnore
    public Cr4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final M76 a() {
            return new M76();
        }

        @DexIgnore
        public final String b() {
            return M76.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Bw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ M76 a;

        @DexIgnore
        public Bi(M76 m76) {
            this.a = m76;
        }

        @DexIgnore
        @Override // com.fossil.Bw5.Bi
        public void a(String str) {
            Wg6.c(str, "address");
            M76.N6(this.a).n(Wt7.u0(str).toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ P35 c;

        @DexIgnore
        public Ci(View view, P35 p35) {
            this.b = view;
            this.c = p35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.x.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                P35 p35 = this.c;
                Wg6.b(p35, "binding");
                p35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = M76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                Wg6.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ M76 b;

        @DexIgnore
        public Di(M76 m76, P35 p35) {
            this.b = m76;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            Cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.f(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                Wg6.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = N76.s.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                this.b.k = fullText.toString();
                if (this.b.k != null) {
                    String str = this.b.k;
                    if (str != null) {
                        if (str.length() > 0) {
                            V76 N6 = M76.N6(this.b);
                            String str2 = this.b.k;
                            if (str2 == null) {
                                Wg6.i();
                                throw null;
                            } else if (str2 != null) {
                                N6.n(Wt7.u0(str2).toString());
                            } else {
                                throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ P35 b;

        @DexIgnore
        public Ei(M76 m76, P35 p35) {
            this.b = p35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.b.r;
            Wg6.b(imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ M76 b;

        @DexIgnore
        public Fi(M76 m76, P35 p35) {
            this.b = m76;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            Wg6.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ M76 b;

        @DexIgnore
        public Gi(M76 m76) {
            this.b = m76;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ M76 b;
        @DexIgnore
        public /* final */ /* synthetic */ P35 c;

        @DexIgnore
        public Hi(M76 m76, P35 p35) {
            this.b = m76;
            this.c = p35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.k = "";
            this.c.q.setText("");
            this.b.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ M76 b;

        @DexIgnore
        public Ii(M76 m76) {
            this.b = m76;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
            Editable text;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            M76 m76 = this.b;
            P35 p35 = (P35) M76.M6(m76).a();
            if (p35 == null || (flexibleAutoCompleteTextView = p35.q) == null || (text = flexibleAutoCompleteTextView.getText()) == null || (str = text.toString()) == null) {
                str = "";
            }
            MapPickerActivity.a.b(aVar, m76, 0.0d, 0.0d, str, 6, null);
        }
    }

    /*
    static {
        String simpleName = M76.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 M6(M76 m76) {
        G37<P35> g37 = m76.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ V76 N6(M76 m76) {
        V76 v76 = m76.h;
        if (v76 != null) {
            return v76;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cr4.Ai
    public void D1(boolean z) {
        G37<P35> g37 = this.g;
        if (g37 != null) {
            P35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.k = null;
                        T6();
                        return;
                    }
                }
                Q6();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        String str;
        G37<P35> g37 = this.g;
        if (g37 == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (g37.a() == null) {
            return true;
        } else {
            V76 v76 = this.h;
            if (v76 != null) {
                String str2 = this.k;
                if (str2 == null) {
                    str = null;
                } else if (str2 != null) {
                    str = Wt7.u0(str2).toString();
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                }
                v76.n(str);
                return true;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.W76
    public void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            Cr4 cr4 = new Cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            G37<P35> g37 = this.g;
            if (g37 != null) {
                P35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    Wg6.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = Wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    Q6();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(V76 v76) {
        R6(v76);
    }

    @DexIgnore
    public void Q6() {
        G37<P35> g37 = this.g;
        if (g37 != null) {
            P35 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.z;
                Wg6.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void R6(V76 v76) {
        Wg6.c(v76, "presenter");
        this.h = v76;
    }

    @DexIgnore
    @Override // com.fossil.W76
    public void S3(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void S6(String str) {
        Wg6.c(str, "address");
        G37<P35> g37 = this.g;
        if (g37 != null) {
            P35 a2 = g37.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.k = str;
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void T6() {
        G37<P35> g37 = this.g;
        if (g37 != null) {
            P35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp instance = PortfolioApp.get.instance();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(instance.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                Wg6.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.z;
                Wg6.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W76
    public void V1(String str) {
        boolean z = true;
        Wg6.c(str, "address");
        if (isActive()) {
            G37<P35> g37 = this.g;
            if (g37 != null) {
                P35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    Wg6.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = Wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.W76
    public void h0(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        Wg6.c(list, "recentSearchedList");
        Q6();
        if (!list.isEmpty()) {
            Bw5 bw5 = this.i;
            if (bw5 != null) {
                bw5.k(Pm7.j0(list));
            }
            G37<P35> g37 = this.g;
            if (g37 != null) {
                P35 a2 = g37.a();
                if (a2 != null && (linearLayout2 = a2.z) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<P35> g372 = this.g;
        if (g372 != null) {
            P35 a3 = g372.a();
            if (a3 != null && (linearLayout = a3.z) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                S6(string);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        P35 p35 = (P35) Aq0.f(layoutInflater, 2131558517, viewGroup, false, A6());
        String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d)) {
            p35.x.setBackgroundColor(Color.parseColor(d));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = p35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(W6.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new Di(this, p35));
        flexibleAutoCompleteTextView.addTextChangedListener(new Ei(this, p35));
        flexibleAutoCompleteTextView.setOnKeyListener(new Fi(this, p35));
        p35.u.setOnClickListener(new Gi(this));
        p35.r.setOnClickListener(new Hi(this, p35));
        p35.v.setOnClickListener(new Ii(this));
        Bw5 bw5 = new Bw5();
        bw5.l(new Bi(this));
        this.i = bw5;
        RecyclerView recyclerView = p35.B;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        Wg6.b(p35, "binding");
        View n = p35.n();
        Wg6.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Ci(n, p35));
        this.g = new G37<>(this, p35);
        return p35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        V76 v76 = this.h;
        if (v76 != null) {
            v76.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        V76 v76 = this.h;
        if (v76 != null) {
            v76.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.W76
    public void setTitle(String str) {
        Wg6.c(str, "title");
        G37<P35> g37 = this.g;
        if (g37 != null) {
            P35 a2 = g37.a();
            FlexibleTextView flexibleTextView = a2 != null ? a2.t : null;
            if (flexibleTextView != null) {
                Wg6.b(flexibleTextView, "mBinding.get()?.ftvTitle!!");
                flexibleTextView.setText(str);
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
