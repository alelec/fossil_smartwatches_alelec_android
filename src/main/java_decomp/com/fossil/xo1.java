package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xo1 extends O80 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xo1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Xo1 a(Parcel parcel) {
            return new Xo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xo1 createFromParcel(Parcel parcel) {
            return new Xo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xo1[] newArray(int i) {
            return new Xo1[i];
        }
    }

    @DexIgnore
    public Xo1() {
        super(Ap1.TIMER, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ Xo1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Xo1(Vw1 vw1) {
        super(Ap1.TIMER, vw1, null, 4);
    }
}
