package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ao extends Ro {
    @DexIgnore
    public /* final */ Yn1 S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ao(K5 k5, I60 i60, Yn1 yn1, short s, String str, int i) {
        super(k5, i60, Yp.A, true, (i & 8) != 0 ? Ke.b.b(k5.x, Ob.h) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? E.a("UUID.randomUUID().toString()") : str, false, 160);
        this.S = yn1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.v, this.S.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public byte[] M() {
        try {
            Aa aa = Aa.d;
            short s = this.D;
            Ry1 ry1 = this.x.a().h().get(Short.valueOf(Ob.h.b));
            if (ry1 == null) {
                ry1 = Hd0.y.d();
            }
            return aa.a(s, ry1, this.S);
        } catch (Sx1 e) {
            return new byte[0];
        }
    }
}
