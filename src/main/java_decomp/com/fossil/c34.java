package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class C34<K, V> extends H34<Map.Entry<K, V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ A34<K, V> map;

        @DexIgnore
        public Ai(A34<K, V> a34) {
            this.map = a34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<K, V> extends C34<K, V> {
        @DexIgnore
        @Weak
        public /* final */ transient A34<K, V> c;
        @DexIgnore
        public /* final */ transient Map.Entry<K, V>[] d;

        @DexIgnore
        public Bi(A34<K, V> a34, Map.Entry<K, V>[] entryArr) {
            this.c = a34;
            this.d = entryArr;
        }

        @DexIgnore
        @Override // com.fossil.H34
        public Y24<Map.Entry<K, V>> createAsList() {
            return new M44(this, this.d);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
        public H54<Map.Entry<K, V>> iterator() {
            return P34.l(this.d);
        }

        @DexIgnore
        @Override // com.fossil.C34
        public A34<K, V> map() {
            return this.c;
        }
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        V v = map().get(entry.getKey());
        return v != null && v.equals(entry.getValue());
    }

    @DexIgnore
    @Override // com.fossil.H34
    public int hashCode() {
        return map().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.H34
    public boolean isHashCodeFast() {
        return map().isHashCodeFast();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return map().isPartialView();
    }

    @DexIgnore
    public abstract A34<K, V> map();

    @DexIgnore
    public int size() {
        return map().size();
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.H34
    public Object writeReplace() {
        return new Ai(map());
    }
}
