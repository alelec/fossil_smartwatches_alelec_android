package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cd3 extends As2 implements Cc3 {
    @DexIgnore
    public Cd3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IProjectionDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Cc3
    public final We3 H0() throws RemoteException {
        Parcel e = e(3, d());
        We3 we3 = (We3) Es2.b(e, We3.CREATOR);
        e.recycle();
        return we3;
    }

    @DexIgnore
    @Override // com.fossil.Cc3
    public final Rg2 k0(LatLng latLng) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLng);
        Parcel e = e(2, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Cc3
    public final LatLng m2(Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Es2.c(d, rg2);
        Parcel e = e(1, d);
        LatLng latLng = (LatLng) Es2.b(e, LatLng.CREATOR);
        e.recycle();
        return latLng;
    }
}
