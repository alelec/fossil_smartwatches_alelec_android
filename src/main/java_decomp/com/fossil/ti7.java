package com.fossil;

import android.content.Context;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ti7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;

    @DexIgnore
    public Ti7(Context context) {
        this.b = context;
    }

    @DexIgnore
    public final void run() {
        Tg7.a(Ig7.r).l();
        Ei7.e(this.b, true);
        Gh7.b(this.b);
        Qi7.f(this.b);
        Thread.UncaughtExceptionHandler unused = Ig7.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Ah7());
        if (Fg7.I() == Gg7.APP_LAUNCH) {
            Ig7.o(this.b, -1);
        }
        if (Fg7.K()) {
            Ig7.m.b("Init MTA StatService success.");
        }
    }
}
