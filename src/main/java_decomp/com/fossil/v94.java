package com.fossil;

import android.content.Context;
import java.io.File;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V94 {
    @DexIgnore
    public static /* final */ Ci d; // = new Ci();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Bi b;
    @DexIgnore
    public U94 c;

    @DexIgnore
    public interface Bi {
        @DexIgnore
        File a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements U94 {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.U94
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.U94
        public String b() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.U94
        public byte[] c() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.U94
        public void d() {
        }

        @DexIgnore
        @Override // com.fossil.U94
        public void e(long j, String str) {
        }
    }

    @DexIgnore
    public V94(Context context, Bi bi) {
        this(context, bi, null);
    }

    @DexIgnore
    public V94(Context context, Bi bi, String str) {
        this.a = context;
        this.b = bi;
        this.c = d;
        g(str);
    }

    @DexIgnore
    public void a() {
        this.c.d();
    }

    @DexIgnore
    public void b(Set<String> set) {
        File[] listFiles = this.b.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(e(file))) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    public byte[] c() {
        return this.c.c();
    }

    @DexIgnore
    public String d() {
        return this.c.b();
    }

    @DexIgnore
    public final String e(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        return lastIndexOf == -1 ? name : name.substring(20, lastIndexOf);
    }

    @DexIgnore
    public final File f(String str) {
        return new File(this.b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    @DexIgnore
    public final void g(String str) {
        this.c.a();
        this.c = d;
        if (str != null) {
            if (!R84.l(this.a, "com.crashlytics.CollectCustomLogs", true)) {
                X74.f().b("Preferences requested no custom logs. Aborting log file creation.");
            } else {
                h(f(str), 65536);
            }
        }
    }

    @DexIgnore
    public void h(File file, int i) {
        this.c = new X94(file, i);
    }

    @DexIgnore
    public void i(long j, String str) {
        this.c.e(j, str);
    }
}
