package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L74 extends M74 {
    @DexIgnore
    public /* final */ List<A74<?>> componentsInCycle;

    @DexIgnore
    public L74(List<A74<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.componentsInCycle = list;
    }

    @DexIgnore
    public List<A74<?>> getComponentsInCycle() {
        return this.componentsInCycle;
    }
}
