package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fb0 extends Va0 {
    @DexIgnore
    public static /* final */ Eb0 CREATOR; // = new Eb0(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ Fb0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.c);
        order.put(this.d ? (byte) 1 : 0);
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Fb0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Fb0 fb0 = (Fb0) obj;
            if (this.d != fb0.d) {
                return false;
            }
            return this.c == fb0.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.SwitchActivityInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return (this.c * 31) + Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.P3, Byte.valueOf(this.c)), Jd0.T3, Boolean.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
