package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface M81 {
    @DexIgnore
    Object a(G51 g51, Bitmap bitmap, F81 f81, Xe6<? super Bitmap> xe6);

    @DexIgnore
    String key();
}
