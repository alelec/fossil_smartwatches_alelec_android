package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pr3 extends Zq3 {
    @DexIgnore
    public String d;
    @DexIgnore
    public Set<Integer> e;
    @DexIgnore
    public Map<Integer, Rr3> f;
    @DexIgnore
    public Long g;
    @DexIgnore
    public Long h;

    @DexIgnore
    public Pr3(Yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    @Override // com.fossil.Zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final Rr3 u(int i) {
        if (this.f.containsKey(Integer.valueOf(i))) {
            return this.f.get(Integer.valueOf(i));
        }
        Rr3 rr3 = new Rr3(this, this.d, null);
        this.f.put(Integer.valueOf(i), rr3);
        return rr3;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0661, code lost:
        if (r3.E() == false) goto L_0x06d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x0663, code lost:
        r3 = java.lang.Integer.valueOf(r3.G());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x066b, code lost:
        r5.c("Invalid property filter ID. appId, id", r10, java.lang.String.valueOf(r3));
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x06d0, code lost:
        r3 = null;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:112:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:263:0x0375 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.Uu2> v(java.lang.String r25, java.util.List<com.fossil.Wu2> r26, java.util.List<com.fossil.Ev2> r27, java.lang.Long r28, java.lang.Long r29) {
        /*
        // Method dump skipped, instructions count: 1934
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Pr3.v(java.lang.String, java.util.List, java.util.List, java.lang.Long, java.lang.Long):java.util.List");
    }

    @DexIgnore
    public final boolean w(int i, int i2) {
        if (this.f.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.f.get(Integer.valueOf(i)).d.get(i2);
    }
}
