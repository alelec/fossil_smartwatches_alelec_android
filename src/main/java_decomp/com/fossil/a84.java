package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A84 {
    @DexIgnore
    public /* final */ Kb4 a; // = new Kb4();
    @DexIgnore
    public /* final */ J64 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public PackageManager d;
    @DexIgnore
    public String e;
    @DexIgnore
    public PackageInfo f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public H94 l;
    @DexIgnore
    public C94 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Mt3<Wc4, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ Qc4 b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;

        @DexIgnore
        public Ai(String str, Qc4 qc4, Executor executor) {
            this.a = str;
            this.b = qc4;
            this.c = executor;
        }

        @DexIgnore
        public Nt3<Void> a(Wc4 wc4) throws Exception {
            try {
                A84.this.i(wc4, this.a, this.b, this.c, true);
                return null;
            } catch (Exception e) {
                X74.f().e("Error performing auto configuration.", e);
                throw e;
            }
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Nt3' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Mt3
        public /* bridge */ /* synthetic */ Nt3<Void> then(Wc4 wc4) throws Exception {
            return a(wc4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Mt3<Void, Wc4> {
        @DexIgnore
        public /* final */ /* synthetic */ Qc4 a;

        @DexIgnore
        public Bi(A84 a84, Qc4 qc4) {
            this.a = qc4;
        }

        @DexIgnore
        public Nt3<Wc4> a(Void r2) throws Exception {
            return this.a.a();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Nt3' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Mt3
        public /* bridge */ /* synthetic */ Nt3<Wc4> then(Void r2) throws Exception {
            return a(r2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Ft3<Void, Object> {
        @DexIgnore
        public Ci(A84 a84) {
        }

        @DexIgnore
        @Override // com.fossil.Ft3
        public Object then(Nt3<Void> nt3) throws Exception {
            if (nt3.q()) {
                return null;
            }
            X74.f().e("Error fetching settings.", nt3.l());
            return null;
        }
    }

    @DexIgnore
    public A84(J64 j64, Context context, H94 h94, C94 c94) {
        this.b = j64;
        this.c = context;
        this.l = h94;
        this.m = c94;
    }

    @DexIgnore
    public static String g() {
        return W84.i();
    }

    @DexIgnore
    public final Vc4 b(String str, String str2) {
        return new Vc4(str, str2, e().d(), this.h, this.g, R84.h(R84.p(d()), str2, this.h, this.g), this.j, E94.determineFrom(this.i).getId(), this.k, "0");
    }

    @DexIgnore
    public void c(Executor executor, Qc4 qc4) {
        this.m.d().s(executor, new Bi(this, qc4)).s(executor, new Ai(this.b.j().c(), qc4, executor));
    }

    @DexIgnore
    public Context d() {
        return this.c;
    }

    @DexIgnore
    public final H94 e() {
        return this.l;
    }

    @DexIgnore
    public String f() {
        return R84.u(this.c, "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public boolean h() {
        try {
            this.i = this.l.e();
            this.d = this.c.getPackageManager();
            String packageName = this.c.getPackageName();
            this.e = packageName;
            PackageInfo packageInfo = this.d.getPackageInfo(packageName, 0);
            this.f = packageInfo;
            this.g = Integer.toString(packageInfo.versionCode);
            this.h = this.f.versionName == null ? "0.0" : this.f.versionName;
            this.j = this.d.getApplicationLabel(this.c.getApplicationInfo()).toString();
            this.k = Integer.toString(this.c.getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            X74.f().e("Failed init", e2);
            return false;
        }
    }

    @DexIgnore
    public final void i(Wc4 wc4, String str, Qc4 qc4, Executor executor, boolean z) {
        if ("new".equals(wc4.a)) {
            if (j(wc4, str, z)) {
                qc4.o(Pc4.SKIP_CACHE_LOOKUP, executor);
            } else {
                X74.f().e("Failed to create app with Crashlytics service.", null);
            }
        } else if ("configured".equals(wc4.a)) {
            qc4.o(Pc4.SKIP_CACHE_LOOKUP, executor);
        } else if (wc4.f) {
            X74.f().b("Server says an update is required - forcing a full App update.");
            k(wc4, str, z);
        }
    }

    @DexIgnore
    public final boolean j(Wc4 wc4, String str, boolean z) {
        return new Dd4(f(), wc4.b, this.a, g()).i(b(wc4.e, str), z);
    }

    @DexIgnore
    public final boolean k(Wc4 wc4, String str, boolean z) {
        return new Gd4(f(), wc4.b, this.a, g()).i(b(wc4.e, str), z);
    }

    @DexIgnore
    public Qc4 l(Context context, J64 j64, Executor executor) {
        Qc4 l2 = Qc4.l(context, j64.j().c(), this.l, this.a, this.g, this.h, f(), this.m);
        l2.p(executor).i(executor, new Ci(this));
        return l2;
    }
}
