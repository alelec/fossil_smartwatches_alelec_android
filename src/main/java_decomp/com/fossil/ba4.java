package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ba4 extends Ta4.Ci {
    @DexIgnore
    public /* final */ Ua4<Ta4.Ci.Bii> a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Ci.Aii {
        @DexIgnore
        public Ua4<Ta4.Ci.Bii> a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Aii
        public Ta4.Ci a() {
            String str = "";
            if (this.a == null) {
                str = " files";
            }
            if (str.isEmpty()) {
                return new Ba4(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Aii
        public Ta4.Ci.Aii b(Ua4<Ta4.Ci.Bii> ua4) {
            if (ua4 != null) {
                this.a = ua4;
                return this;
            }
            throw new NullPointerException("Null files");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Aii
        public Ta4.Ci.Aii c(String str) {
            this.b = str;
            return this;
        }
    }

    @DexIgnore
    public Ba4(Ua4<Ta4.Ci.Bii> ua4, String str) {
        this.a = ua4;
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Ci
    public Ua4<Ta4.Ci.Bii> b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Ci
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Ci)) {
            return false;
        }
        Ta4.Ci ci = (Ta4.Ci) obj;
        if (this.a.equals(ci.b())) {
            String str = this.b;
            if (str == null) {
                if (ci.c() == null) {
                    return true;
                }
            } else if (str.equals(ci.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        String str = this.b;
        return (str == null ? 0 : str.hashCode()) ^ ((hashCode ^ 1000003) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "FilesPayload{files=" + this.a + ", orgId=" + this.b + "}";
    }
}
