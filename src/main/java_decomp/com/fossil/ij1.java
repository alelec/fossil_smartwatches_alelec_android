package com.fossil;

import com.fossil.Cj1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ij1 implements Cj1, Bj1 {
    @DexIgnore
    public /* final */ Cj1 a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public volatile Bj1 c;
    @DexIgnore
    public volatile Bj1 d;
    @DexIgnore
    public Cj1.Ai e;
    @DexIgnore
    public Cj1.Ai f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public Ij1(Object obj, Cj1 cj1) {
        Cj1.Ai ai = Cj1.Ai.CLEARED;
        this.e = ai;
        this.f = ai;
        this.b = obj;
        this.a = cj1;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public void a(Bj1 bj1) {
        synchronized (this.b) {
            if (!bj1.equals(this.c)) {
                this.f = Cj1.Ai.FAILED;
                return;
            }
            this.e = Cj1.Ai.FAILED;
            if (this.a != null) {
                this.a.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Bj1, com.fossil.Cj1
    public boolean b() {
        boolean z;
        synchronized (this.b) {
            z = this.d.b() || this.c.b();
        }
        return z;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v1, types: [com.fossil.Cj1] */
    /* JADX WARN: Type inference failed for: r2v3 */
    @Override // com.fossil.Cj1
    public Cj1 c() {
        Object r2;
        synchronized (this.b) {
            Cj1 cj1 = this.a;
            this = this;
            if (cj1 != null) {
                r2 = this.a.c();
            }
        }
        return r2;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            this.e = Cj1.Ai.CLEARED;
            this.f = Cj1.Ai.CLEARED;
            this.d.clear();
            this.c.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean d(Bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = m() && bj1.equals(this.c) && !b();
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean e(Bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = n() && (bj1.equals(this.c) || this.e != Cj1.Ai.SUCCESS);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void f() {
        synchronized (this.b) {
            this.g = true;
            try {
                if (!(this.e == Cj1.Ai.SUCCESS || this.f == Cj1.Ai.RUNNING)) {
                    this.f = Cj1.Ai.RUNNING;
                    this.d.f();
                }
                if (this.g && this.e != Cj1.Ai.RUNNING) {
                    this.e = Cj1.Ai.RUNNING;
                    this.c.f();
                }
            } finally {
                this.g = false;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0017 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.fossil.Bj1
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.fossil.Bj1 r4) {
        /*
            r3 = this;
            boolean r1 = r4 instanceof com.fossil.Ij1
            r0 = 0
            if (r1 == 0) goto L_0x0018
            com.fossil.Ij1 r4 = (com.fossil.Ij1) r4
            com.fossil.Bj1 r1 = r3.c
            if (r1 != 0) goto L_0x0019
            com.fossil.Bj1 r1 = r4.c
            if (r1 != 0) goto L_0x0018
        L_0x000f:
            com.fossil.Bj1 r1 = r3.d
            if (r1 != 0) goto L_0x0024
            com.fossil.Bj1 r1 = r4.d
            if (r1 != 0) goto L_0x0018
        L_0x0017:
            r0 = 1
        L_0x0018:
            return r0
        L_0x0019:
            com.fossil.Bj1 r1 = r3.c
            com.fossil.Bj1 r2 = r4.c
            boolean r1 = r1.g(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x000f
        L_0x0024:
            com.fossil.Bj1 r1 = r3.d
            com.fossil.Bj1 r2 = r4.d
            boolean r1 = r1.g(r2)
            if (r1 == 0) goto L_0x0018
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ij1.g(com.fossil.Bj1):boolean");
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean h() {
        boolean z;
        synchronized (this.b) {
            z = this.e == Cj1.Ai.CLEARED;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public void i(Bj1 bj1) {
        synchronized (this.b) {
            if (bj1.equals(this.d)) {
                this.f = Cj1.Ai.SUCCESS;
                return;
            }
            this.e = Cj1.Ai.SUCCESS;
            if (this.a != null) {
                this.a.i(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == Cj1.Ai.RUNNING;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public boolean j() {
        boolean z;
        synchronized (this.b) {
            z = this.e == Cj1.Ai.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Cj1
    public boolean k(Bj1 bj1) {
        boolean z;
        synchronized (this.b) {
            z = l() && bj1.equals(this.c) && this.e != Cj1.Ai.PAUSED;
        }
        return z;
    }

    @DexIgnore
    public final boolean l() {
        Cj1 cj1 = this.a;
        return cj1 == null || cj1.k(this);
    }

    @DexIgnore
    public final boolean m() {
        Cj1 cj1 = this.a;
        return cj1 == null || cj1.d(this);
    }

    @DexIgnore
    public final boolean n() {
        Cj1 cj1 = this.a;
        return cj1 == null || cj1.e(this);
    }

    @DexIgnore
    public void o(Bj1 bj1, Bj1 bj12) {
        this.c = bj1;
        this.d = bj12;
    }

    @DexIgnore
    @Override // com.fossil.Bj1
    public void pause() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = Cj1.Ai.PAUSED;
                this.d.pause();
            }
            if (!this.e.isComplete()) {
                this.e = Cj1.Ai.PAUSED;
                this.c.pause();
            }
        }
    }
}
