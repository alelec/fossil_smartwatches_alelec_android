package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Io1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Oo1 d;
    @DexIgnore
    public /* final */ byte[] e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Io1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Io1 createFromParcel(Parcel parcel) {
            byte readByte = parcel.readByte();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Parcelable readParcelable = parcel.readParcelable(Oo1.class.getClassLoader());
                if (readParcelable != null) {
                    return new Io1(readByte, readString, (Oo1) readParcelable);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Io1[] newArray(int i) {
            return new Io1[i];
        }
    }

    @DexIgnore
    public Io1(byte b2, String str, Oo1 oo1) {
        this.b = (byte) b2;
        this.c = str;
        this.d = oo1;
        this.e = a();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Io1(byte b2, String str, Oo1 oo1, int i, Qg6 qg6) {
        this(b2, str, (i & 4) != 0 ? new Oo1("", new byte[0]) : oo1);
    }

    @DexIgnore
    public final byte[] a() {
        byte[] p;
        String a2 = Iy1.a(this.c);
        Charset c2 = Hd0.y.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c2);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            if (this.d.d()) {
                p = new byte[0];
            } else {
                String fileName = this.d.getFileName();
                Charset defaultCharset = Charset.defaultCharset();
                Wg6.b(defaultCharset, "Charset.defaultCharset()");
                if (fileName != null) {
                    byte[] bytes2 = fileName.getBytes(defaultCharset);
                    Wg6.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                    p = Dm7.p(bytes2, (byte) 0);
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            int length = bytes.length;
            int length2 = p.length;
            int i = length + 8 + length2;
            byte[] array = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN).putShort((short) i).put((byte) 8).put(this.b).putShort((short) length).putShort((short) length2).put(bytes).put(p).array();
            Wg6.b(array, "ByteBuffer.allocate(entr\u2026\n                .array()");
            return array;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] b() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Io1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Io1 io1 = (Io1) obj;
            if (this.b != io1.b) {
                return false;
            }
            if (!Wg6.a(this.c, io1.c)) {
                return false;
            }
            return !(Wg6.a(this.d, io1.d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationReplyMessage");
    }

    @DexIgnore
    public final String getMessageContent() {
        return this.c;
    }

    @DexIgnore
    public final Oo1 getMessageIcon() {
        return this.d;
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        short p = Hy1.p(this.b);
        return (((p * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.I4, Byte.valueOf(this.b)), Jd0.k, this.c), Jd0.g5, this.d.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.b);
        parcel.writeString(this.c);
        parcel.writeParcelable(this.d, i);
    }
}
