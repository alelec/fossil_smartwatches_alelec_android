package com.fossil;

import com.google.gson.Gson;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oz4 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore
    public Oz4() {
        Gson d = new Zi4().d();
        Wg6.b(d, "GsonBuilder().create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(Date date) {
        String str;
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.n.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                return str;
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            str = null;
        }
    }

    @DexIgnore
    public final String b(Ht4 ht4) {
        Wg6.c(ht4, "owner");
        try {
            return this.a.u(ht4, Ht4.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final Ht4 c(String str) {
        Wg6.c(str, "value");
        try {
            return (Ht4) this.a.k(str, Ht4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Date d(String str) {
        Date date;
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.n.get();
            if (simpleDateFormat != null) {
                date = simpleDateFormat.parse(str);
                return date;
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            date = null;
        }
    }
}
