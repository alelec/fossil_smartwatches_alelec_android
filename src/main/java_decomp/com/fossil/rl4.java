package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rl4 extends Exception {
    @DexIgnore
    public Rl4() {
    }

    @DexIgnore
    public Rl4(String str) {
        super(str);
    }

    @DexIgnore
    public Rl4(Throwable th) {
        super(th);
    }
}
