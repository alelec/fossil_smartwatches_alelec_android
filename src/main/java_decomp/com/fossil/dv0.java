package com.fossil;

import android.graphics.Canvas;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Dv0 {
    @DexIgnore
    void a(View view);

    @DexIgnore
    void b(View view);

    @DexIgnore
    void c(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z);

    @DexIgnore
    void d(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z);
}
