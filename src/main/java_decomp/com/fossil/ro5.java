package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ro5 implements Qo5 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Mo5> b;
    @DexIgnore
    public /* final */ Po5 c; // = new Po5();
    @DexIgnore
    public /* final */ Gh<Mo5> d;
    @DexIgnore
    public /* final */ Vh e;
    @DexIgnore
    public /* final */ Vh f;
    @DexIgnore
    public /* final */ Vh g;
    @DexIgnore
    public /* final */ Vh h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<Mo5> {
        @DexIgnore
        public Ai(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Mo5 mo5) {
            if (mo5.c() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, mo5.c());
            }
            if (mo5.l() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, mo5.l());
            }
            mi.bindLong(3, (long) mo5.h());
            if (mo5.e() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, mo5.e());
            }
            String a2 = Ro5.this.c.a(mo5.a());
            if (a2 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a2);
            }
            if (mo5.b() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, mo5.b());
            }
            if (mo5.d() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, mo5.d());
            }
            if (mo5.i() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, mo5.i());
            }
            mi.bindLong(9, mo5.m() ? 1 : 0);
            if (mo5.f() == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, mo5.f());
            }
            if (mo5.j() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, mo5.j());
            }
            if (mo5.k() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, mo5.k());
            }
            if (mo5.g() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, mo5.g());
            }
            mi.bindLong(14, mo5.n() ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Mo5 mo5) {
            a(mi, mo5);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `diana_watchface_preset` (`createdAt`,`updatedAt`,`pinType`,`id`,`buttons`,`checksumFace`,`faceUrl`,`previewFaceUrl`,`isActive`,`name`,`serialNumber`,`uid`,`originalItemIdInStore`,`isNew`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Gh<Mo5> {
        @DexIgnore
        public Bi(Ro5 ro5, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Mo5 mo5) {
            if (mo5.e() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, mo5.e());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Mo5 mo5) {
            a(mi, mo5);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `diana_watchface_preset` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Ro5 ro5, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Vh {
        @DexIgnore
        public Di(Ro5 ro5, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Vh {
        @DexIgnore
        public Ei(Ro5 ro5, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE diana_watchface_preset SET pinType=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Vh {
        @DexIgnore
        public Fi(Ro5 ro5, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM diana_watchface_preset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements Callable<List<Mo5>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Gi(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public List<Mo5> a() throws Exception {
            Cursor b2 = Ex0.b(Ro5.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "createdAt");
                int c2 = Dx0.c(b2, "updatedAt");
                int c3 = Dx0.c(b2, "pinType");
                int c4 = Dx0.c(b2, "id");
                int c5 = Dx0.c(b2, "buttons");
                int c6 = Dx0.c(b2, "checksumFace");
                int c7 = Dx0.c(b2, "faceUrl");
                int c8 = Dx0.c(b2, "previewFaceUrl");
                int c9 = Dx0.c(b2, "isActive");
                int c10 = Dx0.c(b2, "name");
                int c11 = Dx0.c(b2, "serialNumber");
                int c12 = Dx0.c(b2, "uid");
                int c13 = Dx0.c(b2, "originalItemIdInStore");
                int c14 = Dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    Mo5 mo5 = new Mo5(b2.getString(c4), Ro5.this.c.b(b2.getString(c5)), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getInt(c9) != 0, b2.getString(c10), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getInt(c14) != 0);
                    mo5.q(b2.getString(c));
                    mo5.t(b2.getString(c2));
                    mo5.s(b2.getInt(c3));
                    arrayList.add(mo5);
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ List<Mo5> call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore
    public Ro5(Oh oh) {
        this.a = oh;
        this.b = new Ai(oh);
        this.d = new Bi(this, oh);
        this.e = new Ci(this, oh);
        this.f = new Di(this, oh);
        this.g = new Ei(this, oh);
        this.h = new Fi(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void a() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.h.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public Mo5 b(String str) {
        Throwable th;
        Mo5 mo5;
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE isActive = 1 AND serialNumber=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "createdAt");
            int c3 = Dx0.c(b2, "updatedAt");
            int c4 = Dx0.c(b2, "pinType");
            int c5 = Dx0.c(b2, "id");
            int c6 = Dx0.c(b2, "buttons");
            int c7 = Dx0.c(b2, "checksumFace");
            int c8 = Dx0.c(b2, "faceUrl");
            int c9 = Dx0.c(b2, "previewFaceUrl");
            int c10 = Dx0.c(b2, "isActive");
            int c11 = Dx0.c(b2, "name");
            int c12 = Dx0.c(b2, "serialNumber");
            int c13 = Dx0.c(b2, "uid");
            int c14 = Dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = Dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new Mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void c(Mo5 mo5) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.d.handle(mo5);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public List<Mo5> d(String str) {
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 0", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "createdAt");
            int c3 = Dx0.c(b2, "updatedAt");
            int c4 = Dx0.c(b2, "pinType");
            int c5 = Dx0.c(b2, "id");
            int c6 = Dx0.c(b2, "buttons");
            int c7 = Dx0.c(b2, "checksumFace");
            int c8 = Dx0.c(b2, "faceUrl");
            int c9 = Dx0.c(b2, "previewFaceUrl");
            int c10 = Dx0.c(b2, "isActive");
            int c11 = Dx0.c(b2, "name");
            int c12 = Dx0.c(b2, "serialNumber");
            int c13 = Dx0.c(b2, "uid");
            int c14 = Dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = Dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    Mo5 mo5 = new Mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                    arrayList.add(mo5);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void e(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.f.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public List<Mo5> f(String str) {
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "createdAt");
            int c3 = Dx0.c(b2, "updatedAt");
            int c4 = Dx0.c(b2, "pinType");
            int c5 = Dx0.c(b2, "id");
            int c6 = Dx0.c(b2, "buttons");
            int c7 = Dx0.c(b2, "checksumFace");
            int c8 = Dx0.c(b2, "faceUrl");
            int c9 = Dx0.c(b2, "previewFaceUrl");
            int c10 = Dx0.c(b2, "isActive");
            int c11 = Dx0.c(b2, "name");
            int c12 = Dx0.c(b2, "serialNumber");
            int c13 = Dx0.c(b2, "uid");
            int c14 = Dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = Dx0.c(b2, "isNew");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    Mo5 mo5 = new Mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                    arrayList.add(mo5);
                }
                b2.close();
                f2.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void g(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.e.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public Mo5 getPresetById(String str) {
        Throwable th;
        Mo5 mo5;
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE id=?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "createdAt");
            int c3 = Dx0.c(b2, "updatedAt");
            int c4 = Dx0.c(b2, "pinType");
            int c5 = Dx0.c(b2, "id");
            int c6 = Dx0.c(b2, "buttons");
            int c7 = Dx0.c(b2, "checksumFace");
            int c8 = Dx0.c(b2, "faceUrl");
            int c9 = Dx0.c(b2, "previewFaceUrl");
            int c10 = Dx0.c(b2, "isActive");
            int c11 = Dx0.c(b2, "name");
            int c12 = Dx0.c(b2, "serialNumber");
            int c13 = Dx0.c(b2, "uid");
            int c14 = Dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = Dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new Mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public int h(String str) {
        int i = 0;
        Rh f2 = Rh.f("SELECT COUNT(id) FROM diana_watchface_preset WHERE serialNumber=? AND pinType <> 3", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i = b2.getInt(0);
            }
            return i;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void i(String str, int i) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.g.acquire();
        acquire.bindLong(1, (long) i);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public Long[] insert(List<Mo5> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public void j(List<Mo5> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(list);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public Mo5 k() {
        Throwable th;
        Mo5 mo5;
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE isActive = 1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "createdAt");
            int c3 = Dx0.c(b2, "updatedAt");
            int c4 = Dx0.c(b2, "pinType");
            int c5 = Dx0.c(b2, "id");
            int c6 = Dx0.c(b2, "buttons");
            int c7 = Dx0.c(b2, "checksumFace");
            int c8 = Dx0.c(b2, "faceUrl");
            int c9 = Dx0.c(b2, "previewFaceUrl");
            int c10 = Dx0.c(b2, "isActive");
            int c11 = Dx0.c(b2, "name");
            int c12 = Dx0.c(b2, "serialNumber");
            int c13 = Dx0.c(b2, "uid");
            int c14 = Dx0.c(b2, "originalItemIdInStore");
            try {
                int c15 = Dx0.c(b2, "isNew");
                if (b2.moveToFirst()) {
                    mo5 = new Mo5(b2.getString(c5), this.c.b(b2.getString(c6)), b2.getString(c7), b2.getString(c8), b2.getString(c9), b2.getInt(c10) != 0, b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getInt(c15) != 0);
                    mo5.q(b2.getString(c2));
                    mo5.t(b2.getString(c3));
                    mo5.s(b2.getInt(c4));
                } else {
                    mo5 = null;
                }
                b2.close();
                f2.m();
                return mo5;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public long l(Mo5 mo5) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(mo5);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qo5
    public LiveData<List<Mo5>> m(String str) {
        Rh f2 = Rh.f("SELECT * FROM diana_watchface_preset WHERE serialNumber = ? AND pinType <> 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Gi gi = new Gi(f2);
        return invalidationTracker.d(new String[]{"diana_watchface_preset"}, false, gi);
    }
}
