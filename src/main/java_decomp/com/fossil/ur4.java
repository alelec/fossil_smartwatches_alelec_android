package com.fossil;

import android.content.Context;
import com.fossil.Vr4;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ur4 implements Vr4 {
    @DexIgnore
    public /* final */ List<String> a; // = Hm7.i("Agree terms of use and privacy");

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean b() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean d() {
        return !Wg6.a(h(), "CN");
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean e() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean f() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean g() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public String h() {
        return Vr4.Ai.a(this);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean i() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean j() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public void k(BaseFragment baseFragment) {
        Wg6.c(baseFragment, "fragment");
        HelpActivity.a aVar = HelpActivity.B;
        Context requireContext = baseFragment.requireContext();
        Wg6.b(requireContext, "fragment.requireContext()");
        aVar.a(requireContext);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean l() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean m(List<String> list) {
        Wg6.c(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean n() {
        return false;
    }
}
