package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rs3 implements Parcelable.Creator<Ss3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ss3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Sc2 sc2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i = Ad2.v(parcel, t);
            } else if (l != 2) {
                Ad2.B(parcel, t);
            } else {
                sc2 = (Sc2) Ad2.e(parcel, t, Sc2.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Ss3(i, sc2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ss3[] newArray(int i) {
        return new Ss3[i];
    }
}
