package com.fossil;

import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pk6 implements MembersInjector<ActiveTimeDetailActivity> {
    @DexIgnore
    public static void a(ActiveTimeDetailActivity activeTimeDetailActivity, ActiveTimeDetailPresenter activeTimeDetailPresenter) {
        activeTimeDetailActivity.A = activeTimeDetailPresenter;
    }
}
