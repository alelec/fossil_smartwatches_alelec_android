package com.fossil;

import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.HomePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wy5 implements MembersInjector<HomeActivity> {
    @DexIgnore
    public static void a(HomeActivity homeActivity, HomePresenter homePresenter) {
        homeActivity.A = homePresenter;
    }
}
