package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey4 extends ts0 {
    @DexIgnore
    public static /* final */ String n;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Boolean> f1004a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<String, String>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ServerError> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ u08 j; // = w08.b(false, 1, null);
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public /* final */ LiveData<List<Object>> l;
    @DexIgnore
    public /* final */ zt4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public rv7<kz4<List<xs4>>> f1005a;
        @DexIgnore
        public rv7<kz4<List<xs4>>> b;
        @DexIgnore
        public rv7<kz4<List<xs4>>> c;
        @DexIgnore
        public rv7<kz4<List<xs4>>> d;

        @DexIgnore
        public a(rv7<kz4<List<xs4>>> rv7, rv7<kz4<List<xs4>>> rv72, rv7<kz4<List<xs4>>> rv73, rv7<kz4<List<xs4>>> rv74) {
            pq7.c(rv7, "sentRequestFriend");
            pq7.c(rv72, NativeProtocol.AUDIENCE_FRIENDS);
            pq7.c(rv73, "receivedRequestFriends");
            pq7.c(rv74, "blockedFriends");
            this.f1005a = rv7;
            this.b = rv72;
            this.c = rv73;
            this.d = rv74;
        }

        @DexIgnore
        public final rv7<kz4<List<xs4>>> a() {
            return this.d;
        }

        @DexIgnore
        public final rv7<kz4<List<xs4>>> b() {
            return this.b;
        }

        @DexIgnore
        public final rv7<kz4<List<xs4>>> c() {
            return this.c;
        }

        @DexIgnore
        public final rv7<kz4<List<xs4>>> d() {
            return this.f1005a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1005a, aVar.f1005a) || !pq7.a(this.b, aVar.b) || !pq7.a(this.c, aVar.c) || !pq7.a(this.d, aVar.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            rv7<kz4<List<xs4>>> rv7 = this.f1005a;
            int hashCode = rv7 != null ? rv7.hashCode() : 0;
            rv7<kz4<List<xs4>>> rv72 = this.b;
            int hashCode2 = rv72 != null ? rv72.hashCode() : 0;
            rv7<kz4<List<xs4>>> rv73 = this.c;
            int hashCode3 = rv73 != null ? rv73.hashCode() : 0;
            rv7<kz4<List<xs4>>> rv74 = this.d;
            if (rv74 != null) {
                i = rv74.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "AllFriendRequest(sentRequestFriend=" + this.f1005a + ", friends=" + this.b + ", receivedRequestFriends=" + this.c + ", blockedFriends=" + this.d + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ey4 f1006a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$1", f = "BCFriendTabViewModel.kt", l = {243}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $allFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, MutableLiveData mutableLiveData, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$allFriends = mutableLiveData;
                this.$it = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$allFriends, this.$it, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX INFO: finally extract failed */
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                u08 u08;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    u08 = this.this$0.f1006a.j;
                    this.L$0 = iv7;
                    this.L$1 = u08;
                    this.label = 1;
                    if (u08.a(null, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    u08 = (u08) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                try {
                    MutableLiveData mutableLiveData = this.$allFriends;
                    ey4 ey4 = this.this$0.f1006a;
                    List list = this.$it;
                    pq7.b(list, "it");
                    mutableLiveData.l(ey4.v(list));
                    tl7 tl7 = tl7.f3441a;
                    u08.b(null);
                    return tl7.f3441a;
                } catch (Throwable th) {
                    u08.b(null);
                    throw th;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ey4$b$b")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$2", f = "BCFriendTabViewModel.kt", l = {86, 86}, m = "invokeSuspend")
        /* renamed from: com.fossil.ey4$b$b  reason: collision with other inner class name */
        public static final class C0071b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0071b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0071b bVar = new C0071b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0071b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004f  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 0
                    r5 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0051
                    if (r0 == r3) goto L_0x0035
                    if (r0 != r5) goto L_0x002d
                    java.lang.Object r0 = r7.L$0
                    com.fossil.iv7 r0 = (com.fossil.iv7) r0
                    com.fossil.el7.b(r8)
                L_0x0016:
                    com.fossil.ey4$b r0 = r7.this$0
                    com.fossil.ey4 r0 = r0.f1006a
                    androidx.lifecycle.MutableLiveData r0 = com.fossil.ey4.d(r0)
                    r1 = 0
                    java.lang.Boolean r1 = com.fossil.ao7.a(r1)
                    com.fossil.cl7 r1 = com.fossil.hl7.a(r1, r6)
                    r0.l(r1)
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x002c:
                    return r0
                L_0x002d:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0035:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.ey4 r0 = (com.fossil.ey4) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r8)
                    r3 = r0
                    r2 = r8
                L_0x0042:
                    r0 = r2
                    com.fossil.ey4$a r0 = (com.fossil.ey4.a) r0
                    r7.L$0 = r1
                    r7.label = r5
                    java.lang.Object r0 = r3.u(r0, r7)
                    if (r0 != r4) goto L_0x0016
                    r0 = r4
                    goto L_0x002c
                L_0x0051:
                    com.fossil.el7.b(r8)
                    com.fossil.iv7 r1 = r7.p$
                    com.fossil.ey4$b r0 = r7.this$0
                    com.fossil.ey4 r0 = r0.f1006a
                    androidx.lifecycle.MutableLiveData r0 = com.fossil.ey4.d(r0)
                    java.lang.Boolean r2 = com.fossil.ao7.a(r3)
                    com.fossil.cl7 r2 = com.fossil.hl7.a(r2, r6)
                    r0.l(r2)
                    com.fossil.ey4$b r0 = r7.this$0
                    com.fossil.ey4 r0 = r0.f1006a
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r3
                    java.lang.Object r2 = r0.l(r7)
                    if (r2 != r4) goto L_0x007b
                    r0 = r4
                    goto L_0x002c
                L_0x007b:
                    r3 = r0
                    goto L_0x0042
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ey4.b.C0071b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(ey4 ey4) {
            this.f1006a = ey4;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<Object>> apply(List<xs4> list) {
            MutableLiveData<List<Object>> mutableLiveData = new MutableLiveData<>();
            if (list.isEmpty()) {
                if (!this.f1006a.k) {
                    this.f1006a.f1004a.l(Boolean.TRUE);
                }
                mutableLiveData.l(hm7.e());
            } else {
                this.f1006a.f1004a.l(Boolean.FALSE);
                this.f1006a.b.l(hl7.a(Boolean.FALSE, null));
                xw7 unused = gu7.d(us0.a(this.f1006a), bw7.a(), null, new a(this, mutableLiveData, list, null), 2, null);
            }
            if (this.f1006a.k) {
                this.f1006a.k = false;
                xw7 unused2 = gu7.d(us0.a(this.f1006a), null, null, new C0071b(this, null), 3, null);
            }
            return mutableLiveData;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$block$1", f = "BCFriendTabViewModel.kt", l = {181}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ey4 ey4, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$friend, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object a2;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                zt4 zt4 = this.this$0.m;
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                a2 = zt4.a(xs4, this);
                if (a2 == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                a2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) a2;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(hl7.a(null, ao7.a(false)));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$cancelRequest$1", f = "BCFriendTabViewModel.kt", l = {205}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ey4 ey4, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$friend, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object b;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                zt4 zt4 = this.this$0.m;
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                b = zt4.b(xs4, this);
                if (b == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                b = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) b;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(hl7.a(null, ao7.a(false)));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {145, 145}, m = "fetchAll")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ey4 ey4, qn7 qn7) {
            super(qn7);
            this.this$0 = ey4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super rv7<? extends a>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super a>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ey4$f$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$blockedFriends$1", f = "BCFriendTabViewModel.kt", l = {160}, m = "invokeSuspend")
            /* renamed from: com.fossil.ey4$f$a$a  reason: collision with other inner class name */
            public static final class C0072a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0072a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0072a aVar = new C0072a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                    throw null;
                    //return ((C0072a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        zt4 zt4 = this.this$0.this$0.this$0.m;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object f = zt4.f(this);
                        return f == d ? d : f;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$friends$1", f = "BCFriendTabViewModel.kt", l = {152}, m = "invokeSuspend")
            public static final class b extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        zt4 zt4 = this.this$0.this$0.this$0.m;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object g = zt4.g(this);
                        return g == d ? d : g;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$receivedRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {156}, m = "invokeSuspend")
            public static final class c extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    throw null;
                    //return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                    throw null;
                    //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        zt4 zt4 = this.this$0.this$0.this$0.m;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object h = zt4.h(this);
                        return h == d ? d : h;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$sentRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {148}, m = "invokeSuspend")
            public static final class d extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public d(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    d dVar = new d(this.this$0, qn7);
                    dVar.p$ = (iv7) obj;
                    throw null;
                    //return dVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                    throw null;
                    //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        zt4 zt4 = this.this$0.this$0.this$0.m;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object i2 = zt4.i(this);
                        return i2 == d ? d : i2;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super a> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    return new a(gu7.b(iv7, null, null, new d(this, null), 3, null), gu7.b(iv7, null, null, new b(this, null), 3, null), gu7.b(iv7, null, null, new c(this, null), 3, null), gu7.b(iv7, null, null, new C0072a(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ey4 ey4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super rv7<? extends a>> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return gu7.b(this.p$, bw7.b(), null, new a(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {107, 108, 109, 110}, m = "handleFriendResult")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ey4 ey4, qn7 qn7) {
            super(qn7);
            this.this$0 = ey4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$refresh$1", f = "BCFriendTabViewModel.kt", l = {96, 96}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ey4 ey4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x004d  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x004f
                if (r0 == r2) goto L_0x0033
                if (r0 != r5) goto L_0x002b
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0015:
                com.fossil.ey4 r0 = r6.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.ey4.d(r0)
                r1 = 0
                java.lang.Boolean r1 = com.fossil.ao7.a(r1)
                r2 = 0
                com.fossil.cl7 r1 = com.fossil.hl7.a(r1, r2)
                r0.l(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002a:
                return r0
            L_0x002b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0033:
                java.lang.Object r0 = r6.L$1
                com.fossil.ey4 r0 = (com.fossil.ey4) r0
                java.lang.Object r1 = r6.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r7)
                r3 = r0
                r2 = r7
            L_0x0040:
                r0 = r2
                com.fossil.ey4$a r0 = (com.fossil.ey4.a) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.u(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x002a
            L_0x004f:
                com.fossil.el7.b(r7)
                com.fossil.iv7 r1 = r6.p$
                com.fossil.ey4 r0 = r6.this$0
                r6.L$0 = r1
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r0.l(r6)
                if (r2 != r4) goto L_0x0064
                r0 = r4
                goto L_0x002a
            L_0x0064:
                r3 = r0
                goto L_0x0040
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ey4.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$responseToFriendRequest$1", f = "BCFriendTabViewModel.kt", l = {219}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $confirmation;
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(ey4 ey4, boolean z, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
            this.$confirmation = z;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, this.$confirmation, this.$friend, qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                zt4 zt4 = this.this$0.m;
                boolean z = this.$confirmation;
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                p = zt4.p(z, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) p;
            if (kz4.c() != null) {
                this.this$0.f.l(hl7.a(this.$confirmation ? "bc_accept_friend_request" : "bc_reject_friend_request", ((xs4) kz4.c()).d()));
            } else {
                this.this$0.g.l(kz4.a());
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unblock$1", f = "BCFriendTabViewModel.kt", l = {193}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(ey4 ey4, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, this.$friend, qn7);
            jVar.p$ = (iv7) obj;
            throw null;
            //return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object t;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                zt4 zt4 = this.this$0.m;
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                t = zt4.t(xs4, this);
                if (t == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                t = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) t;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(hl7.a(null, ao7.a(false)));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unfriend$1", f = "BCFriendTabViewModel.kt", l = {170}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ey4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(ey4 ey4, xs4 xs4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ey4;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, this.$friend, qn7);
            kVar.p$ = (iv7) obj;
            throw null;
            //return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object s;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                zt4 zt4 = this.this$0.m;
                xs4 xs4 = this.$friend;
                this.L$0 = iv7;
                this.label = 1;
                s = zt4.s(xs4, this);
                if (s == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                s = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kz4 kz4 = (kz4) s;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(hl7.a(null, ao7.a(false)));
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = ey4.class.getSimpleName();
        pq7.b(simpleName, "BCFriendTabViewModel::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public ey4(on5 on5, zt4 zt4) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(zt4, "friendRepository");
        this.m = zt4;
        LiveData<List<Object>> c2 = ss0.c(cz4.a(this.m.m()), new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026\n        allFriends\n    }");
        this.l = c2;
    }

    @DexIgnore
    public final void j(xs4 xs4) {
        pq7.c(xs4, "friend");
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new c(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void k(xs4 xs4) {
        pq7.c(xs4, "friend");
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new d(this, xs4, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(com.fossil.qn7<? super com.fossil.ey4.a> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.ey4.e
            if (r0 == 0) goto L_0x002b
            r0 = r7
            com.fossil.ey4$e r0 = (com.fossil.ey4.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002b
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0051
            if (r0 == r4) goto L_0x003a
            if (r0 != r5) goto L_0x0032
            java.lang.Object r0 = r2.L$0
            com.fossil.ey4 r0 = (com.fossil.ey4) r0
            com.fossil.el7.b(r1)
        L_0x002a:
            return r1
        L_0x002b:
            com.fossil.ey4$e r0 = new com.fossil.ey4$e
            r0.<init>(r6, r7)
            r2 = r0
            goto L_0x0015
        L_0x0032:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003a:
            java.lang.Object r0 = r2.L$0
            com.fossil.ey4 r0 = (com.fossil.ey4) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0042:
            r0 = r1
            com.fossil.rv7 r0 = (com.fossil.rv7) r0
            r2.L$0 = r6
            r2.label = r5
            java.lang.Object r1 = r0.l(r2)
            if (r1 != r3) goto L_0x002a
            r1 = r3
            goto L_0x002a
        L_0x0051:
            com.fossil.el7.b(r1)
            com.fossil.ey4$f r0 = new com.fossil.ey4$f
            r1 = 0
            r0.<init>(r6, r1)
            r2.L$0 = r6
            r2.label = r4
            java.lang.Object r1 = com.fossil.jv7.e(r0, r2)
            if (r1 != r3) goto L_0x0042
            r1 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ey4.l(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<Object>> m() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<String> n() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.f1004a;
    }

    @DexIgnore
    public final LiveData<Boolean> p() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, Boolean>> q() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<cl7<String, String>> s() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<ServerError> t() {
        return this.g;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        if (r1 != null) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0190, code lost:
        if (r1 != null) goto L_0x0192;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01ec, code lost:
        if (r1 != null) goto L_0x01ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0244, code lost:
        if (r1 != null) goto L_0x0246;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0265  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02ca  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02e0 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.fossil.ey4.a r16, com.fossil.qn7<? super com.fossil.tl7> r17) {
        /*
        // Method dump skipped, instructions count: 761
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ey4.u(com.fossil.ey4$a, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final List<Object> v(List<xs4> list) {
        return list.isEmpty() ? hm7.e() : hj5.c(list);
    }

    @DexIgnore
    public final void w() {
        xw7 unused = gu7.d(us0.a(this), null, null, new h(this, null), 3, null);
    }

    @DexIgnore
    public final void x(xs4 xs4, boolean z) {
        pq7.c(xs4, "friend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = n;
        local.e(str, "responseToFriendRequest - friend: " + xs4 + " - confirmation: " + z);
        this.e.l(xs4.d());
        if (o37.b(PortfolioApp.h0.c())) {
            xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new i(this, z, xs4, null), 2, null);
        } else {
            this.d.l(Boolean.TRUE);
        }
    }

    @DexIgnore
    public final void y(xs4 xs4) {
        pq7.c(xs4, "friend");
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new j(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void z(xs4 xs4) {
        pq7.c(xs4, "friend");
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new k(this, xs4, null), 2, null);
    }
}
