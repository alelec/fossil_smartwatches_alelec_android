package com.fossil;

import android.os.Build;
import android.util.Log;
import com.fossil.Kk1;
import com.fossil.Sc1;
import com.fossil.Ua1;
import com.fossil.Vc1;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uc1<R> implements Sc1.Ai, Runnable, Comparable<Uc1<?>>, Kk1.Fi {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Object B;
    @DexIgnore
    public Thread C;
    @DexIgnore
    public Mb1 D;
    @DexIgnore
    public Mb1 E;
    @DexIgnore
    public Object F;
    @DexIgnore
    public Gb1 G;
    @DexIgnore
    public Wb1<?> H;
    @DexIgnore
    public volatile Sc1 I;
    @DexIgnore
    public volatile boolean J;
    @DexIgnore
    public volatile boolean K;
    @DexIgnore
    public /* final */ Tc1<R> b; // = new Tc1<>();
    @DexIgnore
    public /* final */ List<Throwable> c; // = new ArrayList();
    @DexIgnore
    public /* final */ Mk1 d; // = Mk1.a();
    @DexIgnore
    public /* final */ Ei e;
    @DexIgnore
    public /* final */ Mn0<Uc1<?>> f;
    @DexIgnore
    public /* final */ Di<?> g; // = new Di<>();
    @DexIgnore
    public /* final */ Fi h; // = new Fi();
    @DexIgnore
    public Qa1 i;
    @DexIgnore
    public Mb1 j;
    @DexIgnore
    public Sa1 k;
    @DexIgnore
    public Ad1 l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public Wc1 t;
    @DexIgnore
    public Ob1 u;
    @DexIgnore
    public Bi<R> v;
    @DexIgnore
    public int w;
    @DexIgnore
    public Hi x;
    @DexIgnore
    public Gi y;
    @DexIgnore
    public long z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c;

        /*
        static {
            int[] iArr = new int[Ib1.values().length];
            c = iArr;
            try {
                iArr[Ib1.SOURCE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                c[Ib1.TRANSFORMED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            int[] iArr2 = new int[Hi.values().length];
            b = iArr2;
            try {
                iArr2[Hi.RESOURCE_CACHE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[Hi.DATA_CACHE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                b[Hi.SOURCE.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                b[Hi.FINISHED.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[Hi.INITIALIZE.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            int[] iArr3 = new int[Gi.values().length];
            a = iArr3;
            try {
                iArr3[Gi.INITIALIZE.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Gi.SWITCH_TO_SOURCE_SERVICE.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[Gi.DECODE_DATA.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
        }
        */
    }

    @DexIgnore
    public interface Bi<R> {
        @DexIgnore
        void a(Dd1 dd1);

        @DexIgnore
        void c(Id1<R> id1, Gb1 gb1);

        @DexIgnore
        void d(Uc1<?> uc1);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci<Z> implements Vc1.Ai<Z> {
        @DexIgnore
        public /* final */ Gb1 a;

        @DexIgnore
        public Ci(Gb1 gb1) {
            this.a = gb1;
        }

        @DexIgnore
        @Override // com.fossil.Vc1.Ai
        public Id1<Z> a(Id1<Z> id1) {
            return Uc1.this.x(this.a, id1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<Z> {
        @DexIgnore
        public Mb1 a;
        @DexIgnore
        public Rb1<Z> b;
        @DexIgnore
        public Hd1<Z> c;

        @DexIgnore
        public void a() {
            this.a = null;
            this.b = null;
            this.c = null;
        }

        @DexIgnore
        public void b(Ei ei, Ob1 ob1) {
            Lk1.a("DecodeJob.encode");
            try {
                ei.a().a(this.a, new Rc1(this.b, this.c, ob1));
            } finally {
                this.c.h();
                Lk1.d();
            }
        }

        @DexIgnore
        public boolean c() {
            return this.c != null;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Rb1<X> */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.Hd1<X> */
        /* JADX WARN: Multi-variable type inference failed */
        public <X> void d(Mb1 mb1, Rb1<X> rb1, Hd1<X> hd1) {
            this.a = mb1;
            this.b = rb1;
            this.c = hd1;
        }
    }

    @DexIgnore
    public interface Ei {
        @DexIgnore
        Be1 a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public final boolean a(boolean z) {
            return (this.c || z || this.b) && this.a;
        }

        @DexIgnore
        public boolean b() {
            boolean a2;
            synchronized (this) {
                this.b = true;
                a2 = a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean c() {
            boolean a2;
            synchronized (this) {
                this.c = true;
                a2 = a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean d(boolean z) {
            boolean a2;
            synchronized (this) {
                this.a = true;
                a2 = a(z);
            }
            return a2;
        }

        @DexIgnore
        public void e() {
            synchronized (this) {
                this.b = false;
                this.a = false;
                this.c = false;
            }
        }
    }

    @DexIgnore
    public enum Gi {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    @DexIgnore
    public enum Hi {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    @DexIgnore
    public Uc1(Ei ei, Mn0<Uc1<?>> mn0) {
        this.e = ei;
        this.f = mn0;
    }

    @DexIgnore
    public final void A() {
        this.C = Thread.currentThread();
        this.z = Ek1.b();
        boolean z2 = false;
        while (!this.K && this.I != null && !(z2 = this.I.c())) {
            this.x = m(this.x);
            this.I = l();
            if (this.x == Hi.SOURCE) {
                b();
                return;
            }
        }
        if ((this.x == Hi.FINISHED || this.K) && !z2) {
            u();
        }
    }

    @DexIgnore
    public final <Data, ResourceType> Id1<R> B(Data data, Gb1 gb1, Gd1<Data, ResourceType, R> gd1) throws Dd1 {
        Ob1 n = n(gb1);
        Xb1<Data> l2 = this.i.h().l(data);
        try {
            return gd1.a(l2, n, this.m, this.s, new Ci(gb1));
        } finally {
            l2.a();
        }
    }

    @DexIgnore
    public final void C() {
        int i2 = Ai.a[this.y.ordinal()];
        if (i2 == 1) {
            this.x = m(Hi.INITIALIZE);
            this.I = l();
            A();
        } else if (i2 == 2) {
            A();
        } else if (i2 == 3) {
            k();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.y);
        }
    }

    @DexIgnore
    public final void D() {
        Throwable th;
        this.d.c();
        if (this.J) {
            if (this.c.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.c;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.J = true;
    }

    @DexIgnore
    public boolean E() {
        Hi m2 = m(Hi.INITIALIZE);
        return m2 == Hi.RESOURCE_CACHE || m2 == Hi.DATA_CACHE;
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void a(Mb1 mb1, Exception exc, Wb1<?> wb1, Gb1 gb1) {
        wb1.a();
        Dd1 dd1 = new Dd1("Fetching data failed", exc);
        dd1.setLoggingDetails(mb1, gb1, wb1.getDataClass());
        this.c.add(dd1);
        if (Thread.currentThread() != this.C) {
            this.y = Gi.SWITCH_TO_SOURCE_SERVICE;
            this.v.d(this);
            return;
        }
        A();
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void b() {
        this.y = Gi.SWITCH_TO_SOURCE_SERVICE;
        this.v.d(this);
    }

    @DexIgnore
    public void c() {
        this.K = true;
        Sc1 sc1 = this.I;
        if (sc1 != null) {
            sc1.cancel();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Uc1<?> uc1) {
        return h(uc1);
    }

    @DexIgnore
    @Override // com.fossil.Sc1.Ai
    public void e(Mb1 mb1, Object obj, Wb1<?> wb1, Gb1 gb1, Mb1 mb12) {
        this.D = mb1;
        this.F = obj;
        this.H = wb1;
        this.G = gb1;
        this.E = mb12;
        if (Thread.currentThread() != this.C) {
            this.y = Gi.DECODE_DATA;
            this.v.d(this);
            return;
        }
        Lk1.a("DecodeJob.decodeFromRetrievedData");
        try {
            k();
        } finally {
            Lk1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.Kk1.Fi
    public Mk1 f() {
        return this.d;
    }

    @DexIgnore
    public int h(Uc1<?> uc1) {
        int o = o() - uc1.o();
        return o == 0 ? this.w - uc1.w : o;
    }

    @DexIgnore
    public final <Data> Id1<R> i(Wb1<?> wb1, Data data, Gb1 gb1) throws Dd1 {
        if (data == null) {
            wb1.a();
            return null;
        }
        try {
            long b2 = Ek1.b();
            Id1<R> j2 = j(data, gb1);
            if (Log.isLoggable("DecodeJob", 2)) {
                q("Decoded result " + j2, b2);
            }
            return j2;
        } finally {
            wb1.a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: com.fossil.Gd1<Data, ?, R>, com.fossil.Gd1<Data, ResourceType, R> */
    public final <Data> Id1<R> j(Data data, Gb1 gb1) throws Dd1 {
        return B(data, gb1, (Gd1<Data, ?, R>) this.b.h(data.getClass()));
    }

    @DexIgnore
    public final void k() {
        Id1<R> id1;
        if (Log.isLoggable("DecodeJob", 2)) {
            r("Retrieved data", this.z, "data: " + this.F + ", cache key: " + this.D + ", fetcher: " + this.H);
        }
        try {
            id1 = i(this.H, this.F, this.G);
        } catch (Dd1 e2) {
            e2.setLoggingDetails(this.E, this.G);
            this.c.add(e2);
            id1 = null;
        }
        if (id1 != null) {
            t(id1, this.G);
        } else {
            A();
        }
    }

    @DexIgnore
    public final Sc1 l() {
        int i2 = Ai.b[this.x.ordinal()];
        if (i2 == 1) {
            return new Jd1(this.b, this);
        }
        if (i2 == 2) {
            return new Pc1(this.b, this);
        }
        if (i2 == 3) {
            return new Md1(this.b, this);
        }
        if (i2 == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.x);
    }

    @DexIgnore
    public final Hi m(Hi hi) {
        int i2 = Ai.b[hi.ordinal()];
        if (i2 == 1) {
            return this.t.a() ? Hi.DATA_CACHE : m(Hi.DATA_CACHE);
        }
        if (i2 == 2) {
            return this.A ? Hi.FINISHED : Hi.SOURCE;
        }
        if (i2 == 3 || i2 == 4) {
            return Hi.FINISHED;
        }
        if (i2 == 5) {
            return this.t.b() ? Hi.RESOURCE_CACHE : m(Hi.RESOURCE_CACHE);
        }
        throw new IllegalArgumentException("Unrecognized stage: " + hi);
    }

    @DexIgnore
    public final Ob1 n(Gb1 gb1) {
        Ob1 ob1 = this.u;
        if (Build.VERSION.SDK_INT < 26) {
            return ob1;
        }
        boolean z2 = gb1 == Gb1.RESOURCE_DISK_CACHE || this.b.w();
        Boolean bool = (Boolean) ob1.c(Gg1.i);
        if (bool != null && (!bool.booleanValue() || z2)) {
            return ob1;
        }
        Ob1 ob12 = new Ob1();
        ob12.d(this.u);
        ob12.e(Gg1.i, Boolean.valueOf(z2));
        return ob12;
    }

    @DexIgnore
    public final int o() {
        return this.k.ordinal();
    }

    @DexIgnore
    public Uc1<R> p(Qa1 qa1, Object obj, Ad1 ad1, Mb1 mb1, int i2, int i3, Class<?> cls, Class<R> cls2, Sa1 sa1, Wc1 wc1, Map<Class<?>, Sb1<?>> map, boolean z2, boolean z3, boolean z4, Ob1 ob1, Bi<R> bi, int i4) {
        this.b.u(qa1, obj, mb1, i2, i3, wc1, cls, cls2, sa1, ob1, map, z2, z3, this.e);
        this.i = qa1;
        this.j = mb1;
        this.k = sa1;
        this.l = ad1;
        this.m = i2;
        this.s = i3;
        this.t = wc1;
        this.A = z4;
        this.u = ob1;
        this.v = bi;
        this.w = i4;
        this.y = Gi.INITIALIZE;
        this.B = obj;
        return this;
    }

    @DexIgnore
    public final void q(String str, long j2) {
        r(str, j2, null);
    }

    @DexIgnore
    public final void r(String str, long j2, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(Ek1.a(j2));
        sb.append(", load key: ");
        sb.append(this.l);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    @DexIgnore
    public void run() {
        Lk1.b("DecodeJob#run(model=%s)", this.B);
        Wb1<?> wb1 = this.H;
        try {
            if (this.K) {
                u();
                if (wb1 != null) {
                    wb1.a();
                }
                Lk1.d();
                return;
            }
            C();
            if (wb1 != null) {
                wb1.a();
            }
            Lk1.d();
        } catch (Oc1 e2) {
            throw e2;
        } catch (Throwable th) {
            if (wb1 != null) {
                wb1.a();
            }
            Lk1.d();
            throw th;
        }
    }

    @DexIgnore
    public final void s(Id1<R> id1, Gb1 gb1) {
        D();
        this.v.c(id1, gb1);
    }

    @DexIgnore
    public final void t(Id1<R> id1, Gb1 gb1) {
        Hd1 hd1;
        if (id1 instanceof Ed1) {
            ((Ed1) id1).a();
        }
        if (this.g.c()) {
            Hd1 e2 = Hd1.e(id1);
            id1 = e2;
            hd1 = e2;
        } else {
            hd1 = null;
        }
        s(id1, gb1);
        this.x = Hi.ENCODE;
        try {
            if (this.g.c()) {
                this.g.b(this.e, this.u);
            }
            v();
        } finally {
            if (hd1 != null) {
                hd1.h();
            }
        }
    }

    @DexIgnore
    public final void u() {
        D();
        this.v.a(new Dd1("Failed to load resource", new ArrayList(this.c)));
        w();
    }

    @DexIgnore
    public final void v() {
        if (this.h.b()) {
            z();
        }
    }

    @DexIgnore
    public final void w() {
        if (this.h.c()) {
            z();
        }
    }

    @DexIgnore
    public <Z> Id1<Z> x(Gb1 gb1, Id1<Z> id1) {
        Sb1<Z> sb1;
        Id1<Z> id12;
        Ib1 ib1;
        Rb1<X> rb1;
        Mb1 qc1;
        Class<?> cls = id1.get().getClass();
        if (gb1 != Gb1.RESOURCE_DISK_CACHE) {
            sb1 = this.b.r(cls);
            id12 = sb1.b(this.i, id1, this.m, this.s);
        } else {
            sb1 = null;
            id12 = id1;
        }
        if (!id1.equals(id12)) {
            id1.b();
        }
        if (this.b.v(id12)) {
            Rb1<Z> n = this.b.n(id12);
            ib1 = n.b(this.u);
            rb1 = (Rb1<X>) n;
        } else {
            ib1 = Ib1.NONE;
            rb1 = null;
        }
        if (!this.t.d(!this.b.x(this.D), gb1, ib1)) {
            return id12;
        }
        if (rb1 != null) {
            int i2 = Ai.c[ib1.ordinal()];
            if (i2 == 1) {
                qc1 = new Qc1(this.D, this.j);
            } else if (i2 == 2) {
                qc1 = new Kd1(this.b.b(), this.D, this.j, this.m, this.s, sb1, cls, this.u);
            } else {
                throw new IllegalArgumentException("Unknown strategy: " + ib1);
            }
            Hd1 e2 = Hd1.e(id12);
            this.g.d(qc1, rb1, e2);
            return e2;
        }
        throw new Ua1.Di(id12.get().getClass());
    }

    @DexIgnore
    public void y(boolean z2) {
        if (this.h.d(z2)) {
            z();
        }
    }

    @DexIgnore
    public final void z() {
        this.h.e();
        this.g.a();
        this.b.a();
        this.J = false;
        this.i = null;
        this.j = null;
        this.u = null;
        this.k = null;
        this.l = null;
        this.v = null;
        this.x = null;
        this.I = null;
        this.C = null;
        this.D = null;
        this.F = null;
        this.G = null;
        this.H = null;
        this.z = 0;
        this.K = false;
        this.B = null;
        this.c.clear();
        this.f.a(this);
    }
}
