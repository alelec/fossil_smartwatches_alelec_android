package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class At2 implements Parcelable.Creator<Xs2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xs2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        Bundle bundle = null;
        String str = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 2:
                    j = Ad2.y(parcel, t);
                    break;
                case 3:
                    z = Ad2.m(parcel, t);
                    break;
                case 4:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 5:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 6:
                    str = Ad2.f(parcel, t);
                    break;
                case 7:
                    bundle = Ad2.a(parcel, t);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Xs2(j2, j, z, str3, str2, str, bundle);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xs2[] newArray(int i) {
        return new Xs2[i];
    }
}
