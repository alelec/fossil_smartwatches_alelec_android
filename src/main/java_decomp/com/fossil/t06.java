package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T06 implements MembersInjector<NotificationAppsPresenter> {
    @DexIgnore
    public static void a(NotificationAppsPresenter notificationAppsPresenter) {
        notificationAppsPresenter.P();
    }
}
