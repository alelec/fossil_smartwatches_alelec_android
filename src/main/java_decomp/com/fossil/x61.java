package com.fossil;

import android.os.Build;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X61 {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final X61 a() {
            if (Build.VERSION.SDK_INT < 26 || W61.b.a()) {
                return new Y61(false);
            }
            int i = Build.VERSION.SDK_INT;
            return (i == 26 || i == 27) ? B71.e : new Y61(true);
        }
    }

    @DexIgnore
    public X61() {
    }

    @DexIgnore
    public /* synthetic */ X61(Qg6 qg6) {
        this();
    }

    @DexIgnore
    public abstract boolean a(F81 f81);
}
