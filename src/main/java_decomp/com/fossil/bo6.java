package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.ao6;
import com.fossil.iq4;
import com.fossil.vu5;
import com.fossil.xu5;
import com.fossil.zu5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo6 extends zn6 {
    @DexIgnore
    public static /* final */ String G;
    @DexIgnore
    public static /* final */ a H; // = new a(null);
    @DexIgnore
    public /* final */ SummariesRepository A;
    @DexIgnore
    public /* final */ SleepSummariesRepository B;
    @DexIgnore
    public /* final */ zu5 C;
    @DexIgnore
    public /* final */ hu4 D;
    @DexIgnore
    public /* final */ tt4 E;
    @DexIgnore
    public /* final */ on5 F;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.h0.c().J());
    @DexIgnore
    public /* final */ ArrayList<b> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Device> g; // = new ArrayList<>();
    @DexIgnore
    public MFUser h;
    @DexIgnore
    public LiveData<h47<ActivityStatistic>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<h47<ActivitySummary>> j; // = new MutableLiveData();
    @DexIgnore
    public LiveData<h47<SleepStatistic>> k; // = new MutableLiveData();
    @DexIgnore
    public /* final */ LiveData<List<Device>> l; // = this.y.getAllDeviceAsLiveData();
    @DexIgnore
    public ActivityStatistic.ActivityDailyBest m;
    @DexIgnore
    public long n;
    @DexIgnore
    public /* final */ u08 o; // = w08.b(false, 1, null);
    @DexIgnore
    public /* final */ Handler p; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public boolean q; // = true;
    @DexIgnore
    public String r;
    @DexIgnore
    public /* final */ Runnable s; // = new h(this);
    @DexIgnore
    public /* final */ g t; // = new g(this);
    @DexIgnore
    public /* final */ WeakReference<ao6> u;
    @DexIgnore
    public /* final */ PortfolioApp v;
    @DexIgnore
    public /* final */ vu5 w;
    @DexIgnore
    public /* final */ xu5 x;
    @DexIgnore
    public /* final */ DeviceRepository y;
    @DexIgnore
    public /* final */ UserRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return bo6.G;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f464a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(String str, boolean z, String str2, int i, boolean z2, boolean z3) {
            pq7.c(str, "serial");
            pq7.c(str2, "deviceName");
            this.f464a = str;
            this.b = z;
            this.c = str2;
            this.d = i;
            this.e = z2;
            this.f = z3;
        }

        @DexIgnore
        public final int a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final String c() {
            return this.f464a;
        }

        @DexIgnore
        public final boolean d() {
            return this.e;
        }

        @DexIgnore
        public final boolean e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (!(pq7.a(this.f464a, bVar.f464a) && this.b == bVar.b && pq7.a(this.c, bVar.c) && this.d == bVar.d && this.e == bVar.e && this.f == bVar.f)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            int i2 = 1;
            String str = this.f464a;
            int hashCode = str != null ? str.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            String str2 = this.c;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i3 = this.d;
            boolean z2 = this.e;
            if (z2) {
                z2 = true;
            }
            boolean z3 = this.f;
            if (!z3) {
                i2 = z3 ? 1 : 0;
            }
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = z ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = z2 ? 1 : 0;
            return (((((((((hashCode * 31) + i4) * 31) + i) * 31) + i3) * 31) + i7) * 31) + i2;
        }

        @DexIgnore
        public String toString() {
            return "DeviceWrapper(serial=" + this.f464a + ", isConnected=" + this.b + ", deviceName=" + this.c + ", batteryLevel=" + this.d + ", isActive=" + this.e + ", isLatestFw=" + this.f + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1", f = "HomeProfilePresenter.kt", l = {430}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAddNewDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bo6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$checkCurrentChallenge$1$challenge$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.E.g(new String[]{"running", "waiting"}, xy4.f4212a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bo6 bo6, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bo6;
            this.$isAddNewDevice = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$isAddNewDevice, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.this$0.q) {
                    dv7 b = bw7.b();
                    a aVar = new a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.H3(false, null, this.$isAddNewDevice);
                    }
                    return tl7.f3441a;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ps4 ps4 = (ps4) g;
            if (ps4 == null) {
                this.this$0.F.m0(ao7.a(false));
                ao6 ao62 = this.this$0.d0().get();
                if (ao62 != null) {
                    ao62.H3(false, ps4, this.$isAddNewDevice);
                }
            } else {
                ao6 ao63 = this.this$0.d0().get();
                if (ao63 != null) {
                    ao63.H3(true, ps4, this.$isAddNewDevice);
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1", f = "HomeProfilePresenter.kt", l = {289}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bo6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$loadSocialProfile$1$socialProfile$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super it4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super it4> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.D.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bo6 bo6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bo6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                bo6 bo6 = this.this$0;
                Boolean b = bo6.F.b();
                pq7.b(b, "sharedPreferencesManager.bcStatus()");
                bo6.q = b.booleanValue();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bo6.H.a();
                local.e(a2, "loadProfile - isBCOn: " + this.this$0.q);
                if (this.this$0.q) {
                    dv7 b2 = bw7.b();
                    a aVar = new a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else {
                    ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.i5(null);
                    }
                    return tl7.f3441a;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            it4 it4 = (it4) g;
            ao6 ao62 = this.this$0.d0().get();
            if (ao62 != null) {
                ao62.i5(it4 != null ? it4.g() : null);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements iq4.e<vu5.a, iq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bo6 f465a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(bo6 bo6) {
            this.f465a = bo6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(iq4.a aVar) {
            pq7.c(aVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(vu5.a aVar) {
            pq7.c(aVar, "responseValue");
            MFUser a2 = aVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = bo6.H.a();
            local.d(a3, "loadUser " + a2);
            if (a2 != null) {
                this.f465a.h0(a2);
                ao6 ao6 = this.f465a.d0().get();
                if (ao6 != null) {
                    ao6.updateUser(a2);
                }
                ao6 ao62 = this.f465a.d0().get();
                if (ao62 != null) {
                    ao6.a.a(ao62, a2, null, 2, null);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements iq4.e<zu5.d, zu5.c> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bo6 f466a;

        @DexIgnore
        public f(bo6 bo6) {
            this.f466a = bo6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(zu5.c cVar) {
            pq7.c(cVar, "errorValue");
            ao6 ao6 = this.f466a.d0().get();
            if (ao6 != null) {
                ao6.k();
                ao6.o(cVar.a(), "");
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(zu5.d dVar) {
            pq7.c(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bo6.H.a(), "logOut success, hide loading}");
            ao6 ao6 = this.f466a.d0().get();
            if (ao6 != null) {
                ao6.k();
                ao6.i3();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bo6 f467a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(bo6 bo6) {
            this.f467a = bo6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bo6.H.a();
            local.d(a2, "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
            if (vt7.j(stringExtra, this.f467a.v.J(), true) && (!this.f467a.b0().isEmpty())) {
                Iterator<T> it = this.f467a.b0().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.c(), stringExtra)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 == null) {
                    return;
                }
                if (intExtra == ConnectionStateChange.GATT_ON.ordinal() || intExtra == ConnectionStateChange.GATT_OFF.ordinal()) {
                    t2.f(intExtra == ConnectionStateChange.GATT_ON.ordinal());
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = bo6.H.a();
                    local2.d(a3, "active device status change connected=" + t2.e());
                    ao6 ao6 = this.f467a.d0().get();
                    if (ao6 != null) {
                        ao6.G4(this.f467a.b0());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ bo6 b;

        @DexIgnore
        public h(bo6 bo6) {
            this.b = bo6;
        }

        @DexIgnore
        public final void run() {
            ao6 ao6 = this.b.d0().get();
            if (ao6 != null) {
                pq7.b(ao6, "it");
                if (ao6.isActive() && (!this.b.b0().isEmpty())) {
                    ao6.z2();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$onProfilePictureChanged$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Uri $imageUri;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bo6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(bo6 bo6, Uri uri, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bo6;
            this.$imageUri = uri;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.this$0, this.$imageUri, qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                try {
                    this.this$0.r = i37.f(!TextUtils.equals(this.$imageUri.getLastPathSegment(), "pickerImage.jpg") ? (Bitmap) tj5.a(PortfolioApp.h0.c()).e().J0(this.$imageUri).u0(((fj1) ((fj1) new fj1().l(wc1.f3916a)).m0(true)).o0(new hk5())).R0(200, 200).get() : ry5.q(this.$imageUri.getPath(), 256, true));
                    MFUser c0 = this.this$0.c0();
                    if (c0 != null) {
                        String str = this.this$0.r;
                        if (str != null) {
                            c0.setProfilePicture(str);
                            this.this$0.k0(this.this$0.c0());
                            return tl7.f3441a;
                        }
                        pq7.i();
                        throw null;
                    }
                    pq7.i();
                    throw null;
                } catch (Exception e) {
                    e.printStackTrace();
                    ao6 ao6 = this.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.E0();
                    }
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1", f = "HomeProfilePresenter.kt", l = {416}, m = "invokeSuspend")
    public static final class j extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bo6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$openCameraIntent$1$1$1$pickerImageIntent$1", f = "HomeProfilePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Intent>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ FragmentActivity $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(FragmentActivity fragmentActivity, qn7 qn7) {
                super(2, qn7);
                this.$it = fragmentActivity;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$it, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Intent> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return vk5.f(this.$it);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(bo6 bo6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bo6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            j jVar = new j(this.this$0, qn7);
            jVar.p$ = (iv7) obj;
            throw null;
            //return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((j) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ao6 ao6;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ao6 ao62 = this.this$0.d0().get();
                if (ao62 != null) {
                    if (ao62 != null) {
                        FragmentActivity activity = ((mz5) ao62).getActivity();
                        if (activity != null) {
                            dv7 h = this.this$0.h();
                            a aVar = new a(activity, null);
                            this.L$0 = iv7;
                            this.L$1 = ao62;
                            this.L$2 = activity;
                            this.L$3 = activity;
                            this.label = 1;
                            g = eu7.g(h, aVar, this);
                            if (g == d) {
                                return d;
                            }
                            ao6 = ao62;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                FragmentActivity fragmentActivity = (FragmentActivity) this.L$3;
                FragmentActivity fragmentActivity2 = (FragmentActivity) this.L$2;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                ao6 = (ao6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Intent intent = (Intent) g;
            if (intent != null) {
                ((mz5) ao6).startActivityForResult(intent, 1234);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter$start$1", f = "HomeProfilePresenter.kt", l = {162, 165}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bo6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements ls0<h47<? extends ActivitySummary>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ k f468a;

            @DexIgnore
            public a(k kVar) {
                this.f468a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<ActivitySummary> h47) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bo6.H.a();
                local.d(a2, "XXX- summaryChanged -- summary=" + h47);
                if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                    ActivitySummary c = h47 != null ? h47.c() : null;
                    this.f468a.this$0.n = c != null ? (long) c.getSteps() : 0;
                    bo6 bo6 = this.f468a.this$0;
                    bo6.j0(bo6.m, this.f468a.this$0.n);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements ls0<h47<? extends ActivityStatistic>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ k f469a;

            @DexIgnore
            public b(k kVar) {
                this.f469a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<ActivityStatistic> h47) {
                ActivityStatistic.ActivityDailyBest activityDailyBest = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bo6.H.a();
                local.d(a2, "start - mActivityStatisticLiveData -- resource=" + h47);
                if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                    ActivityStatistic c = h47 != null ? h47.c() : null;
                    ao6 ao6 = this.f469a.this$0.d0().get();
                    if (ao6 != null) {
                        ao6.X3(c);
                    }
                    bo6 bo6 = this.f469a.this$0;
                    if (c != null) {
                        activityDailyBest = c.getStepsBestDay();
                    }
                    bo6.m = activityDailyBest;
                    bo6 bo62 = this.f469a.this$0;
                    bo62.j0(bo62.m, this.f469a.this$0.n);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements ls0<h47<? extends SleepStatistic>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ k f470a;

            @DexIgnore
            public c(k kVar) {
                this.f470a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(h47<SleepStatistic> h47) {
                SleepStatistic sleepStatistic = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bo6.H.a();
                local.d(a2, "start - mSleepStatisticLiveData -- resource=" + h47);
                if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                    if (h47 != null) {
                        h47.c();
                    }
                    ao6 ao6 = this.f470a.this$0.d0().get();
                    if (ao6 != null) {
                        if (h47 != null) {
                            sleepStatistic = h47.c();
                        }
                        ao6.U2(sleepStatistic);
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d<T> implements ls0<List<? extends Device>> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ k f471a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ ArrayList $deviceWrappers;
                @DexIgnore
                public /* final */ /* synthetic */ List $devices;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public Object L$4;
                @DexIgnore
                public Object L$5;
                @DexIgnore
                public boolean Z$0;
                @DexIgnore
                public boolean Z$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ d this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bo6$k$d$a$a")
                /* renamed from: com.fossil.bo6$k$d$a$a  reason: collision with other inner class name */
                public static final class C0019a extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Device $deviceModel;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0019a(Device device, qn7 qn7, a aVar) {
                        super(2, qn7);
                        this.$deviceModel = device;
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0019a aVar = new C0019a(this.$deviceModel, qn7, this.this$0);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                        throw null;
                        //return ((C0019a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        yn7.d();
                        if (this.label == 0) {
                            el7.b(obj);
                            return this.this$0.this$0.f471a.this$0.y.getDeviceNameBySerial(this.$deviceModel.getDeviceId());
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class b extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ String $activeSerial;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public b(String str, qn7 qn7) {
                        super(2, qn7);
                        this.$activeSerial = str;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        b bVar = new b(this.$activeSerial, qn7);
                        bVar.p$ = (iv7) obj;
                        throw null;
                        //return bVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                        throw null;
                        //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            p37 a2 = p37.h.a();
                            String str = this.$activeSerial;
                            this.L$0 = iv7;
                            this.label = 1;
                            Object j = a2.j(str, null, this);
                            return j == d ? d : j;
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(d dVar, List list, ArrayList arrayList, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = dVar;
                    this.$devices = list;
                    this.$deviceWrappers = arrayList;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    a aVar = new a(this.this$0, this.$devices, this.$deviceWrappers, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:30:0x00c5 A[Catch:{ all -> 0x0276 }] */
                /* JADX WARNING: Removed duplicated region for block: B:39:0x00ee A[Catch:{ all -> 0x03b0 }] */
                /* JADX WARNING: Removed duplicated region for block: B:65:0x024a  */
                /* JADX WARNING: Removed duplicated region for block: B:84:0x02a4  */
                /* JADX WARNING: Removed duplicated region for block: B:93:0x02f6  */
                /* JADX WARNING: Removed duplicated region for block: B:96:0x0358  */
                /* JADX WARNING: Removed duplicated region for block: B:99:0x0382  */
                @Override // com.fossil.zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r20) {
                    /*
                    // Method dump skipped, instructions count: 948
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fossil.bo6.k.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public d(k kVar) {
                this.f471a = kVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(List<Device> list) {
                ArrayList arrayList = new ArrayList();
                this.f471a.this$0.g.clear();
                ArrayList arrayList2 = this.f471a.this$0.g;
                if (list != null) {
                    arrayList2.addAll(list);
                    xw7 unused = gu7.d(this.f471a.this$0.k(), null, null, new a(this, list, arrayList, null), 3, null);
                    return;
                }
                pq7.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(bo6 bo6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bo6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, qn7);
            kVar.p$ = (iv7) obj;
            throw null;
            //return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00d1  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00f1  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 254
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.bo6.k.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bo6 f472a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bo6$l$a$a")
            /* renamed from: com.fossil.bo6$l$a$a  reason: collision with other inner class name */
            public static final class C0020a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0020a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0020a aVar = new C0020a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                    throw null;
                    //return ((C0020a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        UserRepository userRepository = this.this$0.this$0.f472a.z;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object currentUser = userRepository.getCurrentUser(this);
                        return currentUser == d ? d : currentUser;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, l lVar) {
                super(2, qn7);
                this.this$0 = lVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                ao6 ao6;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 i2 = this.this$0.f472a.i();
                    C0020a aVar = new C0020a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(i2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFUser mFUser = (MFUser) g;
                if (!(mFUser == null || (ao6 = this.this$0.f472a.d0().get()) == null)) {
                    ao6.R4(mFUser, this.this$0.f472a.r);
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public l(bo6 bo6) {
            this.f472a = bo6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bo6.H.a();
            local.d(a2, ".Inside updateUser onError errorCode=" + bVar.a());
            ao6 ao6 = this.f472a.d0().get();
            if (ao6 != null) {
                pq7.b(ao6, "view");
                if (ao6.isActive()) {
                    ao6.k();
                    ao6.o(bVar.a(), "");
                }
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bo6.H.a(), ".Inside updateUser onSuccess");
            ao6 ao6 = this.f472a.d0().get();
            if (ao6 != null) {
                pq7.b(ao6, "view");
                if (ao6.isActive()) {
                    ao6.k();
                    xw7 unused = gu7.d(this.f472a.k(), null, null, new a(null, this), 3, null);
                }
            }
        }
    }

    /*
    static {
        String simpleName = bo6.class.getSimpleName();
        pq7.b(simpleName, "HomeProfilePresenter::class.java.simpleName");
        G = simpleName;
    }
    */

    @DexIgnore
    public bo6(WeakReference<ao6> weakReference, PortfolioApp portfolioApp, vu5 vu5, xu5 xu5, DeviceRepository deviceRepository, UserRepository userRepository, SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, zu5 zu5, hu4 hu4, tt4 tt4, on5 on5) {
        pq7.c(weakReference, "mWeakRefView");
        pq7.c(portfolioApp, "mApp");
        pq7.c(vu5, "mGetUser");
        pq7.c(xu5, "mUpdateUser");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(zu5, "mDeleteLogoutUserUseCase");
        pq7.c(hu4, "socialProfileRepository");
        pq7.c(tt4, "challengeRepository");
        pq7.c(on5, "sharedPreferencesManager");
        this.u = weakReference;
        this.v = portfolioApp;
        this.w = vu5;
        this.x = xu5;
        this.y = deviceRepository;
        this.z = userRepository;
        this.A = summariesRepository;
        this.B = sleepSummariesRepository;
        this.C = zu5;
        this.D = hu4;
        this.E = tt4;
        this.F = on5;
    }

    @DexIgnore
    public final void a0(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = G;
        local.d(str2, "checkToHideWorkoutSettings, serial = " + str);
        boolean w2 = nk5.o.w(FossilDeviceSerialPatternUtil.getDeviceBySerial(str));
        ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.Q2(!w2);
        }
    }

    @DexIgnore
    public final ArrayList<b> b0() {
        return this.f;
    }

    @DexIgnore
    public final MFUser c0() {
        return this.h;
    }

    @DexIgnore
    public final WeakReference<ao6> d0() {
        return this.u;
    }

    @DexIgnore
    public final void e0() {
        this.p.removeCallbacksAndMessages(null);
        this.p.postDelayed(this.s, 60000);
    }

    @DexIgnore
    public final void f0() {
        this.w.e(null, new e(this));
    }

    @DexIgnore
    public void g0(Intent intent) {
        pq7.c(intent, "intent");
        ao6 ao6 = this.u.get();
        if (ao6 != null) {
            pq7.b(ao6, "it");
            if (!ao6.isActive()) {
                return;
            }
        }
        String stringExtra = intent.getStringExtra("SERIAL");
        if (!TextUtils.isEmpty(stringExtra) && vt7.j(stringExtra, PortfolioApp.h0.c().J(), true)) {
            ao6 ao62 = this.u.get();
            if (ao62 != null) {
                ao62.z2();
            }
            e0();
        }
    }

    @DexIgnore
    public final void h0(MFUser mFUser) {
        this.h = mFUser;
    }

    @DexIgnore
    public void i0() {
        ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.M5(this);
        }
    }

    @DexIgnore
    public final void j0(ActivityStatistic.ActivityDailyBest activityDailyBest, long j2) {
        if (j2 > (activityDailyBest != null ? (long) activityDailyBest.getValue() : 0)) {
            yn6 yn6 = new yn6(new Date(), j2);
            ao6 ao6 = this.u.get();
            if (ao6 != null) {
                ao6.J4(yn6);
            }
        }
    }

    @DexIgnore
    public final void k0(MFUser mFUser) {
        if (mFUser != null) {
            ao6 ao6 = this.u.get();
            if (ao6 != null) {
                ao6.m();
            }
            this.x.e(new xu5.a(mFUser), new l(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        PortfolioApp portfolioApp = this.v;
        g gVar = this.t;
        portfolioApp.registerReceiver(gVar, new IntentFilter(this.v.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        f0();
        p();
        xw7 unused = gu7.d(k(), null, null, new k(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        ao6 ao6 = this.u.get();
        if (ao6 != null) {
            LiveData<h47<ActivitySummary>> liveData = this.j;
            if (ao6 != null) {
                liveData.n((mz5) ao6);
                LifecycleOwner lifecycleOwner = (LifecycleOwner) ao6;
                this.l.n(lifecycleOwner);
                this.i.n(lifecycleOwner);
                this.k.n(lifecycleOwner);
            } else {
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
            }
        }
        this.p.removeCallbacksAndMessages(null);
        try {
            this.v.unregisterReceiver(this.t);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = G;
            local.d(str, "stop with " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public void n(boolean z2) {
        xw7 unused = gu7.d(k(), null, null, new c(this, z2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public FossilDeviceSerialPatternUtil.DEVICE o() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public void p() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public void q() {
        ao6 ao6 = this.u.get();
        if (ao6 != null) {
            ao6.m();
            zu5 zu5 = this.C;
            if (ao6 != null) {
                FragmentActivity activity = ((mz5) ao6).getActivity();
                if (activity != null) {
                    zu5.e(new zu5.b(1, new WeakReference(activity)), new f(this));
                    return;
                }
                throw new il7("null cannot be cast to non-null type android.app.Activity");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public void r(Intent intent) {
        Uri g2 = vk5.g(intent, PortfolioApp.h0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = G;
        StringBuilder sb = new StringBuilder();
        sb.append("Inside .onActivityResult imageUri=");
        if (g2 != null) {
            sb.append(g2);
            local.d(str, sb.toString());
            if (PortfolioApp.h0.c().w0(intent, g2)) {
                xw7 unused = gu7.d(k(), bw7.b(), null, new i(this, g2, null), 2, null);
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zn6
    public void s() {
        xw7 unused = gu7.d(k(), null, null, new j(this, null), 3, null);
    }
}
