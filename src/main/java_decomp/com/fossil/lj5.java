package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Lj5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        a = iArr;
        iArr[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        a[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        a[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        a[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        a[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
        int[] iArr2 = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
        b = iArr2;
        iArr2[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 2;
        b[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 3;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 4;
        b[FossilDeviceSerialPatternUtil.DEVICE.IVY.ordinal()] = 5;
    }
    */
}
