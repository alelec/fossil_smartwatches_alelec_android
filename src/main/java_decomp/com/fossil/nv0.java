package com.fossil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nv0 extends Rn0 {
    @DexIgnore
    public /* final */ RecyclerView d;
    @DexIgnore
    public /* final */ Ai e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Rn0 {
        @DexIgnore
        public /* final */ Nv0 d;
        @DexIgnore
        public Map<View, Rn0> e; // = new WeakHashMap();

        @DexIgnore
        public Ai(Nv0 nv0) {
            this.d = nv0;
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            Rn0 rn0 = this.e.get(view);
            return rn0 != null ? rn0.a(view, accessibilityEvent) : super.a(view, accessibilityEvent);
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public Zo0 b(View view) {
            Rn0 rn0 = this.e.get(view);
            return rn0 != null ? rn0.b(view) : super.b(view);
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void f(View view, AccessibilityEvent accessibilityEvent) {
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                rn0.f(view, accessibilityEvent);
            } else {
                super.f(view, accessibilityEvent);
            }
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void g(View view, Yo0 yo0) {
            if (this.d.o() || this.d.d.getLayoutManager() == null) {
                super.g(view, yo0);
                return;
            }
            this.d.d.getLayoutManager().P0(view, yo0);
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                rn0.g(view, yo0);
            } else {
                super.g(view, yo0);
            }
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void h(View view, AccessibilityEvent accessibilityEvent) {
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                rn0.h(view, accessibilityEvent);
            } else {
                super.h(view, accessibilityEvent);
            }
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public boolean i(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            Rn0 rn0 = this.e.get(viewGroup);
            return rn0 != null ? rn0.i(viewGroup, view, accessibilityEvent) : super.i(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public boolean j(View view, int i, Bundle bundle) {
            if (this.d.o() || this.d.d.getLayoutManager() == null) {
                return super.j(view, i, bundle);
            }
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                if (rn0.j(view, i, bundle)) {
                    return true;
                }
            } else if (super.j(view, i, bundle)) {
                return true;
            }
            return this.d.d.getLayoutManager().j1(view, i, bundle);
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void l(View view, int i) {
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                rn0.l(view, i);
            } else {
                super.l(view, i);
            }
        }

        @DexIgnore
        @Override // com.fossil.Rn0
        public void m(View view, AccessibilityEvent accessibilityEvent) {
            Rn0 rn0 = this.e.get(view);
            if (rn0 != null) {
                rn0.m(view, accessibilityEvent);
            } else {
                super.m(view, accessibilityEvent);
            }
        }

        @DexIgnore
        public Rn0 n(View view) {
            return this.e.remove(view);
        }

        @DexIgnore
        public void o(View view) {
            Rn0 k = Mo0.k(view);
            if (k != null && k != this) {
                this.e.put(view, k);
            }
        }
    }

    @DexIgnore
    public Nv0(RecyclerView recyclerView) {
        this.d = recyclerView;
        Rn0 n = n();
        if (n == null || !(n instanceof Ai)) {
            this.e = new Ai(this);
        } else {
            this.e = (Ai) n;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public void f(View view, AccessibilityEvent accessibilityEvent) {
        super.f(view, accessibilityEvent);
        if ((view instanceof RecyclerView) && !o()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().L0(accessibilityEvent);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public void g(View view, Yo0 yo0) {
        super.g(view, yo0);
        if (!o() && this.d.getLayoutManager() != null) {
            this.d.getLayoutManager().N0(yo0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rn0
    public boolean j(View view, int i, Bundle bundle) {
        if (super.j(view, i, bundle)) {
            return true;
        }
        if (o() || this.d.getLayoutManager() == null) {
            return false;
        }
        return this.d.getLayoutManager().h1(i, bundle);
    }

    @DexIgnore
    public Rn0 n() {
        return this.e;
    }

    @DexIgnore
    public boolean o() {
        return this.d.hasPendingAdapterUpdates();
    }
}
