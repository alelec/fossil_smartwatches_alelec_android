package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J74 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ A74<?> a;
        @DexIgnore
        public /* final */ Set<Bi> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Bi> c; // = new HashSet();

        @DexIgnore
        public Bi(A74<?> a74) {
            this.a = a74;
        }

        @DexIgnore
        public void a(Bi bi) {
            this.b.add(bi);
        }

        @DexIgnore
        public void b(Bi bi) {
            this.c.add(bi);
        }

        @DexIgnore
        public A74<?> c() {
            return this.a;
        }

        @DexIgnore
        public Set<Bi> d() {
            return this.b;
        }

        @DexIgnore
        public boolean e() {
            return this.b.isEmpty();
        }

        @DexIgnore
        public boolean f() {
            return this.c.isEmpty();
        }

        @DexIgnore
        public void g(Bi bi) {
            this.c.remove(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ Class<?> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ci(Class<?> cls, boolean z) {
            this.a = cls;
            this.b = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Ci)) {
                return false;
            }
            Ci ci = (Ci) obj;
            return ci.a.equals(this.a) && ci.b == this.b;
        }

        @DexIgnore
        public int hashCode() {
            return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Boolean.valueOf(this.b).hashCode();
        }
    }

    @DexIgnore
    public static void a(List<A74<?>> list) {
        Set<Bi> c = c(list);
        Set<Bi> b = b(c);
        int i = 0;
        while (!b.isEmpty()) {
            Bi next = b.iterator().next();
            b.remove(next);
            int i2 = i + 1;
            for (Bi bi : next.d()) {
                bi.g(next);
                if (bi.f()) {
                    b.add(bi);
                }
            }
            i = i2;
        }
        if (i != list.size()) {
            ArrayList arrayList = new ArrayList();
            for (Bi bi2 : c) {
                if (!bi2.f() && !bi2.e()) {
                    arrayList.add(bi2.c());
                }
            }
            throw new L74(arrayList);
        }
    }

    @DexIgnore
    public static Set<Bi> b(Set<Bi> set) {
        HashSet hashSet = new HashSet();
        for (Bi bi : set) {
            if (bi.f()) {
                hashSet.add(bi);
            }
        }
        return hashSet;
    }

    @DexIgnore
    public static Set<Bi> c(List<A74<?>> list) {
        Set<Bi> set;
        HashMap hashMap = new HashMap(list.size());
        for (A74<?> a74 : list) {
            Bi bi = new Bi(a74);
            Iterator<Class<? super Object>> it = a74.e().iterator();
            while (true) {
                if (it.hasNext()) {
                    Class<? super Object> next = it.next();
                    Ci ci = new Ci(next, !a74.k());
                    if (!hashMap.containsKey(ci)) {
                        hashMap.put(ci, new HashSet());
                    }
                    Set set2 = (Set) hashMap.get(ci);
                    if (set2.isEmpty() || ci.b) {
                        set2.add(bi);
                    } else {
                        throw new IllegalArgumentException(String.format("Multiple components provide %s.", next));
                    }
                }
            }
        }
        for (Set<Bi> set3 : hashMap.values()) {
            for (Bi bi2 : set3) {
                for (K74 k74 : bi2.c().c()) {
                    if (k74.b() && (set = (Set) hashMap.get(new Ci(k74.a(), k74.d()))) != null) {
                        for (Bi bi3 : set) {
                            bi2.a(bi3);
                            bi3.b(bi2);
                        }
                    }
                }
            }
        }
        HashSet hashSet = new HashSet();
        for (Set set4 : hashMap.values()) {
            hashSet.addAll(set4);
        }
        return hashSet;
    }
}
