package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class E32 implements J32.Bi {
    @DexIgnore
    public /* final */ J32 a;
    @DexIgnore
    public /* final */ H02 b;
    @DexIgnore
    public /* final */ C02 c;

    @DexIgnore
    public E32(J32 j32, H02 h02, C02 c02) {
        this.a = j32;
        this.b = h02;
        this.c = c02;
    }

    @DexIgnore
    public static J32.Bi a(J32 j32, H02 h02, C02 c02) {
        return new E32(j32, h02, c02);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.V(this.a, this.b, this.c, (SQLiteDatabase) obj);
    }
}
