package com.fossil;

import com.fossil.E13;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D33 implements K23 {
    @DexIgnore
    public /* final */ M23 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public D33(M23 m23, String str, Object[] objArr) {
        this.a = m23;
        this.b = str;
        this.c = objArr;
        char charAt = str.charAt(0);
        if (charAt < '\ud800') {
            this.d = charAt;
            return;
        }
        int i = charAt & '\u1fff';
        int i2 = 13;
        int i3 = 1;
        while (true) {
            char charAt2 = str.charAt(i3);
            if (charAt2 >= '\ud800') {
                i |= (charAt2 & '\u1fff') << i2;
                i2 += 13;
                i3++;
            } else {
                this.d = (charAt2 << i2) | i;
                return;
            }
        }
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object[] b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.K23
    public final int zza() {
        return (this.d & 1) == 1 ? E13.Fi.i : E13.Fi.j;
    }

    @DexIgnore
    @Override // com.fossil.K23
    public final boolean zzb() {
        return (this.d & 2) == 2;
    }

    @DexIgnore
    @Override // com.fossil.K23
    public final M23 zzc() {
        return this.a;
    }
}
