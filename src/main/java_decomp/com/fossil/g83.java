package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G83 implements Xw2<F83> {
    @DexIgnore
    public static G83 c; // = new G83();
    @DexIgnore
    public /* final */ Xw2<F83> b;

    @DexIgnore
    public G83() {
        this(Ww2.b(new I83()));
    }

    @DexIgnore
    public G83(Xw2<F83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((F83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ F83 zza() {
        return this.b.zza();
    }
}
