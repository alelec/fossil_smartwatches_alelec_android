package com.fossil;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sm2 {
    @DexIgnore
    public /* final */ ConcurrentHashMap<Vm2, List<Throwable>> a; // = new ConcurrentHashMap<>(16, 0.75f, 10);
    @DexIgnore
    public /* final */ ReferenceQueue<Throwable> b; // = new ReferenceQueue<>();

    @DexIgnore
    public final List<Throwable> a(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.b.poll();
        while (poll != null) {
            this.a.remove(poll);
            poll = this.b.poll();
        }
        List<Throwable> list = this.a.get(new Vm2(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.a.putIfAbsent(new Vm2(th, this.b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
