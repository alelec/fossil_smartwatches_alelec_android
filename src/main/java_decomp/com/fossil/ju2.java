package com.fossil;

import com.fossil.E13;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ju2 extends E13<Ju2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Ju2 zzi;
    @DexIgnore
    public static volatile Z23<Ju2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public String zzg; // = "";
    @DexIgnore
    public String zzh; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Ju2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Ju2.zzi);
        }

        @DexIgnore
        public /* synthetic */ Ai(Fu2 fu2) {
            this();
        }
    }

    @DexIgnore
    public enum Bi implements G13 {
        zza(0),
        zzb(1),
        zzc(2),
        zzd(3),
        zze(4);
        
        @DexIgnore
        public /* final */ int zzg;

        @DexIgnore
        public Bi(int i) {
            this.zzg = i;
        }

        @DexIgnore
        public static Bi zza(int i) {
            if (i == 0) {
                return zza;
            }
            if (i == 1) {
                return zzb;
            }
            if (i == 2) {
                return zzc;
            }
            if (i == 3) {
                return zzd;
            }
            if (i != 4) {
                return null;
            }
            return zze;
        }

        @DexIgnore
        public static I13 zzb() {
            return Mu2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + Bi.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.G13
        public final int zza() {
            return this.zzg;
        }
    }

    /*
    static {
        Ju2 ju2 = new Ju2();
        zzi = ju2;
        E13.u(Ju2.class, ju2);
    }
    */

    @DexIgnore
    public static Ju2 N() {
        return zzi;
    }

    @DexIgnore
    public final boolean C() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final Bi D() {
        Bi zza = Bi.zza(this.zzd);
        return zza == null ? Bi.zza : zza;
    }

    @DexIgnore
    public final boolean E() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final boolean G() {
        return this.zze;
    }

    @DexIgnore
    public final boolean H() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String I() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean J() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String K() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean L() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final String M() {
        return this.zzh;
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Ju2();
            case 2:
                return new Ai(null);
            case 3:
                I13 zzb = Bi.zzb();
                return E13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u100c\u0000\u0002\u1007\u0001\u0003\u1008\u0002\u0004\u1008\u0003\u0005\u1008\u0004", new Object[]{"zzc", "zzd", zzb, "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                Z23<Ju2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Ju2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
