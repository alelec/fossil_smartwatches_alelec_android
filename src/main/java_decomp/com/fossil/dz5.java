package com.fossil;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.n04;
import com.fossil.t47;
import com.fossil.wz5;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dz5 extends qv5 implements y66, View.OnClickListener, t47.g, aw5 {
    @DexIgnore
    public static /* final */ a E; // = new a(null);
    @DexIgnore
    public float A; // = 175.0f;
    @DexIgnore
    public long B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public HashMap D;
    @DexIgnore
    public x66 h;
    @DexIgnore
    public ConstraintLayout i;
    @DexIgnore
    public ViewPager2 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public g37<x75> l;
    @DexIgnore
    public bp5 m;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public FloatingActionButton w;
    @DexIgnore
    public int[] x; // = new int[2];
    @DexIgnore
    public float y; // = 90.0f;
    @DexIgnore
    public float z; // = 130.0f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final dz5 a(String str) {
            pq7.c(str, "navigatedPresetId");
            dz5 dz5 = new dz5();
            dz5.setArguments(nm0.a(new cl7("OUT_STATE_PRESET_ID", str)));
            return dz5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public b(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void run() {
            ConstraintLayout constraintLayout;
            x75 x75 = (x75) dz5.M6(this.b).a();
            if (x75 != null && (constraintLayout = x75.u) != null) {
                constraintLayout.setVisibility(4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dz5 f859a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(dz5 dz5) {
            this.f859a = dz5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g v;
            Drawable e;
            TabLayout tabLayout2;
            TabLayout.g v2;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.f859a.t)) {
                int parseColor = Color.parseColor(this.f859a.t);
                x75 x75 = (x75) dz5.M6(this.f859a).a();
                if (!(x75 == null || (tabLayout2 = x75.M) == null || (v2 = tabLayout2.v(i)) == null || (e2 = v2.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.f859a.s) && this.f859a.Y6() != i) {
                int parseColor2 = Color.parseColor(this.f859a.s);
                x75 x752 = (x75) dz5.M6(this.f859a).a();
                if (!(x752 == null || (tabLayout = x752.M) == null || (v = tabLayout.v(this.f859a.Y6())) == null || (e = v.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.f859a.e7(i);
            this.f859a.Z6().n(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public d(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceEditActivity.a aVar = WatchFaceEditActivity.A;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext, this.b.Z6().q());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public e(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public f(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceListActivity.a aVar = WatchFaceListActivity.C;
            Context requireContext = this.b.requireContext();
            pq7.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public g(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            this.b.Z6().v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public h(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.v) {
                this.b.K1();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;

        @DexIgnore
        public i(dz5 dz5) {
            this.b = dz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            this.b.B = System.currentTimeMillis();
            this.b.Z6().x();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements cp5 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dz5 f860a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements wz5.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ j f861a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;

            @DexIgnore
            public a(j jVar, String str) {
                this.f861a = jVar;
                this.b = str;
            }

            @DexIgnore
            @Override // com.fossil.wz5.b
            public void a(String str) {
                pq7.c(str, "presetName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    this.f861a.f860a.Z6().t(str, this.b);
                }
            }

            @DexIgnore
            @Override // com.fossil.wz5.b
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public j(dz5 dz5) {
            this.f860a = dz5;
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void N(String str, String str2) {
            pq7.c(str, "presetName");
            pq7.c(str2, "presetId");
            if (this.f860a.getChildFragmentManager().Z("RenamePresetDialogFragment") == null) {
                wz5 a2 = wz5.i.a(str, new a(this, str2));
                if (this.f860a.isActive()) {
                    a2.show(this.f860a.getChildFragmentManager(), "RenamePresetDialogFragment");
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void a() {
            this.f860a.Z6().o();
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void b(String str, boolean z, View view) {
            pq7.c(str, "presetId");
            pq7.c(view, "view");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onEditClicked id " + str + " isEditable " + z);
            if (view instanceof FloatingActionButton) {
                this.f860a.w = (FloatingActionButton) view;
                view.getLocationOnScreen(this.f860a.x);
                if (this.f860a.v) {
                    this.f860a.a7();
                    this.f860a.X6(true);
                    this.f860a.c7(true);
                    return;
                }
                this.f860a.X6(false);
                this.f860a.c7(false);
                this.f860a.g7(z);
            }
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void c() {
            T t;
            String u = this.f860a.Z6().u();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onDataRenderSuccess presetId " + u);
            if (!TextUtils.isEmpty(u)) {
                Iterator<T> it = dz5.N6(this.f860a).j().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.c(), u)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    int indexOf = dz5.N6(this.f860a).j().indexOf(t2);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "onDataRenderSuccess navigate to index " + indexOf);
                    dz5.P6(this.f860a).setCurrentItem(indexOf);
                    this.f860a.Z6().r("");
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void d(String str, String str2) {
            pq7.c(str, "presetId");
            pq7.c(str2, "position");
            WatchAppEditActivity.a aVar = WatchAppEditActivity.D;
            FragmentActivity requireActivity = this.f860a.requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            aVar.a(requireActivity, str, str2);
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void e(String str, String str2) {
            pq7.c(str, "presetId");
            this.f860a.Z6().s(str, str2);
        }

        @DexIgnore
        @Override // com.fossil.cp5
        public void f(String str) {
            pq7.c(str, "presetId");
            this.f860a.Z6().w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Animator.AnimatorListener {
        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ dz5 b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;

        @DexIgnore
        public l(dz5 dz5, View view, long j) {
            this.b = dz5;
            this.c = view;
            this.d = j;
        }

        @DexIgnore
        public void onGlobalLayout() {
            this.c.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            dz5 dz5 = this.b;
            dz5.y6(this.c, this.d, dz5.x);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements n04.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ dz5 f862a;

        @DexIgnore
        public m(dz5 dz5) {
            this.f862a = dz5;
        }

        @DexIgnore
        @Override // com.fossil.n04.b
        public final void a(TabLayout.g gVar, int i) {
            pq7.c(gVar, "tab");
            int itemCount = this.f862a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.f862a.s)) {
                int parseColor = Color.parseColor(this.f862a.s);
                if (i != 0) {
                    gVar.o(2131230966);
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor);
                    }
                } else {
                    gVar.o(2131230943);
                    Drawable e2 = gVar.e();
                    if (e2 != null) {
                        e2.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.f862a.t) && i == this.f862a.Y6()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.f862a.Y6());
                    Drawable e3 = gVar.e();
                    if (e3 != null) {
                        e3.setTint(Color.parseColor(this.f862a.t));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ g37 M6(dz5 dz5) {
        g37<x75> g37 = dz5.l;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ bp5 N6(dz5 dz5) {
        bp5 bp5 = dz5.m;
        if (bp5 != null) {
            return bp5;
        }
        pq7.n("presetAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ViewPager2 P6(dz5 dz5) {
        ViewPager2 viewPager2 = dz5.j;
        if (viewPager2 != null) {
            return viewPager2;
        }
        pq7.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void C0(String str) {
        pq7.c(str, "id");
        WatchFaceListActivity.a aVar = WatchFaceListActivity.C;
        FragmentActivity requireActivity = requireActivity();
        pq7.b(requireActivity, "requireActivity()");
        aVar.c(requireActivity, str, "CREATED_ID", false);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void E5(int i2, String str) {
        pq7.c(str, "errorMessage");
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.O(childFragmentManager, i2, str);
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void F5(List<dp5> list, Map<String, ? extends List<? extends s87>> map) {
        pq7.c(list, "data");
        pq7.c(map, "configs");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size() + " - config=" + map.size());
        bp5 bp5 = this.m;
        if (bp5 != null) {
            bp5.m(list, map);
        } else {
            pq7.n("presetAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        if (!this.v) {
            return false;
        }
        a7();
        c7(true);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void K1() {
        a7();
        X6(true);
        c7(true);
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void M4() {
        bp5 bp5;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onUpdateFwComplete");
        if (this.h != null && this.C && (bp5 = this.m) != null) {
            if (bp5 == null) {
                pq7.n("presetAdapter");
                throw null;
            } else if (bp5.j().isEmpty()) {
                x66 x66 = this.h;
                if (x66 != null) {
                    x66.l();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void O(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i2);
        bp5 bp5 = this.m;
        if (bp5 == null) {
            pq7.n("presetAdapter");
            throw null;
        } else if (bp5.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        String str2;
        pq7.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1353443012) {
            if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION") && i2 == 2131363373) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((ls5) activity).y();
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363373) {
            if (intent == null || (str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID")) == null) {
                str2 = "";
            }
            x66 x66 = this.h;
            if (x66 != null) {
                x66.p(str2);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void W3(String str) {
        pq7.c(str, "title");
        if (str.length() > 0) {
            H6(str);
        } else {
            b();
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void W4(dp5 dp5, List<? extends s87> list) {
        pq7.c(dp5, "uiDianaPreset");
        bp5 bp5 = this.m;
        if (bp5 != null) {
            bp5.n(dp5, list);
        } else {
            pq7.n("presetAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void X6(boolean z2) {
        ConstraintLayout constraintLayout;
        if (z2) {
            g37<x75> g37 = this.l;
            if (g37 != null) {
                x75 a2 = g37.a();
                ViewPropertyAnimator animate = (a2 == null || (constraintLayout = a2.u) == null) ? null : constraintLayout.animate();
                if (animate != null) {
                    ViewPropertyAnimator withEndAction = animate.alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).withEndAction(new b(this));
                    pq7.b(withEndAction, "mBinding.get()?.fabBackg\u2026BLE\n                    }");
                    withEndAction.setDuration(250);
                    x6("#80000000", "#FAF8F6", 250);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        g37<x75> g372 = this.l;
        if (g372 != null) {
            x75 a3 = g372.a();
            ConstraintLayout constraintLayout2 = a3 != null ? a3.u : null;
            if (constraintLayout2 != null) {
                pq7.b(constraintLayout2, "it");
                constraintLayout2.setVisibility(0);
                constraintLayout2.animate().alpha(1.0f).setDuration(250);
                x6("#FAF8F6", "#80000000", 250);
                return;
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final int Y6() {
        return this.k;
    }

    @DexIgnore
    public final x66 Z6() {
        x66 x66 = this.h;
        if (x66 != null) {
            return x66;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void a7() {
        g37<x75> g37 = this.l;
        if (g37 != null) {
            x75 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.G;
                pq7.b(linearLayout, "binding.llMyFaces");
                z6(linearLayout, 300, this.x);
                LinearLayout linearLayout2 = a2.F;
                pq7.b(linearLayout2, "binding.llGallery");
                z6(linearLayout2, 350, this.x);
                LinearLayout linearLayout3 = a2.E;
                pq7.b(linearLayout3, "binding.llEdit");
                z6(linearLayout3, 400, this.x);
                LinearLayout linearLayout4 = a2.I;
                pq7.b(linearLayout4, "binding.llShare");
                z6(linearLayout4, 450, this.x);
                LinearLayout linearLayout5 = a2.H;
                pq7.b(linearLayout5, "binding.llSave");
                z6(linearLayout5, 500, this.x);
                x66 x66 = this.h;
                if (x66 != null) {
                    x66.y(false);
                    this.v = false;
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z2) {
        if (z2) {
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    public final void b7(x75 x75) {
        LinearLayout linearLayout = x75.E;
        pq7.b(linearLayout, "binding.llEdit");
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        if (layoutParams != null) {
            this.y = ((ConstraintLayout.LayoutParams) layoutParams).o;
            LinearLayout linearLayout2 = x75.I;
            pq7.b(linearLayout2, "binding.llShare");
            ViewGroup.LayoutParams layoutParams2 = linearLayout2.getLayoutParams();
            if (layoutParams2 != null) {
                this.z = ((ConstraintLayout.LayoutParams) layoutParams2).o;
                LinearLayout linearLayout3 = x75.H;
                pq7.b(linearLayout3, "binding.llSave");
                ViewGroup.LayoutParams layoutParams3 = linearLayout3.getLayoutParams();
                if (layoutParams3 != null) {
                    this.A = ((ConstraintLayout.LayoutParams) layoutParams3).o;
                    this.s = qn5.l.a().d("nonBrandSwitchDisabledGray");
                    this.t = qn5.l.a().d("primaryColor");
                    this.u = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
                    j jVar = new j(this);
                    Context requireContext = requireContext();
                    pq7.b(requireContext, "requireContext()");
                    this.m = new bp5(requireContext, jVar);
                    ConstraintLayout constraintLayout = x75.r;
                    pq7.b(constraintLayout, "binding.clNoDevice");
                    this.i = constraintLayout;
                    x75.D.setImageResource(2131230945);
                    x75.C.setOnClickListener(this);
                    ViewPager2 viewPager2 = x75.L;
                    pq7.b(viewPager2, "binding.rvPreset");
                    this.j = viewPager2;
                    if (viewPager2 != null) {
                        if (viewPager2.getChildAt(0) != null) {
                            ViewPager2 viewPager22 = this.j;
                            if (viewPager22 != null) {
                                View childAt = viewPager22.getChildAt(0);
                                if (childAt != null) {
                                    ((RecyclerView) childAt).setOverScrollMode(2);
                                } else {
                                    throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                                }
                            } else {
                                pq7.n("rvCustomize");
                                throw null;
                            }
                        }
                        ViewPager2 viewPager23 = this.j;
                        if (viewPager23 != null) {
                            bp5 bp5 = this.m;
                            if (bp5 != null) {
                                viewPager23.setAdapter(bp5);
                                if (!TextUtils.isEmpty(this.u)) {
                                    TabLayout tabLayout = x75.M;
                                    pq7.b(tabLayout, "binding.tab");
                                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                                }
                                ViewPager2 viewPager24 = this.j;
                                if (viewPager24 != null) {
                                    viewPager24.g(new c(this));
                                    ViewPager2 viewPager25 = this.j;
                                    if (viewPager25 != null) {
                                        viewPager25.setCurrentItem(this.k);
                                        x75.v.setOnClickListener(new d(this));
                                        x75.w.setOnClickListener(new e(this));
                                        x75.x.setOnClickListener(new f(this));
                                        x75.y.setOnClickListener(new g(this));
                                        x75.u.setOnClickListener(new h(this));
                                        x75.z.setOnClickListener(new i(this));
                                        return;
                                    }
                                    pq7.n("rvCustomize");
                                    throw null;
                                }
                                pq7.n("rvCustomize");
                                throw null;
                            }
                            pq7.n("presetAdapter");
                            throw null;
                        }
                        pq7.n("rvCustomize");
                        throw null;
                    }
                    pq7.n("rvCustomize");
                    throw null;
                }
                throw new il7("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            }
            throw new il7("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
        }
        throw new il7("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void c0(int i2) {
        if (isActive()) {
            bp5 bp5 = this.m;
            if (bp5 == null) {
                pq7.n("presetAdapter");
                throw null;
            } else if (bp5.getItemCount() > i2) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void c7(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "rotateFab, isCurrentlyExpand " + z2);
        FloatingActionButton floatingActionButton = this.w;
        if (floatingActionButton != null) {
            ViewPropertyAnimator animate = floatingActionButton.animate();
            if (animate != null) {
                animate.setDuration(250).setListener(new k()).rotation(!z2 ? 180.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void d7(View view, float f2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            layoutParams2.o = f2;
            view.setLayoutParams(layoutParams2);
            return;
        }
        throw new il7("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
    }

    @DexIgnore
    public final void e7(int i2) {
        this.k = i2;
    }

    @DexIgnore
    /* renamed from: f7 */
    public void M5(x66 x66) {
        String str;
        pq7.c(x66, "presenter");
        this.h = x66;
        if (x66 != null) {
            Bundle arguments = getArguments();
            if (arguments == null || (str = arguments.getString("OUT_STATE_PRESET_ID", "")) == null) {
                str = "";
            }
            x66.r(str);
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void g2(String str, int i2) {
        if (str != null) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", str);
            startActivity(Intent.createChooser(intent, getString(2131887333)));
            b77.f401a.h("wf_sharing_session", this.B, System.currentTimeMillis());
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDianaCustomizeFragment", "onWFSharing error " + i2);
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, null, childFragmentManager);
    }

    @DexIgnore
    public final void g7(boolean z2) {
        g37<x75> g37 = this.l;
        if (g37 != null) {
            x75 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.G;
                pq7.b(linearLayout, "binding.llMyFaces");
                h7(linearLayout, 300);
                LinearLayout linearLayout2 = a2.F;
                pq7.b(linearLayout2, "binding.llGallery");
                h7(linearLayout2, 350);
                if (z2) {
                    LinearLayout linearLayout3 = a2.I;
                    pq7.b(linearLayout3, "binding.llShare");
                    d7(linearLayout3, this.z);
                    LinearLayout linearLayout4 = a2.H;
                    pq7.b(linearLayout4, "binding.llSave");
                    d7(linearLayout4, this.A);
                    FlexibleTextView flexibleTextView = a2.O;
                    pq7.b(flexibleTextView, "binding.tvEdit");
                    flexibleTextView.setVisibility(0);
                    FlexibleTextView flexibleTextView2 = a2.O;
                    pq7.b(flexibleTextView2, "binding.tvEdit");
                    flexibleTextView2.setAlpha(1.0f);
                    CustomizeWidget customizeWidget = a2.v;
                    pq7.b(customizeWidget, "binding.fabEdit");
                    customizeWidget.setVisibility(0);
                    CustomizeWidget customizeWidget2 = a2.v;
                    pq7.b(customizeWidget2, "binding.fabEdit");
                    customizeWidget2.setAlpha(1.0f);
                    CustomizeWidget customizeWidget3 = a2.v;
                    pq7.b(customizeWidget3, "binding.fabEdit");
                    customizeWidget3.setEnabled(true);
                    CustomizeWidget customizeWidget4 = a2.v;
                    pq7.b(customizeWidget4, "binding.fabEdit");
                    customizeWidget4.setClickable(true);
                    LinearLayout linearLayout5 = a2.E;
                    pq7.b(linearLayout5, "binding.llEdit");
                    h7(linearLayout5, 400);
                    FlexibleTextView flexibleTextView3 = a2.R;
                    pq7.b(flexibleTextView3, "binding.tvSave");
                    flexibleTextView3.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = a2.R;
                    pq7.b(flexibleTextView4, "binding.tvSave");
                    flexibleTextView4.setAlpha(1.0f);
                    CustomizeWidget customizeWidget5 = a2.y;
                    pq7.b(customizeWidget5, "binding.fabSave");
                    customizeWidget5.setVisibility(0);
                    CustomizeWidget customizeWidget6 = a2.y;
                    pq7.b(customizeWidget6, "binding.fabSave");
                    customizeWidget6.setAlpha(1.0f);
                    CustomizeWidget customizeWidget7 = a2.y;
                    pq7.b(customizeWidget7, "binding.fabSave");
                    customizeWidget7.setEnabled(true);
                    CustomizeWidget customizeWidget8 = a2.y;
                    pq7.b(customizeWidget8, "binding.fabSave");
                    customizeWidget8.setClickable(true);
                    LinearLayout linearLayout6 = a2.H;
                    pq7.b(linearLayout6, "binding.llSave");
                    h7(linearLayout6, 400);
                } else {
                    LinearLayout linearLayout7 = a2.I;
                    pq7.b(linearLayout7, "binding.llShare");
                    d7(linearLayout7, this.y);
                    LinearLayout linearLayout8 = a2.H;
                    pq7.b(linearLayout8, "binding.llSave");
                    d7(linearLayout8, this.z);
                    CustomizeWidget customizeWidget9 = a2.v;
                    pq7.b(customizeWidget9, "binding.fabEdit");
                    customizeWidget9.setVisibility(4);
                    FlexibleTextView flexibleTextView5 = a2.O;
                    pq7.b(flexibleTextView5, "binding.tvEdit");
                    flexibleTextView5.setVisibility(4);
                    CustomizeWidget customizeWidget10 = a2.y;
                    pq7.b(customizeWidget10, "binding.fabSave");
                    customizeWidget10.setVisibility(4);
                    FlexibleTextView flexibleTextView6 = a2.R;
                    pq7.b(flexibleTextView6, "binding.tvSave");
                    flexibleTextView6.setVisibility(4);
                }
                LinearLayout linearLayout9 = a2.I;
                pq7.b(linearLayout9, "binding.llShare");
                h7(linearLayout9, 450);
                LinearLayout linearLayout10 = a2.H;
                pq7.b(linearLayout10, "binding.llSave");
                h7(linearLayout10, 500);
                x66 x66 = this.h;
                if (x66 != null) {
                    x66.y(true);
                    this.v = true;
                    return;
                }
                pq7.n("mPresenter");
                throw null;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public int getItemCount() {
        ViewPager2 viewPager2 = this.j;
        if (viewPager2 == null) {
            return 0;
        }
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        pq7.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void h1() {
        if (isActive()) {
            Toast.makeText(requireContext(), "Can't delete only preset", 1).show();
        }
    }

    @DexIgnore
    public final void h7(View view, long j2) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new l(this, view, j2));
    }

    @DexIgnore
    public final void i7() {
        g37<x75> g37 = this.l;
        if (g37 != null) {
            x75 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.M : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    new n04(tabLayout, viewPager2, new m(this)).a();
                } else {
                    pq7.n("rvCustomize");
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void m0(int i2) {
        bp5 bp5 = this.m;
        if (bp5 == null) {
            pq7.n("presetAdapter");
            throw null;
        } else if (bp5.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                pq7.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        pq7.c(view, "v");
        if (view.getId() == 2131362501 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            pq7.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onCreateView");
        x75 x75 = (x75) aq0.f(layoutInflater, 2131558575, viewGroup, false, A6());
        pq7.b(x75, "binding");
        b7(x75);
        g37<x75> g37 = new g37<>(this, x75);
        this.l = g37;
        if (g37 != null) {
            x75 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        x66 x66 = this.h;
        if (x66 != null) {
            if (x66 != null) {
                x66.m();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        x66 x66 = this.h;
        if (x66 != null) {
            if (x66 != null) {
                x66.l();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
        vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onViewCreated");
        i7();
        E6("customize_view");
        this.C = true;
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void r(boolean z2) {
        if (z2) {
            ConstraintLayout constraintLayout = this.i;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                pq7.n("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.i;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                pq7.n("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void r0(boolean z2, String str, String str2, String str3) {
        pq7.c(str, "currentPresetName");
        pq7.c(str2, "nextPresetName");
        pq7.c(str3, "nextPresetId");
        isActive();
        String string = requireActivity().getString(2131886547);
        pq7.b(string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
        if (z2) {
            String string2 = requireActivity().getString(2131886548);
            pq7.b(string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            hr7 hr7 = hr7.f1520a;
            string = String.format(string2, Arrays.copyOf(new Object[]{vt7.g(str2)}, 1));
            pq7.b(string, "java.lang.String.format(format, *args)");
        }
        Bundle bundle = new Bundle();
        bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
        t47.f fVar = new t47.f(2131558484);
        hr7 hr72 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886549);
        pq7.b(c2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{str}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        fVar.e(2131363410, format);
        fVar.e(2131363317, string);
        fVar.e(2131363373, um5.c(PortfolioApp.h0.c(), 2131886546));
        fVar.e(2131363291, um5.c(PortfolioApp.h0.c(), 2131886545));
        fVar.b(2131363373);
        fVar.b(2131363291);
        fVar.m(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            pq7.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.h0.c().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.D;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.y66
    public void w() {
        a();
    }
}
