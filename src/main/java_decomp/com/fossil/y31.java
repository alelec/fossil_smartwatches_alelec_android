package com.fossil;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y31 {
    @DexIgnore
    public static /* final */ String a; // = X01.f("PackageManagerHelper");

    @DexIgnore
    public static void a(Context context, Class<?> cls, boolean z) {
        String str = "enabled";
        try {
            context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, cls.getName()), z ? 1 : 2, 1);
            X01.c().a(a, String.format("%s %s", cls.getName(), z ? "enabled" : "disabled"), new Throwable[0]);
        } catch (Exception e) {
            X01 c = X01.c();
            String str2 = a;
            String name = cls.getName();
            if (!z) {
                str = "disabled";
            }
            c.a(str2, String.format("%s could not be %s", name, str), e);
        }
    }
}
