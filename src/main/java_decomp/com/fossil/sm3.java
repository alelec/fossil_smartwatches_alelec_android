package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sm3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 c;

    @DexIgnore
    public Sm3(Qm3 qm3, Or3 or3) {
        this.c = qm3;
        this.b = or3;
    }

    @DexIgnore
    public final void run() {
        this.c.b.d0();
        Yq3 yq3 = this.c.b;
        Or3 or3 = this.b;
        yq3.c().h();
        yq3.b0();
        Rc2.g(or3.b);
        yq3.P(or3);
    }
}
