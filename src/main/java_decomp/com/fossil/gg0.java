package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gg0 implements Lg0, Ig0, AdapterView.OnItemClickListener {
    @DexIgnore
    public Rect b;

    @DexIgnore
    public static int p(ListAdapter listAdapter, ViewGroup viewGroup, Context context, int i) {
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        ViewGroup viewGroup2 = viewGroup;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        View view = null;
        while (i4 < count) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i2) {
                i2 = itemViewType;
                view = null;
            }
            if (viewGroup2 == null) {
                viewGroup2 = new FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, viewGroup2);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth <= i3) {
                measuredWidth = i3;
            }
            i4++;
            i3 = measuredWidth;
        }
        return i3;
    }

    @DexIgnore
    public static boolean y(Cg0 cg0) {
        int size = cg0.size();
        for (int i = 0; i < size; i++) {
            MenuItem item = cg0.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static Bg0 z(ListAdapter listAdapter) {
        return listAdapter instanceof HeaderViewListAdapter ? (Bg0) ((HeaderViewListAdapter) listAdapter).getWrappedAdapter() : (Bg0) listAdapter;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean e(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean f(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public int getId() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
    }

    @DexIgnore
    public abstract void m(Cg0 cg0);

    @DexIgnore
    public boolean n() {
        return true;
    }

    @DexIgnore
    public Rect o() {
        return this.b;
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ListAdapter listAdapter = (ListAdapter) adapterView.getAdapter();
        z(listAdapter).b.O((MenuItem) listAdapter.getItem(i), this, n() ? 0 : 4);
    }

    @DexIgnore
    public abstract void q(View view);

    @DexIgnore
    public void r(Rect rect) {
        this.b = rect;
    }

    @DexIgnore
    public abstract void s(boolean z);

    @DexIgnore
    public abstract void t(int i);

    @DexIgnore
    public abstract void u(int i);

    @DexIgnore
    public abstract void v(PopupWindow.OnDismissListener onDismissListener);

    @DexIgnore
    public abstract void w(boolean z);

    @DexIgnore
    public abstract void x(int i);
}
