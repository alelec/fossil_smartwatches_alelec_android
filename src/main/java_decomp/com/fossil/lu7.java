package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lk6;
import com.mapped.Rm6;
import com.mapped.Xe6;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lu7<T> extends Yv7<T> implements Lk6<T>, Do7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater g; // = AtomicIntegerFieldUpdater.newUpdater(Lu7.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater h; // = AtomicReferenceFieldUpdater.newUpdater(Lu7.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _parentHandle; // = null;
    @DexIgnore
    public volatile Object _state; // = Bu7.b;
    @DexIgnore
    public /* final */ Af6 e;
    @DexIgnore
    public /* final */ Xe6<T> f;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Xe6<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Lu7(Xe6<? super T> xe6, int i) {
        super(i);
        this.f = xe6;
        this.e = xe6.getContext();
    }

    @DexIgnore
    public final void A(Throwable th) {
        if (!m(th)) {
            l(th);
            p();
        }
    }

    @DexIgnore
    public final boolean B() {
        if (Nv7.a()) {
            if (!(s() != Lx7.b)) {
                throw new AssertionError();
            }
        }
        Object obj = this._state;
        if (Nv7.a() && !(!(obj instanceof Mx7))) {
            throw new AssertionError();
        } else if (obj instanceof Xu7) {
            o();
            return false;
        } else {
            this._decision = 0;
            this._state = Bu7.b;
            return true;
        }
    }

    @DexIgnore
    public final Ou7 C(Object obj, int i) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof Mx7)) {
                if (obj2 instanceof Ou7) {
                    Ou7 ou7 = (Ou7) obj2;
                    if (ou7.c()) {
                        return ou7;
                    }
                }
                k(obj);
                throw null;
            }
        } while (!h.compareAndSet(this, obj2, obj));
        p();
        q(i);
        return null;
    }

    @DexIgnore
    public final void D(Dw7 dw7) {
        this._parentHandle = dw7;
    }

    @DexIgnore
    public final void E() {
        Rm6 rm6;
        if (!n() && s() == null && (rm6 = (Rm6) this.f.getContext().get(Rm6.r)) != null) {
            rm6.start();
            Dw7 d = Rm6.Ai.d(rm6, true, false, new Pu7(rm6, this), 2, null);
            D(d);
            if (v() && !w()) {
                d.dispose();
                D(Lx7.b);
            }
        }
    }

    @DexIgnore
    public final boolean F() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!g.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean G() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!g.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Yv7
    public void a(Object obj, Throwable th) {
        if (obj instanceof Yu7) {
            try {
                ((Yu7) obj).b.invoke(th);
            } catch (Throwable th2) {
                Af6 context = getContext();
                Fv7.a(context, new Av7("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Lk6
    public Object b(T t, Object obj) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof Mx7)) {
                if (obj2 instanceof Xu7) {
                    Xu7 xu7 = (Xu7) obj2;
                    if (xu7.a == obj) {
                        if (Nv7.a()) {
                            if (!(xu7.b == t)) {
                                throw new AssertionError();
                            }
                        }
                        return Mu7.a;
                    }
                }
                return null;
            }
        } while (!h.compareAndSet(this, obj2, obj == null ? t : new Xu7(obj, t)));
        p();
        return Mu7.a;
    }

    @DexIgnore
    @Override // com.fossil.Yv7
    public final Xe6<T> c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.Lk6
    public void e(Hg6<? super Throwable, Cd6> hg6) {
        Object obj;
        Throwable th = null;
        Iu7 iu7 = null;
        do {
            obj = this._state;
            if (obj instanceof Bu7) {
                if (iu7 == null) {
                    iu7 = x(hg6);
                }
            } else if (obj instanceof Iu7) {
                y(hg6, obj);
                throw null;
            } else if (!(obj instanceof Ou7)) {
                return;
            } else {
                if (((Ou7) obj).b()) {
                    try {
                        Vu7 vu7 = (Vu7) (!(obj instanceof Vu7) ? null : obj);
                        if (vu7 != null) {
                            th = vu7.a;
                        }
                        hg6.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        Fv7.a(getContext(), new Av7("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    y(hg6, obj);
                    throw null;
                }
            }
        } while (!h.compareAndSet(this, obj, iu7));
    }

    @DexIgnore
    @Override // com.mapped.Lk6
    public void f(Dv7 dv7, T t) {
        Dv7 dv72 = null;
        Xe6<T> xe6 = this.f;
        if (!(xe6 instanceof Vv7)) {
            xe6 = null;
        }
        Vv7 vv7 = (Vv7) xe6;
        if (vv7 != null) {
            dv72 = vv7.h;
        }
        C(t, dv72 == dv7 ? 2 : this.d);
    }

    @DexIgnore
    @Override // com.mapped.Lk6
    public void g(Object obj) {
        if (Nv7.a()) {
            if (!(obj == Mu7.a)) {
                throw new AssertionError();
            }
        }
        q(this.d);
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public Do7 getCallerFrame() {
        Xe6<T> xe6 = this.f;
        if (!(xe6 instanceof Do7)) {
            xe6 = null;
        }
        return (Do7) xe6;
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public Af6 getContext() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Yv7
    public <T> T h(Object obj) {
        return obj instanceof Xu7 ? (T) ((Xu7) obj).b : obj instanceof Yu7 ? (T) ((Yu7) obj).a : obj;
    }

    @DexIgnore
    @Override // com.mapped.Lk6
    public boolean isActive() {
        return u() instanceof Mx7;
    }

    @DexIgnore
    @Override // com.fossil.Yv7
    public Object j() {
        return u();
    }

    @DexIgnore
    public final void k(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public boolean l(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof Mx7)) {
                return false;
            }
            z = obj instanceof Iu7;
        } while (!h.compareAndSet(this, obj, new Ou7(this, th, z)));
        if (z) {
            try {
                ((Iu7) obj).a(th);
            } catch (Throwable th2) {
                Af6 context = getContext();
                Fv7.a(context, new Av7("Exception in cancellation handler for " + this, th2));
            }
        }
        p();
        q(0);
        return true;
    }

    @DexIgnore
    public final boolean m(Throwable th) {
        if (this.d != 0) {
            return false;
        }
        Xe6<T> xe6 = this.f;
        if (!(xe6 instanceof Vv7)) {
            xe6 = null;
        }
        Vv7 vv7 = (Vv7) xe6;
        if (vv7 != null) {
            return vv7.p(th);
        }
        return false;
    }

    @DexIgnore
    public final boolean n() {
        boolean z;
        Throwable k;
        boolean v = v();
        if (this.d != 0) {
            return v;
        }
        Xe6<T> xe6 = this.f;
        if (!(xe6 instanceof Vv7)) {
            xe6 = null;
        }
        Vv7 vv7 = (Vv7) xe6;
        if (vv7 == null || (k = vv7.k(this)) == null) {
            z = v;
        } else {
            if (!v) {
                l(k);
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final void o() {
        Dw7 s = s();
        if (s != null) {
            s.dispose();
        }
        D(Lx7.b);
    }

    @DexIgnore
    public final void p() {
        if (!w()) {
            o();
        }
    }

    @DexIgnore
    public final void q(int i) {
        if (!F()) {
            Zv7.a(this, i);
        }
    }

    @DexIgnore
    public Throwable r(Rm6 rm6) {
        return rm6.k();
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public void resumeWith(Object obj) {
        C(Wu7.c(obj, this), this.d);
    }

    @DexIgnore
    public final Dw7 s() {
        return (Dw7) this._parentHandle;
    }

    @DexIgnore
    public final Object t() {
        Rm6 rm6;
        E();
        if (G()) {
            return Yn7.d();
        }
        Object u = u();
        if (u instanceof Vu7) {
            Throwable th = ((Vu7) u).a;
            if (Nv7.d()) {
                throw Uz7.a(th, this);
            }
            throw th;
        } else if (this.d != 1 || (rm6 = (Rm6) getContext().get(Rm6.r)) == null || rm6.isActive()) {
            return h(u);
        } else {
            CancellationException k = rm6.k();
            a(u, k);
            if (Nv7.d()) {
                throw Uz7.a(k, this);
            }
            throw k;
        }
    }

    @DexIgnore
    public String toString() {
        return z() + '(' + Ov7.c(this.f) + "){" + u() + "}@" + Ov7.b(this);
    }

    @DexIgnore
    public final Object u() {
        return this._state;
    }

    @DexIgnore
    public boolean v() {
        return !(u() instanceof Mx7);
    }

    @DexIgnore
    public final boolean w() {
        Xe6<T> xe6 = this.f;
        return (xe6 instanceof Vv7) && ((Vv7) xe6).o();
    }

    @DexIgnore
    public final Iu7 x(Hg6<? super Throwable, Cd6> hg6) {
        return hg6 instanceof Iu7 ? (Iu7) hg6 : new Uw7(hg6);
    }

    @DexIgnore
    public final void y(Hg6<? super Throwable, Cd6> hg6, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + hg6 + ", already has " + obj).toString());
    }

    @DexIgnore
    public String z() {
        return "CancellableContinuation";
    }
}
