package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xv1 extends Sv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Xv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xv1 createFromParcel(Parcel parcel) {
            return new Xv1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Xv1[] newArray(int i) {
            return new Xv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Xv1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Xv1(Jv1 jv1, Kv1 kv1) {
        super(Vv1.HEART_RATE, jv1, kv1, false, null, null, 56);
    }

    @DexIgnore
    public Xv1(JSONObject jSONObject, Cc0[] cc0Arr) {
        super(jSONObject, cc0Arr, null, 4);
    }

    @DexIgnore
    @Override // com.fossil.Sv1, java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public Xv1 clone() {
        return new Xv1(b().clone(), c().clone());
    }

    @DexIgnore
    @Override // com.fossil.Sv1
    public Xv1 setBackgroundImage(Tv1 tv1) {
        return (Xv1) super.setBackgroundImage(tv1);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledHeight(float f) {
        Mv1 scaledHeight = super.setScaledHeight(f);
        if (scaledHeight != null) {
            return (Xv1) scaledHeight;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledPosition(Jv1 jv1) {
        Mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (Xv1) scaledPosition;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledSize(Kv1 kv1) {
        Mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (Xv1) scaledSize;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledWidth(float f) {
        Mv1 scaledWidth = super.setScaledWidth(f);
        if (scaledWidth != null) {
            return (Xv1) scaledWidth;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledX(float f) {
        Mv1 scaledX = super.setScaledX(f);
        if (scaledX != null) {
            return (Xv1) scaledX;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Xv1 setScaledY(float f) {
        Mv1 scaledY = super.setScaledY(f);
        if (scaledY != null) {
            return (Xv1) scaledY;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Sv1
    public Xv1 setTheme(Uv1 uv1) {
        Sv1 theme = super.setTheme(uv1);
        if (theme != null) {
            return (Xv1) theme;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.HeartRateComplicationElement");
    }
}
