package com.fossil;

import android.view.View;
import io.flutter.embedding.engine.systemchannels.PlatformViewsChannel;
import io.flutter.plugin.platform.PlatformViewsController;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Ok7 implements View.OnFocusChangeListener {
    @DexIgnore
    public /* final */ /* synthetic */ PlatformViewsController.Anon1 b;
    @DexIgnore
    public /* final */ /* synthetic */ PlatformViewsChannel.PlatformViewCreationRequest c;

    @DexIgnore
    public /* synthetic */ Ok7(PlatformViewsController.Anon1 anon1, PlatformViewsChannel.PlatformViewCreationRequest platformViewCreationRequest) {
        this.b = anon1;
        this.c = platformViewCreationRequest;
    }

    @DexIgnore
    public final void onFocusChange(View view, boolean z) {
        this.b.a(this.c, view, z);
    }
}
