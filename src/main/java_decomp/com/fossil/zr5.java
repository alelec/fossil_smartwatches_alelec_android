package com.fossil;

import com.mapped.HybridMessageNotificationComponent;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zr5 implements Factory<HybridMessageNotificationComponent> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Zr5 a; // = new Zr5();
    }

    @DexIgnore
    public static Zr5 a() {
        return Ai.a;
    }

    @DexIgnore
    public static HybridMessageNotificationComponent c() {
        return new HybridMessageNotificationComponent();
    }

    @DexIgnore
    public HybridMessageNotificationComponent b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
