package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ct0 {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    public static Ct0 g;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ HashMap<BroadcastReceiver, ArrayList<Ci>> b; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, ArrayList<Ci>> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ ArrayList<Bi> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Handler e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Handler {
        @DexIgnore
        public Ai(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            if (message.what != 1) {
                super.handleMessage(message);
            } else {
                Ct0.this.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ ArrayList<Ci> b;

        @DexIgnore
        public Bi(Intent intent, ArrayList<Ci> arrayList) {
            this.a = intent;
            this.b = arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ IntentFilter a;
        @DexIgnore
        public /* final */ BroadcastReceiver b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public Ci(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.a = intentFilter;
            this.b = broadcastReceiver;
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.b);
            sb.append(" filter=");
            sb.append(this.a);
            if (this.d) {
                sb.append(" DEAD");
            }
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore
    public Ct0(Context context) {
        this.a = context;
        this.e = new Ai(context.getMainLooper());
    }

    @DexIgnore
    public static Ct0 b(Context context) {
        Ct0 ct0;
        synchronized (f) {
            if (g == null) {
                g = new Ct0(context.getApplicationContext());
            }
            ct0 = g;
        }
        return ct0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r1 >= r4) goto L_0x0001;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r6 = r5[r1];
        r7 = r6.b.size();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
        if (r3 >= r7) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        r0 = r6.b.get(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        if (r0.d != false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        r0.b.onReceive(r10.a, r6.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003e, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        r1 = r1 + 1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r10 = this;
            r2 = 0
        L_0x0001:
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<com.fossil.Ct0$Ci>> r1 = r10.b
            monitor-enter(r1)
            java.util.ArrayList<com.fossil.Ct0$Bi> r0 = r10.d     // Catch:{ all -> 0x0046 }
            int r4 = r0.size()     // Catch:{ all -> 0x0046 }
            if (r4 > 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            return
        L_0x000e:
            com.fossil.Ct0$Bi[] r5 = new com.fossil.Ct0.Bi[r4]     // Catch:{ all -> 0x0046 }
            java.util.ArrayList<com.fossil.Ct0$Bi> r0 = r10.d     // Catch:{ all -> 0x0046 }
            r0.toArray(r5)     // Catch:{ all -> 0x0046 }
            java.util.ArrayList<com.fossil.Ct0$Bi> r0 = r10.d     // Catch:{ all -> 0x0046 }
            r0.clear()     // Catch:{ all -> 0x0046 }
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            r1 = r2
        L_0x001c:
            if (r1 >= r4) goto L_0x0001
            r6 = r5[r1]
            java.util.ArrayList<com.fossil.Ct0$Ci> r0 = r6.b
            int r7 = r0.size()
            r3 = r2
        L_0x0027:
            if (r3 >= r7) goto L_0x0042
            java.util.ArrayList<com.fossil.Ct0$Ci> r0 = r6.b
            java.lang.Object r0 = r0.get(r3)
            com.fossil.Ct0$Ci r0 = (com.fossil.Ct0.Ci) r0
            boolean r8 = r0.d
            if (r8 != 0) goto L_0x003e
            android.content.BroadcastReceiver r0 = r0.b
            android.content.Context r8 = r10.a
            android.content.Intent r9 = r6.a
            r0.onReceive(r8, r9)
        L_0x003e:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0027
        L_0x0042:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001c
        L_0x0046:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ct0.a():void");
    }

    @DexIgnore
    public void c(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.b) {
            Ci ci = new Ci(intentFilter, broadcastReceiver);
            ArrayList<Ci> arrayList = this.b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList<>(1);
                this.b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(ci);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList<Ci> arrayList2 = this.c.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList<>(1);
                    this.c.put(action, arrayList2);
                }
                arrayList2.add(ci);
            }
        }
    }

    @DexIgnore
    public boolean d(Intent intent) {
        ArrayList arrayList;
        synchronized (this.b) {
            String action = intent.getAction();
            String resolveTypeIfNeeded = intent.resolveTypeIfNeeded(this.a.getContentResolver());
            Uri data = intent.getData();
            String scheme = intent.getScheme();
            Set<String> categories = intent.getCategories();
            boolean z = (intent.getFlags() & 8) != 0;
            if (z) {
                Log.v("LocalBroadcastManager", "Resolving type " + resolveTypeIfNeeded + " scheme " + scheme + " of intent " + intent);
            }
            ArrayList<Ci> arrayList2 = this.c.get(intent.getAction());
            if (arrayList2 != null) {
                if (z) {
                    Log.v("LocalBroadcastManager", "Action list: " + arrayList2);
                }
                ArrayList arrayList3 = null;
                int i = 0;
                while (i < arrayList2.size()) {
                    Ci ci = arrayList2.get(i);
                    if (z) {
                        Log.v("LocalBroadcastManager", "Matching against filter " + ci.a);
                    }
                    if (ci.c) {
                        if (z) {
                            Log.v("LocalBroadcastManager", "  Filter's target already added");
                        }
                        arrayList = arrayList3;
                    } else {
                        int match = ci.a.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                        if (match >= 0) {
                            if (z) {
                                Log.v("LocalBroadcastManager", "  Filter matched!  match=0x" + Integer.toHexString(match));
                            }
                            arrayList = arrayList3 == null ? new ArrayList() : arrayList3;
                            arrayList.add(ci);
                            ci.c = true;
                        } else {
                            if (z) {
                                Log.v("LocalBroadcastManager", "  Filter did not match: " + (match != -4 ? match != -3 ? match != -2 ? match != -1 ? "unknown reason" : "type" : "data" : "action" : "category"));
                            }
                            arrayList = arrayList3;
                        }
                    }
                    i++;
                    arrayList3 = arrayList;
                }
                if (arrayList3 != null) {
                    for (int i2 = 0; i2 < arrayList3.size(); i2++) {
                        ((Ci) arrayList3.get(i2)).c = false;
                    }
                    this.d.add(new Bi(intent, arrayList3));
                    if (!this.e.hasMessages(1)) {
                        this.e.sendEmptyMessage(1);
                    }
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void e(BroadcastReceiver broadcastReceiver) {
        synchronized (this.b) {
            ArrayList<Ci> remove = this.b.remove(broadcastReceiver);
            if (remove != null) {
                for (int size = remove.size() - 1; size >= 0; size--) {
                    Ci ci = remove.get(size);
                    ci.d = true;
                    for (int i = 0; i < ci.a.countActions(); i++) {
                        String action = ci.a.getAction(i);
                        ArrayList<Ci> arrayList = this.c.get(action);
                        if (arrayList != null) {
                            for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                                Ci ci2 = arrayList.get(size2);
                                if (ci2.b == broadcastReceiver) {
                                    ci2.d = true;
                                    arrayList.remove(size2);
                                }
                            }
                            if (arrayList.size() <= 0) {
                                this.c.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }
}
