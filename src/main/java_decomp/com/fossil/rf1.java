package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rf1<T> implements Qb1<ImageDecoder.Source, T> {
    @DexIgnore
    public /* final */ Lg1 a; // = Lg1.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ImageDecoder.OnHeaderDecodedListener {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ Hb1 d;
        @DexIgnore
        public /* final */ /* synthetic */ Fg1 e;
        @DexIgnore
        public /* final */ /* synthetic */ Pb1 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements ImageDecoder.OnPartialImageListener {
            @DexIgnore
            public Aii(Ai ai) {
            }

            @DexIgnore
            public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        @DexIgnore
        public Ai(int i, int i2, boolean z, Hb1 hb1, Fg1 fg1, Pb1 pb1) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.d = hb1;
            this.e = fg1;
            this.f = pb1;
        }

        @DexIgnore
        @SuppressLint({"Override"})
        public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
            if (Rf1.this.a.c(this.a, this.b, this.c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.d == Hb1.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new Aii(this));
            Size size = imageInfo.getSize();
            int i = this.a;
            if (i == Integer.MIN_VALUE) {
                i = size.getWidth();
            }
            int i2 = this.b;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getHeight();
            }
            float b2 = this.e.b(size.getWidth(), size.getHeight(), i, i2);
            int round = Math.round(((float) size.getWidth()) * b2);
            int round2 = Math.round(((float) size.getHeight()) * b2);
            if (Log.isLoggable("ImageDecoder", 2)) {
                Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b2);
            }
            imageDecoder.setTargetSize(round, round2);
            int i3 = Build.VERSION.SDK_INT;
            if (i3 >= 28) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(this.f == Pb1.DISPLAY_P3 && imageInfo.getColorSpace() != null && imageInfo.getColorSpace().isWideGamut() ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
            } else if (i3 >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(ImageDecoder.Source source, Ob1 ob1) throws IOException {
        return e(source, ob1);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1 b(ImageDecoder.Source source, int i, int i2, Ob1 ob1) throws IOException {
        return d(source, i, i2, ob1);
    }

    @DexIgnore
    public abstract Id1<T> c(ImageDecoder.Source source, int i, int i2, ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws IOException;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: com.fossil.Ob1 */
    /* JADX WARN: Multi-variable type inference failed */
    public final Id1<T> d(ImageDecoder.Source source, int i, int i2, Ob1 ob1) throws IOException {
        return c(source, i, i2, new Ai(i, i2, ob1.c(Gg1.i) != null && ((Boolean) ob1.c(Gg1.i)).booleanValue(), (Hb1) ob1.c(Gg1.f), (Fg1) ob1.c(Fg1.f), (Pb1) ob1.c(Gg1.g)));
    }

    @DexIgnore
    public final boolean e(ImageDecoder.Source source, Ob1 ob1) {
        return true;
    }
}
