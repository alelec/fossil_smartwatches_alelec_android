package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pe3 {
    @DexIgnore
    public /* final */ Rs2 a;

    @DexIgnore
    public Pe3(Rs2 rs2) {
        Rc2.k(rs2);
        this.a = rs2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void b() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.b(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void d(int i) {
        try {
            this.a.setColor(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void e(Ce3 ce3) {
        Rc2.l(ce3, "endCap must not be null");
        try {
            this.a.setEndCap(ce3);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Pe3)) {
            return false;
        }
        try {
            return this.a.B0(((Pe3) obj).a);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void f(boolean z) {
        try {
            this.a.setGeodesic(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void g(int i) {
        try {
            this.a.setJointType(i);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void h(List<Me3> list) {
        try {
            this.a.setPattern(list);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void i(List<LatLng> list) {
        try {
            this.a.setPoints(list);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void j(Ce3 ce3) {
        Rc2.l(ce3, "startCap must not be null");
        try {
            this.a.setStartCap(ce3);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void k(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void l(float f) {
        try {
            this.a.setWidth(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final void m(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
