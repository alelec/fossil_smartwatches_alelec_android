package com.fossil;

import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ck0 {
    @DexIgnore
    public HashSet<Ck0> a; // = new HashSet<>(2);
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public void a(Ck0 ck0) {
        this.a.add(ck0);
    }

    @DexIgnore
    public void b() {
        this.b = 1;
        Iterator<Ck0> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().f();
        }
    }

    @DexIgnore
    public void c() {
        this.b = 0;
        Iterator<Ck0> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().c();
        }
    }

    @DexIgnore
    public boolean d() {
        return this.b == 1;
    }

    @DexIgnore
    public void e() {
        this.b = 0;
        this.a.clear();
    }

    @DexIgnore
    public void f() {
    }
}
