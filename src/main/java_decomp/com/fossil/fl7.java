package com.fossil;

import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fl7<T> implements Yk7<T>, Serializable {
    @DexIgnore
    public volatile Object _value;
    @DexIgnore
    public Gg6<? extends T> initializer;
    @DexIgnore
    public /* final */ Object lock;

    @DexIgnore
    public Fl7(Gg6<? extends T> gg6, Object obj) {
        Wg6.c(gg6, "initializer");
        this.initializer = gg6;
        this._value = Pl7.a;
        this.lock = obj == null ? this : obj;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Fl7(Gg6 gg6, Object obj, int i, Qg6 qg6) {
        this(gg6, (i & 2) != 0 ? null : obj);
    }

    @DexIgnore
    private final Object writeReplace() {
        return new Vk7(getValue());
    }

    @DexIgnore
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0011 */
    @Override // com.fossil.Yk7
    public T getValue() {
        Object obj = (T) this._value;
        if (obj == Pl7.a) {
            synchronized (this.lock) {
                Object obj2 = this._value;
                obj = obj2;
                if (obj2 == Pl7.a) {
                    Gg6<? extends T> gg6 = this.initializer;
                    if (gg6 != null) {
                        Object invoke = gg6.invoke();
                        this._value = invoke;
                        this.initializer = null;
                        obj = invoke;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }
        return (T) obj;
    }

    @DexIgnore
    public boolean isInitialized() {
        return this._value != Pl7.a;
    }

    @DexIgnore
    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }
}
