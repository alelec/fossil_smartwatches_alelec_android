package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import com.mapped.W6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ii0 {
    @DexIgnore
    public /* final */ Intent a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public ArrayList<Bundle> b;
        @DexIgnore
        public Bundle c;
        @DexIgnore
        public ArrayList<Bundle> d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public Ai() {
            this(null);
        }

        @DexIgnore
        public Ai(Ki0 ki0) {
            IBinder iBinder = null;
            Intent intent = new Intent("android.intent.action.VIEW");
            this.a = intent;
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (ki0 != null) {
                intent.setPackage(ki0.b().getPackageName());
            }
            Bundle bundle = new Bundle();
            Vk0.b(bundle, "android.support.customtabs.extra.SESSION", ki0 != null ? ki0.a() : iBinder);
            this.a.putExtras(bundle);
        }

        @DexIgnore
        public Ii0 a() {
            ArrayList<Bundle> arrayList = this.b;
            if (arrayList != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            ArrayList<Bundle> arrayList2 = this.d;
            if (arrayList2 != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new Ii0(this.a, this.c);
        }
    }

    @DexIgnore
    public Ii0(Intent intent, Bundle bundle) {
        this.a = intent;
        this.b = bundle;
    }

    @DexIgnore
    public void a(Context context, Uri uri) {
        this.a.setData(uri);
        W6.n(context, this.a, this.b);
    }
}
