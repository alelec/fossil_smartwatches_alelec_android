package com.fossil;

import android.media.RemoteController;
import android.view.KeyEvent;
import com.facebook.internal.AnalyticsEvents;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tr5 extends Qr5 {
    @DexIgnore
    public /* final */ String c; // = "OldMusicController";
    @DexIgnore
    public /* final */ RemoteController.OnClientUpdateListener d;
    @DexIgnore
    public /* final */ RemoteController e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Rr5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements RemoteController.OnClientUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ Tr5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ai(Tr5 tr5, String str) {
            this.a = tr5;
            this.b = str;
        }

        @DexIgnore
        public void onClientChange(boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientChange clearing=" + z);
        }

        @DexIgnore
        public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
            if (metadataEditor != null) {
                String b2 = Ij5.b(metadataEditor.getString(7, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b3 = Ij5.b(metadataEditor.getString(2, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b4 = Ij5.b(metadataEditor.getString(1, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a.c;
                local.d(str, "SystemCallback of " + this.b + " - onMetadataChanged, title=" + b2 + ", artist=" + b3 + ", album=" + b4);
                Rr5 rr5 = new Rr5(this.a.d(), this.b, b2, b3, b4);
                if (!Wg6.a(rr5, this.a.h())) {
                    Rr5 h = this.a.h();
                    this.a.l(rr5);
                    this.a.j(h, rr5);
                }
            }
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i) {
            int g = this.a.g(i);
            if (g != this.a.i()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a.c;
                local.d(str, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i);
                int i2 = this.a.i();
                this.a.m(g);
                Tr5 tr5 = this.a;
                tr5.k(i2, g, tr5);
            }
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i, long j, long j2, float f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i + ", stateChangeTimeMs=" + j + ", currentPosMs=" + j2 + ", speed=" + f);
        }

        @DexIgnore
        public void onClientTransportControlUpdate(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a.c;
            local.d(str, "SystemCallback of " + this.b + " - onClientTransportControlUpdate(), transportControlFlags=" + i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Tr5(String str) {
        super(str, "All apps");
        Wg6.c(str, "appName");
        this.d = new Ai(this, str);
        this.g = new Rr5(d(), str, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
        this.e = new RemoteController(PortfolioApp.get.instance(), this.d);
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public boolean a(KeyEvent keyEvent) {
        Wg6.c(keyEvent, "keyEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, ".dispatchMediaButtonEvent of " + d() + " keyEvent " + keyEvent);
        return this.e.sendMediaKeyEvent(keyEvent);
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public Rr5 c() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public int e() {
        return this.f;
    }

    @DexIgnore
    public final int g(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 10;
            case 7:
                return 9;
            case 8:
                return 6;
            case 9:
                return 7;
            default:
                return 0;
        }
    }

    @DexIgnore
    public final Rr5 h() {
        return this.g;
    }

    @DexIgnore
    public final int i() {
        return this.f;
    }

    @DexIgnore
    public void j(Rr5 rr5, Rr5 rr52) {
        Wg6.c(rr5, "oldMetadata");
        Wg6.c(rr52, "newMetadata");
    }

    @DexIgnore
    public void k(int i, int i2, Qr5 qr5) {
        Wg6.c(qr5, "controller");
    }

    @DexIgnore
    public final void l(Rr5 rr5) {
        Wg6.c(rr5, "<set-?>");
        this.g = rr5;
    }

    @DexIgnore
    public final void m(int i) {
        this.f = i;
    }
}
