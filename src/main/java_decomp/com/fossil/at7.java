package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class At7 extends Zs7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterable<T>, Jr7 {
        @DexIgnore
        public /* final */ /* synthetic */ Ts7 b;

        @DexIgnore
        public Ai(Ts7 ts7) {
            this.b = ts7;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return this.b.iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<T, Boolean> {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();

        @DexIgnore
        public Bi() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'boolean' to match base method */
        @Override // com.mapped.Hg6
        public final Boolean invoke(T t) {
            throw null;
            //return t == null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Coroutine<T, R, Lc6<? extends T, ? extends R>> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(2);
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Lc6<T, R> invoke(T t, R r) {
            return Hl7.a(t, r);
        }
    }

    @DexIgnore
    public static final <T> Iterable<T> e(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$asIterable");
        return new Ai(ts7);
    }

    @DexIgnore
    public static final <T> boolean f(Ts7<? extends T> ts7, T t) {
        Wg6.c(ts7, "$this$contains");
        return k(ts7, t) >= 0;
    }

    @DexIgnore
    public static final <T> int g(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$count");
        Iterator<? extends T> it = ts7.iterator();
        int i = 0;
        while (it.hasNext()) {
            it.next();
            i++;
            if (i < 0) {
                Hm7.k();
                throw null;
            }
        }
        return i;
    }

    @DexIgnore
    public static final <T> Ts7<T> h(Ts7<? extends T> ts7, Hg6<? super T, Boolean> hg6) {
        Wg6.c(ts7, "$this$filter");
        Wg6.c(hg6, "predicate");
        return new Qs7(ts7, true, hg6);
    }

    @DexIgnore
    public static final <T> Ts7<T> i(Ts7<? extends T> ts7, Hg6<? super T, Boolean> hg6) {
        Wg6.c(ts7, "$this$filterNot");
        Wg6.c(hg6, "predicate");
        return new Qs7(ts7, false, hg6);
    }

    @DexIgnore
    public static final <T> Ts7<T> j(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$filterNotNull");
        Ts7<T> i = i(ts7, Bi.INSTANCE);
        if (i != null) {
            return i;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.sequences.Sequence<T>");
    }

    @DexIgnore
    public static final <T> int k(Ts7<? extends T> ts7, T t) {
        Wg6.c(ts7, "$this$indexOf");
        int i = 0;
        for (Object obj : ts7) {
            if (i < 0) {
                Hm7.l();
                throw null;
            } else if (Wg6.a(t, obj)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T, A extends Appendable> A l(Ts7<? extends T> ts7, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super T, ? extends CharSequence> hg6) {
        Wg6.c(ts7, "$this$joinTo");
        Wg6.c(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object obj : ts7) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            Nt7.a(a2, obj, hg6);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static final <T> String m(Ts7<? extends T> ts7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super T, ? extends CharSequence> hg6) {
        Wg6.c(ts7, "$this$joinToString");
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        l(ts7, sb, charSequence, charSequence2, charSequence3, i, charSequence4, hg6);
        String sb2 = sb.toString();
        Wg6.b(sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }

    @DexIgnore
    public static /* synthetic */ String n(Ts7 ts7, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6 hg6, int i2, Object obj) {
        String str = (i2 & 1) != 0 ? ", " : charSequence;
        CharSequence charSequence5 = "";
        String str2 = (i2 & 2) != 0 ? "" : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        return m(ts7, str, str2, charSequence5, (i2 & 8) != 0 ? -1 : i, (i2 & 16) != 0 ? "..." : charSequence4, (i2 & 32) != 0 ? null : hg6);
    }

    @DexIgnore
    public static final <T, R> Ts7<R> o(Ts7<? extends T> ts7, Hg6<? super T, ? extends R> hg6) {
        Wg6.c(ts7, "$this$map");
        Wg6.c(hg6, "transform");
        return new Bt7(ts7, hg6);
    }

    @DexIgnore
    public static final <T, R> Ts7<R> p(Ts7<? extends T> ts7, Hg6<? super T, ? extends R> hg6) {
        Wg6.c(ts7, "$this$mapNotNull");
        Wg6.c(hg6, "transform");
        return j(new Bt7(ts7, hg6));
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T q(com.fossil.Ts7<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$max"
            com.mapped.Wg6.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 >= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.At7.q(com.fossil.Ts7):java.lang.Comparable");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T r(com.fossil.Ts7<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$min"
            com.mapped.Wg6.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 <= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.At7.r(com.fossil.Ts7):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C s(Ts7<? extends T> ts7, C c) {
        Wg6.c(ts7, "$this$toCollection");
        Wg6.c(c, ShareConstants.DESTINATION);
        Iterator<? extends T> it = ts7.iterator();
        while (it.hasNext()) {
            c.add(it.next());
        }
        return c;
    }

    @DexIgnore
    public static final <T> List<T> t(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$toList");
        return Hm7.j(u(ts7));
    }

    @DexIgnore
    public static final <T> List<T> u(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$toMutableList");
        ArrayList arrayList = new ArrayList();
        s(ts7, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T, R> Ts7<Lc6<T, R>> v(Ts7<? extends T> ts7, Ts7<? extends R> ts72) {
        Wg6.c(ts7, "$this$zip");
        Wg6.c(ts72, FacebookRequestErrorClassification.KEY_OTHER);
        return new Ss7(ts7, ts72, Ci.INSTANCE);
    }
}
