package com.fossil;

import com.fossil.Iw7;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jw7 extends Hw7 {
    @DexIgnore
    public abstract Thread t0();

    @DexIgnore
    public final void u0(long j, Iw7.Ci ci) {
        if (Nv7.a()) {
            if (!(this != Pv7.i)) {
                throw new AssertionError();
            }
        }
        Pv7.i.F0(j, ci);
    }

    @DexIgnore
    public final void v0() {
        Thread t0 = t0();
        if (Thread.currentThread() != t0) {
            Xx7 a2 = Yx7.a();
            if (a2 != null) {
                a2.e(t0);
            } else {
                LockSupport.unpark(t0);
            }
        }
    }
}
