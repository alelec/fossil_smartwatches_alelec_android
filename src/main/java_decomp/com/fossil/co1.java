package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Co1 {
    SUCCESS((byte) 0),
    FAILED((byte) 1),
    NOT_FOUND((byte) 2);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Co1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
