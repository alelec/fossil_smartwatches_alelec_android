package com.fossil;

import android.text.TextUtils;
import com.fossil.Ji5;
import com.fossil.Mi5;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q05 {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<ArrayList<WorkoutStateChange>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<List<WorkoutGpsPoint>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends TypeToken<List<WorkoutStateChange>> {
    }

    /*
    static {
        String simpleName = Q05.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutTypeConverter::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(List<WorkoutGpsPoint> list) {
        if (list == null) {
            return null;
        }
        try {
            return new Gson().t(list);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromGpsPoints ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String b(WorkoutCadence workoutCadence) {
        try {
            return new Gson().t(workoutCadence);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCadence ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String c(WorkoutCalorie workoutCalorie) {
        if (workoutCalorie == null) {
            return null;
        }
        try {
            return new Gson().t(workoutCalorie);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String d(WorkoutDistance workoutDistance) {
        if (workoutDistance == null) {
            return null;
        }
        try {
            return new Gson().t(workoutDistance);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String e(WorkoutHeartRate workoutHeartRate) {
        if (workoutHeartRate == null) {
            return null;
        }
        try {
            return new Gson().t(workoutHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String f(Gi5 gi5) {
        if (gi5 != null) {
            return gi5.name();
        }
        return null;
    }

    @DexIgnore
    public final String g(WorkoutPace workoutPace) {
        try {
            return new Gson().t(workoutPace);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutPace ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String h(Ji5 ji5) {
        if (ji5 != null) {
            return ji5.getMValue();
        }
        return null;
    }

    @DexIgnore
    public final String i(WorkoutSpeed workoutSpeed) {
        try {
            return new Gson().t(workoutSpeed);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String j(List<WorkoutStateChange> list) {
        Wg6.c(list, "workoutStateChangeList");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String u = new Gson().u(list, new Ai().getType());
            Wg6.b(u, "Gson().toJson(workoutStateChangeList, type)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String k(WorkoutStep workoutStep) {
        if (workoutStep == null) {
            return null;
        }
        try {
            return new Gson().t(workoutStep);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            StringBuilder sb = new StringBuilder();
            sb.append("fromWorkoutStep ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String l(Mi5 mi5) {
        if (mi5 != null) {
            return mi5.name();
        }
        return null;
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> m(String str) {
        List<WorkoutGpsPoint> list;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            list = (List) new Gson().l(str, new Bi().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toGpsPoints ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            list = null;
        }
        return list;
    }

    @DexIgnore
    public final WorkoutCadence n(String str) {
        WorkoutCadence workoutCadence;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutCadence = (WorkoutCadence) new Gson().k(str, WorkoutCadence.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCadence ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutCadence = null;
        }
        return workoutCadence;
    }

    @DexIgnore
    public final WorkoutCalorie o(String str) {
        WorkoutCalorie workoutCalorie;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutCalorie = (WorkoutCalorie) new Gson().k(str, WorkoutCalorie.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutCalorie ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutCalorie = null;
        }
        return workoutCalorie;
    }

    @DexIgnore
    public final WorkoutDistance p(String str) {
        WorkoutDistance workoutDistance;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutDistance = (WorkoutDistance) new Gson().k(str, WorkoutDistance.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutDistance ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutDistance = null;
        }
        return workoutDistance;
    }

    @DexIgnore
    public final WorkoutHeartRate q(String str) {
        WorkoutHeartRate workoutHeartRate;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutHeartRate = (WorkoutHeartRate) new Gson().k(str, WorkoutHeartRate.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutHeartRate ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutHeartRate = null;
        }
        return workoutHeartRate;
    }

    @DexIgnore
    public final Gi5 r(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str != null) {
            return Gi5.valueOf(str);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final WorkoutPace s(String str) {
        WorkoutPace workoutPace;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutPace = (WorkoutPace) new Gson().k(str, WorkoutPace.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutPace ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutPace = null;
        }
        return workoutPace;
    }

    @DexIgnore
    public final Ji5 t(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Ji5.Ai ai = Ji5.Companion;
        if (str != null) {
            return ai.a(str);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final WorkoutSpeed u(String str) {
        WorkoutSpeed workoutSpeed;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutSpeed = (WorkoutSpeed) new Gson().k(str, WorkoutSpeed.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSpeed ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutSpeed = null;
        }
        return workoutSpeed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> v(String str) {
        Wg6.c(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList();
        }
        try {
            Object l = new Gson().l(str, new Ci().getType());
            Wg6.b(l, "Gson().fromJson(data, type)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStateChangeList ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final WorkoutStep w(String str) {
        WorkoutStep workoutStep;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            workoutStep = (WorkoutStep) new Gson().k(str, WorkoutStep.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutStep ex:");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            workoutStep = null;
        }
        return workoutStep;
    }

    @DexIgnore
    public final Mi5 x(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Mi5.Ai ai = Mi5.Companion;
        if (str != null) {
            return ai.a(str);
        }
        Wg6.i();
        throw null;
    }
}
