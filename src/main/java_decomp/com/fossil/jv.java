package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jv extends Mv {
    @DexIgnore
    public boolean M;

    @DexIgnore
    public Jv(short s, K5 k5) {
        super(s, Hs.n, k5, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        if (this.v.d != Lw.b) {
            this.E = true;
        } else {
            this.M = true;
        }
        return new JSONObject();
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void i(P7 p7) {
        if (p7.a != F5.b) {
            m(Mw.a(this.v, null, null, Lw.u, null, null, 27));
        } else if (p7.b == 19 || this.M) {
            m(this.v);
        } else {
            m(Mw.a(this.v, null, null, Lw.g, null, null, 27));
        }
    }
}
