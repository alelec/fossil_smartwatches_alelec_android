package com.fossil;

import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wx5 implements MembersInjector<AlarmPresenter> {
    @DexIgnore
    public static void a(AlarmPresenter alarmPresenter) {
        alarmPresenter.X();
    }
}
