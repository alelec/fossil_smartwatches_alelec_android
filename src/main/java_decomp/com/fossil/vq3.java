package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vq3 {
    @DexIgnore
    public /* final */ Ef2 a;
    @DexIgnore
    public long b;

    @DexIgnore
    public Vq3(Ef2 ef2) {
        Rc2.k(ef2);
        this.a = ef2;
    }

    @DexIgnore
    public final void a() {
        this.b = this.a.c();
    }

    @DexIgnore
    public final boolean b(long j) {
        return this.b == 0 || this.a.c() - this.b >= 3600000;
    }

    @DexIgnore
    public final void c() {
        this.b = 0;
    }
}
