package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qi2 implements Parcelable.Creator<Wh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Wh2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Boolean bool = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                str = Ad2.f(parcel, t);
            } else if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                bool = Ad2.n(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Wh2(str, i, bool);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Wh2[] newArray(int i) {
        return new Wh2[i];
    }
}
