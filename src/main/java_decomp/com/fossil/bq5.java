package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bq5 implements Factory<Aq5> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Bq5(Provider<DianaNotificationComponent> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Bq5 a(Provider<DianaNotificationComponent> provider, Provider<An4> provider2) {
        return new Bq5(provider, provider2);
    }

    @DexIgnore
    public static Aq5 c() {
        return new Aq5();
    }

    @DexIgnore
    public Aq5 b() {
        Aq5 c = c();
        Cq5.a(c, this.a.get());
        Cq5.b(c, this.b.get());
        return c;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
