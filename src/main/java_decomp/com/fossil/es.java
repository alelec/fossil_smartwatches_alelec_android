package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Es implements Parcelable.Creator<Gs> {
    @DexIgnore
    public /* synthetic */ Es(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Gs createFromParcel(Parcel parcel) {
        N6 n6 = N6.values()[parcel.readInt()];
        long readLong = parcel.readLong();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            Wg6.b(createByteArray, "parcel.createByteArray()!!");
            return new Gs(n6, readLong, createByteArray);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Gs[] newArray(int i) {
        return new Gs[i];
    }
}
