package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Na5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ Guideline E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ LinearLayout H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ ScrollView J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ View L;
    @DexIgnore
    public /* final */ View M;
    @DexIgnore
    public /* final */ Guideline N;
    @DexIgnore
    public /* final */ Barrier q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ CustomEditGoalView s;
    @DexIgnore
    public /* final */ CustomEditGoalView t;
    @DexIgnore
    public /* final */ CustomEditGoalView u;
    @DexIgnore
    public /* final */ CustomEditGoalView v;
    @DexIgnore
    public /* final */ FlexibleEditText w;
    @DexIgnore
    public /* final */ FlexibleEditText x;
    @DexIgnore
    public /* final */ FlexibleEditText y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public Na5(Object obj, View view, int i, Barrier barrier, ConstraintLayout constraintLayout, CustomEditGoalView customEditGoalView, CustomEditGoalView customEditGoalView2, CustomEditGoalView customEditGoalView3, CustomEditGoalView customEditGoalView4, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleEditText flexibleEditText3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, Guideline guideline, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout2, ScrollView scrollView, FlexibleTextView flexibleTextView6, View view2, View view3, Guideline guideline2) {
        super(obj, view, i);
        this.q = barrier;
        this.r = constraintLayout;
        this.s = customEditGoalView;
        this.t = customEditGoalView2;
        this.u = customEditGoalView3;
        this.v = customEditGoalView4;
        this.w = flexibleEditText;
        this.x = flexibleEditText2;
        this.y = flexibleEditText3;
        this.z = flexibleTextView;
        this.A = flexibleTextView2;
        this.B = flexibleTextView3;
        this.C = flexibleTextView4;
        this.D = flexibleTextView5;
        this.E = guideline;
        this.F = rTLImageView;
        this.G = linearLayout;
        this.H = linearLayout2;
        this.I = constraintLayout2;
        this.J = scrollView;
        this.K = flexibleTextView6;
        this.L = view2;
        this.M = view3;
        this.N = guideline2;
    }
}
