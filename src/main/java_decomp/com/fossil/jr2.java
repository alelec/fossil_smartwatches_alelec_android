package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jr2 implements Parcelable.Creator<Ir2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ir2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        List<Zb2> list = Ir2.i;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        String str = null;
        String str2 = null;
        LocationRequest locationRequest = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l != 1) {
                switch (l) {
                    case 5:
                        list = Ad2.j(parcel, t, Zb2.CREATOR);
                        continue;
                    case 6:
                        str2 = Ad2.f(parcel, t);
                        continue;
                    case 7:
                        z3 = Ad2.m(parcel, t);
                        continue;
                    case 8:
                        z2 = Ad2.m(parcel, t);
                        continue;
                    case 9:
                        z = Ad2.m(parcel, t);
                        continue;
                    case 10:
                        str = Ad2.f(parcel, t);
                        continue;
                    default:
                        Ad2.B(parcel, t);
                        continue;
                }
            } else {
                locationRequest = (LocationRequest) Ad2.e(parcel, t, LocationRequest.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Ir2(locationRequest, list, str2, z3, z2, z, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ir2[] newArray(int i) {
        return new Ir2[i];
    }
}
