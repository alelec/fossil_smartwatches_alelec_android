package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qi6 extends ni6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public LiveData<h47<List<HeartRateSample>>> g;
    @DexIgnore
    public LiveData<h47<List<WorkoutSession>>> h;
    @DexIgnore
    public /* final */ oi6 i;
    @DexIgnore
    public /* final */ HeartRateSampleRepository j;
    @DexIgnore
    public /* final */ WorkoutSessionRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qi6 f2987a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qi6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mHeartRateSamples$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {35, 35}, m = "invokeSuspend")
        /* renamed from: com.fossil.qi6$a$a  reason: collision with other inner class name */
        public static final class C0197a extends ko7 implements vp7<hs0<h47<? extends List<HeartRateSample>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0197a(a aVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0197a aVar = new C0197a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<HeartRateSample>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0197a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.qi6$a r1 = r8.this$0
                    com.fossil.qi6 r1 = r1.f2987a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.fossil.qi6.p(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.qi6.a.C0197a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public a(qi6 qi6) {
            this.f2987a = qi6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<HeartRateSample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mHeartRateSamples onDateChange " + date);
            return or0.c(null, 0, new C0197a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qi6 f2988a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$mWorkoutSessions$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {41, 41}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<WorkoutSession>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<WorkoutSession>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.qi6$b r1 = r8.this$0
                    com.fossil.qi6 r1 = r1.f2988a
                    com.portfolio.platform.data.source.WorkoutSessionRepository r1 = com.fossil.qi6.r(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getWorkoutSessions(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.qi6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(qi6 qi6) {
            this.f2988a = qi6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<WorkoutSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "mWorkoutSessions onDateChange " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<HeartRateSample>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qi6 f2989a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {63, 71, 78}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qi6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.qi6$c$a$a  reason: collision with other inner class name */
            public static final class C0198a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qi6$c$a$a$a")
                /* renamed from: com.fossil.qi6$c$a$a$a  reason: collision with other inner class name */
                public static final class C0199a<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return mn7.c(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0198a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0198a aVar = new C0198a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0198a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        if (list.size() > 1) {
                            lm7.r(list, new C0199a());
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$2", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $listTimeZoneChange;
                @DexIgnore
                public /* final */ /* synthetic */ List $listTodayHeartRateModel;
                @DexIgnore
                public /* final */ /* synthetic */ int $maxHR;
                @DexIgnore
                public /* final */ /* synthetic */ br7 $previousTimeZoneOffset;
                @DexIgnore
                public /* final */ /* synthetic */ long $startOfDay;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, long j, br7 br7, List list, List list2, int i, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$startOfDay = j;
                    this.$previousTimeZoneOffset = br7;
                    this.$listTimeZoneChange = list;
                    this.$listTodayHeartRateModel = list2;
                    this.$maxHR = i;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    StringBuilder sb;
                    char c;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        List list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        for (T t : list) {
                            long millis = (t.getStartTimeId().getMillis() - this.$startOfDay) / 60000;
                            long millis2 = (t.getEndTime().getMillis() - this.$startOfDay) / 60000;
                            long j = (millis2 + millis) / ((long) 2);
                            if (t.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                                int hourOfDay = t.getStartTimeId().getHourOfDay();
                                String c2 = kl5.c(hourOfDay);
                                hr7 hr7 = hr7.f1520a;
                                String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{ao7.e(Math.abs(t.getTimezoneOffsetInSecond() / 3600)), ao7.e(Math.abs((t.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                                pq7.b(format, "java.lang.String.format(format, *args)");
                                List list2 = this.$listTimeZoneChange;
                                Integer e = ao7.e((int) j);
                                cl7 cl7 = new cl7(ao7.e(hourOfDay), ao7.d(((float) t.getTimezoneOffsetInSecond()) / 3600.0f));
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(c2);
                                if (t.getTimezoneOffsetInSecond() >= 0) {
                                    sb = new StringBuilder();
                                    c = '+';
                                } else {
                                    sb = new StringBuilder();
                                    c = '-';
                                }
                                sb.append(c);
                                sb.append(format);
                                sb2.append(sb.toString());
                                list2.add(new gl7(e, cl7, sb2.toString()));
                                this.$previousTimeZoneOffset.element = t.getTimezoneOffsetInSecond();
                            }
                            if (!this.$listTodayHeartRateModel.isEmpty()) {
                                w57 w57 = (w57) pm7.P(this.$listTodayHeartRateModel);
                                if (millis - ((long) w57.a()) > 1) {
                                    this.$listTodayHeartRateModel.add(new w57(0, 0, 0, w57.a(), (int) millis, (int) j));
                                }
                            }
                            int average = (int) t.getAverage();
                            if (t.getMax() == this.$maxHR) {
                                average = t.getMax();
                            }
                            this.$listTodayHeartRateModel.add(new w57(average, t.getMin(), t.getMax(), (int) millis, (int) millis2, (int) j));
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.qi6$c$a$c")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter$start$1$1$maxHR$1", f = "HeartRateOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.qi6$c$a$c  reason: collision with other inner class name */
            public static final class C0200c extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0200c(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0200c cVar = new C0200c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    throw null;
                    //return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                    throw null;
                    //return ((C0200c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                /* JADX WARN: Multi-variable type inference failed */
                /* JADX WARN: Type inference failed for: r0v13, types: [java.lang.Comparable] */
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    int i;
                    T t;
                    Integer e;
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        List list = this.this$0.$data;
                        if (list != null) {
                            Iterator<T> it = list.iterator();
                            if (!it.hasNext()) {
                                t = null;
                            } else {
                                T next = it.next();
                                if (it.hasNext()) {
                                    Integer num = ao7.e(next.getMax());
                                    while (true) {
                                        next = it.next();
                                        Integer e2 = ao7.e(next.getMax());
                                        int compareTo = num.compareTo(e2);
                                        Integer num2 = e2;
                                        if (compareTo >= 0) {
                                            num2 = num;
                                            next = next;
                                        }
                                        if (!it.hasNext()) {
                                            break;
                                        }
                                        num = num2;
                                    }
                                }
                                t = next;
                            }
                            T t2 = t;
                            if (!(t2 == null || (e = ao7.e(t2.getMax())) == null)) {
                                i = e.intValue();
                                return ao7.e(i);
                            }
                        }
                        i = 0;
                        return ao7.e(i);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v136, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v138, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r0v141, types: [java.util.List] */
            /* JADX WARN: Type inference failed for: r1v39, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x0129  */
            /* JADX WARNING: Removed duplicated region for block: B:45:0x017a  */
            /* JADX WARNING: Removed duplicated region for block: B:50:0x01b9  */
            /* JADX WARNING: Unknown variable types count: 4 */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 884
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.qi6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(qi6 qi6) {
            this.f2989a = qi6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<HeartRateSample>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSamples -- heartRateSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                xw7 unused = gu7.d(this.f2989a.k(), null, null, new a(this, list, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<h47<? extends List<WorkoutSession>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ qi6 f2990a;

        @DexIgnore
        public d(qi6 qi6) {
            this.f2990a = qi6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<WorkoutSession>> h47) {
            xh5 a2 = h47.a();
            List<WorkoutSession> list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == xh5.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.f2990a.i.v(false, new ArrayList());
            } else {
                this.f2990a.i.v(true, list);
            }
        }
    }

    @DexIgnore
    public qi6(oi6 oi6, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        pq7.c(oi6, "mView");
        pq7.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        this.i = oi6;
        this.j = heartRateSampleRepository;
        this.k = workoutSessionRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<h47<List<HeartRateSample>>> c2 = ss0.c(mutableLiveData, new a(this));
        pq7.b(c2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = c2;
        LiveData<h47<List<WorkoutSession>>> c3 = ss0.c(this.f, new b(this));
        pq7.b(c3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.h = c3;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !lk5.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewDayPresenter", "loadData - mDate=" + this.e);
        LiveData<h47<List<HeartRateSample>>> liveData = this.g;
        oi6 oi6 = this.i;
        if (oi6 != null) {
            liveData.h((pi6) oi6, new c(this));
            this.h.h((LifecycleOwner) this.i, new d(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        try {
            LiveData<h47<List<HeartRateSample>>> liveData = this.g;
            oi6 oi6 = this.i;
            if (oi6 != null) {
                liveData.n((pi6) oi6);
                this.h.n((LifecycleOwner) this.i);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewDayPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    public final String u(Float f2) {
        if (f2 == null) {
            return "";
        }
        hr7 hr7 = hr7.f1520a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        pq7.b(format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public void v() {
        this.i.M5(this);
    }
}
