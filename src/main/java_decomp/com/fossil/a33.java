package com.fossil;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A33<E> extends Rz2<E> implements RandomAccess {
    @DexIgnore
    public static /* final */ A33<Object> e;
    @DexIgnore
    public E[] c;
    @DexIgnore
    public int d;

    /*
    static {
        A33<Object> a33 = new A33<>(new Object[0], 0);
        e = a33;
        a33.zzb();
    }
    */

    @DexIgnore
    public A33(E[] eArr, int i) {
        this.c = eArr;
        this.d = i;
    }

    @DexIgnore
    public static <E> A33<E> d() {
        return (A33<E>) e;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final void add(int i, E e2) {
        int i2;
        a();
        if (i < 0 || i > (i2 = this.d)) {
            throw new IndexOutOfBoundsException(c(i));
        }
        E[] eArr = this.c;
        if (i2 < eArr.length) {
            System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
        } else {
            E[] eArr2 = (E[]) new Object[(((i2 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i);
            System.arraycopy(this.c, i, eArr2, i + 1, this.d - i);
            this.c = eArr2;
        }
        this.c[i] = e2;
        this.d++;
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, com.fossil.Rz2
    public final boolean add(E e2) {
        a();
        int i = this.d;
        E[] eArr = this.c;
        if (i == eArr.length) {
            this.c = (E[]) Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.c;
        int i2 = this.d;
        this.d = i2 + 1;
        eArr2[i2] = e2;
        ((AbstractList) this).modCount++;
        return true;
    }

    @DexIgnore
    public final void b(int i) {
        if (i < 0 || i >= this.d) {
            throw new IndexOutOfBoundsException(c(i));
        }
    }

    @DexIgnore
    public final String c(int i) {
        int i2 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final E get(int i) {
        b(i);
        return this.c[i];
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final E remove(int i) {
        a();
        b(i);
        E[] eArr = this.c;
        E e2 = eArr[i];
        int i2 = this.d;
        if (i < i2 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.d--;
        ((AbstractList) this).modCount++;
        return e2;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final E set(int i, E e2) {
        a();
        b(i);
        E[] eArr = this.c;
        E e3 = eArr[i];
        eArr[i] = e2;
        ((AbstractList) this).modCount++;
        return e3;
    }

    @DexIgnore
    public final int size() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.M13
    public final /* synthetic */ M13 zza(int i) {
        if (i >= this.d) {
            return new A33(Arrays.copyOf(this.c, i), this.d);
        }
        throw new IllegalArgumentException();
    }
}
