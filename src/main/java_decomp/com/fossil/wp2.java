package com.fossil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Wp2 {
    @DexIgnore
    ExecutorService a(int i, ThreadFactory threadFactory, int i2);

    @DexIgnore
    ExecutorService b(ThreadFactory threadFactory, int i);
}
