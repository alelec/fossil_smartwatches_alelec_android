package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bl3 extends Pu2 implements Cl3 {
    @DexIgnore
    public Bl3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.Pu2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                E1((Vg3) Qt2.a(parcel, Vg3.CREATOR), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 2:
                u2((Fr3) Qt2.a(parcel, Fr3.CREATOR), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                r1((Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 5:
                K1((Vg3) Qt2.a(parcel, Vg3.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 6:
                P1((Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 7:
                List<Fr3> p1 = p1((Or3) Qt2.a(parcel, Or3.CREATOR), Qt2.e(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(p1);
                break;
            case 9:
                byte[] w2 = w2((Vg3) Qt2.a(parcel, Vg3.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(w2);
                break;
            case 10:
                N0(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                break;
            case 11:
                String n0 = n0((Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(n0);
                break;
            case 12:
                s((Xr3) Qt2.a(parcel, Xr3.CREATOR), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 13:
                D1((Xr3) Qt2.a(parcel, Xr3.CREATOR));
                parcel2.writeNoException();
                break;
            case 14:
                List<Fr3> o1 = o1(parcel.readString(), parcel.readString(), Qt2.e(parcel), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(o1);
                break;
            case 15:
                List<Fr3> S = S(parcel.readString(), parcel.readString(), parcel.readString(), Qt2.e(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(S);
                break;
            case 16:
                List<Xr3> V0 = V0(parcel.readString(), parcel.readString(), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(V0);
                break;
            case 17:
                List<Xr3> T0 = T0(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(T0);
                break;
            case 18:
                S0((Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
            case 19:
                n2((Bundle) Qt2.a(parcel, Bundle.CREATOR), (Or3) Qt2.a(parcel, Or3.CREATOR));
                parcel2.writeNoException();
                break;
        }
        return true;
    }
}
