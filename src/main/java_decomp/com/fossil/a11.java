package com.fossil;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface A11 {
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final Bi.Cii a = new Bi.Cii();
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final Bi.Bii b = new Bi.Bii();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Bi {
            @DexIgnore
            public /* final */ Throwable a;

            @DexIgnore
            public Aii(Throwable th) {
                this.a = th;
            }

            @DexIgnore
            public Throwable a() {
                return this.a;
            }

            @DexIgnore
            public String toString() {
                return String.format("FAILURE (%s)", this.a.getMessage());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Bi {
            @DexIgnore
            public Bii() {
            }

            @DexIgnore
            public String toString() {
                return "IN_PROGRESS";
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Bi {
            @DexIgnore
            public Cii() {
            }

            @DexIgnore
            public String toString() {
                return "SUCCESS";
            }
        }
    }
}
