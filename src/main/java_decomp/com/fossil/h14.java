package com.fossil;

import java.io.Serializable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H14<T> extends Z04<Iterable<T>> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public /* final */ Z04<? super T> elementEquivalence;

    @DexIgnore
    public H14(Z04<? super T> z04) {
        I14.l(z04);
        this.elementEquivalence = z04;
    }

    @DexIgnore
    public boolean doEquivalent(Iterable<T> iterable, Iterable<T> iterable2) {
        Iterator<T> it = iterable.iterator();
        Iterator<T> it2 = iterable2.iterator();
        while (it.hasNext() && it2.hasNext()) {
            if (!this.elementEquivalence.equivalent(it.next(), it2.next())) {
                return false;
            }
        }
        return !it.hasNext() && !it2.hasNext();
    }

    @DexIgnore
    @Override // com.fossil.Z04
    public /* bridge */ /* synthetic */ boolean doEquivalent(Object obj, Object obj2) {
        return doEquivalent((Iterable) ((Iterable) obj), (Iterable) ((Iterable) obj2));
    }

    @DexIgnore
    public int doHash(Iterable<T> iterable) {
        int i = 78721;
        for (T t : iterable) {
            i = (i * 24943) + this.elementEquivalence.hash(t);
        }
        return i;
    }

    @DexIgnore
    @Override // com.fossil.Z04
    public /* bridge */ /* synthetic */ int doHash(Object obj) {
        return doHash((Iterable) ((Iterable) obj));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof H14) {
            return this.elementEquivalence.equals(((H14) obj).elementEquivalence);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.elementEquivalence.hashCode() ^ 1185147655;
    }

    @DexIgnore
    public String toString() {
        return this.elementEquivalence + ".pairwise()";
    }
}
