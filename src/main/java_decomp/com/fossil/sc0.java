package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sc0 implements Parcelable.Creator<Tc0> {
    @DexIgnore
    public /* synthetic */ Sc0(Qg6 qg6) {
    }

    @DexIgnore
    public Tc0 a(Parcel parcel) {
        return a(parcel);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public Tc0 createFromParcel(Parcel parcel) {
        return a(parcel);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public Tc0[] newArray(int i) {
        return new Tc0[i];
    }
}
