package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vi7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable c;

    @DexIgnore
    public Vi7(Context context, Throwable th) {
        this.b = context;
        this.c = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (Fg7.M()) {
                new Ch7(new Mg7(this.b, Ig7.a(this.b, false, null), 99, this.c, Pg7.m)).b();
            }
        } catch (Throwable th) {
            Th7 th7 = Ig7.m;
            th7.d("reportSdkSelfException error: " + th);
        }
    }
}
