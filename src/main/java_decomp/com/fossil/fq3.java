package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Fq3 implements Runnable {
    @DexIgnore
    public /* final */ Gq3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Kl3 d;
    @DexIgnore
    public /* final */ Intent e;

    @DexIgnore
    public Fq3(Gq3 gq3, int i, Kl3 kl3, Intent intent) {
        this.b = gq3;
        this.c = i;
        this.d = kl3;
        this.e = intent;
    }

    @DexIgnore
    public final void run() {
        this.b.d(this.c, this.d, this.e);
    }
}
