package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zo2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Zo2> CREATOR; // = new Cp2();
    @DexIgnore
    public /* final */ Uh2 b;

    @DexIgnore
    public Zo2(Uh2 uh2) {
        this.b = uh2;
    }

    @DexIgnore
    public final Uh2 c() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", this.b);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, this.b, i, false);
        Bd2.b(parcel, a2);
    }
}
