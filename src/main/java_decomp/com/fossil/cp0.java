package com.fossil;

import android.content.res.Resources;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cp0 implements View.OnTouchListener {
    @DexIgnore
    public static /* final */ int x; // = ViewConfiguration.getTapTimeout();
    @DexIgnore
    public /* final */ Ai b; // = new Ai();
    @DexIgnore
    public /* final */ Interpolator c; // = new AccelerateInterpolator();
    @DexIgnore
    public /* final */ View d;
    @DexIgnore
    public Runnable e;
    @DexIgnore
    public float[] f; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] g; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public float[] j; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] k; // = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
    @DexIgnore
    public float[] l; // = {Float.MAX_VALUE, Float.MAX_VALUE};
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;
        @DexIgnore
        public long e; // = Long.MIN_VALUE;
        @DexIgnore
        public long f; // = 0;
        @DexIgnore
        public int g; // = 0;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public long i; // = -1;
        @DexIgnore
        public float j;
        @DexIgnore
        public int k;

        @DexIgnore
        public void a() {
            if (this.f != 0) {
                long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
                float g2 = g(e(currentAnimationTimeMillis));
                long j2 = this.f;
                this.f = currentAnimationTimeMillis;
                float f2 = ((float) (currentAnimationTimeMillis - j2)) * g2;
                this.g = (int) (this.c * f2);
                this.h = (int) (f2 * this.d);
                return;
            }
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }

        @DexIgnore
        public int b() {
            return this.g;
        }

        @DexIgnore
        public int c() {
            return this.h;
        }

        @DexIgnore
        public int d() {
            float f2 = this.c;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public final float e(long j2) {
            if (j2 < this.e) {
                return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            long j3 = this.i;
            if (j3 < 0 || j2 < j3) {
                return Cp0.e(((float) (j2 - this.e)) / ((float) this.a), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f) * 0.5f;
            }
            float f2 = this.j;
            return (Cp0.e(((float) (j2 - j3)) / ((float) this.k), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f) * f2) + (1.0f - f2);
        }

        @DexIgnore
        public int f() {
            float f2 = this.d;
            return (int) (f2 / Math.abs(f2));
        }

        @DexIgnore
        public final float g(float f2) {
            return (-4.0f * f2 * f2) + (4.0f * f2);
        }

        @DexIgnore
        public boolean h() {
            return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
        }

        @DexIgnore
        public void i() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.k = Cp0.f((int) (currentAnimationTimeMillis - this.e), 0, this.b);
            this.j = e(currentAnimationTimeMillis);
            this.i = currentAnimationTimeMillis;
        }

        @DexIgnore
        public void j(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public void k(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public void l(float f2, float f3) {
            this.c = f2;
            this.d = f3;
        }

        @DexIgnore
        public void m() {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.e = currentAnimationTimeMillis;
            this.i = -1;
            this.f = currentAnimationTimeMillis;
            this.j = 0.5f;
            this.g = 0;
            this.h = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void run() {
            Cp0 cp0 = Cp0.this;
            if (cp0.u) {
                if (cp0.s) {
                    cp0.s = false;
                    cp0.b.m();
                }
                Ai ai = Cp0.this.b;
                if (ai.h() || !Cp0.this.u()) {
                    Cp0.this.u = false;
                    return;
                }
                Cp0 cp02 = Cp0.this;
                if (cp02.t) {
                    cp02.t = false;
                    cp02.c();
                }
                ai.a();
                Cp0.this.j(ai.b(), ai.c());
                Mo0.d0(Cp0.this.d, this);
            }
        }
    }

    @DexIgnore
    public Cp0(View view) {
        this.d = view;
        float f2 = Resources.getSystem().getDisplayMetrics().density;
        float f3 = (float) ((int) ((1575.0f * f2) + 0.5f));
        o(f3, f3);
        float f4 = (float) ((int) ((f2 * 315.0f) + 0.5f));
        p(f4, f4);
        l(1);
        n(Float.MAX_VALUE, Float.MAX_VALUE);
        s(0.2f, 0.2f);
        t(1.0f, 1.0f);
        k(x);
        r(500);
        q(500);
    }

    @DexIgnore
    public static float e(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    @DexIgnore
    public static int f(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    @DexIgnore
    public abstract boolean a(int i2);

    @DexIgnore
    public abstract boolean b(int i2);

    @DexIgnore
    public void c() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        this.d.onTouchEvent(obtain);
        obtain.recycle();
    }

    @DexIgnore
    public final float d(int i2, float f2, float f3, float f4) {
        float h2 = h(this.f[i2], f3, this.g[i2], f2);
        int i3 = (h2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (h2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
        if (i3 == 0) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f5 = this.j[i2];
        float f6 = this.k[i2];
        float f7 = this.l[i2];
        float f8 = f5 * f4;
        return i3 > 0 ? e(f8 * h2, f6, f7) : -e(f8 * (-h2), f6, f7);
    }

    @DexIgnore
    public final float g(float f2, float f3) {
        if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i2 = this.h;
        if (i2 != 0 && i2 != 1) {
            return (i2 != 2 || f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f2 / (-f3);
        }
        if (f2 >= f3) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return 1.0f - (f2 / f3);
        }
        if (!this.u || this.h != 1) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return 1.0f;
    }

    @DexIgnore
    public final float h(float f2, float f3, float f4, float f5) {
        float interpolation;
        float e2 = e(f2 * f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4);
        float g2 = g(f3 - f5, e2) - g(f5, e2);
        if (g2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            interpolation = -this.c.getInterpolation(-g2);
        } else if (g2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            interpolation = this.c.getInterpolation(g2);
        }
        return e(interpolation, -1.0f, 1.0f);
    }

    @DexIgnore
    public final void i() {
        if (this.s) {
            this.u = false;
        } else {
            this.b.i();
        }
    }

    @DexIgnore
    public abstract void j(int i2, int i3);

    @DexIgnore
    public Cp0 k(int i2) {
        this.i = i2;
        return this;
    }

    @DexIgnore
    public Cp0 l(int i2) {
        this.h = i2;
        return this;
    }

    @DexIgnore
    public Cp0 m(boolean z) {
        if (this.v && !z) {
            i();
        }
        this.v = z;
        return this;
    }

    @DexIgnore
    public Cp0 n(float f2, float f3) {
        float[] fArr = this.g;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public Cp0 o(float f2, float f3) {
        float[] fArr = this.l;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r2 != 3) goto L_0x0015;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r7, android.view.MotionEvent r8) {
        /*
            r6 = this;
            r0 = 1
            r1 = 0
            boolean r2 = r6.v
            if (r2 != 0) goto L_0x0007
        L_0x0006:
            return r1
        L_0x0007:
            int r2 = r8.getActionMasked()
            if (r2 == 0) goto L_0x0023
            if (r2 == r0) goto L_0x001f
            r3 = 2
            if (r2 == r3) goto L_0x0027
            r3 = 3
            if (r2 == r3) goto L_0x001f
        L_0x0015:
            boolean r2 = r6.w
            if (r2 == 0) goto L_0x0062
            boolean r2 = r6.u
            if (r2 == 0) goto L_0x0062
        L_0x001d:
            r1 = r0
            goto L_0x0006
        L_0x001f:
            r6.i()
            goto L_0x0015
        L_0x0023:
            r6.t = r0
            r6.m = r1
        L_0x0027:
            float r2 = r8.getX()
            int r3 = r7.getWidth()
            float r3 = (float) r3
            android.view.View r4 = r6.d
            int r4 = r4.getWidth()
            float r4 = (float) r4
            float r2 = r6.d(r1, r2, r3, r4)
            float r3 = r8.getY()
            int r4 = r7.getHeight()
            float r4 = (float) r4
            android.view.View r5 = r6.d
            int r5 = r5.getHeight()
            float r5 = (float) r5
            float r3 = r6.d(r0, r3, r4, r5)
            com.fossil.Cp0$Ai r4 = r6.b
            r4.l(r2, r3)
            boolean r2 = r6.u
            if (r2 != 0) goto L_0x0015
            boolean r2 = r6.u()
            if (r2 == 0) goto L_0x0015
            r6.v()
            goto L_0x0015
        L_0x0062:
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Cp0.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public Cp0 p(float f2, float f3) {
        float[] fArr = this.k;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public Cp0 q(int i2) {
        this.b.j(i2);
        return this;
    }

    @DexIgnore
    public Cp0 r(int i2) {
        this.b.k(i2);
        return this;
    }

    @DexIgnore
    public Cp0 s(float f2, float f3) {
        float[] fArr = this.f;
        fArr[0] = f2;
        fArr[1] = f3;
        return this;
    }

    @DexIgnore
    public Cp0 t(float f2, float f3) {
        float[] fArr = this.j;
        fArr[0] = f2 / 1000.0f;
        fArr[1] = f3 / 1000.0f;
        return this;
    }

    @DexIgnore
    public boolean u() {
        Ai ai = this.b;
        int f2 = ai.f();
        int d2 = ai.d();
        return (f2 != 0 && b(f2)) || (d2 != 0 && a(d2));
    }

    @DexIgnore
    public final void v() {
        int i2;
        if (this.e == null) {
            this.e = new Bi();
        }
        this.u = true;
        this.s = true;
        if (this.m || (i2 = this.i) <= 0) {
            this.e.run();
        } else {
            Mo0.e0(this.d, this.e, (long) i2);
        }
        this.m = true;
    }
}
