package com.fossil;

import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Te2 {
    @DexIgnore
    public static Ai a;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        ScheduledExecutorService a();
    }

    @DexIgnore
    public static Ai a() {
        Ai ai;
        synchronized (Te2.class) {
            try {
                if (a == null) {
                    a = new Ue2();
                }
                ai = a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ai;
    }
}
