package com.fossil;

import com.fossil.iq4.a;
import com.fossil.iq4.b;
import com.fossil.iq4.d;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iq4<Q extends b, R extends d, E extends a> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public e<? super R, ? super E> f1653a;
    @DexIgnore
    public String b; // = "CoroutineUseCase";
    @DexIgnore
    public /* final */ iv7 c; // = jv7.a(bw7.a());

    @DexIgnore
    public interface a extends c {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d extends c {
    }

    @DexIgnore
    public interface e<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.CoroutineUseCase$executeUseCase$1", f = "CoroutineUseCase.kt", l = {35}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ e $callBack;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(iq4 iq4, e eVar, b bVar, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iq4;
            this.$callBack = eVar;
            this.$requestValues = bVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$callBack, this.$requestValues, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v4, resolved type: com.fossil.iq4 */
        /* JADX DEBUG: Multi-variable search result rejected for r1v3, resolved type: com.fossil.iq4 */
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.iq4 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object k;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.f1653a = this.$callBack;
                iq4 iq4 = this.this$0;
                iq4.b = iq4.h();
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Start UseCase");
                iq4 iq42 = this.this$0;
                b bVar = this.$requestValues;
                this.L$0 = iv7;
                this.label = 1;
                k = iq42.k(bVar, this);
                if (k == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                k = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (k instanceof d) {
                this.this$0.j((d) k);
            } else if (k instanceof a) {
                this.this$0.i((a) k);
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.CoroutineUseCase$onError$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ a $errorValue;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(iq4 iq4, a aVar, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iq4;
            this.$errorValue = aVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$errorValue, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback failed");
                e eVar = this.this$0.f1653a;
                if (eVar != null) {
                    eVar.a(this.$errorValue);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.CoroutineUseCase$onSuccess$1", f = "CoroutineUseCase.kt", l = {}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ d $response;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(iq4 iq4, d dVar, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iq4;
            this.$response = dVar;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$response, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                FLogger.INSTANCE.getLocal().d(this.this$0.b, "Trigger UseCase callback success");
                e eVar = this.this$0.f1653a;
                if (eVar != null) {
                    eVar.onSuccess(this.$response);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final xw7 e(Q q, e<? super R, ? super E> eVar) {
        return gu7.d(this.c, null, null, new f(this, eVar, q, null), 3, null);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.iq4$e<? super R extends com.fossil.iq4$d, ? super E extends com.fossil.iq4$a>, com.fossil.iq4$e<R extends com.fossil.iq4$d, E extends com.fossil.iq4$a> */
    public final e<R, E> f() {
        return (e<? super R, ? super E>) this.f1653a;
    }

    @DexIgnore
    public final iv7 g() {
        return this.c;
    }

    @DexIgnore
    public abstract String h();

    @DexIgnore
    public final xw7 i(E e2) {
        pq7.c(e2, "errorValue");
        return gu7.d(this.c, bw7.c(), null, new g(this, e2, null), 2, null);
    }

    @DexIgnore
    public final xw7 j(R r) {
        pq7.c(r, "response");
        return gu7.d(this.c, bw7.c(), null, new h(this, r, null), 2, null);
    }

    @DexIgnore
    public abstract Object k(Q q, qn7<Object> qn7);

    @DexIgnore
    public final void l(e<? super R, ? super E> eVar) {
        pq7.c(eVar, Constants.CALLBACK);
        this.f1653a = eVar;
    }
}
