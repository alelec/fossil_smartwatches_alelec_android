package com.fossil;

import android.graphics.Typeface;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static qn5 k;
    @DexIgnore
    public static /* final */ a l; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3001a; // = "theme/fonts/";
    @DexIgnore
    public /* final */ String b; // = "theme/fonts/ginger/";
    @DexIgnore
    public /* final */ String c; // = "theme/fonts/sf/";
    @DexIgnore
    public /* final */ String d; // = ".otf";
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Style> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ ej0<String, String> f; // = new ej0<>(10);
    @DexIgnore
    public /* final */ ej0<String, Typeface> g; // = new ej0<>(10);
    @DexIgnore
    public /* final */ ArrayList<String> h; // = hm7.c(this.f3001a, this.b, this.c);
    @DexIgnore
    public ThemeRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final qn5 a() {
            qn5 qn5;
            synchronized (this) {
                if (qn5.k == null) {
                    qn5.k = new qn5();
                }
                qn5 = qn5.k;
                if (qn5 == null) {
                    pq7.i();
                    throw null;
                }
            }
            return qn5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.ThemeManager$addNewTheme$1", f = "ThemeManager.kt", l = {128, 132}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qn5 qn5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = qn5;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x006e  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0055
                if (r0 == r2) goto L_0x0024
                if (r0 != r4) goto L_0x001c
                java.lang.Object r0 = r5.L$1
                com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                java.lang.Object r0 = r5.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r6)
            L_0x0019:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001b:
                return r0
            L_0x001c:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0024:
                java.lang.Object r0 = r5.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r6)
                r2 = r0
                r1 = r6
            L_0x002d:
                r0 = r1
                com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
                if (r0 == 0) goto L_0x006e
                java.lang.String r1 = r5.$id
                r0.setId(r1)
                java.lang.String r1 = "New theme"
                r0.setName(r1)
                java.lang.String r1 = "modified"
                r0.setType(r1)
                com.fossil.qn5 r1 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.h()
                r5.L$0 = r2
                r5.L$1 = r0
                r5.label = r4
                java.lang.Object r0 = r1.upsertUserTheme(r0, r5)
                if (r0 != r3) goto L_0x0019
                r0 = r3
                goto L_0x001b
            L_0x0055:
                com.fossil.el7.b(r6)
                com.fossil.iv7 r0 = r5.p$
                com.fossil.qn5 r1 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.h()
                r5.L$0 = r0
                r5.label = r2
                java.lang.String r2 = "default"
                java.lang.Object r1 = r1.getThemeById(r2, r5)
                if (r1 != r3) goto L_0x0073
                r0 = r3
                goto L_0x001b
            L_0x006e:
                com.fossil.pq7.i()
                r0 = 0
                throw r0
            L_0x0073:
                r2 = r0
                goto L_0x002d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn5.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.ThemeManager", f = "ThemeManager.kt", l = {38}, m = "reloadStyleCache")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qn5 qn5, qn7 qn7) {
            super(qn7);
            this.this$0 = qn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    /*
    static {
        String simpleName = qn5.class.getSimpleName();
        pq7.b(simpleName, "ThemeManager::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public qn5() {
        PortfolioApp.h0.c().M().O1(this);
    }

    @DexIgnore
    public final void c(String str) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "addNewTheme id=" + str);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, str, null), 3, null);
    }

    @DexIgnore
    public final String d(String str) {
        T t;
        pq7.c(str, "themeStyle");
        String d2 = this.f.d(str);
        if (d2 == null) {
            Iterator<T> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.getKey(), str)) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            d2 = t2 != null ? t2.getValue() : null;
            if (!TextUtils.isEmpty(d2)) {
                ej0<String, String> ej0 = this.f;
                if (d2 != null) {
                    ej0.f(str, d2);
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        return d2;
    }

    @DexIgnore
    public final String e(String str, List<Style> list) {
        T t;
        pq7.c(str, "themeStyle");
        pq7.c(list, "predefineStyles");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return t2.getValue();
        }
        return null;
    }

    @DexIgnore
    public final Typeface f(String str) {
        T t;
        Typeface typeface;
        pq7.c(str, "themeStyle");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        ej0<String, Typeface> ej0 = this.g;
        if (value != null) {
            Typeface d2 = ej0.d(value);
            if (d2 == null) {
                Iterator<T> it2 = this.h.iterator();
                while (true) {
                    typeface = d2;
                    if (!it2.hasNext()) {
                        break;
                    }
                    try {
                        Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.h0.c().getAssets(), ((String) it2.next()) + value + this.d);
                        ej0<String, Typeface> ej02 = this.g;
                        if (createFromAsset != null) {
                            ej02.f(value, createFromAsset);
                            return createFromAsset;
                        }
                        pq7.i();
                        throw null;
                    } catch (Exception e2) {
                        d2 = typeface;
                        FLogger.INSTANCE.getLocal().d(j, "font file is not exists");
                    }
                }
            } else {
                typeface = d2;
            }
            return typeface;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final Typeface g(String str, List<Style> list) {
        T t;
        pq7.c(str, "themeStyle");
        pq7.c(list, "predefineStyles");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getKey(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        String value = t2 != null ? t2.getValue() : null;
        Iterator<T> it2 = this.h.iterator();
        Typeface typeface = null;
        while (it2.hasNext()) {
            try {
                Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.h0.c().getAssets(), ((String) it2.next()) + value + this.d);
                ej0<String, Typeface> ej0 = this.g;
                if (createFromAsset != null) {
                    ej0.f(str, createFromAsset);
                    return createFromAsset;
                }
                pq7.i();
                throw null;
            } catch (Exception e2) {
                typeface = typeface;
            }
        }
        return typeface;
    }

    @DexIgnore
    public final ThemeRepository h() {
        ThemeRepository themeRepository = this.i;
        if (themeRepository != null) {
            return themeRepository;
        }
        pq7.n("mThemeRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r6 = this;
            r2 = 0
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.qn5.c
            if (r0 == 0) goto L_0x005d
            r0 = r7
            com.fossil.qn5$c r0 = (com.fossil.qn5.c) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x005d
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x006b
            if (r4 != r5) goto L_0x0063
            java.lang.Object r0 = r0.L$0
            com.fossil.qn5 r0 = (com.fossil.qn5) r0
            com.fossil.el7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.portfolio.platform.data.model.Theme r0 = (com.portfolio.platform.data.model.Theme) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r1.getLocal()
            java.lang.String r4 = com.fossil.qn5.j
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "reload style cache currentId "
            r5.append(r1)
            if (r0 == 0) goto L_0x0099
            java.lang.String r1 = r0.getId()
        L_0x0043:
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r3.d(r4, r1)
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r1 = r6.e
            if (r0 == 0) goto L_0x008d
            java.util.ArrayList r0 = r0.getStyles()
            if (r0 == 0) goto L_0x008d
        L_0x0057:
            r1.addAll(r0)
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x005c:
            return r0
        L_0x005d:
            com.fossil.qn5$c r0 = new com.fossil.qn5$c
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0063:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006b:
            com.fossil.el7.b(r1)
            com.fossil.ej0<java.lang.String, java.lang.String> r1 = r6.f
            r1.c()
            com.fossil.ej0<java.lang.String, android.graphics.Typeface> r1 = r6.g
            r1.c()
            java.util.concurrent.CopyOnWriteArrayList<com.portfolio.platform.data.model.Style> r1 = r6.e
            r1.clear()
            com.portfolio.platform.data.source.ThemeRepository r1 = r6.i
            if (r1 == 0) goto L_0x0093
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = r1.getCurrentTheme(r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x005c
        L_0x008d:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x0057
        L_0x0093:
            java.lang.String r0 = "mThemeRepository"
            com.fossil.pq7.n(r0)
            throw r2
        L_0x0099:
            r1 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn5.i(com.fossil.qn7):java.lang.Object");
    }
}
