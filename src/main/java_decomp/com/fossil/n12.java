package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N12 {
    @DexIgnore
    public static H22 a(Context context, K22 k22, V12 v12, T32 t32) {
        return Build.VERSION.SDK_INT >= 21 ? new T12(context, k22, v12) : new P12(context, k22, t32, v12);
    }
}
