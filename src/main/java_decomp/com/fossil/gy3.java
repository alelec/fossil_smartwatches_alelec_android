package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gy3<S> extends Ly3<S> {
    @DexIgnore
    public Zx3<S> c;
    @DexIgnore
    public Wx3 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Ky3<S> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Ky3
        public void a(S s) {
            Iterator<Ky3<S>> it = Gy3.this.b.iterator();
            while (it.hasNext()) {
                it.next().a(s);
            }
        }
    }

    @DexIgnore
    public static <T> Gy3<T> x6(Zx3<T> zx3, Wx3 wx3) {
        Gy3<T> gy3 = new Gy3<>();
        Bundle bundle = new Bundle();
        bundle.putParcelable("DATE_SELECTOR_KEY", zx3);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", wx3);
        gy3.setArguments(bundle);
        return gy3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.c = (Zx3) bundle.getParcelable("DATE_SELECTOR_KEY");
        this.d = (Wx3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.c.G(layoutInflater, viewGroup, bundle, this.d, new Ai());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("DATE_SELECTOR_KEY", this.c);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.d);
    }
}
