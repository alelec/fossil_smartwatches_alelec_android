package com.fossil;

import android.content.Context;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Ji4 implements Callable {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public /* final */ FirebaseInstanceId c;
    @DexIgnore
    public /* final */ Rf4 d;
    @DexIgnore
    public /* final */ Ef4 e;

    @DexIgnore
    public Ji4(Context context, ScheduledExecutorService scheduledExecutorService, FirebaseInstanceId firebaseInstanceId, Rf4 rf4, Ef4 ef4) {
        this.a = context;
        this.b = scheduledExecutorService;
        this.c = firebaseInstanceId;
        this.d = rf4;
        this.e = ef4;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Ki4.i(this.a, this.b, this.c, this.d, this.e);
    }
}
