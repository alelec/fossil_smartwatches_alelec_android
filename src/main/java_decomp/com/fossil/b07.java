package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B07 extends Yz6 {
    @DexIgnore
    public RequestEmailOtp e;
    @DexIgnore
    public VerifyEmailOtp f;
    @DexIgnore
    public SignUpEmailAuth g;
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ Zz6 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<RequestEmailOtp.Ci, RequestEmailOtp.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ B07 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(B07 b07) {
            this.a = b07;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(RequestEmailOtp.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(RequestEmailOtp.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.i.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "resendOtpCode errorCode=" + bi.a() + " message=" + bi.b());
            this.a.i.l5(bi.a(), bi.b());
        }

        @DexIgnore
        public void c(RequestEmailOtp.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.i.h();
            this.a.i.D5();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(RequestEmailOtp.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<VerifyEmailOtp.Ci, VerifyEmailOtp.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ B07 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(B07 b07) {
            this.a = b07;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(VerifyEmailOtp.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(VerifyEmailOtp.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.i.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "verifyOtpCode errorCode=" + bi.a() + " message=" + bi.b());
            this.a.i.c5(bi.a() == 401);
            this.a.i.l5(bi.a(), bi.b());
        }

        @DexIgnore
        public void c(VerifyEmailOtp.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.i.h();
            Zz6 zz6 = this.a.i;
            SignUpEmailAuth t = this.a.t();
            if (t != null) {
                zz6.z1(t.getEmail(), 10, 20);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(VerifyEmailOtp.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore
    public B07(Zz6 zz6) {
        Wg6.c(zz6, "mView");
        this.i = zz6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            this.i.T0(signUpEmailAuth.getEmail());
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Yz6
    public void n(String[] strArr) {
        String str;
        boolean z = true;
        Wg6.c(strArr, "codes");
        v(strArr);
        String str2 = this.h;
        if ((str2 == null || Vt7.l(str2)) || (str = this.h) == null || str.length() != 4) {
            z = false;
        }
        this.i.M1(z);
    }

    @DexIgnore
    @Override // com.fossil.Yz6
    public void o() {
        this.i.U1();
    }

    @DexIgnore
    @Override // com.fossil.Yz6
    public void p() {
        this.i.c5(false);
        this.i.i();
        RequestEmailOtp requestEmailOtp = this.e;
        if (requestEmailOtp != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            String email = signUpEmailAuth != null ? signUpEmailAuth.getEmail() : null;
            if (email != null) {
                requestEmailOtp.e(new RequestEmailOtp.Ai(email), new Ai(this));
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yz6
    public void q() {
        Zz6 zz6 = this.i;
        SignUpEmailAuth signUpEmailAuth = this.g;
        if (signUpEmailAuth != null) {
            zz6.B1(signUpEmailAuth);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yz6
    public void r() {
        this.i.c5(false);
        this.i.i();
        VerifyEmailOtp verifyEmailOtp = this.f;
        if (verifyEmailOtp != null) {
            SignUpEmailAuth signUpEmailAuth = this.g;
            if (signUpEmailAuth != null) {
                String email = signUpEmailAuth.getEmail();
                String str = this.h;
                if (str != null) {
                    verifyEmailOtp.e(new VerifyEmailOtp.Ai(email, str), new Bi(this));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mVerifyEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public final SignUpEmailAuth t() {
        return this.g;
    }

    @DexIgnore
    public final void u(SignUpEmailAuth signUpEmailAuth) {
        Wg6.c(signUpEmailAuth, "emailAuth");
        this.g = signUpEmailAuth;
    }

    @DexIgnore
    public final void v(String[] strArr) {
        this.h = "";
        for (String str : strArr) {
            String str2 = this.h;
            if (str != null) {
                this.h = Wg6.h(str2, Wt7.u0(str).toString());
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }

    @DexIgnore
    public void w() {
        this.i.M5(this);
    }
}
