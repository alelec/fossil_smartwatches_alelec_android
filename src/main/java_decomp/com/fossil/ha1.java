package com.fossil;

import com.fossil.O91;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ha1 extends Ia1<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ha1(int i, String str, JSONObject jSONObject, O91.Bi<JSONObject> bi, O91.Ai ai) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bi, ai);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Ha1(String str, JSONObject jSONObject, O91.Bi<JSONObject> bi, O91.Ai ai) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bi, ai);
    }

    @DexIgnore
    @Override // com.fossil.M91, com.fossil.Ia1
    public O91<JSONObject> parseNetworkResponse(J91 j91) {
        try {
            return O91.c(new JSONObject(new String(j91.b, Ba1.d(j91.c, Ia1.PROTOCOL_CHARSET))), Ba1.c(j91));
        } catch (UnsupportedEncodingException e) {
            return O91.a(new L91(e));
        } catch (JSONException e2) {
            return O91.a(new L91(e2));
        }
    }
}
