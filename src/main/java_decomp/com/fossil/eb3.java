package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.mapped.Wv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eb3 extends Sq2 {
    @DexIgnore
    public /* final */ /* synthetic */ Ot3 b;

    @DexIgnore
    public Eb3(Wv2 wv2, Ot3 ot3) {
        this.b = ot3;
    }

    @DexIgnore
    @Override // com.fossil.Rq2
    public final void M(Pq2 pq2) throws RemoteException {
        Status a2 = pq2.a();
        if (a2 == null) {
            this.b.d(new N62(new Status(8, "Got null status from location service")));
        } else if (a2.f() == 0) {
            this.b.c(Boolean.TRUE);
        } else {
            this.b.d(Xb2.a(a2));
        }
    }
}
