package com.fossil;

import com.mapped.Wg6;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ho7 {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null, null, null);
    @DexIgnore
    public static Ai b;
    @DexIgnore
    public static /* final */ Ho7 c; // = new Ho7();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public Ai(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    @DexIgnore
    public final Ai a(Zn7 zn7) {
        try {
            Ai ai = new Ai(Class.class.getDeclaredMethod("getModule", new Class[0]), zn7.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), zn7.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", new Class[0]));
            b = ai;
            return ai;
        } catch (Exception e) {
            Ai ai2 = a;
            b = ai2;
            return ai2;
        }
    }

    @DexIgnore
    public final String b(Zn7 zn7) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        Wg6.c(zn7, "continuation");
        Ai ai = b;
        if (ai == null) {
            ai = a(zn7);
        }
        if (ai == a || (method = ai.a) == null || (invoke = method.invoke(zn7.getClass(), new Object[0])) == null || (method2 = ai.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = ai.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        if (!(invoke3 instanceof String)) {
            invoke3 = null;
        }
        return (String) invoke3;
    }
}
