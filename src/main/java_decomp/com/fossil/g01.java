package com.fossil;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G01 extends RecyclerView.ViewHolder {
    @DexIgnore
    public G01(FrameLayout frameLayout) {
        super(frameLayout);
    }

    @DexIgnore
    public static G01 a(ViewGroup viewGroup) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        frameLayout.setId(Mo0.j());
        frameLayout.setSaveEnabled(false);
        return new G01(frameLayout);
    }

    @DexIgnore
    public FrameLayout b() {
        return (FrameLayout) this.itemView;
    }
}
