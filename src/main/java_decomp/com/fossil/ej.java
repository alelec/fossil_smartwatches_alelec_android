package com.fossil;

import com.fossil.Ix1;
import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ej extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Nm b;
    @DexIgnore
    public /* final */ /* synthetic */ short c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ej(Nm nm, short s) {
        super(1);
        this.b = nm;
        this.c = (short) s;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        byte[] bArr = ((Vv) fs).Q;
        if (bArr.length == 0) {
            Nm nm = this.b;
            nm.l(Nr.a(nm.v, null, Zq.o, null, null, 13));
        } else {
            Nm nm2 = this.b;
            nm2.G.add(new J0(nm2.w.x, new Kb(this.c).b, new Kb(this.c).c, bArr, (long) bArr.length, Ix1.a.b(bArr, Ix1.Ai.CRC32), this.b.A(), true));
            Nm.J(this.b);
        }
        return Cd6.a;
    }
}
