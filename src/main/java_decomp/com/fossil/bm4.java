package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bm4 implements Cloneable {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int[] e;

    @DexIgnore
    public Bm4(int i) {
        this(i, i);
    }

    @DexIgnore
    public Bm4(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.b = i;
        this.c = i2;
        int i3 = (i + 31) / 32;
        this.d = i3;
        this.e = new int[(i3 * i2)];
    }

    @DexIgnore
    public Bm4(int i, int i2, int i3, int[] iArr) {
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = iArr;
    }

    @DexIgnore
    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return g();
    }

    @DexIgnore
    public final String d(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(this.c * (this.b + 1));
        for (int i = 0; i < this.c; i++) {
            for (int i2 = 0; i2 < this.b; i2++) {
                sb.append(i(i2, i) ? str : str2);
            }
            sb.append(str3);
        }
        return sb.toString();
    }

    @DexIgnore
    public void e() {
        int length = this.e.length;
        for (int i = 0; i < length; i++) {
            this.e[i] = 0;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Bm4)) {
            return false;
        }
        Bm4 bm4 = (Bm4) obj;
        return this.b == bm4.b && this.c == bm4.c && this.d == bm4.d && Arrays.equals(this.e, bm4.e);
    }

    @DexIgnore
    public Bm4 g() {
        return new Bm4(this.b, this.c, this.d, (int[]) this.e.clone());
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        return ((((((i + (i * 31)) * 31) + this.c) * 31) + this.d) * 31) + Arrays.hashCode(this.e);
    }

    @DexIgnore
    public boolean i(int i, int i2) {
        return ((this.e[(this.d * i2) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    @DexIgnore
    public int j() {
        return this.c;
    }

    @DexIgnore
    public int l() {
        return this.b;
    }

    @DexIgnore
    public void n(int i, int i2) {
        int i3 = (this.d * i2) + (i / 32);
        int[] iArr = this.e;
        iArr[i3] = (1 << (i & 31)) | iArr[i3];
    }

    @DexIgnore
    public void o(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i3 + i;
            int i6 = i4 + i2;
            if (i6 > this.c || i5 > this.b) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = this.d;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.e;
                    int i9 = (i8 / 32) + (i7 * i2);
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    @DexIgnore
    public String p(String str, String str2) {
        return d(str, str2, "\n");
    }

    @DexIgnore
    public String toString() {
        return p("X ", "  ");
    }
}
