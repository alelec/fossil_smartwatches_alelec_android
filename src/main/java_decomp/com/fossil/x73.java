package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X73 implements Y73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.sdk.screen.manual_screen_view_logging", true);
        b = hw2.d("measurement.sdk.screen.disabling_automatic_reporting", true);
    }
    */

    @DexIgnore
    @Override // com.fossil.Y73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Y73
    public final boolean zzb() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.Y73
    public final boolean zzc() {
        return b.o().booleanValue();
    }
}
