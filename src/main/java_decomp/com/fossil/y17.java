package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.au5;
import com.fossil.cu5;
import com.fossil.f17;
import com.fossil.fu5;
import com.fossil.gu5;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.MFDeviceService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y17 extends u17 {
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<f17.d> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<f17.d> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Runnable j;
    @DexIgnore
    public /* final */ e k;
    @DexIgnore
    public /* final */ d l;
    @DexIgnore
    public /* final */ ct0 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ on5 o;
    @DexIgnore
    public /* final */ v17 p;
    @DexIgnore
    public /* final */ cu5 q;
    @DexIgnore
    public /* final */ fu5 r;
    @DexIgnore
    public /* final */ au5 s;
    @DexIgnore
    public /* final */ gu5 t;
    @DexIgnore
    public /* final */ PortfolioApp u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<cu5.c, cu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4228a;

        @DexIgnore
        public a(y17 y17) {
            this.f4228a = y17;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(cu5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onError");
            this.f4228a.p.r6();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(cu5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onSuccess");
            this.f4228a.C(cVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<au5.c, au5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4229a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(y17 y17) {
            this.f4229a = y17;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(au5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName onError");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(au5.c cVar) {
            pq7.c(cVar, "responseValue");
            String a2 = cVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "GetCityName onSuccess - address: " + a2);
            this.f4229a.p.L2(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.e<fu5.c, fu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4230a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(y17 y17) {
            this.f4230a = y17;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(fu5.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onError");
            this.f4230a.y();
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(fu5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onSuccess");
            if (pq7.a(this.f4230a.B(), cVar.a().getDeviceSerial())) {
                this.f4230a.C(cVar.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4231a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(y17 y17) {
            this.f4231a = y17;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            DeviceLocation deviceLocation = (DeviceLocation) intent.getSerializableExtra("device_location");
            String stringExtra = intent.getStringExtra("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "onReceive - location: " + deviceLocation + ",serial: " + stringExtra);
            if (pq7.a(stringExtra, this.f4231a.B()) && deviceLocation != null) {
                this.f4231a.C(deviceLocation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4232a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(y17 y17) {
            this.f4232a = y17;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            pq7.c(context, "context");
            pq7.c(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (pq7.a(stringExtra, this.f4232a.B()) && pq7.a(stringExtra, this.f4232a.z())) {
                this.f4232a.A().l(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4233a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1", f = "FindDevicePresenter.kt", l = {66, 69}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.y17$f$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1", f = "FindDevicePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.y17$f$a$a  reason: collision with other inner class name */
            public static final class C0292a extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0292a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0292a aVar = new C0292a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
                    throw null;
                    //return ((C0292a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        DeviceRepository deviceRepository = this.this$0.this$0.f4233a.n;
                        String str = this.this$0.$serial;
                        pq7.b(str, "serial");
                        return deviceRepository.getDeviceBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(qn7 qn7, a aVar) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(qn7, this.this$0);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        DeviceRepository deviceRepository = this.this$0.this$0.f4233a.n;
                        String str = this.this$0.$serial;
                        pq7.b(str, "serial");
                        return deviceRepository.getDeviceNameBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$serial, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r11) {
                /*
                // Method dump skipped, instructions count: 238
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.y17.f.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public f(y17 y17) {
            this.f4233a = y17;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<f17.d> apply(String str) {
            this.f4233a.j.run();
            xw7 unused = gu7.d(this.f4233a.k(), null, null, new a(this, str, null), 3, null);
            return this.f4233a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ y17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements iq4.e<gu5.e, gu5.b> {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ g f4234a;

            @DexIgnore
            public a(g gVar) {
                this.f4234a = gVar;
            }

            @DexIgnore
            /* renamed from: b */
            public void a(gu5.b bVar) {
                pq7.c(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "getRssi onError");
                this.f4234a.b.G(-9999);
                this.f4234a.b.J(-9999);
            }

            @DexIgnore
            /* renamed from: c */
            public void onSuccess(gu5.e eVar) {
                pq7.c(eVar, "responseValue");
                int a2 = eVar.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDevicePresenter", "getRssi onSuccess - rssi: " + a2);
                this.f4234a.b.G(a2);
                this.f4234a.b.J(a2);
            }
        }

        @DexIgnore
        public g(y17 y17) {
            this.b = y17;
        }

        @DexIgnore
        public final void run() {
            String B = this.b.B();
            if (B != null) {
                this.b.t.e(new gu5.d(B), new a(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ls0<f17.d> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ y17 f4235a;

        @DexIgnore
        public h(y17 y17) {
            this.f4235a = y17;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(f17.d dVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f17.C.a();
            local.d(a2, "observer device " + dVar);
            if (dVar != null) {
                this.f4235a.p.w3(dVar);
            }
        }
    }

    @DexIgnore
    public y17(ct0 ct0, DeviceRepository deviceRepository, on5 on5, v17 v17, cu5 cu5, fu5 fu5, au5 au5, gu5 gu5, PortfolioApp portfolioApp) {
        pq7.c(ct0, "mLocalBroadcastManager");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(v17, "mView");
        pq7.c(cu5, "mGetLocation");
        pq7.c(fu5, "mLoadLocation");
        pq7.c(au5, "mGetAddress");
        pq7.c(gu5, "mGetRssi");
        pq7.c(portfolioApp, "mApp");
        this.m = ct0;
        this.n = deviceRepository;
        this.o = on5;
        this.p = v17;
        this.q = cu5;
        this.r = fu5;
        this.s = au5;
        this.t = gu5;
        this.u = portfolioApp;
        LiveData<f17.d> c2 = ss0.c(this.e, new f(this));
        pq7.b(c2, "Transformations.switchMa\u2026viceWrapperLiveData\n    }");
        this.g = c2;
        this.i = new Handler();
        this.j = new g(this);
        this.k = new e(this);
        this.l = new d(this);
    }

    @DexIgnore
    public final MutableLiveData<String> A() {
        return this.e;
    }

    @DexIgnore
    public String B() {
        return this.e.e();
    }

    @DexIgnore
    public final void C(DeviceLocation deviceLocation) {
        pq7.c(deviceLocation, PlaceFields.LOCATION);
        this.p.I2(deviceLocation.getTimeStamp());
        double latitude = deviceLocation.getLatitude();
        double longitude = deviceLocation.getLongitude();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "handleLocation latitude=" + latitude + ", longitude=" + longitude);
        if (latitude != 0.0d && longitude != 0.0d) {
            this.p.E1(Double.valueOf(latitude), Double.valueOf(longitude));
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName");
            this.s.e(new au5.a(latitude, longitude), new b(this));
        }
    }

    @DexIgnore
    public final boolean D(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0;
    }

    @DexIgnore
    public final void E() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation");
        this.r.e(new fu5.a(), new c(this));
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "locateOnMap");
        if (!NetworkUtils.isNetworkAvailable(this.u)) {
            this.p.o(601, null);
            return;
        }
        jn5 jn5 = jn5.b;
        v17 v17 = this.p;
        if (v17 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment");
        } else if (!jn5.c(jn5, ((b17) v17).getContext(), jn5.a.FIND_DEVICE, false, false, false, null, 60, null)) {
            this.p.j1();
        } else {
            String B = B();
            if (B != null) {
                if (TextUtils.equals(B, z()) && x(B)) {
                    Context applicationContext = this.u.getApplicationContext();
                    pq7.b(applicationContext, "mApp.applicationContext");
                    if (D(applicationContext)) {
                        E();
                        return;
                    }
                }
                y();
            }
        }
    }

    @DexIgnore
    public final void G(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void H(String str) {
        pq7.c(str, "serial");
        this.e.l(str);
    }

    @DexIgnore
    public void I() {
        this.p.M5(this);
    }

    @DexIgnore
    public final void J(int i2) {
        if (i2 != -9999) {
            this.p.W0(i2);
        }
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, v);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        this.m.c(this.l, new IntentFilter(MFDeviceService.a0.a()));
        PortfolioApp portfolioApp = this.u;
        e eVar = this.k;
        portfolioApp.registerReceiver(eVar, new IntentFilter(this.u.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<f17.d> liveData = this.g;
        v17 v17 = this.p;
        if (v17 != null) {
            liveData.h((pv5) v17, new h(this));
            this.t.p();
            if (pq7.a(this.u.J(), this.e.e())) {
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "update RSSI only for active device");
                J(this.h);
                return;
            }
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        try {
            MutableLiveData<f17.d> mutableLiveData = this.f;
            v17 v17 = this.p;
            if (v17 != null) {
                mutableLiveData.n((pv5) v17);
                this.e.n((LifecycleOwner) this.p);
                this.g.n((LifecycleOwner) this.p);
                this.m.e(this.l);
                this.u.unregisterReceiver(this.k);
                this.t.s();
                this.i.removeCallbacksAndMessages(null);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("FindDevicePresenter", "stop with " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.u17
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "enableLocate: enable = " + z);
        o(z);
        this.p.f3(z, this.o.d0());
        if (z) {
            F();
        }
    }

    @DexIgnore
    @Override // com.fossil.u17
    public void o(boolean z) {
        this.o.a1(z);
    }

    @DexIgnore
    public final boolean x(String str) {
        IButtonConnectivity b2 = PortfolioApp.h0.b();
        return b2 != null && b2.getGattState(str) == 2;
    }

    @DexIgnore
    public final void y() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation");
        String B = B();
        if (B != null) {
            this.q.e(new cu5.a(B), new a(this));
        }
    }

    @DexIgnore
    public final String z() {
        return PortfolioApp.h0.c().J();
    }
}
