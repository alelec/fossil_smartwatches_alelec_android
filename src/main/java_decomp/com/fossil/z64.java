package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Z64 implements D74 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public Z64(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static D74 b(Object obj) {
        return new Z64(obj);
    }

    @DexIgnore
    @Override // com.fossil.D74
    public Object a(B74 b74) {
        Object obj = this.a;
        A74.l(obj, b74);
        return obj;
    }
}
