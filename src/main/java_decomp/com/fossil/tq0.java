package com.fossil;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tq0 extends E01 {
    @DexIgnore
    public /* final */ FragmentManager b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public Xq0 d;
    @DexIgnore
    public Fragment e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    @Deprecated
    public Tq0(FragmentManager fragmentManager) {
        this(fragmentManager, 0);
    }

    @DexIgnore
    public Tq0(FragmentManager fragmentManager, int i) {
        this.d = null;
        this.e = null;
        this.b = fragmentManager;
        this.c = i;
    }

    @DexIgnore
    public static String w(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }

    @DexIgnore
    @Override // com.fossil.E01
    public void b(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.d == null) {
            this.d = this.b.j();
        }
        this.d.l(fragment);
        if (fragment.equals(this.e)) {
            this.e = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.E01
    public void d(ViewGroup viewGroup) {
        Xq0 xq0 = this.d;
        if (xq0 != null) {
            if (!this.f) {
                try {
                    this.f = true;
                    xq0.k();
                    this.f = false;
                } catch (Throwable th) {
                    this.f = false;
                    throw th;
                }
            }
            this.d = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.E01
    public Object j(ViewGroup viewGroup, int i) {
        if (this.d == null) {
            this.d = this.b.j();
        }
        long v = v(i);
        Fragment Z = this.b.Z(w(viewGroup.getId(), v));
        if (Z != null) {
            this.d.g(Z);
        } else {
            Z = u(i);
            this.d.b(viewGroup.getId(), Z, w(viewGroup.getId(), v));
        }
        if (Z != this.e) {
            Z.setMenuVisibility(false);
            if (this.c == 1) {
                this.d.t(Z, Lifecycle.State.STARTED);
            } else {
                Z.setUserVisibleHint(false);
            }
        }
        return Z;
    }

    @DexIgnore
    @Override // com.fossil.E01
    public boolean k(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    @DexIgnore
    @Override // com.fossil.E01
    public void m(Parcelable parcelable, ClassLoader classLoader) {
    }

    @DexIgnore
    @Override // com.fossil.E01
    public Parcelable n() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.E01
    public void p(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        Fragment fragment2 = this.e;
        if (fragment != fragment2) {
            if (fragment2 != null) {
                fragment2.setMenuVisibility(false);
                if (this.c == 1) {
                    if (this.d == null) {
                        this.d = this.b.j();
                    }
                    this.d.t(this.e, Lifecycle.State.STARTED);
                } else {
                    this.e.setUserVisibleHint(false);
                }
            }
            fragment.setMenuVisibility(true);
            if (this.c == 1) {
                if (this.d == null) {
                    this.d = this.b.j();
                }
                this.d.t(fragment, Lifecycle.State.RESUMED);
            } else {
                fragment.setUserVisibleHint(true);
            }
            this.e = fragment;
        }
    }

    @DexIgnore
    @Override // com.fossil.E01
    public void s(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    @DexIgnore
    public abstract Fragment u(int i);

    @DexIgnore
    public long v(int i) {
        return (long) i;
    }
}
