package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P97 extends Hq4 {
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<List<Object>, Integer>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Fb7> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Object> k; // = new MutableLiveData<>();
    @DexIgnore
    public List<Object> l; // = new ArrayList();
    @DexIgnore
    public String m; // = "";
    @DexIgnore
    public String n; // = "";
    @DexIgnore
    public /* final */ U08 o; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(P97 p97, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = p97;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$id, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00ba  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x019d  */
        /* JADX WARNING: Removed duplicated region for block: B:75:0x0201  */
        /* JADX WARNING: Removed duplicated region for block: B:79:0x022a  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 563
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.P97.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(P97 p97, Xe6 xe6) {
            super(xe6);
            this.this$0 = p97;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r2 = 0
                    r3 = 1
                    r4 = 2
                    java.lang.Object r6 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003f
                    if (r0 == r3) goto L_0x0021
                    if (r0 != r4) goto L_0x0019
                    java.lang.Object r0 = r7.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r8)
                L_0x0016:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0018:
                    return r0
                L_0x0019:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0021:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.P97 r0 = (com.fossil.P97) r0
                    java.lang.Object r1 = r7.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r8)
                    r3 = r8
                    r5 = r1
                L_0x002e:
                    r1 = r3
                    java.util.List r1 = (java.util.List) r1
                    r7.L$0 = r5
                    r7.label = r4
                    r3 = r7
                    r5 = r2
                    java.lang.Object r0 = com.fossil.P97.H(r0, r1, r2, r3, r4, r5)
                    if (r0 != r6) goto L_0x0016
                    r0 = r6
                    goto L_0x0018
                L_0x003f:
                    com.fossil.El7.b(r8)
                    com.mapped.Il6 r1 = r7.p$
                    com.fossil.P97$Ci r0 = r7.this$0
                    com.fossil.P97 r0 = r0.this$0
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r3
                    java.lang.Object r3 = r0.v(r7)
                    if (r3 != r6) goto L_0x0056
                    r0 = r6
                    goto L_0x0018
                L_0x0056:
                    r5 = r1
                    goto L_0x002e
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.P97.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(P97 p97, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = p97;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $newBackgroundId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(P97 p97, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = p97;
            this.$newBackgroundId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$newBackgroundId, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00ce  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00e9  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00ee  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00e7 A[SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 242
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.P97.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSet;
        @DexIgnore
        public /* final */ /* synthetic */ String $wfId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(P97 p97, boolean z, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = p97;
            this.$isSet = z;
            this.$wfId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$isSet, this.$wfId, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$isSet) {
                    this.this$0.n = this.$wfId;
                }
                P97 p97 = this.this$0;
                String str = this.$wfId;
                this.L$0 = il6;
                this.label = 1;
                if (P97.H(p97, null, str, this, 1, null) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ P97 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(P97 p97, Xe6 xe6) {
            super(xe6);
            this.this$0 = p97;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(null, null, this);
        }
    }

    @DexIgnore
    public P97(WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        Wg6.c(wFBackgroundPhotoRepository, "photoRepository");
        this.p = wFBackgroundPhotoRepository;
        String simpleName = P97.class.getSimpleName();
        Wg6.b(simpleName, "WatchFacePhotoBackground\u2026el::class.java.simpleName");
        this.h = simpleName;
    }

    @DexIgnore
    public static /* synthetic */ void F(P97 p97, String str, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        p97.E(str, z);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.P97 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Object H(P97 p97, List list, String str, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            list = null;
        }
        if ((i2 & 2) != 0) {
            str = null;
        }
        return p97.G(list, str, xe6);
    }

    @DexIgnore
    public final void A(String str) {
        Wg6.c(str, "newBackgroundId");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Di(this, str, null), 2, null);
    }

    @DexIgnore
    public final void B(Fb7 fb7, int i2) {
        Wg6.c(fb7, "backgroundPhoto");
        this.j.l(fb7);
    }

    @DexIgnore
    public final void C(String str) {
        if (!Wg6.a(this.m, str)) {
            this.m = str;
        }
    }

    @DexIgnore
    public final void D(List<Object> list) {
        if (!Wg6.a(this.l, list)) {
            this.l = list;
        }
    }

    @DexIgnore
    public final void E(String str, boolean z) {
        Wg6.c(str, "wfId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, z, str, null), 3, null);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e A[Catch:{ all -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f A[Catch:{ all -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00bb A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object G(java.util.List<java.lang.Object> r10, java.lang.String r11, com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
            r9 = this;
            r4 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 0
            r5 = 1
            boolean r0 = r12 instanceof com.fossil.P97.Fi
            if (r0 == 0) goto L_0x0088
            r0 = r12
            com.fossil.P97$Fi r0 = (com.fossil.P97.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0088
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0016:
            java.lang.Object r6 = r3.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r1 = r3.label
            if (r1 == 0) goto L_0x0097
            if (r1 != r5) goto L_0x008f
            java.lang.Object r0 = r3.L$3
            com.fossil.U08 r0 = (com.fossil.U08) r0
            java.lang.Object r1 = r3.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r3.L$1
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r3.L$0
            com.fossil.P97 r3 = (com.fossil.P97) r3
            com.fossil.El7.b(r6)
            r6 = r0
        L_0x0036:
            if (r2 == 0) goto L_0x00b0
            r0 = r2
        L_0x0039:
            r3.D(r0)     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x00b3
            r0 = r1
        L_0x003f:
            r3.C(r0)     // Catch:{ all -> 0x00bd }
            java.util.List<java.lang.Object> r0 = r3.l     // Catch:{ all -> 0x00bd }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x00bd }
            r1 = r4
        L_0x0049:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x00bb
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x00bd }
            boolean r7 = r0 instanceof com.fossil.Fb7     // Catch:{ all -> 0x00bd }
            if (r7 == 0) goto L_0x00b6
            com.fossil.Fb7 r0 = (com.fossil.Fb7) r0     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = r0.b()     // Catch:{ all -> 0x00bd }
            java.lang.String r7 = r3.m     // Catch:{ all -> 0x00bd }
            boolean r0 = com.mapped.Wg6.a(r0, r7)     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x00b6
            r0 = r5
        L_0x0066:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)     // Catch:{ all -> 0x00bd }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x00bd }
            if (r0 == 0) goto L_0x00b8
            r0 = r1
        L_0x0071:
            androidx.lifecycle.MutableLiveData<com.mapped.Lc6<java.util.List<java.lang.Object>, java.lang.Integer>> r1 = r3.i     // Catch:{ all -> 0x00bd }
            java.util.List<java.lang.Object> r2 = r3.l     // Catch:{ all -> 0x00bd }
            java.lang.Integer r0 = com.fossil.Ao7.e(r0)     // Catch:{ all -> 0x00bd }
            com.mapped.Lc6 r0 = com.fossil.Hl7.a(r2, r0)     // Catch:{ all -> 0x00bd }
            r1.l(r0)     // Catch:{ all -> 0x00bd }
            com.mapped.Cd6 r0 = com.mapped.Cd6.a     // Catch:{ all -> 0x00bd }
            r6.b(r8)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0087:
            return r0
        L_0x0088:
            com.fossil.P97$Fi r0 = new com.fossil.P97$Fi
            r0.<init>(r9, r12)
            r3 = r0
            goto L_0x0016
        L_0x008f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0097:
            com.fossil.El7.b(r6)
            com.fossil.U08 r6 = r9.o
            r3.L$0 = r9
            r3.L$1 = r10
            r3.L$2 = r11
            r3.L$3 = r6
            r3.label = r5
            java.lang.Object r1 = r6.a(r8, r3)
            if (r1 == r0) goto L_0x0087
            r3 = r9
            r1 = r11
            r2 = r10
            goto L_0x0036
        L_0x00b0:
            java.util.List<java.lang.Object> r0 = r3.l
            goto L_0x0039
        L_0x00b3:
            java.lang.String r0 = r3.m
            goto L_0x003f
        L_0x00b6:
            r0 = r4
            goto L_0x0066
        L_0x00b8:
            int r1 = r1 + 1
            goto L_0x0049
        L_0x00bb:
            r0 = -1
            goto L_0x0071
        L_0x00bd:
            r0 = move-exception
            r6.b(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.P97.G(java.util.List, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "id");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ai(this, str, null), 2, null);
    }

    @DexIgnore
    public final LiveData<Object> u() {
        return this.k;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object v(com.mapped.Xe6<? super java.util.List<java.lang.Object>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.P97.Bi
            if (r0 == 0) goto L_0x0040
            r0 = r6
            com.fossil.P97$Bi r0 = (com.fossil.P97.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0040
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x004e
            if (r3 != r4) goto L_0x0046
            java.lang.Object r0 = r0.L$0
            com.fossil.P97 r0 = (com.fossil.P97) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            java.util.List r0 = (java.util.List) r0
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r2 = ""
            r1.add(r2)
            java.util.List r0 = com.fossil.Pm7.X(r0)
            java.util.List r0 = com.fossil.D97.d(r0)
            r1.addAll(r0)
            r0 = r1
        L_0x003f:
            return r0
        L_0x0040:
            com.fossil.P97$Bi r0 = new com.fossil.P97$Bi
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.El7.b(r1)
            com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository r1 = r5.p
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.d(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.P97.v(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<Lc6<List<Object>, Integer>> w() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Fb7> x() {
        return this.j;
    }

    @DexIgnore
    public final String y() {
        return this.n;
    }

    @DexIgnore
    public final void z() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, null), 3, null);
    }
}
