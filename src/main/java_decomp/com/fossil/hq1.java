package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hq1 extends Lq1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Bv1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hq1 createFromParcel(Parcel parcel) {
            return new Hq1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hq1[] newArray(int i) {
            return new Hq1[i];
        }
    }

    @DexIgnore
    public Hq1(byte b, Bv1 bv1) {
        super(E90.COMMUTE_TIME_TRAVEL_MICRO_APP, b, bv1);
        this.f = bv1;
    }

    @DexIgnore
    public /* synthetic */ Hq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(Bv1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (Bv1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Lq1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Hq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.f, ((Hq1) obj).f) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest");
    }

    @DexIgnore
    public final Bv1 getCommuteTimeTravelMicroAppEvent() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Lq1
    public int hashCode() {
        return (super.hashCode() * 31) + this.f.hashCode();
    }
}
