package com.fossil;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pm2 extends FilterInputStream {
    @DexIgnore
    public long b;
    @DexIgnore
    public long c; // = -1;

    @DexIgnore
    public Pm2(InputStream inputStream, long j) {
        super(inputStream);
        Km2.a(inputStream);
        this.b = 1048577;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int available() throws IOException {
        return (int) Math.min((long) ((FilterInputStream) this).in.available(), this.b);
    }

    @DexIgnore
    public final void mark(int i) {
        synchronized (this) {
            ((FilterInputStream) this).in.mark(i);
            this.c = this.b;
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read() throws IOException {
        if (this.b == 0) {
            return -1;
        }
        int read = ((FilterInputStream) this).in.read();
        if (read != -1) {
            this.b--;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.b;
        if (j == 0) {
            return -1;
        }
        int read = ((FilterInputStream) this).in.read(bArr, i, (int) Math.min((long) i2, j));
        if (read != -1) {
            this.b -= (long) read;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public final void reset() throws IOException {
        synchronized (this) {
            if (!((FilterInputStream) this).in.markSupported()) {
                throw new IOException("Mark not supported");
            } else if (this.c != -1) {
                ((FilterInputStream) this).in.reset();
                this.b = this.c;
            } else {
                throw new IOException("Mark not set");
            }
        }
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public final long skip(long j) throws IOException {
        long skip = ((FilterInputStream) this).in.skip(Math.min(j, this.b));
        this.b -= skip;
        return skip;
    }
}
