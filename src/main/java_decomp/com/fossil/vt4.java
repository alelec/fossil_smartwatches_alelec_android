package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vt4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3819a;
    @DexIgnore
    public /* final */ String b; // = "deviceToken";
    @DexIgnore
    public /* final */ on5 c;
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {30}, m = "registerFCM")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vt4 vt4, qn7 qn7) {
            super(qn7);
            this.this$0 = vt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$registerFCM$response$1", f = "FCMRepository.kt", l = {30}, m = "invokeSuspend")
    public static final class b extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ vt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vt4 vt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = vt4;
            this.$body = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new b(this.this$0, this.$body, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            throw null;
            //return ((b) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.d;
                gj4 gj4 = this.$body;
                this.label = 1;
                Object pushFCMToken = apiServiceV2.pushFCMToken(gj4, this);
                return pushFCMToken == d ? d : pushFCMToken;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {52}, m = "retry")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vt4 vt4, qn7 qn7) {
            super(qn7);
            this.this$0 = vt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {72}, m = "unregisterFCM")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(vt4 vt4, qn7 qn7) {
            super(qn7);
            this.this$0 = vt4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$unregisterFCM$response$1", f = "FCMRepository.kt", l = {72}, m = "invokeSuspend")
    public static final class e extends ko7 implements rp7<qn7<? super q88<gj4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ gj4 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ vt4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(vt4 vt4, gj4 gj4, qn7 qn7) {
            super(1, qn7);
            this.this$0 = vt4;
            this.$body = gj4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new e(this.this$0, this.$body, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<gj4>> qn7) {
            throw null;
            //return ((e) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.d;
                gj4 gj4 = this.$body;
                this.label = 1;
                Object removeFCMToken = apiServiceV2.removeFCMToken(gj4, this);
                return removeFCMToken == d ? d : removeFCMToken;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public vt4(on5 on5, ApiServiceV2 apiServiceV2) {
        pq7.c(on5, "shared");
        pq7.c(apiServiceV2, "api");
        this.c = on5;
        this.d = apiServiceV2;
        String simpleName = vt4.class.getSimpleName();
        pq7.b(simpleName, "FCMRepository::class.java.simpleName");
        this.f3819a = simpleName;
    }

    @DexIgnore
    public final gj4 b(String str) {
        gj4 gj4 = new gj4();
        gj4.n(this.b, str);
        return gj4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r10, com.fossil.qn7<? super com.fossil.kz4<java.lang.Boolean>> r11) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt4.c(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.qn7<? super com.fossil.kz4<java.lang.Boolean>> r12) {
        /*
            r11 = this;
            r10 = 0
            r4 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r12 instanceof com.fossil.vt4.c
            if (r0 == 0) goto L_0x0035
            r0 = r12
            com.fossil.vt4$c r0 = (com.fossil.vt4.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0035
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0044
            if (r0 != r5) goto L_0x003c
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.vt4 r0 = (com.fossil.vt4) r0
            com.fossil.el7.b(r2)
            r0 = r2
        L_0x0032:
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
        L_0x0034:
            return r0
        L_0x0035:
            com.fossil.vt4$c r0 = new com.fossil.vt4$c
            r0.<init>(r11, r12)
            r1 = r0
            goto L_0x0016
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.el7.b(r2)
            com.fossil.on5 r0 = r11.c
            java.lang.String r0 = r0.p()
            com.fossil.on5 r2 = r11.c
            java.lang.String r2 = r2.i()
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r11.f3819a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "retry - consider - deviceToken: "
            r8.append(r9)
            r8.append(r0)
            java.lang.String r9 = " - authorisedToken: "
            r8.append(r9)
            r8.append(r2)
            java.lang.String r8 = r8.toString()
            r6.e(r7, r8)
            boolean r6 = com.fossil.pq7.a(r0, r2)
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x009c
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r6 = r11.f3819a
            java.lang.String r7 = "do retry"
            r4.e(r6, r7)
            r1.L$0 = r11
            r1.L$1 = r0
            r1.L$2 = r2
            r1.label = r5
            java.lang.Object r0 = r11.c(r0, r1)
            if (r0 != r3) goto L_0x0032
            r0 = r3
            goto L_0x0034
        L_0x009c:
            if (r0 == 0) goto L_0x00a4
            int r0 = r0.length()
            if (r0 != 0) goto L_0x00b4
        L_0x00a4:
            r0 = r5
        L_0x00a5:
            if (r0 == 0) goto L_0x00b6
            com.fossil.kz4 r0 = new com.fossil.kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            java.lang.String r2 = "deviceToken is empty"
            r1.<init>(r4, r2)
            r0.<init>(r1)
            goto L_0x0034
        L_0x00b4:
            r0 = r4
            goto L_0x00a5
        L_0x00b6:
            com.fossil.kz4 r0 = new com.fossil.kz4
            java.lang.Boolean r1 = com.fossil.ao7.a(r5)
            r2 = 2
            r0.<init>(r1, r10, r2, r10)
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt4.d(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.qn7<? super com.fossil.kz4<java.lang.Boolean>> r11) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vt4.e(com.fossil.qn7):java.lang.Object");
    }
}
