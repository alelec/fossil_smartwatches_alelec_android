package com.fossil;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Se1<Data> implements Af1<File, Data> {
    @DexIgnore
    public /* final */ Di<Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<Data> implements Bf1<File, Data> {
        @DexIgnore
        public /* final */ Di<Data> a;

        @DexIgnore
        public Ai(Di<Data> di) {
            this.a = di;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public final Af1<File, Data> b(Ef1 ef1) {
            return new Se1(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Ai<ParcelFileDescriptor> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Di<ParcelFileDescriptor> {
            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Se1.Di
            public /* bridge */ /* synthetic */ void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                c(parcelFileDescriptor);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Se1.Di
            public /* bridge */ /* synthetic */ ParcelFileDescriptor b(File file) throws FileNotFoundException {
                return d(file);
            }

            @DexIgnore
            public void c(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }

            @DexIgnore
            public ParcelFileDescriptor d(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, SQLiteDatabase.CREATE_IF_NECESSARY);
            }

            @DexIgnore
            @Override // com.fossil.Se1.Di
            public Class<ParcelFileDescriptor> getDataClass() {
                return ParcelFileDescriptor.class;
            }
        }

        @DexIgnore
        public Bi() {
            super(new Aii());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<Data> implements Wb1<Data> {
        @DexIgnore
        public /* final */ File b;
        @DexIgnore
        public /* final */ Di<Data> c;
        @DexIgnore
        public Data d;

        @DexIgnore
        public Ci(File file, Di<Data> di) {
            this.b = file;
            this.c = di;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
            Data data = this.d;
            if (data != null) {
                try {
                    this.c.a(data);
                } catch (IOException e) {
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super Data> ai) {
            try {
                Data b2 = this.c.b(this.b);
                this.d = b2;
                ai.e(b2);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                ai.b(e);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<Data> getDataClass() {
            return this.c.getDataClass();
        }
    }

    @DexIgnore
    public interface Di<Data> {
        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Data b(File file) throws FileNotFoundException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Ai<InputStream> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Di<InputStream> {
            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Se1.Di
            public /* bridge */ /* synthetic */ void a(InputStream inputStream) throws IOException {
                c(inputStream);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // com.fossil.Se1.Di
            public /* bridge */ /* synthetic */ InputStream b(File file) throws FileNotFoundException {
                return d(file);
            }

            @DexIgnore
            public void c(InputStream inputStream) throws IOException {
                inputStream.close();
            }

            @DexIgnore
            public InputStream d(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }

            @DexIgnore
            @Override // com.fossil.Se1.Di
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }
        }

        @DexIgnore
        public Ei() {
            super(new Aii());
        }
    }

    @DexIgnore
    public Se1(Di<Data> di) {
        this.a = di;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(File file) {
        return d(file);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(File file, int i, int i2, Ob1 ob1) {
        return c(file, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(File file, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(file), new Ci(file, this.a));
    }

    @DexIgnore
    public boolean d(File file) {
        return true;
    }
}
