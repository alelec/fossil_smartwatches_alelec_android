package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Ni5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e;

    /*
    static {
        int[] iArr = new int[Gi5.values().length];
        a = iArr;
        iArr[Gi5.INDOOR.ordinal()] = 1;
        a[Gi5.OUTDOOR.ordinal()] = 2;
        int[] iArr2 = new int[Mi5.values().length];
        b = iArr2;
        iArr2[Mi5.UNKNOWN.ordinal()] = 1;
        b[Mi5.RUNNING.ordinal()] = 2;
        b[Mi5.CYCLING.ordinal()] = 3;
        b[Mi5.SPINNING.ordinal()] = 4;
        b[Mi5.TREADMILL.ordinal()] = 5;
        b[Mi5.ELLIPTICAL.ordinal()] = 6;
        b[Mi5.WEIGHTS.ordinal()] = 7;
        b[Mi5.WORKOUT.ordinal()] = 8;
        b[Mi5.YOGA.ordinal()] = 9;
        b[Mi5.WALKING.ordinal()] = 10;
        b[Mi5.ROWING.ordinal()] = 11;
        b[Mi5.SWIMMING.ordinal()] = 12;
        b[Mi5.AEROBIC.ordinal()] = 13;
        b[Mi5.HIKING.ordinal()] = 14;
        int[] iArr3 = new int[Oi5.values().length];
        c = iArr3;
        iArr3[Oi5.UNKNOWN.ordinal()] = 1;
        c[Oi5.RUNNING.ordinal()] = 2;
        c[Oi5.SPINNING.ordinal()] = 3;
        c[Oi5.OUTDOOR_CYCLING.ordinal()] = 4;
        c[Oi5.TREADMILL.ordinal()] = 5;
        c[Oi5.ELLIPTICAL.ordinal()] = 6;
        c[Oi5.WEIGHTS.ordinal()] = 7;
        c[Oi5.WORKOUT.ordinal()] = 8;
        c[Oi5.WALK.ordinal()] = 9;
        c[Oi5.ROW_MACHINE.ordinal()] = 10;
        c[Oi5.HIKING.ordinal()] = 11;
        int[] iArr4 = new int[Oi5.values().length];
        d = iArr4;
        iArr4[Oi5.UNKNOWN.ordinal()] = 1;
        d[Oi5.SPINNING.ordinal()] = 2;
        d[Oi5.OUTDOOR_CYCLING.ordinal()] = 3;
        d[Oi5.TREADMILL.ordinal()] = 4;
        d[Oi5.ELLIPTICAL.ordinal()] = 5;
        d[Oi5.WEIGHTS.ordinal()] = 6;
        d[Oi5.WORKOUT.ordinal()] = 7;
        d[Oi5.ROW_MACHINE.ordinal()] = 8;
        int[] iArr5 = new int[Oi5.values().length];
        e = iArr5;
        iArr5[Oi5.RUNNING.ordinal()] = 1;
        e[Oi5.SPINNING.ordinal()] = 2;
        e[Oi5.OUTDOOR_CYCLING.ordinal()] = 3;
        e[Oi5.TREADMILL.ordinal()] = 4;
        e[Oi5.ELLIPTICAL.ordinal()] = 5;
        e[Oi5.WEIGHTS.ordinal()] = 6;
        e[Oi5.WORKOUT.ordinal()] = 7;
        e[Oi5.WALK.ordinal()] = 8;
        e[Oi5.ROW_MACHINE.ordinal()] = 9;
        e[Oi5.HIKING.ordinal()] = 10;
        e[Oi5.UNKNOWN.ordinal()] = 11;
    }
    */
}
