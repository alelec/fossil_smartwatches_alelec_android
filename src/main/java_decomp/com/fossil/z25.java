package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ Vd5 s;
    @DexIgnore
    public /* final */ Vd5 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public Z25(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, Vd5 vd5, Vd5 vd52, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = vd5;
        x(vd5);
        this.t = vd52;
        x(vd52);
        this.u = linearLayout;
    }
}
