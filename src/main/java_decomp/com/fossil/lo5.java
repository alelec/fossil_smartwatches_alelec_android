package com.fossil;

import android.text.TextUtils;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.source.FileRepository;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lo5 {
    @DexIgnore
    public static final Mo5 a(Mo5 mo5) {
        Wg6.c(mo5, "$this$clone");
        Mo5 mo52 = new Mo5(mo5.e(), mo5.a(), mo5.b(), mo5.d(), mo5.i(), mo5.m(), mo5.f(), mo5.j(), mo5.k(), mo5.g(), mo5.n());
        mo52.q(mo5.c());
        mo52.t(mo5.l());
        mo52.s(mo5.h());
        return mo52;
    }

    @DexIgnore
    public static final Mo5 b(No5 no5, String str, String str2, String str3) {
        Wg6.c(no5, "$this$toDianaPreset");
        Wg6.c(str, "presetId");
        Wg6.c(str2, "serial");
        Wg6.c(str3, ButtonService.USER_ID);
        List j0 = Pm7.j0(no5.a());
        String b = no5.b();
        if (b == null) {
            b = "";
        }
        return new Mo5(str, j0, b, "", null, no5.i(), no5.f(), str2, str3, null, true);
    }

    @DexIgnore
    public static final Jo5 c(Mo5 mo5, FileRepository fileRepository) {
        String str;
        byte[] b;
        Wg6.c(mo5, "$this$toDianaPresetDraft");
        Wg6.c(fileRepository, "fileRepository");
        File fileByFileName = fileRepository.getFileByFileName(mo5.e());
        if (fileByFileName == null || (b = J37.b(fileByFileName)) == null || (str = J37.a(b)) == null) {
            str = "";
        }
        return d(mo5, str);
    }

    @DexIgnore
    public static final Jo5 d(Mo5 mo5, String str) {
        Wg6.c(mo5, "$this$toDianaPresetDraft");
        Wg6.c(str, "base64Data");
        return new Jo5(mo5.e(), mo5.a(), mo5.f(), mo5.i(), str, mo5.j(), mo5.m(), mo5.g(), mo5.c(), mo5.l());
    }

    @DexIgnore
    public static final Jo5 e(No5 no5, String str, String str2, String str3) {
        String str4;
        Wg6.c(no5, "$this$toDianaPresetDraft");
        Wg6.c(str, "presetId");
        Wg6.c(str2, "base64Data");
        Wg6.c(str3, "serial");
        SimpleDateFormat simpleDateFormat = TimeUtils.p.get();
        if (simpleDateFormat == null || (str4 = simpleDateFormat.format(new Date())) == null) {
            str4 = "2016-01-01T01:01:01.001Z";
        }
        return new Jo5(str, no5.a(), no5.f(), null, str2, str3, no5.i(), null, str4, str4);
    }

    @DexIgnore
    public static final List<Jo5> f(List<Mo5> list, FileRepository fileRepository) {
        Wg6.c(list, "$this$toDianaPresetDrafts");
        Wg6.c(fileRepository, "fileRepository");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(c(it.next(), fileRepository));
        }
        return arrayList;
    }

    @DexIgnore
    public static final Ku3 g(List<Mo5> list) {
        Wg6.c(list, "$this$toJsonDeletedIds");
        Ku3 ku3 = new Ku3();
        Fu3 fu3 = new Fu3();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            fu3.l(it.next().e());
        }
        ku3.k("_ids", fu3);
        return ku3;
    }

    @DexIgnore
    public static final Ku3 h(Jo5 jo5) {
        Wg6.c(jo5, "$this$toJsonObject");
        Ku3 ku3 = new Ku3();
        Fu3 fu3 = new Fu3();
        List<Oo5> a2 = jo5.a();
        if (a2 != null) {
            for (T t : a2) {
                Ku3 ku32 = new Ku3();
                ku32.n("appId", t.a());
                ku32.n("buttonPosition", t.b());
                ku32.n("localUpdatedAt", t.c());
                fu3.k(ku32);
            }
        }
        ku3.k("buttons", fu3);
        ku3.l("isActive", Boolean.valueOf(jo5.i()));
        ku3.n("dianaFaceBase64Data", jo5.c());
        ku3.n("name", jo5.e());
        ku3.n("previewFaceUrl", jo5.g());
        ku3.n("serialNumber", jo5.h());
        ku3.n("id", jo5.d());
        if (!TextUtils.isEmpty(jo5.f())) {
            ku3.n("originalItemIdInStore", jo5.f());
        }
        return ku3;
    }

    @DexIgnore
    public static final Ku3 i(List<Jo5> list) {
        Wg6.c(list, "$this$toJsonObject");
        Ku3 ku3 = new Ku3();
        Fu3 fu3 = new Fu3();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            fu3.k(h(it.next()));
        }
        ku3.k(CloudLogWriter.ITEMS_PARAM, fu3);
        return ku3;
    }

    @DexIgnore
    public static final Object j(Mo5 mo5, boolean z, boolean z2, Ob7 ob7, List<Fb7> list, List<Fb7> list2, List<Fb7> list3, String str, Xe6<? super Dp5> xe6) {
        T t;
        T t2;
        T t3;
        byte[] a2;
        ArrayList arrayList = new ArrayList();
        if (ob7 == null) {
            return new Dp5(mo5.e(), z, z2, mo5.f(), mo5.a(), null, str, mo5.d());
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            String b = next.b();
            Hb7 b2 = ob7.b();
            if (Ao7.a(Wg6.a(b, b2 != null ? b2.b() : null)).booleanValue()) {
                t = next;
                break;
            }
        }
        T t4 = t;
        String f = t4 != null ? t4.f() : null;
        Hb7 b3 = ob7.b();
        Hb7 b4 = ob7.b();
        Rb7 rb7 = new Rb7(b3, (b4 == null || (a2 = b4.a()) == null) ? null : J37.c(a2), f, null, 8, null);
        for (T t5 : ob7.a()) {
            if (t5 instanceof Pb7) {
                Iterator<T> it2 = list2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (Ao7.a(Wg6.a(next2.d(), t5.d())).booleanValue()) {
                        t3 = next2;
                        break;
                    }
                }
                T t6 = t3;
                T t7 = t5;
                arrayList.add(new Vb7(t7, t7.e().length == 0 ? null : J37.c(t7.e()), t6 != null ? t6.f() : null));
            } else if (t5 instanceof Ib7) {
                Iterator<T> it3 = list3.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (Ao7.a(Wg6.a(next3.d(), t5.f())).booleanValue()) {
                        t2 = next3;
                        break;
                    }
                }
                T t8 = t2;
                T t9 = t5;
                arrayList.add(new Sb7(t9, t9.h().length == 0 ? null : J37.c(t9.h()), t8 != null ? t8.f() : null));
            } else if (t5 instanceof Mb7) {
                arrayList.add(new Tb7(t5));
            }
        }
        return new Dp5(mo5.e(), z, z2, mo5.f(), mo5.a(), new Ub7(rb7, arrayList), str, mo5.d());
    }
}
