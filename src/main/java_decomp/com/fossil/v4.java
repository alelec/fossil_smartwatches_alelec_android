package com.fossil;

import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LinkedHashSet<Long> c;

    @DexIgnore
    public V4(int i, int i2, LinkedHashSet<Long> linkedHashSet) {
        this.a = i;
        this.b = i2;
        this.c = linkedHashSet;
    }
}
