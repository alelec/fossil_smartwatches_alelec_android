package com.fossil;

import android.os.Handler;
import android.os.Message;
import com.fossil.Kf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Lf4 implements Handler.Callback {
    @DexIgnore
    public /* final */ Kf4.Bi a;

    @DexIgnore
    public Lf4(Kf4.Bi bi) {
        this.a = bi;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.a.h(message);
    }
}
