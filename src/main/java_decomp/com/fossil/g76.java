package com.fossil;

import com.mapped.WatchAppEditFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G76 implements MembersInjector<WatchAppEditFragment> {
    @DexIgnore
    public static void a(WatchAppEditFragment watchAppEditFragment, WatchAppsPresenter watchAppsPresenter) {
        watchAppEditFragment.l = watchAppsPresenter;
    }

    @DexIgnore
    public static void b(WatchAppEditFragment watchAppEditFragment, Po4 po4) {
        watchAppEditFragment.m = po4;
    }
}
