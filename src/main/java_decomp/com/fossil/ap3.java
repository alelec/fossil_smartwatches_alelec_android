package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ap3 extends Lm3 {
    @DexIgnore
    public volatile Xo3 c;
    @DexIgnore
    public Xo3 d;
    @DexIgnore
    public Xo3 e;
    @DexIgnore
    public /* final */ Map<Activity, Xo3> f; // = new ConcurrentHashMap();
    @DexIgnore
    public Activity g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public volatile Xo3 i;
    @DexIgnore
    public Xo3 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ Object l; // = new Object();
    @DexIgnore
    public String m;

    @DexIgnore
    public Ap3(Pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public static String E(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @DexIgnore
    public static void L(Xo3 xo3, Bundle bundle, boolean z) {
        if (bundle != null && xo3 != null && (!bundle.containsKey("_sc") || z)) {
            String str = xo3.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            String str2 = xo3.b;
            if (str2 != null) {
                bundle.putString("_sc", str2);
            } else {
                bundle.remove("_sc");
            }
            bundle.putLong("_si", xo3.c);
        } else if (bundle != null && xo3 == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @DexIgnore
    @Override // com.fossil.Lm3
    public final boolean A() {
        return false;
    }

    @DexIgnore
    public final Xo3 D(boolean z) {
        x();
        h();
        if (!m().s(Xg3.D0) || !z) {
            return this.e;
        }
        Xo3 xo3 = this.e;
        return xo3 == null ? this.j : xo3;
    }

    @DexIgnore
    public final void F(Activity activity) {
        if (m().s(Xg3.D0)) {
            synchronized (this.l) {
                this.k = true;
                if (activity != this.g) {
                    synchronized (this.l) {
                        this.g = activity;
                        this.h = false;
                    }
                    if (m().s(Xg3.C0) && m().K().booleanValue()) {
                        this.i = null;
                        c().y(new Gp3(this));
                    }
                }
            }
        }
        if (!m().s(Xg3.C0) || m().K().booleanValue()) {
            H(activity, W(activity), false);
            Gg3 o = o();
            o.c().y(new Kk3(o, o.zzm().c()));
            return;
        }
        this.c = this.i;
        c().y(new Bp3(this));
    }

    @DexIgnore
    public final void G(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (m().K().booleanValue() && bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f.put(activity, new Xo3(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    @DexIgnore
    public final void H(Activity activity, Xo3 xo3, boolean z) {
        Xo3 xo32;
        Xo3 xo33 = this.c == null ? this.d : this.c;
        if (xo3.b == null) {
            xo32 = new Xo3(xo3.a, activity != null ? E(activity.getClass().getCanonicalName()) : null, xo3.c, xo3.e, xo3.f);
        } else {
            xo32 = xo3;
        }
        this.d = this.c;
        this.c = xo32;
        c().y(new Cp3(this, xo32, xo33, zzm().c(), z));
    }

    @DexIgnore
    public final void I(Activity activity, String str, String str2) {
        if (!m().K().booleanValue()) {
            d().K().a("setCurrentScreen cannot be called while screen reporting is disabled.");
        } else if (this.c == null) {
            d().K().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f.get(activity) == null) {
            d().K().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = E(activity.getClass().getCanonicalName());
            }
            boolean z0 = Kr3.z0(this.c.b, str2);
            boolean z02 = Kr3.z0(this.c.a, str);
            if (z0 && z02) {
                d().K().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                d().K().b("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                d().N().c("Setting current screen to name, class", str == null ? "null" : str, str2);
                Xo3 xo3 = new Xo3(str, str2, k().D0());
                this.f.put(activity, xo3);
                H(activity, xo3, true);
            } else {
                d().K().b("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d7, code lost:
        r4 = d().N();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00df, code lost:
        if (r1 != null) goto L_0x0123;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e1, code lost:
        r3 = "null";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e4, code lost:
        if (r2 != null) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e6, code lost:
        r0 = "null";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e8, code lost:
        r4.c("Logging screen view with name, class", r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ef, code lost:
        if (r10.c != null) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f1, code lost:
        r8 = r10.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f4, code lost:
        r0 = new com.fossil.Xo3(r1, r2, k().D0(), true, r12);
        r10.c = r0;
        r10.d = r8;
        r10.i = r0;
        c().y(new com.fossil.Zo3(r10, r11, r0, r8, zzm().c()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0123, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0125, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0127, code lost:
        r8 = r10.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void J(android.os.Bundle r11, long r12) {
        /*
        // Method dump skipped, instructions count: 299
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ap3.J(android.os.Bundle, long):void");
    }

    @DexIgnore
    public final void K(Bundle bundle, Xo3 xo3, Xo3 xo32, long j2) {
        if (bundle != null) {
            bundle.remove("screen_name");
            bundle.remove("screen_class");
        }
        M(xo3, xo32, j2, true, k().B(null, "screen_view", bundle, null, true, true));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x010b, code lost:
        if (r6 != 0) goto L_0x010d;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void M(com.fossil.Xo3 r14, com.fossil.Xo3 r15, long r16, boolean r18, android.os.Bundle r19) {
        /*
        // Method dump skipped, instructions count: 355
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ap3.M(com.fossil.Xo3, com.fossil.Xo3, long, boolean, android.os.Bundle):void");
    }

    @DexIgnore
    public final void N(Xo3 xo3, boolean z, long j2) {
        o().v(zzm().c());
        if (u().E(xo3 != null && xo3.d, z, j2) && xo3 != null) {
            xo3.d = false;
        }
    }

    @DexIgnore
    public final void R(String str, Xo3 xo3) {
        h();
        synchronized (this) {
            if (this.m == null || this.m.equals(str) || xo3 != null) {
                this.m = str;
            }
        }
    }

    @DexIgnore
    public final Xo3 S() {
        f();
        return this.c;
    }

    @DexIgnore
    public final void T(Activity activity) {
        if (m().s(Xg3.D0)) {
            synchronized (this.l) {
                this.k = false;
                this.h = true;
            }
        }
        long c2 = zzm().c();
        if (!m().s(Xg3.C0) || m().K().booleanValue()) {
            Xo3 W = W(activity);
            this.d = this.c;
            this.c = null;
            c().y(new Dp3(this, W, c2));
            return;
        }
        this.c = null;
        c().y(new Ep3(this, c2));
    }

    @DexIgnore
    public final void U(Activity activity, Bundle bundle) {
        Xo3 xo3;
        if (m().K().booleanValue() && bundle != null && (xo3 = this.f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", xo3.c);
            bundle2.putString("name", xo3.a);
            bundle2.putString("referrer_name", xo3.b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    @DexIgnore
    public final void V(Activity activity) {
        synchronized (this.l) {
            if (activity == this.g) {
                this.g = null;
            }
        }
        if (m().K().booleanValue()) {
            this.f.remove(activity);
        }
    }

    @DexIgnore
    public final Xo3 W(Activity activity) {
        Rc2.k(activity);
        Xo3 xo3 = this.f.get(activity);
        if (xo3 == null) {
            xo3 = new Xo3(null, E(activity.getClass().getCanonicalName()), k().D0());
            this.f.put(activity, xo3);
        }
        return (m().s(Xg3.D0) && this.i != null) ? this.i : xo3;
    }
}
