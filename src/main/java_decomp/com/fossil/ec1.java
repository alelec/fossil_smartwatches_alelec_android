package com.fossil;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import com.fossil.Wb1;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ec1<T> implements Wb1<T> {
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentResolver c;
    @DexIgnore
    public T d;

    @DexIgnore
    public Ec1(ContentResolver contentResolver, Uri uri) {
        this.c = contentResolver;
        this.b = uri;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void a() {
        T t = this.d;
        if (t != null) {
            try {
                b(t);
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    public abstract void b(T t) throws IOException;

    @DexIgnore
    @Override // com.fossil.Wb1
    public Gb1 c() {
        return Gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public final void d(Sa1 sa1, Wb1.Ai<? super T> ai) {
        try {
            T e = e(this.b, this.c);
            this.d = e;
            ai.e(e);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e2);
            }
            ai.b(e2);
        }
    }

    @DexIgnore
    public abstract T e(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;
}
