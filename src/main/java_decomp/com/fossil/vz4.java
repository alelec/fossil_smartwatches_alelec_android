package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.fitnessdata.StressWrapper;
import com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vz4 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends TypeToken<ArrayList<RestingWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends TypeToken<ArrayList<SleepSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends TypeToken<ArrayList<WorkoutSessionWrapper>> {
    }

    /*
    static {
        String simpleName = D05.class.getSimpleName();
        Wg6.b(simpleName, "JsonObjectConverter::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public Vz4() {
        Zi4 zi4 = new Zi4();
        zi4.f(DateTime.class, new GsonConvertDateTime());
        Gson d = zi4.d();
        Wg6.b(d, "GsonBuilder().registerTy\u2026nvertDateTime()).create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(ActiveMinuteWrapper activeMinuteWrapper) {
        Wg6.c(activeMinuteWrapper, "activeMinute");
        try {
            String u = this.a.u(activeMinuteWrapper, ActiveMinuteWrapper.class);
            Wg6.b(u, "mGson.toJson(activeMinut\u2026inuteWrapper::class.java)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("activeMinuteToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String b(CalorieWrapper calorieWrapper) {
        Wg6.c(calorieWrapper, "calorie");
        try {
            String u = this.a.u(calorieWrapper, CalorieWrapper.class);
            Wg6.b(u, "mGson.toJson(calorie, CalorieWrapper::class.java)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("calorieToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String c(DistanceWrapper distanceWrapper) {
        Wg6.c(distanceWrapper, "distance");
        try {
            String u = this.a.u(distanceWrapper, DistanceWrapper.class);
            Wg6.b(u, "mGson.toJson(distance, D\u2026tanceWrapper::class.java)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("distanceToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String d(HeartRateWrapper heartRateWrapper) {
        if (heartRateWrapper == null) {
            return null;
        }
        try {
            return this.a.u(heartRateWrapper, HeartRateWrapper.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("heartRateToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String e(List<RestingWrapper> list) {
        Wg6.c(list, "resting");
        try {
            return this.a.u(list, new Ai().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("restingToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String f(List<SleepSessionWrapper> list) {
        Wg6.c(list, "sleepSessions");
        try {
            String u = this.a.u(list, new Bi().getType());
            Wg6.b(u, "mGson.toJson(sleepSessions, listType)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("sleepSessionsToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String g(StepWrapper stepWrapper) {
        Wg6.c(stepWrapper, "step");
        try {
            String u = this.a.u(stepWrapper, StepWrapper.class);
            Wg6.b(u, "mGson.toJson(step, StepWrapper::class.java)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stepToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final String h(StressWrapper stressWrapper) {
        if (stressWrapper == null) {
            return null;
        }
        try {
            return this.a.u(stressWrapper, StressWrapper.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("stressToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final ActiveMinuteWrapper i(String str) {
        Wg6.c(str, "data");
        try {
            Object k = this.a.k(str, ActiveMinuteWrapper.class);
            Wg6.b(k, "mGson.fromJson(data, Act\u2026inuteWrapper::class.java)");
            return (ActiveMinuteWrapper) k;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toActiveMinute exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new ActiveMinuteWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final CalorieWrapper j(String str) {
        Wg6.c(str, "data");
        try {
            Object k = this.a.k(str, CalorieWrapper.class);
            Wg6.b(k, "mGson.fromJson(data, CalorieWrapper::class.java)");
            return (CalorieWrapper) k;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toCalorie exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new CalorieWrapper(0, new ArrayList(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public final DistanceWrapper k(String str) {
        Wg6.c(str, "data");
        try {
            Object k = this.a.k(str, DistanceWrapper.class);
            Wg6.b(k, "mGson.fromJson(data, DistanceWrapper::class.java)");
            return (DistanceWrapper) k;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new DistanceWrapper(0, new ArrayList(), 0.0d);
        }
    }

    @DexIgnore
    public final HeartRateWrapper l(String str) {
        HeartRateWrapper heartRateWrapper;
        if (str == null) {
            return null;
        }
        try {
            heartRateWrapper = (HeartRateWrapper) this.a.k(str, HeartRateWrapper.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toDistance exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            heartRateWrapper = null;
        }
        return heartRateWrapper;
    }

    @DexIgnore
    public final List<RestingWrapper> m(String str) {
        try {
            Object l = this.a.l(str, new Ci().getType());
            Wg6.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toResting exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final List<SleepSessionWrapper> n(String str) {
        Wg6.c(str, "data");
        try {
            Object l = this.a.l(str, new Di().getType());
            Wg6.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toSleepSessions exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final StepWrapper o(String str) {
        Wg6.c(str, "data");
        try {
            Object k = this.a.k(str, StepWrapper.class);
            Wg6.b(k, "mGson.fromJson(data, StepWrapper::class.java)");
            return (StepWrapper) k;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStep exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new StepWrapper(0, new ArrayList(), 0);
        }
    }

    @DexIgnore
    public final StressWrapper p(String str) {
        StressWrapper stressWrapper;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            stressWrapper = (StressWrapper) this.a.k(str, StressWrapper.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toStress exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            stressWrapper = null;
        }
        return stressWrapper;
    }

    @DexIgnore
    public final List<WorkoutSessionWrapper> q(String str) {
        Wg6.c(str, "data");
        try {
            Object l = this.a.l(str, new Ei().getType());
            Wg6.b(l, "mGson.fromJson(data, listType)");
            return (List) l;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = b;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str2, sb.toString());
            return new ArrayList();
        }
    }

    @DexIgnore
    public final String r(List<WorkoutSessionWrapper> list) {
        Wg6.c(list, "workoutSessions");
        try {
            String u = this.a.u(list, new Fi().getType());
            Wg6.b(u, "mGson.toJson(workoutSessions, listType)");
            return u;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = b;
            StringBuilder sb = new StringBuilder();
            sb.append("workoutSessionToString exception=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d(str, sb.toString());
            return "";
        }
    }
}
