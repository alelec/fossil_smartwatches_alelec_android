package com.fossil;

import android.content.Context;
import android.content.res.Resources;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G62 extends H62 {
    @DexIgnore
    @Deprecated
    public static /* final */ int f; // = H62.a;

    @DexIgnore
    public static Context d(Context context) {
        return H62.d(context);
    }

    @DexIgnore
    public static Resources e(Context context) {
        return H62.e(context);
    }

    @DexIgnore
    @Deprecated
    public static int g(Context context) {
        return H62.g(context);
    }

    @DexIgnore
    @Deprecated
    public static int h(Context context, int i) {
        return H62.h(context, i);
    }
}
