package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B27 extends BaseFragment {
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ B27 b;

        @DexIgnore
        public Ai(B27 b27) {
            this.b = b27;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558520, viewGroup, false);
        ((ImageView) inflate.findViewById(2131362730)).setOnClickListener(new Ai(this));
        return inflate;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
