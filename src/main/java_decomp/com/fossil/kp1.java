package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kp1 extends Nu1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Bi e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kp1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    Wg6.b(createByteArray, "parcel.createByteArray()!!");
                    return new Kp1(readString, createByteArray, Bi.values()[parcel.readInt()]);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kp1[] newArray(int i) {
            return new Kp1[i];
        }
    }

    @DexIgnore
    public enum Bi {
        c("vertical"),
        d(MessengerShareContentUtility.IMAGE_RATIO_HORIZONTAL);
        
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Bi(String str) {
            this.b = str;
        }
    }

    @DexIgnore
    public Kp1(String str, byte[] bArr, Bi bi) {
        super(str, bArr);
        this.e = bi;
    }

    @DexIgnore
    public final Bi e() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Nu1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.N5, Ey1.a(this.e));
    }

    @DexIgnore
    @Override // com.fossil.Nu1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.e.ordinal());
        }
    }
}
