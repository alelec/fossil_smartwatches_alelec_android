package com.fossil;

import com.fossil.V18;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xc0 implements Interceptor {
    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        V18.Ai h = chain.c().h();
        h.a("Content-Type", Constants.CONTENT_TYPE);
        h.a("User-Agent", Hd0.y.y());
        h.a("locale", Hd0.y.n());
        Response d = chain.d(h.b());
        Wg6.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
