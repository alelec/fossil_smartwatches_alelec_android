package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H47<T> {
    @DexIgnore
    public static /* final */ Ai e; // = new Ai(null);
    @DexIgnore
    public /* final */ Xh5 a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Integer c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final <T> H47<T> a(T t) {
            return new H47<>(Xh5.DATABASE_LOADING, t, null, null);
        }

        @DexIgnore
        public final <T> H47<T> b(int i, String str, T t) {
            Wg6.c(str, "msg");
            return new H47<>(Xh5.ERROR, t, Integer.valueOf(i), str);
        }

        @DexIgnore
        public final <T> H47<T> c(T t) {
            return new H47<>(Xh5.NETWORK_LOADING, t, null, null);
        }

        @DexIgnore
        public final <T> H47<T> d(T t) {
            return new H47<>(Xh5.SUCCESS, t, null, null);
        }
    }

    @DexIgnore
    public H47(Xh5 xh5, T t, Integer num, String str) {
        Wg6.c(xh5, "status");
        this.a = xh5;
        this.b = t;
        this.c = num;
        this.d = str;
    }

    @DexIgnore
    public final Xh5 a() {
        return this.a;
    }

    @DexIgnore
    public final T b() {
        return this.b;
    }

    @DexIgnore
    public final T c() {
        return this.b;
    }

    @DexIgnore
    public final Xh5 d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof H47) {
                H47 h47 = (H47) obj;
                if (!Wg6.a(this.a, h47.a) || !Wg6.a(this.b, h47.b) || !Wg6.a(this.c, h47.c) || !Wg6.a(this.d, h47.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Xh5 xh5 = this.a;
        int hashCode = xh5 != null ? xh5.hashCode() : 0;
        T t = this.b;
        int hashCode2 = t != null ? t.hashCode() : 0;
        Integer num = this.c;
        int hashCode3 = num != null ? num.hashCode() : 0;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Resource(status=" + this.a + ", data=" + ((Object) this.b) + ", code=" + this.c + ", message=" + this.d + ")";
    }
}
