package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ View s;

    @DexIgnore
    public M15(Object obj, View view, int i, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, View view2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = rTLImageView;
        this.s = view2;
    }

    @DexIgnore
    @Deprecated
    public static M15 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (M15) ViewDataBinding.p(layoutInflater, 2131558487, viewGroup, z, obj);
    }

    @DexIgnore
    public static M15 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
