package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Yi0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ui0 implements Wi0 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Yi0.Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Yi0.Ai
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = rectF.height();
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                Ui0.this.a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas.translate(rectF.left + f3, rectF.top + f3);
                canvas.drawArc(Ui0.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(Ui0.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate((height - f2) - 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(Ui0.this.a, 180.0f, 90.0f, true, paint);
                canvas.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas.rotate(90.0f);
                canvas.drawArc(Ui0.this.a, 180.0f, 90.0f, true, paint);
                canvas.restoreToCount(save);
                float f5 = rectF.left;
                float f6 = rectF.top;
                canvas.drawRect((f5 + f3) - 1.0f, f6, 1.0f + (rectF.right - f3), f6 + f3, paint);
                float f7 = rectF.left;
                float f8 = rectF.bottom;
                canvas.drawRect((f7 + f3) - 1.0f, f8 - f3, 1.0f + (rectF.right - f3), f8, paint);
            }
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom - f, paint);
        }
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void a(Vi0 vi0, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        Yi0 p = p(context, colorStateList, f, f2, f3);
        p.m(vi0.d());
        vi0.c(p);
        i(vi0);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void b(Vi0 vi0, float f) {
        q(vi0).p(f);
        i(vi0);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float c(Vi0 vi0) {
        return q(vi0).l();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float d(Vi0 vi0) {
        return q(vi0).g();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void e(Vi0 vi0) {
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void f(Vi0 vi0, float f) {
        q(vi0).r(f);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float g(Vi0 vi0) {
        return q(vi0).i();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public ColorStateList h(Vi0 vi0) {
        return q(vi0).f();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void i(Vi0 vi0) {
        Rect rect = new Rect();
        q(vi0).h(rect);
        vi0.b((int) Math.ceil((double) l(vi0)), (int) Math.ceil((double) k(vi0)));
        vi0.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void j() {
        Yi0.r = new Ai();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float k(Vi0 vi0) {
        return q(vi0).j();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float l(Vi0 vi0) {
        return q(vi0).k();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void m(Vi0 vi0) {
        q(vi0).m(vi0.d());
        i(vi0);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void n(Vi0 vi0, ColorStateList colorStateList) {
        q(vi0).o(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void o(Vi0 vi0, float f) {
        q(vi0).q(f);
        i(vi0);
    }

    @DexIgnore
    public final Yi0 p(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new Yi0(context.getResources(), colorStateList, f, f2, f3);
    }

    @DexIgnore
    public final Yi0 q(Vi0 vi0) {
        return (Yi0) vi0.f();
    }
}
