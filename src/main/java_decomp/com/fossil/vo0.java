package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.view.WindowInsets;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vo0 {
    @DexIgnore
    public static /* final */ Vo0 b; // = new Ai().a().a().b().c();
    @DexIgnore
    public /* final */ Ii a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Di a;

        @DexIgnore
        public Ai() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.a = new Ci();
            } else if (i >= 20) {
                this.a = new Bi();
            } else {
                this.a = new Di();
            }
        }

        @DexIgnore
        public Ai(Vo0 vo0) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 29) {
                this.a = new Ci(vo0);
            } else if (i >= 20) {
                this.a = new Bi(vo0);
            } else {
                this.a = new Di(vo0);
            }
        }

        @DexIgnore
        public Vo0 a() {
            return this.a.a();
        }

        @DexIgnore
        public Ai b(Ql0 ql0) {
            this.a.b(ql0);
            return this;
        }

        @DexIgnore
        public Ai c(Ql0 ql0) {
            this.a.c(ql0);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Di {
        @DexIgnore
        public static Field c;
        @DexIgnore
        public static boolean d;
        @DexIgnore
        public static Constructor<WindowInsets> e;
        @DexIgnore
        public static boolean f;
        @DexIgnore
        public WindowInsets b;

        @DexIgnore
        public Bi() {
            this.b = d();
        }

        @DexIgnore
        public Bi(Vo0 vo0) {
            this.b = vo0.n();
        }

        @DexIgnore
        public static WindowInsets d() {
            if (!d) {
                try {
                    c = WindowInsets.class.getDeclaredField("CONSUMED");
                } catch (ReflectiveOperationException e2) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", e2);
                }
                d = true;
            }
            Field field = c;
            if (field != null) {
                try {
                    WindowInsets windowInsets = (WindowInsets) field.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                } catch (ReflectiveOperationException e3) {
                    Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", e3);
                }
            }
            if (!f) {
                try {
                    e = WindowInsets.class.getConstructor(Rect.class);
                } catch (ReflectiveOperationException e4) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", e4);
                }
                f = true;
            }
            Constructor<WindowInsets> constructor = e;
            if (constructor != null) {
                try {
                    return constructor.newInstance(new Rect());
                } catch (ReflectiveOperationException e5) {
                    Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", e5);
                }
            }
            return null;
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Di
        public Vo0 a() {
            return Vo0.o(this.b);
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Di
        public void c(Ql0 ql0) {
            WindowInsets windowInsets = this.b;
            if (windowInsets != null) {
                this.b = windowInsets.replaceSystemWindowInsets(ql0.a, ql0.b, ql0.c, ql0.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Di {
        @DexIgnore
        public /* final */ WindowInsets.Builder b;

        @DexIgnore
        public Ci() {
            this.b = new WindowInsets.Builder();
        }

        @DexIgnore
        public Ci(Vo0 vo0) {
            WindowInsets n = vo0.n();
            this.b = n != null ? new WindowInsets.Builder(n) : new WindowInsets.Builder();
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Di
        public Vo0 a() {
            return Vo0.o(this.b.build());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Di
        public void b(Ql0 ql0) {
            this.b.setStableInsets(ql0.b());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Di
        public void c(Ql0 ql0) {
            this.b.setSystemWindowInsets(ql0.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ Vo0 a;

        @DexIgnore
        public Di() {
            this(new Vo0((Vo0) null));
        }

        @DexIgnore
        public Di(Vo0 vo0) {
            this.a = vo0;
        }

        @DexIgnore
        public Vo0 a() {
            return this.a;
        }

        @DexIgnore
        public void b(Ql0 ql0) {
        }

        @DexIgnore
        public void c(Ql0 ql0) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends Ii {
        @DexIgnore
        public /* final */ WindowInsets b;
        @DexIgnore
        public Ql0 c;

        @DexIgnore
        public Ei(Vo0 vo0, WindowInsets windowInsets) {
            super(vo0);
            this.c = null;
            this.b = windowInsets;
        }

        @DexIgnore
        public Ei(Vo0 vo0, Ei ei) {
            this(vo0, new WindowInsets(ei.b));
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public final Ql0 f() {
            if (this.c == null) {
                this.c = Ql0.a(this.b.getSystemWindowInsetLeft(), this.b.getSystemWindowInsetTop(), this.b.getSystemWindowInsetRight(), this.b.getSystemWindowInsetBottom());
            }
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public Vo0 g(int i, int i2, int i3, int i4) {
            Ai ai = new Ai(Vo0.o(this.b));
            ai.c(Vo0.k(f(), i, i2, i3, i4));
            ai.b(Vo0.k(e(), i, i2, i3, i4));
            return ai.a();
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public boolean i() {
            return this.b.isRound();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi extends Ei {
        @DexIgnore
        public Ql0 d; // = null;

        @DexIgnore
        public Fi(Vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public Fi(Vo0 vo0, Fi fi) {
            super(vo0, fi);
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public Vo0 b() {
            return Vo0.o(this.b.consumeStableInsets());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public Vo0 c() {
            return Vo0.o(this.b.consumeSystemWindowInsets());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public final Ql0 e() {
            if (this.d == null) {
                this.d = Ql0.a(this.b.getStableInsetLeft(), this.b.getStableInsetTop(), this.b.getStableInsetRight(), this.b.getStableInsetBottom());
            }
            return this.d;
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public boolean h() {
            return this.b.isConsumed();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi extends Fi {
        @DexIgnore
        public Gi(Vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public Gi(Vo0 vo0, Gi gi) {
            super(vo0, gi);
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public Vo0 a() {
            return Vo0.o(this.b.consumeDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public Tn0 d() {
            return Tn0.a(this.b.getDisplayCutout());
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Gi)) {
                return false;
            }
            return Objects.equals(this.b, ((Gi) obj).b);
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ii
        public int hashCode() {
            return this.b.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Hi extends Gi {
        @DexIgnore
        public Hi(Vo0 vo0, WindowInsets windowInsets) {
            super(vo0, windowInsets);
        }

        @DexIgnore
        public Hi(Vo0 vo0, Hi hi) {
            super(vo0, hi);
        }

        @DexIgnore
        @Override // com.fossil.Vo0.Ei, com.fossil.Vo0.Ii
        public Vo0 g(int i, int i2, int i3, int i4) {
            return Vo0.o(this.b.inset(i, i2, i3, i4));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ii {
        @DexIgnore
        public /* final */ Vo0 a;

        @DexIgnore
        public Ii(Vo0 vo0) {
            this.a = vo0;
        }

        @DexIgnore
        public Vo0 a() {
            return this.a;
        }

        @DexIgnore
        public Vo0 b() {
            return this.a;
        }

        @DexIgnore
        public Vo0 c() {
            return this.a;
        }

        @DexIgnore
        public Tn0 d() {
            return null;
        }

        @DexIgnore
        public Ql0 e() {
            return Ql0.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ii)) {
                return false;
            }
            Ii ii = (Ii) obj;
            return i() == ii.i() && h() == ii.h() && Kn0.a(f(), ii.f()) && Kn0.a(e(), ii.e()) && Kn0.a(d(), ii.d());
        }

        @DexIgnore
        public Ql0 f() {
            return Ql0.e;
        }

        @DexIgnore
        public Vo0 g(int i, int i2, int i3, int i4) {
            return Vo0.b;
        }

        @DexIgnore
        public boolean h() {
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return Kn0.b(Boolean.valueOf(i()), Boolean.valueOf(h()), f(), e(), d());
        }

        @DexIgnore
        public boolean i() {
            return false;
        }
    }

    @DexIgnore
    public Vo0(WindowInsets windowInsets) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            this.a = new Hi(this, windowInsets);
        } else if (i >= 28) {
            this.a = new Gi(this, windowInsets);
        } else if (i >= 21) {
            this.a = new Fi(this, windowInsets);
        } else if (i >= 20) {
            this.a = new Ei(this, windowInsets);
        } else {
            this.a = new Ii(this);
        }
    }

    @DexIgnore
    public Vo0(Vo0 vo0) {
        if (vo0 != null) {
            Ii ii = vo0.a;
            if (Build.VERSION.SDK_INT >= 29 && (ii instanceof Hi)) {
                this.a = new Hi(this, (Hi) ii);
            } else if (Build.VERSION.SDK_INT >= 28 && (ii instanceof Gi)) {
                this.a = new Gi(this, (Gi) ii);
            } else if (Build.VERSION.SDK_INT >= 21 && (ii instanceof Fi)) {
                this.a = new Fi(this, (Fi) ii);
            } else if (Build.VERSION.SDK_INT < 20 || !(ii instanceof Ei)) {
                this.a = new Ii(this);
            } else {
                this.a = new Ei(this, (Ei) ii);
            }
        } else {
            this.a = new Ii(this);
        }
    }

    @DexIgnore
    public static Ql0 k(Ql0 ql0, int i, int i2, int i3, int i4) {
        int max = Math.max(0, ql0.a - i);
        int max2 = Math.max(0, ql0.b - i2);
        int max3 = Math.max(0, ql0.c - i3);
        int max4 = Math.max(0, ql0.d - i4);
        return (max == i && max2 == i2 && max3 == i3 && max4 == i4) ? ql0 : Ql0.a(max, max2, max3, max4);
    }

    @DexIgnore
    public static Vo0 o(WindowInsets windowInsets) {
        Pn0.d(windowInsets);
        return new Vo0(windowInsets);
    }

    @DexIgnore
    public Vo0 a() {
        return this.a.a();
    }

    @DexIgnore
    public Vo0 b() {
        return this.a.b();
    }

    @DexIgnore
    public Vo0 c() {
        return this.a.c();
    }

    @DexIgnore
    public int d() {
        return h().d;
    }

    @DexIgnore
    public int e() {
        return h().a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Vo0)) {
            return false;
        }
        return Kn0.a(this.a, ((Vo0) obj).a);
    }

    @DexIgnore
    public int f() {
        return h().c;
    }

    @DexIgnore
    public int g() {
        return h().b;
    }

    @DexIgnore
    public Ql0 h() {
        return this.a.f();
    }

    @DexIgnore
    public int hashCode() {
        Ii ii = this.a;
        if (ii == null) {
            return 0;
        }
        return ii.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return !h().equals(Ql0.e);
    }

    @DexIgnore
    public Vo0 j(int i, int i2, int i3, int i4) {
        return this.a.g(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean l() {
        return this.a.h();
    }

    @DexIgnore
    @Deprecated
    public Vo0 m(int i, int i2, int i3, int i4) {
        Ai ai = new Ai(this);
        ai.c(Ql0.a(i, i2, i3, i4));
        return ai.a();
    }

    @DexIgnore
    public WindowInsets n() {
        Ii ii = this.a;
        if (ii instanceof Ei) {
            return ((Ei) ii).b;
        }
        return null;
    }
}
