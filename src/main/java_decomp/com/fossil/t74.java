package com.fossil;

import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.fossil.M64;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class T74 implements M64.Bi {
    @DexIgnore
    public C84 a;
    @DexIgnore
    public C84 b;

    @DexIgnore
    public static void b(C84 c84, String str, Bundle bundle) {
        if (c84 != null) {
            c84.q(str, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.M64.Bi
    public void a(int i, Bundle bundle) {
        String string;
        X74 f = X74.f();
        f.b("Received Analytics message: " + i + " " + bundle);
        if (bundle != null && (string = bundle.getString("name")) != null) {
            Bundle bundle2 = bundle.getBundle(NativeProtocol.WEB_DIALOG_PARAMS);
            if (bundle2 == null) {
                bundle2 = new Bundle();
            }
            c(string, bundle2);
        }
    }

    @DexIgnore
    public final void c(String str, Bundle bundle) {
        b("clx".equals(bundle.getString("_o")) ? this.a : this.b, str, bundle);
    }

    @DexIgnore
    public void d(C84 c84) {
        this.b = c84;
    }

    @DexIgnore
    public void e(C84 c84) {
        this.a = c84;
    }
}
