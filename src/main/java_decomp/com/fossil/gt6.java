package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gt6 implements Factory<CustomizeSleepChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Gt6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Gt6 a(Provider<ThemeRepository> provider) {
        return new Gt6(provider);
    }

    @DexIgnore
    public static CustomizeSleepChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeSleepChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeSleepChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
