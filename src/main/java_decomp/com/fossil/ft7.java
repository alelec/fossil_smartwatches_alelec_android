package com.fossil;

import com.mapped.Coroutine;
import com.mapped.Lc6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ft7 implements Ts7<Wr7> {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Coroutine<CharSequence, Integer, Lc6<Integer, Integer>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterator<Wr7>, Jr7 {
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public Wr7 e;
        @DexIgnore
        public int f;
        @DexIgnore
        public /* final */ /* synthetic */ Ft7 g;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Ft7 ft7) {
            this.g = ft7;
            int i = Bs7.i(ft7.b, 0, ft7.a.length());
            this.c = i;
            this.d = i;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0021, code lost:
            if (r0 < r6.g.c) goto L_0x0023;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a() {
            /*
                r6 = this;
                r2 = 1
                r3 = 0
                r5 = -1
                int r0 = r6.d
                if (r0 >= 0) goto L_0x000d
                r6.b = r3
                r0 = 0
                r6.e = r0
            L_0x000c:
                return
            L_0x000d:
                com.fossil.Ft7 r0 = r6.g
                int r0 = com.fossil.Ft7.c(r0)
                if (r0 <= 0) goto L_0x0023
                int r0 = r6.f
                int r0 = r0 + 1
                r6.f = r0
                com.fossil.Ft7 r1 = r6.g
                int r1 = com.fossil.Ft7.c(r1)
                if (r0 >= r1) goto L_0x0031
            L_0x0023:
                int r0 = r6.d
                com.fossil.Ft7 r1 = r6.g
                java.lang.CharSequence r1 = com.fossil.Ft7.b(r1)
                int r1 = r1.length()
                if (r0 <= r1) goto L_0x0049
            L_0x0031:
                com.fossil.Wr7 r0 = new com.fossil.Wr7
                int r1 = r6.c
                com.fossil.Ft7 r3 = r6.g
                java.lang.CharSequence r3 = com.fossil.Ft7.b(r3)
                int r3 = com.fossil.Wt7.A(r3)
                r0.<init>(r1, r3)
                r6.e = r0
                r6.d = r5
            L_0x0046:
                r6.b = r2
                goto L_0x000c
            L_0x0049:
                com.fossil.Ft7 r0 = r6.g
                com.mapped.Coroutine r0 = com.fossil.Ft7.a(r0)
                com.fossil.Ft7 r1 = r6.g
                java.lang.CharSequence r1 = com.fossil.Ft7.b(r1)
                int r4 = r6.d
                java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
                java.lang.Object r0 = r0.invoke(r1, r4)
                com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                if (r0 != 0) goto L_0x0079
                com.fossil.Wr7 r0 = new com.fossil.Wr7
                int r1 = r6.c
                com.fossil.Ft7 r3 = r6.g
                java.lang.CharSequence r3 = com.fossil.Ft7.b(r3)
                int r3 = com.fossil.Wt7.A(r3)
                r0.<init>(r1, r3)
                r6.e = r0
                r6.d = r5
                goto L_0x0046
            L_0x0079:
                java.lang.Object r1 = r0.component1()
                java.lang.Number r1 = (java.lang.Number) r1
                int r1 = r1.intValue()
                java.lang.Object r0 = r0.component2()
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                int r4 = r6.c
                com.fossil.Wr7 r4 = com.fossil.Bs7.m(r4, r1)
                r6.e = r4
                int r1 = r1 + r0
                r6.c = r1
                if (r0 != 0) goto L_0x009f
                r0 = r2
            L_0x009b:
                int r0 = r0 + r1
                r6.d = r0
                goto L_0x0046
            L_0x009f:
                r0 = r3
                goto L_0x009b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ft7.Ai.a():void");
        }

        @DexIgnore
        public Wr7 b() {
            if (this.b == -1) {
                a();
            }
            if (this.b != 0) {
                Wr7 wr7 = this.e;
                if (wr7 != null) {
                    this.e = null;
                    this.b = -1;
                    return wr7;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b == -1) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.Iterator
        public /* bridge */ /* synthetic */ Wr7 next() {
            return b();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.mapped.Coroutine<? super java.lang.CharSequence, ? super java.lang.Integer, com.mapped.Lc6<java.lang.Integer, java.lang.Integer>> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ft7(CharSequence charSequence, int i, int i2, Coroutine<? super CharSequence, ? super Integer, Lc6<Integer, Integer>> coroutine) {
        Wg6.c(charSequence, "input");
        Wg6.c(coroutine, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = coroutine;
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<Wr7> iterator() {
        return new Ai(this);
    }
}
