package com.fossil;

import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kt7 extends Qq7 implements Hg6<T, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ int $value$inlined;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Kt7(int i) {
        super(1);
        this.$value$inlined = i;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Boolean invoke(Object obj) {
        return Boolean.valueOf(invoke((Kt7) ((Enum) obj)));
    }

    @DexIgnore
    public final boolean invoke(T t) {
        T t2 = t;
        return (this.$value$inlined & t2.getMask()) == t2.getValue();
    }
}
