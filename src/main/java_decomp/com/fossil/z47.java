package com.fossil;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z47 extends MetricAffectingSpan {
    @DexIgnore
    public Typeface b;

    @DexIgnore
    public Z47(Typeface typeface) {
        Wg6.c(typeface, "typeface");
        this.b = typeface;
    }

    @DexIgnore
    public final void a(TextPaint textPaint, Typeface typeface) {
        textPaint.setTypeface(typeface);
    }

    @DexIgnore
    public void updateDrawState(TextPaint textPaint) {
        if (textPaint != null) {
            Typeface typeface = this.b;
            if (typeface != null) {
                a(textPaint, typeface);
            } else {
                Wg6.n("typeface");
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void updateMeasureState(TextPaint textPaint) {
        Wg6.c(textPaint, "textPaint");
        Typeface typeface = this.b;
        if (typeface != null) {
            a(textPaint, typeface);
        } else {
            Wg6.n("typeface");
            throw null;
        }
    }
}
