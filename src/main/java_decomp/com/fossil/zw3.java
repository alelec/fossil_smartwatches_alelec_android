package com.fossil;

import android.graphics.Matrix;
import android.util.Property;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zw3 extends Property<ImageView, Matrix> {
    @DexIgnore
    public /* final */ Matrix a; // = new Matrix();

    @DexIgnore
    public Zw3() {
        super(Matrix.class, "imageMatrixProperty");
    }

    @DexIgnore
    public Matrix a(ImageView imageView) {
        this.a.set(imageView.getImageMatrix());
        return this.a;
    }

    @DexIgnore
    public void b(ImageView imageView, Matrix matrix) {
        imageView.setImageMatrix(matrix);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ Matrix get(ImageView imageView) {
        return a(imageView);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ void set(ImageView imageView, Matrix matrix) {
        b(imageView, matrix);
    }
}
