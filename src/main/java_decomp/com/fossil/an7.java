package com.fossil;

import com.mapped.Lc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class An7 extends Zm7 {
    @DexIgnore
    public static final <K, V> List<Lc6<K, V>> q(Map<? extends K, ? extends V> map) {
        Wg6.c(map, "$this$toList");
        if (map.size() == 0) {
            return Hm7.e();
        }
        Iterator<Map.Entry<? extends K, ? extends V>> it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return Hm7.e();
        }
        Map.Entry<? extends K, ? extends V> next = it.next();
        if (!it.hasNext()) {
            return Gm7.b(new Lc6(next.getKey(), next.getValue()));
        }
        ArrayList arrayList = new ArrayList(map.size());
        arrayList.add(new Lc6(next.getKey(), next.getValue()));
        do {
            Map.Entry<? extends K, ? extends V> next2 = it.next();
            arrayList.add(new Lc6(next2.getKey(), next2.getValue()));
        } while (it.hasNext());
        return arrayList;
    }
}
