package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zr7 extends Xr7 {
    /*
    static {
        new Zr7(1, 0);
    }
    */

    @DexIgnore
    public Zr7(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean e(long j) {
        return a() <= j && j <= b();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Zr7) {
            if (!isEmpty() || !((Zr7) obj).isEmpty()) {
                Zr7 zr7 = (Zr7) obj;
                if (!(a() == zr7.a() && b() == zr7.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (a() ^ (a() >>> 32))) + (b() ^ (b() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
