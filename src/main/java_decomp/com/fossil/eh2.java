package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eh2 implements DynamiteModule.b.b {
    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b.b
    public final int a(Context context, String str) {
        return DynamiteModule.a(context, str);
    }

    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b.b
    public final int b(Context context, String str, boolean z) throws DynamiteModule.a {
        return DynamiteModule.f(context, str, z);
    }
}
