package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nv extends Ss {
    @DexIgnore
    public long A; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public R4 B; // = R4.b;

    @DexIgnore
    public Nv(K5 k5) {
        super(Hs.a0, k5);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.D0, Ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new G6(this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void g(U5 u5) {
        this.B = ((G6) u5).k;
        this.g.add(new Hw(0, null, null, G80.k(new JSONObject(), Jd0.D0, Ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.o1, Ey1.a(this.y.H()));
    }
}
