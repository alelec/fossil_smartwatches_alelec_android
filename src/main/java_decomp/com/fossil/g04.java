package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G04 {
    @DexIgnore
    public static /* final */ Yz3 m; // = new E04(0.5f);
    @DexIgnore
    public Zz3 a;
    @DexIgnore
    public Zz3 b;
    @DexIgnore
    public Zz3 c;
    @DexIgnore
    public Zz3 d;
    @DexIgnore
    public Yz3 e;
    @DexIgnore
    public Yz3 f;
    @DexIgnore
    public Yz3 g;
    @DexIgnore
    public Yz3 h;
    @DexIgnore
    public B04 i;
    @DexIgnore
    public B04 j;
    @DexIgnore
    public B04 k;
    @DexIgnore
    public B04 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Zz3 a; // = D04.b();
        @DexIgnore
        public Zz3 b; // = D04.b();
        @DexIgnore
        public Zz3 c; // = D04.b();
        @DexIgnore
        public Zz3 d; // = D04.b();
        @DexIgnore
        public Yz3 e; // = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public Yz3 f; // = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public Yz3 g; // = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public Yz3 h; // = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        @DexIgnore
        public B04 i; // = D04.c();
        @DexIgnore
        public B04 j; // = D04.c();
        @DexIgnore
        public B04 k; // = D04.c();
        @DexIgnore
        public B04 l; // = D04.c();

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(G04 g04) {
            this.a = g04.a;
            this.b = g04.b;
            this.c = g04.c;
            this.d = g04.d;
            this.e = g04.e;
            this.f = g04.f;
            this.g = g04.g;
            this.h = g04.h;
            this.i = g04.i;
            this.j = g04.j;
            this.k = g04.k;
            this.l = g04.l;
        }

        @DexIgnore
        public static float n(Zz3 zz3) {
            if (zz3 instanceof F04) {
                return ((F04) zz3).a;
            }
            if (zz3 instanceof A04) {
                return ((A04) zz3).a;
            }
            return -1.0f;
        }

        @DexIgnore
        public Bi A(float f2) {
            this.e = new Wz3(f2);
            return this;
        }

        @DexIgnore
        public Bi B(Yz3 yz3) {
            this.e = yz3;
            return this;
        }

        @DexIgnore
        public Bi C(int i2, Yz3 yz3) {
            D(D04.a(i2));
            F(yz3);
            return this;
        }

        @DexIgnore
        public Bi D(Zz3 zz3) {
            this.b = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                E(n);
            }
            return this;
        }

        @DexIgnore
        public Bi E(float f2) {
            this.f = new Wz3(f2);
            return this;
        }

        @DexIgnore
        public Bi F(Yz3 yz3) {
            this.f = yz3;
            return this;
        }

        @DexIgnore
        public G04 m() {
            return new G04(this);
        }

        @DexIgnore
        public Bi o(float f2) {
            A(f2);
            E(f2);
            v(f2);
            r(f2);
            return this;
        }

        @DexIgnore
        public Bi p(int i2, Yz3 yz3) {
            q(D04.a(i2));
            s(yz3);
            return this;
        }

        @DexIgnore
        public Bi q(Zz3 zz3) {
            this.d = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                r(n);
            }
            return this;
        }

        @DexIgnore
        public Bi r(float f2) {
            this.h = new Wz3(f2);
            return this;
        }

        @DexIgnore
        public Bi s(Yz3 yz3) {
            this.h = yz3;
            return this;
        }

        @DexIgnore
        public Bi t(int i2, Yz3 yz3) {
            u(D04.a(i2));
            w(yz3);
            return this;
        }

        @DexIgnore
        public Bi u(Zz3 zz3) {
            this.c = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                v(n);
            }
            return this;
        }

        @DexIgnore
        public Bi v(float f2) {
            this.g = new Wz3(f2);
            return this;
        }

        @DexIgnore
        public Bi w(Yz3 yz3) {
            this.g = yz3;
            return this;
        }

        @DexIgnore
        public Bi x(B04 b04) {
            this.i = b04;
            return this;
        }

        @DexIgnore
        public Bi y(int i2, Yz3 yz3) {
            z(D04.a(i2));
            B(yz3);
            return this;
        }

        @DexIgnore
        public Bi z(Zz3 zz3) {
            this.a = zz3;
            float n = n(zz3);
            if (n != -1.0f) {
                A(n);
            }
            return this;
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Yz3 a(Yz3 yz3);
    }

    @DexIgnore
    public G04() {
        this.a = D04.b();
        this.b = D04.b();
        this.c = D04.b();
        this.d = D04.b();
        this.e = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.h = new Wz3(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.i = D04.c();
        this.j = D04.c();
        this.k = D04.c();
        this.l = D04.c();
    }

    @DexIgnore
    public G04(Bi bi) {
        this.a = bi.a;
        this.b = bi.b;
        this.c = bi.c;
        this.d = bi.d;
        this.e = bi.e;
        this.f = bi.f;
        this.g = bi.g;
        this.h = bi.h;
        this.i = bi.i;
        this.j = bi.j;
        this.k = bi.k;
        this.l = bi.l;
    }

    @DexIgnore
    public static Bi a() {
        return new Bi();
    }

    @DexIgnore
    public static Bi b(Context context, int i2, int i3) {
        return c(context, i2, i3, 0);
    }

    @DexIgnore
    public static Bi c(Context context, int i2, int i3, int i4) {
        return d(context, i2, i3, new Wz3((float) i4));
    }

    @DexIgnore
    public static Bi d(Context context, int i2, int i3, Yz3 yz3) {
        if (i3 != 0) {
            context = new ContextThemeWrapper(context, i2);
        } else {
            i3 = i2;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i3, Tw3.ShapeAppearance);
        try {
            int i4 = obtainStyledAttributes.getInt(Tw3.ShapeAppearance_cornerFamily, 0);
            int i5 = obtainStyledAttributes.getInt(Tw3.ShapeAppearance_cornerFamilyTopLeft, i4);
            int i6 = obtainStyledAttributes.getInt(Tw3.ShapeAppearance_cornerFamilyTopRight, i4);
            int i7 = obtainStyledAttributes.getInt(Tw3.ShapeAppearance_cornerFamilyBottomRight, i4);
            int i8 = obtainStyledAttributes.getInt(Tw3.ShapeAppearance_cornerFamilyBottomLeft, i4);
            Yz3 m2 = m(obtainStyledAttributes, Tw3.ShapeAppearance_cornerSize, yz3);
            Yz3 m3 = m(obtainStyledAttributes, Tw3.ShapeAppearance_cornerSizeTopLeft, m2);
            Yz3 m4 = m(obtainStyledAttributes, Tw3.ShapeAppearance_cornerSizeTopRight, m2);
            Yz3 m5 = m(obtainStyledAttributes, Tw3.ShapeAppearance_cornerSizeBottomRight, m2);
            Yz3 m6 = m(obtainStyledAttributes, Tw3.ShapeAppearance_cornerSizeBottomLeft, m2);
            Bi bi = new Bi();
            bi.y(i5, m3);
            bi.C(i6, m4);
            bi.t(i7, m5);
            bi.p(i8, m6);
            return bi;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public static Bi e(Context context, AttributeSet attributeSet, int i2, int i3) {
        return f(context, attributeSet, i2, i3, 0);
    }

    @DexIgnore
    public static Bi f(Context context, AttributeSet attributeSet, int i2, int i3, int i4) {
        return g(context, attributeSet, i2, i3, new Wz3((float) i4));
    }

    @DexIgnore
    public static Bi g(Context context, AttributeSet attributeSet, int i2, int i3, Yz3 yz3) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, Tw3.MaterialShape, i2, i3);
        int resourceId = obtainStyledAttributes.getResourceId(Tw3.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(Tw3.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return d(context, resourceId, resourceId2, yz3);
    }

    @DexIgnore
    public static Yz3 m(TypedArray typedArray, int i2, Yz3 yz3) {
        TypedValue peekValue = typedArray.peekValue(i2);
        if (peekValue == null) {
            return yz3;
        }
        int i3 = peekValue.type;
        return i3 == 5 ? new Wz3((float) TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics())) : i3 == 6 ? new E04(peekValue.getFraction(1.0f, 1.0f)) : yz3;
    }

    @DexIgnore
    public B04 h() {
        return this.k;
    }

    @DexIgnore
    public Zz3 i() {
        return this.d;
    }

    @DexIgnore
    public Yz3 j() {
        return this.h;
    }

    @DexIgnore
    public Zz3 k() {
        return this.c;
    }

    @DexIgnore
    public Yz3 l() {
        return this.g;
    }

    @DexIgnore
    public B04 n() {
        return this.l;
    }

    @DexIgnore
    public B04 o() {
        return this.j;
    }

    @DexIgnore
    public B04 p() {
        return this.i;
    }

    @DexIgnore
    public Zz3 q() {
        return this.a;
    }

    @DexIgnore
    public Yz3 r() {
        return this.e;
    }

    @DexIgnore
    public Zz3 s() {
        return this.b;
    }

    @DexIgnore
    public Yz3 t() {
        return this.f;
    }

    @DexIgnore
    public boolean u(RectF rectF) {
        boolean z = this.l.getClass().equals(B04.class) && this.j.getClass().equals(B04.class) && this.i.getClass().equals(B04.class) && this.k.getClass().equals(B04.class);
        float a2 = this.e.a(rectF);
        return z && ((this.f.a(rectF) > a2 ? 1 : (this.f.a(rectF) == a2 ? 0 : -1)) == 0 && (this.h.a(rectF) > a2 ? 1 : (this.h.a(rectF) == a2 ? 0 : -1)) == 0 && (this.g.a(rectF) > a2 ? 1 : (this.g.a(rectF) == a2 ? 0 : -1)) == 0) && ((this.b instanceof F04) && (this.a instanceof F04) && (this.c instanceof F04) && (this.d instanceof F04));
    }

    @DexIgnore
    public Bi v() {
        return new Bi(this);
    }

    @DexIgnore
    public G04 w(float f2) {
        Bi v = v();
        v.o(f2);
        return v.m();
    }

    @DexIgnore
    public G04 x(Ci ci) {
        Bi v = v();
        v.B(ci.a(r()));
        v.F(ci.a(t()));
        v.s(ci.a(j()));
        v.w(ci.a(l()));
        return v.m();
    }
}
