package com.fossil;

import android.content.Context;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Jn5;
import com.mapped.An4;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.PermissionData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jz6 extends Cz6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ Dz6 e;
    @DexIgnore
    public /* final */ List<PermissionData> f;
    @DexIgnore
    public /* final */ An4 g;

    /*
    static {
        String simpleName = Jz6.class.getSimpleName();
        Wg6.b(simpleName, "PermissionPresenter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public Jz6(Dz6 dz6, List<PermissionData> list, An4 an4) {
        Wg6.c(dz6, "mView");
        Wg6.c(list, "mListPerms");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.e = dz6;
        this.f = list;
        this.g = an4;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        boolean z;
        FLogger.INSTANCE.getLocal().d(h, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Dz6 dz6 = this.e;
        if (dz6 != null) {
            Context context = ((Ez6) dz6).getContext();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (T t : this.f) {
                ArrayList<String> androidPermissionSet = t.getAndroidPermissionSet();
                ArrayList arrayList3 = new ArrayList();
                for (T t2 : androidPermissionSet) {
                    T t3 = t2;
                    if (context == null) {
                        Wg6.i();
                        throw null;
                    } else if (!V78.a(context, t3)) {
                        arrayList3.add(t2);
                    }
                }
                if (t.getSettingPermissionId() != null) {
                    Jn5 jn5 = Jn5.b;
                    Jn5.Ci settingPermissionId = t.getSettingPermissionId();
                    if (settingPermissionId != null) {
                        z = jn5.n(settingPermissionId);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    z = true;
                }
                FLogger.INSTANCE.getLocal().d(h, "check " + ((Object) t) + " androidPermissionsNotGranted " + arrayList3 + " settingPermissionGranted " + z);
                t.setGranted(arrayList3.isEmpty() && z);
                if (Wg6.a(t.getType(), "PERMISSION_SETTING_TYPE")) {
                    arrayList.add(t);
                }
                if (!t.isGranted()) {
                    arrayList2.add(t);
                }
            }
            if (!arrayList2.isEmpty() || !arrayList.isEmpty()) {
                this.e.d5(this.f);
            } else {
                this.e.close();
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.PermissionFragment");
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    @Override // com.fossil.Cz6
    public boolean n(String str) {
        Wg6.c(str, "permission");
        Boolean e0 = this.g.e0(str);
        Wg6.b(e0, "mSharedPreferencesManage\u2026rstTimeAsking(permission)");
        return e0.booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.Cz6
    public void o(String str, boolean z) {
        Wg6.c(str, "permission");
        this.g.f1(str, Boolean.valueOf(z));
    }

    @DexIgnore
    public void p() {
        this.e.M5(this);
    }
}
