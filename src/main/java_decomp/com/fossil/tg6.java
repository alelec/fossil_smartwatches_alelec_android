package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg6 extends qg6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public List<ActivitySummary> i; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<h47<List<ActivitySummary>>> j;
    @DexIgnore
    public TreeMap<Long, Float> k;
    @DexIgnore
    public /* final */ rg6 l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ SummariesRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$1", f = "CaloriesOverviewMonthPresenter.kt", l = {101}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tg6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tg6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$loadData$1$currentUser$1", f = "CaloriesOverviewMonthPresenter.kt", l = {101}, m = "invokeSuspend")
        /* renamed from: com.fossil.tg6$a$a  reason: collision with other inner class name */
        public static final class C0237a extends ko7 implements vp7<iv7, qn7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0237a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0237a aVar = new C0237a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super MFUser> qn7) {
                throw null;
                //return ((C0237a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.m;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(tg6 tg6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = tg6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                C0237a aVar = new C0237a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                this.this$0.h = lk5.q0(mFUser.getCreatedAt());
                rg6 rg6 = this.this$0.l;
                Date date = this.this$0.g;
                if (date != null) {
                    Date date2 = this.this$0.h;
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    rg6.g(date, date2);
                    this.this$0.f.l(this.this$0.g);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tg6 f3411a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$mActivitySummaries$1$1", f = "CaloriesOverviewMonthPresenter.kt", l = {50, 50}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySummary>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.tg6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(tg6 tg6) {
            this.f3411a = tg6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySummary>>> apply(Date date) {
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ tg6 f3412a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$1$1", f = "CaloriesOverviewMonthPresenter.kt", l = {74}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tg6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter$start$1$1$1", f = "CaloriesOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.tg6$c$a$a  reason: collision with other inner class name */
            public static final class C0238a extends ko7 implements vp7<iv7, qn7<? super TreeMap<Long, Float>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0238a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0238a aVar = new C0238a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super TreeMap<Long, Float>> qn7) {
                    throw null;
                    //return ((C0238a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        tg6 tg6 = this.this$0.this$0.f3412a;
                        Object e = tg6.f.e();
                        if (e != null) {
                            pq7.b(e, "mDateLiveData.value!!");
                            return tg6.F((Date) e, this.this$0.$data);
                        }
                        pq7.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                tg6 tg6;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    this.this$0.f3412a.i = this.$data;
                    tg6 tg62 = this.this$0.f3412a;
                    dv7 h = tg62.h();
                    C0238a aVar = new C0238a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = tg62;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    tg6 = tg62;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    tg6 = (tg6) this.L$1;
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                tg6.k = (TreeMap) g;
                rg6 rg6 = this.this$0.f3412a.l;
                TreeMap<Long, Float> treeMap = this.this$0.f3412a.k;
                if (treeMap == null) {
                    treeMap = new TreeMap<>();
                }
                rg6.e(treeMap);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(tg6 tg6) {
            this.f3412a = tg6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<ActivitySummary>> h47) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(h47 != null ? h47.d() : null);
            sb.append(" -- data.size=");
            sb.append((h47 == null || (list = (List) h47.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("CaloriesOverviewMonthPresenter", sb.toString());
            if ((h47 != null ? h47.d() : null) != xh5.DATABASE_LOADING) {
                List list2 = h47 != null ? (List) h47.c() : null;
                if (list2 != null && (!pq7.a(this.f3412a.i, list2))) {
                    xw7 unused = gu7.d(this.f3412a.k(), null, null, new a(this, list2, null), 3, null);
                }
            }
        }
    }

    @DexIgnore
    public tg6(rg6 rg6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        pq7.c(rg6, "mView");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(portfolioApp, "mApp");
        this.l = rg6;
        this.m = userRepository;
        this.n = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        LiveData<h47<List<ActivitySummary>>> c2 = ss0.c(this.f, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.j = c2;
    }

    @DexIgnore
    public void D() {
        Date date = this.g;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.g = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthPresenter", "loadData - mDateLiveData=" + this.g);
            xw7 unused = gu7.d(k(), null, null, new a(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("CaloriesOverviewMonthPresenter", "loadData - mDateLiveData=" + this.g);
    }

    @DexIgnore
    public void E() {
        this.l.M5(this);
    }

    @DexIgnore
    public final TreeMap<Long, Float> F(Date date, List<ActivitySummary> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("CaloriesOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (ActivitySummary activitySummary : list) {
                instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay(), 0, 0, 0);
                instance.set(14, 0);
                if (activitySummary.getCaloriesGoal() > 0) {
                    pq7.b(instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) activitySummary.getCalories()) / ((float) activitySummary.getCaloriesGoal())));
                } else {
                    pq7.b(instance, "calendar");
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                }
            }
        }
        return treeMap;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        D();
        LiveData<h47<List<ActivitySummary>>> liveData = this.j;
        rg6 rg6 = this.l;
        if (rg6 != null) {
            liveData.h((sg6) rg6, new c(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthPresenter", "stop");
        try {
            LiveData<h47<List<ActivitySummary>>> liveData = this.j;
            rg6 rg6 = this.l;
            if (rg6 != null) {
                liveData.n((sg6) rg6);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qg6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.qg6
    public void o(Date date) {
        pq7.c(date, "date");
        if (this.f.e() == null || !lk5.m0(this.f.e(), date)) {
            this.f.l(date);
        }
    }
}
