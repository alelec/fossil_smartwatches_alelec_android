package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ir2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ir2> CREATOR; // = new Jr2();
    @DexIgnore
    public static /* final */ List<Zb2> i; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest b;
    @DexIgnore
    public List<Zb2> c;
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public String h;

    @DexIgnore
    public Ir2(LocationRequest locationRequest, List<Zb2> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.b = locationRequest;
        this.c = list;
        this.d = str;
        this.e = z;
        this.f = z2;
        this.g = z3;
        this.h = str2;
    }

    @DexIgnore
    @Deprecated
    public static Ir2 c(LocationRequest locationRequest) {
        return new Ir2(locationRequest, i, null, false, false, false, null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Ir2)) {
            return false;
        }
        Ir2 ir2 = (Ir2) obj;
        return Pc2.a(this.b, ir2.b) && Pc2.a(this.c, ir2.c) && Pc2.a(this.d, ir2.d) && this.e == ir2.e && this.f == ir2.f && this.g == ir2.g && Pc2.a(this.h, ir2.h);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        if (this.d != null) {
            sb.append(" tag=");
            sb.append(this.d);
        }
        if (this.h != null) {
            sb.append(" moduleId=");
            sb.append(this.h);
        }
        sb.append(" hideAppOps=");
        sb.append(this.e);
        sb.append(" clients=");
        sb.append(this.c);
        sb.append(" forceCoarseLocation=");
        sb.append(this.f);
        if (this.g) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, this.b, i2, false);
        Bd2.y(parcel, 5, this.c, false);
        Bd2.u(parcel, 6, this.d, false);
        Bd2.c(parcel, 7, this.e);
        Bd2.c(parcel, 8, this.f);
        Bd2.c(parcel, 9, this.g);
        Bd2.u(parcel, 10, this.h, false);
        Bd2.b(parcel, a2);
    }
}
