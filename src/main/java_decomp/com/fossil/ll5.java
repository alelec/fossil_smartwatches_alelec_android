package com.fossil;

import android.text.SpannableString;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ll5 {
    @DexIgnore
    public static int a(int i) {
        if (i >= 60) {
            return i / 60;
        }
        return 0;
    }

    @DexIgnore
    public static int b(int i) {
        return i >= 60 ? i - ((i / 60) * 60) : i;
    }

    @DexIgnore
    public static String c(Date date) {
        return DateFormat.format("MMMM dd", date).toString();
    }

    @DexIgnore
    public static String d(Date date) {
        return DateFormat.format("MMM d", date).toString();
    }

    @DexIgnore
    public static int e(int i) {
        if (i < 1 || i > 12) {
            return -1;
        }
        return (i * 30) - 1;
    }

    @DexIgnore
    public static String f(Date date) {
        return DateFormat.format("HH:mm", date).toString();
    }

    @DexIgnore
    public static String g(int i) {
        int i2 = i / 60;
        int i3 = i % 60;
        if (i2 == 0) {
            return i3 + " " + Um5.c(PortfolioApp.d0, 2131886118);
        } else if (i2 <= 0 || i3 != 0) {
            return String.format(Um5.c(PortfolioApp.d0, 2131886696), Integer.valueOf(i2), Integer.valueOf(i3));
        } else {
            return i2 + " " + Um5.c(PortfolioApp.d0, 2131886117);
        }
    }

    @DexIgnore
    public static SpannableString h(int i) {
        int i2 = 12;
        int i3 = i / 60;
        int i4 = i % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.d0)) {
            return new SpannableString(String.format(Locale.US, "%02d", Integer.valueOf(i3)) + ":" + String.format(Locale.US, "%02d", Integer.valueOf(i4)));
        } else if (i < 720) {
            if (i3 != 0) {
                i2 = i3;
            }
            return Jl5.b.g(String.format(Locale.US, "%02d", Integer.valueOf(i2)) + ":" + String.format(Locale.US, "%02d", Integer.valueOf(i4)), Um5.c(PortfolioApp.d0, 2131886102), 1.0f);
        } else {
            if (i3 > 12) {
                i2 = i3 - 12;
            }
            return Jl5.b.g(String.format(Locale.US, "%02d", Integer.valueOf(i2)) + ":" + String.format(Locale.US, "%02d", Integer.valueOf(i4)), Um5.c(PortfolioApp.d0, 2131886104), 1.0f);
        }
    }

    @DexIgnore
    public static int i() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }

    @DexIgnore
    public static int j(String str) {
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (!timeZone.inDaylightTime(Calendar.getInstance().getTime())) {
            return timeZone.getRawOffset() / 60000;
        }
        return (timeZone.getDSTSavings() + timeZone.getRawOffset()) / 60000;
    }
}
