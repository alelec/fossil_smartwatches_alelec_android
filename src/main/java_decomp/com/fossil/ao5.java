package com.fossil;

import android.net.ConnectivityManager;
import android.net.NetworkRequest;
import android.os.Build;
import com.mapped.Gg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ao5 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ Yk7<ConnectivityManager> b; // = Zk7.a(Ai.INSTANCE);
    @DexIgnore
    public static /* final */ Ao5 c; // = new Ao5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<ConnectivityManager> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final ConnectivityManager invoke() {
            if (Build.VERSION.SDK_INT >= 24) {
                return (ConnectivityManager) PortfolioApp.get.instance().getSystemService(ConnectivityManager.class);
            }
            Object systemService = PortfolioApp.get.instance().getSystemService("connectivity");
            if (systemService != null) {
                return (ConnectivityManager) systemService;
            }
            throw new Rc6("null cannot be cast to non-null type android.net.ConnectivityManager");
        }
    }

    /*
    static {
        String simpleName = Ao5.class.getSimpleName();
        Wg6.b(simpleName, "NetworkStateChangeManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void a(Zn5 zn5) {
        Wg6.c(zn5, "stateChangeCallback");
        FLogger.INSTANCE.getLocal().d(a, "registerNetworkCallback");
        ConnectivityManager value = b.getValue();
        if (value != null) {
            value.registerNetworkCallback(new NetworkRequest.Builder().build(), zn5);
        }
    }

    @DexIgnore
    public final void b(Zn5 zn5) {
        Wg6.c(zn5, "stateChangeCallback");
        FLogger.INSTANCE.getLocal().d(a, "unregisterNetworkCallback");
        ConnectivityManager value = b.getValue();
        if (value != null) {
            value.unregisterNetworkCallback(zn5);
        }
    }
}
