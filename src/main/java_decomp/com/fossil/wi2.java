package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.Pc2;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wi2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Wi2> CREATOR; // = new Aj2();
    @DexIgnore
    public static /* final */ TimeUnit f; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ Zh2 b;
    @DexIgnore
    public /* final */ List<DataSet> c;
    @DexIgnore
    public /* final */ List<DataPoint> d;
    @DexIgnore
    public /* final */ Mo2 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Zh2 a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<Uh2> d; // = new ArrayList();

        @DexIgnore
        public Ai a(DataSet dataSet) {
            Rc2.b(dataSet != null, "Must specify a valid data set.");
            Uh2 A = dataSet.A();
            Rc2.p(!this.d.contains(A), "Data set for this data source %s is already added.", A);
            Rc2.b(!dataSet.k().isEmpty(), "No data points specified in the input data set.");
            this.d.add(A);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public Wi2 b() {
            boolean z = true;
            Rc2.o(this.a != null, "Must specify a valid session.");
            if (this.a.f(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            Rc2.o(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet dataSet : this.b) {
                for (DataPoint dataPoint : dataSet.k()) {
                    g(dataPoint);
                }
            }
            for (DataPoint dataPoint2 : this.c) {
                g(dataPoint2);
            }
            return new Wi2(this);
        }

        @DexIgnore
        public Ai c(Zh2 zh2) {
            this.a = zh2;
            return this;
        }

        @DexIgnore
        public final void g(DataPoint dataPoint) {
            long A = this.a.A(TimeUnit.NANOSECONDS);
            long f = this.a.f(TimeUnit.NANOSECONDS);
            long F = dataPoint.F(TimeUnit.NANOSECONDS);
            if (F != 0) {
                long a2 = (F < A || F > f) ? Fp2.a(F, TimeUnit.NANOSECONDS, Wi2.f) : F;
                Rc2.p(a2 >= A && a2 <= f, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint, Long.valueOf(A), Long.valueOf(f));
                if (dataPoint.F(TimeUnit.NANOSECONDS) != a2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.F(TimeUnit.NANOSECONDS)), Long.valueOf(a2), Wi2.f));
                    dataPoint.p0(a2, TimeUnit.NANOSECONDS);
                }
            }
            long A2 = this.a.A(TimeUnit.NANOSECONDS);
            long f2 = this.a.f(TimeUnit.NANOSECONDS);
            long D = dataPoint.D(TimeUnit.NANOSECONDS);
            long k = dataPoint.k(TimeUnit.NANOSECONDS);
            if (D != 0 && k != 0) {
                if (k > f2) {
                    k = Fp2.a(k, TimeUnit.NANOSECONDS, Wi2.f);
                }
                Rc2.p(D >= A2 && k <= f2, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint, Long.valueOf(A2), Long.valueOf(f2));
                if (k != dataPoint.k(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", Long.valueOf(dataPoint.k(TimeUnit.NANOSECONDS)), Long.valueOf(k), Wi2.f));
                    dataPoint.o0(D, k, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public Wi2(Ai ai) {
        this(ai.a, ai.b, ai.c, (Mo2) null);
    }

    @DexIgnore
    public Wi2(Wi2 wi2, Mo2 mo2) {
        this(wi2.b, wi2.c, wi2.d, mo2);
    }

    @DexIgnore
    public Wi2(Zh2 zh2, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.b = zh2;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
        this.e = Oo2.e(iBinder);
    }

    @DexIgnore
    public Wi2(Zh2 zh2, List<DataSet> list, List<DataPoint> list2, Mo2 mo2) {
        this.b = zh2;
        this.c = Collections.unmodifiableList(list);
        this.d = Collections.unmodifiableList(list2);
        this.e = mo2;
    }

    @DexIgnore
    public List<DataPoint> c() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (!(obj instanceof Wi2)) {
                return false;
            }
            Wi2 wi2 = (Wi2) obj;
            if (!(Pc2.a(this.b, wi2.b) && Pc2.a(this.c, wi2.c) && Pc2.a(this.d, wi2.d))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public List<DataSet> f() {
        return this.c;
    }

    @DexIgnore
    public Zh2 h() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(this.b, this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a(Constants.SESSION, this.b);
        c2.a("dataSets", this.c);
        c2.a("aggregateDataPoints", this.d);
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, h(), i, false);
        Bd2.y(parcel, 2, f(), false);
        Bd2.y(parcel, 3, c(), false);
        Mo2 mo2 = this.e;
        Bd2.m(parcel, 4, mo2 == null ? null : mo2.asBinder(), false);
        Bd2.b(parcel, a2);
    }
}
