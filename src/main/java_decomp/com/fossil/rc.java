package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Q40;
import com.mapped.U40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rc extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Sc b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Rc(Sc sc) {
        super(1);
        this.b = sc;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Yx1 yx1) {
        Zx1 errorCode = yx1.getErrorCode();
        if (errorCode == U40.SECRET_KEY_IS_REQUIRED || errorCode == U40.AUTHENTICATION_FAILED) {
            Sc sc = this.b;
            E60 e60 = sc.b;
            Rp1 rp1 = new Rp1(sc.c.c);
            Q40.Bi bi = e60.w;
            if (bi != null) {
                bi.onEventReceived(e60, rp1);
            }
        }
        return Cd6.a;
    }
}
