package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Oz0 extends Uy0 {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    public static int d(Wy0 wy0, int i) {
        if (wy0 == null) {
            return -1;
        }
        int[] iArr = (int[]) wy0.a.get("android:visibilityPropagation:center");
        if (iArr == null) {
            return -1;
        }
        return iArr[i];
    }

    @DexIgnore
    @Override // com.fossil.Uy0
    public void a(Wy0 wy0) {
        View view = wy0.b;
        Integer num = (Integer) wy0.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        wy0.a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = (view.getHeight() / 2) + iArr[1];
        wy0.a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    @Override // com.fossil.Uy0
    public String[] b() {
        return a;
    }

    @DexIgnore
    public int e(Wy0 wy0) {
        if (wy0 == null) {
            return 8;
        }
        Integer num = (Integer) wy0.a.get("android:visibilityPropagation:visibility");
        if (num == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int f(Wy0 wy0) {
        return d(wy0, 0);
    }

    @DexIgnore
    public int g(Wy0 wy0) {
        return d(wy0, 1);
    }
}
