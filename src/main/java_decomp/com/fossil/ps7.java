package com.fossil;

import com.mapped.Wg6;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ps7<T> implements Ts7<T> {
    @DexIgnore
    public /* final */ AtomicReference<Ts7<T>> a;

    @DexIgnore
    public Ps7(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "sequence");
        this.a = new AtomicReference<>(ts7);
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<T> iterator() {
        Ts7<T> andSet = this.a.getAndSet(null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
