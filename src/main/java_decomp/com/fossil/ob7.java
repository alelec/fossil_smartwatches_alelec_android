package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ob7 {
    @DexIgnore
    public Hb7 a;
    @DexIgnore
    public List<Gb7> b;

    @DexIgnore
    public Ob7(Hb7 hb7, List<Gb7> list) {
        Wg6.c(list, "movingObjects");
        this.a = hb7;
        this.b = list;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ob7(Hb7 hb7, List list, int i, Qg6 qg6) {
        this(hb7, (i & 2) != 0 ? new ArrayList() : list);
    }

    @DexIgnore
    public final List<Gb7> a() {
        return this.b;
    }

    @DexIgnore
    public final Hb7 b() {
        return this.a;
    }

    @DexIgnore
    public final void c(List<Gb7> list) {
        Wg6.c(list, "<set-?>");
        this.b = list;
    }

    @DexIgnore
    public final void d(Hb7 hb7) {
        this.a = hb7;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Ob7 ob7 = (Ob7) (!(obj instanceof Ob7) ? null : obj);
        if (ob7 == null) {
            return false;
        }
        return Wg6.a(this.a, ob7.a) && Cc7.a(this.b, ob7.b);
    }

    @DexIgnore
    public int hashCode() {
        Hb7 hb7 = this.a;
        return ((hb7 != null ? hb7.hashCode() : 0) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "WFThemeData(wfBackground=" + this.a + ", movingObjects=" + this.b + ")";
    }
}
