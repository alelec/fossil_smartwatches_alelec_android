package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xr5 implements Factory<DianaNotificationComponent> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<DNDSettingsDatabase> b;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> d;

    @DexIgnore
    public Xr5(Provider<An4> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static Xr5 a(Provider<An4> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        return new Xr5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static DianaNotificationComponent c(An4 an4, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new DianaNotificationComponent(an4, dNDSettingsDatabase, quickResponseRepository, notificationSettingsDatabase);
    }

    @DexIgnore
    public DianaNotificationComponent b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
