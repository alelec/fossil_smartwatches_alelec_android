package com.fossil;

import android.util.Log;
import com.fossil.Ag7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yf7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Ag7.Ai b;

    @DexIgnore
    public Yf7(Ag7.Ai ai) {
        this.b = ai;
    }

    @DexIgnore
    public void run() {
        if (Ag7.e() != null && Ag7.Ai.a(this.b)) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onBackground");
            Hg7.d(Ag7.Ai.c(this.b), "onBackground_WX", null);
            Ag7.Ai.b(this.b, false);
        }
    }
}
