package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a37 extends iq4<a, Object, b> {
    @DexIgnore
    public /* final */ v36 d;
    @DexIgnore
    public /* final */ d26 e;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase f;
    @DexIgnore
    public /* final */ NotificationsRepository g;
    @DexIgnore
    public /* final */ DeviceRepository h;
    @DexIgnore
    public /* final */ on5 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f189a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, "deviceId");
            this.f189a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f189a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {
        @DexIgnore
        public b(String str) {
            pq7.c(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.SetNotificationUseCase", f = "SetNotificationUseCase.kt", l = {38}, m = "run")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ a37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(a37 a37, qn7 qn7) {
            super(qn7);
            this.this$0 = a37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public a37(v36 v36, d26 d26, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, on5 on5) {
        pq7.c(v36, "mGetApp");
        pq7.c(d26, "mGetAllContactGroups");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        pq7.c(notificationsRepository, "mNotificationsRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(on5, "mSharePref");
        this.d = v36;
        this.e = d26;
        this.f = notificationSettingsDatabase;
        this.g = notificationsRepository;
        this.h = deviceRepository;
        this.i = on5;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetNotificationUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.a37.a r10, com.fossil.qn7<java.lang.Object> r11) {
        /*
        // Method dump skipped, instructions count: 261
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a37.k(com.fossil.a37$a, com.fossil.qn7):java.lang.Object");
    }
}
