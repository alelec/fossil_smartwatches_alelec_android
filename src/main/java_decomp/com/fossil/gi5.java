package com.fossil;

import com.fossil.fitness.WorkoutMode;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Gi5 {
    UNKNOWN("UNKNOWN"),
    INDOOR("INDOOR"),
    OUTDOOR("OUTDOOR");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Gi5 a(int i) {
            return i == WorkoutMode.OUTDOOR.ordinal() ? Gi5.OUTDOOR : i == WorkoutMode.INDOOR.ordinal() ? Gi5.INDOOR : Gi5.INDOOR;
        }

        @DexIgnore
        public final WorkoutMode b(Gi5 gi5) {
            if (gi5 != null) {
                int i = Fi5.a[gi5.ordinal()];
                if (i == 1) {
                    return WorkoutMode.OUTDOOR;
                }
                if (i == 2) {
                    return WorkoutMode.INDOOR;
                }
            }
            return WorkoutMode.INDOOR;
        }
    }

    @DexIgnore
    public Gi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
