package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Fw5;
import com.mapped.AlertDialogFragment;
import com.mapped.MicroAppsAdapter;
import com.mapped.PermissionUtils;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ub6 extends BaseFragment implements Jc6, AlertDialogFragment.Gi {
    @DexIgnore
    public G37<P85> g;
    @DexIgnore
    public Ic6 h;
    @DexIgnore
    public Fw5 i;
    @DexIgnore
    public MicroAppsAdapter j;
    @DexIgnore
    public Po4 k;
    @DexIgnore
    public HybridCustomizeViewModel l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements MicroAppsAdapter.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ub6 a;

        @DexIgnore
        public Ai(Ub6 ub6) {
            this.a = ub6;
        }

        @DexIgnore
        @Override // com.mapped.MicroAppsAdapter.Bi
        public void a(MicroApp microApp) {
            Wg6.c(microApp, "microApp");
            Ub6.K6(this.a).q(microApp.getId());
        }

        @DexIgnore
        @Override // com.mapped.MicroAppsAdapter.Bi
        public void b(MicroApp microApp) {
            Wg6.c(microApp, "microApp");
            Jn5.c(Jn5.b, this.a.getContext(), Hl5.a.d(microApp.getId()), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Fw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ub6 a;

        @DexIgnore
        public Bi(Ub6 ub6) {
            this.a = ub6;
        }

        @DexIgnore
        @Override // com.fossil.Fw5.Bi
        public void a(Category category) {
            Wg6.c(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppFragment", "onItemClicked category=" + category);
            Ub6.K6(this.a).p(category);
        }

        @DexIgnore
        @Override // com.fossil.Fw5.Bi
        public void b() {
            Ub6.K6(this.a).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ub6 b;

        @DexIgnore
        public Ci(Ub6 ub6) {
            this.b = ub6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Ub6.K6(this.b).o();
        }
    }

    @DexIgnore
    public static final /* synthetic */ Ic6 K6(Ub6 ub6) {
        Ic6 ic6 = ub6.h;
        if (ic6 != null) {
            return ic6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void A1(MicroApp microApp) {
        if (microApp != null) {
            MicroAppsAdapter microAppsAdapter = this.j;
            if (microAppsAdapter != null) {
                microAppsAdapter.q(microApp.getId());
                N6(microApp);
                return;
            }
            Wg6.n("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "MicroAppFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final void L6() {
        if (isActive()) {
            MicroAppsAdapter microAppsAdapter = this.j;
            if (microAppsAdapter != null) {
                microAppsAdapter.j();
            } else {
                Wg6.n("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ic6 ic6) {
        M6(ic6);
    }

    @DexIgnore
    public void M6(Ic6 ic6) {
        Wg6.c(ic6, "presenter");
        this.h = ic6;
    }

    @DexIgnore
    public final void N6(MicroApp microApp) {
        G37<P85> g37 = this.g;
        if (g37 != null) {
            P85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.y;
                Wg6.b(flexibleTextView, "binding.tvSelectedMicroApp");
                flexibleTextView.setText(Um5.d(PortfolioApp.get.instance(), microApp.getNameKey(), microApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.u;
                Wg6.b(flexibleTextView2, "binding.tvMicroAppDetail");
                flexibleTextView2.setText(Um5.d(PortfolioApp.get.instance(), microApp.getDescriptionKey(), microApp.getDescription()));
                MicroAppsAdapter microAppsAdapter = this.j;
                if (microAppsAdapter != null) {
                    int k2 = microAppsAdapter.k(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + k2);
                    if (k2 >= 0) {
                        a2.t.scrollToPosition(k2);
                        return;
                    }
                    return;
                }
                Wg6.n("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        FragmentActivity activity2;
        FragmentActivity activity3;
        Wg6.c(str, "tag");
        if (Wg6.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131363373 && (activity3 = getActivity()) != null) {
                if (activity3 != null) {
                    ((BaseActivity) activity3).x();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (Wg6.a(str, S37.c.f())) {
            if (i2 == 2131363373 && (activity2 = getActivity()) != null) {
                if (!V78.d(this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    PermissionUtils.a.t(this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity2 != null) {
                    ((BaseActivity) activity2).x();
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (Wg6.a(str, "LOCATION_PERMISSION_TAG")) {
            if (i2 == 2131363373 && (activity = getActivity()) != null) {
                if (activity != null) {
                    ((BaseActivity) activity).x();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (Wg6.a(str, InAppPermission.NOTIFICATION_ACCESS) && i2 == 2131363373) {
            startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void S(List<Category> list) {
        Wg6.c(list, "categories");
        Fw5 fw5 = this.i;
        if (fw5 != null) {
            fw5.n(list);
        } else {
            Wg6.n("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void U5(List<MicroApp> list) {
        Wg6.c(list, "microApps");
        MicroAppsAdapter microAppsAdapter = this.j;
        if (microAppsAdapter != null) {
            microAppsAdapter.o(list);
        } else {
            Wg6.n("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void a0(String str, String str2, String str3) {
        Wg6.c(str, "topMicroApp");
        Wg6.c(str2, "middleMicroApp");
        Wg6.c(str3, "bottomMicroApp");
        SearchMicroAppActivity.B.a(this, str, str2, str3);
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void a5(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void d1(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void f0(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.B.a(this, str);
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void g0(boolean z, String str, String str2, String str3) {
        Wg6.c(str, "microAppId");
        Wg6.c(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        G37<P85> g37 = this.g;
        if (g37 != null) {
            P85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                Wg6.b(flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView2 = a2.w;
                    Wg6.b(flexibleTextView2, "it.tvMicroAppSetting");
                    flexibleTextView2.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView3 = a2.w;
                        Wg6.b(flexibleTextView3, "it.tvMicroAppSetting");
                        flexibleTextView3.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView4 = a2.w;
                    Wg6.b(flexibleTextView4, "it.tvMicroAppSetting");
                    flexibleTextView4.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView5 = a2.w;
                Wg6.b(flexibleTextView5, "it.tvMicroAppSetting");
                flexibleTextView5.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void g3(String str) {
        Wg6.c(str, "content");
        G37<P85> g37 = this.g;
        if (g37 != null) {
            P85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.tvMicroAppDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            Po4 po4 = this.k;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(hybridCustomizeEditActivity, po4).a(HybridCustomizeViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                HybridCustomizeViewModel hybridCustomizeViewModel = (HybridCustomizeViewModel) a2;
                this.l = hybridCustomizeViewModel;
                Ic6 ic6 = this.h;
                if (ic6 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (hybridCustomizeViewModel != null) {
                    ic6.r(hybridCustomizeViewModel);
                } else {
                    Wg6.n("mShareViewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        Ringtone ringtone;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppFragment", "onActivityResult requestCode " + i2 + " resultCode " + i3);
        if (i2 != 100) {
            if (i2 != 102) {
                if (i2 != 104) {
                    if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                        Ic6 ic6 = this.h;
                        if (ic6 != null) {
                            ic6.s(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), Jj5.a(commuteTimeSetting));
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    }
                } else if (intent != null && i3 == -1 && (ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE")) != null) {
                    Ic6 ic62 = this.h;
                    if (ic62 != null) {
                        ic62.s(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), Jj5.a(ringtone));
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    Ic6 ic63 = this.h;
                    if (ic63 != null) {
                        Wg6.b(stringExtra, "selectedMicroAppId");
                        ic63.q(stringExtra);
                        return;
                    }
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            Ic6 ic64 = this.h;
            if (ic64 != null) {
                ic64.s(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), Jj5.a(secondTimezoneSetting));
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        P85 p85 = (P85) Aq0.f(layoutInflater, 2131558585, viewGroup, false, A6());
        PortfolioApp.get.instance().getIface().g1(new Kc6(this)).a(this);
        this.g = new G37<>(this, p85);
        Wg6.b(p85, "binding");
        return p85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Ic6 ic6 = this.h;
        if (ic6 != null) {
            ic6.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Ic6 ic6 = this.h;
        if (ic6 != null) {
            ic6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        MicroAppsAdapter microAppsAdapter = new MicroAppsAdapter(null, null, 3, null);
        microAppsAdapter.p(new Ai(this));
        this.j = microAppsAdapter;
        Fw5 fw5 = new Fw5(null, null, 3, null);
        fw5.o(new Bi(this));
        this.i = fw5;
        G37<P85> g37 = this.g;
        if (g37 != null) {
            P85 a2 = g37.a();
            if (a2 != null) {
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    a2.r.setBackgroundColor(Color.parseColor(d));
                }
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                Fw5 fw52 = this.i;
                if (fw52 != null) {
                    recyclerView.setAdapter(fw52);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    MicroAppsAdapter microAppsAdapter2 = this.j;
                    if (microAppsAdapter2 != null) {
                        recyclerView2.setAdapter(microAppsAdapter2);
                        a2.w.setOnClickListener(new Ci(this));
                        return;
                    }
                    Wg6.n("mMicroAppAdapter");
                    throw null;
                }
                Wg6.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jc6
    public void u0(String str) {
        Wg6.c(str, "category");
        G37<P85> g37 = this.g;
        if (g37 != null) {
            P85 a2 = g37.a();
            if (a2 != null) {
                Fw5 fw5 = this.i;
                if (fw5 != null) {
                    int j2 = fw5.j(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppFragment", "scrollToCategory category=" + str + " scrollTo " + j2);
                    if (j2 >= 0) {
                        Fw5 fw52 = this.i;
                        if (fw52 != null) {
                            fw52.p(j2);
                            a2.s.smoothScrollToPosition(j2);
                            return;
                        }
                        Wg6.n("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                Wg6.n("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
