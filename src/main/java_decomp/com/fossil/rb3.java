package com.fossil;

import android.content.Context;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rb3 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static int a(Context context) {
        int i = 0;
        synchronized (Rb3.class) {
            try {
                Rc2.l(context, "Context is null");
                if (!a) {
                    try {
                        Md3 a2 = Jd3.a(context);
                        try {
                            Pb3.k(a2.zze());
                            Ae3.e(a2.zzf());
                            a = true;
                        } catch (RemoteException e) {
                            throw new Se3(e);
                        }
                    } catch (E62 e2) {
                        i = e2.errorCode;
                    }
                }
            } finally {
            }
        }
        return i;
    }
}
