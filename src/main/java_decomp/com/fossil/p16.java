package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P16 implements Factory<NotificationContactsPresenter> {
    @DexIgnore
    public static NotificationContactsPresenter a(J16 j16, Uq4 uq4, D26 d26, G26 g26, LoaderManager loaderManager) {
        return new NotificationContactsPresenter(j16, uq4, d26, g26, loaderManager);
    }
}
