package com.fossil;

import java.nio.charset.Charset;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sl4 implements Ql4 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("ISO-8859-1");

    @DexIgnore
    public static Bm4 b(String str, Kl4 kl4, int i, int i2, Charset charset, int i3, int i4) {
        if (kl4 == Kl4.AZTEC) {
            return c(Vl4.d(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got " + kl4);
    }

    @DexIgnore
    public static Bm4 c(Tl4 tl4, int i, int i2) {
        Bm4 a2 = tl4.a();
        if (a2 != null) {
            int l = a2.l();
            int j = a2.j();
            int max = Math.max(i, l);
            int max2 = Math.max(i2, j);
            int min = Math.min(max / l, max2 / j);
            int i3 = (max - (l * min)) / 2;
            Bm4 bm4 = new Bm4(max, max2);
            int i4 = 0;
            int i5 = (max2 - (j * min)) / 2;
            while (i4 < j) {
                int i6 = 0;
                int i7 = i3;
                while (i6 < l) {
                    if (a2.i(i6, i4)) {
                        bm4.o(i7, i5, min, min);
                    }
                    i6++;
                    i7 += min;
                }
                i4++;
                i5 += min;
            }
            return bm4;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) {
        int i3;
        int i4;
        Charset charset;
        int i5 = 33;
        Charset charset2 = a;
        if (map != null) {
            if (map.containsKey(Ml4.CHARACTER_SET)) {
                charset2 = Charset.forName(map.get(Ml4.CHARACTER_SET).toString());
            }
            if (map.containsKey(Ml4.ERROR_CORRECTION)) {
                i5 = Integer.parseInt(map.get(Ml4.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(Ml4.AZTEC_LAYERS)) {
                i3 = Integer.parseInt(map.get(Ml4.AZTEC_LAYERS).toString());
                i4 = i5;
                charset = charset2;
                return b(str, kl4, i, i2, charset, i4, i3);
            }
        }
        i3 = 0;
        i4 = i5;
        charset = charset2;
        return b(str, kl4, i, i2, charset, i4, i3);
    }
}
