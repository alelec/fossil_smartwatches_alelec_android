package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mc4 {
    @DexIgnore
    public static /* final */ Cb4 b; // = new Cb4();
    @DexIgnore
    public static /* final */ String c; // = d("hts/cahyiseot-agolai.o/1frlglgc/aclg", "tp:/rsltcrprsp.ogepscmv/ieo/eaybtho");
    @DexIgnore
    public static /* final */ String d; // = d("AzSBpY4F0rHiHFdinTvM", "IayrSTFL9eJ69YeSUO2");
    @DexIgnore
    public static /* final */ Wy1<Ta4, byte[]> e; // = Lc4.a();
    @DexIgnore
    public /* final */ Xy1<Ta4> a;

    @DexIgnore
    public Mc4(Xy1<Ta4> xy1, Wy1<Ta4, byte[]> wy1) {
        this.a = xy1;
    }

    @DexIgnore
    public static Mc4 a(Context context) {
        M02.f(context);
        return new Mc4(M02.c().g(new Az1(c, d)).b("FIREBASE_CRASHLYTICS_REPORT", Ta4.class, Ty1.b("json"), e), e);
    }

    @DexIgnore
    public static /* synthetic */ void b(Ot3 ot3, Z84 z84, Exception exc) {
        if (exc != null) {
            ot3.d(exc);
        } else {
            ot3.e(z84);
        }
    }

    @DexIgnore
    public static String d(String str, String str2) {
        int length = str.length() - str2.length();
        if (length < 0 || length > 1) {
            throw new IllegalArgumentException("Invalid input received");
        }
        StringBuilder sb = new StringBuilder(str.length() + str2.length());
        for (int i = 0; i < str.length(); i++) {
            sb.append(str.charAt(i));
            if (str2.length() > i) {
                sb.append(str2.charAt(i));
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public Nt3<Z84> e(Z84 z84) {
        Ta4 b2 = z84.b();
        Ot3 ot3 = new Ot3();
        this.a.b(Uy1.f(b2), Kc4.b(ot3, z84));
        return ot3.a();
    }
}
