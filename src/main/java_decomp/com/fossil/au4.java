package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Au4 implements Factory<FriendRepository> {
    @DexIgnore
    public /* final */ Provider<Xt4> a;
    @DexIgnore
    public /* final */ Provider<FriendRemoteDataSource> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Au4(Provider<Xt4> provider, Provider<FriendRemoteDataSource> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Au4 a(Provider<Xt4> provider, Provider<FriendRemoteDataSource> provider2, Provider<An4> provider3) {
        return new Au4(provider, provider2, provider3);
    }

    @DexIgnore
    public static FriendRepository c(Xt4 xt4, FriendRemoteDataSource friendRemoteDataSource, An4 an4) {
        return new FriendRepository(xt4, friendRemoteDataSource, an4);
    }

    @DexIgnore
    public FriendRepository b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
