package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O44<E> extends Y24<E> {
    @DexIgnore
    public static /* final */ Y24<Object> EMPTY; // = new O44(H44.a);
    @DexIgnore
    public /* final */ transient Object[] b;

    @DexIgnore
    public O44(Object[] objArr) {
        this.b = objArr;
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.Y24
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.b;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return this.b.length + i;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.b[i];
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.Y24, com.fossil.Y24
    public I54<E> listIterator(int i) {
        Object[] objArr = this.b;
        return P34.m(objArr, 0, objArr.length, i);
    }

    @DexIgnore
    public int size() {
        return this.b.length;
    }
}
