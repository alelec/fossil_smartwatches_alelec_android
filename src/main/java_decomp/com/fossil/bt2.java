package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bt2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle f;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Bt2(Zs2 zs2, Bundle bundle) {
        super(zs2);
        this.g = zs2;
        this.f = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.g.h.setConditionalUserProperty(this.f, this.b);
    }
}
