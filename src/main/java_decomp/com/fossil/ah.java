package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ah extends Lp {
    @DexIgnore
    public Zk1 C;
    @DexIgnore
    public /* final */ ArrayList<Ow> D; // = By1.a(this.i, Hm7.c(Ow.d, Ow.e));

    @DexIgnore
    public Ah(K5 k5, I60 i60, String str) {
        super(k5, i60, Yp.A0, str, false, 16);
        this.C = new Zk1(k5.C(), k5.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    public static final /* synthetic */ void G(Ah ah) {
        K5 k5 = ah.w;
        Lp.h(ah, new Ij(k5, ah.x, Ke.b.a(k5.x, Ob.o), ah.z), new Tq(ah), new Hr(ah), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Av(this.w), new Zf(this), new Ng(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.b, this.C.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.D;
    }
}
