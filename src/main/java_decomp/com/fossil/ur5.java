package com.fossil;

import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ur5<T> {
    @DexIgnore
    public /* final */ Set<T> a; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ Object b; // = new Object();

    @DexIgnore
    public final boolean a(T t) {
        boolean add;
        synchronized (this.b) {
            add = !this.a.contains(t) ? this.a.add(t) : false;
        }
        return add;
    }

    @DexIgnore
    public final T b(Hg6<? super T, Boolean> hg6) {
        T t;
        Wg6.c(hg6, "predicate");
        synchronized (this.b) {
            Iterator<T> it = this.a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (hg6.invoke(next).booleanValue()) {
                    t = next;
                    break;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public final boolean c(T t) {
        boolean remove;
        synchronized (this.b) {
            remove = this.a.remove(t);
        }
        return remove;
    }
}
