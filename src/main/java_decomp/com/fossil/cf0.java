package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.fossil.Cg0;
import com.fossil.Ig0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cf0 extends ActionBar {
    @DexIgnore
    public Ch0 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Window.Callback c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ArrayList<ActionBar.a> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Runnable g; // = new Ai();
    @DexIgnore
    public /* final */ Toolbar.e h; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            Cf0.this.A();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Toolbar.e {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.Toolbar.e
        public boolean onMenuItemClick(MenuItem menuItem) {
            return Cf0.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements Ig0.Ai {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.Ig0.Ai
        public void b(Cg0 cg0, boolean z) {
            if (!this.b) {
                this.b = true;
                Cf0.this.a.h();
                Window.Callback callback = Cf0.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, cg0);
                }
                this.b = false;
            }
        }

        @DexIgnore
        @Override // com.fossil.Ig0.Ai
        public boolean c(Cg0 cg0) {
            Window.Callback callback = Cf0.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, cg0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements Cg0.Ai {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        @Override // com.fossil.Cg0.Ai
        public boolean a(Cg0 cg0, MenuItem menuItem) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Cg0.Ai
        public void b(Cg0 cg0) {
            Cf0 cf0 = Cf0.this;
            if (cf0.c == null) {
                return;
            }
            if (cf0.a.b()) {
                Cf0.this.c.onPanelClosed(108, cg0);
            } else if (Cf0.this.c.onPreparePanel(0, null, cg0)) {
                Cf0.this.c.onMenuOpened(108, cg0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Vf0 {
        @DexIgnore
        public Ei(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        @Override // com.fossil.Vf0
        public View onCreatePanelView(int i) {
            return i == 0 ? new View(Cf0.this.a.getContext()) : super.onCreatePanelView(i);
        }

        @DexIgnore
        @Override // com.fossil.Vf0
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                Cf0 cf0 = Cf0.this;
                if (!cf0.b) {
                    cf0.a.c();
                    Cf0.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public Cf0(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new Uh0(toolbar, false);
        Ei ei = new Ei(callback);
        this.c = ei;
        this.a.setWindowCallback(ei);
        toolbar.setOnMenuItemClickListener(this.h);
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void A() {
        Menu y = y();
        Cg0 cg0 = y instanceof Cg0 ? (Cg0) y : null;
        if (cg0 != null) {
            cg0.h0();
        }
        try {
            y.clear();
            if (!this.c.onCreatePanelMenu(0, y) || !this.c.onPreparePanel(0, null, y)) {
                y.clear();
            }
        } finally {
            if (cg0 != null) {
                cg0.g0();
            }
        }
    }

    @DexIgnore
    public void B(int i, int i2) {
        this.a.k((this.a.s() & i2) | (i & i2));
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean g() {
        return this.a.f();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean h() {
        if (!this.a.j()) {
            return false;
        }
        this.a.collapseActionView();
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void i(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public int j() {
        return this.a.s();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public Context k() {
        return this.a.getContext();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean l() {
        this.a.q().removeCallbacks(this.g);
        Mo0.d0(this.a.q(), this.g);
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void m(Configuration configuration) {
        super.m(configuration);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void n() {
        this.a.q().removeCallbacks(this.g);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean o(int i, KeyEvent keyEvent) {
        Menu y = y();
        if (y == null) {
            return false;
        }
        y.setQwertyMode(KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1);
        return y.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean p(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            q();
        }
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean q() {
        return this.a.g();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void r(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void s(boolean z) {
        B(z ? 4 : 0, 4);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void t(float f2) {
        Mo0.s0(this.a.q(), f2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void u(boolean z) {
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void v(CharSequence charSequence) {
        this.a.setTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void w(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public final Menu y() {
        if (!this.d) {
            this.a.p(new Ci(), new Di());
            this.d = true;
        }
        return this.a.l();
    }

    @DexIgnore
    public Window.Callback z() {
        return this.c;
    }
}
