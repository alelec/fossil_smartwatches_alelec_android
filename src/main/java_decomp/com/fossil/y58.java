package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y58 {
    @DexIgnore
    public static String a(String str) {
        if (str.startsWith("\"")) {
            str = str.substring(1, str.length());
        }
        return str.endsWith("\"") ? str.substring(0, str.length() - 1) : str;
    }

    @DexIgnore
    public static String b(String str) {
        if (str == null) {
            return null;
        }
        return str.startsWith("--") ? str.substring(2, str.length()) : str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR) ? str.substring(1, str.length()) : str;
    }
}
