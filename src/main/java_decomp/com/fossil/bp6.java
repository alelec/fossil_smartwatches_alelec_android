package com.fossil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.Utility;
import com.fossil.iq4;
import com.fossil.vu5;
import com.fossil.xu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp6 extends ts0 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ b l; // = new b(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MFUser f477a;
    @DexIgnore
    public MFUser b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public MutableLiveData<c> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ xu5 i;
    @DexIgnore
    public /* final */ vu5 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f478a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;

        @DexIgnore
        public a() {
            this(null, null, null, 7, null);
        }

        @DexIgnore
        public a(String str, String str2, String str3) {
            this.f478a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(String str, String str2, String str3, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.f478a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f478a, aVar.f478a) || !pq7.a(this.b, aVar.b) || !pq7.a(this.c, aVar.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.f478a;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.b;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.c;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return (((hashCode * 31) + hashCode2) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "CapturedData(userFirstName=" + this.f478a + ", userLastName=" + this.b + ", bitmapData=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return bp6.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public MFUser f479a;
        @DexIgnore
        public Uri b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public cl7<Integer, String> d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public MFUser f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Bundle h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public a k;

        @DexIgnore
        public c(MFUser mFUser, Uri uri, Boolean bool, cl7<Integer, String> cl7, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, a aVar) {
            this.f479a = mFUser;
            this.b = uri;
            this.c = bool;
            this.d = cl7;
            this.e = z;
            this.f = mFUser2;
            this.g = z2;
            this.h = bundle;
            this.i = str;
            this.j = str2;
            this.k = aVar;
        }

        @DexIgnore
        public final String a() {
            return this.i;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final a c() {
            return this.k;
        }

        @DexIgnore
        public final Bundle d() {
            return this.h;
        }

        @DexIgnore
        public final Uri e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (!pq7.a(this.f479a, cVar.f479a) || !pq7.a(this.b, cVar.b) || !pq7.a(this.c, cVar.c) || !pq7.a(this.d, cVar.d) || this.e != cVar.e || !pq7.a(this.f, cVar.f) || this.g != cVar.g || !pq7.a(this.h, cVar.h) || !pq7.a(this.i, cVar.i) || !pq7.a(this.j, cVar.j) || !pq7.a(this.k, cVar.k)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.e;
        }

        @DexIgnore
        public final cl7<Integer, String> h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 1;
            int i3 = 0;
            MFUser mFUser = this.f479a;
            int hashCode = mFUser != null ? mFUser.hashCode() : 0;
            Uri uri = this.b;
            int hashCode2 = uri != null ? uri.hashCode() : 0;
            Boolean bool = this.c;
            int hashCode3 = bool != null ? bool.hashCode() : 0;
            cl7<Integer, String> cl7 = this.d;
            int hashCode4 = cl7 != null ? cl7.hashCode() : 0;
            boolean z = this.e;
            if (z) {
                z = true;
            }
            MFUser mFUser2 = this.f;
            int hashCode5 = mFUser2 != null ? mFUser2.hashCode() : 0;
            boolean z2 = this.g;
            if (!z2) {
                i2 = z2 ? 1 : 0;
            }
            Bundle bundle = this.h;
            int hashCode6 = bundle != null ? bundle.hashCode() : 0;
            String str = this.i;
            int hashCode7 = str != null ? str.hashCode() : 0;
            String str2 = this.j;
            int hashCode8 = str2 != null ? str2.hashCode() : 0;
            a aVar = this.k;
            if (aVar != null) {
                i3 = aVar.hashCode();
            }
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = z ? 1 : 0;
            return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i4) * 31) + hashCode5) * 31) + i2) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i3;
        }

        @DexIgnore
        public final MFUser i() {
            return this.f;
        }

        @DexIgnore
        public final MFUser j() {
            return this.f479a;
        }

        @DexIgnore
        public final Boolean k() {
            return this.c;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(user=" + this.f479a + ", imageUri=" + this.b + ", isUserChanged=" + this.c + ", showServerError=" + this.d + ", showProcessImageError=" + this.e + ", showSuccess=" + this.f + ", showLoading=" + this.g + ", dobBundle=" + this.h + ", birthday=" + this.i + ", birthdayErrorMsg=" + this.j + ", capturedData=" + this.k + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.e<vu5.a, iq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bp6 f480a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(bp6 bp6) {
            this.f480a = bp6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(iq4.a aVar) {
            pq7.c(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(bp6.l.a(), ".Inside mGetUser onError");
            bp6.h(this.f480a, null, null, null, null, false, null, false, null, null, null, null, 1983, null);
            this.f480a.f = true;
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(vu5.a aVar) {
            pq7.c(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bp6.l.a(), ".Inside mGetUser onSuccess");
            bp6.h(this.f480a, null, null, null, null, false, null, false, null, null, null, null, 1983, null);
            if (aVar.a() != null) {
                this.f480a.b = aVar.a();
                this.f480a.f477a = MFUser.copy$default(aVar.a(), null, null, null, null, null, false, null, false, false, null, null, 0, null, null, null, null, false, null, false, false, false, null, 0, 0, null, 0, 67108863, null);
                bp6 bp6 = this.f480a;
                bp6.h(bp6, bp6.b, null, null, null, false, null, false, null, null, null, null, 2046, null);
            } else {
                FLogger.INSTANCE.getLocal().d(bp6.l.a(), "loadUserFirstTime user is null");
            }
            this.f480a.f = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bp6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bp6 bp6, Intent intent, qn7 qn7) {
            super(2, qn7);
            this.this$0 = bp6;
            this.$data = intent;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$data, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Bitmap bitmap;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                Uri g = vk5.g(this.$data, PortfolioApp.h0.c());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bp6.l.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .onActivityResult imageUri=");
                if (g != null) {
                    sb.append(g);
                    local.d(a2, sb.toString());
                    if (!PortfolioApp.h0.c().w0(this.$data, g)) {
                        return tl7.f3441a;
                    }
                    bp6.h(this.this$0, null, g, null, null, false, null, false, null, null, null, null, 2045, null);
                    try {
                        if (!TextUtils.equals(g.getLastPathSegment(), "pickerImage.jpg")) {
                            bitmap = (Bitmap) tj5.a(PortfolioApp.h0.c()).e().J0(g).u0(((fj1) ((fj1) new fj1().l(wc1.f3916a)).m0(true)).o0(new hk5())).R0(200, 200).get();
                        } else {
                            Bitmap q = ry5.q(g.getPath(), 256, true);
                            this.this$0.c = true;
                            String f = i37.f(q);
                            MFUser mFUser = this.this$0.b;
                            String firstName = mFUser != null ? mFUser.getFirstName() : null;
                            MFUser mFUser2 = this.this$0.b;
                            bp6.h(this.this$0, null, null, null, null, false, null, false, null, null, null, new a(firstName, mFUser2 != null ? mFUser2.getLastName() : null, f), BasicChronology.CACHE_MASK, null);
                            bitmap = q;
                        }
                        MFUser mFUser3 = this.this$0.b;
                        if (mFUser3 != null) {
                            if (bitmap != null) {
                                String f2 = i37.f(bitmap);
                                pq7.b(f2, "BitmapUtils.encodeToBase64(bitmap!!)");
                                mFUser3.setProfilePicture(f2);
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                        this.this$0.c = true;
                        bp6.h(this.this$0, null, null, ao7.a(this.this$0.l()), null, false, null, false, null, null, null, null, 2043, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        bp6.h(this.this$0, null, null, null, null, true, null, false, null, null, null, null, 2031, null);
                    }
                    return tl7.f3441a;
                }
                pq7.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements iq4.e<xu5.c, xu5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ bp6 f481a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(bp6 bp6) {
            this.f481a = bp6;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(xu5.b bVar) {
            pq7.c(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bp6.l.a();
            local.d(a2, ".Inside updateUser onError, errorCode=" + bVar.a());
            bp6.h(this.f481a, null, null, null, new cl7(Integer.valueOf(bVar.a()), ""), false, null, false, null, null, null, null, 1975, null);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(xu5.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bp6.l.a(), ".Inside updateUser onSuccess");
            bp6.h(this.f481a, null, null, null, null, false, cVar.a(), false, null, null, null, null, 1951, null);
        }
    }

    /*
    static {
        String simpleName = bp6.class.getSimpleName();
        pq7.b(simpleName, "ProfileEditViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public bp6(xu5 xu5, vu5 vu5) {
        pq7.c(xu5, "mUpdateUser");
        pq7.c(vu5, "mGetUser");
        this.i = xu5;
        this.j = vu5;
    }

    @DexIgnore
    public static /* synthetic */ void h(bp6 bp6, MFUser mFUser, Uri uri, Boolean bool, cl7 cl7, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, a aVar, int i2, Object obj) {
        bp6.g((i2 & 1) != 0 ? null : mFUser, (i2 & 2) != 0 ? null : uri, (i2 & 4) != 0 ? null : bool, (i2 & 8) != 0 ? null : cl7, (i2 & 16) != 0 ? false : z, (i2 & 32) != 0 ? null : mFUser2, (i2 & 64) != 0 ? false : z2, (i2 & 128) != 0 ? null : bundle, (i2 & 256) != 0 ? null : str, (i2 & 512) != 0 ? null : str2, (i2 & 1024) != 0 ? null : aVar);
    }

    @DexIgnore
    public final void g(MFUser mFUser, Uri uri, Boolean bool, cl7<Integer, String> cl7, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, a aVar) {
        this.h.l(new c(mFUser, uri, bool, cl7, z, mFUser2, z2, bundle, str, str2, aVar));
    }

    @DexIgnore
    public final MutableLiveData<c> i() {
        return this.h;
    }

    @DexIgnore
    public final boolean j(Date date) {
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "calendar");
        instance.setTime(date);
        Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(instance), LocalDate.now());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("age=");
        pq7.b(yearsBetween, "age");
        sb.append(yearsBetween.getYears());
        local.d(str, sb.toString());
        return yearsBetween.getYears() >= 16;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r0 != r4.getHeightInCentimeters()) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0074, code lost:
        if (com.fossil.lr7.b(r4 * r5) != com.fossil.lr7.b((((float) r5.getWeightInGrams()) / 1000.0f) * r5)) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e7, code lost:
        if ((r4 + r6) == (r0.intValue() + r7)) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e9, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x011c, code lost:
        if (com.fossil.lr7.b(r4 * r5) == com.fossil.lr7.b(r1 * r5)) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x011e, code lost:
        r1 = false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean k() {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bp6.k():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x008a, code lost:
        if ((!com.fossil.pq7.a(r0, r3.getBirthday())) != false) goto L_0x008c;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean l() {
        /*
            r4 = this;
            r2 = 0
            r1 = 0
            com.portfolio.platform.data.model.MFUser r0 = r4.f477a
            if (r0 == 0) goto L_0x000a
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 != 0) goto L_0x000c
        L_0x000a:
            r0 = r2
        L_0x000b:
            return r0
        L_0x000c:
            if (r0 == 0) goto L_0x00c3
            java.lang.String r0 = r0.getFirstName()
            if (r0 == 0) goto L_0x009b
            if (r0 == 0) goto L_0x0093
            java.lang.CharSequence r0 = com.fossil.wt7.u0(r0)
            java.lang.String r0 = r0.toString()
        L_0x001e:
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00bf
            java.lang.String r3 = r3.getFirstName()
            boolean r0 = com.fossil.pq7.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.f477a
            if (r0 == 0) goto L_0x00bb
            java.lang.String r0 = r0.getLastName()
            if (r0 == 0) goto L_0x00a5
            if (r0 == 0) goto L_0x009d
            java.lang.CharSequence r0 = com.fossil.wt7.u0(r0)
            java.lang.String r0 = r0.toString()
        L_0x0042:
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00b7
            java.lang.String r3 = r3.getLastName()
            boolean r0 = com.fossil.pq7.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            boolean r0 = r4.k()
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.f477a
            if (r0 == 0) goto L_0x00b3
            java.lang.String r0 = r0.getGender()
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00af
            java.lang.String r3 = r3.getGender()
            boolean r0 = com.fossil.pq7.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            boolean r0 = r4.c
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.f477a
            if (r0 == 0) goto L_0x00ab
            java.lang.String r0 = r0.getBirthday()
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00a7
            java.lang.String r1 = r3.getBirthday()
            boolean r0 = com.fossil.pq7.a(r0, r1)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x00c7
        L_0x008c:
            boolean r0 = r4.g
            if (r0 == 0) goto L_0x00c7
            r0 = 1
            goto L_0x000b
        L_0x0093:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x009b:
            r0 = r1
            goto L_0x001e
        L_0x009d:
            com.fossil.il7 r0 = new com.fossil.il7
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x00a5:
            r0 = r1
            goto L_0x0042
        L_0x00a7:
            com.fossil.pq7.i()
            throw r1
        L_0x00ab:
            com.fossil.pq7.i()
            throw r1
        L_0x00af:
            com.fossil.pq7.i()
            throw r1
        L_0x00b3:
            com.fossil.pq7.i()
            throw r1
        L_0x00b7:
            com.fossil.pq7.i()
            throw r1
        L_0x00bb:
            com.fossil.pq7.i()
            throw r1
        L_0x00bf:
            com.fossil.pq7.i()
            throw r1
        L_0x00c3:
            com.fossil.pq7.i()
            throw r1
        L_0x00c7:
            r0 = r2
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bp6.l():boolean");
    }

    @DexIgnore
    public final void m() {
        if (!this.f) {
            h(this, null, null, null, null, false, null, true, null, null, null, null, 1983, null);
            this.j.e(null, new d(this));
        }
    }

    @DexIgnore
    public final void n(Date date) {
        pq7.c(date, "birthDay");
        FLogger.INSTANCE.getLocal().d(k, "onBirthDayChanged");
        this.d = lk5.k(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "mBirthday=" + this.d);
        MFUser mFUser = this.b;
        if (mFUser != null) {
            String str2 = this.d;
            if (str2 != null) {
                mFUser.setBirthday(str2);
            } else {
                pq7.i();
                throw null;
            }
        }
        if (!j(date)) {
            this.g = false;
            h(this, null, null, Boolean.FALSE, null, false, null, false, null, lk5.i(date), um5.c(PortfolioApp.h0.c(), 2131886965), null, 1275, null);
            return;
        }
        this.g = true;
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, lk5.i(date), null, null, 1787, null);
    }

    @DexIgnore
    public final void o() {
        List Y;
        String birthday;
        List Y2;
        Bundle bundle = new Bundle();
        String str = this.d;
        if (str == null || (Y2 = wt7.Y(str, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, null)) == null) {
            MFUser mFUser = this.b;
            Y = (mFUser == null || (birthday = mFUser.getBirthday()) == null) ? null : wt7.Y(birthday, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, null);
        } else {
            Y = Y2;
        }
        FLogger.INSTANCE.getLocal().d(k, String.valueOf(Y));
        if (Y != null && Y.size() == 3) {
            bundle.putInt("DAY", Integer.parseInt((String) Y.get(2)));
            bundle.putInt("MONTH", Integer.parseInt((String) Y.get(1)));
            bundle.putInt("YEAR", Integer.parseInt((String) Y.get(0)));
        }
        h(this, null, null, null, null, false, null, false, bundle, null, null, null, 1919, null);
    }

    @DexIgnore
    public final void p(String str) {
        pq7.c(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setFirstName(str);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void q(qh5 qh5) {
        pq7.c(qh5, "gender");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setGender(qh5.toString());
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void r(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void s(String str) {
        pq7.c(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setLastName(str);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void t(Intent intent) {
        xw7 unused = gu7.d(us0.a(this), bw7.b(), null, new e(this, intent, null), 2, null);
    }

    @DexIgnore
    public final void u(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void v() {
        if (this.b != null) {
            h(this, null, null, null, null, false, null, true, null, null, null, null, 1983, null);
            MFUser mFUser = this.b;
            if (mFUser != null) {
                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                    MFUser mFUser2 = this.b;
                    if (mFUser2 != null) {
                        String profilePicture = mFUser2.getProfilePicture();
                        Boolean valueOf = profilePicture != null ? Boolean.valueOf(wt7.v(profilePicture, Utility.URL_SCHEME, false, 2, null)) : null;
                        if (valueOf == null) {
                            pq7.i();
                            throw null;
                        } else if (valueOf.booleanValue()) {
                            MFUser mFUser3 = this.b;
                            if (mFUser3 != null) {
                                mFUser3.setProfilePicture("");
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
                MFUser mFUser4 = this.b;
                if (mFUser4 != null) {
                    if (mFUser4.getUseDefaultBiometric() && this.e) {
                        MFUser mFUser5 = this.b;
                        if (mFUser5 != null) {
                            mFUser5.setUseDefaultBiometric(false);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    xu5 xu5 = this.i;
                    MFUser mFUser6 = this.b;
                    if (mFUser6 != null) {
                        xu5.e(new xu5.a(mFUser6), new f(this));
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }
}
