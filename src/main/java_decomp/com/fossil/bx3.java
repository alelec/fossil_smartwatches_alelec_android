package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;
import android.util.Property;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bx3 {
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Cx3> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, PropertyValuesHolder[]> b; // = new SimpleArrayMap<>();

    @DexIgnore
    public static void a(Bx3 bx3, Animator animator) {
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            bx3.l(objectAnimator.getPropertyName(), objectAnimator.getValues());
            bx3.m(objectAnimator.getPropertyName(), Cx3.b(objectAnimator));
            return;
        }
        throw new IllegalArgumentException("Animator must be an ObjectAnimator: " + animator);
    }

    @DexIgnore
    public static Bx3 c(Context context, TypedArray typedArray, int i) {
        int resourceId;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return null;
        }
        return d(context, resourceId);
    }

    @DexIgnore
    public static Bx3 d(Context context, int i) {
        try {
            Animator loadAnimator = AnimatorInflater.loadAnimator(context, i);
            if (loadAnimator instanceof AnimatorSet) {
                return e(((AnimatorSet) loadAnimator).getChildAnimations());
            }
            if (loadAnimator == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(loadAnimator);
            return e(arrayList);
        } catch (Exception e) {
            Log.w("MotionSpec", "Can't load animation resource ID #0x" + Integer.toHexString(i), e);
            return null;
        }
    }

    @DexIgnore
    public static Bx3 e(List<Animator> list) {
        Bx3 bx3 = new Bx3();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            a(bx3, list.get(i));
        }
        return bx3;
    }

    @DexIgnore
    public final PropertyValuesHolder[] b(PropertyValuesHolder[] propertyValuesHolderArr) {
        PropertyValuesHolder[] propertyValuesHolderArr2 = new PropertyValuesHolder[propertyValuesHolderArr.length];
        for (int i = 0; i < propertyValuesHolderArr.length; i++) {
            propertyValuesHolderArr2[i] = propertyValuesHolderArr[i].clone();
        }
        return propertyValuesHolderArr2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Bx3)) {
            return false;
        }
        return this.a.equals(((Bx3) obj).a);
    }

    @DexIgnore
    public <T> ObjectAnimator f(String str, T t, Property<T, ?> property) {
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(t, g(str));
        ofPropertyValuesHolder.setProperty(property);
        h(str).a(ofPropertyValuesHolder);
        return ofPropertyValuesHolder;
    }

    @DexIgnore
    public PropertyValuesHolder[] g(String str) {
        if (j(str)) {
            return b(this.b.get(str));
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public Cx3 h(String str) {
        if (k(str)) {
            return this.a.get(str);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public long i() {
        int size = this.a.size();
        long j = 0;
        for (int i = 0; i < size; i++) {
            Cx3 n = this.a.n(i);
            j = Math.max(j, n.c() + n.d());
        }
        return j;
    }

    @DexIgnore
    public boolean j(String str) {
        return this.b.get(str) != null;
    }

    @DexIgnore
    public boolean k(String str) {
        return this.a.get(str) != null;
    }

    @DexIgnore
    public void l(String str, PropertyValuesHolder[] propertyValuesHolderArr) {
        this.b.put(str, propertyValuesHolderArr);
    }

    @DexIgnore
    public void m(String str, Cx3 cx3) {
        this.a.put(str, cx3);
    }

    @DexIgnore
    public String toString() {
        return '\n' + Bx3.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " timings: " + this.a + "}\n";
    }
}
