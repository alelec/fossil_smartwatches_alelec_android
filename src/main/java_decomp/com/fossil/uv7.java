package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uv7 {
    @DexIgnore
    public static final Object a(long j, Xe6<? super Cd6> xe6) {
        if (j <= 0) {
            return Cd6.a;
        }
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        b(lu7.getContext()).f(j, lu7);
        Object t = lu7.t();
        if (t != Yn7.d()) {
            return t;
        }
        Go7.c(xe6);
        return t;
    }

    @DexIgnore
    public static final Tv7 b(Af6 af6) {
        Af6.Bi bi = af6.get(Rn7.p);
        if (!(bi instanceof Tv7)) {
            bi = null;
        }
        Tv7 tv7 = (Tv7) bi;
        return tv7 != null ? tv7 : Qv7.a();
    }
}
