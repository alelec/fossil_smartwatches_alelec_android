package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Np4 implements Factory<Ct0> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Np4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Np4 a(Uo4 uo4) {
        return new Np4(uo4);
    }

    @DexIgnore
    public static Ct0 c(Uo4 uo4) {
        Ct0 u = uo4.u();
        Lk7.c(u, "Cannot return null from a non-@Nullable @Provides method");
        return u;
    }

    @DexIgnore
    public Ct0 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
