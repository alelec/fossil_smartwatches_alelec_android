package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ii5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public float d;

    @DexIgnore
    public Ii5(int i, int i2, int i3, float f) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = f;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }

    @DexIgnore
    public final float c() {
        return this.d;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ii5) {
                Ii5 ii5 = (Ii5) obj;
                if (!(this.a == ii5.a && this.b == ii5.b && this.c == ii5.c && Float.compare(this.d, ii5.d) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + Float.floatToIntBits(this.d);
    }

    @DexIgnore
    public String toString() {
        return "WorkoutPickerDataWrapper(minValue=" + this.a + ", maxValue=" + this.b + ", value=" + this.c + ", step=" + this.d + ")";
    }
}
