package com.fossil;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Kw0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Lw0 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Binder implements Lw0 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii implements Lw0 {
            @DexIgnore
            public IBinder b;

            @DexIgnore
            public Aii(IBinder iBinder) {
                this.b = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.Lw0
            public void E2(int i, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeInt(i);
                    obtain.writeStringArray(strArr);
                    this.b.transact(3, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.fossil.Lw0
            public void M2(Kw0 kw0, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongBinder(kw0 != null ? kw0.asBinder() : null);
                    obtain.writeInt(i);
                    this.b.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.b;
            }

            @DexIgnore
            @Override // com.fossil.Lw0
            public int l0(Kw0 kw0, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongBinder(kw0 != null ? kw0.asBinder() : null);
                    obtain.writeString(str);
                    this.b.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        public Ai() {
            attachInterface(this, "androidx.room.IMultiInstanceInvalidationService");
        }

        @DexIgnore
        public static Lw0 d(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof Lw0)) ? new Aii(iBinder) : (Lw0) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                int l0 = l0(Kw0.Ai.d(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(l0);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                M2(Kw0.Ai.d(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                E2(parcel.readInt(), parcel.createStringArray());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationService");
                return true;
            }
        }
    }

    @DexIgnore
    void E2(int i, String[] strArr) throws RemoteException;

    @DexIgnore
    void M2(Kw0 kw0, int i) throws RemoteException;

    @DexIgnore
    int l0(Kw0 kw0, String str) throws RemoteException;
}
