package com.fossil;

import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wl6 implements MembersInjector<GoalTrackingDetailActivity> {
    @DexIgnore
    public static void a(GoalTrackingDetailActivity goalTrackingDetailActivity, GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
        goalTrackingDetailActivity.A = goalTrackingDetailPresenter;
    }
}
