package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import com.fossil.Ry5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Py5 extends AsyncTask<Void, Void, Ai> {
    @DexIgnore
    public /* final */ WeakReference<CropImageView> a;
    @DexIgnore
    public /* final */ Bitmap b;
    @DexIgnore
    public /* final */ Uri c;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ float[] e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ boolean n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public /* final */ CropImageView.j p;
    @DexIgnore
    public Uri q;
    @DexIgnore
    public Bitmap.CompressFormat r;
    @DexIgnore
    public int s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ Uri b;
        @DexIgnore
        public /* final */ Exception c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public Ai(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = null;
            this.c = null;
            this.d = i;
        }

        @DexIgnore
        public Ai(Uri uri, int i) {
            this.a = null;
            this.b = uri;
            this.c = null;
            this.d = i;
        }

        @DexIgnore
        public Ai(Exception exc, boolean z) {
            this.a = null;
            this.b = null;
            this.c = exc;
            this.d = 1;
        }
    }

    @DexIgnore
    public Py5(CropImageView cropImageView, Bitmap bitmap, float[] fArr, int i2, boolean z, int i3, int i4, int i5, int i6, boolean z2, boolean z3, CropImageView.j jVar, Uri uri, Bitmap.CompressFormat compressFormat, int i7) {
        this.a = new WeakReference<>(cropImageView);
        this.d = cropImageView.getContext();
        this.b = bitmap;
        this.e = fArr;
        this.c = null;
        this.f = i2;
        this.i = z;
        this.j = i3;
        this.k = i4;
        this.l = i5;
        this.m = i6;
        this.n = z2;
        this.o = z3;
        this.p = jVar;
        this.q = uri;
        this.r = compressFormat;
        this.s = i7;
        this.g = 0;
        this.h = 0;
    }

    @DexIgnore
    public Py5(CropImageView cropImageView, Uri uri, float[] fArr, int i2, int i3, int i4, boolean z, int i5, int i6, int i7, int i8, boolean z2, boolean z3, CropImageView.j jVar, Uri uri2, Bitmap.CompressFormat compressFormat, int i9) {
        this.a = new WeakReference<>(cropImageView);
        this.d = cropImageView.getContext();
        this.c = uri;
        this.e = fArr;
        this.f = i2;
        this.i = z;
        this.j = i5;
        this.k = i6;
        this.g = i3;
        this.h = i4;
        this.l = i7;
        this.m = i8;
        this.n = z2;
        this.o = z3;
        this.p = jVar;
        this.q = uri2;
        this.r = compressFormat;
        this.s = i9;
        this.b = null;
    }

    @DexIgnore
    public Ai a(Void... voidArr) {
        Ry5.Ai h2;
        try {
            if (isCancelled()) {
                return null;
            }
            if (this.c != null) {
                h2 = Ry5.e(this.d, this.c, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o);
            } else if (this.b == null) {
                return new Ai((Bitmap) null, 1);
            } else {
                h2 = Ry5.h(this.b, this.e, this.f, this.i, this.j, this.k, this.n, this.o);
            }
            Bitmap s2 = Ry5.s(Ry5.D(h2.a, this.l, this.m, this.p), false);
            Uri fromFile = Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/" + File.separator + "0.png"));
            this.q = fromFile;
            Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.PNG;
            this.r = compressFormat;
            this.s = 100;
            Ry5.I(this.d, s2, fromFile, compressFormat, 100);
            s2.recycle();
            return new Ai(this.q, h2.b);
        } catch (Exception e2) {
            return new Ai(e2, this.q != null);
        }
    }

    @DexIgnore
    public void b(Ai ai) {
        Bitmap bitmap;
        CropImageView cropImageView;
        if (ai != null) {
            boolean z = false;
            if (!isCancelled() && (cropImageView = this.a.get()) != null) {
                z = true;
                cropImageView.o(ai);
            }
            if (!z && (bitmap = ai.a) != null) {
                bitmap.recycle();
            }
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ Ai doInBackground(Void[] voidArr) {
        return a(voidArr);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.os.AsyncTask
    public /* bridge */ /* synthetic */ void onPostExecute(Ai ai) {
        b(ai);
    }
}
