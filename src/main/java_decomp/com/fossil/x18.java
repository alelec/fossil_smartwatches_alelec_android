package com.fossil;

import java.net.InetSocketAddress;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X18 {
    @DexIgnore
    public /* final */ X08 a;
    @DexIgnore
    public /* final */ Proxy b;
    @DexIgnore
    public /* final */ InetSocketAddress c;

    @DexIgnore
    public X18(X08 x08, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (x08 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.a = x08;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    @DexIgnore
    public X08 a() {
        return this.a;
    }

    @DexIgnore
    public Proxy b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.a.i != null && this.b.type() == Proxy.Type.HTTP;
    }

    @DexIgnore
    public InetSocketAddress d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof X18) {
            X18 x18 = (X18) obj;
            return x18.a.equals(this.a) && x18.b.equals(this.b) && x18.c.equals(this.c);
        }
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() + 527) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Route{" + this.c + "}";
    }
}
