package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I5 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ N6 d;
    @DexIgnore
    public /* final */ /* synthetic */ U6 e;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] f;

    @DexIgnore
    public I5(K5 k5, G7 g7, N6 n6, U6 u6, byte[] bArr) {
        this.b = k5;
        this.c = g7;
        this.d = n6;
        this.e = u6;
        this.f = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.b.f();
        this.b.z.b.c(new B7(this.c, this.d, this.e, this.f));
    }
}
