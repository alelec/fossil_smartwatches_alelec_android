package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sa4 extends Ta4.Di.Fii {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Fii.Aiii {
        @DexIgnore
        public String a;

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Fii.Aiii
        public Ta4.Di.Fii a() {
            String str = "";
            if (this.a == null) {
                str = " identifier";
            }
            if (str.isEmpty()) {
                return new Sa4(this.a);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Fii.Aiii
        public Ta4.Di.Fii.Aiii b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }
    }

    @DexIgnore
    public Sa4(String str) {
        this.a = str;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Fii
    public String b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Ta4.Di.Fii) {
            return this.a.equals(((Ta4.Di.Fii) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "User{identifier=" + this.a + "}";
    }
}
