package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Lc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nw5 extends RecyclerView.g<Bi> {
    @DexIgnore
    public List<Lc6<ShineDevice, String>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ Wa1 b;
    @DexIgnore
    public Ai c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void p1(View view, Bi bi, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ ImageView d;
        @DexIgnore
        public /* final */ View e;
        @DexIgnore
        public ShineDevice f;
        @DexIgnore
        public /* final */ /* synthetic */ Nw5 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                Wg6.c(str, "serial");
                Wg6.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "set image for serial=" + str + " path " + str2);
                this.a.g.b.t(str2).u0(((Fj1) new Fj1().c0(2131231334)).n()).F0(this.a.d);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                Wg6.c(str, "fastPairId");
                Wg6.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "setImageCallback() for wearOS, fastPairId=" + str);
                this.a.g.b.t(str2).u0(((Fj1) new Fj1().c0(2131231082)).n()).F0(this.a.d);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Nw5 nw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.g = nw5;
            View findViewById = view.findViewById(2131362414);
            Wg6.b(findViewById, "view.findViewById(R.id.ftv_device_serial)");
            this.b = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362413);
            Wg6.b(findViewById2, "view.findViewById(R.id.ftv_device_name)");
            this.c = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131362692);
            Wg6.b(findViewById3, "view.findViewById(R.id.iv_device_image)");
            this.d = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131363079);
            Wg6.b(findViewById4, "view.findViewById(R.id.scan_device_container)");
            this.e = findViewById4;
            findViewById4.setOnClickListener(this);
        }

        @DexIgnore
        public final void b(ShineDevice shineDevice, String str) {
            boolean z = true;
            Wg6.c(shineDevice, "shineDevice");
            Wg6.c(str, "deviceName");
            this.f = shineDevice;
            this.c.setText(str);
            String serial = shineDevice.getSerial();
            Wg6.b(serial, "shineDevice.serial");
            if (serial.length() > 0) {
                FlexibleTextView flexibleTextView = this.b;
                Hr7 hr7 = Hr7.a;
                Locale locale = Locale.US;
                Wg6.b(locale, "Locale.US");
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131887221);
                Wg6.b(c2, "LanguageHelper.getString\u2026 R.string.Serial_pattern)");
                String format = String.format(locale, c2, Arrays.copyOf(new Object[]{shineDevice.getSerial()}, 1));
                Wg6.b(format, "java.lang.String.format(locale, format, *args)");
                flexibleTextView.setText(format);
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(4);
            }
            String serial2 = shineDevice.getSerial();
            Wg6.b(serial2, "shineDevice.serial");
            if (serial2.length() == 0) {
                String fastPairId = shineDevice.getFastPairId();
                Wg6.b(fastPairId, "shineDevice.fastPairId");
                if (fastPairId.length() != 0) {
                    z = false;
                }
                if (z) {
                    this.d.setImageDrawable(PortfolioApp.get.instance().getDrawable(2131231082));
                    return;
                }
            }
            c(shineDevice);
        }

        @DexIgnore
        public final void c(ShineDevice shineDevice) {
            String serial = shineDevice.getSerial();
            Wg6.b(serial, "shineDevice.serial");
            if (serial.length() > 0) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                String serial2 = shineDevice.getSerial();
                Wg6.b(serial2, "shineDevice.serial");
                CloudImageHelper.ItemImage type = with.setSerialNumber(serial2).setSerialPrefix(DeviceHelper.o.m(shineDevice.getSerial())).setType(Constants.DeviceType.TYPE_LARGE);
                ImageView imageView = this.d;
                DeviceHelper.Ai ai = DeviceHelper.o;
                String serial3 = shineDevice.getSerial();
                Wg6.b(serial3, "shineDevice.serial");
                type.setPlaceHolder(imageView, ai.i(serial3, DeviceHelper.Bi.LARGE)).setImageCallback(new Aii(this)).download();
                return;
            }
            CloudImageHelper.ItemImage with2 = CloudImageHelper.Companion.getInstance().with();
            String fastPairId = shineDevice.getFastPairId();
            Wg6.b(fastPairId, "shineDevice.fastPairId");
            with2.setFastPairId(fastPairId).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder(this.d, 2131231082).setImageCallback(new Bii(this)).downloadForWearOS();
        }

        @DexIgnore
        public void onClick(View view) {
            Ai ai;
            Wg6.c(view, "view");
            if (this.g.getItemCount() > getAdapterPosition() && getAdapterPosition() != -1 && (ai = this.g.c) != null) {
                ai.p1(view, this, getAdapterPosition());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Comparator<Lc6<? extends ShineDevice, ? extends String>> {
        @DexIgnore
        public static /* final */ Ci b; // = new Ci();

        @DexIgnore
        public final int a(Lc6<ShineDevice, String> lc6, Lc6<ShineDevice, String> lc62) {
            int i;
            int i2 = -1;
            if (lc6 == null || lc62 == null) {
                int i3 = lc6 == null ? -1 : 1;
                if (lc62 != null) {
                    i2 = 1;
                }
                i = i3 - i2;
            } else {
                i = lc6.getFirst().getRssi() - lc62.getFirst().getRssi();
            }
            return -i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Lc6<? extends ShineDevice, ? extends String> lc6, Lc6<? extends ShineDevice, ? extends String> lc62) {
            return a(lc6, lc62);
        }
    }

    @DexIgnore
    public Nw5(Wa1 wa1, Ai ai) {
        Wg6.c(wa1, "mRequestManager");
        this.b = wa1;
        this.c = ai;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void i(Bi bi, int i) {
        Wg6.c(bi, "viewHolder");
        bi.b(this.a.get(i).getFirst(), this.a.get(i).getSecond());
    }

    @DexIgnore
    public Bi j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558714, viewGroup, false);
        Wg6.b(inflate, "v");
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.width = (int) (((float) viewGroup.getMeasuredWidth()) * 0.5f);
        inflate.setLayoutParams(layoutParams);
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void k(List<Lc6<ShineDevice, String>> list) {
        Wg6.c(list, "data");
        this.a.clear();
        Lm7.r(list, Ci.b);
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        i(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
