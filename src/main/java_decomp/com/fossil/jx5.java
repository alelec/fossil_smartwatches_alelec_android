package com.fossil;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.Ix5;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jx5<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, Ix5.Ai {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Cursor c;
    @DexIgnore
    public Jx5<VH>.a d;
    @DexIgnore
    public DataSetObserver e;
    @DexIgnore
    public Ix5 f;
    @DexIgnore
    public FilterQueryProvider g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends ContentObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            Jx5.this.j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends DataSetObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        public void onChanged() {
            Jx5.this.b = true;
            Jx5.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            Jx5.this.b = false;
            Jx5 jx5 = Jx5.this;
            jx5.notifyItemRangeRemoved(0, jx5.getItemCount());
        }
    }

    /*
    static {
        String simpleName = Jx5.class.getSimpleName();
        Wg6.b(simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public Jx5(Cursor cursor) {
        boolean z = cursor != null;
        this.c = cursor;
        this.b = z;
        this.d = new Ai();
        this.e = new Bi();
        if (z) {
            Jx5<VH>.a aVar = this.d;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.e;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ix5.Ai
    public void a(Cursor cursor) {
        Wg6.c(cursor, "cursor");
        Cursor l = l(cursor);
        if (l != null) {
            l.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ix5.Ai
    public Cursor b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ix5.Ai
    public CharSequence d(Cursor cursor) {
        String obj;
        return (cursor == null || (obj = cursor.toString()) == null) ? "" : obj;
    }

    @DexIgnore
    @Override // com.fossil.Ix5.Ai
    public Cursor e(CharSequence charSequence) {
        Wg6.c(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.g;
        if (filterQueryProvider == null) {
            return this.c;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.f == null) {
            this.f = new Ix5(this);
        }
        Ix5 ix5 = this.f;
        if (ix5 != null) {
            return ix5;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Cursor cursor;
        if (!this.b || (cursor = this.c) == null) {
            return 0;
        }
        if (cursor != null) {
            return cursor.getCount();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public abstract void i(VH vh, Cursor cursor, int i);

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(h, ".Inside onContentChanged");
    }

    @DexIgnore
    public final void k(FilterQueryProvider filterQueryProvider) {
        Wg6.c(filterQueryProvider, "filterQueryProvider");
        this.g = filterQueryProvider;
    }

    @DexIgnore
    public Cursor l(Cursor cursor) {
        if (Wg6.a(cursor, this.c)) {
            return null;
        }
        Cursor cursor2 = this.c;
        if (cursor2 != null) {
            Jx5<VH>.a aVar = this.d;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.e;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.c = cursor;
        if (cursor != null) {
            Jx5<VH>.a aVar2 = this.d;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.e;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.b = true;
            notifyDataSetChanged();
            return cursor2;
        }
        this.b = false;
        notifyItemRangeRemoved(0, getItemCount());
        return cursor2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(VH vh, int i) {
        Wg6.c(vh, "holder");
        if (!this.b) {
            FLogger.INSTANCE.getLocal().d(h, ".Inside onBindViewHolder the cursor is invalid");
        }
        i(vh, this.c, i);
    }
}
