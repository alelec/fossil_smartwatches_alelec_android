package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ap7 extends Zo7 {
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006d, code lost:
        com.fossil.So7.a(r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0070, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final byte[] a(java.io.File r9) {
        /*
            r8 = 0
            r1 = 0
            java.lang.String r0 = "$this$readBytes"
            com.mapped.Wg6.c(r9, r0)
            java.io.FileInputStream r4 = new java.io.FileInputStream
            r4.<init>(r9)
            long r2 = r9.length()     // Catch:{ all -> 0x006a }
            r0 = 2147483647(0x7fffffff, float:NaN)
            long r6 = (long) r0     // Catch:{ all -> 0x006a }
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 > 0) goto L_0x008d
            int r3 = (int) r2     // Catch:{ all -> 0x006a }
            byte[] r0 = new byte[r3]     // Catch:{ all -> 0x006a }
            r2 = r3
        L_0x001c:
            if (r2 <= 0) goto L_0x0024
            int r5 = r4.read(r0, r1, r2)     // Catch:{ all -> 0x006a }
            if (r5 >= 0) goto L_0x0033
        L_0x0024:
            if (r2 <= 0) goto L_0x0036
            byte[] r0 = java.util.Arrays.copyOf(r0, r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "java.util.Arrays.copyOf(this, newSize)"
            com.mapped.Wg6.b(r0, r1)     // Catch:{ all -> 0x006a }
        L_0x002f:
            com.fossil.So7.a(r4, r8)
            return r0
        L_0x0033:
            int r2 = r2 - r5
            int r1 = r1 + r5
            goto L_0x001c
        L_0x0036:
            int r1 = r4.read()
            r2 = -1
            if (r1 == r2) goto L_0x002f
            com.fossil.Uo7 r2 = new com.fossil.Uo7
            r5 = 8193(0x2001, float:1.1481E-41)
            r2.<init>(r5)
            r2.write(r1)
            r1 = 0
            r5 = 2
            r6 = 0
            com.fossil.Ro7.b(r4, r2, r1, r5, r6)
            int r1 = r2.size()
            int r1 = r1 + r3
            if (r1 < 0) goto L_0x0071
            byte[] r5 = r2.a()
            byte[] r0 = java.util.Arrays.copyOf(r0, r1)
            java.lang.String r1 = "java.util.Arrays.copyOf(this, newSize)"
            com.mapped.Wg6.b(r0, r1)
            r1 = 0
            int r2 = r2.size()
            com.fossil.Dm7.g(r5, r0, r3, r1, r2)
            goto L_0x002f
        L_0x006a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x006c }
        L_0x006c:
            r1 = move-exception
            com.fossil.So7.a(r4, r0)
            throw r1
        L_0x0071:
            java.lang.OutOfMemoryError r0 = new java.lang.OutOfMemoryError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "File "
            r1.append(r2)
            r1.append(r9)
            java.lang.String r2 = " is too big to fit in memory."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x008d:
            java.lang.OutOfMemoryError r0 = new java.lang.OutOfMemoryError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = "File "
            r1.append(r5)
            r1.append(r9)
            java.lang.String r5 = " is too big ("
            r1.append(r5)
            r1.append(r2)
            java.lang.String r2 = " bytes) to fit in memory."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ap7.a(java.io.File):byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        com.fossil.So7.a(r1, r0);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void b(java.io.File r3, byte[] r4) {
        /*
            java.lang.String r0 = "$this$writeBytes"
            com.mapped.Wg6.c(r3, r0)
            java.lang.String r0 = "array"
            com.mapped.Wg6.c(r4, r0)
            java.io.FileOutputStream r1 = new java.io.FileOutputStream
            r1.<init>(r3)
            r1.write(r4)     // Catch:{ all -> 0x0019 }
            com.mapped.Cd6 r0 = com.mapped.Cd6.a     // Catch:{ all -> 0x0019 }
            r0 = 0
            com.fossil.So7.a(r1, r0)
            return
        L_0x0019:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x001b }
        L_0x001b:
            r2 = move-exception
            com.fossil.So7.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ap7.b(java.io.File, byte[]):void");
    }
}
