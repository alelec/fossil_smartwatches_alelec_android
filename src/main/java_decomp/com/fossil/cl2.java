package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cl2 extends Ec2<Dl2> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public Cl2(Context context, Looper looper, Ac2 ac2, F42 f42, R62.Bi bi, R62.Ci ci) {
        super(context, looper, 16, ac2, bi, ci);
        if (f42 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final Bundle F() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String p() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        return queryLocalInterface instanceof Dl2 ? (Dl2) queryLocalInterface : new El2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public final int s() {
        return H62.a;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public final boolean v() {
        Ac2 o0 = o0();
        return !TextUtils.isEmpty(o0.b()) && !o0.e(E42.c).isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String x() {
        return "com.google.android.gms.auth.service.START";
    }
}
