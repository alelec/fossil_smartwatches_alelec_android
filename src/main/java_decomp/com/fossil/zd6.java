package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zd6 extends wd6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ MutableLiveData<Date> i;
    @DexIgnore
    public /* final */ LiveData<h47<List<ActivitySummary>>> j;
    @DexIgnore
    public /* final */ LiveData<h47<List<ActivitySample>>> k;
    @DexIgnore
    public /* final */ LiveData<h47<List<WorkoutSession>>> l;
    @DexIgnore
    public /* final */ xd6 m;
    @DexIgnore
    public /* final */ SummariesRepository n;
    @DexIgnore
    public /* final */ ActivitiesRepository o;
    @DexIgnore
    public /* final */ WorkoutSessionRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4453a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zd6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$mActivitySamples$1$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {52, 52}, m = "invokeSuspend")
        /* renamed from: com.fossil.zd6$a$a  reason: collision with other inner class name */
        public static final class C0304a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySample>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0304a(a aVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0304a aVar = new C0304a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySample>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0304a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.zd6$a r1 = r8.this$0
                    com.fossil.zd6 r1 = r1.f4453a
                    com.portfolio.platform.data.source.ActivitiesRepository r1 = com.fossil.zd6.p(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getActivityList(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.zd6.a.C0304a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public a(zd6 zd6) {
            this.f4453a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewDayPresenter", "mActivitySamples onDateChange " + date);
            return or0.c(null, 0, new C0304a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4454a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$mActivitySummaries$1$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {46, 46}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySummary>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.zd6$b r1 = r8.this$0
                    com.fossil.zd6 r1 = r1.f4454a
                    com.portfolio.platform.data.source.SummariesRepository r1 = com.fossil.zd6.u(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getSummaries(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.zd6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(zd6 zd6) {
            this.f4454a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySummary>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewDayPresenter", "mActivitySummaries onDateChange " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4455a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$mWorkoutSessions$1$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {58, 58}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<WorkoutSession>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<WorkoutSession>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x0015:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.hs0 r1 = (com.fossil.hs0) r1
                    com.fossil.el7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.zd6$c r1 = r8.this$0
                    com.fossil.zd6 r1 = r1.f4455a
                    com.portfolio.platform.data.source.WorkoutSessionRepository r1 = com.fossil.zd6.x(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.fossil.pq7.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.fossil.pq7.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    java.lang.Object r2 = r1.getWorkoutSessions(r2, r3, r6, r8)
                    if (r2 != r4) goto L_0x0065
                    r0 = r4
                    goto L_0x0017
                L_0x0065:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.zd6.c.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public c(zd6 zd6) {
            this.f4455a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<WorkoutSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewDayPresenter", "mWorkoutSessions onDateChange " + date);
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {127, 129, 130}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zd6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$activitySummary$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ActivitySummary> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object obj2;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    h47 h47 = (h47) this.this$0.this$0.j.e();
                    if (h47 == null || (list = (List) h47.c()) == null) {
                        return null;
                    }
                    Iterator it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        Object next = it.next();
                        if (ao7.a(lk5.m0(((ActivitySummary) next).getDate(), this.this$0.this$0.f)).booleanValue()) {
                            obj2 = next;
                            break;
                        }
                    }
                    return (ActivitySummary) obj2;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$maxValue$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(ArrayList arrayList, qn7 qn7) {
                super(2, qn7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.$data, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            pq7.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                pq7.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += ao7.e(it4.next().e()).intValue();
                    }
                    return ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter$showDetailChart$1$pair$1", f = "ActiveTimeOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super cl7<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    cn6 cn6 = cn6.f632a;
                    Date date = this.this$0.this$0.f;
                    if (date != null) {
                        h47 h47 = (h47) this.this$0.this$0.k.e();
                        return cn6.b(date, h47 != null ? (List) h47.c() : null, 1);
                    }
                    pq7.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zd6 zd6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zd6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0099  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00c6  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00e8  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 252
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zd6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<h47<? extends List<ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4456a;

        @DexIgnore
        public e(zd6 zd6) {
            this.f4456a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<ActivitySummary>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActiveTimeOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                this.f4456a.g = true;
                if (this.f4456a.g && this.f4456a.h) {
                    this.f4456a.C();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<h47<? extends List<ActivitySample>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4457a;

        @DexIgnore
        public f(zd6 zd6) {
            this.f4457a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<ActivitySample>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySamples -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActiveTimeOverviewDayPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                this.f4457a.h = true;
                if (this.f4457a.g && this.f4457a.h) {
                    this.f4457a.C();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<h47<? extends List<WorkoutSession>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ zd6 f4458a;

        @DexIgnore
        public g(zd6 zd6) {
            this.f4458a = zd6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<WorkoutSession>> h47) {
            xh5 a2 = h47.a();
            List<WorkoutSession> list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("ActiveTimeOverviewDayPresenter", sb.toString());
            if (a2 == xh5.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.f4458a.m.v(false, new ArrayList());
            } else {
                this.f4458a.m.v(true, list);
            }
        }
    }

    @DexIgnore
    public zd6(xd6 xd6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        pq7.c(xd6, "mView");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(activitiesRepository, "mActivitiesRepository");
        pq7.c(workoutSessionRepository, "mWorkoutSessionRepository");
        pq7.c(portfolioApp, "mApp");
        this.m = xd6;
        this.n = summariesRepository;
        this.o = activitiesRepository;
        this.p = workoutSessionRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.i = mutableLiveData;
        LiveData<h47<List<ActivitySummary>>> c2 = ss0.c(mutableLiveData, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.j = c2;
        LiveData<h47<List<ActivitySample>>> c3 = ss0.c(this.i, new a(this));
        pq7.b(c3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.k = c3;
        LiveData<h47<List<WorkoutSession>>> c4 = ss0.c(this.i, new c(this));
        pq7.b(c4, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.l = c4;
    }

    @DexIgnore
    public void B() {
        this.m.M5(this);
    }

    @DexIgnore
    public final void C() {
        xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.f;
        if (date == null || !lk5.p0(date).booleanValue()) {
            this.g = false;
            this.h = false;
            Date date2 = new Date();
            this.f = date2;
            this.i.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayPresenter", "loadData - mDate=" + this.f);
        LiveData<h47<List<ActivitySummary>>> liveData = this.j;
        xd6 xd6 = this.m;
        if (xd6 != null) {
            liveData.h((yd6) xd6, new e(this));
            this.k.h((LifecycleOwner) this.m, new f(this));
            this.l.h((LifecycleOwner) this.m, new g(this));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayPresenter", "stop");
        try {
            LiveData<h47<List<ActivitySample>>> liveData = this.k;
            xd6 xd6 = this.m;
            if (xd6 != null) {
                liveData.n((yd6) xd6);
                this.j.n((LifecycleOwner) this.m);
                this.l.n((LifecycleOwner) this.m);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.wd6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }
}
