package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Xe6;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sy7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(Sy7.class, Object.class, "_state");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(Sy7.class, "_updating");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater c; // = AtomicReferenceFieldUpdater.newUpdater(Sy7.class, Object.class, "onCloseHandler");
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public static /* final */ Vz7 e;
    @DexIgnore
    public static /* final */ Bi<Object> f;
    @DexIgnore
    public volatile Object _state; // = f;
    @DexIgnore
    public volatile int _updating; // = 0;
    @DexIgnore
    public volatile Object onCloseHandler; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Throwable a;

        @DexIgnore
        public Ai(Throwable th) {
            this.a = th;
        }

        @DexIgnore
        public final Throwable a() {
            Throwable th = this.a;
            return th != null ? th : new Ry7("Channel was closed");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<E> {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Ci<E>[] b;

        @DexIgnore
        public Bi(Object obj, Ci<E>[] ciArr) {
            this.a = obj;
            this.b = ciArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<E> extends Ty7<E> implements Vy7<E> {
        @DexIgnore
        public Ci(Sy7<E> sy7) {
        }

        @DexIgnore
        @Override // com.fossil.Ty7
        public Object x(E e) {
            return super.x(e);
        }
    }

    /*
    static {
        Vz7 vz7 = new Vz7("UNDEFINED");
        e = vz7;
        f = new Bi<>(vz7, null);
    }
    */

    @DexIgnore
    public final Ci<E>[] a(Ci<E>[] ciArr, Ci<E> ci) {
        if (ciArr != null) {
            return (Ci[]) Dm7.r(ciArr, ci);
        }
        Ci<E>[] ciArr2 = new Ci[1];
        for (int i = 0; i < 1; i++) {
            ciArr2[i] = ci;
        }
        return ciArr2;
    }

    @DexIgnore
    public void b(CancellationException cancellationException) {
        c(cancellationException);
    }

    @DexIgnore
    public boolean c(Throwable th) {
        Object obj;
        do {
            obj = this._state;
            if (obj instanceof Ai) {
                return false;
            }
            if (!(obj instanceof Bi)) {
                throw new IllegalStateException(("Invalid state " + obj).toString());
            }
        } while (!a.compareAndSet(this, obj, th == null ? d : new Ai(th)));
        if (obj != null) {
            Ci<E>[] ciArr = ((Bi) obj).b;
            if (ciArr != null) {
                for (Ci<E> ci : ciArr) {
                    ci.c(th);
                }
            }
            d(th);
            return true;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
    }

    @DexIgnore
    public final void d(Throwable th) {
        Object obj;
        Object obj2 = this.onCloseHandler;
        if (obj2 != null && obj2 != (obj = Ky7.c) && c.compareAndSet(this, obj2, obj)) {
            Ir7.d(obj2, 1);
            ((Hg6) obj2).invoke(th);
        }
    }

    @DexIgnore
    public final Ai e(E e2) {
        Object obj;
        if (!b.compareAndSet(this, 0, 1)) {
            return null;
        }
        do {
            try {
                obj = this._state;
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    this._updating = 0;
                    return ai;
                } else if (!(obj instanceof Bi)) {
                    throw new IllegalStateException(("Invalid state " + obj).toString());
                } else if (obj == null) {
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
                }
            } finally {
                this._updating = 0;
            }
        } while (!a.compareAndSet(this, obj, new Bi(e2, ((Bi) obj).b)));
        Ci<E>[] ciArr = ((Bi) obj).b;
        if (ciArr != null) {
            for (Ci<E> ci : ciArr) {
                ci.x(e2);
            }
        }
        return null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.Sy7<E> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Sy7$Ci */
    /* JADX WARN: Multi-variable type inference failed */
    public Vy7<E> f() {
        Object obj;
        Bi bi;
        Object obj2;
        Ci ci = new Ci(this);
        do {
            obj = this._state;
            if (obj instanceof Ai) {
                ci.c(((Ai) obj).a);
                return ci;
            } else if (obj instanceof Bi) {
                bi = (Bi) obj;
                Object obj3 = bi.a;
                if (obj3 != e) {
                    ci.x(obj3);
                }
                obj2 = bi.a;
                if (obj != null) {
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
                }
            } else {
                throw new IllegalStateException(("Invalid state " + obj).toString());
            }
        } while (!a.compareAndSet(this, obj, new Bi(obj2, a(bi.b, ci))));
        return ci;
    }

    @DexIgnore
    public Object g(E e2, Xe6<? super Cd6> xe6) {
        Ai e3 = e(e2);
        if (e3 == null) {
            return e3 == Yn7.d() ? e3 : Cd6.a;
        }
        throw e3.a();
    }
}
