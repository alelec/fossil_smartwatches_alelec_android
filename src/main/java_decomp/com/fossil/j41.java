package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J41<V> extends H41<V> {
    @DexIgnore
    public static <V> J41<V> t() {
        return new J41<>();
    }

    @DexIgnore
    @Override // com.fossil.H41
    public boolean p(V v) {
        return super.p(v);
    }

    @DexIgnore
    @Override // com.fossil.H41
    public boolean q(Throwable th) {
        return super.q(th);
    }

    @DexIgnore
    @Override // com.fossil.H41
    public boolean r(G64<? extends V> g64) {
        return super.r(g64);
    }
}
