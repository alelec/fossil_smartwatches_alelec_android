package com.fossil;

import android.util.SparseArray;
import com.fossil.Jz1;
import com.portfolio.platform.data.InAppPermission;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Ai a(Bi bi);

        @DexIgnore
        public abstract Ai b(Ci ci);

        @DexIgnore
        public abstract Pz1 c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Bi extends Enum<Bi> {
        @DexIgnore
        public static /* final */ SparseArray<Bi> b;
        @DexIgnore
        public static /* final */ Bi zza; // = new Bi("UNKNOWN_MOBILE_SUBTYPE", 0, 0);
        @DexIgnore
        public static /* final */ Bi zzb; // = new Bi("GPRS", 1, 1);
        @DexIgnore
        public static /* final */ Bi zzc; // = new Bi("EDGE", 2, 2);
        @DexIgnore
        public static /* final */ Bi zzd; // = new Bi("UMTS", 3, 3);
        @DexIgnore
        public static /* final */ Bi zze; // = new Bi("CDMA", 4, 4);
        @DexIgnore
        public static /* final */ Bi zzf; // = new Bi("EVDO_0", 5, 5);
        @DexIgnore
        public static /* final */ Bi zzg; // = new Bi("EVDO_A", 6, 6);
        @DexIgnore
        public static /* final */ Bi zzh; // = new Bi("RTT", 7, 7);
        @DexIgnore
        public static /* final */ Bi zzi; // = new Bi("HSDPA", 8, 8);
        @DexIgnore
        public static /* final */ Bi zzj; // = new Bi("HSUPA", 9, 9);
        @DexIgnore
        public static /* final */ Bi zzk; // = new Bi("HSPA", 10, 10);
        @DexIgnore
        public static /* final */ Bi zzl; // = new Bi("IDEN", 11, 11);
        @DexIgnore
        public static /* final */ Bi zzm; // = new Bi("EVDO_B", 12, 12);
        @DexIgnore
        public static /* final */ Bi zzn; // = new Bi("LTE", 13, 13);
        @DexIgnore
        public static /* final */ Bi zzo; // = new Bi("EHRPD", 14, 14);
        @DexIgnore
        public static /* final */ Bi zzp; // = new Bi("HSPAP", 15, 15);
        @DexIgnore
        public static /* final */ Bi zzq; // = new Bi("GSM", 16, 16);
        @DexIgnore
        public static /* final */ Bi zzr; // = new Bi("TD_SCDMA", 17, 17);
        @DexIgnore
        public static /* final */ Bi zzs; // = new Bi("IWLAN", 18, 18);
        @DexIgnore
        public static /* final */ Bi zzt; // = new Bi("LTE_CA", 19, 19);
        @DexIgnore
        public static /* final */ Bi zzu; // = new Bi("COMBINED", 20, 100);
        @DexIgnore
        public /* final */ int zzw;

        /*
        static {
            SparseArray<Bi> sparseArray = new SparseArray<>();
            b = sparseArray;
            sparseArray.put(0, zza);
            b.put(1, zzb);
            b.put(2, zzc);
            b.put(3, zzd);
            b.put(4, zze);
            b.put(5, zzf);
            b.put(6, zzg);
            b.put(7, zzh);
            b.put(8, zzi);
            b.put(9, zzj);
            b.put(10, zzk);
            b.put(11, zzl);
            b.put(12, zzm);
            b.put(13, zzn);
            b.put(14, zzo);
            b.put(15, zzp);
            b.put(16, zzq);
            b.put(17, zzr);
            b.put(18, zzs);
            b.put(19, zzt);
        }
        */

        @DexIgnore
        public Bi(String str, int i, int i2) {
            this.zzw = i2;
        }

        @DexIgnore
        public static Bi zza(int i) {
            return b.get(i);
        }

        @DexIgnore
        public int zza() {
            return this.zzw;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Ci extends Enum<Ci> {
        @DexIgnore
        public static /* final */ SparseArray<Ci> b;
        @DexIgnore
        public static /* final */ Ci zza; // = new Ci("MOBILE", 0, 0);
        @DexIgnore
        public static /* final */ Ci zzb; // = new Ci("WIFI", 1, 1);
        @DexIgnore
        public static /* final */ Ci zzc; // = new Ci("MOBILE_MMS", 2, 2);
        @DexIgnore
        public static /* final */ Ci zzd; // = new Ci("MOBILE_SUPL", 3, 3);
        @DexIgnore
        public static /* final */ Ci zze; // = new Ci("MOBILE_DUN", 4, 4);
        @DexIgnore
        public static /* final */ Ci zzf; // = new Ci("MOBILE_HIPRI", 5, 5);
        @DexIgnore
        public static /* final */ Ci zzg; // = new Ci("WIMAX", 6, 6);
        @DexIgnore
        public static /* final */ Ci zzh; // = new Ci(InAppPermission.BLUETOOTH, 7, 7);
        @DexIgnore
        public static /* final */ Ci zzi; // = new Ci("DUMMY", 8, 8);
        @DexIgnore
        public static /* final */ Ci zzj; // = new Ci("ETHERNET", 9, 9);
        @DexIgnore
        public static /* final */ Ci zzk; // = new Ci("MOBILE_FOTA", 10, 10);
        @DexIgnore
        public static /* final */ Ci zzl; // = new Ci("MOBILE_IMS", 11, 11);
        @DexIgnore
        public static /* final */ Ci zzm; // = new Ci("MOBILE_CBS", 12, 12);
        @DexIgnore
        public static /* final */ Ci zzn; // = new Ci("WIFI_P2P", 13, 13);
        @DexIgnore
        public static /* final */ Ci zzo; // = new Ci("MOBILE_IA", 14, 14);
        @DexIgnore
        public static /* final */ Ci zzp; // = new Ci("MOBILE_EMERGENCY", 15, 15);
        @DexIgnore
        public static /* final */ Ci zzq; // = new Ci("PROXY", 16, 16);
        @DexIgnore
        public static /* final */ Ci zzr; // = new Ci("VPN", 17, 17);
        @DexIgnore
        public static /* final */ Ci zzs; // = new Ci("NONE", 18, -1);
        @DexIgnore
        public /* final */ int zzu;

        /*
        static {
            SparseArray<Ci> sparseArray = new SparseArray<>();
            b = sparseArray;
            sparseArray.put(0, zza);
            b.put(1, zzb);
            b.put(2, zzc);
            b.put(3, zzd);
            b.put(4, zze);
            b.put(5, zzf);
            b.put(6, zzg);
            b.put(7, zzh);
            b.put(8, zzi);
            b.put(9, zzj);
            b.put(10, zzk);
            b.put(11, zzl);
            b.put(12, zzm);
            b.put(13, zzn);
            b.put(14, zzo);
            b.put(15, zzp);
            b.put(16, zzq);
            b.put(17, zzr);
            b.put(-1, zzs);
        }
        */

        @DexIgnore
        public Ci(String str, int i, int i2) {
            this.zzu = i2;
        }

        @DexIgnore
        public static Ci zza(int i) {
            return b.get(i);
        }

        @DexIgnore
        public int zza() {
            return this.zzu;
        }
    }

    @DexIgnore
    public static Ai a() {
        return new Jz1.Bi();
    }

    @DexIgnore
    public abstract Bi b();

    @DexIgnore
    public abstract Ci c();
}
