package com.fossil;

import android.content.Context;
import android.os.Build;
import com.fossil.M62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oh2 {
    @DexIgnore
    @Deprecated
    public static /* final */ M62<M62.Di.Dii> a; // = Fn2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ M62<M62.Di.Dii> b; // = Zm2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ M62<M62.Di.Dii> c; // = Op2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ Nh2 d; // = new Po2();

    /*
    static {
        M62<M62.Di.Dii> m62 = Kn2.G;
        M62<M62.Di.Dii> m622 = On2.G;
        M62<M62.Di.Dii> m623 = Up2.G;
        M62<M62.Di.Dii> m624 = Kp2.G;
        if (Build.VERSION.SDK_INT < 18) {
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static Rh2 a(Context context, GoogleSignInAccount googleSignInAccount) {
        Rc2.k(googleSignInAccount);
        return new Rh2(context, new Kj2(context, googleSignInAccount));
    }

    @DexIgnore
    public static Th2 b(Context context, GoogleSignInAccount googleSignInAccount) {
        Rc2.k(googleSignInAccount);
        return new Th2(context, new Kj2(context, googleSignInAccount));
    }
}
