package com.fossil;

import com.fossil.A18;
import com.mapped.Dx6;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K88<T> implements Call<T> {
    @DexIgnore
    public /* final */ P88 b;
    @DexIgnore
    public /* final */ Object[] c;
    @DexIgnore
    public /* final */ A18.Ai d;
    @DexIgnore
    public /* final */ E88<W18, T> e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public A18 g;
    @DexIgnore
    public Throwable h;
    @DexIgnore
    public boolean i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements B18 {
        @DexIgnore
        public /* final */ /* synthetic */ Dx6 b;

        @DexIgnore
        public Ai(Dx6 dx6) {
            this.b = dx6;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.b.onFailure(K88.this, th);
            } catch (Throwable th2) {
                U88.t(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        @Override // com.fossil.B18
        public void onFailure(A18 a18, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        @Override // com.fossil.B18
        public void onResponse(A18 a18, Response response) {
            try {
                try {
                    this.b.onResponse(K88.this, K88.this.g(response));
                } catch (Throwable th) {
                    U88.t(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                U88.t(th2);
                a(th2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends W18 {
        @DexIgnore
        public /* final */ W18 b;
        @DexIgnore
        public /* final */ K48 c;
        @DexIgnore
        public IOException d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends N48 {
            @DexIgnore
            public Aii(C58 c58) {
                super(c58);
            }

            @DexIgnore
            @Override // com.fossil.N48, com.fossil.C58
            public long d0(I48 i48, long j) throws IOException {
                try {
                    return super.d0(i48, j);
                } catch (IOException e) {
                    Bi.this.d = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public Bi(W18 w18) {
            this.b = w18;
            this.c = S48.d(new Aii(w18.source()));
        }

        @DexIgnore
        public void a() throws IOException {
            IOException iOException = this.d;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        @Override // com.fossil.W18, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.b.close();
        }

        @DexIgnore
        @Override // com.fossil.W18
        public long contentLength() {
            return this.b.contentLength();
        }

        @DexIgnore
        @Override // com.fossil.W18
        public R18 contentType() {
            return this.b.contentType();
        }

        @DexIgnore
        @Override // com.fossil.W18
        public K48 source() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends W18 {
        @DexIgnore
        public /* final */ R18 b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public Ci(R18 r18, long j) {
            this.b = r18;
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.W18
        public long contentLength() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.W18
        public R18 contentType() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.W18
        public K48 source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public K88(P88 p88, Object[] objArr, A18.Ai ai, E88<W18, T> e88) {
        this.b = p88;
        this.c = objArr;
        this.d = ai;
        this.e = e88;
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void D(Dx6<T> dx6) {
        Throwable th;
        A18 a18;
        U88.b(dx6, "callback == null");
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                A18 a182 = this.g;
                th = this.h;
                if (a182 == null && th == null) {
                    try {
                        a18 = e();
                        this.g = a18;
                    } catch (Throwable th2) {
                        th = th2;
                        U88.t(th);
                        this.h = th;
                        a18 = a182;
                    }
                } else {
                    a18 = a182;
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            dx6.onFailure(this, th);
            return;
        }
        if (this.f) {
            a18.cancel();
        }
        a18.m(new Ai(dx6));
    }

    @DexIgnore
    @Override // retrofit2.Call
    public /* bridge */ /* synthetic */ Call L() {
        return d();
    }

    @DexIgnore
    @Override // retrofit2.Call
    public Q88<T> a() throws IOException {
        A18 a18;
        synchronized (this) {
            if (!this.i) {
                this.i = true;
                if (this.h == null) {
                    a18 = this.g;
                    if (a18 == null) {
                        try {
                            a18 = e();
                            this.g = a18;
                        } catch (IOException | Error | RuntimeException e2) {
                            U88.t(e2);
                            this.h = e2;
                            throw e2;
                        }
                    }
                } else if (this.h instanceof IOException) {
                    throw ((IOException) this.h);
                } else if (this.h instanceof RuntimeException) {
                    throw ((RuntimeException) this.h);
                } else {
                    throw ((Error) this.h);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.f) {
            a18.cancel();
        }
        return g(a18.a());
    }

    @DexIgnore
    @Override // retrofit2.Call
    public V18 c() {
        V18 c2;
        synchronized (this) {
            A18 a18 = this.g;
            if (a18 != null) {
                c2 = a18.c();
            } else if (this.h == null) {
                try {
                    A18 e2 = e();
                    this.g = e2;
                    c2 = e2.c();
                } catch (Error | RuntimeException e3) {
                    U88.t(e3);
                    this.h = e3;
                    throw e3;
                } catch (IOException e4) {
                    this.h = e4;
                    throw new RuntimeException("Unable to create request.", e4);
                }
            } else if (this.h instanceof IOException) {
                throw new RuntimeException("Unable to create request.", this.h);
            } else if (this.h instanceof RuntimeException) {
                throw ((RuntimeException) this.h);
            } else {
                throw ((Error) this.h);
            }
        }
        return c2;
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void cancel() {
        A18 a18;
        this.f = true;
        synchronized (this) {
            a18 = this.g;
        }
        if (a18 != null) {
            a18.cancel();
        }
    }

    @DexIgnore
    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return d();
    }

    @DexIgnore
    public K88<T> d() {
        return new K88<>(this.b, this.c, this.d, this.e);
    }

    @DexIgnore
    public final A18 e() throws IOException {
        A18 d2 = this.d.d(this.b.a(this.c));
        if (d2 != null) {
            return d2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    @Override // retrofit2.Call
    public boolean f() {
        boolean z = true;
        if (!this.f) {
            synchronized (this) {
                if (this.g == null || !this.g.f()) {
                    z = false;
                }
            }
        }
        return z;
    }

    @DexIgnore
    public Q88<T> g(Response response) throws IOException {
        W18 a2 = response.a();
        Response.a B = response.B();
        B.b(new Ci(a2.contentType(), a2.contentLength()));
        Response c2 = B.c();
        int f2 = c2.f();
        if (f2 < 200 || f2 >= 300) {
            try {
                return Q88.c(U88.a(a2), c2);
            } finally {
                a2.close();
            }
        } else if (f2 == 204 || f2 == 205) {
            a2.close();
            return Q88.h(null, c2);
        } else {
            Bi bi = new Bi(a2);
            try {
                return Q88.h(this.e.a(bi), c2);
            } catch (RuntimeException e2) {
                bi.a();
                throw e2;
            }
        }
    }
}
