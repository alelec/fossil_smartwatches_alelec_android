package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class Ey0 extends FrameLayout {
    @DexIgnore
    public ViewGroup b;
    @DexIgnore
    public boolean c; // = true;

    @DexIgnore
    public Ey0(ViewGroup viewGroup) {
        super(viewGroup.getContext());
        setClipChildren(false);
        this.b = viewGroup;
        viewGroup.setTag(Ny0.ghost_view_holder, this);
        Cz0.b(this.b).c(this);
    }

    @DexIgnore
    public static Ey0 b(ViewGroup viewGroup) {
        return (Ey0) viewGroup.getTag(Ny0.ghost_view_holder);
    }

    @DexIgnore
    public static void d(View view, ArrayList<View> arrayList) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            d((View) parent, arrayList);
        }
        arrayList.add(view);
    }

    @DexIgnore
    public static boolean e(View view, View view2) {
        boolean z = false;
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        int childCount = viewGroup.getChildCount();
        if (Build.VERSION.SDK_INT >= 21 && view.getZ() != view2.getZ()) {
            return view.getZ() > view2.getZ();
        }
        int i = 0;
        while (true) {
            if (i < childCount) {
                View childAt = viewGroup.getChildAt(Cz0.a(viewGroup, i));
                if (childAt == view) {
                    break;
                } else if (childAt == view2) {
                    break;
                } else {
                    i++;
                }
            } else {
                break;
            }
        }
        z = true;
        return z;
    }

    @DexIgnore
    public static boolean f(ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        if (arrayList.isEmpty() || arrayList2.isEmpty()) {
            return true;
        }
        if (arrayList.get(0) != arrayList2.get(0)) {
            return true;
        }
        int min = Math.min(arrayList.size(), arrayList2.size());
        for (int i = 1; i < min; i++) {
            View view = arrayList.get(i);
            View view2 = arrayList2.get(i);
            if (view != view2) {
                return e(view, view2);
            }
        }
        return arrayList2.size() == min;
    }

    @DexIgnore
    public void a(Gy0 gy0) {
        ArrayList<View> arrayList = new ArrayList<>();
        d(gy0.d, arrayList);
        int c2 = c(arrayList);
        if (c2 < 0 || c2 >= getChildCount()) {
            addView(gy0);
        } else {
            addView(gy0, c2);
        }
    }

    @DexIgnore
    public final int c(ArrayList<View> arrayList) {
        int i;
        ArrayList arrayList2 = new ArrayList();
        int childCount = getChildCount() - 1;
        int i2 = 0;
        while (i2 <= childCount) {
            int i3 = (i2 + childCount) / 2;
            d(((Gy0) getChildAt(i3)).d, arrayList2);
            if (f(arrayList, arrayList2)) {
                i = i3 + 1;
            } else {
                childCount = i3 - 1;
                i = i2;
            }
            arrayList2.clear();
            i2 = i;
        }
        return i2;
    }

    @DexIgnore
    public void g() {
        if (this.c) {
            Cz0.b(this.b).d(this);
            Cz0.b(this.b).c(this);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }

    @DexIgnore
    public void onViewAdded(View view) {
        if (this.c) {
            super.onViewAdded(view);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }

    @DexIgnore
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        if ((getChildCount() == 1 && getChildAt(0) == view) || getChildCount() == 0) {
            this.b.setTag(Ny0.ghost_view_holder, null);
            Cz0.b(this.b).d(this);
            this.c = false;
        }
    }
}
