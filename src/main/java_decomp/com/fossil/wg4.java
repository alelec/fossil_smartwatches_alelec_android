package com.fossil;

import com.google.firebase.installations.FirebaseInstallationsRegistrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Wg4 implements D74 {
    @DexIgnore
    public static /* final */ Wg4 a; // = new Wg4();

    @DexIgnore
    public static D74 b() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.D74
    public Object a(B74 b74) {
        return FirebaseInstallationsRegistrar.lambda$getComponents$0(b74);
    }
}
