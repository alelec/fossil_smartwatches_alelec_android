package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S33 implements V33 {
    @DexIgnore
    public /* final */ /* synthetic */ Xz2 a;

    @DexIgnore
    public S33(Xz2 xz2) {
        this.a = xz2;
    }

    @DexIgnore
    @Override // com.fossil.V33
    public final byte zza(int i) {
        return this.a.zza(i);
    }

    @DexIgnore
    @Override // com.fossil.V33
    public final int zza() {
        return this.a.zza();
    }
}
