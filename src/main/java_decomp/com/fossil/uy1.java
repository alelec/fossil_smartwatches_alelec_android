package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Uy1<T> {
    @DexIgnore
    public static <T> Uy1<T> d(T t) {
        return new Sy1(null, t, Vy1.DEFAULT);
    }

    @DexIgnore
    public static <T> Uy1<T> e(T t) {
        return new Sy1(null, t, Vy1.VERY_LOW);
    }

    @DexIgnore
    public static <T> Uy1<T> f(T t) {
        return new Sy1(null, t, Vy1.HIGHEST);
    }

    @DexIgnore
    public abstract Integer a();

    @DexIgnore
    public abstract T b();

    @DexIgnore
    public abstract Vy1 c();
}
