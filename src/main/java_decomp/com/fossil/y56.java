package com.fossil;

import com.fossil.Tq4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y56 extends Tq4<Bi, Ci, Ai> {
    @DexIgnore
    public static /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tq4.Ai {
        @DexIgnore
        public Ai(String str) {
            Wg6.c(str, "message");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Ci {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.contact.ContactGroup> */
        /* JADX WARN: Multi-variable type inference failed */
        public Ci(List<? extends ContactGroup> list) {
            Wg6.c(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = Y56.class.getSimpleName();
        Wg6.b(simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Bi bi) {
        f(bi);
    }

    @DexIgnore
    public void f(Bi bi) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            b().onSuccess(new Ci(allContactGroups));
        } else {
            b().a(new Ai("Get all contact group failed"));
        }
    }
}
