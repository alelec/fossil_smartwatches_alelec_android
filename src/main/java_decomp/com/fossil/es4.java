package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Fs4;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Es4 extends U47 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ Ai A; // = new Ai(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public List<Gs4> k; // = Hm7.e();
    @DexIgnore
    public Fs4 l;
    @DexIgnore
    public /* final */ Zp0 m; // = new Sr4(this);
    @DexIgnore
    public G37<K15> s;
    @DexIgnore
    public My5 t;
    @DexIgnore
    public Gs4 u;
    @DexIgnore
    public String v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Es4.z;
        }

        @DexIgnore
        public final Es4 b() {
            return new Es4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Es4 b;

        @DexIgnore
        public Bi(Es4 es4) {
            this.b = es4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Es4 b;

        @DexIgnore
        public Ci(Es4 es4) {
            this.b = es4;
        }

        @DexIgnore
        public final void onClick(View view) {
            My5 my5;
            Gs4 gs4 = this.b.u;
            if (!(gs4 == null || (my5 = this.b.t) == null)) {
                my5.a(gs4);
            }
            this.b.dismiss();
        }
    }

    /*
    static {
        String name = Es4.class.getName();
        Wg6.b(name, "BottomDialog::class.java.name");
        z = name;
    }
    */

    @DexIgnore
    public final void D6() {
        this.l = new Fs4(this.k, this);
        G37<K15> g37 = this.s;
        if (g37 != null) {
            K15 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                Wg6.b(flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.v);
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                Wg6.b(recyclerView, "this");
                Fs4 fs4 = this.l;
                if (fs4 != null) {
                    recyclerView.setAdapter(fs4);
                    recyclerView.setHasFixedSize(true);
                    a2.t.setOnClickListener(new Bi(this));
                    if (this.w) {
                        FlexibleButton flexibleButton = a2.q;
                        Wg6.b(flexibleButton, "btnOk");
                        flexibleButton.setVisibility(0);
                        RTLImageView rTLImageView = a2.t;
                        Wg6.b(rTLImageView, "ivClose");
                        rTLImageView.setVisibility(8);
                    } else {
                        FlexibleButton flexibleButton2 = a2.q;
                        Wg6.b(flexibleButton2, "btnOk");
                        flexibleButton2.setVisibility(8);
                        RTLImageView rTLImageView2 = a2.t;
                        Wg6.b(rTLImageView2, "ivClose");
                        rTLImageView2.setVisibility(0);
                    }
                    a2.q.setOnClickListener(new Ci(this));
                    return;
                }
                Wg6.n("bottomDialogAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final Es4 E6(List<Gs4> list) {
        int i;
        Wg6.c(list, "newData");
        Iterator<Gs4> it = list.iterator();
        int i2 = 0;
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (it.next().c()) {
                break;
            } else {
                i2 = i + 1;
            }
        }
        if (i != -1) {
            this.u = list.get(i);
        }
        this.k = list;
        return this;
    }

    @DexIgnore
    public final void F6(boolean z2) {
        this.w = z2;
    }

    @DexIgnore
    public final void G6(My5 my5) {
        Wg6.c(my5, "listener");
        this.t = my5;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        D6();
    }

    @DexIgnore
    public void onClick(View view) {
        Object tag = view != null ? view.getTag() : null;
        if (tag != null) {
            Fs4.Ai ai = (Fs4.Ai) tag;
            Gs4 gs4 = this.k.get(ai.getAdapterPosition());
            this.u = gs4;
            if (!this.x) {
                Iterator<Gs4> it = this.k.iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i = -1;
                        break;
                    } else if (it.next().c()) {
                        break;
                    } else {
                        i++;
                    }
                }
                int adapterPosition = ai.getAdapterPosition();
                if (!(i == -1 || i == adapterPosition)) {
                    this.k.get(i).d(false);
                    this.k.get(adapterPosition).d(true);
                    Fs4 fs4 = this.l;
                    if (fs4 != null) {
                        fs4.notifyDataSetChanged();
                    } else {
                        Wg6.n("bottomDialogAdapter");
                        throw null;
                    }
                }
            }
            if (!this.w) {
                My5 my5 = this.t;
                if (my5 != null) {
                    my5.a(gs4);
                }
                dismiss();
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.customview.BottomDialogAdapter.PrivacyHolder");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        K15 k15 = (K15) Aq0.f(layoutInflater, 2131558443, viewGroup, false, this.m);
        this.s = new G37<>(this, k15);
        Wg6.b(k15, "binding");
        return k15.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    public final void setTitle(String str) {
        Wg6.c(str, "title");
        this.v = str;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
