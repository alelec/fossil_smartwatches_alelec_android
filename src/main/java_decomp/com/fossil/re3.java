package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Re3 extends Ce3 {
    @DexIgnore
    public Re3() {
        super(2);
    }

    @DexIgnore
    @Override // com.fossil.Ce3
    public final String toString() {
        return "[RoundCap]";
    }
}
