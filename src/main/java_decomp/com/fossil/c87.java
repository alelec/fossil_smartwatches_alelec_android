package com.fossil;

import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.WatchFaceEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C87 implements Factory<WatchFaceEditViewModel> {
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataManager> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<CustomizeRealDataRepository> c;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<FileRepository> e;
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> f;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> g;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> h;

    @DexIgnore
    public C87(Provider<CustomizeRealDataManager> provider, Provider<UserRepository> provider2, Provider<CustomizeRealDataRepository> provider3, Provider<DianaPresetRepository> provider4, Provider<FileRepository> provider5, Provider<WFAssetRepository> provider6, Provider<DianaAppSettingRepository> provider7, Provider<DianaWatchFaceRepository> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static C87 a(Provider<CustomizeRealDataManager> provider, Provider<UserRepository> provider2, Provider<CustomizeRealDataRepository> provider3, Provider<DianaPresetRepository> provider4, Provider<FileRepository> provider5, Provider<WFAssetRepository> provider6, Provider<DianaAppSettingRepository> provider7, Provider<DianaWatchFaceRepository> provider8) {
        return new C87(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static WatchFaceEditViewModel c(CustomizeRealDataManager customizeRealDataManager, UserRepository userRepository, CustomizeRealDataRepository customizeRealDataRepository, DianaPresetRepository dianaPresetRepository, FileRepository fileRepository, WFAssetRepository wFAssetRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        return new WatchFaceEditViewModel(customizeRealDataManager, userRepository, customizeRealDataRepository, dianaPresetRepository, fileRepository, wFAssetRepository, dianaAppSettingRepository, dianaWatchFaceRepository);
    }

    @DexIgnore
    public WatchFaceEditViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
