package com.fossil;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.fossil.Af1;
import com.fossil.Wb1;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rj5 implements Af1<Sj5, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements Wb1<InputStream> {
        @DexIgnore
        public Wb1<InputStream> b;
        @DexIgnore
        public volatile boolean c;
        @DexIgnore
        public /* final */ Sj5 d;

        @DexIgnore
        public Ai(Rj5 rj5, Sj5 sj5) {
            Wg6.c(sj5, "mAvatarModel");
            this.d = sj5;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
            Wb1<InputStream> wb1 = this.b;
            if (wb1 != null) {
                wb1.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.REMOTE;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
            this.c = true;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super InputStream> ai) {
            Wg6.c(sa1, "priority");
            Wg6.c(ai, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.d.e())) {
                    this.b = new Cc1(new Te1(this.d.e()), 100);
                } else if (this.d.d() != null) {
                    this.b = new Hc1(PortfolioApp.get.instance().getContentResolver(), this.d.d());
                }
                Wb1<InputStream> wb1 = this.b;
                if (wb1 != null) {
                    wb1.d(sa1, ai);
                }
            } catch (Exception e) {
                Wb1<InputStream> wb12 = this.b;
                if (wb12 != null) {
                    wb12.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String c2 = this.d.c();
            if (c2 == null) {
                c2 = "";
            }
            Bitmap j = I37.j(c2);
            if (j != null) {
                j.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            ai.e(this.c ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Bf1<Sj5, InputStream> {
        @DexIgnore
        public Rj5 a(Ef1 ef1) {
            Wg6.c(ef1, "multiFactory");
            return new Rj5();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Af1' to match base method */
        @Override // com.fossil.Bf1
        public /* bridge */ /* synthetic */ Af1<Sj5, InputStream> b(Ef1 ef1) {
            return a(ef1);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Sj5 sj5) {
        return d(sj5);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Sj5 sj5, int i, int i2, Ob1 ob1) {
        return c(sj5, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(Sj5 sj5, int i, int i2, Ob1 ob1) {
        Wg6.c(sj5, "avatarModel");
        Wg6.c(ob1, "options");
        return new Af1.Ai<>(sj5, new Ai(this, sj5));
    }

    @DexIgnore
    public boolean d(Sj5 sj5) {
        Wg6.c(sj5, "avatarModel");
        return true;
    }
}
