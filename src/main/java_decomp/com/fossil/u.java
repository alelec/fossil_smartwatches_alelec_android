package com.fossil;

import com.fossil.blesdk.database.SdkDatabase;
import com.mapped.Mi;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U {
    @DexIgnore
    public static /* final */ U a; // = new U();

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int a(String str) {
        Integer num;
        Z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            Mi acquire = a2.e.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            a2.a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                a2.e.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.a.endTransaction();
                a2.e.release(acquire);
                throw th;
            }
        }
        G80.k(new JSONObject(), Jd0.w4, num != null ? num : -1);
        R r = R.e;
        M80.c.a("SdkDatabaseWrapper", "deleteFileByDeviceMacAddress: deviceMacAddress=%s, numberOfDeletedRow=%d.", str, num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int b(String str, byte b) {
        Integer num;
        Z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            Mi acquire = a2.d.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            a2.a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                a2.d.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.a.endTransaction();
                a2.d.release(acquire);
                throw th;
            }
        }
        G80.k(G80.k(new Kb(b, (byte) 255).toJSONObject(), Jd0.N2, JSONObject.NULL), Jd0.w4, num != null ? num : -1);
        R r = R.c;
        M80.c.a("SdkDatabaseWrapper", "deleteFileByFileType: deviceMacAddress=%s, fileType=%d, numberOfDeletedRow=%d.", str, Byte.valueOf(b), num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int c(String str, byte b, byte b2) {
        Integer num;
        Z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            num = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            Mi acquire = a2.c.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            acquire.bindLong(3, (long) b2);
            a2.a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                a2.c.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.a.endTransaction();
                a2.c.release(acquire);
                throw th;
            }
        }
        G80.k(G80.k(new Kb(b, b2).toJSONObject(), Jd0.N2, Boolean.FALSE), Jd0.w4, num != null ? num : -1);
        R r = R.c;
        M80.c.a("SdkDatabaseWrapper", "deleteIncompleteFileByFileHandle: deviceMacAddress=%s, fileType=%d, fileIndex=%d, numberOfDeletedRow=%d.", str, Byte.valueOf(b), Byte.valueOf(b2), num);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final long d(J0 j0) {
        Long l;
        Z a2;
        SdkDatabase c = SdkDatabase.g.c();
        if (c == null || (a2 = c.a()) == null) {
            l = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            a2.a.beginTransaction();
            try {
                long insertAndReturnId = a2.b.insertAndReturnId(j0);
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                l = Long.valueOf(insertAndReturnId);
            } catch (Throwable th) {
                a2.a.endTransaction();
                throw th;
            }
        }
        Zr7 n = Bs7.n(Long.MIN_VALUE, 0);
        if (l != null) {
            n.e(l.longValue());
        }
        G80.k(G80.k(new JSONObject(), Jd0.u4, j0.a(true)), Jd0.v4, l != null ? l : -1);
        R r = R.b;
        M80.c.a("SdkDatabaseWrapper", "addFile: macAddress=%s, fileType=%d, fileIndex=%d, fileCrc=%d, dataLength=%d, fileLength=%d, isCompleted=%b, insertedRowId=%d.", j0.c, Byte.valueOf(j0.d), Byte.valueOf(j0.e), Long.valueOf(j0.h), Integer.valueOf(j0.f.length), Long.valueOf(j0.g), Boolean.valueOf(j0.j), l);
        if (l != null) {
            return l.longValue();
        }
        return -1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.K0 r13, com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 447
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U.e(com.fossil.K0, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.fossil.Ec0 r12, com.fossil.Ry1 r13, com.mapped.Xe6<? super com.fossil.Hc0> r14) {
        /*
        // Method dump skipped, instructions count: 284
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U.f(com.fossil.Ec0, com.fossil.Ry1, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void g() {
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
        if (r2 != null) goto L_0x011e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.J0> h(java.lang.String r27, byte r28) {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U.h(java.lang.String, byte):java.util.List");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0122, code lost:
        if (r2 != null) goto L_0x0124;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.J0> i(java.lang.String r27, byte r28, byte r29) {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U.i(java.lang.String, byte, byte):java.util.List");
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011c, code lost:
        if (r2 != null) goto L_0x011e;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.J0> j(java.lang.String r27, byte r28) {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U.j(java.lang.String, byte):java.util.List");
    }
}
