package com.fossil;

import com.fossil.O91;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ia1<T> extends M91<T> {
    @DexIgnore
    public static /* final */ String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ String PROTOCOL_CONTENT_TYPE; // = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    @DexIgnore
    public O91.Bi<T> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ String mRequestBody;

    @DexIgnore
    public Ia1(int i, String str, String str2, O91.Bi<T> bi, O91.Ai ai) {
        super(i, str, ai);
        this.mLock = new Object();
        this.mListener = bi;
        this.mRequestBody = str2;
    }

    @DexIgnore
    @Deprecated
    public Ia1(String str, String str2, O91.Bi<T> bi, O91.Ai ai) {
        this(-1, str, str2, bi, ai);
    }

    @DexIgnore
    @Override // com.fossil.M91
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.M91
    public void deliverResponse(T t) {
        O91.Bi<T> bi;
        synchronized (this.mLock) {
            bi = this.mListener;
        }
        if (bi != null) {
            bi.onResponse(t);
        }
    }

    @DexIgnore
    @Override // com.fossil.M91
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            U91.f("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.M91
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @Override // com.fossil.M91
    @Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @Override // com.fossil.M91
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    @Override // com.fossil.M91
    public abstract O91<T> parseNetworkResponse(J91 j91);
}
