package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H24 extends I34<Object, Object> {
    @DexIgnore
    public static /* final */ H24 INSTANCE; // = new H24();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    public H24() {
        super(A34.of(), 0, null);
    }

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }
}
