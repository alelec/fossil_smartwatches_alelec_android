package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cr3 implements Callable<String> {
    @DexIgnore
    public /* final */ /* synthetic */ Or3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Yq3 b;

    @DexIgnore
    public Cr3(Yq3 yq3, Or3 or3) {
        this.b = yq3;
        this.a = or3;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public final /* synthetic */ String call() throws Exception {
        Ll3 P = this.b.P(this.a);
        if (P != null) {
            return P.x();
        }
        this.b.d().I().a("App info was null when attempting to get app instance id");
        return null;
    }
}
