package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.uirenew.customview.TitleValueCell;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.zendesk.sdk.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zb5 extends Yb5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d L; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray M;
    @DexIgnore
    public /* final */ ConstraintLayout J;
    @DexIgnore
    public long K;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        M = sparseIntArray;
        sparseIntArray.put(2131363410, 1);
        M.put(2131362666, 2);
        M.put(2131362627, 3);
        M.put(2131363170, 4);
        M.put(R.id.scroll, 5);
        M.put(2131362428, 6);
        M.put(2131362049, 7);
        M.put(2131362624, 8);
        M.put(2131362374, 9);
        M.put(2131362382, 10);
        M.put(2131362544, 11);
        M.put(2131362404, 12);
        M.put(2131362390, 13);
        M.put(2131362340, 14);
        M.put(2131362216, 15);
        M.put(2131363148, 16);
        M.put(2131362966, 17);
        M.put(2131363431, 18);
        M.put(2131361965, 19);
    }
    */

    @DexIgnore
    public Zb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 20, L, M));
    }

    @DexIgnore
    public Zb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[19], (ConstraintLayout) objArr[7], (TitleValueCell) objArr[15], (FriendsInView) objArr[14], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[6], (TimerTextView) objArr[11], (ImageView) objArr[8], (RTLImageView) objArr[3], (RTLImageView) objArr[2], (TitleValueCell) objArr[17], (NestedScrollView) objArr[5], (TitleValueCell) objArr[16], (SwipeRefreshLayout) objArr[4], (FlexibleTextView) objArr[1], (TitleValueCell) objArr[18]);
        this.K = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.J = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.K = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.K != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.K = 1;
        }
        w();
    }
}
