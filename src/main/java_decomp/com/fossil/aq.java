package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aq extends Rp {
    @DexIgnore
    public /* final */ boolean U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Aq(com.fossil.K5 r10, com.fossil.I60 r11, boolean r12, java.lang.String r13, int r14) {
        /*
            r9 = this;
            r0 = r14 & 8
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = "UUID.randomUUID().toString()"
            java.lang.String r7 = com.fossil.E.a(r0)
        L_0x000a:
            com.fossil.Yp r3 = com.fossil.Yp.C
            com.fossil.Rb r0 = new com.fossil.Rb
            r0.<init>(r12)
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0036 }
            r1.<init>()     // Catch:{ JSONException -> 0x0036 }
            java.lang.String r2 = "set"
            org.json.JSONObject r0 = r0.a()     // Catch:{ JSONException -> 0x0036 }
            r1.put(r2, r0)     // Catch:{ JSONException -> 0x0036 }
            java.lang.String r0 = "push"
            r4.put(r0, r1)     // Catch:{ JSONException -> 0x0036 }
        L_0x0029:
            r5 = 0
            r6 = 0
            r8 = 48
            r0 = r9
            r1 = r10
            r2 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            r9.U = r12
            return
        L_0x0036:
            r0 = move-exception
            com.fossil.D90 r1 = com.fossil.D90.i
            r1.i(r0)
            goto L_0x0029
        L_0x003d:
            r7 = r13
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Aq.<init>(com.fossil.K5, com.fossil.I60, boolean, java.lang.String, int):void");
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.F, Boolean.valueOf(this.U));
    }
}
