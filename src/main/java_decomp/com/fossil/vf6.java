package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vf6 extends sf6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public /* final */ MutableLiveData<Date> g;
    @DexIgnore
    public /* final */ LiveData<h47<List<ActivitySummary>>> h;
    @DexIgnore
    public BarChart.c i;
    @DexIgnore
    public /* final */ tf6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ SummariesRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter", f = "ActivityOverviewWeekPresenter.kt", l = {159}, m = "calculateStartAndEndDate")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vf6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vf6 vf6, qn7 qn7) {
            super(qn7);
            this.this$0 = vf6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vf6 f3760a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$mActivitySummaries$1$1", f = "ActivityOverviewWeekPresenter.kt", l = {42, 47, 45}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<hs0<h47<? extends List<ActivitySummary>>>, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vf6$b$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$mActivitySummaries$1$1$startAndEnd$1", f = "ActivityOverviewWeekPresenter.kt", l = {42}, m = "invokeSuspend")
            /* renamed from: com.fossil.vf6$b$a$a  reason: collision with other inner class name */
            public static final class C0263a extends ko7 implements vp7<iv7, qn7<? super cl7<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0263a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0263a aVar = new C0263a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super cl7<? extends Date, ? extends Date>> qn7) {
                    throw null;
                    //return ((C0263a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        a aVar = this.this$0;
                        vf6 vf6 = aVar.this$0.f3760a;
                        Date date = aVar.$it;
                        pq7.b(date, "it");
                        this.L$0 = iv7;
                        this.label = 1;
                        Object w = vf6.w(date, this);
                        return w == d ? d : w;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$it, qn7);
                aVar.p$ = (hs0) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(hs0<h47<? extends List<ActivitySummary>>> hs0, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(hs0, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r6 = com.fossil.yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ab
                    if (r0 == r4) goto L_0x0049
                    if (r0 == r5) goto L_0x0027
                    if (r0 != r7) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    com.fossil.cl7 r0 = (com.fossil.cl7) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                L_0x001c:
                    com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                L_0x001e:
                    return r0
                L_0x001f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0027:
                    java.lang.Object r0 = r8.L$2
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    java.lang.Object r1 = r8.L$1
                    com.fossil.cl7 r1 = (com.fossil.cl7) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.hs0 r2 = (com.fossil.hs0) r2
                    com.fossil.el7.b(r9)
                    r5 = r0
                    r3 = r9
                L_0x0038:
                    r0 = r3
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r7
                    java.lang.Object r0 = r5.a(r0, r8)
                    if (r0 != r6) goto L_0x001c
                    r0 = r6
                    goto L_0x001e
                L_0x0049:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.hs0 r0 = (com.fossil.hs0) r0
                    com.fossil.el7.b(r9)
                    r4 = r0
                    r1 = r9
                L_0x0052:
                    r0 = r1
                    com.fossil.cl7 r0 = (com.fossil.cl7) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r1 = "mActivitySummaries onDateChanged - startDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = ", endDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getSecond()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = "ActivityOverviewWeekPresenter"
                    java.lang.String r3 = r3.toString()
                    r2.d(r1, r3)
                    com.fossil.vf6$b r1 = r8.this$0
                    com.fossil.vf6 r1 = r1.f3760a
                    com.portfolio.platform.data.source.SummariesRepository r3 = com.fossil.vf6.r(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    java.lang.Object r2 = r0.getSecond()
                    java.util.Date r2 = (java.util.Date) r2
                    r8.L$0 = r4
                    r8.L$1 = r0
                    r8.L$2 = r4
                    r8.label = r5
                    r5 = 0
                    java.lang.Object r3 = r3.getSummaries(r1, r2, r5, r8)
                    if (r3 != r6) goto L_0x00cb
                    r0 = r6
                    goto L_0x001e
                L_0x00ab:
                    com.fossil.el7.b(r9)
                    com.fossil.hs0 r0 = r8.p$
                    com.fossil.vf6$b r1 = r8.this$0
                    com.fossil.vf6 r1 = r1.f3760a
                    com.fossil.dv7 r1 = com.fossil.vf6.o(r1)
                    com.fossil.vf6$b$a$a r2 = new com.fossil.vf6$b$a$a
                    r3 = 0
                    r2.<init>(r8, r3)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.eu7.g(r1, r2, r8)
                    if (r1 != r6) goto L_0x00d0
                    r0 = r6
                    goto L_0x001e
                L_0x00cb:
                    r2 = r4
                    r1 = r0
                    r5 = r4
                    goto L_0x0038
                L_0x00d0:
                    r4 = r0
                    goto L_0x0052
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.vf6.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public b(vf6 vf6) {
            this.f3760a = vf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<h47<List<ActivitySummary>>> apply(Date date) {
            return or0.c(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<h47<? extends List<ActivitySummary>>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ vf6 f3761a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$1", f = "ActivityOverviewWeekPresenter.kt", l = {72}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vf6$c$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1$1$1", f = "ActivityOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.vf6$c$a$a  reason: collision with other inner class name */
            public static final class C0264a extends ko7 implements vp7<iv7, qn7<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0264a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0264a aVar = new C0264a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super BarChart.c> qn7) {
                    throw null;
                    //return ((C0264a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        vf6 vf6 = this.this$0.this$0.f3761a;
                        Date date = vf6.f;
                        if (date != null) {
                            return vf6.z(date, this.this$0.$data);
                        }
                        pq7.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, List list, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$data, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                vf6 vf6;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    vf6 vf62 = this.this$0.f3761a;
                    dv7 h = vf62.h();
                    C0264a aVar = new C0264a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = vf62;
                    this.label = 1;
                    g = eu7.g(h, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    vf6 = vf62;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    vf6 = (vf6) this.L$1;
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                vf6.i = (BarChart.c) g;
                tf6 tf6 = this.this$0.f3761a.j;
                BarChart.c cVar = this.this$0.f3761a.i;
                if (cVar == null) {
                    cVar = new BarChart.c(0, 0, null, 7, null);
                }
                tf6.p(cVar);
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public c(vf6 vf6) {
            this.f3761a = vf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(h47<? extends List<ActivitySummary>> h47) {
            xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("ActivityOverviewWeekPresenter", sb.toString());
            if (a2 != xh5.DATABASE_LOADING) {
                xw7 unused = gu7.d(this.f3761a.k(), null, null, new a(this, list, null), 3, null);
            }
        }
    }

    @DexIgnore
    public vf6(tf6 tf6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        pq7.c(tf6, "mView");
        pq7.c(userRepository, "userRepository");
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(portfolioApp, "mApp");
        this.j = tf6;
        this.k = userRepository;
        this.l = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<h47<List<ActivitySummary>>> c2 = ss0.c(mutableLiveData, new b(this));
        pq7.b(c2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.h = c2;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        x();
        Date date = this.f;
        if (date == null || !lk5.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.f = date2;
            this.g.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewWeekPresenter", "start with date " + this.f);
        LiveData<h47<List<ActivitySummary>>> liveData = this.h;
        tf6 tf6 = this.j;
        if (tf6 != null) {
            liveData.h((uf6) tf6, new c(this));
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("ActivityOverviewWeekPresenter", "start - mDate=" + this.f);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "stop");
        try {
            LiveData<h47<List<ActivitySummary>>> liveData = this.h;
            tf6 tf6 = this.j;
            if (tf6 != null) {
                liveData.n((uf6) tf6);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.sf6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        pq7.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(java.util.Date r6, com.fossil.qn7<? super com.fossil.cl7<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.vf6.a
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.fossil.vf6$a r0 = (com.fossil.vf6.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.fossil.dr7 r0 = (com.fossil.dr7) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.fossil.vf6 r2 = (com.fossil.vf6) r2
            com.fossil.el7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.fossil.lk5.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.fossil.lk5.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.fossil.cl7 r2 = new com.fossil.cl7
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.fossil.vf6$a r0 = new com.fossil.vf6$a
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.el7.b(r3)
            com.fossil.dr7 r0 = new com.fossil.dr7
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.fossil.lk5.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.k
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r1 = r6
            r4 = r0
            goto L_0x0031
        L_0x008a:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vf6.w(java.util.Date, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public void x() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void y() {
        this.j.M5(this);
    }

    @DexIgnore
    public final BarChart.c z(Date date, List<ActivitySummary> list) {
        T t;
        boolean z;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        pq7.b(instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 <= 6) {
            Date time = instance.getTime();
            pq7.b(time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (t2.getDay() == instance.get(5) && t2.getMonth() == instance.get(2) + 1 && t2.getYear() == instance.get(1)) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    i3 = sk5.c.d(t3, rh5.TOTAL_STEPS);
                    int steps = (int) t3.getSteps();
                    i2 = Math.max(Math.max(i3, steps), i2);
                    cVar.b().add(new BarChart.a(i3, hm7.c(hm7.c(new BarChart.b(0, null, 0, steps, null, 23, null))), time2, i4 == 6));
                } else {
                    cVar.b().add(new BarChart.a(i3, hm7.c(hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
                }
            } else {
                cVar.b().add(new BarChart.a(i3, hm7.c(hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
            }
            instance.add(5, 1);
            i3 = i3;
            i2 = i2;
            i4++;
        }
        if (i2 > 0) {
            i3 = i2;
        }
        cVar.f(i3);
        return cVar;
    }
}
