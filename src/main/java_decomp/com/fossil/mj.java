package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mj extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.d, Ow.e));
    @DexIgnore
    public /* final */ short D;

    @DexIgnore
    public Mj(K5 k5, I60 i60, Yp yp, short s, String str, boolean z) {
        super(k5, i60, yp, str, z);
        this.D = (short) s;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(G80.k(super.C(), Jd0.A0, Hy1.l(this.D, null, 1, null)), Jd0.g4, Kb.e.a(this.D));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean q(Lp lp) {
        return !(lp instanceof Mj) || ((Mj) lp).D != this.D;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean r(Fs fs) {
        Hs hs = fs != null ? fs.x : null;
        return hs != null && Aj.a[hs.ordinal()] == 1;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
