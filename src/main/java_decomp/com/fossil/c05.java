package com.fossil;

import android.text.TextUtils;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C05 {
    @DexIgnore
    public final List<Integer> a(String str) {
        Wg6.c(str, "arrayValue");
        ArrayList arrayList = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return arrayList;
        }
        JSONArray jSONArray = new JSONArray(str);
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                arrayList.add((Integer) obj);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Int");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final String b(List<Integer> list) {
        if (list == null) {
            return "";
        }
        String jSONArray = new JSONArray((Collection) list).toString();
        Wg6.b(jSONArray, "JSONArray(value).toString()");
        return jSONArray;
    }
}
