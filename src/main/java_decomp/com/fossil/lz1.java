package com.fossil;

import com.fossil.Fz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Ai a(Bz1 bz1);

        @DexIgnore
        public abstract Ai b(Bi bi);

        @DexIgnore
        public abstract Lz1 c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Bi extends Enum<Bi> {
        @DexIgnore
        public static /* final */ Bi zza; // = new Bi("UNKNOWN", 0, 0);
        @DexIgnore
        public static /* final */ Bi zzb; // = new Bi("ANDROID_FIREBASE", 1, 23);

        @DexIgnore
        public Bi(String str, int i, int i2) {
        }
    }

    @DexIgnore
    public static Ai a() {
        return new Fz1.Bi();
    }

    @DexIgnore
    public abstract Bz1 b();

    @DexIgnore
    public abstract Bi c();
}
