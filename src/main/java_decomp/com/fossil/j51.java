package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface J51 {
    @DexIgnore
    public static final Ai a = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ Ai a; // = new Ai();

        @DexIgnore
        public final J51 a() {
            int i = Build.VERSION.SDK_INT;
            return i >= 23 ? new L51() : i >= 19 ? new K51() : new I51();
        }
    }

    @DexIgnore
    String a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String d(Bitmap bitmap);

    @DexIgnore
    Bitmap removeLast();
}
