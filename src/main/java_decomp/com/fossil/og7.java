package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Og7 extends Enum<Og7> {
    @DexIgnore
    public static /* final */ Og7 a; // = new Og7("PAGE_VIEW", 0, 1);
    @DexIgnore
    public static /* final */ Og7 b; // = new Og7("SESSION_ENV", 1, 2);
    @DexIgnore
    public static /* final */ Og7 c; // = new Og7("ERROR", 2, 3);
    @DexIgnore
    public static /* final */ Og7 d; // = new Og7("CUSTOM", 3, 1000);
    @DexIgnore
    public static /* final */ Og7 e; // = new Og7("ADDITION", 4, 1001);
    @DexIgnore
    public static /* final */ Og7 f; // = new Og7("MONITOR_STAT", 5, 1002);
    @DexIgnore
    public static /* final */ Og7 g; // = new Og7("MTA_GAME_USER", 6, 1003);
    @DexIgnore
    public static /* final */ Og7 h; // = new Og7("NETWORK_MONITOR", 7, 1004);
    @DexIgnore
    public static /* final */ Og7 i; // = new Og7("NETWORK_DETECTOR", 8, 1005);
    @DexIgnore
    public int j;

    @DexIgnore
    public Og7(String str, int i2, int i3) {
        this.j = i3;
    }

    @DexIgnore
    public final int a() {
        return this.j;
    }
}
