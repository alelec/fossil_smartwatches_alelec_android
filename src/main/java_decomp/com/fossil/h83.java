package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H83 implements Xw2<K83> {
    @DexIgnore
    public static H83 c; // = new H83();
    @DexIgnore
    public /* final */ Xw2<K83> b;

    @DexIgnore
    public H83() {
        this(Ww2.b(new J83()));
    }

    @DexIgnore
    public H83(Xw2<K83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((K83) c.zza()).zza();
    }

    @DexIgnore
    public static double b() {
        return ((K83) c.zza()).zzb();
    }

    @DexIgnore
    public static long c() {
        return ((K83) c.zza()).zzc();
    }

    @DexIgnore
    public static long d() {
        return ((K83) c.zza()).zzd();
    }

    @DexIgnore
    public static String e() {
        return ((K83) c.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ K83 zza() {
        return this.b.zza();
    }
}
