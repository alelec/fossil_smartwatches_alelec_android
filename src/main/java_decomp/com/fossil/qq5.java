package com.fossil;

import android.text.TextUtils;
import com.fossil.v18;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq5 implements Interceptor {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public AtomicInteger f3007a; // = new AtomicInteger(0);
    @DexIgnore
    public Auth b;
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ on5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return qq5.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.retrofit.AuthenticationInterceptor$intercept$1", f = "AuthenticationInterceptor.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $currentUser;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MFUser mFUser, qn7 qn7) {
            super(2, qn7);
            this.$currentUser = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$currentUser, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                UserDatabase B = bn5.j.B();
                if (!(B == null || (userDao = B.userDao()) == null)) {
                    userDao.updateUser(this.$currentUser);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = qq5.class.getSimpleName();
        pq7.b(simpleName, "AuthenticationInterceptor::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public qq5(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, on5 on5) {
        pq7.c(portfolioApp, "mApplication");
        pq7.c(authApiGuestService, "mAuthApiGuestService");
        pq7.c(on5, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = authApiGuestService;
        this.e = on5;
    }

    @DexIgnore
    public final Auth b(MFUser mFUser, boolean z) {
        Auth auth;
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "getAuth needProcess=" + z + " mCount=" + this.f3007a);
            if (z) {
                gj4 gj4 = new gj4();
                gj4.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                try {
                    q88<Auth> a2 = this.d.tokenRefresh(gj4).a();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Refresh Token error=");
                    sb.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                    sb.append(" body=");
                    sb.append(a2 != null ? a2.d() : null);
                    local2.d(str2, sb.toString());
                    this.b = a2.a();
                } catch (Exception e2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    local3.d(str3, "getAuth needProcess=" + z + " mCount=" + this.f3007a + " exception=" + e2.getMessage());
                    e2.printStackTrace();
                    return null;
                }
            }
            auth = this.b;
        }
        return auth;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        MFUser.Auth auth;
        UserDao userDao;
        int i = 0;
        pq7.c(chain, "chain");
        v18.a h = chain.c().h();
        UserDatabase B = bn5.j.B();
        MFUser currentUser = (B == null || (userDao = B.userDao()) == null) ? null : userDao.getCurrentUser();
        String accessToken = (currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
        if (!TextUtils.isEmpty(accessToken) && currentUser != null) {
            long currentTimeMillis = (System.currentTimeMillis() - this.e.f()) / ((long) 1000);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "loginDurationInSeconds = " + currentTimeMillis + " tokenExpiresIn " + currentUser.getAuth().getAccessTokenExpiresIn());
            if (currentTimeMillis >= ((long) currentUser.getAuth().getAccessTokenExpiresIn())) {
                Auth b2 = b(currentUser, this.f3007a.getAndIncrement() == 0);
                if (this.f3007a.decrementAndGet() == 0) {
                    this.b = null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = f;
                local2.d(str2, "intercept decrementAndGet mCount=" + this.f3007a);
                if ((b2 != null ? b2.getAccessToken() : null) != null) {
                    MFUser.Auth auth2 = currentUser.getAuth();
                    String accessToken2 = b2.getAccessToken();
                    if (accessToken2 != null) {
                        auth2.setAccessToken(accessToken2);
                        MFUser.Auth auth3 = currentUser.getAuth();
                        String refreshToken = b2.getRefreshToken();
                        if (refreshToken != null) {
                            auth3.setRefreshToken(refreshToken);
                            MFUser.Auth auth4 = currentUser.getAuth();
                            String w0 = lk5.w0(b2.getAccessTokenExpiresAt());
                            pq7.b(w0, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                            auth4.setAccessTokenExpiresAt(w0);
                            MFUser.Auth auth5 = currentUser.getAuth();
                            Integer accessTokenExpiresIn = b2.getAccessTokenExpiresIn();
                            if (accessTokenExpiresIn != null) {
                                i = accessTokenExpiresIn.intValue();
                            }
                            auth5.setAccessTokenExpiresIn(i);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = f;
                            local3.d(str3, "accessToken = " + b2.getAccessToken() + ", refreshToken = " + b2.getRefreshToken());
                            xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(currentUser, null), 3, null);
                            this.e.V1(b2.getAccessToken());
                            this.e.H0(System.currentTimeMillis());
                            h.a("Authorization", "Bearer " + b2.getAccessToken());
                            h.b();
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Bearer ");
                    if (accessToken != null) {
                        sb.append(accessToken);
                        h.a("Authorization", sb.toString());
                        h.b();
                    } else {
                        pq7.i();
                        throw null;
                    }
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bearer ");
                if (accessToken != null) {
                    sb2.append(accessToken);
                    h.a("Authorization", sb2.toString());
                    h.b();
                } else {
                    pq7.i();
                    throw null;
                }
            }
        }
        h.a("Content-Type", Constants.CONTENT_TYPE);
        String g2 = this.e.g();
        if (g2 == null) {
            g2 = "";
        }
        h.a("X-Active-Device", g2);
        h.a("User-Agent", lh5.b.a());
        h.a("locale", this.c.X());
        try {
            Response d2 = chain.d(h.b());
            pq7.b(d2, "chain.proceed(requestBuilder.build())");
            return d2;
        } catch (Exception e2) {
            if (e2 instanceof ServerErrorException) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = f;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("exception=");
                ServerErrorException serverErrorException = (ServerErrorException) e2;
                sb3.append(serverErrorException.getServerError());
                local4.d(str4, sb3.toString());
                serverError = serverErrorException.getServerError();
            } else {
                serverError = e2 instanceof UnknownHostException ? new ServerError(601, "") : e2 instanceof SocketTimeoutException ? new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "") : new ServerError(600, "");
            }
            Response.a aVar = new Response.a();
            aVar.p(h.b());
            aVar.n(t18.HTTP_1_1);
            Integer code = serverError.getCode();
            pq7.b(code, "serverError.code");
            aVar.g(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.k(message);
            aVar.b(w18.create(r18.d(com.zendesk.sdk.network.Constants.APPLICATION_JSON), new Gson().t(serverError)));
            aVar.a("Content-Type", Constants.CONTENT_TYPE);
            aVar.a("User-Agent", lh5.b.a());
            String g3 = this.e.g();
            if (g3 == null) {
                g3 = "";
            }
            aVar.a("X-Active-Device", g3);
            aVar.a("Locale", this.c.X());
            Response c2 = aVar.c();
            pq7.b(c2, "Response.Builder()\n     \u2026                 .build()");
            return c2;
        }
    }
}
