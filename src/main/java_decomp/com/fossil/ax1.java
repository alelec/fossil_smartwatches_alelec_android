package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ax1 extends Yx1 {
    @DexIgnore
    public /* final */ Bx1 b;
    @DexIgnore
    public /* final */ Integer c;

    @DexIgnore
    public Ax1(Bx1 bx1, Integer num) {
        super(bx1);
        this.b = bx1;
        this.c = num;
    }

    @DexIgnore
    @Override // com.fossil.Yx1
    public Bx1 getErrorCode() {
        return this.b;
    }

    @DexIgnore
    public final Integer getHttpStatus() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Yx1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.d6, this.c);
    }
}
