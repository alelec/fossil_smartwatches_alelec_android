package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ns3 extends Gl2 implements Ks3 {
    @DexIgnore
    public Ns3() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.Gl2
    public boolean X2(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 3:
                o2((Z52) Il2.b(parcel, Z52.CREATOR), (Js3) Il2.b(parcel, Js3.CREATOR));
                break;
            case 4:
                f1((Status) Il2.b(parcel, Status.CREATOR));
                break;
            case 5:
            default:
                return false;
            case 6:
                s1((Status) Il2.b(parcel, Status.CREATOR));
                break;
            case 7:
                v((Status) Il2.b(parcel, Status.CREATOR), (GoogleSignInAccount) Il2.b(parcel, GoogleSignInAccount.CREATOR));
                break;
            case 8:
                Q2((Us3) Il2.b(parcel, Us3.CREATOR));
                break;
            case 9:
                M1((Os3) Il2.b(parcel, Os3.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
