package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tv3 implements Parcelable.Creator<Sv3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Sv3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = null;
        byte b = 0;
        byte b2 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                b = Ad2.o(parcel, t);
            } else if (l == 3) {
                b2 = Ad2.o(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                str = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Sv3(b, b2, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Sv3[] newArray(int i) {
        return new Sv3[i];
    }
}
