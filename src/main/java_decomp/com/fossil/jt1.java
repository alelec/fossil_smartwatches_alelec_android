package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ry1 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Jt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Gq1.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                Gq1 gq1 = (Gq1) readParcelable;
                Nt1 nt1 = (Nt1) parcel.readParcelable(Nt1.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(Ry1.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new Jt1(gq1, nt1, (Ry1) readParcelable2, parcel.readInt(), parcel.readInt());
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jt1[] newArray(int i) {
            return new Jt1[i];
        }
    }

    @DexIgnore
    public Jt1(Gq1 gq1, Nt1 nt1, Ry1 ry1, int i, int i2) {
        super(gq1, nt1);
        this.e = i;
        this.f = i2;
        this.d = ry1;
    }

    @DexIgnore
    public Jt1(Gq1 gq1, Ry1 ry1, int i, int i2) {
        super(gq1, null);
        this.e = i;
        this.f = i2;
        this.d = ry1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(G80.k(G80.k(super.a(), Jd0.t3, this.d.toString()), Jd0.A, Integer.valueOf(this.e)), Jd0.B, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        try {
            I9 i9 = I9.d;
            X90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return i9.a(s, ry1, new Ib0(((Lq1) deviceRequest).c(), new Ry1(this.d.getMajor(), this.d.getMinor()), this.e, this.f).a());
            }
            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (Sx1 e2) {
            D90.i.i(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Jt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Jt1 jt1 = (Jt1) obj;
            if (!Wg6.a(this.d, jt1.d)) {
                return false;
            }
            if (this.e != jt1.e) {
                return false;
            }
            return this.f == jt1.f;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeETAMicroAppData");
    }

    @DexIgnore
    public final int getHour() {
        return this.e;
    }

    @DexIgnore
    public final Ry1 getMicroAppVersion() {
        return this.d;
    }

    @DexIgnore
    public final int getMinute() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Integer.valueOf(this.e).hashCode()) * 31) + Integer.valueOf(this.f).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
