package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Md5 extends Ld5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131362660, 1);
        z.put(2131362830, 2);
        z.put(2131362367, 3);
        z.put(2131362372, 4);
        z.put(2131363161, 5);
        z.put(2131362783, 6);
    }
    */

    @DexIgnore
    public Md5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 7, y, z));
    }

    @DexIgnore
    public Md5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[4], (ImageView) objArr[1], (View) objArr[6], (ConstraintLayout) objArr[2], (FlexibleSwitchCompat) objArr[5]);
        this.x = -1;
        this.q.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.x != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.x = 1;
        }
        w();
    }
}
