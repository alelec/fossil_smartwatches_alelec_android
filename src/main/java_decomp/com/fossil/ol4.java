package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ol4 implements Ql4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Kl4.values().length];
            a = iArr;
            try {
                iArr[Kl4.EAN_8.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Kl4.UPC_E.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Kl4.EAN_13.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Kl4.UPC_A.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Kl4.QR_CODE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Kl4.CODE_39.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Kl4.CODE_93.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[Kl4.CODE_128.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Kl4.ITF.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[Kl4.PDF_417.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[Kl4.CODABAR.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[Kl4.DATA_MATRIX.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[Kl4.AZTEC.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
        }
        */
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        Ql4 fn4;
        switch (Ai.a[kl4.ordinal()]) {
            case 1:
                fn4 = new Fn4();
                break;
            case 2:
                fn4 = new On4();
                break;
            case 3:
                fn4 = new En4();
                break;
            case 4:
                fn4 = new Kn4();
                break;
            case 5:
                fn4 = new Xn4();
                break;
            case 6:
                fn4 = new An4();
                break;
            case 7:
                fn4 = new Cn4();
                break;
            case 8:
                fn4 = new Ym4();
                break;
            case 9:
                fn4 = new Hn4();
                break;
            case 10:
                fn4 = new Pn4();
                break;
            case 11:
                fn4 = new Wm4();
                break;
            case 12:
                fn4 = new Gm4();
                break;
            case 13:
                fn4 = new Sl4();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + kl4);
        }
        return fn4.a(str, kl4, i, i2, map);
    }
}
