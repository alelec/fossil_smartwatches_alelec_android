package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Qs4 {
    @DexIgnore
    List<Ms4> a();

    @DexIgnore
    int b(String str);

    @DexIgnore
    LiveData<Ps4> c(String[] strArr, Date date);

    @DexIgnore
    Ks4 d(String str);

    @DexIgnore
    Bt4 e(String str);

    @DexIgnore
    Ps4 f(String[] strArr, Date date);

    @DexIgnore
    Object g();  // void declaration

    @DexIgnore
    long h(Ls4 ls4);

    @DexIgnore
    LiveData<List<Bt4>> i();

    @DexIgnore
    Long[] insert(List<Ps4> list);

    @DexIgnore
    List<Ls4> j();

    @DexIgnore
    long k(Ps4 ps4);

    @DexIgnore
    LiveData<Bt4> l(String str);

    @DexIgnore
    Ps4 m(String str);

    @DexIgnore
    int n(String str);

    @DexIgnore
    Object o();  // void declaration

    @DexIgnore
    Object p();  // void declaration

    @DexIgnore
    Object q();  // void declaration

    @DexIgnore
    int r(Bt4 bt4);

    @DexIgnore
    LiveData<Ps4> s(String str);

    @DexIgnore
    Long[] t(List<Bt4> list);

    @DexIgnore
    void u(String[] strArr);

    @DexIgnore
    Long[] v(List<Ms4> list);

    @DexIgnore
    Object w();  // void declaration

    @DexIgnore
    Object x();  // void declaration

    @DexIgnore
    Long[] y(List<Ks4> list);
}
