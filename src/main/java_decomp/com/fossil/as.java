package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class As extends Ss {
    @DexIgnore
    public long A;
    @DexIgnore
    public F5 B; // = F5.b;
    @DexIgnore
    public /* final */ boolean C;

    @DexIgnore
    public As(K5 k5, boolean z, long j) {
        super(Hs.c, k5);
        this.C = z;
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.B0, Ey1.a(this.B));
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new Y5(this.C, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.A = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void g(U5 u5) {
        this.B = ((Y5) u5).l;
        this.g.add(new Hw(0, null, null, G80.k(new JSONObject(), Jd0.B0, Ey1.a(this.B)), 7));
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void i(P7 p7) {
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(super.z(), Jd0.o1, Ey1.a(this.y.H())), Jd0.f5, Integer.valueOf(this.y.A.getType())), Jd0.f1, Boolean.valueOf(this.C)), Jd0.s1, Ey1.a(this.y.w)), Jd0.k0, this.y.x);
    }
}
