package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import com.fossil.Cg0;
import com.fossil.Ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ch0 {
    @DexIgnore
    void a(Menu menu, Ig0.Ai ai);

    @DexIgnore
    boolean b();

    @DexIgnore
    Object c();  // void declaration

    @DexIgnore
    Object collapseActionView();  // void declaration

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean g();

    @DexIgnore
    Context getContext();

    @DexIgnore
    CharSequence getTitle();

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    void i(Mh0 mh0);

    @DexIgnore
    boolean j();

    @DexIgnore
    void k(int i);

    @DexIgnore
    Menu l();

    @DexIgnore
    void m(int i);

    @DexIgnore
    int n();

    @DexIgnore
    Ro0 o(int i, long j);

    @DexIgnore
    void p(Ig0.Ai ai, Cg0.Ai ai2);

    @DexIgnore
    ViewGroup q();

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    int s();

    @DexIgnore
    void setIcon(int i);

    @DexIgnore
    void setIcon(Drawable drawable);

    @DexIgnore
    void setTitle(CharSequence charSequence);

    @DexIgnore
    void setVisibility(int i);

    @DexIgnore
    void setWindowCallback(Window.Callback callback);

    @DexIgnore
    void setWindowTitle(CharSequence charSequence);

    @DexIgnore
    Object t();  // void declaration

    @DexIgnore
    Object u();  // void declaration

    @DexIgnore
    void v(boolean z);
}
