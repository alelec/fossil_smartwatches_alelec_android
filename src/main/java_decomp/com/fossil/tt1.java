package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ku1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Tt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Tt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Oq1.class.getClassLoader());
            if (readParcelable != null) {
                return new Tt1((Oq1) readParcelable, (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()), Ku1.values()[parcel.readInt()]);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Tt1[] newArray(int i) {
            return new Tt1[i];
        }
    }

    @DexIgnore
    public Tt1(Ku1 ku1) {
        this(null, null, ku1);
    }

    @DexIgnore
    public Tt1(Oq1 oq1, Ku1 ku1) {
        super(oq1, null);
        this.d = ku1;
    }

    @DexIgnore
    public Tt1(Oq1 oq1, Nt1 nt1, Ku1 ku1) {
        super(oq1, nt1);
        this.d = ku1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        return G80.k(super.a(), Jd0.n1, Ey1.a(this.d));
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Constants.RING_MY_PHONE, new JSONObject().put(Constants.RESULT, Ey1.a(this.d)));
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Tt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((Tt1) obj).d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingPhoneData");
    }

    @DexIgnore
    public final Ku1 getState() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        return (super.hashCode() * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }
}
