package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xa2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Ya2 b;

    @DexIgnore
    public Xa2(Ya2 ya2) {
        this.b = ya2;
    }

    @DexIgnore
    public final void run() {
        Ya2.n(this.b).lock();
        try {
            Ya2.w(this.b);
        } finally {
            Ya2.n(this.b).unlock();
        }
    }
}
