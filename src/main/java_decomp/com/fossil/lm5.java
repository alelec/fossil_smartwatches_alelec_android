package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lm5 {
    @DexIgnore
    public static /* final */ byte[] c; // = "from-data".getBytes();
    @DexIgnore
    public static /* final */ byte[] d; // = MessengerShareContentUtility.ATTACHMENT.getBytes();
    @DexIgnore
    public static /* final */ byte[] e; // = "inline".getBytes();
    @DexIgnore
    public Map<Integer, Object> a;
    @DexIgnore
    public byte[] b;

    @DexIgnore
    public Lm5() {
        this.a = null;
        this.b = null;
        this.a = new HashMap();
    }

    @DexIgnore
    public byte[] a() {
        return (byte[]) this.a.get(192);
    }

    @DexIgnore
    public byte[] b() {
        return (byte[]) this.a.get(142);
    }

    @DexIgnore
    public byte[] c() {
        return (byte[]) this.a.get(200);
    }

    @DexIgnore
    public byte[] d() {
        return (byte[]) this.a.get(145);
    }

    @DexIgnore
    public byte[] e() {
        return (byte[]) this.a.get(152);
    }

    @DexIgnore
    public byte[] f() {
        return (byte[]) this.a.get(151);
    }

    @DexIgnore
    public void g(int i) {
        this.a.put(129, Integer.valueOf(i));
    }

    @DexIgnore
    public void h(byte[] bArr) {
        if (bArr != null) {
            this.a.put(197, bArr);
            return;
        }
        throw new NullPointerException("null content-disposition");
    }

    @DexIgnore
    public void i(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("Content-Id may not be null or empty.");
        } else if (bArr.length > 1 && ((char) bArr[0]) == '<' && ((char) bArr[bArr.length - 1]) == '>') {
            this.a.put(192, bArr);
        } else {
            int length = bArr.length + 2;
            byte[] bArr2 = new byte[length];
            bArr2[0] = (byte) 60;
            bArr2[length - 1] = (byte) 62;
            System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
            this.a.put(192, bArr2);
        }
    }

    @DexIgnore
    public void j(byte[] bArr) {
        if (bArr != null) {
            this.a.put(142, bArr);
            return;
        }
        throw new NullPointerException("null content-location");
    }

    @DexIgnore
    public void k(byte[] bArr) {
        if (bArr != null) {
            this.a.put(200, bArr);
            return;
        }
        throw new NullPointerException("null content-transfer-encoding");
    }

    @DexIgnore
    public void l(byte[] bArr) {
        if (bArr != null) {
            this.a.put(145, bArr);
            return;
        }
        throw new NullPointerException("null content-type");
    }

    @DexIgnore
    public void m(byte[] bArr) {
        if (bArr != null) {
            byte[] bArr2 = new byte[bArr.length];
            this.b = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        }
    }

    @DexIgnore
    public void n(byte[] bArr) {
        if (bArr != null) {
            this.a.put(152, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }

    @DexIgnore
    public void o(byte[] bArr) {
        if (bArr != null) {
            this.a.put(151, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }
}
