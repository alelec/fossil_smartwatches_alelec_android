package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eb7 {
    @DexIgnore
    public /* final */ Ab7 a;
    @DexIgnore
    public /* final */ Db7 b;

    @DexIgnore
    public Eb7(Ab7 ab7, Db7 db7) {
        Wg6.c(ab7, Constants.EVENT);
        Wg6.c(db7, "selectedComplications");
        this.a = ab7;
        this.b = db7;
    }

    @DexIgnore
    public final Ab7 a() {
        return this.a;
    }

    @DexIgnore
    public final Db7 b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Eb7) {
                Eb7 eb7 = (Eb7) obj;
                if (!Wg6.a(this.a, eb7.a) || !Wg6.a(this.b, eb7.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Ab7 ab7 = this.a;
        int hashCode = ab7 != null ? ab7.hashCode() : 0;
        Db7 db7 = this.b;
        if (db7 != null) {
            i = db7.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "SelectedComplicationsEvent(event=" + this.a + ", selectedComplications=" + this.b + ")";
    }
}
