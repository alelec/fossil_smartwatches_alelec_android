package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k76 extends ts0 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public mo5 f1871a;
    @DexIgnore
    public List<oo5> b;
    @DexIgnore
    public List<DianaAppSetting> c; // = new ArrayList();
    @DexIgnore
    public List<DianaAppSetting> d; // = new ArrayList();
    @DexIgnore
    public MutableLiveData<List<oo5>> e; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<WatchApp> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson k; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> l;
    @DexIgnore
    public /* final */ LiveData<WatchApp> m;
    @DexIgnore
    public /* final */ LiveData<Boolean> n;
    @DexIgnore
    public /* final */ ls0<List<oo5>> o;
    @DexIgnore
    public /* final */ uo5 p;
    @DexIgnore
    public /* final */ DianaAppSettingRepository q;
    @DexIgnore
    public /* final */ WatchAppRepository r;
    @DexIgnore
    public /* final */ on5 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return k76.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ls0<List<? extends oo5>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k76 f1872a;

        @DexIgnore
        public b(k76 k76) {
            this.f1872a = k76;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: a */
        public final void onChanged(List<oo5> list) {
            T t;
            boolean z;
            if (list != null) {
                String str = (String) this.f1872a.h.e();
                String str2 = (String) this.f1872a.i.e();
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (!pq7.a(t2.b(), str) || vt7.j(t2.a(), str2, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    FLogger.INSTANCE.getLocal().d(k76.u.a(), "Update new watchapp id=" + t3.a() + " at position=" + str);
                    this.f1872a.i.l(t3.a());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$init$1", f = "WatchAppEditViewModel.kt", l = {123, 127}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ k76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$init$1$1", f = "WatchAppEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.e.i(this.this$0.this$0.o);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(k76 k76, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = k76;
            this.$presetId = str;
            this.$watchAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$watchAppPos, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r4) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0015:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r7)
            L_0x0027:
                com.fossil.k76 r2 = r6.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.k76.i(r2)
                java.lang.String r3 = r6.$watchAppPos
                r2.l(r3)
                com.fossil.jx7 r2 = com.fossil.bw7.c()
                com.fossil.k76$c$a r3 = new com.fossil.k76$c$a
                r4 = 0
                r3.<init>(r6, r4)
                r6.L$0 = r0
                r6.label = r5
                java.lang.Object r0 = com.fossil.eu7.g(r2, r3, r6)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0048:
                com.fossil.el7.b(r7)
                com.fossil.iv7 r0 = r6.p$
                com.fossil.k76 r2 = r6.this$0
                java.util.List r2 = com.fossil.k76.f(r2)
                if (r2 != 0) goto L_0x0027
                com.fossil.k76 r2 = r6.this$0
                java.lang.String r3 = r6.$presetId
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r2 = r2.B(r3, r6)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.k76.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$initFromSaveInstanceState$1", f = "WatchAppEditViewModel.kt", l = {135, 138, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 147}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ List $savedCurrentPresetButtons;
        @DexIgnore
        public /* final */ /* synthetic */ List $savedOriginalPresetButton;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ k76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$initFromSaveInstanceState$1$1", f = "WatchAppEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.e.i(this.this$0.this$0.o);
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(k76 k76, String str, List list, String str2, List list2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = k76;
            this.$presetId = str;
            this.$savedOriginalPresetButton = list;
            this.$watchAppPos = str2;
            this.$savedCurrentPresetButtons = list2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPresetButton, this.$watchAppPos, this.$savedCurrentPresetButtons, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0084  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00b2  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 4
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0094
                if (r0 == r5) goto L_0x006d
                if (r0 == r6) goto L_0x0065
                if (r0 == r7) goto L_0x0036
                if (r0 != r8) goto L_0x002e
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
            L_0x001b:
                com.fossil.k76 r0 = r9.this$0
                com.fossil.k76.a(r0)
                com.fossil.k76 r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.fossil.k76.a(r0)
                java.util.List r1 = r9.$savedCurrentPresetButtons
                r0.l(r1)
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002d:
                return r0
            L_0x002e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0036:
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
            L_0x003d:
                com.fossil.k76 r1 = r9.this$0
                java.util.List r2 = r9.$savedOriginalPresetButton
                com.fossil.k76.m(r1, r2)
            L_0x0044:
                com.fossil.k76 r1 = r9.this$0
                androidx.lifecycle.MutableLiveData r1 = com.fossil.k76.i(r1)
                java.lang.String r2 = r9.$watchAppPos
                r1.l(r2)
                com.fossil.jx7 r1 = com.fossil.bw7.c()
                com.fossil.k76$d$a r2 = new com.fossil.k76$d$a
                r3 = 0
                r2.<init>(r9, r3)
                r9.L$0 = r0
                r9.label = r8
                java.lang.Object r0 = com.fossil.eu7.g(r1, r2, r9)
                if (r0 != r4) goto L_0x001b
                r0 = r4
                goto L_0x002d
            L_0x0065:
                java.lang.Object r0 = r9.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r10)
                goto L_0x0044
            L_0x006d:
                java.lang.Object r0 = r9.L$1
                com.fossil.k76 r0 = (com.fossil.k76) r0
                java.lang.Object r1 = r9.L$0
                com.fossil.iv7 r1 = (com.fossil.iv7) r1
                com.fossil.el7.b(r10)
                r3 = r0
                r2 = r10
            L_0x007a:
                r0 = r2
                com.fossil.mo5 r0 = (com.fossil.mo5) r0
                com.fossil.k76.l(r3, r0)
                java.util.List r0 = r9.$savedOriginalPresetButton
                if (r0 != 0) goto L_0x00b2
                com.fossil.k76 r0 = r9.this$0
                java.lang.String r2 = r9.$presetId
                r9.L$0 = r1
                r9.label = r6
                java.lang.Object r0 = r0.B(r2, r9)
                if (r0 != r4) goto L_0x00b0
                r0 = r4
                goto L_0x002d
            L_0x0094:
                com.fossil.el7.b(r10)
                com.fossil.iv7 r1 = r9.p$
                com.fossil.k76 r0 = r9.this$0
                com.fossil.uo5 r2 = com.fossil.k76.c(r0)
                java.lang.String r3 = r9.$presetId
                r9.L$0 = r1
                r9.L$1 = r0
                r9.label = r5
                java.lang.Object r2 = r2.n(r3, r9)
                if (r2 != r4) goto L_0x00c4
                r0 = r4
                goto L_0x002d
            L_0x00b0:
                r0 = r1
                goto L_0x0044
            L_0x00b2:
                com.fossil.k76 r0 = r9.this$0
                r9.L$0 = r1
                r9.label = r7
                java.lang.Object r0 = r0.A(r9)
                if (r0 != r4) goto L_0x00c1
                r0 = r4
                goto L_0x002d
            L_0x00c1:
                r0 = r1
                goto L_0x003d
            L_0x00c4:
                r3 = r0
                goto L_0x007a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.k76.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel", f = "WatchAppEditViewModel.kt", l = {Action.Presenter.NEXT, 312, 314}, m = "initWatchAppAndSetting")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ k76 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(k76 k76, qn7 qn7) {
            super(qn7);
            this.this$0 = k76;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel", f = "WatchAppEditViewModel.kt", l = {290, 291}, m = "initializePreset")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ k76 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(k76 k76, qn7 qn7) {
            super(qn7);
            this.this$0 = k76;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.B(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k76 f1873a;

        @DexIgnore
        public g(k76 k76) {
            this.f1873a = k76;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<WatchApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = k76.u.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            k76 k76 = this.f1873a;
            pq7.b(str, "id");
            WatchApp o = k76.o(str);
            if (o == null) {
                o = ol5.c.a();
            }
            this.f1873a.j.l(o);
            return this.f1873a.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k76 f1874a;

        @DexIgnore
        public h(k76 k76) {
            this.f1874a = k76;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            Object obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = k76.u.a();
            local.d(a2, "transformWatchAppPosToId pos=" + str);
            List list = (List) this.f1874a.e.e();
            if (list != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    Object next = it.next();
                    if (pq7.a(((oo5) next).b(), str)) {
                        obj = next;
                        break;
                    }
                }
                oo5 oo5 = (oo5) obj;
                if (oo5 != null) {
                    this.f1874a.i.o(oo5.a());
                    return this.f1874a.i;
                }
            }
            this.f1874a.i.o("empty_watch_app_id");
            return this.f1874a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ k76 f1875a;

        @DexIgnore
        public i(k76 k76) {
            this.f1875a = k76;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(List<oo5> list) {
            Object obj;
            Object obj2;
            Object obj3;
            boolean z = false;
            pq7.b(list, "buttonList");
            boolean z2 = false;
            boolean z3 = false;
            for (T t : list) {
                List list2 = this.f1875a.b;
                if (list2 != null) {
                    Iterator it = list2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        Object next = it.next();
                        if (pq7.a(((oo5) next).b(), t.b())) {
                            obj = next;
                            break;
                        }
                    }
                    oo5 oo5 = (oo5) obj;
                    String a2 = oo5 != null ? oo5.a() : null;
                    if (a2 == null) {
                        pq7.i();
                        throw null;
                    } else if (!pq7.a(a2, t.a())) {
                        z3 = true;
                    } else {
                        Iterator it2 = this.f1875a.c.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            Object next2 = it2.next();
                            if (pq7.a(t.a(), ((DianaAppSetting) next2).getAppId())) {
                                obj2 = next2;
                                break;
                            }
                        }
                        DianaAppSetting dianaAppSetting = (DianaAppSetting) obj2;
                        Iterator it3 = this.f1875a.d.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                obj3 = null;
                                break;
                            }
                            Object next3 = it3.next();
                            if (pq7.a(t.a(), ((DianaAppSetting) next3).getAppId())) {
                                obj3 = next3;
                                break;
                            }
                        }
                        DianaAppSetting dianaAppSetting2 = (DianaAppSetting) obj3;
                        FLogger.INSTANCE.getLocal().d(k76.u.a(), "verify setting of " + t.a() + " originalSetting " + dianaAppSetting + " currentSetting " + dianaAppSetting2);
                        if (!pq7.a(dianaAppSetting, dianaAppSetting2)) {
                            z2 = true;
                        }
                    }
                } else {
                    pq7.i();
                    throw null;
                }
            }
            FLogger.INSTANCE.getLocal().d(k76.u.a(), "check preset isChanged isConfigChanged " + z3 + " isSettingChanged " + z2);
            MutableLiveData mutableLiveData = this.f1875a.f;
            if (z3 || z2) {
                z = true;
            }
            mutableLiveData.l(Boolean.valueOf(z));
            return this.f1875a.f;
        }
    }

    /*
    static {
        String simpleName = k76.class.getSimpleName();
        pq7.b(simpleName, "WatchAppEditViewModel::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public k76(uo5 uo5, DianaAppSettingRepository dianaAppSettingRepository, WatchAppRepository watchAppRepository, on5 on5) {
        pq7.c(uo5, "mDianaPresetRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        pq7.c(watchAppRepository, "mWatchAppRepository");
        pq7.c(on5, "sharedPreferencesManager");
        this.p = uo5;
        this.q = dianaAppSettingRepository;
        this.r = watchAppRepository;
        this.s = on5;
        LiveData<String> c2 = ss0.c(this.h, new h(this));
        pq7.b(c2, "Transformations.switchMa\u2026dWatchAppIdLiveData\n    }");
        this.l = c2;
        LiveData<WatchApp> c3 = ss0.c(c2, new g(this));
        pq7.b(c3, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.m = c3;
        LiveData<Boolean> c4 = ss0.c(this.e, new i(this));
        pq7.b(c4, "Transformations.switchMa\u2026resetButtonsChanged\n    }");
        this.n = c4;
        this.o = new b(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0137  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object A(com.fossil.qn7<? super com.fossil.tl7> r11) {
        /*
        // Method dump skipped, instructions count: 319
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k76.A(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object B(java.lang.String r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r8 = this;
            r7 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.fossil.k76.f
            if (r0 == 0) goto L_0x0046
            r0 = r10
            com.fossil.k76$f r0 = (com.fossil.k76.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r3 = r4.result
            java.lang.Object r6 = com.fossil.yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x0078
            if (r0 == r5) goto L_0x0055
            if (r0 != r7) goto L_0x004d
            java.lang.Object r0 = r4.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$0
            com.fossil.k76 r0 = (com.fossil.k76) r0
            com.fossil.el7.b(r3)
        L_0x002e:
            com.fossil.mo5 r1 = r0.f1871a
            if (r1 == 0) goto L_0x0043
            java.util.List r1 = r1.a()
            if (r1 == 0) goto L_0x0043
            r0.b = r1
            androidx.lifecycle.MutableLiveData<java.util.List<com.fossil.oo5>> r0 = r0.e
            java.util.ArrayList r1 = com.fossil.kj5.a(r1)
            r0.l(r1)
        L_0x0043:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0045:
            return r0
        L_0x0046:
            com.fossil.k76$f r0 = new com.fossil.k76$f
            r0.<init>(r8, r10)
            r4 = r0
            goto L_0x0015
        L_0x004d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            java.lang.Object r0 = r4.L$2
            com.fossil.k76 r0 = (com.fossil.k76) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.fossil.k76 r2 = (com.fossil.k76) r2
            com.fossil.el7.b(r3)
            r5 = r0
        L_0x0065:
            r0 = r3
            com.fossil.mo5 r0 = (com.fossil.mo5) r0
            r5.f1871a = r0
            r4.L$0 = r2
            r4.L$1 = r1
            r4.label = r7
            java.lang.Object r0 = r2.A(r4)
            if (r0 != r6) goto L_0x00ad
            r0 = r6
            goto L_0x0045
        L_0x0078:
            com.fossil.el7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.k76.t
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "initializePreset presetId="
            r2.append(r3)
            r2.append(r9)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.uo5 r0 = r8.p
            r4.L$0 = r8
            r4.L$1 = r9
            r4.L$2 = r8
            r4.label = r5
            java.lang.Object r3 = r0.n(r9, r4)
            if (r3 != r6) goto L_0x00a9
            r0 = r6
            goto L_0x0045
        L_0x00a9:
            r1 = r9
            r5 = r8
            r2 = r8
            goto L_0x0065
        L_0x00ad:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k76.B(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final boolean C() {
        Boolean e2 = this.f.e();
        if (e2 != null) {
            return e2.booleanValue();
        }
        return false;
    }

    @DexIgnore
    public final boolean D(String str) {
        T t2;
        T t3;
        pq7.c(str, "watchAppId");
        if (pq7.a(str, "empty")) {
            return false;
        }
        List<oo5> e2 = this.e.e();
        if (e2 != null) {
            Iterator<T> it = e2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.a(), str)) {
                    t3 = next;
                    break;
                }
            }
            t2 = t3;
        } else {
            t2 = null;
        }
        return t2 != null;
    }

    @DexIgnore
    public final Object E(qn7<? super tl7> qn7) {
        Object upsertDianaAppSetting = this.q.upsertDianaAppSetting(this.d, qn7);
        return upsertDianaAppSetting == yn7.d() ? upsertDianaAppSetting : tl7.f3441a;
    }

    @DexIgnore
    public final void F(String str) {
        pq7.c(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "setSelectedWatchApp watchAppPos=" + str);
        this.h.l(str);
    }

    @DexIgnore
    public final void G(List<oo5> list) {
        pq7.c(list, "presetButton");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "savePreset presetButton=" + list);
        this.e.l(kj5.a(list));
    }

    @DexIgnore
    public final void H(String str, Parcelable parcelable) {
        T t2;
        String str2 = null;
        pq7.c(str, "appId");
        pq7.c(parcelable, "settingParcelable");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getAppId(), str)) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        FLogger.INSTANCE.getLocal().d(t, "update setting of " + str + " setting " + parcelable + " currentAppSetting " + ((Object) t3));
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                str2 = jj5.a((WeatherWatchAppSetting) parcelable);
            }
        } else if (str.equals("commute-time")) {
            str2 = jj5.a((CommuteTimeWatchAppSetting) parcelable);
        }
        FLogger.INSTANCE.getLocal().d(t, "json setting of " + str + " setting " + str2);
        if (str2 != null) {
            if (t3 != null) {
                t3.setSetting(str2);
            } else {
                String e2 = ym5.e(24);
                pq7.b(e2, "StringHelper.randomUUID(24)");
                this.d.add(new DianaAppSetting(e2, str, "watch_app", str2));
            }
            MutableLiveData<List<oo5>> mutableLiveData = this.e;
            mutableLiveData.l(mutableLiveData.e());
        }
    }

    @DexIgnore
    public final List<WatchApp> n(String str) {
        pq7.c(str, "category");
        CopyOnWriteArrayList<WatchApp> copyOnWriteArrayList = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            if (t2.getCategories().contains(str)) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final WatchApp o(String str) {
        T t2;
        pq7.c(str, "watchAppId");
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (pq7.a(str, next.getWatchappId())) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.ts0
    public void onCleared() {
        this.e.m(this.o);
        super.onCleared();
    }

    @DexIgnore
    public final mo5 p() {
        return this.f1871a;
    }

    @DexIgnore
    public final MutableLiveData<List<oo5>> q() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.n;
    }

    @DexIgnore
    public final List<oo5> s() {
        return this.b;
    }

    @DexIgnore
    public final List<DianaAppSetting> t() {
        List<DianaAppSetting> list = this.d;
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            T t3 = t2;
            if (pq7.a(t3.getAppId(), "commute-time") || pq7.a(t3.getAppId(), "weather")) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final LiveData<WatchApp> u() {
        return this.m;
    }

    @DexIgnore
    public final MutableLiveData<String> v() {
        return this.h;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v15, resolved type: com.portfolio.platform.data.model.setting.WeatherWatchAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0095, code lost:
        if (r1 != null) goto L_0x0097;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object w(java.lang.String r9, com.fossil.qn7<? super android.os.Parcelable> r10) {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.k76.w(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final List<DianaAppSetting> x() {
        return this.d;
    }

    @DexIgnore
    public final xw7 y(String str, String str2) {
        pq7.c(str, "presetId");
        return gu7.d(jv7.a(bw7.a()), null, null, new c(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final xw7 z(String str, List<oo5> list, List<oo5> list2, String str2) {
        pq7.c(str, "presetId");
        return gu7.d(jv7.a(bw7.a()), null, null, new d(this, str, list, str2, list2, null), 3, null);
    }
}
