package com.fossil;

import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yw7 extends CancellationException implements Bv7<Yw7> {
    @DexIgnore
    public /* final */ Rm6 job;

    @DexIgnore
    public Yw7(String str, Throwable th, Rm6 rm6) {
        super(str);
        this.job = rm6;
        if (th != null) {
            initCause(th);
        }
    }

    @DexIgnore
    @Override // com.fossil.Bv7
    public Yw7 createCopy() {
        if (!Nv7.c()) {
            return null;
        }
        String message = getMessage();
        if (message != null) {
            return new Yw7(message, this, this.job);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof Yw7) {
                Yw7 yw7 = (Yw7) obj;
                if (!Wg6.a(yw7.getMessage(), getMessage()) || !Wg6.a(yw7.job, this.job) || !Wg6.a(yw7.getCause(), getCause())) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        return Nv7.c() ? super.fillInStackTrace() : this;
    }

    @DexIgnore
    public int hashCode() {
        String message = getMessage();
        if (message != null) {
            int hashCode = message.hashCode();
            int hashCode2 = this.job.hashCode();
            Throwable cause = getCause();
            return (cause != null ? cause.hashCode() : 0) + (((hashCode * 31) + hashCode2) * 31);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "; job=" + this.job;
    }
}
