package com.fossil;

import android.content.Context;
import com.mapped.An4;
import com.mapped.Kk4;
import com.mapped.U04;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jp4 implements Factory<Kk4> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<U04> c;
    @DexIgnore
    public /* final */ Provider<An4> d;

    @DexIgnore
    public Jp4(Uo4 uo4, Provider<Context> provider, Provider<U04> provider2, Provider<An4> provider3) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static Jp4 a(Uo4 uo4, Provider<Context> provider, Provider<U04> provider2, Provider<An4> provider3) {
        return new Jp4(uo4, provider, provider2, provider3);
    }

    @DexIgnore
    public static Kk4 c(Uo4 uo4, Context context, U04 u04, An4 an4) {
        Kk4 q = uo4.q(context, u04, an4);
        Lk7.c(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    public Kk4 b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
