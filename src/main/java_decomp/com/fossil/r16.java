package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R16 extends Ts0 {
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ai(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(this.a == ai.a && this.b == ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return i2 + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "NotificationSetting(type=" + this.a + ", isCall=" + this.b + ")";
        }
    }

    @DexIgnore
    public final MutableLiveData<Ai> a() {
        return this.a;
    }
}
