package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.tabs.TabLayout;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N04 {
    @DexIgnore
    public /* final */ TabLayout a;
    @DexIgnore
    public /* final */ ViewPager2 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ Bi d;
    @DexIgnore
    public RecyclerView.g<?> e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Ci g;
    @DexIgnore
    public TabLayout.d h;
    @DexIgnore
    public RecyclerView.AdapterDataObserver i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            N04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void b(int i, int i2) {
            N04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void c(int i, int i2, Object obj) {
            N04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void d(int i, int i2) {
            N04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void e(int i, int i2, int i3) {
            N04.this.b();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void f(int i, int i2) {
            N04.this.b();
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(TabLayout.g gVar, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ WeakReference<TabLayout> a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public Ci(TabLayout tabLayout) {
            this.a = new WeakReference<>(tabLayout);
            d();
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i) {
            this.b = this.c;
            this.c = i;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            boolean z = false;
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null) {
                boolean z2 = this.c != 2 || this.b == 1;
                if (!(this.c == 2 && this.b == 0)) {
                    z = true;
                }
                tabLayout.G(i, f, z2, z);
            }
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            TabLayout tabLayout = this.a.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i && i < tabLayout.getTabCount()) {
                int i2 = this.c;
                tabLayout.D(tabLayout.v(i), i2 == 0 || (i2 == 2 && this.b == 0));
            }
        }

        @DexIgnore
        public void d() {
            this.c = 0;
            this.b = 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements TabLayout.d {
        @DexIgnore
        public /* final */ ViewPager2 a;

        @DexIgnore
        public Di(ViewPager2 viewPager2) {
            this.a = viewPager2;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            this.a.j(gVar.f(), true);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore
    public N04(TabLayout tabLayout, ViewPager2 viewPager2, Bi bi) {
        this(tabLayout, viewPager2, true, bi);
    }

    @DexIgnore
    public N04(TabLayout tabLayout, ViewPager2 viewPager2, boolean z, Bi bi) {
        this.a = tabLayout;
        this.b = viewPager2;
        this.c = z;
        this.d = bi;
    }

    @DexIgnore
    public void a() {
        if (!this.f) {
            RecyclerView.g<?> adapter = this.b.getAdapter();
            this.e = adapter;
            if (adapter != null) {
                this.f = true;
                Ci ci = new Ci(this.a);
                this.g = ci;
                this.b.g(ci);
                Di di = new Di(this.b);
                this.h = di;
                this.a.c(di);
                if (this.c) {
                    Ai ai = new Ai();
                    this.i = ai;
                    this.e.registerAdapterDataObserver(ai);
                }
                b();
                this.a.F(this.b.getCurrentItem(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, true);
                return;
            }
            throw new IllegalStateException("TabLayoutMediator attached before ViewPager2 has an adapter");
        }
        throw new IllegalStateException("TabLayoutMediator is already attached");
    }

    @DexIgnore
    public void b() {
        this.a.z();
        RecyclerView.g<?> gVar = this.e;
        if (gVar != null) {
            int itemCount = gVar.getItemCount();
            for (int i2 = 0; i2 < itemCount; i2++) {
                TabLayout.g w = this.a.w();
                this.d.a(w, i2);
                this.a.f(w, false);
            }
            if (itemCount > 0) {
                int min = Math.min(this.b.getCurrentItem(), this.a.getTabCount() - 1);
                if (min != this.a.getSelectedTabPosition()) {
                    TabLayout tabLayout = this.a;
                    tabLayout.C(tabLayout.v(min));
                }
            }
        }
    }
}
