package com.fossil;

import com.mapped.Af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C08 {
    @DexIgnore
    public Object[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Af6 c;

    @DexIgnore
    public C08(Af6 af6, int i) {
        this.c = af6;
        this.a = new Object[i];
    }

    @DexIgnore
    public final void a(Object obj) {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        objArr[i] = obj;
    }

    @DexIgnore
    public final Af6 b() {
        return this.c;
    }

    @DexIgnore
    public final void c() {
        this.b = 0;
    }

    @DexIgnore
    public final Object d() {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        return objArr[i];
    }
}
