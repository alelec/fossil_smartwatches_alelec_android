package com.fossil;

import com.mapped.Wg6;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ay1 {
    @DexIgnore
    public static final JSONArray a(int[] iArr) {
        Wg6.c(iArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (int i : iArr) {
            jSONArray.put(i);
        }
        return jSONArray;
    }

    @DexIgnore
    public static final JSONArray b(String[] strArr) {
        Wg6.c(strArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (String str : strArr) {
            jSONArray.put(str);
        }
        return jSONArray;
    }
}
