package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ry5 {
    @DexIgnore
    public static /* final */ Rect a; // = new Rect();
    @DexIgnore
    public static /* final */ RectF b; // = new RectF();
    @DexIgnore
    public static /* final */ RectF c; // = new RectF();
    @DexIgnore
    public static /* final */ float[] d; // = new float[6];
    @DexIgnore
    public static /* final */ float[] e; // = new float[6];
    @DexIgnore
    public static int f;
    @DexIgnore
    public static Pair<String, WeakReference<Bitmap>> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ai(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Bi(Bitmap bitmap, int i) {
            this.a = bitmap;
            this.b = i;
        }
    }

    @DexIgnore
    public static float A(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }

    @DexIgnore
    public static float B(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static float C(float[] fArr) {
        return A(fArr) - z(fArr);
    }

    @DexIgnore
    public static Bitmap D(Bitmap bitmap, int i, int i2, CropImageView.j jVar) {
        if (i <= 0 || i2 <= 0) {
            return bitmap;
        }
        try {
            if (jVar != CropImageView.j.RESIZE_FIT && jVar != CropImageView.j.RESIZE_INSIDE && jVar != CropImageView.j.RESIZE_EXACT) {
                return bitmap;
            }
            Bitmap bitmap2 = null;
            if (jVar == CropImageView.j.RESIZE_EXACT) {
                bitmap2 = Bitmap.createScaledBitmap(bitmap, i, i2, false);
            } else {
                float width = (float) bitmap.getWidth();
                float height = (float) bitmap.getHeight();
                float max = Math.max(width / ((float) i), height / ((float) i2));
                if (max > 1.0f || jVar == CropImageView.j.RESIZE_FIT) {
                    bitmap2 = Bitmap.createScaledBitmap(bitmap, (int) (width / max), (int) (height / max), false);
                }
            }
            if (bitmap2 == null) {
                return bitmap;
            }
            if (bitmap2 != bitmap) {
                bitmap.recycle();
            }
            return bitmap2;
        } catch (Exception e2) {
            Log.w("AIC", "Failed to resize cropped image, return bitmap before resize", e2);
            return bitmap;
        }
    }

    @DexIgnore
    public static Bitmap E(Bitmap bitmap, int i, boolean z, boolean z2) {
        float f2 = -1.0f;
        if (i <= 0 && !z && !z2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i);
        float f3 = z ? -1.0f : 1.0f;
        if (!z2) {
            f2 = 1.0f;
        }
        matrix.postScale(f3, f2);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        if (createBitmap != bitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bi F(Bitmap bitmap, Context context, Uri uri) {
        Exception e2;
        int i = 0;
        try {
            if (!(uri.getPath() == null || uri.getScheme() == null)) {
                if (uri.getScheme().equals("file")) {
                    i = new Eq0(uri.getPath()).q();
                } else {
                    Cursor query = context.getContentResolver().query(uri, new String[]{"orientation"}, null, null, null);
                    if (query != null) {
                        int i2 = query.moveToFirst() ? query.getInt(0) : 0;
                        try {
                            query.close();
                            i = i2;
                        } catch (Exception e3) {
                            e2 = e3;
                            i = i2;
                            e2.printStackTrace();
                            return new Bi(bitmap, i);
                        }
                    }
                }
            }
        } catch (Exception e4) {
            e2 = e4;
            e2.printStackTrace();
            return new Bi(bitmap, i);
        }
        return new Bi(bitmap, i);
    }

    @DexIgnore
    public static Bi G(Bitmap bitmap, Eq0 eq0) {
        int g2 = eq0.g("Orientation", 1);
        return new Bi(bitmap, g2 != 3 ? g2 != 6 ? g2 != 8 ? 0 : 270 : 90 : 180);
    }

    @DexIgnore
    public static void H(Context context, String str, String str2, Bitmap bitmap) {
        try {
            I(context, bitmap, Uri.fromFile(new File(str.concat(str2))), Bitmap.CompressFormat.PNG, 100);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public static void I(Context context, Bitmap bitmap, Uri uri, Bitmap.CompressFormat compressFormat, int i) throws FileNotFoundException {
        OutputStream outputStream = null;
        try {
            outputStream = context.getContentResolver().openOutputStream(uri);
            bitmap.compress(compressFormat, i, outputStream);
        } finally {
            d(outputStream);
        }
    }

    @DexIgnore
    public static Uri J(Context context, Bitmap bitmap, Uri uri) {
        Uri uri2;
        boolean z = true;
        if (uri == null) {
            try {
                uri2 = Uri.fromFile(File.createTempFile("aic_state_store_temp", ".jpg", context.getCacheDir()));
            } catch (Exception e2) {
                Log.w("AIC", "Failed to write bitmap to temp file for image-cropper save instance state", e2);
                return null;
            }
        } else if (new File(uri.getPath()).exists()) {
            z = false;
            uri2 = uri;
        } else {
            uri2 = uri;
        }
        if (!z) {
            return uri2;
        }
        I(context, bitmap, uri2, Bitmap.CompressFormat.JPEG, 95);
        return uri2;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i, int i2) {
        Exception e2;
        try {
            Bitmap.createScaledBitmap(bitmap, i, i2, false);
            try {
                return s(bitmap, false);
            } catch (Exception e3) {
                e2 = e3;
            }
        } catch (Exception e4) {
            e2 = e4;
            e2.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static int b(int i, int i2) {
        int i3 = 1;
        if (f == 0) {
            f = t();
        }
        if (f > 0) {
            while (true) {
                int i4 = i2 / i3;
                int i5 = f;
                if (i4 <= i5 && i / i3 <= i5) {
                    break;
                }
                i3 *= 2;
            }
        }
        return i3;
    }

    @DexIgnore
    public static int c(int i, int i2, int i3, int i4) {
        int i5 = 1;
        if (i2 > i4 || i > i3) {
            while ((i2 / 2) / i5 > i4 && (i / 2) / i5 > i3) {
                i5 *= 2;
            }
        }
        return i5;
    }

    @DexIgnore
    public static void d(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e2) {
            }
        }
    }

    @DexIgnore
    public static Ai e(Context context, Uri uri, float[] fArr, int i, int i2, int i3, boolean z, int i4, int i5, int i6, int i7, boolean z2, boolean z3) {
        int i8 = 1;
        do {
            try {
                return f(context, uri, fArr, i, i2, i3, z, i4, i5, i6, i7, z2, z3, i8);
            } catch (OutOfMemoryError e2) {
                i8 *= 2;
                if (i8 > 16) {
                    throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + e2.getMessage(), e2);
                }
            }
        } while (i8 > 16);
        throw new RuntimeException("Failed to handle OOM by sampling (" + i8 + "): " + uri + "\r\n" + e2.getMessage(), e2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Ry5.Ai f(android.content.Context r21, android.net.Uri r22, float[] r23, int r24, int r25, int r26, boolean r27, int r28, int r29, int r30, int r31, boolean r32, boolean r33, int r34) {
        /*
            r3 = r23
            r4 = r25
            r5 = r26
            r6 = r27
            r7 = r28
            r8 = r29
            android.graphics.Rect r5 = x(r3, r4, r5, r6, r7, r8)
            if (r30 <= 0) goto L_0x004e
            r6 = r30
        L_0x0014:
            if (r31 <= 0) goto L_0x0053
            r7 = r31
        L_0x0018:
            r9 = 0
            r10 = 1
            r3 = r21
            r4 = r22
            r8 = r34
            com.fossil.Ry5$Ai r4 = p(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0080 }
            android.graphics.Bitmap r3 = r4.a     // Catch:{ Exception -> 0x0080 }
            int r4 = r4.b     // Catch:{ Exception -> 0x0083 }
            r10 = r4
        L_0x0029:
            if (r3 == 0) goto L_0x005f
            r0 = r24
            r1 = r32
            r2 = r33
            android.graphics.Bitmap r3 = E(r3, r0, r1, r2)     // Catch:{ OutOfMemoryError -> 0x0058 }
            int r4 = r24 % 90
            if (r4 == 0) goto L_0x0047
            r4 = r23
            r6 = r24
            r7 = r27
            r8 = r28
            r9 = r29
            android.graphics.Bitmap r3 = k(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ OutOfMemoryError -> 0x007e }
        L_0x0047:
            com.fossil.Ry5$Ai r4 = new com.fossil.Ry5$Ai
            r4.<init>(r3, r10)
            r3 = r4
        L_0x004d:
            return r3
        L_0x004e:
            int r6 = r5.width()
            goto L_0x0014
        L_0x0053:
            int r7 = r5.height()
            goto L_0x0018
        L_0x0058:
            r4 = move-exception
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            r3.recycle()
        L_0x005e:
            throw r4
        L_0x005f:
            r8 = r21
            r9 = r22
            r10 = r23
            r11 = r24
            r12 = r27
            r13 = r28
            r14 = r29
            r15 = r34
            r16 = r5
            r17 = r6
            r18 = r7
            r19 = r32
            r20 = r33
            com.fossil.Ry5$Ai r3 = g(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x004d
        L_0x007e:
            r4 = move-exception
            goto L_0x0059
        L_0x0080:
            r3 = move-exception
            r3 = r9
            goto L_0x0029
        L_0x0083:
            r4 = move-exception
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ry5.f(android.content.Context, android.net.Uri, float[], int, int, int, boolean, int, int, int, int, boolean, boolean, int):com.fossil.Ry5$Ai");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0065, code lost:
        r3.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0069, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008c, code lost:
        throw new java.lang.RuntimeException("Failed to load sampled bitmap: " + r14 + "\r\n" + r2.getMessage(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008d, code lost:
        r2 = e;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0069 A[ExcHandler: Exception (r2v0 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Ry5.Ai g(android.content.Context r13, android.net.Uri r14, float[] r15, int r16, boolean r17, int r18, int r19, int r20, android.graphics.Rect r21, int r22, int r23, boolean r24, boolean r25) {
        /*
            r11 = 0
            r3 = 0
            android.graphics.BitmapFactory$Options r5 = new android.graphics.BitmapFactory$Options     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            r5.<init>()     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            int r2 = r21.width()     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            int r4 = r21.height()     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            r0 = r22
            r1 = r23
            int r2 = c(r2, r4, r0, r1)     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            int r12 = r2 * r20
            r5.inSampleSize = r12     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            android.content.ContentResolver r2 = r13.getContentResolver()     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            android.graphics.Bitmap r2 = l(r2, r14, r5)     // Catch:{ OutOfMemoryError -> 0x0061, Exception -> 0x0069 }
            if (r2 == 0) goto L_0x0091
            int r6 = r15.length     // Catch:{ all -> 0x005a }
            float[] r3 = new float[r6]     // Catch:{ all -> 0x005a }
            int r7 = r15.length     // Catch:{ all -> 0x005a }
            r4 = 0
            r8 = 0
            r9 = 0
            java.lang.System.arraycopy(r15, r8, r3, r9, r7)     // Catch:{ all -> 0x005a }
        L_0x002f:
            if (r4 >= r6) goto L_0x003c
            r7 = r3[r4]     // Catch:{ all -> 0x005a }
            int r8 = r5.inSampleSize     // Catch:{ all -> 0x005a }
            float r8 = (float) r8     // Catch:{ all -> 0x005a }
            float r7 = r7 / r8
            r3[r4] = r7     // Catch:{ all -> 0x005a }
            int r4 = r4 + 1
            goto L_0x002f
        L_0x003c:
            r8 = 1065353216(0x3f800000, float:1.0)
            r4 = r16
            r5 = r17
            r6 = r18
            r7 = r19
            r9 = r24
            r10 = r25
            android.graphics.Bitmap r3 = i(r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x005a }
            if (r3 == r2) goto L_0x008f
            r2.recycle()     // Catch:{ OutOfMemoryError -> 0x008d, Exception -> 0x0069 }
            r2 = r3
        L_0x0054:
            com.fossil.Ry5$Ai r3 = new com.fossil.Ry5$Ai
            r3.<init>(r2, r12)
            return r3
        L_0x005a:
            r3 = move-exception
            if (r2 == 0) goto L_0x0060
            r2.recycle()
        L_0x0060:
            throw r3
        L_0x0061:
            r2 = move-exception
            r3 = r11
        L_0x0063:
            if (r3 == 0) goto L_0x0068
            r3.recycle()
        L_0x0068:
            throw r2
        L_0x0069:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Failed to load sampled bitmap: "
            r3.append(r4)
            r3.append(r14)
            java.lang.String r4 = "\r\n"
            r3.append(r4)
            java.lang.String r4 = r2.getMessage()
            r3.append(r4)
            java.lang.RuntimeException r4 = new java.lang.RuntimeException
            java.lang.String r3 = r3.toString()
            r4.<init>(r3, r2)
            throw r4
        L_0x008d:
            r2 = move-exception
            goto L_0x0063
        L_0x008f:
            r2 = r3
            goto L_0x0054
        L_0x0091:
            r2 = r3
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ry5.g(android.content.Context, android.net.Uri, float[], int, boolean, int, int, int, android.graphics.Rect, int, int, boolean, boolean):com.fossil.Ry5$Ai");
    }

    @DexIgnore
    public static Ai h(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
        int i4 = 1;
        while (true) {
            try {
                return new Ai(i(bitmap, fArr, i, z, i2, i3, 1.0f / ((float) i4), z2, z3), i4);
            } catch (OutOfMemoryError e2) {
                i4 *= 2;
                if (i4 > 8) {
                    throw e2;
                }
            }
        }
    }

    @DexIgnore
    public static Bitmap i(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, float f2, boolean z2, boolean z3) {
        Rect x = x(fArr, bitmap.getWidth(), bitmap.getHeight(), z, i2, i3);
        Matrix matrix = new Matrix();
        matrix.setRotate((float) i, (float) (bitmap.getWidth() / 2), (float) (bitmap.getHeight() / 2));
        float f3 = z2 ? -f2 : f2;
        if (z3) {
            f2 = -f2;
        }
        matrix.postScale(f3, f2);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, x.left, x.top, x.width(), x.height(), matrix, true);
        if (createBitmap == bitmap) {
            createBitmap = bitmap.copy(bitmap.getConfig(), false);
        }
        return i % 90 != 0 ? k(createBitmap, fArr, x, i, z, i2, i3) : createBitmap;
    }

    @DexIgnore
    public static Bitmap j(Bitmap bitmap) {
        return bitmap.getWidth() >= bitmap.getHeight() ? Bitmap.createBitmap(bitmap, (bitmap.getWidth() / 2) - (bitmap.getHeight() / 2), 0, bitmap.getHeight(), bitmap.getHeight()) : Bitmap.createBitmap(bitmap, 0, (bitmap.getHeight() / 2) - (bitmap.getWidth() / 2), bitmap.getWidth(), bitmap.getWidth());
    }

    @DexIgnore
    public static Bitmap k(Bitmap bitmap, float[] fArr, Rect rect, int i, boolean z, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        if (i % 90 == 0) {
            return bitmap;
        }
        double radians = Math.toRadians((double) i);
        int i7 = (i < 90 || (i > 180 && i < 270)) ? rect.left : rect.right;
        int i8 = 0;
        int i9 = 0;
        while (true) {
            if (i9 < fArr.length) {
                if (fArr[i9] >= ((float) (i7 - 1)) && fArr[i9] <= ((float) (i7 + 1))) {
                    int i10 = i9 + 1;
                    i8 = (int) Math.abs(Math.sin(radians) * ((double) (((float) rect.bottom) - fArr[i10])));
                    i5 = (int) Math.abs(Math.cos(radians) * ((double) (fArr[i10] - ((float) rect.top))));
                    i6 = (int) Math.abs(((double) (fArr[i10] - ((float) rect.top))) / Math.sin(radians));
                    i4 = (int) Math.abs(((double) (((float) rect.bottom) - fArr[i10])) / Math.cos(radians));
                    break;
                }
                i9 += 2;
            } else {
                i4 = 0;
                i5 = 0;
                i6 = 0;
                break;
            }
        }
        rect.set(i8, i5, i6 + i8, i4 + i5);
        if (z) {
            r(rect, i2, i3);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top, rect.width(), rect.height());
        if (bitmap != createBitmap) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap l(ContentResolver contentResolver, Uri uri, BitmapFactory.Options options) throws FileNotFoundException {
        InputStream inputStream;
        Throwable th;
        if (TextUtils.equals(uri.getLastPathSegment(), "pickerImage.jpg")) {
            return BitmapFactory.decodeFile(uri.getPath(), options);
        }
        do {
            try {
                inputStream = contentResolver.openInputStream(uri);
                try {
                    Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, a, options);
                    d(inputStream);
                    return decodeStream;
                } catch (OutOfMemoryError e2) {
                }
            } catch (OutOfMemoryError e3) {
                inputStream = null;
                try {
                    options.inSampleSize *= 2;
                    d(inputStream);
                    if (options.inSampleSize > 512) {
                        throw new RuntimeException("Failed to decode image: " + uri);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    d(inputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                inputStream = null;
                d(inputStream);
                throw th;
            }
        } while (options.inSampleSize > 512);
        throw new RuntimeException("Failed to decode image: " + uri);
    }

    @DexIgnore
    public static BitmapFactory.Options m(ContentResolver contentResolver, Uri uri) throws FileNotFoundException {
        InputStream inputStream;
        Throwable th;
        try {
            inputStream = contentResolver.openInputStream(uri);
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, a, options);
                options.inJustDecodeBounds = false;
                d(inputStream);
                return options;
            } catch (Throwable th2) {
                th = th2;
                d(inputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            d(inputStream);
            throw th;
        }
    }

    @DexIgnore
    public static BitmapFactory.Options n(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri.getPath(), options);
        options.inJustDecodeBounds = false;
        return options;
    }

    @DexIgnore
    public static Ai o(Context context, Uri uri, int i, int i2) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            BitmapFactory.Options n = TextUtils.equals(uri.getLastPathSegment(), "pickerImage.jpg") ? n(uri) : m(contentResolver, uri);
            if (n.outWidth == -1 && n.outHeight == -1) {
                throw new RuntimeException("File is not a picture");
            }
            n.inSampleSize = Math.max(c(n.outWidth, n.outHeight, i, i2), b(n.outWidth, n.outHeight));
            return new Ai(l(contentResolver, uri, n), n.inSampleSize);
        } catch (Exception e2) {
            throw new RuntimeException("Failed to load sampled bitmap: " + uri + "\r\n" + e2.getMessage(), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0057  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Ry5.Ai p(android.content.Context r7, android.net.Uri r8, android.graphics.Rect r9, int r10, int r11, int r12) {
        /*
            r2 = 0
            android.graphics.BitmapFactory$Options r4 = new android.graphics.BitmapFactory$Options     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            r4.<init>()     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            int r0 = r9.width()     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            int r1 = r9.height()     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            int r0 = c(r0, r1, r10, r11)     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            int r0 = r0 * r12
            r4.inSampleSize = r0     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            android.content.ContentResolver r0 = r7.getContentResolver()     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            java.io.InputStream r3 = r0.openInputStream(r8)     // Catch:{ Exception -> 0x0086, all -> 0x0082 }
            r0 = 0
            android.graphics.BitmapRegionDecoder r1 = android.graphics.BitmapRegionDecoder.newInstance(r3, r0)     // Catch:{ Exception -> 0x005b, all -> 0x0050 }
        L_0x0022:
            com.fossil.Ry5$Ai r0 = new com.fossil.Ry5$Ai     // Catch:{ OutOfMemoryError -> 0x0036 }
            android.graphics.Bitmap r5 = r1.decodeRegion(r9, r4)     // Catch:{ OutOfMemoryError -> 0x0036 }
            int r6 = r4.inSampleSize     // Catch:{ OutOfMemoryError -> 0x0036 }
            r0.<init>(r5, r6)     // Catch:{ OutOfMemoryError -> 0x0036 }
            d(r3)
            if (r1 == 0) goto L_0x0035
            r1.recycle()
        L_0x0035:
            return r0
        L_0x0036:
            r0 = move-exception
            int r0 = r4.inSampleSize     // Catch:{ Exception -> 0x008a, all -> 0x008c }
            int r0 = r0 * 2
            r4.inSampleSize = r0     // Catch:{ Exception -> 0x008a, all -> 0x008c }
            r5 = 512(0x200, float:7.175E-43)
            if (r0 <= r5) goto L_0x0022
            d(r3)
            if (r1 == 0) goto L_0x0049
            r1.recycle()
        L_0x0049:
            com.fossil.Ry5$Ai r0 = new com.fossil.Ry5$Ai
            r1 = 1
            r0.<init>(r2, r1)
            goto L_0x0035
        L_0x0050:
            r0 = move-exception
            r1 = r2
        L_0x0052:
            d(r3)
            if (r1 == 0) goto L_0x005a
            r1.recycle()
        L_0x005a:
            throw r0
        L_0x005b:
            r0 = move-exception
            r1 = r2
        L_0x005d:
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r4.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r5 = "Failed to load sampled bitmap: "
            r4.append(r5)     // Catch:{ all -> 0x0080 }
            r4.append(r8)     // Catch:{ all -> 0x0080 }
            java.lang.String r5 = "\r\n"
            r4.append(r5)     // Catch:{ all -> 0x0080 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0080 }
            r4.append(r5)     // Catch:{ all -> 0x0080 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0080 }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x0080 }
            throw r2     // Catch:{ all -> 0x0080 }
        L_0x0080:
            r0 = move-exception
            goto L_0x0052
        L_0x0082:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0052
        L_0x0086:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x005d
        L_0x008a:
            r0 = move-exception
            goto L_0x005d
        L_0x008c:
            r0 = move-exception
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ry5.p(android.content.Context, android.net.Uri, android.graphics.Rect, int, int, int):com.fossil.Ry5$Ai");
    }

    @DexIgnore
    public static Bitmap q(String str, int i, boolean z) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        int max = Math.max(1, Math.min(options.outWidth / i, options.outHeight / i));
        options.inJustDecodeBounds = false;
        options.inSampleSize = max;
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        Bi G = G(decodeFile, new Eq0(str));
        Matrix matrix = new Matrix();
        matrix.postRotate((float) G.b);
        Bitmap bitmap = G.a;
        Bitmap j = j(Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), G.a.getHeight(), matrix, true));
        G.a.recycle();
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(j, i, i, false);
        j.recycle();
        decodeFile.recycle();
        Bitmap s = s(createScaledBitmap, z);
        createScaledBitmap.recycle();
        return s;
    }

    @DexIgnore
    public static void r(Rect rect, int i, int i2) {
        if (i == i2 && rect.width() != rect.height()) {
            if (rect.height() > rect.width()) {
                rect.bottom -= rect.height() - rect.width();
            } else {
                rect.right -= rect.width() - rect.height();
            }
        }
    }

    @DexIgnore
    public static Bitmap s(Bitmap bitmap, boolean z) {
        if (bitmap == null) {
            return null;
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        Canvas canvas = new Canvas(createBitmap);
        int width = bitmap.getWidth() / 2;
        int height = bitmap.getHeight() / 2;
        Paint paint = new Paint();
        if (z) {
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            paint.setShader(new BitmapShader(bitmap, tileMode, tileMode));
        }
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setARGB(255, 0, 0, 0);
        canvas.drawCircle((float) width, (float) height, (float) Math.min(width, height), paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    @DexIgnore
    public static int t() {
        try {
            EGL10 egl10 = (EGL10) EGLContext.getEGL();
            EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            egl10.eglInitialize(eglGetDisplay, new int[2]);
            int[] iArr = new int[1];
            egl10.eglGetConfigs(eglGetDisplay, null, 0, iArr);
            EGLConfig[] eGLConfigArr = new EGLConfig[iArr[0]];
            egl10.eglGetConfigs(eglGetDisplay, eGLConfigArr, iArr[0], iArr);
            int[] iArr2 = new int[1];
            int i = 0;
            for (int i2 = 0; i2 < iArr[0]; i2++) {
                egl10.eglGetConfigAttrib(eglGetDisplay, eGLConfigArr[i2], 12332, iArr2);
                if (i < iArr2[0]) {
                    i = iArr2[0];
                }
            }
            egl10.eglTerminate(eglGetDisplay);
            return Math.max(i, 2048);
        } catch (Exception e2) {
            return 2048;
        }
    }

    @DexIgnore
    public static float u(float[] fArr) {
        return Math.max(Math.max(Math.max(fArr[1], fArr[3]), fArr[5]), fArr[7]);
    }

    @DexIgnore
    public static float v(float[] fArr) {
        return (A(fArr) + z(fArr)) / 2.0f;
    }

    @DexIgnore
    public static float w(float[] fArr) {
        return (u(fArr) + B(fArr)) / 2.0f;
    }

    @DexIgnore
    public static Rect x(float[] fArr, int i, int i2, boolean z, int i3, int i4) {
        Rect rect = new Rect(Math.round(Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, z(fArr))), Math.round(Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, B(fArr))), Math.round(Math.min((float) i, A(fArr))), Math.round(Math.min((float) i2, u(fArr))));
        if (z) {
            r(rect, i3, i4);
        }
        return rect;
    }

    @DexIgnore
    public static float y(float[] fArr) {
        return u(fArr) - B(fArr);
    }

    @DexIgnore
    public static float z(float[] fArr) {
        return Math.min(Math.min(Math.min(fArr[0], fArr[2]), fArr[4]), fArr[6]);
    }
}
