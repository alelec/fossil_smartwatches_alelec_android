package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class al7 extends RuntimeException {
    @DexIgnore
    public al7() {
    }

    @DexIgnore
    public al7(String str) {
        super(str);
    }

    @DexIgnore
    public al7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public al7(Throwable th) {
        super(th);
    }
}
