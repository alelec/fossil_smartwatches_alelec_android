package com.fossil;

import com.google.maps.model.TravelMode;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum CAR can be incorrect */
public enum Mh5 {
    CAR(r0, r1);
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public /* final */ TravelMode travelMode;
    @DexIgnore
    public /* final */ String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final TravelMode a(String str) {
            Mh5 mh5;
            TravelMode travelMode;
            Wg6.c(str, "type");
            Mh5[] values = Mh5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    mh5 = null;
                    break;
                }
                mh5 = values[i];
                if (Wg6.a(mh5.getType(), str)) {
                    break;
                }
                i++;
            }
            return (mh5 == null || (travelMode = mh5.getTravelMode()) == null) ? Mh5.CAR.getTravelMode() : travelMode;
        }
    }

    /*
    static {
        TravelMode travelMode2 = TravelMode.DRIVING;
        Wg6.b(Um5.c(PortfolioApp.get.instance(), 2131887318), "LanguageHelper.getString\u2026nstance, R.string.by_car)");
    }
    */

    @DexIgnore
    public Mh5(TravelMode travelMode2, String str) {
        this.travelMode = travelMode2;
        this.type = str;
    }

    @DexIgnore
    public static final TravelMode getTravelModeFromType(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public final TravelMode getTravelMode() {
        return this.travelMode;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }
}
