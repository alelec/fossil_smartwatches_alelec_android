package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import com.mapped.W6;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cg0 implements Gm0 {
    @DexIgnore
    public static /* final */ int[] A; // = {1, 4, 5, 3, 2, 0};
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Resources b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Ai e;
    @DexIgnore
    public ArrayList<Eg0> f;
    @DexIgnore
    public ArrayList<Eg0> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public ArrayList<Eg0> i;
    @DexIgnore
    public ArrayList<Eg0> j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public ContextMenu.ContextMenuInfo m;
    @DexIgnore
    public CharSequence n;
    @DexIgnore
    public Drawable o;
    @DexIgnore
    public View p;
    @DexIgnore
    public boolean q; // = false;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public boolean s; // = false;
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public boolean u; // = false;
    @DexIgnore
    public ArrayList<Eg0> v; // = new ArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<WeakReference<Ig0>> w; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Eg0 x;
    @DexIgnore
    public boolean y; // = false;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        boolean a(Cg0 cg0, MenuItem menuItem);

        @DexIgnore
        void b(Cg0 cg0);
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        boolean a(Eg0 eg0);
    }

    @DexIgnore
    public Cg0(Context context) {
        this.a = context;
        this.b = context.getResources();
        this.f = new ArrayList<>();
        this.g = new ArrayList<>();
        this.h = true;
        this.i = new ArrayList<>();
        this.j = new ArrayList<>();
        this.k = true;
        f0(true);
    }

    @DexIgnore
    public static int D(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0) {
            int[] iArr = A;
            if (i3 < iArr.length) {
                return (iArr[i3] << 16) | (65535 & i2);
            }
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    @DexIgnore
    public static int p(ArrayList<Eg0> arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).f() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    @DexIgnore
    public View A() {
        return this.p;
    }

    @DexIgnore
    public ArrayList<Eg0> B() {
        t();
        return this.j;
    }

    @DexIgnore
    public boolean C() {
        return this.t;
    }

    @DexIgnore
    public Resources E() {
        return this.b;
    }

    @DexIgnore
    public Cg0 F() {
        return this;
    }

    @DexIgnore
    public ArrayList<Eg0> G() {
        if (!this.h) {
            return this.g;
        }
        this.g.clear();
        int size = this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            Eg0 eg0 = this.f.get(i2);
            if (eg0.isVisible()) {
                this.g.add(eg0);
            }
        }
        this.h = false;
        this.k = true;
        return this.g;
    }

    @DexIgnore
    public boolean H() {
        return this.y;
    }

    @DexIgnore
    public boolean I() {
        return this.c;
    }

    @DexIgnore
    public boolean J() {
        return this.d;
    }

    @DexIgnore
    public void K(Eg0 eg0) {
        this.k = true;
        M(true);
    }

    @DexIgnore
    public void L(Eg0 eg0) {
        this.h = true;
        M(true);
    }

    @DexIgnore
    public void M(boolean z2) {
        if (!this.q) {
            if (z2) {
                this.h = true;
                this.k = true;
            }
            i(z2);
            return;
        }
        this.r = true;
        if (z2) {
            this.s = true;
        }
    }

    @DexIgnore
    public boolean N(MenuItem menuItem, int i2) {
        return O(menuItem, null, i2);
    }

    @DexIgnore
    public boolean O(MenuItem menuItem, Ig0 ig0, int i2) {
        Eg0 eg0 = (Eg0) menuItem;
        if (eg0 == null || !eg0.isEnabled()) {
            return false;
        }
        boolean k2 = eg0.k();
        Sn0 b2 = eg0.b();
        boolean z2 = b2 != null && b2.hasSubMenu();
        if (eg0.j()) {
            boolean expandActionView = eg0.expandActionView() | k2;
            if (!expandActionView) {
                return expandActionView;
            }
            e(true);
            return expandActionView;
        } else if (eg0.hasSubMenu() || z2) {
            if ((i2 & 4) == 0) {
                e(false);
            }
            if (!eg0.hasSubMenu()) {
                eg0.x(new Ng0(w(), this, eg0));
            }
            Ng0 ng0 = (Ng0) eg0.getSubMenu();
            if (z2) {
                b2.onPrepareSubMenu(ng0);
            }
            boolean l2 = l(ng0, ig0) | k2;
            if (l2) {
                return l2;
            }
            e(true);
            return l2;
        } else if ((i2 & 1) != 0) {
            return k2;
        } else {
            e(true);
            return k2;
        }
    }

    @DexIgnore
    public final void P(int i2, boolean z2) {
        if (i2 >= 0 && i2 < this.f.size()) {
            this.f.remove(i2);
            if (z2) {
                M(true);
            }
        }
    }

    @DexIgnore
    public void Q(Ig0 ig0) {
        Iterator<WeakReference<Ig0>> it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference<Ig0> next = it.next();
            Ig0 ig02 = next.get();
            if (ig02 == null || ig02 == ig0) {
                this.w.remove(next);
            }
        }
    }

    @DexIgnore
    public void R(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray(v());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View actionView = item.getActionView();
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((Ng0) item.getSubMenu()).R(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                findItem.expandActionView();
            }
        }
    }

    @DexIgnore
    public void S(Bundle bundle) {
        j(bundle);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0043 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void T(android.os.Bundle r8) {
        /*
            r7 = this;
            int r3 = r7.size()
            r0 = 0
            r1 = 0
            r2 = r1
        L_0x0007:
            if (r2 >= r3) goto L_0x0047
            android.view.MenuItem r4 = r7.getItem(r2)
            android.view.View r1 = r4.getActionView()
            if (r1 == 0) goto L_0x0051
            int r5 = r1.getId()
            r6 = -1
            if (r5 == r6) goto L_0x0051
            if (r0 != 0) goto L_0x0021
            android.util.SparseArray r0 = new android.util.SparseArray
            r0.<init>()
        L_0x0021:
            r1.saveHierarchyState(r0)
            boolean r1 = r4.isActionViewExpanded()
            if (r1 == 0) goto L_0x0051
            java.lang.String r1 = "android:menu:expandedactionview"
            int r5 = r4.getItemId()
            r8.putInt(r1, r5)
            r1 = r0
        L_0x0034:
            boolean r0 = r4.hasSubMenu()
            if (r0 == 0) goto L_0x0043
            android.view.SubMenu r0 = r4.getSubMenu()
            com.fossil.Ng0 r0 = (com.fossil.Ng0) r0
            r0.T(r8)
        L_0x0043:
            int r2 = r2 + 1
            r0 = r1
            goto L_0x0007
        L_0x0047:
            if (r0 == 0) goto L_0x0050
            java.lang.String r1 = r7.v()
            r8.putSparseParcelableArray(r1, r0)
        L_0x0050:
            return
        L_0x0051:
            r1 = r0
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Cg0.T(android.os.Bundle):void");
    }

    @DexIgnore
    public void U(Bundle bundle) {
        k(bundle);
    }

    @DexIgnore
    public void V(Ai ai) {
        this.e = ai;
    }

    @DexIgnore
    public Cg0 W(int i2) {
        this.l = i2;
        return this;
    }

    @DexIgnore
    public void X(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.f.size();
        h0();
        for (int i2 = 0; i2 < size; i2++) {
            Eg0 eg0 = this.f.get(i2);
            if (eg0.getGroupId() == groupId && eg0.m() && eg0.isCheckable()) {
                eg0.s(eg0 == menuItem);
            }
        }
        g0();
    }

    @DexIgnore
    public Cg0 Y(int i2) {
        a0(0, null, i2, null, null);
        return this;
    }

    @DexIgnore
    public Cg0 Z(Drawable drawable) {
        a0(0, null, 0, drawable, null);
        return this;
    }

    @DexIgnore
    public MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int D = D(i4);
        Eg0 g2 = g(i2, i3, i4, D, charSequence, this.l);
        ContextMenu.ContextMenuInfo contextMenuInfo = this.m;
        if (contextMenuInfo != null) {
            g2.v(contextMenuInfo);
        }
        ArrayList<Eg0> arrayList = this.f;
        arrayList.add(p(arrayList, D), g2);
        M(true);
        return g2;
    }

    @DexIgnore
    public final void a0(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources E = E();
        if (view != null) {
            this.p = view;
            this.n = null;
            this.o = null;
        } else {
            if (i2 > 0) {
                this.n = E.getText(i2);
            } else if (charSequence != null) {
                this.n = charSequence;
            }
            if (i3 > 0) {
                this.o = W6.f(w(), i3);
            } else if (drawable != null) {
                this.o = drawable;
            }
            this.p = null;
        }
        M(false);
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i2) {
        return a(0, 0, 0, this.b.getString(i2));
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.b.getString(i5));
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    @DexIgnore
    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        int i6;
        int i7;
        PackageManager packageManager = this.a.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
            i6 = 0;
        } else {
            i6 = 0;
        }
        while (i6 < size) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            int i8 = resolveInfo.specificIndex;
            Intent intent2 = new Intent(i8 < 0 ? intent : intentArr[i8]);
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            intent2.setComponent(new ComponentName(activityInfo.applicationInfo.packageName, activityInfo.name));
            MenuItem intent3 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null && (i7 = resolveInfo.specificIndex) >= 0) {
                menuItemArr[i7] = intent3;
            }
            i6++;
        }
        return size;
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.b.getString(i2));
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.b.getString(i5));
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        Eg0 eg0 = (Eg0) a(i2, i3, i4, charSequence);
        Ng0 ng0 = new Ng0(this.a, this, eg0);
        eg0.x(ng0);
        return ng0;
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    @DexIgnore
    public void b(Ig0 ig0) {
        c(ig0, this.a);
    }

    @DexIgnore
    public Cg0 b0(int i2) {
        a0(i2, null, 0, null, null);
        return this;
    }

    @DexIgnore
    public void c(Ig0 ig0, Context context) {
        this.w.add(new WeakReference<>(ig0));
        ig0.h(context, this);
        this.k = true;
    }

    @DexIgnore
    public Cg0 c0(CharSequence charSequence) {
        a0(0, charSequence, 0, null, null);
        return this;
    }

    @DexIgnore
    public void clear() {
        Eg0 eg0 = this.x;
        if (eg0 != null) {
            f(eg0);
        }
        this.f.clear();
        M(true);
    }

    @DexIgnore
    public void clearHeader() {
        this.o = null;
        this.n = null;
        this.p = null;
        M(false);
    }

    @DexIgnore
    public void close() {
        e(true);
    }

    @DexIgnore
    public void d() {
        Ai ai = this.e;
        if (ai != null) {
            ai.b(this);
        }
    }

    @DexIgnore
    public Cg0 d0(View view) {
        a0(0, null, 0, null, view);
        return this;
    }

    @DexIgnore
    public final void e(boolean z2) {
        if (!this.u) {
            this.u = true;
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    ig0.b(this, z2);
                }
            }
            this.u = false;
        }
    }

    @DexIgnore
    public void e0(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public boolean f(Eg0 eg0) {
        boolean z2 = false;
        if (!this.w.isEmpty() && this.x == eg0) {
            h0();
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            boolean z3 = false;
            while (true) {
                if (!it.hasNext()) {
                    z2 = z3;
                    break;
                }
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    z2 = ig0.e(this, eg0);
                    if (z2) {
                        break;
                    }
                    z3 = z2;
                }
            }
            g0();
            if (z2) {
                this.x = null;
            }
        }
        return z2;
    }

    @DexIgnore
    public final void f0(boolean z2) {
        boolean z3 = true;
        if (!z2 || this.b.getConfiguration().keyboard == 1 || !No0.e(ViewConfiguration.get(this.a), this.a)) {
            z3 = false;
        }
        this.d = z3;
    }

    @DexIgnore
    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            Eg0 eg0 = this.f.get(i3);
            if (eg0.getItemId() == i2) {
                return eg0;
            }
            if (eg0.hasSubMenu() && (findItem = eg0.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    @DexIgnore
    public final Eg0 g(int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        return new Eg0(this, i2, i3, i4, i5, charSequence, i6);
    }

    @DexIgnore
    public void g0() {
        this.q = false;
        if (this.r) {
            this.r = false;
            M(this.s);
        }
    }

    @DexIgnore
    public MenuItem getItem(int i2) {
        return this.f.get(i2);
    }

    @DexIgnore
    public boolean h(Cg0 cg0, MenuItem menuItem) {
        Ai ai = this.e;
        return ai != null && ai.a(cg0, menuItem);
    }

    @DexIgnore
    public void h0() {
        if (!this.q) {
            this.q = true;
            this.r = false;
            this.s = false;
        }
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        if (this.z) {
            return true;
        }
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.f.get(i2).isVisible()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void i(boolean z2) {
        if (!this.w.isEmpty()) {
            h0();
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    ig0.c(z2);
                }
            }
            g0();
        }
    }

    @DexIgnore
    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return r(i2, keyEvent) != null;
    }

    @DexIgnore
    public final void j(Bundle bundle) {
        Parcelable parcelable;
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.w.isEmpty()) {
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    int id = ig0.getId();
                    if (id > 0 && (parcelable = (Parcelable) sparseParcelableArray.get(id)) != null) {
                        ig0.i(parcelable);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void k(Bundle bundle) {
        Parcelable l2;
        if (!this.w.isEmpty()) {
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    int id = ig0.getId();
                    if (id > 0 && (l2 = ig0.l()) != null) {
                        sparseArray.put(id, l2);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    @DexIgnore
    public final boolean l(Ng0 ng0, Ig0 ig0) {
        boolean z2 = false;
        if (this.w.isEmpty()) {
            return false;
        }
        if (ig0 != null) {
            z2 = ig0.k(ng0);
        }
        Iterator<WeakReference<Ig0>> it = this.w.iterator();
        boolean z3 = z2;
        while (it.hasNext()) {
            WeakReference<Ig0> next = it.next();
            Ig0 ig02 = next.get();
            if (ig02 == null) {
                this.w.remove(next);
            } else if (!z3) {
                z3 = ig02.k(ng0);
            }
        }
        return z3;
    }

    @DexIgnore
    public boolean m(Eg0 eg0) {
        boolean z2 = false;
        if (!this.w.isEmpty()) {
            h0();
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            boolean z3 = false;
            while (true) {
                if (!it.hasNext()) {
                    z2 = z3;
                    break;
                }
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    z2 = ig0.f(this, eg0);
                    if (z2) {
                        break;
                    }
                    z3 = z2;
                }
            }
            g0();
            if (z2) {
                this.x = eg0;
            }
        }
        return z2;
    }

    @DexIgnore
    public int n(int i2) {
        return o(i2, 0);
    }

    @DexIgnore
    public int o(int i2, int i3) {
        int size = size();
        for (int i4 = i3 < 0 ? 0 : i3; i4 < size; i4++) {
            if (this.f.get(i4).getGroupId() == i2) {
                return i4;
            }
        }
        return -1;
    }

    @DexIgnore
    public boolean performIdentifierAction(int i2, int i3) {
        return N(findItem(i2), i3);
    }

    @DexIgnore
    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        Eg0 r2 = r(i2, keyEvent);
        boolean N = r2 != null ? N(r2, i3) : false;
        if ((i3 & 2) != 0) {
            e(true);
        }
        return N;
    }

    @DexIgnore
    public int q(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.f.get(i3).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    @DexIgnore
    public Eg0 r(int i2, KeyEvent keyEvent) {
        ArrayList<Eg0> arrayList = this.v;
        arrayList.clear();
        s(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean I = I();
        for (int i3 = 0; i3 < size; i3++) {
            Eg0 eg0 = arrayList.get(i3);
            char alphabeticShortcut = I ? eg0.getAlphabeticShortcut() : eg0.getNumericShortcut();
            if (alphabeticShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return eg0;
            }
            if (alphabeticShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return eg0;
            }
            if (I && alphabeticShortcut == '\b' && i2 == 67) {
                return eg0;
            }
        }
        return null;
    }

    @DexIgnore
    public void removeGroup(int i2) {
        int n2 = n(i2);
        if (n2 >= 0) {
            int size = this.f.size();
            for (int i3 = 0; i3 < size - n2 && this.f.get(n2).getGroupId() == i2; i3++) {
                P(n2, false);
            }
            M(true);
        }
    }

    @DexIgnore
    public void removeItem(int i2) {
        P(q(i2), true);
    }

    @DexIgnore
    public void s(List<Eg0> list, int i2, KeyEvent keyEvent) {
        boolean I = I();
        int modifiers = keyEvent.getModifiers();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.f.size();
            for (int i3 = 0; i3 < size; i3++) {
                Eg0 eg0 = this.f.get(i3);
                if (eg0.hasSubMenu()) {
                    ((Cg0) eg0.getSubMenu()).s(list, i2, keyEvent);
                }
                char alphabeticShortcut = I ? eg0.getAlphabeticShortcut() : eg0.getNumericShortcut();
                if (((modifiers & 69647) == ((I ? eg0.getAlphabeticModifiers() : eg0.getNumericModifiers()) & 69647)) && alphabeticShortcut != 0) {
                    char[] cArr = keyData.meta;
                    if ((alphabeticShortcut == cArr[0] || alphabeticShortcut == cArr[2] || (I && alphabeticShortcut == '\b' && i2 == 67)) && eg0.isEnabled()) {
                        list.add(eg0);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void setGroupCheckable(int i2, boolean z2, boolean z3) {
        int size = this.f.size();
        for (int i3 = 0; i3 < size; i3++) {
            Eg0 eg0 = this.f.get(i3);
            if (eg0.getGroupId() == i2) {
                eg0.t(z3);
                eg0.setCheckable(z2);
            }
        }
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void setGroupEnabled(int i2, boolean z2) {
        int size = this.f.size();
        for (int i3 = 0; i3 < size; i3++) {
            Eg0 eg0 = this.f.get(i3);
            if (eg0.getGroupId() == i2) {
                eg0.setEnabled(z2);
            }
        }
    }

    @DexIgnore
    public void setGroupVisible(int i2, boolean z2) {
        int size = this.f.size();
        boolean z3 = false;
        int i3 = 0;
        while (i3 < size) {
            Eg0 eg0 = this.f.get(i3);
            i3++;
            z3 = (eg0.getGroupId() != i2 || !eg0.y(z2)) ? z3 : true;
        }
        if (z3) {
            M(true);
        }
    }

    @DexIgnore
    public void setQwertyMode(boolean z2) {
        this.c = z2;
        M(false);
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    public void t() {
        ArrayList<Eg0> G = G();
        if (this.k) {
            Iterator<WeakReference<Ig0>> it = this.w.iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                WeakReference<Ig0> next = it.next();
                Ig0 ig0 = next.get();
                if (ig0 == null) {
                    this.w.remove(next);
                } else {
                    z2 = ig0.d() | z2;
                }
            }
            if (z2) {
                this.i.clear();
                this.j.clear();
                int size = G.size();
                for (int i2 = 0; i2 < size; i2++) {
                    Eg0 eg0 = G.get(i2);
                    if (eg0.l()) {
                        this.i.add(eg0);
                    } else {
                        this.j.add(eg0);
                    }
                }
            } else {
                this.i.clear();
                this.j.clear();
                this.j.addAll(G());
            }
            this.k = false;
        }
    }

    @DexIgnore
    public ArrayList<Eg0> u() {
        t();
        return this.i;
    }

    @DexIgnore
    public String v() {
        return "android:menu:actionviewstates";
    }

    @DexIgnore
    public Context w() {
        return this.a;
    }

    @DexIgnore
    public Eg0 x() {
        return this.x;
    }

    @DexIgnore
    public Drawable y() {
        return this.o;
    }

    @DexIgnore
    public CharSequence z() {
        return this.n;
    }
}
