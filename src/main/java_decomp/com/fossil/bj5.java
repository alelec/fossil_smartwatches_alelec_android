package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bj5 extends Ri5 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Bj5(String str, int i) {
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }
}
