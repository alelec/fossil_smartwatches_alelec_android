package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rx1<T> {
    @DexIgnore
    public /* final */ Ry1 a;

    @DexIgnore
    public Rx1(Ry1 ry1) {
        Wg6.c(ry1, "baseVersion");
        this.a = ry1;
    }

    @DexIgnore
    public final Ry1 a() {
        return this.a;
    }

    @DexIgnore
    public abstract T b(byte[] bArr) throws Sx1;
}
