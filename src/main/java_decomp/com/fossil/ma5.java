package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ma5 extends La5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362172, 1);
        P.put(2131362666, 2);
        P.put(2131361905, 3);
        P.put(2131362766, 4);
        P.put(2131362639, 5);
        P.put(2131362235, 6);
        P.put(2131362640, 7);
        P.put(2131362237, 8);
        P.put(2131363325, 9);
        P.put(2131362636, 10);
        P.put(2131363320, 11);
        P.put(2131362439, 12);
        P.put(2131362277, 13);
        P.put(2131362272, 14);
        P.put(2131362280, 15);
        P.put(2131362449, 16);
        P.put(2131362450, 17);
        P.put(2131363065, 18);
        P.put(2131362559, 19);
        P.put(2131362560, 20);
        P.put(2131363067, 21);
        P.put(2131363069, 22);
    }
    */

    @DexIgnore
    public Ma5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 23, O, P));
    }

    @DexIgnore
    public Ma5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FossilCircleImageView) objArr[3], (ConstraintLayout) objArr[1], (FlexibleTextInputEditText) objArr[6], (FlexibleTextInputEditText) objArr[8], (FlexibleButton) objArr[14], (FlexibleButton) objArr[13], (FlexibleButton) objArr[15], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[20], (FlexibleTextInputLayout) objArr[10], (FlexibleTextInputLayout) objArr[5], (FlexibleTextInputLayout) objArr[7], (RTLImageView) objArr[2], (FossilCircleImageView) objArr[4], (ConstraintLayout) objArr[0], (RulerValuePicker) objArr[18], (RulerValuePicker) objArr[21], (ProgressButton) objArr[22], (FlexibleTextInputEditText) objArr[11], (FlexibleTextView) objArr[9]);
        this.N = -1;
        this.H.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.N != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.N = 1;
        }
        w();
    }
}
