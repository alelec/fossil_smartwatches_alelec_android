package com.fossil;

import com.fossil.C44;
import com.fossil.H34;
import com.fossil.U24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class G34<E> extends U24<E> implements C44<E> {
    @DexIgnore
    @LazyInit
    public transient Y24<E> b;
    @DexIgnore
    @LazyInit
    public transient H34<C44.Ai<E>> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends H54<E> {
        @DexIgnore
        public int b;
        @DexIgnore
        public E c;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator d;

        @DexIgnore
        public Ai(G34 g34, Iterator it) {
            this.d = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b > 0 || this.d.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (this.b <= 0) {
                C44.Ai ai = (C44.Ai) this.d.next();
                this.c = (E) ai.getElement();
                this.b = ai.getCount();
            }
            this.b--;
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<E> extends U24.Bi<E> {
        @DexIgnore
        public /* final */ C44<E> a;

        @DexIgnore
        public Bi() {
            this(R34.create());
        }

        @DexIgnore
        public Bi(C44<E> c44) {
            this.a = c44;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.U24.Bi
        public /* bridge */ /* synthetic */ U24.Bi a(Object obj) {
            return e(obj);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<E> e(E e) {
            C44<E> c44 = this.a;
            I14.l(e);
            c44.add(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi<E> f(E... eArr) {
            super.b(eArr);
            return this;
        }

        @DexIgnore
        public G34<E> g() {
            return G34.copyOf(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends H34.Bi<C44.Ai<E>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public /* synthetic */ Ci(G34 g34, Ai ai) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            if (!(obj instanceof C44.Ai)) {
                return false;
            }
            C44.Ai ai = (C44.Ai) obj;
            return ai.getCount() > 0 && G34.this.count(ai.getElement()) == ai.getCount();
        }

        @DexIgnore
        @Override // com.fossil.H34.Bi
        public C44.Ai<E> get(int i) {
            return G34.this.getEntry(i);
        }

        @DexIgnore
        @Override // com.fossil.H34
        public int hashCode() {
            return G34.this.hashCode();
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return G34.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return G34.this.elementSet().size();
        }

        @DexIgnore
        @Override // com.fossil.U24, com.fossil.H34
        public Object writeReplace() {
            return new Di(G34.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<E> implements Serializable {
        @DexIgnore
        public /* final */ G34<E> multiset;

        @DexIgnore
        public Di(G34<E> g34) {
            this.multiset = g34;
        }

        @DexIgnore
        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int[] counts;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public Ei(C44<?> c44) {
            int size = c44.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            int i = 0;
            for (C44.Ai<?> ai : c44.entrySet()) {
                this.elements[i] = ai.getElement();
                this.counts[i] = ai.getCount();
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            R34 create = R34.create(this.elements.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.elements;
                if (i >= objArr.length) {
                    return G34.copyOf(create);
                }
                create.add(objArr[i], this.counts[i]);
                i++;
            }
        }
    }

    @DexIgnore
    public static <E> G34<E> a(E... eArr) {
        R34 create = R34.create();
        Collections.addAll(create, eArr);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> Bi<E> builder() {
        return new Bi<>();
    }

    @DexIgnore
    public static <E> G34<E> copyFromEntries(Collection<? extends C44.Ai<? extends E>> collection) {
        return collection.isEmpty() ? of() : new Q44(collection);
    }

    @DexIgnore
    public static <E> G34<E> copyOf(Iterable<? extends E> iterable) {
        if (iterable instanceof G34) {
            G34<E> g34 = (G34) iterable;
            if (!g34.isPartialView()) {
                return g34;
            }
        }
        return copyFromEntries((iterable instanceof C44 ? D44.b(iterable) : R34.create(iterable)).entrySet());
    }

    @DexIgnore
    public static <E> G34<E> copyOf(Iterator<? extends E> it) {
        R34 create = R34.create();
        P34.a(create, it);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> G34<E> copyOf(E[] eArr) {
        return a(eArr);
    }

    @DexIgnore
    public static <E> G34<E> of() {
        return Q44.EMPTY;
    }

    @DexIgnore
    public static <E> G34<E> of(E e) {
        return a(e);
    }

    @DexIgnore
    public static <E> G34<E> of(E e, E e2) {
        return a(e, e2);
    }

    @DexIgnore
    public static <E> G34<E> of(E e, E e2, E e3) {
        return a(e, e2, e3);
    }

    @DexIgnore
    public static <E> G34<E> of(E e, E e2, E e3, E e4) {
        return a(e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> G34<E> of(E e, E e2, E e3, E e4, E e5) {
        return a(e, e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> G34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        Bi bi = new Bi();
        bi.e(e);
        bi.e(e2);
        bi.e(e3);
        bi.e(e4);
        bi.e(e5);
        bi.e(e6);
        bi.f(eArr);
        return bi.g();
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    @Deprecated
    public final int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public Y24<E> asList() {
        Y24<E> y24 = this.b;
        if (y24 != null) {
            return y24;
        }
        Y24<E> createAsList = createAsList();
        this.b = createAsList;
        return createAsList;
    }

    @DexIgnore
    public final H34<C44.Ai<E>> b() {
        return isEmpty() ? H34.of() : new Ci(this, null);
    }

    @DexIgnore
    @Override // com.fossil.C44, com.fossil.U24
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = entrySet().iterator();
        while (it.hasNext()) {
            C44.Ai ai = (C44.Ai) it.next();
            Arrays.fill(objArr, i, ai.getCount() + i, ai.getElement());
            i += ai.getCount();
        }
        return i;
    }

    @DexIgnore
    @Override // com.fossil.C44
    public abstract /* synthetic */ int count(Object obj);

    @DexIgnore
    public Y24<E> createAsList() {
        return isEmpty() ? Y24.of() : new M44(this, toArray());
    }

    @DexIgnore
    @Override // com.fossil.C44
    public abstract /* synthetic */ Set<E> elementSet();

    @DexIgnore
    @Override // com.fossil.C44
    public H34<C44.Ai<E>> entrySet() {
        H34<C44.Ai<E>> h34 = this.c;
        if (h34 != null) {
            return h34;
        }
        H34<C44.Ai<E>> b2 = b();
        this.c = b2;
        return b2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return D44.c(this, obj);
    }

    @DexIgnore
    public abstract C44.Ai<E> getEntry(int i);

    @DexIgnore
    public int hashCode() {
        return X44.b(entrySet());
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.U24, com.fossil.U24, java.lang.Iterable
    public H54<E> iterator() {
        return new Ai(this, entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    @Deprecated
    public final int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    @Deprecated
    public final int setCount(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    @Deprecated
    public final boolean setCount(E e, int i, int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public Object writeReplace() {
        return new Ei(this);
    }
}
