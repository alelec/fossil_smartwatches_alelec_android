package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gv4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ Ai v; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<J85> h;
    @DexIgnore
    public BCLeaderBoardViewModel i;
    @DexIgnore
    public String j;
    @DexIgnore
    public long k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public String m;
    @DexIgnore
    public Kv4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Gv4.u;
        }

        @DexIgnore
        public final Gv4 b(String str, long j, int i, String str2) {
            Gv4 gv4 = new Gv4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putLong("challenge_start_time_extra", j);
            bundle.putInt("challenge_target_extra", i);
            bundle.putString("challenge_status_extra", str2);
            gv4.setArguments(bundle);
            return gv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 b;

        @DexIgnore
        public Bi(Gv4 gv4) {
            this.b = gv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ J85 a;
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 b;

        @DexIgnore
        public Ci(J85 j85, Gv4 gv4) {
            this.a = j85;
            this.b = gv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.q;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            String str = this.b.j;
            if (str != null) {
                Gv4.O6(this.b).m(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 b;

        @DexIgnore
        public Di(Gv4 gv4) {
            this.b = gv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            BCFindFriendsActivity.a aVar = BCFindFriendsActivity.A;
            Gv4 gv4 = this.b;
            aVar.a(gv4, gv4.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 a;

        @DexIgnore
        public Ei(Gv4 gv4) {
            this.a = gv4;
        }

        @DexIgnore
        public final void a(List<? extends Object> list) {
            Kv4 M6 = Gv4.M6(this.a);
            Wg6.b(list, "it");
            M6.g(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Object> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 a;

        @DexIgnore
        public Fi(Gv4 gv4) {
            this.a = gv4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            J85 j85 = (J85) Gv4.K6(this.a).a();
            if (j85 != null) {
                SwipeRefreshLayout swipeRefreshLayout = j85.w;
                Wg6.b(swipeRefreshLayout, "swipe");
                Wg6.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 a;

        @DexIgnore
        public Gi(Gv4 gv4) {
            this.a = gv4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            if (lc6.getFirst().booleanValue()) {
                J85 j85 = (J85) Gv4.K6(this.a).a();
                if (j85 != null) {
                    FlexibleTextView flexibleTextView = j85.q;
                    Wg6.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, Um5.c(activity, 2131886231), 1).show();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ Gv4 a;

        @DexIgnore
        public Hi(Gv4 gv4) {
            this.a = gv4;
        }

        @DexIgnore
        public final void a(String str) {
            J85 j85;
            ImageView imageView;
            if (Wg6.a("completed", str) && (j85 = (J85) Gv4.K6(this.a).a()) != null && (imageView = j85.s) != null) {
                imageView.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    /*
    static {
        String simpleName = Gv4.class.getSimpleName();
        Wg6.b(simpleName, "BCLeaderBoardFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Gv4 gv4) {
        G37<J85> g37 = gv4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Kv4 M6(Gv4 gv4) {
        Kv4 kv4 = gv4.s;
        if (kv4 != null) {
            return kv4;
        }
        Wg6.n("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCLeaderBoardViewModel O6(Gv4 gv4) {
        BCLeaderBoardViewModel bCLeaderBoardViewModel = gv4.i;
        if (bCLeaderBoardViewModel != null) {
            return bCLeaderBoardViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        this.s = new Kv4(this.k, this.l);
        G37<J85> g37 = this.h;
        if (g37 != null) {
            J85 a2 = g37.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.v;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                Kv4 kv4 = this.s;
                if (kv4 != null) {
                    recyclerView.setAdapter(kv4);
                    a2.t.setOnClickListener(new Bi(this));
                    a2.w.setOnRefreshListener(new Ci(a2, this));
                    a2.s.setOnClickListener(new Di(this));
                    return;
                }
                Wg6.n("leaderBoardAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        BCLeaderBoardViewModel bCLeaderBoardViewModel = this.i;
        if (bCLeaderBoardViewModel != null) {
            bCLeaderBoardViewModel.i().h(getViewLifecycleOwner(), new Ei(this));
            BCLeaderBoardViewModel bCLeaderBoardViewModel2 = this.i;
            if (bCLeaderBoardViewModel2 != null) {
                bCLeaderBoardViewModel2.h().h(getViewLifecycleOwner(), new Fi(this));
                BCLeaderBoardViewModel bCLeaderBoardViewModel3 = this.i;
                if (bCLeaderBoardViewModel3 != null) {
                    bCLeaderBoardViewModel3.g().h(getViewLifecycleOwner(), new Gi(this));
                    BCLeaderBoardViewModel bCLeaderBoardViewModel4 = this.i;
                    if (bCLeaderBoardViewModel4 != null) {
                        bCLeaderBoardViewModel4.f().h(getViewLifecycleOwner(), new Hi(this));
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 16 && i3 == -1 && (str = this.j) != null) {
            BCLeaderBoardViewModel bCLeaderBoardViewModel = this.i;
            if (bCLeaderBoardViewModel == null) {
                Wg6.n("viewModel");
                throw null;
            } else if (str != null) {
                bCLeaderBoardViewModel.m(str);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        String str = null;
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().C0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCLeaderBoardViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.i = (BCLeaderBoardViewModel) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? arguments.getString("challenge_id_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? arguments2.getLong("challenge_start_time_extra") : new Date().getTime();
            Bundle arguments3 = getArguments();
            this.l = arguments3 != null ? arguments3.getInt("challenge_target_extra") : -1;
            Bundle arguments4 = getArguments();
            if (arguments4 != null) {
                str = arguments4.getString("challenge_status_extra");
            }
            this.m = str;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        J85 j85 = (J85) Aq0.f(layoutInflater, 2131558581, viewGroup, false, A6());
        this.h = new G37<>(this, j85);
        Wg6.b(j85, "binding");
        return j85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_full_leader_board", activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCLeaderBoardViewModel bCLeaderBoardViewModel = this.i;
        if (bCLeaderBoardViewModel != null) {
            bCLeaderBoardViewModel.k(this.j, this.m);
            P6();
            Q6();
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
