package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ee3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ee3> CREATOR; // = new Ze3();
    @DexIgnore
    public LatLng b; // = null;
    @DexIgnore
    public double c; // = 0.0d;
    @DexIgnore
    public float d; // = 10.0f;
    @DexIgnore
    public int e; // = -16777216;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public float g; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public List<Me3> j; // = null;

    @DexIgnore
    public Ee3() {
    }

    @DexIgnore
    public Ee3(LatLng latLng, double d2, float f2, int i2, int i3, float f3, boolean z, boolean z2, List<Me3> list) {
        this.b = latLng;
        this.c = d2;
        this.d = f2;
        this.e = i2;
        this.f = i3;
        this.g = f3;
        this.h = z;
        this.i = z2;
        this.j = list;
    }

    @DexIgnore
    public final int A() {
        return this.f;
    }

    @DexIgnore
    public final double D() {
        return this.c;
    }

    @DexIgnore
    public final int F() {
        return this.e;
    }

    @DexIgnore
    public final List<Me3> L() {
        return this.j;
    }

    @DexIgnore
    public final Ee3 c(LatLng latLng) {
        this.b = latLng;
        return this;
    }

    @DexIgnore
    public final Ee3 f(boolean z) {
        this.i = z;
        return this;
    }

    @DexIgnore
    public final Ee3 h(int i2) {
        this.f = i2;
        return this;
    }

    @DexIgnore
    public final LatLng k() {
        return this.b;
    }

    @DexIgnore
    public final float o0() {
        return this.d;
    }

    @DexIgnore
    public final float p0() {
        return this.g;
    }

    @DexIgnore
    public final boolean q0() {
        return this.i;
    }

    @DexIgnore
    public final boolean r0() {
        return this.h;
    }

    @DexIgnore
    public final Ee3 s0(double d2) {
        this.c = d2;
        return this;
    }

    @DexIgnore
    public final Ee3 t0(int i2) {
        this.e = i2;
        return this;
    }

    @DexIgnore
    public final Ee3 u0(float f2) {
        this.d = f2;
        return this;
    }

    @DexIgnore
    public final Ee3 v0(boolean z) {
        this.h = z;
        return this;
    }

    @DexIgnore
    public final Ee3 w0(float f2) {
        this.g = f2;
        return this;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 2, k(), i2, false);
        Bd2.h(parcel, 3, D());
        Bd2.j(parcel, 4, o0());
        Bd2.n(parcel, 5, F());
        Bd2.n(parcel, 6, A());
        Bd2.j(parcel, 7, p0());
        Bd2.c(parcel, 8, r0());
        Bd2.c(parcel, 9, q0());
        Bd2.y(parcel, 10, L(), false);
        Bd2.b(parcel, a2);
    }
}
