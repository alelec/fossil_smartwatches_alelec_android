package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us7<T> extends Vs7<T> implements Iterator<T>, Xe6<Cd6>, Jr7 {
    @DexIgnore
    public int b;
    @DexIgnore
    public T c;
    @DexIgnore
    public Iterator<? extends T> d;
    @DexIgnore
    public Xe6<? super Cd6> e;

    @DexIgnore
    @Override // com.fossil.Vs7
    public Object a(T t, Xe6<? super Cd6> xe6) {
        this.c = t;
        this.b = 3;
        this.e = xe6;
        Object d2 = Yn7.d();
        if (d2 == Yn7.d()) {
            Go7.c(xe6);
        }
        return d2 == Yn7.d() ? d2 : Cd6.a;
    }

    @DexIgnore
    public final Throwable c() {
        int i = this.b;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        return new IllegalStateException("Unexpected state of the iterator: " + this.b);
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public Af6 getContext() {
        return Un7.INSTANCE;
    }

    @DexIgnore
    public final T h() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public boolean hasNext() {
        while (true) {
            int i = this.b;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.d;
                    if (it == null) {
                        Wg6.i();
                        throw null;
                    } else if (it.hasNext()) {
                        this.b = 2;
                        return true;
                    } else {
                        this.d = null;
                    }
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw c();
                }
            }
            this.b = 5;
            Xe6<? super Cd6> xe6 = this.e;
            if (xe6 != null) {
                this.e = null;
                Cd6 cd6 = Cd6.a;
                Dl7.Ai ai = Dl7.Companion;
                xe6.resumeWith(Dl7.constructor-impl(cd6));
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void i(Xe6<? super Cd6> xe6) {
        this.e = xe6;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        int i = this.b;
        if (i == 0 || i == 1) {
            return h();
        }
        if (i == 2) {
            this.b = 1;
            Iterator<? extends T> it = this.d;
            if (it != null) {
                return (T) it.next();
            }
            Wg6.i();
            throw null;
        } else if (i == 3) {
            this.b = 0;
            T t = this.c;
            this.c = null;
            return t;
        } else {
            throw c();
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public void resumeWith(Object obj) {
        El7.b(obj);
        this.b = 4;
    }
}
