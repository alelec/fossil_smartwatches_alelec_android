package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cu3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Bu3 c;

    @DexIgnore
    public Cu3(Bu3 bu3, Nt3 nt3) {
        this.c = bu3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (Bu3.b(this.c)) {
            if (Bu3.c(this.c) != null) {
                Bu3.c(this.c).onFailure(this.b.l());
            }
        }
    }
}
