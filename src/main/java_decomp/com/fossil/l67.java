package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mapped.An4;
import com.mapped.Rc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.Arrays;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L67 extends LinearLayout implements CustomSwipeRefreshLayout.b {
    @DexIgnore
    public ViewGroup b;
    @DexIgnore
    public ImageView c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleTextView e;
    @DexIgnore
    public FlexibleTextView f;
    @DexIgnore
    public FlexibleTextView g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ String j; // = ThemeManager.l.a().d("nonBrandSurface");
    @DexIgnore
    public Wa1 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ L67 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(L67 l67) {
            this.a = l67;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            Va1<Drawable> t = this.a.getMRequestManager$app_fossilRelease().t(str2);
            ImageView ivDevice$app_fossilRelease = this.a.getIvDevice$app_fossilRelease();
            if (ivDevice$app_fossilRelease != null) {
                t.F0(ivDevice$app_fossilRelease);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public L67(Context context) {
        super(context);
        Wg6.c(context, "context");
        Wa1 t = Oa1.t(PortfolioApp.get.instance());
        Wg6.b(t, "Glide.with(PortfolioApp.instance)");
        this.k = t;
        setWillNotDraw(false);
        b();
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.b
    public void a(CustomSwipeRefreshLayout.d dVar, CustomSwipeRefreshLayout.d dVar2) {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        Wg6.c(dVar, "currentState");
        Wg6.c(dVar2, "lastState");
        int a2 = dVar.a();
        int a3 = dVar2.a();
        if (a2 != a3) {
            if (a2 == 0) {
                FlexibleTextView flexibleTextView3 = this.e;
                if (flexibleTextView3 != null) {
                    flexibleTextView3.setText(Um5.c(getContext(), 2131886809));
                }
            } else if (a2 != 1) {
                if (a2 == 2) {
                    FlexibleTextView flexibleTextView4 = this.e;
                    if (flexibleTextView4 != null) {
                        flexibleTextView4.setText(Um5.c(getContext(), 2131886812));
                    }
                } else if (a2 == 3 && (flexibleTextView2 = this.e) != null) {
                    flexibleTextView2.setText(Um5.c(getContext(), 2131886782));
                }
            } else if (!(a3 == 1 || (flexibleTextView = this.e) == null)) {
                flexibleTextView.setText(Um5.c(getContext(), 2131886810));
            }
            c(this.h, this.i);
        }
    }

    @DexIgnore
    public final void b() {
        View inflate = LayoutInflater.from(getContext()).inflate(2131558842, (ViewGroup) null);
        if (inflate != null) {
            this.b = (ViewGroup) inflate;
            ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            ViewGroup viewGroup = this.b;
            if (viewGroup != null) {
                this.d = (FlexibleTextView) viewGroup.findViewById(2131362343);
                this.e = (FlexibleTextView) viewGroup.findViewById(2131362351);
                this.f = (FlexibleTextView) viewGroup.findViewById(2131362346);
                this.g = (FlexibleTextView) viewGroup.findViewById(2131362348);
                this.c = (ImageView) viewGroup.findViewById(2131362653);
                String str = this.j;
                if (str != null) {
                    viewGroup.setBackgroundColor(Color.parseColor(str));
                }
                addView(viewGroup, layoutParams);
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @DexIgnore
    public final void c(String str, String str2) {
        String format;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeadViewCard", "updateStaticInfo - deviceSerial=" + str);
        this.h = str;
        this.i = str2;
        PortfolioApp instance = PortfolioApp.get.instance();
        if (TextUtils.isEmpty(str)) {
            FlexibleTextView flexibleTextView = this.g;
            if (flexibleTextView != null) {
                flexibleTextView.setVisibility(0);
                return;
            }
            return;
        }
        FlexibleTextView flexibleTextView2 = this.g;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setVisibility(8);
        }
        FlexibleTextView flexibleTextView3 = this.d;
        if (flexibleTextView3 != null) {
            flexibleTextView3.setText(str2);
        }
        long C = new An4(instance).C(instance.J());
        if (C <= 0) {
            format = Um5.c(instance, 2131887397);
            Wg6.b(format, "LanguageHelper.getString\u2026 R.string.empty_hour_min)");
        } else {
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "calendar");
            instance2.setTimeInMillis(C);
            Boolean p0 = TimeUtils.p0(instance2.getTime());
            Wg6.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                Hr7 hr7 = Hr7.a;
                String c2 = Um5.c(instance, 2131886691);
                Wg6.b(c2, "LanguageHelper.getString\u2026ay_Title__TodayMonthDate)");
                format = String.format(c2, Arrays.copyOf(new Object[]{TimeUtils.q(instance2.getTime())}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
            } else {
                Calendar instance3 = Calendar.getInstance();
                Wg6.b(instance3, "Calendar.getInstance()");
                long timeInMillis = (instance3.getTimeInMillis() - C) / 3600000;
                long j2 = (long) 24;
                if (timeInMillis < j2) {
                    Hr7 hr72 = Hr7.a;
                    String c3 = Um5.c(instance, 2131887556);
                    Wg6.b(c3, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    format = String.format(c3, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis)}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                } else {
                    Hr7 hr73 = Hr7.a;
                    String c4 = Um5.c(instance, 2131887556);
                    Wg6.b(c4, "LanguageHelper.getString\u2026ontext, R.string.s_h_ago)");
                    format = String.format(c4, Arrays.copyOf(new Object[]{String.valueOf(timeInMillis / j2)}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                }
            }
        }
        FlexibleTextView flexibleTextView4 = this.f;
        if (flexibleTextView4 != null) {
            Hr7 hr74 = Hr7.a;
            String c5 = Um5.c(instance, 2131887179);
            Wg6.b(c5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
            String format2 = String.format(c5, Arrays.copyOf(new Object[]{format}, 1));
            Wg6.b(format2, "java.lang.String.format(format, *args)");
            flexibleTextView4.setText(format2);
        }
        FlexibleTextView flexibleTextView5 = this.f;
        if (flexibleTextView5 != null) {
            flexibleTextView5.setSelected(true);
        }
        CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
        if (str != null) {
            CloudImageHelper.ItemImage type = with.setSerialNumber(str).setSerialPrefix(DeviceHelper.o.m(str)).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.c;
            if (imageView != null) {
                type.setPlaceHolder(imageView, DeviceHelper.o.i(str, DeviceHelper.Bi.SMALL)).setImageCallback(new Ai(this)).download();
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final ImageView getIvDevice$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final Wa1 getMRequestManager$app_fossilRelease() {
        return this.k;
    }

    @DexIgnore
    public final void setIvDevice$app_fossilRelease(ImageView imageView) {
        this.c = imageView;
    }

    @DexIgnore
    public final void setMRequestManager$app_fossilRelease(Wa1 wa1) {
        Wg6.c(wa1, "<set-?>");
        this.k = wa1;
    }
}
