package com.fossil;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Em7 extends Dm7 {
    @DexIgnore
    public static final boolean A(long[] jArr, long j) {
        Wg6.c(jArr, "$this$contains");
        return P(jArr, j) >= 0;
    }

    @DexIgnore
    public static final <T> boolean B(T[] tArr, T t) {
        Wg6.c(tArr, "$this$contains");
        return Q(tArr, t) >= 0;
    }

    @DexIgnore
    public static final boolean C(short[] sArr, short s) {
        Wg6.c(sArr, "$this$contains");
        return R(sArr, s) >= 0;
    }

    @DexIgnore
    public static final <T> List<T> D(T[] tArr) {
        Wg6.c(tArr, "$this$distinct");
        return Pm7.h0(g0(tArr));
    }

    @DexIgnore
    public static final <T> List<T> E(T[] tArr) {
        Wg6.c(tArr, "$this$filterNotNull");
        ArrayList arrayList = new ArrayList();
        F(tArr, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <C extends Collection<? super T>, T> C F(T[] tArr, C c) {
        Wg6.c(tArr, "$this$filterNotNullTo");
        Wg6.c(c, ShareConstants.DESTINATION);
        for (T t : tArr) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }

    @DexIgnore
    public static final Wr7 G(byte[] bArr) {
        Wg6.c(bArr, "$this$indices");
        return new Wr7(0, I(bArr));
    }

    @DexIgnore
    public static final Wr7 H(int[] iArr) {
        Wg6.c(iArr, "$this$indices");
        return new Wr7(0, J(iArr));
    }

    @DexIgnore
    public static final int I(byte[] bArr) {
        Wg6.c(bArr, "$this$lastIndex");
        return bArr.length - 1;
    }

    @DexIgnore
    public static final int J(int[] iArr) {
        Wg6.c(iArr, "$this$lastIndex");
        return iArr.length - 1;
    }

    @DexIgnore
    public static final <T> int K(T[] tArr) {
        Wg6.c(tArr, "$this$lastIndex");
        return tArr.length - 1;
    }

    @DexIgnore
    public static final <T> T L(T[] tArr, int i) {
        Wg6.c(tArr, "$this$getOrNull");
        if (i < 0 || i > K(tArr)) {
            return null;
        }
        return tArr[i];
    }

    @DexIgnore
    public static final int M(byte[] bArr, byte b) {
        Wg6.c(bArr, "$this$indexOf");
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            if (b == bArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final int N(char[] cArr, char c) {
        Wg6.c(cArr, "$this$indexOf");
        int length = cArr.length;
        for (int i = 0; i < length; i++) {
            if (c == cArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final int O(int[] iArr, int i) {
        Wg6.c(iArr, "$this$indexOf");
        int length = iArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (i == iArr[i2]) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final int P(long[] jArr, long j) {
        Wg6.c(jArr, "$this$indexOf");
        int length = jArr.length;
        for (int i = 0; i < length; i++) {
            if (j == jArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> int Q(T[] tArr, T t) {
        int i = 0;
        Wg6.c(tArr, "$this$indexOf");
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
        } else {
            int length2 = tArr.length;
            while (i < length2) {
                if (Wg6.a(t, tArr[i])) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final int R(short[] sArr, short s) {
        Wg6.c(sArr, "$this$indexOf");
        int length = sArr.length;
        for (int i = 0; i < length; i++) {
            if (s == sArr[i]) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <A extends Appendable> A S(char[] cArr, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super Character, ? extends CharSequence> hg6) {
        Wg6.c(cArr, "$this$joinTo");
        Wg6.c(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        a2.append(charSequence2);
        int length = cArr.length;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                break;
            }
            char c = cArr[i3];
            int i4 = i2 + 1;
            if (i4 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i4 > i) {
                i2 = i4;
                break;
            }
            if (hg6 != null) {
                a2.append((CharSequence) hg6.invoke(Character.valueOf(c)));
            } else {
                a2.append(c);
            }
            i3++;
            i2 = i4;
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static final String T(char[] cArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super Character, ? extends CharSequence> hg6) {
        Wg6.c(cArr, "$this$joinToString");
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        S(cArr, sb, charSequence, charSequence2, charSequence3, i, charSequence4, hg6);
        String sb2 = sb.toString();
        Wg6.b(sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }

    @DexIgnore
    public static /* synthetic */ String U(char[] cArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6 hg6, int i2, Object obj) {
        String str = (i2 & 1) != 0 ? ", " : charSequence;
        CharSequence charSequence5 = "";
        String str2 = (i2 & 2) != 0 ? "" : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        return T(cArr, str, str2, charSequence5, (i2 & 8) != 0 ? -1 : i, (i2 & 16) != 0 ? "..." : charSequence4, (i2 & 32) != 0 ? null : hg6);
    }

    @DexIgnore
    public static final byte[] V(byte[] bArr) {
        int i = 0;
        Wg6.c(bArr, "$this$reversedArray");
        if (bArr.length == 0) {
            return bArr;
        }
        byte[] bArr2 = new byte[bArr.length];
        int I = I(bArr);
        if (I >= 0) {
            while (true) {
                bArr2[I - i] = (byte) bArr[i];
                if (i == I) {
                    break;
                }
                i++;
            }
        }
        return bArr2;
    }

    @DexIgnore
    public static final char W(char[] cArr) {
        Wg6.c(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    @DexIgnore
    public static final <T> T X(T[] tArr) {
        Wg6.c(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T[] Y(T[] tArr) {
        Wg6.c(tArr, "$this$sortedArray");
        if (tArr.length == 0) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        T[] tArr2 = (T[]) ((Comparable[]) copyOf);
        if (tArr2 != null) {
            Dm7.u(tArr2);
            return tArr2;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }

    @DexIgnore
    public static final <T> T[] Z(T[] tArr, Comparator<? super T> comparator) {
        Wg6.c(tArr, "$this$sortedArrayWith");
        Wg6.c(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] tArr2 = (T[]) Arrays.copyOf(tArr, tArr.length);
        Wg6.b(tArr2, "java.util.Arrays.copyOf(this, size)");
        Dm7.v(tArr2, comparator);
        return tArr2;
    }

    @DexIgnore
    public static final <T> List<T> a0(T[] tArr, Comparator<? super T> comparator) {
        Wg6.c(tArr, "$this$sortedWith");
        Wg6.c(comparator, "comparator");
        return Dm7.d(Z(tArr, comparator));
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C b0(T[] tArr, C c) {
        Wg6.c(tArr, "$this$toCollection");
        Wg6.c(c, ShareConstants.DESTINATION);
        for (T t : tArr) {
            c.add(t);
        }
        return c;
    }

    @DexIgnore
    public static final List<Integer> c0(int[] iArr) {
        Wg6.c(iArr, "$this$toList");
        int length = iArr.length;
        return length != 0 ? length != 1 ? e0(iArr) : Gm7.b(Integer.valueOf(iArr[0])) : Hm7.e();
    }

    @DexIgnore
    public static final <T> List<T> d0(T[] tArr) {
        Wg6.c(tArr, "$this$toList");
        int length = tArr.length;
        return length != 0 ? length != 1 ? f0(tArr) : Gm7.b(tArr[0]) : Hm7.e();
    }

    @DexIgnore
    public static final List<Integer> e0(int[] iArr) {
        Wg6.c(iArr, "$this$toMutableList");
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int i : iArr) {
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> f0(T[] tArr) {
        Wg6.c(tArr, "$this$toMutableList");
        return new ArrayList(Hm7.d(tArr));
    }

    @DexIgnore
    public static final <T> Set<T> g0(T[] tArr) {
        Wg6.c(tArr, "$this$toMutableSet");
        LinkedHashSet linkedHashSet = new LinkedHashSet(Ym7.b(tArr.length));
        for (T t : tArr) {
            linkedHashSet.add(t);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static final boolean x(byte[] bArr, byte b) {
        Wg6.c(bArr, "$this$contains");
        return M(bArr, b) >= 0;
    }

    @DexIgnore
    public static final boolean y(char[] cArr, char c) {
        Wg6.c(cArr, "$this$contains");
        return N(cArr, c) >= 0;
    }

    @DexIgnore
    public static final boolean z(int[] iArr, int i) {
        Wg6.c(iArr, "$this$contains");
        return O(iArr, i) >= 0;
    }
}
