package com.fossil;

import android.content.Context;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yr3 {
    @DexIgnore
    public Yr3(Context context) {
    }

    @DexIgnore
    public static boolean a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
