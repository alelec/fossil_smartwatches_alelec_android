package com.fossil;

import com.facebook.LegacyTokenHelper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H68 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends ObjectInputStream {
        @DexIgnore
        public static /* final */ Map<String, Class<?>> c;
        @DexIgnore
        public /* final */ ClassLoader b;

        /*
        static {
            HashMap hashMap = new HashMap();
            c = hashMap;
            hashMap.put(LegacyTokenHelper.TYPE_BYTE, Byte.TYPE);
            c.put(LegacyTokenHelper.TYPE_SHORT, Short.TYPE);
            c.put(LegacyTokenHelper.TYPE_INTEGER, Integer.TYPE);
            c.put(LegacyTokenHelper.TYPE_LONG, Long.TYPE);
            c.put(LegacyTokenHelper.TYPE_FLOAT, Float.TYPE);
            c.put(LegacyTokenHelper.TYPE_DOUBLE, Double.TYPE);
            c.put("boolean", Boolean.TYPE);
            c.put(LegacyTokenHelper.TYPE_CHAR, Character.TYPE);
            c.put("void", Void.TYPE);
        }
        */

        @DexIgnore
        public Ai(InputStream inputStream, ClassLoader classLoader) throws IOException {
            super(inputStream);
            this.b = classLoader;
        }

        @DexIgnore
        @Override // java.io.ObjectInputStream
        public Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
            String name = objectStreamClass.getName();
            try {
                return Class.forName(name, false, this.b);
            } catch (ClassNotFoundException e) {
                try {
                    return Class.forName(name, false, Thread.currentThread().getContextClassLoader());
                } catch (ClassNotFoundException e2) {
                    Class<?> cls = c.get(name);
                    if (cls != null) {
                        return cls;
                    }
                    throw e2;
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003b A[SYNTHETIC, Splitter:B:20:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T extends java.io.Serializable> T a(T r4) {
        /*
            r2 = 0
            if (r4 != 0) goto L_0x0004
        L_0x0003:
            return r2
        L_0x0004:
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r1 = c(r4)
            r0.<init>(r1)
            com.fossil.H68$Ai r1 = new com.fossil.H68$Ai     // Catch:{ ClassNotFoundException -> 0x003f, IOException -> 0x002e, all -> 0x0052 }
            java.lang.Class r3 = r4.getClass()     // Catch:{ ClassNotFoundException -> 0x003f, IOException -> 0x002e, all -> 0x0052 }
            java.lang.ClassLoader r3 = r3.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x003f, IOException -> 0x002e, all -> 0x0052 }
            r1.<init>(r0, r3)     // Catch:{ ClassNotFoundException -> 0x003f, IOException -> 0x002e, all -> 0x0052 }
            java.lang.Object r0 = r1.readObject()     // Catch:{ ClassNotFoundException -> 0x0055, IOException -> 0x0057, all -> 0x0059 }
            java.io.Serializable r0 = (java.io.Serializable) r0     // Catch:{ ClassNotFoundException -> 0x0055, IOException -> 0x0057, all -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0025 }
            r2 = r0
            goto L_0x0003
        L_0x0025:
            r0 = move-exception
            com.fossil.G68 r1 = new com.fossil.G68
            java.lang.String r2 = "IOException on closing cloned object data InputStream."
            r1.<init>(r2, r0)
            throw r1
        L_0x002e:
            r0 = move-exception
            r1 = r2
        L_0x0030:
            com.fossil.G68 r2 = new com.fossil.G68     // Catch:{ all -> 0x0038 }
            java.lang.String r3 = "IOException while reading cloned object data"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0038 }
            throw r2     // Catch:{ all -> 0x0038 }
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()     // Catch:{ IOException -> 0x0049 }
        L_0x003e:
            throw r0
        L_0x003f:
            r0 = move-exception
            r1 = r2
        L_0x0041:
            com.fossil.G68 r2 = new com.fossil.G68
            java.lang.String r3 = "ClassNotFoundException while reading cloned object data"
            r2.<init>(r3, r0)
            throw r2
        L_0x0049:
            r0 = move-exception
            com.fossil.G68 r1 = new com.fossil.G68
            java.lang.String r2 = "IOException on closing cloned object data InputStream."
            r1.<init>(r2, r0)
            throw r1
        L_0x0052:
            r0 = move-exception
            r1 = r2
            goto L_0x0039
        L_0x0055:
            r0 = move-exception
            goto L_0x0041
        L_0x0057:
            r0 = move-exception
            goto L_0x0030
        L_0x0059:
            r0 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H68.a(java.io.Serializable):java.io.Serializable");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x001a A[SYNTHETIC, Splitter:B:15:0x001a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(java.io.Serializable r3, java.io.OutputStream r4) {
        /*
            r2 = 0
            if (r4 == 0) goto L_0x001e
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x000f, all -> 0x002e }
            r1.<init>(r4)     // Catch:{ IOException -> 0x000f, all -> 0x002e }
            r1.writeObject(r3)     // Catch:{ IOException -> 0x0026, all -> 0x0028 }
            r1.close()     // Catch:{ IOException -> 0x002a }
        L_0x000e:
            return
        L_0x000f:
            r0 = move-exception
            r1 = r2
        L_0x0011:
            com.fossil.G68 r2 = new com.fossil.G68     // Catch:{ all -> 0x0017 }
            r2.<init>(r0)     // Catch:{ all -> 0x0017 }
            throw r2     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r0 = move-exception
        L_0x0018:
            if (r1 == 0) goto L_0x001d
            r1.close()     // Catch:{ IOException -> 0x002c }
        L_0x001d:
            throw r0
        L_0x001e:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "The OutputStream must not be null"
            r0.<init>(r1)
            throw r0
        L_0x0026:
            r0 = move-exception
            goto L_0x0011
        L_0x0028:
            r0 = move-exception
            goto L_0x0018
        L_0x002a:
            r0 = move-exception
            goto L_0x000e
        L_0x002c:
            r1 = move-exception
            goto L_0x001d
        L_0x002e:
            r0 = move-exception
            r1 = r2
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.H68.b(java.io.Serializable, java.io.OutputStream):void");
    }

    @DexIgnore
    public static byte[] c(Serializable serializable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        b(serializable, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
