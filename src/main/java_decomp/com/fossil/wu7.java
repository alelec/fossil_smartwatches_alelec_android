package com.fossil;

import com.fossil.Dl7;
import com.mapped.Lk6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wu7 {
    @DexIgnore
    public static final <T> Object a(Object obj, Xe6<? super T> xe6) {
        if (obj instanceof Vu7) {
            Dl7.Ai ai = Dl7.Companion;
            Throwable th = ((Vu7) obj).a;
            if (Nv7.d() && (xe6 instanceof Do7)) {
                th = Uz7.a(th, (Do7) xe6);
            }
            return Dl7.constructor-impl(El7.a(th));
        }
        Dl7.Ai ai2 = Dl7.Companion;
        return Dl7.constructor-impl(obj);
    }

    @DexIgnore
    public static final <T> Object b(Object obj) {
        Throwable r0 = Dl7.exceptionOrNull-impl(obj);
        return r0 == null ? obj : new Vu7(r0, false, 2, null);
    }

    @DexIgnore
    public static final <T> Object c(Object obj, Lk6<?> lk6) {
        Throwable r0 = Dl7.exceptionOrNull-impl(obj);
        if (r0 == null) {
            return obj;
        }
        if (Nv7.d() && (lk6 instanceof Do7)) {
            r0 = Uz7.a(r0, (Do7) lk6);
        }
        return new Vu7(r0, false, 2, null);
    }
}
