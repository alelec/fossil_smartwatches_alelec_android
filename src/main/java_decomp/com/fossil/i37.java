package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.F57;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I37 {
    @DexIgnore
    public static /* final */ String a; // = "i37";
    @DexIgnore
    public static /* final */ Bitmap.Config b; // = Bitmap.Config.ARGB_8888;

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4, int i) {
        Bitmap m = m(bitmap, i);
        Bitmap m2 = m(bitmap2, i);
        Bitmap m3 = m(bitmap3, i);
        Bitmap m4 = m(bitmap4, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (m != null) {
            canvas.drawBitmap(m, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            m.recycle();
        }
        if (m2 != null) {
            canvas.drawBitmap(m2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i, (Paint) null);
            m2.recycle();
        }
        if (m3 != null) {
            canvas.drawBitmap(m3, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            m3.recycle();
        }
        if (m4 != null) {
            float f = (float) i;
            canvas.drawBitmap(m4, f, f, (Paint) null);
            m4.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap b(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, int i) {
        Bitmap l = l(bitmap, i);
        Bitmap m = m(bitmap2, i);
        Bitmap m2 = m(bitmap3, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (l != null) {
            canvas.drawBitmap(l, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            l.recycle();
        }
        if (m != null) {
            canvas.drawBitmap(m, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            m.recycle();
        }
        if (m2 != null) {
            float f = (float) i;
            canvas.drawBitmap(m2, f, f, (Paint) null);
            m2.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap c(Bitmap bitmap, Bitmap bitmap2, int i) {
        Bitmap l = l(bitmap, i);
        Bitmap l2 = l(bitmap2, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (l != null) {
            canvas.drawBitmap(l, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            l.recycle();
        }
        if (l2 != null) {
            canvas.drawBitmap(l2, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            l2.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap d(Resources resources, Drawable drawable) {
        Bitmap createBitmap;
        int i;
        int i2;
        if (drawable instanceof BitmapDrawable) {
            createBitmap = ((BitmapDrawable) drawable).getBitmap();
        } else {
            createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        }
        int dimensionPixelSize = resources.getDimensionPixelSize(2131165418);
        if (createBitmap.getHeight() <= dimensionPixelSize && createBitmap.getWidth() <= dimensionPixelSize) {
            return createBitmap;
        }
        int height = createBitmap.getHeight();
        int width = createBitmap.getWidth();
        if (height > width) {
            i2 = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
            i = dimensionPixelSize;
        } else {
            i = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
            i2 = dimensionPixelSize;
        }
        return Bitmap.createScaledBitmap(createBitmap, i2, i, false);
    }

    @DexIgnore
    public static Bitmap e(String str) {
        try {
            byte[] decode = Base64.decode(str, 0);
            return BitmapFactory.decodeByteArray(decode, 0, decode.length);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static String f(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        try {
            String str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 2), "UTF-8");
            try {
                byteArrayOutputStream.close();
                return str;
            } catch (Exception e) {
                e.printStackTrace();
                return str;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                byteArrayOutputStream.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            return "";
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap g(String str) {
        String d = ThemeManager.l.a().d("nonBrandPlaceholderText");
        int parseColor = !TextUtils.isEmpty(d) ? Color.parseColor(d) : PortfolioApp.d0.getResources().getColor(2131099827);
        Typeface f = ThemeManager.l.a().f("nonBrandTextStyle");
        String d2 = ThemeManager.l.a().d("nonBrandSurface");
        int parseColor2 = !TextUtils.isEmpty(d2) ? Color.parseColor(d2) : PortfolioApp.d0.getResources().getColor(2131100360);
        F57.Ci g = F57.a().g();
        g.c(50);
        g.h(50);
        g.d(25);
        g.i(parseColor2);
        g.j(f);
        return i(g.b().a(str, parseColor));
    }

    @DexIgnore
    public static Bitmap h(Contact contact) {
        String[] split = contact.getDisplayName().split(" ");
        String str = split[0];
        String str2 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str)) {
            str = str.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        String d = ThemeManager.l.a().d("primaryText");
        int parseColor = !TextUtils.isEmpty(d) ? Color.parseColor(d) : PortfolioApp.d0.getResources().getColor(2131099967);
        Typeface f = ThemeManager.l.a().f("nonBrandTextStyle");
        String d2 = ThemeManager.l.a().d("nonBrandSurface");
        int parseColor2 = !TextUtils.isEmpty(d2) ? Color.parseColor(d2) : PortfolioApp.d0.getResources().getColor(2131100360);
        F57.Ci g = F57.a().g();
        g.c(200);
        g.h(200);
        g.d(n(25));
        g.i(parseColor2);
        g.j(f);
        F57.Di b2 = g.b();
        return i(b2.a(str + str2, parseColor));
    }

    @DexIgnore
    public static Bitmap i(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        try {
            Bitmap createBitmap = drawable instanceof ColorDrawable ? Bitmap.createBitmap(2, 2, b) : Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), b);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return createBitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static Bitmap j(String str) {
        String[] split = str.split(" ");
        String str2 = split[0];
        String str3 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str3)) {
            str3 = str3.substring(0, 1);
        }
        int color = PortfolioApp.d0.getResources().getColor(2131099827);
        int color2 = PortfolioApp.d0.getResources().getColor(2131100360);
        F57.Ci g = F57.a().g();
        g.c(200);
        g.h(200);
        g.d(n(30));
        g.i(color2);
        F57.Di b2 = g.b();
        return i(b2.a(str2 + str3, color));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d A[SYNTHETIC, Splitter:B:18:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0072 A[SYNTHETIC, Splitter:B:28:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap k(java.lang.Long r7) {
        /*
            r6 = 1
            r1 = 0
            com.mapped.PermissionUtils$Ai r0 = com.mapped.PermissionUtils.a
            com.portfolio.platform.PortfolioApp r2 = com.portfolio.platform.PortfolioApp.d0
            r3 = 4
            java.lang.String[] r3 = new java.lang.String[r3]
            r4 = 0
            java.lang.String r5 = "android.permission.READ_CONTACTS"
            r3[r4] = r5
            java.lang.String r4 = "android.permission.READ_PHONE_STATE"
            r3[r6] = r4
            r4 = 2
            java.lang.String r5 = "android.permission.READ_CALL_LOG"
            r3[r4] = r5
            r4 = 3
            java.lang.String r5 = "android.permission.READ_SMS"
            r3[r4] = r5
            boolean r0 = r0.j(r2, r3)
            if (r0 != 0) goto L_0x0030
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.I37.a
            java.lang.String r3 = "getContactPhotoById does not enough permissions"
            r0.d(r2, r3)
        L_0x002f:
            return r1
        L_0x0030:
            com.portfolio.platform.PortfolioApp r0 = com.portfolio.platform.PortfolioApp.d0     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            android.net.Uri r2 = android.provider.ContactsContract.Contacts.CONTENT_URI     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            long r4 = r7.longValue()     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            android.net.Uri r2 = android.content.ContentUris.withAppendedId(r2, r4)     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            r3 = 1
            java.io.InputStream r0 = android.provider.ContactsContract.Contacts.openContactPhotoInputStream(r0, r2, r3)     // Catch:{ Exception -> 0x0076, all -> 0x006b }
            if (r0 == 0) goto L_0x0085
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0056, all -> 0x0083 }
            r0.close()     // Catch:{ Exception -> 0x0056, all -> 0x0083 }
            r3 = r1
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0054:
            r1 = r3
            goto L_0x002f
        L_0x0056:
            r2 = move-exception
            r3 = r1
        L_0x0058:
            r2.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r0 == 0) goto L_0x0054
            r0.close()     // Catch:{ IOException -> 0x0061 }
            goto L_0x0054
        L_0x0061:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0054
        L_0x0066:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0054
        L_0x006b:
            r0 = move-exception
            r2 = r0
            r3 = r1
        L_0x006e:
            r0 = r3
            r1 = r2
        L_0x0070:
            if (r0 == 0) goto L_0x0075
            r0.close()     // Catch:{ IOException -> 0x007a }
        L_0x0075:
            throw r1
        L_0x0076:
            r2 = move-exception
            r0 = r1
            r3 = r1
            goto L_0x0058
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0075
        L_0x007f:
            r1 = move-exception
            r2 = r1
            r3 = r0
            goto L_0x006e
        L_0x0083:
            r1 = move-exception
            goto L_0x0070
        L_0x0085:
            r3 = r1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.I37.k(java.lang.Long):android.graphics.Bitmap");
    }

    @DexIgnore
    public static Bitmap l(Bitmap bitmap, int i) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i2 = width * 2;
        Bitmap createBitmap = height > i2 ? Bitmap.createBitmap(bitmap, 0, (height / 2) - width, width, i2) : Bitmap.createBitmap(bitmap, (width / 2) - (height / 4), 0, height / 2, height);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(createBitmap, i, i * 2, false);
        createBitmap.recycle();
        Bitmap createBitmap2 = Bitmap.createBitmap(createScaledBitmap.getWidth() + 4, createScaledBitmap.getHeight() + 4, createScaledBitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap2);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(createScaledBitmap, 2.0f, 2.0f, (Paint) null);
        createScaledBitmap.recycle();
        return createBitmap2;
    }

    @DexIgnore
    public static Bitmap m(Bitmap bitmap, int i) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = width > height ? Bitmap.createBitmap(bitmap, (width / 2) - (height / 2), 0, height, height) : Bitmap.createBitmap(bitmap, 0, (height / 2) - (width / 2), width, width);
        Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap.getWidth() + 4, createBitmap.getHeight() + 4, createBitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap2);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(createBitmap, 2.0f, 2.0f, (Paint) null);
        createBitmap.recycle();
        return Bitmap.createScaledBitmap(createBitmap2, i, i, false);
    }

    @DexIgnore
    public static int n(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, PortfolioApp.d0.getResources().getDisplayMetrics());
    }
}
