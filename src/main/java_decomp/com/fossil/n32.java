package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.P32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class N32 implements P32.Ai {
    @DexIgnore
    public static /* final */ N32 a; // = new N32();

    @DexIgnore
    public static P32.Ai b() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.P32.Ai
    public void a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE events ADD COLUMN payload_encoding TEXT");
    }
}
