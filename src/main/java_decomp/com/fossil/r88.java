package com.fossil;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R88<T> {
    @DexIgnore
    public static <T> R88<T> b(Retrofit retrofit3, Method method) {
        P88 b = P88.b(retrofit3, method);
        Type genericReturnType = method.getGenericReturnType();
        if (U88.k(genericReturnType)) {
            throw U88.n(method, "Method return type must not include a type variable or wildcard: %s", genericReturnType);
        } else if (genericReturnType != Void.TYPE) {
            return H88.f(retrofit3, method, b);
        } else {
            throw U88.n(method, "Service methods cannot return void.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract T a(Object[] objArr);
}
