package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J91 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ List<F91> d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    public J91(int i, byte[] bArr, Map<String, String> map, List<F91> list, boolean z, long j) {
        this.a = i;
        this.b = bArr;
        this.c = map;
        if (list == null) {
            this.d = null;
        } else {
            this.d = Collections.unmodifiableList(list);
        }
        this.e = z;
    }

    @DexIgnore
    @Deprecated
    public J91(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        this(i, bArr, map, a(map), z, j);
    }

    @DexIgnore
    public J91(int i, byte[] bArr, boolean z, long j, List<F91> list) {
        this(i, bArr, b(list), list, z, j);
    }

    @DexIgnore
    @Deprecated
    public J91(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false, 0L);
    }

    @DexIgnore
    public static List<F91> a(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            arrayList.add(new F91(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }

    @DexIgnore
    public static Map<String, String> b(List<F91> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (F91 f91 : list) {
            treeMap.put(f91.a(), f91.b());
        }
        return treeMap;
    }
}
