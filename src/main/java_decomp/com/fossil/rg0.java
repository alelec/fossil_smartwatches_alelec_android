package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.Ig0;
import com.fossil.Jg0;
import com.fossil.Sn0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rg0 extends Xf0 implements Sn0.Ai {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ SparseBooleanArray C; // = new SparseBooleanArray();
    @DexIgnore
    public Ei D;
    @DexIgnore
    public Ai E;
    @DexIgnore
    public Ci F;
    @DexIgnore
    public Bi G;
    @DexIgnore
    public /* final */ Fi H; // = new Fi();
    @DexIgnore
    public int I;
    @DexIgnore
    public Di k;
    @DexIgnore
    public Drawable l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hg0 {
        @DexIgnore
        public Ai(Context context, Ng0 ng0, View view) {
            super(context, ng0, view, false, Le0.actionOverflowMenuStyle);
            if (!((Eg0) ng0.getItem()).l()) {
                View view2 = Rg0.this.k;
                f(view2 == null ? (View) Rg0.this.i : view2);
            }
            j(Rg0.this.H);
        }

        @DexIgnore
        @Override // com.fossil.Hg0
        public void e() {
            Rg0 rg0 = Rg0.this;
            rg0.E = null;
            rg0.I = 0;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends ActionMenuItemView.b {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // androidx.appcompat.view.menu.ActionMenuItemView.b
        public Lg0 a() {
            Ai ai = Rg0.this.E;
            if (ai != null) {
                return ai.c();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public Ei b;

        @DexIgnore
        public Ci(Ei ei) {
            this.b = ei;
        }

        @DexIgnore
        public void run() {
            if (Rg0.this.d != null) {
                Rg0.this.d.d();
            }
            View view = (View) Rg0.this.i;
            if (!(view == null || view.getWindowToken() == null || !this.b.m())) {
                Rg0.this.D = this.b;
            }
            Rg0.this.F = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends AppCompatImageView implements ActionMenuView.a {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Gh0 {
            @DexIgnore
            public Aii(View view, Rg0 rg0) {
                super(view);
            }

            @DexIgnore
            @Override // com.fossil.Gh0
            public Lg0 b() {
                Ei ei = Rg0.this.D;
                if (ei == null) {
                    return null;
                }
                return ei.c();
            }

            @DexIgnore
            @Override // com.fossil.Gh0
            public boolean c() {
                Rg0.this.N();
                return true;
            }

            @DexIgnore
            @Override // com.fossil.Gh0
            public boolean d() {
                Rg0 rg0 = Rg0.this;
                if (rg0.F != null) {
                    return false;
                }
                rg0.E();
                return true;
            }
        }

        @DexIgnore
        public Di(Context context) {
            super(context, null, Le0.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            Vh0.a(this, getContentDescription());
            setOnTouchListener(new Aii(this, Rg0.this));
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean c() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                Rg0.this.N();
            }
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                Am0.l(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Hg0 {
        @DexIgnore
        public Ei(Context context, Cg0 cg0, View view, boolean z) {
            super(context, cg0, view, z, Le0.actionOverflowMenuStyle);
            h(8388613);
            j(Rg0.this.H);
        }

        @DexIgnore
        @Override // com.fossil.Hg0
        public void e() {
            if (Rg0.this.d != null) {
                Rg0.this.d.close();
            }
            Rg0.this.D = null;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements Ig0.Ai {
        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        @Override // com.fossil.Ig0.Ai
        public void b(Cg0 cg0, boolean z) {
            if (cg0 instanceof Ng0) {
                cg0.F().e(false);
            }
            Ig0.Ai p = Rg0.this.p();
            if (p != null) {
                p.b(cg0, z);
            }
        }

        @DexIgnore
        @Override // com.fossil.Ig0.Ai
        public boolean c(Cg0 cg0) {
            if (cg0 == Rg0.this.d) {
                return false;
            }
            Rg0.this.I = ((Ng0) cg0).getItem().getItemId();
            Ig0.Ai p = Rg0.this.p();
            return p != null ? p.c(cg0) : false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"BanParcelableUsage"})
    public static class Gi implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<Gi> CREATOR; // = new Aii();
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Parcelable.Creator<Gi> {
            @DexIgnore
            public Gi a(Parcel parcel) {
                return new Gi(parcel);
            }

            @DexIgnore
            public Gi[] b(int i) {
                return new Gi[i];
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Gi createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Gi[] newArray(int i) {
                return b(i);
            }
        }

        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        public Gi(Parcel parcel) {
            this.b = parcel.readInt();
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    public Rg0(Context context) {
        super(context, Re0.abc_action_menu_layout, Re0.abc_action_menu_item_layout);
    }

    @DexIgnore
    public boolean B() {
        return E() | F();
    }

    @DexIgnore
    public final View C(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.i;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof Jg0.Ai) && ((Jg0.Ai) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public Drawable D() {
        Di di = this.k;
        if (di != null) {
            return di.getDrawable();
        }
        if (this.m) {
            return this.l;
        }
        return null;
    }

    @DexIgnore
    public boolean E() {
        Jg0 jg0;
        Ci ci = this.F;
        if (ci == null || (jg0 = this.i) == null) {
            Ei ei = this.D;
            if (ei == null) {
                return false;
            }
            ei.b();
            return true;
        }
        ((View) jg0).removeCallbacks(ci);
        this.F = null;
        return true;
    }

    @DexIgnore
    public boolean F() {
        Ai ai = this.E;
        if (ai == null) {
            return false;
        }
        ai.b();
        return true;
    }

    @DexIgnore
    public boolean G() {
        return this.F != null || H();
    }

    @DexIgnore
    public boolean H() {
        Ei ei = this.D;
        return ei != null && ei.d();
    }

    @DexIgnore
    public void I(Configuration configuration) {
        if (!this.x) {
            this.w = Of0.b(this.c).d();
        }
        Cg0 cg0 = this.d;
        if (cg0 != null) {
            cg0.M(true);
        }
    }

    @DexIgnore
    public void J(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    public void K(ActionMenuView actionMenuView) {
        this.i = actionMenuView;
        actionMenuView.b(this.d);
    }

    @DexIgnore
    public void L(Drawable drawable) {
        Di di = this.k;
        if (di != null) {
            di.setImageDrawable(drawable);
            return;
        }
        this.m = true;
        this.l = drawable;
    }

    @DexIgnore
    public void M(boolean z2) {
        this.s = z2;
        this.t = true;
    }

    @DexIgnore
    public boolean N() {
        Cg0 cg0;
        if (!this.s || H() || (cg0 = this.d) == null || this.i == null || this.F != null || cg0.B().isEmpty()) {
            return false;
        }
        Ci ci = new Ci(new Ei(this.c, this.d, this.k, true));
        this.F = ci;
        ((View) this.i).post(ci);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Sn0.Ai
    public void a(boolean z2) {
        if (z2) {
            super.k(null);
            return;
        }
        Cg0 cg0 = this.d;
        if (cg0 != null) {
            cg0.e(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.Xf0, com.fossil.Ig0
    public void b(Cg0 cg0, boolean z2) {
        B();
        super.b(cg0, z2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008d  */
    @Override // com.fossil.Xf0, com.fossil.Ig0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(boolean r7) {
        /*
            r6 = this;
            r1 = 1
            r2 = 0
            super.c(r7)
            com.fossil.Jg0 r0 = r6.i
            android.view.View r0 = (android.view.View) r0
            r0.requestLayout()
            com.fossil.Cg0 r0 = r6.d
            if (r0 == 0) goto L_0x002e
            java.util.ArrayList r4 = r0.u()
            int r5 = r4.size()
            r3 = r2
        L_0x0019:
            if (r3 >= r5) goto L_0x002e
            java.lang.Object r0 = r4.get(r3)
            com.fossil.Eg0 r0 = (com.fossil.Eg0) r0
            com.fossil.Sn0 r0 = r0.b()
            if (r0 == 0) goto L_0x002a
            r0.setSubUiVisibilityListener(r6)
        L_0x002a:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0019
        L_0x002e:
            com.fossil.Cg0 r0 = r6.d
            if (r0 == 0) goto L_0x0087
            java.util.ArrayList r0 = r0.B()
        L_0x0036:
            boolean r3 = r6.s
            if (r3 == 0) goto L_0x00a1
            if (r0 == 0) goto L_0x00a1
            int r3 = r0.size()
            if (r3 != r1) goto L_0x0089
            java.lang.Object r0 = r0.get(r2)
            com.fossil.Eg0 r0 = (com.fossil.Eg0) r0
            boolean r0 = r0.isActionViewExpanded()
            r0 = r0 ^ 1
        L_0x004e:
            if (r0 == 0) goto L_0x008d
            com.fossil.Rg0$Di r0 = r6.k
            if (r0 != 0) goto L_0x005d
            com.fossil.Rg0$Di r0 = new com.fossil.Rg0$Di
            android.content.Context r1 = r6.b
            r0.<init>(r1)
            r6.k = r0
        L_0x005d:
            com.fossil.Rg0$Di r0 = r6.k
            android.view.ViewParent r0 = r0.getParent()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            com.fossil.Jg0 r1 = r6.i
            if (r0 == r1) goto L_0x007d
            if (r0 == 0) goto L_0x0070
            com.fossil.Rg0$Di r1 = r6.k
            r0.removeView(r1)
        L_0x0070:
            com.fossil.Jg0 r0 = r6.i
            androidx.appcompat.widget.ActionMenuView r0 = (androidx.appcompat.widget.ActionMenuView) r0
            com.fossil.Rg0$Di r1 = r6.k
            androidx.appcompat.widget.ActionMenuView$LayoutParams r2 = r0.i()
            r0.addView(r1, r2)
        L_0x007d:
            com.fossil.Jg0 r0 = r6.i
            androidx.appcompat.widget.ActionMenuView r0 = (androidx.appcompat.widget.ActionMenuView) r0
            boolean r1 = r6.s
            r0.setOverflowReserved(r1)
            return
        L_0x0087:
            r0 = 0
            goto L_0x0036
        L_0x0089:
            if (r3 <= 0) goto L_0x00a1
            r0 = r1
            goto L_0x004e
        L_0x008d:
            com.fossil.Rg0$Di r0 = r6.k
            if (r0 == 0) goto L_0x007d
            android.view.ViewParent r1 = r0.getParent()
            com.fossil.Jg0 r0 = r6.i
            if (r1 != r0) goto L_0x007d
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            com.fossil.Rg0$Di r1 = r6.k
            r0.removeView(r1)
            goto L_0x007d
        L_0x00a1:
            r0 = r2
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Rg0.c(boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        int i;
        ArrayList<Eg0> arrayList;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z2;
        int i6;
        int i7;
        int i8;
        int i9;
        Cg0 cg0 = this.d;
        if (cg0 != null) {
            ArrayList<Eg0> G2 = cg0.G();
            i = G2.size();
            arrayList = G2;
        } else {
            i = 0;
            arrayList = null;
        }
        int i10 = this.w;
        int i11 = this.v;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.i;
        boolean z3 = false;
        int i12 = 0;
        int i13 = 0;
        int i14 = 0;
        while (i14 < i) {
            Eg0 eg0 = arrayList.get(i14);
            if (eg0.o()) {
                i12++;
            } else if (eg0.n()) {
                i13++;
            } else {
                z3 = true;
            }
            i14++;
            i10 = (!this.A || !eg0.isActionViewExpanded()) ? i10 : 0;
        }
        if (this.s && (z3 || i13 + i12 > i10)) {
            i10--;
        }
        int i15 = i10 - i12;
        SparseBooleanArray sparseBooleanArray = this.C;
        sparseBooleanArray.clear();
        if (this.y) {
            int i16 = this.B;
            i2 = i11 / i16;
            i3 = i16 + ((i11 % i16) / i2);
        } else {
            i2 = 0;
            i3 = 0;
        }
        int i17 = 0;
        int i18 = 0;
        while (i18 < i) {
            Eg0 eg02 = arrayList.get(i18);
            if (eg02.o()) {
                View q = q(eg02, null, viewGroup);
                if (this.y) {
                    i2 -= ActionMenuView.o(q, i3, i2, makeMeasureSpec, 0);
                } else {
                    q.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i5 = q.getMeasuredWidth();
                int i19 = i11 - i5;
                if (i17 != 0) {
                    i5 = i17;
                }
                int groupId = eg02.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                eg02.u(true);
                i4 = i19;
                i9 = i2;
            } else if (eg02.n()) {
                int groupId2 = eg02.getGroupId();
                boolean z4 = sparseBooleanArray.get(groupId2);
                boolean z5 = (i15 > 0 || z4) && i11 > 0 && (!this.y || i2 > 0);
                if (z5) {
                    View q2 = q(eg02, null, viewGroup);
                    if (this.y) {
                        int o = ActionMenuView.o(q2, i3, i2, makeMeasureSpec, 0);
                        i6 = i2 - o;
                        if (o == 0) {
                            z5 = false;
                        }
                    } else {
                        q2.measure(makeMeasureSpec, makeMeasureSpec);
                        i6 = i2;
                    }
                    int measuredWidth = q2.getMeasuredWidth();
                    i11 -= measuredWidth;
                    if (i17 == 0) {
                        i17 = measuredWidth;
                    }
                    z2 = z5 & (!this.y ? i11 + i17 > 0 : i11 >= 0);
                    i7 = i17;
                } else {
                    z2 = z5;
                    i6 = i2;
                    i7 = i17;
                }
                if (z2 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i8 = i15;
                } else if (z4) {
                    sparseBooleanArray.put(groupId2, false);
                    int i20 = i15;
                    for (int i21 = 0; i21 < i18; i21++) {
                        Eg0 eg03 = arrayList.get(i21);
                        if (eg03.getGroupId() == groupId2) {
                            if (eg03.l()) {
                                i20++;
                            }
                            eg03.u(false);
                        }
                    }
                    i8 = i20;
                } else {
                    i8 = i15;
                }
                if (z2) {
                    i8--;
                }
                eg02.u(z2);
                i4 = i11;
                i9 = i6;
                i15 = i8;
                i5 = i7;
            } else {
                eg02.u(false);
                i4 = i11;
                i5 = i17;
                i18++;
                i11 = i4;
                i17 = i5;
            }
            i2 = i9;
            i18++;
            i11 = i4;
            i17 = i5;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Xf0, com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
        super.h(context, cg0);
        Resources resources = context.getResources();
        Of0 b = Of0.b(context);
        if (!this.t) {
            this.s = b.h();
        }
        if (!this.z) {
            this.u = b.c();
        }
        if (!this.x) {
            this.w = b.d();
        }
        int i = this.u;
        if (this.s) {
            if (this.k == null) {
                Di di = new Di(this.b);
                this.k = di;
                if (this.m) {
                    di.setImageDrawable(this.l);
                    this.l = null;
                    this.m = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.k.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.k.getMeasuredWidth();
        } else {
            this.k = null;
        }
        this.v = i;
        this.B = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof Gi) && (i = ((Gi) parcelable).b) > 0 && (findItem = this.d.findItem(i)) != null) {
            k((Ng0) findItem.getSubMenu());
        }
    }

    @DexIgnore
    @Override // com.fossil.Xf0, com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        boolean z2;
        if (!ng0.hasVisibleItems()) {
            return false;
        }
        Ng0 ng02 = ng0;
        while (ng02.i0() != this.d) {
            ng02 = (Ng0) ng02.i0();
        }
        View C2 = C(ng02.getItem());
        if (C2 == null) {
            return false;
        }
        this.I = ng0.getItem().getItemId();
        int size = ng0.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                z2 = false;
                break;
            }
            MenuItem item = ng0.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        Ai ai = new Ai(this.c, ng0, C2);
        this.E = ai;
        ai.g(z2);
        this.E.k();
        super.k(ng0);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        Gi gi = new Gi();
        gi.b = this.I;
        return gi;
    }

    @DexIgnore
    @Override // com.fossil.Xf0
    public void m(Eg0 eg0, Jg0.Ai ai) {
        ai.f(eg0, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) ai;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.i);
        if (this.G == null) {
            this.G = new Bi();
        }
        actionMenuItemView.setPopupCallback(this.G);
    }

    @DexIgnore
    @Override // com.fossil.Xf0
    public boolean o(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.k) {
            return false;
        }
        return super.o(viewGroup, i);
    }

    @DexIgnore
    @Override // com.fossil.Xf0
    public View q(Eg0 eg0, View view, ViewGroup viewGroup) {
        View actionView = eg0.getActionView();
        if (actionView == null || eg0.j()) {
            actionView = super.q(eg0, view, viewGroup);
        }
        actionView.setVisibility(eg0.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.h(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    @Override // com.fossil.Xf0
    public Jg0 r(ViewGroup viewGroup) {
        Jg0 jg0 = this.i;
        Jg0 r = super.r(viewGroup);
        if (jg0 != r) {
            ((ActionMenuView) r).setPresenter(this);
        }
        return r;
    }

    @DexIgnore
    @Override // com.fossil.Xf0
    public boolean t(int i, Eg0 eg0) {
        return eg0.l();
    }
}
