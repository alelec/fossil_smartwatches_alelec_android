package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ga5 extends Fa5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362116, 1);
        Q.put(2131361851, 2);
        Q.put(2131363410, 3);
        Q.put(2131362579, 4);
        Q.put(2131362345, 5);
        Q.put(2131363220, 6);
        Q.put(2131363206, 7);
        Q.put(2131363210, 8);
        Q.put(2131363555, 9);
        Q.put(2131362356, 10);
        Q.put(2131363224, 11);
        Q.put(2131363208, 12);
        Q.put(2131363212, 13);
        Q.put(2131362211, 14);
        Q.put(2131362344, 15);
        Q.put(2131363219, 16);
        Q.put(2131363205, 17);
        Q.put(2131363209, 18);
        Q.put(2131363185, 19);
        Q.put(2131362352, 20);
        Q.put(2131363223, 21);
        Q.put(2131363207, 22);
        Q.put(2131363211, 23);
    }
    */

    @DexIgnore
    public Ga5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public Ga5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[2], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[14], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[20], (FlexibleTextView) objArr[10], (ConstraintLayout) objArr[4], (RelativeLayout) objArr[0], (ConstraintLayout) objArr[19], (TabItem) objArr[17], (TabItem) objArr[7], (TabItem) objArr[22], (TabItem) objArr[12], (TabItem) objArr[18], (TabItem) objArr[8], (TabItem) objArr[23], (TabItem) objArr[13], (TabLayout) objArr[16], (TabLayout) objArr[6], (TabLayout) objArr[21], (TabLayout) objArr[11], (FlexibleTextView) objArr[3], (ConstraintLayout) objArr[9]);
        this.O = -1;
        this.y.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
