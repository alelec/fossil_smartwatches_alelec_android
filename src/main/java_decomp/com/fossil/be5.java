package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Be5 extends Ae5 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.d z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362366, 1);
        A.put(2131362081, 2);
        A.put(2131361814, 3);
        A.put(2131362830, 4);
        A.put(2131362931, 5);
        A.put(2131362930, 6);
        A.put(2131363466, 7);
    }
    */

    @DexIgnore
    public Be5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 8, z, A));
    }

    @DexIgnore
    public Be5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleCheckBox) objArr[3], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[1], (LinearLayout) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (View) objArr[7]);
        this.y = -1;
        this.s.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.y != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.y = 1;
        }
        w();
    }
}
