package com.fossil;

import com.fossil.Xd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Xd4<T extends Xd4<T>> {
    @DexIgnore
    <U> T a(Class<U> cls, Sd4<? super U> sd4);
}
