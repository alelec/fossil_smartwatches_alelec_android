package com.fossil;

import android.bluetooth.BluetoothDevice;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X4 {
    @DexIgnore
    public static /* final */ Hashtable<String, K5> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ X4 b; // = new X4();

    @DexIgnore
    public final K5 a(BluetoothDevice bluetoothDevice) {
        K5 k5;
        M80.c.a("Peripheral.Factory", "getPeripheral: bluetoothDevice=%s.", bluetoothDevice.getAddress());
        synchronized (a) {
            k5 = a.get(bluetoothDevice.getAddress());
            if (k5 == null) {
                k5 = new K5(bluetoothDevice, null);
                a.put(k5.A.getAddress(), k5);
            }
        }
        return k5;
    }
}
