package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.Tu3;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zu3 extends Service implements Su3 {
    @DexIgnore
    public ComponentName b;
    @DexIgnore
    public Ci c;
    @DexIgnore
    public IBinder d;
    @DexIgnore
    public Intent e;
    @DexIgnore
    public Looper f;
    @DexIgnore
    public /* final */ Object g; // = new Object();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Dv3 i; // = new Dv3(new Ai());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends Tu3.Bi {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Tu3.Bi
        public final void a(Tu3.Ai ai, int i, int i2) {
            Zu3.this.g(ai, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.Tu3.Bi
        public final void b(Tu3.Ai ai) {
            Zu3.this.h(ai);
        }

        @DexIgnore
        @Override // com.fossil.Tu3.Bi
        public final void c(Tu3.Ai ai, int i, int i2) {
            Zu3.this.l(ai, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.Tu3.Bi
        public final void d(Tu3.Ai ai, int i, int i2) {
            Zu3.this.o(ai, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements ServiceConnection {
        @DexIgnore
        public Bi(Zu3 zu3) {
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends Handler {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ Bi b; // = new Bi();

        @DexIgnore
        public Ci(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void a() {
            getLooper().quit();
            c("quit");
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final void b() {
            synchronized (this) {
                if (!this.a) {
                    if (Log.isLoggable("WearableLS", 2)) {
                        String valueOf = String.valueOf(Zu3.this.b);
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
                        sb.append("bindService: ");
                        sb.append(valueOf);
                        Log.v("WearableLS", sb.toString());
                    }
                    Zu3.this.bindService(Zu3.this.e, this.b, 1);
                    this.a = true;
                }
            }
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final void c(String str) {
            synchronized (this) {
                if (this.a) {
                    if (Log.isLoggable("WearableLS", 2)) {
                        String valueOf = String.valueOf(Zu3.this.b);
                        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 17 + String.valueOf(valueOf).length());
                        sb.append("unbindService: ");
                        sb.append(str);
                        sb.append(", ");
                        sb.append(valueOf);
                        Log.v("WearableLS", sb.toString());
                    }
                    try {
                        Zu3.this.unbindService(this.b);
                    } catch (RuntimeException e) {
                        Log.e("WearableLS", "Exception when unbinding from local service", e);
                    }
                    this.a = false;
                }
            }
        }

        @DexIgnore
        public final void dispatchMessage(Message message) {
            b();
            try {
                super.dispatchMessage(message);
            } finally {
                if (!hasMessages(0)) {
                    c("dispatch");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends Mv3 {
        @DexIgnore
        public volatile int b;

        @DexIgnore
        public Di() {
            this.b = -1;
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void L0(Sv3 sv3) {
            e(new Gw3(this, sv3), "onEntityUpdate", sv3);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void O(DataHolder dataHolder) {
            Zv3 zv3 = new Zv3(this, dataHolder);
            try {
                String valueOf = String.valueOf(dataHolder);
                int count = dataHolder.getCount();
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append(valueOf);
                sb.append(", rows=");
                sb.append(count);
                if (e(zv3, "onDataItemChanged", sb.toString())) {
                }
            } finally {
                dataHolder.close();
            }
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void T1(Pv3 pv3) {
            e(new Cw3(this, pv3), "onPeerDisconnected", pv3);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void U2(List<Pv3> list) {
            e(new Dw3(this, list), "onConnectedNodes", list);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void a1(Nv3 nv3) {
            e(new Aw3(this, nv3), "onMessageReceived", nv3);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0075  */
        /* JADX WARNING: Removed duplicated region for block: B:29:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean e(java.lang.Runnable r7, java.lang.String r8, java.lang.Object r9) {
            /*
                r6 = this;
                r4 = 3
                r1 = 1
                r0 = 0
                java.lang.String r2 = "WearableLS"
                boolean r2 = android.util.Log.isLoggable(r2, r4)
                if (r2 == 0) goto L_0x0029
                java.lang.String r2 = "WearableLS"
                java.lang.String r3 = "%s: %s %s"
                java.lang.Object[] r4 = new java.lang.Object[r4]
                r4[r0] = r8
                com.fossil.Zu3 r5 = com.fossil.Zu3.this
                android.content.ComponentName r5 = com.fossil.Zu3.r(r5)
                java.lang.String r5 = r5.toString()
                r4[r1] = r5
                r5 = 2
                r4[r5] = r9
                java.lang.String r3 = java.lang.String.format(r3, r4)
                android.util.Log.d(r2, r3)
            L_0x0029:
                int r2 = android.os.Binder.getCallingUid()
                int r3 = r6.b
                if (r2 != r3) goto L_0x0035
            L_0x0031:
                r2 = r1
            L_0x0032:
                if (r2 != 0) goto L_0x0075
            L_0x0034:
                return r0
            L_0x0035:
                com.fossil.Zu3 r3 = com.fossil.Zu3.this
                com.fossil.Rv3 r3 = com.fossil.Rv3.a(r3)
                java.lang.String r4 = "com.google.android.wearable.app.cn"
                boolean r3 = r3.e(r4)
                if (r3 == 0) goto L_0x0050
                com.fossil.Zu3 r3 = com.fossil.Zu3.this
                java.lang.String r4 = "com.google.android.wearable.app.cn"
                boolean r3 = com.fossil.Pf2.b(r3, r2, r4)
                if (r3 == 0) goto L_0x0050
                r6.b = r2
                goto L_0x0031
            L_0x0050:
                com.fossil.Zu3 r3 = com.fossil.Zu3.this
                boolean r3 = com.fossil.Pf2.a(r3, r2)
                if (r3 == 0) goto L_0x005b
                r6.b = r2
                goto L_0x0031
            L_0x005b:
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r4 = 57
                r3.<init>(r4)
                java.lang.String r4 = "Caller is not GooglePlayServices; caller UID: "
                r3.append(r4)
                r3.append(r2)
                java.lang.String r2 = "WearableLS"
                java.lang.String r3 = r3.toString()
                android.util.Log.e(r2, r3)
                r2 = r0
                goto L_0x0032
            L_0x0075:
                com.fossil.Zu3 r2 = com.fossil.Zu3.this
                java.lang.Object r2 = com.fossil.Zu3.u(r2)
                monitor-enter(r2)
                com.fossil.Zu3 r3 = com.fossil.Zu3.this     // Catch:{ all -> 0x0086 }
                boolean r3 = com.fossil.Zu3.v(r3)     // Catch:{ all -> 0x0086 }
                if (r3 == 0) goto L_0x0089
                monitor-exit(r2)     // Catch:{ all -> 0x0086 }
                goto L_0x0034
            L_0x0086:
                r0 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0086 }
                throw r0
            L_0x0089:
                com.fossil.Zu3 r0 = com.fossil.Zu3.this
                com.fossil.Zu3$Ci r0 = com.fossil.Zu3.w(r0)
                r0.post(r7)
                monitor-exit(r2)
                r0 = r1
                goto L_0x0034
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Zu3.Di.e(java.lang.Runnable, java.lang.String, java.lang.Object):boolean");
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void i0(Pv3 pv3) {
            e(new Bw3(this, pv3), "onPeerConnected", pv3);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void k2(Uv3 uv3) {
            e(new Fw3(this, uv3), "onNotificationReceived", uv3);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void r(Ev3 ev3) {
            e(new Hw3(this, ev3), "onChannelEvent", ev3);
        }

        @DexIgnore
        @Override // com.fossil.Lv3
        public final void t0(Av3 av3) {
            e(new Ew3(this, av3), "onConnectedCapabilityChanged", av3);
        }
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public void a(Ru3 ru3) {
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public void b(Ru3 ru3, int i2, int i3) {
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public void c(Ru3 ru3, int i2, int i3) {
    }

    @DexIgnore
    @Override // com.fossil.Su3
    public void d(Ru3 ru3, int i2, int i3) {
    }

    @DexIgnore
    public Looper e() {
        if (this.f == null) {
            HandlerThread handlerThread = new HandlerThread("WearableListenerService");
            handlerThread.start();
            this.f = handlerThread.getLooper();
        }
        return this.f;
    }

    @DexIgnore
    public void f(Qu3 qu3) {
    }

    @DexIgnore
    public void g(Tu3.Ai ai, int i2, int i3) {
    }

    @DexIgnore
    public void h(Tu3.Ai ai) {
    }

    @DexIgnore
    public void i(List<Yu3> list) {
    }

    @DexIgnore
    public void j(Uu3 uu3) {
    }

    @DexIgnore
    public void k(Wv3 wv3) {
    }

    @DexIgnore
    public void l(Tu3.Ai ai, int i2, int i3) {
    }

    @DexIgnore
    public void m(Xu3 xu3) {
    }

    @DexIgnore
    public void n(Xv3 xv3) {
    }

    @DexIgnore
    public void o(Tu3.Ai ai, int i2, int i3) {
    }

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        if ("com.google.android.gms.wearable.BIND_LISTENER".equals(intent.getAction())) {
            return this.d;
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.b = new ComponentName(this, Zu3.class.getName());
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.b);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 10);
            sb.append("onCreate: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        this.c = new Ci(e());
        Intent intent = new Intent("com.google.android.gms.wearable.BIND_LISTENER");
        this.e = intent;
        intent.setComponent(this.b);
        this.d = new Di();
    }

    @DexIgnore
    public void onDestroy() {
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.b);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 11);
            sb.append("onDestroy: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        synchronized (this.g) {
            this.h = true;
            if (this.c != null) {
                this.c.a();
            } else {
                String valueOf2 = String.valueOf(this.b);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 111);
                sb2.append("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
        super.onDestroy();
    }

    @DexIgnore
    public void p(Yu3 yu3) {
    }

    @DexIgnore
    public void q(Yu3 yu3) {
    }
}
