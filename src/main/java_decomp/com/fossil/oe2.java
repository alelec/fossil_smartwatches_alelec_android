package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oe2 extends Rl2 implements Ne2 {
    @DexIgnore
    public Oe2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    @Override // com.fossil.Ne2
    public final boolean y2(Kg2 kg2, Rg2 rg2) throws RemoteException {
        Parcel d = d();
        Sl2.d(d, kg2);
        Sl2.c(d, rg2);
        Parcel e = e(5, d);
        boolean e2 = Sl2.e(e);
        e.recycle();
        return e2;
    }
}
