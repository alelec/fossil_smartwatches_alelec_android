package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yp4 implements Factory<Pl5> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Yp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Yp4 a(Uo4 uo4) {
        return new Yp4(uo4);
    }

    @DexIgnore
    public static Pl5 c(Uo4 uo4) {
        Pl5 F = uo4.F();
        Lk7.c(F, "Cannot return null from a non-@Nullable @Provides method");
        return F;
    }

    @DexIgnore
    public Pl5 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
