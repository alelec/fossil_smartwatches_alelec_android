package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pq2 extends Zc2 implements Z62 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Pq2> CREATOR; // = new Qq2();
    @DexIgnore
    public /* final */ Status b;

    /*
    static {
        Status status = Status.f;
    }
    */

    @DexIgnore
    public Pq2(Status status) {
        this.b = status;
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public final Status a() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.t(parcel, 1, a(), i, false);
        Bd2.b(parcel, a2);
    }
}
