package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ny6 implements Factory<PairingPresenter> {
    @DexIgnore
    public static PairingPresenter a(Iy6 iy6, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, Cj4 cj4, MusicControlComponent musicControlComponent, SetNotificationUseCase setNotificationUseCase, An4 an4) {
        return new PairingPresenter(iy6, linkDeviceUseCase, deviceRepository, cj4, musicControlComponent, setNotificationUseCase, an4);
    }
}
