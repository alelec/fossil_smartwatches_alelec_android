package com.fossil;

import com.fossil.Kk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hd1<Z> implements Id1<Z>, Kk1.Fi {
    @DexIgnore
    public static /* final */ Mn0<Hd1<?>> f; // = Kk1.d(20, new Ai());
    @DexIgnore
    public /* final */ Mk1 b; // = Mk1.a();
    @DexIgnore
    public Id1<Z> c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Kk1.Di<Hd1<?>> {
        @DexIgnore
        public Hd1<?> a() {
            return new Hd1<>();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Kk1.Di
        public /* bridge */ /* synthetic */ Hd1<?> create() {
            return a();
        }
    }

    @DexIgnore
    public static <Z> Hd1<Z> e(Id1<Z> id1) {
        Hd1<?> b2 = f.b();
        Ik1.d(b2);
        Hd1<Z> hd1 = (Hd1<Z>) b2;
        hd1.a(id1);
        return hd1;
    }

    @DexIgnore
    public final void a(Id1<Z> id1) {
        this.e = false;
        this.d = true;
        this.c = id1;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
        synchronized (this) {
            this.b.c();
            this.e = true;
            if (!this.d) {
                this.c.b();
                g();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return this.c.c();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<Z> d() {
        return this.c.d();
    }

    @DexIgnore
    @Override // com.fossil.Kk1.Fi
    public Mk1 f() {
        return this.b;
    }

    @DexIgnore
    public final void g() {
        this.c = null;
        f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Z get() {
        return this.c.get();
    }

    @DexIgnore
    public void h() {
        synchronized (this) {
            this.b.c();
            if (this.d) {
                this.d = false;
                if (this.e) {
                    b();
                }
            } else {
                throw new IllegalStateException("Already unlocked");
            }
        }
    }
}
