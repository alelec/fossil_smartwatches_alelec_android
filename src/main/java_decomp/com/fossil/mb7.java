package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mb7 implements Gb7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public float c;
    @DexIgnore
    public Nb7 d;
    @DexIgnore
    public Jb7 e;
    @DexIgnore
    public Lb7 f;
    @DexIgnore
    public int g;

    @DexIgnore
    public Mb7(String str, String str2, float f2, Nb7 nb7, Jb7 jb7, Lb7 lb7, int i) {
        Wg6.c(str, "text");
        Wg6.c(str2, "fontType");
        Wg6.c(nb7, "colour");
        Wg6.c(jb7, "metric");
        Wg6.c(lb7, "type");
        this.a = str;
        this.b = str2;
        this.c = f2;
        this.d = nb7;
        this.e = jb7;
        this.f = lb7;
        this.g = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Mb7(String str, String str2, float f2, Nb7 nb7, Jb7 jb7, Lb7 lb7, int i, int i2, Qg6 qg6) {
        this(str, str2, f2, (i2 & 8) != 0 ? Nb7.DEFAULT : nb7, jb7, (i2 & 32) != 0 ? Lb7.TEXT : lb7, i);
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public int a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Jb7 b() {
        return this.e;
    }

    @DexIgnore
    public final Nb7 c() {
        return this.d;
    }

    @DexIgnore
    public final float d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Mb7) {
                Mb7 mb7 = (Mb7) obj;
                if (!Wg6.a(this.a, mb7.a) || !Wg6.a(this.b, mb7.b) || Float.compare(this.c, mb7.c) != 0 || !Wg6.a(this.d, mb7.d) || !Wg6.a(b(), mb7.b()) || !Wg6.a(getType(), mb7.getType()) || a() != mb7.a()) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Lb7 getType() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        int floatToIntBits = Float.floatToIntBits(this.c);
        Nb7 nb7 = this.d;
        int hashCode3 = nb7 != null ? nb7.hashCode() : 0;
        Jb7 b2 = b();
        int hashCode4 = b2 != null ? b2.hashCode() : 0;
        Lb7 type = getType();
        if (type != null) {
            i = type.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + floatToIntBits) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + a();
    }

    @DexIgnore
    public String toString() {
        return "WFTextData(text=" + this.a + ", fontType=" + this.b + ", fontScaled=" + this.c + ", colour=" + this.d + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
