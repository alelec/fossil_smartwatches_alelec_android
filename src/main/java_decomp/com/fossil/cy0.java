package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.transition.Transition;
import androidx.transition.TransitionSet;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedApi"})
public class Cy0 extends Ar0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Transition.e {
        @DexIgnore
        public /* final */ /* synthetic */ Rect a;

        @DexIgnore
        public Ai(Cy0 cy0, Rect rect) {
            this.a = rect;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Transition.f {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public Bi(Cy0 cy0, View view, ArrayList arrayList) {
            this.a = view;
            this.b = arrayList;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void a(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void b(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void c(Transition transition) {
            transition.d0(this);
            this.a.setVisibility(8);
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                ((View) this.b.get(i)).setVisibility(0);
            }
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void d(Transition transition) {
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void e(Transition transition) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Sy0 {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;

        @DexIgnore
        public Ci(Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3) {
            this.a = obj;
            this.b = arrayList;
            this.c = obj2;
            this.d = arrayList2;
            this.e = obj3;
            this.f = arrayList3;
        }

        @DexIgnore
        @Override // com.fossil.Sy0, androidx.transition.Transition.f
        public void a(Transition transition) {
            Object obj = this.a;
            if (obj != null) {
                Cy0.this.q(obj, this.b, null);
            }
            Object obj2 = this.c;
            if (obj2 != null) {
                Cy0.this.q(obj2, this.d, null);
            }
            Object obj3 = this.e;
            if (obj3 != null) {
                Cy0.this.q(obj3, this.f, null);
            }
        }

        @DexIgnore
        @Override // androidx.transition.Transition.f
        public void c(Transition transition) {
            transition.d0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Transition.e {
        @DexIgnore
        public /* final */ /* synthetic */ Rect a;

        @DexIgnore
        public Di(Cy0 cy0, Rect rect) {
            this.a = rect;
        }

        @DexIgnore
        @Override // androidx.transition.Transition.e
        public Rect a(Transition transition) {
            Rect rect = this.a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.a;
        }
    }

    @DexIgnore
    public static boolean C(Transition transition) {
        return !Ar0.l(transition.K()) || !Ar0.l(transition.M()) || !Ar0.l(transition.N());
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void A(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        TransitionSet transitionSet = (TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.O().clear();
            transitionSet.O().addAll(arrayList2);
            q(transitionSet, arrayList, arrayList2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public Object B(Object obj) {
        if (obj == null) {
            return null;
        }
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.t0((Transition) obj);
        return transitionSet;
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void a(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).e(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void b(Object obj, ArrayList<View> arrayList) {
        Transition transition = (Transition) obj;
        if (transition != null) {
            if (transition instanceof TransitionSet) {
                TransitionSet transitionSet = (TransitionSet) transition;
                int w0 = transitionSet.w0();
                for (int i = 0; i < w0; i++) {
                    b(transitionSet.v0(i), arrayList);
                }
            } else if (!C(transition) && Ar0.l(transition.O())) {
                int size = arrayList.size();
                for (int i2 = 0; i2 < size; i2++) {
                    transition.e(arrayList.get(i2));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void c(ViewGroup viewGroup, Object obj) {
        Ty0.a(viewGroup, (Transition) obj);
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public boolean e(Object obj) {
        return obj instanceof Transition;
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public Object g(Object obj) {
        if (obj != null) {
            return ((Transition) obj).t();
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public Object m(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet;
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition == null || transition2 == null) {
            transitionSet = transition != null ? transition : transition2 != null ? transition2 : null;
        } else {
            TransitionSet transitionSet2 = new TransitionSet();
            transitionSet2.t0(transition);
            transitionSet2.t0(transition2);
            transitionSet2.B0(1);
            transitionSet = transitionSet2;
        }
        if (transition3 == null) {
            return transitionSet;
        }
        TransitionSet transitionSet3 = new TransitionSet();
        if (transitionSet != null) {
            transitionSet3.t0(transitionSet);
        }
        transitionSet3.t0(transition3);
        return transitionSet3;
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public Object n(Object obj, Object obj2, Object obj3) {
        TransitionSet transitionSet = new TransitionSet();
        if (obj != null) {
            transitionSet.t0((Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.t0((Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.t0((Transition) obj3);
        }
        return transitionSet;
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void p(Object obj, View view) {
        if (obj != null) {
            ((Transition) obj).e0(view);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void q(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        int i;
        int i2;
        Transition transition = (Transition) obj;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int w0 = transitionSet.w0();
            for (int i3 = 0; i3 < w0; i3++) {
                q(transitionSet.v0(i3), arrayList, arrayList2);
            }
        } else if (!C(transition)) {
            List<View> O = transition.O();
            if (O.size() == arrayList.size() && O.containsAll(arrayList)) {
                if (arrayList2 == null) {
                    i2 = 0;
                    i = 0;
                } else {
                    i2 = arrayList2.size();
                    i = 0;
                }
                while (i < i2) {
                    transition.e(arrayList2.get(i));
                    i++;
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    transition.e0(arrayList.get(size));
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void r(Object obj, View view, ArrayList<View> arrayList) {
        ((Transition) obj).d(new Bi(this, view, arrayList));
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void t(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3) {
        ((Transition) obj).d(new Ci(obj2, arrayList, obj3, arrayList2, obj4, arrayList3));
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void u(Object obj, Rect rect) {
        if (obj != null) {
            ((Transition) obj).j0(new Di(this, rect));
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void v(Object obj, View view) {
        if (view != null) {
            Rect rect = new Rect();
            k(view, rect);
            ((Transition) obj).j0(new Ai(this, rect));
        }
    }

    @DexIgnore
    @Override // com.fossil.Ar0
    public void z(Object obj, View view, ArrayList<View> arrayList) {
        TransitionSet transitionSet = (TransitionSet) obj;
        List<View> O = transitionSet.O();
        O.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Ar0.d(O, arrayList.get(i));
        }
        O.add(view);
        arrayList.add(view);
        b(transitionSet, arrayList);
    }
}
