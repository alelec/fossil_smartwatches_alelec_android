package com.fossil;

import com.google.gson.JsonElement;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj4 extends JsonElement {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ ak4<String, JsonElement> f1315a; // = new ak4<>();

    @DexIgnore
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.f1315a.entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof gj4) && ((gj4) obj).f1315a.equals(this.f1315a));
    }

    @DexIgnore
    public int hashCode() {
        return this.f1315a.hashCode();
    }

    @DexIgnore
    public void k(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = fj4.f1138a;
        }
        this.f1315a.put(str, jsonElement);
    }

    @DexIgnore
    public void l(String str, Boolean bool) {
        k(str, o(bool));
    }

    @DexIgnore
    public void m(String str, Number number) {
        k(str, o(number));
    }

    @DexIgnore
    public void n(String str, String str2) {
        k(str, o(str2));
    }

    @DexIgnore
    public final JsonElement o(Object obj) {
        return obj == null ? fj4.f1138a : new jj4(obj);
    }

    @DexIgnore
    public JsonElement p(String str) {
        return this.f1315a.get(str);
    }

    @DexIgnore
    public bj4 q(String str) {
        return (bj4) this.f1315a.get(str);
    }

    @DexIgnore
    public gj4 r(String str) {
        return (gj4) this.f1315a.get(str);
    }

    @DexIgnore
    public boolean s(String str) {
        return this.f1315a.containsKey(str);
    }

    @DexIgnore
    public int size() {
        return this.f1315a.size();
    }
}
