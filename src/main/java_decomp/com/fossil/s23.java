package com.fossil;

import com.fossil.E13;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S23<T> implements F33<T> {
    @DexIgnore
    public /* final */ M23 a;
    @DexIgnore
    public /* final */ X33<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ S03<?> d;

    @DexIgnore
    public S23(X33<?, ?> x33, S03<?> s03, M23 m23) {
        this.b = x33;
        this.c = s03.e(m23);
        this.d = s03;
        this.a = m23;
    }

    @DexIgnore
    public static <T> S23<T> c(X33<?, ?> x33, S03<?> s03, M23 m23) {
        return new S23<>(x33, s03, m23);
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void a(T t, byte[] bArr, int i, int i2, Sz2 sz2) throws IOException {
        T t2 = t;
        W33 w33 = t2.zzb;
        if (w33 == W33.a()) {
            w33 = W33.g();
            t2.zzb = w33;
        }
        t.C();
        E13.Di di = null;
        while (i < i2) {
            int i3 = Tz2.i(bArr, i, sz2);
            int i4 = sz2.a;
            if (i4 == 11) {
                int i5 = 0;
                Xz2 xz2 = null;
                i = i3;
                while (i < i2) {
                    i = Tz2.i(bArr, i, sz2);
                    int i6 = sz2.a;
                    int i7 = i6 >>> 3;
                    int i8 = i6 & 7;
                    if (i7 != 2) {
                        if (i7 == 3) {
                            if (di != null) {
                                B33.a();
                                throw new NoSuchMethodError();
                            } else if (i8 == 2) {
                                i = Tz2.q(bArr, i, sz2);
                                xz2 = (Xz2) sz2.c;
                            }
                        }
                    } else if (i8 == 0) {
                        i = Tz2.i(bArr, i, sz2);
                        i5 = sz2.a;
                        di = (E13.Di) this.d.c(sz2.d, this.a, i5);
                    }
                    if (i6 == 12) {
                        break;
                    }
                    i = Tz2.a(i6, bArr, i, i2, sz2);
                }
                if (xz2 != null) {
                    w33.c((i5 << 3) | 2, xz2);
                }
            } else if ((i4 & 7) == 2) {
                di = (E13.Di) this.d.c(sz2.d, this.a, i4 >>> 3);
                if (di == null) {
                    i = Tz2.c(i4, bArr, i3, i2, w33, sz2);
                } else {
                    B33.a();
                    throw new NoSuchMethodError();
                }
            } else {
                i = Tz2.a(i4, bArr, i3, i2, sz2);
            }
        }
        if (i != i2) {
            throw L13.zzg();
        }
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void b(T t, R43 r43) throws IOException {
        Iterator<Map.Entry<?, Object>> p = this.d.b(t).p();
        while (p.hasNext()) {
            Map.Entry<?, Object> next = p.next();
            V03 v03 = (V03) next.getKey();
            if (v03.zzc() != S43.zzi || v03.zzd() || v03.zze()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof S13) {
                r43.zza(v03.zza(), ((S13) next).a().d());
            } else {
                r43.zza(v03.zza(), next.getValue());
            }
        }
        X33<?, ?> x33 = this.b;
        x33.g(x33.f(t), r43);
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final int zza(T t) {
        int hashCode = this.b.f(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.b(t).hashCode() : hashCode;
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final T zza() {
        return (T) this.a.i().b();
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final boolean zza(T t, T t2) {
        if (!this.b.f(t).equals(this.b.f(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.b(t).equals(this.d.b(t2));
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final int zzb(T t) {
        X33<?, ?> x33 = this.b;
        int k = x33.k(x33.f(t)) + 0;
        return this.c ? k + this.d.b(t).s() : k;
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void zzb(T t, T t2) {
        H33.o(this.b, t, t2);
        if (this.c) {
            H33.m(this.d, t, t2);
        }
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final void zzc(T t) {
        this.b.j(t);
        this.d.g(t);
    }

    @DexIgnore
    @Override // com.fossil.F33
    public final boolean zzd(T t) {
        return this.d.b(t).r();
    }
}
