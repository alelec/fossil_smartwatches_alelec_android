package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N18 extends RequestBody {
    @DexIgnore
    public static /* final */ R18 c; // = R18.c("application/x-www-form-urlencoded");
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public Ai() {
            this(null);
        }

        @DexIgnore
        public Ai(Charset charset) {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public Ai a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(Q18.c(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(Q18.c(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public Ai b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(Q18.c(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(Q18.c(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public N18 c() {
            return new N18(this.a, this.b);
        }
    }

    @DexIgnore
    public N18(List<String> list, List<String> list2) {
        this.a = B28.t(list);
        this.b = B28.t(list2);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() {
        return i(null, true);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public R18 b() {
        return c;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void h(J48 j48) throws IOException {
        i(j48, false);
    }

    @DexIgnore
    public final long i(J48 j48, boolean z) {
        I48 i48 = z ? new I48() : j48.d();
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                i48.w0(38);
            }
            i48.D0(this.a.get(i));
            i48.w0(61);
            i48.D0(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long p0 = i48.p0();
        i48.j();
        return p0;
    }
}
