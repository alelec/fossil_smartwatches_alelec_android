package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jn4 implements Ql4 {
    @DexIgnore
    public static int b(boolean[] zArr, int i, int[] iArr, boolean z) {
        int i2 = i;
        int i3 = 0;
        for (int i4 : iArr) {
            int i5 = 0;
            while (i5 < i4) {
                zArr[i2] = z;
                i5++;
                i2++;
            }
            i3 += i4;
            z = !z;
        }
        return i3;
    }

    @DexIgnore
    public static Bm4 e(boolean[] zArr, int i, int i2, int i3) {
        int length = zArr.length;
        int i4 = i3 + length;
        int max = Math.max(i, i4);
        int max2 = Math.max(1, i2);
        int i5 = max / i4;
        int i6 = (max - (length * i5)) / 2;
        Bm4 bm4 = new Bm4(max, max2);
        int i7 = 0;
        while (i7 < length) {
            if (zArr[i7]) {
                bm4.o(i6, 0, i5, max2);
            }
            i7++;
            i6 += i5;
        }
        return bm4;
    }

    @DexIgnore
    @Override // com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Negative size is not allowed. Input: " + i + 'x' + i2);
        } else {
            int d = d();
            if (map != null && map.containsKey(Ml4.MARGIN)) {
                d = Integer.parseInt(map.get(Ml4.MARGIN).toString());
            }
            return e(c(str), i, i2, d);
        }
    }

    @DexIgnore
    public abstract boolean[] c(String str);

    @DexIgnore
    public int d() {
        return 10;
    }
}
