package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rb extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Qb CREATOR; // = new Qb(null);
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public Rb(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("is_front_light_enabled", this.b);
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Rb.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((Rb) obj).b;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.frontlight.FrontLightConfig");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.b).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b ? 1 : 0);
        }
    }
}
