package com.fossil;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aa6 extends v96 {
    @DexIgnore
    public k76 e;
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<WatchApp>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<String, Boolean, Parcelable>> i; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<Category> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ ls0<String> l;
    @DexIgnore
    public /* final */ LiveData<List<WatchApp>> m;
    @DexIgnore
    public /* final */ ls0<List<WatchApp>> n;
    @DexIgnore
    public /* final */ ls0<gl7<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ w96 p;
    @DexIgnore
    public /* final */ CategoryRepository q;
    @DexIgnore
    public /* final */ on5 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1", f = "WatchAppsPresenter.kt", l = {237}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ aa6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.aa6$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1", f = "WatchAppsPresenter.kt", l = {237}, m = "invokeSuspend")
        /* renamed from: com.fossil.aa6$a$a  reason: collision with other inner class name */
        public static final class C0009a extends ko7 implements vp7<iv7, qn7<? super Parcelable>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0009a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0009a aVar = new C0009a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Parcelable> qn7) {
                throw null;
                //return ((C0009a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    k76 D = aa6.D(this.this$0.this$0);
                    String str = this.this$0.$id;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object w = D.w(str, this);
                    return w == d ? d : w;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(aa6 aa6, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = aa6;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$id, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            boolean g;
            Object g2;
            Parcelable parcelable = null;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                g = ol5.c.g(this.$id);
                if (g) {
                    dv7 h = this.this$0.h();
                    C0009a aVar = new C0009a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = null;
                    this.Z$0 = g;
                    this.label = 1;
                    g2 = eu7.g(h, aVar, this);
                    if (g2 == d) {
                        return d;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.i.l(new gl7(this.$id, ao7.a(g), parcelable));
                return tl7.f3441a;
            } else if (i == 1) {
                g = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) g2;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("WatchAppsPresenter", "checkSettingOfSelectedWatchApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.i.l(new gl7(this.$id, ao7.a(g), parcelable));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $watchappId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ aa6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, qn7 qn7, aa6 aa6) {
            super(2, qn7);
            this.$watchappId = str;
            this.this$0 = aa6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.$watchappId, qn7, this.this$0);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            String setting;
            String str;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                Iterator<T> it = aa6.D(this.this$0).x().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (ao7.a(pq7.a(next.getAppId(), this.$watchappId)).booleanValue()) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                String str2 = this.$watchappId;
                int hashCode = str2.hashCode();
                String str3 = "";
                if (hashCode != -829740640) {
                    if (hashCode == 1223440372 && str2.equals("weather")) {
                        w96 w96 = this.this$0.p;
                        if (t2 == null || (str = t2.getSetting()) == null) {
                            str = str3;
                        }
                        w96.d6(str);
                    }
                } else if (str2.equals("commute-time")) {
                    w96 w962 = this.this$0.p;
                    if (!(t2 == null || (setting = t2.getSetting()) == null)) {
                        str3 = setting;
                    }
                    w962.f0(str3);
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f234a;

        @DexIgnore
        public c(aa6 aa6) {
            this.f234a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged category=" + str);
            if (str != null) {
                this.f234a.p.u0(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f235a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1", f = "WatchAppsPresenter.kt", l = {81}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ aa6 $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.aa6$d$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$mCategoryOfSelectedWatchAppTransformation$1$3$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.aa6$d$a$a  reason: collision with other inner class name */
            public static final class C0010a extends ko7 implements vp7<iv7, qn7<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0010a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0010a aVar = new C0010a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends Category>> qn7) {
                    throw null;
                    //return ((C0010a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(aa6 aa6, qn7 qn7) {
                super(2, qn7);
                this.$this_run = aa6;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.$this_run, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 b = bw7.b();
                    C0010a aVar = new C0010a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(b, aVar, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (!list.isEmpty()) {
                    this.$this_run.g.l(((Category) list.get(0)).getId());
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public d(aa6 aa6) {
            this.f235a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(WatchApp watchApp) {
            if (watchApp != null) {
                this.f235a.G(watchApp.getWatchappId());
            }
            String str = (String) this.f235a.g.e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transform from selected WatchApp to category");
            sb.append(" currentCategory=");
            sb.append(str);
            sb.append(" watchAppCategories=");
            sb.append(watchApp != null ? watchApp.getCategories() : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (watchApp != null) {
                ArrayList<String> categories = watchApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.f235a.g.l(categories.get(0));
                } else {
                    this.f235a.g.l(str);
                }
            } else {
                aa6 aa6 = this.f235a;
                xw7 unused = gu7.d(aa6.k(), null, null, new a(aa6, null), 3, null);
            }
            return this.f235a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<gl7<? extends String, ? extends Boolean, ? extends Parcelable>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f236a;

        @DexIgnore
        public e(aa6 aa6) {
            this.f236a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gl7<String, Boolean, ? extends Parcelable> gl7) {
            String str;
            Exception e;
            String c;
            String name;
            if (gl7 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppsPresenter", "onLiveDataChanged setting of " + gl7.getFirst() + " isSettingRequired " + gl7.getSecond().booleanValue() + " setting " + ((Parcelable) gl7.getThird()) + ' ');
                String str2 = "";
                if (gl7.getSecond().booleanValue()) {
                    Parcelable parcelable = (Parcelable) gl7.getThird();
                    if (parcelable == null) {
                        str = "";
                        str2 = ol5.c.b(gl7.getFirst());
                    } else {
                        try {
                            String first = gl7.getFirst();
                            int hashCode = first.hashCode();
                            if (hashCode == -829740640) {
                                if (first.equals("commute-time")) {
                                    c = um5.c(PortfolioApp.h0.c(), 2131886363);
                                    pq7.b(c, "LanguageHelper.getString\u2026Title__SavedDestinations)");
                                }
                                str = "";
                            } else if (hashCode != 1223440372) {
                                c = "";
                            } else {
                                if (first.equals("weather")) {
                                    List<WeatherLocationWrapper> locations = ((WeatherWatchAppSetting) parcelable).getLocations();
                                    Iterator<WeatherLocationWrapper> it = locations.iterator();
                                    str = "";
                                    while (it.hasNext()) {
                                        try {
                                            WeatherLocationWrapper next = it.next();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(str);
                                            if (locations.indexOf(next) < hm7.g(locations)) {
                                                StringBuilder sb2 = new StringBuilder();
                                                sb2.append(next != null ? next.getName() : null);
                                                sb2.append("; ");
                                                name = sb2.toString();
                                            } else {
                                                name = next != null ? next.getName() : null;
                                            }
                                            sb.append(name);
                                            str = sb.toString();
                                        } catch (Exception e2) {
                                            e = e2;
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            local2.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                                            this.f236a.p.g0(true, gl7.getFirst(), str2, str);
                                            return;
                                        }
                                    }
                                }
                                str = "";
                            }
                            str = c;
                        } catch (Exception e3) {
                            e = e3;
                            str = "";
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            local22.d("WatchAppsPresenter", "exception when parse micro app setting " + e);
                            this.f236a.p.g0(true, gl7.getFirst(), str2, str);
                            return;
                        }
                    }
                    this.f236a.p.g0(true, gl7.getFirst(), str2, str);
                    return;
                }
                this.f236a.p.g0(false, gl7.getFirst(), "", null);
                WatchApp e4 = aa6.D(this.f236a).u().e();
                if (e4 != null) {
                    w96 w96 = this.f236a.p;
                    String d = um5.d(PortfolioApp.h0.c(), e4.getDescriptionKey(), e4.getDescription());
                    pq7.b(d, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    w96.U4(d);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<List<? extends WatchApp>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f237a;

        @DexIgnore
        public f(aa6 aa6) {
            this.f237a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged WatchApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("WatchAppsPresenter", sb.toString());
            if (list != null) {
                this.f237a.p.S5(list);
                WatchApp e = aa6.D(this.f237a).u().e();
                if (e != null) {
                    this.f237a.p.o5(e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements gi0<X, LiveData<Y>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f238a;

        @DexIgnore
        public g(aa6 aa6) {
            this.f238a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<List<WatchApp>> apply(String str) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "transform from category to list watchapps with category=" + str);
            k76 D = aa6.D(this.f238a);
            pq7.b(str, "category");
            List<WatchApp> n = D.n(str);
            ArrayList arrayList = new ArrayList();
            List<oo5> e = aa6.D(this.f238a).q().e();
            WatchApp e2 = aa6.D(this.f238a).u().e();
            String watchappId = e2 != null ? e2.getWatchappId() : null;
            if (e != null) {
                for (WatchApp watchApp : n) {
                    Iterator<T> it = e.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        T t2 = next;
                        if (!pq7.a(t2.a(), watchApp.getWatchappId()) || !(!pq7.a(t2.a(), watchappId))) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    if (t == null || pq7.a(watchApp.getWatchappId(), "empty")) {
                        arrayList.add(watchApp);
                    }
                }
            }
            this.f238a.h.l(arrayList);
            return this.f238a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1", f = "WatchAppsPresenter.kt", l = {194}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ aa6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Category>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(aa6 aa6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = aa6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(i2, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) g) {
                if (!aa6.D(this.this$0).n(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.j.clear();
            this.this$0.j.addAll(arrayList);
            this.this$0.p.S(this.this$0.j);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements ls0<WatchApp> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ aa6 f239a;

        @DexIgnore
        public i(aa6 aa6) {
            this.f239a = aa6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(WatchApp watchApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsPresenter", "onLiveDataChanged selectedWatchApp value=" + watchApp);
            this.f239a.f.l(watchApp);
            if (watchApp != null) {
                this.f239a.p.c1(ol5.c.f(watchApp.getWatchappId()));
            }
        }
    }

    @DexIgnore
    public aa6(w96 w96, CategoryRepository categoryRepository, on5 on5) {
        pq7.c(w96, "mView");
        pq7.c(categoryRepository, "mCategoryRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        this.p = w96;
        this.q = categoryRepository;
        this.r = on5;
        new Gson();
        LiveData<String> c2 = ss0.c(this.f, new d(this));
        pq7.b(c2, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.k = c2;
        this.l = new c(this);
        LiveData<List<WatchApp>> c3 = ss0.c(this.k, new g(this));
        pq7.b(c3, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = c3;
        this.n = new f(this);
        this.o = new e(this);
    }

    @DexIgnore
    public static final /* synthetic */ k76 D(aa6 aa6) {
        k76 k76 = aa6.e;
        if (k76 != null) {
            return k76;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public final xw7 G(String str) {
        return gu7.d(k(), null, null, new a(this, str, null), 3, null);
    }

    @DexIgnore
    public void H() {
        this.p.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onStart");
        this.k.i(this.l);
        this.m.i(this.n);
        this.i.i(this.o);
        if (this.j.isEmpty()) {
            xw7 unused = gu7.d(k(), null, null, new h(this, null), 3, null);
        } else {
            this.p.S(this.j);
        }
        k76 k76 = this.e;
        if (k76 != null) {
            LiveData<WatchApp> u = k76.u();
            w96 w96 = this.p;
            if (w96 != null) {
                u.h((x96) w96, new i(this));
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        k76 k76 = this.e;
        if (k76 != null) {
            LiveData<WatchApp> u = k76.u();
            w96 w96 = this.p;
            if (w96 != null) {
                u.n((x96) w96);
                this.h.m(this.n);
                this.g.m(this.l);
                this.i.m(this.o);
                return;
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment");
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void n() {
        T t;
        T t2;
        T t3;
        String str;
        String a2;
        String a3;
        k76 k76 = this.e;
        if (k76 != null) {
            List<oo5> e2 = k76.q().e();
            if (e2 != null) {
                Iterator<T> it = e2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (pq7.a(next.b(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                String str2 = (t4 == null || (a3 = t4.a()) == null) ? "empty" : a3;
                Iterator<T> it2 = e2.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (pq7.a(next2.b(), "middle")) {
                        t2 = next2;
                        break;
                    }
                }
                T t5 = t2;
                String str3 = (t5 == null || (a2 = t5.a()) == null) ? "empty" : a2;
                Iterator<T> it3 = e2.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (pq7.a(next3.b(), "bottom")) {
                        t3 = next3;
                        break;
                    }
                }
                T t6 = t3;
                if (t6 == null || (str = t6.a()) == null) {
                    str = "empty";
                }
                this.p.a0(str2, str3, str);
                return;
            }
            this.p.a0("empty", "empty", "empty");
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void o() {
        k76 k76 = this.e;
        if (k76 != null) {
            WatchApp e2 = k76.u().e();
            if (e2 != null) {
                xw7 unused = gu7.d(k(), null, null, new b(e2.component1(), null, this), 3, null);
                return;
            }
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void p() {
        k76 k76 = this.e;
        if (k76 != null) {
            WatchApp e2 = k76.u().e();
            if (e2 != null) {
                String component1 = e2.component1();
                if (ol5.c.f(component1)) {
                    this.p.i0(component1);
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void q(Category category) {
        pq7.c(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "category change " + category);
        this.g.l(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void r(String str) {
        T t;
        Object obj;
        pq7.c(str, "watchappId");
        k76 k76 = this.e;
        if (k76 != null) {
            WatchApp o2 = k76.o(str);
            FLogger.INSTANCE.getLocal().d("WatchAppsPresenter", "onUserChooseWatchApp " + o2);
            if (o2 != null) {
                k76 k762 = this.e;
                if (k762 != null) {
                    List<oo5> e2 = k762.q().e();
                    if (e2 != null) {
                        pq7.b(e2, "presetButtons");
                        ArrayList<oo5> a2 = kj5.a(e2);
                        ArrayList arrayList = new ArrayList();
                        k76 k763 = this.e;
                        if (k763 != null) {
                            String e3 = k763.v().e();
                            if (e3 != null) {
                                pq7.b(e3, "mWatchAppEditViewModel.g\u2026ctedWatchAppPos().value!!");
                                String str2 = e3;
                                k76 k764 = this.e;
                                if (k764 != null) {
                                    if (!k764.D(str)) {
                                        Iterator<oo5> it = a2.iterator();
                                        while (it.hasNext()) {
                                            oo5 next = it.next();
                                            if (pq7.a(next.b(), str2)) {
                                                arrayList.add(new oo5(str, str2, next.c()));
                                            } else {
                                                arrayList.add(next);
                                            }
                                        }
                                        Iterator it2 = arrayList.iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                obj = null;
                                                break;
                                            }
                                            obj = it2.next();
                                            if (pq7.a(((oo5) obj).b(), str2)) {
                                                break;
                                            }
                                        }
                                        if (obj == null) {
                                            SimpleDateFormat simpleDateFormat = lk5.n.get();
                                            if (simpleDateFormat != null) {
                                                String format = simpleDateFormat.format(new Date());
                                                pq7.b(format, "DateHelper.LOCAL_DATE_SP\u2026AT.get()!!.format(Date())");
                                                arrayList.add(new oo5(str2, str, format));
                                            } else {
                                                pq7.i();
                                                throw null;
                                            }
                                        }
                                        a2.clear();
                                        a2.addAll(arrayList);
                                        k76 k765 = this.e;
                                        if (k765 != null) {
                                            k765.G(a2);
                                        } else {
                                            pq7.n("mWatchAppEditViewModel");
                                            throw null;
                                        }
                                    } else {
                                        Iterator<T> it3 = e2.iterator();
                                        while (true) {
                                            if (!it3.hasNext()) {
                                                t = null;
                                                break;
                                            }
                                            T next2 = it3.next();
                                            if (pq7.a(next2.a(), str)) {
                                                t = next2;
                                                break;
                                            }
                                        }
                                        T t2 = t;
                                        if (t2 != null) {
                                            k76 k766 = this.e;
                                            if (k766 != null) {
                                                k766.F(t2.b());
                                            } else {
                                                pq7.n("mWatchAppEditViewModel");
                                                throw null;
                                            }
                                        }
                                    }
                                    String watchappId = o2.getWatchappId();
                                    if (watchappId.hashCode() == -829740640 && watchappId.equals("commute-time") && !this.r.t()) {
                                        this.r.s1(true);
                                        this.p.i0("commute-time");
                                        return;
                                    }
                                    return;
                                }
                                pq7.n("mWatchAppEditViewModel");
                                throw null;
                            }
                            pq7.i();
                            throw null;
                        }
                        pq7.n("mWatchAppEditViewModel");
                        throw null;
                    }
                    return;
                }
                pq7.n("mWatchAppEditViewModel");
                throw null;
            }
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void s(k76 k76) {
        pq7.c(k76, "viewModel");
        this.e = k76;
    }

    @DexIgnore
    @Override // com.fossil.v96
    public void t(String str, Parcelable parcelable) {
        pq7.c(str, "watchAppId");
        pq7.c(parcelable, MicroAppSetting.SETTING);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsPresenter", "updateSettingForWatchApp watchAppId " + str + " setting " + parcelable);
        k76 k76 = this.e;
        if (k76 != null) {
            k76.H(str, parcelable);
            G(str);
            return;
        }
        pq7.n("mWatchAppEditViewModel");
        throw null;
    }
}
