package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tz5 implements Factory<Lb6> {
    @DexIgnore
    public static Lb6 a(Oz5 oz5) {
        Lb6 e = oz5.e();
        Lk7.c(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
