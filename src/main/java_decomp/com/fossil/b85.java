package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class B85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RelativeLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView A0;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ FlexibleTextView B0;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ View C0;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ View D0;
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public /* final */ Guideline E0;
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public /* final */ ConstraintLayout G;
    @DexIgnore
    public /* final */ FlexibleButton H;
    @DexIgnore
    public /* final */ FlexibleButton I;
    @DexIgnore
    public /* final */ FlexibleButton J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ FlexibleTextView L;
    @DexIgnore
    public /* final */ Barrier M;
    @DexIgnore
    public /* final */ Guideline N;
    @DexIgnore
    public /* final */ ImageView O;
    @DexIgnore
    public /* final */ ImageView P;
    @DexIgnore
    public /* final */ ImageView Q;
    @DexIgnore
    public /* final */ ImageView R;
    @DexIgnore
    public /* final */ RTLImageView S;
    @DexIgnore
    public /* final */ RTLImageView T;
    @DexIgnore
    public /* final */ RTLImageView U;
    @DexIgnore
    public /* final */ RTLImageView V;
    @DexIgnore
    public /* final */ RTLImageView W;
    @DexIgnore
    public /* final */ RTLImageView X;
    @DexIgnore
    public /* final */ RTLImageView Y;
    @DexIgnore
    public /* final */ RTLImageView Z;
    @DexIgnore
    public /* final */ RTLImageView a0;
    @DexIgnore
    public /* final */ RTLImageView b0;
    @DexIgnore
    public /* final */ FossilCircleImageView c0;
    @DexIgnore
    public /* final */ FossilCircleImageView d0;
    @DexIgnore
    public /* final */ ConstraintLayout e0;
    @DexIgnore
    public /* final */ LinearLayout f0;
    @DexIgnore
    public /* final */ NestedScrollView g0;
    @DexIgnore
    public /* final */ RecyclerView h0;
    @DexIgnore
    public /* final */ AutoResizeTextView i0;
    @DexIgnore
    public /* final */ FlexibleTextView j0;
    @DexIgnore
    public /* final */ FlexibleTextView k0;
    @DexIgnore
    public /* final */ AutoResizeTextView l0;
    @DexIgnore
    public /* final */ FlexibleTextView m0;
    @DexIgnore
    public /* final */ FlexibleTextView n0;
    @DexIgnore
    public /* final */ AutoResizeTextView o0;
    @DexIgnore
    public /* final */ FlexibleTextView p0;
    @DexIgnore
    public /* final */ Barrier q;
    @DexIgnore
    public /* final */ FlexibleTextView q0;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView r0;
    @DexIgnore
    public /* final */ RelativeLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView s0;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView t0;
    @DexIgnore
    public /* final */ RelativeLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView u0;
    @DexIgnore
    public /* final */ RelativeLayout v;
    @DexIgnore
    public /* final */ FlexibleTextView v0;
    @DexIgnore
    public /* final */ RelativeLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView w0;
    @DexIgnore
    public /* final */ FlexibleButton x;
    @DexIgnore
    public /* final */ FlexibleTextView x0;
    @DexIgnore
    public /* final */ RelativeLayout y;
    @DexIgnore
    public /* final */ FlexibleTextView y0;
    @DexIgnore
    public /* final */ RelativeLayout z;
    @DexIgnore
    public /* final */ FlexibleTextView z0;

    @DexIgnore
    public B85(Object obj, View view, int i, Barrier barrier, RelativeLayout relativeLayout, RelativeLayout relativeLayout2, RelativeLayout relativeLayout3, RelativeLayout relativeLayout4, RelativeLayout relativeLayout5, RelativeLayout relativeLayout6, FlexibleButton flexibleButton, RelativeLayout relativeLayout7, RelativeLayout relativeLayout8, RelativeLayout relativeLayout9, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, Barrier barrier2, Guideline guideline, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, RTLImageView rTLImageView5, RTLImageView rTLImageView6, RTLImageView rTLImageView7, RTLImageView rTLImageView8, RTLImageView rTLImageView9, RTLImageView rTLImageView10, FossilCircleImageView fossilCircleImageView, FossilCircleImageView fossilCircleImageView2, ConstraintLayout constraintLayout7, LinearLayout linearLayout, NestedScrollView nestedScrollView, RecyclerView recyclerView, AutoResizeTextView autoResizeTextView, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, AutoResizeTextView autoResizeTextView2, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, AutoResizeTextView autoResizeTextView3, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, FlexibleTextView flexibleTextView16, FlexibleTextView flexibleTextView17, FlexibleTextView flexibleTextView18, FlexibleTextView flexibleTextView19, View view2, View view3, Guideline guideline2) {
        super(obj, view, i);
        this.q = barrier;
        this.r = relativeLayout;
        this.s = relativeLayout2;
        this.t = relativeLayout3;
        this.u = relativeLayout4;
        this.v = relativeLayout5;
        this.w = relativeLayout6;
        this.x = flexibleButton;
        this.y = relativeLayout7;
        this.z = relativeLayout8;
        this.A = relativeLayout9;
        this.B = constraintLayout;
        this.C = constraintLayout2;
        this.D = constraintLayout3;
        this.E = constraintLayout4;
        this.F = constraintLayout5;
        this.G = constraintLayout6;
        this.H = flexibleButton2;
        this.I = flexibleButton3;
        this.J = flexibleButton4;
        this.K = flexibleTextView;
        this.L = flexibleTextView2;
        this.M = barrier2;
        this.N = guideline;
        this.O = imageView;
        this.P = imageView2;
        this.Q = imageView3;
        this.R = imageView4;
        this.S = rTLImageView;
        this.T = rTLImageView2;
        this.U = rTLImageView3;
        this.V = rTLImageView4;
        this.W = rTLImageView5;
        this.X = rTLImageView6;
        this.Y = rTLImageView7;
        this.Z = rTLImageView8;
        this.a0 = rTLImageView9;
        this.b0 = rTLImageView10;
        this.c0 = fossilCircleImageView;
        this.d0 = fossilCircleImageView2;
        this.e0 = constraintLayout7;
        this.f0 = linearLayout;
        this.g0 = nestedScrollView;
        this.h0 = recyclerView;
        this.i0 = autoResizeTextView;
        this.j0 = flexibleTextView3;
        this.k0 = flexibleTextView4;
        this.l0 = autoResizeTextView2;
        this.m0 = flexibleTextView5;
        this.n0 = flexibleTextView6;
        this.o0 = autoResizeTextView3;
        this.p0 = flexibleTextView7;
        this.q0 = flexibleTextView8;
        this.r0 = flexibleTextView9;
        this.s0 = flexibleTextView10;
        this.t0 = flexibleTextView11;
        this.u0 = flexibleTextView12;
        this.v0 = flexibleTextView13;
        this.w0 = flexibleTextView14;
        this.x0 = flexibleTextView15;
        this.y0 = flexibleTextView16;
        this.z0 = flexibleTextView17;
        this.A0 = flexibleTextView18;
        this.B0 = flexibleTextView19;
        this.C0 = view2;
        this.D0 = view3;
        this.E0 = guideline2;
    }
}
