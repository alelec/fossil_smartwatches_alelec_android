package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.N04;
import com.fossil.X37;
import com.google.android.material.tabs.TabLayout;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ux6 extends Ey6 implements Gq4, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String w; // = "STATE";
    @DexIgnore
    public static /* final */ Ai x; // = new Ai(null);
    @DexIgnore
    public Hy6 h;
    @DexIgnore
    public Qy6 i;
    @DexIgnore
    public G37<Ub5> j;
    @DexIgnore
    public Gr4 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public int m;
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String t; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String u; // = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ux6 a(String str, boolean z, int i) {
            Wg6.c(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putString("SERIAL", str);
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putInt(Ux6.w, i);
            Ux6 ux6 = new Ux6();
            ux6.setArguments(bundle);
            return ux6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ux6 b;

        @DexIgnore
        public Bi(Ux6 ux6, String str) {
            this.b = ux6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ Ub5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ux6 b;

        @DexIgnore
        public Ci(Ub5 ub5, Ux6 ux6, String str) {
            this.a = ub5;
            this.b = ux6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.t)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "set icon color " + this.b.t);
                int parseColor = Color.parseColor(this.b.t);
                TabLayout.g v = this.a.A.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.s) && this.b.m != i) {
                int parseColor2 = Color.parseColor(this.b.s);
                TabLayout.g v2 = this.a.A.v(this.b.m);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.m = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ux6 b;

        @DexIgnore
        public Di(Ux6 ux6, String str) {
            this.b = ux6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().B();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ux6 b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public Ei(Ux6 ux6, String str) {
            this.b = ux6;
            this.c = str;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                Context requireContext = this.b.requireContext();
                Wg6.b(requireContext, "requireContext()");
                TroubleshootingActivity.a.c(aVar, requireContext, this.c, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ux6 a;

        @DexIgnore
        public Fi(Ux6 ux6) {
            this.a = ux6;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.s) && !TextUtils.isEmpty(this.a.t)) {
                int parseColor = Color.parseColor(this.a.s);
                int parseColor2 = Color.parseColor(this.a.t);
                gVar.o(2131230966);
                if (i == this.a.m) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    public final void A(boolean z) {
        if (isActive()) {
            G37<Ub5> g37 = this.j;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    Wg6.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    Wg6.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    Wg6.b(flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    Wg6.b(flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    Wg6.b(flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setProgress(1000);
                    FlexibleTextView flexibleTextView2 = a2.y;
                    Wg6.b(flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887045));
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.q;
                Wg6.b(constraintLayout3, "it.clUpdateFwFail");
                constraintLayout3.setVisibility(0);
                ConstraintLayout constraintLayout4 = a2.r;
                Wg6.b(constraintLayout4, "it.clUpdatingFw");
                constraintLayout4.setVisibility(8);
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        Q6((Hy6) obj);
    }

    @DexIgnore
    public final Hy6 P6() {
        Hy6 hy6 = this.h;
        if (hy6 != null) {
            return hy6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q6(Hy6 hy6) {
        Wg6.c(hy6, "presenter");
        this.h = hy6;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.l);
            if (i2 == 2131362290) {
                Hy6 hy6 = this.h;
                if (hy6 != null) {
                    hy6.B();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131362385 && getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.B;
                FragmentActivity requireActivity = requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore
    public void R6() {
        DashBar dashBar;
        G37<Ub5> g37 = this.j;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.c(dashBar, this.l, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        G37<Ub5> g37 = this.j;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                G37<Ub5> g372 = this.j;
                if (g372 != null) {
                    Ub5 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new N04(tabLayout, viewPager2, new Fi(this)).a();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V2() {
        if (isActive()) {
            G37<Ub5> g37 = this.j;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    Wg6.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    Wg6.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void X(List<? extends Explore> list) {
        Wg6.c(list, "data");
        Gr4 gr4 = this.k;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            Wg6.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        G37<Ub5> g37 = this.j;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void f() {
        DashBar dashBar;
        G37<Ub5> g37 = this.j;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                dashBar.setVisibility(0);
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.i(dashBar, this.l, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Ub5 ub5 = (Ub5) Aq0.f(layoutInflater, 2131558632, viewGroup, false, A6());
        this.j = new G37<>(this, ub5);
        Wg6.b(ub5, "binding");
        return ub5.n();
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Qy6 qy6 = this.i;
        if (qy6 != null) {
            qy6.e();
        } else {
            Wg6.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Qy6 qy6 = this.i;
        if (qy6 != null) {
            qy6.d();
        } else {
            Wg6.n("mSubPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String string;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        this.l = arguments != null ? arguments.getBoolean("IS_ONBOARDING_FLOW") : false;
        Bundle arguments2 = getArguments();
        String str = (arguments2 == null || (string = arguments2.getString("SERIAL", "")) == null) ? "" : string;
        Bundle arguments3 = getArguments();
        int i2 = arguments3 != null ? arguments3.getInt(w) : 0;
        this.i = new Qy6(str, this);
        this.k = new Gr4(new ArrayList());
        G37<Ub5> g37 = this.j;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                Wg6.b(constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                Wg6.b(constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                Wg6.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                Wg6.b(flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                Wg6.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new Bi(this, str));
                ViewPager2 viewPager2 = a2.F;
                Wg6.b(viewPager2, "binding.rvpTutorial");
                Gr4 gr4 = this.k;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (a2.F.getChildAt(0) != null) {
                        View childAt = a2.F.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    if (!TextUtils.isEmpty(this.u)) {
                        TabLayout tabLayout = a2.A;
                        Wg6.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                    }
                    S6();
                    a2.F.g(new Ci(a2, this, str));
                    a2.t.setOnClickListener(new Di(this, str));
                    a2.w.setOnClickListener(new Ei(this, str));
                } else {
                    Wg6.n("mAdapterUpdateFirmware");
                    throw null;
                }
            }
            if (i2 == 1) {
                t3();
            } else if (i2 == 2) {
                A(true);
            } else if (i2 == 3) {
                A(false);
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t3() {
        if (isActive()) {
            G37<Ub5> g37 = this.j;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 != null) {
                    Qy6 qy6 = this.i;
                    if (qy6 != null) {
                        qy6.f();
                        ConstraintLayout constraintLayout = a2.q;
                        Wg6.b(constraintLayout, "it.clUpdateFwFail");
                        constraintLayout.setVisibility(8);
                        ConstraintLayout constraintLayout2 = a2.r;
                        Wg6.b(constraintLayout2, "it.clUpdatingFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleButton flexibleButton = a2.s;
                        Wg6.b(flexibleButton, "it.fbContinue");
                        flexibleButton.setVisibility(0);
                        FlexibleTextView flexibleTextView = a2.z;
                        Wg6.b(flexibleTextView, "it.ftvUpdateWarning");
                        flexibleTextView.setVisibility(4);
                        FlexibleProgressBar flexibleProgressBar = a2.D;
                        Wg6.b(flexibleProgressBar, "it.progressUpdate");
                        flexibleProgressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        Wg6.b(flexibleTextView2, "it.ftvCountdownTime");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.y;
                        Wg6.b(flexibleTextView3, "it.ftvUpdate");
                        flexibleTextView3.setText(Um5.c(PortfolioApp.get.instance(), 2131886783));
                        return;
                    }
                    Wg6.n("mSubPresenter");
                    throw null;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ey6, com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
