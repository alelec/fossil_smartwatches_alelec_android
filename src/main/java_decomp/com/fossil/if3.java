package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class If3 implements Parcelable.Creator<Ue3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ue3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        float f = 0.0f;
        float f2 = 0.0f;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                f2 = Ad2.r(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                f = Ad2.r(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ue3(f2, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ue3[] newArray(int i) {
        return new Ue3[i];
    }
}
