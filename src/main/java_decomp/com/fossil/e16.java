package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E16 implements Factory<NotificationCallsAndMessagesPresenter> {
    @DexIgnore
    public static NotificationCallsAndMessagesPresenter a(Y06 y06, Uq4 uq4, D26 d26, F26 f26, G26 g26, V36 v36, An4 an4, NotificationSettingsDao notificationSettingsDao, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationCallsAndMessagesPresenter(y06, uq4, d26, f26, g26, v36, an4, notificationSettingsDao, setReplyMessageMappingUseCase, quickResponseRepository, notificationSettingsDatabase);
    }
}
