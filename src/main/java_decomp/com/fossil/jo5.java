package com.fossil;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jo5 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ List<Oo5> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ String j;

    @DexIgnore
    public Jo5(String str, List<Oo5> list, String str2, String str3, String str4, String str5, boolean z, String str6, String str7, String str8) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(str4, "faceData");
        Wg6.c(str5, "serial");
        Wg6.c(str7, "createdAt");
        Wg6.c(str8, "updatedAt");
        this.a = str;
        this.b = list;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = z;
        this.h = str6;
        this.i = str7;
        this.j = str8;
    }

    @DexIgnore
    public final List<Oo5> a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.i;
    }

    @DexIgnore
    public final String c() {
        return this.e;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Jo5) {
                Jo5 jo5 = (Jo5) obj;
                if (!Wg6.a(this.a, jo5.a) || !Wg6.a(this.b, jo5.b) || !Wg6.a(this.c, jo5.c) || !Wg6.a(this.d, jo5.d) || !Wg6.a(this.e, jo5.e) || !Wg6.a(this.f, jo5.f) || this.g != jo5.g || !Wg6.a(this.h, jo5.h) || !Wg6.a(this.i, jo5.i) || !Wg6.a(this.j, jo5.j)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.h;
    }

    @DexIgnore
    public final String g() {
        return this.d;
    }

    @DexIgnore
    public final String h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        List<Oo5> list = this.b;
        int hashCode2 = list != null ? list.hashCode() : 0;
        String str2 = this.c;
        int hashCode3 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode4 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.f;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        boolean z = this.g;
        if (z) {
            z = true;
        }
        String str6 = this.h;
        int hashCode7 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.i;
        int hashCode8 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.j;
        if (str8 != null) {
            i2 = str8.hashCode();
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i3) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2;
    }

    @DexIgnore
    public final boolean i() {
        return this.g;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id:");
        sb.append(this.a);
        sb.append("|name:");
        sb.append(this.c);
        sb.append("|originalItemIdInStore:");
        sb.append(this.h);
        sb.append("|isActive:");
        sb.append(this.g);
        sb.append("|buttons:");
        sb.append(this.b);
        sb.append("|hasData");
        sb.append(this.e.length() > 0);
        return sb.toString();
    }
}
