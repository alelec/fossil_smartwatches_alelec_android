package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dm0 extends Drawable implements Drawable.Callback, Cm0, Bm0 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode h; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public int b;
    @DexIgnore
    public PorterDuff.Mode c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Fm0 e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Drawable g;

    @DexIgnore
    public Dm0(Drawable drawable) {
        this.e = d();
        a(drawable);
    }

    @DexIgnore
    public Dm0(Fm0 fm0, Resources resources) {
        this.e = fm0;
        e(resources);
    }

    @DexIgnore
    @Override // com.fossil.Cm0
    public final void a(Drawable drawable) {
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.g = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            Fm0 fm0 = this.e;
            if (fm0 != null) {
                fm0.b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }

    @DexIgnore
    @Override // com.fossil.Cm0
    public final Drawable b() {
        return this.g;
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final Fm0 d() {
        return new Fm0(this.e);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.g.draw(canvas);
    }

    @DexIgnore
    public final void e(Resources resources) {
        Drawable.ConstantState constantState;
        Fm0 fm0 = this.e;
        if (fm0 != null && (constantState = fm0.b) != null) {
            a(constantState.newDrawable(resources));
        }
    }

    @DexIgnore
    public final boolean f(int[] iArr) {
        if (!c()) {
            return false;
        }
        Fm0 fm0 = this.e;
        ColorStateList colorStateList = fm0.c;
        PorterDuff.Mode mode = fm0.d;
        if (colorStateList == null || mode == null) {
            this.d = false;
            clearColorFilter();
            return false;
        }
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.d && colorForState == this.b && mode == this.c) {
            return false;
        }
        setColorFilter(colorForState, mode);
        this.b = colorForState;
        this.c = mode;
        this.d = true;
        return true;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        Fm0 fm0 = this.e;
        return (fm0 != null ? fm0.getChangingConfigurations() : 0) | changingConfigurations | this.g.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        Fm0 fm0 = this.e;
        if (fm0 == null || !fm0.a()) {
            return null;
        }
        this.e.a = getChangingConfigurations();
        return this.e;
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.g.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.g.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.g.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.g.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.g.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.g.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.g.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.g.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.g.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.g.isAutoMirrored();
    }

    @DexIgnore
    public boolean isStateful() {
        Fm0 fm0;
        ColorStateList colorStateList = (!c() || (fm0 = this.e) == null) ? null : fm0.c;
        return (colorStateList != null && colorStateList.isStateful()) || this.g.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        this.g.jumpToCurrentState();
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.f && super.mutate() == this) {
            this.e = d();
            Drawable drawable = this.g;
            if (drawable != null) {
                drawable.mutate();
            }
            Fm0 fm0 = this.e;
            if (fm0 != null) {
                Drawable drawable2 = this.g;
                fm0.b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.f = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.g;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.g.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.g.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        this.g.setAutoMirrored(z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.g.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.g.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.g.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.g.setFilterBitmap(z);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return f(iArr) || this.g.setState(iArr);
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTint(int i) {
        setTintList(ColorStateList.valueOf(i));
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTintList(ColorStateList colorStateList) {
        this.e.c = colorStateList;
        f(getState());
    }

    @DexIgnore
    @Override // com.fossil.Bm0
    public void setTintMode(PorterDuff.Mode mode) {
        this.e.d = mode;
        f(getState());
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.g.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
