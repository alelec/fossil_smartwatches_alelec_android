package com.fossil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Op5 extends BaseDbProvider implements Np5 {
    @DexIgnore
    public static /* final */ String a; // = "op5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements UpgradeCommand {
        @DexIgnore
        public Ai(Op5 op5) {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(Op5.a, " ---- UPGRADE DB phone_favorites_contact, rename table uappsystemversion to PhoneFavoritesContact");
            sQLiteDatabase.execSQL("ALTER TABLE uappsystemversion RENAME TO PhoneFavoritesContact");
            FLogger.INSTANCE.getLocal().d(Op5.a, " ---- UPGRADE DB phone_favorites_contact, rename table uappsystemversion to PhoneFavoritesContact SUCCESS");
        }
    }

    @DexIgnore
    public Op5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.Np5
    public void g(PhoneFavoritesContact phoneFavoritesContact) {
        if (phoneFavoritesContact != null) {
            try {
                o().createOrUpdate(phoneFavoritesContact);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, "addOrUpdatePhoneFavoritesContact - number= " + phoneFavoritesContact.getPhoneNumber());
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = a;
                local2.e(str2, "addOrUpdatePhoneFavoritesContact - e= " + e);
            }
        } else {
            FLogger.INSTANCE.getLocal().e(a, "addOrUpdatePhoneFavoritesContact - contact=NULL!!!");
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{PhoneFavoritesContact.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        HashMap hashMap = new HashMap();
        hashMap.put(2, new Ai(this));
        return hashMap;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.Np5
    public void i() {
        FLogger.INSTANCE.getLocal().d(a, "removeAllPhoneFavoritesContacts");
        try {
            o().deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.e(str, "removeAllPhoneFavoritesContacts - e=" + e);
        }
    }

    @DexIgnore
    public final Dao<PhoneFavoritesContact, Integer> o() throws SQLException {
        return this.databaseHelper.getDao(PhoneFavoritesContact.class);
    }

    @DexIgnore
    @Override // com.fossil.Np5
    public void removePhoneFavoritesContact(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "removePhoneFavoritesContact - number=" + str);
        try {
            DeleteBuilder<PhoneFavoritesContact, Integer> deleteBuilder = o().deleteBuilder();
            deleteBuilder.where().eq(PhoneFavoritesContact.COLUMN_PHONE_NUMBER, str);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.e(str3, "removePhoneFavoritesContact - e=" + e);
        }
    }
}
