package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.chart.HeartRateSleepSessionChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class An6 extends RecyclerView.g<Bi> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public ArrayList<Ai> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ ArrayList<V57> a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ short c;
        @DexIgnore
        public /* final */ short d;

        @DexIgnore
        public Ai() {
            this(null, 0, 0, 0, 15, null);
        }

        @DexIgnore
        public Ai(ArrayList<V57> arrayList, int i, short s, short s2) {
            Wg6.c(arrayList, "heartRateSessionData");
            this.a = arrayList;
            this.b = i;
            this.c = (short) s;
            this.d = (short) s2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(ArrayList arrayList, int i, short s, short s2, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? new ArrayList() : arrayList, (i2 & 2) != 0 ? 1 : i, (i2 & 4) != 0 ? 0 : s, (i2 & 8) != 0 ? 0 : s2);
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<V57> b() {
            return this.a;
        }

        @DexIgnore
        public final short c() {
            return this.d;
        }

        @DexIgnore
        public final short d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(Wg6.a(this.a, ai.a) && this.b == ai.b && this.c == ai.c && this.d == ai.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            ArrayList<V57> arrayList = this.a;
            return ((((((arrayList != null ? arrayList.hashCode() : 0) * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return "SleepHeartRateUIData(heartRateSessionData=" + this.a + ", duration=" + this.b + ", minHR=" + ((int) this.c) + ", maxHR=" + ((int) this.d) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ HeartRateSleepSessionChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(View view) {
            super(view);
            Wg6.c(view, "itemView");
            View findViewById = view.findViewById(2131362590);
            Wg6.b(findViewById, "itemView.findViewById(R.id.hrssc)");
            this.a = (HeartRateSleepSessionChart) findViewById;
        }

        @DexIgnore
        public final HeartRateSleepSessionChart a() {
            return this.a;
        }
    }

    @DexIgnore
    public An6(ArrayList<Ai> arrayList) {
        Wg6.c(arrayList, "data");
        this.d = arrayList;
        String d2 = ThemeManager.l.a().d("awakeSleep");
        this.a = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("lightSleep");
        this.b = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("deepSleep");
        this.c = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
    }

    @DexIgnore
    public void g(Bi bi, int i) {
        Wg6.c(bi, "holder");
        Ai ai = this.d.get(i);
        Wg6.b(ai, "data[position]");
        Ai ai2 = ai;
        bi.a().setMDuration(ai2.a());
        bi.a().setMMinHRValue(ai2.d());
        bi.a().setMMaxHRValue(ai2.c());
        bi.a().q(this.c, this.b, this.a);
        bi.a().m(ai2.b());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.d.size();
    }

    @DexIgnore
    public Bi h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558694, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Bi(inflate);
    }

    @DexIgnore
    public final void i(ArrayList<Ai> arrayList) {
        Wg6.c(arrayList, "data");
        this.d = arrayList;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        g(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }
}
