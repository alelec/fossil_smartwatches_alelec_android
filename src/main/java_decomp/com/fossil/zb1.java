package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zb1 extends FilterInputStream {
    @DexIgnore
    public static /* final */ byte[] d;
    @DexIgnore
    public static /* final */ int e;
    @DexIgnore
    public static /* final */ int f;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public int c;

    /*
    static {
        byte[] bArr = {-1, -31, 0, 28, 69, Framer.EXIT_FRAME_PREFIX, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, DateTimeFieldType.MINUTE_OF_DAY, 0, 2, 0, 0, 0, 1, 0};
        d = bArr;
        int length = bArr.length;
        e = length;
        f = length + 2;
    }
    */

    @DexIgnore
    public Zb1(InputStream inputStream, int i) {
        super(inputStream);
        if (i < -1 || i > 8) {
            throw new IllegalArgumentException("Cannot add invalid orientation: " + i);
        }
        this.b = (byte) ((byte) i);
    }

    @DexIgnore
    public void mark(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean markSupported() {
        return false;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read() throws IOException {
        int i;
        int i2 = this.c;
        int read = (i2 < 2 || i2 > (i = f)) ? super.read() : i2 == i ? this.b : d[i2 - 2] & 255;
        if (read != -1) {
            this.c++;
        }
        return read;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int min;
        int i3 = this.c;
        int i4 = f;
        if (i3 > i4) {
            min = super.read(bArr, i, i2);
        } else if (i3 == i4) {
            bArr[i] = (byte) this.b;
            min = 1;
        } else if (i3 < 2) {
            min = super.read(bArr, i, 2 - i3);
        } else {
            min = Math.min(i4 - i3, i2);
            System.arraycopy(d, this.c - 2, bArr, i, min);
        }
        if (min > 0) {
            this.c += min;
        }
        return min;
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.io.FilterInputStream, java.io.InputStream
    public long skip(long j) throws IOException {
        long skip = super.skip(j);
        if (skip > 0) {
            this.c = (int) (((long) this.c) + skip);
        }
        return skip;
    }
}
