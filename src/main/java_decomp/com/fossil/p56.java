package com.fossil;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface P56 extends Gq4<O56> {
    @DexIgnore
    Object A3();  // void declaration

    @DexIgnore
    void I(ArrayList<J06> arrayList);

    @DexIgnore
    void T(Cursor cursor);

    @DexIgnore
    Object V();  // void declaration

    @DexIgnore
    void n1(List<J06> list, FilterQueryProvider filterQueryProvider, int i);
}
