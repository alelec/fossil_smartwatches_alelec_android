package com.fossil;

import com.mapped.Af6;
import com.mapped.Qg6;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class I08 extends Mw7 {
    @DexIgnore
    public G08 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public I08(int i, int i2, long j, String str) {
        this.d = i;
        this.e = i2;
        this.f = j;
        this.g = str;
        this.c = V();
    }

    @DexIgnore
    public I08(int i, int i2, String str) {
        this(i, i2, Q08.d, str);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ I08(int i, int i2, String str, int i3, Qg6 qg6) {
        this((i3 & 1) != 0 ? Q08.b : i, (i3 & 2) != 0 ? Q08.c : i2, (i3 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        try {
            G08.k(this.c, runnable, null, false, 6, null);
        } catch (RejectedExecutionException e2) {
            Pv7.i.M(af6, runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void P(Af6 af6, Runnable runnable) {
        try {
            G08.k(this.c, runnable, null, true, 2, null);
        } catch (RejectedExecutionException e2) {
            Pv7.i.P(af6, runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.Mw7
    public Executor S() {
        return this.c;
    }

    @DexIgnore
    public final Dv7 T(int i) {
        if (i > 0) {
            return new K08(this, i, 1);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i).toString());
    }

    @DexIgnore
    public final G08 V() {
        return new G08(this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public final void X(Runnable runnable, O08 o08, boolean z) {
        try {
            this.c.j(runnable, o08, z);
        } catch (RejectedExecutionException e2) {
            Pv7.i.z0(this.c.f(runnable, o08));
        }
    }
}
