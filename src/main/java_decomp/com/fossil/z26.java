package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z26 extends BaseFragment implements Y26 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public X26 g;
    @DexIgnore
    public G37<N95> h;
    @DexIgnore
    public R26 i;
    @DexIgnore
    public I36 j;
    @DexIgnore
    public InactivityNudgeTimePresenter k;
    @DexIgnore
    public RemindTimePresenter l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Z26.s;
        }

        @DexIgnore
        public final Z26 b() {
            return new Z26();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Bi(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Ci(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Di(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Ei(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Fi(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Gi(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            P47.l(view);
            R26 r26 = this.b.i;
            if (r26 != null) {
                r26.C6(0);
            }
            R26 r262 = this.b.i;
            if (r262 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                r262.show(childFragmentManager, R26.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Hi(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            P47.l(view);
            R26 r26 = this.b.i;
            if (r26 != null) {
                r26.C6(1);
            }
            R26 r262 = this.b.i;
            if (r262 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                r262.show(childFragmentManager, R26.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Ii(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            P47.l(view);
            I36 i36 = this.b.j;
            if (i36 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                i36.show(childFragmentManager, I36.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z26 b;

        @DexIgnore
        public Ji(Z26 z26) {
            this.b = z26;
        }

        @DexIgnore
        public final void onClick(View view) {
            Z26.L6(this.b).r();
        }
    }

    /*
    static {
        String simpleName = Z26.class.getSimpleName();
        Wg6.b(simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ X26 L6(Z26 z26) {
        X26 x26 = z26.g;
        if (x26 != null) {
            return x26;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        X26 x26 = this.g;
        if (x26 != null) {
            x26.r();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(X26 x26) {
        O6(x26);
    }

    @DexIgnore
    public void O6(X26 x26) {
        Wg6.c(x26, "presenter");
        this.g = x26;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void S1(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.T) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void Y(boolean z) {
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
                if (z) {
                    FlexibleButton flexibleButton2 = a2.v;
                    if (flexibleButton2 != null) {
                        flexibleButton2.d("flexible_button_primary");
                        return;
                    }
                    return;
                }
                FlexibleButton flexibleButton3 = a2.v;
                if (flexibleButton3 != null) {
                    flexibleButton3.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void Y5(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        Wg6.c(spannableString, LogBuilder.KEY_TIME);
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.A) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void b6(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, LogBuilder.KEY_TIME);
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.H) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void n4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.U) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        N95 n95 = (N95) Aq0.f(layoutInflater, 2131558597, viewGroup, false, A6());
        R26 r26 = (R26) getChildFragmentManager().Z(R26.u.a());
        this.i = r26;
        if (r26 == null) {
            this.i = R26.u.b();
        }
        I36 i36 = (I36) getChildFragmentManager().Z(I36.u.a());
        this.j = i36;
        if (i36 == null) {
            this.j = I36.u.b();
        }
        String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            n95.V.setBackgroundColor(parseColor);
            n95.W.setBackgroundColor(parseColor);
            n95.X.setBackgroundColor(parseColor);
        }
        FlexibleTextView flexibleTextView = n95.K;
        Wg6.b(flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setSelected(true);
        n95.L.setOnClickListener(new Bi(this));
        n95.S.setOnClickListener(new Ci(this));
        n95.T.setOnClickListener(new Di(this));
        n95.U.setOnClickListener(new Ei(this));
        n95.R.setOnClickListener(new Fi(this));
        n95.N.setOnClickListener(new Gi(this));
        n95.M.setOnClickListener(new Hi(this));
        n95.O.setOnClickListener(new Ii(this));
        n95.v.setOnClickListener(new Ji(this));
        this.h = new G37<>(this, n95);
        Iface iface = PortfolioApp.get.instance().getIface();
        R26 r262 = this.i;
        if (r262 != null) {
            I36 i362 = this.j;
            if (i362 != null) {
                iface.j1(new N36(r262, i362)).a(this);
                E6("reminder_view");
                Wg6.b(n95, "binding");
                return n95.n();
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        X26 x26 = this.g;
        if (x26 != null) {
            x26.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
            }
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        X26 x26 = this.g;
        if (x26 != null) {
            x26.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.t;
                Wg6.b(constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                FlexibleButton flexibleButton = a2.v;
                Wg6.b(flexibleButton, "fbSave");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void s1(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.R) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void t5(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        Wg6.c(spannableString, LogBuilder.KEY_TIME);
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.C) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y26
    public void w4(boolean z) {
        G37<N95> g37 = this.h;
        if (g37 != null) {
            N95 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.S;
                if (flexibleSwitchCompat != null) {
                    flexibleSwitchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.r;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.r;
                Wg6.b(constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.r.getChildAt(i2);
                    Wg6.b(childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }
}
