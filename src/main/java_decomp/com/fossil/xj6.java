package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xj6 implements MembersInjector<SleepOverviewFragment> {
    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewDayPresenter sleepOverviewDayPresenter) {
        sleepOverviewFragment.h = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public static void b(SleepOverviewFragment sleepOverviewFragment, SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        sleepOverviewFragment.j = sleepOverviewMonthPresenter;
    }

    @DexIgnore
    public static void c(SleepOverviewFragment sleepOverviewFragment, SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
        sleepOverviewFragment.i = sleepOverviewWeekPresenter;
    }
}
