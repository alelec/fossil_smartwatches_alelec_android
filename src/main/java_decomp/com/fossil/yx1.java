package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yx1 extends Ox1 {
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public static /* final */ String JSON_ERROR_CODE_KEY; // = "error_code";
    @DexIgnore
    public /* final */ Zx1 errorCode;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public Yx1(Zx1 zx1) {
        Wg6.c(zx1, "errorCode");
        this.errorCode = zx1;
    }

    @DexIgnore
    public Zx1 getErrorCode() {
        return this.errorCode;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("error_code", getErrorCode().getLogName());
        } catch (JSONException e) {
        }
        return jSONObject;
    }
}
