package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ps4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    @Vu3("id")
    public String b;
    @DexIgnore
    @Vu3("challengeType")
    public String c;
    @DexIgnore
    @Vu3("name")
    public String d;
    @DexIgnore
    @Vu3("description")
    public String e;
    @DexIgnore
    @Vu3("owner")
    public Ht4 f;
    @DexIgnore
    @Vu3("numberOfPlayers")
    public Integer g;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_START_TIME)
    public Date h;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_END_TIME)
    public Date i;
    @DexIgnore
    @Vu3("target")
    public Integer j;
    @DexIgnore
    @Vu3("duration")
    public Integer k;
    @DexIgnore
    @Vu3(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String l;
    @DexIgnore
    @Vu3("version")
    public String m;
    @DexIgnore
    @Vu3("status")
    public String s;
    @DexIgnore
    @Vu3("syncStatusDataBase64")
    public String t;
    @DexIgnore
    @Vu3("createdAt")
    public Date u;
    @DexIgnore
    @Vu3("updatedAt")
    public Date v;
    @DexIgnore
    public String w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ps4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Ps4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ps4(parcel);
        }

        @DexIgnore
        public Ps4[] b(int i) {
            return new Ps4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ps4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ps4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ps4(android.os.Parcel r25) {
        /*
            r24 = this;
            java.lang.String r4 = "parcel"
            r0 = r25
            com.mapped.Wg6.c(r0, r4)
            java.lang.String r5 = r25.readString()
            r14 = 0
            if (r5 == 0) goto L_0x00a9
            java.lang.String r4 = "parcel.readString()!!"
            com.mapped.Wg6.b(r5, r4)
            java.lang.String r6 = r25.readString()
            java.lang.String r7 = r25.readString()
            java.lang.String r8 = r25.readString()
            java.lang.Class<com.fossil.Ht4> r4 = com.fossil.Ht4.class
            java.lang.ClassLoader r4 = r4.getClassLoader()
            r0 = r25
            android.os.Parcelable r9 = r0.readParcelable(r4)
            com.fossil.Ht4 r9 = (com.fossil.Ht4) r9
            java.lang.Class r4 = java.lang.Integer.TYPE
            java.lang.ClassLoader r4 = r4.getClassLoader()
            r0 = r25
            java.lang.Object r10 = r0.readValue(r4)
            boolean r4 = r10 instanceof java.lang.Integer
            if (r4 != 0) goto L_0x003e
            r10 = 0
        L_0x003e:
            java.lang.Integer r10 = (java.lang.Integer) r10
            java.util.Date r11 = new java.util.Date
            long r12 = r25.readLong()
            r11.<init>(r12)
            java.util.Date r12 = new java.util.Date
            long r16 = r25.readLong()
            r0 = r16
            r12.<init>(r0)
            java.lang.Class r4 = java.lang.Integer.TYPE
            java.lang.ClassLoader r4 = r4.getClassLoader()
            r0 = r25
            java.lang.Object r13 = r0.readValue(r4)
            boolean r4 = r13 instanceof java.lang.Integer
            if (r4 != 0) goto L_0x0065
            r13 = 0
        L_0x0065:
            java.lang.Integer r13 = (java.lang.Integer) r13
            java.lang.Class r4 = java.lang.Integer.TYPE
            java.lang.ClassLoader r4 = r4.getClassLoader()
            r0 = r25
            java.lang.Object r4 = r0.readValue(r4)
            boolean r15 = r4 instanceof java.lang.Integer
            if (r15 != 0) goto L_0x00ae
        L_0x0077:
            java.lang.Integer r14 = (java.lang.Integer) r14
            java.lang.String r15 = r25.readString()
            java.lang.String r16 = r25.readString()
            java.lang.String r17 = r25.readString()
            java.lang.String r18 = r25.readString()
            java.util.Date r19 = new java.util.Date
            long r20 = r25.readLong()
            r19.<init>(r20)
            java.util.Date r20 = new java.util.Date
            long r22 = r25.readLong()
            r0 = r20
            r1 = r22
            r0.<init>(r1)
            java.lang.String r21 = r25.readString()
            r4 = r24
            r4.<init>(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21)
            return
        L_0x00a9:
            com.mapped.Wg6.i()
            r4 = 0
            throw r4
        L_0x00ae:
            r14 = r4
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ps4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Ps4(String str, String str2, String str3, String str4, Ht4 ht4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, String str8, Date date3, Date date4, String str9) {
        Wg6.c(str, "id");
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = ht4;
        this.g = num;
        this.h = date;
        this.i = date2;
        this.j = num2;
        this.k = num3;
        this.l = str5;
        this.m = str6;
        this.s = str7;
        this.t = str8;
        this.u = date3;
        this.v = date4;
        this.w = str9;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ps4(String str, String str2, String str3, String str4, Ht4 ht4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, String str8, Date date3, Date date4, String str9, int i2, Qg6 qg6) {
        this(str, str2, str3, str4, ht4, num, date, date2, num2, num3, str5, str6, str7, str8, date3, date4, (65536 & i2) != 0 ? "joined_challenge" : str9);
    }

    @DexIgnore
    public final String a() {
        return this.w;
    }

    @DexIgnore
    public final Date b() {
        return this.u;
    }

    @DexIgnore
    public final String c() {
        return this.e;
    }

    @DexIgnore
    public final Integer d() {
        return this.k;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Date e() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ps4) {
                Ps4 ps4 = (Ps4) obj;
                if (!Wg6.a(this.b, ps4.b) || !Wg6.a(this.c, ps4.c) || !Wg6.a(this.d, ps4.d) || !Wg6.a(this.e, ps4.e) || !Wg6.a(this.f, ps4.f) || !Wg6.a(this.g, ps4.g) || !Wg6.a(this.h, ps4.h) || !Wg6.a(this.i, ps4.i) || !Wg6.a(this.j, ps4.j) || !Wg6.a(this.k, ps4.k) || !Wg6.a(this.l, ps4.l) || !Wg6.a(this.m, ps4.m) || !Wg6.a(this.s, ps4.s) || !Wg6.a(this.t, ps4.t) || !Wg6.a(this.u, ps4.u) || !Wg6.a(this.v, ps4.v) || !Wg6.a(this.w, ps4.w)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.b;
    }

    @DexIgnore
    public final String g() {
        return this.d;
    }

    @DexIgnore
    public final Integer h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.b;
        int i2 = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.d;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.e;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        Ht4 ht4 = this.f;
        int hashCode5 = ht4 != null ? ht4.hashCode() : 0;
        Integer num = this.g;
        int hashCode6 = num != null ? num.hashCode() : 0;
        Date date = this.h;
        int hashCode7 = date != null ? date.hashCode() : 0;
        Date date2 = this.i;
        int hashCode8 = date2 != null ? date2.hashCode() : 0;
        Integer num2 = this.j;
        int hashCode9 = num2 != null ? num2.hashCode() : 0;
        Integer num3 = this.k;
        int hashCode10 = num3 != null ? num3.hashCode() : 0;
        String str5 = this.l;
        int hashCode11 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.m;
        int hashCode12 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.s;
        int hashCode13 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.t;
        int hashCode14 = str8 != null ? str8.hashCode() : 0;
        Date date3 = this.u;
        int hashCode15 = date3 != null ? date3.hashCode() : 0;
        Date date4 = this.v;
        int hashCode16 = date4 != null ? date4.hashCode() : 0;
        String str9 = this.w;
        if (str9 != null) {
            i2 = str9.hashCode();
        }
        return (((((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + i2;
    }

    @DexIgnore
    public final Ht4 i() {
        return this.f;
    }

    @DexIgnore
    public final String k() {
        return this.l;
    }

    @DexIgnore
    public final Date m() {
        return this.h;
    }

    @DexIgnore
    public final String n() {
        return this.s;
    }

    @DexIgnore
    public final String p() {
        return this.t;
    }

    @DexIgnore
    public final Integer q() {
        return this.j;
    }

    @DexIgnore
    public final String r() {
        return this.c;
    }

    @DexIgnore
    public final Date s() {
        return this.v;
    }

    @DexIgnore
    public final String t() {
        return this.m;
    }

    @DexIgnore
    public String toString() {
        return "Challenge(id=" + this.b + ", type=" + this.c + ", name=" + this.d + ", des=" + this.e + ", owner=" + this.f + ", numberOfPlayers=" + this.g + ", startTime=" + this.h + ", endTime=" + this.i + ", target=" + this.j + ", duration=" + this.k + ", privacy=" + this.l + ", version=" + this.m + ", status=" + this.s + ", syncData=" + this.t + ", createdAt=" + this.u + ", updatedAt=" + this.v + ", category=" + this.w + ")";
    }

    @DexIgnore
    public final void u(Integer num) {
        this.g = num;
    }

    @DexIgnore
    public final void v(String str) {
        this.s = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeParcelable(this.f, i2);
        parcel.writeValue(this.g);
        Date date = this.h;
        if (date != null) {
            parcel.writeLong(date.getTime());
        }
        Date date2 = this.i;
        if (date2 != null) {
            parcel.writeLong(date2.getTime());
        }
        parcel.writeValue(this.j);
        parcel.writeValue(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeString(this.s);
        parcel.writeString(this.t);
        Date date3 = this.u;
        if (date3 != null) {
            parcel.writeLong(date3.getTime());
        }
        Date date4 = this.v;
        if (date4 != null) {
            parcel.writeLong(date4.getTime());
        }
        parcel.writeString(this.w);
    }
}
