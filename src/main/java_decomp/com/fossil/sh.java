package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sh extends Cq {
    @DexIgnore
    public /* final */ ArrayList<Ow> E; // = By1.a(this.C, Hm7.c(Ow.g));
    @DexIgnore
    public /* final */ Ho1 F;

    @DexIgnore
    public Sh(K5 k5, I60 i60, Ho1 ho1) {
        super(k5, i60, Yp.p0, new Us(ho1, k5));
        this.F = ho1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Cq
    public JSONObject C() {
        return G80.k(super.C(), Jd0.e4, this.F.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Cq
    public ArrayList<Ow> z() {
        return this.E;
    }
}
