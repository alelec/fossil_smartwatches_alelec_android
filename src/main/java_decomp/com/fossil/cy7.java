package com.fossil;

import com.mapped.Af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cy7 extends Dv7 {
    @DexIgnore
    public static /* final */ Cy7 c; // = new Cy7();

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        Ey7 ey7 = (Ey7) af6.get(Ey7.c);
        if (ey7 != null) {
            ey7.b = true;
            return;
        }
        throw new UnsupportedOperationException("Dispatchers.Unconfined.dispatch function can only be used by the yield function. If you wrap Unconfined dispatcher in your code, make sure you properly delegate isDispatchNeeded and dispatch calls.");
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public boolean Q(Af6 af6) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        return "Unconfined";
    }
}
