package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ye5 extends Xe5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362402, 1);
        E.put(2131362401, 2);
        E.put(2131362783, 3);
        E.put(2131362056, 4);
        E.put(2131362517, 5);
        E.put(2131362516, 6);
        E.put(2131363111, 7);
        E.put(2131362482, 8);
        E.put(2131362481, 9);
        E.put(2131362492, 10);
        E.put(2131362462, 11);
    }
    */

    @DexIgnore
    public Ye5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public Ye5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (View) objArr[3], (View) objArr[7]);
        this.C = -1;
        this.r.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
