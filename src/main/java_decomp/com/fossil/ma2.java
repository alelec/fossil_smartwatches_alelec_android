package com.fossil;

import android.os.RemoteException;
import com.fossil.L72;
import com.fossil.P72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ma2 extends V92<Boolean> {
    @DexIgnore
    public /* final */ P72.Ai<?> c;

    @DexIgnore
    public Ma2(P72.Ai<?> ai, Ot3<Boolean> ot3) {
        super(4, ot3);
        this.c = ai;
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final /* bridge */ /* synthetic */ void d(A82 a82, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final B62[] g(L72.Ai<?> ai) {
        U92 u92 = ai.A().get(this.c);
        if (u92 == null) {
            return null;
        }
        return u92.a.c();
    }

    @DexIgnore
    @Override // com.fossil.Ja2
    public final boolean h(L72.Ai<?> ai) {
        U92 u92 = ai.A().get(this.c);
        return u92 != null && u92.a.e();
    }

    @DexIgnore
    @Override // com.fossil.V92
    public final void i(L72.Ai<?> ai) throws RemoteException {
        U92 remove = ai.A().remove(this.c);
        if (remove != null) {
            remove.b.b(ai.R(), this.b);
            remove.a.a();
            return;
        }
        this.b.e((T) Boolean.FALSE);
    }
}
