package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.Ig0;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zf0 extends Gg0 implements Ig0, View.OnKeyListener, PopupWindow.OnDismissListener {
    @DexIgnore
    public static /* final */ int H; // = Re0.abc_cascading_menu_item_layout;
    @DexIgnore
    public int A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public Ig0.Ai D;
    @DexIgnore
    public ViewTreeObserver E;
    @DexIgnore
    public PopupWindow.OnDismissListener F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ List<Cg0> i; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Di> j; // = new ArrayList();
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener k; // = new Ai();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener l; // = new Bi();
    @DexIgnore
    public /* final */ Hh0 m; // = new Ci();
    @DexIgnore
    public int s; // = 0;
    @DexIgnore
    public int t; // = 0;
    @DexIgnore
    public View u;
    @DexIgnore
    public View v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (Zf0.this.a() && Zf0.this.j.size() > 0 && !Zf0.this.j.get(0).a.w()) {
                View view = Zf0.this.v;
                if (view == null || !view.isShown()) {
                    Zf0.this.dismiss();
                    return;
                }
                for (Di di : Zf0.this.j) {
                    di.a.show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnAttachStateChangeListener {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = Zf0.this.E;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    Zf0.this.E = view.getViewTreeObserver();
                }
                Zf0 zf0 = Zf0.this;
                zf0.E.removeGlobalOnLayoutListener(zf0.k);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Hh0 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;
            @DexIgnore
            public /* final */ /* synthetic */ MenuItem c;
            @DexIgnore
            public /* final */ /* synthetic */ Cg0 d;

            @DexIgnore
            public Aii(Di di, MenuItem menuItem, Cg0 cg0) {
                this.b = di;
                this.c = menuItem;
                this.d = cg0;
            }

            @DexIgnore
            public void run() {
                Di di = this.b;
                if (di != null) {
                    Zf0.this.G = true;
                    di.b.e(false);
                    Zf0.this.G = false;
                }
                if (this.c.isEnabled() && this.c.hasSubMenu()) {
                    this.d.N(this.c, 4);
                }
            }
        }

        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.Hh0
        public void d(Cg0 cg0, MenuItem menuItem) {
            int i;
            Zf0.this.h.removeCallbacksAndMessages(null);
            int size = Zf0.this.j.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (cg0 == Zf0.this.j.get(i2).b) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                Zf0.this.h.postAtTime(new Aii(i3 < Zf0.this.j.size() ? Zf0.this.j.get(i3) : null, menuItem, cg0), cg0, SystemClock.uptimeMillis() + 200);
            }
        }

        @DexIgnore
        @Override // com.fossil.Hh0
        public void g(Cg0 cg0, MenuItem menuItem) {
            Zf0.this.h.removeCallbacksAndMessages(cg0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ Ih0 a;
        @DexIgnore
        public /* final */ Cg0 b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Di(Ih0 ih0, Cg0 cg0, int i) {
            this.a = ih0;
            this.b = cg0;
            this.c = i;
        }

        @DexIgnore
        public ListView a() {
            return this.a.j();
        }
    }

    @DexIgnore
    public Zf0(Context context, View view, int i2, int i3, boolean z2) {
        this.c = context;
        this.u = view;
        this.e = i2;
        this.f = i3;
        this.g = z2;
        this.B = false;
        this.w = E();
        Resources resources = context.getResources();
        this.d = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(Oe0.abc_config_prefDialogWidth));
        this.h = new Handler();
    }

    @DexIgnore
    public final Ih0 A() {
        Ih0 ih0 = new Ih0(this.c, null, this.e, this.f);
        ih0.O(this.m);
        ih0.G(this);
        ih0.F(this);
        ih0.y(this.u);
        ih0.B(this.t);
        ih0.E(true);
        ih0.D(2);
        return ih0;
    }

    @DexIgnore
    public final int B(Cg0 cg0) {
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (cg0 == this.j.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public final MenuItem C(Cg0 cg0, Cg0 cg02) {
        int size = cg0.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = cg0.getItem(i2);
            if (item.hasSubMenu() && cg02 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @DexIgnore
    public final View D(Di di, Cg0 cg0) {
        Bg0 bg0;
        int i2;
        int i3;
        int i4 = 0;
        MenuItem C2 = C(di.b, cg0);
        if (C2 == null) {
            return null;
        }
        ListView a2 = di.a();
        ListAdapter adapter = a2.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            bg0 = (Bg0) headerViewListAdapter.getWrappedAdapter();
        } else {
            bg0 = (Bg0) adapter;
            i2 = 0;
        }
        int count = bg0.getCount();
        while (true) {
            if (i4 >= count) {
                i3 = -1;
                break;
            } else if (C2 == bg0.c(i4)) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a2.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a2.getChildCount()) {
            return null;
        }
        return a2.getChildAt(firstVisiblePosition);
    }

    @DexIgnore
    public final int E() {
        return Mo0.z(this.u) == 1 ? 0 : 1;
    }

    @DexIgnore
    public final int F(int i2) {
        List<Di> list = this.j;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.v.getWindowVisibleDisplayFrame(rect);
        if (this.w != 1) {
            return iArr[0] - i2 < 0 ? 1 : 0;
        }
        return (a2.getWidth() + iArr[0]) + i2 > rect.right ? 0 : 1;
    }

    @DexIgnore
    public final void G(Cg0 cg0) {
        View view;
        Di di;
        int i2;
        int i3;
        int width;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.c);
        Bg0 bg0 = new Bg0(cg0, from, this.g, H);
        if (!a() && this.B) {
            bg0.d(true);
        } else if (a()) {
            bg0.d(Gg0.y(cg0));
        }
        int p = Gg0.p(bg0, null, this.c, this.d);
        Ih0 A2 = A();
        A2.o(bg0);
        A2.A(p);
        A2.B(this.t);
        if (this.j.size() > 0) {
            List<Di> list = this.j;
            Di di2 = list.get(list.size() - 1);
            view = D(di2, cg0);
            di = di2;
        } else {
            view = null;
            di = null;
        }
        if (view != null) {
            A2.P(false);
            A2.M(null);
            int F2 = F(p);
            boolean z2 = F2 == 1;
            this.w = F2;
            if (Build.VERSION.SDK_INT >= 26) {
                A2.y(view);
                i2 = 0;
                i3 = 0;
            } else {
                int[] iArr = new int[2];
                this.u.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.t & 7) == 5) {
                    iArr[0] = iArr[0] + this.u.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.t & 5) != 5) {
                if (z2) {
                    width = view.getWidth();
                }
                i4 = i2 - p;
                A2.e(i4);
                A2.H(true);
                A2.k(i3);
            } else if (z2) {
                width = p;
            } else {
                p = view.getWidth();
                i4 = i2 - p;
                A2.e(i4);
                A2.H(true);
                A2.k(i3);
            }
            i4 = width + i2;
            A2.e(i4);
            A2.H(true);
            A2.k(i3);
        } else {
            if (this.x) {
                A2.e(this.z);
            }
            if (this.y) {
                A2.k(this.A);
            }
            A2.C(o());
        }
        this.j.add(new Di(A2, cg0, this.w));
        A2.show();
        ListView j2 = A2.j();
        j2.setOnKeyListener(this);
        if (di == null && this.C && cg0.z() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(Re0.abc_popup_menu_header_item_layout, (ViewGroup) j2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(cg0.z());
            j2.addHeaderView(frameLayout, null, false);
            A2.show();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public boolean a() {
        return this.j.size() > 0 && this.j.get(0).a.a();
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z2) {
        int B2 = B(cg0);
        if (B2 >= 0) {
            int i2 = B2 + 1;
            if (i2 < this.j.size()) {
                this.j.get(i2).b.e(false);
            }
            Di remove = this.j.remove(B2);
            remove.b.Q(this);
            if (this.G) {
                remove.a.N(null);
                remove.a.z(0);
            }
            remove.a.dismiss();
            int size = this.j.size();
            if (size > 0) {
                this.w = this.j.get(size - 1).c;
            } else {
                this.w = E();
            }
            if (size == 0) {
                dismiss();
                Ig0.Ai ai = this.D;
                if (ai != null) {
                    ai.b(cg0, true);
                }
                ViewTreeObserver viewTreeObserver = this.E;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.E.removeGlobalOnLayoutListener(this.k);
                    }
                    this.E = null;
                }
                this.v.removeOnAttachStateChangeListener(this.l);
                this.F.onDismiss();
            } else if (z2) {
                this.j.get(0).b.e(false);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z2) {
        for (Di di : this.j) {
            Gg0.z(di.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public void dismiss() {
        int size = this.j.size();
        if (size > 0) {
            Di[] diArr = (Di[]) this.j.toArray(new Di[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                Di di = diArr[i2];
                if (di.a.a()) {
                    di.a.dismiss();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
        this.D = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public ListView j() {
        if (this.j.isEmpty()) {
            return null;
        }
        List<Di> list = this.j;
        return list.get(list.size() - 1).a();
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        for (Di di : this.j) {
            if (ng0 == di.b) {
                di.a().requestFocus();
                return true;
            }
        }
        if (!ng0.hasVisibleItems()) {
            return false;
        }
        m(ng0);
        Ig0.Ai ai = this.D;
        if (ai != null) {
            ai.c(ng0);
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void m(Cg0 cg0) {
        cg0.c(this, this.c);
        if (a()) {
            G(cg0);
        } else {
            this.i.add(cg0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public boolean n() {
        return false;
    }

    @DexIgnore
    public void onDismiss() {
        Di di;
        int size = this.j.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                di = null;
                break;
            }
            di = this.j.get(i2);
            if (!di.a.a()) {
                break;
            }
            i2++;
        }
        if (di != null) {
            di.b.e(false);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void q(View view) {
        if (this.u != view) {
            this.u = view;
            this.t = Wn0.b(this.s, Mo0.z(view));
        }
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void s(boolean z2) {
        this.B = z2;
    }

    @DexIgnore
    @Override // com.fossil.Lg0
    public void show() {
        if (!a()) {
            for (Cg0 cg0 : this.i) {
                G(cg0);
            }
            this.i.clear();
            View view = this.u;
            this.v = view;
            if (view != null) {
                boolean z2 = this.E == null;
                ViewTreeObserver viewTreeObserver = this.v.getViewTreeObserver();
                this.E = viewTreeObserver;
                if (z2) {
                    viewTreeObserver.addOnGlobalLayoutListener(this.k);
                }
                this.v.addOnAttachStateChangeListener(this.l);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void t(int i2) {
        if (this.s != i2) {
            this.s = i2;
            this.t = Wn0.b(i2, Mo0.z(this.u));
        }
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void u(int i2) {
        this.x = true;
        this.z = i2;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void v(PopupWindow.OnDismissListener onDismissListener) {
        this.F = onDismissListener;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void w(boolean z2) {
        this.C = z2;
    }

    @DexIgnore
    @Override // com.fossil.Gg0
    public void x(int i2) {
        this.y = true;
        this.A = i2;
    }
}
