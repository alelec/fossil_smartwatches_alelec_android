package com.fossil;

import com.mapped.Wg6;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oz5 {
    @DexIgnore
    public /* final */ Ld6 a;
    @DexIgnore
    public /* final */ Y66 b;
    @DexIgnore
    public /* final */ Lb6 c;
    @DexIgnore
    public /* final */ Ao6 d;
    @DexIgnore
    public /* final */ A06 e;
    @DexIgnore
    public /* final */ Y36 f;
    @DexIgnore
    public /* final */ Sn6 g;

    @DexIgnore
    public Oz5(Ld6 ld6, Y66 y66, Lb6 lb6, Ao6 ao6, A06 a06, Y36 y36, Sn6 sn6) {
        Wg6.c(ld6, "mDashboardView");
        Wg6.c(y66, "mDianaCustomizeView");
        Wg6.c(lb6, "mHybridCustomizeView");
        Wg6.c(ao6, "mProfileView");
        Wg6.c(a06, "mAlertsView");
        Wg6.c(y36, "mAlertsHybridView");
        Wg6.c(sn6, "mUpdateFirmwareView");
        this.a = ld6;
        this.b = y66;
        this.c = lb6;
        this.d = ao6;
        this.e = a06;
        this.f = y36;
        this.g = sn6;
    }

    @DexIgnore
    public final Y36 a() {
        return this.f;
    }

    @DexIgnore
    public final A06 b() {
        return this.e;
    }

    @DexIgnore
    public final Ld6 c() {
        return this.a;
    }

    @DexIgnore
    public final Y66 d() {
        return this.b;
    }

    @DexIgnore
    public final Lb6 e() {
        return this.c;
    }

    @DexIgnore
    public final WeakReference<Ao6> f() {
        return new WeakReference<>(this.d);
    }

    @DexIgnore
    public final Sn6 g() {
        return this.g;
    }
}
