package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wt7 extends Vt7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Coroutine<CharSequence, Integer, Lc6<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ char[] $delimiters;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(char[] cArr, boolean z) {
            super(2);
            this.$delimiters = cArr;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final Lc6<Integer, Integer> invoke(CharSequence charSequence, int i) {
            Wg6.c(charSequence, "$receiver");
            int H = Wt7.H(charSequence, this.$delimiters, i, this.$ignoreCase);
            if (H < 0) {
                return null;
            }
            return Hl7.a(Integer.valueOf(H), 1);
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Lc6<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Coroutine<CharSequence, Integer, Lc6<? extends Integer, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ List $delimitersList;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $ignoreCase;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(List list, boolean z) {
            super(2);
            this.$delimitersList = list;
            this.$ignoreCase = z;
        }

        @DexIgnore
        public final Lc6<Integer, Integer> invoke(CharSequence charSequence, int i) {
            Wg6.c(charSequence, "$receiver");
            Lc6 y = Wt7.y(charSequence, this.$delimitersList, i, this.$ignoreCase, false);
            if (y != null) {
                return Hl7.a(y.getFirst(), Integer.valueOf(((String) y.getSecond()).length()));
            }
            return null;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Lc6<? extends Integer, ? extends Integer> invoke(CharSequence charSequence, Integer num) {
            return invoke(charSequence, num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<Wr7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ String invoke(Wr7 wr7) {
            return invoke(wr7);
        }

        @DexIgnore
        public final String invoke(Wr7 wr7) {
            Wg6.c(wr7, "it");
            return Wt7.f0(this.$this_splitToSequence, wr7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Hg6<Wr7, String> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $this_splitToSequence;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(CharSequence charSequence) {
            super(1);
            this.$this_splitToSequence = charSequence;
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ String invoke(Wr7 wr7) {
            return invoke(wr7);
        }

        @DexIgnore
        public final String invoke(Wr7 wr7) {
            Wg6.c(wr7, "it");
            return Wt7.f0(this.$this_splitToSequence, wr7);
        }
    }

    @DexIgnore
    public static final int A(CharSequence charSequence) {
        Wg6.c(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    @DexIgnore
    public static final int B(CharSequence charSequence, char c, int i, boolean z) {
        Wg6.c(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c, i);
        }
        return H(charSequence, new char[]{c}, i, z);
    }

    @DexIgnore
    public static final int C(CharSequence charSequence, String str, int i, boolean z) {
        Wg6.c(charSequence, "$this$indexOf");
        Wg6.c(str, LegacyTokenHelper.TYPE_STRING);
        return (z || !(charSequence instanceof String)) ? E(charSequence, str, i, charSequence.length(), z, false, 16, null) : ((String) charSequence).indexOf(str, i);
    }

    @DexIgnore
    public static final int D(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        Ur7 wr7 = !z2 ? new Wr7(Bs7.d(i, 0), Bs7.g(i2, charSequence.length())) : Bs7.k(Bs7.g(i, A(charSequence)), Bs7.d(i2, 0));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a2 = wr7.a();
            int b = wr7.b();
            int c = wr7.c();
            if (c < 0 ? a2 >= b : a2 <= b) {
                while (!T(charSequence2, 0, charSequence, a2, charSequence2.length(), z)) {
                    if (a2 != b) {
                        a2 += c;
                    }
                }
                return a2;
            }
        } else {
            int a3 = wr7.a();
            int b2 = wr7.b();
            int c2 = wr7.c();
            if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                while (!Vt7.m((String) charSequence2, 0, (String) charSequence, a3, charSequence2.length(), z)) {
                    if (a3 != b2) {
                        a3 += c2;
                    }
                }
                return a3;
            }
        }
        return -1;
    }

    @DexIgnore
    public static /* synthetic */ int E(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        return D(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    @DexIgnore
    public static /* synthetic */ int F(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return B(charSequence, c, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int G(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return C(charSequence, str, i, z);
    }

    @DexIgnore
    public static final int H(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        Wg6.c(charSequence, "$this$indexOfAny");
        Wg6.c(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int d = Bs7.d(i, 0);
            int A = A(charSequence);
            if (d <= A) {
                while (true) {
                    char charAt = charSequence.charAt(d);
                    int length = cArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            z2 = false;
                            break;
                        } else if (Dt7.d(cArr[i2], charAt, z)) {
                            z2 = true;
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (!z2) {
                        if (d == A) {
                            break;
                        }
                        d++;
                    } else {
                        return d;
                    }
                }
            }
            return -1;
        }
        return ((String) charSequence).indexOf(Em7.W(cArr), i);
    }

    @DexIgnore
    public static final int I(CharSequence charSequence, char c, int i, boolean z) {
        Wg6.c(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c, i);
        }
        return M(charSequence, new char[]{c}, i, z);
    }

    @DexIgnore
    public static final int J(CharSequence charSequence, String str, int i, boolean z) {
        Wg6.c(charSequence, "$this$lastIndexOf");
        Wg6.c(str, LegacyTokenHelper.TYPE_STRING);
        return (z || !(charSequence instanceof String)) ? D(charSequence, str, i, 0, z, true) : ((String) charSequence).lastIndexOf(str, i);
    }

    @DexIgnore
    public static /* synthetic */ int K(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = A(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return I(charSequence, c, i, z);
    }

    @DexIgnore
    public static /* synthetic */ int L(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = A(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return J(charSequence, str, i, z);
    }

    @DexIgnore
    public static final int M(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        Wg6.c(charSequence, "$this$lastIndexOfAny");
        Wg6.c(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int g = Bs7.g(i, A(charSequence)); g >= 0; g--) {
                char charAt = charSequence.charAt(g);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (Dt7.d(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return g;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(Em7.W(cArr), i);
    }

    @DexIgnore
    public static final CharSequence N(CharSequence charSequence, int i, char c) {
        int i2 = 1;
        Wg6.c(charSequence, "$this$padStart");
        if (i < 0) {
            throw new IllegalArgumentException("Desired length " + i + " is less than zero.");
        } else if (i <= charSequence.length()) {
            return charSequence.subSequence(0, charSequence.length());
        } else {
            StringBuilder sb = new StringBuilder(i);
            int length = i - charSequence.length();
            if (1 <= length) {
                while (true) {
                    sb.append(c);
                    if (i2 == length) {
                        break;
                    }
                    i2++;
                }
            }
            sb.append(charSequence);
            return sb;
        }
    }

    @DexIgnore
    public static final String O(String str, int i, char c) {
        Wg6.c(str, "$this$padStart");
        return N(str, i, c).toString();
    }

    @DexIgnore
    public static final Ts7<Wr7> P(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new Ft7(charSequence, i, i2, new Ai(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static final Ts7<Wr7> Q(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new Ft7(charSequence, i, i2, new Bi(Dm7.d(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ Ts7 R(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return P(charSequence, cArr, i, z, i2);
    }

    @DexIgnore
    public static /* synthetic */ Ts7 S(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return Q(charSequence, strArr, i, z, i2);
    }

    @DexIgnore
    public static final boolean T(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        Wg6.c(charSequence, "$this$regionMatchesImpl");
        Wg6.c(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!Dt7.d(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static final String U(String str, CharSequence charSequence) {
        Wg6.c(str, "$this$removePrefix");
        Wg6.c(charSequence, "prefix");
        if (!e0(str, charSequence, false, 2, null)) {
            return str;
        }
        String substring = str.substring(charSequence.length());
        Wg6.b(substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    @DexIgnore
    public static final String V(String str, CharSequence charSequence, CharSequence charSequence2) {
        Wg6.c(str, "$this$removeSurrounding");
        Wg6.c(charSequence, "prefix");
        Wg6.c(charSequence2, "suffix");
        if (str.length() < charSequence.length() + charSequence2.length() || !e0(str, charSequence, false, 2, null) || !x(str, charSequence2, false, 2, null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final List<String> W(CharSequence charSequence, String[] strArr, boolean z, int i) {
        boolean z2 = true;
        Wg6.c(charSequence, "$this$split");
        Wg6.c(strArr, "delimiters");
        if (strArr.length == 1) {
            String str = strArr[0];
            if (str.length() != 0) {
                z2 = false;
            }
            if (!z2) {
                return X(charSequence, str, z, i);
            }
        }
        Iterable<Wr7> e = At7.e(S(charSequence, strArr, 0, z, i, 2, null));
        ArrayList arrayList = new ArrayList(Im7.m(e, 10));
        for (Wr7 wr7 : e) {
            arrayList.add(f0(charSequence, wr7));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<String> X(CharSequence charSequence, String str, boolean z, int i) {
        int length;
        int i2 = 10;
        int i3 = 0;
        if (i >= 0) {
            int C = C(charSequence, str, 0, z);
            if (C == -1 || i == 1) {
                return Gm7.b(charSequence.toString());
            }
            boolean z2 = i > 0;
            if (z2) {
                i2 = Bs7.g(i, 10);
            }
            ArrayList arrayList = new ArrayList(i2);
            int i4 = C;
            while (true) {
                arrayList.add(charSequence.subSequence(i3, i4).toString());
                length = str.length() + i4;
                if ((!z2 || arrayList.size() != i - 1) && (i4 = C(charSequence, str, length, z)) != -1) {
                    i3 = length;
                }
            }
            arrayList.add(charSequence.subSequence(length, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public static /* synthetic */ List Y(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return W(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final Ts7<String> Z(CharSequence charSequence, char[] cArr, boolean z, int i) {
        Wg6.c(charSequence, "$this$splitToSequence");
        Wg6.c(cArr, "delimiters");
        return At7.o(R(charSequence, cArr, 0, z, i, 2, null), new Di(charSequence));
    }

    @DexIgnore
    public static final Ts7<String> a0(CharSequence charSequence, String[] strArr, boolean z, int i) {
        Wg6.c(charSequence, "$this$splitToSequence");
        Wg6.c(strArr, "delimiters");
        return At7.o(S(charSequence, strArr, 0, z, i, 2, null), new Ci(charSequence));
    }

    @DexIgnore
    public static /* synthetic */ Ts7 b0(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return Z(charSequence, cArr, z, i);
    }

    @DexIgnore
    public static /* synthetic */ Ts7 c0(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a0(charSequence, strArr, z, i);
    }

    @DexIgnore
    public static final boolean d0(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Wg6.c(charSequence, "$this$startsWith");
        Wg6.c(charSequence2, "prefix");
        return (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) ? T(charSequence, 0, charSequence2, 0, charSequence2.length(), z) : Vt7.s((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ boolean e0(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return d0(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final String f0(CharSequence charSequence, Wr7 wr7) {
        Wg6.c(charSequence, "$this$substring");
        Wg6.c(wr7, "range");
        return charSequence.subSequence(wr7.h().intValue(), wr7.g().intValue() + 1).toString();
    }

    @DexIgnore
    public static final String g0(String str, char c, String str2) {
        Wg6.c(str, "$this$substringAfter");
        Wg6.c(str2, "missingDelimiterValue");
        int F = F(str, c, 0, false, 6, null);
        if (F == -1) {
            return str2;
        }
        String substring = str.substring(F + 1, str.length());
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String h0(String str, String str2, String str3) {
        Wg6.c(str, "$this$substringAfter");
        Wg6.c(str2, "delimiter");
        Wg6.c(str3, "missingDelimiterValue");
        int G = G(str, str2, 0, false, 6, null);
        if (G == -1) {
            return str3;
        }
        String substring = str.substring(G + str2.length(), str.length());
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String i0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return g0(str, c, str2);
    }

    @DexIgnore
    public static /* synthetic */ String j0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return h0(str, str2, str3);
    }

    @DexIgnore
    public static final String k0(String str, char c, String str2) {
        Wg6.c(str, "$this$substringAfterLast");
        Wg6.c(str2, "missingDelimiterValue");
        int K = K(str, c, 0, false, 6, null);
        if (K == -1) {
            return str2;
        }
        String substring = str.substring(K + 1, str.length());
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String l0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return k0(str, c, str2);
    }

    @DexIgnore
    public static final String m0(String str, char c, String str2) {
        Wg6.c(str, "$this$substringBefore");
        Wg6.c(str2, "missingDelimiterValue");
        int F = F(str, c, 0, false, 6, null);
        if (F == -1) {
            return str2;
        }
        String substring = str.substring(0, F);
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String n0(String str, String str2, String str3) {
        Wg6.c(str, "$this$substringBefore");
        Wg6.c(str2, "delimiter");
        Wg6.c(str3, "missingDelimiterValue");
        int G = G(str, str2, 0, false, 6, null);
        if (G == -1) {
            return str3;
        }
        String substring = str.substring(0, G);
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String o0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return m0(str, c, str2);
    }

    @DexIgnore
    public static /* synthetic */ String p0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return n0(str, str2, str3);
    }

    @DexIgnore
    public static final String q0(String str, char c, String str2) {
        Wg6.c(str, "$this$substringBeforeLast");
        Wg6.c(str2, "missingDelimiterValue");
        int K = K(str, c, 0, false, 6, null);
        if (K == -1) {
            return str2;
        }
        String substring = str.substring(0, K);
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static final String r0(String str, String str2, String str3) {
        Wg6.c(str, "$this$substringBeforeLast");
        Wg6.c(str2, "delimiter");
        Wg6.c(str3, "missingDelimiterValue");
        int L = L(str, str2, 0, false, 6, null);
        if (L == -1) {
            return str3;
        }
        String substring = str.substring(0, L);
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        return substring;
    }

    @DexIgnore
    public static /* synthetic */ String s0(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return q0(str, c, str2);
    }

    @DexIgnore
    public static /* synthetic */ String t0(String str, String str2, String str3, int i, Object obj) {
        if ((i & 2) != 0) {
            str3 = str;
        }
        return r0(str, str2, str3);
    }

    @DexIgnore
    public static final boolean u(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Wg6.c(charSequence, "$this$contains");
        Wg6.c(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (charSequence2 instanceof String) {
            if (G(charSequence, (String) charSequence2, 0, z, 2, null) >= 0) {
                return true;
            }
        } else if (E(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, null) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static final CharSequence u0(CharSequence charSequence) {
        Wg6.c(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        boolean z = false;
        int i = 0;
        while (i <= length) {
            boolean c = Ct7.c(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!c) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!c) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    @DexIgnore
    public static /* synthetic */ boolean v(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return u(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final String v0(String str, char... cArr) {
        CharSequence charSequence;
        Wg6.c(str, "$this$trimEnd");
        Wg6.c(cArr, "chars");
        int length = str.length();
        while (true) {
            length--;
            if (length >= 0) {
                if (!Em7.y(cArr, str.charAt(length))) {
                    charSequence = str.subSequence(0, length + 1);
                    break;
                }
            } else {
                charSequence = "";
                break;
            }
        }
        return charSequence.toString();
    }

    @DexIgnore
    public static final boolean w(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Wg6.c(charSequence, "$this$endsWith");
        Wg6.c(charSequence2, "suffix");
        return (z || !(charSequence instanceof String) || !(charSequence2 instanceof String)) ? T(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z) : Vt7.i((String) charSequence, (String) charSequence2, false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ boolean x(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return w(charSequence, charSequence2, z);
    }

    @DexIgnore
    public static final Lc6<Integer, String> y(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        T t;
        T t2;
        if (z || collection.size() != 1) {
            Ur7 wr7 = !z2 ? new Wr7(Bs7.d(i, 0), charSequence.length()) : Bs7.k(Bs7.g(i, A(charSequence)), 0);
            if (charSequence instanceof String) {
                int a2 = wr7.a();
                int b = wr7.b();
                int c = wr7.c();
                if (c < 0 ? a2 >= b : a2 <= b) {
                    while (true) {
                        Iterator<T> it = collection.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t2 = null;
                                break;
                            }
                            T next = it.next();
                            T t3 = next;
                            if (Vt7.m(t3, 0, (String) charSequence, a2, t3.length(), z)) {
                                t2 = next;
                                break;
                            }
                        }
                        T t4 = t2;
                        if (t4 == null) {
                            if (a2 == b) {
                                break;
                            }
                            a2 += c;
                        } else {
                            return Hl7.a(Integer.valueOf(a2), t4);
                        }
                    }
                }
            } else {
                int a3 = wr7.a();
                int b2 = wr7.b();
                int c2 = wr7.c();
                if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                    while (true) {
                        Iterator<T> it2 = collection.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            T next2 = it2.next();
                            T t5 = next2;
                            if (T(t5, 0, charSequence, a3, t5.length(), z)) {
                                t = next2;
                                break;
                            }
                        }
                        T t6 = t;
                        if (t6 == null) {
                            if (a3 == b2) {
                                break;
                            }
                            a3 += c2;
                        } else {
                            return Hl7.a(Integer.valueOf(a3), t6);
                        }
                    }
                }
            }
            return null;
        }
        String str = (String) Pm7.Y(collection);
        int G = !z2 ? G(charSequence, str, i, false, 4, null) : L(charSequence, str, i, false, 4, null);
        if (G < 0) {
            return null;
        }
        return Hl7.a(Integer.valueOf(G), str);
    }

    @DexIgnore
    public static final Wr7 z(CharSequence charSequence) {
        Wg6.c(charSequence, "$this$indices");
        return new Wr7(0, charSequence.length() - 1);
    }
}
