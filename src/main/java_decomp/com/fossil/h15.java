package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H15 extends G15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        w = sparseIntArray;
        sparseIntArray.put(2131361851, 1);
        w.put(2131362353, 2);
        w.put(2131363553, 3);
    }
    */

    @DexIgnore
    public H15(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 4, v, w));
    }

    @DexIgnore
    public H15(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[1], (FlexibleTextView) objArr[2], (WebView) objArr[3]);
        this.u = -1;
        LinearLayout linearLayout = (LinearLayout) objArr[0];
        this.t = linearLayout;
        linearLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.u != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.u = 1;
        }
        w();
    }
}
