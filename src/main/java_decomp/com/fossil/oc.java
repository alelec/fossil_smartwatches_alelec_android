package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;
import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oc extends Qq7 implements Hg6<Cd6, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Zq7 b;
    @DexIgnore
    public /* final */ /* synthetic */ Mp1 c;
    @DexIgnore
    public /* final */ /* synthetic */ Zq7 d;
    @DexIgnore
    public /* final */ /* synthetic */ E60 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Oc(Zq7 zq7, Mp1 mp1, Zq7 zq72, E60 e60) {
        super(1);
        this.b = zq7;
        this.c = mp1;
        this.d = zq72;
        this.e = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Cd6 cd6) {
        if (this.b.element) {
            this.e.s0(this.c);
        }
        if (this.d.element) {
            WorkoutSessionManager workoutSessionManager = this.e.t;
            Boolean valueOf = workoutSessionManager != null ? Boolean.valueOf(workoutSessionManager.clearAccumulateDistance()) : null;
            M80 m80 = M80.c;
            m80.a("DeviceEventManager", "clearAccumulateDistance result = " + valueOf + '.', new Object[0]);
        }
        return Cd6.a;
    }
}
