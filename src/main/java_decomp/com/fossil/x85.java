package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ LinearLayout B;
    @DexIgnore
    public /* final */ LinearLayout C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ RecyclerView E;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ View I;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    public X85(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ConstraintLayout constraintLayout2, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = flexibleTextView4;
        this.v = flexibleTextView5;
        this.w = flexibleTextView6;
        this.x = rTLImageView;
        this.y = rTLImageView2;
        this.z = constraintLayout;
        this.A = linearLayout;
        this.B = linearLayout2;
        this.C = linearLayout3;
        this.D = constraintLayout2;
        this.E = recyclerView;
        this.F = flexibleSwitchCompat;
        this.G = view2;
        this.H = view3;
        this.I = view4;
    }
}
