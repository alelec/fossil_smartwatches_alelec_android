package com.fossil;

import android.os.DeadObjectException;
import com.fossil.I72;
import com.fossil.L72;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La2<A extends I72<? extends Z62, M62.Bi>> extends Z82 {
    @DexIgnore
    public /* final */ A b;

    @DexIgnore
    public La2(int i, A a2) {
        super(i);
        this.b = a2;
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void b(Status status) {
        this.b.A(status);
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void c(L72.Ai<?> ai) throws DeadObjectException {
        try {
            this.b.y(ai.R());
        } catch (RuntimeException e) {
            e(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void d(A82 a82, boolean z) {
        a82.b(this.b, z);
    }

    @DexIgnore
    @Override // com.fossil.Z82
    public final void e(Exception exc) {
        String simpleName = exc.getClass().getSimpleName();
        String localizedMessage = exc.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.b.A(new Status(10, sb.toString()));
    }
}
