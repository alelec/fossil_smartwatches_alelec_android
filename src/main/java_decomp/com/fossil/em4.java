package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Em4 {
    @DexIgnore
    public /* final */ Dm4 a;
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore
    public Em4(Dm4 dm4, int[] iArr) {
        if (iArr.length != 0) {
            this.a = dm4;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.b = new int[]{0};
                return;
            }
            int[] iArr2 = new int[(length - i)];
            this.b = iArr2;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public Em4 a(Em4 em4) {
        int[] iArr;
        int[] iArr2;
        if (!this.a.equals(em4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (f()) {
            return em4;
        } else {
            if (em4.f()) {
                return this;
            }
            int[] iArr3 = this.b;
            int[] iArr4 = em4.b;
            if (iArr3.length > iArr4.length) {
                iArr = iArr4;
                iArr2 = iArr3;
            } else {
                iArr = iArr3;
                iArr2 = iArr4;
            }
            int[] iArr5 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr5, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr5[i] = Dm4.a(iArr[i - length], iArr2[i]);
            }
            return new Em4(this.a, iArr5);
        }
    }

    @DexIgnore
    public Em4[] b(Em4 em4) {
        if (!this.a.equals(em4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!em4.f()) {
            Em4 e = this.a.e();
            int f = this.a.f(em4.c(em4.e()));
            Em4 em42 = this;
            while (em42.e() >= em4.e() && !em42.f()) {
                int e2 = em42.e() - em4.e();
                int h = this.a.h(em42.c(em42.e()), f);
                Em4 h2 = em4.h(e2, h);
                e = e.a(this.a.b(e2, h));
                em42 = em42.a(h2);
            }
            return new Em4[]{e, em42};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    @DexIgnore
    public int c(int i) {
        int[] iArr = this.b;
        return iArr[(iArr.length - 1) - i];
    }

    @DexIgnore
    public int[] d() {
        return this.b;
    }

    @DexIgnore
    public int e() {
        return this.b.length - 1;
    }

    @DexIgnore
    public boolean f() {
        return this.b[0] == 0;
    }

    @DexIgnore
    public Em4 g(Em4 em4) {
        if (!this.a.equals(em4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (f() || em4.f()) {
            return this.a.e();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = em4.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = Dm4.a(iArr3[i4], this.a.h(i2, iArr2[i3]));
                }
            }
            return new Em4(this.a, iArr3);
        }
    }

    @DexIgnore
    public Em4 h(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.a.e();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.a.h(this.b[i3], i2);
            }
            return new Em4(this.a, iArr);
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(e() * 8);
        for (int e = e(); e >= 0; e--) {
            int c = c(e);
            if (c != 0) {
                if (c < 0) {
                    sb.append(" - ");
                    c = -c;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (e == 0 || c != 1) {
                    int g = this.a.g(c);
                    if (g == 0) {
                        sb.append('1');
                    } else if (g == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(g);
                    }
                }
                if (e != 0) {
                    if (e == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(e);
                    }
                }
            }
        }
        return sb.toString();
    }
}
