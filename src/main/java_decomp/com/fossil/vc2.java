package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.fossil.Vg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vc2 extends Vg2<Oc2> {
    @DexIgnore
    public static /* final */ Vc2 c; // = new Vc2();

    @DexIgnore
    public Vc2() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    @DexIgnore
    public static View c(Context context, int i, int i2) throws Vg2.Ai {
        return c.e(context, i, i2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Vg2
    public final /* bridge */ /* synthetic */ Oc2 a(IBinder iBinder) {
        return d(iBinder);
    }

    @DexIgnore
    public final Oc2 d(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        return queryLocalInterface instanceof Oc2 ? (Oc2) queryLocalInterface : new Vd2(iBinder);
    }

    @DexIgnore
    public final View e(Context context, int i, int i2) throws Vg2.Ai {
        try {
            Uc2 uc2 = new Uc2(i, i2, null);
            return (View) Tg2.i(((Oc2) b(context)).P0(Tg2.n(context), uc2));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new Vg2.Ai(sb.toString(), e);
        }
    }
}
