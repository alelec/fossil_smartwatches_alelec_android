package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryViewModel;
import com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya7 implements Factory<WatchFaceGalleryViewModel> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> b;
    @DexIgnore
    public /* final */ Provider<PreviewWatchFaceUseCase> c;

    @DexIgnore
    public Ya7(Provider<UserRepository> provider, Provider<DianaWatchFaceRepository> provider2, Provider<PreviewWatchFaceUseCase> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Ya7 a(Provider<UserRepository> provider, Provider<DianaWatchFaceRepository> provider2, Provider<PreviewWatchFaceUseCase> provider3) {
        return new Ya7(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchFaceGalleryViewModel c(UserRepository userRepository, DianaWatchFaceRepository dianaWatchFaceRepository, PreviewWatchFaceUseCase previewWatchFaceUseCase) {
        return new WatchFaceGalleryViewModel(userRepository, dianaWatchFaceRepository, previewWatchFaceUseCase);
    }

    @DexIgnore
    public WatchFaceGalleryViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
