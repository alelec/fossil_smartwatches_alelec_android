package com.fossil;

import com.fossil.T62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z72 implements T62.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ BasePendingResult a;
    @DexIgnore
    public /* final */ /* synthetic */ A82 b;

    @DexIgnore
    public Z72(A82 a82, BasePendingResult basePendingResult) {
        this.b = a82;
        this.a = basePendingResult;
    }

    @DexIgnore
    @Override // com.fossil.T62.Ai
    public final void a(Status status) {
        A82.a(this.b).remove(this.a);
    }
}
