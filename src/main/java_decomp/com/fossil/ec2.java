package com.fossil;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.Fc2;
import com.fossil.M62;
import com.fossil.R62;
import com.fossil.Yb2;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ec2<T extends IInterface> extends Yb2<T> implements M62.Fi, Fc2.Ai {
    @DexIgnore
    public /* final */ Ac2 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ec2(android.content.Context r10, android.os.Looper r11, int r12, com.fossil.Ac2 r13, com.fossil.K72 r14, com.fossil.R72 r15) {
        /*
            r9 = this;
            com.fossil.Gc2 r3 = com.fossil.Gc2.b(r10)
            com.fossil.C62 r4 = com.fossil.C62.q()
            com.fossil.Rc2.k(r14)
            r7 = r14
            com.fossil.K72 r7 = (com.fossil.K72) r7
            com.fossil.Rc2.k(r15)
            r8 = r15
            com.fossil.R72 r8 = (com.fossil.R72) r8
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ec2.<init>(android.content.Context, android.os.Looper, int, com.fossil.Ac2, com.fossil.K72, com.fossil.R72):void");
    }

    @DexIgnore
    @Deprecated
    public Ec2(Context context, Looper looper, int i, Ac2 ac2, R62.Bi bi, R62.Ci ci) {
        this(context, looper, i, ac2, (K72) bi, (R72) ci);
    }

    @DexIgnore
    public Ec2(Context context, Looper looper, Gc2 gc2, C62 c62, int i, Ac2 ac2, K72 k72, R72 r72) {
        super(context, looper, gc2, c62, i, q0(k72), r0(r72), ac2.h());
        this.B = ac2;
        this.D = ac2.a();
        this.C = s0(ac2.d());
    }

    @DexIgnore
    public static Yb2.Ai q0(K72 k72) {
        if (k72 == null) {
            return null;
        }
        return new Ud2(k72);
    }

    @DexIgnore
    public static Yb2.Bi r0(R72 r72) {
        if (r72 == null) {
            return null;
        }
        return new Td2(r72);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final Account C() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final Set<Scope> H() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi
    public Set<Scope> h() {
        return v() ? this.C : Collections.emptySet();
    }

    @DexIgnore
    public final Ac2 o0() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> p0(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public int s() {
        return super.s();
    }

    @DexIgnore
    public final Set<Scope> s0(Set<Scope> set) {
        Set<Scope> p0 = p0(set);
        for (Scope scope : p0) {
            if (!set.contains(scope)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return p0;
    }
}
