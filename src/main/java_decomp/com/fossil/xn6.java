package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentManager;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xn6 extends BaseFragment implements Cy5 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public G37<Z35> g;
    @DexIgnore
    public By5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Xn6.j;
        }

        @DexIgnore
        public final Xn6 b() {
            return new Xn6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xn6 a;

        @DexIgnore
        public Bi(Xn6 xn6) {
            this.a = xn6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Wg6.c(compoundButton, "button");
            if (compoundButton.isPressed()) {
                Xn6.K6(this.a).n();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xn6 b;

        @DexIgnore
        public Ci(Xn6 xn6) {
            this.b = xn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                this.b.requireActivity().finish();
            }
        }
    }

    /*
    static {
        String simpleName = Xn6.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ By5 K6(Xn6 xn6) {
        By5 by5 = xn6.h;
        if (by5 != null) {
            return by5;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Cy5
    public void I5(int i2) {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, "", childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(By5 by5) {
        M6(by5);
    }

    @DexIgnore
    public void M6(By5 by5) {
        Wg6.c(by5, "presenter");
        this.h = by5;
    }

    @DexIgnore
    @Override // com.fossil.Cy5
    public void N2(boolean z) {
        if (isActive()) {
            G37<Z35> g37 = this.g;
            if (g37 != null) {
                Z35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = a2.q;
                    Wg6.b(flexibleSwitchCompat, "it.cbGooglefit");
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Cy5
    public void f1(Z52 z52) {
        Wg6.c(z52, "connectionResult");
        z52.D(getActivity(), 1);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1 && i3 == -1) {
            By5 by5 = this.h;
            if (by5 != null) {
                by5.o();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<Z35> g37 = new G37<>(this, (Z35) Aq0.f(layoutInflater, 2131558523, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            Z35 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        By5 by5 = this.h;
        if (by5 != null) {
            by5.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        By5 by5 = this.h;
        if (by5 != null) {
            by5.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Z35> g37 = this.g;
        if (g37 != null) {
            Z35 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new Bi(this));
                a2.w.setOnClickListener(new Ci(this));
            }
            E6("connected_app_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
