package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface S32 {

    @DexIgnore
    public interface Ai<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    <T> T a(Ai<T> ai);
}
