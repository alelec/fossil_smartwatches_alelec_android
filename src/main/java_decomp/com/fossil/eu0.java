package com.fossil;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu0<T> extends AbstractList<T> {
    @DexIgnore
    public static /* final */ List k; // = new ArrayList();
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ ArrayList<List<T>> c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        void b(int i, int i2, int i3);

        @DexIgnore
        void c(int i);

        @DexIgnore
        void d(int i);

        @DexIgnore
        void e(int i, int i2);

        @DexIgnore
        void f(int i, int i2);

        @DexIgnore
        Object g();  // void declaration

        @DexIgnore
        void h(int i, int i2);

        @DexIgnore
        void i(int i, int i2, int i3);
    }

    @DexIgnore
    public Eu0() {
        this.b = 0;
        this.c = new ArrayList<>();
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 1;
        this.i = 0;
        this.j = 0;
    }

    @DexIgnore
    public Eu0(Eu0<T> eu0) {
        this.b = eu0.b;
        this.c = new ArrayList<>(eu0.c);
        this.d = eu0.d;
        this.e = eu0.e;
        this.f = eu0.f;
        this.g = eu0.g;
        this.h = eu0.h;
        this.i = eu0.i;
        this.j = eu0.j;
    }

    @DexIgnore
    public boolean A(int i2, int i3, int i4) {
        return this.f + i4 > i2 && this.c.size() > 1 && this.f >= i3;
    }

    @DexIgnore
    public Eu0<T> B() {
        return new Eu0<>(this);
    }

    @DexIgnore
    public boolean D(boolean z, int i2, int i3, Ai ai) {
        int i4 = 0;
        while (w(i2, i3)) {
            ArrayList<List<T>> arrayList = this.c;
            List<T> remove = arrayList.remove(arrayList.size() - 1);
            int size = remove == null ? this.h : remove.size();
            i4 += size;
            this.g -= size;
            this.f -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            int i5 = this.b + this.g;
            if (z) {
                this.d += i4;
                ai.e(i5, i4);
            } else {
                ai.f(i5, i4);
            }
        }
        return i4 > 0;
    }

    @DexIgnore
    public boolean E(boolean z, int i2, int i3, Ai ai) {
        int i4 = 0;
        while (x(i2, i3)) {
            List<T> remove = this.c.remove(0);
            int size = remove == null ? this.h : remove.size();
            i4 += size;
            this.g -= size;
            this.f -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            if (z) {
                int i5 = this.b;
                this.b = i5 + i4;
                ai.e(i5, i4);
            } else {
                this.e += i4;
                ai.f(this.b, i4);
            }
        }
        return i4 > 0;
    }

    @DexIgnore
    public void F(int i2, List<T> list, int i3, int i4, int i5, Ai ai) {
        boolean z = i4 != Integer.MAX_VALUE;
        boolean z2 = i3 > i();
        if (!z || !A(i4, i5, list.size()) || !y(i2, z2)) {
            t(i2, list, ai);
        } else {
            this.c.set((i2 - this.b) / this.h, null);
            this.g -= list.size();
            if (z2) {
                this.c.remove(0);
                this.b += list.size();
            } else {
                ArrayList<List<T>> arrayList = this.c;
                arrayList.remove(arrayList.size() - 1);
                this.d += list.size();
            }
        }
        if (!z) {
            return;
        }
        if (z2) {
            E(true, i4, i5, ai);
        } else {
            D(true, i4, i5, ai);
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int i4;
        int i5 = this.b / this.h;
        if (i2 < i5) {
            int i6 = 0;
            while (true) {
                i4 = i5 - i2;
                if (i6 >= i4) {
                    break;
                }
                this.c.add(0, null);
                i6++;
            }
            int i7 = this.h * i4;
            this.g += i7;
            this.b -= i7;
        } else {
            i2 = i5;
        }
        if (i3 >= this.c.size() + i2) {
            int min = Math.min(this.d, ((i3 + 1) - (this.c.size() + i2)) * this.h);
            for (int size = this.c.size(); size <= i3 - i2; size++) {
                ArrayList<List<T>> arrayList = this.c;
                arrayList.add(arrayList.size(), null);
            }
            this.g += min;
            this.d -= min;
        }
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, Ai ai) {
        int i5 = this.h;
        if (i4 != i5) {
            if (i4 < i5) {
                throw new IllegalArgumentException("Page size cannot be reduced");
            } else if (this.c.size() == 1 && this.d == 0) {
                this.h = i4;
            } else {
                throw new IllegalArgumentException("Page size can change only if last page is only one present");
            }
        }
        int size = size();
        int i6 = this.h;
        int i7 = ((size + i6) - 1) / i6;
        int max = Math.max((i2 - i3) / i6, 0);
        int min = Math.min((i2 + i3) / this.h, i7 - 1);
        a(max, min);
        int i8 = this.b / this.h;
        while (max <= min) {
            int i9 = max - i8;
            if (this.c.get(i9) == null) {
                this.c.set(i9, k);
                ai.d(max);
            }
            max++;
        }
    }

    @DexIgnore
    public void c(List<T> list, Ai ai) {
        int size = list.size();
        if (size == 0) {
            ai.a();
            return;
        }
        if (this.h > 0) {
            ArrayList<List<T>> arrayList = this.c;
            int size2 = arrayList.get(arrayList.size() - 1).size();
            int i2 = this.h;
            if (size2 != i2 || size > i2) {
                this.h = -1;
            }
        }
        this.c.add(list);
        this.f += size;
        this.g += size;
        int min = Math.min(this.d, size);
        if (min != 0) {
            this.d -= min;
        }
        this.j += size;
        ai.i((this.b + this.g) - size, min, size - min);
    }

    @DexIgnore
    public int d() {
        int i2 = this.b;
        int size = this.c.size();
        for (int i3 = 0; i3 < size; i3++) {
            List<T> list = this.c.get(i3);
            if (!(list == null || list == k)) {
                break;
            }
            i2 += this.h;
        }
        return i2;
    }

    @DexIgnore
    public int e() {
        int i2 = this.d;
        for (int size = this.c.size() - 1; size >= 0; size--) {
            List<T> list = this.c.get(size);
            if (!(list == null || list == k)) {
                break;
            }
            i2 += this.h;
        }
        return i2;
    }

    @DexIgnore
    public T f() {
        return this.c.get(0).get(0);
    }

    @DexIgnore
    public T g() {
        ArrayList<List<T>> arrayList = this.c;
        List<T> list = arrayList.get(arrayList.size() - 1);
        return list.get(list.size() - 1);
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public T get(int i2) {
        int i3;
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        int i4 = i2 - this.b;
        if (i4 >= 0 && i4 < this.g) {
            if (!u()) {
                int size = this.c.size();
                i3 = 0;
                while (i3 < size) {
                    int size2 = this.c.get(i3).size();
                    if (size2 > i4) {
                        break;
                    }
                    i4 -= size2;
                    i3++;
                }
            } else {
                int i5 = this.h;
                i3 = i4 / i5;
                i4 %= i5;
            }
            List<T> list = this.c.get(i3);
            if (!(list == null || list.size() == 0)) {
                return list.get(i4);
            }
        }
        return null;
    }

    @DexIgnore
    public int h() {
        return this.b;
    }

    @DexIgnore
    public int i() {
        return this.b + this.e + (this.g / 2);
    }

    @DexIgnore
    public int j() {
        return this.j;
    }

    @DexIgnore
    public int k() {
        return this.i;
    }

    @DexIgnore
    public int l() {
        return this.c.size();
    }

    @DexIgnore
    public int m() {
        return this.e;
    }

    @DexIgnore
    public int n() {
        return this.g;
    }

    @DexIgnore
    public int o() {
        return this.d;
    }

    @DexIgnore
    public boolean p(int i2, int i3) {
        int i4 = this.b / i2;
        if (i3 >= i4) {
            if (i3 >= this.c.size() + i4) {
                return false;
            }
            List<T> list = this.c.get(i3 - i4);
            if (!(list == null || list == k)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void q(int i2, List<T> list, int i3, int i4) {
        this.b = i2;
        this.c.clear();
        this.c.add(list);
        this.d = i3;
        this.e = i4;
        int size = list.size();
        this.f = size;
        this.g = size;
        this.h = list.size();
        this.i = 0;
        this.j = 0;
    }

    @DexIgnore
    public void r(int i2, List<T> list, int i3, int i4, Ai ai) {
        q(i2, list, i3, i4);
        ai.c(size());
    }

    @DexIgnore
    public void s(int i2, List<T> list, int i3, int i4, int i5, Ai ai) {
        int size = (list.size() + (i5 - 1)) / i5;
        int i6 = 0;
        while (i6 < size) {
            int i7 = i6 * i5;
            int i8 = i6 + 1;
            List<T> subList = list.subList(i7, Math.min(list.size(), i8 * i5));
            if (i6 == 0) {
                q(i2, subList, (list.size() + i3) - subList.size(), i4);
            } else {
                t(i7 + i2, subList, null);
            }
            i6 = i8;
        }
        ai.c(size());
    }

    @DexIgnore
    public int size() {
        return this.b + this.g + this.d;
    }

    @DexIgnore
    public void t(int i2, List<T> list, Ai ai) {
        boolean z = false;
        int size = list.size();
        if (size != this.h) {
            int size2 = size();
            int i3 = this.h;
            boolean z2 = i2 == size2 - (size2 % i3) && size < i3;
            if (this.d == 0 && this.c.size() == 1 && size > this.h) {
                z = true;
            }
            if (!z && !z2) {
                throw new IllegalArgumentException("page introduces incorrect tiling");
            } else if (z) {
                this.h = size;
            }
        }
        int i4 = i2 / this.h;
        a(i4, i4);
        int i5 = i4 - (this.b / this.h);
        List<T> list2 = this.c.get(i5);
        if (list2 == null || list2 == k) {
            this.c.set(i5, list);
            this.f += size;
            if (ai != null) {
                ai.h(i2, size);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid position " + i2 + ": data already loaded");
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("leading " + this.b + ", storage " + this.g + ", trailing " + o());
        for (int i2 = 0; i2 < this.c.size(); i2++) {
            sb.append(" ");
            sb.append(this.c.get(i2));
        }
        return sb.toString();
    }

    @DexIgnore
    public boolean u() {
        return this.h > 0;
    }

    @DexIgnore
    public final boolean v(int i2, int i3, int i4) {
        List<T> list = this.c.get(i4);
        return list == null || (this.f > i2 && this.c.size() > 2 && list != k && this.f - list.size() >= i3);
    }

    @DexIgnore
    public boolean w(int i2, int i3) {
        return v(i2, i3, this.c.size() - 1);
    }

    @DexIgnore
    public boolean x(int i2, int i3) {
        return v(i2, i3, 0);
    }

    @DexIgnore
    public boolean y(int i2, boolean z) {
        if (this.h < 1 || this.c.size() < 2) {
            throw new IllegalStateException("Trimming attempt before sufficient load");
        }
        int i3 = this.b;
        if (i2 < i3) {
            return z;
        }
        if (i2 >= this.g + i3) {
            return !z;
        }
        int i4 = (i2 - i3) / this.h;
        if (z) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (this.c.get(i5) != null) {
                    return false;
                }
            }
        } else {
            for (int size = this.c.size() - 1; size > i4; size--) {
                if (this.c.get(size) != null) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public void z(List<T> list, Ai ai) {
        int size = list.size();
        if (size == 0) {
            ai.g();
            return;
        }
        int i2 = this.h;
        if (i2 > 0 && size != i2) {
            if (this.c.size() != 1 || size <= this.h) {
                this.h = -1;
            } else {
                this.h = size;
            }
        }
        this.c.add(0, list);
        this.f += size;
        this.g += size;
        int min = Math.min(this.b, size);
        int i3 = size - min;
        if (min != 0) {
            this.b -= min;
        }
        this.e -= i3;
        this.i = size + this.i;
        ai.b(this.b, min, i3);
    }
}
