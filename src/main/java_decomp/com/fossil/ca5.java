package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.WaveView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ca5 extends Ba5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        y.put(2131362968, 2);
        y.put(2131362546, 3);
        y.put(2131362692, 4);
        y.put(2131362405, 5);
    }
    */

    @DexIgnore
    public Ca5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 6, x, y));
    }

    @DexIgnore
    public Ca5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (RTLImageView) objArr[1], (WaveView) objArr[4], (DashBar) objArr[2], (ConstraintLayout) objArr[0]);
        this.w = -1;
        this.v.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.w != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.w = 1;
        }
        w();
    }
}
