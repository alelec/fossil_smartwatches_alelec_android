package com.fossil;

import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gm6 implements MembersInjector<HeartRateDetailActivity> {
    @DexIgnore
    public static void a(HeartRateDetailActivity heartRateDetailActivity, HeartRateDetailPresenter heartRateDetailPresenter) {
        heartRateDetailActivity.A = heartRateDetailPresenter;
    }
}
