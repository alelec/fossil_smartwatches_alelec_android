package com.fossil;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Af4 implements Ee4 {
    @DexIgnore
    public /* final */ FirebaseInstanceId.a a;

    @DexIgnore
    public Af4(FirebaseInstanceId.a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.Ee4
    public final void a(De4 de4) {
        this.a.d(de4);
    }
}
