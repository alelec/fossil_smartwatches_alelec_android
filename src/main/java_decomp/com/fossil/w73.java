package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W73 implements T73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;
    @DexIgnore
    public static /* final */ Xv2<Boolean> c;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        hw2.b("measurement.id.lifecycle.app_in_background_parameter", 0);
        a = hw2.d("measurement.lifecycle.app_backgrounded_engagement", false);
        b = hw2.d("measurement.lifecycle.app_backgrounded_tracking", true);
        c = hw2.d("measurement.lifecycle.app_in_background_parameter", false);
        hw2.b("measurement.id.lifecycle.app_backgrounded_tracking", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.T73
    public final boolean zza() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.T73
    public final boolean zzb() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.T73
    public final boolean zzc() {
        return c.o().booleanValue();
    }
}
