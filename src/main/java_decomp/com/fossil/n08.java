package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N08 implements Runnable {
    @DexIgnore
    public long b;
    @DexIgnore
    public O08 c;

    @DexIgnore
    public N08() {
        this(0, M08.c);
    }

    @DexIgnore
    public N08(long j, O08 o08) {
        this.b = j;
        this.c = o08;
    }
}
