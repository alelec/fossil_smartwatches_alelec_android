package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.fossil.Wb1;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kc1 implements Wb1<InputStream> {
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ Mc1 c;
    @DexIgnore
    public InputStream d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Lc1 {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public Ai(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.Lc1
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Lc1 {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public Bi(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        @Override // com.fossil.Lc1
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @DexIgnore
    public Kc1(Uri uri, Mc1 mc1) {
        this.b = uri;
        this.c = mc1;
    }

    @DexIgnore
    public static Kc1 b(Context context, Uri uri, Lc1 lc1) {
        return new Kc1(uri, new Mc1(Oa1.c(context).j().g(), lc1, Oa1.c(context).e(), context.getContentResolver()));
    }

    @DexIgnore
    public static Kc1 e(Context context, Uri uri) {
        return b(context, uri, new Ai(context.getContentResolver()));
    }

    @DexIgnore
    public static Kc1 f(Context context, Uri uri) {
        return b(context, uri, new Bi(context.getContentResolver()));
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void a() {
        InputStream inputStream = this.d;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e) {
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Gb1 c() {
        return Gb1.LOCAL;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void cancel() {
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public void d(Sa1 sa1, Wb1.Ai<? super InputStream> ai) {
        try {
            InputStream g = g();
            this.d = g;
            ai.e(g);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            ai.b(e);
        }
    }

    @DexIgnore
    public final InputStream g() throws FileNotFoundException {
        InputStream d2 = this.c.d(this.b);
        int a2 = d2 != null ? this.c.a(this.b) : -1;
        return a2 != -1 ? new Zb1(d2, a2) : d2;
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
