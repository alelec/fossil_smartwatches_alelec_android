package com.fossil;

import com.mapped.Af;
import com.mapped.V3;
import com.mapped.Xe;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ku0<K, A, B> extends Af<K, B> {
    @DexIgnore
    public /* final */ Af<K, A> a;
    @DexIgnore
    public /* final */ V3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Af.Ci<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ Af.Ci a;

        @DexIgnore
        public Ai(Af.Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        @Override // com.mapped.Af.Ci
        public void a(List<A> list, K k, K k2) {
            this.a.a(Xe.convert(Ku0.this.b, list), k, k2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Af.Ai<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ Af.Ai a;

        @DexIgnore
        public Bi(Af.Ai ai) {
            this.a = ai;
        }

        @DexIgnore
        @Override // com.mapped.Af.Ai
        public void a(List<A> list, K k) {
            this.a.a(Xe.convert(Ku0.this.b, list), k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Af.Ai<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ Af.Ai a;

        @DexIgnore
        public Ci(Af.Ai ai) {
            this.a = ai;
        }

        @DexIgnore
        @Override // com.mapped.Af.Ai
        public void a(List<A> list, K k) {
            this.a.a(Xe.convert(Ku0.this.b, list), k);
        }
    }

    @DexIgnore
    public Ku0(Af<K, A> af, V3<List<A>, List<B>> v3) {
        this.a = af;
        this.b = v3;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void addInvalidatedCallback(Xe.Ci ci) {
        this.a.addInvalidatedCallback(ci);
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadAfter(Af.Fi<K> fi, Af.Ai<K, B> ai) {
        this.a.loadAfter(fi, new Ci(ai));
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadBefore(Af.Fi<K> fi, Af.Ai<K, B> ai) {
        this.a.loadBefore(fi, new Bi(ai));
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadInitial(Af.Ei<K> ei, Af.Ci<K, B> ci) {
        this.a.loadInitial(ei, new Ai(ci));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void removeInvalidatedCallback(Xe.Ci ci) {
        this.a.removeInvalidatedCallback(ci);
    }
}
