package com.fossil;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ ProgressBar w;
    @DexIgnore
    public /* final */ RelativeLayout x;
    @DexIgnore
    public /* final */ ViewPager2 y;
    @DexIgnore
    public /* final */ TabLayout z;

    @DexIgnore
    public Z75(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton, RTLImageView rTLImageView, ProgressBar progressBar, RelativeLayout relativeLayout, ViewPager2 viewPager2, TabLayout tabLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = flexibleButton;
        this.v = rTLImageView;
        this.w = progressBar;
        this.x = relativeLayout;
        this.y = viewPager2;
        this.z = tabLayout;
        this.A = flexibleTextView3;
        this.B = flexibleTextView4;
        this.C = flexibleTextView5;
    }
}
