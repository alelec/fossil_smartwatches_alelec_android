package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vg7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;

    @DexIgnore
    public Vg7(Context context) {
        this.b = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new Bh7(this.b, null, null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            Ig7.m.e(th);
            Ig7.f(this.b, th);
        }
    }
}
