package com.fossil;

import android.os.DeadObjectException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sr2 implements Mr2<Uq2> {
    @DexIgnore
    public /* final */ /* synthetic */ Rr2 a;

    @DexIgnore
    public Sr2(Rr2 rr2) {
        this.a = rr2;
    }

    @DexIgnore
    @Override // com.fossil.Mr2
    public final void a() {
        this.a.A();
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.Mr2
    public final /* synthetic */ Uq2 getService() throws DeadObjectException {
        return (Uq2) this.a.I();
    }
}
