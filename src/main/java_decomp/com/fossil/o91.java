package com.fossil;

import com.fossil.A91;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O91<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ A91.Ai b;
    @DexIgnore
    public /* final */ T91 c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void onErrorResponse(T91 t91);
    }

    @DexIgnore
    public interface Bi<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public O91(T91 t91) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = t91;
    }

    @DexIgnore
    public O91(T t, A91.Ai ai) {
        this.d = false;
        this.a = t;
        this.b = ai;
        this.c = null;
    }

    @DexIgnore
    public static <T> O91<T> a(T91 t91) {
        return new O91<>(t91);
    }

    @DexIgnore
    public static <T> O91<T> c(T t, A91.Ai ai) {
        return new O91<>(t, ai);
    }

    @DexIgnore
    public boolean b() {
        return this.c == null;
    }
}
