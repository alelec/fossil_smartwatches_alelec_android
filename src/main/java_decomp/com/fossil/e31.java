package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E31 implements D31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<C31> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<C31> {
        @DexIgnore
        public Ai(E31 e31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, C31 c31) {
            String str = c31.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            Long l = c31.b;
            if (l == null) {
                mi.bindNull(2);
            } else {
                mi.bindLong(2, l.longValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, C31 c31) {
            a(mi, c31);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public E31(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.D31
    public Long a(String str) {
        Long l = null;
        Rh f = Rh.f("SELECT long_value FROM Preference where `key`=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            if (b2.moveToFirst() && !b2.isNull(0)) {
                l = Long.valueOf(b2.getLong(0));
            }
            return l;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.D31
    public void b(C31 c31) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert((Hh<C31>) c31);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }
}
