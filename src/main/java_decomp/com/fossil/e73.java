package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E73 implements B73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.collection.efficient_engagement_reporting_enabled_2", true);
        b = hw2.d("measurement.collection.redundant_engagement_removal_enabled", false);
        hw2.b("measurement.id.collection.redundant_engagement_removal_enabled", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.B73
    public final boolean zza() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.B73
    public final boolean zzb() {
        return b.o().booleanValue();
    }
}
