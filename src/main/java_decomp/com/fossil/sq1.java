package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Sq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sq1 createFromParcel(Parcel parcel) {
            return new Sq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sq1[] newArray(int i) {
            return new Sq1[i];
        }
    }

    @DexIgnore
    public Sq1(byte b, int i, long j) {
        super(E90.WORKOUT_GPS_DISTANCE, b, i);
        this.e = j;
    }

    @DexIgnore
    public /* synthetic */ Sq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = parcel.readLong();
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Sq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.e == ((Sq1) obj).e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutGPSDistanceRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + Long.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.H5, Long.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
    }
}
