package com.fossil;

import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.receiver.BootReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yp5 implements MembersInjector<BootReceiver> {
    @DexIgnore
    public static void a(BootReceiver bootReceiver, AlarmHelper alarmHelper) {
        bootReceiver.a = alarmHelper;
    }
}
