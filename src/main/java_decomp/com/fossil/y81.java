package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.StatFs;
import com.mapped.W6;
import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y81 {
    @DexIgnore
    public static /* final */ Y81 a; // = new Y81();

    @DexIgnore
    public final int a(int i, int i2, Bitmap.Config config) {
        return i * i2 * W81.c(config);
    }

    @DexIgnore
    public final long b(Context context, double d) {
        Wg6.c(context, "context");
        Object j = W6.j(context, ActivityManager.class);
        if (j != null) {
            ActivityManager activityManager = (ActivityManager) j;
            double d2 = (double) 1024;
            return (long) (((double) ((context.getApplicationInfo().flags & 1048576) != 0 ? activityManager.getLargeMemoryClass() : activityManager.getMemoryClass())) * d * d2 * d2);
        }
        throw new IllegalStateException(("System service of type " + ActivityManager.class + " was not found.").toString());
    }

    @DexIgnore
    public final long c(File file) {
        Wg6.c(file, "cacheDirectory");
        try {
            StatFs statFs = new StatFs(file.getAbsolutePath());
            return Bs7.j((long) (((double) (Build.VERSION.SDK_INT > 18 ? statFs.getBlockSizeLong() : (long) statFs.getBlockSize())) * 0.02d * ((double) (Build.VERSION.SDK_INT > 18 ? statFs.getBlockCountLong() : (long) statFs.getBlockCount()))), 10485760, 262144000);
        } catch (Exception e) {
            return 10485760;
        }
    }

    @DexIgnore
    public final double d(Context context) {
        Wg6.c(context, "context");
        Object j = W6.j(context, ActivityManager.class);
        if (j != null) {
            return Build.VERSION.SDK_INT < 19 || ((ActivityManager) j).isLowRamDevice() ? 0.15d : 0.25d;
        }
        throw new IllegalStateException(("System service of type " + ActivityManager.class + " was not found.").toString());
    }

    @DexIgnore
    public final Bitmap.Config e() {
        return Build.VERSION.SDK_INT >= 26 ? Bitmap.Config.HARDWARE : Bitmap.Config.ARGB_8888;
    }

    @DexIgnore
    public final double f() {
        return Build.VERSION.SDK_INT >= 26 ? 0.25d : 0.5d;
    }

    @DexIgnore
    public final File g(Context context) {
        Wg6.c(context, "context");
        File file = new File(context.getCacheDir(), "image_cache");
        file.mkdirs();
        return file;
    }
}
