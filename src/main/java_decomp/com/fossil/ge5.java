package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ge5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ View t;

    @DexIgnore
    public Ge5(Object obj, View view, int i, ImageView imageView, ImageView imageView2, FlexibleTextView flexibleTextView, View view2) {
        super(obj, view, i);
        this.q = imageView;
        this.r = imageView2;
        this.s = flexibleTextView;
        this.t = view2;
    }

    @DexIgnore
    @Deprecated
    public static Ge5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Ge5) ViewDataBinding.p(layoutInflater, 2131558681, viewGroup, z, obj);
    }

    @DexIgnore
    public static Ge5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
