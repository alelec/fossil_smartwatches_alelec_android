package com.fossil;

import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A90 extends Ox1 {
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ V80 f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public String k;
    @DexIgnore
    public Zk1 l;
    @DexIgnore
    public E90 m;
    @DexIgnore
    public JSONObject n;

    @DexIgnore
    public /* synthetic */ A90(String str, V80 v80, String str2, String str3, String str4, boolean z, String str5, Zk1 zk1, E90 e90, JSONObject jSONObject, int i2) {
        str5 = (i2 & 64) != 0 ? "" : str5;
        zk1 = (i2 & 128) != 0 ? new Zk1("", str2, "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136) : zk1;
        E90 e902 = (i2 & 256) != 0 ? new E90("", "", "", 0, "", null, 32) : e90;
        jSONObject = (i2 & 512) != 0 ? new JSONObject() : jSONObject;
        String str6 = str2 + str4 + str3 + str + Ey1.a(v80);
        this.b = System.currentTimeMillis();
        this.e = str;
        this.f = v80;
        this.g = str2;
        this.h = str3;
        this.i = str4;
        this.j = z;
        this.k = str5;
        this.l = zk1;
        this.m = e902;
        this.n = jSONObject;
        this.d = E.a("UUID.randomUUID().toString()");
    }

    @DexIgnore
    public final String a() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("timestamp", Hy1.f(this.b)).put("line_number", this.c);
        Wg6.b(put, "JSONObject().put(LogEntr\u2026.LINE_NUMBER, lineNumber)");
        put.put("phase_uuid", this.i).put("phase_name", this.h).put("entry_uuid", this.d).put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, this.e).put("type", Ey1.a(this.f)).put("is_success", this.j).put("value", this.n).put("user_id", this.m.c).put(Constants.SERIAL_NUMBER, this.l.getSerialNumber()).put("model_number", this.l.getModelNumber()).put(Constants.FIRMWARE_VERSION, this.l.getFirmwareVersion()).put("phone_model", this.m.b).put("os", this.m.f).put(Constants.OS_VERSION, this.m.a).put(FetchedAppGateKeepersManager.APPLICATION_SDK_VERSION, this.m.e).put("session_uuid", this.k).put(LegacySecondTimezoneSetting.COLUMN_TIMEZONE_OFFSET, this.m.d);
        return put;
    }
}
