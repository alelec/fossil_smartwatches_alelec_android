package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq6 extends pv5 implements hq6 {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public g37<r95> g;
    @DexIgnore
    public gq6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final iq6 a() {
            return new iq6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iq6 b;

        @DexIgnore
        public b(iq6 iq6) {
            this.b = iq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iq6 b;
        @DexIgnore
        public /* final */ /* synthetic */ r95 c;

        @DexIgnore
        public c(iq6 iq6, r95 r95) {
            this.b = iq6;
            this.c = r95;
        }

        @DexIgnore
        public final void onClick(View view) {
            iq6 iq6 = this.b;
            FlexibleSwitchCompat flexibleSwitchCompat = this.c.q;
            pq7.b(flexibleSwitchCompat, "binding.anonymousSwitch");
            iq6.N6("Usage_Data", flexibleSwitchCompat.isChecked());
            ck5 B6 = this.b.B6();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.c.q;
            pq7.b(flexibleSwitchCompat2, "binding.anonymousSwitch");
            B6.v(flexibleSwitchCompat2.isChecked());
            gq6 L6 = iq6.L6(this.b);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.c.q;
            pq7.b(flexibleSwitchCompat3, "binding.anonymousSwitch");
            L6.n(flexibleSwitchCompat3.isChecked());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iq6 b;
        @DexIgnore
        public /* final */ /* synthetic */ r95 c;

        @DexIgnore
        public d(iq6 iq6, r95 r95) {
            this.b = iq6;
            this.c = r95;
        }

        @DexIgnore
        public final void onClick(View view) {
            iq6 iq6 = this.b;
            FlexibleSwitchCompat flexibleSwitchCompat = this.c.y;
            pq7.b(flexibleSwitchCompat, "binding.scSubcribeEmail");
            iq6.N6("Emails", flexibleSwitchCompat.isChecked());
            ck5 B6 = this.b.B6();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.c.y;
            pq7.b(flexibleSwitchCompat2, "binding.scSubcribeEmail");
            B6.u(flexibleSwitchCompat2.isChecked());
            gq6 L6 = iq6.L6(this.b);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.c.y;
            pq7.b(flexibleSwitchCompat3, "binding.scSubcribeEmail");
            L6.o(flexibleSwitchCompat3.isChecked());
        }
    }

    /*
    static {
        pq7.b(iq6.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ gq6 L6(iq6 iq6) {
        gq6 gq6 = iq6.h;
        if (gq6 != null) {
            return gq6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void C(int i2, String str) {
        pq7.c(str, "message");
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void F4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<r95> g37 = this.g;
        if (g37 != null) {
            r95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.q) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void J5() {
        a();
    }

    @DexIgnore
    public final void N6(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        w6("profile_optin", hashMap);
    }

    @DexIgnore
    /* renamed from: O6 */
    public void M5(gq6 gq6) {
        pq7.c(gq6, "presenter");
        i14.l(gq6);
        pq7.b(gq6, "checkNotNull(presenter)");
        this.h = gq6;
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void P0() {
        String string = getString(2131886911);
        pq7.b(string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.hq6
    public void Y4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<r95> g37 = this.g;
        if (g37 != null) {
            r95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        r95 r95 = (r95) aq0.e(LayoutInflater.from(getContext()), 2131558599, null, false);
        r95.w.setOnClickListener(new b(this));
        FlexibleTextView flexibleTextView = r95.u;
        pq7.b(flexibleTextView, "binding.ftvDescriptionSubcribeEmail");
        hr7 hr7 = hr7.f1520a;
        String c2 = um5.c(PortfolioApp.h0.c(), 2131887216);
        pq7.b(c2, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        String format = String.format(c2, Arrays.copyOf(new Object[]{PortfolioApp.h0.c().Q()}, 1));
        pq7.b(format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        r95.q.setOnClickListener(new c(this, r95));
        r95.y.setOnClickListener(new d(this, r95));
        this.g = new g37<>(this, r95);
        pq7.b(r95, "binding");
        return r95.n();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        gq6 gq6 = this.h;
        if (gq6 != null) {
            gq6.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gq6 gq6 = this.h;
        if (gq6 != null) {
            gq6.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
