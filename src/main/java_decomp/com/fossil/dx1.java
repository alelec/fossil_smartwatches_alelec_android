package com.fossil;

import com.mapped.Wg6;
import java.nio.charset.Charset;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dx1 {
    @DexIgnore
    public static /* final */ Locale a;
    @DexIgnore
    public static /* final */ Charset b; // = Et7.a;

    /*
    static {
        Locale locale = Locale.ROOT;
        Wg6.b(locale, "Locale.ROOT");
        a = locale;
    }
    */

    @DexIgnore
    public static final Charset a() {
        return b;
    }

    @DexIgnore
    public static final Locale b() {
        return a;
    }
}
