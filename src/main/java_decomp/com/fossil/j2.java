package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J2 implements Parcelable.Creator<K2> {
    @DexIgnore
    public /* synthetic */ J2(Qg6 qg6) {
    }

    @DexIgnore
    public K2 a(Parcel parcel) {
        return new K2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public K2 createFromParcel(Parcel parcel) {
        return new K2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public K2[] newArray(int i) {
        return new K2[i];
    }
}
