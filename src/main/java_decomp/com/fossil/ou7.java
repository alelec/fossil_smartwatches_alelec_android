package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ou7 extends Vu7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(Ou7.class, "_resumed");
    @DexIgnore
    public volatile int _resumed;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ou7(com.mapped.Xe6<?> r3, java.lang.Throwable r4, boolean r5) {
        /*
            r2 = this;
            if (r4 == 0) goto L_0x0009
        L_0x0002:
            r2.<init>(r4, r5)
            r0 = 0
            r2._resumed = r0
            return
        L_0x0009:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Continuation "
            r0.append(r1)
            r0.append(r3)
            java.lang.String r1 = " was cancelled normally"
            r0.append(r1)
            java.util.concurrent.CancellationException r4 = new java.util.concurrent.CancellationException
            java.lang.String r0 = r0.toString()
            r4.<init>(r0)
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ou7.<init>(com.mapped.Xe6, java.lang.Throwable, boolean):void");
    }

    @DexIgnore
    public final boolean c() {
        return c.compareAndSet(this, 0, 1);
    }
}
