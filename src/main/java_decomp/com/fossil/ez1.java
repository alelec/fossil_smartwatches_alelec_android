package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ez1 extends Kz1 {
    @DexIgnore
    public /* final */ List<Nz1> a;

    @DexIgnore
    public Ez1(List<Nz1> list) {
        if (list != null) {
            this.a = list;
            return;
        }
        throw new NullPointerException("Null logRequests");
    }

    @DexIgnore
    @Override // com.fossil.Kz1
    public List<Nz1> b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Kz1) {
            return this.a.equals(((Kz1) obj).b());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "BatchedLogRequest{logRequests=" + this.a + "}";
    }
}
