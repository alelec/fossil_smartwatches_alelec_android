package com.fossil;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.g47;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr5 {
    @DexIgnore
    public static /* final */ HashMap<String, c> o; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, j> p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f3810a;
    @DexIgnore
    public /* final */ d b; // = new d(this.k);
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, Long> c; // = new ConcurrentHashMap<>();
    @DexIgnore
    public /* final */ yk7 d; // = zk7.a(o.INSTANCE);
    @DexIgnore
    public /* final */ yk7 e; // = zk7.a(p.INSTANCE);
    @DexIgnore
    public int f; // = 1380;
    @DexIgnore
    public int g; // = 1140;
    @DexIgnore
    public h h;
    @DexIgnore
    public f i;
    @DexIgnore
    public /* final */ Queue<NotificationBaseObj> j; // = l44.a(i24.create(20));
    @DexIgnore
    public /* final */ on5 k;
    @DexIgnore
    public /* final */ DNDSettingsDatabase l;
    @DexIgnore
    public /* final */ QuickResponseRepository m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final HashMap<String, c> a() {
            if (vr5.o.isEmpty()) {
                int i = 1;
                for (T t : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = t.getPackageName();
                    if (!pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !pq7.a(packageName, DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        vr5.o.put(t.getPackageName(), new c(i, t, false, false));
                    } else {
                        vr5.o.put(t.getPackageName(), new c(i, t, false, true));
                    }
                    i++;
                }
            }
            return vr5.o;
        }
    }

    @DexIgnore
    public enum b {
        ACTIVE,
        SILENT,
        SKIP
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ DianaNotificationObj.AApplicationName f3811a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public c(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            pq7.c(aApplicationName, "notificationApp");
            this.f3811a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.f3811a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<e> f3812a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.z();
        @DexIgnore
        public /* final */ on5 c;

        @DexIgnore
        public d(on5 on5) {
            pq7.c(on5, "mSharedPref");
            this.c = on5;
        }

        @DexIgnore
        public final e a(e eVar) {
            T t;
            boolean z;
            synchronized (this) {
                pq7.c(eVar, "notificationStatus");
                if (this.f3812a.contains(eVar)) {
                    eVar = null;
                } else if (vt7.j(eVar.e(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.h0.c()), true)) {
                    Iterator<T> it = this.f3812a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        T t2 = next;
                        if (!vt7.j(t2.i(), eVar.i(), true) || (t2.k() != eVar.k() && eVar.k() - t2.k() > 1)) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    T t3 = t;
                    if (t3 != null) {
                        t3.w(eVar.g());
                        t3.B(eVar.k());
                        return null;
                    }
                    eVar.t(b());
                    this.f3812a.add(eVar);
                    e(b() + 1);
                } else {
                    eVar.t(b());
                    this.f3812a.add(eVar);
                    e(b() + 1);
                }
                return eVar;
            }
        }

        @DexIgnore
        public final int b() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        @DexIgnore
        public final List<e> c(String str, String str2) {
            ArrayList arrayList;
            synchronized (this) {
                pq7.c(str, "realId");
                pq7.c(str2, "packageName");
                List<e> list = this.f3812a;
                arrayList = new ArrayList();
                for (T t : list) {
                    T t2 = t;
                    if (pq7.a(t2.g(), str) && pq7.a(t2.e(), str2)) {
                        arrayList.add(t);
                    }
                }
                this.f3812a.removeAll(arrayList);
            }
            return arrayList;
        }

        @DexIgnore
        public final List<e> d(String str) {
            ArrayList arrayList;
            synchronized (this) {
                pq7.c(str, "name");
                List<e> list = this.f3812a;
                arrayList = new ArrayList();
                for (T t : list) {
                    T t2 = t;
                    if (PhoneNumberUtils.compare(str, t2.h()) && pq7.a(t2.e(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                        arrayList.add(t);
                    }
                }
                this.f3812a.removeAll(arrayList);
            }
            return arrayList;
        }

        @DexIgnore
        public final void e(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.x1(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3813a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public long h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public int k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore
        public e() {
            this.f3813a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
            this.g = "";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public e(StatusBarNotification statusBarNotification) {
            this();
            String obj;
            Bundle bundle;
            DianaNotificationObj.AApplicationName a2;
            pq7.c(statusBarNotification, "sbn");
            this.f3813a = b((long) statusBarNotification.getId(), statusBarNotification.getTag());
            String packageName = statusBarNotification.getPackageName();
            pq7.b(packageName, "sbn.packageName");
            this.c = packageName;
            c cVar = vr5.q.a().get(this.c);
            String N = (cVar == null || (a2 = cVar.a()) == null || (N = a2.getAppName()) == null) ? PortfolioApp.h0.c().N(this.c) : N;
            CharSequence charSequence = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            String obj2 = (charSequence != null ? charSequence : N).toString();
            this.d = obj2;
            x(obj2);
            CharSequence charSequence2 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            if (!(charSequence2 == null || vt7.l(charSequence2))) {
                Notification notification = statusBarNotification.getNotification();
                obj = String.valueOf((notification == null || (bundle = notification.extras) == null) ? null : bundle.getCharSequence("android.bigText"));
            } else {
                String charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.text");
                obj = (charSequence3 == null ? "" : charSequence3).toString();
            }
            z(obj);
            String str = statusBarNotification.getNotification().category;
            this.g = str == null ? "" : str;
            this.i = r(statusBarNotification);
            this.j = p(statusBarNotification);
            this.h = statusBarNotification.getNotification().when;
            this.k = statusBarNotification.getNotification().priority;
            this.l = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            if (n(statusBarNotification)) {
                this.m = true;
                CharSequence charSequence4 = statusBarNotification.getNotification().tickerText;
                if (charSequence4 != null) {
                    int length = charSequence4.length();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            i2 = -1;
                            break;
                        } else if (pq7.a(String.valueOf(charSequence4.charAt(i2)), ":")) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        String obj3 = charSequence4.subSequence(0, i2).toString();
                        if (obj3 != null) {
                            x(wt7.u0(obj3).toString());
                            String obj4 = charSequence4.subSequence(i2 + 1, charSequence4.length()).toString();
                            if (obj4 != null) {
                                z(wt7.u0(obj4).toString());
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                        throw new il7("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }

        @DexIgnore
        public final void A(String str) {
            pq7.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void B(long j2) {
            this.h = j2;
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new jt7("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            boolean z = false;
            int i2 = 0;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final String b(long j2, String str) {
            return j2 + ':' + str;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final int d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            return pq7.a(this.f3813a, eVar.f3813a) && this.h == eVar.h;
        }

        @DexIgnore
        public final int f() {
            return this.k;
        }

        @DexIgnore
        public final String g() {
            return this.f3813a;
        }

        @DexIgnore
        public final String h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final String i() {
            return this.f;
        }

        @DexIgnore
        public final String j() {
            return this.d;
        }

        @DexIgnore
        public final long k() {
            return this.h;
        }

        @DexIgnore
        public final boolean l() {
            return this.l;
        }

        @DexIgnore
        public final boolean m() {
            return this.m;
        }

        @DexIgnore
        public final boolean n(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!wt7.u(tag, "sms", true) && !wt7.u(tag, "mms", true) && !wt7.u(tag, "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        @DexIgnore
        public final boolean o() {
            return this.j;
        }

        @DexIgnore
        public final boolean p(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final boolean q() {
            return this.i;
        }

        @DexIgnore
        public final boolean r(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexIgnore
        public final void s(boolean z) {
            this.l = z;
        }

        @DexIgnore
        public final void t(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void u(boolean z) {
            this.j = z;
        }

        @DexIgnore
        public final void v(String str) {
            pq7.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void w(String str) {
            pq7.c(str, "<set-?>");
            this.f3813a = str;
        }

        @DexIgnore
        public final void x(String str) {
            pq7.c(str, "value");
            this.e = a(str);
        }

        @DexIgnore
        public final void y(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final void z(String str) {
            pq7.c(str, "value");
            this.f = a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends ContentObserver {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public b f3814a; // = new b(this, 0, null, 0, 0, 15, null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    f fVar = this.this$0;
                    b d = fVar.d();
                    if (d == null) {
                        d = new b(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    fVar.f3814a = d;
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ long f3815a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public b(f fVar, long j, String str, int i, long j2) {
                pq7.c(str, "number");
                this.f3815a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ b(f fVar, long j, String str, int i, long j2, int i2, kq7 kq7) {
                this(fVar, (i2 & 1) != 0 ? -1 : j, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
            }

            @DexIgnore
            public final long a() {
                return this.f3815a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(f fVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    b d = this.this$0.d();
                    if (d != null) {
                        b bVar = this.this$0.f3814a;
                        if (bVar != null) {
                            if (bVar.a() < d.a()) {
                                if (d.c() == 3) {
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String s = vr5.this.s();
                                    local.d(s, "processMissedCall - number = " + d.b());
                                    vr5.this.U(d.b(), new Date(), g.MISSED);
                                } else if (d.c() == 5 || d.c() == 6 || d.c() == 1) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String s2 = vr5.this.s();
                                    local2.d(s2, "processHangUpCall - number = " + d.b());
                                    vr5.this.U(d.b(), new Date(), g.PICKED);
                                }
                            }
                            this.this$0.f3814a = d;
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f() {
            super(null);
            g47.a aVar = g47.f1261a;
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            if (aVar.k(applicationContext)) {
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new a(this, null), 3, null);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x007c, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x007d, code lost:
            com.fossil.so7.a(r9, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0080, code lost:
            throw r1;
         */
        @DexIgnore
        @android.annotation.SuppressLint({"MissingPermission"})
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.vr5.f.b d() {
            /*
                r10 = this;
                r8 = 0
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0     // Catch:{ Exception -> 0x0081 }
                com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x0081 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0081 }
                android.net.Uri r1 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x0081 }
                r2 = 4
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0081 }
                r3 = 0
                java.lang.String r4 = "_id"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 1
                java.lang.String r4 = "number"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 2
                java.lang.String r4 = "date"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 3
                java.lang.String r4 = "type"
                r2[r3] = r4     // Catch:{ Exception -> 0x0081 }
                r3 = 0
                r4 = 0
                java.lang.String r5 = "date DESC LIMIT 1"
                android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0081 }
                if (r9 == 0) goto L_0x0078
                boolean r0 = r9.moveToFirst()     // Catch:{ all -> 0x007a }
                if (r0 == 0) goto L_0x006f
                java.lang.String r0 = "_id"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                long r2 = r9.getLong(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "number"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r4 = r9.getString(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "type"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                int r5 = r9.getInt(r0)     // Catch:{ all -> 0x007a }
                java.lang.String r0 = "date"
                int r0 = r9.getColumnIndex(r0)     // Catch:{ all -> 0x007a }
                long r6 = r9.getLong(r0)     // Catch:{ all -> 0x007a }
                r9.close()     // Catch:{ all -> 0x007a }
                com.fossil.vr5$f$b r0 = new com.fossil.vr5$f$b     // Catch:{ all -> 0x007a }
                java.lang.String r1 = "number"
                com.fossil.pq7.b(r4, r1)     // Catch:{ all -> 0x007a }
                r1 = r10
                r0.<init>(r1, r2, r4, r5, r6)     // Catch:{ all -> 0x007a }
                r1 = 0
                com.fossil.so7.a(r9, r1)
            L_0x006e:
                return r0
            L_0x006f:
                r9.close()
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                r0 = 0
                com.fossil.so7.a(r9, r0)
            L_0x0078:
                r0 = r8
                goto L_0x006e
            L_0x007a:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x007c }
            L_0x007c:
                r1 = move-exception
                com.fossil.so7.a(r9, r0)
                throw r1
            L_0x0081:
                r0 = move-exception
                r0.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vr5 r2 = com.fossil.vr5.this
                java.lang.String r2 = r2.s()
                java.lang.String r0 = r0.getMessage()
                r1.e(r2, r0)
                r0 = r8
                goto L_0x006e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.f.d():com.fossil.vr5$f$b");
        }

        @DexIgnore
        public final void e() {
            g47.a aVar = g47.f1261a;
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            if (!aVar.k(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(vr5.this.s(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                xw7 unused = gu7.d(jv7.a(bw7.a()), null, null, new c(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            e();
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            e();
        }
    }

    @DexIgnore
    public enum g {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends e {
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public h(String str, String str2, String str3, String str4, Date date) {
            this();
            pq7.c(str, "packageName");
            pq7.c(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            pq7.c(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            pq7.c(str4, "message");
            pq7.c(date, GoalPhase.COLUMN_START_DATE);
            w(b(date.getTime(), null));
            v(str);
            A(str2);
            x(str2);
            z(str4);
            B(date.getTime());
            this.n = str3;
        }

        @DexIgnore
        public final h C() {
            h hVar = new h();
            hVar.w(g());
            hVar.t(d());
            hVar.v(e());
            hVar.x(h());
            hVar.z(i());
            hVar.A(j());
            hVar.B(k());
            hVar.s(l());
            hVar.y(q());
            hVar.u(o());
            hVar.n = this.n;
            hVar.o = this.o;
            return hVar;
        }

        @DexIgnore
        public final boolean D() {
            return this.o;
        }

        @DexIgnore
        public final String E() {
            return this.n;
        }

        @DexIgnore
        public final void F(boolean z) {
            this.o = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends e {
        @DexIgnore
        public String n;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public i(String str, String str2, String str3, long j) {
            this();
            pq7.c(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            pq7.c(str2, "message");
            pq7.c(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            w(b(j, null));
            v(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            A(str);
            x(str);
            z(str2);
            this.n = str3;
            B(j);
        }

        @DexIgnore
        public final String C() {
            return this.n;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public String f3816a;

        @DexIgnore
        public j() {
            this.f3816a = "";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public j(String str, String str2) {
            this();
            pq7.c(str, "packageName");
            pq7.c(str2, "tag");
            this.f3816a = str2;
        }

        @DexIgnore
        public final boolean a(String str) {
            pq7.c(str, "realId");
            return wt7.u(str, this.f3816a, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {459}, m = "checkConditionsToSendToDevice")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(vr5 vr5, qn7 qn7) {
            super(qn7);
            this.this$0 = vr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {390, 395, Action.ActivityTracker.TAG_ACTIVITY}, m = "handleNewLogic")
    public static final class l extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(vr5 vr5, qn7 qn7) {
            super(qn7);
            this.this$0 = vr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {338}, m = "handleNotificationAdded")
    public static final class m extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(vr5 vr5, qn7 qn7) {
            super(qn7);
            this.this$0 = vr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$initializeInCalls$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class n extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(vr5 vr5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vr5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            n nVar = new n(this.this$0, qn7);
            nVar.p$ = (iv7) obj;
            throw null;
            //return nVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((n) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x009b, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x009c, code lost:
            com.fossil.so7.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x009f, code lost:
            throw r2;
         */
        @DexIgnore
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                com.fossil.yn7.d()
                int r0 = r10.label
                if (r0 != 0) goto L_0x00c5
                com.fossil.el7.b(r11)
                com.fossil.vr5 r0 = r10.this$0
                java.util.concurrent.ConcurrentHashMap r0 = com.fossil.vr5.a(r0)
                r0.clear()
                com.fossil.g47$a r0 = com.fossil.g47.f1261a
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.h0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                android.content.Context r1 = r1.getApplicationContext()
                java.lang.String r2 = "PortfolioApp.instance.applicationContext"
                com.fossil.pq7.b(r1, r2)
                boolean r0 = r0.k(r1)
                if (r0 != 0) goto L_0x002d
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x002c:
                return r0
            L_0x002d:
                long r6 = java.lang.System.currentTimeMillis()
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.h0     // Catch:{ Exception -> 0x00a0 }
                com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x00a0 }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x00a0 }
                android.net.Uri r1 = android.provider.CallLog.Calls.CONTENT_URI     // Catch:{ Exception -> 0x00a0 }
                r2 = 3
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x00a0 }
                r3 = 0
                java.lang.String r4 = "number"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                r3 = 1
                java.lang.String r4 = "date"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                r3 = 2
                java.lang.String r4 = "type"
                r2[r3] = r4     // Catch:{ Exception -> 0x00a0 }
                java.lang.String r3 = "date >= ? AND type IN(?,?)"
                r4 = 3
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00a0 }
                r5 = 0
                r8 = 900000(0xdbba0, double:4.44659E-318)
                long r6 = r6 - r8
                java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x00a0 }
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                r5 = 1
                java.lang.String r6 = "1"
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                r5 = 2
                java.lang.String r6 = "3"
                r4[r5] = r6     // Catch:{ Exception -> 0x00a0 }
                java.lang.String r5 = "date DESC"
                android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00a0 }
                if (r1 == 0) goto L_0x00b7
            L_0x0071:
                boolean r0 = r1.moveToNext()     // Catch:{ all -> 0x0099 }
                if (r0 == 0) goto L_0x00bb
                java.lang.String r0 = "number"
                int r0 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x0099 }
                java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0099 }
                java.lang.String r2 = "date"
                int r2 = r1.getColumnIndex(r2)     // Catch:{ all -> 0x0099 }
                long r2 = r1.getLong(r2)     // Catch:{ all -> 0x0099 }
                com.fossil.vr5 r4 = r10.this$0     // Catch:{ all -> 0x0099 }
                java.util.concurrent.ConcurrentHashMap r4 = com.fossil.vr5.a(r4)     // Catch:{ all -> 0x0099 }
                java.lang.Long r2 = com.fossil.ao7.f(r2)     // Catch:{ all -> 0x0099 }
                r4.putIfAbsent(r0, r2)     // Catch:{ all -> 0x0099 }
                goto L_0x0071
            L_0x0099:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x009b }
            L_0x009b:
                r2 = move-exception
                com.fossil.so7.a(r1, r0)
                throw r2
            L_0x00a0:
                r0 = move-exception
                r0.printStackTrace()
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.vr5 r2 = r10.this$0
                java.lang.String r2 = r2.s()
                java.lang.String r0 = r0.getMessage()
                r1.e(r2, r0)
            L_0x00b7:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                goto L_0x002c
            L_0x00bb:
                r1.close()
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
                r0 = 0
                com.fossil.so7.a(r1, r0)
                goto L_0x00b7
            L_0x00c5:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.n.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends qq7 implements gp7<AudioManager> {
        @DexIgnore
        public static /* final */ o INSTANCE; // = new o();

        @DexIgnore
        public o() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final AudioManager invoke() {
            Object systemService = PortfolioApp.h0.c().getSystemService("audio");
            if (systemService != null) {
                return (AudioManager) systemService;
            }
            throw new il7("null cannot be cast to non-null type android.media.AudioManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends qq7 implements gp7<NotificationManager> {
        @DexIgnore
        public static /* final */ p INSTANCE; // = new p();

        @DexIgnore
        public p() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.gp7
        public final NotificationManager invoke() {
            Object systemService = PortfolioApp.h0.c().getSystemService("notification");
            if (systemService != null) {
                return (NotificationManager) systemService;
            }
            throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class q extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(vr5 vr5, StatusBarNotification statusBarNotification, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vr5;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            q qVar = new q(this.this$0, this.$sbn, qn7);
            qVar.p$ = (iv7) obj;
            throw null;
            //return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((q) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                e eVar = new e(this.$sbn);
                c cVar = vr5.q.a().get(eVar.e());
                if (cVar != null) {
                    eVar.x(cVar.a().getAppName());
                }
                if (this.this$0.G(this.$sbn)) {
                    this.this$0.S(new e(this.$sbn));
                } else {
                    this.this$0.T(new e(this.$sbn));
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$1", f = "DianaNotificationComponent.kt", l = {193}, m = "invokeSuspend")
    public static final class r extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(vr5 vr5, StatusBarNotification statusBarNotification, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vr5;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            r rVar = new r(this.this$0, this.$sbn, qn7);
            rVar.p$ = (iv7) obj;
            throw null;
            //return rVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((r) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                e eVar = new e(this.$sbn);
                String str = this.$sbn.getNotification().category;
                vr5 vr5 = this.this$0;
                this.L$0 = iv7;
                this.L$1 = eVar;
                this.label = 1;
                if (vr5.t(eVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                e eVar2 = (e) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", f = "DianaNotificationComponent.kt", l = {143, 169}, m = "invokeSuspend")
    public static final class s extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startTime;
        @DexIgnore
        public /* final */ /* synthetic */ g $state;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(vr5 vr5, g gVar, String str, Date date, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vr5;
            this.$state = gVar;
            this.$phoneNumber = str;
            this.$startTime = date;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            s sVar = new s(this.this$0, this.$state, this.$phoneNumber, this.$startTime, qn7);
            sVar.p$ = (iv7) obj;
            throw null;
            //return sVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((s) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String s = this.this$0.s();
                local.d(s, "process phone call state " + this.$state + " phoneNumber " + this.$phoneNumber);
                int i2 = wr5.f3990a[this.$state.ordinal()];
                if (i2 == 1) {
                    String W = this.this$0.W(this.$phoneNumber);
                    h hVar = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), W, this.$phoneNumber, "Incoming Call", this.$startTime);
                    this.this$0.Z(this.$startTime.getTime());
                    hVar.F(this.this$0.c.containsKey(this.$phoneNumber));
                    this.this$0.c.put(this.$phoneNumber, ao7.f(this.$startTime.getTime()));
                    if (!this.this$0.y(this.$phoneNumber, true)) {
                        return tl7.f3441a;
                    }
                    vr5 vr5 = this.this$0;
                    Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                    pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                    vr5.b0(applicationContext);
                    vr5 vr52 = this.this$0;
                    Context applicationContext2 = PortfolioApp.h0.c().getApplicationContext();
                    pq7.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                    vr52.X(applicationContext2);
                    h hVar2 = this.this$0.h;
                    if (hVar2 != null) {
                        this.this$0.T(hVar2);
                    }
                    this.this$0.h = hVar.C();
                    vr5 vr53 = this.this$0;
                    this.L$0 = iv7;
                    this.L$1 = W;
                    this.L$2 = hVar;
                    this.label = 1;
                    if (vr53.u(hVar, this) == d) {
                        return d;
                    }
                } else if (i2 == 2) {
                    h hVar3 = this.this$0.h;
                    if (hVar3 != null && pq7.a(hVar3.E(), this.$phoneNumber)) {
                        h hVar4 = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hVar4.w(hVar3.g());
                        hVar4.t(hVar3.d());
                        this.this$0.T(hVar4);
                        this.this$0.h = null;
                    }
                    vr5 vr54 = this.this$0;
                    Context applicationContext3 = PortfolioApp.h0.c().getApplicationContext();
                    pq7.b(applicationContext3, "PortfolioApp.instance.applicationContext");
                    vr54.b0(applicationContext3);
                } else if (i2 == 3) {
                    if (!this.this$0.y(this.$phoneNumber, true)) {
                        return tl7.f3441a;
                    }
                    h hVar5 = this.this$0.h;
                    if (hVar5 != null && pq7.a(hVar5.E(), this.$phoneNumber)) {
                        h hVar6 = new h(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hVar6.t(hVar5.d());
                        hVar6.w(hVar5.g());
                        this.this$0.T(hVar6);
                        this.this$0.h = null;
                    }
                    String W2 = this.this$0.W(this.$phoneNumber);
                    vr5 vr55 = this.this$0;
                    String packageName = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName();
                    String str = this.$phoneNumber;
                    Calendar instance = Calendar.getInstance();
                    pq7.b(instance, "Calendar.getInstance()");
                    Date time = instance.getTime();
                    pq7.b(time, "Calendar.getInstance().time");
                    h hVar7 = new h(packageName, W2, str, "Missed Call", time);
                    this.L$0 = iv7;
                    this.L$1 = W2;
                    this.label = 2;
                    if (vr55.u(hVar7, this) == d) {
                        return d;
                    }
                }
                return tl7.f3441a;
            } else if (i == 1) {
                h hVar8 = (h) this.L$2;
                String str2 = (String) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return tl7.f3441a;
            } else if (i == 2) {
                String str3 = (String) this.L$1;
                iv7 iv73 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            vr5 vr56 = this.this$0;
            Context applicationContext4 = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext4, "PortfolioApp.instance.applicationContext");
            vr56.b0(applicationContext4);
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", f = "DianaNotificationComponent.kt", l = {182}, m = "invokeSuspend")
    public static final class t extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $content;
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ long $receivedTime;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vr5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(vr5 vr5, String str, String str2, long j, qn7 qn7) {
            super(2, qn7);
            this.this$0 = vr5;
            this.$phoneNumber = str;
            this.$content = str2;
            this.$receivedTime = j;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            t tVar = new t(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, qn7);
            tVar.p$ = (iv7) obj;
            throw null;
            //return tVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((t) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FLogger.INSTANCE.getLocal().d(this.this$0.s(), "Process SMS/MMS by old solution");
                if (!this.this$0.y(this.$phoneNumber, false)) {
                    return tl7.f3441a;
                }
                String W = this.this$0.W(this.$phoneNumber);
                vr5 vr5 = this.this$0;
                i iVar = new i(W, this.$content, this.$phoneNumber, this.$receivedTime);
                this.L$0 = iv7;
                this.L$1 = W;
                this.label = 1;
                if (vr5.u(iVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    /*
    static {
        HashMap<String, j> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new j("com.facebook.orca", "SMS"));
        p = hashMap;
    }
    */

    @DexIgnore
    public vr5(on5 on5, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        pq7.c(quickResponseRepository, "mQuickResponseRepository");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = on5;
        this.l = dNDSettingsDatabase;
        this.m = quickResponseRepository;
        this.n = notificationSettingsDatabase;
        String simpleName = vr5.class.getSimpleName();
        pq7.b(simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.f3810a = simpleName;
        q.a();
    }

    @DexIgnore
    public final boolean A(String str) {
        List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            pq7.b(contactGroup, "contactGroup");
            List<Contact> contacts = contactGroup.getContacts();
            pq7.b(contacts, "contactGroup.contacts");
            if (!contacts.isEmpty()) {
                Contact contact = contactGroup.getContacts().get(0);
                pq7.b(contact, "contactGroup.contacts[0]");
                if (vt7.j(contact.getDisplayName(), str, true)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean B(java.lang.String r7) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r6.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            com.portfolio.platform.data.model.NotificationSettingsModel r0 = r0.getNotificationSettingsWithIsCallNoLiveData(r1)
            if (r0 == 0) goto L_0x004c
            int r0 = r0.getSettingsType()
            if (r0 == 0) goto L_0x0019
            if (r0 == r2) goto L_0x0047
            r3 = 2
            if (r0 == r3) goto L_0x001a
        L_0x0019:
            r1 = r2
        L_0x001a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.f3810a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isAllowMessageFromContactName() - from sender "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " is "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = ", type = type = "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.d(r3, r0)
            return r1
        L_0x0047:
            boolean r1 = r6.A(r7)
            goto L_0x001a
        L_0x004c:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.B(java.lang.String):boolean");
    }

    @DexIgnore
    public final boolean C(e eVar, c cVar) {
        return cVar.b() || eVar.f() > -2;
    }

    @DexIgnore
    public final boolean D(e eVar) {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByCallDND()");
        Object systemService = PortfolioApp.h0.c().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 8) == 0) {
                FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByCallDND() - Don't allow any calls");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityCallSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                return !J(eVar, o(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                return !J(eVar, r(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByCallDND() - Allow calls");
                return false;
            }
        } else {
            throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    public final boolean E(e eVar) {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByMessageDND()");
        Object systemService = PortfolioApp.h0.c().getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 4) == 0) {
                FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByMessageDND() - Don't allow any messages");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityMessageSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                return !J(eVar, o(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.h0.c().getApplicationContext();
                pq7.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                return !J(eVar, r(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.f3810a, "isContactBockedByMessageDND() - Allow messages");
                return false;
            }
        } else {
            throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0116, code lost:
        if (r4 >= r2) goto L_0x0118;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean F() {
        /*
        // Method dump skipped, instructions count: 325
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.F():boolean");
    }

    @DexIgnore
    public final boolean G(StatusBarNotification statusBarNotification) {
        if (Build.VERSION.SDK_INT < 26) {
            return pq7.a(statusBarNotification.getPackageName(), "com.android.server.telecom");
        }
        if (statusBarNotification.getGroupKey() == null) {
            return false;
        }
        String groupKey = statusBarNotification.getGroupKey();
        pq7.b(groupKey, "sbn.groupKey");
        return wt7.v(groupKey, "MissedCall", false, 2, null);
    }

    @DexIgnore
    public final boolean H() {
        return p().getStreamVolume(5) == 0;
    }

    @DexIgnore
    public final boolean I() {
        return p().getStreamVolume(2) == 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean J(com.fossil.vr5.e r6, java.util.List<com.fossil.cl7<java.lang.String, java.lang.String>> r7) {
        /*
            r5 = this;
            java.lang.String r2 = r6.h()
            java.lang.String r3 = r6.j()
            java.util.Iterator r4 = r7.iterator()
        L_0x000c:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x003f
            java.lang.Object r0 = r4.next()
            com.fossil.cl7 r0 = (com.fossil.cl7) r0
            java.lang.Object r1 = r0.getFirst()
            java.lang.String r1 = (java.lang.String) r1
            boolean r1 = com.fossil.pq7.a(r1, r2)
            if (r1 != 0) goto L_0x0030
            java.lang.Object r0 = r0.getSecond()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = com.fossil.pq7.a(r0, r3)
            if (r0 == 0) goto L_0x000c
        L_0x0030:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.f3810a
            java.lang.String r2 = "isSenderInContactList() - Found contact"
            r0.d(r1, r2)
            r0 = 1
        L_0x003e:
            return r0
        L_0x003f:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.f3810a
            java.lang.String r2 = "isSenderInContactList() - Not found contact"
            r0.d(r1, r2)
            r0 = 0
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.J(com.fossil.vr5$e, java.util.List):boolean");
    }

    @DexIgnore
    public final void K() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.f3810a;
        local.d(str, "onDeviceConnected, notificationList = " + this.j);
        Queue<NotificationBaseObj> queue = this.j;
        pq7.b(queue, "mDeviceNotifications");
        synchronized (queue) {
            for (NotificationBaseObj notificationBaseObj : this.j) {
                PortfolioApp c2 = PortfolioApp.h0.c();
                String n2 = n();
                pq7.b(notificationBaseObj, "notification");
                c2.h1(n2, notificationBaseObj);
            }
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final xw7 L(StatusBarNotification statusBarNotification) {
        pq7.c(statusBarNotification, "sbn");
        return gu7.d(jv7.a(bw7.a()), null, null, new q(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final void M(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.f3810a;
        local.d(str, "onNotificationRemoved, id = " + i2);
        Y(i2);
    }

    @DexIgnore
    public final void N(int i2, boolean z) {
        T t2;
        boolean z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.f3810a;
        local.d(str, "onNotificationSentResult, id = " + i2 + ", isSuccess = " + z);
        if (z) {
            Queue<NotificationBaseObj> queue = this.j;
            pq7.b(queue, "mDeviceNotifications");
            synchronized (queue) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.f3810a;
                local2.d(str2, "onNotificationSentResult, to silent notification, id = " + i2);
                Queue<NotificationBaseObj> queue2 = this.j;
                pq7.b(queue2, "mDeviceNotifications");
                Iterator<T> it = queue2.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next = it.next();
                    if (next.getUid() == i2) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (z2) {
                        t2 = next;
                        break;
                    }
                }
                T t3 = t2;
                if (t3 != null) {
                    t3.toExistedNotification();
                    tl7 tl7 = tl7.f3441a;
                }
            }
        }
    }

    @DexIgnore
    public final void O() {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "onServiceConnected");
        v();
    }

    @DexIgnore
    public final void P() {
        this.c.clear();
    }

    @DexIgnore
    public final void Q(e eVar, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.f3810a;
        local.d(str, "  Notification Posted: " + eVar.e());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.f3810a;
        local2.d(str2, "  Id: " + eVar.g());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.f3810a;
        local3.d(str3, "  Sender: " + eVar.h());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.f3810a;
        local4.d(str4, "  Title: " + eVar.j());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.f3810a;
        local5.d(str5, "  Text: " + eVar.i());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.f3810a;
        local6.d(str6, "  Summary: " + eVar.q());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.f3810a;
        local7.d(str7, "  IsOngoing: " + eVar.o());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.f3810a;
        local8.d(str8, "  When: " + eVar.k());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.f3810a;
        local9.d(str9, "  Priority: " + eVar.f());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.f3810a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.f3810a;
        local11.d(str11, "  IsDownloadEvent: " + eVar.l());
    }

    @DexIgnore
    public final xw7 R(StatusBarNotification statusBarNotification) {
        pq7.c(statusBarNotification, "sbn");
        return gu7.d(jv7.a(bw7.a()), null, null, new r(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final void S(e eVar) {
        synchronized (this) {
            pq7.c(eVar, "notification");
            for (T t2 : this.b.d(eVar.i())) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.f3810a;
                local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t2));
                DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t2.d(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", eVar.e(), "", NotificationBaseObj.ANotificationType.REMOVED), t2.j(), t2.h(), -1, t2.i(), hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t2.k()), null, null, 1536, null);
                Y(t2.d());
                PortfolioApp.h0.c().h1(n(), dianaNotificationObj);
            }
        }
    }

    @DexIgnore
    public final void T(e eVar) {
        synchronized (this) {
            pq7.c(eVar, "notification");
            FLogger.INSTANCE.getLocal().d(this.f3810a, "processNotificationRemoved");
            for (T t2 : this.b.c(eVar.g(), eVar.e())) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.f3810a;
                local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t2));
                DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t2.d(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", eVar.e(), "", NotificationBaseObj.ANotificationType.REMOVED), t2.j(), t2.h(), -1, t2.i(), hm7.i(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t2.k()), null, null, 1536, null);
                Y(t2.d());
                PortfolioApp.h0.c().h1(n(), dianaNotificationObj);
            }
        }
    }

    @DexIgnore
    public final xw7 U(String str, Date date, g gVar) {
        pq7.c(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        pq7.c(date, SampleRaw.COLUMN_START_TIME);
        pq7.c(gVar, "state");
        return gu7.d(jv7.a(bw7.a()), null, null, new s(this, gVar, str, date, null), 3, null);
    }

    @DexIgnore
    public final xw7 V(String str, String str2, long j2) {
        pq7.c(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        pq7.c(str2, "content");
        return gu7.d(jv7.a(bw7.a()), null, null, new t(this, str, str2, j2, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String W(java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 235
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.W(java.lang.String):java.lang.String");
    }

    @DexIgnore
    public final void X(Context context) {
        if (g47.f1261a.k(context)) {
            this.i = new f();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            f fVar = this.i;
            if (fVar != null) {
                contentResolver.registerContentObserver(uri, false, fVar);
                FLogger.INSTANCE.getLocal().d(this.f3810a, "registerPhoneCallObserver success");
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void Y(int i2) {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "removeNotification");
        Queue<NotificationBaseObj> queue = this.j;
        pq7.b(queue, "mDeviceNotifications");
        synchronized (queue) {
            Iterator<NotificationBaseObj> it = this.j.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getUid() == i2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.f3810a;
                        local.d(str, "notification removed, id = " + i2);
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            tl7 tl7 = tl7.f3441a;
        }
    }

    @DexIgnore
    public final void Z(long j2) {
        Iterator<Map.Entry<String, Long>> it = this.c.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Long> next = it.next();
            pq7.b(next, "iterator.next()");
            Long value = next.getValue();
            pq7.b(value, "item.value");
            if (j2 - value.longValue() >= 900000) {
                it.remove();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0257  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a0(com.fossil.vr5.e r17, com.fossil.vr5.c r18, boolean r19, com.fossil.qn7<? super com.fossil.tl7> r20) {
        /*
        // Method dump skipped, instructions count: 622
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.a0(com.fossil.vr5$e, com.fossil.vr5$c, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void b0(Context context) {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "unregisterPhoneCallObserver");
        try {
            f fVar = this.i;
            if (fVar != null) {
                context.getContentResolver().unregisterContentObserver(fVar);
                this.i = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.f3810a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    public final b k(e eVar, c cVar) {
        FLogger.INSTANCE.getLocal().d(this.f3810a, "applyDND()");
        if (Build.VERSION.SDK_INT >= 23) {
            Object systemService = PortfolioApp.h0.c().getSystemService("notification");
            if (systemService != null) {
                NotificationManager notificationManager = (NotificationManager) systemService;
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.f3810a;
                local.d(str, "currentInterruptionFilter = " + currentInterruptionFilter);
                if (currentInterruptionFilter == 2) {
                    return (pq7.a(cVar.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || pq7.a(cVar.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) ? eVar instanceof h ? ((notificationManager.getNotificationPolicy().priorityCategories & 16) == 0 || !((h) eVar).D()) ? !D(eVar) ? !I() ? b.ACTIVE : b.SILENT : b.SILENT : !I() ? b.ACTIVE : b.SILENT : D(eVar) ? b.SILENT : b.ACTIVE : (pq7.a(cVar.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName()) || eVar.m()) ? !E(eVar) ? !H() ? b.ACTIVE : b.SILENT : b.SILENT : pq7.a(eVar.c(), Alarm.TABLE_NAME) ? (w() || x()) ? b.SILENT : b.ACTIVE : b.SILENT;
                }
                if (currentInterruptionFilter == 3) {
                    return b.SILENT;
                }
                if (currentInterruptionFilter == 4) {
                    return (x() || !pq7.a(eVar.c(), Alarm.TABLE_NAME)) ? b.SILENT : b.ACTIVE;
                }
                if (pq7.a(cVar.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || pq7.a(cVar.a().getPackageName(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                    return I() ? b.SILENT : b.ACTIVE;
                }
                if (pq7.a(eVar.c(), Alarm.TABLE_NAME)) {
                    return x() ? b.SILENT : b.ACTIVE;
                }
            } else {
                throw new il7("null cannot be cast to non-null type android.app.NotificationManager");
            }
        }
        return H() ? b.SILENT : b.ACTIVE;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(com.fossil.vr5.e r6, com.fossil.vr5.c r7, boolean r8, com.fossil.qn7<? super com.fossil.tl7> r9) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.fossil.vr5.k
            if (r0 == 0) goto L_0x0038
            r0 = r9
            com.fossil.vr5$k r0 = (com.fossil.vr5.k) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0047
            if (r3 != r4) goto L_0x003f
            java.lang.Object r0 = r1.L$3
            com.fossil.vr5$e r0 = (com.fossil.vr5.e) r0
            boolean r0 = r1.Z$0
            java.lang.Object r0 = r1.L$2
            com.fossil.vr5$c r0 = (com.fossil.vr5.c) r0
            java.lang.Object r0 = r1.L$1
            com.fossil.vr5$e r0 = (com.fossil.vr5.e) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.vr5 r0 = (com.fossil.vr5) r0
            com.fossil.el7.b(r2)
        L_0x0035:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0037:
            return r0
        L_0x0038:
            com.fossil.vr5$k r0 = new com.fossil.vr5$k
            r0.<init>(r5, r9)
            r1 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.el7.b(r2)
            com.fossil.vr5$d r2 = r5.b
            com.fossil.vr5$e r2 = r2.a(r6)
            if (r2 == 0) goto L_0x0082
            boolean r3 = r5.F()
            if (r3 != 0) goto L_0x006c
            java.lang.String r3 = r2.h()
            boolean r3 = com.fossil.vt7.l(r3)
            if (r3 == 0) goto L_0x006f
            java.lang.String r3 = r2.i()
            boolean r3 = com.fossil.vt7.l(r3)
            if (r3 == 0) goto L_0x006f
        L_0x006c:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            goto L_0x0037
        L_0x006f:
            r1.L$0 = r5
            r1.L$1 = r6
            r1.L$2 = r7
            r1.Z$0 = r8
            r1.L$3 = r2
            r1.label = r4
            java.lang.Object r1 = r5.a0(r2, r7, r8, r1)
            if (r1 != r0) goto L_0x0035
            goto L_0x0037
        L_0x0082:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r5.f3810a
            java.lang.String r2 = "processNotificationAdd() - this notification existed."
            r0.d(r1, r2)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.l(com.fossil.vr5$e, com.fossil.vr5$c, boolean, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0002: ARITH  (r0v1 int) = (r3v0 int) / (60 int)), (':' char), (wrap: int : 0x0011: ARITH  (r0v3 int) = (r3v0 int) % (60 int))] */
    public final String m(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final String n() {
        return PortfolioApp.h0.c().J();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006a, code lost:
        if (r0 != null) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006c, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006f, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0070, code lost:
        if (r0 != null) goto L_0x006c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.cl7<java.lang.String, java.lang.String>> o(android.content.Context r9) {
        /*
            r8 = this;
            r6 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r8.f3810a
            java.lang.String r2 = "getAllContacts()"
            r0.d(r1, r2)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.ContentResolver r0 = r9.getContentResolver()     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 1
            java.lang.String r4 = "display_name"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 2
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x007c, all -> 0x0073 }
            if (r0 == 0) goto L_0x0070
        L_0x0034:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0057 }
            if (r1 == 0) goto L_0x0070
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = "data1"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0057 }
            com.fossil.cl7 r3 = new com.fossil.cl7     // Catch:{ Exception -> 0x0057 }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x0057 }
            r7.add(r3)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0034
        L_0x0057:
            r1 = move-exception
        L_0x0058:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x007f }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x007f }
            java.lang.String r3 = r8.f3810a     // Catch:{ all -> 0x007f }
            java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x007f }
            r2.e(r3, r4)     // Catch:{ all -> 0x007f }
            r1.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r0 == 0) goto L_0x006f
        L_0x006c:
            r0.close()
        L_0x006f:
            return r7
        L_0x0070:
            if (r0 == 0) goto L_0x006f
            goto L_0x006c
        L_0x0073:
            r0 = move-exception
            r1 = r0
            r2 = r6
        L_0x0076:
            if (r2 == 0) goto L_0x007b
            r2.close()
        L_0x007b:
            throw r1
        L_0x007c:
            r1 = move-exception
            r0 = r6
            goto L_0x0058
        L_0x007f:
            r1 = move-exception
            r2 = r0
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.o(android.content.Context):java.util.List");
    }

    @DexIgnore
    public final AudioManager p() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final NotificationManager q() {
        return (NotificationManager) this.e.getValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0072, code lost:
        if (r0 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0074, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0077, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0078, code lost:
        if (r0 != null) goto L_0x0074;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.cl7<java.lang.String, java.lang.String>> r(android.content.Context r10) {
        /*
            r9 = this;
            r6 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = r9.f3810a
            java.lang.String r2 = "getStaredContacts()"
            r0.d(r1, r2)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            android.net.Uri r1 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 0
            java.lang.String r4 = "contact_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 1
            java.lang.String r4 = "display_name"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r3 = 2
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            java.lang.String r3 = "starred = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r5 = 0
            java.lang.String r8 = "1"
            r4[r5] = r8     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0084, all -> 0x007b }
            if (r0 == 0) goto L_0x0078
        L_0x003c:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x005f }
            if (r1 == 0) goto L_0x0078
            java.lang.String r1 = "display_name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x005f }
            java.lang.String r2 = "data1"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x005f }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x005f }
            com.fossil.cl7 r3 = new com.fossil.cl7     // Catch:{ Exception -> 0x005f }
            r3.<init>(r1, r2)     // Catch:{ Exception -> 0x005f }
            r7.add(r3)     // Catch:{ Exception -> 0x005f }
            goto L_0x003c
        L_0x005f:
            r1 = move-exception
        L_0x0060:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0087 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x0087 }
            java.lang.String r3 = r9.f3810a     // Catch:{ all -> 0x0087 }
            java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x0087 }
            r2.e(r3, r4)     // Catch:{ all -> 0x0087 }
            r1.printStackTrace()     // Catch:{ all -> 0x0087 }
            if (r0 == 0) goto L_0x0077
        L_0x0074:
            r0.close()
        L_0x0077:
            return r7
        L_0x0078:
            if (r0 == 0) goto L_0x0077
            goto L_0x0074
        L_0x007b:
            r0 = move-exception
            r1 = r0
            r2 = r6
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            throw r1
        L_0x0084:
            r1 = move-exception
            r0 = r6
            goto L_0x0060
        L_0x0087:
            r1 = move-exception
            r2 = r0
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.r(android.content.Context):java.util.List");
    }

    @DexIgnore
    public final String s() {
        return this.f3810a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(com.fossil.vr5.e r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 697
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.t(com.fossil.vr5$e, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.fossil.vr5.e r14, com.fossil.qn7<? super com.fossil.tl7> r15) {
        /*
        // Method dump skipped, instructions count: 361
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.u(com.fossil.vr5$e, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final xw7 v() {
        return gu7.d(jv7.a(bw7.b()), null, null, new n(this, null), 3, null);
    }

    @DexIgnore
    public final boolean w() {
        if ((q().getNotificationPolicy().priorityCategories & 32) == 0) {
            FLogger.INSTANCE.getLocal().d(this.f3810a, "isAlarmBlockedByDND() - Don't allow any alarms");
            return true;
        }
        FLogger.INSTANCE.getLocal().d(this.f3810a, "isAlarmBlockedByDND() - Allow alarms");
        return false;
    }

    @DexIgnore
    public final boolean x() {
        return p().getStreamVolume(4) == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean y(java.lang.String r7, boolean r8) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r0 = r6.n
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r0 = r0.getNotificationSettingsDao()
            com.portfolio.platform.data.model.NotificationSettingsModel r0 = r0.getNotificationSettingsWithIsCallNoLiveData(r8)
            if (r0 == 0) goto L_0x004c
            int r0 = r0.getSettingsType()
            if (r0 == 0) goto L_0x0019
            if (r0 == r2) goto L_0x0047
            r3 = 2
            if (r0 == r3) goto L_0x001a
        L_0x0019:
            r1 = r2
        L_0x001a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = r6.f3810a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "isAllowCallOrMessageEvent() - from sender "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " is "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r5 = " because type = type = "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r2.d(r3, r0)
            return r1
        L_0x0047:
            boolean r1 = r6.z(r7)
            goto L_0x001a
        L_0x004c:
            r0 = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vr5.y(java.lang.String, boolean):boolean");
    }

    @DexIgnore
    public final boolean z(String str) {
        List<ContactGroup> allContactGroups = mn5.p.a().d().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            if (contactGroup.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }
}
