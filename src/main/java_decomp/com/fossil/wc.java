package com.fossil;

import com.mapped.Cd6;
import com.mapped.Kc6;
import com.mapped.L70;
import com.mapped.Lc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wc {
    @DexIgnore
    public static /* final */ HashMap<Lc6<String, Lt>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Wc b; // = new Wc();

    @DexIgnore
    public final C2 a(String str, byte[] bArr) {
        if (bArr.length < 3) {
            M80.c.a("DeviceEventParser", "Invalid rawData.", new Object[0]);
            return null;
        }
        Ot a2 = Ot.e.a(bArr[0]);
        Lt a3 = Lt.p.a(bArr[1]);
        byte b2 = (byte) (bArr[2] & -1);
        if (a2 == null) {
            M80.c.a("DeviceEventParser", "Unknown command type.", new Object[0]);
        }
        if (a3 == null) {
            M80.c.a("DeviceEventParser", "Invalid event type.", new Object[0]);
            return null;
        }
        a.put(new Lc6<>(str, a3), Byte.valueOf(b2));
        byte[] k = Dm7.k(bArr, 3, bArr.length);
        switch (Vc.a[a3.ordinal()]) {
            case 1:
                try {
                    Charset forName = Charset.forName("UTF-8");
                    Wg6.b(forName, "Charset.forName(\"UTF-8\")");
                    JSONObject jSONObject = new JSONObject(new String(k, forName)).getJSONObject("req");
                    int i = jSONObject.getInt("id");
                    Wg6.b(jSONObject, "requestJSON");
                    return new Q2(b2, i, jSONObject);
                } catch (JSONException e) {
                    D90.i.i(e);
                    return null;
                }
            case 2:
                return new O2(b2);
            case 3:
                break;
            case 4:
                try {
                    int i2 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                    Do1 a4 = Do1.d.a(k[4]);
                    if (a4 == null) {
                        return null;
                    }
                    int i3 = Vc.b[a4.ordinal()];
                    if (i3 == 1) {
                        return new Y1(b2, i2, new Ao1());
                    }
                    if (i3 == 2) {
                        return new Y1(b2, i2, new Fo1());
                    }
                    if (i3 == 3) {
                        return new Y1(b2, i2, new Eo1());
                    }
                    if (i3 == 4) {
                        byte b3 = k[5];
                        ByteBuffer order = ByteBuffer.wrap(k, 6, 4).order(ByteOrder.LITTLE_ENDIAN);
                        Wg6.b(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
                        return new Y1(b2, i2, new Go1(b3, order.getInt()));
                    }
                    throw new Kc6();
                } catch (Exception e2) {
                    D90.i.i(e2);
                    return null;
                }
            case 5:
                try {
                    L70 a5 = L70.d.a(k[0]);
                    if (a5 != null) {
                        return new U2(b2, a5);
                    }
                } catch (IllegalArgumentException e3) {
                    D90.i.i(e3);
                    return null;
                }
                break;
            case 6:
                try {
                    return G2.CREATOR.a(b2, k);
                } catch (IllegalArgumentException e4) {
                    D90.i.i(e4);
                    return null;
                }
            case 7:
                return new W2(b2);
            case 8:
                try {
                    return S2.CREATOR.a(b2, k);
                } catch (IllegalArgumentException e5) {
                    D90.i.i(e5);
                    return null;
                }
            case 9:
                return new Z1(b2);
            case 10:
                return new E2(b2);
            case 11:
                try {
                    Cu1 a6 = Cu1.d.a(k[0]);
                    if (a6 != null) {
                        return new I2(b2, a6, k[1]);
                    }
                } catch (Exception e6) {
                    D90.i.i(e6);
                    return null;
                }
                break;
            case 12:
                return b(b2, k);
            default:
                throw new Kc6();
        }
        return null;
    }

    @DexIgnore
    public final M2 b(byte b2, byte[] bArr) {
        int i;
        int i2;
        try {
            Gu1 a2 = Gu1.d.a(bArr[0]);
            Fu1 a3 = Fu1.d.a(bArr[1]);
            Iu1 a4 = Iu1.d.a(bArr[2]);
            byte[] k = Dm7.k(bArr, 3, bArr.length);
            if (a3 == Fu1.XOR) {
                ByteBuffer wrap = ByteBuffer.wrap(Dm7.k(bArr, 6, 10));
                i2 = Hy1.n(wrap.getShort(0));
                i = Hy1.n(wrap.getShort(2));
            } else {
                i = 0;
                i2 = 0;
            }
            if (!(a2 == null || a3 == null || a4 == null)) {
                return new M2(b2, a2, a3, a4, k, i2, i);
            }
        } catch (Exception e) {
            D90.i.i(e);
        }
        return null;
    }

    @DexIgnore
    public final void c(String str) {
        synchronized (a) {
            Set<Map.Entry<Lc6<String, Lt>, Byte>> entrySet = a.entrySet();
            Wg6.b(entrySet, "eventSequenceManager.entries");
            for (T t : entrySet) {
                if (Wg6.a(str, (String) ((Lc6) t.getKey()).getFirst())) {
                    AbstractMap abstractMap = a;
                    Object key = t.getKey();
                    Wg6.b(key, "entry.key");
                    abstractMap.put(key, (byte) -1);
                }
            }
            Cd6 cd6 = Cd6.a;
        }
    }
}
