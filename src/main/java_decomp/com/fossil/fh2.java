package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fh2 implements DynamiteModule.b {
    @DexIgnore
    @Override // com.google.android.gms.dynamite.DynamiteModule.b
    public final DynamiteModule.b.a a(Context context, String str, DynamiteModule.b.b bVar) throws DynamiteModule.a {
        DynamiteModule.b.a aVar = new DynamiteModule.b.a();
        int a2 = bVar.a(context, str);
        aVar.a = a2;
        if (a2 != 0) {
            aVar.c = -1;
        } else {
            int b = bVar.b(context, str, true);
            aVar.b = b;
            if (b != 0) {
                aVar.c = 1;
            }
        }
        return aVar;
    }
}
