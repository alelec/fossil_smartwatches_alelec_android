package com.fossil;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ds0 {
    @DexIgnore
    public static final Yr0 a(LifecycleOwner lifecycleOwner) {
        Wg6.c(lifecycleOwner, "$this$lifecycleScope");
        Lifecycle lifecycle = lifecycleOwner.getLifecycle();
        Wg6.b(lifecycle, "lifecycle");
        return Bs0.a(lifecycle);
    }
}
