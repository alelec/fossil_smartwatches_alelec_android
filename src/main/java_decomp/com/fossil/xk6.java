package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xk6 implements Factory<ActiveTimeDetailPresenter> {
    @DexIgnore
    public static ActiveTimeDetailPresenter a(Sk6 sk6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, U04 u04, PortfolioApp portfolioApp) {
        return new ActiveTimeDetailPresenter(sk6, summariesRepository, activitiesRepository, userRepository, workoutSessionRepository, fileRepository, u04, portfolioApp);
    }
}
