package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f4506a;
    @DexIgnore
    public /* final */ yo5 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository", f = "DianaRecommendedPresetRepository.kt", l = {21, 31}, m = "fetchRecommendPreset")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zo5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(zo5 zo5, qn7 qn7) {
            super(qn7);
            this.this$0 = zo5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore
    public zo5(yo5 yo5) {
        pq7.c(yo5, "remote");
        this.b = yo5;
        String simpleName = zo5.class.getSimpleName();
        pq7.b(simpleName, "DianaRecommendedPresetRe\u2026ry::class.java.simpleName");
        this.f4506a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r11, com.fossil.qn7<? super com.fossil.kz4<java.util.List<com.fossil.no5>>> r12) {
        /*
        // Method dump skipped, instructions count: 302
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zo5.a(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
