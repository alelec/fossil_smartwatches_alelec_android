package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gv2 {
    @DexIgnore
    public static /* final */ Uri a; // = Uri.parse("content://com.google.android.gsf.gservices");
    @DexIgnore
    public static /* final */ Uri b; // = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    @DexIgnore
    public static /* final */ Pattern c; // = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    @DexIgnore
    public static /* final */ Pattern d; // = Pattern.compile("^(0|false|f|off|no|n)$", 2);
    @DexIgnore
    public static /* final */ AtomicBoolean e; // = new AtomicBoolean();
    @DexIgnore
    public static HashMap<String, String> f;
    @DexIgnore
    public static /* final */ HashMap<String, Boolean> g; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Integer> h; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Long> i; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Float> j; // = new HashMap<>();
    @DexIgnore
    public static Object k;
    @DexIgnore
    public static boolean l;
    @DexIgnore
    public static String[] m; // = new String[0];

    @DexIgnore
    public static String a(ContentResolver contentResolver, String str, String str2) {
        String str3 = null;
        synchronized (Gv2.class) {
            try {
                if (f == null) {
                    e.set(false);
                    f = new HashMap<>();
                    k = new Object();
                    l = false;
                    contentResolver.registerContentObserver(a, true, new Iv2(null));
                } else if (e.getAndSet(false)) {
                    f.clear();
                    g.clear();
                    h.clear();
                    i.clear();
                    j.clear();
                    k = new Object();
                    l = false;
                }
                Object obj = k;
                if (f.containsKey(str)) {
                    String str4 = f.get(str);
                    if (str4 != null) {
                        str3 = str4;
                    }
                } else {
                    String[] strArr = m;
                    int length = strArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            Cursor query = contentResolver.query(a, null, null, new String[]{str}, null);
                            if (query != null) {
                                try {
                                    if (!query.moveToFirst()) {
                                        d(obj, str, null);
                                        if (query != null) {
                                            query.close();
                                        }
                                    } else {
                                        String string = query.getString(1);
                                        if (string != null && string.equals(null)) {
                                            string = null;
                                        }
                                        d(obj, str, string);
                                        if (string != null) {
                                            str3 = string;
                                        }
                                        if (query != null) {
                                            query.close();
                                        }
                                    }
                                } catch (Throwable th) {
                                    if (query != null) {
                                        query.close();
                                    }
                                    throw th;
                                }
                            } else if (query != null) {
                                query.close();
                            }
                        } else if (!str.startsWith(strArr[i2])) {
                            i2++;
                        } else if (!l || f.isEmpty()) {
                            f.putAll(b(contentResolver, m));
                            l = true;
                            if (f.containsKey(str)) {
                                String str5 = f.get(str);
                                if (str5 != null) {
                                    str3 = str5;
                                }
                            }
                        }
                    }
                }
            } catch (Throwable th2) {
                throw th2;
            }
        }
        return str3;
    }

    @DexIgnore
    public static Map<String, String> b(ContentResolver contentResolver, String... strArr) {
        Cursor query = contentResolver.query(b, null, null, strArr, null);
        TreeMap treeMap = new TreeMap();
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    treeMap.put(query.getString(0), query.getString(1));
                } finally {
                    query.close();
                }
            }
        }
        return treeMap;
    }

    @DexIgnore
    public static void d(Object obj, String str, String str2) {
        synchronized (Gv2.class) {
            try {
                if (obj == k) {
                    f.put(str, str2);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }
}
