package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Eg2 extends Ke2 {
    @DexIgnore
    public int b;

    @DexIgnore
    public Eg2(byte[] bArr) {
        Rc2.a(bArr.length == 25);
        this.b = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] n(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof Le2)) {
            try {
                Le2 le2 = (Le2) obj;
                if (le2.zzc() != hashCode()) {
                    return false;
                }
                Rg2 zzb = le2.zzb();
                if (zzb == null) {
                    return false;
                }
                return Arrays.equals(i(), (byte[]) Tg2.i(zzb));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    public abstract byte[] i();

    @DexIgnore
    @Override // com.fossil.Le2
    public final Rg2 zzb() {
        return Tg2.n(i());
    }

    @DexIgnore
    @Override // com.fossil.Le2
    public final int zzc() {
        return hashCode();
    }
}
