package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ky6 implements Factory<Iy6> {
    @DexIgnore
    public static Iy6 a(Jy6 jy6) {
        Iy6 a2 = jy6.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
