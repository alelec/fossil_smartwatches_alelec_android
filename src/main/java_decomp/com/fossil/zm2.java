package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.M62;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zm2 extends Ip2<Fo2> {
    @DexIgnore
    public static /* final */ Ep2 E; // = Ep2.zzbl;
    @DexIgnore
    public static /* final */ M62.Gi<Zm2> F; // = new M62.Gi<>();
    @DexIgnore
    public static /* final */ M62<M62.Di.Dii> G; // = new M62<>("Fitness.API", new Bn2(), F);
    @DexIgnore
    public static /* final */ M62<M62.Di.Bii> H; // = new M62<>("Fitness.CLIENT", new Dn2(), F);

    @DexIgnore
    public Zm2(Context context, Looper looper, Ac2 ac2, R62.Bi bi, R62.Ci ci) {
        super(context, looper, E, bi, ci, ac2);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String p() {
        return "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
        return queryLocalInterface instanceof Fo2 ? (Fo2) queryLocalInterface : new Eo2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public final int s() {
        return H62.a;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String x() {
        return "com.google.android.gms.fitness.HistoryApi";
    }
}
