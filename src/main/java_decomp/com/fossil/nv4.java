package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nv4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<Tc5> g;
    @DexIgnore
    public BCMainViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Nv4.k;
        }

        @DexIgnore
        public final Nv4 b() {
            return new Nv4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Nv4 b;

        @DexIgnore
        public Bi(Nv4 nv4) {
            this.b = nv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            Nv4.M6(this.b).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Nv4 a;

        @DexIgnore
        public Ci(Nv4 nv4) {
            this.a = nv4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Tc5 tc5 = (Tc5) Nv4.K6(this.a).a();
            if (tc5 != null) {
                Wg6.b(bool, "it");
                if (bool.booleanValue()) {
                    ConstraintLayout constraintLayout = tc5.r;
                    Wg6.b(constraintLayout, "clError");
                    constraintLayout.setVisibility(8);
                    ProgressBar progressBar = tc5.u;
                    Wg6.b(progressBar, "prg");
                    progressBar.setVisibility(0);
                    return;
                }
                ProgressBar progressBar2 = tc5.u;
                Wg6.b(progressBar2, "prg");
                progressBar2.setVisibility(8);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<BCMainViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Nv4 a;

        @DexIgnore
        public Di(Nv4 nv4) {
            this.a = nv4;
        }

        @DexIgnore
        public final void a(BCMainViewModel.Ai ai) {
            Tc5 tc5;
            if (ai.a()) {
                Tc5 tc52 = (Tc5) Nv4.K6(this.a).a();
                if (tc52 != null) {
                    ProgressBar progressBar = tc52.u;
                    Wg6.b(progressBar, "prg");
                    progressBar.setVisibility(8);
                    this.a.P6();
                }
            } else if (ai.b() && (tc5 = (Tc5) Nv4.K6(this.a).a()) != null) {
                ProgressBar progressBar2 = tc5.u;
                Wg6.b(progressBar2, "prg");
                progressBar2.setVisibility(8);
                this.a.Q6();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(BCMainViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Nv4 a;

        @DexIgnore
        public Ei(Nv4 nv4) {
            this.a = nv4;
        }

        @DexIgnore
        public final void a(Integer num) {
            Tc5 tc5 = (Tc5) Nv4.K6(this.a).a();
            if (tc5 != null) {
                FlexibleTextView flexibleTextView = tc5.t;
                Wg6.b(flexibleTextView, "ftvError");
                flexibleTextView.setText(Jl5.b.c(num, ""));
                ConstraintLayout constraintLayout = tc5.r;
                Wg6.b(constraintLayout, "clError");
                constraintLayout.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    /*
    static {
        String simpleName = Nv4.class.getSimpleName();
        Wg6.b(simpleName, "BCMainFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Nv4 nv4) {
        G37<Tc5> g37 = nv4.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCMainViewModel M6(Nv4 nv4) {
        BCMainViewModel bCMainViewModel = nv4.h;
        if (bCMainViewModel != null) {
            return bCMainViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return k;
    }

    @DexIgnore
    public final void P6() {
        Xq0 j2 = getChildFragmentManager().j();
        G37<Tc5> g37 = this.g;
        if (g37 != null) {
            Tc5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "binding.get()!!");
                View n = a2.n();
                Wg6.b(n, "binding.get()!!.root");
                j2.b(n.getId(), Av4.s.c(), Av4.s.b());
                j2.h();
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        Xq0 j2 = getChildFragmentManager().j();
        G37<Tc5> g37 = this.g;
        if (g37 != null) {
            Tc5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "binding.get()!!");
                View n = a2.n();
                Wg6.b(n, "binding.get()!!.root");
                j2.s(n.getId(), Zw4.m.b(), Zw4.m.a());
                j2.h();
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        G37<Tc5> g37 = this.g;
        if (g37 != null) {
            Tc5 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new Bi(this));
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        BCMainViewModel bCMainViewModel = this.h;
        if (bCMainViewModel != null) {
            bCMainViewModel.g().h(getViewLifecycleOwner(), new Ci(this));
            BCMainViewModel bCMainViewModel2 = this.h;
            if (bCMainViewModel2 != null) {
                bCMainViewModel2.i().h(getViewLifecycleOwner(), new Di(this));
                BCMainViewModel bCMainViewModel3 = this.h;
                if (bCMainViewModel3 != null) {
                    bCMainViewModel3.h().h(getViewLifecycleOwner(), new Ei(this));
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == Av4.s.a()) {
            Q6();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().V().a(this);
        FragmentActivity requireActivity = requireActivity();
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.f(requireActivity, po4).a(BCMainViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(re\u2026ainViewModel::class.java)");
            this.h = (BCMainViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Tc5 tc5 = (Tc5) Aq0.f(layoutInflater, 2131558647, viewGroup, false, A6());
        this.g = new G37<>(this, tc5);
        Wg6.b(tc5, "thisBinding");
        return tc5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCMainViewModel bCMainViewModel = this.h;
        if (bCMainViewModel != null) {
            bCMainViewModel.d();
            R6();
            S6();
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
