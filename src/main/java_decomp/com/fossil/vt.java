package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vt {
    @DexIgnore
    public /* synthetic */ Vt(Qg6 qg6) {
    }

    @DexIgnore
    public final Wt a(byte b) {
        Wt wt;
        Wt[] values = Wt.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                wt = null;
                break;
            }
            wt = values[i];
            if (wt.c == b) {
                break;
            }
            i++;
        }
        return wt != null ? wt : Wt.e;
    }
}
