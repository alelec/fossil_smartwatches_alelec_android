package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface E52 extends IInterface {
    @DexIgnore
    void A(Status status) throws RemoteException;

    @DexIgnore
    void W(Status status) throws RemoteException;

    @DexIgnore
    void y0(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException;
}
