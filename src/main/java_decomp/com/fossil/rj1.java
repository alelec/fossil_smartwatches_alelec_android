package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class Rj1<T extends View, Z> extends Jj1<Z> {
    @DexIgnore
    public static int g; // = Ta1.glide_custom_view_target_tag;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public View.OnAttachStateChangeListener d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static Integer e;
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ List<Pj1> b; // = new ArrayList();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public Aii d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public /* final */ WeakReference<Ai> b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = new WeakReference<>(ai);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                Ai ai = this.b.get();
                if (ai == null) {
                    return true;
                }
                ai.a();
                return true;
            }
        }

        @DexIgnore
        public Ai(View view) {
            this.a = view;
        }

        @DexIgnore
        public static int c(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                Ik1.d(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        @DexIgnore
        public void a() {
            if (!this.b.isEmpty()) {
                int g = g();
                int f = f();
                if (i(g, f)) {
                    j(g, f);
                    b();
                }
            }
        }

        @DexIgnore
        public void b() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        @DexIgnore
        public void d(Pj1 pj1) {
            int g = g();
            int f = f();
            if (i(g, f)) {
                pj1.d(g, f);
                return;
            }
            if (!this.b.contains(pj1)) {
                this.b.add(pj1);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
                Aii aii = new Aii(this);
                this.d = aii;
                viewTreeObserver.addOnPreDrawListener(aii);
            }
        }

        @DexIgnore
        public final int e(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return c(this.a.getContext());
        }

        @DexIgnore
        public final int f() {
            int paddingTop = this.a.getPaddingTop();
            int paddingBottom = this.a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return e(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop + paddingBottom);
        }

        @DexIgnore
        public final int g() {
            int paddingLeft = this.a.getPaddingLeft();
            int paddingRight = this.a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return e(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft + paddingRight);
        }

        @DexIgnore
        public final boolean h(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        public final boolean i(int i, int i2) {
            return h(i) && h(i2);
        }

        @DexIgnore
        public final void j(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((Pj1) it.next()).d(i, i2);
            }
        }

        @DexIgnore
        public void k(Pj1 pj1) {
            this.b.remove(pj1);
        }
    }

    @DexIgnore
    public Rj1(T t) {
        Ik1.d(t);
        this.b = t;
        this.c = new Ai(t);
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void a(Pj1 pj1) {
        this.c.k(pj1);
    }

    @DexIgnore
    public final Object c() {
        return this.b.getTag(g);
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void d(Bj1 bj1) {
        l(bj1);
    }

    @DexIgnore
    public final void e() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && !this.f) {
            this.b.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = true;
        }
    }

    @DexIgnore
    public final void g() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.d;
        if (onAttachStateChangeListener != null && this.f) {
            this.b.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f = false;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jj1, com.fossil.Qj1
    public void h(Drawable drawable) {
        super.h(drawable);
        e();
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public Bj1 i() {
        Object c2 = c();
        if (c2 == null) {
            return null;
        }
        if (c2 instanceof Bj1) {
            return (Bj1) c2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    @Override // com.fossil.Jj1, com.fossil.Qj1
    public void j(Drawable drawable) {
        super.j(drawable);
        this.c.b();
        if (!this.e) {
            g();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void k(Pj1 pj1) {
        this.c.d(pj1);
    }

    @DexIgnore
    public final void l(Object obj) {
        this.b.setTag(g, obj);
    }

    @DexIgnore
    public String toString() {
        return "Target for: " + this.b;
    }
}
