package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xq4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public List<Alarm> a; // = new ArrayList();
    @DexIgnore
    public Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Alarm alarm);

        @DexIgnore
        void b(Alarm alarm);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Jd5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Xq4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (ai = this.b.b.b) != null) {
                    ai.b((Alarm) this.b.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (ai = this.b.b.b) != null) {
                    ai.a((Alarm) this.b.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Xq4 xq4, Jd5 jd5) {
            super(jd5.n());
            Wg6.c(jd5, "binding");
            this.b = xq4;
            this.a = jd5;
            jd5.q.setOnClickListener(new Aii(this));
            this.a.u.setOnClickListener(new Bii(this));
            String d = ThemeManager.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d)) {
                this.a.q.setBackgroundColor(Color.parseColor(d));
            }
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(Alarm alarm, boolean z) {
            int i = 12;
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            FLogger.INSTANCE.getLocal().d(Xq4.c, "Alarm: " + alarm + ", isSingleAlarm=" + z);
            int totalMinutes = alarm.getTotalMinutes();
            int hour = alarm.getHour();
            int minute = alarm.getMinute();
            View n = this.a.n();
            Wg6.b(n, "binding.root");
            if (DateFormat.is24HourFormat(n.getContext())) {
                FlexibleTextView flexibleTextView = this.a.s;
                Wg6.b(flexibleTextView, "binding.ftvTime");
                StringBuilder sb = new StringBuilder();
                Hr7 hr7 = Hr7.a;
                Locale locale = Locale.US;
                Wg6.b(locale, "Locale.US");
                String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(hour)}, 1));
                Wg6.b(format, "java.lang.String.format(locale, format, *args)");
                sb.append(format);
                sb.append(':');
                Hr7 hr72 = Hr7.a;
                Locale locale2 = Locale.US;
                Wg6.b(locale2, "Locale.US");
                String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                Wg6.b(format2, "java.lang.String.format(locale, format, *args)");
                sb.append(format2);
                flexibleTextView.setText(sb.toString());
            } else if (totalMinutes < 720) {
                if (hour != 0) {
                    i = hour;
                }
                FlexibleTextView flexibleTextView2 = this.a.s;
                Wg6.b(flexibleTextView2, "binding.ftvTime");
                Jl5 jl5 = Jl5.b;
                StringBuilder sb2 = new StringBuilder();
                Hr7 hr73 = Hr7.a;
                Locale locale3 = Locale.US;
                Wg6.b(locale3, "Locale.US");
                String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                Wg6.b(format3, "java.lang.String.format(locale, format, *args)");
                sb2.append(format3);
                sb2.append(':');
                Hr7 hr74 = Hr7.a;
                Locale locale4 = Locale.US;
                Wg6.b(locale4, "Locale.US");
                String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                Wg6.b(format4, "java.lang.String.format(locale, format, *args)");
                sb2.append(format4);
                sb2.append(' ');
                String sb3 = sb2.toString();
                View n2 = this.a.n();
                Wg6.b(n2, "binding.root");
                String c = Um5.c(n2.getContext(), 2131886102);
                Wg6.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                flexibleTextView2.setText(jl5.g(sb3, c, 1.0f));
            } else {
                if (hour > 12) {
                    i = hour - 12;
                }
                FlexibleTextView flexibleTextView3 = this.a.s;
                Wg6.b(flexibleTextView3, "binding.ftvTime");
                Jl5 jl52 = Jl5.b;
                StringBuilder sb4 = new StringBuilder();
                Hr7 hr75 = Hr7.a;
                Locale locale5 = Locale.US;
                Wg6.b(locale5, "Locale.US");
                String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                Wg6.b(format5, "java.lang.String.format(locale, format, *args)");
                sb4.append(format5);
                sb4.append(':');
                Hr7 hr76 = Hr7.a;
                Locale locale6 = Locale.US;
                Wg6.b(locale6, "Locale.US");
                String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(minute)}, 1));
                Wg6.b(format6, "java.lang.String.format(locale, format, *args)");
                sb4.append(format6);
                sb4.append(' ');
                String sb5 = sb4.toString();
                View n3 = this.a.n();
                Wg6.b(n3, "binding.root");
                String c2 = Um5.c(n3.getContext(), 2131886104);
                Wg6.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                flexibleTextView3.setText(jl52.g(sb5, c2, 1.0f));
            }
            int[] days = alarm.getDays();
            int length = days != null ? days.length : 0;
            StringBuilder sb6 = new StringBuilder("");
            if (length > 0 && alarm.isRepeated()) {
                if (length == 7) {
                    sb6.append(Um5.c(PortfolioApp.get.instance(), 2131886108));
                    Wg6.b(sb6, "strDays.append(LanguageH\u2026_Alerts_Label__EveryDay))");
                } else {
                    if (length == 2) {
                        Xq4 xq4 = this.b;
                        if (days == null) {
                            Wg6.i();
                            throw null;
                        } else if (xq4.k(days)) {
                            sb6.append(Um5.c(PortfolioApp.get.instance(), 2131886109));
                            Wg6.b(sb6, "strDays.append(LanguageH\u2026rts_Label__EveryWeekend))");
                        }
                    }
                    if (days != null) {
                        Dm7.t(days);
                        for (int i2 = 0; i2 < length; i2++) {
                            int i3 = 1;
                            while (true) {
                                if (i3 > 7) {
                                    break;
                                } else if (i3 == days[i2]) {
                                    sb6.append(Jl5.b.h(i3));
                                    if (i2 < length - 1) {
                                        sb6.append(", ");
                                    }
                                } else {
                                    i3++;
                                }
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            FlexibleTextView flexibleTextView4 = this.a.t;
            Wg6.b(flexibleTextView4, "binding.ftvTitle");
            flexibleTextView4.setText(alarm.getTitle());
            FlexibleTextView flexibleTextView5 = this.a.r;
            Wg6.b(flexibleTextView5, "binding.ftvRepeatedDays");
            flexibleTextView5.setText(sb6.toString());
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
            Wg6.b(flexibleSwitchCompat, "binding.swEnabled");
            flexibleSwitchCompat.setChecked(alarm.isActive());
            if (z) {
                CardView cardView = this.a.q;
                Wg6.b(cardView, "binding.cvRoot");
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                Al5 a2 = Al5.a();
                Wg6.b(a2, "MeasureHelper.getInstance()");
                layoutParams.width = (int) (((float) a2.b()) - P47.b(32.0f));
                return;
            }
            CardView cardView2 = this.a.q;
            Wg6.b(cardView2, "binding.cvRoot");
            ViewGroup.LayoutParams layoutParams2 = cardView2.getLayoutParams();
            Al5 a3 = Al5.a();
            Wg6.b(a3, "MeasureHelper.getInstance()");
            layoutParams2.width = (int) ((((float) a3.b()) - P47.b(32.0f)) / 2.2f);
        }
    }

    /*
    static {
        String simpleName = Xq4.class.getSimpleName();
        Wg6.b(simpleName, "AlarmsAdapter::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final boolean k(int[] iArr) {
        if (iArr.length != 2) {
            return false;
        }
        return (iArr[0] == 1 && iArr[1] == 7) || (iArr[0] == 7 && iArr[1] == 1);
    }

    @DexIgnore
    public void l(Bi bi, int i) {
        boolean z = true;
        Wg6.c(bi, "holder");
        Alarm alarm = this.a.get(i);
        if (this.a.size() != 1) {
            z = false;
        }
        bi.a(alarm, z);
    }

    @DexIgnore
    public Bi m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Jd5 z = Jd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemAlarmBinding.inflate\u2026.context), parent, false)");
        return new Bi(this, z);
    }

    @DexIgnore
    public final void n(List<Alarm> list) {
        Wg6.c(list, "alarms");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        l(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }
}
