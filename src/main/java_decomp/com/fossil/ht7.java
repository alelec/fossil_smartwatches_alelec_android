package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ht7 {
    @DexIgnore
    Wr7 a();

    @DexIgnore
    String getValue();

    @DexIgnore
    Ht7 next();
}
