package com.fossil;

import com.mapped.Wg6;
import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q48 implements C58 {
    @DexIgnore
    public int b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ K48 d;
    @DexIgnore
    public /* final */ Inflater e;

    @DexIgnore
    public Q48(K48 k48, Inflater inflater) {
        Wg6.c(k48, "source");
        Wg6.c(inflater, "inflater");
        this.d = k48;
        this.e = inflater;
    }

    @DexIgnore
    public final boolean a() throws IOException {
        if (!this.e.needsInput()) {
            return false;
        }
        b();
        if (!(this.e.getRemaining() == 0)) {
            throw new IllegalStateException("?".toString());
        } else if (this.d.u()) {
            return true;
        } else {
            X48 x48 = this.d.t().b;
            if (x48 != null) {
                int i = x48.c;
                int i2 = x48.b;
                int i3 = i - i2;
                this.b = i3;
                this.e.setInput(x48.a, i2, i3);
                return false;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void b() {
        int i = this.b;
        if (i != 0) {
            int remaining = i - this.e.getRemaining();
            this.b -= remaining;
            this.d.skip((long) remaining);
        }
    }

    @DexIgnore
    @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.c) {
            this.e.end();
            this.c = true;
            this.d.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.C58
    public long d0(I48 i48, long j) throws IOException {
        boolean a2;
        boolean z = true;
        Wg6.c(i48, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i < 0) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        } else if (!(!this.c)) {
            throw new IllegalStateException("closed".toString());
        } else if (i == 0) {
            return 0;
        } else {
            do {
                a2 = a();
                try {
                    X48 s0 = i48.s0(1);
                    int inflate = this.e.inflate(s0.a, s0.c, (int) Math.min(j, (long) (8192 - s0.c)));
                    if (inflate > 0) {
                        s0.c += inflate;
                        long j2 = (long) inflate;
                        i48.o0(i48.p0() + j2);
                        return j2;
                    } else if (this.e.finished() || this.e.needsDictionary()) {
                        b();
                        if (s0.b == s0.c) {
                            i48.b = s0.b();
                            Y48.c.a(s0);
                        }
                        return -1;
                    }
                } catch (DataFormatException e2) {
                    throw new IOException(e2);
                }
            } while (!a2);
            throw new EOFException("source exhausted prematurely");
        }
    }

    @DexIgnore
    @Override // com.fossil.C58
    public D58 e() {
        return this.d.e();
    }
}
