package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jm1 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Jm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Jm1 a(Parcel parcel) {
            return new Jm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jm1 createFromParcel(Parcel parcel) {
            return new Jm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jm1[] newArray(int i) {
            return new Jm1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Jm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Jm1(Bt1 bt1) {
        super(Fm1.STEPS, bt1, null, null, 12);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Jm1(Bt1 bt1, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new Bt1(false) : bt1);
    }

    @DexIgnore
    public Jm1(Dt1 dt1, Et1 et1, Bt1 bt1) {
        super(Fm1.STEPS, bt1, dt1, et1);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Jm1(Dt1 dt1, Et1 et1, Bt1 bt1, int i, Qg6 qg6) {
        this(dt1, (i & 2) != 0 ? new Et1(Et1.CREATOR.a()) : et1, (i & 4) != 0 ? new Bt1(false) : bt1);
    }

    @DexIgnore
    public final Bt1 getDataConfig() {
        At1 at1 = this.c;
        if (at1 != null) {
            return (Bt1) at1;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.PercentageCircleComplicationDataConfig");
    }
}
