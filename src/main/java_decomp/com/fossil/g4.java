package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ F5 d;

    @DexIgnore
    public G4(K5 k5, G7 g7, F5 f5) {
        this.b = k5;
        this.c = g7;
        this.d = f5;
    }

    @DexIgnore
    public final void run() {
        this.b.z.e.f();
        this.b.z.e.c(new Z6(this.c, this.d));
    }
}
