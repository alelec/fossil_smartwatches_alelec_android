package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.Yb2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hl3 extends Yb2<Cl3> {
    @DexIgnore
    public Hl3(Context context, Looper looper, Yb2.Ai ai, Yb2.Bi bi) {
        super(context, looper, 93, ai, bi, null);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String p() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    @DexIgnore
    /* Return type fixed from 'android.os.IInterface' to match base method */
    @Override // com.fossil.Yb2
    public final /* synthetic */ Cl3 q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        return queryLocalInterface instanceof Cl3 ? (Cl3) queryLocalInterface : new El3(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final int s() {
        return H62.a;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String x() {
        return "com.google.android.gms.measurement.START";
    }
}
