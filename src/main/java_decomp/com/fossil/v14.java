package com.fossil;

import com.fossil.C44;
import com.fossil.D44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V14<E> extends AbstractCollection<E> implements C44<E> {
    @DexIgnore
    public transient Set<E> b;
    @DexIgnore
    public transient Set<C44.Ai<E>> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends D44.Ci<E> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.D44.Ci
        public C44<E> a() {
            return V14.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends D44.Di<E> {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.D44.Di
        public C44<E> a() {
            return V14.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<C44.Ai<E>> iterator() {
            return V14.this.entryIterator();
        }

        @DexIgnore
        public int size() {
            return V14.this.distinctElements();
        }
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    public int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.C44
    @CanIgnoreReturnValue
    public boolean add(E e) {
        add(e, 1);
        return true;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        return D44.a(this, collection);
    }

    @DexIgnore
    public abstract void clear();

    @DexIgnore
    @Override // com.fossil.C44
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    @Override // com.fossil.C44
    public abstract int count(Object obj);

    @DexIgnore
    public Set<E> createElementSet() {
        return new Ai();
    }

    @DexIgnore
    public Set<C44.Ai<E>> createEntrySet() {
        return new Bi();
    }

    @DexIgnore
    public abstract int distinctElements();

    @DexIgnore
    @Override // com.fossil.C44
    public Set<E> elementSet() {
        Set<E> set = this.b;
        if (set != null) {
            return set;
        }
        Set<E> createElementSet = createElementSet();
        this.b = createElementSet;
        return createElementSet;
    }

    @DexIgnore
    public abstract Iterator<C44.Ai<E>> entryIterator();

    @DexIgnore
    @Override // com.fossil.C44
    public Set<C44.Ai<E>> entrySet() {
        Set<C44.Ai<E>> set = this.c;
        if (set != null) {
            return set;
        }
        Set<C44.Ai<E>> createEntrySet = createEntrySet();
        this.c = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return D44.c(this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return entrySet().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
    public abstract Iterator<E> iterator();

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    public abstract int remove(Object obj, int i);

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean removeAll(Collection<?> collection) {
        return D44.f(this, collection);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection
    @CanIgnoreReturnValue
    public boolean retainAll(Collection<?> collection) {
        return D44.g(this, collection);
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    public int setCount(E e, int i) {
        return D44.h(this, e, i);
    }

    @DexIgnore
    @Override // com.fossil.C44
    @CanIgnoreReturnValue
    public boolean setCount(E e, int i, int i2) {
        return D44.i(this, e, i, i2);
    }

    @DexIgnore
    public int size() {
        return D44.j(this);
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }
}
