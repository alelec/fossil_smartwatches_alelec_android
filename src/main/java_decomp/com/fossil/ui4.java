package com.fossil;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ui4 extends TypeAdapter<Date> {
    @DexIgnore
    public /* final */ Class<? extends Date> a;
    @DexIgnore
    public /* final */ List<DateFormat> b; // = new ArrayList();

    @DexIgnore
    public Ui4(Class<? extends Date> cls, int i, int i2) {
        b(cls);
        this.a = cls;
        this.b.add(DateFormat.getDateTimeInstance(i, i2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.b.add(DateFormat.getDateTimeInstance(i, i2));
        }
        if (Xj4.e()) {
            this.b.add(Ck4.c(i, i2));
        }
    }

    @DexIgnore
    public Ui4(Class<? extends Date> cls, String str) {
        b(cls);
        this.a = cls;
        this.b.add(new SimpleDateFormat(str, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.b.add(new SimpleDateFormat(str));
        }
    }

    @DexIgnore
    public static Class<? extends Date> b(Class<? extends Date> cls) {
        if (cls == Date.class || cls == java.sql.Date.class || cls == Timestamp.class) {
            return cls;
        }
        throw new IllegalArgumentException("Date type must be one of " + Date.class + ", " + Timestamp.class + ", or " + java.sql.Date.class + " but was " + cls);
    }

    @DexIgnore
    public final Date a(String str) {
        Date c;
        synchronized (this.b) {
            Iterator<DateFormat> it = this.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    try {
                        c = it.next().parse(str);
                        break;
                    } catch (ParseException e) {
                    }
                } else {
                    try {
                        c = Jk4.c(str, new ParsePosition(0));
                        break;
                    } catch (ParseException e2) {
                        throw new Mj4(str, e2);
                    }
                }
            }
        }
        return c;
    }

    @DexIgnore
    @Override // com.google.gson.TypeAdapter
    public Date read(JsonReader jsonReader) throws IOException {
        if (jsonReader.V() == Nk4.NULL) {
            jsonReader.P();
            return null;
        }
        Date a2 = a(jsonReader.S());
        Class<? extends Date> cls = this.a;
        if (cls == Date.class) {
            return a2;
        }
        if (cls == Timestamp.class) {
            return new Timestamp(a2.getTime());
        }
        if (cls == java.sql.Date.class) {
            return new java.sql.Date(a2.getTime());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public String toString() {
        DateFormat dateFormat = this.b.get(0);
        if (dateFormat instanceof SimpleDateFormat) {
            return "DefaultDateTypeAdapter(" + ((SimpleDateFormat) dateFormat).toPattern() + ')';
        }
        return "DefaultDateTypeAdapter(" + dateFormat.getClass().getSimpleName() + ')';
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.google.gson.stream.JsonWriter, java.lang.Object] */
    @Override // com.google.gson.TypeAdapter
    public /* bridge */ /* synthetic */ void write(JsonWriter jsonWriter, Date date) throws IOException {
        write(jsonWriter, date);
    }

    @DexIgnore
    public void write(JsonWriter jsonWriter, Date date) throws IOException {
        if (date == null) {
            jsonWriter.C();
            return;
        }
        synchronized (this.b) {
            jsonWriter.g0(this.b.get(0).format(date));
        }
    }
}
