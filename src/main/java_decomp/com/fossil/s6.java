package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S6 {
    @DexIgnore
    public /* synthetic */ S6(Qg6 qg6) {
    }

    @DexIgnore
    public final U6 a(UUID uuid) {
        return Wg6.a(uuid, Hd0.y.a()) ? U6.b : U6.c;
    }

    @DexIgnore
    public final byte[] b() {
        return V6.c;
    }

    @DexIgnore
    public final byte[] c() {
        return V6.b;
    }

    @DexIgnore
    public final byte[] d() {
        return V6.a;
    }
}
