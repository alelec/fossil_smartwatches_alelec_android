package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Nd0;
import com.mapped.Qd0;
import com.mapped.Rc6;
import com.mapped.Rd0;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T90 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ S90 CREATOR; // = new S90(null);
    @DexIgnore
    public /* final */ Nd0[] b;
    @DexIgnore
    public /* final */ Ry1 c;
    @DexIgnore
    public /* final */ List<byte[]> d; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> f; // = new ArrayList();

    @DexIgnore
    public T90(Nd0[] nd0Arr, Ry1 ry1) {
        this.b = nd0Arr;
        this.c = ry1;
        for (Nd0 nd0 : nd0Arr) {
            ArrayList<U90> arrayList = new ArrayList();
            Qd0[] microAppDeclarations = nd0.getMicroAppDeclarations();
            for (Qd0 qd0 : microAppDeclarations) {
                O90 o90 = new O90(qd0.a(), qd0.getMicroAppVersion(), qd0.getVariationNumber());
                arrayList.add(new U90(o90));
                this.d.add(qd0.getData());
                Vu1 customization = qd0.getCustomization();
                if (customization != null) {
                    customization.a(o90);
                    this.e.add(customization.a());
                }
            }
            Rd0 microAppButton = nd0.getMicroAppButton();
            byte[] bArr = new byte[0];
            for (U90 u90 : arrayList) {
                bArr = Dy1.a(bArr, u90.a);
            }
            ByteBuffer order = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN);
            Wg6.b(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
            order.put(microAppButton.a());
            order.put((byte) arrayList.size());
            order.put(bArr);
            byte[] array = order.array();
            Wg6.b(array, "byteBuffer.array()");
            this.f.add(array);
        }
    }

    @DexIgnore
    public final byte[] a(List<byte[]> list) {
        byte[] bArr = new byte[0];
        for (byte[] bArr2 : list) {
            bArr = Dy1.a(bArr, bArr2);
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 1).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) list.size());
        order.put(bArr);
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(T90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            T90 t90 = (T90) obj;
            if (!Wg6.a(this.c, t90.c)) {
                return false;
            }
            return Arrays.equals(this.b, t90.b);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration");
    }

    @DexIgnore
    public int hashCode() {
        return (this.c.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (Nd0 nd0 : this.b) {
            jSONArray.put(nd0.toJSONObject());
        }
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.Z3, this.c.toJSONObject()), Jd0.a4, jSONArray), Jd0.b4, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
