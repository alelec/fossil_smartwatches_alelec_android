package com.fossil;

import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gb2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ LifecycleCallback b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Hb2 d;

    @DexIgnore
    public Gb2(Hb2 hb2, LifecycleCallback lifecycleCallback, String str) {
        this.d = hb2;
        this.b = lifecycleCallback;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        if (this.d.c > 0) {
            this.b.f(this.d.d != null ? this.d.d.getBundle(this.c) : null);
        }
        if (this.d.c >= 2) {
            this.b.j();
        }
        if (this.d.c >= 3) {
            this.b.h();
        }
        if (this.d.c >= 4) {
            this.b.k();
        }
        if (this.d.c >= 5) {
            this.b.g();
        }
    }
}
