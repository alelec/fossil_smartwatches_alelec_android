package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xx3 {
    @DexIgnore
    public /* final */ Rect a;
    @DexIgnore
    public /* final */ ColorStateList b;
    @DexIgnore
    public /* final */ ColorStateList c;
    @DexIgnore
    public /* final */ ColorStateList d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ G04 f;

    @DexIgnore
    public Xx3(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i, G04 g04, Rect rect) {
        Pn0.c(rect.left);
        Pn0.c(rect.top);
        Pn0.c(rect.right);
        Pn0.c(rect.bottom);
        this.a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.e = i;
        this.f = g04;
    }

    @DexIgnore
    public static Xx3 a(Context context, int i) {
        Pn0.b(i != 0, "Cannot create a CalendarItemStyle with a styleResId of 0");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, Tw3.MaterialCalendarItem);
        Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(Tw3.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(Tw3.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(Tw3.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(Tw3.MaterialCalendarItem_android_insetBottom, 0));
        ColorStateList a2 = Oz3.a(context, obtainStyledAttributes, Tw3.MaterialCalendarItem_itemFillColor);
        ColorStateList a3 = Oz3.a(context, obtainStyledAttributes, Tw3.MaterialCalendarItem_itemTextColor);
        ColorStateList a4 = Oz3.a(context, obtainStyledAttributes, Tw3.MaterialCalendarItem_itemStrokeColor);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(Tw3.MaterialCalendarItem_itemStrokeWidth, 0);
        G04 m = G04.b(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(Tw3.MaterialCalendarItem_itemShapeAppearanceOverlay, 0)).m();
        obtainStyledAttributes.recycle();
        return new Xx3(a2, a3, a4, dimensionPixelSize, m, rect);
    }

    @DexIgnore
    public int b() {
        return this.a.bottom;
    }

    @DexIgnore
    public int c() {
        return this.a.top;
    }

    @DexIgnore
    public void d(TextView textView) {
        C04 c04 = new C04();
        C04 c042 = new C04();
        c04.setShapeAppearanceModel(this.f);
        c042.setShapeAppearanceModel(this.f);
        c04.V(this.c);
        c04.e0((float) this.e, this.d);
        textView.setTextColor(this.b);
        Drawable rippleDrawable = Build.VERSION.SDK_INT >= 21 ? new RippleDrawable(this.b.withAlpha(30), c04, c042) : c04;
        Rect rect = this.a;
        Mo0.o0(textView, new InsetDrawable(rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }
}
