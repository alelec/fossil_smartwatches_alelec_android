package com.fossil;

import com.fossil.C44;
import com.fossil.V44;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import com.misfit.frameworks.common.constants.Constants;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class F34<K, V> extends U14<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient A34<K, ? extends U24<V>> map;
    @DexIgnore
    public /* final */ transient int size;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends F34<K, V>.f {
        @DexIgnore
        public Ai(F34 f34) {
            super(f34, null);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) {
            return b(obj, obj2);
        }

        @DexIgnore
        public Map.Entry<K, V> b(K k, V v) {
            return X34.e(k, v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends F34<K, V>.f {
        @DexIgnore
        public Bi(F34 f34) {
            super(f34, null);
        }

        @DexIgnore
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<K, V> {
        @DexIgnore
        public Y34<K, V> a;
        @DexIgnore
        public Comparator<? super K> b;
        @DexIgnore
        public Comparator<? super V> c;

        @DexIgnore
        public Ci() {
            this(A44.a().a().c());
        }

        @DexIgnore
        public Ci(Y34<K, V> y34) {
            this.a = y34;
        }

        @DexIgnore
        public F34<K, V> a() {
            if (this.c != null) {
                Iterator<Collection<V>> it = this.a.asMap().values().iterator();
                while (it.hasNext()) {
                    Collections.sort((List) it.next(), this.c);
                }
            }
            if (this.b != null) {
                S34<K, V> c2 = A44.a().a().c();
                for (Map.Entry entry : I44.from(this.b).onKeys().immutableSortedCopy(this.a.asMap().entrySet())) {
                    c2.putAll((K) entry.getKey(), (Iterable) entry.getValue());
                }
                this.a = c2;
            }
            return F34.copyOf(this.a);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ci<K, V> b(K k, V v) {
            A24.a(k, v);
            this.a.put(k, v);
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.F34$Ci<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public Ci<K, V> c(Map.Entry<? extends K, ? extends V> entry) {
            return b(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ci<K, V> d(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            for (Map.Entry<? extends K, ? extends V> entry : iterable) {
                c(entry);
            }
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<K, V> extends U24<Map.Entry<K, V>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ F34<K, V> multimap;

        @DexIgnore
        public Di(F34<K, V> f34) {
            this.multimap = f34;
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.multimap.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return this.multimap.isPartialView();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.U24, com.fossil.U24, java.lang.Iterable
        public H54<Map.Entry<K, V>> iterator() {
            return this.multimap.entryIterator();
        }

        @DexIgnore
        public int size() {
            return this.multimap.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public static /* final */ V44.Bi<F34> a; // = V44.a(F34.class, Constants.MAP);
        @DexIgnore
        public static /* final */ V44.Bi<F34> b; // = V44.a(F34.class, "size");
        @DexIgnore
        public static /* final */ V44.Bi<I34> c; // = V44.a(I34.class, "emptySet");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Fi<T> extends H54<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> b;
        @DexIgnore
        public K c;
        @DexIgnore
        public Iterator<V> d;

        @DexIgnore
        public Fi() {
            this.b = F34.this.asMap().entrySet().iterator();
            this.c = null;
            this.d = P34.h();
        }

        @DexIgnore
        public /* synthetic */ Fi(F34 f34, Ai ai) {
            this();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() || this.d.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.d.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.b.next();
                this.c = next.getKey();
                this.d = next.getValue().iterator();
            }
            return a(this.c, this.d.next());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends G34<K> {
        @DexIgnore
        public Gi() {
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.U24, com.fossil.G34
        public boolean contains(Object obj) {
            return F34.this.containsKey(obj);
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.G34
        public int count(Object obj) {
            Collection collection = (Collection) F34.this.map.get(obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        @Override // com.fossil.C44, com.fossil.G34
        public Set<K> elementSet() {
            return F34.this.keySet();
        }

        @DexIgnore
        @Override // com.fossil.G34
        public C44.Ai<K> getEntry(int i) {
            Map.Entry<K, ? extends U24<V>> entry = F34.this.map.entrySet().asList().get(i);
            return D44.d(entry.getKey(), ((Collection) entry.getValue()).size());
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return F34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<K, V> extends U24<V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        @Weak
        public /* final */ transient F34<K, V> b;

        @DexIgnore
        public Hi(F34<K, V> f34) {
            this.b = f34;
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            return this.b.containsValue(obj);
        }

        @DexIgnore
        @Override // com.fossil.U24
        public int copyIntoArray(Object[] objArr, int i) {
            Iterator it = this.b.map.values().iterator();
            while (it.hasNext()) {
                i = ((U24) it.next()).copyIntoArray(objArr, i);
            }
            return i;
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, com.fossil.U24, com.fossil.U24, java.lang.Iterable
        public H54<V> iterator() {
            return this.b.valueIterator();
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }
    }

    @DexIgnore
    public F34(A34<K, ? extends U24<V>> a34, int i) {
        this.map = a34;
        this.size = i;
    }

    @DexIgnore
    public static <K, V> Ci<K, V> builder() {
        return new Ci<>();
    }

    @DexIgnore
    public static <K, V> F34<K, V> copyOf(Y34<? extends K, ? extends V> y34) {
        if (y34 instanceof F34) {
            F34<K, V> f34 = (F34) y34;
            if (!f34.isPartialView()) {
                return f34;
            }
        }
        return Z24.copyOf((Y34) y34);
    }

    @DexIgnore
    public static <K, V> F34<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return Z24.copyOf((Iterable) iterable);
    }

    @DexIgnore
    public static <K, V> F34<K, V> of() {
        return Z24.of();
    }

    @DexIgnore
    public static <K, V> F34<K, V> of(K k, V v) {
        return Z24.of((Object) k, (Object) v);
    }

    @DexIgnore
    public static <K, V> F34<K, V> of(K k, V v, K k2, V v2) {
        return Z24.of((Object) k, (Object) v, (Object) k2, (Object) v2);
    }

    @DexIgnore
    public static <K, V> F34<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return Z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3);
    }

    @DexIgnore
    public static <K, V> F34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return Z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4);
    }

    @DexIgnore
    public static <K, V> F34<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return Z24.of((Object) k, (Object) v, (Object) k2, (Object) v2, (Object) k3, (Object) v3, (Object) k4, (Object) v4, (Object) k5, (Object) v5);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: com.fossil.A34<K, ? extends com.fossil.U24<V>>, com.fossil.A34<K, java.util.Collection<V>> */
    @Override // com.fossil.U14, com.fossil.Y34
    public A34<K, Collection<V>> asMap() {
        return (A34<K, ? extends U24<V>>) this.map;
    }

    @DexIgnore
    @Override // com.fossil.Y34
    @Deprecated
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public /* bridge */ /* synthetic */ boolean containsEntry(Object obj, Object obj2) {
        return super.containsEntry(obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public boolean containsKey(Object obj) {
        return this.map.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public boolean containsValue(Object obj) {
        return obj != null && super.containsValue(obj);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public Map<K, Collection<V>> createAsMap() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // com.fossil.U14
    public U24<Map.Entry<K, V>> createEntries() {
        return new Di(this);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public G34<K> createKeys() {
        return new Gi();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public U24<V> createValues() {
        return new Hi(this);
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public U24<Map.Entry<K, V>> entries() {
        return (U24) super.entries();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public H54<Map.Entry<K, V>> entryIterator() {
        return new Ai(this);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public abstract U24<V> get(K k);

    @DexIgnore
    @Override // com.fossil.U14
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    public abstract F34<V, K> inverse();

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.map.isPartialView();
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    public H34<K> keySet() {
        return this.map.keySet();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public G34<K> keys() {
        return (G34) super.keys();
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U14
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(Y34<? extends K, ? extends V> y34) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean putAll(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U14, com.fossil.Y34
    @CanIgnoreReturnValue
    @Deprecated
    public boolean remove(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public U24<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.U14
    @CanIgnoreReturnValue
    @Deprecated
    public U24<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.Y34
    public int size() {
        return this.size;
    }

    @DexIgnore
    @Override // com.fossil.U14
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    @Override // com.fossil.U14
    public H54<V> valueIterator() {
        return new Bi(this);
    }

    @DexIgnore
    @Override // com.fossil.U14
    public U24<V> values() {
        return (U24) super.values();
    }
}
