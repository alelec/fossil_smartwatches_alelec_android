package com.fossil;

import com.fossil.Zu3;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zv3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DataHolder b;
    @DexIgnore
    public /* final */ /* synthetic */ Zu3.Di c;

    @DexIgnore
    public Zv3(Zu3.Di di, DataHolder dataHolder) {
        this.c = di;
        this.b = dataHolder;
    }

    @DexIgnore
    public final void run() {
        Uu3 uu3 = new Uu3(this.b);
        try {
            Zu3.this.j(uu3);
        } finally {
            uu3.release();
        }
    }
}
