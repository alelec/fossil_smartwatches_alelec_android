package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ab3 implements Parcelable.Creator<Za3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Za3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                z = Ad2.m(parcel, t);
            } else if (l == 2) {
                j = Ad2.y(parcel, t);
            } else if (l == 3) {
                f = Ad2.r(parcel, t);
            } else if (l == 4) {
                j2 = Ad2.y(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                i = Ad2.v(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Za3(z, j, f, j2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Za3[] newArray(int i) {
        return new Za3[i];
    }
}
