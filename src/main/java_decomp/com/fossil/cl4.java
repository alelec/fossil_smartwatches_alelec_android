package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cl4 extends Hl4 {
    @DexIgnore
    public Bl4[] a;

    @DexIgnore
    public Cl4() {
        c();
    }

    @DexIgnore
    @Override // com.fossil.Hl4
    public /* bridge */ /* synthetic */ Hl4 b(El4 el4) throws IOException {
        return d(el4);
    }

    @DexIgnore
    public Cl4 c() {
        this.a = Bl4.d();
        return this;
    }

    @DexIgnore
    public Cl4 d(El4 el4) throws IOException {
        while (true) {
            int q = el4.q();
            if (q == 0) {
                break;
            } else if (q == 10) {
                int a2 = Jl4.a(el4, 10);
                Bl4[] bl4Arr = this.a;
                int length = bl4Arr == null ? 0 : bl4Arr.length;
                int i = a2 + length;
                Bl4[] bl4Arr2 = new Bl4[i];
                if (length != 0) {
                    System.arraycopy(this.a, 0, bl4Arr2, 0, length);
                }
                while (length < i - 1) {
                    bl4Arr2[length] = new Bl4();
                    el4.j(bl4Arr2[length]);
                    el4.q();
                    length++;
                }
                bl4Arr2[length] = new Bl4();
                el4.j(bl4Arr2[length]);
                this.a = bl4Arr2;
            } else if (!Jl4.e(el4, q)) {
                break;
            }
        }
        return this;
    }
}
