package com.fossil;

import org.joda.time.DateTimeFieldType;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F43 {
    @DexIgnore
    public static void f(byte b, byte b2, byte b3, byte b4, char[] cArr, int i) throws L13 {
        if (o(b2) || (((b << 28) + (b2 + 112)) >> 30) != 0 || o(b3) || o(b4)) {
            throw L13.zzh();
        }
        int i2 = ((b & 7) << 18) | ((b2 & 63) << 12) | ((b3 & 63) << 6) | (b4 & 63);
        cArr[i] = (char) ((char) ((i2 >>> 10) + 55232));
        cArr[i + 1] = (char) ((char) ((i2 & BasicChronology.CACHE_MASK) + 56320));
    }

    @DexIgnore
    public static void g(byte b, byte b2, byte b3, char[] cArr, int i) throws L13 {
        if (o(b2) || ((b == -32 && b2 < -96) || ((b == -19 && b2 >= -96) || o(b3)))) {
            throw L13.zzh();
        }
        cArr[i] = (char) ((char) (((b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 12) | ((b2 & 63) << 6) | (b3 & 63)));
    }

    @DexIgnore
    public static void h(byte b, byte b2, char[] cArr, int i) throws L13 {
        if (b < -62 || o(b2)) {
            throw L13.zzh();
        }
        cArr[i] = (char) ((char) (((b & 31) << 6) | (b2 & 63)));
    }

    @DexIgnore
    public static void i(byte b, char[] cArr, int i) {
        cArr[i] = (char) ((char) b);
    }

    @DexIgnore
    public static boolean l(byte b) {
        return b >= 0;
    }

    @DexIgnore
    public static boolean m(byte b) {
        return b < -32;
    }

    @DexIgnore
    public static boolean n(byte b) {
        return b < -16;
    }

    @DexIgnore
    public static boolean o(byte b) {
        return b > -65;
    }
}
