package com.fossil;

import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ay7<U, T extends U> extends Tz7<T> implements Runnable {
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public Ay7(long j, Xe6<? super U> xe6) {
        super(xe6.getContext(), xe6);
        this.f = j;
    }

    @DexIgnore
    @Override // com.fossil.Au7, com.fossil.Fx7
    public String Z() {
        return super.Z() + "(timeMillis=" + this.f + ')';
    }

    @DexIgnore
    public void run() {
        s(By7.a(this.f, this));
    }
}
