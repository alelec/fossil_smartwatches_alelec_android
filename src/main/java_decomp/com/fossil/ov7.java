package com.fossil;

import com.fossil.Dl7;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ov7 {
    @DexIgnore
    public static final String a(Object obj) {
        return obj.getClass().getSimpleName();
    }

    @DexIgnore
    public static final String b(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    @DexIgnore
    public static final String c(Xe6<?> xe6) {
        String r0;
        if (xe6 instanceof Vv7) {
            return xe6.toString();
        }
        try {
            Dl7.Ai ai = Dl7.Companion;
            r0 = Dl7.constructor-impl(xe6 + '@' + b(xe6));
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            r0 = Dl7.constructor-impl(El7.a(th));
        }
        if (Dl7.exceptionOrNull-impl(r0) != null) {
            r0 = xe6.getClass().getName() + '@' + b(xe6);
        }
        return (String) r0;
    }
}
