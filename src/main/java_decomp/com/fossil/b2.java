package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B2 implements Parcelable.Creator<C2> {
    @DexIgnore
    public /* synthetic */ B2(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public C2 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            Lt valueOf = Lt.valueOf(readString);
            parcel.setDataPosition(0);
            switch (A2.a[valueOf.ordinal()]) {
                case 1:
                    return Q2.CREATOR.a(parcel);
                case 2:
                    return O2.CREATOR.a(parcel);
                case 3:
                    return K2.CREATOR.a(parcel);
                case 4:
                    return Y1.CREATOR.a(parcel);
                case 5:
                    return U2.CREATOR.a(parcel);
                case 6:
                    return G2.CREATOR.b(parcel);
                case 7:
                    return W2.CREATOR.a(parcel);
                case 8:
                    return S2.CREATOR.b(parcel);
                case 9:
                    return Z1.CREATOR.a(parcel);
                case 10:
                    return E2.CREATOR.a(parcel);
                case 11:
                    return new I2(parcel);
                case 12:
                    return new M2(parcel);
                default:
                    throw new Kc6();
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public C2[] newArray(int i) {
        return new C2[i];
    }
}
