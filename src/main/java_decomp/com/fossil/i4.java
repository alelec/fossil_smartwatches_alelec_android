package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ B5 d;
    @DexIgnore
    public /* final */ /* synthetic */ B5 e;

    @DexIgnore
    public I4(K5 k5, G7 g7, B5 b5, B5 b52) {
        this.b = k5;
        this.c = g7;
        this.d = b5;
        this.e = b52;
    }

    @DexIgnore
    public final void run() {
        this.b.z.k.f();
        this.b.z.k.c(new Y6(this.c, this.d, this.e));
    }
}
