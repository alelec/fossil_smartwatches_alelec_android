package com.fossil;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q08 {
    @DexIgnore
    public static /* final */ long a; // = Yz7.e("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, null);
    @DexIgnore
    public static /* final */ int b; // = Yz7.d("kotlinx.coroutines.scheduler.core.pool.size", Bs7.d(Wz7.a(), 2), 1, 0, 8, null);
    @DexIgnore
    public static /* final */ int c; // = Yz7.d("kotlinx.coroutines.scheduler.max.pool.size", Bs7.i(Wz7.a() * 128, b, 2097150), 0, 2097150, 4, null);
    @DexIgnore
    public static /* final */ long d; // = TimeUnit.SECONDS.toNanos(Yz7.e("kotlinx.coroutines.scheduler.keep.alive.sec", 60, 0, 0, 12, null));
    @DexIgnore
    public static R08 e; // = L08.a;

    /*
    static {
        int unused = Yz7.d("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, null);
    }
    */
}
