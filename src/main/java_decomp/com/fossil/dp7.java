package com.fossil;

import com.mapped.Wg6;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dp7 {
    @DexIgnore
    public static final long a(Reader reader, Writer writer, int i) {
        Wg6.c(reader, "$this$copyTo");
        Wg6.c(writer, "out");
        char[] cArr = new char[i];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += (long) read;
            read = reader.read(cArr);
        }
        return j;
    }

    @DexIgnore
    public static /* synthetic */ long b(Reader reader, Writer writer, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return a(reader, writer, i);
    }

    @DexIgnore
    public static final String c(Reader reader) {
        Wg6.c(reader, "$this$readText");
        StringWriter stringWriter = new StringWriter();
        b(reader, stringWriter, 0, 2, null);
        String stringWriter2 = stringWriter.toString();
        Wg6.b(stringWriter2, "buffer.toString()");
        return stringWriter2;
    }
}
