package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.legacy.onedotfive.FavoriteMappingSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vs4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    @Vu3("name")
    public String b;
    @DexIgnore
    @Vu3("description")
    public String c;
    @DexIgnore
    @Vu3(FavoriteMappingSet.COLUMN_THUMBNAIL)
    public int d;
    @DexIgnore
    @Vu3("type")
    public String e;
    @DexIgnore
    @Vu3("step")
    public int f;
    @DexIgnore
    @Vu3("duration")
    public int g;
    @DexIgnore
    @Vu3(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Vs4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Vs4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Vs4(parcel);
        }

        @DexIgnore
        public Vs4[] b(int i) {
            return new Vs4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Vs4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Vs4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Vs4(android.os.Parcel r10) {
        /*
            r9 = this;
            r8 = 0
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r10, r0)
            java.lang.String r1 = r10.readString()
            java.lang.String r2 = r10.readString()
            int r3 = r10.readInt()
            java.lang.String r4 = r10.readString()
            if (r4 == 0) goto L_0x0039
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r4, r0)
            int r5 = r10.readInt()
            int r6 = r10.readInt()
            java.lang.String r7 = r10.readString()
            if (r7 == 0) goto L_0x0035
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r7, r0)
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            return
        L_0x0035:
            com.mapped.Wg6.i()
            throw r8
        L_0x0039:
            com.mapped.Wg6.i()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vs4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Vs4(String str, String str2, int i, String str3, int i2, int i3, String str4) {
        Wg6.c(str3, "type");
        Wg6.c(str4, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = str3;
        this.f = i2;
        this.g = i3;
        this.h = str4;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final int b() {
        return this.g;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final String d() {
        return this.h;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Vs4) {
                Vs4 vs4 = (Vs4) obj;
                if (!Wg6.a(this.b, vs4.b) || !Wg6.a(this.c, vs4.c) || this.d != vs4.d || !Wg6.a(this.e, vs4.e) || this.f != vs4.f || this.g != vs4.g || !Wg6.a(this.h, vs4.h)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.b;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        int i2 = this.d;
        String str3 = this.e;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        int i3 = this.f;
        int i4 = this.g;
        String str4 = this.h;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + hashCode3) * 31) + i3) * 31) + i4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ChallengeTemplate(name=" + this.b + ", des=" + this.c + ", thumbnail=" + this.d + ", type=" + this.e + ", target=" + this.f + ", duration=" + this.g + ", privacy=" + this.h + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
    }
}
