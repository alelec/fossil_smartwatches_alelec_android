package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import com.fossil.V18;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.mapped.An4;
import com.mapped.Cj4;
import com.mapped.InAppNotificationManager;
import com.mapped.Kk4;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiService2Dot1;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import com.portfolio.platform.manager.FileDownloadManager;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uo4 {
    @DexIgnore
    public /* final */ PortfolioApp a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ Uo4 a;
        @DexIgnore
        public /* final */ /* synthetic */ An4 b;

        @DexIgnore
        public Ai(Uo4 uo4, An4 an4) {
            this.a = uo4;
            this.b = an4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            V18.Ai h = chain.c().h();
            h.a("Content-Type", Constants.CONTENT_TYPE);
            h.a("User-Agent", Lh5.b.a());
            h.a("locale", this.a.a.X());
            String g = this.b.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ Uo4 a;
        @DexIgnore
        public /* final */ /* synthetic */ An4 b;

        @DexIgnore
        public Bi(Uo4 uo4, An4 an4) {
            this.a = uo4;
            this.b = an4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            V18.Ai h = chain.c().h();
            h.a("Content-Type", Constants.CONTENT_TYPE);
            h.a("User-Agent", Lh5.b.a());
            String g = this.b.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("locale", this.a.a.X());
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ An4 a;

        @DexIgnore
        public Ci(An4 an4) {
            this.a = an4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            UserDao userDao;
            MFUser currentUser;
            MFUser.Auth auth;
            V18.Ai h = chain.c().h();
            String j = TimeUtils.j(DateTime.now());
            UserDatabase B = EncryptedDatabaseManager.j.B();
            String accessToken = (B == null || (userDao = B.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
            h.a(ConfigItem.KEY_DATE, j);
            String g = this.a.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("Authorization", "Bearer " + accessToken);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ An4 a;

        @DexIgnore
        public Di(An4 an4) {
            this.a = an4;
        }

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            UserDao userDao;
            MFUser currentUser;
            MFUser.Auth auth;
            V18.Ai h = chain.c().h();
            String j = TimeUtils.j(DateTime.now());
            UserDatabase B = EncryptedDatabaseManager.j.B();
            String accessToken = (B == null || (userDao = B.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
            h.a(ConfigItem.KEY_DATE, j);
            String g = this.a.g();
            if (g == null) {
                g = "";
            }
            h.a("X-Active-Device", g);
            h.a("Authorization", "Bearer " + accessToken);
            return chain.d(h.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Interceptor {
        @DexIgnore
        public static /* final */ Ei a; // = new Ei();

        @DexIgnore
        @Override // okhttp3.Interceptor
        public final Response intercept(Interceptor.Chain chain) {
            return chain.d(chain.c().h().b());
        }
    }

    @DexIgnore
    public Uo4(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "mApplication");
        this.a = portfolioApp;
    }

    @DexIgnore
    public final SecureApiService2Dot1 A(An4 an4) {
        Wg6.c(an4, "sharedPreferencesManager");
        Di di = new Di(an4);
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.f(di);
        return (SecureApiService2Dot1) Tq5.g.b(SecureApiService2Dot1.class);
    }

    @DexIgnore
    public final An4 B(Context context) {
        Wg6.c(context, "context");
        return new An4(context);
    }

    @DexIgnore
    public final ShortcutApiService C(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        Wg6.c(authenticationInterceptor, "interceptor");
        Wg6.c(uq5, "authenticator");
        return (ShortcutApiService) d(authenticationInterceptor, uq5, ShortcutApiService.class);
    }

    @DexIgnore
    public final Uq4 D() {
        return new Uq4(new Wq4());
    }

    @DexIgnore
    public final N47 E() {
        return N47.d.a();
    }

    @DexIgnore
    public final Pl5 F() {
        Pl5 h = Pl5.h();
        Wg6.b(h, "WatchHelper.getInstance()");
        return h;
    }

    @DexIgnore
    public final Rl5 G(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        Wg6.c(deviceRepository, "deviceRepository");
        Wg6.c(portfolioApp, "app");
        return new Rl5(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final MFLoginWechatManager H() {
        return new MFLoginWechatManager();
    }

    @DexIgnore
    public final Xn5 I() {
        return new Xn5();
    }

    @DexIgnore
    public final DownloadServiceApi J() {
        Ei ei = Ei.a;
        Tq5 tq5 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq5.e(cacheDir);
        Tq5.g.g(3);
        Tq5.g.f(ei);
        return (DownloadServiceApi) Tq5.g.b(DownloadServiceApi.class);
    }

    @DexIgnore
    public final AlarmHelper b(An4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        Wg6.c(an4, "sharedPreferencesManager");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(alarmsRepository, "alarmsRepository");
        return new AlarmHelper(an4, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final AnalyticsHelper c() {
        return AnalyticsHelper.f.g();
    }

    @DexIgnore
    public final <S> S d(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5, Class<S> cls) {
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.b(this.a, 0) + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.c(uq5);
        Tq5.g.f(authenticationInterceptor);
        return (S) Tq5.g.b(cls);
    }

    @DexIgnore
    public final ApiServiceV2 e(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        Wg6.c(authenticationInterceptor, "interceptor");
        Wg6.c(uq5, "authenticator");
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.c(uq5);
        Tq5.g.f(authenticationInterceptor);
        return (ApiServiceV2) Tq5.g.b(ApiServiceV2.class);
    }

    @DexIgnore
    public final ApiService2Dot1 f(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        Wg6.c(authenticationInterceptor, "interceptor");
        Wg6.c(uq5, "authenticator");
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2.1") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.c(uq5);
        Tq5.g.f(authenticationInterceptor);
        return (ApiService2Dot1) Tq5.g.b(ApiService2Dot1.class);
    }

    @DexIgnore
    public final PortfolioApp g() {
        return this.a;
    }

    @DexIgnore
    public final Context h() {
        Context applicationContext = this.a.getApplicationContext();
        Wg6.b(applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ApplicationEventListener i(PortfolioApp portfolioApp, An4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, Cj4 cj4, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, RingStyleRepository ringStyleRepository, ProfileRepository profileRepository, FriendRepository friendRepository, Tt4 tt4, FileRepository fileRepository, FCMRepository fCMRepository, WorkoutSettingRepository workoutSettingRepository, FlagRepository flagRepository, BuddyChallengeManager buddyChallengeManager, ThemeRepository themeRepository, WFAssetRepository wFAssetRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository dianaPresetRepository2, DianaRecommendedPresetRepository dianaRecommendedPresetRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        Wg6.c(portfolioApp, "app");
        Wg6.c(an4, "sharedPreferencesManager");
        Wg6.c(hybridPresetRepository, "hybridPresetRepository");
        Wg6.c(categoryRepository, "categoryRepository");
        Wg6.c(watchAppRepository, "watchAppRepository");
        Wg6.c(complicationRepository, "complicationRepository");
        Wg6.c(microAppRepository, "microAppRepository");
        Wg6.c(dianaPresetRepository, "dianaPresetRepository");
        Wg6.c(deviceRepository, "deviceRepository");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(alarmsRepository, "alarmsRepository");
        Wg6.c(cj4, "deviceSettingFactory");
        Wg6.c(dianaWatchFaceRepository, "watchFaceRepository");
        Wg6.c(watchLocalizationRepository, "watchLocalizationRepository");
        Wg6.c(ringStyleRepository, "ringStyleRepository");
        Wg6.c(profileRepository, "socialProfileRepository");
        Wg6.c(friendRepository, "socialFriendRepository");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(fCMRepository, "fcmRepository");
        Wg6.c(workoutSettingRepository, "workoutSettingRepository");
        Wg6.c(flagRepository, "flagRepository");
        Wg6.c(buddyChallengeManager, "buddyChallengeManager");
        Wg6.c(themeRepository, "themeRepository");
        Wg6.c(wFAssetRepository, "assetRepository");
        Wg6.c(dianaPresetRepository2, "dianaWatchFaceRepository");
        Wg6.c(dianaRecommendedPresetRepository, "dianaRecommendedPresetRepository");
        Wg6.c(wFBackgroundPhotoRepository, "wfBackgroundPhotoRepository");
        Wg6.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        return new ApplicationEventListener(portfolioApp, an4, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, ringStyleRepository, deviceRepository, userRepository, cj4, alarmsRepository, dianaWatchFaceRepository, watchLocalizationRepository, profileRepository, friendRepository, tt4, fileRepository, fCMRepository, workoutSettingRepository, flagRepository, buddyChallengeManager, themeRepository, wFAssetRepository, dianaPresetRepository2, dianaRecommendedPresetRepository, wFBackgroundPhotoRepository, dianaAppSettingRepository);
    }

    @DexIgnore
    public final AuthApiGuestService j(An4 an4) {
        Wg6.c(an4, "sharedPreferencesManager");
        Ai ai = new Ai(this, an4);
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2.1") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.f(ai);
        return (AuthApiGuestService) Tq5.g.b(AuthApiGuestService.class);
    }

    @DexIgnore
    public final AuthApiUserService k(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        Wg6.c(authenticationInterceptor, "interceptor");
        Wg6.c(uq5, "authenticator");
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2.1") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.c(uq5);
        Tq5.g.f(authenticationInterceptor);
        return (AuthApiUserService) Tq5.g.b(AuthApiUserService.class);
    }

    @DexIgnore
    public final ContentResolver l() {
        ContentResolver contentResolver = this.a.getContentResolver();
        Wg6.b(contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final Un5 m() {
        return new Un5();
    }

    @DexIgnore
    public final FileDownloadManager n() {
        return new FileDownloadManager();
    }

    @DexIgnore
    public final FirmwareFileRepository o() {
        Context applicationContext = this.a.getApplicationContext();
        Wg6.b(applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    public final GoogleApiService p(AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        Wg6.c(authenticationInterceptor, "interceptor");
        Wg6.c(uq5, "authenticator");
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.b(this.a, 4) + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.c(uq5);
        Tq5.g.f(authenticationInterceptor);
        return (GoogleApiService) Tq5.g.b(GoogleApiService.class);
    }

    @DexIgnore
    public final Kk4 q(Context context, U04 u04, An4 an4) {
        Wg6.c(context, "context");
        Wg6.c(u04, "appExecutors");
        Wg6.c(an4, "sharedPreferencesManager");
        return new Kk4(context, u04.b(), an4);
    }

    @DexIgnore
    public final GuestApiService r(An4 an4) {
        Wg6.c(an4, "sharedPreferencesManager");
        Bi bi = new Bi(this, an4);
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.f(bi);
        return (GuestApiService) Tq5.g.b(GuestApiService.class);
    }

    @DexIgnore
    public final InAppNotificationManager s(InAppNotificationRepository inAppNotificationRepository) {
        Wg6.c(inAppNotificationRepository, "repository");
        return new InAppNotificationManager(inAppNotificationRepository);
    }

    @DexIgnore
    public final LinkStreamingManager t(HybridPresetRepository hybridPresetRepository, BuddyChallengeManager buddyChallengeManager, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, An4 an4, SetNotificationUseCase setNotificationUseCase, MusicControlComponent musicControlComponent, WorkoutTetherGpsManager workoutTetherGpsManager) {
        Wg6.c(hybridPresetRepository, "hybridPresetRepository");
        Wg6.c(buddyChallengeManager, "buddyChallengeManager");
        Wg6.c(quickResponseRepository, "quickResponseRepository");
        Wg6.c(alarmsRepository, "alarmsRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        Wg6.c(setNotificationUseCase, "setNotificationUseCase");
        Wg6.c(musicControlComponent, "musicControlComponent");
        Wg6.c(workoutTetherGpsManager, "mWorkoutTetherGpsManager");
        return new LinkStreamingManager(hybridPresetRepository, alarmsRepository, buddyChallengeManager, workoutTetherGpsManager, quickResponseRepository, musicControlComponent, setNotificationUseCase, an4);
    }

    @DexIgnore
    public final Ct0 u() {
        Ct0 b = Ct0.b(this.a);
        Wg6.b(b, "LocalBroadcastManager.getInstance(mApplication)");
        return b;
    }

    @DexIgnore
    public final Vn5 v() {
        return new Vn5();
    }

    @DexIgnore
    public final Lo4 w() {
        Lo4 h = Lo4.h(this.a);
        Wg6.b(h, "MFLocationService.getInstance(mApplication)");
        return h;
    }

    @DexIgnore
    public final MigrationHelper x(An4 an4, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, MigrationManager migrationManager) {
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(deviceRepository, "deviceRepository");
        Wg6.c(dianaPresetRepository, "oldPresetRepository");
        Wg6.c(migrationManager, "migrationManager");
        return new MigrationHelper(an4, deviceRepository, dianaPresetRepository, migrationManager);
    }

    @DexIgnore
    public final MigrationManager y(An4 an4, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, Cj4 cj4, FlagRepository flagRepository, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository dianaPresetRepository2, WFAssetRepository wFAssetRepository, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(getHybridDeviceSettingUseCase, "getHybridUseCase");
        Wg6.c(notificationsRepository, "mNotificationRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(goalTrackingRepository, "goalTrackingRepo");
        Wg6.c(deviceDao, "deviceDao");
        Wg6.c(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        Wg6.c(microAppLastSettingRepository, "microAppLastSettingRepository");
        Wg6.c(cj4, "deviceSettingFactory");
        Wg6.c(flagRepository, "flagRepository");
        Wg6.c(dianaPresetRepository, "oldPresetRepository");
        Wg6.c(watchFaceRepository, "oldWatchFaceRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(wFBackgroundPhotoRepository, "newPhotoWFBackgroundPhotoRepository");
        Wg6.c(dianaPresetRepository2, "newPresetRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(deviceRepository, "deviceRepository");
        Wg6.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        Wg6.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        return new MigrationManager(an4, userRepository, notificationsRepository, getHybridDeviceSettingUseCase, portfolioApp, goalTrackingRepository, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, cj4, flagRepository, dianaPresetRepository, watchFaceRepository, fileRepository, wFBackgroundPhotoRepository, dianaPresetRepository2, wFAssetRepository, deviceRepository, dianaAppSettingRepository, dianaWatchFaceRepository);
    }

    @DexIgnore
    public final SecureApiService z(An4 an4) {
        Wg6.c(an4, "sharedPreferencesManager");
        Ci ci = new Ci(an4);
        Tq5 tq5 = Tq5.g;
        tq5.d(H37.b.c(this.a, 0, "2") + "/");
        Tq5 tq52 = Tq5.g;
        File cacheDir = this.a.getCacheDir();
        Wg6.b(cacheDir, "mApplication.cacheDir");
        tq52.e(cacheDir);
        Tq5.g.f(ci);
        return (SecureApiService) Tq5.g.b(SecureApiService.class);
    }
}
