package com.fossil;

import com.mapped.Kc6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Oi5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    SPINNING("Spinning"),
    OUTDOOR_CYCLING("outdoor_cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALK("walk"),
    ROW_MACHINE("row_machine"),
    HIKING("hiking");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Lc6<Integer, Integer> a(Oi5 oi5) {
            if (oi5 == null) {
                oi5 = Oi5.UNKNOWN;
            }
            if (oi5 != null) {
                switch (Ni5.e[oi5.ordinal()]) {
                    case 1:
                        return new Lc6<>(2131231216, 2131886616);
                    case 2:
                        return new Lc6<>(2131231217, 2131886617);
                    case 3:
                        return new Lc6<>(2131231211, 2131886611);
                    case 4:
                        return new Lc6<>(2131231218, 2131886618);
                    case 5:
                        return new Lc6<>(2131231212, 2131886612);
                    case 6:
                        return new Lc6<>(2131231220, 2131886620);
                    case 7:
                        return new Lc6<>(2131231221, 2131886621);
                    case 8:
                        return new Lc6<>(2131231219, 2131886619);
                    case 9:
                        return new Lc6<>(2131231215, 2131886615);
                    case 10:
                        return new Lc6<>(2131231214, 2131886613);
                    case 11:
                        return new Lc6<>(2131231213, 2131886621);
                }
            }
            throw new Kc6();
        }

        @DexIgnore
        public final Gi5 b(Oi5 oi5) {
            Wg6.c(oi5, "workoutWrapperType");
            switch (Ni5.d[oi5.ordinal()]) {
                case 1:
                    return Gi5.INDOOR;
                case 2:
                    return Gi5.INDOOR;
                case 3:
                    return Gi5.OUTDOOR;
                case 4:
                    return Gi5.INDOOR;
                case 5:
                    return Gi5.INDOOR;
                case 6:
                    return Gi5.INDOOR;
                case 7:
                    return Gi5.INDOOR;
                case 8:
                    return Gi5.INDOOR;
                default:
                    return null;
            }
        }

        @DexIgnore
        public final Mi5 c(Oi5 oi5) {
            Wg6.c(oi5, "workoutWrapperType");
            switch (Ni5.c[oi5.ordinal()]) {
                case 1:
                    return Mi5.UNKNOWN;
                case 2:
                    return Mi5.RUNNING;
                case 3:
                    return Mi5.SPINNING;
                case 4:
                    return Mi5.CYCLING;
                case 5:
                    return Mi5.TREADMILL;
                case 6:
                    return Mi5.ELLIPTICAL;
                case 7:
                    return Mi5.WEIGHTS;
                case 8:
                    return Mi5.WORKOUT;
                case 9:
                    return Mi5.WALKING;
                case 10:
                    return Mi5.ROWING;
                case 11:
                    return Mi5.HIKING;
                default:
                    throw new Kc6();
            }
        }

        @DexIgnore
        public final Oi5 d(Mi5 mi5, Gi5 gi5) {
            if (mi5 != null) {
                switch (Ni5.b[mi5.ordinal()]) {
                    case 1:
                        return Oi5.UNKNOWN;
                    case 2:
                        return Oi5.RUNNING;
                    case 3:
                        if (gi5 != null) {
                            int i = Ni5.a[gi5.ordinal()];
                            if (i == 1) {
                                return Oi5.SPINNING;
                            }
                            if (i == 2) {
                                return Oi5.OUTDOOR_CYCLING;
                            }
                        }
                        return Oi5.OUTDOOR_CYCLING;
                    case 4:
                        return Oi5.SPINNING;
                    case 5:
                        return Oi5.TREADMILL;
                    case 6:
                        return Oi5.ELLIPTICAL;
                    case 7:
                        return Oi5.WEIGHTS;
                    case 8:
                        return Oi5.WORKOUT;
                    case 9:
                        return Oi5.UNKNOWN;
                    case 10:
                        return Oi5.WALK;
                    case 11:
                        return Oi5.ROW_MACHINE;
                    case 12:
                        return Oi5.UNKNOWN;
                    case 13:
                        return Oi5.UNKNOWN;
                    case 14:
                        return Oi5.HIKING;
                }
            }
            return Oi5.UNKNOWN;
        }
    }

    @DexIgnore
    public Oi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
