package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sz5 implements Factory<Y66> {
    @DexIgnore
    public static Y66 a(Oz5 oz5) {
        Y66 d = oz5.d();
        Lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
