package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L13 extends IOException {
    @DexIgnore
    public M23 zza; // = null;

    @DexIgnore
    public L13(String str) {
        super(str);
    }

    @DexIgnore
    public static L13 zza() {
        return new L13("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    @DexIgnore
    public static L13 zzb() {
        return new L13("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static L13 zzc() {
        return new L13("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static L13 zzd() {
        return new L13("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static L13 zze() {
        return new L13("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static O13 zzf() {
        return new O13("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static L13 zzg() {
        return new L13("Failed to parse the message.");
    }

    @DexIgnore
    public static L13 zzh() {
        return new L13("Protocol message had invalid UTF-8.");
    }
}
