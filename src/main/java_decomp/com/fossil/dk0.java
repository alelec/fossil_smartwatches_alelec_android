package com.fossil;

import com.fossil.Tj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dk0 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public ArrayList<Ai> e; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Tj0 a;
        @DexIgnore
        public Tj0 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public Tj0.Ci d;
        @DexIgnore
        public int e;

        @DexIgnore
        public Ai(Tj0 tj0) {
            this.a = tj0;
            this.b = tj0.i();
            this.c = tj0.d();
            this.d = tj0.h();
            this.e = tj0.c();
        }

        @DexIgnore
        public void a(Uj0 uj0) {
            uj0.h(this.a.j()).b(this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        public void b(Uj0 uj0) {
            Tj0 h = uj0.h(this.a.j());
            this.a = h;
            if (h != null) {
                this.b = h.i();
                this.c = this.a.d();
                this.d = this.a.h();
                this.e = this.a.c();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = Tj0.Ci.STRONG;
            this.e = 0;
        }
    }

    @DexIgnore
    public Dk0(Uj0 uj0) {
        this.a = uj0.G();
        this.b = uj0.H();
        this.c = uj0.D();
        this.d = uj0.r();
        ArrayList<Tj0> i = uj0.i();
        int size = i.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.e.add(new Ai(i.get(i2)));
        }
    }

    @DexIgnore
    public void a(Uj0 uj0) {
        uj0.C0(this.a);
        uj0.D0(this.b);
        uj0.y0(this.c);
        uj0.b0(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(uj0);
        }
    }

    @DexIgnore
    public void b(Uj0 uj0) {
        this.a = uj0.G();
        this.b = uj0.H();
        this.c = uj0.D();
        this.d = uj0.r();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(uj0);
        }
    }
}
