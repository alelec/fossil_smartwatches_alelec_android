package com.fossil;

import com.mapped.Cd6;
import com.mapped.Rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pu7 extends Zw7<Rm6> {
    @DexIgnore
    public /* final */ Lu7<?> f;

    @DexIgnore
    public Pu7(Rm6 rm6, Lu7<?> lu7) {
        super(rm6);
        this.f = lu7;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        w(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.Lz7
    public String toString() {
        return "ChildContinuation[" + this.f + ']';
    }

    @DexIgnore
    @Override // com.fossil.Zu7
    public void w(Throwable th) {
        Lu7<?> lu7 = this.f;
        lu7.A(lu7.r(this.e));
    }
}
