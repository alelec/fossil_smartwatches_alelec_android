package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Me2 extends Rl2 implements Le2 {
    @DexIgnore
    public Me2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    @Override // com.fossil.Le2
    public final Rg2 zzb() throws RemoteException {
        Parcel e = e(1, d());
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Le2
    public final int zzc() throws RemoteException {
        Parcel e = e(2, d());
        int readInt = e.readInt();
        e.recycle();
        return readInt;
    }
}
