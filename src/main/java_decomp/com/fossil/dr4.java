package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.MetaData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public List<p77> f823a; // = new ArrayList();
    @DexIgnore
    public a b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(p77 p77);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ rf5 f824a;
        @DexIgnore
        public /* final */ /* synthetic */ dr4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    a aVar = this.b.b.b;
                    if (aVar != null) {
                        aVar.a((p77) this.b.b.f823a.get(this.b.getAdapterPosition()));
                    }
                    b bVar = this.b;
                    bVar.b.d = bVar.getAdapterPosition();
                    this.b.b.notifyDataSetChanged();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dr4 dr4, rf5 rf5) {
            super(rf5.b());
            pq7.c(rf5, "view");
            this.b = dr4;
            this.f824a = rf5;
            rf5.b.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(p77 p77, boolean z) {
            pq7.c(p77, "ring");
            tj5.b(this.f824a.b).t(p77.c().a()).F0(this.f824a.b);
            FlexibleTextView flexibleTextView = this.f824a.c;
            pq7.b(flexibleTextView, "view.tvName");
            PortfolioApp c = PortfolioApp.h0.c();
            MetaData e = p77.e();
            flexibleTextView.setText(um5.d(c, e != null ? e.getName() : null, p77.f()));
            if (z) {
                View view = this.f824a.d;
                pq7.b(view, "view.vBackgroundSelected");
                view.setVisibility(0);
            } else {
                View view2 = this.f824a.d;
                pq7.b(view2, "view.vBackgroundSelected");
                view2.setVisibility(4);
            }
            if (this.b.e) {
                View view3 = this.f824a.e;
                pq7.b(view3, "view.vDisable");
                view3.setVisibility(8);
                rf5 rf5 = this.f824a;
                FlexibleTextView flexibleTextView2 = rf5.c;
                ConstraintLayout b2 = rf5.b();
                pq7.b(b2, "view.root");
                flexibleTextView2.setTextColor(gl0.d(b2.getContext(), 2131099968));
                ImageView imageView = this.f824a.b;
                pq7.b(imageView, "view.ivBackgroundPreview");
                imageView.setClickable(true);
                return;
            }
            rf5 rf52 = this.f824a;
            FlexibleTextView flexibleTextView3 = rf52.c;
            ConstraintLayout b3 = rf52.b();
            pq7.b(b3, "view.root");
            flexibleTextView3.setTextColor(gl0.d(b3.getContext(), 2131099938));
            View view4 = this.f824a.e;
            pq7.b(view4, "view.vDisable");
            view4.setVisibility(0);
            ImageView imageView2 = this.f824a.b;
            pq7.b(imageView2, "view.ivBackgroundPreview");
            imageView2.setClickable(false);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.f823a.size();
    }

    @DexIgnore
    public final int k(String str) {
        T t;
        T t2;
        T t3;
        pq7.c(str, "ringId");
        if (vt7.l(str)) {
            Iterator<T> it = this.f823a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                T next = it.next();
                if (vt7.j(next.f(), "empty", true)) {
                    t3 = next;
                    break;
                }
            }
            t2 = t3;
        } else {
            Iterator<T> it2 = this.f823a.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                T next2 = it2.next();
                if (pq7.a(next2.d(), str)) {
                    t = next2;
                    break;
                }
            }
            t2 = t;
        }
        if (t2 != null) {
            return this.f823a.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: l */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        int i2 = this.d;
        bVar.a(this.f823a.get(i), i2 != -1 && i2 == i && this.e);
    }

    @DexIgnore
    /* renamed from: m */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        rf5 c2 = rf5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        pq7.b(c2, "ItemRingBinding.inflate(\u2026.context), parent, false)");
        return new b(this, c2);
    }

    @DexIgnore
    public final void n(List<p77> list) {
        pq7.c(list, "rings");
        this.f823a.clear();
        this.f823a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("RingAdapter", "setListEnable, value = " + z);
        this.e = z;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void p(a aVar) {
        pq7.c(aVar, "listener");
        this.b = aVar;
    }

    @DexIgnore
    public final void q(String str) {
        int i;
        int i2 = 0;
        FLogger.INSTANCE.getLocal().d("RingAdapter", "selectedRing = " + str);
        this.c = str;
        if (str == null) {
            i = -1;
        } else if (str != null) {
            if (!vt7.l(str)) {
                Iterator<p77> it = this.f823a.iterator();
                while (true) {
                    i = i2;
                    if (it.hasNext()) {
                        if (pq7.a(it.next().d(), str)) {
                            break;
                        }
                        i2 = i + 1;
                    } else {
                        break;
                    }
                }
            } else {
                Iterator<p77> it2 = this.f823a.iterator();
                while (true) {
                    i = i2;
                    if (it2.hasNext()) {
                        if (vt7.j(it2.next().f(), "empty", true)) {
                            break;
                        }
                        i2 = i + 1;
                    } else {
                        break;
                    }
                }
            }
            i = -1;
        } else {
            pq7.i();
            throw null;
        }
        this.d = i;
        notifyDataSetChanged();
    }
}
