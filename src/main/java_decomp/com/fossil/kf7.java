package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kf7 extends Df7 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public Kf7(Bundle bundle) {
        b(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public void b(Bundle bundle) {
        super.b(bundle);
        this.c = bundle.getString("_wxapi_getmessage_req_lang");
        this.d = bundle.getString("_wxapi_getmessage_req_country");
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public int c() {
        return 3;
    }

    @DexIgnore
    @Override // com.fossil.Df7
    public void d(Bundle bundle) {
        super.d(bundle);
        bundle.putString("_wxapi_getmessage_req_lang", this.c);
        bundle.putString("_wxapi_getmessage_req_country", this.d);
    }
}
