package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ju1 {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Ju1 a(String str) {
            Ju1[] values = Ju1.values();
            for (Ju1 ju1 : values) {
                String a2 = Ey1.a(ju1);
                String lowerCase = str.toLowerCase(Hd0.y.e());
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (Wg6.a(a2, lowerCase)) {
                    return ju1;
                }
            }
            return null;
        }
    }
}
