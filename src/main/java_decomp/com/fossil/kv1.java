package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kv1 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Kv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kv1 createFromParcel(Parcel parcel) {
            return new Kv1(parcel.readFloat(), parcel.readFloat());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Kv1[] newArray(int i) {
            return new Kv1[i];
        }
    }

    @DexIgnore
    public Kv1(float f, float f2) {
        this.b = f;
        this.c = f2;
    }

    @DexIgnore
    public final void a(float f) {
        this.c = f;
    }

    @DexIgnore
    public final void b(float f) {
        this.b = f;
    }

    @DexIgnore
    @Override // java.lang.Object
    public Kv1 clone() {
        return new Kv1(this.b, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Kv1) {
                Kv1 kv1 = (Kv1) obj;
                if (!(Float.compare(this.b, kv1.b) == 0 && Float.compare(this.c, kv1.c) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getScaledHeight() {
        return this.c;
    }

    @DexIgnore
    public final float getScaledWidth() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (Float.floatToIntBits(this.b) * 31) + Float.floatToIntBits(this.c);
    }

    @DexIgnore
    public final Lv1 toActualSize(int i, int i2) {
        return new Lv1(Lr7.b(this.b * ((float) i)), Lr7.b(this.c * ((float) i2)));
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return Gy1.d(Gy1.d(new JSONObject(), Jd0.V5, Float.valueOf(this.b)), Jd0.W5, Float.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e = E.e("ScaledSize(scaledWidth=");
        e.append(this.b);
        e.append(", scaledHeight=");
        e.append(this.c);
        e.append(")");
        return e.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
    }
}
