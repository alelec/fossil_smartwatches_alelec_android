package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Qt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qt1 createFromParcel(Parcel parcel) {
            return new Qt1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qt1[] newArray(int i) {
            return new Qt1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Qt1(Parcel parcel, Qg6 qg6) {
        super((X90) parcel.readParcelable(X90.class.getClassLoader()), (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()));
        Parcelable readParcelable = parcel.readParcelable(Zp1.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (Zp1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Qt1(Zp1 zp1, Nt1 nt1) {
        super(null, nt1);
        this.d = zp1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        Integer valueOf = Integer.valueOf(this.d.b());
        JSONObject jSONObject = new JSONObject();
        try {
            Nt1 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.toJSONObject());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    D90.i.i(e);
                }
                String jSONObject4 = jSONObject2.toString();
                Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = Hd0.y.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            Wg6.i();
            throw null;
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Tc0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        try {
            G80.k(jSONObject, Jd0.o, this.d.toJSONObject());
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
