package com.fossil;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import com.fossil.Zk0;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.W6;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.concurrent.atomic.AtomicInteger;
import net.sqlcipher.database.SQLiteDatabase;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mh4 {
    @DexIgnore
    public static /* final */ AtomicInteger a; // = new AtomicInteger((int) SystemClock.elapsedRealtime());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ Zk0.Ei a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Ai(Zk0.Ei ei, String str, int i) {
            this.a = ei;
            this.b = str;
            this.c = i;
        }
    }

    @DexIgnore
    public static PendingIntent a(Context context, Bi4 bi4, String str, PackageManager packageManager) {
        Intent f = f(str, bi4, packageManager);
        if (f == null) {
            return null;
        }
        f.addFlags(67108864);
        f.putExtras(bi4.y());
        PendingIntent activity = PendingIntent.getActivity(context, g(), f, 1073741824);
        return p(bi4) ? q(context, bi4, activity) : activity;
    }

    @DexIgnore
    public static PendingIntent b(Context context, Bi4 bi4) {
        if (!p(bi4)) {
            return null;
        }
        return c(context, new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(bi4.x()));
    }

    @DexIgnore
    public static PendingIntent c(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, g(), new Intent("com.google.firebase.MESSAGING_EVENT").setComponent(new ComponentName(context, "com.google.firebase.iid.FirebaseInstanceIdReceiver")).putExtra("wrapped_intent", intent), 1073741824);
    }

    @DexIgnore
    public static Ai d(Context context, Bi4 bi4) {
        Bundle j = j(context.getPackageManager(), context.getPackageName());
        return e(context, context.getPackageName(), bi4, k(context, bi4.k(), j), context.getResources(), context.getPackageManager(), j);
    }

    @DexIgnore
    public static Ai e(Context context, String str, Bi4 bi4, String str2, Resources resources, PackageManager packageManager, Bundle bundle) {
        Zk0.Ei ei = new Zk0.Ei(context, str2);
        String n = bi4.n(resources, str, "gcm.n.title");
        if (!TextUtils.isEmpty(n)) {
            ei.n(n);
        }
        String n2 = bi4.n(resources, str, "gcm.n.body");
        if (!TextUtils.isEmpty(n2)) {
            ei.m(n2);
            Zk0.Ci ci = new Zk0.Ci();
            ci.g(n2);
            ei.A(ci);
        }
        ei.y(l(packageManager, resources, str, bi4.p("gcm.n.icon"), bundle));
        Uri m = m(str, bi4, resources);
        if (m != null) {
            ei.z(m);
        }
        ei.l(a(context, bi4, str, packageManager));
        PendingIntent b = b(context, bi4);
        if (b != null) {
            ei.p(b);
        }
        Integer h = h(context, bi4.p("gcm.n.color"), bundle);
        if (h != null) {
            ei.k(h.intValue());
        }
        ei.g(!bi4.a("gcm.n.sticky"));
        ei.t(bi4.a("gcm.n.local_only"));
        String p = bi4.p("gcm.n.ticker");
        if (p != null) {
            ei.B(p);
        }
        Integer m2 = bi4.m();
        if (m2 != null) {
            ei.w(m2.intValue());
        }
        Integer r = bi4.r();
        if (r != null) {
            ei.D(r.intValue());
        }
        Integer l = bi4.l();
        if (l != null) {
            ei.u(l.intValue());
        }
        Long j = bi4.j("gcm.n.event_time");
        if (j != null) {
            ei.x(true);
            ei.E(j.longValue());
        }
        long[] q = bi4.q();
        if (q != null) {
            ei.C(q);
        }
        int[] e = bi4.e();
        if (e != null) {
            ei.s(e[0], e[1], e[2]);
        }
        ei.o(i(bi4));
        return new Ai(ei, n(bi4), 0);
    }

    @DexIgnore
    public static Intent f(String str, Bi4 bi4, PackageManager packageManager) {
        String p = bi4.p("gcm.n.click_action");
        if (!TextUtils.isEmpty(p)) {
            Intent intent = new Intent(p);
            intent.setPackage(str);
            intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
            return intent;
        }
        Uri f = bi4.f();
        if (f != null) {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setPackage(str);
            intent2.setData(f);
            return intent2;
        }
        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
        if (launchIntentForPackage != null) {
            return launchIntentForPackage;
        }
        Log.w("FirebaseMessaging", "No activity found to launch app");
        return launchIntentForPackage;
    }

    @DexIgnore
    public static int g() {
        return a.incrementAndGet();
    }

    @DexIgnore
    public static Integer h(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException e) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 56);
                sb.append("Color is invalid: ");
                sb.append(str);
                sb.append(". Notification will use default color.");
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i == 0) {
            return null;
        }
        try {
            return Integer.valueOf(W6.d(context, i));
        } catch (Resources.NotFoundException e2) {
            Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            return null;
        }
    }

    @DexIgnore
    public static int i(Bi4 bi4) {
        int i = bi4.a("gcm.n.default_sound") ? 1 : 0;
        if (bi4.a("gcm.n.default_vibrate_timings")) {
            i |= 2;
        }
        return bi4.a("gcm.n.default_light_settings") ? i | 4 : i;
    }

    @DexIgnore
    public static Bundle j(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                return applicationInfo.metaData;
            }
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 35);
            sb.append("Couldn't get own application info: ");
            sb.append(valueOf);
            Log.w("FirebaseMessaging", sb.toString());
        }
        return Bundle.EMPTY;
    }

    @DexIgnore
    @TargetApi(26)
    public static String k(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 26) {
            return null;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion < 26) {
                return null;
            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NotificationManager.class);
            if (!TextUtils.isEmpty(str)) {
                if (notificationManager.getNotificationChannel(str) != null) {
                    return str;
                }
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 122);
                sb.append("Notification Channel requested (");
                sb.append(str);
                sb.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                Log.w("FirebaseMessaging", sb.toString());
            }
            String string = bundle.getString("com.google.firebase.messaging.default_notification_channel_id");
            if (TextUtils.isEmpty(string)) {
                Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
            } else if (notificationManager.getNotificationChannel(string) != null) {
                return string;
            } else {
                Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
            }
            if (notificationManager.getNotificationChannel("fcm_fallback_notification_channel") == null) {
                notificationManager.createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", context.getString(context.getResources().getIdentifier("fcm_fallback_notification_channel_label", LegacyTokenHelper.TYPE_STRING, context.getPackageName())), 3));
            }
            return "fcm_fallback_notification_channel";
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    @DexIgnore
    public static int l(PackageManager packageManager, Resources resources, String str, String str2, Bundle bundle) {
        int i;
        if (!TextUtils.isEmpty(str2)) {
            int identifier = resources.getIdentifier(str2, ResourceManager.DRAWABLE, str);
            if (identifier != 0 && o(resources, identifier)) {
                return identifier;
            }
            int identifier2 = resources.getIdentifier(str2, "mipmap", str);
            if (identifier2 != 0 && o(resources, identifier2)) {
                return identifier2;
            }
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 61);
            sb.append("Icon resource ");
            sb.append(str2);
            sb.append(" not found. Notification will use default icon.");
            Log.w("FirebaseMessaging", sb.toString());
        }
        int i2 = bundle.getInt("com.google.firebase.messaging.default_notification_icon", 0);
        if (i2 == 0 || !o(resources, i2)) {
            try {
                i = packageManager.getApplicationInfo(str, 0).icon;
            } catch (PackageManager.NameNotFoundException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 35);
                sb2.append("Couldn't get own application info: ");
                sb2.append(valueOf);
                Log.w("FirebaseMessaging", sb2.toString());
                i = i2;
            }
        } else {
            i = i2;
        }
        if (i == 0 || !o(resources, i)) {
            return 17301651;
        }
        return i;
    }

    @DexIgnore
    public static Uri m(String str, Bi4 bi4, Resources resources) {
        String o = bi4.o();
        if (TextUtils.isEmpty(o)) {
            return null;
        }
        if (Endpoints.DEFAULT_NAME.equals(o) || resources.getIdentifier(o, OrmLiteConfigUtil.RAW_DIR_NAME, str) == 0) {
            return RingtoneManager.getDefaultUri(2);
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 24 + String.valueOf(o).length());
        sb.append("android.resource://");
        sb.append(str);
        sb.append("/raw/");
        sb.append(o);
        return Uri.parse(sb.toString());
    }

    @DexIgnore
    public static String n(Bi4 bi4) {
        String p = bi4.p("gcm.n.tag");
        if (!TextUtils.isEmpty(p)) {
            return p;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        StringBuilder sb = new StringBuilder(37);
        sb.append("FCM-Notification:");
        sb.append(uptimeMillis);
        return sb.toString();
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean o(Resources resources, int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (!(resources.getDrawable(i, null) instanceof AdaptiveIconDrawable)) {
                return true;
            }
            StringBuilder sb = new StringBuilder(77);
            sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
            sb.append(i);
            Log.e("FirebaseMessaging", sb.toString());
            return false;
        } catch (Resources.NotFoundException e) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Couldn't find resource ");
            sb2.append(i);
            sb2.append(", treating it as an invalid icon");
            Log.e("FirebaseMessaging", sb2.toString());
            return false;
        }
    }

    @DexIgnore
    public static boolean p(Bi4 bi4) {
        return bi4.a("google.c.a.e");
    }

    @DexIgnore
    public static PendingIntent q(Context context, Bi4 bi4, PendingIntent pendingIntent) {
        return c(context, new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN").putExtras(bi4.x()).putExtra("pending_intent", pendingIntent));
    }
}
