package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import com.fossil.M62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wc2<T extends IInterface> extends Ec2<T> {
    @DexIgnore
    public /* final */ M62.Hi<T> E;

    @DexIgnore
    @Override // com.fossil.Yb2
    public void P(int i, T t) {
        this.E.m(i, t);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String p() {
        return this.E.p();
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public T q(IBinder iBinder) {
        return this.E.q(iBinder);
    }

    @DexIgnore
    public M62.Hi<T> t0() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public String x() {
        return this.E.x();
    }
}
