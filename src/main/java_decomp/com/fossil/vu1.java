package com.fossil;

import android.os.Parcelable;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vu1 extends Ox1 implements Parcelable {
    @DexIgnore
    public O90 b;

    @DexIgnore
    public final void a(O90 o90) {
        this.b = o90;
    }

    @DexIgnore
    public final byte[] a() {
        O90 o90 = this.b;
        if (o90 != null) {
            int length = o90.a.length + 3 + c().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            Wg6.b(allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            O90 o902 = this.b;
            if (o902 != null) {
                allocate.put(o902.a);
                allocate.putShort((short) length);
                allocate.put((byte) b());
                allocate.put(c());
                byte[] array = allocate.array();
                Wg6.b(array, "byteBuffer.array()");
                return array;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract byte[] c();
}
