package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ok5 implements MembersInjector<DeviceHelper> {
    @DexIgnore
    public static void a(DeviceHelper deviceHelper, DeviceRepository deviceRepository) {
        deviceHelper.b = deviceRepository;
    }

    @DexIgnore
    public static void b(DeviceHelper deviceHelper, An4 an4) {
        deviceHelper.a = an4;
    }
}
