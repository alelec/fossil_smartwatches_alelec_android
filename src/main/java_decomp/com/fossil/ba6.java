package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ba6 implements Factory<WatchAppsPresenter> {
    @DexIgnore
    public static WatchAppsPresenter a(W96 w96, CategoryRepository categoryRepository, An4 an4) {
        return new WatchAppsPresenter(w96, categoryRepository, an4);
    }
}
