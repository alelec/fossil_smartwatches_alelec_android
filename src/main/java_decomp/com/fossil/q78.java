package com.fossil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q78 implements D78 {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ Map<String, P78> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<K78> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    @Override // com.fossil.D78
    public E78 a(String str) {
        P78 p78;
        synchronized (this) {
            p78 = this.b.get(str);
            if (p78 == null) {
                p78 = new P78(str, this.c, this.a);
                this.b.put(str, p78);
            }
        }
        return p78;
    }

    @DexIgnore
    public void b() {
        this.b.clear();
        this.c.clear();
    }

    @DexIgnore
    public LinkedBlockingQueue<K78> c() {
        return this.c;
    }

    @DexIgnore
    public List<P78> d() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void e() {
        this.a = true;
    }
}
