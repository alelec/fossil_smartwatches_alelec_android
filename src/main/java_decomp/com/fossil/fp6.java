package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Fp6 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[Rh5.values().length];
        a = iArr;
        iArr[Rh5.CALORIES.ordinal()] = 1;
        a[Rh5.ACTIVE_TIME.ordinal()] = 2;
        a[Rh5.TOTAL_STEPS.ordinal()] = 3;
        a[Rh5.TOTAL_SLEEP.ordinal()] = 4;
        a[Rh5.GOAL_TRACKING.ordinal()] = 5;
        int[] iArr2 = new int[Rh5.values().length];
        b = iArr2;
        iArr2[Rh5.TOTAL_STEPS.ordinal()] = 1;
        b[Rh5.CALORIES.ordinal()] = 2;
        b[Rh5.ACTIVE_TIME.ordinal()] = 3;
        b[Rh5.GOAL_TRACKING.ordinal()] = 4;
    }
    */
}
