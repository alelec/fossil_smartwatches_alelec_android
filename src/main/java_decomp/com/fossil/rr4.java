package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rr4 {
    @DexIgnore
    @Vu3("id")
    public String a;
    @DexIgnore
    @Vu3("variantKey")
    public String b;

    @DexIgnore
    public Rr4(String str, String str2) {
        Wg6.c(str, "id");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Rr4) {
                Rr4 rr4 = (Rr4) obj;
                if (!Wg6.a(this.a, rr4.a) || !Wg6.a(this.b, rr4.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Flag(id=" + this.a + ", variantKey=" + this.b + ")";
    }
}
