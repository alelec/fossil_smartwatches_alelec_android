package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Do1 {
    ACCEPT_PHONE_CALL((byte) 0),
    REJECT_PHONE_CALL((byte) 1),
    DISMISS_NOTIFICATION((byte) 2),
    REPLY_MESSAGE((byte) 3);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Do1 a(byte b) {
            Do1[] values = Do1.values();
            for (Do1 do1 : values) {
                if (do1.a() == b) {
                    return do1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Do1(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
