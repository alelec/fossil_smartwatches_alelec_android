package com.fossil;

import com.fossil.Um1;
import com.mapped.Hg6;
import com.mapped.Hi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Lb extends Nq7 implements Hg6<byte[], Um1> {
    @DexIgnore
    public Lb(Um1.Ai ai) {
        super(1, ai);
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final Hi6 getOwner() {
        return Er7.b(Um1.Ai.class);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailySleepConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Um1 invoke(byte[] bArr) {
        return ((Um1.Ai) this.receiver).a(bArr);
    }
}
