package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Zm1 {
    BIOMETRIC_PROFILE(1, "biometric_profile"),
    DAILY_STEP(2, "daily_step"),
    DAILY_STEP_GOAL(3, "daily_step_goal"),
    DAILY_CALORIE(4, "daily_calories"),
    DAILY_CALORIE_GOAL(5, "daily_calories_goal"),
    DAILY_TOTAL_ACTIVE_MINUTE(6, "daily_active_minutes"),
    DAILY_ACTIVE_MINUTE_GOAL(7, "daily_active_minute_goal"),
    DAILY_DISTANCE(8, "daily_distance"),
    INACTIVE_NUDGE(9, "inactive_nudge"),
    VIBE_STRENGTH(10, "vibe_strength_level"),
    DO_NOT_DISTURB_SCHEDULE(11, "dnd_schedule"),
    TIME(12, LogBuilder.KEY_TIME),
    BATTERY(13, Constants.BATTERY),
    HEART_RATE_MODE(14, "heart_rate_mode"),
    DAILY_SLEEP(15, "daily_sleep"),
    DISPLAY_UNIT(16, "display_unit"),
    SECOND_TIMEZONE_OFFSET(17, "second_timezone_offset"),
    CURRENT_HEART_RATE(18, "current_heart_rate"),
    HELLAS_BATTERY(19, "hellas_battery"),
    AUTO_WORKOUT_DETECTION(20, "auto_workout_detection"),
    CYCLING_CADENCE(21, "cycling_cadence");
    
    @DexIgnore
    public static /* final */ Ai e; // = new Ai(null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Zm1 a(String str) {
            Zm1[] values = Zm1.values();
            for (Zm1 zm1 : values) {
                if (Wg6.a(str, zm1.b())) {
                    return zm1;
                }
            }
            return null;
        }

        @DexIgnore
        public final Zm1 b(short s) {
            Zm1[] values = Zm1.values();
            for (Zm1 zm1 : values) {
                if (zm1.a() == s) {
                    return zm1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Zm1(short s, String str) {
        this.b = (short) s;
        this.c = str;
    }

    @DexIgnore
    public final short a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }
}
