package com.fossil;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bp4 implements Factory<PortfolioApp> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Bp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Bp4 a(Uo4 uo4) {
        return new Bp4(uo4);
    }

    @DexIgnore
    public static PortfolioApp c(Uo4 uo4) {
        PortfolioApp g = uo4.g();
        Lk7.c(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    public PortfolioApp b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
