package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface I28 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    void b(F28 f28);

    @DexIgnore
    void c(V18 v18) throws IOException;

    @DexIgnore
    E28 d(Response response) throws IOException;

    @DexIgnore
    Response e(V18 v18) throws IOException;

    @DexIgnore
    void f(Response response, Response response2);
}
