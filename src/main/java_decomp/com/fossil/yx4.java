package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yx4 {
    @DexIgnore
    public TimerViewObserver a;
    @DexIgnore
    public /* final */ F57.Bi b;
    @DexIgnore
    public /* final */ Uy4 c; // = Uy4.d.b();
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Lf5 a;
        @DexIgnore
        public /* final */ View b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Lf5 lf5, View view) {
            super(view);
            Wg6.c(lf5, "binding");
            Wg6.c(view, "root");
            this.a = lf5;
            this.b = view;
        }

        @DexIgnore
        public final void a(Mt4 mt4, TimerViewObserver timerViewObserver, F57.Bi bi, Uy4 uy4) {
            String str;
            String str2 = null;
            Wg6.c(mt4, "recommended");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Lf5 lf5 = this.a;
            Ps4 b2 = mt4.b();
            lf5.w.setEndingText(Um5.c(PortfolioApp.get.instance(), 2131886306));
            TimerTextView timerTextView = lf5.w;
            Date m = b2.m();
            timerTextView.setTime(m != null ? m.getTime() : 0);
            lf5.w.setObserver(timerViewObserver);
            FlexibleTextView flexibleTextView = lf5.r;
            Wg6.b(flexibleTextView, "ftvChallengeName");
            flexibleTextView.setText(b2.g());
            Ht4 i = b2.i();
            if (i == null || (str = i.c()) == null) {
                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            }
            ImageView imageView = lf5.x;
            Wg6.b(imageView, "imgAvatar");
            Ht4 i2 = b2.i();
            Ty4.b(imageView, i2 != null ? i2.e() : null, str, bi, uy4);
            String c = b2.c();
            if (c != null) {
                FlexibleTextView flexibleTextView2 = lf5.q;
                Wg6.b(flexibleTextView2, "ftvChallengeDes");
                flexibleTextView2.setText(c);
            } else {
                FlexibleTextView flexibleTextView3 = lf5.q;
                Wg6.b(flexibleTextView3, "ftvChallengeDes");
                flexibleTextView3.setText("");
            }
            FlexibleTextView flexibleTextView4 = lf5.t;
            Wg6.b(flexibleTextView4, "ftvDuration");
            Integer d = b2.d();
            flexibleTextView4.setText(d != null ? Kl5.a(d.intValue()) : null);
            Integer h = b2.h();
            int intValue = h != null ? h.intValue() : 0;
            FlexibleTextView flexibleTextView5 = lf5.s;
            Wg6.b(flexibleTextView5, "ftvCountPlayer");
            flexibleTextView5.setText(String.valueOf(intValue));
            int d2 = W6.d(PortfolioApp.get.instance(), 2131099689);
            int d3 = W6.d(PortfolioApp.get.instance(), 2131099677);
            if (Wg6.a(b2.r(), "activity_best_result")) {
                lf5.r.setTextColor(d3);
                FlexibleTextView flexibleTextView6 = lf5.v;
                Wg6.b(flexibleTextView6, "ftvSteps");
                flexibleTextView6.setVisibility(8);
            } else {
                lf5.r.setTextColor(d2);
                FlexibleTextView flexibleTextView7 = lf5.v;
                Wg6.b(flexibleTextView7, "ftvSteps");
                flexibleTextView7.setVisibility(0);
                FlexibleTextView flexibleTextView8 = lf5.v;
                Wg6.b(flexibleTextView8, "ftvSteps");
                Integer q = b2.q();
                if (q != null) {
                    str2 = String.valueOf(q.intValue());
                }
                flexibleTextView8.setText(str2);
            }
            FlexibleTextView flexibleTextView9 = lf5.u;
            Wg6.b(flexibleTextView9, "ftvNew");
            flexibleTextView9.setVisibility(mt4.c() ? 0 : 8);
            String k = b2.k();
            if (k != null && k.hashCode() == -314497661 && k.equals("private")) {
                lf5.s.setCompoundDrawablesWithIntrinsicBounds(2131231028, 0, 0, 0);
            } else {
                lf5.s.setCompoundDrawablesWithIntrinsicBounds(2131231029, 0, 0, 0);
            }
        }
    }

    @DexIgnore
    public Yx4(int i) {
        this.d = i;
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.b = f;
    }

    @DexIgnore
    public final int a() {
        return this.d;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof Mt4) && Wg6.a(((Mt4) obj).a(), "pending-invitation");
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Mt4)) {
            obj = null;
        }
        Mt4 mt4 = (Mt4) obj;
        if (viewHolder instanceof Ai) {
            ai = viewHolder;
        }
        Ai ai2 = ai;
        if (mt4 != null && ai2 != null) {
            ai2.a(mt4, this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public Ai d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Lf5 z = Lf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemRecommendationChalle\u2026(inflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "binding.root");
        return new Ai(z, n);
    }

    @DexIgnore
    public final void e(TimerViewObserver timerViewObserver) {
        this.a = timerViewObserver;
    }
}
