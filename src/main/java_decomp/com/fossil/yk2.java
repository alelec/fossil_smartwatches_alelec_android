package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.D42;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yk2 extends Ec2<Zk2> {
    @DexIgnore
    public /* final */ D42.Ai E;

    @DexIgnore
    public Yk2(Context context, Looper looper, Ac2 ac2, D42.Ai ai, R62.Bi bi, R62.Ci ci) {
        super(context, looper, 68, ac2, bi, ci);
        this.E = ai;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final Bundle F() {
        D42.Ai ai = this.E;
        return ai == null ? new Bundle() : ai.a();
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String p() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        return queryLocalInterface instanceof Zk2 ? (Zk2) queryLocalInterface : new Al2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public final int s() {
        return 12800000;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String x() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
