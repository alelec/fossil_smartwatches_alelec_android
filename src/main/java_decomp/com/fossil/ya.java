package com.fossil;

import com.mapped.A70;
import com.mapped.H60;
import com.mapped.R60;
import com.mapped.V60;
import com.mapped.Wg6;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya extends Vx1<R60[], HashMap<Zm1, R60>> {
    @DexIgnore
    public static /* final */ Za[] b;
    @DexIgnore
    public static /* final */ Za[] c;
    @DexIgnore
    public static /* final */ Qx1<R60[]>[] d; // = {new Oa(), new Ra()};
    @DexIgnore
    public static /* final */ Rx1<HashMap<Zm1, R60>>[] e; // = {new Ua(Ob.l.c), new Wa(Ob.l.c)};
    @DexIgnore
    public static /* final */ Ya f; // = new Ya();

    /*
    static {
        Za[] zaArr = {new Za(Zm1.BIOMETRIC_PROFILE, new B9(H60.CREATOR)), new Za(Zm1.DAILY_STEP, new E9(Vm1.CREATOR)), new Za(Zm1.DAILY_STEP_GOAL, new H9(Wm1.CREATOR)), new Za(Zm1.DAILY_CALORIE, new K9(Rm1.CREATOR)), new Za(Zm1.DAILY_CALORIE_GOAL, new N9(Sm1.CREATOR)), new Za(Zm1.DAILY_TOTAL_ACTIVE_MINUTE, new Q9(Xm1.CREATOR)), new Za(Zm1.DAILY_ACTIVE_MINUTE_GOAL, new T9(Qm1.CREATOR)), new Za(Zm1.DAILY_DISTANCE, new W9(Tm1.CREATOR)), new Za(Zm1.INACTIVE_NUDGE, new Z9(En1.CREATOR)), new Za(Zm1.VIBE_STRENGTH, new Bb(A70.CREATOR)), new Za(Zm1.DO_NOT_DISTURB_SCHEDULE, new Db(Bn1.CREATOR)), new Za(Zm1.TIME, new Fb(Gn1.CREATOR)), new Za(Zm1.BATTERY, new Hb(Mm1.CREATOR)), new Za(Zm1.HEART_RATE_MODE, new Jb(V60.CREATOR)), new Za(Zm1.DAILY_SLEEP, new Lb(Um1.CREATOR)), new Za(Zm1.DISPLAY_UNIT, new Nb(An1.CREATOR)), new Za(Zm1.SECOND_TIMEZONE_OFFSET, new Pb(Fn1.CREATOR))};
        b = zaArr;
        c = (Za[]) Dm7.s(zaArr, new Za[]{new Za(Zm1.CURRENT_HEART_RATE, new Ca(Om1.CREATOR)), new Za(Zm1.HELLAS_BATTERY, new Fa(Dn1.CREATOR)), new Za(Zm1.AUTO_WORKOUT_DETECTION, new Ia(Jn1.CREATOR)), new Za(Zm1.CYCLING_CADENCE, new La(Pm1.CREATOR))});
    }
    */

    @DexIgnore
    @Override // com.fossil.Vx1
    public Qx1<R60[]>[] b() {
        return d;
    }

    @DexIgnore
    @Override // com.fossil.Vx1
    public Rx1<HashMap<Zm1, R60>>[] c() {
        return e;
    }

    @DexIgnore
    public final HashMap<Zm1, R60> h(byte[] bArr, Za[] zaArr) {
        R60 r60;
        Za za;
        int length = (bArr.length - 12) - 4;
        byte[] k = Dm7.k(bArr, 12, length + 12);
        ByteBuffer order = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
        HashMap<Zm1, R60> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i + 3;
            if (i2 > length) {
                return hashMap;
            }
            short s = order.getShort(i);
            short p = Hy1.p(order.get(i + 2));
            Zm1 b2 = Zm1.e.b(s);
            try {
                byte[] k2 = Dm7.k(k, i2, i2 + p);
                int length2 = zaArr.length;
                int i3 = 0;
                while (true) {
                    r60 = null;
                    if (i3 >= length2) {
                        za = null;
                        break;
                    }
                    za = zaArr[i3];
                    if (za.a == b2) {
                        break;
                    }
                    i3++;
                }
                if (za != null) {
                    r60 = za.b.invoke(k2);
                }
                if (!(b2 == null || r60 == null)) {
                    hashMap.put(b2, r60);
                }
            } catch (Exception e2) {
                D90.i.i(e2);
            }
            i = p + 3 + i;
        }
    }

    @DexIgnore
    public final byte[] j(R60[] r60Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (R60 r60 : r60Arr) {
            byteArrayOutputStream.write(r60.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "entryData.toByteArray()");
        return byteArray;
    }
}
