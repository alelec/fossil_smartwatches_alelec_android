package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.N04;
import com.fossil.X37;
import com.google.android.material.tabs.TabLayout;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ox6 extends BaseFragment implements Qw6, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai v; // = new Ai(null);
    @DexIgnore
    public Pw6 g;
    @DexIgnore
    public G37<Ub5> h;
    @DexIgnore
    public Gr4 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public String l; // = "";
    @DexIgnore
    public /* final */ String m; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public /* final */ String t; // = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ox6 a(boolean z, String str) {
            Wg6.c(str, "serial");
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            bundle.putString("SERIAL", str);
            Ox6 ox6 = new Ox6();
            ox6.setArguments(bundle);
            return ox6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ox6 b;

        @DexIgnore
        public Bi(Ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ Ub5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ox6 b;

        @DexIgnore
        public Ci(Ub5 ub5, Ox6 ox6) {
            this.a = ub5;
            this.b = ox6;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.s)) {
                int parseColor = Color.parseColor(this.b.s);
                TabLayout.g v = this.a.A.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.m) && this.b.k != i) {
                int parseColor2 = Color.parseColor(this.b.m);
                TabLayout.g v2 = this.a.A.v(this.b.k);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.k = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ox6 b;

        @DexIgnore
        public Di(Ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6().s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ox6 b;

        @DexIgnore
        public Ei(Ox6 ox6) {
            this.b = ox6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                Wg6.b(activity, "it");
                TroubleshootingActivity.a.c(aVar, activity, this.b.l, false, false, 12, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ox6 a;

        @DexIgnore
        public Fi(Ox6 ox6) {
            this.a = ox6;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.m) && !TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.m);
                int parseColor2 = Color.parseColor(this.a.s);
                gVar.o(2131230966);
                if (i == this.a.k) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void A(boolean z) {
        if (isActive()) {
            G37<Ub5> g37 = this.h;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 != null) {
                    Pw6 pw6 = this.g;
                    if (pw6 == null) {
                        Wg6.n("mPresenter");
                        throw null;
                    } else if (pw6.q()) {
                        if (z) {
                            ConstraintLayout constraintLayout = a2.q;
                            Wg6.b(constraintLayout, "it.clUpdateFwFail");
                            constraintLayout.setVisibility(8);
                            ConstraintLayout constraintLayout2 = a2.r;
                            Wg6.b(constraintLayout2, "it.clUpdatingFw");
                            constraintLayout2.setVisibility(0);
                            FlexibleButton flexibleButton = a2.s;
                            Wg6.b(flexibleButton, "it.fbContinue");
                            flexibleButton.setVisibility(0);
                            FlexibleTextView flexibleTextView = a2.z;
                            Wg6.b(flexibleTextView, "it.ftvUpdateWarning");
                            flexibleTextView.setVisibility(4);
                            FlexibleProgressBar flexibleProgressBar = a2.D;
                            Wg6.b(flexibleProgressBar, "it.progressUpdate");
                            flexibleProgressBar.setProgress(1000);
                            FlexibleTextView flexibleTextView2 = a2.y;
                            Wg6.b(flexibleTextView2, "it.ftvUpdate");
                            flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887045));
                            return;
                        }
                        ConstraintLayout constraintLayout3 = a2.q;
                        Wg6.b(constraintLayout3, "it.clUpdateFwFail");
                        constraintLayout3.setVisibility(0);
                        ConstraintLayout constraintLayout4 = a2.r;
                        Wg6.b(constraintLayout4, "it.clUpdatingFw");
                        constraintLayout4.setVisibility(8);
                    } else if (getActivity() == null) {
                    } else {
                        if (z) {
                            Pw6 pw62 = this.g;
                            if (pw62 != null) {
                                pw62.n();
                                l();
                                return;
                            }
                            Wg6.n("mPresenter");
                            throw null;
                        }
                        TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                        Context requireContext = requireContext();
                        Wg6.b(requireContext, "requireContext()");
                        TroubleshootingActivity.a.c(aVar, requireContext, this.l, false, false, 12, null);
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void K0() {
        G37<Ub5> g37 = this.h;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            DashBar dashBar = a2 != null ? a2.C : null;
            if (dashBar != null) {
                Wg6.b(dashBar, "mBinding.get()?.progressBar!!");
                dashBar.setVisibility(8);
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Pw6 pw6) {
        Q6(pw6);
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void O1() {
        if (isAdded()) {
            AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558481);
            fi.e(2131363410, Um5.c(PortfolioApp.get.instance(), 2131886795));
            fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886794));
            fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886797));
            fi.b(2131363373);
            fi.h(false);
            fi.k(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public final Pw6 P6() {
        Pw6 pw6 = this.g;
        if (pw6 != null) {
            return pw6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q6(Pw6 pw6) {
        Wg6.c(pw6, "presenter");
        this.g = pw6;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.j);
                if (i2 == 2131362290) {
                    Pw6 pw6 = this.g;
                    if (pw6 != null) {
                        pw6.s();
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else if (i2 != 2131362385) {
                    if (i2 == 2131362686) {
                        Pw6 pw62 = this.g;
                        if (pw62 != null) {
                            pw62.n();
                            requireActivity().finish();
                            return;
                        }
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.B;
                    FragmentActivity requireActivity = requireActivity();
                    Wg6.b(requireActivity, "requireActivity()");
                    aVar.a(requireActivity);
                }
            }
        } else if (str.equals("NETWORK_ERROR") && i2 == 2131362279) {
            Pw6 pw63 = this.g;
            if (pw63 != null) {
                pw63.o();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6() {
        G37<Ub5> g37 = this.h;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                G37<Ub5> g372 = this.h;
                if (g372 != null) {
                    Ub5 a3 = g372.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.F : null;
                    if (viewPager2 != null) {
                        new N04(tabLayout, viewPager2, new Fi(this)).a();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mBinding");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void V2() {
        if (isActive()) {
            G37<Ub5> g37 = this.h;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    Wg6.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    Wg6.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void X(List<? extends Explore> list) {
        Wg6.c(list, "data");
        Gr4 gr4 = this.i;
        if (gr4 != null) {
            gr4.h(list);
        } else {
            Wg6.n("mAdapterUpdateFirmware");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void Z(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        G37<Ub5> g37 = this.h;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.D) != null) {
                flexibleProgressBar.setProgress(i2);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void d0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.B;
            Wg6.b(activity, "it");
            Pw6 pw6 = this.g;
            if (pw6 != null) {
                aVar.a(activity, pw6.q());
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void f() {
        DashBar dashBar;
        G37<Ub5> g37 = this.h;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null && (dashBar = a2.C) != null) {
                G37<Ub5> g372 = this.h;
                if (g372 != null) {
                    Ub5 a3 = g372.a();
                    DashBar dashBar2 = a3 != null ? a3.C : null;
                    if (dashBar2 != null) {
                        Wg6.b(dashBar2, "mBinding.get()?.progressBar!!");
                        dashBar2.setVisibility(0);
                        X37.Ai ai = X37.a;
                        Wg6.b(dashBar, "this");
                        ai.i(dashBar, this.j, 500);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            Wg6.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Ub5 ub5 = (Ub5) Aq0.f(layoutInflater, 2131558632, viewGroup, false, A6());
        this.h = new G37<>(this, ub5);
        Wg6.b(ub5, "binding");
        return ub5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Pw6 pw6 = this.g;
        if (pw6 != null) {
            pw6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Pw6 pw6 = this.g;
        if (pw6 != null) {
            pw6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.j = arguments.getBoolean("IS_ONBOARDING_FLOW");
            String string = arguments.getString("SERIAL");
            if (string == null) {
                string = "";
            }
            this.l = string;
            Pw6 pw6 = this.g;
            if (pw6 != null) {
                pw6.p(this.j, string);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        this.i = new Gr4(new ArrayList());
        G37<Ub5> g37 = this.h;
        if (g37 != null) {
            Ub5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                Wg6.b(constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                Wg6.b(constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                FlexibleProgressBar flexibleProgressBar = a2.D;
                Wg6.b(flexibleProgressBar, "binding.progressUpdate");
                flexibleProgressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                Wg6.b(flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.z;
                Wg6.b(flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new Bi(this));
                ViewPager2 viewPager2 = a2.F;
                Wg6.b(viewPager2, "binding.rvpTutorial");
                Gr4 gr4 = this.i;
                if (gr4 != null) {
                    viewPager2.setAdapter(gr4);
                    if (!TextUtils.isEmpty(this.t)) {
                        TabLayout tabLayout = a2.A;
                        Wg6.b(tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.t)));
                    }
                    R6();
                    a2.F.g(new Ci(a2, this));
                    a2.t.setOnClickListener(new Di(this));
                    a2.w.setOnClickListener(new Ei(this));
                    return;
                }
                Wg6.n("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qw6
    public void t3() {
        if (isActive()) {
            G37<Ub5> g37 = this.h;
            if (g37 != null) {
                Ub5 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    Wg6.b(constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    Wg6.b(constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    Wg6.b(flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.z;
                    Wg6.b(flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    FlexibleProgressBar flexibleProgressBar = a2.D;
                    Wg6.b(flexibleProgressBar, "it.progressUpdate");
                    flexibleProgressBar.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    Wg6.b(flexibleTextView2, "it.ftvCountdownTime");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.y;
                    Wg6.b(flexibleTextView3, "it.ftvUpdate");
                    flexibleTextView3.setText(Um5.c(PortfolioApp.get.instance(), 2131886783));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
