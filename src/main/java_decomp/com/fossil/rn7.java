package com.fossil;

import com.mapped.Af6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rn7 extends Af6.Bi {
    @DexIgnore
    public static final Bi p = Bi.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static <E extends Af6.Bi> E a(Rn7 rn7, Af6.Ci<E> ci) {
            Wg6.c(ci, "key");
            if (ci instanceof On7) {
                On7 on7 = (On7) ci;
                if (!on7.a(rn7.getKey())) {
                    return null;
                }
                E e = (E) on7.b(rn7);
                if (!(e instanceof Af6.Bi)) {
                    return null;
                }
                return e;
            }
            if (Rn7.p != ci) {
                rn7 = null;
            } else if (rn7 == null) {
                throw new Rc6("null cannot be cast to non-null type E");
            }
            return rn7;
        }

        @DexIgnore
        public static Af6 b(Rn7 rn7, Af6.Ci<?> ci) {
            Wg6.c(ci, "key");
            if (!(ci instanceof On7)) {
                return Rn7.p == ci ? Un7.INSTANCE : rn7;
            }
            On7 on7 = (On7) ci;
            return (!on7.a(rn7.getKey()) || on7.b(rn7) == null) ? rn7 : Un7.INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Af6.Ci<Rn7> {
        @DexIgnore
        public static /* final */ /* synthetic */ Bi a; // = new Bi();
    }

    @DexIgnore
    void a(Xe6<?> xe6);

    @DexIgnore
    <T> Xe6<T> c(Xe6<? super T> xe6);
}
