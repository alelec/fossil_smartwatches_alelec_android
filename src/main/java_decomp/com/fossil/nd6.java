package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nd6 implements Factory<HomeDashboardPresenter> {
    @DexIgnore
    public static HomeDashboardPresenter a(Ld6 ld6, PortfolioApp portfolioApp, DeviceRepository deviceRepository, Cj4 cj4, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, An4 an4, HeartRateSampleRepository heartRateSampleRepository, Tt4 tt4) {
        return new HomeDashboardPresenter(ld6, portfolioApp, deviceRepository, cj4, summariesRepository, goalTrackingRepository, sleepSummariesRepository, an4, heartRateSampleRepository, tt4);
    }
}
