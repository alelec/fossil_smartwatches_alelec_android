package com.fossil;

import com.fossil.Tz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rz1 implements D12 {
    @DexIgnore
    public /* final */ Tz1 a;

    @DexIgnore
    public Rz1(Tz1 tz1) {
        this.a = tz1;
    }

    @DexIgnore
    public static D12 a(Tz1 tz1) {
        return new Rz1(tz1);
    }

    @DexIgnore
    @Override // com.fossil.D12
    public Object apply(Object obj) {
        return this.a.d((Tz1.Ai) obj);
    }
}
