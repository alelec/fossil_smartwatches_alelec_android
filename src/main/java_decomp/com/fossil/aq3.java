package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import com.fossil.Yb2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aq3 implements ServiceConnection, Yb2.Ai, Yb2.Bi {
    @DexIgnore
    public volatile boolean a;
    @DexIgnore
    public volatile Hl3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 c;

    @DexIgnore
    public Aq3(Fp3 fp3) {
        this.c = fp3;
    }

    @DexIgnore
    public final void a() {
        if (this.b != null && (this.b.c() || this.b.j())) {
            this.b.a();
        }
        this.b = null;
    }

    @DexIgnore
    public final void b(Intent intent) {
        this.c.h();
        Context e = this.c.e();
        Ve2 b2 = Ve2.b();
        synchronized (this) {
            if (this.a) {
                this.c.d().N().a("Connection attempt already in progress");
                return;
            }
            this.c.d().N().a("Using local app measurement service");
            this.a = true;
            b2.a(e, intent, this.c.c, 129);
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Ai
    public final void d(int i) {
        Rc2.f("MeasurementServiceConnection.onConnectionSuspended");
        this.c.d().M().a("Service connection suspended");
        this.c.c().y(new Eq3(this));
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Ai
    public final void e(Bundle bundle) {
        Rc2.f("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.c().y(new Bq3(this, (Cl3) this.b.I()));
            } catch (DeadObjectException | IllegalStateException e) {
                this.b = null;
                this.a = false;
            }
        }
    }

    @DexIgnore
    public final void f() {
        this.c.h();
        Context e = this.c.e();
        synchronized (this) {
            if (this.a) {
                this.c.d().N().a("Connection attempt already in progress");
            } else if (this.b == null || (!this.b.j() && !this.b.c())) {
                this.b = new Hl3(e, Looper.getMainLooper(), this, this);
                this.c.d().N().a("Connecting to remote service");
                this.a = true;
                this.b.z();
            } else {
                this.c.d().N().a("Already awaiting connection attempt");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb2.Bi
    public final void n(Z52 z52) {
        Rc2.f("MeasurementServiceConnection.onConnectionFailed");
        Kl3 A = this.c.a.A();
        if (A != null) {
            A.I().b("Service connection failed", z52);
        }
        synchronized (this) {
            this.a = false;
            this.b = null;
        }
        this.c.c().y(new Dq3(this));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r5, android.os.IBinder r6) {
        /*
            r4 = this;
            r1 = 0
            java.lang.String r0 = "MeasurementServiceConnection.onServiceConnected"
            com.fossil.Rc2.f(r0)
            monitor-enter(r4)
            if (r6 != 0) goto L_0x001d
            r0 = 0
            r4.a = r0     // Catch:{ all -> 0x0056 }
            com.fossil.Fp3 r0 = r4.c     // Catch:{ all -> 0x0056 }
            com.fossil.Kl3 r0 = r0.d()     // Catch:{ all -> 0x0056 }
            com.fossil.Nl3 r0 = r0.F()     // Catch:{ all -> 0x0056 }
            java.lang.String r1 = "Service connected with null binder"
            r0.a(r1)     // Catch:{ all -> 0x0056 }
            monitor-exit(r4)     // Catch:{ all -> 0x0056 }
        L_0x001c:
            return
        L_0x001d:
            java.lang.String r0 = r6.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x006c }
            java.lang.String r2 = "com.google.android.gms.measurement.internal.IMeasurementService"
            boolean r2 = r2.equals(r0)     // Catch:{ RemoteException -> 0x006c }
            if (r2 == 0) goto L_0x007d
            if (r6 != 0) goto L_0x0059
            r0 = r1
        L_0x002c:
            com.fossil.Fp3 r1 = r4.c     // Catch:{ RemoteException -> 0x009e }
            com.fossil.Kl3 r1 = r1.d()     // Catch:{ RemoteException -> 0x009e }
            com.fossil.Nl3 r1 = r1.N()     // Catch:{ RemoteException -> 0x009e }
            java.lang.String r2 = "Bound to IMeasurementService interface"
            r1.a(r2)     // Catch:{ RemoteException -> 0x009e }
            r1 = r0
        L_0x003c:
            if (r1 != 0) goto L_0x008d
            r0 = 0
            r4.a = r0
            com.fossil.Ve2 r0 = com.fossil.Ve2.b()     // Catch:{ IllegalArgumentException -> 0x009c }
            com.fossil.Fp3 r1 = r4.c     // Catch:{ IllegalArgumentException -> 0x009c }
            android.content.Context r1 = r1.e()     // Catch:{ IllegalArgumentException -> 0x009c }
            com.fossil.Fp3 r2 = r4.c     // Catch:{ IllegalArgumentException -> 0x009c }
            com.fossil.Aq3 r2 = com.fossil.Fp3.C(r2)     // Catch:{ IllegalArgumentException -> 0x009c }
            r0.c(r1, r2)     // Catch:{ IllegalArgumentException -> 0x009c }
        L_0x0054:
            monitor-exit(r4)
            goto L_0x001c
        L_0x0056:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0059:
            java.lang.String r0 = "com.google.android.gms.measurement.internal.IMeasurementService"
            android.os.IInterface r0 = r6.queryLocalInterface(r0)
            boolean r2 = r0 instanceof com.fossil.Cl3
            if (r2 == 0) goto L_0x0066
            com.fossil.Cl3 r0 = (com.fossil.Cl3) r0
            goto L_0x002c
        L_0x0066:
            com.fossil.El3 r0 = new com.fossil.El3
            r0.<init>(r6)
            goto L_0x002c
        L_0x006c:
            r0 = move-exception
        L_0x006d:
            com.fossil.Fp3 r0 = r4.c
            com.fossil.Kl3 r0 = r0.d()
            com.fossil.Nl3 r0 = r0.F()
            java.lang.String r2 = "Service connect failed to get IMeasurementService"
            r0.a(r2)
            goto L_0x003c
        L_0x007d:
            com.fossil.Fp3 r2 = r4.c
            com.fossil.Kl3 r2 = r2.d()
            com.fossil.Nl3 r2 = r2.F()
            java.lang.String r3 = "Got binder with a wrong descriptor"
            r2.b(r3, r0)
            goto L_0x003c
        L_0x008d:
            com.fossil.Fp3 r0 = r4.c
            com.fossil.Im3 r0 = r0.c()
            com.fossil.Zp3 r2 = new com.fossil.Zp3
            r2.<init>(r4, r1)
            r0.y(r2)
            goto L_0x0054
        L_0x009c:
            r0 = move-exception
            goto L_0x0054
        L_0x009e:
            r1 = move-exception
            r1 = r0
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Aq3.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        Rc2.f("MeasurementServiceConnection.onServiceDisconnected");
        this.c.d().M().a("Service disconnected");
        this.c.c().y(new Cq3(this, componentName));
    }
}
