package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G41 {
    @DexIgnore
    public static /* final */ String f; // = X01.f("WorkTimer");
    @DexIgnore
    public /* final */ ThreadFactory a; // = new Ai(this);
    @DexIgnore
    public /* final */ ScheduledExecutorService b; // = Executors.newSingleThreadScheduledExecutor(this.a);
    @DexIgnore
    public /* final */ Map<String, Ci> c; // = new HashMap();
    @DexIgnore
    public /* final */ Map<String, Bi> d; // = new HashMap();
    @DexIgnore
    public /* final */ Object e; // = new Object();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ThreadFactory {
        @DexIgnore
        public int a; // = 0;

        @DexIgnore
        public Ai(G41 g41) {
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.a);
            this.a = this.a + 1;
            return newThread;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Runnable {
        @DexIgnore
        public /* final */ G41 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public Ci(G41 g41, String str) {
            this.b = g41;
            this.c = str;
        }

        @DexIgnore
        public void run() {
            synchronized (this.b.e) {
                if (this.b.c.remove(this.c) != null) {
                    Bi remove = this.b.d.remove(this.c);
                    if (remove != null) {
                        remove.a(this.c);
                    }
                } else {
                    X01.c().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.c), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        if (!this.b.isShutdown()) {
            this.b.shutdownNow();
        }
    }

    @DexIgnore
    public void b(String str, long j, Bi bi) {
        synchronized (this.e) {
            X01.c().a(f, String.format("Starting timer for %s", str), new Throwable[0]);
            c(str);
            Ci ci = new Ci(this, str);
            this.c.put(str, ci);
            this.d.put(str, bi);
            this.b.schedule(ci, j, TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void c(String str) {
        synchronized (this.e) {
            if (this.c.remove(str) != null) {
                X01.c().a(f, String.format("Stopping timer for %s", str), new Throwable[0]);
                this.d.remove(str);
            }
        }
    }
}
