package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ld1 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new Ai());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((Id1) message.obj).b();
            return true;
        }
    }

    @DexIgnore
    public void a(Id1<?> id1, boolean z) {
        synchronized (this) {
            if (this.a || z) {
                this.b.obtainMessage(1, id1).sendToTarget();
            } else {
                this.a = true;
                id1.b();
                this.a = false;
            }
        }
    }
}
