package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ju {
    @DexIgnore
    public /* synthetic */ Ju(Qg6 qg6) {
    }

    @DexIgnore
    public final Ku a(byte b) {
        Ku ku;
        Ku[] values = Ku.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                ku = null;
                break;
            }
            ku = values[i];
            if (ku.c == b) {
                break;
            }
            i++;
        }
        return ku != null ? ku : Ku.f;
    }
}
