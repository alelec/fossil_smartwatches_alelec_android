package com.fossil;

import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A0 extends Hh<K0> {
    @DexIgnore
    public /* final */ /* synthetic */ G0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public A0(G0 g0, Oh oh) {
        super(oh);
        this.a = g0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
    @Override // com.mapped.Hh
    public void bind(Mi mi, K0 k0) {
        K0 k02 = k0;
        String str = k02.a;
        if (str == null) {
            mi.bindNull(1);
        } else {
            mi.bindString(1, str);
        }
        mi.bindLong(2, (long) this.a.c.a(k02.b));
        String b = this.a.d.b(k02.c);
        if (b == null) {
            mi.bindNull(3);
        } else {
            mi.bindString(3, b);
        }
        String str2 = k02.d;
        if (str2 == null) {
            mi.bindNull(4);
        } else {
            mi.bindString(4, str2);
        }
        String str3 = k02.e;
        if (str3 == null) {
            mi.bindNull(5);
        } else {
            mi.bindString(5, str3);
        }
        mi.bindLong(6, this.a.e.a(k02.b()));
        mi.bindLong(7, this.a.e.a(k02.a()));
        byte[] bArr = k02.h;
        if (bArr == null) {
            mi.bindNull(8);
        } else {
            mi.bindBlob(8, bArr);
        }
    }

    @DexIgnore
    @Override // com.mapped.Vh
    public String createQuery() {
        return "INSERT OR REPLACE INTO `ThemeTemplateEntity` (`id`,`classifier`,`packageOSVersion`,`checksum`,`downloadUrl`,`updatedAt`,`createdAt`,`data`) VALUES (?,?,?,?,?,?,?,?)";
    }
}
