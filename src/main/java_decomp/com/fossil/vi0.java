package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Vi0 {
    @DexIgnore
    void a(int i, int i2, int i3, int i4);

    @DexIgnore
    void b(int i, int i2);

    @DexIgnore
    void c(Drawable drawable);

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    Drawable f();

    @DexIgnore
    View g();
}
