package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q7 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ K5 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public Q7(K5 k5) {
        this.a = k5;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == 2084501365 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED")) {
            R4 a2 = R4.f.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", 10));
            R4 a3 = R4.f.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", 10));
            if (Wg6.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE"), this.a.A)) {
                if (a3 == R4.b) {
                    this.a.e = true;
                }
                K5 k5 = this.a;
                k5.b.post(new M4(k5, new G7(F7.b, 0, 2), a2, a3));
                K5 k52 = this.a;
                k52.b.post(new C5(k52, new G7(F7.b, 0, 2), a2, a3));
            }
        }
    }
}
