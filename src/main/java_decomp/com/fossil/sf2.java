package com.fossil;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sf2 implements ThreadFactory {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ThreadFactory b;

    @DexIgnore
    public Sf2(String str) {
        this(str, 0);
    }

    @DexIgnore
    public Sf2(String str, int i) {
        this.b = Executors.defaultThreadFactory();
        Rc2.l(str, "Name must not be null");
        this.a = str;
    }

    @DexIgnore
    public Thread newThread(Runnable runnable) {
        Thread newThread = this.b.newThread(new Uf2(runnable, 0));
        newThread.setName(this.a);
        return newThread;
    }
}
