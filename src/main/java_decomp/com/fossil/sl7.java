package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sl7 extends RuntimeException {
    @DexIgnore
    public Sl7() {
    }

    @DexIgnore
    public Sl7(String str) {
        super(str);
    }

    @DexIgnore
    public Sl7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public Sl7(Throwable th) {
        super(th);
    }
}
