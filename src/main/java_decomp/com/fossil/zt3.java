package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zt3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Au3 c;

    @DexIgnore
    public Zt3(Au3 au3, Nt3 nt3) {
        this.c = au3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        synchronized (Au3.b(this.c)) {
            if (Au3.c(this.c) != null) {
                Au3.c(this.c).onComplete(this.b);
            }
        }
    }
}
