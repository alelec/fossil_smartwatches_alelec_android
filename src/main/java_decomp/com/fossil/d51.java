package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "coil.RealImageLoader$loadData$2", f = "RealImageLoader.kt", l = {369, 385, 511}, m = "invokeSuspend")
public final class D51 extends Ko7 implements Coroutine<Il6, Xe6<? super C61>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Object $data;
    @DexIgnore
    public /* final */ /* synthetic */ E61 $fetcher;
    @DexIgnore
    public /* final */ /* synthetic */ Object $mappedData;
    @DexIgnore
    public /* final */ /* synthetic */ X71 $request;
    @DexIgnore
    public /* final */ /* synthetic */ E81 $scale;
    @DexIgnore
    public /* final */ /* synthetic */ F81 $size;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$11;
    @DexIgnore
    public Object L$12;
    @DexIgnore
    public Object L$13;
    @DexIgnore
    public Object L$14;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ C51 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public D51(C51 c51, X71 x71, F81 f81, E81 e81, E61 e61, Object obj, Object obj2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = c51;
        this.$request = x71;
        this.$size = f81;
        this.$scale = e81;
        this.$fetcher = e61;
        this.$mappedData = obj;
        this.$data = obj2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        D51 d51 = new D51(this.this$0, this.$request, this.$size, this.$scale, this.$fetcher, this.$mappedData, this.$data, xe6);
        d51.p$ = (Il6) obj;
        throw null;
        //return d51;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super C61> xe6) {
        throw null;
        //return ((D51) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0176 A[SYNTHETIC, Splitter:B:38:0x0176] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0255  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r23) {
        /*
        // Method dump skipped, instructions count: 777
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.D51.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
