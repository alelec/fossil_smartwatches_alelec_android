package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E14 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Aii b;
        @DexIgnore
        public Aii c;
        @DexIgnore
        public boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public String a;
            @DexIgnore
            public Object b;
            @DexIgnore
            public Aii c;

            @DexIgnore
            public Aii() {
            }
        }

        @DexIgnore
        public Bi(String str) {
            Aii aii = new Aii();
            this.b = aii;
            this.c = aii;
            this.d = false;
            I14.l(str);
            this.a = str;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi a(String str, int i) {
            e(str, String.valueOf(i));
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi b(String str, Object obj) {
            e(str, obj);
            return this;
        }

        @DexIgnore
        public final Aii c() {
            Aii aii = new Aii();
            this.c.c = aii;
            this.c = aii;
            return aii;
        }

        @DexIgnore
        public final Bi d(Object obj) {
            c().b = obj;
            return this;
        }

        @DexIgnore
        public final Bi e(String str, Object obj) {
            Aii c2 = c();
            c2.b = obj;
            I14.l(str);
            c2.a = str;
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Bi f(Object obj) {
            d(obj);
            return this;
        }

        @DexIgnore
        public String toString() {
            boolean z = this.d;
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.a);
            sb.append('{');
            String str = "";
            for (Aii aii = this.b.c; aii != null; aii = aii.c) {
                Object obj = aii.b;
                if (!z || obj != null) {
                    sb.append(str);
                    String str2 = aii.a;
                    if (str2 != null) {
                        sb.append(str2);
                        sb.append('=');
                    }
                    if (obj == null || !obj.getClass().isArray()) {
                        sb.append(obj);
                    } else {
                        String deepToString = Arrays.deepToString(new Object[]{obj});
                        sb.append((CharSequence) deepToString, 1, deepToString.length() - 1);
                    }
                    str = ", ";
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    @DexIgnore
    public static <T> T a(T t, T t2) {
        if (t != null) {
            return t;
        }
        I14.l(t2);
        return t2;
    }

    @DexIgnore
    public static Bi b(Object obj) {
        return new Bi(obj.getClass().getSimpleName());
    }
}
