package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz6 implements MembersInjector<SignUpPresenter> {
    @DexIgnore
    public static void A(SignUpPresenter signUpPresenter, ReconnectDeviceUseCase reconnectDeviceUseCase) {
        signUpPresenter.u = reconnectDeviceUseCase;
    }

    @DexIgnore
    public static void B(SignUpPresenter signUpPresenter, RequestEmailOtp requestEmailOtp) {
        signUpPresenter.H = requestEmailOtp;
    }

    @DexIgnore
    public static void C(SignUpPresenter signUpPresenter, An4 an4) {
        signUpPresenter.K = an4;
    }

    @DexIgnore
    public static void D(SignUpPresenter signUpPresenter, An4 an4) {
        signUpPresenter.x = an4;
    }

    @DexIgnore
    public static void E(SignUpPresenter signUpPresenter, SleepSummariesRepository sleepSummariesRepository) {
        signUpPresenter.D = sleepSummariesRepository;
    }

    @DexIgnore
    public static void F(SignUpPresenter signUpPresenter, SummariesRepository summariesRepository) {
        signUpPresenter.C = summariesRepository;
    }

    @DexIgnore
    public static void G(SignUpPresenter signUpPresenter, Uq4 uq4) {
        signUpPresenter.m = uq4;
    }

    @DexIgnore
    public static void H(SignUpPresenter signUpPresenter, UserRepository userRepository) {
        signUpPresenter.k = userRepository;
    }

    @DexIgnore
    public static void I(SignUpPresenter signUpPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        signUpPresenter.J = watchLocalizationRepository;
    }

    @DexIgnore
    public static void J(SignUpPresenter signUpPresenter, WorkoutSettingRepository workoutSettingRepository) {
        signUpPresenter.N = workoutSettingRepository;
    }

    @DexIgnore
    public static void K(SignUpPresenter signUpPresenter) {
        signUpPresenter.c0();
    }

    @DexIgnore
    public static void a(SignUpPresenter signUpPresenter, FCMRepository fCMRepository) {
        signUpPresenter.L = fCMRepository;
    }

    @DexIgnore
    public static void b(SignUpPresenter signUpPresenter, FlagRepository flagRepository) {
        signUpPresenter.M = flagRepository;
    }

    @DexIgnore
    public static void c(SignUpPresenter signUpPresenter, AlarmsRepository alarmsRepository) {
        signUpPresenter.t = alarmsRepository;
    }

    @DexIgnore
    public static void d(SignUpPresenter signUpPresenter, AnalyticsHelper analyticsHelper) {
        signUpPresenter.A = analyticsHelper;
    }

    @DexIgnore
    public static void e(SignUpPresenter signUpPresenter, CheckAuthenticationEmailExisting checkAuthenticationEmailExisting) {
        signUpPresenter.y = checkAuthenticationEmailExisting;
    }

    @DexIgnore
    public static void f(SignUpPresenter signUpPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        signUpPresenter.z = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void g(SignUpPresenter signUpPresenter, DeviceRepository deviceRepository) {
        signUpPresenter.l = deviceRepository;
    }

    @DexIgnore
    public static void h(SignUpPresenter signUpPresenter, Cj4 cj4) {
        signUpPresenter.v = cj4;
    }

    @DexIgnore
    public static void i(SignUpPresenter signUpPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        signUpPresenter.w = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void j(SignUpPresenter signUpPresenter, FetchActivities fetchActivities) {
        signUpPresenter.p = fetchActivities;
    }

    @DexIgnore
    public static void k(SignUpPresenter signUpPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        signUpPresenter.F = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void l(SignUpPresenter signUpPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        signUpPresenter.s = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void m(SignUpPresenter signUpPresenter, FetchGoalTrackingData fetchGoalTrackingData) {
        signUpPresenter.G = fetchGoalTrackingData;
    }

    @DexIgnore
    public static void n(SignUpPresenter signUpPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        signUpPresenter.r = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void o(SignUpPresenter signUpPresenter, FetchSleepSessions fetchSleepSessions) {
        signUpPresenter.n = fetchSleepSessions;
    }

    @DexIgnore
    public static void p(SignUpPresenter signUpPresenter, FetchSleepSummaries fetchSleepSummaries) {
        signUpPresenter.o = fetchSleepSummaries;
    }

    @DexIgnore
    public static void q(SignUpPresenter signUpPresenter, FetchSummaries fetchSummaries) {
        signUpPresenter.q = fetchSummaries;
    }

    @DexIgnore
    public static void r(SignUpPresenter signUpPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        signUpPresenter.I = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void s(SignUpPresenter signUpPresenter, GetUser getUser) {
        signUpPresenter.B = getUser;
    }

    @DexIgnore
    public static void t(SignUpPresenter signUpPresenter, GoalTrackingRepository goalTrackingRepository) {
        signUpPresenter.E = goalTrackingRepository;
    }

    @DexIgnore
    public static void u(SignUpPresenter signUpPresenter, Dv5 dv5) {
        signUpPresenter.e = dv5;
    }

    @DexIgnore
    public static void v(SignUpPresenter signUpPresenter, Ev5 ev5) {
        signUpPresenter.g = ev5;
    }

    @DexIgnore
    public static void w(SignUpPresenter signUpPresenter, LoginSocialUseCase loginSocialUseCase) {
        signUpPresenter.j = loginSocialUseCase;
    }

    @DexIgnore
    public static void x(SignUpPresenter signUpPresenter, Gv5 gv5) {
        signUpPresenter.i = gv5;
    }

    @DexIgnore
    public static void y(SignUpPresenter signUpPresenter, Xn5 xn5) {
        signUpPresenter.f = xn5;
    }

    @DexIgnore
    public static void z(SignUpPresenter signUpPresenter, Hv5 hv5) {
        signUpPresenter.h = hv5;
    }
}
