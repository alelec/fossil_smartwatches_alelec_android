package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.widget.ImageView;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Iy0 {
    @DexIgnore
    public static boolean a; // = true;
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.widget.ImageView r5, android.graphics.Matrix r6) {
        /*
            r1 = 0
            r4 = 0
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 29
            if (r0 < r2) goto L_0x000c
            r5.animateTransform(r6)
        L_0x000b:
            return
        L_0x000c:
            if (r6 != 0) goto L_0x0037
            android.graphics.drawable.Drawable r0 = r5.getDrawable()
            if (r0 == 0) goto L_0x000b
            int r1 = r5.getWidth()
            int r2 = r5.getPaddingLeft()
            int r1 = r1 - r2
            int r2 = r5.getPaddingRight()
            int r1 = r1 - r2
            int r2 = r5.getHeight()
            int r3 = r5.getPaddingTop()
            int r2 = r2 - r3
            int r3 = r5.getPaddingBottom()
            int r2 = r2 - r3
            r0.setBounds(r4, r4, r1, r2)
            r5.invalidate()
            goto L_0x000b
        L_0x0037:
            r2 = 21
            if (r0 < r2) goto L_0x003f
            c(r5, r6)
            goto L_0x000b
        L_0x003f:
            android.graphics.drawable.Drawable r0 = r5.getDrawable()
            if (r0 == 0) goto L_0x000b
            int r2 = r0.getIntrinsicWidth()
            int r3 = r0.getIntrinsicHeight()
            r0.setBounds(r4, r4, r2, r3)
            b()
            java.lang.reflect.Field r0 = com.fossil.Iy0.b
            if (r0 == 0) goto L_0x0069
            java.lang.Object r0 = r0.get(r5)     // Catch:{ IllegalAccessException -> 0x0075 }
            android.graphics.Matrix r0 = (android.graphics.Matrix) r0     // Catch:{ IllegalAccessException -> 0x0075 }
            if (r0 != 0) goto L_0x0073
            android.graphics.Matrix r1 = new android.graphics.Matrix     // Catch:{ IllegalAccessException -> 0x0072 }
            r1.<init>()     // Catch:{ IllegalAccessException -> 0x0072 }
            java.lang.reflect.Field r0 = com.fossil.Iy0.b     // Catch:{ IllegalAccessException -> 0x0079 }
            r0.set(r5, r1)     // Catch:{ IllegalAccessException -> 0x0079 }
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.set(r6)
        L_0x006e:
            r5.invalidate()
            goto L_0x000b
        L_0x0072:
            r1 = move-exception
        L_0x0073:
            r1 = r0
            goto L_0x0069
        L_0x0075:
            r0 = move-exception
            r0 = r1
        L_0x0077:
            r1 = r0
            goto L_0x0069
        L_0x0079:
            r0 = move-exception
            r0 = r1
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iy0.a(android.widget.ImageView, android.graphics.Matrix):void");
    }

    @DexIgnore
    public static void b() {
        if (!c) {
            try {
                Field declaredField = ImageView.class.getDeclaredField("mDrawMatrix");
                b = declaredField;
                declaredField.setAccessible(true);
            } catch (NoSuchFieldException e) {
            }
            c = true;
        }
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void c(ImageView imageView, Matrix matrix) {
        if (a) {
            try {
                imageView.animateTransform(matrix);
            } catch (NoSuchMethodError e) {
                a = false;
            }
        }
    }
}
