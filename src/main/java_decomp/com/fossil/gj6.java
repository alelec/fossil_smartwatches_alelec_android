package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gj6 extends BaseFragment implements Fj6 {
    @DexIgnore
    public G37<H75> g;
    @DexIgnore
    public Ej6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void K6(Ej6 ej6) {
        Wg6.c(ej6, "presenter");
        this.h = ej6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ej6 ej6) {
        K6(ej6);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        G37<H75> g37 = new G37<>(this, (H75) Aq0.f(layoutInflater, 2131558567, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            H75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        Ej6 ej6 = this.h;
        if (ej6 != null) {
            ej6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        Ej6 ej6 = this.h;
        if (ej6 != null) {
            ej6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fj6
    public void q5(List<Integer> list, List<String> list2) {
        WeekHeartRateChart weekHeartRateChart;
        Wg6.c(list, "data");
        Wg6.c(list2, "listWeekDays");
        G37<H75> g37 = this.g;
        if (g37 != null) {
            H75 a2 = g37.a();
            if (a2 != null && (weekHeartRateChart = a2.q) != null) {
                weekHeartRateChart.setListWeekDays(list2);
                weekHeartRateChart.m(list);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
