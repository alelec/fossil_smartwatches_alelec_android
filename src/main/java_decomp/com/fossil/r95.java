package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleSwitchCompat q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public R95(Object obj, View view, int i, FlexibleSwitchCompat flexibleSwitchCompat, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ConstraintLayout constraintLayout4, FlexibleSwitchCompat flexibleSwitchCompat2, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = flexibleSwitchCompat;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = rTLImageView;
        this.x = constraintLayout4;
        this.y = flexibleSwitchCompat2;
        this.z = flexibleTextView3;
    }
}
