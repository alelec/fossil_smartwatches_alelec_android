package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class I15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ NumberPicker s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ NumberPicker u;
    @DexIgnore
    public /* final */ NumberPicker v;

    @DexIgnore
    public I15(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, NumberPicker numberPicker, FlexibleTextView flexibleTextView, NumberPicker numberPicker2, NumberPicker numberPicker3) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = numberPicker;
        this.t = flexibleTextView;
        this.u = numberPicker2;
        this.v = numberPicker3;
    }
}
