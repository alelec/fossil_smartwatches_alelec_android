package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Os6 implements Factory<CustomizeGoalTrackingChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Os6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Os6 a(Provider<ThemeRepository> provider) {
        return new Os6(provider);
    }

    @DexIgnore
    public static CustomizeGoalTrackingChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeGoalTrackingChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeGoalTrackingChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
