package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sk4 {
    /*
    static {
        Logger.getLogger(Sk4.class.getName());
        Collections.synchronizedMap(new HashMap());
        Collections.synchronizedMap(new HashMap());
        Pk4.a();
        Zk4.a();
    }
    */

    @DexIgnore
    public static El4 a(ObjectInputStream objectInputStream, int i) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[i];
        while (true) {
            int read = objectInputStream.read(bArr, 0, i);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return El4.d(byteArrayOutputStream.toByteArray());
            }
        }
    }
}
