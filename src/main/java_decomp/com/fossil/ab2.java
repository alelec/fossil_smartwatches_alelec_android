package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ab2 implements R92 {
    @DexIgnore
    public /* final */ /* synthetic */ Ya2 a;

    @DexIgnore
    public Ab2(Ya2 ya2) {
        this.a = ya2;
    }

    @DexIgnore
    public /* synthetic */ Ab2(Ya2 ya2, Xa2 xa2) {
        this(ya2);
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void a(Z52 z52) {
        this.a.s.lock();
        try {
            this.a.k = z52;
            this.a.C();
        } finally {
            this.a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void b(Bundle bundle) {
        this.a.s.lock();
        try {
            this.a.p(bundle);
            this.a.k = Z52.f;
            this.a.C();
        } finally {
            this.a.s.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.R92
    public final void c(int i, boolean z) {
        this.a.s.lock();
        try {
            if ((this.a.m) || this.a.l == null || !this.a.l.A()) {
                this.a.m = false;
                this.a.o(i, z);
                return;
            }
            this.a.m = true;
            this.a.f.d(i);
            this.a.s.unlock();
        } finally {
            this.a.s.unlock();
        }
    }
}
