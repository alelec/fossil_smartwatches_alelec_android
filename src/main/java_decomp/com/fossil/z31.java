package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z31 {
    @DexIgnore
    public /* final */ WorkDatabase a;

    @DexIgnore
    public Z31(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    @DexIgnore
    public static void b(Context context, Lx0 lx0) {
        long j = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.preferences", 0);
        if (sharedPreferences.contains("reschedule_needed") || sharedPreferences.contains("last_cancel_all_time_ms")) {
            long j2 = sharedPreferences.getLong("last_cancel_all_time_ms", 0);
            if (sharedPreferences.getBoolean("reschedule_needed", false)) {
                j = 1;
            }
            lx0.beginTransaction();
            try {
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"last_cancel_all_time_ms", Long.valueOf(j2)});
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", Long.valueOf(j)});
                sharedPreferences.edit().clear().apply();
                lx0.setTransactionSuccessful();
            } finally {
                lx0.endTransaction();
            }
        }
    }

    @DexIgnore
    public boolean a() {
        Long a2 = this.a.f().a("reschedule_needed");
        return a2 != null && a2.longValue() == 1;
    }

    @DexIgnore
    public void c(boolean z) {
        this.a.f().b(new C31("reschedule_needed", z));
    }
}
