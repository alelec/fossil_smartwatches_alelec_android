package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y02 implements Factory<X02> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<T32> b;
    @DexIgnore
    public /* final */ Provider<T32> c;

    @DexIgnore
    public Y02(Provider<Context> provider, Provider<T32> provider2, Provider<T32> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Y02 a(Provider<Context> provider, Provider<T32> provider2, Provider<T32> provider3) {
        return new Y02(provider, provider2, provider3);
    }

    @DexIgnore
    public X02 b() {
        return new X02(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
