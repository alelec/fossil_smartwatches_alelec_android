package com.fossil;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ko0 {
    @DexIgnore
    public Object a;

    @DexIgnore
    public Ko0(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static Ko0 b(Context context, int i) {
        return Build.VERSION.SDK_INT >= 24 ? new Ko0(PointerIcon.getSystemIcon(context, i)) : new Ko0(null);
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }
}
