package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ks7<R> extends Ds7<R> {

    @DexIgnore
    public interface Ai<R> extends Object<R> {
    }

    @DexIgnore
    boolean isConst();

    @DexIgnore
    boolean isLateinit();
}
