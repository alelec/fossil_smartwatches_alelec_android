package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fg4 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.MINUTES.toMillis(1);
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Bt3 c;

    @DexIgnore
    public static void a(Context context) {
        if (c == null) {
            Bt3 bt3 = new Bt3(context, 1, "wake:com.google.firebase.iid.WakeLockHolder");
            c = bt3;
            bt3.c(true);
        }
    }

    @DexIgnore
    public static void b(Intent intent) {
        synchronized (b) {
            if (c != null && c(intent)) {
                d(intent, false);
                c.b();
            }
        }
    }

    @DexIgnore
    public static boolean c(Intent intent) {
        return intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
    }

    @DexIgnore
    public static void d(Intent intent, boolean z) {
        intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", z);
    }

    @DexIgnore
    public static ComponentName e(Context context, Intent intent) {
        synchronized (b) {
            a(context);
            boolean c2 = c(intent);
            d(intent, true);
            ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            if (!c2) {
                c.a(a);
            }
            return startService;
        }
    }
}
