package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dr6 implements Factory<ThemesViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Dr6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Dr6 a(Provider<ThemeRepository> provider) {
        return new Dr6(provider);
    }

    @DexIgnore
    public static ThemesViewModel c(ThemeRepository themeRepository) {
        return new ThemesViewModel(themeRepository);
    }

    @DexIgnore
    public ThemesViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
