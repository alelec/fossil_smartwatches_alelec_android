package com.fossil;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hy2 {
    @DexIgnore
    @NullableDecl
    public static <T> T a(Iterator<? extends T> it, @NullableDecl T t) {
        if (it.hasNext()) {
            return (T) it.next();
        }
        return null;
    }
}
