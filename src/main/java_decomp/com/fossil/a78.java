package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a78 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static byte[] f210a; // = new byte[255];
    @DexIgnore
    public static byte[] b; // = new byte[64];

    /*
    static {
        int i;
        int i2;
        int i3 = 0;
        "\r\n".getBytes();
        for (int i4 = 0; i4 < 255; i4++) {
            f210a[i4] = (byte) -1;
        }
        for (int i5 = 90; i5 >= 65; i5--) {
            f210a[i5] = (byte) ((byte) (i5 - 65));
        }
        int i6 = 122;
        while (true) {
            i = 26;
            if (i6 < 97) {
                break;
            }
            f210a[i6] = (byte) ((byte) ((i6 - 97) + 26));
            i6--;
        }
        int i7 = 57;
        while (true) {
            i2 = 52;
            if (i7 < 48) {
                break;
            }
            f210a[i7] = (byte) ((byte) ((i7 - 48) + 52));
            i7--;
        }
        byte[] bArr = f210a;
        bArr[43] = (byte) 62;
        bArr[47] = (byte) 63;
        for (int i8 = 0; i8 <= 25; i8++) {
            b[i8] = (byte) ((byte) (i8 + 65));
        }
        int i9 = 0;
        while (i <= 51) {
            b[i] = (byte) ((byte) (i9 + 97));
            i++;
            i9++;
        }
        while (i2 <= 61) {
            b[i2] = (byte) ((byte) (i3 + 48));
            i2++;
            i3++;
        }
        byte[] bArr2 = b;
        bArr2[62] = (byte) 43;
        bArr2[63] = (byte) 47;
    }
    */

    @DexIgnore
    public static byte[] a(byte[] bArr) {
        byte[] b2 = b(bArr);
        if (b2.length == 0) {
            return new byte[0];
        }
        int length = b2.length / 4;
        int length2 = b2.length;
        while (b2[length2 - 1] == 61) {
            length2--;
            if (length2 == 0) {
                return new byte[0];
            }
        }
        byte[] bArr2 = new byte[(length2 - length)];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i2 * 4;
            byte b3 = b2[i3 + 2];
            byte b4 = b2[i3 + 3];
            byte[] bArr3 = f210a;
            byte b5 = bArr3[b2[i3]];
            byte b6 = bArr3[b2[i3 + 1]];
            if (b3 != 61 && b4 != 61) {
                byte b7 = bArr3[b3];
                byte b8 = bArr3[b4];
                bArr2[i] = (byte) ((byte) ((b5 << 2) | (b6 >> 4)));
                bArr2[i + 1] = (byte) ((byte) (((b6 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 4) | ((b7 >> 2) & 15)));
                bArr2[i + 2] = (byte) ((byte) ((b7 << 6) | b8));
            } else if (b3 == 61) {
                bArr2[i] = (byte) ((byte) ((b6 >> 4) | (b5 << 2)));
            } else if (b4 == 61) {
                byte b9 = f210a[b3];
                bArr2[i] = (byte) ((byte) ((b5 << 2) | (b6 >> 4)));
                bArr2[i + 1] = (byte) ((byte) (((b6 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 4) | ((b9 >> 2) & 15)));
            }
            i += 3;
        }
        return bArr2;
    }

    @DexIgnore
    public static byte[] b(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (byte b2 : bArr) {
            if (c(b2)) {
                bArr2[i] = b2;
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        return bArr3;
    }

    @DexIgnore
    public static boolean c(byte b2) {
        if (b2 == 61) {
            return true;
        }
        return b2 >= 0 && f210a[b2] != -1;
    }
}
