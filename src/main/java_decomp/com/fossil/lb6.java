package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Lb6 extends Rv5<Kb6> {
    @DexIgnore
    void C5(String str);

    @DexIgnore
    void J2(List<M66> list);

    @DexIgnore
    Object N3();  // void declaration

    @DexIgnore
    void O(int i);

    @DexIgnore
    void c0(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j2(boolean z);

    @DexIgnore
    void m0(int i);

    @DexIgnore
    void r(boolean z);

    @DexIgnore
    Object u();  // void declaration

    @DexIgnore
    Object w();  // void declaration

    @DexIgnore
    Object y();  // void declaration
}
