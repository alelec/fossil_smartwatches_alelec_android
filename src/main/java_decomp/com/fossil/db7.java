package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Db7 {
    @DexIgnore
    public HashSet<String> a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public Db7() {
        this(null, null, null, false, 15, null);
    }

    @DexIgnore
    public Db7(HashSet<String> hashSet, String str, String str2, boolean z) {
        Wg6.c(hashSet, "list");
        Wg6.c(str2, "ringId");
        this.a = hashSet;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Db7(HashSet hashSet, String str, String str2, boolean z, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new HashSet() : hashSet, (i & 2) != 0 ? null : str, (i & 4) != 0 ? "" : str2, (i & 8) != 0 ? false : z);
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final HashSet<String> b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e(String str) {
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Db7) {
                Db7 db7 = (Db7) obj;
                if (!Wg6.a(this.a, db7.a) || !Wg6.a(this.b, db7.b) || !Wg6.a(this.c, db7.c) || this.d != db7.d) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void f(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void g(String str) {
        Wg6.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        HashSet<String> hashSet = this.a;
        int hashCode = hashSet != null ? hashSet.hashCode() : 0;
        String str = this.b;
        int hashCode2 = str != null ? str.hashCode() : 0;
        String str2 = this.c;
        if (str2 != null) {
            i = str2.hashCode();
        }
        boolean z = this.d;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "SelectedComplications(list=" + this.a + ", currentId=" + this.b + ", ringId=" + this.c + ", isGoalRingEnabled=" + this.d + ")";
    }
}
