package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Yb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R0 extends Qq7 implements Hg6<Lp, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Yb0 b;
    @DexIgnore
    public /* final */ /* synthetic */ E60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public R0(Yb0 yb0, E60 e60, Lp lp) {
        super(1);
        this.b = yb0;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Lp lp) {
        Lp lp2 = lp;
        if (lp2.v.c == Zq.b) {
            Object x = lp2.x();
            if (x instanceof Cd6) {
                this.c.c.post(new L60(this, x));
            } else {
                this.c.c.post(new C70(this));
            }
        } else {
            this.c.c.post(new U70(this, lp2));
            this.c.n0(lp2.v);
        }
        return Cd6.a;
    }
}
