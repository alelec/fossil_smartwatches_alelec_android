package com.fossil;

import com.mapped.FileHelper;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D97 {
    @DexIgnore
    public static final F97 a(G97 g97) {
        Wg6.c(g97, "$this$toPhotoDraft");
        byte[] b = J37.b(new File(g97.e()));
        Wg6.b(b, "File(localPath).toBinary()");
        return new F97(g97.d(), J37.a(b));
    }

    @DexIgnore
    public static final List<F97> b(List<G97> list) {
        Wg6.c(list, "$this$toPhotoDrafts");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(a(it.next()));
        }
        return arrayList;
    }

    @DexIgnore
    public static final Fb7 c(G97 g97) {
        Wg6.c(g97, "$this$toUIBackgroundPhoto");
        int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165379);
        return new Fb7(g97.d(), g97.d(), g97.c(), g97.c(), L77.BACKGROUND_PHOTO, g97.a(), FileHelper.a.d(g97.e(), dimensionPixelSize, dimensionPixelSize, FileType.WATCH_FACE), null, g97.e());
    }

    @DexIgnore
    public static final List<Fb7> d(List<G97> list) {
        Wg6.c(list, "$this$toUIBackgroundPhotos");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(c(it.next()));
        }
        return arrayList;
    }
}
