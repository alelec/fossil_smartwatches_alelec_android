package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B31 implements A31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Z21> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<Z21> {
        @DexIgnore
        public Ai(B31 b31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Z21 z21) {
            String str = z21.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            String str2 = z21.b;
            if (str2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, str2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Z21 z21) {
            a(mi, z21);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public B31(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.A31
    public void a(Z21 z21) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert((Hh<Z21>) z21);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.A31
    public List<String> b(String str) {
        Rh f = Rh.f("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.A31
    public boolean c(String str) {
        boolean z = true;
        Rh f = Rh.f("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            if (!b2.moveToFirst()) {
                z = false;
            } else if (b2.getInt(0) == 0) {
                z = false;
            }
            return z;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.A31
    public boolean d(String str) {
        boolean z = true;
        Rh f = Rh.f("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            if (!b2.moveToFirst()) {
                z = false;
            } else if (b2.getInt(0) == 0) {
                z = false;
            }
            return z;
        } finally {
            b2.close();
            f.m();
        }
    }
}
