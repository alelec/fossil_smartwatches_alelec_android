package com.fossil;

import com.fossil.S32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class H12 implements S32.Ai {
    @DexIgnore
    public /* final */ I12 a;
    @DexIgnore
    public /* final */ H02 b;
    @DexIgnore
    public /* final */ C02 c;

    @DexIgnore
    public H12(I12 i12, H02 h02, C02 c02) {
        this.a = i12;
        this.b = h02;
        this.c = c02;
    }

    @DexIgnore
    public static S32.Ai b(I12 i12, H02 h02, C02 c02) {
        return new H12(i12, h02, c02);
    }

    @DexIgnore
    @Override // com.fossil.S32.Ai
    public Object a() {
        return I12.b(this.a, this.b, this.c);
    }
}
