package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yl extends Bi {
    @DexIgnore
    public Yl(K5 k5, I60 i60, Yp yp, HashMap<Hu1, Object> hashMap, String str) {
        super(k5, i60, yp, Ke.b.a(k5.x, Ob.u), false, hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 80);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public JSONObject E() {
        JSONArray jSONArray = new JSONArray();
        Iterator<J0> it = this.I.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().a(false));
        }
        JSONObject put = super.E().put(Ey1.a(Hu1.SKIP_ERASE), this.Q);
        Wg6.b(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        return G80.k(put, Jd0.P2, jSONArray);
    }

    @DexIgnore
    public byte[][] a0() {
        ArrayList<J0> arrayList = this.I;
        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(it.next().f);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public /* bridge */ /* synthetic */ Object x() {
        return a0();
    }
}
