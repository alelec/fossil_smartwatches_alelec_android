package com.fossil;

import android.os.Bundle;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E84 implements C84, I84 {
    @DexIgnore
    public H84 a;

    @DexIgnore
    public static String b(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }

    @DexIgnore
    @Override // com.fossil.I84
    public void a(H84 h84) {
        this.a = h84;
        X74.f().b("Registered Firebase Analytics event receiver for breadcrumbs");
    }

    @DexIgnore
    @Override // com.fossil.C84
    public void q(String str, Bundle bundle) {
        H84 h84 = this.a;
        if (h84 != null) {
            try {
                h84.a("$A$:" + b(str, bundle));
            } catch (JSONException e) {
                X74.f().i("Unable to serialize Firebase Analytics event to breadcrumb.");
            }
        }
    }
}
