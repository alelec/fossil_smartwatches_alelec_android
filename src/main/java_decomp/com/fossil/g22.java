package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G22 implements Factory<F22> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<K22> b;
    @DexIgnore
    public /* final */ Provider<H22> c;
    @DexIgnore
    public /* final */ Provider<S32> d;

    @DexIgnore
    public G22(Provider<Executor> provider, Provider<K22> provider2, Provider<H22> provider3, Provider<S32> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static G22 a(Provider<Executor> provider, Provider<K22> provider2, Provider<H22> provider3, Provider<S32> provider4) {
        return new G22(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public F22 b() {
        return new F22(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
