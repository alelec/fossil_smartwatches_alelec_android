package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vt7 extends Ut7 {
    @DexIgnore
    public static final String g(String str) {
        Wg6.c(str, "$this$capitalize");
        if (!(str.length() > 0) || !Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        String substring = str.substring(0, 1);
        Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring != null) {
            String upperCase = substring.toUpperCase();
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
            sb.append(upperCase);
            String substring2 = str.substring(1);
            Wg6.b(substring2, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring2);
            return sb.toString();
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final boolean h(String str, String str2, boolean z) {
        Wg6.c(str, "$this$endsWith");
        Wg6.c(str2, "suffix");
        return !z ? str.endsWith(str2) : m(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    @DexIgnore
    public static /* synthetic */ boolean i(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return h(str, str2, z);
    }

    @DexIgnore
    public static final boolean j(String str, String str2, boolean z) {
        return str == null ? str2 == null : !z ? str.equals(str2) : str.equalsIgnoreCase(str2);
    }

    @DexIgnore
    public static /* synthetic */ boolean k(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return j(str, str2, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean l(java.lang.CharSequence r4) {
        /*
            r3 = 1
            r2 = 0
            java.lang.String r0 = "$this$isBlank"
            com.mapped.Wg6.c(r4, r0)
            int r0 = r4.length()
            if (r0 == 0) goto L_0x0021
            com.fossil.Wr7 r1 = com.fossil.Wt7.z(r4)
            boolean r0 = r1 instanceof java.util.Collection
            if (r0 == 0) goto L_0x0023
            r0 = r1
            java.util.Collection r0 = (java.util.Collection) r0
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0023
        L_0x001e:
            r0 = r3
        L_0x001f:
            if (r0 == 0) goto L_0x0040
        L_0x0021:
            r0 = r3
        L_0x0022:
            return r0
        L_0x0023:
            java.util.Iterator r1 = r1.iterator()
        L_0x0027:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x001e
            r0 = r1
            com.fossil.Um7 r0 = (com.fossil.Um7) r0
            int r0 = r0.b()
            char r0 = r4.charAt(r0)
            boolean r0 = com.fossil.Ct7.c(r0)
            if (r0 != 0) goto L_0x0027
            r0 = r2
            goto L_0x001f
        L_0x0040:
            r0 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vt7.l(java.lang.CharSequence):boolean");
    }

    @DexIgnore
    public static final boolean m(String str, int i, String str2, int i2, int i3, boolean z) {
        Wg6.c(str, "$this$regionMatches");
        Wg6.c(str2, FacebookRequestErrorClassification.KEY_OTHER);
        return !z ? str.regionMatches(i, str2, i2, i3) : str.regionMatches(z, i, str2, i2, i3);
    }

    @DexIgnore
    public static final String n(String str, char c, char c2, boolean z) {
        Wg6.c(str, "$this$replace");
        if (!z) {
            String replace = str.replace(c, c2);
            Wg6.b(replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return At7.n(Wt7.b0(str, new char[]{c}, z, 0, 4, null), String.valueOf(c2), null, null, 0, null, null, 62, null);
    }

    @DexIgnore
    public static final String o(String str, String str2, String str3, boolean z) {
        Wg6.c(str, "$this$replace");
        Wg6.c(str2, "oldValue");
        Wg6.c(str3, "newValue");
        return At7.n(Wt7.c0(str, new String[]{str2}, z, 0, 4, null), str3, null, null, 0, null, null, 62, null);
    }

    @DexIgnore
    public static /* synthetic */ String p(String str, char c, char c2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return n(str, c, c2, z);
    }

    @DexIgnore
    public static /* synthetic */ String q(String str, String str2, String str3, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return o(str, str2, str3, z);
    }

    @DexIgnore
    public static final boolean r(String str, String str2, boolean z) {
        Wg6.c(str, "$this$startsWith");
        Wg6.c(str2, "prefix");
        return !z ? str.startsWith(str2) : m(str, 0, str2, 0, str2.length(), z);
    }

    @DexIgnore
    public static /* synthetic */ boolean s(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return r(str, str2, z);
    }
}
