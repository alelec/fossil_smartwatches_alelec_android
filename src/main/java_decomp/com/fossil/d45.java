package com.fossil;

import android.view.View;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class D45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ ProgressBar s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public D45(Object obj, View view, int i, ConstraintLayout constraintLayout, RTLImageView rTLImageView, ProgressBar progressBar, ConstraintLayout constraintLayout2, RecyclerView recyclerView, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = rTLImageView;
        this.s = progressBar;
        this.t = constraintLayout2;
        this.u = recyclerView;
        this.v = flexibleTextView;
    }
}
