package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z48 extends L48 {
    @DexIgnore
    public /* final */ transient byte[][] d;
    @DexIgnore
    public /* final */ transient int[] e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Z48(byte[][] bArr, int[] iArr) {
        super(L48.EMPTY.getData$okio());
        Wg6.c(bArr, "segments");
        Wg6.c(iArr, "directory");
        this.d = bArr;
        this.e = iArr;
    }

    @DexIgnore
    private final Object writeReplace() {
        L48 a2 = a();
        if (a2 != null) {
            return a2;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.Object");
    }

    @DexIgnore
    public final L48 a() {
        return new L48(toByteArray());
    }

    @DexIgnore
    @Override // com.fossil.L48
    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer();
        Wg6.b(asReadOnlyBuffer, "ByteBuffer.wrap(toByteArray()).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public String base64() {
        return a().base64();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public String base64Url() {
        return a().base64Url();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public L48 digest$okio(String str) {
        Wg6.c(str, "algorithm");
        MessageDigest instance = MessageDigest.getInstance(str);
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            int i3 = getDirectory$okio()[length + i2];
            int i4 = getDirectory$okio()[i2];
            instance.update(getSegments$okio()[i2], i3, i4 - i);
            i2++;
            i = i4;
        }
        byte[] digest = instance.digest();
        Wg6.b(digest, "digest.digest()");
        return new L48(digest);
    }

    @DexIgnore
    @Override // com.fossil.L48
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof L48) {
            L48 l48 = (L48) obj;
            if (l48.size() == size() && rangeEquals(0, l48, 0, size())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int[] getDirectory$okio() {
        return this.e;
    }

    @DexIgnore
    public final byte[][] getSegments$okio() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public int getSize$okio() {
        return getDirectory$okio()[getSegments$okio().length - 1];
    }

    @DexIgnore
    @Override // com.fossil.L48
    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio == 0) {
            int length = getSegments$okio().length;
            hashCode$okio = 1;
            int i = 0;
            int i2 = 0;
            while (i2 < length) {
                int i3 = getDirectory$okio()[length + i2];
                int i4 = getDirectory$okio()[i2];
                byte[] bArr = getSegments$okio()[i2];
                int i5 = hashCode$okio;
                for (int i6 = i3; i6 < (i4 - i) + i3; i6++) {
                    i5 = bArr[i6] + (i5 * 31);
                }
                i = i4;
                i2++;
                hashCode$okio = i5;
            }
            setHashCode$okio(hashCode$okio);
        }
        return hashCode$okio;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public String hex() {
        return a().hex();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public L48 hmac$okio(String str, L48 l48) {
        Wg6.c(str, "algorithm");
        Wg6.c(l48, "key");
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(l48.toByteArray(), str));
            int length = getSegments$okio().length;
            int i = 0;
            int i2 = 0;
            while (i2 < length) {
                int i3 = getDirectory$okio()[length + i2];
                int i4 = getDirectory$okio()[i2];
                instance.update(getSegments$okio()[i2], i3, i4 - i);
                i2++;
                i = i4;
            }
            byte[] doFinal = instance.doFinal();
            Wg6.b(doFinal, "mac.doFinal()");
            return new L48(doFinal);
        } catch (InvalidKeyException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.L48
    public int indexOf(byte[] bArr, int i) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return a().indexOf(bArr, i);
    }

    @DexIgnore
    @Override // com.fossil.L48
    public byte[] internalArray$okio() {
        return toByteArray();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public byte internalGet$okio(int i) {
        F48.b((long) getDirectory$okio()[getSegments$okio().length - 1], (long) i, 1);
        int b = G58.b(this, i);
        return getSegments$okio()[b][(i - (b == 0 ? 0 : getDirectory$okio()[b - 1])) + getDirectory$okio()[getSegments$okio().length + b]];
    }

    @DexIgnore
    @Override // com.fossil.L48
    public int lastIndexOf(byte[] bArr, int i) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return a().lastIndexOf(bArr, i);
    }

    @DexIgnore
    @Override // com.fossil.L48
    public boolean rangeEquals(int i, L48 l48, int i2, int i3) {
        Wg6.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        if (i < 0 || i > size() - i3) {
            return false;
        }
        int i4 = i3 + i;
        int b = G58.b(this, i);
        while (i < i4) {
            int i5 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i6 = getDirectory$okio()[b];
            int i7 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i4, (i6 - i5) + i5) - i;
            if (!l48.rangeEquals(i2, getSegments$okio()[b], (i - i5) + i7, min)) {
                return false;
            }
            i2 += min;
            i += min;
            b++;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        if (i < 0 || i > size() - i3 || i2 < 0 || i2 > bArr.length - i3) {
            return false;
        }
        int i4 = i3 + i;
        int b = G58.b(this, i);
        while (i < i4) {
            int i5 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i6 = getDirectory$okio()[b];
            int i7 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i4, (i6 - i5) + i5) - i;
            if (!F48.a(getSegments$okio()[b], (i - i5) + i7, bArr, i2, min)) {
                return false;
            }
            i2 += min;
            i += min;
            b++;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public String string(Charset charset) {
        Wg6.c(charset, "charset");
        return a().string(charset);
    }

    @DexIgnore
    @Override // com.fossil.L48
    public L48 substring(int i, int i2) {
        boolean z = true;
        int i3 = 0;
        if (i >= 0) {
            if (i2 <= size()) {
                int i4 = i2 - i;
                if (i4 < 0) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(("endIndex=" + i2 + " < beginIndex=" + i).toString());
                } else if (i == 0 && i2 == size()) {
                    return this;
                } else {
                    if (i == i2) {
                        return L48.EMPTY;
                    }
                    int b = G58.b(this, i);
                    int b2 = G58.b(this, i2 - 1);
                    byte[][] bArr = (byte[][]) Dm7.l(getSegments$okio(), b, b2 + 1);
                    int[] iArr = new int[(bArr.length * 2)];
                    if (b <= b2) {
                        int i5 = 0;
                        int i6 = b;
                        while (true) {
                            iArr[i5] = Math.min(getDirectory$okio()[i6] - i, i4);
                            iArr[bArr.length + i5] = getDirectory$okio()[getSegments$okio().length + i6];
                            if (i6 == b2) {
                                break;
                            }
                            i6++;
                            i5++;
                        }
                    }
                    if (b != 0) {
                        i3 = getDirectory$okio()[b - 1];
                    }
                    int length = bArr.length;
                    iArr[length] = (i - i3) + iArr[length];
                    return new Z48(bArr, iArr);
                }
            } else {
                throw new IllegalArgumentException(("endIndex=" + i2 + " > length(" + size() + ')').toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex=" + i + " < 0").toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.L48
    public L48 toAsciiLowercase() {
        return a().toAsciiLowercase();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public L48 toAsciiUppercase() {
        return a().toAsciiUppercase();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public byte[] toByteArray() {
        byte[] bArr = new byte[size()];
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            int i4 = getDirectory$okio()[length + i3];
            int i5 = getDirectory$okio()[i3];
            int i6 = i5 - i2;
            Dm7.g(getSegments$okio()[i3], bArr, i, i4, i4 + i6);
            i += i6;
            i3++;
            i2 = i5;
        }
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.L48
    public String toString() {
        return a().toString();
    }

    @DexIgnore
    @Override // com.fossil.L48
    public void write(OutputStream outputStream) throws IOException {
        Wg6.c(outputStream, "out");
        int length = getSegments$okio().length;
        int i = 0;
        int i2 = 0;
        while (i2 < length) {
            int i3 = getDirectory$okio()[length + i2];
            int i4 = getDirectory$okio()[i2];
            outputStream.write(getSegments$okio()[i2], i3, i4 - i);
            i2++;
            i = i4;
        }
    }

    @DexIgnore
    @Override // com.fossil.L48
    public void write$okio(I48 i48, int i, int i2) {
        Wg6.c(i48, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        int i3 = i2 + i;
        int b = G58.b(this, i);
        while (i < i3) {
            int i4 = b == 0 ? 0 : getDirectory$okio()[b - 1];
            int i5 = getDirectory$okio()[b];
            int i6 = getDirectory$okio()[getSegments$okio().length + b];
            int min = Math.min(i3, (i5 - i4) + i4) - i;
            int i7 = i6 + (i - i4);
            X48 x48 = new X48(getSegments$okio()[b], i7, i7 + min, true, false);
            X48 x482 = i48.b;
            if (x482 == null) {
                x48.g = x48;
                x48.f = x48;
                i48.b = x48;
            } else if (x482 != null) {
                X48 x483 = x482.g;
                if (x483 != null) {
                    x483.c(x48);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
            i += min;
            b++;
        }
        i48.o0(i48.p0() + ((long) size()));
    }
}
