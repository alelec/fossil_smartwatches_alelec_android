package com.fossil;

import android.content.Context;
import android.os.PowerManager;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.foreground.SystemForegroundService;
import com.fossil.T11;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class M11 implements K11, X21 {
    @DexIgnore
    public static /* final */ String m; // = X01.f("Processor");
    @DexIgnore
    public PowerManager.WakeLock b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public O01 d;
    @DexIgnore
    public K41 e;
    @DexIgnore
    public WorkDatabase f;
    @DexIgnore
    public Map<String, T11> g; // = new HashMap();
    @DexIgnore
    public Map<String, T11> h; // = new HashMap();
    @DexIgnore
    public List<N11> i;
    @DexIgnore
    public Set<String> j;
    @DexIgnore
    public /* final */ List<K11> k;
    @DexIgnore
    public /* final */ Object l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Runnable {
        @DexIgnore
        public K11 b;
        @DexIgnore
        public String c;
        @DexIgnore
        public G64<Boolean> d;

        @DexIgnore
        public Ai(K11 k11, String str, G64<Boolean> g64) {
            this.b = k11;
            this.c = str;
            this.d = g64;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.d.get().booleanValue();
            } catch (InterruptedException | ExecutionException e) {
                z = true;
            }
            this.b.d(this.c, z);
        }
    }

    @DexIgnore
    public M11(Context context, O01 o01, K41 k41, WorkDatabase workDatabase, List<N11> list) {
        this.c = context;
        this.d = o01;
        this.e = k41;
        this.f = workDatabase;
        this.i = list;
        this.j = new HashSet();
        this.k = new ArrayList();
        this.b = null;
        this.l = new Object();
    }

    @DexIgnore
    public static boolean c(String str, T11 t11) {
        if (t11 != null) {
            t11.d();
            X01.c().a(m, String.format("WorkerWrapper interrupted for %s", str), new Throwable[0]);
            return true;
        }
        X01.c().a(m, String.format("WorkerWrapper could not be found for %s", str), new Throwable[0]);
        return false;
    }

    @DexIgnore
    @Override // com.fossil.X21
    public void a(String str) {
        synchronized (this.l) {
            this.g.remove(str);
            l();
        }
    }

    @DexIgnore
    public void b(K11 k11) {
        synchronized (this.l) {
            this.k.add(k11);
        }
    }

    @DexIgnore
    @Override // com.fossil.K11
    public void d(String str, boolean z) {
        synchronized (this.l) {
            this.h.remove(str);
            X01.c().a(m, String.format("%s %s executed; reschedule = %s", getClass().getSimpleName(), str, Boolean.valueOf(z)), new Throwable[0]);
            for (K11 k11 : this.k) {
                k11.d(str, z);
            }
        }
    }

    @DexIgnore
    public boolean e(String str) {
        boolean contains;
        synchronized (this.l) {
            contains = this.j.contains(str);
        }
        return contains;
    }

    @DexIgnore
    public boolean f(String str) {
        boolean z;
        synchronized (this.l) {
            z = this.h.containsKey(str) || this.g.containsKey(str);
        }
        return z;
    }

    @DexIgnore
    public boolean g(String str) {
        boolean containsKey;
        synchronized (this.l) {
            containsKey = this.g.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public void h(K11 k11) {
        synchronized (this.l) {
            this.k.remove(k11);
        }
    }

    @DexIgnore
    public boolean i(String str) {
        return j(str, null);
    }

    @DexIgnore
    public boolean j(String str, WorkerParameters.a aVar) {
        synchronized (this.l) {
            if (f(str)) {
                X01.c().a(m, String.format("Work %s is already enqueued for processing", str), new Throwable[0]);
                return false;
            }
            T11.Ci ci = new T11.Ci(this.c, this.d, this.e, this, this.f, str);
            ci.c(this.i);
            ci.b(aVar);
            T11 a2 = ci.a();
            G64<Boolean> b2 = a2.b();
            b2.c(new Ai(this, str, b2), this.e.a());
            this.h.put(str, a2);
            this.e.c().execute(a2);
            X01.c().a(m, String.format("%s: processing %s", M11.class.getSimpleName(), str), new Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    public boolean k(String str) {
        boolean c2;
        boolean z = true;
        synchronized (this.l) {
            X01.c().a(m, String.format("Processor cancelling %s", str), new Throwable[0]);
            this.j.add(str);
            T11 remove = this.g.remove(str);
            if (remove == null) {
                z = false;
            }
            if (remove == null) {
                remove = this.h.remove(str);
            }
            c2 = c(str, remove);
            if (z) {
                l();
            }
        }
        return c2;
    }

    @DexIgnore
    public final void l() {
        synchronized (this.l) {
            if (!(!this.g.isEmpty())) {
                SystemForegroundService e2 = SystemForegroundService.e();
                if (e2 != null) {
                    X01.c().a(m, "No more foreground work. Stopping SystemForegroundService", new Throwable[0]);
                    e2.g();
                } else {
                    X01.c().a(m, "No more foreground work. SystemForegroundService is already stopped", new Throwable[0]);
                }
                if (this.b != null) {
                    this.b.release();
                    this.b = null;
                }
            }
        }
    }

    @DexIgnore
    public boolean m(String str) {
        boolean c2;
        synchronized (this.l) {
            X01.c().a(m, String.format("Processor stopping foreground work %s", str), new Throwable[0]);
            c2 = c(str, this.g.remove(str));
        }
        return c2;
    }

    @DexIgnore
    public boolean n(String str) {
        boolean c2;
        synchronized (this.l) {
            X01.c().a(m, String.format("Processor stopping background work %s", str), new Throwable[0]);
            c2 = c(str, this.h.remove(str));
        }
        return c2;
    }
}
