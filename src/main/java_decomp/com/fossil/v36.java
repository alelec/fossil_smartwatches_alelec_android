package com.fossil;

import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.mapped.AppWrapper;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.AppHelper;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V36 extends CoroutineUseCase<CoroutineUseCase.Bi, Ai, CoroutineUseCase.Ai> {
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public Ai(List<AppWrapper> list) {
            Wg6.c(list, "apps");
            I14.o(list, "apps cannot be null!", new Object[0]);
            Wg6.b(list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    @DexIgnore
    public V36() {
        String simpleName = V36.class.getSimpleName();
        Wg6.b(simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return this.d;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public Object k(CoroutineUseCase.Bi bi, Xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List<AppFilter> allAppFilters = Mn5.p.a().c().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.Bi> it = AppHelper.g.b().iterator();
        while (it.hasNext()) {
            AppHelper.Bi next = it.next();
            if (TextUtils.isEmpty(next.b()) || !Vt7.j(next.b(), PortfolioApp.get.instance().getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), Ao7.a(false));
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    Wg6.b(next2, "appFilter");
                    if (Wg6.a(next2.getType(), installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        Lm7.q(linkedList);
        return new Ai(linkedList);
    }
}
