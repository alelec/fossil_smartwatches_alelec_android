package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wm1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ long d; // = Hy1.b(Oq7.a);
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Wm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Wm1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new Wm1(Hy1.o(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public Wm1 b(Parcel parcel) {
            return new Wm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wm1 createFromParcel(Parcel parcel) {
            return new Wm1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wm1[] newArray(int i) {
            return new Wm1[i];
        }
    }

    @DexIgnore
    public Wm1(long j) throws IllegalArgumentException {
        super(Zm1.DAILY_STEP_GOAL);
        this.c = j;
        d();
    }

    @DexIgnore
    public /* synthetic */ Wm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readLong();
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.c).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public Long c() {
        return Long.valueOf(this.c);
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        long j = d;
        long j2 = this.c;
        if (!(0 <= j2 && j >= j2)) {
            StringBuilder e = E.e("step(");
            e.append(this.c);
            e.append(") is out of range ");
            e.append("[0, ");
            e.append(d);
            e.append("].");
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Wm1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Wm1) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
    }

    @DexIgnore
    public final long getStep() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return Long.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.c);
        }
    }
}
