package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Bo1 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Qp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qp1 createFromParcel(Parcel parcel) {
            return new Qp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qp1[] newArray(int i) {
            return new Qp1[i];
        }
    }

    @DexIgnore
    public Qp1(byte b, int i, Bo1 bo1) {
        super(E90.APP_NOTIFICATION_CONTROL, b);
        this.d = bo1;
        this.e = i;
    }

    @DexIgnore
    public /* synthetic */ Qp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(Bo1.class.getClassLoader());
        if (readParcelable != null) {
            this.d = (Bo1) readParcelable;
            this.e = parcel.readInt();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Qp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            Qp1 qp1 = (Qp1) obj;
            if (!Wg6.a(this.d, qp1.d)) {
                return false;
            }
            return this.e == qp1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.AppNotificationControlNotification");
    }

    @DexIgnore
    public final Bo1 getAction() {
        return this.d;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        int hashCode = super.hashCode();
        return (((hashCode * 31) + this.d.hashCode()) * 31) + Integer.valueOf(this.e).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.v0, this.d.toJSONObject()), Jd0.d4, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
