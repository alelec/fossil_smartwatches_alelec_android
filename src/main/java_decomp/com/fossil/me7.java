package com.fossil;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Me7 extends Qe7 {
    @DexIgnore
    public Me7(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to InternalStorage");
            Le7.b(Environment.getExternalStorageDirectory() + "/" + Se7.h("6X8Y4XdM2Vhvn0I="));
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(Environment.getExternalStorageDirectory(), Se7.h("6X8Y4XdM2Vhvn0KfzcEatGnWaNU="))));
                bufferedWriter.write(Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw==") + "," + str);
                bufferedWriter.write("\n");
                bufferedWriter.close();
            } catch (Exception e) {
                Log.w("MID", e);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final boolean c() {
        return Se7.d(this.a, "android.permission.WRITE_EXTERNAL_STORAGE") && Environment.getExternalStorageState().equals("mounted");
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final String d() {
        String str;
        synchronized (this) {
            Log.i("MID", "read mid from InternalStorage");
            try {
                Iterator<String> it = Le7.a(new File(Environment.getExternalStorageDirectory(), Se7.h("6X8Y4XdM2Vhvn0KfzcEatGnWaNU="))).iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    String[] split = it.next().split(",");
                    if (split.length == 2 && split[0].equals(Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="))) {
                        Log.i("MID", "read mid from InternalStorage:" + split[1]);
                        str = split[1];
                        break;
                    }
                }
            } catch (IOException e) {
                Log.w("MID", e);
            }
            str = null;
        }
        return str;
    }
}
