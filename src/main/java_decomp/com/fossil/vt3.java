package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vt3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Nt3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Tt3 c;

    @DexIgnore
    public Vt3(Tt3 tt3, Nt3 nt3) {
        this.c = tt3;
        this.b = nt3;
    }

    @DexIgnore
    public final void run() {
        if (this.b.o()) {
            this.c.c.v();
            return;
        }
        try {
            this.c.c.u(this.c.b.then(this.b));
        } catch (Lt3 e) {
            if (e.getCause() instanceof Exception) {
                this.c.c.t((Exception) e.getCause());
            } else {
                this.c.c.t(e);
            }
        } catch (Exception e2) {
            this.c.c.t(e2);
        }
    }
}
