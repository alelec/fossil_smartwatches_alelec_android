package com.fossil;

import com.fossil.Dl7;
import com.mapped.Lc6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayDeque;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uz7 {
    @DexIgnore
    public static /* final */ String a;

    /*
    static {
        Object r0;
        Object r02;
        try {
            Dl7.Ai ai = Dl7.Companion;
            r0 = Dl7.constructor-impl(Class.forName("com.fossil.zn7").getCanonicalName());
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            r0 = Dl7.constructor-impl(El7.a(th));
        }
        if (Dl7.exceptionOrNull-impl(r0) != null) {
            r0 = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) r0;
        try {
            Dl7.Ai ai3 = Dl7.Companion;
            r02 = Dl7.constructor-impl(Class.forName("com.fossil.uz7").getCanonicalName());
        } catch (Throwable th2) {
            Dl7.Ai ai4 = Dl7.Companion;
            r02 = Dl7.constructor-impl(El7.a(th2));
        }
        if (Dl7.exceptionOrNull-impl(r02) != null) {
            r02 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        String str = (String) r02;
    }
    */

    @DexIgnore
    public static final StackTraceElement b(String str) {
        return new StackTraceElement("\b\b\b(" + str, "\b", "\b", -1);
    }

    @DexIgnore
    public static final <E extends Throwable> Lc6<E, StackTraceElement[]> c(E e) {
        boolean z;
        Throwable cause = e.getCause();
        if (cause == null || !Wg6.a(cause.getClass(), e.getClass())) {
            return Hl7.a(e, new StackTraceElement[0]);
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (h(stackTrace[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        return z ? Hl7.a(cause, stackTrace) : Hl7.a(e, new StackTraceElement[0]);
    }

    @DexIgnore
    public static final <E extends Throwable> E d(E e, E e2, ArrayDeque<StackTraceElement> arrayDeque) {
        int i = 0;
        arrayDeque.addFirst(b("Coroutine boundary"));
        StackTraceElement[] stackTrace = e.getStackTrace();
        int g = g(stackTrace, a);
        if (g == -1) {
            Object[] array = arrayDeque.toArray(new StackTraceElement[0]);
            if (array != null) {
                e2.setStackTrace((StackTraceElement[]) array);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            StackTraceElement[] stackTraceElementArr = new StackTraceElement[(arrayDeque.size() + g)];
            for (int i2 = 0; i2 < g; i2++) {
                stackTraceElementArr[i2] = stackTrace[i2];
            }
            Iterator<T> it = arrayDeque.iterator();
            while (it.hasNext()) {
                stackTraceElementArr[g + i] = it.next();
                i++;
            }
            e2.setStackTrace(stackTraceElementArr);
        }
        return e2;
    }

    @DexIgnore
    public static final ArrayDeque<StackTraceElement> e(Do7 do7) {
        ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<>();
        StackTraceElement stackTraceElement = do7.getStackTraceElement();
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
        }
        while (true) {
            if (!(do7 instanceof Do7)) {
                do7 = null;
            }
            if (do7 == null || (do7 = do7.getCallerFrame()) == null) {
                return arrayDeque;
            }
            StackTraceElement stackTraceElement2 = do7.getStackTraceElement();
            if (stackTraceElement2 != null) {
                arrayDeque.add(stackTraceElement2);
            }
        }
        return arrayDeque;
    }

    @DexIgnore
    public static final boolean f(StackTraceElement stackTraceElement, StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && Wg6.a(stackTraceElement.getMethodName(), stackTraceElement2.getMethodName()) && Wg6.a(stackTraceElement.getFileName(), stackTraceElement2.getFileName()) && Wg6.a(stackTraceElement.getClassName(), stackTraceElement2.getClassName());
    }

    @DexIgnore
    public static final int g(StackTraceElement[] stackTraceElementArr, String str) {
        int length = stackTraceElementArr.length;
        for (int i = 0; i < length; i++) {
            if (Wg6.a(str, stackTraceElementArr[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final boolean h(StackTraceElement stackTraceElement) {
        return Vt7.s(stackTraceElement.getClassName(), "\b\b\b", false, 2, null);
    }

    @DexIgnore
    public static final void i(StackTraceElement[] stackTraceElementArr, ArrayDeque<StackTraceElement> arrayDeque) {
        int length = stackTraceElementArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                i = -1;
                break;
            } else if (h(stackTraceElementArr[i])) {
                break;
            } else {
                i++;
            }
        }
        int i2 = i + 1;
        int length2 = stackTraceElementArr.length - 1;
        if (length2 >= i2) {
            while (true) {
                if (f(stackTraceElementArr[length2], arrayDeque.getLast())) {
                    arrayDeque.removeLast();
                }
                arrayDeque.addFirst(stackTraceElementArr[length2]);
                if (length2 != i2) {
                    length2--;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public static final <E extends Throwable> E j(E e, Do7 do7) {
        Lc6 c = c(e);
        Throwable th = (Throwable) c.component1();
        StackTraceElement[] stackTraceElementArr = (StackTraceElement[]) c.component2();
        E e2 = (E) Fz7.e(th);
        if (e2 == null || (!Wg6.a(e2.getMessage(), th.getMessage()))) {
            return e;
        }
        ArrayDeque<StackTraceElement> e3 = e(do7);
        if (e3.isEmpty()) {
            return e;
        }
        if (th != e) {
            i(stackTraceElementArr, e3);
        }
        d(th, e2, e3);
        return e2;
    }

    @DexIgnore
    public static final <E extends Throwable> E k(E e) {
        E e2 = (E) e.getCause();
        if (e2 == null) {
            return e;
        }
        boolean z = true;
        if (!Wg6.a(e2.getClass(), e.getClass())) {
            return e;
        }
        StackTraceElement[] stackTrace = e.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (h(stackTrace[i])) {
                break;
            } else {
                i++;
            }
        }
        return z ? e2 : e;
    }
}
