package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class If7 extends Ef7 {
    @DexIgnore
    public If7(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Ef7
    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.getString("_wxapi_join_chatroom_ext_msg");
    }

    @DexIgnore
    @Override // com.fossil.Ef7
    public int b() {
        return 15;
    }
}
