package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mz4 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore
    public Mz4() {
        Zi4 zi4 = new Zi4();
        zi4.f(Date.class, new GsonConverterShortDate());
        Gson d = zi4.d();
        Wg6.b(d, "GsonBuilder().registerTy\u2026rterShortDate()).create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(ActivityStatistic.ActivityDailyBest activityDailyBest) {
        if (activityDailyBest == null) {
            return null;
        }
        try {
            return this.a.t(activityDailyBest);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String b(ActivityStatistic.CaloriesBestDay caloriesBestDay) {
        if (caloriesBestDay == null) {
            return null;
        }
        try {
            return this.a.t(caloriesBestDay);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ActivityStatistic.ActivityDailyBest c(String str) {
        ActivityStatistic.ActivityDailyBest activityDailyBest;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            activityDailyBest = (ActivityStatistic.ActivityDailyBest) this.a.k(str, ActivityStatistic.ActivityDailyBest.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d("ActivityStatisticConverter", sb.toString());
            activityDailyBest = null;
        }
        return activityDailyBest;
    }

    @DexIgnore
    public final ActivityStatistic.CaloriesBestDay d(String str) {
        ActivityStatistic.CaloriesBestDay caloriesBestDay;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            caloriesBestDay = (ActivityStatistic.CaloriesBestDay) this.a.k(str, ActivityStatistic.CaloriesBestDay.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(Cd6.a);
            local.d("ActivityStatisticConverter", sb.toString());
            caloriesBestDay = null;
        }
        return caloriesBestDay;
    }
}
