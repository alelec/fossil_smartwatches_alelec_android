package com.fossil;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.R62;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ip2<T extends IInterface> extends Ec2<T> {
    @DexIgnore
    public Ip2(Context context, Looper looper, Ep2 ep2, R62.Bi bi, R62.Ci ci, Ac2 ac2) {
        super(context, looper, ep2.zzc(), ac2, bi, ci);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public boolean Q() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2
    public Set<Scope> h() {
        return H();
    }

    @DexIgnore
    @Override // com.fossil.Ec2
    public Set<Scope> p0(Set<Scope> set) {
        return Lj2.a(set);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public boolean v() {
        return !If2.b(E());
    }
}
