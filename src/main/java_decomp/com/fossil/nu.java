package com.fossil;

import com.mapped.HandMovingConfig;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nu {
    @DexIgnore
    public /* synthetic */ Nu(Qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(HandMovingConfig[] handMovingConfigArr) {
        ByteBuffer allocate = ByteBuffer.allocate(handMovingConfigArr.length * 5);
        for (HandMovingConfig handMovingConfig : handMovingConfigArr) {
            allocate.put(handMovingConfig.a());
        }
        byte[] array = allocate.array();
        Wg6.b(array, "array.array()");
        return array;
    }
}
