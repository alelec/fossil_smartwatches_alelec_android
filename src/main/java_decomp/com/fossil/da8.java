package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Da8 {
    @DexIgnore
    public static /* final */ Ai a; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(PluginRegistry.Registrar registrar) {
            Wg6.c(registrar, "registrar");
            new MethodChannel(registrar.messenger(), "top.kikt/photo_manager").setMethodCallHandler(new Ga8(registrar));
        }
    }

    /*
    static {
        new Ta8(null, 1, null);
    }
    */

    @DexIgnore
    public static final void a(PluginRegistry.Registrar registrar) {
        a.a(registrar);
    }
}
