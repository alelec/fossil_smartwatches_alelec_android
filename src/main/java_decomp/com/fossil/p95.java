package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class P95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TabLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleButton C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ DashBar u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RulerValuePicker w;
    @DexIgnore
    public /* final */ RulerValuePicker x;
    @DexIgnore
    public /* final */ ScrollView y;
    @DexIgnore
    public /* final */ TabLayout z;

    @DexIgnore
    public P95(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageView imageView, DashBar dashBar, ConstraintLayout constraintLayout, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ScrollView scrollView, TabLayout tabLayout, TabLayout tabLayout2, FlexibleTextView flexibleTextView3, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = imageView;
        this.u = dashBar;
        this.v = constraintLayout;
        this.w = rulerValuePicker;
        this.x = rulerValuePicker2;
        this.y = scrollView;
        this.z = tabLayout;
        this.A = tabLayout2;
        this.B = flexibleTextView3;
        this.C = flexibleButton2;
        this.D = flexibleTextView4;
    }
}
