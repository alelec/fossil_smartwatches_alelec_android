package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru4 extends ts0 {
    @DexIgnore
    public static /* final */ String c;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<Boolean> f3166a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<vs4>> b; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroViewModel$fetchTemplates$1", f = "BCCreateChallengeIntroViewModel.kt", l = {35}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ru4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ru4$a$a")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroViewModel$fetchTemplates$1$templates$1", f = "BCCreateChallengeIntroViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.ru4$a$a  reason: collision with other inner class name */
        public static final class C0209a extends ko7 implements vp7<iv7, qn7<? super List<vs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public C0209a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0209a aVar = new C0209a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<vs4>> qn7) {
                throw null;
                //return ((C0209a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return qy4.f3046a.a();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ru4 ru4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ru4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FLogger.INSTANCE.getLocal().d(ru4.c, "fetchTemplates");
                this.this$0.f3166a.l(ao7.a(true));
                dv7 b = bw7.b();
                C0209a aVar = new C0209a(null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(b, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f3166a.l(ao7.a(false));
            this.this$0.b.l((List) g);
            return tl7.f3441a;
        }
    }

    /*
    static {
        String simpleName = ey4.class.getSimpleName();
        pq7.b(simpleName, "BCFriendTabViewModel::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final void d() {
        xw7 unused = gu7.d(us0.a(this), null, null, new a(this, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.f3166a;
    }

    @DexIgnore
    public final LiveData<List<vs4>> f() {
        return this.b;
    }
}
