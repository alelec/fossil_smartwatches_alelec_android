package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Et5 implements Factory<HybridSyncUseCase> {
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> a;
    @DexIgnore
    public /* final */ Provider<An4> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> f;
    @DexIgnore
    public /* final */ Provider<AnalyticsHelper> g;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> h;

    @DexIgnore
    public Et5(Provider<MicroAppRepository> provider, Provider<An4> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<AnalyticsHelper> provider7, Provider<AlarmsRepository> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static Et5 a(Provider<MicroAppRepository> provider, Provider<An4> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<AnalyticsHelper> provider7, Provider<AlarmsRepository> provider8) {
        return new Et5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static HybridSyncUseCase c(MicroAppRepository microAppRepository, An4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, HybridPresetRepository hybridPresetRepository, NotificationsRepository notificationsRepository, AnalyticsHelper analyticsHelper, AlarmsRepository alarmsRepository) {
        return new HybridSyncUseCase(microAppRepository, an4, deviceRepository, portfolioApp, hybridPresetRepository, notificationsRepository, analyticsHelper, alarmsRepository);
    }

    @DexIgnore
    public HybridSyncUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
