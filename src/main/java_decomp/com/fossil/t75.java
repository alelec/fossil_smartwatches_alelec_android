package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.FlexibleTabLayout;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ViewPager2 t;
    @DexIgnore
    public /* final */ FlexibleTabLayout u;

    @DexIgnore
    public T75(Object obj, View view, int i, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout, ViewPager2 viewPager2, FlexibleTabLayout flexibleTabLayout) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = rTLImageView2;
        this.s = constraintLayout;
        this.t = viewPager2;
        this.u = flexibleTabLayout;
    }
}
