package com.fossil;

import android.view.View;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fz4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $onSafeClick;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Hg6 hg6) {
            super(1);
            this.$onSafeClick = hg6;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.$onSafeClick.invoke(view);
        }
    }

    @DexIgnore
    public static final void a(View view, Hg6<? super View, Cd6> hg6) {
        Wg6.c(view, "$this$setOnSafeClickListener");
        Wg6.c(hg6, "onSafeClick");
        view.setOnClickListener(new Ez4(0, new Ai(hg6), 1, null));
    }
}
