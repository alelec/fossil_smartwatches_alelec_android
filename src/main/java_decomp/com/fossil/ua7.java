package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ua7 extends Qv5 {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public Vg5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ua7 a() {
            return new Ua7();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ua7 b;

        @DexIgnore
        public Bi(Ua7 ua7) {
            this.b = ua7;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Vg5 c = Vg5.c(layoutInflater, viewGroup, false);
        this.h = c;
        if (c != null) {
            return c.b();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Vg5 vg5 = this.h;
        if (vg5 != null) {
            vg5.b.setOnClickListener(new Bi(this));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
