package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tx5 implements Factory<Ox5> {
    @DexIgnore
    public static Ox5 a(Qx5 qx5) {
        Ox5 d = qx5.d();
        Lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
