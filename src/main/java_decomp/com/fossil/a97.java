package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;
import java.util.LinkedHashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A97 {
    @DexIgnore
    public static /* final */ Map<String, Bitmap> a; // = new LinkedHashMap();
    @DexIgnore
    public static /* final */ A97 b; // = new A97();

    @DexIgnore
    public final void a(String str, Bitmap bitmap) {
        Wg6.c(str, "id");
        Wg6.c(bitmap, "bitmap");
        a.put(str, bitmap);
    }

    @DexIgnore
    public final void b() {
        a.clear();
    }

    @DexIgnore
    public final Bitmap c(String str) {
        Wg6.c(str, "id");
        return a.get(str);
    }

    @DexIgnore
    public final boolean d() {
        return !a.isEmpty();
    }
}
