package com.fossil;

import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.uirenew.welcome.WelcomePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D27 implements MembersInjector<WelcomeActivity> {
    @DexIgnore
    public static void a(WelcomeActivity welcomeActivity, WelcomePresenter welcomePresenter) {
        welcomeActivity.A = welcomePresenter;
    }
}
