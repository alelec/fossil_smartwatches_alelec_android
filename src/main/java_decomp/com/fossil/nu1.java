package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ix1;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nu1 extends Ox1 implements Parcelable, Serializable {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public Nu1(String str, byte[] bArr) {
        this.c = bArr;
        this.d = Ix1.a.b(bArr, Ix1.Ai.CRC32);
        this.b = str.length() == 0 ? Hy1.k((int) this.d, null, 1, null) : str;
    }

    @DexIgnore
    public final byte[] a() {
        if (!(!(this.c.length == 0))) {
            return new byte[0];
        }
        String str = this.b;
        Charset defaultCharset = Charset.defaultCharset();
        Wg6.b(defaultCharset, "Charset.defaultCharset()");
        if (str != null) {
            byte[] bytes = str.getBytes(defaultCharset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] p = Dm7.p(bytes, (byte) 0);
            int length = this.c.length;
            ByteBuffer allocate = ByteBuffer.allocate(p.length + 2 + length);
            Wg6.b(allocate, "ByteBuffer.allocate(totalLen)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putShort((short) (length + p.length));
            allocate.put(p);
            allocate.put(this.c);
            byte[] array = allocate.array();
            Wg6.b(array, "result.array()");
            return array;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final long b() {
        return this.d;
    }

    @DexIgnore
    public final Object c() {
        Object obj = d() ? JSONObject.NULL : this.b;
        Wg6.b(obj, "if (isEmptyFile()) {\n   \u2026   fileName\n            }");
        return obj;
    }

    @DexIgnore
    public final boolean d() {
        return this.c.length == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Rl1 rl1 = (Rl1) obj;
            return Wg6.a(this.b, rl1.getFileName()) && Arrays.equals(this.c, rl1.getFileData());
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImage");
    }

    @DexIgnore
    public final byte[] getFileData() {
        return this.c;
    }

    @DexIgnore
    public final String getFileName() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + Arrays.hashCode(this.c);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.H, this.b);
            G80.k(jSONObject, Jd0.I, Integer.valueOf(this.c.length));
            G80.k(jSONObject, Jd0.J, Long.valueOf(this.d));
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.c);
        }
    }
}
