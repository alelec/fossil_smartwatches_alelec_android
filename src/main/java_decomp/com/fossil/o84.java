package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.zendesk.sdk.support.help.HelpRecyclerViewAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O84 {
    @DexIgnore
    public /* final */ Float a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public O84(Float f, boolean z) {
        this.b = z;
        this.a = f;
    }

    @DexIgnore
    public static O84 a(Context context) {
        boolean z;
        Float f = null;
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            z = e(registerReceiver);
            f = d(registerReceiver);
        } else {
            z = false;
        }
        return new O84(f, z);
    }

    @DexIgnore
    public static Float d(Intent intent) {
        int intExtra = intent.getIntExtra(HelpRecyclerViewAdapter.CategoryViewHolder.ROTATION_PROPERTY_NAME, -1);
        int intExtra2 = intent.getIntExtra("scale", -1);
        if (intExtra == -1 || intExtra2 == -1) {
            return null;
        }
        return Float.valueOf(((float) intExtra) / ((float) intExtra2));
    }

    @DexIgnore
    public static boolean e(Intent intent) {
        int intExtra = intent.getIntExtra("status", -1);
        if (intExtra == -1) {
            return false;
        }
        return intExtra == 2 || intExtra == 5;
    }

    @DexIgnore
    public Float b() {
        return this.a;
    }

    @DexIgnore
    public int c() {
        Float f;
        if (!this.b || (f = this.a) == null) {
            return 1;
        }
        return ((double) f.floatValue()) < 0.99d ? 2 : 3;
    }
}
