package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ji1 {
    @DexIgnore
    public /* final */ Set<Bj1> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<Bj1> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(Bj1 bj1) {
        boolean z = true;
        if (bj1 != null) {
            boolean remove = this.a.remove(bj1);
            if (!this.b.remove(bj1) && !remove) {
                z = false;
            }
            if (z) {
                bj1.clear();
            }
        }
        return z;
    }

    @DexIgnore
    public void b() {
        for (Bj1 bj1 : Jk1.j(this.a)) {
            a(bj1);
        }
        this.b.clear();
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (Bj1 bj1 : Jk1.j(this.a)) {
            if (bj1.isRunning() || bj1.j()) {
                bj1.clear();
                this.b.add(bj1);
            }
        }
    }

    @DexIgnore
    public void d() {
        this.c = true;
        for (Bj1 bj1 : Jk1.j(this.a)) {
            if (bj1.isRunning()) {
                bj1.pause();
                this.b.add(bj1);
            }
        }
    }

    @DexIgnore
    public void e() {
        for (Bj1 bj1 : Jk1.j(this.a)) {
            if (!bj1.j() && !bj1.h()) {
                bj1.clear();
                if (!this.c) {
                    bj1.f();
                } else {
                    this.b.add(bj1);
                }
            }
        }
    }

    @DexIgnore
    public void f() {
        this.c = false;
        for (Bj1 bj1 : Jk1.j(this.a)) {
            if (!bj1.j() && !bj1.isRunning()) {
                bj1.f();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public void g(Bj1 bj1) {
        this.a.add(bj1);
        if (!this.c) {
            bj1.f();
            return;
        }
        bj1.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(bj1);
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }
}
