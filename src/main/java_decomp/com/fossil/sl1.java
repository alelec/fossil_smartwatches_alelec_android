package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sl1 extends Hv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ ArrayList<Rl1> b; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Sl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final boolean a(Rl1 rl1, Rl1 rl12) {
            return Wg6.a(rl1.getFileName(), rl12.getFileName()) && !Arrays.equals(rl1.getFileData(), rl12.getFileData());
        }

        @DexIgnore
        public Sl1 b(Parcel parcel) {
            return new Sl1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sl1 createFromParcel(Parcel parcel) {
            return new Sl1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Sl1[] newArray(int i) {
            return new Sl1[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<Rl1, Boolean> {
        @DexIgnore
        public static /* final */ Bi b; // = new Bi();

        @DexIgnore
        public Bi() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public Boolean invoke(Rl1 rl1) {
            return Boolean.valueOf(rl1.d());
        }
    }

    @DexIgnore
    public /* synthetic */ Sl1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        ArrayList createTypedArrayList = parcel.createTypedArrayList(Rl1.CREATOR);
        if (createTypedArrayList != null) {
            this.b.addAll(createTypedArrayList);
        }
    }

    @DexIgnore
    public Sl1(Rl1 rl1, Rl1 rl12, Rl1 rl13, Rl1 rl14, Rl1 rl15) throws IllegalArgumentException {
        rl1.a(new Vs1(0, 0, 0));
        rl12.a(new Vs1(0, 62, 1));
        rl13.a(new Vs1(90, 62, 1));
        rl14.a(new Vs1(180, 62, 1));
        rl15.a(new Vs1(270, 62, 1));
        this.b.addAll(Hm7.c(rl1, rl12, rl13, rl14, rl15));
        d();
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        Iterator<T> it = this.b.iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().e());
        }
        JSONObject put = new JSONObject().put("watchFace._.config.backgrounds", jSONArray);
        Wg6.b(put, "JSONObject().put(UIScrip\u2026oundsAssignmentJsonArray)");
        return put;
    }

    @DexIgnore
    public final ArrayList<Rl1> c() {
        return this.b;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        int size = this.b.size();
        for (int i = 0; i < size - 1; i++) {
            int size2 = this.b.size();
            for (int i2 = i; i2 < size2; i2++) {
                Ai ai = CREATOR;
                Rl1 rl1 = this.b.get(i);
                Wg6.b(rl1, "backgroundImageList[i]");
                Rl1 rl12 = this.b.get(i2);
                Wg6.b(rl12, "backgroundImageList[j]");
                if (ai.a(rl1, rl12)) {
                    StringBuilder e = E.e("Background images ");
                    e.append(this.b.get(i).getFileName());
                    e.append(" and  ");
                    e.append(this.b.get(i2).getFileName());
                    e.append(' ');
                    e.append("have same names but different data.");
                    throw new IllegalArgumentException(e.toString());
                }
            }
        }
        Mm7.w(this.b, Bi.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Sl1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.b, ((Sl1) obj).b) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImageConfig");
    }

    @DexIgnore
    public final Rl1 getBottomBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 180 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Rl1 getLeftBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 270 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Rl1 getMainBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 0 && t2.getPositionConfig().getZIndex() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Rl1 getRightBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 90 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final Rl1 getTopBackground() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            T t2 = next;
            if (t2.getPositionConfig().getAngle() == 0 && t2.getPositionConfig().getDistanceFromCenter() == 62 && t2.getPositionConfig().getZIndex() == 1) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            Jd0 jd0 = Jd0.G;
            Object[] array = this.b.toArray(new Rl1[0]);
            if (array != null) {
                G80.k(jSONObject, jd0, Px1.a((Ox1[]) array));
                return jSONObject;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        } catch (JSONException e) {
            D90.i.i(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedList(this.b);
        }
    }
}
