package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class GpsLogFileManager {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends GpsLogFileManager {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native GpsLogFileManager defaultManager();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native void native_append(long j, GpsDataPoint gpsDataPoint);

        @DexIgnore
        private native ArrayList<String> native_list(long j);

        @DexIgnore
        public static native ArrayList<GpsDataPoint> parse(byte[] bArr);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        @Override // com.fossil.fitness.GpsLogFileManager
        public void append(GpsDataPoint gpsDataPoint) {
            native_append(this.nativeRef, gpsDataPoint);
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.fitness.GpsLogFileManager
        public ArrayList<String> list() {
            return native_list(this.nativeRef);
        }
    }

    @DexIgnore
    public static GpsLogFileManager defaultManager() {
        return CppProxy.defaultManager();
    }

    @DexIgnore
    public static ArrayList<GpsDataPoint> parse(byte[] bArr) {
        return CppProxy.parse(bArr);
    }

    @DexIgnore
    public abstract void append(GpsDataPoint gpsDataPoint);

    @DexIgnore
    public abstract ArrayList<String> list();
}
