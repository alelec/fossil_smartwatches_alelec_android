package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum WorkoutMode {
    UNKNOWN(0),
    INDOOR(1),
    OUTDOOR(2);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public WorkoutMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
