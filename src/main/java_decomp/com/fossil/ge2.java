package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.fossil.Gc2;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ge2 implements ServiceConnection {
    @DexIgnore
    public /* final */ Map<ServiceConnection, ServiceConnection> a; // = new HashMap();
    @DexIgnore
    public int b; // = 2;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public IBinder d;
    @DexIgnore
    public /* final */ Gc2.Ai e;
    @DexIgnore
    public ComponentName f;
    @DexIgnore
    public /* final */ /* synthetic */ He2 g;

    @DexIgnore
    public Ge2(He2 he2, Gc2.Ai ai) {
        this.g = he2;
        this.e = ai;
    }

    @DexIgnore
    public final IBinder a() {
        return this.d;
    }

    @DexIgnore
    public final ComponentName b() {
        return this.f;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }

    @DexIgnore
    public final void e(ServiceConnection serviceConnection, ServiceConnection serviceConnection2, String str) {
        Ve2 unused = this.g.g;
        Context unused2 = this.g.e;
        this.e.c(this.g.e);
        this.a.put(serviceConnection, serviceConnection2);
    }

    @DexIgnore
    public final void f(ServiceConnection serviceConnection, String str) {
        Ve2 unused = this.g.g;
        Context unused2 = this.g.e;
        this.a.remove(serviceConnection);
    }

    @DexIgnore
    public final boolean g(ServiceConnection serviceConnection) {
        return this.a.containsKey(serviceConnection);
    }

    @DexIgnore
    public final void h(String str) {
        this.b = 3;
        boolean d2 = this.g.g.d(this.g.e, str, this.e.c(this.g.e), this, this.e.e());
        this.c = d2;
        if (d2) {
            this.g.f.sendMessageDelayed(this.g.f.obtainMessage(1, this.e), this.g.i);
            return;
        }
        this.b = 2;
        try {
            this.g.g.c(this.g.e, this);
        } catch (IllegalArgumentException e2) {
        }
    }

    @DexIgnore
    public final void i(String str) {
        this.g.f.removeMessages(1, this.e);
        this.g.g.c(this.g.e, this);
        this.c = false;
        this.b = 2;
    }

    @DexIgnore
    public final boolean j() {
        return this.a.isEmpty();
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.d) {
            this.g.f.removeMessages(1, this.e);
            this.d = iBinder;
            this.f = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceConnected(componentName, iBinder);
            }
            this.b = 1;
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.d) {
            this.g.f.removeMessages(1, this.e);
            this.d = null;
            this.f = componentName;
            for (ServiceConnection serviceConnection : this.a.values()) {
                serviceConnection.onServiceDisconnected(componentName);
            }
            this.b = 2;
        }
    }
}
