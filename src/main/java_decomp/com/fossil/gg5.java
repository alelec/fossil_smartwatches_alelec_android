package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ Guideline u;
    @DexIgnore
    public /* final */ Guideline v;
    @DexIgnore
    public /* final */ Guideline w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    public Gg5(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, Guideline guideline, Guideline guideline2, Guideline guideline3, RTLImageView rTLImageView, FlexibleSwitchCompat flexibleSwitchCompat, View view2) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = guideline;
        this.v = guideline2;
        this.w = guideline3;
        this.x = rTLImageView;
        this.y = flexibleSwitchCompat;
        this.z = view2;
    }

    @DexIgnore
    @Deprecated
    public static Gg5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2, Object obj) {
        return (Gg5) ViewDataBinding.p(layoutInflater, 2131558729, viewGroup, z2, obj);
    }

    @DexIgnore
    public static Gg5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2) {
        return A(layoutInflater, viewGroup, z2, Aq0.d());
    }
}
