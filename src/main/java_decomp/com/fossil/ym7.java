package com.fossil;

import com.mapped.Lc6;
import com.mapped.Wg6;
import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ym7 extends Xm7 {
    @DexIgnore
    public static final int b(int i) {
        if (i < 0) {
            return i;
        }
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> c(Lc6<? extends K, ? extends V> lc6) {
        Wg6.c(lc6, "pair");
        Map<K, V> singletonMap = Collections.singletonMap(lc6.getFirst(), lc6.getSecond());
        Wg6.b(singletonMap, "java.util.Collections.si\u2026(pair.first, pair.second)");
        return singletonMap;
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> d(Lc6<? extends K, ? extends V>... lc6Arr) {
        Wg6.c(lc6Arr, "pairs");
        TreeMap treeMap = new TreeMap();
        Zm7.m(treeMap, lc6Arr);
        return treeMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> e(Map<? extends K, ? extends V> map) {
        Wg6.c(map, "$this$toSingletonMap");
        Map.Entry<? extends K, ? extends V> next = map.entrySet().iterator().next();
        Map<K, V> singletonMap = Collections.singletonMap(next.getKey(), next.getValue());
        Wg6.b(singletonMap, "java.util.Collections.singletonMap(key, value)");
        Wg6.b(singletonMap, "with(entries.iterator().\u2026ingletonMap(key, value) }");
        return singletonMap;
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> f(Map<? extends K, ? extends V> map) {
        Wg6.c(map, "$this$toSortedMap");
        return new TreeMap(map);
    }
}
