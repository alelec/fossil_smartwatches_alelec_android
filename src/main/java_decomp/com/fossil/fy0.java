package com.fossil;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fy0 implements Dy0 {
    @DexIgnore
    public static Class<?> c;
    @DexIgnore
    public static boolean d;
    @DexIgnore
    public static Method e;
    @DexIgnore
    public static boolean f;
    @DexIgnore
    public static Method g;
    @DexIgnore
    public static boolean h;
    @DexIgnore
    public /* final */ View b;

    @DexIgnore
    public Fy0(View view) {
        this.b = view;
    }

    @DexIgnore
    public static Dy0 b(View view, ViewGroup viewGroup, Matrix matrix) {
        c();
        Method method = e;
        if (method != null) {
            try {
                return new Fy0((View) method.invoke(null, view, viewGroup, matrix));
            } catch (IllegalAccessException e2) {
            } catch (InvocationTargetException e3) {
                throw new RuntimeException(e3.getCause());
            }
        }
        return null;
    }

    @DexIgnore
    public static void c() {
        if (!f) {
            try {
                d();
                Method declaredMethod = c.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class);
                e = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve addGhost method", e2);
            }
            f = true;
        }
    }

    @DexIgnore
    public static void d() {
        if (!d) {
            try {
                c = Class.forName("android.view.GhostView");
            } catch (ClassNotFoundException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve GhostView class", e2);
            }
            d = true;
        }
    }

    @DexIgnore
    public static void e() {
        if (!h) {
            try {
                d();
                Method declaredMethod = c.getDeclaredMethod("removeGhost", View.class);
                g = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", e2);
            }
            h = true;
        }
    }

    @DexIgnore
    public static void f(View view) {
        e();
        Method method = g;
        if (method != null) {
            try {
                method.invoke(null, view);
            } catch (IllegalAccessException e2) {
            } catch (InvocationTargetException e3) {
                throw new RuntimeException(e3.getCause());
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Dy0
    public void a(ViewGroup viewGroup, View view) {
    }

    @DexIgnore
    @Override // com.fossil.Dy0
    public void setVisibility(int i) {
        this.b.setVisibility(i);
    }
}
