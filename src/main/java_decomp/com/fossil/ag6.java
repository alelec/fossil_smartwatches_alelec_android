package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ag6 extends pv5 implements zf6, zw5, aw5 {
    @DexIgnore
    public g37<n55> g;
    @DexIgnore
    public yf6 h;
    @DexIgnore
    public vw5 i;
    @DexIgnore
    public CaloriesOverviewFragment j;
    @DexIgnore
    public f67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends f67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ ag6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, ag6 ag6, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = ag6;
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void b(int i) {
            ag6.K6(this.f).p();
        }

        @DexIgnore
        @Override // com.fossil.f67
        public void c(int i, int i2) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ yf6 K6(ag6 ag6) {
        yf6 yf6 = ag6.h;
        if (yf6 != null) {
            return yf6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "DashboardCaloriesFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final n55 L6() {
        g37<n55> g37 = this.g;
        if (g37 != null) {
            return g37.a();
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: M6 */
    public void M5(yf6 yf6) {
        pq7.c(yf6, "presenter");
        this.h = yf6;
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void Q(Date date) {
        pq7.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.C;
            pq7.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z) {
        n55 L6;
        RecyclerView recyclerView;
        View view;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardCaloriesFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(C6());
        sb.append(", isRunning=");
        vl5 C6 = C6();
        sb.append(C6 != null ? Boolean.valueOf(C6.f()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            vl5 C62 = C6();
            if (C62 != null) {
                C62.i();
            }
            if (isVisible() && this.g != null && (L6 = L6()) != null && (recyclerView = L6.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    recyclerView.smoothScrollToPosition(0);
                    f67 f67 = this.k;
                    if (f67 != null) {
                        f67.d();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        vl5 C63 = C6();
        if (C63 != null) {
            C63.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void d() {
        f67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        g37<n55> g37 = new g37<>(this, (n55) aq0.f(layoutInflater, 2131558544, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            n55 a2 = g37.a();
            if (a2 != null) {
                pq7.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.o();
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        CaloriesOverviewFragment caloriesOverviewFragment = (CaloriesOverviewFragment) getChildFragmentManager().Z("CaloriesOverviewFragment");
        this.j = caloriesOverviewFragment;
        if (caloriesOverviewFragment == null) {
            this.j = new CaloriesOverviewFragment();
        }
        sw5 sw5 = new sw5();
        PortfolioApp c = PortfolioApp.h0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        CaloriesOverviewFragment caloriesOverviewFragment2 = this.j;
        if (caloriesOverviewFragment2 != null) {
            this.i = new vw5(sw5, c, this, childFragmentManager, caloriesOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            n55 L6 = L6();
            if (!(L6 == null || (recyclerView2 = L6.q) == null)) {
                pq7.b(recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                vw5 vw5 = this.i;
                if (vw5 != null) {
                    recyclerView2.setAdapter(vw5);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        a aVar = new a(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.k = aVar;
                        if (aVar != null) {
                            recyclerView2.addOnScrollListener(aVar);
                            recyclerView2.setItemViewCacheSize(0);
                            bd6 bd6 = new bd6(linearLayoutManager.q2());
                            Drawable f = gl0.f(recyclerView2.getContext(), 2131230856);
                            if (f != null) {
                                pq7.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                bd6.h(f);
                                recyclerView2.addItemDecoration(bd6);
                                yf6 yf6 = this.h;
                                if (yf6 != null) {
                                    yf6.n();
                                } else {
                                    pq7.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    pq7.n("mDashboardCaloriesAdapter");
                    throw null;
                }
            }
            n55 L62 = L6();
            if (!(L62 == null || (recyclerView = L62.q) == null)) {
                pq7.b(recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof pv0) {
                    ((pv0) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            E6("calories_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ts0 a2 = vs0.e(activity).a(y67.class);
                pq7.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                y67 y67 = (y67) a2;
                return;
            }
            return;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zf6
    public void q(cu0<ActivitySummary> cu0) {
        vw5 vw5 = this.i;
        if (vw5 != null) {
            vw5.x(cu0);
        } else {
            pq7.n("mDashboardCaloriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zw5
    public void q0(Date date, Date date2) {
        pq7.c(date, "startWeekDate");
        pq7.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
