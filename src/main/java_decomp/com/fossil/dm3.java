package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dm3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ /* synthetic */ Xl3 d;

    @DexIgnore
    public Dm3(Xl3 xl3, String str, String str2) {
        this.d = xl3;
        Rc2.g(str);
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.B().getString(this.a, null);
        }
        return this.c;
    }

    @DexIgnore
    public final void b(String str) {
        if (this.d.m().s(Xg3.x0) || !Kr3.z0(str, this.c)) {
            SharedPreferences.Editor edit = this.d.B().edit();
            edit.putString(this.a, str);
            edit.apply();
            this.c = str;
        }
    }
}
