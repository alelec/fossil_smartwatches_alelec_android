package com.fossil;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Am5 {
    @DexIgnore
    public static /* final */ int[] a; // = {0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 106, 2026, 1000, 1015};
    @DexIgnore
    public static /* final */ String[] b; // = {G78.ANY_MARKER, "us-ascii", "iso-8859-1", "iso-8859-2", "iso-8859-3", "iso-8859-4", "iso-8859-5", "iso-8859-6", "iso-8859-7", "iso-8859-8", "iso-8859-9", "shift_JIS", Ia1.PROTOCOL_CHARSET, "big5", "iso-10646-ucs-2", "utf-16"};
    @DexIgnore
    public static /* final */ HashMap<Integer, String> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Integer> d; // = new HashMap<>();

    /*
    static {
        int length = a.length;
        for (int i = 0; i <= length - 1; i++) {
            c.put(Integer.valueOf(a[i]), b[i]);
            d.put(b[i], Integer.valueOf(a[i]));
        }
    }
    */

    @DexIgnore
    public static int a(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return -1;
        }
        Integer num = d.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new UnsupportedEncodingException();
    }

    @DexIgnore
    public static String b(int i) throws UnsupportedEncodingException {
        String str = c.get(Integer.valueOf(i));
        if (str != null) {
            return str;
        }
        throw new UnsupportedEncodingException();
    }
}
