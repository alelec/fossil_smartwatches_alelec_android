package com.fossil;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L42 implements Z62 {
    @DexIgnore
    public Status b;
    @DexIgnore
    public GoogleSignInAccount c;

    @DexIgnore
    public L42(GoogleSignInAccount googleSignInAccount, Status status) {
        this.c = googleSignInAccount;
        this.b = status;
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public Status a() {
        return this.b;
    }

    @DexIgnore
    public GoogleSignInAccount b() {
        return this.c;
    }

    @DexIgnore
    public boolean c() {
        return this.b.D();
    }
}
