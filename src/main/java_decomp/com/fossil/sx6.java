package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.X37;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sx6 extends Qv5 implements Yx6 {
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<Z95> h;
    @DexIgnore
    public Xx6 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Sx6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            Sx6 sx6 = new Sx6();
            sx6.setArguments(bundle);
            return sx6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sx6 b;

        @DexIgnore
        public Bi(Sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Sx6.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sx6 b;

        @DexIgnore
        public Ci(Sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                if (this.b.j) {
                    HomeActivity.a aVar = HomeActivity.B;
                    Wg6.b(activity, "fragmentActivity");
                    HomeActivity.a.b(aVar, activity, null, 2, null);
                }
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sx6 b;

        @DexIgnore
        public Di(Sx6 sx6, String str) {
            this.b = sx6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore
    public static final /* synthetic */ Xx6 L6(Sx6 sx6) {
        Xx6 xx6 = sx6.i;
        if (xx6 != null) {
            return xx6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FragmentActivity activity;
        Xx6 xx6 = this.i;
        if (xx6 == null) {
            Wg6.n("mPresenter");
            throw null;
        } else if (xx6.o() || (activity = getActivity()) == null) {
            return false;
        } else {
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Xx6 xx6) {
        M6(xx6);
    }

    @DexIgnore
    public void M6(Xx6 xx6) {
        Wg6.c(xx6, "presenter");
        this.i = xx6;
    }

    @DexIgnore
    @Override // com.fossil.Yx6
    public void f() {
        DashBar dashBar;
        G37<Z95> g37 = this.h;
        if (g37 != null) {
            Z95 a2 = g37.a();
            if (a2 != null && (dashBar = a2.z) != null) {
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.g(dashBar, this.j, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Yx6
    public void l1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.C;
            Wg6.b(activity, "fragmentActivity");
            Xx6 xx6 = this.i;
            if (xx6 != null) {
                aVar.b(activity, xx6.o());
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Z95 z95 = (Z95) Aq0.f(layoutInflater, 2131558604, viewGroup, false, A6());
        this.h = new G37<>(this, z95);
        Wg6.b(z95, "binding");
        return z95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Xx6 xx6 = this.i;
        if (xx6 != null) {
            xx6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Xx6 xx6 = this.i;
        if (xx6 != null) {
            xx6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        String c = Um5.c(PortfolioApp.get.instance(), 2131886948);
        G37<Z95> g37 = this.h;
        if (g37 != null) {
            Z95 a2 = g37.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new Bi(this, c));
                a2.s.setOnClickListener(new Ci(this, c));
                a2.w.setOnClickListener(new Di(this, c));
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "binding.ftvWearOs");
                flexibleTextView.setText(c);
                FlexibleTextView flexibleTextView2 = a2.v;
                Wg6.b(flexibleTextView2, "binding.ftvWearOs");
                flexibleTextView2.setTypeface(flexibleTextView2.getTypeface(), 1);
                if (!Wr4.a.a().d()) {
                    RelativeLayout relativeLayout = a2.A;
                    Wg6.b(relativeLayout, "binding.rlWearOsGroup");
                    relativeLayout.setVisibility(8);
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.j = z;
                Xx6 xx6 = this.i;
                if (xx6 != null) {
                    xx6.p(z);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Yx6
    public void y0(boolean z) {
        ImageView imageView;
        if (isActive()) {
            G37<Z95> g37 = this.h;
            if (g37 != null) {
                Z95 a2 = g37.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    Wg6.b(imageView, "it");
                    imageView.setVisibility(!z ? 4 : 0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }
}
