package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface G51 {
    @DexIgnore
    public static final Ai a = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ Ai a; // = new Ai();

        @DexIgnore
        public final G51 a(long j) {
            return new H51(j, null, null, 6, null);
        }
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    void b(Bitmap bitmap);

    @DexIgnore
    Bitmap c(int i, int i2, Bitmap.Config config);

    @DexIgnore
    Bitmap d(int i, int i2, Bitmap.Config config);
}
