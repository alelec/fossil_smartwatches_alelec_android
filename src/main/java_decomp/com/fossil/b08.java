package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface B08 {
    @DexIgnore
    int a();

    @DexIgnore
    void b(A08<?> a08);

    @DexIgnore
    A08<?> e();

    @DexIgnore
    void f(int i);
}
