package com.fossil;

import com.mapped.Rm6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zx7 extends CancellationException implements Bv7<Zx7> {
    @DexIgnore
    public /* final */ Rm6 coroutine;

    @DexIgnore
    public Zx7(String str) {
        this(str, null);
    }

    @DexIgnore
    public Zx7(String str, Rm6 rm6) {
        super(str);
        this.coroutine = rm6;
    }

    @DexIgnore
    @Override // com.fossil.Bv7
    public Zx7 createCopy() {
        String message = getMessage();
        if (message == null) {
            message = "";
        }
        Zx7 zx7 = new Zx7(message, this.coroutine);
        zx7.initCause(this);
        return zx7;
    }
}
