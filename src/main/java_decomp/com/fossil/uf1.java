package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uf1<DataType> implements Qb1<DataType, BitmapDrawable> {
    @DexIgnore
    public /* final */ Qb1<DataType, Bitmap> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public Uf1(Resources resources, Qb1<DataType, Bitmap> qb1) {
        Ik1.d(resources);
        this.b = resources;
        Ik1.d(qb1);
        this.a = qb1;
    }

    @DexIgnore
    @Override // com.fossil.Qb1
    public boolean a(DataType datatype, Ob1 ob1) throws IOException {
        return this.a.a(datatype, ob1);
    }

    @DexIgnore
    @Override // com.fossil.Qb1
    public Id1<BitmapDrawable> b(DataType datatype, int i, int i2, Ob1 ob1) throws IOException {
        return Og1.f(this.b, this.a.b(datatype, i, i2, ob1));
    }
}
