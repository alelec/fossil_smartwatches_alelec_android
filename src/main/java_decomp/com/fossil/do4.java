package com.fossil;

import com.fossil.Ao4;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Do4 {
    @DexIgnore
    public static /* final */ int[] a; // = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Zn4.values().length];
            a = iArr;
            try {
                iArr[Zn4.NUMERIC.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Zn4.ALPHANUMERIC.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Zn4.BYTE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Zn4.KANJI.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
        */
    }

    @DexIgnore
    public static void a(String str, Am4 am4, String str2) throws Rl4 {
        try {
            for (byte b : str.getBytes(str2)) {
                am4.g(b, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new Rl4(e);
        }
    }

    @DexIgnore
    public static void b(CharSequence charSequence, Am4 am4) throws Rl4 {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int p = p(charSequence.charAt(i));
            if (p != -1) {
                int i2 = i + 1;
                if (i2 < length) {
                    int p2 = p(charSequence.charAt(i2));
                    if (p2 != -1) {
                        am4.g(p2 + (p * 45), 11);
                        i += 2;
                    } else {
                        throw new Rl4();
                    }
                } else {
                    am4.g(p, 6);
                    i = i2;
                }
            } else {
                throw new Rl4();
            }
        }
    }

    @DexIgnore
    public static void c(String str, Zn4 zn4, Am4 am4, String str2) throws Rl4 {
        int i = Ai.a[zn4.ordinal()];
        if (i == 1) {
            h(str, am4);
        } else if (i == 2) {
            b(str, am4);
        } else if (i == 3) {
            a(str, am4, str2);
        } else if (i == 4) {
            e(str, am4);
        } else {
            throw new Rl4("Invalid mode: " + zn4);
        }
    }

    @DexIgnore
    public static void d(Cm4 cm4, Am4 am4) {
        am4.g(Zn4.ECI.getBits(), 4);
        am4.g(cm4.getValue(), 8);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028 A[LOOP:0: B:4:0x000d->B:12:0x0028, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void e(java.lang.String r7, com.fossil.Am4 r8) throws com.fossil.Rl4 {
        /*
            r1 = 33088(0x8140, float:4.6366E-41)
            r2 = -1
            java.lang.String r0 = "Shift_JIS"
            byte[] r4 = r7.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x0050 }
            int r5 = r4.length
            r0 = 0
            r3 = r0
        L_0x000d:
            if (r3 >= r5) goto L_0x0057
            byte r0 = r4[r3]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 << 8
            int r6 = r3 + 1
            byte r6 = r4[r6]
            r6 = r6 & 255(0xff, float:3.57E-43)
            r6 = r6 | r0
            if (r6 < r1) goto L_0x0038
            r0 = 40956(0x9ffc, float:5.7392E-41)
            if (r6 > r0) goto L_0x0038
            r0 = r1
        L_0x0024:
            int r0 = r6 - r0
        L_0x0026:
            if (r0 == r2) goto L_0x0048
            int r6 = r0 >> 8
            int r6 = r6 * 192
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 + r6
            r6 = 13
            r8.g(r0, r6)
            int r0 = r3 + 2
            r3 = r0
            goto L_0x000d
        L_0x0038:
            r0 = 57408(0xe040, float:8.0446E-41)
            if (r6 < r0) goto L_0x0046
            r0 = 60351(0xebbf, float:8.457E-41)
            if (r6 > r0) goto L_0x0046
            r0 = 49472(0xc140, float:6.9325E-41)
            goto L_0x0024
        L_0x0046:
            r0 = r2
            goto L_0x0026
        L_0x0048:
            com.fossil.Rl4 r0 = new com.fossil.Rl4
            java.lang.String r1 = "Invalid byte sequence"
            r0.<init>(r1)
            throw r0
        L_0x0050:
            r0 = move-exception
            com.fossil.Rl4 r1 = new com.fossil.Rl4
            r1.<init>(r0)
            throw r1
        L_0x0057:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Do4.e(java.lang.String, com.fossil.Am4):void");
    }

    @DexIgnore
    public static void f(int i, Ao4 ao4, Zn4 zn4, Am4 am4) throws Rl4 {
        int characterCountBits = zn4.getCharacterCountBits(ao4);
        int i2 = 1 << characterCountBits;
        if (i < i2) {
            am4.g(i, characterCountBits);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(" is bigger than ");
        sb.append(i2 - 1);
        throw new Rl4(sb.toString());
    }

    @DexIgnore
    public static void g(Zn4 zn4, Am4 am4) {
        am4.g(zn4.getBits(), 4);
    }

    @DexIgnore
    public static void h(CharSequence charSequence, Am4 am4) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - '0';
            int i2 = i + 2;
            if (i2 < length) {
                am4.g((charSequence.charAt(i2) - '0') + (charAt * 100) + ((charSequence.charAt(i + 1) - '0') * 10), 10);
                i += 3;
            } else {
                i++;
                if (i < length) {
                    am4.g((charSequence.charAt(i) - '0') + (charAt * 10), 7);
                    i = i2;
                } else {
                    am4.g(charAt, 4);
                }
            }
        }
    }

    @DexIgnore
    public static int i(Zn4 zn4, Am4 am4, Am4 am42, Ao4 ao4) {
        return am4.n() + zn4.getCharacterCountBits(ao4) + am42.n();
    }

    @DexIgnore
    public static int j(Co4 co4) {
        return Eo4.a(co4) + Eo4.c(co4) + Eo4.d(co4) + Eo4.e(co4);
    }

    @DexIgnore
    public static int k(Am4 am4, Yn4 yn4, Ao4 ao4, Co4 co4) throws Rl4 {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        int i3 = 0;
        while (i3 < 8) {
            Fo4.a(am4, yn4, ao4, i3, co4);
            int j = j(co4);
            if (j < i) {
                i2 = i3;
            } else {
                j = i;
            }
            i3++;
            i = j;
        }
        return i2;
    }

    @DexIgnore
    public static Zn4 l(String str, String str2) {
        boolean z;
        if ("Shift_JIS".equals(str2) && s(str)) {
            return Zn4.KANJI;
        }
        boolean z2 = false;
        boolean z3 = false;
        int i = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                z2 = true;
                z = z3;
            } else if (p(charAt) == -1) {
                return Zn4.BYTE;
            } else {
                z = true;
            }
            i++;
            z3 = z;
        }
        return z3 ? Zn4.ALPHANUMERIC : z2 ? Zn4.NUMERIC : Zn4.BYTE;
    }

    @DexIgnore
    public static Ao4 m(int i, Yn4 yn4) throws Rl4 {
        for (int i2 = 1; i2 <= 40; i2++) {
            Ao4 e = Ao4.e(i2);
            if (v(i, e, yn4)) {
                return e;
            }
        }
        throw new Rl4("Data too big");
    }

    @DexIgnore
    public static Go4 n(String str, Yn4 yn4, Map<Ml4, ?> map) throws Rl4 {
        Ao4 t;
        Cm4 characterSetECIByName;
        String obj = (map == null || !map.containsKey(Ml4.CHARACTER_SET)) ? "ISO-8859-1" : map.get(Ml4.CHARACTER_SET).toString();
        Zn4 l = l(str, obj);
        Am4 am4 = new Am4();
        if (l == Zn4.BYTE && !"ISO-8859-1".equals(obj) && (characterSetECIByName = Cm4.getCharacterSetECIByName(obj)) != null) {
            d(characterSetECIByName, am4);
        }
        g(l, am4);
        Am4 am42 = new Am4();
        c(str, l, am42, obj);
        if (map == null || !map.containsKey(Ml4.QR_VERSION)) {
            t = t(yn4, l, am4, am42);
        } else {
            t = Ao4.e(Integer.parseInt(map.get(Ml4.QR_VERSION).toString()));
            if (!v(i(l, am4, am42, t), t, yn4)) {
                throw new Rl4("Data too big for requested version");
            }
        }
        Am4 am43 = new Am4();
        am43.e(am4);
        f(l == Zn4.BYTE ? am42.o() : str.length(), t, l, am43);
        am43.e(am42);
        Ao4.Bi c = t.c(yn4);
        int d = t.d() - c.d();
        u(d, am43);
        Am4 r = r(am43, t.d(), d, c.c());
        Go4 go4 = new Go4();
        go4.c(yn4);
        go4.f(l);
        go4.g(t);
        int b = t.b();
        Co4 co4 = new Co4(b, b);
        int k = k(r, yn4, t, co4);
        go4.d(k);
        Fo4.a(r, yn4, t, k, co4);
        go4.e(co4);
        return go4;
    }

    @DexIgnore
    public static byte[] o(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        new Fm4(Dm4.k).b(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) ((byte) iArr[length + i3]);
        }
        return bArr2;
    }

    @DexIgnore
    public static int p(int i) {
        int[] iArr = a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    @DexIgnore
    public static void q(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) throws Rl4 {
        if (i4 < i3) {
            int i5 = i % i3;
            int i6 = i3 - i5;
            int i7 = i / i3;
            int i8 = i2 / i3;
            int i9 = i8 + 1;
            int i10 = i7 - i8;
            int i11 = (i7 + 1) - i9;
            if (i10 != i11) {
                throw new Rl4("EC bytes mismatch");
            } else if (i3 == i6 + i5) {
                if (i != (i5 * (i9 + i11)) + ((i8 + i10) * i6)) {
                    throw new Rl4("Total bytes mismatch");
                } else if (i4 < i6) {
                    iArr[0] = i8;
                    iArr2[0] = i10;
                } else {
                    iArr[0] = i9;
                    iArr2[0] = i11;
                }
            } else {
                throw new Rl4("RS blocks mismatch");
            }
        } else {
            throw new Rl4("Block ID too large");
        }
    }

    @DexIgnore
    public static Am4 r(Am4 am4, int i, int i2, int i3) throws Rl4 {
        if (am4.o() == i2) {
            ArrayList<Bo4> arrayList = new ArrayList(i3);
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            while (i4 < i3) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                q(i, i2, i3, i4, iArr, iArr2);
                int i8 = iArr[0];
                byte[] bArr = new byte[i8];
                am4.q(i7 << 3, bArr, 0, i8);
                byte[] o = o(bArr, iArr2[0]);
                arrayList.add(new Bo4(bArr, o));
                int max = Math.max(i6, i8);
                i4++;
                i5 = Math.max(i5, o.length);
                i6 = max;
                i7 = iArr[0] + i7;
            }
            if (i2 == i7) {
                Am4 am42 = new Am4();
                for (int i9 = 0; i9 < i6; i9++) {
                    for (Bo4 bo4 : arrayList) {
                        byte[] a2 = bo4.a();
                        if (i9 < a2.length) {
                            am42.g(a2[i9], 8);
                        }
                    }
                }
                for (int i10 = 0; i10 < i5; i10++) {
                    for (Bo4 bo42 : arrayList) {
                        byte[] b = bo42.b();
                        if (i10 < b.length) {
                            am42.g(b[i10], 8);
                        }
                    }
                }
                if (i == am42.o()) {
                    return am42;
                }
                throw new Rl4("Interleaving error: " + i + " and " + am42.o() + " differ.");
            }
            throw new Rl4("Data bytes does not match offset");
        }
        throw new Rl4("Number of bits and data bytes does not match");
    }

    @DexIgnore
    public static boolean s(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                int i2 = bytes[i] & 255;
                if ((i2 < 129 || i2 > 159) && (i2 < 224 || i2 > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    @DexIgnore
    public static Ao4 t(Yn4 yn4, Zn4 zn4, Am4 am4, Am4 am42) throws Rl4 {
        return m(i(zn4, am4, am42, m(i(zn4, am4, am42, Ao4.e(1)), yn4)), yn4);
    }

    @DexIgnore
    public static void u(int i, Am4 am4) throws Rl4 {
        int i2 = i << 3;
        if (am4.n() <= i2) {
            for (int i3 = 0; i3 < 4 && am4.n() < i2; i3++) {
                am4.d(false);
            }
            int n = am4.n() & 7;
            if (n > 0) {
                while (n < 8) {
                    am4.d(false);
                    n++;
                }
            }
            int o = am4.o();
            for (int i4 = 0; i4 < i - o; i4++) {
                am4.g((i4 & 1) == 0 ? 236 : 17, 8);
            }
            if (am4.n() != i2) {
                throw new Rl4("Bits size does not equal capacity");
            }
            return;
        }
        throw new Rl4("data bits cannot fit in the QR Code" + am4.n() + " > " + i2);
    }

    @DexIgnore
    public static boolean v(int i, Ao4 ao4, Yn4 yn4) {
        return ao4.d() - ao4.c(yn4).d() >= (i + 7) / 8;
    }
}
