package com.fossil;

import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zx5 implements MembersInjector<ConnectedAppsActivity> {
    @DexIgnore
    public static void a(ConnectedAppsActivity connectedAppsActivity, ConnectedAppsPresenter connectedAppsPresenter) {
        connectedAppsActivity.B = connectedAppsPresenter;
    }
}
