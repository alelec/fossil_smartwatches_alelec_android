package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wt3<TResult> implements Hu3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public Gt3 c;

    @DexIgnore
    public Wt3(Executor executor, Gt3 gt3) {
        this.a = executor;
        this.c = gt3;
    }

    @DexIgnore
    @Override // com.fossil.Hu3
    public final void a(Nt3<TResult> nt3) {
        if (nt3.o()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new Yt3(this));
                }
            }
        }
    }
}
