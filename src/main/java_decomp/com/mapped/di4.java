package com.mapped;

import com.facebook.share.internal.VideoUploader;
import com.fossil.fitness.WorkoutState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Di4 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop"),
    PAUSE("pause"),
    RESUME("resume"),
    IDLE("idle");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Di4 a(String str) {
            Di4 di4;
            Wg6.c(str, "value");
            Di4[] values = Di4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    di4 = null;
                    break;
                }
                di4 = values[i];
                String mValue = di4.getMValue();
                String lowerCase = str.toLowerCase();
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (Wg6.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return di4 != null ? di4 : Di4.IDLE;
        }

        @DexIgnore
        public final Di4 b(int i) {
            return i == WorkoutState.START.ordinal() ? Di4.START : i == WorkoutState.END.ordinal() ? Di4.STOP : i == WorkoutState.PAUSE.ordinal() ? Di4.PAUSE : i == WorkoutState.RESUME.ordinal() ? Di4.RESUME : Di4.IDLE;
        }
    }

    @DexIgnore
    public Di4(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
