package com.mapped;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Du0;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Ml5;
import com.fossil.Sw5;
import com.fossil.Um5;
import com.fossil.Vc5;
import com.fossil.Xc5;
import com.fossil.Xq0;
import com.fossil.Zw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardActiveTimeAdapter extends Du0<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ Zw5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ BaseFragment n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public Ai(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            Wg6.c(str, "mDayOfWeek");
            Wg6.c(str2, "mDayOfMonth");
            Wg6.c(str3, "mDailyValue");
            Wg6.c(str4, "mDailyUnit");
            Wg6.c(str5, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? false : z, (i & 4) == 0 ? z2 : false, (i & 8) != 0 ? "" : str, (i & 16) != 0 ? "" : str2, (i & 32) != 0 ? "" : str3, (i & 64) != 0 ? "" : str4, (i & 128) == 0 ? str5 : "");
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        public final void i(String str) {
            Wg6.c(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void j(String str) {
            Wg6.c(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void k(String str) {
            Wg6.c(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void l(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void m(String str) {
            Wg6.c(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void n(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void o(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final void p(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ Vc5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActiveTimeAdapter d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.a;
                if (date != null) {
                    this.b.d.l.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(DashboardActiveTimeAdapter dashboardActiveTimeAdapter, Vc5 vc5, View view) {
            super(view);
            Wg6.c(vc5, "binding");
            Wg6.c(view, "root");
            this.d = dashboardActiveTimeAdapter;
            this.b = vc5;
            this.c = view;
            vc5.n().setOnClickListener(new Aii(this));
            this.b.r.setTextColor(dashboardActiveTimeAdapter.f);
            this.b.v.setTextColor(dashboardActiveTimeAdapter.f);
        }

        @DexIgnore
        public void b(ActivitySummary activitySummary) {
            Ai v = this.d.v(activitySummary);
            this.a = v.d();
            FlexibleTextView flexibleTextView = this.b.u;
            Wg6.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(v.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            Wg6.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(v.e());
            FlexibleTextView flexibleTextView3 = this.b.s;
            Wg6.b(flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(v.c());
            FlexibleTextView flexibleTextView4 = this.b.r;
            Wg6.b(flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(v.b());
            FlexibleTextView flexibleTextView5 = this.b.v;
            Wg6.b(flexibleTextView5, "binding.ftvEst");
            flexibleTextView5.setText(v.a());
            if (v.g()) {
                this.b.r.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099938));
                FlexibleTextView flexibleTextView6 = this.b.r;
                Wg6.b(flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setAllCaps(true);
            } else {
                this.b.r.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099940));
                FlexibleTextView flexibleTextView7 = this.b.r;
                Wg6.b(flexibleTextView7, "binding.ftvDailyUnit");
                flexibleTextView7.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            Wg6.b(constraintLayout, "binding.container");
            constraintLayout.setSelected(!v.g());
            FlexibleTextView flexibleTextView8 = this.b.u;
            Wg6.b(flexibleTextView8, "binding.ftvDayOfWeek");
            flexibleTextView8.setSelected(v.h());
            FlexibleTextView flexibleTextView9 = this.b.t;
            Wg6.b(flexibleTextView9, "binding.ftvDayOfMonth");
            flexibleTextView9.setSelected(v.h());
            if (v.h()) {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (v.g()) {
                this.b.q.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public Ci(Date date, Date date2, String str, String str2) {
            Wg6.c(str, "mWeekly");
            Wg6.c(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(Date date, Date date2, String str, String str2, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void g(String str) {
            Wg6.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends Bi {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ Xc5 g;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActiveTimeAdapter h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;

            @DexIgnore
            public Aii(Di di) {
                this.b = di;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    Zw5 zw5 = this.b.h.l;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            zw5.q0(date, date2);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Di(com.mapped.DashboardActiveTimeAdapter r4, com.fossil.Xc5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.mapped.Wg6.c(r5, r0)
                r3.h = r4
                com.fossil.Vc5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.mapped.Wg6.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.mapped.Wg6.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.mapped.DashboardActiveTimeAdapter$Di$Aii r1 = new com.mapped.DashboardActiveTimeAdapter$Di$Aii
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.mapped.Wg6.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapped.DashboardActiveTimeAdapter.Di.<init>(com.mapped.DashboardActiveTimeAdapter, com.fossil.Xc5):void");
        }

        @DexIgnore
        @Override // com.mapped.DashboardActiveTimeAdapter.Bi
        public void b(ActivitySummary activitySummary) {
            Ci w = this.h.w(activitySummary);
            this.f = w.a();
            this.e = w.b();
            FlexibleTextView flexibleTextView = this.g.s;
            Wg6.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(w.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            Wg6.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(w.d());
            super.b(activitySummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActiveTimeAdapter b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public Ei(DashboardActiveTimeAdapter dashboardActiveTimeAdapter, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = dashboardActiveTimeAdapter;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            Wg6.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.n.getId() + ", isAdded=" + this.b.n.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.m.Z(this.b.n.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                Xq0 j = this.b.m.j();
                j.b(view.getId(), this.b.n, this.b.n.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                Xq0 j2 = this.b.m.j();
                j2.q(Z);
                j2.k();
                Xq0 j3 = this.b.m.j();
                j3.b(view.getId(), this.b.n, this.b.n.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.n.getId() + ", isAdded2=" + this.b.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            Wg6.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardActiveTimeAdapter(Sw5 sw5, PortfolioApp portfolioApp, Zw5 zw5, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(sw5);
        Wg6.c(sw5, "activityDifference");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(zw5, "mOnItemClick");
        Wg6.c(fragmentManager, "mFragmentManager");
        Wg6.c(baseFragment, "mFragment");
        this.k = portfolioApp;
        this.l = zw5;
        this.m = fragmentManager;
        this.n = baseFragment;
        String d2 = ThemeManager.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("nonBrandNonReachGoal");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("dianaActiveMinutesTab");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = ThemeManager.l.a().d("secondaryText");
        this.h = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        String d7 = ThemeManager.l.a().d("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(d7 == null ? "#FFFFFF" : d7);
        String d8 = ThemeManager.l.a().d("onDianaActiveMinutesTab");
        this.j = Color.parseColor(d8 == null ? "#FFFFFF" : d8);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) getItem(i2);
        if (activitySummary != null) {
            this.c.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            Calendar calendar = this.c;
            Wg6.b(calendar, "mCalendar");
            Boolean p0 = TimeUtils.p0(calendar.getTime());
            Wg6.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        Wg6.c(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            Wg6.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            Wg6.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardActiveTimeAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            Wg6.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new Ei(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((Bi) viewHolder).b((ActivitySummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((Bi) viewHolder).b((ActivitySummary) getItem(i2));
        } else {
            ((Di) viewHolder).b((ActivitySummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        Wg6.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new Fi(frameLayout, frameLayout);
        } else if (i2 == 1) {
            Vc5 z = Vc5.z(from, viewGroup, false);
            Wg6.b(z, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View n2 = z.n();
            Wg6.b(n2, "itemActiveTimeDayBinding.root");
            return new Bi(this, z, n2);
        } else if (i2 != 2) {
            Vc5 z2 = Vc5.z(from, viewGroup, false);
            Wg6.b(z2, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View n3 = z2.n();
            Wg6.b(n3, "itemActiveTimeDayBinding.root");
            return new Bi(this, z2, n3);
        } else {
            Xc5 z3 = Xc5.z(from, viewGroup, false);
            Wg6.b(z3, "ItemActiveTimeWeekBindin\u2026tInflater, parent, false)");
            return new Di(this, z3);
        }
    }

    @DexIgnore
    public final Ai v(ActivitySummary activitySummary) {
        boolean z = false;
        Ai ai = new Ai(null, false, false, null, null, null, null, null, 255, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i2 = instance.get(7);
            Wg6.b(instance, "calendar");
            Boolean p0 = TimeUtils.p0(instance.getTime());
            Wg6.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = Um5.c(this.k, 2131886637);
                Wg6.b(c2, "LanguageHelper.getString\u2026MinutesToday_Text__Today)");
                ai.n(c2);
            } else {
                ai.n(Jl5.b.i(i2));
            }
            ai.l(instance.getTime());
            ai.m(String.valueOf(instance.get(5)));
            if (activitySummary.getActiveTime() > 0) {
                int activeTime = activitySummary.getActiveTime();
                ai.k(Ml5.a.a(Integer.valueOf(activeTime)));
                String c3 = Um5.c(this.k, 2131886635);
                Wg6.b(c3, "LanguageHelper.getString\u2026MinutesToday_Label__Mins)");
                if (c3 != null) {
                    String lowerCase = c3.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    ai.j(lowerCase);
                    ai.i("");
                    if (activitySummary.getActiveTimeGoal() > 0) {
                        if (activeTime >= activitySummary.getActiveTimeGoal()) {
                            z = true;
                        }
                        ai.p(z);
                    } else {
                        ai.p(false);
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String c4 = Um5.c(this.k, 2131886608);
                Wg6.b(c4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                ai.j(c4);
                ai.o(true);
            }
        }
        return ai;
    }

    @DexIgnore
    public final Ci w(ActivitySummary activitySummary) {
        String str;
        Integer num = null;
        Ci ci = new Ci(null, null, null, null, 15, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            Wg6.b(instance, "calendar");
            Boolean p0 = TimeUtils.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = TimeUtils.N(i3);
            int i4 = instance.get(1);
            ci.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = TimeUtils.N(i6);
            int i7 = instance.get(1);
            ci.f(instance.getTime());
            Wg6.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = Um5.c(this.k, 2131886639);
                Wg6.b(str, "LanguageHelper.getString\u2026tesToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            ci.g(str);
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(this.k, 2131886636);
            Wg6.b(c2, "LanguageHelper.getString\u2026esToday_Text__NumberMins)");
            Ml5 ml5 = Ml5.a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            if (totalValuesOfWeek != null) {
                num = Integer.valueOf(totalValuesOfWeek.getTotalActiveTimeOfWeek());
            }
            String format = String.format(c2, Arrays.copyOf(new Object[]{ml5.a(num)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            ci.h(format);
        }
        return ci;
    }

    @DexIgnore
    public final void x(Cf<ActivitySummary> cf) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
        local.d("DashboardActiveTimeAdapter", sb.toString());
        super.i(cf);
    }
}
