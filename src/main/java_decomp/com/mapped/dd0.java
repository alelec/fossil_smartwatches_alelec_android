package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Dd0 {
    IN_PROGRESS("in_progress"),
    END("end"),
    ERROR("error"),
    SUCCESS("success");
    
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Dd0(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
