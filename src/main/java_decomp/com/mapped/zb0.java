package com.mapped;

import com.fossil.Yx1;
import com.fossil.common.task.Promise;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zb0<V> extends Promise<V, Yx1> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Hg6] */
    @Override // com.fossil.common.task.Promise
    public /* bridge */ /* synthetic */ Promise k(Hg6<? super Yx1, Cd6> hg6) {
        return q(hg6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Hg6] */
    @Override // com.fossil.common.task.Promise
    public /* bridge */ /* synthetic */ Promise l(Hg6<? super Yx1, Cd6> hg6) {
        return r(hg6);
    }

    @DexIgnore
    @Override // com.fossil.common.task.Promise
    public /* bridge */ /* synthetic */ Promise m(Hg6 hg6) {
        return s(hg6);
    }

    @DexIgnore
    public Zb0<V> p(Hg6<? super Cd6, Cd6> hg6) {
        Wg6.c(hg6, "actionOnFinal");
        super.e(hg6);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Hg6<? super com.fossil.Yx1, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Zb0<V> q(Hg6<? super Yx1, Cd6> hg6) {
        Wg6.c(hg6, "actionOnCancel");
        super.k(hg6);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Hg6<? super com.fossil.Yx1, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Zb0<V> r(Hg6<? super Yx1, Cd6> hg6) {
        Wg6.c(hg6, "actionOnError");
        super.l(hg6);
        return this;
    }

    @DexIgnore
    public Zb0<V> s(Hg6<? super V, Cd6> hg6) {
        Wg6.c(hg6, "actionOnSuccess");
        super.m(hg6);
        return this;
    }
}
