package com.mapped;

import com.fossil.Bl1;
import com.fossil.C1;
import com.fossil.Ey1;
import com.fossil.Ku;
import com.fossil.Lw;
import com.fossil.Mw;
import com.fossil.Nr;
import com.fossil.Zx1;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum U40 implements Zx1 {
    BLUETOOTH_OFF(0),
    INVALID_PARAMETERS(1),
    DEVICE_BUSY(2),
    EXECUTION_TIMEOUT(3),
    REQUEST_FAILED(4),
    REQUEST_UNSUPPORTED(5),
    RESPONSE_TIMEOUT(6),
    RESPONSE_FAILED(7),
    CONNECTION_DROPPED(8),
    INTERRUPTED(9),
    SERVICE_CHANGED(10),
    AUTHENTICATION_FAILED(11),
    SECRET_KEY_IS_REQUIRED(12),
    DATA_SIZE_OVER_LIMIT(13),
    UNSUPPORTED_FORMAT(14),
    TARGET_FIRMWARE_NOT_MATCHED(15),
    REQUEST_TIMEOUT(16),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ Ai e; // = new Ai(null);
    @DexIgnore
    public /* final */ String b; // = Ey1.a(this);
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final U40 a(Nr nr, HashMap<Bl1.Bi, Object> hashMap) {
            Boolean bool = (Boolean) hashMap.get(Bl1.Bi.b);
            boolean booleanValue = bool != null ? bool.booleanValue() : false;
            switch (C1.a[nr.c.ordinal()]) {
                case 1:
                    return U40.INTERRUPTED;
                case 2:
                    return booleanValue ? U40.SERVICE_CHANGED : U40.CONNECTION_DROPPED;
                case 3:
                    return U40.REQUEST_UNSUPPORTED;
                case 4:
                    Mw mw = nr.d;
                    Lw lw = mw.d;
                    return lw == Lw.q ? U40.REQUEST_TIMEOUT : lw == Lw.r ? U40.RESPONSE_TIMEOUT : (lw == Lw.e || lw == Lw.s) ? U40.RESPONSE_FAILED : mw.f == Ku.e ? U40.DATA_SIZE_OVER_LIMIT : U40.REQUEST_FAILED;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    return U40.RESPONSE_FAILED;
                case 16:
                    return U40.EXECUTION_TIMEOUT;
                case 17:
                case 18:
                    return U40.REQUEST_UNSUPPORTED;
                case 19:
                    return U40.BLUETOOTH_OFF;
                case 20:
                case 21:
                    return U40.AUTHENTICATION_FAILED;
                case 22:
                    return U40.SECRET_KEY_IS_REQUIRED;
                case 23:
                    return U40.INVALID_PARAMETERS;
                case 24:
                    return U40.REQUEST_FAILED;
                case 25:
                    return U40.DEVICE_BUSY;
                case 26:
                    return U40.UNSUPPORTED_FORMAT;
                case 27:
                    return U40.TARGET_FIRMWARE_NOT_MATCHED;
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                    return U40.UNKNOWN_ERROR;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore
    public U40(int i) {
        this.c = i;
    }

    @DexIgnore
    @Override // com.fossil.Zx1
    public int getCode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Zx1
    public String getLogName() {
        return this.b;
    }
}
