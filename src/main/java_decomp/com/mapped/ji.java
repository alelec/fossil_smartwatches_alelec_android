package com.mapped;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.Lx0;
import java.io.Closeable;
import java.io.File;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ji extends Closeable {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Ai(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void a(String str) {
            if (!str.equalsIgnoreCase(SQLiteDatabase.MEMORY) && str.trim().length() != 0) {
                Log.w("SupportSQLite", "deleting the database file: " + str);
                try {
                    if (Build.VERSION.SDK_INT >= 16) {
                        android.database.sqlite.SQLiteDatabase.deleteDatabase(new File(str));
                        return;
                    }
                    try {
                        if (!new File(str).delete()) {
                            Log.e("SupportSQLite", "Could not delete the database file " + str);
                        }
                    } catch (Exception e) {
                        Log.e("SupportSQLite", "error while deleting corrupted database file", e);
                    }
                } catch (Exception e2) {
                    Log.w("SupportSQLite", "delete failed: ", e2);
                }
            }
        }

        @DexIgnore
        public void b(Lx0 lx0) {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x004a, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x004b, code lost:
            if (r0 != null) goto L_0x004d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x004d, code lost:
            r2 = r0.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
            if (r2.hasNext() != false) goto L_0x0057;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0057, code lost:
            a((java.lang.String) r2.next().second);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0065, code lost:
            a(r4.getPath());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
            throw r1;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x004a A[ExcHandler: all (r1v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r0 
          PHI: (r0v2 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) = (r0v0 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v9 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v9 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) binds: [B:3:0x0029, B:5:0x002d, B:6:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0029] */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void c(com.fossil.Lx0 r4) {
            /*
                r3 = this;
                r0 = 0
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Corruption reported by sqlite on database: "
                r1.append(r2)
                java.lang.String r2 = r4.getPath()
                r1.append(r2)
                java.lang.String r2 = "SupportSQLite"
                java.lang.String r1 = r1.toString()
                android.util.Log.e(r2, r1)
                boolean r1 = r4.isOpen()
                if (r1 != 0) goto L_0x0029
                java.lang.String r0 = r4.getPath()
                r3.a(r0)
            L_0x0028:
                return
            L_0x0029:
                java.util.List r0 = r4.getAttachedDbs()     // Catch:{ SQLiteException -> 0x0075, all -> 0x004a }
            L_0x002d:
                r4.close()     // Catch:{ IOException -> 0x0077, all -> 0x004a }
            L_0x0030:
                if (r0 == 0) goto L_0x006d
                java.util.Iterator r1 = r0.iterator()
            L_0x0036:
                boolean r0 = r1.hasNext()
                if (r0 == 0) goto L_0x0028
                java.lang.Object r0 = r1.next()
                android.util.Pair r0 = (android.util.Pair) r0
                java.lang.Object r0 = r0.second
                java.lang.String r0 = (java.lang.String) r0
                r3.a(r0)
                goto L_0x0036
            L_0x004a:
                r1 = move-exception
                if (r0 == 0) goto L_0x0065
                java.util.Iterator r2 = r0.iterator()
            L_0x0051:
                boolean r0 = r2.hasNext()
                if (r0 == 0) goto L_0x006c
                java.lang.Object r0 = r2.next()
                android.util.Pair r0 = (android.util.Pair) r0
                java.lang.Object r0 = r0.second
                java.lang.String r0 = (java.lang.String) r0
                r3.a(r0)
                goto L_0x0051
            L_0x0065:
                java.lang.String r0 = r4.getPath()
                r3.a(r0)
            L_0x006c:
                throw r1
            L_0x006d:
                java.lang.String r0 = r4.getPath()
                r3.a(r0)
                goto L_0x0028
            L_0x0075:
                r1 = move-exception
                goto L_0x002d
            L_0x0077:
                r1 = move-exception
                goto L_0x0030
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapped.Ji.Ai.c(com.fossil.Lx0):void");
        }

        @DexIgnore
        public abstract void d(Lx0 lx0);

        @DexIgnore
        public abstract void e(Lx0 lx0, int i, int i2);

        @DexIgnore
        public void f(Lx0 lx0) {
        }

        @DexIgnore
        public abstract void g(Lx0 lx0, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Ai c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public Context a;
            @DexIgnore
            public String b;
            @DexIgnore
            public Ai c;
            @DexIgnore
            public boolean d;

            @DexIgnore
            public Aii(Context context) {
                this.a = context;
            }

            @DexIgnore
            public Bi a() {
                if (this.c == null) {
                    throw new IllegalArgumentException("Must set a callback to create the configuration.");
                } else if (this.a == null) {
                    throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
                } else if (!this.d || !TextUtils.isEmpty(this.b)) {
                    return new Bi(this.a, this.b, this.c, this.d);
                } else {
                    throw new IllegalArgumentException("Must set a non-null database name to a configuration that uses the no backup directory.");
                }
            }

            @DexIgnore
            public Aii b(Ai ai) {
                this.c = ai;
                return this;
            }

            @DexIgnore
            public Aii c(String str) {
                this.b = str;
                return this;
            }

            @DexIgnore
            public Aii d(boolean z) {
                this.d = z;
                return this;
            }
        }

        @DexIgnore
        public Bi(Context context, String str, Ai ai, boolean z) {
            this.a = context;
            this.b = str;
            this.c = ai;
            this.d = z;
        }

        @DexIgnore
        public static Aii a(Context context) {
            return new Aii(context);
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Ji create(Bi bi);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    Object close();  // void declaration

    @DexIgnore
    String getDatabaseName();

    @DexIgnore
    Lx0 getWritableDatabase();

    @DexIgnore
    void setWriteAheadLoggingEnabled(boolean z);
}
