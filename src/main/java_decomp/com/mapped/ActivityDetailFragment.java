package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.A25;
import com.fossil.Ai5;
import com.fossil.Aq0;
import com.fossil.Bl6;
import com.fossil.Cl6;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Jl6;
import com.fossil.Ml5;
import com.fossil.Mv5;
import com.fossil.Nn6;
import com.fossil.Ok6;
import com.fossil.Um5;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDetailFragment extends BaseFragment implements Cl6, View.OnClickListener, Jl6.Ai {
    @DexIgnore
    public static /* final */ Ai z; // = new Ai(null);
    @DexIgnore
    public Jl6 g;
    @DexIgnore
    public Date h; // = new Date();
    @DexIgnore
    public G37<A25> i;
    @DexIgnore
    public Bl6 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ActivityDetailFragment a(Date date) {
            Wg6.c(date, "date");
            ActivityDetailFragment activityDetailFragment = new ActivityDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            activityDetailFragment.setArguments(bundle);
            return activityDetailFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ Cf b;

        @DexIgnore
        public Bi(ActivityDetailFragment activityDetailFragment, boolean z, Cf cf, Ai5 ai5) {
            this.a = z;
            this.b = cf;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            Wg6.c(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public ActivityDetailFragment() {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        this.s = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = ThemeManager.l.a().d("backgroundDashboard");
        this.t = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("secondaryText");
        this.u = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("primaryText");
        this.v = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = ThemeManager.l.a().d("nonBrandNonReachGoal");
        this.x = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
    }

    @DexIgnore
    @Override // com.fossil.Jl6.Ai
    public void B2(WorkoutSession workoutSession) {
        String name;
        Ai5 o;
        Wg6.c(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Bl6 bl6 = this.j;
            if (bl6 == null || (o = bl6.o()) == null || (name = o.name()) == null) {
                name = Ai5.METRIC.name();
            }
            WorkoutDetailActivity.a aVar = WorkoutDetailActivity.f;
            Wg6.b(activity, "it");
            aVar.a(activity, workoutSession.getId(), name);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActivityDetailFragment";
    }

    @DexIgnore
    @Override // com.fossil.Cl6
    public void J(Ai5 ai5, ActivitySummary activitySummary) {
        A25 a2;
        double d;
        int i2;
        int i3;
        String sb;
        Wg6.c(ai5, "distanceUnit");
        FLogger.INSTANCE.getLocal().d("ActivityDetailFragment", "showDayDetail - distanceUnit=" + ai5 + ", activitySummary=" + activitySummary);
        G37<A25> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            Wg6.b(a2, "binding");
            View n = a2.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            if (activitySummary != null) {
                i2 = (int) activitySummary.getSteps();
                int stepGoal = activitySummary.getStepGoal();
                d = activitySummary.getDistance();
                i3 = stepGoal;
            } else {
                d = 0.0d;
                i2 = 0;
                i3 = 0;
            }
            if (i2 > 0) {
                FlexibleTextView flexibleTextView = a2.x;
                Wg6.b(flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(Ml5.a.d(Integer.valueOf(i2)));
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "binding.ftvDailyUnit");
                String c = Um5.c(context, 2131886678);
                Wg6.b(c, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                if (c != null) {
                    String lowerCase = c.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView2.setText(lowerCase);
                    FlexibleTextView flexibleTextView3 = a2.A;
                    Wg6.b(flexibleTextView3, "binding.ftvEst");
                    if (ai5 == Ai5.IMPERIAL) {
                        StringBuilder sb2 = new StringBuilder();
                        Hr7 hr7 = Hr7.a;
                        String c2 = Um5.c(context, 2131886679);
                        Wg6.b(c2, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format = String.format(c2, Arrays.copyOf(new Object[]{Ml5.a.c(Float.valueOf((float) d), ai5)}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                        sb2.append(format);
                        sb2.append(" ");
                        sb2.append(PortfolioApp.get.instance().getString(2131886851));
                        sb = sb2.toString();
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        Hr7 hr72 = Hr7.a;
                        String c3 = Um5.c(context, 2131886679);
                        Wg6.b(c3, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format2 = String.format(c3, Arrays.copyOf(new Object[]{Ml5.a.c(Float.valueOf((float) d), ai5)}, 1));
                        Wg6.b(format2, "java.lang.String.format(format, *args)");
                        sb3.append(format2);
                        sb3.append(" ");
                        sb3.append(PortfolioApp.get.instance().getString(2131886850));
                        sb = sb3.toString();
                    }
                    flexibleTextView3.setText(sb);
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                FlexibleTextView flexibleTextView4 = a2.x;
                Wg6.b(flexibleTextView4, "binding.ftvDailyValue");
                flexibleTextView4.setText("");
                FlexibleTextView flexibleTextView5 = a2.w;
                Wg6.b(flexibleTextView5, "binding.ftvDailyUnit");
                String c4 = Um5.c(context, 2131886717);
                Wg6.b(c4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                if (c4 != null) {
                    String upperCase = c4.toUpperCase();
                    Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView5.setText(upperCase);
                    FlexibleTextView flexibleTextView6 = a2.A;
                    Wg6.b(flexibleTextView6, "binding.ftvEst");
                    flexibleTextView6.setText("");
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            int i4 = i3 > 0 ? (i2 * 100) / i3 : -1;
            if (i2 >= i3 && i3 > 0) {
                a2.z.setTextColor(this.s);
                a2.y.setTextColor(this.s);
                a2.w.setTextColor(this.s);
                a2.x.setTextColor(this.s);
                a2.A.setTextColor(this.s);
                RTLImageView rTLImageView = a2.G;
                Wg6.b(rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.F;
                Wg6.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                Wg6.b(constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.z;
                Wg6.b(flexibleTextView7, "binding.ftvDayOfWeek");
                flexibleTextView7.setSelected(true);
                FlexibleTextView flexibleTextView8 = a2.y;
                Wg6.b(flexibleTextView8, "binding.ftvDayOfMonth");
                flexibleTextView8.setSelected(true);
                View view = a2.H;
                Wg6.b(view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView9 = a2.x;
                Wg6.b(flexibleTextView9, "binding.ftvDailyValue");
                flexibleTextView9.setSelected(true);
                FlexibleTextView flexibleTextView10 = a2.w;
                Wg6.b(flexibleTextView10, "binding.ftvDailyUnit");
                flexibleTextView10.setSelected(true);
                FlexibleTextView flexibleTextView11 = a2.A;
                Wg6.b(flexibleTextView11, "binding.ftvEst");
                flexibleTextView11.setSelected(true);
                String str = this.m;
                if (str != null) {
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.y.setTextColor(Color.parseColor(str));
                    a2.x.setTextColor(Color.parseColor(str));
                    a2.w.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.H.setBackgroundColor(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                    a2.F.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.k;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i2 > 0) {
                a2.y.setTextColor(this.v);
                a2.z.setTextColor(this.u);
                a2.w.setTextColor(this.x);
                a2.x.setTextColor(this.v);
                a2.A.setTextColor(this.v);
                View view2 = a2.H;
                Wg6.b(view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.G;
                Wg6.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.F;
                Wg6.b(rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                int i5 = this.x;
                a2.H.setBackgroundColor(i5);
                a2.G.setColorFilter(i5);
                a2.F.setColorFilter(i5);
                String str3 = this.l;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.y.setTextColor(this.v);
                a2.z.setTextColor(this.u);
                a2.x.setTextColor(this.w);
                a2.w.setTextColor(this.w);
                RTLImageView rTLImageView5 = a2.G;
                Wg6.b(rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.F;
                Wg6.b(rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                Wg6.b(constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.z;
                Wg6.b(flexibleTextView12, "binding.ftvDayOfWeek");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.y;
                Wg6.b(flexibleTextView13, "binding.ftvDayOfMonth");
                flexibleTextView13.setSelected(false);
                View view3 = a2.H;
                Wg6.b(view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView14 = a2.x;
                Wg6.b(flexibleTextView14, "binding.ftvDailyValue");
                flexibleTextView14.setSelected(false);
                FlexibleTextView flexibleTextView15 = a2.w;
                Wg6.b(flexibleTextView15, "binding.ftvDailyUnit");
                flexibleTextView15.setSelected(false);
                FlexibleTextView flexibleTextView16 = a2.A;
                Wg6.b(flexibleTextView16, "binding.ftvEst");
                flexibleTextView16.setSelected(false);
                int i6 = this.x;
                a2.H.setBackgroundColor(i6);
                a2.G.setColorFilter(i6);
                a2.F.setColorFilter(i6);
                String str4 = this.l;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.J;
                Wg6.b(flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView17 = a2.D;
                Wg6.b(flexibleTextView17, "binding.ftvProgressValue");
                flexibleTextView17.setText(Um5.c(context, 2131887328));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.J;
                Wg6.b(flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView18 = a2.D;
                Wg6.b(flexibleTextView18, "binding.ftvProgressValue");
                flexibleTextView18.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView19 = a2.B;
            Wg6.b(flexibleTextView19, "binding.ftvGoalValue");
            Hr7 hr73 = Hr7.a;
            String c5 = Um5.c(context, 2131886724);
            Wg6.b(c5, "LanguageHelper.getString\u2026age_Title__OfNumberSteps)");
            String format3 = String.format(c5, Arrays.copyOf(new Object[]{Ml5.a.d(Integer.valueOf(i3))}, 1));
            Wg6.b(format3, "java.lang.String.format(format, *args)");
            flexibleTextView19.setText(format3);
        }
    }

    @DexIgnore
    public final void K6(A25 a25) {
        a25.E.setOnClickListener(this);
        a25.F.setOnClickListener(this);
        a25.G.setOnClickListener(this);
        DeviceHelper.Ai ai = DeviceHelper.o;
        Bl6 bl6 = this.j;
        this.k = ai.w(bl6 != null ? bl6.n() : null) ? ThemeManager.l.a().d("dianaStepsTab") : ThemeManager.l.a().d("hybridStepsTab");
        this.l = ThemeManager.l.a().d("nonBrandActivityDetailBackground");
        this.m = ThemeManager.l.a().d("onDianaStepsTab");
        Jl6 jl6 = new Jl6(Jl6.Ci.STEPS, Ai5.IMPERIAL, new WorkoutSessionDifference(), this.k);
        this.g = jl6;
        if (jl6 != null) {
            jl6.u(this);
        }
        a25.I.setBackgroundColor(this.t);
        a25.r.setBackgroundColor(this.s);
        RecyclerView recyclerView = a25.L;
        Wg6.b(recyclerView, "it");
        recyclerView.setAdapter(this.g);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable f = W6.f(recyclerView.getContext(), 2131230857);
        if (f != null) {
            Ok6 ok6 = new Ok6(linearLayoutManager.q2(), false, false, 6, null);
            Wg6.b(f, ResourceManager.DRAWABLE);
            ok6.h(f);
            recyclerView.addItemDecoration(ok6);
        }
    }

    @DexIgnore
    public final void L6() {
        A25 a2;
        OverviewDayChart overviewDayChart;
        G37<A25> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.v) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Bl6 bl6 = this.j;
            if (ai.w(bl6 != null ? bl6.n() : null)) {
                overviewDayChart.D("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Bl6 bl6) {
        M6(bl6);
    }

    @DexIgnore
    public void M6(Bl6 bl6) {
        Wg6.c(bl6, "presenter");
        this.j = bl6;
        Lifecycle lifecycle = getLifecycle();
        Bl6 bl62 = this.j;
        if (bl62 != null) {
            lifecycle.a(bl62.p());
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl6
    public void j(Date date, boolean z2, boolean z3, boolean z4) {
        A25 a2;
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4);
        this.h = date;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        G37<A25> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            a2.q.r(true, true);
            FlexibleTextView flexibleTextView = a2.y;
            Wg6.b(flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(instance.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.F;
                Wg6.b(rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.F;
                Wg6.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.G;
                Wg6.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.z;
                    Wg6.b(flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(Um5.c(getContext(), 2131886662));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.z;
                Wg6.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(Jl5.b.i(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.G;
            Wg6.b(rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.z;
            Wg6.b(flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(Jl5.b.i(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl6
    public void n(Mv5 mv5, ArrayList<String> arrayList) {
        A25 a2;
        OverviewDayChart overviewDayChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDayDetailChart - baseModel=" + mv5);
        G37<A25> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.v) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("ActivityDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    Bl6 bl6 = this.j;
                    if (bl6 != null) {
                        bl6.v();
                        return;
                    }
                    return;
                case 2131362735:
                    Bl6 bl62 = this.j;
                    if (bl62 != null) {
                        bl62.u();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        A25 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        A25 a25 = (A25) Aq0.f(layoutInflater, 2131558496, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.h = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.h = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Wg6.b(a25, "binding");
        K6(a25);
        Bl6 bl6 = this.j;
        if (bl6 != null) {
            bl6.q(this.h);
        }
        this.i = new G37<>(this, a25);
        L6();
        G37<A25> g37 = this.i;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        Bl6 bl6 = this.j;
        if (bl6 != null) {
            bl6.r();
        }
        Lifecycle lifecycle = getLifecycle();
        Bl6 bl62 = this.j;
        if (bl62 != null) {
            lifecycle.c(bl62.p());
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Bl6 bl6 = this.j;
        if (bl6 != null) {
            bl6.m();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Bl6 bl6 = this.j;
        if (bl6 != null) {
            bl6.t(this.h);
        }
        Bl6 bl62 = this.j;
        if (bl62 != null) {
            bl62.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        Bl6 bl6 = this.j;
        if (bl6 != null) {
            bl6.s(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.fossil.Cl6
    public void s(boolean z2, Ai5 ai5, Cf<WorkoutSession> cf) {
        A25 a2;
        Wg6.c(ai5, "distanceUnit");
        Wg6.c(cf, "workoutSessions");
        G37<A25> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z2) {
                LinearLayout linearLayout = a2.I;
                Wg6.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                if (!cf.isEmpty()) {
                    FlexibleTextView flexibleTextView = a2.C;
                    Wg6.b(flexibleTextView, "it.ftvNoWorkoutRecorded");
                    flexibleTextView.setVisibility(8);
                    RecyclerView recyclerView = a2.L;
                    Wg6.b(recyclerView, "it.rvWorkout");
                    recyclerView.setVisibility(0);
                    Jl6 jl6 = this.g;
                    if (jl6 != null) {
                        jl6.t(ai5, cf);
                    }
                } else {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    Wg6.b(flexibleTextView2, "it.ftvNoWorkoutRecorded");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a2.L;
                    Wg6.b(recyclerView2, "it.rvWorkout");
                    recyclerView2.setVisibility(8);
                    Jl6 jl62 = this.g;
                    if (jl62 != null) {
                        jl62.t(ai5, cf);
                    }
                }
            } else {
                LinearLayout linearLayout2 = a2.I;
                Wg6.b(linearLayout2, "it.llWorkout");
                linearLayout2.setVisibility(8);
            }
            AppBarLayout appBarLayout = a2.q;
            Wg6.b(appBarLayout, "it.appBarLayout");
            ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
            if (layoutParams != null) {
                CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.f();
                if (behavior == null) {
                    behavior = new AppBarLayout.Behavior();
                }
                behavior.setDragCallback(new Bi(this, z2, cf, ai5));
                eVar.o(behavior);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
        }
    }

    @DexIgnore
    @Override // com.fossil.Jl6.Ai
    public void t4(WorkoutSession workoutSession) {
        Wg6.c(workoutSession, "workoutSession");
        Nn6 b = Nn6.v.b(workoutSession.getId());
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        b.show(childFragmentManager, Nn6.v.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
