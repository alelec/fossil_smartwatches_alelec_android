package com.mapped;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Jh6;
import com.fossil.Jl5;
import com.fossil.Kh6;
import com.fossil.Mv5;
import com.fossil.T65;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.fossil.Y67;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewDayFragment extends BaseFragment implements Kh6 {
    @DexIgnore
    public Y67 g;
    @DexIgnore
    public G37<T65> h;
    @DexIgnore
    public Jh6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayFragment b;

        @DexIgnore
        public Ai(GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment, T65 t65) {
            this.b = goalTrackingOverviewDayFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewDayFragment.K6(this.b).a().l(2);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Y67 K6(GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment) {
        Y67 y67 = goalTrackingOverviewDayFragment.g;
        if (y67 != null) {
            return y67;
        }
        Wg6.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        T65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        G37<T65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            overviewDayGoalChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Jh6 jh6) {
        M6(jh6);
    }

    @DexIgnore
    public void M6(Jh6 jh6) {
        Wg6.c(jh6, "presenter");
        this.i = jh6;
    }

    @DexIgnore
    @Override // com.fossil.Kh6
    public void n(Mv5 mv5, ArrayList<String> arrayList) {
        T65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + mv5);
        G37<T65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayGoalChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayGoalChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayGoalChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        T65 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        T65 t65 = (T65) Aq0.f(layoutInflater, 2131558560, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Ts0 a3 = Vs0.e(activity).a(Y67.class);
            Wg6.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (Y67) a3;
            t65.s.setOnClickListener(new Ai(this, t65));
        }
        this.h = new G37<>(this, t65);
        L6();
        G37<T65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        L6();
        Jh6 jh6 = this.i;
        if (jh6 != null) {
            jh6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        Jh6 jh6 = this.i;
        if (jh6 != null) {
            jh6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Kh6
    public void x(boolean z) {
        T65 a2;
        G37<T65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                Wg6.b(overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                Wg6.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            Wg6.b(overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            Wg6.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
