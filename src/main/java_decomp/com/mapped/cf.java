package com.mapped;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Eu0;
import com.fossil.Gu0;
import com.fossil.Hu0;
import com.fossil.Iu0;
import com.fossil.Vt0;
import com.fossil.Wt0;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cf<T> extends AbstractList<T> {
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ Executor c;
    @DexIgnore
    public /* final */ Ci<T> d;
    @DexIgnore
    public /* final */ Fi e;
    @DexIgnore
    public /* final */ Eu0<T> f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public T h; // = null;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public int l; // = Integer.MAX_VALUE;
    @DexIgnore
    public int m; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public /* final */ AtomicBoolean s; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ArrayList<WeakReference<Ei>> t; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public Ai(boolean z, boolean z2, boolean z3) {
            this.b = z;
            this.c = z2;
            this.d = z3;
        }

        @DexIgnore
        public void run() {
            if (this.b) {
                Cf.this.d.c();
            }
            if (this.c) {
                Cf.this.j = true;
            }
            if (this.d) {
                Cf.this.k = true;
            }
            Cf.this.E(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public Bi(boolean z, boolean z2) {
            this.b = z;
            this.c = z2;
        }

        @DexIgnore
        public void run() {
            Cf.this.n(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<T> {
        @DexIgnore
        public abstract void a(T t);

        @DexIgnore
        public abstract void b(T t);

        @DexIgnore
        public abstract void c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<Key, Value> {
        @DexIgnore
        public /* final */ Xe<Key, Value> a;
        @DexIgnore
        public /* final */ Fi b;
        @DexIgnore
        public Executor c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public Ci e;
        @DexIgnore
        public Key f;

        @DexIgnore
        public Di(Xe<Key, Value> xe, Fi fi) {
            if (xe == null) {
                throw new IllegalArgumentException("DataSource may not be null");
            } else if (fi != null) {
                this.a = xe;
                this.b = fi;
            } else {
                throw new IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        public Cf<Value> a() {
            Executor executor = this.c;
            if (executor != null) {
                Executor executor2 = this.d;
                if (executor2 != null) {
                    return Cf.k(this.a, executor, executor2, this.e, this.b, this.f);
                }
                throw new IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new IllegalArgumentException("MainThreadExecutor required");
        }

        @DexIgnore
        public Di<Key, Value> b(Ci ci) {
            this.e = ci;
            return this;
        }

        @DexIgnore
        public Di<Key, Value> c(Executor executor) {
            this.d = executor;
            return this;
        }

        @DexIgnore
        public Di<Key, Value> d(Key key) {
            this.f = key;
            return this;
        }

        @DexIgnore
        public Di<Key, Value> e(Executor executor) {
            this.c = executor;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei {
        @DexIgnore
        public abstract void a(int i, int i2);

        @DexIgnore
        public abstract void b(int i, int i2);

        @DexIgnore
        public abstract void c(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public int a; // = -1;
            @DexIgnore
            public int b; // = -1;
            @DexIgnore
            public int c; // = -1;
            @DexIgnore
            public boolean d; // = true;
            @DexIgnore
            public int e; // = Integer.MAX_VALUE;

            @DexIgnore
            public Fi a() {
                if (this.b < 0) {
                    this.b = this.a;
                }
                if (this.c < 0) {
                    this.c = this.a * 3;
                }
                if (this.d || this.b != 0) {
                    int i = this.e;
                    if (i == Integer.MAX_VALUE || i >= this.a + (this.b * 2)) {
                        return new Fi(this.a, this.b, this.d, this.c, this.e);
                    }
                    throw new IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.a + ", prefetchDist=" + this.b + ", maxSize=" + this.e);
                }
                throw new IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }

            @DexIgnore
            public Aii b(boolean z) {
                this.d = z;
                return this;
            }

            @DexIgnore
            public Aii c(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public Aii d(int i) {
                if (i >= 1) {
                    this.a = i;
                    return this;
                }
                throw new IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            public Aii e(int i) {
                this.b = i;
                return this;
            }
        }

        @DexIgnore
        public Fi(int i, int i2, boolean z, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.e = i3;
            this.d = i4;
        }
    }

    @DexIgnore
    public Cf(Eu0<T> eu0, Executor executor, Executor executor2, Ci<T> ci, Fi fi) {
        this.f = eu0;
        this.b = executor;
        this.c = executor2;
        this.d = ci;
        this.e = fi;
        this.i = (fi.b * 2) + fi.a;
    }

    @DexIgnore
    public static <K, T> Cf<T> k(Xe<K, T> xe, Executor executor, Executor executor2, Ci<T> ci, Fi fi, K k2) {
        int i2;
        Xe<K, T> xe2;
        if (xe.isContiguous() || !fi.c) {
            if (!xe.isContiguous()) {
                xe = ((Gu0) xe).wrapAsContiguousWithoutPlaceholders();
                if (k2 != null) {
                    i2 = k2.intValue();
                    xe2 = xe;
                    return new Wt0((Vt0) xe2, executor, executor2, ci, fi, k2, i2);
                }
            }
            i2 = -1;
            xe2 = xe;
            return new Wt0((Vt0) xe2, executor, executor2, ci, fi, k2, i2);
        }
        return new Iu0((Gu0) xe, executor, executor2, ci, fi, k2 != null ? k2.intValue() : 0);
    }

    @DexIgnore
    public void A(int i2) {
        this.g += i2;
        this.l += i2;
        this.m += i2;
    }

    @DexIgnore
    public void B(Ei ei) {
        for (int size = this.t.size() - 1; size >= 0; size--) {
            Ei ei2 = this.t.get(size).get();
            if (ei2 == null || ei2 == ei) {
                this.t.remove(size);
            }
        }
    }

    @DexIgnore
    public List<T> D() {
        return u() ? this : new Hu0(this);
    }

    @DexIgnore
    public void E(boolean z) {
        boolean z2 = true;
        boolean z3 = this.j && this.l <= this.e.b;
        if (!this.k || this.m < (size() - 1) - this.e.b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.j = false;
            }
            if (z2) {
                this.k = false;
            }
            if (z) {
                this.b.execute(new Bi(z3, z2));
            } else {
                n(z3, z2);
            }
        }
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public T get(int i2) {
        T t2 = this.f.get(i2);
        if (t2 != null) {
            this.h = t2;
        }
        return t2;
    }

    @DexIgnore
    public void j(List<T> list, Ei ei) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                o((Cf) list, ei);
            } else if (!this.f.isEmpty()) {
                ei.b(0, this.f.size());
            }
        }
        for (int size = this.t.size() - 1; size >= 0; size--) {
            if (this.t.get(size).get() == null) {
                this.t.remove(size);
            }
        }
        this.t.add(new WeakReference<>(ei));
    }

    @DexIgnore
    public void l(boolean z, boolean z2, boolean z3) {
        if (this.d != null) {
            if (this.l == Integer.MAX_VALUE) {
                this.l = this.f.size();
            }
            if (this.m == Integer.MIN_VALUE) {
                this.m = 0;
            }
            if (z || z2 || z3) {
                this.b.execute(new Ai(z, z2, z3));
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    public void m() {
        this.s.set(true);
    }

    @DexIgnore
    public void n(boolean z, boolean z2) {
        if (z) {
            this.d.b(this.f.f());
        }
        if (z2) {
            this.d.a(this.f.g());
        }
    }

    @DexIgnore
    public abstract void o(Cf<T> cf, Ei ei);

    @DexIgnore
    public abstract Xe<?, T> p();

    @DexIgnore
    public abstract Object q();

    @DexIgnore
    public int r() {
        return this.f.m();
    }

    @DexIgnore
    public abstract boolean s();

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    public boolean t() {
        return this.s.get();
    }

    @DexIgnore
    public boolean u() {
        return t();
    }

    @DexIgnore
    public void v(int i2) {
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        this.g = r() + i2;
        w(i2);
        this.l = Math.min(this.l, i2);
        this.m = Math.max(this.m, i2);
        E(true);
    }

    @DexIgnore
    public abstract void w(int i2);

    @DexIgnore
    public void x(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                Ei ei = this.t.get(size).get();
                if (ei != null) {
                    ei.a(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void y(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                Ei ei = this.t.get(size).get();
                if (ei != null) {
                    ei.b(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void z(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.t.size() - 1; size >= 0; size--) {
                Ei ei = this.t.get(size).get();
                if (ei != null) {
                    ei.c(i2, i3);
                }
            }
        }
    }
}
