package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Hy1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X80 extends Ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ K70 b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ J70 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<X80> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public X80 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                K70 valueOf = K70.valueOf(readString);
                float readFloat = parcel.readFloat();
                float readFloat2 = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    return new X80(valueOf, readFloat, readFloat2, J70.valueOf(readString2));
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public X80[] newArray(int i) {
            return new X80[i];
        }
    }

    @DexIgnore
    public X80(K70 k70, float f, float f2, J70 j70) {
        this.b = k70;
        this.c = Hy1.e(f, 2);
        this.d = Hy1.e(f2, 2);
        this.e = j70;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject put = new JSONObject().put("day", this.b.a()).put("high", Float.valueOf(this.c)).put("low", Float.valueOf(this.d)).put("cond_id", this.e.a());
        Wg6.b(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(X80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            X80 x80 = (X80) obj;
            if (this.b != x80.b) {
                return false;
            }
            if (this.c != x80.c) {
                return false;
            }
            if (this.d != x80.d) {
                return false;
            }
            return this.e == x80.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherDayForecast");
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.c;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.d;
    }

    @DexIgnore
    public final J70 getWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public final K70 getWeekday() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return (((((hashCode * 31) + hashCode2) * 31) + Float.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.b2, Ey1.a(this.b)), Jd0.Z1, Float.valueOf(this.c)), Jd0.a2, Float.valueOf(this.d)), Jd0.t, Ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeFloat(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
