package com.mapped;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.Dl5;
import com.fossil.F67;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Mv5;
import com.fossil.Mw5;
import com.fossil.Nq4;
import com.fossil.P65;
import com.fossil.S37;
import com.fossil.Um5;
import com.fossil.Yl6;
import com.fossil.Zl6;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailFragment extends BaseFragment implements Zl6, View.OnClickListener, Mw5.Bi, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai A; // = new Ai(null);
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public Mw5 h;
    @DexIgnore
    public G37<P65> i;
    @DexIgnore
    public Yl6 j;
    @DexIgnore
    public F67 k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ String y;
    @DexIgnore
    public HashMap z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final GoalTrackingDetailFragment a(Date date) {
            Wg6.c(date, "date");
            GoalTrackingDetailFragment goalTrackingDetailFragment = new GoalTrackingDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            goalTrackingDetailFragment.setArguments(bundle);
            return goalTrackingDetailFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends F67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewEmptySupport e;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailFragment f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(RecyclerViewEmptySupport recyclerViewEmptySupport, LinearLayoutManager linearLayoutManager, GoalTrackingDetailFragment goalTrackingDetailFragment, LinearLayoutManager linearLayoutManager2, P65 p65) {
            super(linearLayoutManager);
            this.e = recyclerViewEmptySupport;
            this.f = goalTrackingDetailFragment;
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void b(int i) {
            Yl6 yl6 = this.f.j;
            if (yl6 != null) {
                yl6.r();
            }
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void c(int i, int i2) {
        }
    }

    @DexIgnore
    public GoalTrackingDetailFragment() {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        this.t = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = ThemeManager.l.a().d("secondaryText");
        this.u = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("primaryText");
        this.v = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("nonBrandDisableCalendarDay");
        this.w = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("nonBrandNonReachGoal");
        this.x = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = ThemeManager.l.a().d("nonBrandSurface");
        this.y = d6 == null ? "#FFFFFF" : d6;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "GoalTrackingDetailFragment";
    }

    @DexIgnore
    public final void L6(P65 p65) {
        p65.F.setOnClickListener(this);
        p65.G.setOnClickListener(this);
        p65.H.setOnClickListener(this);
        if (!TextUtils.isEmpty(this.y)) {
            int parseColor = Color.parseColor(this.y);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131230985);
            if (drawable != null) {
                drawable.setTint(parseColor);
                p65.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        p65.q.setOnClickListener(this);
        this.l = ThemeManager.l.a().d("hybridGoalTrackingTab");
        this.m = ThemeManager.l.a().d("nonBrandActivityDetailBackground");
        this.s = ThemeManager.l.a().d("onHybridGoalTrackingTab");
        p65.r.setBackgroundColor(this.t);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        RecyclerViewEmptySupport recyclerViewEmptySupport = p65.N;
        Wg6.b(recyclerViewEmptySupport, "it");
        recyclerViewEmptySupport.setLayoutManager(linearLayoutManager);
        RecyclerView.m layoutManager = recyclerViewEmptySupport.getLayoutManager();
        if (layoutManager != null) {
            this.k = new Bi(recyclerViewEmptySupport, (LinearLayoutManager) layoutManager, this, linearLayoutManager, p65);
            recyclerViewEmptySupport.setAdapter(this.h);
            FlexibleTextView flexibleTextView = p65.O;
            Wg6.b(flexibleTextView, "binding.tvNotFound");
            recyclerViewEmptySupport.setEmptyView(flexibleTextView);
            F67 f67 = this.k;
            if (f67 != null) {
                recyclerViewEmptySupport.addOnScrollListener(f67);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yl6 yl6) {
        N6(yl6);
    }

    @DexIgnore
    public final void M6() {
        P65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        G37<P65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            overviewDayGoalChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    public void N6(Yl6 yl6) {
        Wg6.c(yl6, "presenter");
        this.j = yl6;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        GoalTrackingData goalTrackingData;
        Yl6 yl6;
        Bundle bundle = null;
        Serializable serializable = null;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 193266439) {
            if (hashCode == 1983205541 && str.equals("GOAL_TRACKING_ADD") && i2 == 2131362260) {
                if (intent != null) {
                    serializable = intent.getSerializableExtra("EXTRA_NUMBER_PICKER_RESULTS");
                }
                if (serializable != null) {
                    HashMap hashMap = (HashMap) serializable;
                    Integer num = (Integer) hashMap.get(2131362892);
                    Integer num2 = (Integer) hashMap.get(2131362893);
                    Integer num3 = (Integer) hashMap.get(2131362896);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailFragment", "onDialogFragmentResult GOAL_TRACKING_ADD: hour=" + num + ", minute=" + num2 + ", suffix=" + num3);
                    Calendar instance = Calendar.getInstance();
                    Wg6.b(instance, "calendar");
                    instance.setTime(this.g);
                    if (num != null && num2 != null && num3 != null) {
                        if (num.intValue() == 12) {
                            num = 0;
                        }
                        instance.set(9, num3.intValue());
                        instance.set(10, num.intValue());
                        instance.set(12, num2.intValue());
                        Calendar instance2 = Calendar.getInstance();
                        Wg6.b(instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        Wg6.b(time, "Calendar.getInstance().time");
                        long time2 = time.getTime();
                        Date time3 = instance.getTime();
                        Wg6.b(time3, "calendar.time");
                        if (time2 > time3.getTime()) {
                            Yl6 yl62 = this.j;
                            if (yl62 != null) {
                                Date time4 = instance.getTime();
                                Wg6.b(time4, "calendar.time");
                                yl62.n(time4);
                                return;
                            }
                            return;
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        if (fragmentManager != null) {
                            S37 s37 = S37.c;
                            Wg6.b(fragmentManager, "this");
                            s37.B(fragmentManager);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Int, kotlin.Int>");
            }
        } else if (str.equals("GOAL_TRACKING_DELETE") && i2 == 2131363373) {
            if (intent != null) {
                bundle = intent.getExtras();
            }
            if (bundle != null && (goalTrackingData = (GoalTrackingData) bundle.getSerializable("GOAL_TRACKING_DELETE_BUNDLE")) != null && (yl6 = this.j) != null) {
                yl6.o(goalTrackingData);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Zl6
    public void X0(GoalTrackingSummary goalTrackingSummary) {
        P65 a2;
        int i2;
        int i3;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailFragment", "showDayDetail - goalTrackingSummary=" + goalTrackingSummary);
        G37<P65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            Wg6.b(a2, "binding");
            View n = a2.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            if (goalTrackingSummary != null) {
                i3 = goalTrackingSummary.getTotalTracked();
                i2 = goalTrackingSummary.getGoalTarget();
            } else {
                i2 = 0;
                i3 = 0;
            }
            FlexibleTextView flexibleTextView = a2.A;
            Wg6.b(flexibleTextView, "binding.ftvDailyValue");
            flexibleTextView.setText(Dl5.c((float) i3, 1));
            FlexibleTextView flexibleTextView2 = a2.z;
            Wg6.b(flexibleTextView2, "binding.ftvDailyUnit");
            flexibleTextView2.setText("/ " + i2 + "  " + Um5.c(PortfolioApp.get.instance(), 2131886745));
            int i4 = i2 > 0 ? (i3 * 100) / i2 : -1;
            if (i3 >= i2 && i2 > 0) {
                a2.C.setTextColor(this.t);
                a2.B.setTextColor(this.t);
                a2.z.setTextColor(this.t);
                a2.A.setTextColor(this.t);
                RTLImageView rTLImageView = a2.H;
                Wg6.b(rTLImageView, "binding.ivNextDate");
                rTLImageView.setSelected(true);
                RTLImageView rTLImageView2 = a2.G;
                Wg6.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setSelected(true);
                ConstraintLayout constraintLayout = a2.s;
                Wg6.b(constraintLayout, "binding.clOverviewDay");
                constraintLayout.setSelected(true);
                FlexibleTextView flexibleTextView3 = a2.C;
                Wg6.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setSelected(true);
                FlexibleTextView flexibleTextView4 = a2.B;
                Wg6.b(flexibleTextView4, "binding.ftvDayOfMonth");
                flexibleTextView4.setSelected(true);
                View view = a2.J;
                Wg6.b(view, "binding.line");
                view.setSelected(true);
                FlexibleTextView flexibleTextView5 = a2.A;
                Wg6.b(flexibleTextView5, "binding.ftvDailyValue");
                flexibleTextView5.setSelected(true);
                FlexibleTextView flexibleTextView6 = a2.z;
                Wg6.b(flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setSelected(true);
                FlexibleTextView flexibleTextView7 = a2.O;
                Wg6.b(flexibleTextView7, "binding.tvNotFound");
                flexibleTextView7.setVisibility(4);
                String str = this.s;
                if (str != null) {
                    a2.C.setTextColor(Color.parseColor(str));
                    a2.B.setTextColor(Color.parseColor(str));
                    a2.A.setTextColor(Color.parseColor(str));
                    a2.z.setTextColor(Color.parseColor(str));
                    a2.J.setBackgroundColor(Color.parseColor(str));
                    a2.H.setColorFilter(Color.parseColor(str));
                    a2.G.setColorFilter(Color.parseColor(str));
                }
                String str2 = this.l;
                if (str2 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str2));
                }
            } else if (i3 > 0) {
                a2.B.setTextColor(this.v);
                a2.C.setTextColor(this.u);
                a2.z.setTextColor(this.x);
                View view2 = a2.J;
                Wg6.b(view2, "binding.line");
                view2.setSelected(false);
                RTLImageView rTLImageView3 = a2.H;
                Wg6.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setSelected(false);
                RTLImageView rTLImageView4 = a2.G;
                Wg6.b(rTLImageView4, "binding.ivBackDate");
                rTLImageView4.setSelected(false);
                a2.A.setTextColor(this.v);
                FlexibleTextView flexibleTextView8 = a2.O;
                Wg6.b(flexibleTextView8, "binding.tvNotFound");
                flexibleTextView8.setVisibility(4);
                int i5 = this.x;
                a2.J.setBackgroundColor(i5);
                a2.H.setColorFilter(i5);
                a2.G.setColorFilter(i5);
                String str3 = this.m;
                if (str3 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str3));
                }
            } else {
                a2.B.setTextColor(this.v);
                a2.C.setTextColor(this.u);
                a2.A.setTextColor(this.v);
                a2.z.setTextColor(this.w);
                RTLImageView rTLImageView5 = a2.H;
                Wg6.b(rTLImageView5, "binding.ivNextDate");
                rTLImageView5.setSelected(false);
                RTLImageView rTLImageView6 = a2.G;
                Wg6.b(rTLImageView6, "binding.ivBackDate");
                rTLImageView6.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.s;
                Wg6.b(constraintLayout2, "binding.clOverviewDay");
                constraintLayout2.setSelected(false);
                FlexibleTextView flexibleTextView9 = a2.C;
                Wg6.b(flexibleTextView9, "binding.ftvDayOfWeek");
                flexibleTextView9.setSelected(false);
                FlexibleTextView flexibleTextView10 = a2.B;
                Wg6.b(flexibleTextView10, "binding.ftvDayOfMonth");
                flexibleTextView10.setSelected(false);
                View view3 = a2.J;
                Wg6.b(view3, "binding.line");
                view3.setSelected(false);
                FlexibleTextView flexibleTextView11 = a2.A;
                Wg6.b(flexibleTextView11, "binding.ftvDailyValue");
                flexibleTextView11.setSelected(false);
                FlexibleTextView flexibleTextView12 = a2.z;
                Wg6.b(flexibleTextView12, "binding.ftvDailyUnit");
                flexibleTextView12.setSelected(false);
                FlexibleTextView flexibleTextView13 = a2.O;
                Wg6.b(flexibleTextView13, "binding.tvNotFound");
                flexibleTextView13.setVisibility(0);
                int i6 = this.x;
                a2.J.setBackgroundColor(i6);
                a2.H.setColorFilter(i6);
                a2.G.setColorFilter(i6);
                String str4 = this.m;
                if (str4 != null) {
                    a2.s.setBackgroundColor(Color.parseColor(str4));
                }
            }
            if (i4 == -1) {
                FlexibleProgressBar flexibleProgressBar = a2.L;
                Wg6.b(flexibleProgressBar, "binding.pbGoal");
                flexibleProgressBar.setProgress(0);
                FlexibleTextView flexibleTextView14 = a2.E;
                Wg6.b(flexibleTextView14, "binding.ftvProgressValue");
                flexibleTextView14.setText(Um5.c(context, 2131887328));
            } else {
                FlexibleProgressBar flexibleProgressBar2 = a2.L;
                Wg6.b(flexibleProgressBar2, "binding.pbGoal");
                flexibleProgressBar2.setProgress(i4);
                FlexibleTextView flexibleTextView15 = a2.E;
                Wg6.b(flexibleTextView15, "binding.ftvProgressValue");
                flexibleTextView15.setText(i4 + "%");
            }
            FlexibleTextView flexibleTextView16 = a2.D;
            Wg6.b(flexibleTextView16, "binding.ftvGoalValue");
            Hr7 hr7 = Hr7.a;
            String c = Um5.c(context, 2131886747);
            Wg6.b(c, "LanguageHelper.getString\u2026age_Title__OfNumberTimes)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView16.setText(format);
        }
    }

    @DexIgnore
    @Override // com.fossil.Mw5.Bi
    public void Z2(GoalTrackingData goalTrackingData) {
        Wg6.c(goalTrackingData, "item");
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.H(childFragmentManager, goalTrackingData);
    }

    @DexIgnore
    @Override // com.fossil.Zl6
    public void d() {
        F67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zl6
    public void j(Date date, boolean z2, boolean z3, boolean z4) {
        P65 a2;
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z2 + " - isToday - " + z3 + " - isDateAfter: " + z4);
        this.g = date;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        int i2 = instance.get(7);
        G37<P65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null) {
            FlexibleTextView flexibleTextView = a2.B;
            Wg6.b(flexibleTextView, "binding.ftvDayOfMonth");
            flexibleTextView.setText(String.valueOf(instance.get(5)));
            if (z2) {
                RTLImageView rTLImageView = a2.G;
                Wg6.b(rTLImageView, "binding.ivBackDate");
                rTLImageView.setVisibility(4);
            } else {
                RTLImageView rTLImageView2 = a2.G;
                Wg6.b(rTLImageView2, "binding.ivBackDate");
                rTLImageView2.setVisibility(0);
            }
            if (z3 || z4) {
                RTLImageView rTLImageView3 = a2.H;
                Wg6.b(rTLImageView3, "binding.ivNextDate");
                rTLImageView3.setVisibility(8);
                if (z3) {
                    FlexibleTextView flexibleTextView2 = a2.C;
                    Wg6.b(flexibleTextView2, "binding.ftvDayOfWeek");
                    flexibleTextView2.setText(Um5.c(getContext(), 2131886662));
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.C;
                Wg6.b(flexibleTextView3, "binding.ftvDayOfWeek");
                flexibleTextView3.setText(Jl5.b.i(i2));
                return;
            }
            RTLImageView rTLImageView4 = a2.H;
            Wg6.b(rTLImageView4, "binding.ivNextDate");
            rTLImageView4.setVisibility(0);
            FlexibleTextView flexibleTextView4 = a2.C;
            Wg6.b(flexibleTextView4, "binding.ftvDayOfWeek");
            flexibleTextView4.setText(Jl5.b.i(i2));
        }
    }

    @DexIgnore
    @Override // com.fossil.Zl6
    public void n(Mv5 mv5, ArrayList<String> arrayList) {
        P65 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailChart - baseModel=" + mv5);
        G37<P65> g37 = this.i;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayGoalChart = a2.y) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayGoalChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayGoalChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayGoalChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zl6
    public void n5(Cf<GoalTrackingData> cf) {
        Mw5 mw5;
        Wg6.c(cf, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailData - goalTrackingDataList=" + cf);
        G37<P65> g37 = this.i;
        if (g37 != null && g37.a() != null && (mw5 = this.h) != null) {
            mw5.i(cf);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int i2;
        int i3;
        int i4 = 12;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("GoalTrackingDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131361881:
                    Boolean p0 = TimeUtils.p0(this.g);
                    Wg6.b(p0, "DateHelper.isToday(mDate)");
                    if (p0.booleanValue()) {
                        Calendar instance = Calendar.getInstance();
                        int i5 = instance.get(10);
                        i3 = instance.get(12);
                        i2 = instance.get(9);
                        if (i5 != 0) {
                            i4 = i5;
                        }
                    } else {
                        i2 = 0;
                        i3 = 0;
                    }
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886102);
                    Wg6.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    if (c != null) {
                        String upperCase = c.toUpperCase();
                        Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886104);
                        Wg6.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                        if (c2 != null) {
                            String upperCase2 = c2.toUpperCase();
                            Wg6.b(upperCase2, "(this as java.lang.String).toUpperCase()");
                            s37.G(childFragmentManager, i4, i3, i2, new String[]{upperCase, upperCase2});
                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    Yl6 yl6 = this.j;
                    if (yl6 != null) {
                        yl6.v();
                        return;
                    }
                    return;
                case 2131362735:
                    Yl6 yl62 = this.j;
                    if (yl62 != null) {
                        yl62.u();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        P65 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        P65 p65 = (P65) Aq0.f(layoutInflater, 2131558558, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.g = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.g = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        this.h = new Mw5(this, new Nq4());
        Wg6.b(p65, "binding");
        L6(p65);
        Yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.p(this.g);
        }
        this.i = new G37<>(this, p65);
        M6();
        G37<P65> g37 = this.i;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        Yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.q();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.m();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        M6();
        Yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.t(this.g);
        }
        Yl6 yl62 = this.j;
        if (yl62 != null) {
            yl62.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        Yl6 yl6 = this.j;
        if (yl6 != null) {
            yl6.s(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.z;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
