package com.mapped;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.B77;
import com.fossil.Ew5;
import com.fossil.Hl7;
import com.fossil.Im7;
import com.fossil.Ka7;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Nm0;
import com.fossil.Oa7;
import com.fossil.Pm7;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Ug5;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.faces.page.MyFaceViewModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MyFaceFragment extends Qv5 implements Ew5.Ai, Ka7.Ai, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai y; // = new Ai(null);
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public Ug5 i;
    @DexIgnore
    public MyFaceViewModel j;
    @DexIgnore
    public Oa7 k;
    @DexIgnore
    public Ka7 l;
    @DexIgnore
    public Ew5 m;
    @DexIgnore
    public List<String> s; // = new ArrayList();
    @DexIgnore
    public String t;
    @DexIgnore
    public int u;
    @DexIgnore
    public String v; // = "";
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final MyFaceFragment a(String str, String str2) {
            Wg6.c(str, "watchFaceId");
            Wg6.c(str2, "type");
            MyFaceFragment myFaceFragment = new MyFaceFragment();
            myFaceFragment.setArguments(Nm0.a(Hl7.a("WATCH_FACE_ID", str), Hl7.a("WATCH_FACE_ID_TYPE", str2)));
            return myFaceFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ MyFaceFragment a;

        @DexIgnore
        public Bi(MyFaceFragment myFaceFragment) {
            this.a = myFaceFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            this.a.t = t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ MyFaceFragment a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Comparator<T> {
            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return Mn7.c(t2.c().getCreatedAt(), t.c().getCreatedAt());
            }
        }

        @DexIgnore
        public Ci(MyFaceFragment myFaceFragment) {
            this.a = myFaceFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            List<Ka7.Bi> b0 = Pm7.b0(t, new Aii());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "watchFace changes " + b0);
            if (b0.isEmpty()) {
                this.a.s.clear();
                return;
            }
            Ka7 ka7 = this.a.l;
            if (ka7 != null) {
                ka7.k(b0);
            }
        }
    }

    @DexIgnore
    public final void N6(Ka7.Bi bi) {
        WatchFacePreviewView watchFacePreviewView;
        if (bi == null) {
            Ug5 ug5 = this.i;
            if (ug5 != null && (watchFacePreviewView = ug5.g) != null) {
                watchFacePreviewView.U();
                return;
            }
            return;
        }
        DianaWatchFaceUser c = bi.c();
        if (c.getPreviewURL().length() > 0) {
            Ug5 ug52 = this.i;
            if (ug52 != null) {
                ug52.g.P(c.getPreviewURL());
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Ug5 ug53 = this.i;
            if (ug53 != null) {
                ug53.g.Q(bi.b(), bi.a());
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() == -466555268 && str.equals("DELETE_MY_FACE") && i2 == 2131363373) {
            String stringExtra = intent != null ? intent.getStringExtra("WATCH_FACE_ID") : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "delete watch face id " + stringExtra);
            Oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.o(stringExtra);
            } else {
                Wg6.n("mSharedViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ew5.Ai
    public void a4(int i2) {
        Ka7 ka7 = this.l;
        if (ka7 == null) {
            return;
        }
        if (i2 >= 0) {
            Ka7.Bi bi = (Ka7.Bi) ka7.getCurrentList().get(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MyFaceFragment", "show watch face " + bi);
            this.t = bi.c().getId();
            N6(bi);
            int size = ka7.getCurrentList().size();
            boolean z = true;
            if (i2 != size - 1) {
                z = false;
            }
            Oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.t(bi.c().getId());
                Oa7 oa72 = this.k;
                if (oa72 != null) {
                    oa72.u(z);
                } else {
                    Wg6.n("mSharedViewModel");
                    throw null;
                }
            } else {
                Wg6.n("mSharedViewModel");
                throw null;
            }
        } else {
            N6(null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ka7.Ai
    public void i6(String str) {
        Wg6.c(str, "watchFaceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MyFaceFragment", "onWatchFaceApply id " + str + " currentId " + this.t);
        Oa7 oa7 = this.k;
        if (oa7 != null) {
            String str2 = this.t;
            if (str2 == null) {
                str2 = str;
            }
            oa7.n(str2);
            B77.a.a(str);
            return;
        }
        Wg6.n("mSharedViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Ug5 c = Ug5.c(layoutInflater, viewGroup, false);
        this.i = c;
        if (c != null) {
            return c.b();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String str;
        String str2;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.get.instance().getIface().q0().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = new ViewModelProvider(this, po4).a(MyFaceViewModel.class);
            Wg6.b(a2, "ViewModelProvider(this, \u2026aceViewModel::class.java)");
            this.j = (MyFaceViewModel) a2;
            FragmentActivity requireActivity = requireActivity();
            if (requireActivity != null) {
                this.k = ((WatchFaceListActivity) requireActivity).M();
                this.l = new Ka7(this);
                Ug5 ug5 = this.i;
                if (ug5 != null) {
                    RecyclerView recyclerView = ug5.e;
                    if (ug5 != null) {
                        Wg6.b(recyclerView, "mBinding!!.rvFaces");
                        recyclerView.setAdapter(this.l);
                        Ug5 ug52 = this.i;
                        if (ug52 != null) {
                            RecyclerView recyclerView2 = ug52.e;
                            Wg6.b(recyclerView2, "mBinding!!.rvFaces");
                            recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
                            Ew5 ew5 = new Ew5(this);
                            this.m = ew5;
                            Ug5 ug53 = this.i;
                            if (ug53 != null) {
                                ew5.b(ug53.e);
                                Bundle arguments = getArguments();
                                if (arguments == null || (str = arguments.getString("WATCH_FACE_ID")) == null) {
                                    str = "";
                                }
                                this.v = str;
                                Bundle arguments2 = getArguments();
                                if (arguments2 == null || (str2 = arguments2.getString("WATCH_FACE_ID_TYPE")) == null) {
                                    str2 = "";
                                }
                                this.w = str2;
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                local.d("MyFaceFragment", "onViewCreated watchFaceId " + this.v + " watchFaceIdType " + this.w);
                                Oa7 oa7 = this.k;
                                if (oa7 != null) {
                                    LiveData<String> q = oa7.q();
                                    LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
                                    Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
                                    q.h(viewLifecycleOwner, new Bi(this));
                                    MyFaceViewModel myFaceViewModel = this.j;
                                    if (myFaceViewModel != null) {
                                        LiveData<List<Ka7.Bi>> A = myFaceViewModel.A();
                                        LifecycleOwner viewLifecycleOwner2 = getViewLifecycleOwner();
                                        Wg6.b(viewLifecycleOwner2, "viewLifecycleOwner");
                                        A.h(viewLifecycleOwner2, new Ci(this));
                                        return;
                                    }
                                    Wg6.n("mViewModel");
                                    throw null;
                                }
                                Wg6.n("mSharedViewModel");
                                throw null;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.faces.WatchFaceListActivity");
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ka7.Ai
    public void s3(List<Ka7.Bi> list) {
        int i2;
        T t2;
        T t3;
        T t4;
        T t5;
        T t6;
        boolean z = false;
        Wg6.c(list, "sortedList");
        FLogger.INSTANCE.getLocal().d("MyFaceFragment", "onCurrentListChanged");
        if (!TextUtils.isEmpty(this.v)) {
            if (Wg6.a(this.w, "ORDER_ID")) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t6 = null;
                        break;
                    }
                    T next = it.next();
                    DianaWatchFaceOrder order = next.c().getOrder();
                    if (Wg6.a(order != null ? order.getOrderId() : null, this.v)) {
                        t6 = next;
                        break;
                    }
                }
                t4 = t6;
            } else if (Wg6.a(this.w, "ITEM_ID")) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t5 = null;
                        break;
                    }
                    T next2 = it2.next();
                    DianaWatchFaceOrder order2 = next2.c().getOrder();
                    if (Wg6.a(order2 != null ? order2.getWatchFaceId() : null, this.v)) {
                        t5 = next2;
                        break;
                    }
                }
                t4 = t5;
            } else {
                Iterator<T> it3 = list.iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (Wg6.a(next3.c().getId(), this.v)) {
                        t3 = next3;
                        break;
                    }
                }
                t4 = t3;
            }
            this.v = "";
            i2 = Pm7.K(list, t4);
        } else if (!TextUtils.isEmpty(this.t)) {
            Iterator<T> it4 = list.iterator();
            while (true) {
                if (!it4.hasNext()) {
                    t2 = null;
                    break;
                }
                T next4 = it4.next();
                if (Wg6.a(next4.c().getId(), this.t)) {
                    t2 = next4;
                    break;
                }
            }
            i2 = Pm7.K(list, t2);
            if (i2 < 0 && (!list.isEmpty())) {
                i2 = Pm7.K(this.s, this.t);
                FLogger.INSTANCE.getLocal().d("MyFaceFragment", "deletePosition " + i2 + " sortedList.size " + list.size());
                if (i2 > list.size() - 1) {
                    i2--;
                }
            }
        } else {
            i2 = 0;
        }
        this.u = i2;
        this.s.clear();
        List<String> list2 = this.s;
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it5 = list.iterator();
        while (it5.hasNext()) {
            arrayList.add(it5.next().c().getId());
        }
        list2.addAll(Pm7.j0(arrayList));
        FLogger.INSTANCE.getLocal().d("MyFaceFragment", "pos " + this.u + " currentId " + this.t + " mWatchFaceIds " + this.s);
        int i3 = this.u;
        if (i3 >= 0) {
            Ka7.Bi bi = list.get(i3);
            this.t = bi.c().getId();
            Oa7 oa7 = this.k;
            if (oa7 != null) {
                oa7.t(bi.c().getId());
                FLogger.INSTANCE.getLocal().d("MyFaceFragment", "show watch face " + bi);
                N6(list.get(this.u));
                if (this.u == list.size() - 1) {
                    z = true;
                }
                Oa7 oa72 = this.k;
                if (oa72 != null) {
                    oa72.u(z);
                    FLogger.INSTANCE.getLocal().d("MyFaceFragment", "onCurrentListChanged scrollToPosition to " + this.u);
                    Ug5 ug5 = this.i;
                    if (ug5 != null) {
                        ug5.e.scrollToPosition(this.u);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mSharedViewModel");
                    throw null;
                }
            } else {
                Wg6.n("mSharedViewModel");
                throw null;
            }
        } else {
            N6(null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ka7.Ai
    public void u6(String str) {
        Wg6.c(str, "watchFaceId");
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.m(childFragmentManager, str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
