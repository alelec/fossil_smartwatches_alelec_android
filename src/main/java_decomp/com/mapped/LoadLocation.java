package com.mapped;

import android.location.Location;
import com.fossil.I14;
import com.fossil.Lo4;
import com.fossil.Mn5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoadLocation extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ Lo4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public Ci(DeviceLocation deviceLocation) {
            Wg6.c(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Lo4.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ LoadLocation a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(LoadLocation loadLocation) {
            this.a = loadLocation;
        }

        @DexIgnore
        @Override // com.fossil.Lo4.Bi
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.get.instance().J(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            Mn5.p.a().i().saveDeviceLocation(deviceLocation);
                            this.a.j(new Ci(deviceLocation));
                        } catch (Exception e) {
                            this.a.i(new Bi());
                        }
                        this.a.m().p(this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.a.m().p(this);
            this.a.i(new Bi());
        }
    }

    @DexIgnore
    public LoadLocation(Lo4 lo4) {
        Wg6.c(lo4, "mfLocationService");
        I14.o(lo4, "mfLocationService cannot be null!", new Object[0]);
        Wg6.b(lo4, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = lo4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "LoadLocation";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return n(ai, xe6);
    }

    @DexIgnore
    public final Lo4 m() {
        return this.d;
    }

    @DexIgnore
    public Object n(Ai ai, Xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.k(new Di(this));
        return new Object();
    }
}
