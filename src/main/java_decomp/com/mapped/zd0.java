package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ev1;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Mo1;
import com.fossil.Ox1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zd0 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Mo1 b;
    @DexIgnore
    public /* final */ Ev1 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Zd0> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zd0 createFromParcel(Parcel parcel) {
            Mo1 mo1 = Mo1.values()[parcel.readInt()];
            Parcelable readParcelable = parcel.readParcelable(Ev1.class.getClassLoader());
            if (readParcelable != null) {
                return new Zd0(mo1, (Ev1) readParcelable);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zd0[] newArray(int i) {
            return new Zd0[i];
        }
    }

    @DexIgnore
    public Zd0(Mo1 mo1, Ev1 ev1) {
        this.b = mo1;
        this.c = ev1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Zd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Zd0 zd0 = (Zd0) obj;
            if (this.b != zd0.b) {
                return false;
            }
            return !(Wg6.a(this.c, zd0.c) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.reply_message.AppNotificationReplyMessageMapping");
    }

    @DexIgnore
    public final Mo1 getNotificationType() {
        return this.b;
    }

    @DexIgnore
    public final Ev1 getReplyMessageGroup() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.e, Ey1.a(this.b)), Jd0.h5, this.c.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
        parcel.writeParcelable(this.c, i);
    }
}
