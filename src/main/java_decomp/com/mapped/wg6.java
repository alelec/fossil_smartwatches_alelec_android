package com.mapped;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.Sl7;
import com.fossil.Wk7;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wg6 {
    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    @DexIgnore
    public static void b(Object obj, String str) {
        if (obj == null) {
            IllegalStateException illegalStateException = new IllegalStateException(str + " must not be null");
            f(illegalStateException);
            throw illegalStateException;
        }
    }

    @DexIgnore
    public static void c(Object obj, String str) {
        if (obj == null) {
            j(str);
            throw null;
        }
    }

    @DexIgnore
    public static int d(int i, int i2) {
        if (i < i2) {
            return -1;
        }
        return i == i2 ? 0 : 1;
    }

    @DexIgnore
    public static void e(int i, String str) {
        k();
        throw null;
    }

    @DexIgnore
    public static <T extends Throwable> T f(T t) {
        g(t, Wg6.class.getName());
        return t;
    }

    @DexIgnore
    public static <T extends Throwable> T g(T t, String str) {
        StackTraceElement[] stackTrace = t.getStackTrace();
        int length = stackTrace.length;
        int i = -1;
        for (int i2 = 0; i2 < length; i2++) {
            if (str.equals(stackTrace[i2].getClassName())) {
                i = i2;
            }
        }
        t.setStackTrace((StackTraceElement[]) Arrays.copyOfRange(stackTrace, i + 1, length));
        return t;
    }

    @DexIgnore
    public static String h(String str, Object obj) {
        return str + obj;
    }

    @DexIgnore
    public static void i() {
        Wk7 wk7 = new Wk7();
        f(wk7);
        throw wk7;
    }

    @DexIgnore
    public static void j(String str) {
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
        String className = stackTraceElement.getClassName();
        String methodName = stackTraceElement.getMethodName();
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Parameter specified as non-null is null: method " + className + CodelessMatcher.CURRENT_CLASS_NAME + methodName + ", parameter " + str);
        f(illegalArgumentException);
        throw illegalArgumentException;
    }

    @DexIgnore
    public static void k() {
        l("This function has a reified type parameter and thus can only be inlined at compilation time, not called directly.");
        throw null;
    }

    @DexIgnore
    public static void l(String str) {
        throw new UnsupportedOperationException(str);
    }

    @DexIgnore
    public static void m(String str) {
        Sl7 sl7 = new Sl7(str);
        f(sl7);
        throw sl7;
    }

    @DexIgnore
    public static void n(String str) {
        m("lateinit property " + str + " has not been initialized");
        throw null;
    }
}
