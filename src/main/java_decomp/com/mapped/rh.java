package com.mapped;

import com.fossil.Nx0;
import com.fossil.Ox0;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rh implements Ox0, Nx0 {
    @DexIgnore
    public static /* final */ TreeMap<Integer, Rh> j; // = new TreeMap<>();
    @DexIgnore
    public volatile String b;
    @DexIgnore
    public /* final */ long[] c;
    @DexIgnore
    public /* final */ double[] d;
    @DexIgnore
    public /* final */ String[] e;
    @DexIgnore
    public /* final */ byte[][] f;
    @DexIgnore
    public /* final */ int[] g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Nx0 {
        @DexIgnore
        public /* final */ /* synthetic */ Rh b;

        @DexIgnore
        public Ai(Rh rh) {
            this.b = rh;
        }

        @DexIgnore
        @Override // com.fossil.Nx0
        public void bindBlob(int i, byte[] bArr) {
            this.b.bindBlob(i, bArr);
        }

        @DexIgnore
        @Override // com.fossil.Nx0
        public void bindDouble(int i, double d) {
            this.b.bindDouble(i, d);
        }

        @DexIgnore
        @Override // com.fossil.Nx0
        public void bindLong(int i, long j) {
            this.b.bindLong(i, j);
        }

        @DexIgnore
        @Override // com.fossil.Nx0
        public void bindNull(int i) {
            this.b.bindNull(i);
        }

        @DexIgnore
        @Override // com.fossil.Nx0
        public void bindString(int i, String str) {
            this.b.bindString(i, str);
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }
    }

    @DexIgnore
    public Rh(int i2) {
        this.h = i2;
        int i3 = i2 + 1;
        this.g = new int[i3];
        this.c = new long[i3];
        this.d = new double[i3];
        this.e = new String[i3];
        this.f = new byte[i3][];
    }

    @DexIgnore
    public static Rh f(String str, int i2) {
        synchronized (j) {
            Map.Entry<Integer, Rh> ceilingEntry = j.ceilingEntry(Integer.valueOf(i2));
            if (ceilingEntry != null) {
                j.remove(ceilingEntry.getKey());
                Rh value = ceilingEntry.getValue();
                value.k(str, i2);
                return value;
            }
            Rh rh = new Rh(i2);
            rh.k(str, i2);
            return rh;
        }
    }

    @DexIgnore
    public static Rh j(Ox0 ox0) {
        Rh f2 = f(ox0.b(), ox0.a());
        ox0.c(new Ai(f2));
        return f2;
    }

    @DexIgnore
    public static void l() {
        if (j.size() > 15) {
            Iterator<Integer> it = j.descendingKeySet().iterator();
            for (int size = j.size() - 10; size > 0; size--) {
                it.next();
                it.remove();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox0
    public int a() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.Ox0
    public String b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Nx0
    public void bindBlob(int i2, byte[] bArr) {
        this.g[i2] = 5;
        this.f[i2] = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Nx0
    public void bindDouble(int i2, double d2) {
        this.g[i2] = 3;
        this.d[i2] = d2;
    }

    @DexIgnore
    @Override // com.fossil.Nx0
    public void bindLong(int i2, long j2) {
        this.g[i2] = 2;
        this.c[i2] = j2;
    }

    @DexIgnore
    @Override // com.fossil.Nx0
    public void bindNull(int i2) {
        this.g[i2] = 1;
    }

    @DexIgnore
    @Override // com.fossil.Nx0
    public void bindString(int i2, String str) {
        this.g[i2] = 4;
        this.e[i2] = str;
    }

    @DexIgnore
    @Override // com.fossil.Ox0
    public void c(Nx0 nx0) {
        for (int i2 = 1; i2 <= this.i; i2++) {
            int i3 = this.g[i2];
            if (i3 == 1) {
                nx0.bindNull(i2);
            } else if (i3 == 2) {
                nx0.bindLong(i2, this.c[i2]);
            } else if (i3 == 3) {
                nx0.bindDouble(i2, this.d[i2]);
            } else if (i3 == 4) {
                nx0.bindString(i2, this.e[i2]);
            } else if (i3 == 5) {
                nx0.bindBlob(i2, this.f[i2]);
            }
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @DexIgnore
    public void h(Rh rh) {
        int a2 = rh.a() + 1;
        System.arraycopy(rh.g, 0, this.g, 0, a2);
        System.arraycopy(rh.c, 0, this.c, 0, a2);
        System.arraycopy(rh.e, 0, this.e, 0, a2);
        System.arraycopy(rh.f, 0, this.f, 0, a2);
        System.arraycopy(rh.d, 0, this.d, 0, a2);
    }

    @DexIgnore
    public void k(String str, int i2) {
        this.b = str;
        this.i = i2;
    }

    @DexIgnore
    public void m() {
        synchronized (j) {
            j.put(Integer.valueOf(this.h), this);
            l();
        }
    }
}
