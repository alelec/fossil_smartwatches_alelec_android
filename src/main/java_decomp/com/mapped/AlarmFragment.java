package com.mapped;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.N25;
import com.fossil.Nx5;
import com.fossil.Ox5;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmFragment extends Qv5 implements Ox5, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<N25> h;
    @DexIgnore
    public Nx5 i;
    @DexIgnore
    public /* final */ Si j; // = new Si(this);
    @DexIgnore
    public /* final */ Pi k; // = new Pi(this);
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return AlarmFragment.m;
        }

        @DexIgnore
        public final AlarmFragment b() {
            return new AlarmFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Bi(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.r;
            Wg6.b(flexibleTextView, "binding.dayFriday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(6, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Ci(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.t;
            Wg6.b(flexibleTextView, "binding.daySaturday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(7, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ N25 b;

        @DexIgnore
        public Di(AlarmFragment alarmFragment, N25 n25) {
            this.a = alarmFragment;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Nx5 L6 = AlarmFragment.L6(this.a);
            NumberPicker numberPicker2 = this.b.J;
            Wg6.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.I;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            L6.t(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ N25 b;

        @DexIgnore
        public Ei(AlarmFragment alarmFragment, N25 n25) {
            this.a = alarmFragment;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Nx5 L6 = AlarmFragment.L6(this.a);
            NumberPicker numberPicker2 = this.b.H;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.I;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            L6.t(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ N25 b;

        @DexIgnore
        public Fi(AlarmFragment alarmFragment, N25 n25) {
            this.a = alarmFragment;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Nx5 L6 = AlarmFragment.L6(this.a);
            NumberPicker numberPicker2 = this.b.H;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.J;
            Wg6.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            L6.t(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        public Gi(AlarmFragment alarmFragment) {
            this.b = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            AlarmFragment.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        public Hi(AlarmFragment alarmFragment) {
            this.b = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.s(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ N25 b;

        @DexIgnore
        public Ii(AlarmFragment alarmFragment, N25 n25) {
            this.a = alarmFragment;
            this.b = n25;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            AlarmFragment.L6(this.a).s(z);
            ConstraintLayout constraintLayout = this.b.q;
            Wg6.b(constraintLayout, "binding.clDaysRepeat");
            constraintLayout.setVisibility(z ? 0 : 8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        public Ji(AlarmFragment alarmFragment) {
            this.b = alarmFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            AlarmFragment.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Ki(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.u;
            Wg6.b(flexibleTextView, "binding.daySunday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(1, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Li(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.s;
            Wg6.b(flexibleTextView, "binding.dayMonday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Mi(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.w;
            Wg6.b(flexibleTextView, "binding.dayTuesday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(3, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Ni(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.x;
            Wg6.b(flexibleTextView, "binding.dayWednesday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(4, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ N25 c;

        @DexIgnore
        public Oi(AlarmFragment alarmFragment, N25 n25) {
            this.b = alarmFragment;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            Wg6.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            AlarmFragment alarmFragment = this.b;
            FlexibleTextView flexibleTextView = this.c.v;
            Wg6.b(flexibleTextView, "binding.dayThursday");
            alarmFragment.O6(flexibleTextView);
            this.b.R6(5, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Pi(AlarmFragment alarmFragment) {
            this.b = alarmFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            N25 n25 = (N25) AlarmFragment.K6(this.b).a();
            if (!(n25 == null || (flexibleEditText = n25.z) == null)) {
                flexibleEditText.requestLayout();
            }
            AlarmFragment.L6(this.b).r(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 50";
                N25 n252 = (N25) AlarmFragment.K6(this.b).a();
                if (n252 != null && (flexibleTextView = n252.B) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText b;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment c;

        @DexIgnore
        public Qi(FlexibleEditText flexibleEditText, AlarmFragment alarmFragment) {
            this.b = flexibleEditText;
            this.c = alarmFragment;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View n;
            int length;
            N25 n25 = (N25) AlarmFragment.K6(this.c).a();
            if (n25 != null && (flexibleTextView = n25.D) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.b;
                    Wg6.b(flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        length = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.b;
                        Wg6.b(flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            length = text2.length();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    Wg6.b(flexibleTextView, "counterTextView");
                    flexibleTextView.setText(length + " / 15");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                N25 n252 = (N25) AlarmFragment.K6(this.c).a();
                if (!(n252 == null || (n = n252.n()) == null)) {
                    n.requestFocus();
                }
                Wg6.b(flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText b;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment c;

        @DexIgnore
        public Ri(FlexibleEditText flexibleEditText, AlarmFragment alarmFragment) {
            this.b = flexibleEditText;
            this.c = alarmFragment;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View n;
            int length;
            N25 n25 = (N25) AlarmFragment.K6(this.c).a();
            if (n25 != null && (flexibleTextView = n25.B) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.b;
                    Wg6.b(flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        length = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.b;
                        Wg6.b(flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            length = text2.length();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    Wg6.b(flexibleTextView, "counterTextView");
                    flexibleTextView.setText(length + " / 50");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                N25 n252 = (N25) AlarmFragment.K6(this.c).a();
                if (!(n252 == null || (n = n252.n()) == null)) {
                    n.requestFocus();
                }
                Wg6.b(flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmFragment b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Si(AlarmFragment alarmFragment) {
            this.b = alarmFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            N25 n25 = (N25) AlarmFragment.K6(this.b).a();
            if (!(n25 == null || (flexibleEditText = n25.A) == null)) {
                flexibleEditText.requestLayout();
            }
            AlarmFragment.L6(this.b).u(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 15";
                N25 n252 = (N25) AlarmFragment.K6(this.b).a();
                if (n252 != null && (flexibleTextView = n252.D) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /*
    static {
        String simpleName = AlarmFragment.class.getSimpleName();
        Wg6.b(simpleName, "AlarmFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(AlarmFragment alarmFragment) {
        G37<N25> g37 = alarmFragment.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Nx5 L6(AlarmFragment alarmFragment) {
        Nx5 nx5 = alarmFragment.i;
        if (nx5 != null) {
            return nx5;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void B0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return m;
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void E(int i2) {
        int i3;
        int i4 = i2 / 60;
        if (i4 >= 12) {
            i3 = 1;
            i4 -= 12;
        } else {
            i3 = 0;
        }
        if (i4 == 0) {
            i4 = 12;
        }
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.H;
                Wg6.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i4);
                NumberPicker numberPicker2 = a2.J;
                Wg6.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i2 % 60);
                NumberPicker numberPicker3 = a2.I;
                Wg6.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i3);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        Nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.o();
            return false;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        S6((Nx5) obj);
    }

    @DexIgnore
    public final void O6(TextView textView) {
        Wg6.c(textView, "textView");
        FlexibleTextView flexibleTextView = (FlexibleTextView) textView;
        if (flexibleTextView.isSelected()) {
            flexibleTextView.m("flexible_tv_selected");
        } else {
            flexibleTextView.m("flexible_tv_unselected");
        }
    }

    @DexIgnore
    public final void P6(boolean z) {
        if (!z) {
            E6("add_alarm_view");
        } else {
            E6("edit_alarm_view");
        }
    }

    @DexIgnore
    public final void Q6(N25 n25) {
        FlexibleTextView flexibleTextView = n25.u;
        Wg6.b(flexibleTextView, "binding.daySunday");
        if (!flexibleTextView.isSelected()) {
            n25.u.m("flexible_tv_selected");
        } else {
            n25.u.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView2 = n25.s;
        Wg6.b(flexibleTextView2, "binding.dayMonday");
        if (!flexibleTextView2.isSelected()) {
            n25.s.m("flexible_tv_selected");
        } else {
            n25.s.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView3 = n25.w;
        Wg6.b(flexibleTextView3, "binding.dayTuesday");
        if (!flexibleTextView3.isSelected()) {
            n25.w.m("flexible_tv_selected");
        } else {
            n25.w.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView4 = n25.x;
        Wg6.b(flexibleTextView4, "binding.dayWednesday");
        if (!flexibleTextView4.isSelected()) {
            n25.x.m("flexible_tv_selected");
        } else {
            n25.x.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView5 = n25.v;
        Wg6.b(flexibleTextView5, "binding.dayThursday");
        if (!flexibleTextView5.isSelected()) {
            n25.v.m("flexible_tv_selected");
        } else {
            n25.v.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView6 = n25.r;
        Wg6.b(flexibleTextView6, "binding.dayFriday");
        if (!flexibleTextView6.isSelected()) {
            n25.r.m("flexible_tv_selected");
        } else {
            n25.r.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView7 = n25.t;
        Wg6.b(flexibleTextView7, "binding.daySaturday");
        if (!flexibleTextView7.isSelected()) {
            n25.t.m("flexible_tv_selected");
        } else {
            n25.t.m("flexible_tv_unselected");
        }
        n25.E.setOnClickListener(new Gi(this));
        n25.F.setOnClickListener(new Hi(this));
        n25.L.setOnCheckedChangeListener(new Ii(this, n25));
        n25.y.setOnClickListener(new Ji(this));
        n25.u.setOnClickListener(new Ki(this, n25));
        n25.s.setOnClickListener(new Li(this, n25));
        n25.w.setOnClickListener(new Mi(this, n25));
        n25.x.setOnClickListener(new Ni(this, n25));
        n25.v.setOnClickListener(new Oi(this, n25));
        n25.r.setOnClickListener(new Bi(this, n25));
        n25.t.setOnClickListener(new Ci(this, n25));
        NumberPicker numberPicker = n25.H;
        Wg6.b(numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = n25.H;
        Wg6.b(numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        n25.H.setOnValueChangedListener(new Di(this, n25));
        NumberPicker numberPicker3 = n25.J;
        Wg6.b(numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = n25.J;
        Wg6.b(numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        n25.J.setOnValueChangedListener(new Ei(this, n25));
        String c = Um5.c(PortfolioApp.get.instance(), 2131886102);
        Wg6.b(c, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
        if (c != null) {
            String upperCase = c.toUpperCase();
            Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886104);
            Wg6.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            if (c2 != null) {
                String upperCase2 = c2.toUpperCase();
                Wg6.b(upperCase2, "(this as java.lang.String).toUpperCase()");
                NumberPicker numberPicker5 = n25.I;
                Wg6.b(numberPicker5, "binding.numberPickerThree");
                numberPicker5.setMinValue(0);
                NumberPicker numberPicker6 = n25.I;
                Wg6.b(numberPicker6, "binding.numberPickerThree");
                numberPicker6.setMaxValue(1);
                n25.I.setDisplayedValues(new String[]{upperCase, upperCase2});
                n25.I.setOnValueChangedListener(new Fi(this, n25));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        Wg6.c(str, "tag");
        super.R5(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != 1038249436) {
                if (hashCode == 1185284775 && str.equals("CONFIRM_SET_ALARM_FAILED") && i2 == 2131362279 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            } else if (str.equals("CONFIRM_DELETE_ALARM") && i2 == 2131363373) {
                Nx5 nx5 = this.i;
                if (nx5 != null) {
                    nx5.n();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 != 2131363373) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                    return;
                }
                return;
            }
            Nx5 nx52 = this.i;
            if (nx52 != null) {
                nx52.p();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6(int i2, boolean z) {
        Nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.q(z, i2);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void S6(Nx5 nx5) {
        Wg6.c(nx5, "presenter");
        this.i = nx5;
    }

    @DexIgnore
    public final void T6(int i2, boolean z) {
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (a2 != null) {
                switch (i2) {
                    case 1:
                        FlexibleTextView flexibleTextView = a2.u;
                        Wg6.b(flexibleTextView, "binding.daySunday");
                        flexibleTextView.setSelected(z);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        Wg6.b(flexibleTextView2, "binding.daySunday");
                        O6(flexibleTextView2);
                        return;
                    case 2:
                        FlexibleTextView flexibleTextView3 = a2.s;
                        Wg6.b(flexibleTextView3, "binding.dayMonday");
                        flexibleTextView3.setSelected(z);
                        FlexibleTextView flexibleTextView4 = a2.s;
                        Wg6.b(flexibleTextView4, "binding.dayMonday");
                        O6(flexibleTextView4);
                        return;
                    case 3:
                        FlexibleTextView flexibleTextView5 = a2.w;
                        Wg6.b(flexibleTextView5, "binding.dayTuesday");
                        flexibleTextView5.setSelected(z);
                        FlexibleTextView flexibleTextView6 = a2.w;
                        Wg6.b(flexibleTextView6, "binding.dayTuesday");
                        O6(flexibleTextView6);
                        return;
                    case 4:
                        FlexibleTextView flexibleTextView7 = a2.x;
                        Wg6.b(flexibleTextView7, "binding.dayWednesday");
                        flexibleTextView7.setSelected(z);
                        FlexibleTextView flexibleTextView8 = a2.x;
                        Wg6.b(flexibleTextView8, "binding.dayWednesday");
                        O6(flexibleTextView8);
                        return;
                    case 5:
                        FlexibleTextView flexibleTextView9 = a2.v;
                        Wg6.b(flexibleTextView9, "binding.dayThursday");
                        flexibleTextView9.setSelected(z);
                        FlexibleTextView flexibleTextView10 = a2.v;
                        Wg6.b(flexibleTextView10, "binding.dayThursday");
                        O6(flexibleTextView10);
                        return;
                    case 6:
                        FlexibleTextView flexibleTextView11 = a2.r;
                        Wg6.b(flexibleTextView11, "binding.dayFriday");
                        flexibleTextView11.setSelected(z);
                        FlexibleTextView flexibleTextView12 = a2.r;
                        Wg6.b(flexibleTextView12, "binding.dayFriday");
                        O6(flexibleTextView12);
                        return;
                    case 7:
                        FlexibleTextView flexibleTextView13 = a2.t;
                        Wg6.b(flexibleTextView13, "binding.daySaturday");
                        flexibleTextView13.setSelected(z);
                        FlexibleTextView flexibleTextView14 = a2.t;
                        Wg6.b(flexibleTextView14, "binding.daySaturday");
                        O6(flexibleTextView14);
                        return;
                    default:
                        return;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void W2() {
        RTLImageView rTLImageView;
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (a2 != null && (rTLImageView = a2.F) != null) {
                rTLImageView.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void Y(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        FlexibleButton flexibleButton3;
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (!(a2 == null || (flexibleButton3 = a2.y) == null)) {
                flexibleButton3.setEnabled(z);
            }
            if (z) {
                G37<N25> g372 = this.h;
                if (g372 != null) {
                    N25 a3 = g372.a();
                    if (a3 != null && (flexibleButton2 = a3.y) != null) {
                        flexibleButton2.d("flexible_button_primary");
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            G37<N25> g373 = this.h;
            if (g373 != null) {
                N25 a4 = g373.a();
                if (a4 != null && (flexibleButton = a4.y) != null) {
                    flexibleButton.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void Y1(Alarm alarm, boolean z) {
        Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "showSetAlarmComplete: alarm = " + alarm + ", isSuccess = " + z);
        if (z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.q0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void Z4(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "startFireBaseTracer(), isAddNewAlarm = " + z);
        P6(z);
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void b5(SparseIntArray sparseIntArray) {
        Wg6.c(sparseIntArray, "daysRepeat");
        int size = sparseIntArray.size();
        int i2 = 1;
        while (i2 <= 7) {
            if (size == 0) {
                T6(i2, false);
            } else {
                T6(i2, sparseIntArray.get(i2) == i2);
            }
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void l2(String str) {
        FlexibleEditText flexibleEditText;
        Wg6.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "showMessage, value = " + str);
        if (!Vt7.l(str)) {
            G37<N25> g37 = this.h;
            if (g37 != null) {
                N25 a2 = g37.a();
                if (a2 != null && (flexibleEditText = a2.z) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void m2(boolean z) {
        ConstraintLayout constraintLayout;
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (!(a2 == null || (flexibleSwitchCompat = a2.L) == null)) {
                flexibleSwitchCompat.setChecked(z);
            }
            G37<N25> g372 = this.h;
            if (g372 != null) {
                N25 a3 = g372.a();
                if (a3 != null && (constraintLayout = a3.q) != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FlexibleEditText flexibleEditText;
        FlexibleEditText flexibleEditText2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        N25 n25 = (N25) Aq0.f(layoutInflater, 2131558503, viewGroup, false, A6());
        Wg6.b(n25, "binding");
        Q6(n25);
        G37<N25> g37 = new G37<>(this, n25);
        this.h = g37;
        N25 a2 = g37.a();
        if (!(a2 == null || (flexibleEditText2 = a2.A) == null)) {
            flexibleEditText2.addTextChangedListener(this.j);
            flexibleEditText2.setOnFocusChangeListener(new Qi(flexibleEditText2, this));
        }
        G37<N25> g372 = this.h;
        if (g372 != null) {
            N25 a3 = g372.a();
            if (!(a3 == null || (flexibleEditText = a3.z) == null)) {
                flexibleEditText.addTextChangedListener(this.k);
                flexibleEditText.setOnFocusChangeListener(new Ri(flexibleEditText, this));
            }
            G37<N25> g373 = this.h;
            if (g373 != null) {
                N25 a4 = g373.a();
                if (a4 != null) {
                    Wg6.b(a4, "mBinding.get()!!");
                    return a4.n();
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        G37<N25> g37 = this.h;
        if (g37 != null) {
            N25 a2 = g37.a();
            if (a2 != null) {
                FlexibleEditText flexibleEditText = a2.A;
                if (flexibleEditText != null) {
                    flexibleEditText.removeTextChangedListener(this.j);
                }
                a2.z.removeTextChangedListener(this.k);
            }
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void q4() {
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.y0(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void s5() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ox5
    public void t(String str) {
        FlexibleEditText flexibleEditText;
        Wg6.c(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "showTitle, value = " + str);
        if (!Vt7.l(str)) {
            G37<N25> g37 = this.h;
            if (g37 != null) {
                N25 a2 = g37.a();
                if (a2 != null && (flexibleEditText = a2.A) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
