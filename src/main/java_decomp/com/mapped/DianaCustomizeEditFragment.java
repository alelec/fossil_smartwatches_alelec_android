package com.mapped;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.G67;
import com.fossil.H85;
import com.fossil.Hl5;
import com.fossil.Hs5;
import com.fossil.Is5;
import com.fossil.Jn5;
import com.fossil.M66;
import com.fossil.N66;
import com.fossil.Po4;
import com.fossil.Qb6;
import com.fossil.Qv5;
import com.fossil.R66;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Ub6;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vs0;
import com.fossil.Yb6;
import com.fossil.Zb6;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeEditFragment extends Qv5 implements Zb6, AlertDialogFragment.Gi, View.OnClickListener {
    @DexIgnore
    public Yb6 h;
    @DexIgnore
    public G37<H85> i;
    @DexIgnore
    public /* final */ ArrayList<Fragment> j; // = new ArrayList<>();
    @DexIgnore
    public Ub6 k;
    @DexIgnore
    public MicroAppPresenter l;
    @DexIgnore
    public Po4 m;
    @DexIgnore
    public HybridCustomizeViewModel s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                Wg6.b(activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.D;
                Wg6.b(stringExtra, "presetId");
                Wg6.b(stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            H85 a2 = this.a.O6().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.D;
                Wg6.b(flexibleTextView, "binding.tvPresetName");
                flexibleTextView.setEllipsize(TextUtils.TruncateAt.END);
                a2.D.setSingleLine(true);
                FlexibleTextView flexibleTextView2 = a2.D;
                Wg6.b(flexibleTextView2, "binding.tvPresetName");
                flexibleTextView2.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            ViewPropertyAnimator duration;
            ViewPropertyAnimator duration2;
            ViewPropertyAnimator duration3;
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            H85 a2 = this.a.O6().a();
            if (a2 != null) {
                Is5 is5 = Is5.a;
                H85 a3 = this.a.O6().a();
                if (a3 != null) {
                    CardView cardView = a3.t;
                    Wg6.b(cardView, "mBinding.get()!!.cvGroup");
                    is5.d(cardView);
                    ViewPropertyAnimator animate = a2.G.animate();
                    if (!(animate == null || (duration3 = animate.setDuration(500)) == null)) {
                        duration3.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate2 = a2.F.animate();
                    if (!(animate2 == null || (duration2 = animate2.setDuration(500)) == null)) {
                        duration2.alpha(1.0f);
                    }
                    ViewPropertyAnimator animate3 = a2.E.animate();
                    if (animate3 != null && (duration = animate3.setDuration(500)) != null) {
                        duration.alpha(1.0f);
                        return;
                    }
                    return;
                }
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements R66.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public Bi(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void a(String str) {
            Wg6.c(str, "label");
            this.a.M6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void b() {
            this.a.K6(ViewHierarchy.DIMENSION_TOP_KEY);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void c(String str) {
            Wg6.c(str, "label");
            this.a.L6(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean d(String str) {
            Wg6.c(str, "fromPos");
            this.a.U6(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean e(View view, String str) {
            Wg6.c(view, "view");
            Wg6.c(str, "id");
            return this.a.N6(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements R66.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public Ci(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void a(String str) {
            Wg6.c(str, "label");
            this.a.M6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void b() {
            this.a.K6("middle");
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void c(String str) {
            Wg6.c(str, "label");
            this.a.L6("middle", str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean d(String str) {
            Wg6.c(str, "fromPos");
            this.a.U6(str, "middle");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean e(View view, String str) {
            Wg6.c(view, "view");
            Wg6.c(str, "id");
            return this.a.N6("middle", view, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements R66.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeEditFragment a;

        @DexIgnore
        public Di(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            this.a = dianaCustomizeEditFragment;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void a(String str) {
            Wg6.c(str, "label");
            this.a.M6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void b() {
            this.a.K6("bottom");
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public void c(String str) {
            Wg6.c(str, "label");
            this.a.L6("bottom", str);
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean d(String str) {
            Wg6.c(str, "fromPos");
            this.a.U6(str, "bottom");
            return true;
        }

        @DexIgnore
        @Override // com.fossil.R66.Ai
        public boolean e(View view, String str) {
            Wg6.c(view, "view");
            Wg6.c(str, "id");
            return this.a.N6("bottom", view, str);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        Yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.o();
            return false;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void K6(String str) {
        Wg6.c(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.J();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                Ub6 ub6 = this.k;
                if (ub6 != null) {
                    ub6.L6();
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void L() {
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558482);
        fi.e(2131363317, Um5.c(PortfolioApp.get.instance(), 2131886536));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886534));
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886535));
        fi.b(2131363291);
        fi.b(2131363373);
        fi.k(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public final void L6(String str, String str2) {
        Wg6.c(str, "position");
        Wg6.c(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.I();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.I();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.I();
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        S6((Yb6) obj);
    }

    @DexIgnore
    public final void M6(String str, String str2) {
        Wg6.c(str, "position");
        Wg6.c(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str.equals("bottom")) {
                    a2.I.J();
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final boolean N6(String str, View view, String str2) {
        Wg6.c(str, "position");
        Wg6.c(view, "view");
        Wg6.c(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.K.J();
                    }
                } else if (str.equals("middle")) {
                    a2.J.J();
                }
            } else if (str.equals("bottom")) {
                a2.I.J();
            }
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            a2.I.setDragMode(false);
            Ub6 ub6 = this.k;
            if (ub6 != null) {
                ub6.L6();
            }
            Yb6 yb6 = this.h;
            if (yb6 != null) {
                yb6.n(str2, str);
                return true;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final G37<H85> O6() {
        G37<H85> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void P() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Wg6.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.get.instance().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    public final Transition P6(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        Wg6.b(window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new Ai(this));
    }

    @DexIgnore
    public final void Q6(String str) {
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.setSelectedWc(true);
                            a2.J.setSelectedWc(false);
                            a2.I.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.K.setSelectedWc(false);
                        a2.J.setSelectedWc(true);
                        a2.I.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.K.setSelectedWc(false);
                    a2.J.setSelectedWc(false);
                    a2.I.setSelectedWc(true);
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i2 == 2131363291) {
                    t0(false);
                    return;
                } else if (i2 == 2131363373) {
                    Yb6 yb6 = this.h;
                    if (yb6 != null) {
                        yb6.q(true);
                        return;
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i2 == 2131363373 && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (Wg6.a(stringExtra2, MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                Jn5 jn5 = Jn5.b;
                Context context = getContext();
                if (context != null) {
                    Jn5.c(jn5, context, Hl5.a.d(stringExtra2), false, false, false, null, 60, null);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Yb6 yb62 = this.h;
                if (yb62 != null) {
                    Wg6.b(stringExtra, "toPos");
                    yb62.p(stringExtra);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        Ub6 ub6 = (Ub6) getChildFragmentManager().Z("MicroAppFragment");
        this.k = ub6;
        if (ub6 == null) {
            this.k = new Ub6();
        }
        Ub6 ub62 = this.k;
        if (ub62 != null) {
            this.j.add(ub62);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        Ub6 ub63 = this.k;
        if (ub63 != null) {
            iface.O0(new Qb6(ub63)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    public void S6(Yb6 yb6) {
        Wg6.c(yb6, "presenter");
        this.h = yb6;
    }

    @DexIgnore
    public final void T6() {
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.K;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                Wg6.b(putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.X(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new R66(new Bi(this)), null, 8, null);
                CustomizeWidget customizeWidget2 = a2.J;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                Wg6.b(putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.X(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new R66(new Ci(this)), null, 8, null);
                CustomizeWidget customizeWidget3 = a2.I;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                Wg6.b(putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.X(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new R66(new Di(this)), null, 8, null);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U6(String str, String str2) {
        Wg6.c(str, "fromPosition");
        Wg6.c(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.K.J();
                        }
                    } else if (str2.equals("middle")) {
                        a2.J.J();
                    }
                } else if (str2.equals("bottom")) {
                    a2.I.J();
                }
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                a2.I.setDragMode(false);
                Yb6 yb6 = this.h;
                if (yb6 != null) {
                    yb6.s(str, str2);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void W6(CustomizeWidget customizeWidget, FlexibleTextView flexibleTextView, N66 n66) {
        if (n66 != null) {
            flexibleTextView.setText(n66.b());
            customizeWidget.S(n66.a());
            customizeWidget.T();
            V6(customizeWidget, n66.a());
            return;
        }
        flexibleTextView.setText("");
        customizeWidget.S("empty");
        customizeWidget.T();
        V6(customizeWidget, "empty");
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void X4(String str, String str2, String str3) {
        Wg6.c(str, "message");
        Wg6.c(str2, "microAppId");
        Wg6.c(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558480);
            fi.e(2131363317, str);
            fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886792));
            fi.b(2131363373);
            fi.m(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void e1(M66 m66) {
        Object obj;
        Object obj2;
        Object obj3;
        Wg6.c(m66, "data");
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + m66.a());
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(m66.a());
                FlexibleTextView flexibleTextView = a2.D;
                Wg6.b(flexibleTextView, "it.tvPresetName");
                String d = m66.d();
                if (d != null) {
                    String upperCase = d.toUpperCase();
                    Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    Iterator it = arrayList.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        Object next = it.next();
                        if (Wg6.a(((N66) next).c(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                            obj = next;
                            break;
                        }
                    }
                    N66 n66 = (N66) obj;
                    Iterator it2 = arrayList.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj2 = null;
                            break;
                        }
                        Object next2 = it2.next();
                        if (Wg6.a(((N66) next2).c(), "middle")) {
                            obj2 = next2;
                            break;
                        }
                    }
                    N66 n662 = (N66) obj2;
                    Iterator it3 = arrayList.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            obj3 = null;
                            break;
                        }
                        Object next3 = it3.next();
                        if (Wg6.a(((N66) next3).c(), "bottom")) {
                            obj3 = next3;
                            break;
                        }
                    }
                    CustomizeWidget customizeWidget = a2.K;
                    Wg6.b(customizeWidget, "it.waTop");
                    FlexibleTextView flexibleTextView2 = a2.G;
                    Wg6.b(flexibleTextView2, "it.tvWaTop");
                    W6(customizeWidget, flexibleTextView2, n66);
                    CustomizeWidget customizeWidget2 = a2.J;
                    Wg6.b(customizeWidget2, "it.waMiddle");
                    FlexibleTextView flexibleTextView3 = a2.F;
                    Wg6.b(flexibleTextView3, "it.tvWaMiddle");
                    W6(customizeWidget2, flexibleTextView3, n662);
                    CustomizeWidget customizeWidget3 = a2.I;
                    Wg6.b(customizeWidget3, "it.waBottom");
                    FlexibleTextView flexibleTextView4 = a2.E;
                    Wg6.b(flexibleTextView4, "it.tvWaBottom");
                    W6(customizeWidget3, flexibleTextView4, (N66) obj3);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            Po4 po4 = this.m;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(hybridCustomizeEditActivity, po4).a(HybridCustomizeViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                HybridCustomizeViewModel hybridCustomizeViewModel = (HybridCustomizeViewModel) a2;
                this.s = hybridCustomizeViewModel;
                Yb6 yb6 = this.h;
                if (yb6 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (hybridCustomizeViewModel != null) {
                    yb6.r(hybridCustomizeViewModel);
                } else {
                    Wg6.n("mShareViewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModelFactory");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 111 && i3 == 1 && Jn5.b.m(getContext(), Jn5.Ci.BLUETOOTH_CONNECTION)) {
            Yb6 yb6 = this.h;
            if (yb6 != null) {
                yb6.q(false);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362528) {
                Yb6 yb6 = this.h;
                if (yb6 != null) {
                    yb6.q(true);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else if (id != 2131363291) {
                switch (id) {
                    case 2131363529:
                        Yb6 yb62 = this.h;
                        if (yb62 != null) {
                            yb62.p("bottom");
                            return;
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    case 2131363530:
                        Yb6 yb63 = this.h;
                        if (yb63 != null) {
                            yb63.p("middle");
                            return;
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    case 2131363531:
                        Yb6 yb64 = this.h;
                        if (yb64 != null) {
                            yb64.p(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                Yb6 yb65 = this.h;
                if (yb65 != null) {
                    yb65.o();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        H85 h85 = (H85) Aq0.f(layoutInflater, 2131558580, viewGroup, false, A6());
        R6();
        this.i = new G37<>(this, h85);
        Wg6.b(h85, "binding");
        return h85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Yb6 yb6 = this.h;
        if (yb6 != null) {
            yb6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Wg6.b(activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            Wg6.b(window, "activity.window");
            window.setEnterTransition(Is5.a.b());
            Window window2 = activity.getWindow();
            Wg6.b(window2, "activity.window");
            window2.setSharedElementEnterTransition(Is5.a.c(PortfolioApp.get.instance()));
            Intent intent = activity.getIntent();
            Wg6.b(intent, "activity.intent");
            activity.setEnterSharedElementCallback(new Hs5(intent, PortfolioApp.get.instance()));
            P6(activity);
        }
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 != null) {
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (!TextUtils.isEmpty(d)) {
                    a2.H.setBackgroundColor(Color.parseColor(d));
                    a2.t.setBackgroundColor(Color.parseColor(d));
                }
                a2.K.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.C.setOnClickListener(this);
                a2.u.setOnClickListener(this);
                ViewPager2 viewPager2 = a2.B;
                Wg6.b(viewPager2, "it.rvPreset");
                viewPager2.setAdapter(new G67(getChildFragmentManager(), this.j));
                ViewPager2 viewPager22 = a2.B;
                Wg6.b(viewPager22, "it.rvPreset");
                viewPager22.setUserInputEnabled(false);
                if (a2.B.getChildAt(0) != null) {
                    View childAt = a2.B.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(2);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
            }
            T6();
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void s0(boolean z) {
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.u;
                Wg6.b(imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.u;
                Wg6.b(imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.u.setBackgroundResource(2131231291);
                return;
            }
            ImageButton imageButton3 = a2.u;
            Wg6.b(imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.u;
            Wg6.b(imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.u.setBackgroundResource(2131231292);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void t0(boolean z) {
        FlexibleTextView flexibleTextView;
        G37<H85> g37 = this.i;
        if (g37 != null) {
            H85 a2 = g37.a();
            if (!(a2 == null || (flexibleTextView = a2.D) == null)) {
                flexibleTextView.setSingleLine(false);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void u4(String str) {
        Wg6.c(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        Q6(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void w() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void y() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886814);
        Wg6.b(c, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        H6(c);
    }

    @DexIgnore
    @Override // com.fossil.Zb6
    public void y1() {
        ImageView imageView;
        if (isActive()) {
            G37<H85> g37 = this.i;
            if (g37 != null) {
                H85 a2 = g37.a();
                if (a2 != null && (imageView = a2.w) != null) {
                    imageView.setImageResource(2131230975);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }
}
