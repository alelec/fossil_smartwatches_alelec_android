package com.mapped;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.fossil.Ry5;
import com.fossil.Tm5;
import com.fossil.Vk5;
import com.fossil.Ym5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileHelper {
    @DexIgnore
    public static /* final */ FileHelper a; // = new FileHelper();

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            if (str != null) {
                return new File(str).exists();
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0071, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0072, code lost:
        com.fossil.So7.a(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0075, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b(java.lang.String r5, com.misfit.frameworks.buttonservice.model.FileType r6) {
        /*
            r4 = this;
            java.lang.String r0 = "nameFile"
            com.mapped.Wg6.c(r5, r0)
            java.lang.String r0 = "type"
            com.mapped.Wg6.c(r6, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "open nameFile "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r2 = "FileHelper"
            java.lang.String r1 = r1.toString()
            r0.d(r2, r1)
            boolean r0 = android.text.TextUtils.isEmpty(r5)
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x002f
        L_0x002e:
            return r1
        L_0x002f:
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get     // Catch:{ Exception -> 0x0076 }
            com.portfolio.platform.PortfolioApp r0 = r0.instance()     // Catch:{ Exception -> 0x0076 }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r0, r6)     // Catch:{ Exception -> 0x0076 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0076 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0076 }
            r3.<init>()     // Catch:{ Exception -> 0x0076 }
            r3.append(r0)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = java.io.File.separator     // Catch:{ Exception -> 0x0076 }
            r3.append(r0)     // Catch:{ Exception -> 0x0076 }
            r3.append(r5)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0076 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0076 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0076 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0076 }
            byte[] r0 = com.fossil.B68.g(r3)     // Catch:{ all -> 0x006f }
            r2 = 0
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r2)     // Catch:{ all -> 0x006f }
            r2 = 0
            com.fossil.So7.a(r3, r2)
        L_0x0068:
            java.lang.String r1 = "try {\n                va\u2026         \"\"\n            }"
            com.mapped.Wg6.b(r0, r1)
            r1 = r0
            goto L_0x002e
        L_0x006f:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0071 }
        L_0x0071:
            r2 = move-exception
            com.fossil.So7.a(r3, r0)
            throw r2
        L_0x0076:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r1
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.FileHelper.b(java.lang.String, com.misfit.frameworks.buttonservice.model.FileType):java.lang.String");
    }

    @DexIgnore
    public final Bitmap c(String str, int i, int i2, FileType fileType) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return Vk5.b(new FileInputStream(new File(FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), fileType) + File.separator + Ym5.b(str))), i, i2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Drawable d(String str, int i, int i2, FileType fileType) {
        BitmapDrawable bitmapDrawable;
        Wg6.c(fileType, "type");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable c = Tm5.d().c(str);
        if (c != null) {
            return c;
        }
        try {
            bitmapDrawable = new BitmapDrawable(PortfolioApp.get.instance().getResources(), c(str, i, i2, fileType));
            Tm5.d().h(str, bitmapDrawable);
        } catch (Exception e) {
            e.printStackTrace();
            bitmapDrawable = null;
        }
        return bitmapDrawable;
    }

    @DexIgnore
    public final void e(String str, String str2, Bitmap bitmap) {
        Wg6.c(str, "directory");
        Wg6.c(str2, "fileName");
        Wg6.c(bitmap, "bitmap");
        FLogger.INSTANCE.getLocal().e("FileHelper", "storeBackgroundPhoto() - bitmap");
        if (new File(str).exists()) {
            Ry5.H(PortfolioApp.get.instance(), str, str2, bitmap);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023 A[Catch:{ IOException -> 0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002c A[Catch:{ IOException -> 0x0030 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean f(android.graphics.Bitmap r4, java.lang.String r5) {
        /*
            r3 = this;
            r2 = 0
            java.lang.String r0 = "body"
            com.mapped.Wg6.c(r4, r0)
            java.lang.String r0 = "path"
            com.mapped.Wg6.c(r5, r0)
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0032, all -> 0x0028 }
            r1.<init>(r5)     // Catch:{ IOException -> 0x0032, all -> 0x0028 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ IOException -> 0x001f, all -> 0x0035 }
            r2 = 100
            r4.compress(r0, r2, r1)     // Catch:{ IOException -> 0x001f, all -> 0x0035 }
            r1.flush()     // Catch:{ IOException -> 0x001f, all -> 0x0035 }
            r1.close()     // Catch:{ IOException -> 0x0030 }
            r0 = 1
        L_0x001e:
            return r0
        L_0x001f:
            r0 = move-exception
            r0 = r1
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.close()     // Catch:{ IOException -> 0x0030 }
        L_0x0026:
            r0 = 0
            goto L_0x001e
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0030 }
        L_0x002f:
            throw r0     // Catch:{ IOException -> 0x0030 }
        L_0x0030:
            r0 = move-exception
            goto L_0x0026
        L_0x0032:
            r0 = move-exception
            r0 = r2
            goto L_0x0021
        L_0x0035:
            r0 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.FileHelper.f(android.graphics.Bitmap, java.lang.String):boolean");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean g(com.fossil.W18 r7, java.lang.String r8) {
        /*
            r6 = this;
            r3 = 0
            r1 = 0
            java.lang.String r0 = "body"
            com.mapped.Wg6.c(r7, r0)
            java.lang.String r0 = "path"
            com.mapped.Wg6.c(r8, r0)
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r0]     // Catch:{ IOException -> 0x005d, all -> 0x0058 }
            java.io.InputStream r4 = r7.byteStream()     // Catch:{ IOException -> 0x005d, all -> 0x0058 }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0055, all -> 0x0052 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x0055, all -> 0x0052 }
            if (r4 == 0) goto L_0x0022
        L_0x001b:
            int r1 = r4.read(r2)     // Catch:{ IOException -> 0x0034, all -> 0x0042 }
            r5 = -1
            if (r1 != r5) goto L_0x002f
        L_0x0022:
            r0.flush()     // Catch:{ IOException -> 0x0034, all -> 0x0042 }
            if (r4 == 0) goto L_0x002a
            r4.close()     // Catch:{ IOException -> 0x0050 }
        L_0x002a:
            r0.close()     // Catch:{ IOException -> 0x0050 }
            r0 = 1
        L_0x002e:
            return r0
        L_0x002f:
            r5 = 0
            r0.write(r2, r5, r1)
            goto L_0x001b
        L_0x0034:
            r1 = move-exception
        L_0x0035:
            r2 = r4
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()
        L_0x003b:
            if (r0 == 0) goto L_0x0040
            r0.close()
        L_0x0040:
            r0 = r3
            goto L_0x002e
        L_0x0042:
            r1 = move-exception
            r2 = r1
        L_0x0044:
            r5 = r0
        L_0x0045:
            if (r4 == 0) goto L_0x004a
            r4.close()
        L_0x004a:
            if (r5 == 0) goto L_0x004f
            r5.close()
        L_0x004f:
            throw r2
        L_0x0050:
            r0 = move-exception
            goto L_0x0040
        L_0x0052:
            r2 = move-exception
            r0 = r1
            goto L_0x0044
        L_0x0055:
            r0 = move-exception
            r0 = r1
            goto L_0x0035
        L_0x0058:
            r0 = move-exception
            r4 = r1
            r5 = r1
            r2 = r0
            goto L_0x0045
        L_0x005d:
            r0 = move-exception
            r2 = r1
            r0 = r1
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.FileHelper.g(com.fossil.W18, java.lang.String):boolean");
    }
}
