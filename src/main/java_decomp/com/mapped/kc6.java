package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kc6 extends RuntimeException {
    @DexIgnore
    public Kc6() {
    }

    @DexIgnore
    public Kc6(String str) {
        super(str);
    }

    @DexIgnore
    public Kc6(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public Kc6(Throwable th) {
        super(th);
    }
}
