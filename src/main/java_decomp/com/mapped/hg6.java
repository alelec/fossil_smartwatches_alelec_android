package com.mapped;

import com.fossil.Uk7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Hg6<P1, R> extends Uk7<R> {
    @DexIgnore
    R invoke(P1 p1);
}
