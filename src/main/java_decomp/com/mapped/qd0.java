package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.G80;
import com.fossil.Hy1;
import com.fossil.Ix1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import com.fossil.P90;
import com.fossil.Vu1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qd0 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Vu1 c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ short g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ short i;
    @DexIgnore
    public /* final */ byte j;
    @DexIgnore
    public /* final */ Td0 k;
    @DexIgnore
    public /* final */ Ud0 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Qd0> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qd0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                Wg6.b(createByteArray, "parcel.createByteArray()!!");
                Parcelable readParcelable = parcel.readParcelable(Vu1.class.getClassLoader());
                if (readParcelable != null) {
                    return new Qd0(createByteArray, (Vu1) readParcelable);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Qd0[] newArray(int i) {
            return new Qd0[i];
        }
    }

    @DexIgnore
    public Qd0(byte[] bArr, Vu1 vu1) {
        boolean z = true;
        this.b = bArr;
        this.c = vu1;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.d = order.get(0);
        this.e = order.get(1);
        this.f = order.get(2);
        this.g = order.getShort(3);
        this.h = order.getShort(7) <= 0 ? false : z;
        this.i = Hy1.p(order.get(9));
        this.j = order.get(10);
        this.k = P90.b.a(this.g);
        this.l = P90.b.b(this.g);
    }

    @DexIgnore
    public final short a() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Qd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Qd0 qd0 = (Qd0) obj;
            return Arrays.equals(this.b, qd0.b) && !(Wg6.a(this.c, qd0.c) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration");
    }

    @DexIgnore
    public final Vu1 getCustomization() {
        return this.c;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.b;
    }

    @DexIgnore
    public final boolean getHasCustomization() {
        return this.h;
    }

    @DexIgnore
    public final byte getMajorVersion() {
        return this.d;
    }

    @DexIgnore
    public final Td0 getMicroAppId() {
        return this.k;
    }

    @DexIgnore
    public final Ud0 getMicroAppVariantId() {
        return this.l;
    }

    @DexIgnore
    public final byte getMicroAppVersion() {
        return this.f;
    }

    @DexIgnore
    public final byte getMinorVersion() {
        return this.e;
    }

    @DexIgnore
    public final byte getVariationNumber() {
        return this.j;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Arrays.hashCode(this.b);
        Vu1 vu1 = this.c;
        return (vu1 != null ? vu1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.F3, this.k.name()), Jd0.t3, Byte.valueOf(this.f)), Jd0.G3, Byte.valueOf(this.e)), Jd0.H3, Byte.valueOf(this.d)), Jd0.I3, Byte.valueOf(this.j)), Jd0.J3, this.l), Jd0.K3, Short.valueOf(this.i)), Jd0.L3, Boolean.valueOf(this.h)), Jd0.M3, Long.valueOf(Ix1.a.b(this.b, Ix1.Ai.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i2);
        }
    }
}
