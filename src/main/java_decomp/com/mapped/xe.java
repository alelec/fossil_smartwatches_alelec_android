package com.mapped;

import com.fossil.Bu0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xe<Key, Value> {
    @DexIgnore
    public AtomicBoolean mInvalid; // = new AtomicBoolean(false);
    @DexIgnore
    public CopyOnWriteArrayList<Ci> mOnInvalidatedCallbacks; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements V3<List<X>, List<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ V3 a;

        @DexIgnore
        public Ai(V3 v3) {
            this.a = v3;
        }

        @DexIgnore
        public List<Y> a(List<X> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.a.apply(list.get(i)));
            }
            return arrayList;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<Key, Value> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Bi<Key, ToValue> {
            @DexIgnore
            public /* final */ /* synthetic */ V3 a;

            @DexIgnore
            public Aii(V3 v3) {
                this.a = v3;
            }

            @DexIgnore
            @Override // com.mapped.Xe.Bi
            public Xe<Key, ToValue> create() {
                return Bi.this.create().mapByPage(this.a);
            }
        }

        @DexIgnore
        public abstract Xe<Key, Value> create();

        @DexIgnore
        public <ToValue> Bi<Key, ToValue> map(V3<Value, ToValue> v3) {
            return mapByPage(Xe.createListFunction(v3));
        }

        @DexIgnore
        public <ToValue> Bi<Key, ToValue> mapByPage(V3<List<Value>, List<ToValue>> v3) {
            return new Aii(v3);
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Xe b;
        @DexIgnore
        public /* final */ Bu0.Ai<T> c;
        @DexIgnore
        public /* final */ Object d; // = new Object();
        @DexIgnore
        public Executor e; // = null;
        @DexIgnore
        public boolean f; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Bu0 b;

            @DexIgnore
            public Aii(Bu0 bu0) {
                this.b = bu0;
            }

            @DexIgnore
            public void run() {
                Di di = Di.this;
                di.c.a(di.a, this.b);
            }
        }

        @DexIgnore
        public Di(Xe xe, int i, Executor executor, Bu0.Ai<T> ai) {
            this.b = xe;
            this.a = i;
            this.e = executor;
            this.c = ai;
        }

        @DexIgnore
        public static void d(List<?> list, int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        public boolean a() {
            if (!this.b.isInvalid()) {
                return false;
            }
            b(Bu0.b());
            return true;
        }

        @DexIgnore
        public void b(Bu0<T> bu0) {
            Executor executor;
            synchronized (this.d) {
                if (!this.f) {
                    this.f = true;
                    executor = this.e;
                } else {
                    throw new IllegalStateException("callback.onResult already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new Aii(bu0));
            } else {
                this.c.a(this.a, bu0);
            }
        }

        @DexIgnore
        public void c(Executor executor) {
            synchronized (this.d) {
                this.e = executor;
            }
        }
    }

    @DexIgnore
    public static <A, B> List<B> convert(V3<List<A>, List<B>> v3, List<A> list) {
        List<B> apply = v3.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new IllegalStateException("Invalid Function " + v3 + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> V3<List<X>, List<Y>> createListFunction(V3<X, Y> v3) {
        return new Ai(v3);
    }

    @DexIgnore
    public void addInvalidatedCallback(Ci ci) {
        this.mOnInvalidatedCallbacks.add(ci);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            Iterator<Ci> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> Xe<Key, ToValue> map(V3<Value, ToValue> v3);

    @DexIgnore
    public abstract <ToValue> Xe<Key, ToValue> mapByPage(V3<List<Value>, List<ToValue>> v3);

    @DexIgnore
    public void removeInvalidatedCallback(Ci ci) {
        this.mOnInvalidatedCallbacks.remove(ci);
    }
}
