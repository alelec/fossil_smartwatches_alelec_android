package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum I70 {
    TWELVE(0),
    TWENTY_FOUR(1);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final I70 a(int i) {
            I70[] values = I70.values();
            for (I70 i70 : values) {
                if (i70.a() == i) {
                    return i70;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public I70(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
