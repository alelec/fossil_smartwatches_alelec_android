package com.mapped;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.H42;
import com.fossil.Oh2;
import com.fossil.Ph2;
import com.fossil.R62;
import com.fossil.T62;
import com.fossil.Z52;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kk4 {
    @DexIgnore
    public static /* final */ String c; // = "uk5";
    @DexIgnore
    public static R62 d;
    @DexIgnore
    public static Di e;
    @DexIgnore
    public An4 a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements R62.Ci {
        @DexIgnore
        public Ai(Kk4 kk4) {
        }

        @DexIgnore
        @Override // com.fossil.R72
        public void n(Z52 z52) {
            Di di = Kk4.e;
            if (di != null) {
                di.c(z52);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements R62.Bi {
        @DexIgnore
        public Bi(Kk4 kk4) {
        }

        @DexIgnore
        @Override // com.fossil.K72
        public void d(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(Kk4.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(Kk4.c, "Connection lost.  Reason: Service Disconnected");
            }
        }

        @DexIgnore
        @Override // com.fossil.K72
        public void e(Bundle bundle) {
            Di di = Kk4.e;
            if (di != null) {
                di.b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void run() {
            T62<Status> a2 = Oh2.d.a(Kk4.d);
            Di di = Kk4.e;
            if (di != null) {
                di.f(a2);
            }
            Kk4.this.a.h1(false);
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void c(Z52 z52);

        @DexIgnore
        void f(T62<Status> t62);
    }

    @DexIgnore
    public Kk4(Context context, Executor executor, An4 an4) {
        if (d == null) {
            R62.Ai ai = new R62.Ai(context);
            ai.c(Oh2.b, new Scope[0]);
            ai.c(Oh2.a, new Scope[0]);
            ai.c(Oh2.c, new Scope[0]);
            ai.f(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            ai.d(new Bi(this));
            ai.e(new Ai(this));
            d = ai.g();
        }
        this.a = an4;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.f();
        this.a.h1(true);
    }

    @DexIgnore
    public void b() {
        d.g();
        this.a.h1(false);
    }

    @DexIgnore
    public final Ph2 c() {
        Ph2.Ai b2 = Ph2.b();
        b2.a(DataType.k, 1);
        return b2.b();
    }

    @DexIgnore
    public boolean d() {
        return H42.c(H42.b(PortfolioApp.d0), c());
    }

    @DexIgnore
    public boolean e() {
        return d.n() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.a.q();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new Ci());
    }

    @DexIgnore
    public void h(Fragment fragment) {
        H42.e(fragment, 1, H42.b(PortfolioApp.d0), c());
    }

    @DexIgnore
    public void i(Di di) {
        e = di;
    }

    @DexIgnore
    public void j() {
        try {
            b();
            H42.a(PortfolioApp.d0, new GoogleSignInOptions.a(GoogleSignInOptions.v).a()).t();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void k() {
        this.a.h1(false);
    }
}
