package com.mapped;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NotificationAppsFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleEditText r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ RecyclerView x;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat y;

    @DexIgnore
    public NotificationAppsFragmentBinding(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout2, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleEditText;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = rTLImageView;
        this.v = rTLImageView2;
        this.w = constraintLayout2;
        this.x = recyclerView;
        this.y = flexibleSwitchCompat;
    }
}
