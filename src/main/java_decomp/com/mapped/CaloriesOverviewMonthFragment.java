package com.mapped;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Aq0;
import com.fossil.B35;
import com.fossil.G37;
import com.fossil.Qg6;
import com.fossil.Rg6;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewMonthFragment extends BaseFragment implements Rg6 {
    @DexIgnore
    public G37<B35> g;
    @DexIgnore
    public Qg6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(CaloriesOverviewMonthFragment caloriesOverviewMonthFragment) {
            this.a = caloriesOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            Wg6.c(calendar, "calendar");
            Qg6 qg6 = this.a.h;
            if (qg6 != null) {
                Date time = calendar.getTime();
                Wg6.b(time, "calendar.time");
                qg6.o(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements RecyclerViewCalendar.a {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthFragment b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(CaloriesOverviewMonthFragment caloriesOverviewMonthFragment) {
            this.b = caloriesOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
        public void k0(int i, Calendar calendar) {
            Wg6.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                CaloriesDetailActivity.a aVar = CaloriesDetailActivity.C;
                Date time = calendar.getTime();
                Wg6.b(time, "it.time");
                Wg6.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "CaloriesOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        B35 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        G37<B35> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Qg6 qg6 = this.h;
            if (ai.w(qg6 != null ? qg6.n() : null)) {
                recyclerViewCalendar.J("dianaActiveCaloriesTab");
            } else {
                recyclerViewCalendar.J("hybridActiveCaloriesTab");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Qg6 qg6) {
        M6(qg6);
    }

    @DexIgnore
    public void M6(Qg6 qg6) {
        Wg6.c(qg6, "presenter");
        this.h = qg6;
    }

    @DexIgnore
    @Override // com.fossil.Rg6
    public void e(TreeMap<Long, Float> treeMap) {
        B35 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        Wg6.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        G37<B35> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.Rg6
    public void g(Date date, Date date2) {
        B35 a2;
        Wg6.c(date, "selectDate");
        Wg6.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        G37<B35> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            Wg6.b(instance, "selectCalendar");
            instance.setTime(date);
            Wg6.b(instance2, "startCalendar");
            instance2.setTime(TimeUtils.V(date2));
            Wg6.b(instance3, "endCalendar");
            instance3.setTime(TimeUtils.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        B35 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onCreateView");
        B35 b35 = (B35) Aq0.f(layoutInflater, 2131558510, viewGroup, false, A6());
        RecyclerViewCalendar recyclerViewCalendar = b35.q;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        b35.q.setOnCalendarMonthChanged(new Ai(this));
        b35.q.setOnCalendarItemClickListener(new Bi(this));
        this.g = new G37<>(this, b35);
        L6();
        G37<B35> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onResume");
        L6();
        Qg6 qg6 = this.h;
        if (qg6 != null) {
            qg6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onStop");
        Qg6 qg6 = this.h;
        if (qg6 != null) {
            qg6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
