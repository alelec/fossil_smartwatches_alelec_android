package com.mapped;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchFacePreviewFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomizeWidget A;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ TextView u;
    @DexIgnore
    public /* final */ TextView v;
    @DexIgnore
    public /* final */ TextView w;
    @DexIgnore
    public /* final */ CustomizeWidget x;
    @DexIgnore
    public /* final */ CustomizeWidget y;
    @DexIgnore
    public /* final */ CustomizeWidget z;

    @DexIgnore
    public WatchFacePreviewFragmentBinding(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, TextView textView, TextView textView2, TextView textView3, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = imageView;
        this.u = textView;
        this.v = textView2;
        this.w = textView3;
        this.x = customizeWidget;
        this.y = customizeWidget2;
        this.z = customizeWidget3;
        this.A = customizeWidget4;
    }
}
