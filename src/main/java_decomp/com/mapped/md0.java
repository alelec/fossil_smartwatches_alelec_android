package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Dm7;
import com.fossil.E;
import com.fossil.G80;
import com.fossil.Hy1;
import com.fossil.Ix1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import com.fossil.Ry1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Md0 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Ry1 c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Md0> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Md0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                Wg6.b(createByteArray, "parcel.createByteArray()!!");
                return new Md0(createByteArray);
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Md0[] newArray(int i) {
            return new Md0[i];
        }
    }

    @DexIgnore
    public Md0(byte[] bArr) throws IllegalArgumentException {
        this.b = bArr;
        if (bArr.length >= 16) {
            this.d = ByteBuffer.wrap(Dm7.k(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
            this.c = new Ry1(bArr[2], bArr[3]);
            return;
        }
        throw new IllegalArgumentException(E.c(E.e("data.size("), this.b.length, ") is not equal or larger ", "than 16"));
    }

    @DexIgnore
    public final short a() {
        return this.d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.b;
    }

    @DexIgnore
    public final Ry1 getWatchParameterVersion() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.A0, Hy1.l(this.d, null, 1, null)), Jd0.j2, this.c.toString()), Jd0.f3, Long.valueOf(Ix1.a.b(this.b, Ix1.Ai.CRC32C))), Jd0.I, Integer.valueOf(this.b.length));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
