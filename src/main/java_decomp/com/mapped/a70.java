package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.stetho.dumpapp.Framer;
import com.fossil.E;
import com.fossil.Ey1;
import com.fossil.Zm1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A70 extends R60 {
    @DexIgnore
    public static /* final */ Bi CREATOR; // = new Bi(null);
    @DexIgnore
    public /* final */ Ai c;

    @DexIgnore
    public enum Ai {
        OFF((byte) 0),
        LOW(Framer.STDERR_FRAME_PREFIX),
        MEDIUM((byte) 75),
        HIGH((byte) 100);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) {
                boolean z = false;
                if (true == (b <= 25)) {
                    return Ai.OFF;
                }
                if (true == (b <= 50)) {
                    return Ai.LOW;
                }
                if (b <= 75) {
                    z = true;
                }
                return true == z ? Ai.MEDIUM : Ai.HIGH;
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Parcelable.Creator<A70> {
        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
        }

        @DexIgnore
        public final A70 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new A70(Ai.d.a(bArr[0]));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public A70 b(Parcel parcel) {
            return new A70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public A70 createFromParcel(Parcel parcel) {
            return new A70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public A70[] newArray(int i) {
            return new A70[i];
        }
    }

    @DexIgnore
    public /* synthetic */ A70(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.c = Ai.valueOf(readString);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public A70(Ai ai) {
        super(Zm1.VIBE_STRENGTH);
        this.c = ai;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public String c() {
        return Ey1.a(this.c);
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(A70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((A70) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
    }

    @DexIgnore
    public final Ai getVibeStrengthLevel() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
