package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class D54 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon18 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;
    @DexIgnore
    public /* final */ /* synthetic */ PresetDataSource.PushPendingSavedPresetsCallback d;

    @DexIgnore
    public /* synthetic */ D54(PresetRepository.Anon18 anon18, List list, PresetDataSource.PushPendingSavedPresetsCallback pushPendingSavedPresetsCallback) {
        this.b = anon18;
        this.c = list;
        this.d = pushPendingSavedPresetsCallback;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c, this.d);
    }
}
