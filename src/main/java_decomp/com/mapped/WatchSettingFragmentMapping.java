package com.mapped;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.Zp0;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WatchSettingFragmentMapping extends WatchSettingFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.d T; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray U;
    @DexIgnore
    public long S;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        U = sparseIntArray;
        sparseIntArray.put(2131361936, 1);
        U.put(2131363410, 2);
        U.put(2131362065, 3);
        U.put(2131362691, 4);
        U.put(2131363319, 5);
        U.put(2131363297, 6);
        U.put(2131361933, 7);
        U.put(2131361939, 8);
        U.put(2131362175, 9);
        U.put(2131362112, 10);
        U.put(2131363345, 11);
        U.put(2131363346, 12);
        U.put(2131363131, 13);
        U.put(2131363336, 14);
        U.put(2131363337, 15);
        U.put(2131363132, 16);
        U.put(2131363393, 17);
        U.put(2131363394, 18);
        U.put(2131363420, 19);
        U.put(2131362292, 20);
        U.put(2131362293, 21);
        U.put(2131362291, 22);
        U.put(2131363332, 23);
        U.put(2131363133, 24);
        U.put(2131363287, 25);
        U.put(2131363288, 26);
        U.put(2131363385, 27);
    }
    */

    @DexIgnore
    public WatchSettingFragmentMapping(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 28, T, U));
    }

    @DexIgnore
    public WatchSettingFragmentMapping(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[7], (RTLImageView) objArr[1], (FlexibleButton) objArr[8], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[10], (CardView) objArr[9], (FlexibleButton) objArr[22], (FlexibleButton) objArr[20], (FlexibleButton) objArr[21], (ImageView) objArr[4], (ScrollView) objArr[0], (View) objArr[13], (View) objArr[16], (View) objArr[24], (FlexibleTextView) objArr[25], (FlexibleTextView) objArr[26], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[27], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[19]);
        this.S = -1;
        this.A.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.S = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.S != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.S = 1;
        }
        w();
    }
}
