package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.D90;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Nt1;
import com.fossil.Ox1;
import com.fossil.Ry1;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tc0 extends Ox1 implements Parcelable {
    @DexIgnore
    public /* final */ X90 b;
    @DexIgnore
    public /* final */ Nt1 c;

    @DexIgnore
    public Tc0(X90 x90, Nt1 nt1) {
        this.b = x90;
        this.c = nt1;
    }

    @DexIgnore
    public JSONObject a() {
        return new JSONObject();
    }

    @DexIgnore
    public abstract byte[] a(short s, Ry1 ry1);

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Tc0 tc0 = (Tc0) obj;
            return !(Wg6.a(this.b, tc0.b) ^ true) && !(Wg6.a(this.c, tc0.c) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.DeviceData");
    }

    @DexIgnore
    public final Nt1 getDeviceMessage() {
        return this.c;
    }

    @DexIgnore
    public final X90 getDeviceRequest() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        X90 x90 = this.b;
        int hashCode = x90 != null ? x90.hashCode() : 0;
        Nt1 nt1 = this.c;
        if (nt1 != null) {
            i = nt1.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        Object obj;
        Object obj2;
        JSONObject jSONObject = new JSONObject();
        try {
            Jd0 jd0 = Jd0.o;
            X90 x90 = this.b;
            if (x90 == null || (obj = x90.toJSONObject()) == null) {
                obj = JSONObject.NULL;
            }
            G80.k(jSONObject, jd0, obj);
            Jd0 jd02 = Jd0.p;
            Nt1 nt1 = this.c;
            if (nt1 == null || (obj2 = nt1.toJSONObject()) == null) {
                obj2 = JSONObject.NULL;
            }
            G80.k(jSONObject, jd02, obj2);
            G80.k(jSONObject, Jd0.Y2, a());
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }
}
