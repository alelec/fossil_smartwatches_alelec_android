package com.mapped;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.B77;
import com.fossil.Cp5;
import com.fossil.Dp5;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.N04;
import com.fossil.Nm0;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.S87;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.fossil.Wz5;
import com.fossil.X66;
import com.fossil.X75;
import com.fossil.Y66;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.faces.WatchFaceListActivity;
import com.portfolio.platform.watchface.gallery.WatchFaceGalleryActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDianaCustomizeFragment extends Qv5 implements Y66, View.OnClickListener, AlertDialogFragment.Gi, Aw5 {
    @DexIgnore
    public static /* final */ Ai E; // = new Ai(null);
    @DexIgnore
    public float A; // = 175.0f;
    @DexIgnore
    public long B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public HashMap D;
    @DexIgnore
    public X66 h;
    @DexIgnore
    public ConstraintLayout i;
    @DexIgnore
    public ViewPager2 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public G37<X75> l;
    @DexIgnore
    public DianaPresetAdapter m;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public FloatingActionButton w;
    @DexIgnore
    public int[] x; // = new int[2];
    @DexIgnore
    public float y; // = 90.0f;
    @DexIgnore
    public float z; // = 130.0f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeDianaCustomizeFragment a(String str) {
            Wg6.c(str, "navigatedPresetId");
            HomeDianaCustomizeFragment homeDianaCustomizeFragment = new HomeDianaCustomizeFragment();
            homeDianaCustomizeFragment.setArguments(Nm0.a(new Lc6("OUT_STATE_PRESET_ID", str)));
            return homeDianaCustomizeFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Bi(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void run() {
            ConstraintLayout constraintLayout;
            X75 x75 = (X75) HomeDianaCustomizeFragment.M6(this.b).a();
            if (x75 != null && (constraintLayout = x75.u) != null) {
                constraintLayout.setVisibility(4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.a = homeDianaCustomizeFragment;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g v;
            Drawable e;
            TabLayout tabLayout2;
            TabLayout.g v2;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.a.t)) {
                int parseColor = Color.parseColor(this.a.t);
                X75 x75 = (X75) HomeDianaCustomizeFragment.M6(this.a).a();
                if (!(x75 == null || (tabLayout2 = x75.M) == null || (v2 = tabLayout2.v(i)) == null || (e2 = v2.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.s) && this.a.Y6() != i) {
                int parseColor2 = Color.parseColor(this.a.s);
                X75 x752 = (X75) HomeDianaCustomizeFragment.M6(this.a).a();
                if (!(x752 == null || (tabLayout = x752.M) == null || (v = tabLayout.v(this.a.Y6())) == null || (e = v.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.a.e7(i);
            this.a.Z6().n(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Di(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceEditActivity.a aVar = WatchFaceEditActivity.A;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext, this.b.Z6().q());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Ei(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceGalleryActivity.a aVar = WatchFaceGalleryActivity.B;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Fi(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            WatchFaceListActivity.a aVar = WatchFaceListActivity.C;
            Context requireContext = this.b.requireContext();
            Wg6.b(requireContext, "requireContext()");
            aVar.a(requireContext);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Gi(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            this.b.Z6().v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Hi(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.v) {
                this.b.K1();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;

        @DexIgnore
        public Ii(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.b = homeDianaCustomizeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.K1();
            this.b.B = System.currentTimeMillis();
            this.b.Z6().x();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements Cp5 {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Wz5.Bi {
            @DexIgnore
            public /* final */ /* synthetic */ Ji a;
            @DexIgnore
            public /* final */ /* synthetic */ String b;

            @DexIgnore
            public Aii(Ji ji, String str) {
                this.a = ji;
                this.b = str;
            }

            @DexIgnore
            @Override // com.fossil.Wz5.Bi
            public void a(String str) {
                Wg6.c(str, "presetName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    this.a.a.Z6().t(str, this.b);
                }
            }

            @DexIgnore
            @Override // com.fossil.Wz5.Bi
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ji(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.a = homeDianaCustomizeFragment;
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void N(String str, String str2) {
            Wg6.c(str, "presetName");
            Wg6.c(str2, "presetId");
            if (this.a.getChildFragmentManager().Z("RenamePresetDialogFragment") == null) {
                Wz5 a2 = Wz5.i.a(str, new Aii(this, str2));
                if (this.a.isActive()) {
                    a2.show(this.a.getChildFragmentManager(), "RenamePresetDialogFragment");
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void a() {
            this.a.Z6().o();
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void b(String str, boolean z, View view) {
            Wg6.c(str, "presetId");
            Wg6.c(view, "view");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onEditClicked id " + str + " isEditable " + z);
            if (view instanceof FloatingActionButton) {
                this.a.w = (FloatingActionButton) view;
                view.getLocationOnScreen(this.a.x);
                if (this.a.v) {
                    this.a.a7();
                    this.a.X6(true);
                    this.a.c7(true);
                    return;
                }
                this.a.X6(false);
                this.a.c7(false);
                this.a.g7(z);
            }
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void c() {
            T t;
            String u = this.a.Z6().u();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onDataRenderSuccess presetId " + u);
            if (!TextUtils.isEmpty(u)) {
                Iterator<T> it = HomeDianaCustomizeFragment.N6(this.a).j().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.c(), u)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    int indexOf = HomeDianaCustomizeFragment.N6(this.a).j().indexOf(t2);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "onDataRenderSuccess navigate to index " + indexOf);
                    HomeDianaCustomizeFragment.P6(this.a).setCurrentItem(indexOf);
                    this.a.Z6().r("");
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void d(String str, String str2) {
            Wg6.c(str, "presetId");
            Wg6.c(str2, "position");
            WatchAppEditActivity.a aVar = WatchAppEditActivity.D;
            FragmentActivity requireActivity = this.a.requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            aVar.a(requireActivity, str, str2);
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void e(String str, String str2) {
            Wg6.c(str, "presetId");
            this.a.Z6().s(str, str2);
        }

        @DexIgnore
        @Override // com.fossil.Cp5
        public void f(String str) {
            Wg6.c(str, "presetId");
            this.a.Z6().w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements Animator.AnimatorListener {
        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ long d;

        @DexIgnore
        public Li(HomeDianaCustomizeFragment homeDianaCustomizeFragment, View view, long j) {
            this.b = homeDianaCustomizeFragment;
            this.c = view;
            this.d = j;
        }

        @DexIgnore
        public void onGlobalLayout() {
            this.c.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            HomeDianaCustomizeFragment homeDianaCustomizeFragment = this.b;
            homeDianaCustomizeFragment.y6(this.c, this.d, homeDianaCustomizeFragment.x);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizeFragment a;

        @DexIgnore
        public Mi(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
            this.a = homeDianaCustomizeFragment;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.s);
                if (i != 0) {
                    gVar.o(2131230966);
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor);
                    }
                } else {
                    gVar.o(2131230943);
                    Drawable e2 = gVar.e();
                    if (e2 != null) {
                        e2.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.t) && i == this.a.Y6()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.Y6());
                    Drawable e3 = gVar.e();
                    if (e3 != null) {
                        e3.setTint(Color.parseColor(this.a.t));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ G37 M6(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
        G37<X75> g37 = homeDianaCustomizeFragment.l;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ DianaPresetAdapter N6(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
        DianaPresetAdapter dianaPresetAdapter = homeDianaCustomizeFragment.m;
        if (dianaPresetAdapter != null) {
            return dianaPresetAdapter;
        }
        Wg6.n("presetAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ViewPager2 P6(HomeDianaCustomizeFragment homeDianaCustomizeFragment) {
        ViewPager2 viewPager2 = homeDianaCustomizeFragment.j;
        if (viewPager2 != null) {
            return viewPager2;
        }
        Wg6.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void C0(String str) {
        Wg6.c(str, "id");
        WatchFaceListActivity.a aVar = WatchFaceListActivity.C;
        FragmentActivity requireActivity = requireActivity();
        Wg6.b(requireActivity, "requireActivity()");
        aVar.c(requireActivity, str, "CREATED_ID", false);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void E5(int i2, String str) {
        Wg6.c(str, "errorMessage");
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.O(childFragmentManager, i2, str);
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void F5(List<Dp5> list, Map<String, ? extends List<? extends S87>> map) {
        Wg6.c(list, "data");
        Wg6.c(map, "configs");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size() + " - config=" + map.size());
        DianaPresetAdapter dianaPresetAdapter = this.m;
        if (dianaPresetAdapter != null) {
            dianaPresetAdapter.m(list, map);
        } else {
            Wg6.n("presetAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        if (!this.v) {
            return false;
        }
        a7();
        c7(true);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void K1() {
        a7();
        X6(true);
        c7(true);
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void M4() {
        DianaPresetAdapter dianaPresetAdapter;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onUpdateFwComplete");
        if (this.h != null && this.C && (dianaPresetAdapter = this.m) != null) {
            if (dianaPresetAdapter == null) {
                Wg6.n("presetAdapter");
                throw null;
            } else if (dianaPresetAdapter.j().isEmpty()) {
                X66 x66 = this.h;
                if (x66 != null) {
                    x66.l();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        f7((X66) obj);
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void O(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i2);
        DianaPresetAdapter dianaPresetAdapter = this.m;
        if (dianaPresetAdapter == null) {
            Wg6.n("presetAdapter");
            throw null;
        } else if (dianaPresetAdapter.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        String str2;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1353443012) {
            if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION") && i2 == 2131363373) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((BaseActivity) activity).y();
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363373) {
            if (intent == null || (str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID")) == null) {
                str2 = "";
            }
            X66 x66 = this.h;
            if (x66 != null) {
                x66.p(str2);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void W3(String str) {
        Wg6.c(str, "title");
        if (str.length() > 0) {
            H6(str);
        } else {
            b();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void W4(Dp5 dp5, List<? extends S87> list) {
        Wg6.c(dp5, "uiDianaPreset");
        DianaPresetAdapter dianaPresetAdapter = this.m;
        if (dianaPresetAdapter != null) {
            dianaPresetAdapter.n(dp5, list);
        } else {
            Wg6.n("presetAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void X6(boolean z2) {
        ConstraintLayout constraintLayout;
        if (z2) {
            G37<X75> g37 = this.l;
            if (g37 != null) {
                X75 a2 = g37.a();
                ViewPropertyAnimator animate = (a2 == null || (constraintLayout = a2.u) == null) ? null : constraintLayout.animate();
                if (animate != null) {
                    ViewPropertyAnimator withEndAction = animate.alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).withEndAction(new Bi(this));
                    Wg6.b(withEndAction, "mBinding.get()?.fabBackg\u2026BLE\n                    }");
                    withEndAction.setDuration(250);
                    x6("#80000000", "#FAF8F6", 250);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<X75> g372 = this.l;
        if (g372 != null) {
            X75 a3 = g372.a();
            ConstraintLayout constraintLayout2 = a3 != null ? a3.u : null;
            if (constraintLayout2 != null) {
                Wg6.b(constraintLayout2, "it");
                constraintLayout2.setVisibility(0);
                constraintLayout2.animate().alpha(1.0f).setDuration(250);
                x6("#FAF8F6", "#80000000", 250);
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final int Y6() {
        return this.k;
    }

    @DexIgnore
    public final X66 Z6() {
        X66 x66 = this.h;
        if (x66 != null) {
            return x66;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void a7() {
        G37<X75> g37 = this.l;
        if (g37 != null) {
            X75 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.G;
                Wg6.b(linearLayout, "binding.llMyFaces");
                z6(linearLayout, 300, this.x);
                LinearLayout linearLayout2 = a2.F;
                Wg6.b(linearLayout2, "binding.llGallery");
                z6(linearLayout2, 350, this.x);
                LinearLayout linearLayout3 = a2.E;
                Wg6.b(linearLayout3, "binding.llEdit");
                z6(linearLayout3, 400, this.x);
                LinearLayout linearLayout4 = a2.I;
                Wg6.b(linearLayout4, "binding.llShare");
                z6(linearLayout4, 450, this.x);
                LinearLayout linearLayout5 = a2.H;
                Wg6.b(linearLayout5, "binding.llSave");
                z6(linearLayout5, 500, this.x);
                X66 x66 = this.h;
                if (x66 != null) {
                    x66.y(false);
                    this.v = false;
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z2) {
        if (z2) {
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    public final void b7(X75 x75) {
        LinearLayout linearLayout = x75.E;
        Wg6.b(linearLayout, "binding.llEdit");
        ViewGroup.LayoutParams layoutParams = linearLayout.getLayoutParams();
        if (layoutParams != null) {
            this.y = ((ConstraintLayout.LayoutParams) layoutParams).o;
            LinearLayout linearLayout2 = x75.I;
            Wg6.b(linearLayout2, "binding.llShare");
            ViewGroup.LayoutParams layoutParams2 = linearLayout2.getLayoutParams();
            if (layoutParams2 != null) {
                this.z = ((ConstraintLayout.LayoutParams) layoutParams2).o;
                LinearLayout linearLayout3 = x75.H;
                Wg6.b(linearLayout3, "binding.llSave");
                ViewGroup.LayoutParams layoutParams3 = linearLayout3.getLayoutParams();
                if (layoutParams3 != null) {
                    this.A = ((ConstraintLayout.LayoutParams) layoutParams3).o;
                    this.s = ThemeManager.l.a().d("nonBrandSwitchDisabledGray");
                    this.t = ThemeManager.l.a().d("primaryColor");
                    this.u = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                    Ji ji = new Ji(this);
                    Context requireContext = requireContext();
                    Wg6.b(requireContext, "requireContext()");
                    this.m = new DianaPresetAdapter(requireContext, ji);
                    ConstraintLayout constraintLayout = x75.r;
                    Wg6.b(constraintLayout, "binding.clNoDevice");
                    this.i = constraintLayout;
                    x75.D.setImageResource(2131230945);
                    x75.C.setOnClickListener(this);
                    ViewPager2 viewPager2 = x75.L;
                    Wg6.b(viewPager2, "binding.rvPreset");
                    this.j = viewPager2;
                    if (viewPager2 != null) {
                        if (viewPager2.getChildAt(0) != null) {
                            ViewPager2 viewPager22 = this.j;
                            if (viewPager22 != null) {
                                View childAt = viewPager22.getChildAt(0);
                                if (childAt != null) {
                                    ((RecyclerView) childAt).setOverScrollMode(2);
                                } else {
                                    throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                                }
                            } else {
                                Wg6.n("rvCustomize");
                                throw null;
                            }
                        }
                        ViewPager2 viewPager23 = this.j;
                        if (viewPager23 != null) {
                            DianaPresetAdapter dianaPresetAdapter = this.m;
                            if (dianaPresetAdapter != null) {
                                viewPager23.setAdapter(dianaPresetAdapter);
                                if (!TextUtils.isEmpty(this.u)) {
                                    TabLayout tabLayout = x75.M;
                                    Wg6.b(tabLayout, "binding.tab");
                                    tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                                }
                                ViewPager2 viewPager24 = this.j;
                                if (viewPager24 != null) {
                                    viewPager24.g(new Ci(this));
                                    ViewPager2 viewPager25 = this.j;
                                    if (viewPager25 != null) {
                                        viewPager25.setCurrentItem(this.k);
                                        x75.v.setOnClickListener(new Di(this));
                                        x75.w.setOnClickListener(new Ei(this));
                                        x75.x.setOnClickListener(new Fi(this));
                                        x75.y.setOnClickListener(new Gi(this));
                                        x75.u.setOnClickListener(new Hi(this));
                                        x75.z.setOnClickListener(new Ii(this));
                                        return;
                                    }
                                    Wg6.n("rvCustomize");
                                    throw null;
                                }
                                Wg6.n("rvCustomize");
                                throw null;
                            }
                            Wg6.n("presetAdapter");
                            throw null;
                        }
                        Wg6.n("rvCustomize");
                        throw null;
                    }
                    Wg6.n("rvCustomize");
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
            }
            throw new Rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
        }
        throw new Rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void c0(int i2) {
        if (isActive()) {
            DianaPresetAdapter dianaPresetAdapter = this.m;
            if (dianaPresetAdapter == null) {
                Wg6.n("presetAdapter");
                throw null;
            } else if (dianaPresetAdapter.getItemCount() > i2) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void c7(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "rotateFab, isCurrentlyExpand " + z2);
        FloatingActionButton floatingActionButton = this.w;
        if (floatingActionButton != null) {
            ViewPropertyAnimator animate = floatingActionButton.animate();
            if (animate != null) {
                animate.setDuration(250).setListener(new Ki()).rotation(!z2 ? 180.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void d7(View view, float f) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) layoutParams;
            layoutParams2.o = f;
            view.setLayoutParams(layoutParams2);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams");
    }

    @DexIgnore
    public final void e7(int i2) {
        this.k = i2;
    }

    @DexIgnore
    public void f7(X66 x66) {
        String str;
        Wg6.c(x66, "presenter");
        this.h = x66;
        if (x66 != null) {
            Bundle arguments = getArguments();
            if (arguments == null || (str = arguments.getString("OUT_STATE_PRESET_ID", "")) == null) {
                str = "";
            }
            x66.r(str);
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void g2(String str, int i2) {
        if (str != null) {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.TEXT", str);
            startActivity(Intent.createChooser(intent, getString(2131887333)));
            B77.a.h("wf_sharing_session", this.B, System.currentTimeMillis());
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDianaCustomizeFragment", "onWFSharing error " + i2);
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, null, childFragmentManager);
    }

    @DexIgnore
    public final void g7(boolean z2) {
        G37<X75> g37 = this.l;
        if (g37 != null) {
            X75 a2 = g37.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.G;
                Wg6.b(linearLayout, "binding.llMyFaces");
                h7(linearLayout, 300);
                LinearLayout linearLayout2 = a2.F;
                Wg6.b(linearLayout2, "binding.llGallery");
                h7(linearLayout2, 350);
                if (z2) {
                    LinearLayout linearLayout3 = a2.I;
                    Wg6.b(linearLayout3, "binding.llShare");
                    d7(linearLayout3, this.z);
                    LinearLayout linearLayout4 = a2.H;
                    Wg6.b(linearLayout4, "binding.llSave");
                    d7(linearLayout4, this.A);
                    FlexibleTextView flexibleTextView = a2.O;
                    Wg6.b(flexibleTextView, "binding.tvEdit");
                    flexibleTextView.setVisibility(0);
                    FlexibleTextView flexibleTextView2 = a2.O;
                    Wg6.b(flexibleTextView2, "binding.tvEdit");
                    flexibleTextView2.setAlpha(1.0f);
                    CustomizeWidget customizeWidget = a2.v;
                    Wg6.b(customizeWidget, "binding.fabEdit");
                    customizeWidget.setVisibility(0);
                    CustomizeWidget customizeWidget2 = a2.v;
                    Wg6.b(customizeWidget2, "binding.fabEdit");
                    customizeWidget2.setAlpha(1.0f);
                    CustomizeWidget customizeWidget3 = a2.v;
                    Wg6.b(customizeWidget3, "binding.fabEdit");
                    customizeWidget3.setEnabled(true);
                    CustomizeWidget customizeWidget4 = a2.v;
                    Wg6.b(customizeWidget4, "binding.fabEdit");
                    customizeWidget4.setClickable(true);
                    LinearLayout linearLayout5 = a2.E;
                    Wg6.b(linearLayout5, "binding.llEdit");
                    h7(linearLayout5, 400);
                    FlexibleTextView flexibleTextView3 = a2.R;
                    Wg6.b(flexibleTextView3, "binding.tvSave");
                    flexibleTextView3.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = a2.R;
                    Wg6.b(flexibleTextView4, "binding.tvSave");
                    flexibleTextView4.setAlpha(1.0f);
                    CustomizeWidget customizeWidget5 = a2.y;
                    Wg6.b(customizeWidget5, "binding.fabSave");
                    customizeWidget5.setVisibility(0);
                    CustomizeWidget customizeWidget6 = a2.y;
                    Wg6.b(customizeWidget6, "binding.fabSave");
                    customizeWidget6.setAlpha(1.0f);
                    CustomizeWidget customizeWidget7 = a2.y;
                    Wg6.b(customizeWidget7, "binding.fabSave");
                    customizeWidget7.setEnabled(true);
                    CustomizeWidget customizeWidget8 = a2.y;
                    Wg6.b(customizeWidget8, "binding.fabSave");
                    customizeWidget8.setClickable(true);
                    LinearLayout linearLayout6 = a2.H;
                    Wg6.b(linearLayout6, "binding.llSave");
                    h7(linearLayout6, 400);
                } else {
                    LinearLayout linearLayout7 = a2.I;
                    Wg6.b(linearLayout7, "binding.llShare");
                    d7(linearLayout7, this.y);
                    LinearLayout linearLayout8 = a2.H;
                    Wg6.b(linearLayout8, "binding.llSave");
                    d7(linearLayout8, this.z);
                    CustomizeWidget customizeWidget9 = a2.v;
                    Wg6.b(customizeWidget9, "binding.fabEdit");
                    customizeWidget9.setVisibility(4);
                    FlexibleTextView flexibleTextView5 = a2.O;
                    Wg6.b(flexibleTextView5, "binding.tvEdit");
                    flexibleTextView5.setVisibility(4);
                    CustomizeWidget customizeWidget10 = a2.y;
                    Wg6.b(customizeWidget10, "binding.fabSave");
                    customizeWidget10.setVisibility(4);
                    FlexibleTextView flexibleTextView6 = a2.R;
                    Wg6.b(flexibleTextView6, "binding.tvSave");
                    flexibleTextView6.setVisibility(4);
                }
                LinearLayout linearLayout9 = a2.I;
                Wg6.b(linearLayout9, "binding.llShare");
                h7(linearLayout9, 450);
                LinearLayout linearLayout10 = a2.H;
                Wg6.b(linearLayout10, "binding.llSave");
                h7(linearLayout10, 500);
                X66 x66 = this.h;
                if (x66 != null) {
                    x66.y(true);
                    this.v = true;
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public int getItemCount() {
        ViewPager2 viewPager2 = this.j;
        if (viewPager2 == null) {
            return 0;
        }
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        Wg6.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void h1() {
        if (isActive()) {
            Toast.makeText(requireContext(), "Can't delete only preset", 1).show();
        }
    }

    @DexIgnore
    public final void h7(View view, long j2) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new Li(this, view, j2));
    }

    @DexIgnore
    public final void i7() {
        G37<X75> g37 = this.l;
        if (g37 != null) {
            X75 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.M : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    new N04(tabLayout, viewPager2, new Mi(this)).a();
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void m0(int i2) {
        DianaPresetAdapter dianaPresetAdapter = this.m;
        if (dianaPresetAdapter == null) {
            Wg6.n("presetAdapter");
            throw null;
        } else if (dianaPresetAdapter.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        Wg6.c(view, "v");
        if (view.getId() == 2131362501 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            Wg6.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onCreateView");
        X75 x75 = (X75) Aq0.f(layoutInflater, 2131558575, viewGroup, false, A6());
        Wg6.b(x75, "binding");
        b7(x75);
        G37<X75> g37 = new G37<>(this, x75);
        this.l = g37;
        if (g37 != null) {
            X75 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        X66 x66 = this.h;
        if (x66 != null) {
            if (x66 != null) {
                x66.m();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        X66 x66 = this.h;
        if (x66 != null) {
            if (x66 != null) {
                x66.l();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onViewCreated");
        i7();
        E6("customize_view");
        this.C = true;
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void r(boolean z2) {
        if (z2) {
            ConstraintLayout constraintLayout = this.i;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                Wg6.n("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.i;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                Wg6.n("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void r0(boolean z2, String str, String str2, String str3) {
        Wg6.c(str, "currentPresetName");
        Wg6.c(str2, "nextPresetName");
        Wg6.c(str3, "nextPresetId");
        isActive();
        String string = requireActivity().getString(2131886547);
        Wg6.b(string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
        if (z2) {
            String string2 = requireActivity().getString(2131886548);
            Wg6.b(string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            Hr7 hr7 = Hr7.a;
            string = String.format(string2, Arrays.copyOf(new Object[]{Vt7.g(str2)}, 1));
            Wg6.b(string, "java.lang.String.format(format, *args)");
        }
        Bundle bundle = new Bundle();
        bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
        AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
        Hr7 hr72 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131886549);
        Wg6.b(c, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
        String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        fi.e(2131363410, format);
        fi.e(2131363317, string);
        fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886546));
        fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886545));
        fi.b(2131363373);
        fi.b(2131363291);
        fi.m(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Wg6.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.get.instance().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.D;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Y66
    public void w() {
        a();
    }
}
