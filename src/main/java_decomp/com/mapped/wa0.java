package com.mapped;

import com.fossil.Lw1;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Wa0 {
    @DexIgnore
    Yb0<Cd6> N(Lw1 lw1);

    @DexIgnore
    Yb0<Cd6> U(ThemeEditor themeEditor);

    @DexIgnore
    Yb0<Lw1> k(Lw1 lw1);

    @DexIgnore
    Yb0<Lw1> r(ThemeEditor themeEditor);
}
