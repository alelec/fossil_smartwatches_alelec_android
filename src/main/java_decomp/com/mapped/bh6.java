package com.mapped;

import com.fossil.Ds7;
import com.fossil.Er7;
import com.fossil.Ls7;
import com.fossil.Yq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bh6 extends Yq7 implements Ls7 {
    @DexIgnore
    public Bh6() {
    }

    @DexIgnore
    public Bh6(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ds7 computeReflected() {
        Er7.f(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get();

    @DexIgnore
    @Override // com.fossil.Ls7
    public Object getDelegate() {
        return ((Ls7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.Yq7, com.fossil.Ls7
    public Ls7.Ai getGetter() {
        return ((Ls7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.mapped.Gg6
    public Object invoke() {
        return get();
    }
}
