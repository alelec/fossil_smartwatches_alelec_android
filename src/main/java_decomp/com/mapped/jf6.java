package com.mapped;

import com.fossil.Bo7;
import com.fossil.Rn7;
import com.fossil.Zn7;
import com.mapped.Af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jf6 extends Zn7 {
    @DexIgnore
    public /* final */ Af6 _context;
    @DexIgnore
    public transient Xe6<Object> intercepted;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Jf6(Xe6<Object> xe6) {
        this(xe6, xe6 != null ? xe6.getContext() : null);
    }

    @DexIgnore
    public Jf6(Xe6<Object> xe6, Af6 af6) {
        super(xe6);
        this._context = af6;
    }

    @DexIgnore
    @Override // com.fossil.Zn7, com.mapped.Xe6
    public Af6 getContext() {
        Af6 af6 = this._context;
        if (af6 != null) {
            return af6;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Xe6<Object> intercepted() {
        Xe6<Object> xe6 = this.intercepted;
        if (xe6 == null) {
            Rn7 rn7 = (Rn7) getContext().get(Rn7.p);
            if (rn7 == null || (xe6 = rn7.c(this)) == null) {
                xe6 = this;
            }
            this.intercepted = xe6;
        }
        return xe6;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public void releaseIntercepted() {
        Xe6<?> xe6 = this.intercepted;
        if (!(xe6 == null || xe6 == this)) {
            Af6.Bi bi = getContext().get(Rn7.p);
            if (bi != null) {
                ((Rn7) bi).a(xe6);
            } else {
                Wg6.i();
                throw null;
            }
        }
        this.intercepted = Bo7.b;
    }
}
