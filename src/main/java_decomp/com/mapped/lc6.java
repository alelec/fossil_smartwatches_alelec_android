package com.mapped;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lc6<A, B> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;

    @DexIgnore
    public Lc6(A a2, B b) {
        this.first = a2;
        this.second = b;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.mapped.Lc6 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Lc6 copy$default(Lc6 lc6, Object obj, Object obj2, int i, Object obj3) {
        if ((i & 1) != 0) {
            obj = lc6.first;
        }
        if ((i & 2) != 0) {
            obj2 = lc6.second;
        }
        return lc6.copy(obj, obj2);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final Lc6<A, B> copy(A a2, B b) {
        return new Lc6<>(a2, b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Lc6) {
                Lc6 lc6 = (Lc6) obj;
                if (!Wg6.a(this.first, lc6.first) || !Wg6.a(this.second, lc6.second)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        A a2 = this.first;
        int hashCode = a2 != null ? a2.hashCode() : 0;
        B b = this.second;
        if (b != null) {
            i = b.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + ((Object) this.first) + ", " + ((Object) this.second) + ')';
    }
}
