package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.El1;
import com.fossil.Em7;
import com.fossil.Gl1;
import com.fossil.Hl1;
import com.fossil.Il1;
import com.fossil.O8;
import com.fossil.Ql1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z40 extends El1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Z40> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Gl1[] a(Gl1... gl1Arr) {
            Object[] array = Em7.E(gl1Arr).toArray(new Gl1[0]);
            if (array != null) {
                return (Gl1[]) array;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public Z40 b(Parcel parcel) {
            return (Z40) El1.CREATOR.b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Z40 createFromParcel(Parcel parcel) {
            return b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Z40[] newArray(int i) {
            return new Z40[i];
        }
    }

    @DexIgnore
    public Z40(Hl1 hl1, Ql1 ql1, Il1 il1) {
        super(O8.c, CREATOR.a(hl1, ql1, il1));
    }
}
