package com.mapped;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import com.fossil.Hm7;
import com.fossil.M47;
import com.fossil.Vr4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CitizenBrandLogic implements Vr4 {
    @DexIgnore
    public /* final */ List<String> a; // = Hm7.i("Agree term of use", "Agree privacy", "Allow location data processing");

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean a() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean c() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean e() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean f() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean g() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public String h() {
        return Vr4.Ai.a(this);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean i() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean j() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public void k(BaseFragment baseFragment) {
        Wg6.c(baseFragment, "fragment");
        String a2 = M47.a(M47.Ci.REPAIR_CENTER, null);
        Wg6.b(a2, "url");
        o(a2, baseFragment);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean l() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean m(List<String> list) {
        Wg6.c(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    @Override // com.fossil.Vr4
    public boolean n() {
        return true;
    }

    @DexIgnore
    public final void o(String str, BaseFragment baseFragment) {
        try {
            baseFragment.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException e) {
            FLogger.INSTANCE.getLocal().e("CitizenBrandLogic", "Exception when start url with no browser app");
        }
    }
}
