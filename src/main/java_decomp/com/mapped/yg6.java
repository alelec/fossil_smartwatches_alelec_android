package com.mapped;

import com.fossil.Ds7;
import com.fossil.Er7;
import com.fossil.Is7;
import com.fossil.Ls7;
import com.fossil.Uq7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yg6 extends Uq7 implements Is7 {
    @DexIgnore
    public Yg6() {
    }

    @DexIgnore
    public Yg6(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ds7 computeReflected() {
        Er7.d(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get();

    @DexIgnore
    @Override // com.fossil.Ls7
    public Object getDelegate() {
        return ((Is7) getReflected()).getDelegate();
    }

    @DexIgnore
    @Override // com.fossil.Yq7, com.fossil.Ls7, com.fossil.Uq7
    public Ls7.Ai getGetter() {
        return ((Is7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.Is7, com.fossil.Uq7
    public Is7.Ai getSetter() {
        return ((Is7) getReflected()).getSetter();
    }

    @DexIgnore
    @Override // com.mapped.Gg6
    public Object invoke() {
        return get();
    }

    @DexIgnore
    public abstract /* synthetic */ void set(R r);
}
