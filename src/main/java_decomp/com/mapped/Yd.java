package com.mapped;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import com.fossil.At0;
import com.fossil.Bt0;
import com.fossil.Qn0;
import com.fossil.Vm0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yd<D> extends At0<D> {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ String TAG; // = "AsyncTaskLoader";
    @DexIgnore
    public volatile Yd<D>.a mCancellingTask;
    @DexIgnore
    public /* final */ Executor mExecutor;
    @DexIgnore
    public Handler mHandler;
    @DexIgnore
    public long mLastLoadCompleteTime;
    @DexIgnore
    public volatile Yd<D>.a mTask;
    @DexIgnore
    public long mUpdateThrottle;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends Bt0<Void, Void, D> implements Runnable {
        @DexIgnore
        public /* final */ CountDownLatch k; // = new CountDownLatch(1);
        @DexIgnore
        public boolean l;

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // com.fossil.Bt0
        public /* bridge */ /* synthetic */ Object b(Void[] voidArr) {
            return n(voidArr);
        }

        @DexIgnore
        @Override // com.fossil.Bt0
        public void h(D d) {
            try {
                Yd.this.dispatchOnCancelled(this, d);
            } finally {
                this.k.countDown();
            }
        }

        @DexIgnore
        @Override // com.fossil.Bt0
        public void i(D d) {
            try {
                Yd.this.dispatchOnLoadComplete(this, d);
            } finally {
                this.k.countDown();
            }
        }

        @DexIgnore
        public D n(Void... voidArr) {
            try {
                return (D) Yd.this.onLoadInBackground();
            } catch (Vm0 e) {
                if (f()) {
                    return null;
                }
                throw e;
            }
        }

        @DexIgnore
        public void o() {
            try {
                this.k.await();
            } catch (InterruptedException e) {
            }
        }

        @DexIgnore
        public void run() {
            this.l = false;
            Yd.this.executePendingTask();
        }
    }

    @DexIgnore
    public Yd(Context context) {
        this(context, Bt0.i);
    }

    @DexIgnore
    public Yd(Context context, Executor executor) {
        super(context);
        this.mLastLoadCompleteTime = -10000;
        this.mExecutor = executor;
    }

    @DexIgnore
    public void cancelLoadInBackground() {
    }

    @DexIgnore
    public void dispatchOnCancelled(Yd<D>.a aVar, D d) {
        onCanceled(d);
        if (this.mCancellingTask == aVar) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    @DexIgnore
    public void dispatchOnLoadComplete(Yd<D>.a aVar, D d) {
        if (this.mTask != aVar) {
            dispatchOnCancelled(aVar, d);
        } else if (isAbandoned()) {
            onCanceled(d);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d);
        }
    }

    @DexIgnore
    @Override // com.fossil.At0
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        if (this.mTask != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.l);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.l);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            Qn0.c(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            Qn0.b(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }

    @DexIgnore
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.l) {
                this.mTask.l = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.c(this.mExecutor, null);
                return;
            }
            this.mTask.l = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    @DexIgnore
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }

    @DexIgnore
    public abstract D loadInBackground();

    @DexIgnore
    @Override // com.fossil.At0
    public boolean onCancelLoad() {
        boolean z = false;
        if (this.mTask != null) {
            if (!this.mStarted) {
                this.mContentChanged = true;
            }
            if (this.mCancellingTask != null) {
                if (this.mTask.l) {
                    this.mTask.l = false;
                    this.mHandler.removeCallbacks(this.mTask);
                }
                this.mTask = null;
            } else if (this.mTask.l) {
                this.mTask.l = false;
                this.mHandler.removeCallbacks(this.mTask);
                this.mTask = null;
            } else {
                z = this.mTask.a(false);
                if (z) {
                    this.mCancellingTask = this.mTask;
                    cancelLoadInBackground();
                }
                this.mTask = null;
            }
        }
        return z;
    }

    @DexIgnore
    public void onCanceled(D d) {
    }

    @DexIgnore
    @Override // com.fossil.At0
    public void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new Ai();
        executePendingTask();
    }

    @DexIgnore
    public D onLoadInBackground() {
        return loadInBackground();
    }

    @DexIgnore
    public void setUpdateThrottle(long j) {
        this.mUpdateThrottle = j;
        if (j != 0) {
            this.mHandler = new Handler();
        }
    }

    @DexIgnore
    public void waitForLoader() {
        Yd<D>.a aVar = this.mTask;
        if (aVar != null) {
            aVar.o();
        }
    }
}
