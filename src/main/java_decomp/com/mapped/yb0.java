package com.mapped;

import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.fossil.Yx1;
import com.fossil.common.task.Promise;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yb0<V> extends Zb0<V> {
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Float, Cd6>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $action;
        @DexIgnore
        public /* final */ /* synthetic */ float $progress$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Hg6 hg6, Xe6 xe6, float f) {
            super(2, xe6);
            this.$action = hg6;
            this.$progress$inlined = f;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$action, xe6, this.$progress$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.$action.invoke(Ao7.d(this.$progress$inlined));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    @Override // com.fossil.common.task.Promise
    public void j() {
        super.j();
        this.h.clear();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Hg6] */
    @Override // com.fossil.common.task.Promise, com.mapped.Zb0
    public /* bridge */ /* synthetic */ Promise k(Hg6<? super Yx1, Cd6> hg6) {
        return u(hg6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Hg6] */
    @Override // com.fossil.common.task.Promise, com.mapped.Zb0
    public /* bridge */ /* synthetic */ Promise l(Hg6<? super Yx1, Cd6> hg6) {
        return v(hg6);
    }

    @DexIgnore
    @Override // com.fossil.common.task.Promise, com.mapped.Zb0
    public /* bridge */ /* synthetic */ Promise m(Hg6 hg6) {
        return x(hg6);
    }

    @DexIgnore
    @Override // com.mapped.Zb0
    public /* bridge */ /* synthetic */ Zb0 q(Hg6 hg6) {
        return u(hg6);
    }

    @DexIgnore
    @Override // com.mapped.Zb0
    public /* bridge */ /* synthetic */ Zb0 r(Hg6 hg6) {
        return v(hg6);
    }

    @DexIgnore
    @Override // com.mapped.Zb0
    public /* bridge */ /* synthetic */ Zb0 s(Hg6 hg6) {
        return x(hg6);
    }

    @DexIgnore
    public Yb0<V> t(Hg6<? super Cd6, Cd6> hg6) {
        Wg6.c(hg6, "actionOnFinal");
        Zb0<V> p = super.p(hg6);
        if (p != null) {
            return (Yb0) p;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public Yb0<V> u(Hg6<? super Yx1, Cd6> hg6) {
        Wg6.c(hg6, "actionOnCancel");
        Zb0<V> q = super.q(hg6);
        if (q != null) {
            return (Yb0) q;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public Yb0<V> v(Hg6<? super Yx1, Cd6> hg6) {
        Wg6.c(hg6, "actionOnError");
        Zb0<V> r = super.r(hg6);
        if (r != null) {
            return (Yb0) r;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public final Yb0<V> w(Hg6<? super Float, Cd6> hg6) {
        Wg6.c(hg6, "actionOnProgressChange");
        if (!g()) {
            this.h.add(hg6);
        }
        return this;
    }

    @DexIgnore
    public Yb0<V> x(Hg6<? super V, Cd6> hg6) {
        Wg6.c(hg6, "actionOnSuccess");
        Zb0<V> s = super.s(hg6);
        if (s != null) {
            return (Yb0) s;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.common.task.ProgressTask<V>");
    }

    @DexIgnore
    public final void y(float f) {
        synchronized (this) {
            Iterator<T> it = this.h.iterator();
            while (it.hasNext()) {
                try {
                    Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ai(it.next(), null, f), 3, null);
                } catch (Exception e) {
                }
            }
        }
    }
}
