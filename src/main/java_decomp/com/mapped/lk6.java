package com.mapped;

import com.fossil.Dv7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Lk6<T> extends Xe6<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* synthetic */ Object a(Lk6 lk6, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return lk6.b(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    @DexIgnore
    Object b(T t, Object obj);

    @DexIgnore
    void e(Hg6<? super Throwable, Cd6> hg6);

    @DexIgnore
    void f(Dv7 dv7, T t);

    @DexIgnore
    void g(Object obj);

    @DexIgnore
    boolean isActive();
}
