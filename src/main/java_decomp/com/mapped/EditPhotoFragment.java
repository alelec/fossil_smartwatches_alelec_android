package com.mapped;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.B65;
import com.fossil.G37;
import com.fossil.Ls0;
import com.fossil.N66;
import com.fossil.Nl5;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.Ry5;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.mapped.AlertDialogFragment;
import com.mapped.ImageFilterAdapter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoFragment extends Qv5 implements ImageFilterAdapter.Ci, CropImageView.i, CropImageView.g, FragmentManager.g, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public EditPhotoViewModel h;
    @DexIgnore
    public G37<B65> i;
    @DexIgnore
    public Uri j;
    @DexIgnore
    public ImageFilterAdapter k;
    @DexIgnore
    public /* final */ ArrayList<FilterType> l;
    @DexIgnore
    public FilterType m;
    @DexIgnore
    public Po4 s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final EditPhotoFragment a(Uri uri) {
            Wg6.c(uri, "uri");
            EditPhotoFragment editPhotoFragment = new EditPhotoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("MAGE_URI_ARG", uri);
            editPhotoFragment.setArguments(bundle);
            return editPhotoFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<EditPhotoViewModel.Di> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Bi(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void a(EditPhotoViewModel.Di di) {
            FragmentActivity activity;
            FlexibleTextView flexibleTextView;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EditPhotoFragment", "Crop image: result = " + di);
            this.a.a();
            B65 S6 = this.a.S6();
            if (!(S6 == null || (flexibleTextView = S6.w) == null)) {
                flexibleTextView.setClickable(true);
            }
            if (di.b() && (activity = this.a.getActivity()) != null) {
                this.a.a();
                if (di.b()) {
                    FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Receive saveThemeResult Success");
                    Intent intent = new Intent();
                    intent.putExtra("WATCH_FACE_ID", di.a());
                    activity.setResult(-1, intent);
                    activity.finish();
                    return;
                }
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Receive saveThemeResult Fail");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(EditPhotoViewModel.Di di) {
            a(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<EditPhotoViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Ci(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void a(EditPhotoViewModel.Bi bi) {
            List<FilterResult> a2 = bi.a();
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(a2));
            this.a.a();
            if (!a2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (FilterResult filterResult : bi.a()) {
                    Bitmap preview = filterResult.getPreview();
                    Wg6.b(preview, "filter.preview");
                    Bitmap s = Ry5.s(preview, false);
                    Wg6.b(s, "circleBitmap");
                    FilterType type = filterResult.getType();
                    Wg6.b(type, "filter.type");
                    arrayList.add(new ImageFilterAdapter.Ai(s, type));
                }
                this.a.m = ((ImageFilterAdapter.Ai) arrayList.get(0)).b();
                ImageFilterAdapter imageFilterAdapter = this.a.k;
                if (imageFilterAdapter != null) {
                    imageFilterAdapter.l(arrayList);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(EditPhotoViewModel.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<EditPhotoViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment a;

        @DexIgnore
        public Di(EditPhotoFragment editPhotoFragment) {
            this.a = editPhotoFragment;
        }

        @DexIgnore
        public final void a(EditPhotoViewModel.Ai ai) {
            CropImageView cropImageView;
            FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(ai.a()));
            FilterResult a2 = ai.a();
            B65 S6 = this.a.S6();
            if (!(S6 == null || (cropImageView = S6.q) == null)) {
                cropImageView.q(a2.getPreview());
            }
            EditPhotoFragment editPhotoFragment = this.a;
            FilterType type = a2.getType();
            Wg6.b(type, "it.type");
            editPhotoFragment.m = type;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(EditPhotoViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment b;

        @DexIgnore
        public Ei(EditPhotoFragment editPhotoFragment, B65 b65) {
            this.b = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoFragment b;

        @DexIgnore
        public Fi(EditPhotoFragment editPhotoFragment, B65 b65) {
            this.b = editPhotoFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.j != null) {
                this.b.Q6();
            } else {
                FLogger.INSTANCE.getLocal().e("EditPhotoFragment", "Can not crop image cause of null imageURI");
            }
        }
    }

    @DexIgnore
    public EditPhotoFragment() {
        ArrayList<FilterType> arrayList = new ArrayList<>();
        arrayList.add(FilterType.ATKINSON_DITHERING);
        arrayList.add(FilterType.BURKES_DITHERING);
        arrayList.add(FilterType.DIRECT_MAPPING);
        arrayList.add(FilterType.JAJUNI_DITHERING);
        arrayList.add(FilterType.ORDERED_DITHERING);
        arrayList.add(FilterType.SIERRA_DITHERING);
        this.l = arrayList;
        FilterType filterType = arrayList.get(0);
        Wg6.b(filterType, "mFilterList[0]");
        this.m = filterType;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.g
    public void E3(Rect rect) {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onCropOverlayReleased()");
        U6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.customview.imagecropper.CropImageView.i
    public void K3(CropImageView cropImageView, Uri uri, Exception exc) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onSetImageUriComplete, uri = " + uri + ", error = " + exc);
        if (isActive()) {
            if (exc == null) {
                U6();
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("EditPhotoFragment", "error = " + exc);
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    public final void P6() {
        EditPhotoViewModel editPhotoViewModel = this.h;
        if (editPhotoViewModel != null) {
            editPhotoViewModel.h().h(getViewLifecycleOwner(), new Bi(this));
            EditPhotoViewModel editPhotoViewModel2 = this.h;
            if (editPhotoViewModel2 != null) {
                editPhotoViewModel2.f().h(getViewLifecycleOwner(), new Ci(this));
                EditPhotoViewModel editPhotoViewModel3 = this.h;
                if (editPhotoViewModel3 != null) {
                    editPhotoViewModel3.e().h(getViewLifecycleOwner(), new Di(this));
                } else {
                    Wg6.n("mViewModel");
                    throw null;
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "cropImage()");
        EditPhotoViewModel.Ci R6 = R6();
        if (R6 != null) {
            b();
            EditPhotoViewModel editPhotoViewModel = this.h;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.d(R6, this.m);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoFragment", "onDialogFragmentResult tag = " + str);
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() == 407101428 && str.equals("PROCESS_IMAGE_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        super.R5(str, i2, intent);
    }

    @DexIgnore
    public final EditPhotoViewModel.Ci R6() {
        CropImageView cropImageView;
        ConstraintLayout constraintLayout;
        B65 S6 = S6();
        if (!(S6 == null || (constraintLayout = S6.r) == null)) {
            constraintLayout.invalidate();
        }
        B65 S62 = S6();
        if (!(S62 == null || (cropImageView = S62.q) == null)) {
            Wg6.b(cropImageView, "it");
            if (cropImageView.getInitializeBitmap() != null) {
                cropImageView.clearAnimation();
                float[] fArr = new float[8];
                float[] cropPoints = cropImageView.getCropPoints();
                Wg6.b(cropPoints, "it.cropPoints");
                int i2 = 0;
                for (float f : cropPoints) {
                    fArr[i2] = f / ((float) cropImageView.getLoadedSampleSize());
                    i2++;
                }
                Bitmap initializeBitmap = cropImageView.getInitializeBitmap();
                Wg6.b(initializeBitmap, "it.initializeBitmap");
                int rotatedDegrees = cropImageView.getRotatedDegrees();
                boolean k2 = cropImageView.k();
                Object obj = cropImageView.getAspectRatio().first;
                Wg6.b(obj, "it.aspectRatio.first");
                int intValue = ((Number) obj).intValue();
                Object obj2 = cropImageView.getAspectRatio().second;
                Wg6.b(obj2, "it.aspectRatio.second");
                return new EditPhotoViewModel.Ci(initializeBitmap, fArr, rotatedDegrees, k2, intValue, ((Number) obj2).intValue(), cropImageView.l(), cropImageView.m());
            }
        }
        return null;
    }

    @DexIgnore
    public final B65 S6() {
        G37<B65> g37 = this.i;
        if (g37 != null) {
            return g37.a();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void T6(Bundle bundle) {
        ArrayList<N66> arrayList = null;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "initialize()");
        if (bundle.containsKey("MAGE_URI_ARG")) {
            Bundle arguments = getArguments();
            this.j = arguments != null ? (Uri) arguments.getParcelable("MAGE_URI_ARG") : null;
        }
        if (bundle.containsKey("COMPLICATIONS_ARG")) {
            EditPhotoViewModel editPhotoViewModel = this.h;
            if (editPhotoViewModel != null) {
                Bundle arguments2 = getArguments();
                if (arguments2 != null) {
                    arrayList = arguments2.getParcelableArrayList("COMPLICATIONS_ARG");
                }
                editPhotoViewModel.i(arrayList);
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void U6() {
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "updateFilterList()");
        EditPhotoViewModel.Ci R6 = R6();
        if (R6 != null) {
            EditPhotoViewModel editPhotoViewModel = this.h;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.c(R6, this.l);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.ImageFilterAdapter.Ci
    public void k4(ImageFilterAdapter.Ai ai) {
        B65 S6;
        CropImageView cropImageView;
        Bitmap initializeBitmap;
        Wg6.c(ai, "imageFilter");
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", String.valueOf(ai.b()));
        if (this.m != ai.b() && (S6 = S6()) != null && (cropImageView = S6.q) != null && (initializeBitmap = cropImageView.getInitializeBitmap()) != null) {
            EditPhotoViewModel editPhotoViewModel = this.h;
            if (editPhotoViewModel != null) {
                editPhotoViewModel.b(initializeBitmap, ai.b());
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (bundle != null) {
            T6(bundle);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentManager.g
    public void onBackStackChanged() {
        CropImageView cropImageView;
        CropImageView cropImageView2;
        FLogger.INSTANCE.getLocal().d("EditPhotoFragment", "onBackStackChanged()");
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        if (childFragmentManager.d0() == 0) {
            G37<B65> g37 = this.i;
            if (g37 != null) {
                B65 a2 = g37.a();
                if (a2 != null && (cropImageView2 = a2.q) != null) {
                    cropImageView2.setOnSetImageUriCompleteListener(this);
                    cropImageView2.setOnSetCropOverlayReleasedListener(this);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<B65> g372 = this.i;
        if (g372 != null) {
            B65 a3 = g372.a();
            if (a3 != null && (cropImageView = a3.q) != null) {
                cropImageView.e();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().v0().a(this);
        FragmentActivity requireActivity = requireActivity();
        Po4 po4 = this.s;
        if (po4 != null) {
            Ts0 a2 = Vs0.f(requireActivity, po4).a(EditPhotoViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(re\u2026otoViewModel::class.java)");
            this.h = (EditPhotoViewModel) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                Wg6.b(arguments, "it");
                T6(arguments);
            }
            getChildFragmentManager().e(this);
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        B65 b65 = (B65) Aq0.f(LayoutInflater.from(getContext()), 2131558551, null, true, A6());
        this.i = new G37<>(this, b65);
        this.k = new ImageFilterAdapter(null, this, 1, null);
        G37<B65> g37 = this.i;
        if (g37 != null) {
            B65 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "fragmentBinding.tvCancel");
                Nl5.a(flexibleTextView, new Ei(this, b65));
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "fragmentBinding.tvPreview");
                Nl5.a(flexibleTextView2, new Fi(this, b65));
                if (this.j != null) {
                    a2.q.setOnSetImageUriCompleteListener(this);
                    CropImageView cropImageView = a2.q;
                    if (cropImageView != null) {
                        cropImageView.setOnSetCropOverlayReleasedListener(this);
                    }
                    b();
                    a2.q.w(this.j, this.m);
                } else {
                    a();
                }
                RecyclerView recyclerView = b65.u;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                recyclerView.setAdapter(this.k);
            }
            P6();
            Wg6.b(b65, "binding");
            return b65.n();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        getChildFragmentManager().R0(this);
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        CropImageView cropImageView;
        G37<B65> g37 = this.i;
        if (g37 != null) {
            B65 a2 = g37.a();
            if (!(a2 == null || (cropImageView = a2.q) == null)) {
                cropImageView.e();
            }
            ImageFilterAdapter imageFilterAdapter = this.k;
            if (imageFilterAdapter != null) {
                imageFilterAdapter.m(null);
            }
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("MAGE_URI_ARG", this.j);
        EditPhotoViewModel editPhotoViewModel = this.h;
        if (editPhotoViewModel != null) {
            bundle.putParcelableArrayList("COMPLICATIONS_ARG", editPhotoViewModel.g());
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
