package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class F54 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon20 b;
    @DexIgnore
    public /* final */ /* synthetic */ SavedPreset c;

    @DexIgnore
    public /* synthetic */ F54(PresetRepository.Anon20 anon20, SavedPreset savedPreset) {
        this.b = anon20;
        this.c = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
