package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Bd2;
import com.fossil.Pc2;
import com.fossil.Pi2;
import com.fossil.Rc2;
import com.fossil.Zc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G72 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<G72> CREATOR; // = new Pi2();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public G72(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, 0);
    }

    @DexIgnore
    public G72(String str, String str2, String str3, int i, int i2) {
        Rc2.k(str);
        this.b = str;
        Rc2.k(str2);
        this.c = str2;
        if (str3 != null) {
            this.d = str3;
            this.e = i;
            this.f = i2;
            return;
        }
        throw new IllegalStateException("Device UID is null.");
    }

    @DexIgnore
    public final String A() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof G72)) {
            return false;
        }
        G72 g72 = (G72) obj;
        return Pc2.a(this.b, g72.b) && Pc2.a(this.c, g72.c) && Pc2.a(this.d, g72.d) && this.e == g72.e && this.f == g72.f;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final String h() {
        return String.format("%s:%s:%s", this.b, this.c, this.d);
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(this.b, this.c, this.d, Integer.valueOf(this.e));
    }

    @DexIgnore
    public final int k() {
        return this.e;
    }

    @DexIgnore
    public final String toString() {
        return String.format("Device{%s:%s:%s}", h(), Integer.valueOf(this.e), Integer.valueOf(this.f));
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, c(), false);
        Bd2.u(parcel, 2, f(), false);
        Bd2.u(parcel, 4, A(), false);
        Bd2.n(parcel, 5, k());
        Bd2.n(parcel, 6, this.f);
        Bd2.b(parcel, a2);
    }
}
