package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Gy1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import com.fossil.Px1;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z80 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ H70 d;
    @DexIgnore
    public /* final */ W80 e;
    @DexIgnore
    public /* final */ Y80[] f;
    @DexIgnore
    public /* final */ X80[] g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Z80> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Z80 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    H70 valueOf = H70.valueOf(readString2);
                    Parcelable readParcelable = parcel.readParcelable(W80.class.getClassLoader());
                    if (readParcelable != null) {
                        W80 w80 = (W80) readParcelable;
                        Object[] createTypedArray = parcel.createTypedArray(Y80.CREATOR);
                        if (createTypedArray != null) {
                            Wg6.b(createTypedArray, "parcel.createTypedArray(\u2026erHourForecast.CREATOR)!!");
                            Y80[] y80Arr = (Y80[]) createTypedArray;
                            Object[] createTypedArray2 = parcel.createTypedArray(X80.CREATOR);
                            if (createTypedArray2 != null) {
                                Wg6.b(createTypedArray2, "parcel.createTypedArray(\u2026herDayForecast.CREATOR)!!");
                                return new Z80(readLong, readString, valueOf, w80, y80Arr, (X80[]) createTypedArray2);
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Z80[] newArray(int i) {
            return new Z80[i];
        }
    }

    @DexIgnore
    public Z80(long j, String str, H70 h70, W80 w80, Y80[] y80Arr, X80[] x80Arr) throws IllegalArgumentException {
        this.b = j;
        this.c = str;
        this.d = h70;
        this.e = w80;
        this.f = y80Arr;
        this.g = x80Arr;
        if (y80Arr.length < 3) {
            throw new IllegalArgumentException("hourForecast must have at least 3 elements.");
        } else if (x80Arr.length < 3) {
            throw new IllegalArgumentException("dayForecast must have at least 3 elements.");
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < 3; i++) {
            jSONArray.put(this.f[i].a());
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i2 = 0; i2 < 3; i2++) {
            jSONArray2.put(this.g[i2].a());
        }
        JSONObject put = new JSONObject().put("alive", this.b).put("city", this.c).put(Constants.PROFILE_KEY_UNIT, Ey1.a(this.d));
        Wg6.b(put, "JSONObject().put(UIScrip\u2026ratureUnit.lowerCaseName)");
        JSONObject put2 = Gy1.c(put, this.e.a()).put("forecast_day", jSONArray).put("forecast_week", jSONArray2);
        Wg6.b(put2, "JSONObject().put(UIScrip\u2026ST_WEEK, dayForecastJSON)");
        return put2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Z80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Z80 z80 = (Z80) obj;
            if (this.b != z80.b) {
                return false;
            }
            if (this.d != z80.d) {
                return false;
            }
            if (!Wg6.a(this.c, z80.c)) {
                return false;
            }
            if (!Wg6.a(this.e, z80.e)) {
                return false;
            }
            if (!Arrays.equals(this.f, z80.f)) {
                return false;
            }
            return Arrays.equals(this.g, z80.g);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherInfo");
    }

    @DexIgnore
    public final W80 getCurrentWeatherInfo() {
        return this.e;
    }

    @DexIgnore
    public final X80[] getDayForecast() {
        return this.g;
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.b;
    }

    @DexIgnore
    public final Y80[] getHourForecast() {
        return this.f;
    }

    @DexIgnore
    public final String getLocation() {
        return this.c;
    }

    @DexIgnore
    public final H70 getTemperatureUnit() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Long.valueOf(this.b).hashCode();
        int hashCode2 = this.d.hashCode();
        return (((((((((hashCode * 31) + hashCode2) * 31) + this.c.hashCode()) * 31) + this.e.hashCode()) * 31) + Arrays.hashCode(this.f)) * 31) + Arrays.hashCode(this.g);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.M2, Long.valueOf(this.b)), Jd0.K, this.c), Jd0.u, Ey1.a(this.d)), Jd0.c2, this.e.toJSONObject()), Jd0.d2, Px1.a(this.f)), Jd0.e2, Px1.a(this.g));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.f, i);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.g, i);
        }
    }
}
