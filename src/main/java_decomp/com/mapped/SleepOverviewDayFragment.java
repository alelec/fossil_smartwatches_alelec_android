package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Aq0;
import com.fossil.Bn6;
import com.fossil.G37;
import com.fossil.Jb5;
import com.fossil.N04;
import com.fossil.Rj6;
import com.fossil.Sj6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewDayFragment extends BaseFragment implements Sj6 {
    @DexIgnore
    public G37<Jb5> g;
    @DexIgnore
    public Rj6 h;
    @DexIgnore
    public Bn6 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ String k; // = ThemeManager.l.a().d("nonBrandSurface");
    @DexIgnore
    public /* final */ String l; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String m; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Bi b; // = new Bi();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ Jb5 b;

        @DexIgnore
        public Ci(SleepOverviewDayFragment sleepOverviewDayFragment, Jb5 jb5) {
            this.a = sleepOverviewDayFragment;
            this.b = jb5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayFragment", "onPageScrolled " + i);
            if (!TextUtils.isEmpty(this.a.m)) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepOverviewDayFragment", "set icon color " + this.a.m);
                int parseColor = Color.parseColor(this.a.m);
                TabLayout.g v = this.b.r.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.l) && this.a.j != i) {
                int parseColor2 = Color.parseColor(this.a.l);
                TabLayout.g v2 = this.b.r.v(this.a.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.a.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayFragment a;

        @DexIgnore
        public Di(SleepOverviewDayFragment sleepOverviewDayFragment) {
            this.a = sleepOverviewDayFragment;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.l) && !TextUtils.isEmpty(this.a.m)) {
                int parseColor = Color.parseColor(this.a.l);
                int parseColor2 = Color.parseColor(this.a.m);
                gVar.o(2131230966);
                if (i == this.a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Rj6 rj6) {
        P6(rj6);
    }

    @DexIgnore
    public final void O6(Jb5 jb5) {
        jb5.s.setOnClickListener(Ai.b);
        String str = this.k;
        if (str != null) {
            jb5.v.setBackgroundColor(Color.parseColor(str));
            jb5.q.setBackgroundColor(Color.parseColor(str));
        }
        jb5.t.setOnClickListener(Bi.b);
        this.i = new Bn6(new ArrayList());
        ViewPager2 viewPager2 = jb5.u;
        Wg6.b(viewPager2, "binding.rvSleeps");
        viewPager2.setAdapter(this.i);
        jb5.u.g(new Ci(this, jb5));
        new N04(jb5.r, jb5.u, new Di(this)).a();
        ViewPager2 viewPager22 = jb5.u;
        Wg6.b(viewPager22, "binding.rvSleeps");
        viewPager22.setCurrentItem(this.j);
    }

    @DexIgnore
    public void P6(Rj6 rj6) {
        Wg6.c(rj6, "presenter");
        this.h = rj6;
    }

    @DexIgnore
    @Override // com.fossil.Sj6
    public void S4(List<Bn6.Bi> list) {
        Jb5 a2;
        TabLayout tabLayout;
        Jb5 a3;
        TabLayout tabLayout2;
        Jb5 a4;
        FlexibleTextView flexibleTextView;
        Jb5 a5;
        FlexibleTextView flexibleTextView2;
        Wg6.c(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            G37<Jb5> g37 = this.g;
            if (g37 != null && (a5 = g37.a()) != null && (flexibleTextView2 = a5.v) != null) {
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        G37<Jb5> g372 = this.g;
        if (!(g372 == null || (a4 = g372.a()) == null || (flexibleTextView = a4.v) == null)) {
            flexibleTextView.setVisibility(8);
        }
        if (list.size() > 1) {
            G37<Jb5> g373 = this.g;
            if (!(g373 == null || (a3 = g373.a()) == null || (tabLayout2 = a3.r) == null)) {
                tabLayout2.setVisibility(0);
            }
        } else {
            G37<Jb5> g374 = this.g;
            if (!(g374 == null || (a2 = g374.a()) == null || (tabLayout = a2.r) == null)) {
                tabLayout.setVisibility(8);
            }
        }
        Bn6 bn6 = this.i;
        if (bn6 != null) {
            bn6.i(list);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Jb5 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        Jb5 jb5 = (Jb5) Aq0.f(layoutInflater, 2131558625, viewGroup, false, A6());
        Wg6.b(jb5, "binding");
        O6(jb5);
        G37<Jb5> g37 = new G37<>(this, jb5);
        this.g = g37;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        Rj6 rj6 = this.h;
        if (rj6 != null) {
            rj6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        Rj6 rj6 = this.h;
        if (rj6 != null) {
            rj6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
