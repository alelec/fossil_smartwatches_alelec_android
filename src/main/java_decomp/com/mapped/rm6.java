package com.mapped;

import com.fossil.Dw7;
import com.fossil.Qu7;
import com.fossil.Su7;
import com.mapped.Af6;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rm6 extends Af6.Bi {
    @DexIgnore
    public static final Bi r = Bi.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* synthetic */ void a(Rm6 rm6, CancellationException cancellationException, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    cancellationException = null;
                }
                rm6.D(cancellationException);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: cancel");
        }

        @DexIgnore
        public static <R> R b(Rm6 rm6, R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
            return (R) Af6.Bi.Aii.a(rm6, r, coroutine);
        }

        @DexIgnore
        public static <E extends Af6.Bi> E c(Rm6 rm6, Af6.Ci<E> ci) {
            return (E) Af6.Bi.Aii.b(rm6, ci);
        }

        @DexIgnore
        public static /* synthetic */ Dw7 d(Rm6 rm6, boolean z, boolean z2, Hg6 hg6, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return rm6.j(z, z2, hg6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }

        @DexIgnore
        public static Af6 e(Rm6 rm6, Af6.Ci<?> ci) {
            return Af6.Bi.Aii.c(rm6, ci);
        }

        @DexIgnore
        public static Af6 f(Rm6 rm6, Af6 af6) {
            return Af6.Bi.Aii.d(rm6, af6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Af6.Ci<Rm6> {
        @DexIgnore
        public static /* final */ /* synthetic */ Bi a; // = new Bi();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.q;
        }
        */
    }

    @DexIgnore
    Dw7 A(Hg6<? super Throwable, Cd6> hg6);

    @DexIgnore
    void D(CancellationException cancellationException);

    @DexIgnore
    Qu7 L(Su7 su7);

    @DexIgnore
    boolean isActive();

    @DexIgnore
    Dw7 j(boolean z, boolean z2, Hg6<? super Throwable, Cd6> hg6);

    @DexIgnore
    CancellationException k();

    @DexIgnore
    boolean start();
}
