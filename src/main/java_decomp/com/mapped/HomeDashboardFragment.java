package com.mapped;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.Bz5;
import com.fossil.Dd6;
import com.fossil.Eh6;
import com.fossil.G37;
import com.fossil.G67;
import com.fossil.Hr7;
import com.fossil.Ii6;
import com.fossil.Kd6;
import com.fossil.L67;
import com.fossil.Ld6;
import com.fossil.Ll5;
import com.fossil.Lr7;
import com.fossil.Ls0;
import com.fossil.Mj6;
import com.fossil.Ml5;
import com.fossil.Oa1;
import com.fossil.Qv5;
import com.fossil.Rh5;
import com.fossil.S37;
import com.fossil.Um5;
import com.fossil.V75;
import com.fossil.Ve6;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.fossil.Wa1;
import com.fossil.Wr4;
import com.google.android.material.appbar.AppBarLayout;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDashboardFragment extends Qv5 implements Ld6, Aw5, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai Y; // = new Ai(null);
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public DashboardActivityPresenter G;
    @DexIgnore
    public DashboardActiveTimePresenter H;
    @DexIgnore
    public DashboardCaloriesPresenter I;
    @DexIgnore
    public DashboardHeartRatePresenter J;
    @DexIgnore
    public DashboardSleepPresenter K;
    @DexIgnore
    public DashboardGoalTrackingPresenter L;
    @DexIgnore
    public G37<V75> M;
    @DexIgnore
    public Kd6 N;
    @DexIgnore
    public /* final */ ArrayList<Fragment> O; // = new ArrayList<>();
    @DexIgnore
    public int P;
    @DexIgnore
    public L67 Q;
    @DexIgnore
    public ObjectAnimator R;
    @DexIgnore
    public int S; // = -1;
    @DexIgnore
    public Wa1 T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public /* final */ int W;
    @DexIgnore
    public HashMap X;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeDashboardFragment a() {
            return new HomeDashboardFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar c;

        @DexIgnore
        public Bi(HomeDashboardFragment homeDashboardFragment, ProgressBar progressBar) {
            this.b = homeDashboardFragment;
            this.c = progressBar;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.c.setProgress(0);
            this.c.setVisibility(4);
            this.b.S = -1;
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ V75 a;
        @DexIgnore
        public /* final */ /* synthetic */ Animation b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore
            public Aii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                Wg6.c(str, "serial");
                Wg6.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "onImageCallback, filePath=" + str2);
                if (!TextUtils.isEmpty(str2)) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onImageCallback, new logo");
                    HomeDashboardFragment.N6(this.a.c).t(str2).F0(this.a.a.O);
                    Ci ci = this.a;
                    ci.a.O.startAnimation(ci.b);
                }
            }
        }

        @DexIgnore
        public Ci(V75 v75, Animation animation, HomeDashboardFragment homeDashboardFragment) {
            this.a = v75;
            this.b = animation;
            this.c = homeDashboardFragment;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "activeDeviceSerialLiveData onChange " + str);
            if (!TextUtils.isEmpty(str)) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                Wg6.b(str, "serial");
                with.setSerialNumber(str).setSerialPrefix(DeviceHelper.o.m(str)).setType(Constants.DeviceType.TYPE_BRAND_LOGO).setImageCallback(new Aii(this)).download();
                return;
            }
            this.a.O.setImageResource(2131231246);
            this.a.O.startAnimation(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Di(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(4);
            HomeDashboardFragment.M6(this.b).q(4);
            this.b.b7(4);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CustomSwipeRefreshLayout.c {
        @DexIgnore
        public /* final */ /* synthetic */ V75 a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Ei(V75 v75, HomeDashboardFragment homeDashboardFragment) {
            this.a = v75;
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void a() {
            FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onRefresh");
            HomeDashboardFragment.M6(this.b).r();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void b() {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onStartSwipe");
            FlexibleProgressBar flexibleProgressBar = this.a.Y;
            Wg6.b(flexibleProgressBar, "binding.syncProgress");
            flexibleProgressBar.setVisibility(4);
            View view = this.a.d0;
            Wg6.b(view, "binding.vBorderBottom");
            view.setVisibility(0);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void c(boolean z) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onEndSwipe");
            this.b.U6(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Fi(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Wg6.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Gi(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final boolean onLongClick(View view) {
            DebugActivity.a aVar = DebugActivity.O;
            FragmentActivity requireActivity = this.b.requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            aVar.a(requireActivity);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Hi(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(0);
            HomeDashboardFragment.M6(this.b).q(0);
            this.b.b7(0);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Ii(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(1);
            HomeDashboardFragment.M6(this.b).q(1);
            this.b.b7(1);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Ji(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(2);
            HomeDashboardFragment.M6(this.b).q(2);
            this.b.b7(2);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Ki(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(3);
            HomeDashboardFragment.M6(this.b).q(3);
            this.b.b7(3);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;

        @DexIgnore
        public Li(HomeDashboardFragment homeDashboardFragment) {
            this.b = homeDashboardFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeDashboardFragment.M6(this.b).s(5);
            HomeDashboardFragment.M6(this.b).q(5);
            this.b.b7(5);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements AppBarLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ V75 a;

        @DexIgnore
        public Mi(V75 v75) {
            this.a = v75;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.c
        public void a(AppBarLayout appBarLayout, int i) {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a.X;
            Wg6.b(customSwipeRefreshLayout, "binding.srlPullToSync");
            customSwipeRefreshLayout.setEnabled(Math.abs(i) == 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 b;

        @DexIgnore
        public Ni(Hh6 hh6, FlexibleProgressBar flexibleProgressBar, Hh6 hh62, HomeDashboardFragment homeDashboardFragment, int i) {
            this.a = flexibleProgressBar;
            this.b = hh62;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FlexibleProgressBar flexibleProgressBar = this.a;
            Wg6.b(flexibleProgressBar, "it");
            int i = this.b.element;
            Wg6.b(valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                flexibleProgressBar.setProgress(((Integer) animatedValue).intValue() + i);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDashboardFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public Oi(Hh6 hh6, FlexibleProgressBar flexibleProgressBar, Hh6 hh62, HomeDashboardFragment homeDashboardFragment, int i) {
            this.a = flexibleProgressBar;
            this.b = homeDashboardFragment;
            this.c = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            Wg6.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animation");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "onAnimationEnd " + this.c);
            HomeDashboardFragment homeDashboardFragment = this.b;
            FlexibleProgressBar flexibleProgressBar = this.a;
            Wg6.b(flexibleProgressBar, "it");
            homeDashboardFragment.R6(flexibleProgressBar, this.c);
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            Wg6.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Wg6.c(animator, "animation");
        }
    }

    @DexIgnore
    public HomeDashboardFragment() {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        this.U = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = ThemeManager.l.a().d("hybridInactiveTab");
        this.V = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        this.W = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
    }

    @DexIgnore
    public static final /* synthetic */ Kd6 M6(HomeDashboardFragment homeDashboardFragment) {
        Kd6 kd6 = homeDashboardFragment.N;
        if (kd6 != null) {
            return kd6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Wa1 N6(HomeDashboardFragment homeDashboardFragment) {
        Wa1 wa1 = homeDashboardFragment.T;
        if (wa1 != null) {
            return wa1;
        }
        Wg6.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void A(boolean z2) {
        if (isActive()) {
            G37<V75> g37 = this.M;
            if (g37 != null) {
                V75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    Wg6.b(constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(8);
                    a2.X.setDisableSwipe(false);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeDashboardFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void G0() {
        AppBarLayout appBarLayout;
        if (isActive()) {
            G37<V75> g37 = this.M;
            if (g37 != null) {
                V75 a2 = g37.a();
                if (a2 != null && (appBarLayout = a2.q) != null) {
                    appBarLayout.r(true, true);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void J1(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z2) {
        double d;
        double d2;
        int i2;
        int i3;
        int i4;
        String sb;
        String format;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("setDataSummaryForTabs - latestGoalTrackingTarget=");
        sb2.append(num);
        sb2.append(", ");
        sb2.append("heartRateResting=");
        sb2.append(num2);
        sb2.append(", isNewSession=");
        sb2.append(z2);
        sb2.append(", mBinding.get()=");
        G37<V75> g37 = this.M;
        if (g37 != null) {
            sb2.append(g37.a());
            sb2.append(", hashCode=");
            sb2.append(hashCode());
            local.d("HomeDashboardFragment", sb2.toString());
            G37<V75> g372 = this.M;
            if (g372 != null) {
                V75 a2 = g372.a();
                if (a2 != null) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    int sleepMinutes = mFSleepDay != null ? mFSleepDay.getSleepMinutes() : 0;
                    int intValue = num2 != null ? num2.intValue() : 0;
                    if (activitySummary != null) {
                        d = activitySummary.getSteps();
                        int activeTime = activitySummary.getActiveTime();
                        d2 = activitySummary.getCalories();
                        i2 = activeTime;
                    } else {
                        d = 0.0d;
                        d2 = 0.0d;
                        i2 = 0;
                    }
                    if (goalTrackingSummary != null) {
                        i3 = goalTrackingSummary.getGoalTarget();
                        i4 = goalTrackingSummary.getTotalTracked();
                    } else {
                        i3 = 0;
                        i4 = 0;
                    }
                    if (i3 == 0 && num != null) {
                        i3 = num.intValue();
                    }
                    String c = Um5.c(instance, 2131887327);
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "setDataSummaryForTabs - steps=" + d + ", activeTime=" + i2 + ", calories=" + d2 + ", goalTarget=" + i3 + ", goalTotalTracked=" + i4 + ", sleepMinutes=" + sleepMinutes + ", resting=" + intValue);
                    FlexibleFitnessTab flexibleFitnessTab = a2.F;
                    int i5 = (d > 0.0d ? 1 : (d == 0.0d ? 0 : -1));
                    String d3 = (i5 != 0 || !z2) ? Ml5.a.d(Integer.valueOf(Lr7.a(d))) : c;
                    Wg6.b(d3, "if (steps == 0.0 && isNe\u2026ormat(steps.roundToInt())");
                    flexibleFitnessTab.K(d3);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.E;
                    String a3 = (i2 != 0 || !z2) ? Ml5.a.a(Integer.valueOf(i2)) : c;
                    Wg6.b(a3, "if (activeTime == 0 && i\u2026iveTimeFormat(activeTime)");
                    flexibleFitnessTab2.K(a3);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String b = (d2 != 0.0d || !z2) ? Ml5.a.b(Float.valueOf((float) d2)) : c;
                    Wg6.b(b, "if (calories == 0.0 && i\u2026ormat(calories.toFloat())");
                    flexibleFitnessTab3.K(b);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.J;
                    if (sleepMinutes != 0 || !z2) {
                        StringBuilder sb3 = new StringBuilder();
                        Hr7 hr7 = Hr7.a;
                        Locale locale = Locale.US;
                        Wg6.b(locale, "Locale.US");
                        String format2 = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes / 60)}, 1));
                        Wg6.b(format2, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format2);
                        sb3.append(":");
                        Hr7 hr72 = Hr7.a;
                        Locale locale2 = Locale.US;
                        Wg6.b(locale2, "Locale.US");
                        String format3 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes % 60)}, 1));
                        Wg6.b(format3, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format3);
                        sb = sb3.toString();
                    } else {
                        sb = Um5.c(instance, 2131887329);
                    }
                    Wg6.b(sb, "if (sleepMinutes == 0 &&\u2026              .toString()");
                    flexibleFitnessTab4.K(sb);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.H;
                    if (i3 != 0 || !z2) {
                        Hr7 hr73 = Hr7.a;
                        String c2 = Um5.c(instance, 2131886756);
                        Wg6.b(c2, "LanguageHelper.getString\u2026ingToday_Label__OfNumber)");
                        format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                    } else {
                        format = c;
                    }
                    Wg6.b(format, "if (goalTarget == 0 && i\u2026l__OfNumber), goalTarget)");
                    flexibleFitnessTab5.J(format);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                    String valueOf = (i4 != 0 || !z2) ? String.valueOf(i4) : c;
                    Wg6.b(valueOf, "if (goalTotalTracked == \u2026alTotalTracked.toString()");
                    flexibleFitnessTab6.K(valueOf);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.I;
                    if (i5 != 0 || !z2) {
                        c = String.valueOf(intValue);
                    }
                    Wg6.b(c, "if (steps == 0.0 && isNe\u2026e else resting.toString()");
                    flexibleFitnessTab7.K(c);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        Y6((Kd6) obj);
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void Q5() {
        if (isActive()) {
            G37<V75> g37 = this.M;
            if (g37 != null) {
                V75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    Wg6.b(constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = a2.Q;
                    Wg6.b(flexibleProgressBar, "it.pbProgress");
                    flexibleProgressBar.setMax(100);
                    a2.X.setDisableSwipe(true);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void R1(String str, String str2) {
        L67 l67 = this.Q;
        if (l67 != null) {
            l67.c(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if ((str.length() > 0) && Wg6.a(str, "ASK_TO_CANCEL_WORKOUT") && getActivity() != null) {
            if (i2 == 2131363291) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "don't quit");
                Kd6 kd6 = this.N;
                if (kd6 != null) {
                    kd6.p(PortfolioApp.get.instance().J(), false);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131363373) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "quit and sync");
                Kd6 kd62 = this.N;
                if (kd62 != null) {
                    kd62.p(PortfolioApp.get.instance().J(), true);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void R6(ProgressBar progressBar, int i2) {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "animateSyncProgressEnd " + progressBar);
        if (i2 == 0) {
            progressBar.setVisibility(0);
        } else if (i2 == 1) {
            X6(2);
        } else if (i2 == 2) {
            TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, progressBar.getY(), progressBar.getY() - ((float) progressBar.getHeight()));
            translateAnimation.setDuration((long) SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            translateAnimation.setAnimationListener(new Bi(this, progressBar));
            progressBar.startAnimation(translateAnimation);
            G37<V75> g37 = this.M;
            if (g37 != null) {
                V75 a2 = g37.a();
                if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                    customSwipeRefreshLayout.k();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void S6() {
        Kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            int o = kd6.o();
            if (!this.O.isEmpty()) {
                int size = this.O.size();
                int i2 = 0;
                while (i2 < size) {
                    if (this.O.get(i2) instanceof Aw5) {
                        Fragment fragment = this.O.get(i2);
                        if (fragment != null) {
                            ((Aw5) fragment).b2(i2 == o);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                        }
                    }
                    i2++;
                }
            }
            if (o == 0) {
                E6("steps_view");
            } else if (o == 1) {
                E6("active_minutes_view");
            } else if (o == 2) {
                E6("calories_view");
            } else if (o == 3) {
                E6("heart_rate_view");
            } else if (o == 5) {
                E6("sleep_view");
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "cancelSyncProgress");
        ObjectAnimator objectAnimator = this.R;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.Y;
                Wg6.b(flexibleProgressBar, "it.syncProgress");
                flexibleProgressBar.setVisibility(4);
                FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                Wg6.b(flexibleProgressBar2, "it.syncProgress");
                flexibleProgressBar2.setProgress(0);
                a2.X.k();
            }
            Kd6 kd6 = this.N;
            if (kd6 != null) {
                kd6.t(false);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void U0() {
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.i(childFragmentManager);
    }

    @DexIgnore
    public final void U6(boolean z2) {
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                if (z2) {
                    FlexibleProgressBar flexibleProgressBar = a2.Y;
                    Wg6.b(flexibleProgressBar, "syncProgress");
                    flexibleProgressBar.setVisibility(0);
                } else {
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    Wg6.b(flexibleProgressBar2, "syncProgress");
                    flexibleProgressBar2.setVisibility(4);
                }
                View view = a2.d0;
                Wg6.b(view, "vBorderBottom");
                view.setVisibility(4);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6() {
        Fragment Z = getChildFragmentManager().Z(Ve6.s.a());
        Fragment Z2 = getChildFragmentManager().Z("DashboardActiveTimeFragment");
        Fragment Z3 = getChildFragmentManager().Z("DashboardCaloriesFragment");
        Fragment Z4 = getChildFragmentManager().Z(Ii6.s.a());
        Fragment Z5 = getChildFragmentManager().Z(Mj6.s.a());
        Fragment Z6 = getChildFragmentManager().Z(Eh6.s.a());
        if (Z == null) {
            Z = Ve6.s.b();
        }
        if (Z2 == null) {
            Z2 = DashboardActiveTimeFragment.m.a();
        }
        if (Z3 == null) {
            Z3 = new DashboardCaloriesFragment();
        }
        if (Z4 == null) {
            Z4 = Ii6.s.b();
        }
        if (Z5 == null) {
            Z5 = Mj6.s.b();
        }
        if (Z6 == null) {
            Z6 = Eh6.s.b();
        }
        this.O.clear();
        this.O.add(Z);
        this.O.add(Z2);
        this.O.add(Z3);
        this.O.add(Z4);
        this.O.add(Z6);
        this.O.add(Z5);
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                Kd6 kd6 = this.N;
                if (kd6 != null) {
                    DeviceHelper.Ai ai = DeviceHelper.o;
                    if (kd6 == null) {
                        Wg6.n("mPresenter");
                        throw null;
                    } else if (ai.w(kd6.n())) {
                        FlexibleFitnessTab flexibleFitnessTab = a2.E;
                        Wg6.b(flexibleFitnessTab, "flexibleTabActiveTime");
                        flexibleFitnessTab.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab2 = a2.I;
                        Wg6.b(flexibleFitnessTab2, "flexibleTabHeartRate");
                        flexibleFitnessTab2.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab3 = a2.H;
                        Wg6.b(flexibleFitnessTab3, "flexibleTabGoalTracking");
                        flexibleFitnessTab3.setVisibility(8);
                    } else {
                        FlexibleFitnessTab flexibleFitnessTab4 = a2.E;
                        Wg6.b(flexibleFitnessTab4, "flexibleTabActiveTime");
                        flexibleFitnessTab4.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab5 = a2.I;
                        Wg6.b(flexibleFitnessTab5, "flexibleTabHeartRate");
                        flexibleFitnessTab5.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                        Wg6.b(flexibleFitnessTab6, "flexibleTabGoalTracking");
                        flexibleFitnessTab6.setVisibility(0);
                    }
                }
                ViewPager2 viewPager2 = a2.W;
                Wg6.b(viewPager2, "rvTabs");
                viewPager2.setAdapter(new G67(getChildFragmentManager(), this.O));
                if (a2.W.getChildAt(0) != null) {
                    View childAt = a2.W.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.W;
                Wg6.b(viewPager22, "rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            Iface iface = PortfolioApp.get.instance().getIface();
            if (Z != null) {
                Ve6 ve6 = (Ve6) Z;
                if (Z2 != null) {
                    DashboardActiveTimeFragment dashboardActiveTimeFragment = (DashboardActiveTimeFragment) Z2;
                    DashboardCaloriesFragment dashboardCaloriesFragment = (DashboardCaloriesFragment) Z3;
                    if (Z4 != null) {
                        Ii6 ii6 = (Ii6) Z4;
                        if (Z5 != null) {
                            Mj6 mj6 = (Mj6) Z5;
                            if (Z6 != null) {
                                iface.t0(new Dd6(ve6, dashboardActiveTimeFragment, dashboardCaloriesFragment, ii6, mj6, (Eh6) Z6)).a(this);
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                        }
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        String d = ThemeManager.l.a().d("onDianaStepsTab");
        if (d != null) {
            this.h = Color.parseColor(d);
            Cd6 cd6 = Cd6.a;
        } else {
            this.h = W6.d(requireContext(), 2131099943);
            Cd6 cd62 = Cd6.a;
        }
        String d2 = ThemeManager.l.a().d("onDianaActiveMinutesTab");
        if (d2 != null) {
            this.i = Color.parseColor(d2);
            Cd6 cd63 = Cd6.a;
        } else {
            this.i = W6.d(requireContext(), 2131099943);
            Cd6 cd64 = Cd6.a;
        }
        String d3 = ThemeManager.l.a().d("onDianaActiveCaloriesTab");
        if (d3 != null) {
            this.j = Color.parseColor(d3);
            Cd6 cd65 = Cd6.a;
        } else {
            this.j = W6.d(requireContext(), 2131099943);
            Cd6 cd66 = Cd6.a;
        }
        String d4 = ThemeManager.l.a().d("onDianaHeartRateTab");
        if (d4 != null) {
            this.k = Color.parseColor(d4);
            Cd6 cd67 = Cd6.a;
        } else {
            this.k = W6.d(requireContext(), 2131099943);
            Cd6 cd68 = Cd6.a;
        }
        String d5 = ThemeManager.l.a().d("onDianaSleepTab");
        if (d5 != null) {
            this.l = Color.parseColor(d5);
            Cd6 cd69 = Cd6.a;
        } else {
            this.l = W6.d(requireContext(), 2131099943);
            Cd6 cd610 = Cd6.a;
        }
        String d6 = ThemeManager.l.a().d("onDianaInactiveTab");
        if (d6 != null) {
            this.m = Color.parseColor(d6);
            Cd6 cd611 = Cd6.a;
        } else {
            this.m = W6.d(requireContext(), 2131099947);
            Cd6 cd612 = Cd6.a;
        }
        String d7 = ThemeManager.l.a().d("onHybridStepsTab");
        if (d7 != null) {
            this.s = Color.parseColor(d7);
            Cd6 cd613 = Cd6.a;
        } else {
            this.s = W6.d(requireContext(), 2131099943);
            Cd6 cd614 = Cd6.a;
        }
        String d8 = ThemeManager.l.a().d("onHybridActiveCaloriesTab");
        if (d8 != null) {
            this.t = Color.parseColor(d8);
            Cd6 cd615 = Cd6.a;
        } else {
            this.t = W6.d(requireContext(), 2131099943);
            Cd6 cd616 = Cd6.a;
        }
        String d9 = ThemeManager.l.a().d("onHybridSleepTab");
        if (d9 != null) {
            this.u = Color.parseColor(d9);
            Cd6 cd617 = Cd6.a;
        } else {
            this.u = W6.d(requireContext(), 2131099943);
            Cd6 cd618 = Cd6.a;
        }
        String d10 = ThemeManager.l.a().d("onHybridInactiveTab");
        if (d10 != null) {
            this.v = Color.parseColor(d10);
            Cd6 cd619 = Cd6.a;
        } else {
            this.v = W6.d(requireContext(), 2131099948);
            Cd6 cd620 = Cd6.a;
        }
        String d11 = ThemeManager.l.a().d("onHybridGoalTrackingTab");
        if (d11 != null) {
            this.w = Color.parseColor(d11);
            Cd6 cd621 = Cd6.a;
        } else {
            this.w = W6.d(requireContext(), 2131099943);
            Cd6 cd622 = Cd6.a;
        }
        String d12 = ThemeManager.l.a().d("dianaStepsTab");
        if (d12 != null) {
            this.x = Color.parseColor(d12);
            Cd6 cd623 = Cd6.a;
        } else {
            this.x = W6.d(requireContext(), 2131099812);
            Cd6 cd624 = Cd6.a;
        }
        String d13 = ThemeManager.l.a().d("dianaActiveMinutesTab");
        if (d13 != null) {
            this.y = Color.parseColor(d13);
            Cd6 cd625 = Cd6.a;
        } else {
            this.y = W6.d(requireContext(), 2131099807);
            Cd6 cd626 = Cd6.a;
        }
        String d14 = ThemeManager.l.a().d("dianaActiveCaloriesTab");
        if (d14 != null) {
            this.z = Color.parseColor(d14);
            Cd6 cd627 = Cd6.a;
        } else {
            this.z = W6.d(requireContext(), 2131099806);
            Cd6 cd628 = Cd6.a;
        }
        String d15 = ThemeManager.l.a().d("dianaHeartRateTab");
        if (d15 != null) {
            this.A = Color.parseColor(d15);
            Cd6 cd629 = Cd6.a;
        } else {
            this.A = W6.d(requireContext(), 2131099808);
            Cd6 cd630 = Cd6.a;
        }
        String d16 = ThemeManager.l.a().d("dianaSleepTab");
        if (d16 != null) {
            this.B = Color.parseColor(d16);
            Cd6 cd631 = Cd6.a;
        } else {
            this.B = W6.d(requireContext(), 2131099810);
            Cd6 cd632 = Cd6.a;
        }
        String d17 = ThemeManager.l.a().d("dianaInactiveTab");
        if (d17 != null) {
            Color.parseColor(d17);
            Cd6 cd633 = Cd6.a;
        } else {
            W6.d(requireContext(), 2131099809);
            Cd6 cd634 = Cd6.a;
        }
        String d18 = ThemeManager.l.a().d("hybridStepsTab");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "ABCD=" + d18);
        String d19 = ThemeManager.l.a().d("hybridStepsTab");
        if (d19 != null) {
            this.C = Color.parseColor(d19);
            Cd6 cd635 = Cd6.a;
        } else {
            this.C = W6.d(requireContext(), 2131099943);
            Cd6 cd636 = Cd6.a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDashboardFragment", "hybridStepsColor=" + this.C);
        String d20 = ThemeManager.l.a().d("hybridActiveCaloriesTab");
        if (d20 != null) {
            this.D = Color.parseColor(d20);
            Cd6 cd637 = Cd6.a;
        } else {
            this.D = W6.d(requireContext(), 2131099943);
            Cd6 cd638 = Cd6.a;
        }
        String d21 = ThemeManager.l.a().d("hybridSleepTab");
        if (d21 != null) {
            this.E = Color.parseColor(d21);
            Cd6 cd639 = Cd6.a;
        } else {
            this.E = W6.d(requireContext(), 2131099943);
            Cd6 cd640 = Cd6.a;
        }
        String d22 = ThemeManager.l.a().d("hybridInactiveTab");
        if (d22 != null) {
            Color.parseColor(d22);
            Cd6 cd641 = Cd6.a;
        } else {
            W6.d(requireContext(), 2131099948);
            Cd6 cd642 = Cd6.a;
        }
        String d23 = ThemeManager.l.a().d("hybridGoalTrackingTab");
        if (d23 != null) {
            this.F = Color.parseColor(d23);
            Cd6 cd643 = Cd6.a;
        } else {
            this.F = W6.d(requireContext(), 2131099853);
            Cd6 cd644 = Cd6.a;
        }
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                String d24 = ThemeManager.l.a().d("primaryText");
                if (d24 != null) {
                    a2.O.setColorFilter(Color.parseColor(d24), PorterDuff.Mode.SRC_ATOP);
                    Cd6 cd645 = Cd6.a;
                }
                a2.w.setBackgroundColor(this.U);
                a2.f0.setBackgroundColor(this.W);
                a2.t.setBackgroundColor(this.W);
                a2.A.setBackgroundColor(this.W);
                a2.u.setBackgroundColor(this.W);
                a2.z.setBackgroundColor(this.W);
                Cd6 cd646 = Cd6.a;
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final void X6(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("runProgress state ");
        sb.append(i2);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        Wg6.b(currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("HomeDashboardFragment", sb.toString());
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.Y) != null) {
                Hh6 hh6 = new Hh6();
                hh6.element = 0;
                Hh6 hh62 = new Hh6();
                hh62.element = 0;
                this.S = i2;
                if (i2 == 0) {
                    hh6.element = 0;
                    hh62.element = 2000;
                    i3 = 2000;
                } else if (i2 == 1) {
                    hh6.element = 2000;
                    i3 = 7000;
                    hh62.element = SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS;
                } else if (i2 != 2) {
                    i3 = 0;
                } else {
                    hh6.element = 9000;
                    hh62.element = 1000;
                    i3 = 1000;
                }
                Wg6.b(flexibleProgressBar, "it");
                flexibleProgressBar.setProgress(hh6.element);
                ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", i3);
                this.R = ofInt;
                if (ofInt != null) {
                    ofInt.setDuration((long) hh62.element);
                    ofInt.addUpdateListener(new Ni(hh62, flexibleProgressBar, hh6, this, i2));
                    ofInt.addListener(new Oi(hh62, flexibleProgressBar, hh6, this, i2));
                    ofInt.start();
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void Y6(Kd6 kd6) {
        Wg6.c(kd6, "presenter");
        this.N = kd6;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void Z1(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDashboardFragment", "syncCompleted - success: " + z2);
        if (!z2) {
            T6();
        } else {
            X6(1);
        }
    }

    @DexIgnore
    public final void Z6() {
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                RingProgressBar ringProgressBar = a2.T;
                Wg6.b(ringProgressBar, "it.rpbBiggest");
                a7(ringProgressBar, RingProgressBar.a.STEPS);
                DeviceHelper.Ai ai = DeviceHelper.o;
                Kd6 kd6 = this.N;
                if (kd6 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (ai.w(kd6.n())) {
                    RingProgressBar ringProgressBar2 = a2.V;
                    Wg6.b(ringProgressBar2, "it.rpbSmallest");
                    ringProgressBar2.setVisibility(0);
                    RingProgressBar ringProgressBar3 = a2.S;
                    Wg6.b(ringProgressBar3, "it.rpbBig");
                    a7(ringProgressBar3, RingProgressBar.a.ACTIVE_TIME);
                    RingProgressBar ringProgressBar4 = a2.U;
                    Wg6.b(ringProgressBar4, "it.rpbMedium");
                    a7(ringProgressBar4, RingProgressBar.a.CALORIES);
                    RingProgressBar ringProgressBar5 = a2.V;
                    Wg6.b(ringProgressBar5, "it.rpbSmallest");
                    a7(ringProgressBar5, RingProgressBar.a.SLEEP);
                    FlexibleFitnessTab flexibleFitnessTab = a2.E;
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886676);
                    Wg6.b(c, "LanguageHelper.getString\u2026n_StepsToday_Label__Mins)");
                    flexibleFitnessTab.J(c);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.F;
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886678);
                    Wg6.b(c2, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab2.J(c2);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String c3 = Um5.c(PortfolioApp.get.instance(), 2131886672);
                    Wg6.b(c3, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab3.J(c3);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.I;
                    String c4 = Um5.c(PortfolioApp.get.instance(), 2131886677);
                    Wg6.b(c4, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
                    flexibleFitnessTab4.J(c4);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.J;
                    String c5 = Um5.c(PortfolioApp.get.instance(), 2131886673);
                    Wg6.b(c5, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab5.J(c5);
                } else {
                    RingProgressBar ringProgressBar6 = a2.V;
                    Wg6.b(ringProgressBar6, "it.rpbSmallest");
                    ringProgressBar6.setVisibility(0);
                    RingProgressBar ringProgressBar7 = a2.S;
                    Wg6.b(ringProgressBar7, "it.rpbBig");
                    a7(ringProgressBar7, RingProgressBar.a.CALORIES);
                    RingProgressBar ringProgressBar8 = a2.U;
                    Wg6.b(ringProgressBar8, "it.rpbMedium");
                    a7(ringProgressBar8, RingProgressBar.a.SLEEP);
                    RingProgressBar ringProgressBar9 = a2.V;
                    Wg6.b(ringProgressBar9, "it.rpbSmallest");
                    a7(ringProgressBar9, RingProgressBar.a.GOAL);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.F;
                    String c6 = Um5.c(PortfolioApp.get.instance(), 2131886778);
                    Wg6.b(c6, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab6.J(c6);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.G;
                    String c7 = Um5.c(PortfolioApp.get.instance(), 2131886775);
                    Wg6.b(c7, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab7.J(c7);
                    FlexibleFitnessTab flexibleFitnessTab8 = a2.H;
                    String c8 = Um5.c(PortfolioApp.get.instance(), 2131886777);
                    Wg6.b(c8, "LanguageHelper.getString\u2026epsToday_Label__OfNumber)");
                    flexibleFitnessTab8.J(c8);
                    FlexibleFitnessTab flexibleFitnessTab9 = a2.J;
                    String c9 = Um5.c(PortfolioApp.get.instance(), 2131886776);
                    Wg6.b(c9, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab9.J(c9);
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(RingProgressBar ringProgressBar, RingProgressBar.a aVar) {
        int i2 = Bz5.a[aVar.ordinal()];
        if (i2 == 1) {
            ringProgressBar.setIconSource(2131231189);
            ringProgressBar.i(this.y, this.W);
            ringProgressBar.j("dianaActiveMinuteRing", "dianaUnfilledRing");
        } else if (i2 == 2) {
            ringProgressBar.setIconSource(2131231193);
            ringProgressBar.i(this.x, this.W);
            DeviceHelper.Ai ai = DeviceHelper.o;
            Kd6 kd6 = this.N;
            if (kd6 == null) {
                Wg6.n("mPresenter");
                throw null;
            } else if (ai.w(kd6.n())) {
                ringProgressBar.j("dianaStepsRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridStepsRing", "hybridUnfilledRing");
            }
        } else if (i2 == 3) {
            ringProgressBar.setIconSource(2131231190);
            ringProgressBar.i(this.z, this.W);
            DeviceHelper.Ai ai2 = DeviceHelper.o;
            Kd6 kd62 = this.N;
            if (kd62 == null) {
                Wg6.n("mPresenter");
                throw null;
            } else if (ai2.w(kd62.n())) {
                ringProgressBar.j("dianaActiveCaloriesRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridActiveCaloriesRing", "hybridUnfilledRing");
            }
        } else if (i2 == 4) {
            ringProgressBar.setIconSource(2131231192);
            ringProgressBar.i(this.B, this.W);
            DeviceHelper.Ai ai3 = DeviceHelper.o;
            Kd6 kd63 = this.N;
            if (kd63 == null) {
                Wg6.n("mPresenter");
                throw null;
            } else if (ai3.w(kd63.n())) {
                ringProgressBar.j("dianaSleepRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridSleepRing", "hybridUnfilledRing");
            }
        } else if (i2 == 5) {
            ringProgressBar.setIconSource(2131231191);
            ringProgressBar.i(this.F, this.W);
            ringProgressBar.j("hybridGoalTrackingRing", "hybridUnfilledRing");
        }
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("HomeDashboardFragment visible=");
        sb.append(z2);
        sb.append(", tracer=");
        sb.append(C6());
        sb.append(", isRunning=");
        Vl5 C6 = C6();
        sb.append(C6 != null ? Boolean.valueOf(C6.f()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z2) {
            Vl5 C62 = C6();
            if (C62 != null) {
                C62.i();
            }
            if (this.M != null) {
                int i2 = this.S;
                if (i2 == 2 || i2 == -1) {
                    T6();
                    return;
                }
                return;
            }
            return;
        }
        Vl5 C63 = C6();
        if (C63 != null) {
            C63.c("");
        }
    }

    @DexIgnore
    public final void b7(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "scroll to position=" + i2);
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                a2.F.I(this.V);
                a2.J.I(this.V);
                a2.G.I(this.V);
                a2.E.I(this.V);
                a2.I.I(this.V);
                a2.H.I(this.V);
                DeviceHelper.Ai ai = DeviceHelper.o;
                Kd6 kd6 = this.N;
                if (kd6 != null) {
                    if (ai.w(kd6.n())) {
                        a2.F.H(this.m);
                        a2.E.H(this.m);
                        a2.G.H(this.m);
                        a2.I.H(this.m);
                        a2.J.H(this.m);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.x);
                            a2.F.H(this.h);
                            a2.F.I(this.x);
                        } else if (i2 == 1) {
                            a2.f0.setBackgroundColor(this.y);
                            a2.E.I(this.y);
                            a2.E.H(this.i);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.z);
                            a2.G.I(this.z);
                            a2.G.H(this.j);
                        } else if (i2 == 3) {
                            a2.f0.setBackgroundColor(this.A);
                            a2.I.I(this.A);
                            a2.I.H(this.k);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.B);
                            a2.J.I(this.B);
                            a2.J.H(this.l);
                        }
                    } else {
                        a2.F.H(this.v);
                        a2.H.H(this.v);
                        a2.G.H(this.v);
                        a2.J.H(this.v);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.C);
                            a2.F.H(this.s);
                            a2.F.I(this.C);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.D);
                            a2.G.I(this.D);
                            a2.G.H(this.t);
                        } else if (i2 == 4) {
                            a2.f0.setBackgroundColor(this.F);
                            a2.H.I(this.F);
                            a2.H.H(this.w);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.E);
                            a2.J.I(this.E);
                            a2.J.H(this.u);
                        }
                    }
                    G37<V75> g372 = this.M;
                    if (g372 != null) {
                        V75 a3 = g372.a();
                        if (!(a3 == null || (viewPager2 = a3.W) == null)) {
                            viewPager2.j(i2, false);
                        }
                        this.P = i2;
                        Kd6 kd62 = this.N;
                        if (kd62 != null) {
                            kd62.q(i2);
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    } else {
                        Wg6.n("mBinding");
                        throw null;
                    }
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void g5(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                int d = FitnessHelper.c.d(activitySummary, Rh5.ACTIVE_TIME);
                float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                if (d > 0) {
                    f = (activitySummary != null ? (float) activitySummary.getActiveTime() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d);
                } else {
                    f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int d2 = FitnessHelper.c.d(activitySummary, Rh5.TOTAL_STEPS);
                if (d2 > 0) {
                    f2 = (activitySummary != null ? (float) activitySummary.getSteps() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d2);
                } else {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int d3 = FitnessHelper.c.d(activitySummary, Rh5.CALORIES);
                if (d3 > 0) {
                    f3 = (activitySummary != null ? (float) activitySummary.getCalories() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d3);
                } else {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int e = FitnessHelper.c.e(mFSleepDay);
                if (e > 0) {
                    f4 = (mFSleepDay != null ? (float) mFSleepDay.getSleepMinutes() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) e);
                } else {
                    f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int c = FitnessHelper.c.c(goalTrackingSummary);
                if (c > 0) {
                    if (goalTrackingSummary != null) {
                        f6 = (float) goalTrackingSummary.getTotalTracked();
                    }
                    f5 = f6 / ((float) c);
                } else {
                    f5 = 0.0f;
                }
                boolean z2 = f2 >= 1.0f && f3 >= 1.0f && f4 >= 1.0f;
                FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "updateVisualization steps: " + f2 + ", time: " + f + ", calories: " + f3 + ", sleep: " + f4);
                DeviceHelper.Ai ai = DeviceHelper.o;
                Kd6 kd6 = this.N;
                if (kd6 == null) {
                    Wg6.n("mPresenter");
                    throw null;
                } else if (ai.w(kd6.n())) {
                    boolean z3 = z2 && f >= 1.0f;
                    a2.T.l(f2, z3);
                    a2.S.l(f, z3);
                    a2.U.l(f3, z3);
                    a2.V.l(f4, z3);
                } else {
                    boolean z4 = z2 && f5 >= 1.0f;
                    a2.T.l(f2, z4);
                    a2.S.l(f3, z4);
                    a2.U.l(f4, z4);
                    a2.V.l(f5, z4);
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void o0(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "updateOtaProgress " + i2 + " isActive " + isActive());
        if (isActive()) {
            G37<V75> g37 = this.M;
            if (g37 != null) {
                V75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    Wg6.b(constraintLayout, "it.clUpdateFw");
                    if (constraintLayout.getVisibility() != 0) {
                        ConstraintLayout constraintLayout2 = a2.A;
                        Wg6.b(constraintLayout2, "it.clUpdateFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar = a2.Q;
                        Wg6.b(flexibleProgressBar, "it.pbProgress");
                        flexibleProgressBar.setMax(100);
                    }
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    Wg6.b(flexibleProgressBar2, "it.syncProgress");
                    if (flexibleProgressBar2.getVisibility() == 0) {
                        T6();
                    }
                    a2.X.setDisableSwipe(true);
                    FlexibleProgressBar flexibleProgressBar3 = a2.Q;
                    Wg6.b(flexibleProgressBar3, "it.pbProgress");
                    flexibleProgressBar3.setProgress(i2);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onActivityCreated");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.M = new G37<>(this, (V75) Aq0.f(layoutInflater, 2131558574, viewGroup, false, A6()));
        W6();
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            kd6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            b7(kd6.o());
            Z6();
            Kd6 kd62 = this.N;
            if (kd62 != null) {
                kd62.l();
                Vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        V6();
        Wa1 v2 = Oa1.v(this);
        Wg6.b(v2, "Glide.with(this)");
        this.T = v2;
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                PortfolioApp.get.instance().K().h(getViewLifecycleOwner(), new Ci(a2, AnimationUtils.loadAnimation(requireContext(), 2130771996), this));
                if (Wr4.a.a().e()) {
                    String Q2 = PortfolioApp.get.instance().Q();
                    if (Q2 != null) {
                        String upperCase = Q2.toUpperCase();
                        Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        FlexibleTextView flexibleTextView = a2.K;
                        Wg6.b(flexibleTextView, "binding.ftvDescription");
                        Hr7 hr7 = Hr7.a;
                        String c = Um5.c(getContext(), 2131887069);
                        Wg6.b(c, "LanguageHelper.getString\u2026rABrandSmartwatchToStart)");
                        String format = String.format(c, Arrays.copyOf(new Object[]{upperCase}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                        flexibleTextView.setText(format);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    String c2 = Um5.c(getContext(), 2131887069);
                    Wg6.b(c2, "withoutDeviceText");
                    String q = Vt7.q(c2, "%s", "", false, 4, null);
                    FlexibleTextView flexibleTextView2 = a2.K;
                    Wg6.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(q);
                }
                FlexibleProgressBar flexibleProgressBar = a2.Y;
                Wg6.b(flexibleProgressBar, "binding.syncProgress");
                flexibleProgressBar.setMax(10000);
                a2.X.setOnRefreshListener(new Ei(a2, this));
                View headView = a2.X.getHeadView();
                if (!(headView instanceof L67)) {
                    headView = null;
                }
                this.Q = (L67) headView;
                a2.D.setOnClickListener(new Fi(this));
                if (!PortfolioApp.get.instance().z0()) {
                    a2.O.setOnLongClickListener(new Gi(this));
                }
                a2.q.b(new Mi(a2));
                a2.F.setOnClickListener(new Hi(this));
                a2.E.setOnClickListener(new Ii(this));
                a2.G.setOnClickListener(new Ji(this));
                a2.I.setOnClickListener(new Ki(this));
                a2.J.setOnClickListener(new Li(this));
                a2.H.setOnClickListener(new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void q1(boolean z2, boolean z3, boolean z4) {
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z2 || !z4) {
                ConstraintLayout constraintLayout = a2.B;
                Wg6.b(constraintLayout, "binding.clVisualization");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.y;
                Wg6.b(constraintLayout2, "binding.clNoDevice");
                constraintLayout2.setVisibility(8);
                a2.X.setByPass(!z3);
                if (!z4 && !z3) {
                    ImageView imageView = a2.N;
                    Wg6.b(imageView, "binding.ivNoWatchFound");
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            ConstraintLayout constraintLayout3 = a2.B;
            Wg6.b(constraintLayout3, "binding.clVisualization");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.y;
            Wg6.b(constraintLayout4, "binding.clNoDevice");
            constraintLayout4.setVisibility(0);
            a2.X.setByPass(true);
            ImageView imageView2 = a2.N;
            Wg6.b(imageView2, "binding.ivNoWatchFound");
            imageView2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void q6() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.x0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void r2(Date date) {
        Wg6.c(date, "date");
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.a0;
                Wg6.b(flexibleTextView, "tvToday");
                Hr7 hr7 = Hr7.a;
                String string = PortfolioApp.get.instance().getString(2131886691);
                Wg6.b(string, "PortfolioApp.instance.ge\u2026ay_Title__TodayMonthDate)");
                String format = String.format(string, Arrays.copyOf(new Object[]{Ll5.c(date)}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void t1() {
        if (isActive()) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.c(childFragmentManager);
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void t6() {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "showAutoSync");
        X6(0);
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                customSwipeRefreshLayout.r();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.X;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ld6
    public void x3(boolean z2) {
        G37<V75> g37 = this.M;
        if (g37 != null) {
            V75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                FlexibleTextView flexibleTextView = a2.M;
                Wg6.b(flexibleTextView, "it.ftvLowBattery");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886847);
                Wg6.b(c, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                String format = String.format(c, Arrays.copyOf(new Object[]{"25%"}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                NestedScrollView nestedScrollView = a2.P;
                Wg6.b(nestedScrollView, "it.nsvLowBattery");
                nestedScrollView.setVisibility(0);
                return;
            }
            NestedScrollView nestedScrollView2 = a2.P;
            Wg6.b(nestedScrollView2, "it.nsvLowBattery");
            nestedScrollView2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }
}
