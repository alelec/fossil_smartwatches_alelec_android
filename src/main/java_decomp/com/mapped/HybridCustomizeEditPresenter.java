package com.mapped;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hl5;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.M66;
import com.fossil.N66;
import com.fossil.Ss0;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Yb6;
import com.fossil.Yn7;
import com.fossil.Zb6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditPresenter extends Yb6 {
    @DexIgnore
    public HybridCustomizeViewModel e;
    @DexIgnore
    public MutableLiveData<HybridPreset> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<M66> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<M66> i;
    @DexIgnore
    public /* final */ Zb6 j;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $currentPreset$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $it;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Parcelable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPresetAppSetting $buttonMapping;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, HybridPresetAppSetting hybridPresetAppSetting, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$buttonMapping = hybridPresetAppSetting;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$buttonMapping, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Parcelable> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return HybridCustomizeEditPresenter.x(this.this$0.this$0).q(this.$buttonMapping.getAppId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements CoroutineUseCase.Ei<SetHybridPresetToWatchUseCase.Ci, SetHybridPresetToWatchUseCase.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Bii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(SetHybridPresetToWatchUseCase.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(SetHybridPresetToWatchUseCase.Ai ai) {
                Wg6.c(ai, "errorValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch onError");
                this.a.this$0.j.w();
                int b = ai.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HybridCustomizeEditPresenter", "setPresetToWatch() - mSetHybridPresetToWatchUseCase - onError - lastErrorCode = " + b);
                if (b != 1101) {
                    if (b == 8888) {
                        this.a.this$0.j.c();
                        return;
                    } else if (!(b == 1112 || b == 1113)) {
                        this.a.this$0.j.P();
                        return;
                    }
                }
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ai.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                Zb6 zb6 = this.a.this$0.j;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    zb6.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }

            @DexIgnore
            public void c(SetHybridPresetToWatchUseCase.Ci ci) {
                Wg6.c(ci, "responseValue");
                FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "setToWatch success");
                this.a.this$0.j.w();
                this.a.this$0.j.t0(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(SetHybridPresetToWatchUseCase.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HybridPreset hybridPreset, Xe6 xe6, HybridCustomizeEditPresenter hybridCustomizeEditPresenter, boolean z, HybridPreset hybridPreset2) {
            super(2, xe6);
            this.$it = hybridPreset;
            this.this$0 = hybridCustomizeEditPresenter;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$currentPreset$inlined = hybridPreset2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$it, xe6, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$currentPreset$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0089, code lost:
            r2 = r14.this$0.h();
            r6 = new com.mapped.HybridCustomizeEditPresenter.Ai.Aii(r14, r1, null);
            r14.L$0 = r3;
            r14.L$1 = r5;
            r14.L$2 = r10;
            r14.L$3 = r1;
            r14.L$4 = r9;
            r14.label = 1;
            r8 = com.fossil.Eu7.g(r2, r6, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a6, code lost:
            if (r8 != r11) goto L_0x011e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x011e, code lost:
            r2 = r1;
            r6 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
            return r11;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0142  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0226  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0123 A[EDGE_INSN: B:81:0x0123->B:37:0x0123 ?: BREAK  , SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 666
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapped.HybridCustomizeEditPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public Bi(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(HybridPreset hybridPreset) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe current preset value=" + hybridPreset);
            MutableLiveData mutableLiveData = this.a.f;
            if (hybridPreset != null) {
                mutableLiveData.l(hybridPreset.clone());
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(HybridPreset hybridPreset) {
            a(hybridPreset);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public Ci(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "start - observe selected microApp value=" + str);
            Zb6 zb6 = this.a.j;
            if (str != null) {
                zb6.u4(str);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<M66> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public Di(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(M66 m66) {
            if (m66 != null) {
                this.a.j.e1(m66);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(M66 m66) {
            a(m66);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public Ei(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "update preset status isChanged=" + bool);
            Zb6 zb6 = this.a.j;
            if (bool != null) {
                zb6.s0(bool.booleanValue());
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeEditPresenter a;

        @DexIgnore
        public Fi(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            this.a = hybridCustomizeEditPresenter;
        }

        @DexIgnore
        public final MutableLiveData<M66> a(HybridPreset hybridPreset) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(hybridPreset.getButtons());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microApps=" + arrayList);
            ArrayList arrayList2 = new ArrayList();
            Hl5 hl5 = Hl5.a;
            Wg6.b(hybridPreset, "preset");
            List<Jn5.Ai> c = hl5.c(hybridPreset);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) it.next();
                String component1 = hybridPresetAppSetting.component1();
                MicroApp n = HybridCustomizeEditPresenter.x(this.a).n(hybridPresetAppSetting.component2());
                if (n != null) {
                    String id = n.getId();
                    String icon = n.getIcon();
                    if (icon == null) {
                        icon = "";
                    }
                    arrayList2.add(new N66(id, icon, Um5.d(PortfolioApp.get.instance(), n.getNameKey(), n.getName()), component1, null, 16, null));
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HybridCustomizeEditPresenter", "wrapperPresetTransformations presetChanged microAppsDetail=" + arrayList2);
            MutableLiveData mutableLiveData = this.a.g;
            String id2 = hybridPreset.getId();
            String name = hybridPreset.getName();
            if (name == null) {
                name = "";
            }
            mutableLiveData.l(new M66(id2, name, arrayList2, c, hybridPreset.isActive()));
            return this.a.g;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((HybridPreset) obj);
        }
    }

    @DexIgnore
    public HybridCustomizeEditPresenter(Zb6 zb6, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase) {
        Wg6.c(zb6, "mView");
        Wg6.c(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        this.j = zb6;
        this.k = setHybridPresetToWatchUseCase;
        LiveData<M66> c = Ss0.c(this.f, new Fi(this));
        Wg6.b(c, "Transformations.switchMa\u2026urrentWrapperPreset\n    }");
        this.i = c;
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel x(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        HybridCustomizeViewModel hybridCustomizeViewModel = hybridCustomizeEditPresenter.e;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset e2 = hybridCustomizeViewModel.o().e();
            if (!(e2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", this.h.t(e2));
            }
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset r = hybridCustomizeViewModel2.r();
                if (!(r == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", this.h.t(r));
                }
                HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                if (hybridCustomizeViewModel3 != null) {
                    String e3 = hybridCustomizeViewModel3.t().e();
                    if (!(e3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", e3);
                    }
                    return bundle;
                }
                Wg6.n("mHybridCustomizeViewModel");
                throw null;
            }
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public void B() {
        this.j.M5(this);
    }

    @DexIgnore
    public final void C(HybridPreset hybridPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HybridCustomizeEditPresenter", "updateCurrentPreset=" + hybridPreset);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.B(hybridPreset);
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.get.instance().J())) {
            this.j.y1();
        }
        this.k.r();
        BleCommandResultManager.d.g(CommunicateMode.SET_LINK_MAPPING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> o = hybridCustomizeViewModel.o();
            Zb6 zb6 = this.j;
            if (zb6 != null) {
                o.h((DianaCustomizeEditFragment) zb6, new Bi(this));
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.t().h((LifecycleOwner) this.j, new Ci(this));
                    this.i.h((LifecycleOwner) this.j, new Di(this));
                    HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                    if (hybridCustomizeViewModel3 != null) {
                        hybridCustomizeViewModel3.p().h((LifecycleOwner) this.j, new Ei(this));
                    } else {
                        Wg6.n("mHybridCustomizeViewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
            }
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.k.w();
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MutableLiveData<HybridPreset> o = hybridCustomizeViewModel.o();
            Zb6 zb6 = this.j;
            if (zb6 != null) {
                o.n((DianaCustomizeEditFragment) zb6);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.t().n((LifecycleOwner) this.j);
                    this.g.n((LifecycleOwner) this.j);
                    return;
                }
                Wg6.n("mHybridCustomizeViewModel");
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment");
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void n(String str, String str2) {
        T t;
        Wg6.c(str, "microAppId");
        Wg6.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - microAppid=" + str + ", toPosition=" + str2);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel == null) {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        } else if (!hybridCustomizeViewModel.y(str)) {
            HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
            if (hybridCustomizeViewModel2 != null) {
                HybridPreset e2 = hybridCustomizeViewModel2.o().e();
                if (e2 != null) {
                    Iterator<T> it = e2.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (Wg6.a(next.getPosition(), str2)) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    if (t2 != null) {
                        t2.setAppId(str);
                        t2.setSettings("");
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "dropMicroApp - update preset");
                    Wg6.b(e2, "currentPreset");
                    C(e2);
                    return;
                }
                return;
            }
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void o() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            boolean z = hybridCustomizeViewModel.z();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "onUserExit isPresetChanged " + z);
            if (z) {
                this.j.L();
            } else {
                this.j.t0(false);
            }
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void p(String str) {
        Wg6.c(str, "microAppPos");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            hybridCustomizeViewModel.A(str);
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void q(boolean z) {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset e2 = hybridCustomizeViewModel.o().e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditPresenter", "setPresetToWatch currentPreset=" + e2);
            if (e2 != null) {
                Rm6 unused = Gu7.d(k(), null, null, new Ai(e2, null, this, z, e2), 3, null);
                return;
            }
            return;
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void r(HybridCustomizeViewModel hybridCustomizeViewModel) {
        Wg6.c(hybridCustomizeViewModel, "viewModel");
        this.e = hybridCustomizeViewModel;
    }

    @DexIgnore
    @Override // com.fossil.Yb6
    public void s(String str, String str2) {
        T t;
        T t2;
        Wg6.c(str, "fromPosition");
        Wg6.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!Wg6.a(str, str2)) {
            HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
            if (hybridCustomizeViewModel != null) {
                HybridPreset e2 = hybridCustomizeViewModel.o().e();
                if (e2 != null) {
                    HybridPreset clone = e2.clone();
                    Iterator<T> it = clone.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (Wg6.a(next.getPosition(), str)) {
                            t = next;
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = clone.getButtons().iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        T next2 = it2.next();
                        if (Wg6.a(next2.getPosition(), str2)) {
                            t2 = next2;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.setPosition(str2);
                    }
                    if (t4 != null) {
                        t4.setPosition(str);
                    }
                    FLogger.INSTANCE.getLocal().d("HybridCustomizeEditPresenter", "swapMicroApp - update preset");
                    C(clone);
                    return;
                }
                return;
            }
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
