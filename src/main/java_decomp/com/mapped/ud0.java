package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ud0 {
    UNDEFINED((byte) 0),
    STANDARD((byte) 1),
    SWEEP((byte) 2),
    SEQUENCED((byte) 3),
    PLAY_PAUSE((byte) 4),
    NEXT((byte) 5),
    PREVIOUS((byte) 6),
    VOLUME_UP((byte) 7),
    VOLUME_DOWN((byte) 8),
    TRAVEL((byte) 9),
    ETA((byte) 10);

    @DexIgnore
    public Ud0(byte b2) {
    }
}
