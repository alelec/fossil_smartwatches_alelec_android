package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class E54 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon19 b;
    @DexIgnore
    public /* final */ /* synthetic */ ActivePreset c;

    @DexIgnore
    public /* synthetic */ E54(PresetRepository.Anon19 anon19, ActivePreset activePreset) {
        this.b = anon19;
        this.c = activePreset;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
