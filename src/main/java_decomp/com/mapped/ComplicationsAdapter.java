package com.mapped;

import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ik5;
import com.fossil.Um5;
import com.fossil.Zd5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationsAdapter extends RecyclerView.g<Ai> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<Complication> c;
    @DexIgnore
    public Rect d;
    @DexIgnore
    public ArrayList<Complication> e;
    @DexIgnore
    public Bi f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationsAdapter f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi m;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (m = this.b.f.m()) != null) {
                    m.a(((Complication) this.b.f.e.get(this.b.getAdapterPosition())).getComplicationId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi m;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (m = this.b.f.m()) != null) {
                    m.b(((Complication) this.b.f.e.get(this.b.getAdapterPosition())).getComplicationId());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Cii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                Wg6.c(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ComplicationsAdapter complicationsAdapter, View view) {
            super(view);
            Wg6.c(view, "view");
            this.f = complicationsAdapter;
            View findViewById = view.findViewById(2131363544);
            Wg6.b(findViewById, "view.findViewById(R.id.wc_complication)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363294);
            Wg6.b(findViewById2, "view.findViewById(R.id.tv_complication_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362728);
            this.d = view.findViewById(2131362767);
        }

        @DexIgnore
        public final void a(Complication complication) {
            Wg6.c(complication, "complication");
            this.a.setOnClickListener(new Aii(this));
            this.a.setWatchFaceRect(this.f.d);
            this.d.setOnClickListener(new Bii(this));
            this.e = complication;
            if (complication != null) {
                this.a.O(complication.getComplicationId());
                this.b.setText(Um5.d(PortfolioApp.get.instance(), complication.getNameKey(), complication.getName()));
            }
            this.a.setSelectedWc(Wg6.a(complication.getComplicationId(), this.f.b));
            if (Wg6.a(complication.getComplicationId(), this.f.b)) {
                View view = this.c;
                Wg6.b(view, "ivIndicator");
                view.setBackground(W6.f(PortfolioApp.get.instance(), 2131230956));
            } else {
                View view2 = this.c;
                Wg6.b(view2, "ivIndicator");
                view2.setBackground(W6.f(PortfolioApp.get.instance(), 2131230957));
            }
            View view3 = this.d;
            Wg6.b(view3, "ivWarning");
            view3.setVisibility(!Ik5.d.f(complication.getComplicationId()) ? 0 : 8);
            this.a.M("complication_list", new Cii(this));
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str);

        @DexIgnore
        void b(String str);
    }

    @DexIgnore
    public ComplicationsAdapter(ArrayList<Complication> arrayList, Bi bi) {
        Wg6.c(arrayList, "mData");
        this.e = arrayList;
        this.f = bi;
        this.b = "empty";
        this.c = new ArrayList<>();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ComplicationsAdapter(ArrayList arrayList, Bi bi, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bi);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d("ComplicationsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
        this.a = null;
    }

    @DexIgnore
    public final int l(String str) {
        T t;
        Wg6.c(str, "complicationId");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getComplicationId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.e.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final Bi m() {
        return this.f;
    }

    @DexIgnore
    public final void n(List<Complication> list) {
        Wg6.c(list, "data");
        for (Complication complication : list) {
            this.c.add(Complication.copy$default(complication, null, null, null, null, null, null, null, null, null, 511, null));
        }
        q(list);
    }

    @DexIgnore
    public void o(Ai ai, int i) {
        Wg6.c(ai, "holder");
        if (getItemCount() > i && i != -1) {
            Complication complication = this.e.get(i);
            Wg6.b(complication, "mData[position]");
            ai.a(complication);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        o(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return p(viewGroup, i);
    }

    @DexIgnore
    public Ai p(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Zd5 c2 = Zd5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(c2, "ItemComplicationBinding.\u2026.context), parent, false)");
        ConstraintLayout b2 = c2.b();
        Wg6.b(b2, "view.root");
        return new Ai(this, b2);
    }

    @DexIgnore
    public final void q(List<Complication> list) {
        this.e.clear();
        this.e.addAll(list);
        v();
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void r(Rect rect) {
        Wg6.c(rect, "rect");
        this.d = rect;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void s(Bi bi) {
        Wg6.c(bi, "listener");
        this.f = bi;
    }

    @DexIgnore
    public final void t(String str) {
        if (str == null) {
            str = "empty";
        }
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void u(HashSet<String> hashSet) {
        Wg6.c(hashSet, "selectedComplications");
        ArrayList<Complication> arrayList = this.c;
        List<Complication> arrayList2 = new ArrayList<>();
        for (Complication complication : arrayList) {
            Complication complication2 = complication;
            if (!(hashSet.contains(complication2.getComplicationId()) && (Wg6.a(complication2.getComplicationId(), this.b) ^ true))) {
                arrayList2.add(complication);
            }
        }
        q(arrayList2);
    }

    @DexIgnore
    public final void v() {
        T t;
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getComplicationId(), "empty")) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            Collections.swap(this.e, this.e.indexOf(t2), 0);
        }
    }
}
