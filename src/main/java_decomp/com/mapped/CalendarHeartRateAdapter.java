package com.mapped;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.P47;
import com.fossil.Pm7;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalendarHeartRateAdapter extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public Calendar a;
    @DexIgnore
    public Calendar b;
    @DexIgnore
    public Calendar c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public /* final */ TreeMap<Long, Integer> f; // = new TreeMap<>();
    @DexIgnore
    public /* final */ int[] g; // = new int[49];
    @DexIgnore
    public int h;
    @DexIgnore
    public RecyclerViewHeartRateCalendar.a i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Calendar o; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ Context p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ CalendarHeartRateAdapter d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(CalendarHeartRateAdapter calendarHeartRateAdapter, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.d = calendarHeartRateAdapter;
            View findViewById = view.findViewById(2131362342);
            Wg6.b(findViewById, "itemView.findViewById(R.id.ftvDay)");
            this.b = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362652);
            Wg6.b(findViewById2, "itemView.findViewById(R.id.ivBackground)");
            this.c = (ImageView) findViewById2;
            view.setOnClickListener(this);
        }

        @DexIgnore
        public static /* synthetic */ void c(Ai ai, ImageView imageView, int i, boolean z, int i2, Object obj) {
            if ((i2 & 4) != 0) {
                z = false;
            }
            ai.b(imageView, i, z);
        }

        @DexIgnore
        public final void a(int i) {
            Integer num;
            if (i != -1) {
                Object clone = this.d.i().clone();
                if (clone != null) {
                    Calendar calendar = (Calendar) clone;
                    int i2 = calendar.get(7) - 1;
                    int i3 = this.d.l()[i % 49];
                    if (i3 < i2 || i3 >= calendar.getActualMaximum(5) + i2) {
                        this.b.setVisibility(4);
                        this.c.setVisibility(4);
                        return;
                    }
                    calendar.add(5, i3 - i2);
                    if (this.d.u() == -1) {
                        Calendar t = this.d.t();
                        if (t == null) {
                            Wg6.i();
                            throw null;
                        } else if (TimeUtils.m0(t.getTime(), calendar.getTime())) {
                            this.d.C(i);
                            FlexibleTextView flexibleTextView = this.b;
                            flexibleTextView.setTypeface(flexibleTextView.getTypeface(), 1);
                            this.b.setVisibility(0);
                            this.c.setVisibility(4);
                            this.b.setText(String.valueOf(calendar.get(5)));
                            this.b.setTextColor(this.d.s());
                            TreeMap<Long, Integer> p = this.d.p();
                            TimeUtils.T(calendar);
                            Wg6.b(calendar, "DateHelper.getStartOfDay(calendar)");
                            num = p.get(Long.valueOf(calendar.getTimeInMillis()));
                            if (num != null && num.intValue() > 0) {
                                this.b.setText(String.valueOf(num.intValue()));
                                this.b.setTextColor(this.d.q());
                                this.c.setVisibility(0);
                                int m = num.intValue() > this.d.k() ? this.d.m() : this.d.o();
                                Boolean p0 = TimeUtils.p0(calendar.getTime());
                                Wg6.b(p0, "DateHelper.isToday(calendar.time)");
                                if (p0.booleanValue()) {
                                    b(this.c, m, true);
                                    return;
                                } else {
                                    c(this, this.c, m, false, 4, null);
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    }
                    FlexibleTextView flexibleTextView2 = this.b;
                    flexibleTextView2.setTypeface(flexibleTextView2.getTypeface(), 0);
                    this.b.setVisibility(0);
                    this.c.setVisibility(4);
                    this.b.setText(String.valueOf(calendar.get(5)));
                    this.b.setTextColor(this.d.s());
                    TreeMap<Long, Integer> p2 = this.d.p();
                    TimeUtils.T(calendar);
                    Wg6.b(calendar, "DateHelper.getStartOfDay(calendar)");
                    num = p2.get(Long.valueOf(calendar.getTimeInMillis()));
                    if (num != null) {
                        return;
                    }
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.util.Calendar");
            }
        }

        @DexIgnore
        public final void b(ImageView imageView, int i, boolean z) {
            Drawable background = imageView.getBackground();
            if (background != null) {
                LayerDrawable layerDrawable = (LayerDrawable) background;
                try {
                    Drawable drawable = layerDrawable.getDrawable(0);
                    if (drawable != null) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        if (z) {
                            gradientDrawable.setStroke((int) P47.b(2.0f), this.d.n());
                        } else {
                            gradientDrawable.setStroke((int) P47.b(2.0f), 0);
                        }
                        if (layerDrawable.getNumberOfLayers() > 1) {
                            Drawable drawable2 = layerDrawable.getDrawable(1);
                            if (drawable2 != null) {
                                ((GradientDrawable) drawable2).setColor(i);
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                        }
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CalendarHeartRateAdapter", "DayViewHolder - e=" + e);
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Calendar h;
            Wg6.c(view, "v");
            int adapterPosition = getAdapterPosition();
            if (this.d.r() != null && adapterPosition != -1 && (h = this.d.h(adapterPosition)) != null && !h.before(this.d.v()) && !h.after(this.d.j())) {
                RecyclerViewHeartRateCalendar.a r = this.d.r();
                if (r != null) {
                    r.k0(adapterPosition, h);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ /* synthetic */ CalendarHeartRateAdapter b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CalendarHeartRateAdapter calendarHeartRateAdapter, View view) {
            super(view);
            Wg6.c(view, "itemView");
            this.b = calendarHeartRateAdapter;
            this.a = (FlexibleTextView) view;
        }

        @DexIgnore
        public final void a(int i) {
            String c;
            switch ((i / 7) % 7) {
                case 0:
                    c = Um5.c(this.b.p, 2131886771);
                    Wg6.b(c, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                    break;
                case 1:
                    c = Um5.c(this.b.p, 2131886768);
                    Wg6.b(c, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                    break;
                case 2:
                    c = Um5.c(this.b.p, 2131886773);
                    Wg6.b(c, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                    break;
                case 3:
                    c = Um5.c(this.b.p, 2131886774);
                    Wg6.b(c, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                    break;
                case 4:
                    c = Um5.c(this.b.p, 2131886772);
                    Wg6.b(c, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                    break;
                case 5:
                    c = Um5.c(this.b.p, 2131886769);
                    Wg6.b(c, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                    break;
                case 6:
                    c = Um5.c(this.b.p, 2131886770);
                    Wg6.b(c, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                    break;
                default:
                    c = "";
                    break;
            }
            this.a.setText(c);
        }
    }

    @DexIgnore
    public CalendarHeartRateAdapter(Context context) {
        Wg6.c(context, "mContext");
        this.p = context;
        int[][] iArr = new int[7][];
        for (int i2 = 0; i2 < 7; i2++) {
            iArr[i2] = new int[7];
        }
        for (int i3 = 0; i3 <= 6; i3++) {
            for (int i4 = 0; i4 <= 6; i4++) {
                iArr[i3][i4] = (i3 * 7) + i4;
            }
        }
        int[][] iArr2 = new int[7][];
        for (int i5 = 0; i5 < 7; i5++) {
            iArr2[i5] = new int[7];
        }
        for (int i6 = 0; i6 <= 6; i6++) {
            for (int i7 = 0; i7 <= 6; i7++) {
                iArr2[i6][i7] = iArr[i7][6 - i6];
            }
        }
        for (int i8 = 0; i8 <= 6; i8++) {
            System.arraycopy(iArr2[i8], 0, this.g, (i8 * 7) + 1, 6);
        }
        this.o = TimeUtils.w(0, this.o);
    }

    @DexIgnore
    public final void A(RecyclerViewHeartRateCalendar.a aVar) {
        Wg6.c(aVar, "listener");
        this.i = aVar;
    }

    @DexIgnore
    public final void B(Calendar calendar) {
        Wg6.c(calendar, "currentDate");
        this.c = calendar;
        if (this.a == null) {
            this.a = calendar;
        }
        if (this.b == null) {
            this.b = this.c;
        }
    }

    @DexIgnore
    public final void C(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final void D(Calendar calendar) {
        this.a = calendar;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Calendar calendar;
        if (this.a == null || (calendar = this.b) == null) {
            return 0;
        }
        if (calendar != null) {
            int i2 = calendar.get(1);
            Calendar calendar2 = this.a;
            if (calendar2 != null) {
                int i3 = calendar2.get(1);
                Calendar calendar3 = this.b;
                if (calendar3 != null) {
                    int i4 = calendar3.get(2);
                    Calendar calendar4 = this.a;
                    if (calendar4 != null) {
                        return (((((i2 - i3) * 12) + i4) - calendar4.get(2)) + 1) * 49;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        return i2 % 7 == 0 ? 0 : 1;
    }

    @DexIgnore
    public final Calendar h(int i2) {
        Object clone = this.o.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            int i3 = this.g[i2 % 49];
            int i4 = calendar.get(7) - 1;
            if (i3 < i4 || i3 >= calendar.getActualMaximum(5) + i4) {
                return null;
            }
            calendar.add(5, i3 - i4);
            FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "getCalendarItem day=" + calendar.get(5) + " month=" + calendar.get(2));
            return calendar;
        }
        throw new Rc6("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final Calendar i() {
        return this.o;
    }

    @DexIgnore
    public final Calendar j() {
        return this.b;
    }

    @DexIgnore
    public final int k() {
        return this.d;
    }

    @DexIgnore
    public final int[] l() {
        return this.g;
    }

    @DexIgnore
    public final int m() {
        return this.m;
    }

    @DexIgnore
    public final int n() {
        return this.n;
    }

    @DexIgnore
    public final int o() {
        return this.l;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        Wg6.c(viewHolder, "holder");
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == 0) {
            ((Bi) viewHolder).a(i2);
        } else if (itemViewType == 1) {
            ((Ai) viewHolder).a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        Wg6.c(viewGroup, "parent");
        if (i2 == 0) {
            View inflate = LayoutInflater.from(this.p).inflate(2131558663, viewGroup, false);
            Wg6.b(inflate, "titleView");
            inflate.getLayoutParams().width = this.h;
            return new Bi(this, inflate);
        } else if (i2 != 1) {
            View inflate2 = LayoutInflater.from(this.p).inflate(2131558696, viewGroup, false);
            Wg6.b(inflate2, "itemView");
            inflate2.getLayoutParams().width = this.h;
            return new Ai(this, inflate2);
        } else {
            View inflate3 = LayoutInflater.from(this.p).inflate(2131558696, viewGroup, false);
            Wg6.b(inflate3, "itemView");
            inflate3.getLayoutParams().width = this.h;
            return new Ai(this, inflate3);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Integer> p() {
        return this.f;
    }

    @DexIgnore
    public final int q() {
        return this.j;
    }

    @DexIgnore
    public final RecyclerViewHeartRateCalendar.a r() {
        return this.i;
    }

    @DexIgnore
    public final int s() {
        return this.k;
    }

    @DexIgnore
    public final Calendar t() {
        return this.c;
    }

    @DexIgnore
    public final int u() {
        return this.e;
    }

    @DexIgnore
    public final Calendar v() {
        return this.a;
    }

    @DexIgnore
    public final void w(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.j = i2;
        this.k = i3;
        this.l = i5;
        this.m = i6;
        this.n = i7;
    }

    @DexIgnore
    public final void x(Map<Long, Integer> map, int i2, int i3, Calendar calendar) {
        int i4;
        int i5 = 0;
        Wg6.c(map, "data");
        Wg6.c(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "setData");
        this.f.putAll(map);
        TreeMap<Long, Integer> treeMap = this.f;
        if (treeMap.isEmpty()) {
            i4 = 0;
        } else {
            int i6 = 0;
            for (Map.Entry<Long, Integer> entry : treeMap.entrySet()) {
                if (entry.getValue().intValue() > 0) {
                    i6++;
                }
            }
            i4 = i6;
        }
        TreeMap<Long, Integer> treeMap2 = this.f;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<Long, Integer> entry2 : treeMap2.entrySet()) {
            if (entry2.getValue().intValue() > 0) {
                linkedHashMap.put(entry2.getKey(), entry2.getValue());
            }
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.size());
        for (Map.Entry entry3 : linkedHashMap.entrySet()) {
            arrayList.add(Integer.valueOf(((Number) entry3.getValue()).intValue()));
        }
        int c0 = Pm7.c0(arrayList);
        if (i4 > 0) {
            i5 = c0 / i4;
        }
        this.d = i5;
        this.o = calendar;
    }

    @DexIgnore
    public final void y(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void z(Calendar calendar) {
        this.b = calendar;
    }
}
