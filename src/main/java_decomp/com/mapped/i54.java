package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class I54 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository b;
    @DexIgnore
    public /* final */ /* synthetic */ RecommendedPreset c;

    @DexIgnore
    public /* synthetic */ I54(PresetRepository presetRepository, RecommendedPreset recommendedPreset) {
        this.b = presetRepository;
        this.c = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
