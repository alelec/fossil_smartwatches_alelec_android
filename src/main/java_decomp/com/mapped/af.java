package com.mapped;

import com.fossil.Bu0;
import com.fossil.Ku0;
import com.fossil.Vt0;
import com.mapped.Xe;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Af<Key, Value> extends Vt0<Key, Value> {
    @DexIgnore
    public /* final */ Object mKeyLock; // = new Object();
    @DexIgnore
    public Key mNextKey; // = null;
    @DexIgnore
    public Key mPreviousKey; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<Key, Value> extends Ai<Key, Value> {
        @DexIgnore
        public /* final */ Xe.Di<Value> a;
        @DexIgnore
        public /* final */ Af<Key, Value> b;

        @DexIgnore
        public Bi(Af<Key, Value> af, int i, Executor executor, Bu0.Ai<Value> ai) {
            this.a = new Xe.Di<>(af, i, executor, ai);
            this.b = af;
        }

        @DexIgnore
        @Override // com.mapped.Af.Ai
        public void a(List<Value> list, Key key) {
            if (!this.a.a()) {
                if (this.a.a == 1) {
                    this.b.setNextKey(key);
                } else {
                    this.b.setPreviousKey(key);
                }
                this.a.b(new Bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<Key, Value> {
        @DexIgnore
        public abstract void a(List<Value> list, Key key, Key key2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<Key, Value> extends Ci<Key, Value> {
        @DexIgnore
        public /* final */ Xe.Di<Value> a;
        @DexIgnore
        public /* final */ Af<Key, Value> b;

        @DexIgnore
        public Di(Af<Key, Value> af, boolean z, Bu0.Ai<Value> ai) {
            this.a = new Xe.Di<>(af, 0, null, ai);
            this.b = af;
        }

        @DexIgnore
        @Override // com.mapped.Af.Ci
        public void a(List<Value> list, Key key, Key key2) {
            if (!this.a.a()) {
                this.b.initKeys(key, key2);
                this.a.b(new Bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<Key> {
        @DexIgnore
        public Ei(int i, boolean z) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi<Key> {
        @DexIgnore
        public /* final */ Key a;

        @DexIgnore
        public Fi(Key key, int i) {
            this.a = key;
        }
    }

    @DexIgnore
    private Key getNextKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mNextKey;
        }
        return key;
    }

    @DexIgnore
    private Key getPreviousKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mPreviousKey;
        }
        return key;
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
        Key nextKey = getNextKey();
        if (nextKey != null) {
            loadAfter(new Fi<>(nextKey, i2), new Bi(this, 1, executor, ai));
        } else {
            ai.a(1, Bu0.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
        Key previousKey = getPreviousKey();
        if (previousKey != null) {
            loadBefore(new Fi<>(previousKey, i2), new Bi(this, 2, executor, ai));
        } else {
            ai.a(2, Bu0.a());
        }
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, Bu0.Ai<Value> ai) {
        Di di = new Di(this, z, ai);
        loadInitial(new Ei<>(i, z), di);
        di.a.c(executor);
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final Key getKey(int i, Value value) {
        return null;
    }

    @DexIgnore
    public void initKeys(Key key, Key key2) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
            this.mNextKey = key2;
        }
    }

    @DexIgnore
    public abstract void loadAfter(Fi<Key> fi, Ai<Key, Value> ai);

    @DexIgnore
    public abstract void loadBefore(Fi<Key> fi, Ai<Key, Value> ai);

    @DexIgnore
    public abstract void loadInitial(Ei<Key> ei, Ci<Key, Value> ci);

    @DexIgnore
    @Override // com.mapped.Xe
    public final <ToValue> Af<Key, ToValue> map(V3<Value, ToValue> v3) {
        return mapByPage((V3) Xe.createListFunction(v3));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public final <ToValue> Af<Key, ToValue> mapByPage(V3<List<Value>, List<ToValue>> v3) {
        return new Ku0(this, v3);
    }

    @DexIgnore
    public void setNextKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mNextKey = key;
        }
    }

    @DexIgnore
    public void setPreviousKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
        }
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public boolean supportsPageDropping() {
        return false;
    }
}
