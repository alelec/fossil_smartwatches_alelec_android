package com.mapped;

import com.fossil.Ey1;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum K70 {
    SUNDAY((byte) 1, "Sun"),
    MONDAY((byte) 2, "Mon"),
    TUESDAY((byte) 4, "Tue"),
    WEDNESDAY((byte) 8, "Wed"),
    THURSDAY(DateTimeFieldType.CLOCKHOUR_OF_DAY, "Thu"),
    FRIDAY((byte) 32, "Fri"),
    SATURDAY((byte) 64, "Sat");
    
    @DexIgnore
    public static /* final */ Ai e; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Set<K70> a(byte b) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            K70[] values = K70.values();
            for (K70 k70 : values) {
                if (((byte) (k70.b() & b)) == k70.b()) {
                    linkedHashSet.add(k70);
                }
            }
            return linkedHashSet;
        }

        @DexIgnore
        public final K70[] b(String[] strArr) {
            K70 k70;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                K70[] values = K70.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        k70 = null;
                        break;
                    }
                    k70 = values[i];
                    if (Wg6.a(Ey1.a(k70), str) || Wg6.a(k70.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (k70 != null) {
                    arrayList.add(k70);
                }
            }
            Object[] array = arrayList.toArray(new K70[0]);
            if (array != null) {
                return (K70[]) array;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public K70(byte b2, String str) {
        this.b = (byte) b2;
        this.c = str;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final byte b() {
        return this.b;
    }
}
