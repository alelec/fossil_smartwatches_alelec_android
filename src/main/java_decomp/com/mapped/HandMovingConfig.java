package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Ox1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HandMovingConfig extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ N50 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ P50 d;
    @DexIgnore
    public /* final */ Q50 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<HandMovingConfig> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public HandMovingConfig createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                N50 valueOf = N50.valueOf(readString);
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    P50 valueOf2 = P50.valueOf(readString2);
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        Wg6.b(readString3, "parcel.readString()!!");
                        return new HandMovingConfig(valueOf, readInt, valueOf2, Q50.valueOf(readString3));
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public HandMovingConfig[] newArray(int i) {
            return new HandMovingConfig[i];
        }
    }

    @DexIgnore
    public HandMovingConfig(N50 n50, int i, P50 p50, Q50 q50) {
        this.b = n50;
        this.c = i;
        this.d = p50;
        this.e = q50;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).putShort((short) this.c).put(this.d.a()).put(this.e.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(HandMovingConfig.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            HandMovingConfig handMovingConfig = (HandMovingConfig) obj;
            if (this.b != handMovingConfig.b) {
                return false;
            }
            if (this.c != handMovingConfig.c) {
                return false;
            }
            if (this.d != handMovingConfig.d) {
                return false;
            }
            return this.e == handMovingConfig.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.calibration.HandMovingConfig");
    }

    @DexIgnore
    public final int getDegree() {
        return this.c;
    }

    @DexIgnore
    public final N50 getHandId() {
        return this.b;
    }

    @DexIgnore
    public final P50 getMovingDirection() {
        return this.d;
    }

    @DexIgnore
    public final Q50 getMovingSpeed() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int i = this.c;
        return (((((hashCode * 31) + i) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.X0, Ey1.a(this.b)), Jd0.Y0, Integer.valueOf(this.c)), Jd0.Z0, Ey1.a(this.d)), Jd0.a1, Ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
