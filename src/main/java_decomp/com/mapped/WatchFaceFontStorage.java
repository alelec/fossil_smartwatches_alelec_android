package com.mapped;

import android.graphics.Typeface;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.Pm7;
import com.fossil.Qq7;
import com.fossil.Wt7;
import com.fossil.Yk7;
import com.fossil.Zk7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceFontStorage {
    @DexIgnore
    public static /* final */ Map<String, Typeface> a; // = new LinkedHashMap();
    @DexIgnore
    public static /* final */ Yk7 b; // = Zk7.a(Ai.INSTANCE);
    @DexIgnore
    public static /* final */ Yk7 c; // = Zk7.a(Bi.INSTANCE);
    @DexIgnore
    public static /* final */ WatchFaceFontStorage d; // = new WatchFaceFontStorage();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<String> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final String invoke() {
            WatchFaceFontStorage watchFaceFontStorage = WatchFaceFontStorage.d;
            return (String) Pm7.G(WatchFaceFontStorage.a.keySet());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Gg6<Typeface> {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();

        @DexIgnore
        public Bi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Typeface invoke() {
            WatchFaceFontStorage watchFaceFontStorage = WatchFaceFontStorage.d;
            return (Typeface) Pm7.G(WatchFaceFontStorage.a.values());
        }
    }

    @DexIgnore
    public final void b() {
        a.clear();
    }

    @DexIgnore
    public final String c() {
        return (String) b.getValue();
    }

    @DexIgnore
    public final Typeface d() {
        return (Typeface) c.getValue();
    }

    @DexIgnore
    public final boolean e() {
        return !a.isEmpty();
    }

    @DexIgnore
    public final List<Typeface> f() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<String, Typeface> entry : a.entrySet()) {
            arrayList.add(entry.getValue());
        }
        return arrayList;
    }

    @DexIgnore
    public final void g() {
        PortfolioApp instance = PortfolioApp.get.instance();
        String[] list = instance.getAssets().list("theme/fonts");
        if (list != null) {
            for (String str : list) {
                String[] list2 = instance.getAssets().list("theme/fonts/" + str);
                if (list2 != null) {
                    for (String str2 : list2) {
                        Typeface createFromAsset = Typeface.createFromAsset(instance.getAssets(), "theme/fonts/" + str + '/' + str2);
                        Wg6.b(str2, "fontFile");
                        int G = Wt7.G(str2, CodelessMatcher.CURRENT_CLASS_NAME, 0, false, 6, null);
                        if (str2 != null) {
                            String substring = str2.substring(0, G);
                            Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            Map<String, Typeface> map = a;
                            Wg6.b(createFromAsset, "typeface");
                            map.put(substring, createFromAsset);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    continue;
                }
            }
        }
        FLogger.INSTANCE.getLocal().d("WatchFaceFontStorage", "init " + a);
    }

    @DexIgnore
    public final String h(Typeface typeface) {
        for (Map.Entry<String, Typeface> entry : a.entrySet()) {
            String key = entry.getKey();
            if (Wg6.a(a.get(key), typeface)) {
                return key;
            }
        }
        return c();
    }

    @DexIgnore
    public final Typeface i(String str) {
        Typeface typeface = a.get(str);
        return typeface != null ? typeface : d();
    }
}
