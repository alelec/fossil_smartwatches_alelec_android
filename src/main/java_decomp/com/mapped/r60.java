package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.An1;
import com.fossil.Bn1;
import com.fossil.D90;
import com.fossil.Dn1;
import com.fossil.En1;
import com.fossil.Fn1;
import com.fossil.G80;
import com.fossil.Gn1;
import com.fossil.Jd0;
import com.fossil.Jn1;
import com.fossil.Mm1;
import com.fossil.Om1;
import com.fossil.Ox1;
import com.fossil.Pm1;
import com.fossil.Qm1;
import com.fossil.Rm1;
import com.fossil.Sm1;
import com.fossil.Tm1;
import com.fossil.Um1;
import com.fossil.Vm1;
import com.fossil.Wm1;
import com.fossil.Xm1;
import com.fossil.Z8;
import com.fossil.Zm1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class R60 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Zm1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<R60> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public R60 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Zm1 valueOf = Zm1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (Z8.a[valueOf.ordinal()]) {
                    case 1:
                        return H60.CREATOR.b(parcel);
                    case 2:
                        return Vm1.CREATOR.b(parcel);
                    case 3:
                        return Wm1.CREATOR.b(parcel);
                    case 4:
                        return Rm1.CREATOR.b(parcel);
                    case 5:
                        return Sm1.CREATOR.b(parcel);
                    case 6:
                        return Xm1.CREATOR.b(parcel);
                    case 7:
                        return Qm1.CREATOR.b(parcel);
                    case 8:
                        return Tm1.CREATOR.b(parcel);
                    case 9:
                        return En1.CREATOR.b(parcel);
                    case 10:
                        return A70.CREATOR.b(parcel);
                    case 11:
                        return Bn1.CREATOR.b(parcel);
                    case 12:
                        return Gn1.CREATOR.b(parcel);
                    case 13:
                        return Mm1.CREATOR.b(parcel);
                    case 14:
                        return V60.CREATOR.b(parcel);
                    case 15:
                        return Um1.CREATOR.b(parcel);
                    case 16:
                        return An1.CREATOR.b(parcel);
                    case 17:
                        return Fn1.CREATOR.b(parcel);
                    case 18:
                        return Om1.CREATOR.b(parcel);
                    case 19:
                        return Dn1.CREATOR.b(parcel);
                    case 20:
                        return Jn1.CREATOR.b(parcel);
                    case 21:
                        return Pm1.CREATOR.b(parcel);
                    default:
                        throw new Kc6();
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public R60[] newArray(int i) {
            return new R60[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public R60(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0013
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Zm1 r0 = com.fossil.Zm1.valueOf(r0)
            r2.<init>(r0)
            return
        L_0x0013:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.R60.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public R60(Zm1 zm1) {
        this.b = zm1;
    }

    @DexIgnore
    public final byte[] a() {
        ByteBuffer allocate = ByteBuffer.allocate(b().length + 3);
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b.a()).put((byte) b().length).array();
        Wg6.b(array, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        byte[] array2 = allocate.put(array).put(b()).array();
        Wg6.b(array2, "ByteBuffer.allocate(ITEM\u2026                 .array()");
        return array2;
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public abstract Object c();

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((R60) obj).b;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DeviceConfigItem");
    }

    @DexIgnore
    public final Zm1 getKey() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public final JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.y0, this.b.b());
            G80.k(jSONObject, Jd0.z0, c());
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }
}
