"""
java -jar apktool-cli-all_b05f19b.jar d .\Fossil_Smartwatches_v4.1.3.apk
python3 smali_patch.py
java -jar apktool-cli-all_b05f19b.jar b --use-aapt2 -o .\Fossil_Smartwatches_v4.1.3.apk .\Fossil_Smartwatches_v4.1.3
%USERPROFILE%\AppData\Local\Android\sdk\build-tools\28.0.1\apksigner.bat sign --ks ..\fossil_alelec.keystore --out Fossil_Smartwatches_v4.1.3.apk Fossil_Smartwatches_v4.1.3.apk
"""

import sys
import os, re
import shutil
import fileinput
from pathlib import Path

manifest = Path(r'Fossil_Smartwatches_v4.1.3\AndroidManifest.xml')
manifest_text = manifest.read_text()
manifest_text = re.sub(r'^( +)(.* android:foregroundServiceType=.*)', r'\1<!-- \2 -->', manifest_text, flags=re.MULTILINE)
manifest.write_text(manifest_text)

for root, dirs, files in os.walk("./Fossil_Smartwatches_v4.1.3"):
    # print(root)
    # remove = []
    # for d in dirs:
    #     if len(Path(root).parts) == 3 and Path(root).parts[2] == 'com':
    #         if d in ('nokia', 'here'):
    #             remove.append(d)
    #             path = os.path.join(root, d)
    #             print("deleting", path)
    #             shutil.rmtree(path)
    # for d in remove:
    #     dirs.remove(d)

    for name in files:

        fpath = os.path.join(root, name)
        fname = origname = Path(fpath)

        sfpath = str(fpath).replace('\\', '/')

        if 'com/misfit/' not in sfpath and 'com/portfolio/' not in sfpath and 'com/fossil/' not in sfpath:
            # print('-', f)
            continue

        if not fpath.endswith('.smali'):
            # print('-', f)
            continue
       
        print(fname.name)

        # if fpath.endswith('com/navdy/obd/Pids.smali'):
        #     fname = fname.rename(fname.parent / "PidsOrig.smali")

        #     text = target.read_text()
        #     text = text.replace("obd/Pids;", "obd/PidsOrig;")
        #     text = text.replace('"Pids.java"', '"PidsOrig.java"')
        #     target.write_text(text)
        #     continue

        if re.search(r'\$\d', fname.name):
            target = fname.with_name(fname.name.replace('$', '$Anon'))
            
            print(fname, "->", target)
            fname.rename(target)
            
        else:
            target = fname

        text = target.read_text()

        def add_anon(match):
            return "$Anon" + match.group(1)

        text = re.sub(r'\$(\d+)', add_anon, text)

        target.write_text(text)
